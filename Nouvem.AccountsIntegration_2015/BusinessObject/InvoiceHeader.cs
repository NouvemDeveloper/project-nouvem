﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceHeader.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class InvoiceHeader
    {
        /// <summary>
        /// Gets or sets the invoice lines.
        /// </summary>
        public IList<InvoiceDetail> Details { get; set; }

        /// <summary>
        /// Gets or sets the partner code.
        /// </summary>
        public string TradingPartnerCode { get; set; }

        /// <summary>
        /// Gets or sets the partner name.
        /// </summary>
        public string TradingPartnerName { get; set; }

        /// <summary>
        /// The customer order no.
        /// </summary>
        public string CustomerOrderNo { get; set; }

        /// <summary>
        /// The sale order no.
        /// </summary>
        public string SaleOrderNo { get; set; }

        /// <summary>
        /// The dispatch docket no.
        /// </summary>
        public string DispatchDocketNo { get; set; }

        /// <summary>
        /// The customer order no.
        /// </summary>
        public string SalesPersonCode { get; set; }

        /// <summary>
        /// Gets or sets the nouvem invoice no.
        /// </summary>
        public int InvoiceNo { get; set; }

         /// <summary>
        /// Gets or sets a value indicating whether tas books invoicing numbers are to be used.
        /// </summary>
        public bool UseTASInvoiceNumbering { get; set; }

        /// <summary>
        /// Gets or sets the invoice creation date.
        /// </summary>
        public DateTime InvoiceDate { get; set; }

        /// <summary>
        /// Gets or sets the vat.
        /// </summary>
        public decimal Vat { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string AddressLine3 { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string AddressLine4 { get; set; }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string DeliveryAddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the del address.
        /// </summary>
        public string DeliveryAddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the del address.
        /// </summary>
        public string DeliveryAddressLine3 { get; set; }

        /// <summary>
        /// Gets or sets the del address.
        /// </summary>
        public string DeliveryAddressLine4 { get; set; }

        /// <summary>
        /// Sets the description.
        /// </summary>
        public string Description { get; set; }
    }
}

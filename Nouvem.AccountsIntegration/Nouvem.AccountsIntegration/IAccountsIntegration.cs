﻿// -----------------------------------------------------------------------
// <copyright file="IAccountsIntegration.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration
{
    using Nouvem.AccountsIntegration.BusinessObject;
    using System;
    using System.Collections.Generic;

    public interface IAccountsIntegration
    {
        /// <summary>
        /// Create accounts db connection.
        /// </summary>
        /// <param name="path">The db path.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string Connect(string path, string userName, string password);

        /// <summary>
        /// Release resource.
        /// </summary>
        void Logout();

        /// <summary>
        /// Posts the sale order invoice.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostSalesInvoice(InvoiceHeader header);

        /// <summary>
        /// Posts the sale order .
        /// </summary>
        /// <param name="header">The sale order data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostSaleOrder(InvoiceHeader header);

        /// <summary>
        /// Posts the sale credit note.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostSalesCreditNote(BusinessObject.InvoiceHeader header);

        /// <summary>
        /// Retrieves the partners.
        /// </summary>
        /// <returns>The accounts partners.</returns>
        Tuple<List<BusinessPartner>, string> GetPartners(string path);

        /// <summary>
        /// Retrieves the partners.
        /// </summary>
        /// <returns>The accounts partners.</returns>
        Tuple<List<BusinessObject.Product>, string> GetSpecialPrices(string path);

        /// <summary>
        /// Retrieves the prices.
        /// </summary>
        /// <returns>The accounts prices.</returns>
        Tuple<List<Prices>, string> GetPrices(string path);

        /// <summary>
        /// Exports a purchase order to Sage.
        /// </summary>
        /// <param name="header">The purchase order details.</param>
        /// <returns>Empty string if successful, otherwise error message.</returns>
        string PostPurchaseOrder(BusinessObject.InvoiceHeader header);

        /// <summary>
        /// Post a purchase invoice.
        /// </summary>
        /// <param name="header">The header data.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostPurchaseInvoice(BusinessObject.InvoiceHeader header);

        /// <summary>
        /// Post a purchase invoice.
        /// </summary>
        /// <param name="header">The header data.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostPurchaseInvoiceCredit(InvoiceHeader header);

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns>The products to return.</returns>
        Tuple<List<Product>, string> GetProducts(string directory);

        /// <summary>
        /// Exports application products to Sage.
        /// </summary>
        /// <param name="products">The products to export.</param>
        /// <returns>Empty string if successful, otherwise error message.</returns>
        string ExportProducts(IList<BusinessObject.Product> products);
    }
}

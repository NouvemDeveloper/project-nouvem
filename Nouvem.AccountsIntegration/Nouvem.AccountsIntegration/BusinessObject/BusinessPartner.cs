﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.AccountsIntegration.BusinessObject
{
    public class BusinessPartner
    {
        /// <summary>
        /// Gets or sets the partner name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the partner code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 1.
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 3.
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 3.
        /// </summary>
        public string AddressLine3 { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 4.
        /// </summary>
        public string AddressLine4 { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 4.
        /// </summary>
        public int AccountStatus { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 4.
        /// </summary>
        public decimal AccountBalance { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 4.
        /// </summary>
        public int CreditLimit { get; set; }

        /// <summary>
        /// Gets or sets the postcode.
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// Gets or sets the postcode.
        /// </summary>
        public string PriceBand { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the tel.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the terms.
        /// </summary>
        public string Terms { get; set; }

        /// <summary>
        /// Gets or sets the terms.
        /// </summary>
        public string Analysis1 { get; set; }

        /// <summary>
        /// Gets or sets the account code id.
        /// </summary>
        public string CustomerAccountCodeID { get; set; }

        /// <summary>
        /// Gets or sets the account contact.
        /// </summary>
        public string CustomerAccountsContact { get; set; }

        /// <summary>
        /// Gets or sets the accounts contact email.
        /// </summary>
        public string CustomerAccountsContactEmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the accounts mobile.
        /// </summary>
        public string CustomerAccountsContactMobileTelNumber { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 1.
        /// </summary>
        public string CustomersAccountsContactSalutation { get; set; }

        /// <summary>
        /// Gets or sets the tel.
        /// </summary>
        public string CustomerAccountsContactTelNumber { get; set; }

        /// <summary>
        /// Gets or sets the credit terms.
        /// </summary>
        public string CustomerCreditTerms { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string CustomerNotes { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the postcode.
        /// </summary>
        public string CustomerPostCode { get; set; }

        /// <summary>
        /// Gets or sets the account code id.
        /// </summary>
        public string SupplierAccountCodeID { get; set; }

        /// <summary>
        /// Gets or sets the account contact.
        /// </summary>
        public string SupplierAccountsContact { get; set; }

        /// <summary>
        /// Gets or sets the accounts contact email.
        /// </summary>
        public string SupplierAccountsContactEmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the accounts mobile.
        /// </summary>
        public string SupplierAccountsContactMobileTelNumber { get; set; }

        /// <summary>
        /// Gets or sets the adddress line 1.
        /// </summary>
        public string SuppliersAccountsContactSalutation { get; set; }

        /// <summary>
        /// Gets or sets the tel.
        /// </summary>
        public string SupplierAccountsContactTelNumber { get; set; }

        /// <summary>
        /// Gets or sets the credit terms.
        /// </summary>
        public string SupplierCreditTerms { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string SupplierNotes { get; set; }

        /// <summary>
        /// Gets or sets the postcode.
        /// </summary>
        public string SupplierPostCode { get; set; }

        /// <summary>
        /// Gets or sets the sales contact.
        /// </summary>
        public string SalesContact { get; set; }

        /// <summary>
        /// Gets or sets the main contact.
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the sales contact email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the sales contact email.
        /// </summary>
        public string SalesContactEmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the sales contact efax.
        /// </summary>
        public string SalesContactFaxNumber { get; set; }

        /// <summary>
        /// Gets or sets the sales contact mobile.
        /// </summary>
        public string SalesContactMobileTelNumber { get; set; }

        /// <summary>
        /// Gets or sets the sales contact salutation.
        /// </summary>
        public string SalesContactSalutation { get; set; }

        /// <summary>
        /// Gets or sets the sales contact tel.
        /// </summary>
        public string SalesContactTelNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the partner is a customer.
        /// </summary>
        public bool IsCustomer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the partner is a supplier.
        /// </summary>
        public bool IsSupplier { get; set; }
    }
}

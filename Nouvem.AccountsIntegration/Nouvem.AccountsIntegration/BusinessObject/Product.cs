﻿// -----------------------------------------------------------------------
// <copyright file="Product.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration.BusinessObject
{
    public class Product
    {
        /// <summary>
        /// The product code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The product nominal code.
        /// </summary>
        public string NominalCode { get; set; }

        /// <summary>
        /// The product group name.
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// The product group name.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// The product group name.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the product shelf life.
        /// </summary>
        public int ShelfLife { get; set; }

        /// <summary>
        /// The product code.
        /// </summary>
        public string PartnerCode { get; set; }

        /// <summary>
        /// The product name.
        /// </summary>
        public string PartnerName { get; set; }

        /// <summary>
        /// The product code.
        /// </summary>
        public double Price { get; set; }
    }
}



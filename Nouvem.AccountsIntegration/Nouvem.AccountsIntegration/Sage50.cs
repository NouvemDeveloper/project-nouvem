﻿// -----------------------------------------------------------------------
// <copyright file="Sage50.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.AccountsIntegration.BusinessObject;
    using Nouvem.AccountsIntegration.Global;
    using Nouvem.Shared;
    using SageDataObject220;

    public class Sage50 : IAccountsIntegration
    {
        #region field

        /// <summary>
        /// The workspace reference.
        /// </summary>
        private WorkSpace workSpace;

        #endregion

        #region constructor

        #endregion

        /// <summary>
        /// Create accounts db connection.
        /// </summary>
        /// <param name="path">The db path.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string Connect(string path, string userName, string password)
        {
            var dataObjectEngine = new SDOEngine();
            this.workSpace = (WorkSpace)dataObjectEngine.Workspaces.Add("Nouvem2");
            var dataPath = dataObjectEngine.SelectCompany(path);

            try
            {
                //Leaving the username and password blank generates a login dialog
                this.workSpace.Connect(dataPath, userName, password, "Nouvem2");
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Close session and release resource.
        /// </summary>
        public void Logout()
        {
            this.workSpace.Disconnect();
        }

        /// <summary>
        /// Retrieves the partners.
        /// </summary>
        /// <returns>The accounts partners.</returns>
        public Tuple<List<BusinessPartner>, string> GetPartners(string path)
        {
            var localPartners = new List<BusinessPartner>();
            var error = string.Empty;
            var salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");
            var purchaseRecord = (PurchaseRecord)this.workSpace.CreateObject("PurchaseRecord");

            try
            {
                for (int i = 0; i < salesRecord.Count; i++)
                {
                    var code = SDOHelper.Read(salesRecord, "ACCOUNT_REF").ToString();
                    var partner = new BusinessPartner();
                    partner.Name = SDOHelper.Read(salesRecord, "NAME").ToString();
                    partner.Code = code;
                    partner.AddressLine1 = SDOHelper.Read(salesRecord, "ADDRESS_1").ToString();
                    partner.AddressLine2 = SDOHelper.Read(salesRecord, "ADDRESS_2").ToString();
                    partner.AddressLine3 = SDOHelper.Read(salesRecord, "ADDRESS_3").ToString();
                    partner.AddressLine4 = SDOHelper.Read(salesRecord, "ADDRESS_4").ToString();
                    partner.PostCode = SDOHelper.Read(salesRecord, "ADDRESS_5").ToString();
                    partner.ContactName = SDOHelper.Read(salesRecord, "CONTACT_NAME").ToString();
                    partner.Phone = SDOHelper.Read(salesRecord, "TELEPHONE").ToString();
                    partner.Fax = SDOHelper.Read(salesRecord, "FAX").ToString();
                    partner.Terms = SDOHelper.Read(salesRecord, "TERMS").ToString();
                    partner.Analysis1 = SDOHelper.Read(salesRecord, "ANALYSIS_1").ToString();
                    partner.IsCustomer = true;

                    localPartners.Add(partner);
                    salesRecord.MoveNext();
                }

                for (int i = 0; i < purchaseRecord.Count; i++)
                {
                    var partnerRef = SDOHelper.Read(purchaseRecord, "ACCOUNT_REF").ToString();
                    var customer = localPartners.FirstOrDefault(x => x.Code.Equals(partnerRef));
                    if (customer != null)
                    {
                        customer.IsSupplier = true;
                        purchaseRecord.MoveNext();
                        continue;
                    }

                    var partner = new BusinessPartner();
                    partner.Name = SDOHelper.Read(purchaseRecord, "NAME").ToString();
                    partner.Code = SDOHelper.Read(purchaseRecord, "ACCOUNT_REF").ToString();
                    partner.AddressLine1 = SDOHelper.Read(purchaseRecord, "ADDRESS_1").ToString();
                    partner.AddressLine2 = SDOHelper.Read(purchaseRecord, "ADDRESS_2").ToString();
                    partner.AddressLine3 = SDOHelper.Read(purchaseRecord, "ADDRESS_3").ToString();
                    partner.AddressLine4 = SDOHelper.Read(purchaseRecord, "ADDRESS_4").ToString();
                    partner.PostCode = SDOHelper.Read(purchaseRecord, "ADDRESS_5").ToString();
                    partner.ContactName = SDOHelper.Read(purchaseRecord, "CONTACT_NAME").ToString();
                    partner.Phone = SDOHelper.Read(purchaseRecord, "TELEPHONE").ToString();
                    partner.Fax = SDOHelper.Read(purchaseRecord, "FAX").ToString();
                    partner.Terms = SDOHelper.Read(purchaseRecord, "TERMS").ToString();
                    partner.Analysis1 = SDOHelper.Read(purchaseRecord, "ANALYSIS_1").ToString();
                    partner.IsSupplier = true;

                    localPartners.Add(partner);
                    purchaseRecord.MoveNext();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return Tuple.Create(localPartners, error);
        }

        public Tuple<List<BusinessObject.Product>, string> GetProducts(string directory)
        {
            var localProducts = new List<BusinessObject.Product>();
            var error = string.Empty;
            var stockRecord = (StockRecord)this.workSpace.CreateObject("StockRecord");

            try
            {
                for (int i = 0; i < stockRecord.Count; i++)
                {
                    var product = new BusinessObject.Product();
                    product.Code = SDOHelper.Read(stockRecord, "STOCK_CODE").ToString();
                    product.Name = SDOHelper.Read(stockRecord, "DESCRIPTION").ToString();
                    product.NominalCode = SDOHelper.Read(stockRecord, "NOMINAL_CODE").ToString();

                    localProducts.Add(product);
                    stockRecord.MoveNext();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return Tuple.Create(localProducts, error);
        }

        /// <summary>
        /// Retrieves the partners.
        /// </summary>
        /// <returns>The accounts partners.</returns>
        public Tuple<List<BusinessObject.Product>, string> GetSpecialPrices(string path)
        {
            var localPartners = new List<BusinessPartner>();
            var error = string.Empty;
            var salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");

            PriceRecord oPriceRecord = (PriceRecord)this.workSpace.CreateObject("PriceRecord");
            do
            {
                sbyte type;
                type = oPriceRecord.Fields.Item("TYPE").Value;

                if (type == 83)
                {
                    var aa = Convert.ToString(SDOHelper.Read(oPriceRecord, "REFERENCE"));
                    var dd = aa;
                }
                else
                {
                }

            } while (oPriceRecord.MoveNext());

            try
            {
                for (int i = 0; i < salesRecord.Count; i++)
                {
                    var code = SDOHelper.Read(salesRecord, "ACCOUNT_REF").ToString();
                    var partner = new BusinessPartner();
                    partner.Code = code;
                    partner.Name = SDOHelper.Read(salesRecord, "NAME").ToString();
                    localPartners.Add(partner);
                    salesRecord.MoveNext();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            var localProducts = new List<BusinessObject.Product>();
            var stockRecord = (StockRecord)this.workSpace.CreateObject("StockRecord");

            try
            {
                for (int i = 0; i < stockRecord.Count; i++)
                {
                    var product = new BusinessObject.Product();
                    product.Code = SDOHelper.Read(stockRecord, "STOCK_CODE").ToString();
                    product.Name = SDOHelper.Read(stockRecord, "DESCRIPTION").ToString();
                    localProducts.Add(product);
                    stockRecord.MoveNext();
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            var productPricing = new List<BusinessObject.Product>();
            try
            {
                Pricing pricing = null;
                this.workSpace.CreateObject("Pricing");
                foreach (var product in localProducts)
                {
                    foreach (var businessPartner in localPartners)
                    {
                        var price = pricing.CustomerUnitPrice(businessPartner.Code, product.Code);
                        productPricing.Add(new BusinessObject.Product
                        {
                            Code = product.Code, PartnerCode = businessPartner.Code, Name = product.Name, PartnerName = businessPartner.Name, Price = price
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return Tuple.Create(productPricing, error);
        }

        /// <summary>
        /// Exports application products to Sage.
        /// </summary>
        /// <param name="products">The products to export.</param>
        /// <returns>Empty string if successful, otherwise error message.</returns>
        public string ExportProducts(IList<BusinessObject.Product> products)
        {
            try
            {
                //Instantiate Stock Record Object
                var stockRecord = (StockRecord)this.workSpace.CreateObject("StockRecord");

                //Add a new record
                stockRecord.AddNew();

                //Populate the fields
                SDOHelper.Write(stockRecord, "STOCK_CODE", (String)"NEWSTOCK");
                SDOHelper.Write(stockRecord, "DESCRIPTION", (String)"New Stock Item");
                SDOHelper.Write(stockRecord, "ITEM_TYPE", (Byte)StockItemType.sdoNonStockItem);
                SDOHelper.Write(stockRecord, "NOMINAL_CODE", (String)"4000");
                SDOHelper.Write(stockRecord, "UNIT_OF_SALE", (String)"Each");

                //Update the record
                if (!stockRecord.Update())
                {
                    return "Error";
                }
            }

            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Exports a purchase order to Sage.
        /// </summary>
        /// <param name="header">The purchase order details.</param>
        /// <returns>Empty string if successful, otherwise error message.</returns>
        public string PostPurchaseOrder(BusinessObject.InvoiceHeader header)
        {
            try
            {
                //Instantiate objects
                var purchaseRecord = (PurchaseRecord)this.workSpace.CreateObject("PurchaseRecord");
                var stockRecord = (StockRecord)this.workSpace.CreateObject("StockRecord");
                var popPost = (PopPost)this.workSpace.CreateObject("PopPost");

                //Read the first supplier
                purchaseRecord.MoveFirst();

                //Populate the order Header Fields
                SDOHelper.Write(popPost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
                SDOHelper.Write(popPost.Header, "NAME", header.TradingPartnerName);
                SDOHelper.Write(popPost.Header, "ADDRESS_1", header.AddressLine1);
                SDOHelper.Write(popPost.Header, "ADDRESS_2", header.AddressLine2);
                SDOHelper.Write(popPost.Header, "ADDRESS_3", header.AddressLine3);
                SDOHelper.Write(popPost.Header, "ADDRESS_4", header.AddressLine4);
                SDOHelper.Write(popPost.Header, "ADDRESS_5", header.PostCode);
                SDOHelper.Write(popPost.Header, "DEL_ADDRESS_1", header.DeliveryAddressLine1);
                SDOHelper.Write(popPost.Header, "DEL_ADDRESS_2", header.DeliveryAddressLine2);
                SDOHelper.Write(popPost.Header, "DEL_ADDRESS_3", header.DeliveryAddressLine3);
                SDOHelper.Write(popPost.Header, "DEL_ADDRESS_4", header.DeliveryAddressLine4);

                if (!header.UseSageNumbers)
                {
                    // using nouvem invoice numbers
                    SDOHelper.Write(popPost.Header, "ORDER_NUMBER", header.InvoiceNo);
                }

                // 1 = Part Post, 0 = Fully posted
                SDOHelper.Write(popPost.Header, "POSTED_CODE", (Byte)header.PartPostOrder);

                //Populate other header information
                SDOHelper.Write(popPost.Header, "ORDER_DATE", (DateTime)header.InvoiceDate);
                SDOHelper.Write(popPost.Header, "NOTES_1", (string)"");
                SDOHelper.Write(popPost.Header, "NOTES_2", (string)"");
                SDOHelper.Write(popPost.Header, "NOTES_3", (string)"");
                SDOHelper.Write(popPost.Header, "SUPP_ORDER_NUMBER", (String)"");
               
                foreach (var detail in header.Details)
                {
                    var popItem = (PopItem)SDOHelper.Add(popPost.Items);
                    //stockRecord.MoveFirst();
                    SDOHelper.Write(popItem, "STOCK_CODE", detail.ProductCode);
                    SDOHelper.Write(popItem, "DESCRIPTION", detail.Description);
                    SDOHelper.Write(popItem, "QTY_ORDER", detail.Quantity);
                    SDOHelper.Write(popItem, "UNIT_PRICE", detail.UnitPrice.ToDouble());
                    SDOHelper.Write(popItem, "NET_AMOUNT", detail.TotalExVat.ToDouble());
                    SDOHelper.Write(popItem, "TAX_AMOUNT", detail.Vat.ToDouble());
                    SDOHelper.Write(popItem, "COMMENT_1", "");
                    SDOHelper.Write(popItem, "COMMENT_2", "");
                    SDOHelper.Write(popItem, "UNIT_OF_SALE", detail.UnitOfSale);
                    SDOHelper.Write(popItem, "FULL_NET_AMOUNT", detail.TotalIncVat.ToDouble());
                    SDOHelper.Write(popItem, "NOMINAL_CODE", detail.NominalCode);
                }

                //Create and order item
              

                //Read the first stock code
                //stockRecord.MoveFirst();
                //SDOHelper.Write(popItem, "STOCK_CODE", "AFPBLACKEYEBEANS10X900GR");
                //SDOHelper.Write(popItem, "DESCRIPTION", "AFP Black Eye Beans 10 x 900gr");
                //SDOHelper.Write(popItem, "NOMINAL_CODE", "4000");
       

                //Populate other fields required for POP Item
                //From 2015 the update method now wraps internal business logic 
                //that calculates the vat amount if a net amount is given.
                //If you wish to calculate your own Tax values you will need
                //to ensure that you set the TAX_FLAG to 1 and set the TAX_AMOUNT value on the item line
                //***Note if a NVD is set the item line values will be recalculated 
                //regardless of the Tax_Flag being set to 1***
                //SDOHelper.Write(popItem, "QTY_ORDER", (Double)2);
                //SDOHelper.Write(popItem, "UNIT_PRICE", (Double)100);
                //SDOHelper.Write(popItem, "NET_AMOUNT", (Double)200);
                //SDOHelper.Write(popItem, "FULL_NET_AMOUNT", (Double)200);
                //SDOHelper.Write(popItem, "COMMENT_1", (String)"");
                //SDOHelper.Write(popItem, "COMMENT_2", (String)"");
                //SDOHelper.Write(popItem, "UNIT_OF_SALE", (String)"");
                //SDOHelper.Write(popItem, "TAX_RATE", (Double)20);

                if (!popPost.Update())
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "";

        }

        /// <summary>
        /// Post a purchase invoice.
        /// </summary>
        /// <param name="header">The header data.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostPurchaseInvoice(BusinessObject.InvoiceHeader header)
        {
            try
            {
                //Instantiate Objects
                var transactionPost = (TransactionPost)this.workSpace.CreateObject("TransactionPost");
                var purchaseRecord = (PurchaseRecord)this.workSpace.CreateObject("PurchaseRecord");

                //Read the first Supplier
                purchaseRecord.MoveFirst();

                //Populate the Header Fields
                //Note:
                //The ACCOUNT_REF field must be populated with a valid
                //Supplier Account Reference

                SDOHelper.Write(transactionPost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
                SDOHelper.Write(transactionPost.Header, "DATE", header.InvoiceDate);
                SDOHelper.Write(transactionPost.Header, "POSTED_DATE", DateTime.Today);
                SDOHelper.Write(transactionPost.Header, "TYPE", (Byte)TransType.sdoPI);
                SDOHelper.Write(transactionPost.Header, "INV_REF", header.Reference);
                SDOHelper.Write(transactionPost.Header, "DETAILS", header.Description);
                

                //Add a split to the header items collection
                var data = (SplitData)SDOHelper.Add(transactionPost.Items);

                //Populate the split fields
                SDOHelper.Write(data, "TYPE", SDOHelper.Read(transactionPost.Header, "TYPE"));
                SDOHelper.Write(data, "NOMINAL_CODE", (String)"5000");
                SDOHelper.Write(data, "DETAILS", header.Description);
                SDOHelper.Write(data, "TAX_CODE", (Int16)1);
                SDOHelper.Write(data, "NET_AMOUNT", header.Total.ToDouble());
                SDOHelper.Write(data, "TAX_AMOUNT", header.Vat.ToDouble());
                SDOHelper.Write(data, "DATE", header.InvoiceDate);

                //Update the TransactionPost object
                if (!transactionPost.Update())
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }


        /// <summary>
        /// Post a purchase invoice.
        /// </summary>
        /// <param name="header">The header data.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostPurchaseInvoiceCredit(InvoiceHeader header)
        {
            try
            {
                //Instantiate Objects
                var transactionPost = (TransactionPost)this.workSpace.CreateObject("TransactionPost");
                var purchaseRecord = (PurchaseRecord)this.workSpace.CreateObject("PurchaseRecord");

                //Read the first Supplier
                purchaseRecord.MoveFirst();

                //Populate the Header Fields
                //Note:
                //The ACCOUNT_REF field must be populated with a valid
                //Supplier Account Reference

                SDOHelper.Write(transactionPost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
                SDOHelper.Write(transactionPost.Header, "DATE", header.InvoiceDate);
                SDOHelper.Write(transactionPost.Header, "POSTED_DATE", DateTime.Today);
                SDOHelper.Write(transactionPost.Header, "TYPE", (Byte)TransType.sdoPC);
                SDOHelper.Write(transactionPost.Header, "INV_REF", header.Reference);
                SDOHelper.Write(transactionPost.Header, "DETAILS", header.Description);

                //Add a split to the header items collection
                var data = (SplitData)SDOHelper.Add(transactionPost.Items);

                //Populate the split fields
                SDOHelper.Write(data, "TYPE", SDOHelper.Read(transactionPost.Header, "TYPE"));
                SDOHelper.Write(data, "NOMINAL_CODE", (String)"5000");
                SDOHelper.Write(data, "DETAILS", header.Description);
                SDOHelper.Write(data, "TAX_CODE", (Int16)1);
                SDOHelper.Write(data, "NET_AMOUNT", header.Total.ToDouble());
                SDOHelper.Write(data, "TAX_AMOUNT", header.Vat.ToDouble());
                SDOHelper.Write(data, "DATE", header.InvoiceDate);

                //Update the TransactionPost object
                if (!transactionPost.Update())
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
            //try
            //{
            //    //Instantiate Objects
            //    var transactionPost = (TransactionPost)this.workSpace.CreateObject("TransactionPost");
            //    var purchaseRecord = (PurchaseRecord)this.workSpace.CreateObject("PurchaseRecord");

            //    //Read the first Supplier
            //    purchaseRecord.MoveFirst();

            //    //Populate the Header Fields

            //    //Note:
            //    //The ACCOUNT_REF field must be populated with a valid
            //    //Supplier Account Reference
            //    SDOHelper.Write(transactionPost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
            //    SDOHelper.Write(transactionPost.Header, "DATE", DateTime.Today);
            //    SDOHelper.Write(transactionPost.Header, "POSTED_DATE", DateTime.Today);
            //    SDOHelper.Write(transactionPost.Header, "TYPE", (Byte)TransType.sdoPC);
            //    SDOHelper.Write(transactionPost.Header, "INV_REF", header.Reference);

            //    //Add a split to the header items collection
            //    var data = (SplitData)SDOHelper.Add(transactionPost.Items);

            //    //Populate the split fields
            //    SDOHelper.Write(data, "TYPE", SDOHelper.Read(transactionPost.Header, "TYPE"));
            //    SDOHelper.Write(data, "DETAILS", header.Description);
            //    SDOHelper.Write(data, "NET_AMOUNT", header.Total.ToDouble());
            //    SDOHelper.Write(data, "TAX_AMOUNT", header.Vat.ToDouble());
            //    SDOHelper.Write(data, "DATE", DateTime.Today);

            //    //Update the TransactionPost object
            //    if (!transactionPost.Update())
            //    {
            //        return "Error";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}

            //return string.Empty;
        }

        /// <summary>
        /// Posts the sale order invoice.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSalesInvoice(BusinessObject.InvoiceHeader header)
        {
            InvoicePost invoicePost;
            InvoiceItem invoiceItem;
            SalesRecord salesRecord;

            //Try a connection, will throw an exception if it fails
            try
            {
                //Instantiate objects
                salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");
                invoicePost = (InvoicePost)this.workSpace.CreateObject("InvoicePost");

                //Set the invoice type
                invoicePost.Type = (InvoiceType)LedgerType.sdoLedgerInvoice;

                //Read the first customer record and use to populate the invoice fields
                salesRecord.MoveFirst();
                SDOHelper.Write(invoicePost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
                SDOHelper.Write(invoicePost.Header, "NAME", header.TradingPartnerName);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_1", header.AddressLine1);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_2", header.AddressLine2);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_3", header.AddressLine3);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_4", header.AddressLine4);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_5", header.PostCode);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_1", header.DeliveryAddressLine1);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_2", header.DeliveryAddressLine2);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_3", header.DeliveryAddressLine3);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_4", header.DeliveryAddressLine4);
                SDOHelper.Write(invoicePost.Header, "INVOICE_DATE", header.InvoiceDate);
                SDOHelper.Write(invoicePost.Header, "ORDER_NUMBER", header.SaleOrderNo);
                SDOHelper.Write(invoicePost.Header, "CUST_ORDER_NUMBER", header.CustomerOrderNo);
                SDOHelper.Write(invoicePost.Header, "INVOICE_NUMBER", header.InvoiceNo);
                SDOHelper.Write(invoicePost.Header, "NOTES_3", header.DispatchDocketNo);

                // If anything is entered in the GLOBAL_NOM_CODE, all of the updated invoice’s splits will have this nominal code and
                // also this willforce anything entered in the GLOBAL_DETAILS field into the all the splits details field. 
                SDOHelper.Write(invoicePost.Header, "GLOBAL_NOM_CODE", "");
                SDOHelper.Write(invoicePost.Header, "GLOBAL_DETAILS", "");
                SDOHelper.Write(invoicePost.Header, "INVOICE_TYPE_CODE", (Byte)InvoiceType.sdoProductInvoice);

                foreach (var detail in header.Details)
                {
                    invoiceItem = (InvoiceItem)SDOHelper.Add(invoicePost.Items);
                    SDOHelper.Write(invoiceItem, "STOCK_CODE", detail.ProductCode);
                    SDOHelper.Write(invoiceItem, "DESCRIPTION", detail.Description);
                    SDOHelper.Write(invoiceItem, "QTY_ORDER", detail.Quantity);
                    SDOHelper.Write(invoiceItem, "UNIT_PRICE", detail.UnitPrice.ToDouble());
                    SDOHelper.Write(invoiceItem, "NET_AMOUNT", detail.TotalExVat.ToDouble());
                    SDOHelper.Write(invoiceItem, "TAX_AMOUNT", detail.Vat.ToDouble());
                    SDOHelper.Write(invoiceItem, "COMMENT_1", "");
                    SDOHelper.Write(invoiceItem, "COMMENT_2", "");
                    SDOHelper.Write(invoiceItem, "UNIT_OF_SALE", detail.UnitOfSale);
                    SDOHelper.Write(invoiceItem, "FULL_NET_AMOUNT", detail.TotalIncVat.ToDouble());
                    SDOHelper.Write(invoiceItem, "TAX_RATE", 0);
                    SDOHelper.Write(invoiceItem, "NOMINAL_CODE", detail.NominalCode);
                }

                //Update the invoice
                if (!invoicePost.Update())
                {
                    return "Error";
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Posts the sale credit note.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSalesCreditNote(BusinessObject.InvoiceHeader header)
        {
            // Declare required objects
            InvoicePost invoicePost;
            InvoiceItem invoiceItem;
            SalesRecord salesRecord;
            StockRecord stockRecord;

            try
            {
                // Instantiate objects
                salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");
                invoicePost = (InvoicePost)this.workSpace.CreateObject("InvoicePost");
                stockRecord = (StockRecord)this.workSpace.CreateObject("StockRecord");

                // Set the type of the invoice so that we can find the next available number
                invoicePost.Type = (InvoiceType)LedgerType.sdoLedgerCredit;


                // Read the first customer and populate invoice header fields
                salesRecord.MoveFirst();

                SDOHelper.Write(invoicePost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
                SDOHelper.Write(invoicePost.Header, "NAME", header.TradingPartnerName);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_1", header.AddressLine1);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_2", header.AddressLine2);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_3", header.AddressLine3);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_4", header.AddressLine4);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_5", header.PostCode);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_1", header.DeliveryAddressLine1);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_2", header.DeliveryAddressLine2);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_3", header.DeliveryAddressLine3);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_4", header.DeliveryAddressLine4);

                // Populate other header information
                SDOHelper.Write(invoicePost.Header, "INVOICE_DATE", header.InvoiceDate);
                SDOHelper.Write(invoicePost.Header, "ORDER_NUMBER", header.SaleOrderNo);
                SDOHelper.Write(invoicePost.Header, "CUST_ORDER_NUMBER", header.CustomerOrderNo);
                SDOHelper.Write(invoicePost.Header, "PAYMENT_REF", (String)"");
                // If anything is entered in the GLOBAL_NOM_CODE, all of the updated invoice’s splits will have this 
                // nominal code and also this willforce anything entered in the GLOBAL_DETAILS field into the all
                // the splits details field. 
                //SDOHelper.Write(invoicePost.Header, "GLOBAL_NOM_CODE", (String)"");
                //SDOHelper.Write(invoicePost.Header, "GLOBAL_DETAILS", (String)"");
                //SDOHelper.Write(invoicePost.Header, "INVOICE_TYPE_CODE", (Byte)InvoiceType.sdoProductCredit);
                //SDOHelper.Write(invoicePost.Header, "ITEMS_NET", (Double)500);
                //SDOHelper.Write(invoicePost.Header, "ITEMS_TAX", (Double)52.5);

                foreach (var detail in header.Details)
                {
                    invoiceItem = (InvoiceItem)SDOHelper.Add(invoicePost.Items);
                    SDOHelper.Write(invoiceItem, "STOCK_CODE", detail.ProductCode);
                    SDOHelper.Write(invoiceItem, "DESCRIPTION", detail.Description);
                    SDOHelper.Write(invoiceItem, "QTY_ORDER", detail.Quantity);
                    SDOHelper.Write(invoiceItem, "UNIT_PRICE", detail.UnitPrice.ToDouble());
                    SDOHelper.Write(invoiceItem, "NET_AMOUNT", detail.TotalExVat.ToDouble());
                    SDOHelper.Write(invoiceItem, "TAX_AMOUNT", detail.Vat.ToDouble());
                    SDOHelper.Write(invoiceItem, "COMMENT_1", "");
                    SDOHelper.Write(invoiceItem, "COMMENT_2", "");
                    SDOHelper.Write(invoiceItem, "UNIT_OF_SALE", detail.UnitOfSale);
                    SDOHelper.Write(invoiceItem, "FULL_NET_AMOUNT", detail.TotalIncVat.ToDouble());
                    SDOHelper.Write(invoiceItem, "TAX_RATE", 0);
                    SDOHelper.Write(invoiceItem, "NOMINAL_CODE", detail.NominalCode);
                }

                // Update the Invoice
                if (!invoicePost.Update())
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        ///// <summary>
        ///// Posts the sale order invoice.
        ///// </summary>
        ///// <param name="header">The invoice data</param>
        ///// <returns>An empty string if successful, otherwise error message.</returns>
        //public string PostSalesCreditNote(BusinessObject.InvoiceHeader header)
        //{
        //    SalesRecord salesRecord;
        //    TransactionPost transactionPost;
        //    SplitData invoiceSplitData;
        //    SplitData creditSplitData;
        //    HeaderData invoiceHeaderData;
        //    HeaderData creditHeaderData;

        //    // Declare Variables
        //    string szDataPath;
        //    bool recordFound;
        //    bool allocated;
        //    double invoiceValue;
        //    double creditValue;
        //    double allocateValue;
        //    double creditTotalOutstanding;
        //    int splitCount;
        //    double invoiceTotalOutstanding;

        //    // Create Instance Objects to Post the Credit Note
        //    salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");
        //    transactionPost = (TransactionPost)this.workSpace.CreateObject("TransactionPost");
        //    creditHeaderData = (HeaderData)this.workSpace.CreateObject("HeaderData");

        //    // Initialise Index Field with value to search 
        //    SDOHelper.Write(salesRecord, "ACCOUNT_REF", (string)"ABS001");

        //    // Perform Find for an exact match
        //    if (salesRecord.Find(false))
        //    {

        //        // Populate Header fields
        //        // Note:
        //        // The ACCOUNT_REF field must be populated with a valid
        //        // Customer Account Reference

        //        SDOHelper.Write(transactionPost.Header, "ACCOUNT_REF", (String)SDOHelper.Read(salesRecord, "ACCOUNT_REF"));
        //        SDOHelper.Write(transactionPost.Header, "DATE", DateTime.Now);
        //        SDOHelper.Write(transactionPost.Header, "POSTED_DATE", (System.DateTime)DateTime.Today);
        //        SDOHelper.Write(transactionPost.Header, "TYPE", (byte)TransType.sdoSC);
        //        SDOHelper.Write(transactionPost.Header, "DETAILS", (string)"Sales Payment to Allocate");
        //        SDOHelper.Write(transactionPost.Header, "INV_REF", (string)"CRD001");

        //        // Loop for the Number of Splits
        //        // Note:
        //        // The Transaction can have 1 or many splits

        //        for (splitCount = 1; splitCount <= 2; splitCount++)
        //        {

        //            // Add a split to the Header Item's collection
        //            invoiceSplitData = (SplitData)SDOHelper.Add(transactionPost.Items);

        //            // Populate Split Fields 
        //            SDOHelper.Write(invoiceSplitData, "TYPE", (Convert.ToByte(SDOHelper.Read(transactionPost.Header, "TYPE"))));
        //            SDOHelper.Write(invoiceSplitData, "NOMINAL_CODE", (String)"4000");
        //            SDOHelper.Write(invoiceSplitData, "TAX_CODE", (short)1);
        //            SDOHelper.Write(invoiceSplitData, "NET_AMOUNT", (Double)100);
        //            SDOHelper.Write(invoiceSplitData, "TAX_AMOUNT", (Double)17.5);
        //            SDOHelper.Write(invoiceSplitData, "DETAILS", (String)"Split Details");
        //            SDOHelper.Write(invoiceSplitData, "DATE", (System.DateTime)System.DateTime.Today);
        //        }

        //        // Update the TransactionPost Object 
        //        if (transactionPost.Update())
        //        {
        //            // Read the HeaderData for Credit 
        //            creditHeaderData.Read(transactionPost.PostingNumber);

        //            // Create a new Transaction post object  
        //            transactionPost = null;
        //            transactionPost = (SageDataObject230.TransactionPost)this.workSpace.CreateObject("TransactionPost");

        //            //Calculate the total outstanding on the Credit Note  
        //            creditTotalOutstanding = (Double)SDOHelper.Read(creditHeaderData, "NET_AMOUNT") +
        //                ((Double)SDOHelper.Read(creditHeaderData, "TAX_AMOUNT")) -
        //                (Double)SDOHelper.Read(creditHeaderData, "AMOUNT_PAID");

        //            //Link to the Credit Splits 
        //            creditSplitData = (SplitData)creditHeaderData.Link;
        //            creditSplitData.MoveFirst();

        //            //Calculate the Credit Split amount outstanding 
        //            creditValue = (Double)SDOHelper.Read(creditSplitData, "NET_AMOUNT") +
        //                (Double)SDOHelper.Read(creditSplitData, "TAX_AMOUNT") -
        //                (Double)SDOHelper.Read(creditSplitData, "AMOUNT_PAID");

        //            // Link to Invoice Headers  
        //            invoiceHeaderData = (HeaderData)salesRecord.Link;

        //            // Move to the first Header on the Customer account 
        //            invoiceHeaderData.MoveFirst();

        //            // Loop through the Headers until an Invoice that has an outstanding amount is found  
        //            do
        //            {
        //                invoiceTotalOutstanding = (Double)SDOHelper.Read(invoiceHeaderData, "NET_AMOUNT") +
        //                    (Double)SDOHelper.Read(invoiceHeaderData, "TAX_AMOUNT") -
        //                    (Double)SDOHelper.Read(invoiceHeaderData, "AMOUNT_PAID");

        //                if (Convert.ToByte(SDOHelper.Read(invoiceHeaderData, "TYPE")) ==
        //                    Convert.ToByte(SageDataObject230.TransType.sdoSI) &
        //                    invoiceTotalOutstanding > 0)
        //                {

        //                    //Link to Splits 
        //                    invoiceSplitData = (SplitData)invoiceHeaderData.Link;
        //                    invoiceSplitData.MoveFirst();

        //                    // Calculate the Invoice Split amount outstanding 
        //                    invoiceValue = (double)SDOHelper.Read(invoiceSplitData, "NET_AMOUNT") +
        //                        (Double)SDOHelper.Read(invoiceSplitData, "TAX_AMOUNT") -
        //                        (Double)SDOHelper.Read(invoiceSplitData, "AMOUNT_PAID");

        //                    do
        //                    {

        //                        if (Math.Round((Decimal)invoiceValue, 2, MidpointRounding.AwayFromZero) > 0)
        //                        {

        //                            // Calculate the amount to allocate 
        //                            if (invoiceValue > creditValue)
        //                            {
        //                                allocateValue = creditValue;

        //                                // Allocate Credit Against Invoice
        //                                allocated = transactionPost.AllocatePayment((int)invoiceSplitData.RecordNumber,
        //                                    (int)creditSplitData.RecordNumber, (double)allocateValue,
        //                                    (System.DateTime)DateTime.Today);
        //                                creditTotalOutstanding -= allocateValue;
        //                                invoiceValue -= allocateValue;
        //                                if (!creditSplitData.MoveNext())
        //                                {

        //                                    break;
        //                                }
        //                                else
        //                                {
        //                                    creditValue = (Double)SDOHelper.Read(creditSplitData, "NET_AMOUNT") +
        //                                        (Double)SDOHelper.Read(creditSplitData, "TAX_AMOUNT") -
        //                                        (Double)SDOHelper.Read(creditSplitData, "AMOUNT_PAID");
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (invoiceValue < creditValue)
        //                                {
        //                                    allocateValue = invoiceValue;

        //                                    // Allocate Credit Against Invoice
        //                                    allocated = transactionPost.AllocatePayment((int)invoiceSplitData.RecordNumber,
        //                                        (int)creditSplitData.RecordNumber, (double)allocateValue, (System.DateTime)DateTime.Today);
        //                                    creditTotalOutstanding -= allocateValue;
        //                                    creditValue -= allocateValue;
        //                                    if (!invoiceSplitData.MoveNext())
        //                                    {
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        invoiceValue = (Double)SDOHelper.Read(invoiceSplitData, "NET_AMOUNT") +
        //                                            (Double)SDOHelper.Read(invoiceSplitData, "TAX_AMOUNT") -
        //                                            (Double)SDOHelper.Read(invoiceSplitData, "AMOUNT_PAID");
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    allocateValue = invoiceValue;

        //                                    // Allocate Credit Against Invoice
        //                                    allocated = transactionPost.AllocatePayment((int)invoiceSplitData.RecordNumber,
        //                                        (int)creditSplitData.RecordNumber, (double)allocateValue, (System.DateTime)DateTime.Now);
        //                                    if (!creditSplitData.MoveNext())
        //                                    {
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        creditValue = (Double)SDOHelper.Read(creditSplitData, "NET_AMOUNT") +
        //                                            (Double)SDOHelper.Read(creditSplitData, "TAX_AMOUNT") -
        //                                            (Double)SDOHelper.Read(creditSplitData, "AMOUNT_PAID");
        //                                        if (!invoiceSplitData.MoveNext())
        //                                        {
        //                                            break;
        //                                        }
        //                                        else
        //                                        {
        //                                            invoiceValue = (double)SDOHelper.Read(invoiceSplitData, "NET_AMOUNT") +
        //                                                (double)SDOHelper.Read(invoiceSplitData, "TAX_AMOUNT") -
        //                                                (double)SDOHelper.Read(invoiceSplitData, "AMOUNT_PAID");

        //                                        }

        //                                    }

        //                                }

        //                            }
        //                        }
        //                        else
        //                        {
        //                            invoiceSplitData.MoveNext();
        //                            invoiceValue = (Double)SDOHelper.Read(invoiceSplitData, "NET_AMOUNT") +
        //                                (Double)SDOHelper.Read(invoiceSplitData, "TAX_AMOUNT") -
        //                                (Double)SDOHelper.Read(invoiceSplitData, "AMOUNT_PAID");

        //                        }
        //                    }

        //                    while (!(creditTotalOutstanding == 0));

        //                }
        //            }

        //            while (!(!invoiceHeaderData.MoveNext() | creditTotalOutstanding == 0));
        //        }

        //        else
        //        {

        //            // Transaction Post Failed

        //            return string.Empty;
        //        }
        //    }

        //    // Destroy Objects
        //    salesRecord = null;
        //    invoiceHeaderData = null;
        //    invoiceSplitData = null;
        //    creditHeaderData = null;
        //    creditSplitData = null;
        //    transactionPost = null;
        //    return string.Empty;
        //}

        /// <summary>
        /// Posts the sale order .
        /// </summary>
        /// <param name="header">The sale order data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSaleOrder(BusinessObject.InvoiceHeader header)
        {
            // not implemented
            return "Error";
        }

        public Tuple<List<Prices>, string> GetPrices(string path)
        {
            var error = string.Empty;
            var productPricing = new List<BusinessObject.Prices>();
            PriceRecord oPriceRecord = (PriceRecord)this.workSpace.CreateObject("PriceRecord");
            try
            {
                do
                {
                    sbyte type;
                    type = oPriceRecord.Fields.Item("TYPE").Value;
                    if (type == 83)
                    {
                        var reference = Convert.ToString(SDOHelper.Read(oPriceRecord, "REFERENCE"));
                        var price = (double)(SDOHelper.Read(oPriceRecord, "STORED_PRICE"));

                        if (reference != string.Empty)
                        {
                            // remove the special prices s
                            reference = reference.Substring(1, reference.Length - 1);
                            var data = reference.Split(' ');
                            if (data.Length == 3)
                            {
                                productPricing.Add(new BusinessObject.Prices
                                {
                                    Code = data[2],
                                    PartnerCode = data[0],
                                    Price = price.ToDecimal()
                                });
                            }
                            else if (data.Length == 2)
                            {
                                productPricing.Add(new BusinessObject.Prices
                                {
                                    Code = data[1],
                                    PartnerCode = data[0],
                                    Price = price.ToDecimal()
                                });
                            }
                        }
                    }

                } while (oPriceRecord.MoveNext());
            }
            catch (Exception e)
            {
                error = e.Message;
            }
           

            //var localPartners = new List<BusinessPartner>();
            //var error = string.Empty;
            //var salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");

            //try
            //{
            //    for (int i = 0; i < salesRecord.Count; i++)
            //    {
            //        var code = SDOHelper.Read(salesRecord, "ACCOUNT_REF").ToString();
            //        var partner = new BusinessPartner();
            //        partner.Code = code;
            //        partner.Name = SDOHelper.Read(salesRecord, "NAME").ToString();
            //        localPartners.Add(partner);
            //        salesRecord.MoveNext();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    error = ex.Message;
            //}

            ////var localProducts = new List<BusinessObject.Product>();
            //var stockRecord = (StockRecord)this.workSpace.CreateObject("StockRecord");

            //try
            //{
            //    for (int i = 0; i < stockRecord.Count; i++)
            //    {
            //        var product = new BusinessObject.Product();
            //        product.Code = SDOHelper.Read(stockRecord, "STOCK_CODE").ToString();
            //        product.Name = SDOHelper.Read(stockRecord, "DESCRIPTION").ToString();
            //        if (product.Code != "1" && !string.IsNullOrWhiteSpace(product.Name))
            //        {
            //            localProducts.Add(product);
            //        }
                  
            //        stockRecord.MoveNext();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    error = ex.Message;
            //}

            //var productPricing = new List<BusinessObject.Prices>();
            //try
            //{
            //    Pricing pricing = this.workSpace.CreateObject("Pricing");
            //    foreach (var product in localProducts)
            //    {
            //        foreach (var businessPartner in localPartners)
            //        {
            //            var price = pricing.CustomerUnitPrice(businessPartner.Code, product.Code);
            //            if (price > 0)
            //            {
            //                productPricing.Add(new BusinessObject.Prices
            //                {
            //                    Code = product.Code,
            //                    PartnerCode = businessPartner.Code,
            //                    Product = product.Name,
            //                    PartnerName = businessPartner.Name,
            //                    Price = price.ToDecimal()
            //                });
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    error = ex.Message;
            //}

            return Tuple.Create(productPricing, error);
        }
    }
}

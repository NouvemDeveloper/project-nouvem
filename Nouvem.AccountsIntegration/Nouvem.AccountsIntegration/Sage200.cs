﻿// -----------------------------------------------------------------------
// <copyright file="Sage50.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;
using System.Reflection;
//using Sage.Accounting.Common;
//using Sage.Accounting.SalesLedger;
//using Sage.Accounting.SOP;
//using Sage.Accounting.Stock;
//using Sage.Accounting.SystemManager;

namespace Nouvem.AccountsIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.AccountsIntegration.BusinessObject;
    using Nouvem.AccountsIntegration.Global;
    using Nouvem.Shared;
    //using Sage.Accounting;
    //using Sage.ObjectStore;

    public class Sage200 : IAccountsIntegration
    {
        //  #region field

        //  private Application application = new Application();

        //  // Local storage for the sales credit instrument
        //  private Sage.Accounting.SalesLedger.SalesInvoiceInstrument salesInvoiceInstrument;

        //  // Local storage for the customer
        //  private Sage.Accounting.SalesLedger.Customer customer = null;

        //  #endregion

        //  #region constructor

        //  #endregion

         public string ConnectionString { get; set; } 

        //  /// <summary>
        //  /// Create accounts db connection.
        //  /// </summary>
        //  /// <param name="path">The db path.</param>
        //  /// <param name="userName">The user name.</param>
        //  /// <param name="password">The password.</param>
        //  /// <returns>An empty string if successful, otherwise error message.</returns>
        //  public string Connect(string path, string userName, string password)
        //  {
        //      try
        //      {
        //          this.application.Connect();
        //          this.application.ActiveCompany = this.application.Companies[0];
        //      }
        //      catch (Exception ex)
        //      {
        //          return ex.Message;
        //      }

        //      return string.Empty;
        //  }

        //  /// <summary>
        //  /// Close session and release resource.
        //  /// </summary>
        //  public void Logout()
        //  {
        //      this.application.Disconnect();
        //  }

        //  /// <summary>
        //  /// Gets the products.
        //  /// </summary>
        //  /// <returns>The products to return.</returns>
        //  public Tuple<List<Product>, string> GetProducts(string directory)
        //  {
        //      var error = string.Empty;
        //      var products = new List<Product>();
        //      try
        //      {
        //          Sage.Accounting.Stock.StockItems stockItems = new Sage.Accounting.Stock.StockItems();
        //          foreach (StockItem stockItem in stockItems)
        //          {
        //              products.Add(new Product { Name = stockItem.Name, Code = stockItem.Code, ShelfLife = stockItem.ShelfLife, GroupName = stockItem.ProductGroup.Description });
        //          }
        //      }
        //      catch (Exception e)
        //      {
        //          error = e.Message;
        //      }

        //      return Tuple.Create(products, error);
        //  }

        //  /// <summary>
        //  /// Retrieves the partners.
        //  /// </summary>
        //  /// <returns>The accounts partners.</returns>
        //  public Tuple<List<BusinessPartner>, string> GetPartners(string path)
        //  {
        //      var localPartners = new List<BusinessPartner>();
        //      var error = string.Empty;

        //      try
        //      {
        //          var dataResult = new DataTable();
        //          using (var conn = new SqlConnection(this.ConnectionString))
        //          {
        //              conn.Open();
        //              var command = new SqlCommand("App_GetSage200Partners", conn);
        //              command.CommandType = CommandType.StoredProcedure;
        //              var dataReader = command.ExecuteReader();
        //              dataResult.Load(dataReader);
        //              conn.Close();
        //          }
        //          //Sage.Accounting.SalesLedger.Customers customers =
        //          //    Sage.Accounting.SalesLedger.CustomersFactory.Factory.CreateNew();

        //          //var localCustomers = customers.GetList();
        //          foreach (DataRow customer in dataResult.Rows)
        //          {
        //              var partner = new BusinessPartner();
        //              partner.Name = customer["CustomerAccountName"].ToString();
        //              partner.Code = customer["CustomerAccountNumber"].ToString();
        //              partner.Currency = customer["Symbol"].ToString();
        //              partner.CreditLimit = customer["CreditLimit"].ToString().ToDecimal().ToInt();
        //              partner.AccountBalance = customer["AccountBalance"].ToString().ToDecimal();
        //              partner.AccountStatus = customer["OnHold"].ToString().ToInt();
        //              partner.AddressLine1 = customer["AddressLine1"].ToString();
        //              partner.AddressLine2 = customer["AddressLine2"].ToString();
        //              partner.AddressLine3 = customer["AddressLine3"].ToString();
        //              partner.AddressLine4 = customer["AddressLine4"].ToString();
        //              partner.PostCode = customer["PostCode"].ToString();
        //              partner.PriceBand = customer["PriceBand"].ToString();
        //              partner.ContactName = customer["ContactName"].ToString();
        //              partner.Phone = customer["MainTel"].ToString();
        //              //partner.Fax = customer.MainFax;
        //              partner.Analysis1 = "Not Applicable";
        //              partner.IsCustomer = true;

        //              localPartners.Add(partner);
        //          }

        //          //customers.Dispose();
        //          //customers = null;
        //      }
        //      catch (Exception ex)
        //      {
        //          error = ex.Message;
        //      }

        //      return Tuple.Create(localPartners, error);
        //  }

        //  /// <summary>
        //  /// Retrieves the prices.
        //  /// </summary>
        //  /// <returns>The accounts prices.</returns>
        //  public Tuple<List<Prices>, string> GetPrices(string path)
        //  {
        //      var localPrices = new List<Prices>();
        //      var error = string.Empty;

        //      try
        //      {
        //          var dataResult = new DataTable();
        //          using (var conn = new SqlConnection(this.ConnectionString))
        //          {
        //              conn.Open();
        //              var command = new SqlCommand("App_GetSage200Prices", conn);
        //              command.CommandType = CommandType.StoredProcedure;
        //              var dataReader = command.ExecuteReader();
        //              dataResult.Load(dataReader);
        //              conn.Close();
        //          }

        //          foreach (DataRow price in dataResult.Rows)
        //          {
        //              var priceData = new Prices();
        //              priceData.Code = price["Code"].ToString();
        //              priceData.EditDate = price["DateTimeUpdated"].ToString().ToDate();
        //              priceData.Price = price["Price"].ToString().ToDecimal();
        //              priceData.PriceBand= price["PriceBand"].ToString();
        //              priceData.Product = price["Product"].ToString();
        //              localPrices.Add(priceData);
        //          }
        //      }
        //      catch (Exception ex)
        //      {
        //          error = ex.Message;
        //      }

        //      return Tuple.Create(localPrices, error);
        //  }

        //  /// <summary>
        //  /// Post a purchase invoice.
        //  /// </summary>
        //  /// <param name="header">The header data.</param>
        //  /// <returns>An empty string if successful, otherwise error message.</returns>
        //  public string PostPurchaseInvoice(BusinessObject.InvoiceHeader header)
        //  {
        //      Sage.Accounting.SOP.SOPInvoiceOrder invoiceOrder = null;
        //      ActiveLock activeLock = null;

        //      try
        //      {
        //          invoiceOrder = new SOPInvoiceOrder();

        //          invoiceOrder.Customer = CustomerFactory.Factory.Fetch(header.TradingPartnerCode);
        //          invoiceOrder.DocumentDate = DateTime.Today;
        //          invoiceOrder.CustomerDocumentNo = "Order No 1";
        //          invoiceOrder.Update();

        //          activeLock = Sage.Accounting.Application.Lock(invoiceOrder);

        //          foreach (var invoiceDetail in header.Details)
        //          {
        //              StockItems stockItems = new StockItems();
        //              stockItems.Query.Filters.Add(new Sage.ObjectStore.Filter(StockItem.FIELD_CODE, invoiceDetail.ProductCode));
        //              stockItems.Find();
        //              StockItem item = stockItems.First;

        //              Sage.Accounting.Stock.Views.WarehouseStockItemViews warehouseStockItemViews = new Sage.Accounting.Stock.Views.WarehouseStockItemViews();
        //              Sage.Accounting.Stock.Views.WarehouseStockItemView warehouseStockItemView = null;

        //              warehouseStockItemViews.Query.Filters.Add(new Sage.ObjectStore.Filter(Sage.Accounting.Stock.Views.WarehouseStockItemView.FIELD_ITEM, item.PrimaryKey));
        //              warehouseStockItemViews.Query.Filters.Add(new Sage.ObjectStore.Filter("[Warehouse].[Name]", "WAREHOUSE"));
        //              warehouseStockItemViews.Query.Find();

        //              // Find the first warehouse bin that has available stock
        //              foreach (Sage.Accounting.Stock.Views.WarehouseStockItemView view in warehouseStockItemViews)
        //              {
        //                  if (view.FreeStockAvailable > System.Decimal.Zero)
        //                  {
        //                      warehouseStockItemView = view;
        //                      break;
        //                  }
        //              }

        //              SOPStandardItemLine line = new SOPStandardItemLine();
        //              line.SOPOrderReturn = invoiceOrder;
        //              line.Item = stockItems.First;
        //              line.WarehouseItem = warehouseStockItemView.WarehouseItem;

        //              line.LineQuantity = 2;

        //              invoiceOrder.Lines.Add(line);
        //              invoiceOrder.Post(false, true);
        //          }

        //          //AddFreeTextLine(invoiceOrder);
        //          //AddAdditionalChargeLine(invoiceOrder);
        //          // AddCommentLine(invoiceOrder);

        //          invoiceOrder.Confirm(false, true, false);

        //          invoiceOrder.Post(false, true);
        //      }
        //      catch (Exception ex)
        //      {
        //          System.Diagnostics.Debug.WriteLine(ex.Message);

        //          // If the validation of the invoice fails we need to remove it and its lines - since these have already been saved.
        //          invoiceOrder.Cancel();
        //      }
        //      finally
        //      {
        //          // Make sure we unlock the invoice
        //          if (activeLock != null)
        //          {
        //              activeLock.Dispose();
        //          }
        //      }

        //      return string.Empty;
        //  }

        //  private void AddNonTraceableStandardItemLine(SOPInvoiceOrder sopInvoiceOrder)
        //  {
        //      StockItems stockItems = new StockItems();
        //      stockItems.Query.Filters.Add(new Sage.ObjectStore.Filter(StockItem.FIELD_CODE, "ACS/BLENDER"));
        //      stockItems.Find();
        //      StockItem item = stockItems.First;

        //      Sage.Accounting.Stock.Views.WarehouseStockItemViews warehouseStockItemViews = new Sage.Accounting.Stock.Views.WarehouseStockItemViews();
        //      Sage.Accounting.Stock.Views.WarehouseStockItemView warehouseStockItemView = null;

        //      warehouseStockItemViews.Query.Filters.Add(new Sage.ObjectStore.Filter(Sage.Accounting.Stock.Views.WarehouseStockItemView.FIELD_ITEM, item.PrimaryKey));
        //      warehouseStockItemViews.Query.Filters.Add(new Sage.ObjectStore.Filter("[Warehouse].[Name]", "WAREHOUSE"));
        //      warehouseStockItemViews.Query.Find();

        //      // Find the first warehouse bin that has available stock
        //      foreach (Sage.Accounting.Stock.Views.WarehouseStockItemView view in warehouseStockItemViews)
        //      {
        //          if (view.FreeStockAvailable > System.Decimal.Zero)
        //          {
        //              warehouseStockItemView = view;
        //              break;
        //          }
        //      }

        //      SOPStandardItemLine line = new SOPStandardItemLine();
        //      line.SOPOrderReturn = sopInvoiceOrder;
        //      line.Item = stockItems.First;
        //      line.WarehouseItem = warehouseStockItemView.WarehouseItem;

        //      line.LineQuantity = 2;

        //      sopInvoiceOrder.Lines.Add(line);

        //      sopInvoiceOrder.Post(false, true);
        //  }

        //  private void AddFreeTextLine(SOPInvoiceOrder sopOrder)
        //  {
        //      Sage.Accounting.SOP.SOPFreeTextLine sopFreeTextLine = new Sage.Accounting.SOP.SOPFreeTextLine();
        //      sopFreeTextLine.SOPOrderReturn = sopOrder;
        //      sopOrder.Lines.Add(sopFreeTextLine);
        //      sopFreeTextLine.ItemDescription = "Free Text 1";
        //      sopFreeTextLine.LineQuantity = 1;
        //      sopFreeTextLine.UnitSellingPrice = 3;

        //      sopFreeTextLine.Post(false);
        //  }

        //  private void AddAdditionalChargeLine(SOPInvoiceOrder sopOrder)
        //  {
        //      // Instantiate the line type
        //      Sage.Accounting.SOP.SOPAdditionalChargeLine sopAdditionalChargeLine = new Sage.Accounting.SOP.SOPAdditionalChargeLine();
        //      sopAdditionalChargeLine.SOPOrderReturn = sopOrder;

        //      SOPAdditionalCharges charges = new SOPAdditionalCharges();
        //      charges.Query.Filters.Add(new Sage.ObjectStore.Filter(SOPAdditionalCharge.FIELD_CODE, "Carriage"));
        //      charges.Find();

        //      sopAdditionalChargeLine.StoredCharge = charges.First;
        //      sopAdditionalChargeLine.ChargeValue = 101;
        //      sopOrder.Lines.Add(sopAdditionalChargeLine);
        //      sopAdditionalChargeLine.Post();
        //  }

        //  private void AddCommentLine(SOPInvoiceOrder sopInvoiceOrder)
        //  {
        //      Sage.Accounting.SOP.SOPCommentLine sopCommentLine = new Sage.Accounting.SOP.SOPCommentLine();
        //      sopCommentLine.SOPOrderReturn = sopInvoiceOrder;
        //      sopCommentLine.ItemDescription = "Thank you for your order";
        //      sopCommentLine.ShowOnCustomerDocs = true;
        //      sopInvoiceOrder.Lines.Add(sopCommentLine);
        //      sopCommentLine.Post();
        //  }


        //  /// <summary>
        //  /// Post a purchase invoice.
        //  /// </summary>
        //  /// <param name="header">The header data.</param>
        //  /// <returns>An empty string if successful, otherwise error message.</returns>
        //  public string PostPurchaseInvoiceCredit(InvoiceHeader header)
        //  {
        //      try
        //      {

        //      }
        //      catch (Exception ex)
        //      {
        //          return ex.Message;
        //      }

        //      return string.Empty;

        //  }

        //  /// <summary>
        //  /// Posts the sale order invoice.
        //  /// </summary>
        //  /// <param name="header">The invoice data</param>
        //  /// <returns>An empty string if successful, otherwise error message.</returns>
        //  public string PostSalesInvoice(BusinessObject.InvoiceHeader header)
        //  {
        //      Sage.Accounting.SOP.SOPInvoiceOrder invoiceOrder = null;
        //      ActiveLock activeLock = null;

        //      try
        //      {
        //          invoiceOrder = new SOPInvoiceOrder();
        //          invoiceOrder.Customer = CustomerFactory.Factory.Fetch(header.TradingPartnerCode);
        //          invoiceOrder.DocumentDate = DateTime.Today;
        //          invoiceOrder.CustomerDocumentNo = header.InvoiceNo.ToString();
        //          invoiceOrder.DocumentNo = header.InvoiceNo.ToString();
        //          //invoiceOrder.Currency = FinancialCurrency.CoreCurrency;
        //          invoiceOrder.Update();

        //          activeLock = Sage.Accounting.Application.Lock(invoiceOrder);

        //          foreach (var invoiceDetail in header.Details)
        //          {
        //              StockItems stockItems = new StockItems();
        //              stockItems.Query.Filters.Add(new Sage.ObjectStore.Filter(StockItem.FIELD_CODE, invoiceDetail.ProductCode));
        //              stockItems.Find();
        //              StockItem item = stockItems.First;

        //              SOPStandardItemLine line = new SOPStandardItemLine();
        //              line.SOPOrderReturn = invoiceOrder;
        //              line.Item = item;
        //              line.LineQuantity = invoiceDetail.Quantity.ToDecimal();
        //              invoiceOrder.Lines.Add(line);
        //              invoiceOrder.Post(false, true);
        //          }

        //          //AddFreeTextLine(invoiceOrder);
        //          //AddAdditionalChargeLine(invoiceOrder);
        //          // AddCommentLine(invoiceOrder);

        //          invoiceOrder.Confirm(false, true, false);
        //          invoiceOrder.Post(false, true);
        //      }
        //      catch (Exception ex)
        //      {
        //          // If the validation of the invoice fails we need to remove it and its lines - since these have already been saved.
        //          invoiceOrder.Cancel();
        //          return ex.Message;
        //      }
        //      finally
        //      {
        //          // Make sure we unlock the invoice
        //          if (activeLock != null)
        //          {
        //              activeLock.Dispose();
        //          }
        //      }

        //      return string.Empty;
        //  }

        //  /// <summary>
        //  /// Posts the sale order .
        //  /// </summary>
        //  /// <param name="header">The sale order data</param>
        //  /// <returns>An empty string if successful, otherwise error message.</returns>
        //  public string PostSaleOrder(BusinessObject.InvoiceHeader header)
        //  {
        //      // not implemented
        //      Sage.Accounting.SOP.SOPAllocationAdjustment sopAllocationAdjustment = new
        //Sage.Accounting.SOP.SOPAllocationAdjustment();

        //      try
        //      {
        //          // Set the criteria
        //          // To select all orders do not set any criteria
        //          // To ignore orders with a due date after a specified date
        //          //  sopAllocationAdjustment.LatestDeliveryDate = Sage.Common.Clock.Today;
        //          // To select all orders that are unprioritised 
        //          //  sopAllocationAdjustment.OrderPriorityFrom = "*";
        //          //  sopAllocationAdjustment.OrderPriorityTo = "*";
        //          // To select orders for a range of priorities
        //          //  sopAllocationAdjustment.OrderPriorityFrom = "A";
        //          //  sopAllocationAdjustment.OrderPriorityTo = "I";
        //          // Select one or a range of orders, here we are select one
        //          sopAllocationAdjustment.OrderNumberFrom = "0000005107";
        //          sopAllocationAdjustment.OrderNumberTo = "0000005107";

        //          // Select the orders to be allocated
        //          Select_Orders(sopAllocationAdjustment);

        //          // Lock the orders
        //          sopAllocationAdjustment.LockingCoordinator.Lock();

        //          if (sopAllocationAdjustment.LockingCoordinator.AllEntriesAreLocked)
        //          {
        //              if (sopAllocationAdjustment.LockingCoordinator.LockedItems.Count > 0)
        //              {
        //                  // Populate the allocations again which includes the orders
        //                  //  that adhere to the criteria and are locked
        //                  sopAllocationAdjustment.PopulateAutomaticAllocations();

        //                  // Create the allocations
        //                  sopAllocationAdjustment.PostAllocationAdjustmentLines();
        //              }
        //          }
        //          else
        //          {
        //              // Some of the orders could not be locked. 
        //              // We can display the {PRODUCT_NAME} form showing the details
        //              //Sage.MMS.FailedBatchLocksForm form = new
        //              //   Sage.MMS.FailedBatchLocksForm(sopAllocationAdjustment.LockingCoordinator);
        //              // form.Caption = "SOP - Allocate Stock";
        //              //form.ShowDialog();
        //              // form.Dispose();
        //          }
        //      }
        //      catch (System.Exception ex)
        //      {
        //          System.Diagnostics.Debug.WriteLine(ex.Message);
        //      }
        //      finally
        //      {
        //          // Clear the locks and dispose
        //          sopAllocationAdjustment.LockingCoordinator.Clear();
        //          sopAllocationAdjustment.Dispose();
        //      }

        //      return "";
        //  }

        //  public void Select_Orders(Sage.Accounting.SOP.SOPAllocationAdjustment sopAllocationAdjustment)
        //  {
        //      sopAllocationAdjustment.LockingCoordinator.Clear();

        //      // Populate the coordinator AllocationAdjustmentLines
        //      sopAllocationAdjustment.PopulateAutomaticOrders();

        //      // The DiscreteOrders does not include duplicates which avoids
        //      //  trying to lock the same order twice
        //      Sage.Accounting.OrderProcessing.DiscreteOrderItems items =
        //        sopAllocationAdjustment.OrderList.DiscreteOrders;

        //      Sage.Accounting.Common.ConcurrencyLockKey concurrencyLockKey = null;
        //      Sage.Accounting.Common.ConcurrencyLock concurrencyLock = null;

        //      switch (items.Count)
        //      {
        //          case 0:
        //              // There is nothing in the order list, we will not continue                        
        //              break;
        //          case 1:
        //              // There is one item in the list
        //              Sage.Accounting.SOP.Views.SOPAllocationView viewItem =
        //                sopAllocationAdjustment.OrderList.First;

        //              // There is only one item in the list, we will automatically create a concurrency 
        //              //  lock and continue without the form
        //              concurrencyLockKey = new Sage.Accounting.Common.ConcurrencyLockKey("SOPOrder",
        //                viewItem.SOPOrderReturnDbKey.ToString());

        //              concurrencyLock =
        //                sopAllocationAdjustment.LockingCoordinator.CreateConcurrencyLock(concurrencyLockKey);

        //              concurrencyLock.AddFieldValue(viewItem.DocumentNo);
        //              concurrencyLock.AddFieldValue(viewItem.DocumentDate.ToShortDateString());
        //              concurrencyLock.AddFieldValue(viewItem.CustomerReference);
        //              concurrencyLock.AddFieldValue(viewItem.CustomerName);
        //              concurrencyLock.AddFieldValue(viewItem.DocumentValue.ToString());

        //              break;
        //          default:

        //              // Select the orders to allocate
        //              // We would normally display an UI and the user would select the line we 
        //              //  wish to allocate from a list. Here I am adding all of the items to be allocated.
        //              // IMPORTANT NOTE: There can be several AllocationAdjustmentLines for one Sales Order.
        //              // We must ensure that we do not try to lock the same Sales Order more than once. 
        //              // Here I am using DiscreteOrders which does not have duplicates.
        //              foreach (Sage.Accounting.OrderProcessing.DiscreteOrderItem oDiscreteOrderItem in
        //                sopAllocationAdjustment.OrderList.DiscreteOrders)
        //              {
        //                  // Create the ConcurrencyLockKey for the order
        //                  concurrencyLockKey = new
        //                    Sage.Accounting.Common.ConcurrencyLockKey("SOPOrder",
        //                    oDiscreteOrderItem.OrderReturnID.ToString());

        //                  // Create/Add the ConcurrencyLockKey to the LockingCoordinator
        //                  concurrencyLock =
        //                    sopAllocationAdjustment.LockingCoordinator.CreateConcurrencyLock(
        //                    concurrencyLockKey);

        //                  // Add additional information to the ConcurrencyLock needed if the Lock fails
        //                  concurrencyLock.AddFieldValue(oDiscreteOrderItem.DocumentNumber);
        //                  concurrencyLock.AddFieldValue(oDiscreteOrderItem.DocumentDate.ToShortDateString());
        //                  concurrencyLock.AddFieldValue(oDiscreteOrderItem.TraderReference);
        //                  concurrencyLock.AddFieldValue(oDiscreteOrderItem.TraderName);
        //                  concurrencyLock.AddFieldValue(oDiscreteOrderItem.DocumentValue.ToString());
        //              }

        //              break;
        //      }
        //  }


        //*************** Non Sage200 ***************************************************************
        public string Connect(string path, string userName, string password)
        {
            throw new NotImplementedException();
        }

        public string ExportProducts(IList<Product> products)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<BusinessPartner>, string> GetPartners(string path)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<Prices>, string> GetPrices(string path)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<Product>, string> GetProducts(string directory)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<Product>, string> GetSpecialPrices(string path)
        {
            throw new NotImplementedException();
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public string PostPurchaseInvoice(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        public string PostPurchaseInvoiceCredit(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        public string PostPurchaseOrder(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        public string PostSaleOrder(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        public string PostSalesCreditNote(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        public string PostSalesInvoice(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        // *********************************************************************************
    }
}


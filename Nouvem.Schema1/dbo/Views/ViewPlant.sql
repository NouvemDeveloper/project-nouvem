﻿CREATE VIEW [dbo].[ViewPlant]
as
select p.PlantID,
       p.Code,
       p.SiteName,
       p.CountryID,
       b.BPAddressID,
       b.AddressLine1 + ' ' + b.AddressLine2 + ' ' + b.AddressLine3 + ' ' + b.AddressLine4 AS BPAddress,
       bm.Name AS BPName
from Plant p left join BPAddress b
   on b.PlantID = p.PlantID
   Left Join BPMaster bm 
   on b.BPMasterID = bm.BPMasterID
where p.Deleted = 0

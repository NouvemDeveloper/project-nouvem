﻿
CREATE VIEW [dbo].[ViewLicenseDetails]
AS
SELECT l.*, ld.LicenceQTY, n.LicenceName
FROM Licence l 
         inner join LicenceDetail ld
		    on l.LicenceID = ld.LicenceID
	     inner join NouLicenceType n
		    on ld.NouLicenceTypeID = n.NouLicenceTypeID

WHERE l.Deleted = 'false' AND ld.Deleted= 'false'


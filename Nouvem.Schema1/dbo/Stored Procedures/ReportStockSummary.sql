﻿







-- =============================================
-- Author:		brian murray
-- Create date: 05/04/2016
-- Description:	Stock level summary.
-- =============================================
CREATE PROCEDURE [dbo].[ReportStockSummary] 

 @CutOffDate DATE,
 @CutOffTime NVARCHAR(20),
 @PriceListId int,
 @PROrderID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ExactCutOffTime AS DATETIME = CONVERT(DATETIME, @CutOffDate) + CONVERT(TIME, @CutOffTime)

	if (@PROrderID = 1000000)
	BEGIN

	SELECT g.INGroupID,
	       g.Name AS 'GroupName',
	       m.Name, 
		   m.Code,
	       SUM(CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END) AS 'BoxCount', 
		   SUM(s.TransactionWeight) AS 'Weight',
		   SUM(s.TransactionQTY) AS 'Pieces',
		   s.WarehouseID,	
		   w.Name AS 'WareHouseName',
		   ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0) AS 'Price',
     CASE WHEN (SELECT Name FROM PriceListDetail 
		              INNER JOIN NouPriceMethod ON PriceListDetail.NouPriceMethodID = NouPriceMethod.NouPriceMethodID
					  WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID)) = 'Qty x Price'
					  THEN 
						 SUM(s.TransactionQty) *
						 ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0)
					  ELSE
						 SUM(s.TransactionWeight) *
						 ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0)
					  END  as 'TotalValue'    	 
	 

	FROM INMaster m INNER JOIN StockTransaction s 
	                           ON s.INMasterID = m.INMasterID
					INNER JOIN INGroup g
					           ON m.INGroupID = g.INGroupID					
				    INNER JOIN Warehouse w 
					           ON s.WarehouseID = w.WarehouseID

	WHERE 
	      w.StockLocation = 1
		  AND s.InLocation = 1
		  AND (s.Deleted is null or s.Deleted > @ExactCutOffTime)
	      AND (s.Consumed is null or s.Consumed > @ExactCutOffTime )  
	      AND s.TransactionDate <= @ExactCutOffTime
	
	
	GROUP BY g.INGroupID, g.Name, m.Name, s.WarehouseID, w.Name,m.Code

	END
	ELSE
	BEGIN

	SELECT g.INGroupID,
	       g.Name AS 'GroupName',
	       m.Name, 
		   m.Code,
	       SUM(CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END) AS 'BoxCount', 
		   SUM(s.TransactionWeight) AS 'Weight',
		   SUM(s.TransactionQTY) AS 'Pieces',
		   s.WarehouseID,	
		   w.Name AS 'WareHouseName',
		   ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0) AS 'Price',
     CASE WHEN (SELECT Name FROM PriceListDetail 
		              INNER JOIN NouPriceMethod ON PriceListDetail.NouPriceMethodID = NouPriceMethod.NouPriceMethodID
					  WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID)) = 'Qty x Price'
					  THEN 
						 SUM(s.TransactionQty) *
						 ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0)
					  ELSE
						 SUM(s.TransactionWeight) *
						 ISNULL(((SELECT MAX(Price) FROM PriceListDetail 
						 WHERE PriceListID = @PriceListId AND PriceListDetail.INMasterID = Max(m.INMasterID))),0)
					  END  as 'TotalValue'    	 
	 

	FROM INMaster m INNER JOIN StockTransaction s 
	                           ON s.INMasterID = m.INMasterID
					INNER JOIN INGroup g
					           ON m.INGroupID = g.INGroupID					
				    INNER JOIN Warehouse w 
					           ON s.WarehouseID = w.WarehouseID
					INNER JOIN PROrder p
					           ON p.PROrderID = s.MasterTableID

	WHERE 
	      w.StockLocation = 1
		  AND s.InLocation = 1
		  AND (s.Deleted is null or s.Deleted > @ExactCutOffTime)
	      AND (s.Consumed is null or s.Consumed > @ExactCutOffTime )  
	      AND s.TransactionDate <= @ExactCutOffTime
		  AND p.PROrderID = @PROrderID
	
	
	GROUP BY g.INGroupID, g.Name, m.Name, s.WarehouseID, w.Name,m.Code

	END

END
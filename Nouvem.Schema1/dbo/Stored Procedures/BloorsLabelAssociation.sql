﻿

-- =============================================
-- Author:		<James Harrison>
-- Create date: <09/08/2016>
-- Description:	<Label association report>
-- =============================================
CREATE PROCEDURE [dbo].[BloorsLabelAssociation]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select 
	ISNULL(bpm.Name, 'All Customers') as 'Customer',
	ISNULL(inm.Name, 'ALL Products') as 'Product',
	ISNULL(inm.Code, '') as 'Product Code',
    lbl.name as 'Label Name',
	ISNULL(ing.Name, '') as 'Product Group',
	ISNULL(ping.Name, '') as 'Parent Product Group' 

	from LabelAssociation lbla
	inner join LabelAssociationLabel lblal on lbla.LabelAssociationID = lblal.LabelAssociationID
	inner join label lbl on lblal.LabelID = lbl.LabelID
	left join BPMaster bpm on bpm.BPMasterID = lbla.BPMasterID
	left join INMaster inm on inm.INMasterID = lbla.INMasterID
	left join INGroup ing on ing.INGroupID = lbla.INGroupID
	left join INGroup ping on ing.ParentInGroupID = ping.INGroupID

	where lbla.Deleted is null and lblal.Deleted is null and lblal.ItemLabel = 1
END
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TestDispatchSummary]
	-- Add the parameters for the stored procedure here
@Dispatchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select inm.Code, 
	       inm.Name,
		   ard.QuantityDelivered as 'Qty',
		   case when inm.NominalWeight > 0 then ard.QuantityDelivered * inm.NominalWeight else ard.WeightDelivered end as 'Weight',
	--	   ard.WeightDelivered as 'Weight',
		   bpm.Name as 'Customer Name',
		   bpa.AddressLine1, BPA.AddressLine2, bpa.AddressLine3, bpa.AddressLine4, bpa.PostCode,
		   ar.Number,
		   ar.CustomerPOReference,
		   ar.DeliveryDate,
		   bpm.Code as 'Customer Code',
		   hbpm.Name as 'HaulierName',
		   (SELECT COUNT(*) FROM StockTransaction WHERE MasterTableID = ard.ArdispatchDetailId
		   and NouTransactionTypeID = 7 and IsBox = 1 and Deleted is null) as 'Box Count',
		  -- inm.QtyPerBox ,
		case when isnull(inm.qtyperbox, 0 ) = 0 then 1 else isnull(inm.qtyperbox, 0 ) end as 'QtyPerBox'
	 from ARDispatch ar inner join ARDispatchDetail ard on ar.ARDispatchID = ard.ARDispatchID
     inner join INMaster inm on inm.INMasterID = ard.INMasterID
     left join BPMaster bpm on ar.BPMasterID_Customer = bpm.BPMasterID
	 left join BPAddress bpa on bpa.BPAddressID = ar.BPAddressID_Delivery 
     left join BPMaster hbpm on hbpm.BPMasterID = ar.BPMasterID_Haulier
      where Ar.ARDispatchID = @Dispatchid and (ard.WeightDelivered > 0 or ard.QuantityDelivered >0) 

END


select * from ARDispatch where number = 4
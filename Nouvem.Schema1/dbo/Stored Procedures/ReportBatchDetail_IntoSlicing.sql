﻿








-- =============================================
-- Author:		brian murray
-- Create date: 06/07/2016
-- Description:	Gets the into slicing transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportBatchDetail_IntoSlicing] 
	-- Add the parameters for the stored procedure here
	@BatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	    w.StockLocation,
		w.Name AS 'Warehouse',     
		p.Number,
		p.Reference,
		s.TransactionWeight,
		s.TransactionQTY,		
		s.Serial,
		s.Pieces,
		s.TransactionDate,
		i.INMasterID,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		bn.Number AS 'BatchNo',		
		
		  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'SupplierBatch') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'SupplierBatch') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch') 
				      END)) AS 'Supplier Batch',

			  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'ACC') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'ACC') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'ACC')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'ACC') 
				      END)) AS 'Accreditation'

    FROM PROrder p      
	   INNER JOIN StockTransaction s
	      ON s.MasterTableID = p.PROrderID
	   INNER JOIN BatchNumber bn
	      ON bn.BatchNumberID = s.BatchNumberID
	   INNER JOIN Warehouse w
	      ON w.WarehouseID = s.WarehouseID
       INNER JOIN INMaster i
          ON s.INMasterID = i.INMasterID     
	   
	   
      
         
     WHERE bn.BatchNumberID = @BatchID AND p.Deleted IS NULL AND s.Deleted IS NULL aND s.NouTransactionTypeID = 3
     END
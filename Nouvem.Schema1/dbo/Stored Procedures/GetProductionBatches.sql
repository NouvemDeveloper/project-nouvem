﻿-- =============================================
-- Author:		brian murray
-- Create date: 30/08/16
-- Description:	Gets the production batch reference data.
-- =============================================
CREATE PROCEDURE GetProductionBatches 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 1000000 AS PROrderID,
	'All Batches' AS 'Reference' 	
	UNION
	SELECT PROrderID, 
	CASE WHEN Reference is NULL OR Reference = '' THEN 'No Reference Entered' ELSE Reference END as 'Reference'
	FROM PROrder WHERE Deleted is null
	ORDER BY PROrderID DESC
END
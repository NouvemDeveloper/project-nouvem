﻿CREATE PROCEDURE [dbo].[GetAllStockByWeight]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT m.Name, 
       SUM(CASE WHEN n.AddToStock = 1 THEN s.TransactionWeight ELSE -s.TransactionWeight END) AS Wgt
    FROM INMaster m 
       INNER JOIN StockTransaction s 
            ON m.INMasterID = s.INMasterID
	    INNER JOIN NouTransactionType n
		    ON s.NouTransactionTypeID = n.NouTransactionTypeID
    WHERE s.Deleted is null AND s.Consumed is NULL
    GROUP BY m.Name
END
﻿-- =============================================
-- Author:		brian murray
-- Create date: 19/11/2015
-- Description:	REmoves all the ar/ap order data, and the traceability results.
-- =============================================
CREATE PROCEDURE TestRemoveOrders
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 delete from [dbo].[ARQuoteDetail]
 delete from [dbo].[ARQuote]
 delete from [dbo].[AROrderDetail]
 delete from [dbo].[AROrder]

 delete from [dbo].[APQuoteDetail]
 delete from [dbo].[APQuote]
 delete from [dbo].[APOrderDetail]
 delete from [dbo].[APOrder]

 delete from [dbo].[BatchTraceability]
 delete from [dbo].[TransactionTraceability]
 delete from [dbo].[APGoodsReceiptDetail]
 delete from [dbo].[APGoodsReceipt]
END

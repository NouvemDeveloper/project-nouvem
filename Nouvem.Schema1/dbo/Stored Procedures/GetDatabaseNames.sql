﻿-- =============================================
-- Author:		Brian Murray
-- Create date: 28/04/2015
-- Description:	Retrieve all the database names on the server.
-- =============================================
CREATE PROCEDURE GetDatabaseNames 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT name
   FROM sys.databases;
   
END

﻿
-- =============================================
-- Author:		brian murray
-- Create date: 27/07/2015
-- Description:	Removes the inventory items/groups etc (For testing purposes)
-- =============================================
CREATE PROCEDURE [dbo].[TestRemoveInventory]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from INAttachment
	delete from INPropertySelection
	delete from INProperty
	delete from InvoiceHeader
	delete from InvoiceDetail
	delete from stock
	delete from StockTransaction
	delete from INMaster
	delete from INGroup
	delete from INMasterSnapshot
END


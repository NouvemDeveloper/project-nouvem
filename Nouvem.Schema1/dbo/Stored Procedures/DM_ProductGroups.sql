﻿




-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the product groups.
-- =============================================
CREATE PROCEDURE [dbo].[DM_ProductGroups]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 		
	 @GroupDescription varchar(70),
	 @ParentGroup bigint

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.GroupDescription,
			p.ParentGroup		

     FROM  [192.168.16.140\DEMSQLSERVER].[DEMMigrationDB].[dbo].[ProductGroups]  p
	
 
     OPEN PRICE_CURSOR;
  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO 
		  @GroupDescription,
		  @ParentGroup
		

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 

	 DECLARE @ParentGroupID int = null
	 IF (@ParentGroup is not null)
		    BEGIN
			  DECLARE @LocalDescription varchar(70) = (SELECT TOP 1 GroupDescription FROM  [192.168.16.140\DEMSQLSERVER].[DEMMigrationDB].[dbo].[ProductGroups] where id = @ParentGroup)		
			  SET @ParentGroupID = (SELECT TOP 1 INGroupID FROM INGroup WHERE Name = @LocalDescription)
		
		    END		
	

	 insert into INGroup (Name, ParentInGroupID,Deleted) values (@GroupDescription, @ParentGroupID, 0)							  
			

	 FETCH NEXT FROM PRICE_CURSOR
		 INTO  @GroupDescription,
		       @ParentGroup
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END

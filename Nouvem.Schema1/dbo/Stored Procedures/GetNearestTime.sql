﻿

-- =============================================
-- Author:		brian murray
-- Create date: 03/05/2016
-- Description:	Gets the snearest time to the current time used in the gettimes sp.
-- =============================================
CREATE PROCEDURE [dbo].[GetNearestTime]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentTime varchar(5) = (SELECT CONVERT(varchar(5),GETDATE() ,108))
	
	DECLARE @TempTable Table(LocalTime varchar(5));	

	WITH CTE(N)
	AS
	(
	SELECT 30
	UNION ALL
	SELECT N+30
	FROM CTE
	WHERE N+5<24*60
	) 

	INSERT INTO @TempTable(LocalTime)
	SELECT CONVERT(varchar(5),DATEADD(minute,N,0) ,108) 
	FROM CTE 
	OPTION (MAXRECURSION 0) 	
	SELECT TOP 1 * FROM @TempTable
	 WHERE CONVERT(TIME(0),LocalTime) >=  CONVERT(TIME(0), @CurrentTime)
	

	
END
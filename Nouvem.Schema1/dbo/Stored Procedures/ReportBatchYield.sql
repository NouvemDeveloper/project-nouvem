﻿




-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the batch yield data.
-- =============================================
CReate PROCEDURE [dbo].[ReportBatchYield] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
SELECT b.BatchNumberID,
       b.Number AS 'BatchNo', 
	   SUM(s.TransactionQTY) AS 'TransactionQty', 
	   SUM(s.TransactionWeight) AS 'TransactionWeight', 
	   SUM(1) AS 'BoxCount', 
	   im.Name, 
	   ntt.AddToStock, 
	   ntt.SubtractFromStock, 
	   SUM((CASE WHEN ntt.SubtractFromStock = 1 THEN s.TransactionWeight ELSE 0 END)) AS InputWgt, 
	   SUM((CASE WHEN ntt.AddToStock = 1 THEN s.TransactionWeight ELSE 0 END)) AS OutputWgt, p.PROrderID

FROM   PROrder AS p INNER JOIN
           BatchNumber AS b ON p.BatchNumberID = b.BatchNumberID INNER JOIN
           StockTransaction AS s ON s.MasterTableID = p.PROrderID INNER JOIN
           NouTransactionType AS ntt ON s.NouTransactionTypeID = ntt.NouTransactionTypeID INNER JOIN
           INMaster AS im ON im.INMasterID = s.INMasterID

WHERE (s.Deleted IS NULL AND s.Consumed IS NULL)

GROUP BY im.Name, p.Number, b.BatchNumberID, b.Number, ntt.AddToStock, ntt.SubtractFromStock, p.PROrderID
END
﻿




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetDeviceSettings_TSBloors]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update devicesettings set Value1 = 'WithProductionOrder' where name = 'IntakeMode'
	update devicesettings set Value1 = 'RecallOrder' where name = 'IntoProductionMode'
	update devicesettings set Value1 = 'RecordWeight' where name = 'DispatchMode'
	update devicesettings set Value1 = 'True' where name = 'CheckWeightBoundaryAtDispatch'
	update devicesettings set Value1 = 'True' where name = 'CheckForPartBoxesAtDispatch'
	update devicesettings set Value1 = 'True' where name = 'AutoCreateInvoiceOnOrderCompletion'
	update devicesettings set Value1 = 'True' where name = 'DispatchOrderNotFilledWarning'
	update devicesettings set Value3 = 'True' where name = 'AutoWeigh'
	update devicesettings set Value1 = 'False' where name = 'AllowDispatchOverAmount'
	update devicesettings set Value1 = '0.3', value2 = '0.3', value3 = '0.3', Value4='0.4' where name = 'MinWeightAllowed'
    update devicesettings set Value1 = 'http://192.168.0.146:80/ReportServer_sql2014/ReportService2010.asmx' where name = 'SSRSWebServiceURL'
	update devicesettings set Value1 = 'nouvem' where name = 'SSRSUserName'
	update devicesettings set Value1 = 'Nouv1234' where name = 'SSRSPassword'
	update devicesettings set Value1 = 'NOUVEMSERVER' where name = 'SSRSDomain'
	update devicesettings set Value1 = 'http://192.168.0.146:80/reportserver_sql2014' where name = 'ReportServerPath'
	update devicesettings set Value1 = 'True' where name = 'ConnectToSSRS'
	update DeviceSettings set Value1 = 'True' where name = 'RouteMustBeSetOnOrder'
END
﻿-- =============================================
-- Author:		brian murray
-- Create date: 7/10/16
-- Description: Reopens a sale order, cancelling associated dockets/transactions.
-- =============================================
CREATE PROCEDURE TestReOpenSaleOrder
	-- Add the parameters for the stored procedure here
	@DocumentNo nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SaleOrderId int = (SELECT TOP 1 AROrderID FROM AROrder WHERE Number = @DocumentNo)

	IF @SaleOrderId is not null
	BEGIN

	   --re-open sale order
	   UPDATE AROrder SET NouDocStatusID = 1 WHERE AROrderID = @SaleOrderId
	   
	
	   DECLARE @DispatchId int = (SELECT TOP 1 ARDispatchID FROM ARDispatch WHERE BaseDocumentReferenceID = @SaleOrderId)

	   IF @DispatchId is not null
	   BEGIN
	          -- re-open dispatch docket, delete lines and transactions
	       UPDATE ARDispatch SET NouDocStatusID = 1, Invoiced = null, SubTotalExVAT = 0, GrandTotalIncVAT = 0, VAT = 0 WHERE ARDispatchID = @DispatchId
		   UPDATE ARDispatchDetail SET QuantityDelivered = 0, WeightDelivered = 0 WHERE ARDispatchID = @DispatchId

		   DECLARE @DispatchTransId int = (SELECT TOP 1 NouTransactionTypeId FROM NouTransactionType WHERE [Description] = 'GoodsDelivery')
		   UPDATE StockTransaction SET DELETED = GETDATE() 
		      WHERE MasterTableID IN (SELECT ARDispatchDetailID FROM ARDispatchDetail WHERE ARDispatchID = @DispatchId)
			  AND NouTransactionTypeID = @DispatchTransId
	   
	       -- Delete the invoice  
		   UPDATE ARInvoice SET DELETED = GETDATE() WHERE BaseDocumentReferenceID = @DispatchId
	   END
	END
END
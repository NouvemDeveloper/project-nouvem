﻿CREATE TABLE [dbo].[NouAuthorisation] (
    [NouAuthorisationID]          INT            IDENTITY (1, 1) NOT NULL,
    [Description]                 NVARCHAR (100) NOT NULL,
    [NouAuthorisationGroupNameID] INT            NOT NULL,
    [CodeSetting]                 NVARCHAR (50)  NOT NULL,
    [Deleted]                     BIT            NOT NULL,
    CONSTRAINT [PK_AuthorisationList] PRIMARY KEY CLUSTERED ([NouAuthorisationID] ASC),
    CONSTRAINT [FK_SysAuthorisationList_SysAuthorisationGroupNames] FOREIGN KEY ([NouAuthorisationGroupNameID]) REFERENCES [dbo].[NouAuthorisationGroupName] ([NouAuthorisationGroupNamesID])
);


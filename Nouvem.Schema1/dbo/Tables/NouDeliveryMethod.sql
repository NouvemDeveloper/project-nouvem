﻿CREATE TABLE [dbo].[NouDeliveryMethod] (
    [NouDeliveryMethodID] INT           IDENTITY (1, 1) NOT NULL,
    [Method]              NVARCHAR (30) NOT NULL,
    [Deleted]             DATETIME      NULL,
    CONSTRAINT [PK_NouDeliveryMethod] PRIMARY KEY CLUSTERED ([NouDeliveryMethodID] ASC)
);


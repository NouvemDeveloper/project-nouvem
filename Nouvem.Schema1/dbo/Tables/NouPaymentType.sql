﻿CREATE TABLE [dbo].[NouPaymentType] (
    [NouPaymentTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [PaymentType]      NVARCHAR (30) NOT NULL,
    [Deleted]          DATETIME      NULL,
    CONSTRAINT [PK_NouPaymentType] PRIMARY KEY CLUSTERED ([NouPaymentTypeID] ASC)
);


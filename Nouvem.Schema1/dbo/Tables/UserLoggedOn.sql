﻿CREATE TABLE [dbo].[UserLoggedOn] (
    [UserLoggedOnID] INT           IDENTITY (1, 1) NOT NULL,
    [UserMasterID]   INT           NOT NULL,
    [DeviceIP]       NVARCHAR (50) NOT NULL,
    [SessionStart]   DATETIME      NULL,
    [SessionEnd]     DATETIME      NULL,
    [Deleted]        BIT           NOT NULL,
    [ForceLogout]    BIT           NULL,
    [PCName]         NVARCHAR (50) NULL,
    CONSTRAINT [PK_UserLoggedOn] PRIMARY KEY CLUSTERED ([UserLoggedOnID] ASC),
    CONSTRAINT [FK_UserLoggedOn_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);


﻿CREATE TABLE [dbo].[TraceabilityTemplateMaster] (
    [TraceabilityTemplatesMasterID] INT            IDENTITY (1, 1) NOT NULL,
    [TraceabilityCode]              NVARCHAR (50)  NOT NULL,
    [NouTraceabilityTypeID]         INT            NOT NULL,
    [SQL]                           NVARCHAR (500) NULL,
    [GS1AIMasterID]                 INT            NULL,
    [Collection]                    NVARCHAR (500) NULL,
    [TraceabilityDescription]       NVARCHAR (100) NULL,
    [Deleted]                       BIT            NOT NULL,
    CONSTRAINT [PK_TracebilityTemplatesMaster] PRIMARY KEY CLUSTERED ([TraceabilityTemplatesMasterID] ASC),
    CONSTRAINT [FK_TraceabilityTemplatesMaster_Gs1AIMaster] FOREIGN KEY ([GS1AIMasterID]) REFERENCES [dbo].[Gs1AIMaster] ([GS1AIMasterID]),
    CONSTRAINT [FK_TracebilityTemplatesMaster_NouTracebilityType] FOREIGN KEY ([NouTraceabilityTypeID]) REFERENCES [dbo].[NouTraceabilityType] ([NouTraceabilityTypeID])
);


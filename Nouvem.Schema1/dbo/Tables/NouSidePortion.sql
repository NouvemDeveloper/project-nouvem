﻿CREATE TABLE [dbo].[NouSidePortion] (
    [NouSidePortionID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (50) NOT NULL,
    [ShortName]        NVARCHAR (50) NOT NULL,
    [Deleted]          DATETIME      NULL
);


﻿CREATE TABLE [dbo].[BPPropertySelection] (
    [BPPropertySelectionID] INT IDENTITY (1, 1) NOT NULL,
    [BPMasterID]            INT NOT NULL,
    [BPPropertyID]          INT NOT NULL,
    [Deleted]               BIT NOT NULL,
    CONSTRAINT [PK_BPPropertySelection] PRIMARY KEY CLUSTERED ([BPPropertySelectionID] ASC),
    CONSTRAINT [FK_BPPropertySelection_BPPropertys] FOREIGN KEY ([BPPropertyID]) REFERENCES [dbo].[BPProperty] ([BPPropertyID]),
    CONSTRAINT [FK_BPPropertySelection_BusinessPartner] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID])
);


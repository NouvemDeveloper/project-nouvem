﻿CREATE TABLE [dbo].[Audit] (
    [AuditID]         INT           IDENTITY (1, 1) NOT NULL,
    [TransactionDate] DATETIME      NOT NULL,
    [TableName]       NCHAR (50)    NOT NULL,
    [TableID]         INT           NOT NULL,
    [UserMasterID]    INT           NOT NULL,
    [UserName]        NVARCHAR (50) NULL,
    [UserFullName]    NVARCHAR (50) NULL,
    [TransactionType] NCHAR (10)    NOT NULL,
    [DataChanges]     XML           NULL,
    [Deleted]         BIT           NOT NULL,
    CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED ([AuditID] ASC),
    CONSTRAINT [FK_Audit_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);


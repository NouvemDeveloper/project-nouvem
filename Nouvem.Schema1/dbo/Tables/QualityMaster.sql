﻿CREATE TABLE [dbo].[QualityMaster] (
    [QualityMasterID]    INT            IDENTITY (1, 1) NOT NULL,
    [NouQualityTypeID]   INT            NOT NULL,
    [QualityCode]        NVARCHAR (50)  NOT NULL,
    [QualityDescription] NVARCHAR (50)  NOT NULL,
    [GS1AIMasterID]      INT            NULL,
    [SQL]                NVARCHAR (500) NULL,
    [Collection]         NVARCHAR (500) NULL,
    [Deleted]            BIT            NOT NULL,
    CONSTRAINT [PK_INQuality] PRIMARY KEY CLUSTERED ([QualityMasterID] ASC),
    CONSTRAINT [FK_QualityMaster_NouQualityType] FOREIGN KEY ([NouQualityTypeID]) REFERENCES [dbo].[NouQualityType] ([NouQualityTypeID])
);


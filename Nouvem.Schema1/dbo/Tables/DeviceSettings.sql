﻿CREATE TABLE [dbo].[DeviceSettings] (
    [DeviceSettingsID] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (100) NOT NULL,
    [Value1]           NVARCHAR (100) NULL,
    [Value2]           NVARCHAR (100) NULL,
    [Value3]           NVARCHAR (100) NULL,
    [Value4]           NVARCHAR (100) NULL,
    [Value5]           NVARCHAR (100) NULL,
    [Value6]           NVARCHAR (100) NULL,
    [Value7]           NVARCHAR (100) NULL,
    [Value8]           NVARCHAR (100) NULL,
    [Value9]           NVARCHAR (100) NULL,
    [Value10]          NVARCHAR (100) NULL,
    [Value11]          NVARCHAR (100) NULL,
    [Value12]          NVARCHAR (100) NULL,
    [Value13]          NVARCHAR (100) NULL,
    [Value14]          NVARCHAR (100) NULL,
    [Deleted]          DATETIME       NULL,
    [DeviceMasterID]   INT            NOT NULL,
    CONSTRAINT [PK_DeviceSettings] PRIMARY KEY CLUSTERED ([DeviceSettingsID] ASC),
    CONSTRAINT [FK_DeviceSettings_DeviceMaster] FOREIGN KEY ([DeviceMasterID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID])
);


﻿CREATE TABLE [dbo].[NouProductionType] (
    [NouProductionTypeID] INT        IDENTITY (1, 1) NOT NULL,
    [Description]         NCHAR (20) NOT NULL,
    [Type]                INT        NOT NULL,
    [Deleted]             DATETIME   NULL,
    CONSTRAINT [PK_NouProductionType] PRIMARY KEY CLUSTERED ([NouProductionTypeID] ASC)
);


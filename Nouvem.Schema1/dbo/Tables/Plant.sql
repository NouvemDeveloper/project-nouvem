﻿CREATE TABLE [dbo].[Plant] (
    [PlantID]   INT           IDENTITY (1, 1) NOT NULL,
    [Code]      NVARCHAR (50) NOT NULL,
    [SiteName]  NVARCHAR (50) NOT NULL,
    [CountryID] INT           NOT NULL,
    [Deleted]   BIT           NOT NULL,
    CONSTRAINT [PK_Plant] PRIMARY KEY CLUSTERED ([PlantID] ASC),
    CONSTRAINT [FK_Country_Plant] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID]) ON DELETE CASCADE ON UPDATE CASCADE
);


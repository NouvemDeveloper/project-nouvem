﻿CREATE TABLE [dbo].[NouDocStatus] (
    [NouDocStatusID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]    NVARCHAR (50) NULL,
    [Value]          NVARCHAR (50) NOT NULL,
    [Deleted]        BIT           NOT NULL,
    CONSTRAINT [PK_NouDocStatus] PRIMARY KEY CLUSTERED ([NouDocStatusID] ASC)
);


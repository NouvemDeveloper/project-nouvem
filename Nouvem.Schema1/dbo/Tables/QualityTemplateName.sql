﻿CREATE TABLE [dbo].[QualityTemplateName] (
    [QualityTemplateNameID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (50) NOT NULL,
    [Deleted]               BIT           NOT NULL,
    CONSTRAINT [PK_INQualityTemp1] PRIMARY KEY CLUSTERED ([QualityTemplateNameID] ASC)
);


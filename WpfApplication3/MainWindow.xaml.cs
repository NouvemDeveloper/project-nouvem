﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
                using (var entities = new NouvemBallonEntities())
                {
                    var labels = entities.Labels.ToList();

                    try
                    {
                        var label = labels.ElementAt(this.count);
                        var inputStream = label.ExternalLabelFile;
                        string input = Encoding.ASCII.GetString(inputStream);
                        //input = input.Replace("\0", string.Empty);
                        File.WriteAllText(@"C:\Users\brian\Desktop\Ballon4.txt", input);
                        this.id = label.LabelID;
                        this.count++;
                    }
                    catch (Exception ex)
                    {

                        var a = ex.Message;
                    }
                  
                }
        }

        private int id;
        private int count = 0;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            using (var entities = new NouvemBallonEntities())
            {
                var labels = entities.Labels.ToList();

                try
                {
                    var data = File.ReadAllBytes(@"C:\Users\brian\Desktop\Ballon4.txt");
                    var label = entities.Labels.First(x => x.LabelID == id);
                    label.ExternalLabelFile = data;
                    entities.SaveChanges();

                }
                catch (Exception ex)
                {

                    var a = ex.Message;
                }

            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var data = File.ReadAllText(@"C:\Users\brian\Desktop\Motion.txt");
            var eol = (char) 13;

            if (data.Contains("G"))
            {
                var a = true;
            }

            if (data.Contains("M"))
            {
                var a = true;
            }

            if (data.Contains(eol))
            {
                var a = true;
            }

            var tt = "GROSSM ";
            var c = tt.Contains("M");

            var d = c;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.AccountsIntegration;
using Nouvem.AccountsIntegration.BusinessObject;
using Nouvem.QAS;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            //var sage = new Sage50();
            //sage.Connect(@"C:\ProgramData\Sage\Accounts\2013\", "Manager", "");

            //var invoice = new InvoiceHeader();
            //invoice.TradingPartnerCode = "ACT001";
            //invoice.DeliveryAddressLine1 = "70 eamondale";
            //invoice.DeliveryAddressLine2 = "Naas";
            //invoice.DeliveryAddressLine3 = "Co.Kildate";
            //invoice.DeliveryAddressLine4 = "Ireland";
            //invoice.InvoiceDate = DateTime.Today;

            //invoice.Details = new List<InvoiceDetail>
            //{
            //    new InvoiceDetail { Quantity = 100, TotalExVat = 270, TotalIncVat = 270, UnitPrice = 2.7M, Description = "Test", ProductCode = "DR233"},
            //    new InvoiceDetail { Quantity = 20.1f, TotalExVat = 400, TotalIncVat = 400, UnitPrice = 20M},
            //    new InvoiceDetail { Quantity = 5, TotalExVat = 40, TotalIncVat = 40, UnitPrice = 9M},
            //    new InvoiceDetail { Quantity = 800, TotalExVat = 800, TotalIncVat = 800, UnitPrice = 1M}
            //};

            //sage.PostSalesInvoice(invoice);
            //sage.PostSaleOrder(null);

            // UN:             dunleaveymeatsltd
            // password:   D9UFJ3

            var herdStatus = Nouvem.QAS.Model.HerdStatus.CreateNewHerdStatus();

            try
            {
                var serviceCall = new Nouvem.QAS.BusinessLogic.WSWrapper();
                //var requestFileName = string.Format("{0}{1}.xml", Constants.BordBiaRequestFilePrefix, DateTime.Now.ToString("yyyyMMddhhss"));
                //var responseFileName = string.Format("{0}{1}.xml", Constants.BordBiaResponseFilePrefix, DateTime.Now.ToString("yyyyMMddhhss"));
                //var responseFilePath = string.Format("{0}{1}{2}.xml", responsePath, Constants.BordBiaResponseFilePrefix, DateTime.Now.ToString("yyyyMMddhhss"));

                //this.logger.LogTraceMessage(this.GetType(), string.Format("Attempting to retrieve the herd status for herd no {0} using username {1} and password {2}", herdNo, username, password));

                // Log the request being sent in the database.
                //this.LogBordBiaRequest(herdNo, DateTime.Now.ToString(), username, requestFileName);

                herdStatus = serviceCall.QueryBeefService("dunleaveymeatsltd", "D9UFJ3", "V1050159", "", true);

                // Log the response being sent in the database.
                //if (herdStatus != null || herdStatus.CertValidUntil != null)
                //{
                //    this.LogBordBiaResponse(herdNo, DateTime.Now.ToString(), responseFileName, herdStatus.HerdOwner, herdStatus.CertValidUntil.ToString());
                //}

                //this.logger.LogTraceMessage(this.GetType(), string.Format("Herd status successfully returned with for herdNo {0} with owner {1}, certvalidateDate {2}, error {3}, NominatedByYourMeatPlant {4}, Scope {5}", herdNo, herdStatus.HerdOwner, herdStatus.CertValidUntil, herdStatus.Error, herdStatus.NominatedByYourMeatPlant, herdStatus.Scope));
            }
            catch (Exception checkHerdStatusEx)
            {
                //this.logger.LogError(this.GetType(), string.Format("Bord Bia service request failed with the following error : {0}", checkHerdStatusEx.ToString()));
            }
        }
    }
}

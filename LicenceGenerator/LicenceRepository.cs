﻿// -----------------------------------------------------------------------
// <copyright file="LicenceGenerator.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace LicenceGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Licencing;

    public class LicenceRepository
    {
        /// <summary>
        /// Add the licence file details to the in-house customer licence table.
        /// </summary>
        /// <param name="detail">The licence details to add.</param>
        /// <param name="upgradedFromLicenceKey">If this is an upgrade, this is the previous licence key.</param>
        public void AddCustomerLicenceDetails(LicenseDetail detail, string upgradedFromLicenceKey)
        {
            using (var entities = new NouvemEntities())
            {
                var customerDetail = new CustomerLicence
                {
                    Customer = detail.Customer,
                    Email = detail.Email,
                    LicenceKey = detail.LicenseKey,
                    PrivateKey = detail.PrivateKey,
                    PublicKey = detail.PublicKey,
                    LicenceFile = detail.File,
                    IssueDate = DateTime.Today,
                    Deleted = false
                };

                entities.CustomerLicences.Add(customerDetail);

                if (upgradedFromLicenceKey != string.Empty)
                {
                    // upgrade, so mark the previous licence with an upgrade date.
                    var licenceUpgradedFrom =
                        entities.CustomerLicences.FirstOrDefault(x => x.LicenceKey.Equals(upgradedFromLicenceKey));

                    if (licenceUpgradedFrom != null)
                    {
                        licenceUpgradedFrom.UpgradeDate = DateTime.Today;
                    }
                }

                entities.SaveChanges();
            }
        }

       /// <summary>
       /// Returns the customer licences.
       /// </summary>
       /// <returns>The customer licences.</returns>
        public IList<CustomerLicence> GetCustomerLicenceDetails()
        {
            using (var entities = new NouvemEntities())
            {
                return entities.CustomerLicences.ToList();
            }
        }

        /// <summary>
        /// Get the licence types.
        /// </summary>
        /// <returns>A collection of licence types.</returns>
        public IList<NouLicenceType> GetLicenceTypes()
        {
            using (var entities = new NouvemEntities())
            {
                return entities.NouLicenceTypes.ToList();
            }
        }

        /// <summary>
        /// Get the authorisations.
        /// </summary>
        /// <returns>A collection of authorisations.</returns>
        public IList<LicenceAuthorisation> GetAuthorisations()
        {
            var authorisations = new List<LicenceAuthorisation>();
            using (var entities = new NouvemEntities())
            {
                var values = this.GetAuthorisationValues().Select(x => x.Description).ToList();
                foreach (var authorisation in entities.NouAuthorisations.Where(x => !x.Deleted))
                {
                    authorisations.Add(new LicenceAuthorisation {Authorisation = authorisation});
                }
            }

            return authorisations;
        }

        /// <summary>
        /// Retrieve all the user group values.
        /// </summary>
        /// <returns>A collection of user group values.</returns>
        public IList<NouAuthorisationValue> GetAuthorisationValues()
        {
            var values = new List<NouAuthorisationValue>();

            using (var entities = new NouvemEntities())
            {
                var dbValues = entities.NouAuthorisationValues.Where(x => !x.Deleted);
                dbValues.ToList().ForEach(x => values.Add(x));
            }

            return values;
        }
    }
}

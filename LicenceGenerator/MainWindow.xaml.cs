﻿// -----------------------------------------------------------------------
// <copyright file="LicenceGenerator.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace LicenceGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using LicenceGenerator.Properties;
    using Nouvem.Licencing;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region field

        /// <summary>
        /// The licence repository reference.
        /// </summary>
        private readonly LicenceRepository repository = new LicenceRepository();

        /// <summary>
        /// The licence generator reference.
        /// </summary>
        private readonly LicenseGenerator licenceGenerator = LicenseGenerator.Create(Settings.Default.LicencePath, true);
   
        /// <summary>
        /// The product modules to add the the licence.
        /// </summary>
        private readonly IDictionary<string, string> modules = new Dictionary<string, string>();

        /// <summary>
        /// The selected customer licence.
        /// </summary>
        private CustomerLicence selectedCustomerLicence;

        /// <summary>
        /// The types count.
        /// </summary>
        private int typeCount;

        /// <summary>
        /// The authorisations count.
        /// </summary>
        private int authorisationCount;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.GetData();
            this.GridControlAuthorisations.ItemsSource = this.Authorisations;
            this.GridControlLicenseType.ItemsSource = this.LicenceTypes;
            this.GridControlLicences.ItemsSource = this.Licences;
            this.TextBoxCustomer.Focus();
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets the authorisations.
        /// </summary>
        public ObservableCollection<LicenceAuthorisation> Authorisations  { get; private set; }

        /// <summary>
        /// Gets the licence types.
        /// </summary>
        public ObservableCollection<NouLicenceType> LicenceTypes { get; private set; }

        /// <summary>
        /// Gets the customer licences.
        /// </summary>
        public ObservableCollection<CustomerLicence> Licences { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the licence to be generated is a trial licence.
        /// </summary>
        public bool IsTrialLicence
        {
            get
            {
                return this.ToInt(this.TextBoxNoOfDays.Text) > 0;
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Retrieves the application data.
        /// </summary>
        private void GetData()
        {
            this.LicenceTypes = new ObservableCollection<NouLicenceType>(this.repository.GetLicenceTypes());
            this.Authorisations = new ObservableCollection<LicenceAuthorisation>(this.repository.GetAuthorisations());
            this.Licences = new ObservableCollection<CustomerLicence>(this.repository.GetCustomerLicenceDetails());
        }
       
        /// <summary>
        /// Generate the licence file.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameter.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.selectedCustomerLicence = this.GridControlLicences.GetFocusedRow() as CustomerLicence;
            if (this.IsTrialLicence)
            {
                // trial licence, so give the user 30 days to use it.
                this.DatePickerFrom.SelectedDate = DateTime.Today;
                this.DatePickerTo.SelectedDate = DateTime.Today.AddDays(30);
            }

            #region validation

            if (!modules.Any())
            {
                MessageBox.Show("You must add at least 1 licence type to the licence");
                return;
            }

            if (string.IsNullOrWhiteSpace(this.TextBoxNoOfDays.Text))
            {
                this.TextBoxNoOfDays.Text = "0";
            }

            if (!this.DatePickerFrom.SelectedDate.HasValue || !this.DatePickerTo.SelectedDate.HasValue)
            {
                MessageBox.Show("Please ensure all dates have been selected");
                return;
            }

            if (this.DatePickerTo.SelectedDate < this.DatePickerFrom.SelectedDate)
            {
                MessageBox.Show("Expiry date cannot precede start date");
                return;
            }

            if (this.DatePickerTo.SelectedDate < DateTime.Today)
            {
                MessageBox.Show("Expiry date has passed");
                return;
            }

            if (this.CheckBoxUpgrade.IsChecked == true && this.selectedCustomerLicence == null)
            {
                MessageBox.Show("Upgrade has been selected, but no licence to upgrade has been selected");
                return;
            }

            #endregion

            try
            {
                modules.Add("ValidFrom", this.DatePickerFrom.SelectedDate.ToString());
                modules.Add("ValidTo", this.DatePickerTo.SelectedDate.ToString());
                modules.Add("IssueDate", DateTime.Today.ToShortDateString());
                modules.Add("NoOfDays", this.TextBoxNoOfDays.Text);

                var upgradedFromLicenceKey = string.Empty;

                if (this.CheckBoxUpgrade.IsChecked == true)
                {
                    modules.Add("Upgrade", this.selectedCustomerLicence.LicenceKey);
                    upgradedFromLicenceKey = this.selectedCustomerLicence.LicenceKey;
                }
                else
                {
                    modules.Add("Upgrade", string.Empty);
                }
                
                var licenceDetail = this.licenceGenerator.GenerateLicense(Convert.ToDateTime(this.DatePickerTo.SelectedDate), 1, modules, this.TextBoxCustomer.Text, this.TextBoxEmail.Text);

                // add the actual file
                licenceDetail.File = File.ReadAllBytes(Path.Combine(Settings.Default.LicencePath, Settings.Default.LicenceName));

                // store to persistence
                this.repository.AddCustomerLicenceDetails(licenceDetail, upgradedFromLicenceKey);

                this.Reset();
                MessageBox.Show("Licence file successfully generated");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Add a licence type to a licence.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameter.</param>
        private void ButtonAddType_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.TextBoxNoOfUsers.Text))
            {
                MessageBox.Show("Number of users cannot be blank");
                return;
            }

            if (this.ToInt(this.TextBoxNoOfUsers.Text) == 0)
            {
                MessageBox.Show("Invalid number of users");
                return;
            }

            try
            {
                var type = this.GridControlLicenseType.SelectedItem as NouLicenceType;
                this.typeCount++;
                this.modules.Add(string.Format("Type{0}", typeCount), string.Format("{0}|{1}", type.LicenceName, this.TextBoxNoOfUsers.Text));

                foreach (var authorisation in this.Authorisations)
                {
                    this.authorisationCount++;
                    var selection = authorisation.FullAccess
                        ? "Full Access" : authorisation.ReadOnly 
                        ? "Read Only" : "No Access";

                    this.modules.Add(string.Format("Authorisation{0}", authorisationCount), string.Format("{0}|{1}|{2}", type.LicenceName, authorisation.Authorisation.Description, selection));
                }

                MessageBox.Show("Licence type successfully added");

                this.TextBoxNoOfUsers.Text = string.Empty;
                this.Authorisations.Clear();
                this.Authorisations = new ObservableCollection<LicenceAuthorisation>(this.repository.GetAuthorisations());
                this.GridControlAuthorisations.ItemsSource = this.Authorisations;

                this.TextBoxNoOfUsers.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Converts a string to a numeric value.
        /// </summary>
        /// <returns>The converted numeric value.</returns>
        private int ToInt(string source)
        {
            var conversion = 0;
            int.TryParse(source, out conversion);

            return conversion;
        }

        /// <summary>
        /// Reset ui/data.
        /// </summary>
        private void Reset()
        {
            this.TextBoxCustomer.Text = string.Empty;
            this.TextBoxEmail.Text = string.Empty;
            this.DatePickerFrom.SelectedDate = null;
            this.DatePickerTo.SelectedDate = null;
            this.modules.Clear();
            this.typeCount = 0;
            this.authorisationCount = 0;
            this.CheckBoxUpgrade.IsChecked = false;
            this.selectedCustomerLicence = null;
            this.Licences = new ObservableCollection<CustomerLicence>(this.repository.GetCustomerLicenceDetails());
        }

        /// <summary>
        /// Download a file.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void GridControlLicences_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                var file = this.selectedCustomerLicence.LicenceFile;

                var path = Path.Combine(Settings.Default.HistoricLicences, Settings.Default.LicenceName);
                File.WriteAllBytes(path, file);
                MessageBox.Show("Licence file downloaded");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion
    }
}

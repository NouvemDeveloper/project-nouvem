﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using Nouvem.EncryptionUI.Properties;
using Path = System.IO.Path;

namespace Nouvem.EncryptionUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
            this.TextBoxPath.Text = Settings.Default.FilePath;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var filePath = this.TextBoxPath.Text;
                var file = new XDocument();

                Security.Security.EncryptXmlFile(file, filePath);

                this.LabelMsg.Content = "File Encrypted";
                Settings.Default.FilePath = this.TextBoxPath.Text;
                Settings.Default.Save();
            }
            catch (Exception ex)
            {
                this.LabelMsg.Content = ex.Message;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var filePath = this.TextBoxPath.Text;
                var file = new XDocument();
                Security.Security.DecryptXmlFile(ref file, filePath);
                file.Save(filePath);
                this.LabelMsg.Content = "File decrypted";
                Settings.Default.FilePath = this.TextBoxPath.Text;
                Settings.Default.Save();
            }
            catch (Exception ex)
            {
                this.LabelMsg.Content = ex.Message;
            }
        }
    }
}

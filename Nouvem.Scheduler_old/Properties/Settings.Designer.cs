﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Scheduler.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string ReportServerPath {
            get {
                return ((string)(this["ReportServerPath"]));
            }
            set {
                this["ReportServerPath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SSRSUserName {
            get {
                return ((string)(this["SSRSUserName"]));
            }
            set {
                this["SSRSUserName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SSRSPassword {
            get {
                return ((string)(this["SSRSPassword"]));
            }
            set {
                this["SSRSPassword"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SSRSWebServiceURL {
            get {
                return ((string)(this["SSRSWebServiceURL"]));
            }
            set {
                this["SSRSWebServiceURL"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SSRSDomain {
            get {
                return ((string)(this["SSRSDomain"]));
            }
            set {
                this["SSRSDomain"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:80/ReportServer/ReportService2010.asmx")]
        public string Nouvem_Scheduler_ReportService_ReportingService2010 {
            get {
                return ((string)(this["Nouvem_Scheduler_ReportService_ReportingService2010"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchReportPrinter1 {
            get {
                return ((string)(this["DispatchReportPrinter1"]));
            }
            set {
                this["DispatchReportPrinter1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchReportPrinter2 {
            get {
                return ((string)(this["DispatchReportPrinter2"]));
            }
            set {
                this["DispatchReportPrinter2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchReportPrinter3 {
            get {
                return ((string)(this["DispatchReportPrinter3"]));
            }
            set {
                this["DispatchReportPrinter3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchReportPrinter4 {
            get {
                return ((string)(this["DispatchReportPrinter4"]));
            }
            set {
                this["DispatchReportPrinter4"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Nouvem\\NorthDown")]
        public string SharedFilePath {
            get {
                return ((string)(this["SharedFilePath"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int DispatchReportPrinterCopies {
            get {
                return ((int)(this["DispatchReportPrinterCopies"]));
            }
            set {
                this["DispatchReportPrinterCopies"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool DispatchReportConnect {
            get {
                return ((bool)(this["DispatchReportConnect"]));
            }
            set {
                this["DispatchReportConnect"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SaleOrderReportPrinter1 {
            get {
                return ((string)(this["SaleOrderReportPrinter1"]));
            }
            set {
                this["SaleOrderReportPrinter1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SaleOrderReportPrinter2 {
            get {
                return ((string)(this["SaleOrderReportPrinter2"]));
            }
            set {
                this["SaleOrderReportPrinter2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SaleOrderReportPrinter3 {
            get {
                return ((string)(this["SaleOrderReportPrinter3"]));
            }
            set {
                this["SaleOrderReportPrinter3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SaleOrderReportPrinter4 {
            get {
                return ((string)(this["SaleOrderReportPrinter4"]));
            }
            set {
                this["SaleOrderReportPrinter4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int SaleOrderReportPrinterCopies {
            get {
                return ((int)(this["SaleOrderReportPrinterCopies"]));
            }
            set {
                this["SaleOrderReportPrinterCopies"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SaleOrderReportConnect {
            get {
                return ((bool)(this["SaleOrderReportConnect"]));
            }
            set {
                this["SaleOrderReportConnect"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string InvoiceReportPrinter1 {
            get {
                return ((string)(this["InvoiceReportPrinter1"]));
            }
            set {
                this["InvoiceReportPrinter1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string InvoiceReportPrinter2 {
            get {
                return ((string)(this["InvoiceReportPrinter2"]));
            }
            set {
                this["InvoiceReportPrinter2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string InvoiceReportPrinter3 {
            get {
                return ((string)(this["InvoiceReportPrinter3"]));
            }
            set {
                this["InvoiceReportPrinter3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string InvoiceReportPrinter4 {
            get {
                return ((string)(this["InvoiceReportPrinter4"]));
            }
            set {
                this["InvoiceReportPrinter4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int InvoiceReportPrinterCopies {
            get {
                return ((int)(this["InvoiceReportPrinterCopies"]));
            }
            set {
                this["InvoiceReportPrinterCopies"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool InvoiceReportConnect {
            get {
                return ((bool)(this["InvoiceReportConnect"]));
            }
            set {
                this["InvoiceReportConnect"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchSummaryReportPrinter1 {
            get {
                return ((string)(this["DispatchSummaryReportPrinter1"]));
            }
            set {
                this["DispatchSummaryReportPrinter1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchSummaryReportPrinter2 {
            get {
                return ((string)(this["DispatchSummaryReportPrinter2"]));
            }
            set {
                this["DispatchSummaryReportPrinter2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchSummaryReportPrinter3 {
            get {
                return ((string)(this["DispatchSummaryReportPrinter3"]));
            }
            set {
                this["DispatchSummaryReportPrinter3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DispatchSummaryReportPrinter4 {
            get {
                return ((string)(this["DispatchSummaryReportPrinter4"]));
            }
            set {
                this["DispatchSummaryReportPrinter4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int DispatchSummaryReportPrinterCopies {
            get {
                return ((int)(this["DispatchSummaryReportPrinterCopies"]));
            }
            set {
                this["DispatchSummaryReportPrinterCopies"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool DispatchSummaryReportConnect {
            get {
                return ((bool)(this["DispatchSummaryReportConnect"]));
            }
            set {
                this["DispatchSummaryReportConnect"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int ReportsProcessingInterval {
            get {
                return ((int)(this["ReportsProcessingInterval"]));
            }
            set {
                this["ReportsProcessingInterval"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool RunReportsProcessing {
            get {
                return ((bool)(this["RunReportsProcessing"]));
            }
            set {
                this["RunReportsProcessing"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool BroadcastDBChanges {
            get {
                return ((bool)(this["BroadcastDBChanges"]));
            }
            set {
                this["BroadcastDBChanges"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int BroadcastDBChangesInterval {
            get {
                return ((int)(this["BroadcastDBChangesInterval"]));
            }
            set {
                this["BroadcastDBChangesInterval"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string AccountsDirectory {
            get {
                return ((string)(this["AccountsDirectory"]));
            }
            set {
                this["AccountsDirectory"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SyncAccountsData {
            get {
                return ((bool)(this["SyncAccountsData"]));
            }
            set {
                this["SyncAccountsData"] = value;
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Data.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Logging;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Trigger
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Jobs;
    using Nouvem.Scheduler.Jobs.Handler;
    using Nouvem.Scheduler.Model.Repository;
    using Nouvem.Scheduler.Properties;
    using Quartz;
    using Quartz.Impl;

    public class Data
    {
        private ILogger log = new Logger();

        /// <summary>
        /// Handles the service start operations.
        /// </summary>
        public void SchedulerStart()
        {
            this.log.LogInfo(this.GetType(), "SchedulerStarting: Registering start up..Version 6");
            var schedFact = new StdSchedulerFactory();
        
            var sched = schedFact.GetScheduler();
            sched.Start();
   
            var job = JobBuilder.Create<SchedulerStartProcessing>()
                .WithIdentity("SchedulerStart")
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            var trigger = TriggerBuilder.Create()
              .WithIdentity("SchedulerTrigger")
              .StartNow()
              .Build();

            sched.ScheduleJob(job, trigger);
            this.log.LogInfo(this.GetType(), "SchedulerStart(): Registered");
        }

        /// <summary>
        /// Saves the scheduler settings.
        /// </summary>
        public void SaveSettings()
        {
            var settings = new List<DeviceSetting>();
            var deviceId = SchedulerGlobal.Device.DeviceID;

            #region settings

            settings.Add(new DeviceSetting
            {
                Name = "Scheduler_DispatchReport",
                Value1 = Settings.Default.DispatchReportPrinter1,
                Value2 = Settings.Default.DispatchReportPrinter2,
                Value3 = Settings.Default.DispatchReportPrinter3,
                Value4 = Settings.Default.DispatchReportPrinter4,
                DeviceMasterID = deviceId
            });

            #endregion

            var data = new DataRepository();
            data.UpdateDeviceSettings(settings);
        }


        public class SchedulerStartProcessing : HandlerBase, IJob
        {
            public void Execute(IJobExecutionContext context)
            {
                this.log.LogInfo(this.GetType(), "SchedulerStartProcessing(): Starting...");
                this.GetDeviceSettings();
                this.ScheduleJobs();
            }

            /// <summary>
            /// Gets the scheduler settings.
            /// </summary>
            private void GetDeviceSettings()
            {
                var settings = Data.GetDeviceSettings();
                foreach (var setting in settings)
                {
                    #region settings

                    if (setting.Name.Equals("ReportServerPath"))
                    {
                        Settings.Default.ReportServerPath = setting.Value1;
                        continue;
                    }

                    if (setting.Name.Equals("SSRSWebServiceURL"))
                    {
                        Settings.Default.SSRSWebServiceURL = setting.Value1;
                        continue;
                    }

                    if (setting.Name.Equals("SSRSUserName"))
                    {
                        Settings.Default.SSRSUserName = setting.Value1;
                        continue;
                    }

                    if (setting.Name.Equals("SSRSPassword"))
                    {
                        Settings.Default.SSRSPassword = setting.Value1;
                        continue;
                    }

                    if (setting.Name.Equals("SSRSDomain"))
                    {
                        Settings.Default.SSRSDomain = setting.Value1;
                        continue;
                    }

                    if (setting.Name.Equals("Scheduler_InvoiceReport"))
                    {
                        Settings.Default.InvoiceReportPrinter1 = setting.Value1;
                        Settings.Default.InvoiceReportPrinter2 = setting.Value2;
                        Settings.Default.InvoiceReportPrinter3 = setting.Value3;
                        Settings.Default.InvoiceReportPrinter4 = setting.Value4;
                        Settings.Default.InvoiceReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.InvoiceReportConnect = setting.Value6.ToBool();
                        continue;
                    }

                    if (setting.Name.Equals("Scheduler_Job_ReportProcessing"))
                    {
                        Settings.Default.ReportsProcessingInterval = setting.Value1.ToInt();
                        Settings.Default.RunReportsProcessing = setting.Value2.ToBool();
                        Settings.Default.SyncAccountsData = setting.Value3.ToBool();
                        continue;
                    }

                    if (setting.Name.Equals("Scheduler_Job_BroadcastDBChanges"))
                    {
                        Settings.Default.BroadcastDBChanges = setting.Value1.ToBool();
                        Settings.Default.BroadcastDBChangesInterval = setting.Value2.ToInt();
                        continue;
                    }

                    if (setting.Name.Equals("Scheduler_DispatchReport"))
                    {
                        Settings.Default.DispatchReportPrinter1 = setting.Value1;
                        Settings.Default.DispatchReportPrinter2 = setting.Value2;
                        Settings.Default.DispatchReportPrinter3 = setting.Value3;
                        Settings.Default.DispatchReportPrinter4 = setting.Value4;
                        Settings.Default.DispatchReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.DispatchReportConnect = setting.Value6.ToBool();
                        continue;
                    }

                    if (setting.Name.Equals("Scheduler_DispatchSummaryReport"))
                    {
                        Settings.Default.DispatchSummaryReportPrinter1 = setting.Value1;
                        Settings.Default.DispatchSummaryReportPrinter2 = setting.Value2;
                        Settings.Default.DispatchSummaryReportPrinter3 = setting.Value3;
                        Settings.Default.DispatchSummaryReportPrinter4 = setting.Value4;
                        Settings.Default.DispatchSummaryReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.DispatchSummaryReportConnect = setting.Value6.ToBool();
                        continue;
                    }

                    if (setting.Name.Equals("Scheduler_SaleOrderReport"))
                    {
                        Settings.Default.SaleOrderReportPrinter1 = setting.Value1;
                        Settings.Default.SaleOrderReportPrinter2 = setting.Value2;
                        Settings.Default.SaleOrderReportPrinter3 = setting.Value3;
                        Settings.Default.SaleOrderReportPrinter4 = setting.Value4;
                        Settings.Default.SaleOrderReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.SaleOrderReportConnect = setting.Value6.ToBool();
                        continue;
                    }

                    if (setting.Name.Equals("AccountsPackage"))
                    {
                        Settings.Default.AccountsDirectory = setting.Value9;
                        continue;
                    }

                    #endregion
                }
            }

            private void ScheduleJobs()
            {
                //this.job.RunAutoSaleOrderProcessor();
                //this.BackUpApplication();
                //this.CheckForDatabaseChanges();
                this.CheckSpecialPrices();
                if (ApplicationSettings.RunReportsProcessing)
                {
                    this.CheckReportsForPrinting();
                }

                if (ApplicationSettings.BroadcastDBChanges)
                {
                    this.CheckForDatabaseChanges();
                }



                //job.CheckDispatchDockets();
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckDataIntegration()
            {
                this.log.LogInfo(this.GetType(), "CheckDataIntegration(): checking..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<DataIntegration>()
                    .WithIdentity("CheckDataIntegration")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("CheckDataIntegrationTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInSeconds(ApplicationSettings.ReportsProcessingInterval)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckReportsForPrinting(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckReportsForPrinting()
            {
                this.log.LogInfo(this.GetType(), "CheckReportsForPrinting(): Registering..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ReportProcessing>()
                    .WithIdentity("CheckReportsForPrintingJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("CheckReportsForPrintingTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInSeconds(ApplicationSettings.ReportsProcessingInterval)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckReportsForPrinting(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForDatabaseChanges()
            {
                this.log.LogInfo(this.GetType(), "CheckForDatabaseChanges(): Registering..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<Broadcaster>()
                    .WithIdentity("CheckForDatabaseChanges")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("CheckForDatabaseChangesTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(ApplicationSettings.BroadcastDBChangesInterval)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForDatabaseChanges(): Registered");
            }

            /// <summary>
            /// Runs the auto sale order processor.
            /// </summary>
            public void RunAutoSaleOrderProcessor()
            {
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<AutoSaleOrderProcessor>()
                    .WithIdentity("RunAutoSaleOrderProcessor")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("RunAutoSaleOrderProcessorTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(100)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
            }


            /// <summary>
            /// Checks for future prices to be implemented.
            /// </summary>
            public void BackUpApplication()
            {
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var specialPricesJob = JobBuilder.Create<BackUpApplication>()
                    .WithIdentity("BackUpApplicationJob")
                    .Build();

                //var specialPricesTrigger = TriggerBuilder.Create()
                //  .WithIdentity("BackUpApplicationTrigger")
                //  .StartNow()
                //  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(21, 00))
                //  .ForJob("BackUpApplicationJob")
                //  .Build();

                var specialPricesTrigger = TriggerBuilder.Create()
                  .WithIdentity("BackUpApplicationTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(100)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(specialPricesJob, specialPricesTrigger);
            }

            /// <summary>
            /// Checks for future prices to be implemented.
            /// </summary>
            public void CheckSpecialPrices()
            {
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var specialPricesJob = JobBuilder.Create<SpecialPrices>()
                    .WithIdentity("SpecialPricesJob")
                    .Build();

                var specialPricesTrigger = TriggerBuilder.Create()
                  .WithIdentity("SpecialPricesTrigger")
                  .StartNow()
                  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(2, 00))
                  .ForJob("SpecialPricesJob")
                  .Build();

                //var specialPricesTrigger = TriggerBuilder.Create()
                //  .WithIdentity("SpecialPricesTrigger")
                //  .StartNow()
                //  .WithSimpleSchedule(x => x
                //      .WithIntervalInMinutes(100)
                //      .RepeatForever())
                //  .Build();

                sched.ScheduleJob(specialPricesJob, specialPricesTrigger);
            }

            /// <summary>
            /// Check the dispatch dockets line/header totals, ensuring they tally.
            /// </summary>
            public void CheckDispatchDockets()
            {
                this.log.LogInfo(this.GetType(), "SchedulerStart(): Registering..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<DispatchDocketTally>()
                    .WithIdentity("DispatchDocketJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("DispatchDocketTrigger")
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(1)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckReportsForPrinting(): Registered");
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ReportProcessing.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Scheduler.Properties;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Scheduler.BusinessLogic;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Model.BusinessObject;
    using Quartz;

    public class ReportProcessing : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "ReportProcessing..");

            var dispatchReportsToPrint = new List<int>();

            var reportPath = string.Empty;
            var manager = ReportManager.Instance;

            this.log.LogInfo(this.GetType(), string.Format("Invoice Connect:{0}", ApplicationSettings.InvoiceReportConnect));
            if (ApplicationSettings.InvoiceReportConnect)
            {
                this.log.LogInfo(this.GetType(), "Processing invoices");
                var invoiceDocket = manager.Reports.FirstOrDefault(x => x.Name == ReportName.InvoiceDocket);
                if (invoiceDocket != null)
                {
                    reportPath = invoiceDocket.Path;
                }

                try
                {
                    var reportsToPrint = Data.CheckForUnprintedInvoiceDockets();
                    this.log.LogInfo(this.GetType(), string.Format("{0} invoices to process", reportsToPrint.Count));
                    foreach (var invoiceId in reportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { invoiceId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.InvoiceDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter1, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.InvoiceDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter2, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.InvoiceDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter3, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.InvoiceDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter4, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }
                    }

                    Data.UpdateInvoiceDockets(reportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }

            if (ApplicationSettings.DispatchReportConnect)
            {
                this.log.LogInfo(this.GetType(), "Processing dispatches");
                var dispatchDocket = manager.Reports.FirstOrDefault(x => x.Name == ReportName.BloorsDispatchDocket);
                if (dispatchDocket != null)
                {
                    reportPath = dispatchDocket.Path;
                }

                try
                {
                    dispatchReportsToPrint = Data.CheckForUnprintedDispatchDockets();
                    this.log.LogInfo(this.GetType(), string.Format("{0} dispatches to process", dispatchReportsToPrint.Count));
                    foreach (var ardispatchId in dispatchReportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { ardispatchId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter1, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter2, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter3, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter4, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }
                    }

                    Data.UpdateDispatchDockets(dispatchReportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }

            if (ApplicationSettings.DispatchSummaryReportConnect)
            {
                this.log.LogInfo(this.GetType(), "Processing dispatches summarys");
                var dispatchSummaryDocket = manager.Reports.FirstOrDefault(x => x.Name == ReportName.BloorsDispatchSummary);
                if (dispatchSummaryDocket != null)
                {
                    reportPath = dispatchSummaryDocket.Path;
                }

                try
                {
                    if (!dispatchReportsToPrint.Any())
                    {
                        dispatchReportsToPrint = Data.CheckForUnprintedDispatchDockets();
                    }

                    this.log.LogInfo(this.GetType(), string.Format("{0} dispatch sumamarys to process", dispatchReportsToPrint.Count));
                    foreach (var ardispatchId in dispatchReportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Dispatchid", Values = { ardispatchId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter1, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter2, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter3, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter4, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }
                    }

                    Data.UpdateDispatchDockets(dispatchReportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }

            if (ApplicationSettings.SaleOrderReportConnect)
            {
                this.log.LogInfo(this.GetType(), "Processing sale orders");
                var saleOrder = manager.Reports.FirstOrDefault(x => x.Name == ReportName.BloorsSaleOrder);
                if (saleOrder != null)
                {
                    reportPath = saleOrder.Path;
                }

                try
                {
                    var reportsToPrint = Data.CheckForUnprintedSaleOrderDockets();
                    this.log.LogInfo(this.GetType(), string.Format("{0} sale orders to process", reportsToPrint.Count));
                    foreach (var saleOrderId in reportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "SalesID", Values = { saleOrderId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsSaleOrder, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter1, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsSaleOrder, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter2, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsSaleOrder, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter3, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsSaleOrder, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter4, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }
                    }

                    Data.UpdateSaleOrderDockets(reportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }
        }
    }
}
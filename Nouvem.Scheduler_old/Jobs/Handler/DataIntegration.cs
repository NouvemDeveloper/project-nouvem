﻿// -----------------------------------------------------------------------
// <copyright file="Broadcaster.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using Nouvem.Scheduler.Global;
    using Quartz;
    using Nouvem.AccountsIntegration;

    public class DataIntegration : HandlerBase, IJob
    {
        private Nouvem.AccountsIntegration.Exchequer accounts = new Exchequer();

        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "broadcasting check..");

        }

        private void GetPartners()
        {
            var data = this.accounts.GetPartners(ApplicationSettings.AccountsDirectory);
            var partners = data.Item1;
            var error = data.Item2;
            foreach (var partner in partners)
            {
                
            }
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="DispatchDocketTally.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Collections.Generic;
    using System.Net.Mail;
    using Quartz;
    using Nouvem.Email;

    public class DispatchDocketTally : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var testResult = Data.CheckDispatchDockets();
            if (testResult != string.Empty)
            {
                var sendMessage = string.Format(
                    "Topping Meats: The following dispatch dockets are not tallying:{0}{1}", Environment.NewLine,
                    testResult);

                try
                {
                    System.IO.File.WriteAllText(@"C:\Nouvem\Test.txt", sendMessage);
                    var mail = new Email();
                    mail.SendEmail(new EmailSettings
                    {
                        FromAddress = "Nouvem.TaskScheduler@gmail.com",
                        ToAddress = "brianmurray@nouvem.com",
                        FromMailPassword = "Nouv1234",
                        Subject = "Task Scheduler Notification Alert ! (TEST)",
                        Body = sendMessage,
                        CCAddresses = new List<MailAddress>{new MailAddress("oliverhayden@nouvem.com")}
                    });
                }
                catch (Exception ex)
                {
                    System.IO.File.WriteAllText(@"C:\Nouvem\Test.txt", ex.Message);
                }
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Report.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Model.BusinessObject
{
    using System.Collections.Generic;
    using Microsoft.Reporting.WinForms;

    public class ReportData
    {
        /// <summary>
        /// The report name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The report path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public IEnumerable<ReportParameter> ReportParameters { get; set; }
    }
}

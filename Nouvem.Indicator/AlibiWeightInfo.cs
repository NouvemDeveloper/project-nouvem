﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Indicator
{
    public class AlibiWeightInfo
    {
        /// <summary>
        /// Gets or sets the alibi number.
        /// </summary>
        public long Alibi { get; set; }

        /// <summary>
        /// Gets or sets the date stored..
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// Gets or sets the time stored.
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Gets or sets the alibi associated weight.
        /// </summary>
        public decimal NetWeight { get; set; }

        /// <summary>
        /// Gets or sets the weight associated tare.
        /// </summary>
        public decimal Tare { get; set; }

        /// <summary>
        /// Gets the gross weight.
        /// </summary>
        public decimal GrossWeight
        {
            get { return this.NetWeight + this.Tare; }
        }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public decimal Total { get; set; }
    }
}

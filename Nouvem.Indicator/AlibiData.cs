﻿// -----------------------------------------------------------------------
// <copyright file="AlibiData.cs" company="Nouvem Limited">
// Copyright (c)Nouvem Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Nouvem.Indicator
{
    /// <summary>
    /// Struct that holds the alibi data.
    /// </summary>
    public struct AlibiData
    {
        /// <summary>
        /// Gets or sets the alibi number.
        /// </summary>
        public long Alibi { get; set; }

        /// <summary>
        /// Gets or sets the date of storage.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Gets or sets the time of storage.
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        /// Gets or sets the tare weight.
        /// </summary>
        public decimal Tare { get; set; }

        /// <summary>
        /// Gets or sets the tare weight.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the tare weight.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Gets or sets the scales sensitivity.
        /// </summary>
        public int Sensitivity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the scales are in motion.
        /// </summary>
        public bool Motion { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the scales are in stable.
        /// </summary>
        public bool Stable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the weight is clean.
        /// </summary>
        public bool IsClean { get; set; }
    }
}


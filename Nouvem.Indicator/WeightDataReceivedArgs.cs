﻿// -----------------------------------------------------------------------
// <copyright file="WeightDataReceivedArgs.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Indicator
{
    using System;

    /// <summary>
    /// Event arguments class for the weight received event.
    /// </summary>
    public class WeightDataReceivedArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeightDataReceivedArgs "/> class.
        /// </summary>
        /// <param name="serialData">The weight value to broadcast.</param>
        /// <param name="isClean">The weight clean reads flag to broadcast.</param>
        /// <param name="inMotion">The weight in motion flag.</param>
        public WeightDataReceivedArgs(string serialData, bool isClean, bool inMotion)
        {
            this.SerialData = serialData;
            this.IsClean = isClean;
            this.InMotion = inMotion;
        }

        /// <summary>
        /// Gets or sets the serial data,
        /// </summary>
        public string SerialData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the weight is clean or not.
        /// </summary>
        public bool IsClean { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the acles are in motion.
        /// </summary>
        public bool InMotion { get; set; }
    }
}



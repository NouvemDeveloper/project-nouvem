﻿// -----------------------------------------------------------------------
// <copyright file="Designer.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ReportDesigner
{
    using System.Windows.Forms;

    public partial class Designer : Form
    {
        public Designer()
        {
            this.InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }
    } 
}

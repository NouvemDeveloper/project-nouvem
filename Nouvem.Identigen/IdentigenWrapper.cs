﻿// -----------------------------------------------------------------------
// <copyright file="IdentigenWrapper.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Identigen
{
    using System;

    public class IdentigenWrapper
    {
       /// <summary>
       /// Uploads the sample file to identigen.
       /// </summary>
       /// <param name="userName">The user name.</param>
       /// <param name="password">The password.</param>
       /// <param name="file">The file to send.</param>
       /// <param name="killdate">The kill date.</param>
       /// <param name="location">The factory code/name.</param>
        public void UploadSamples(string userName, string password, string file, DateTime killdate, string location)
        {
            var wrapper = new com.identigen.cloud1.WsIdentigenSamplesUpload();
            wrapper.UploadCsvWithKillDate_v2(userName, password, file, killdate, location);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="LicenceGenerator.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.PasswordRetriever
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
            this.DatePickerDate.SelectedDate = DateTime.Today;
        }

        /// <summary>
        /// Returns a 4 digit monthy password.
        /// </summary>
        /// <returns>A 4 digit monthly password.</returns>
        public string GetNouvemPassword(DateTime date)
        {
            var year = date.Year;
            var month = date.Month;
            var temp = year%2 == 0 ? (year/month)/4 : (year/month)/7;
            var temp2 = month%2 == 0 ? (temp*93) : (temp*54);

            return new string(temp2.ToString().ToCharArray().Reverse().ToArray()).PadRight(4, '9').Substring(0,4);
        }

        /// <summary>
        /// Handles the date changed event, retrieving the associated nouvem password.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arg.</param>
        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DatePickerDate.SelectedDate.HasValue)
            {
                var date = Convert.ToDateTime(this.DatePickerDate.SelectedDate);
                this.TextBoxPassword.Text = this.GetNouvemPassword(date);
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="PrintManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Scheduler.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Scheduler.BusinessLogic
{
    using System;
    using System.Net;
    using Neodynamic.SDK.Printing;
    using Nouvem.Logging;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Model.Repository;

    public class PrintManager
    {
        #region field

        /// <summary>
        /// The manager singleton.
        /// </summary>
        private static PrintManager instance = new PrintManager();

        /// <summary>
        /// The application products.
        /// </summary>
        private IList<INMaster> products = new List<INMaster>();

        /// <summary>
        /// The application partners.
        /// </summary>
        private IList<BPMaster> partners = new List<BPMaster>();

        /// <summary>
        /// The app logger.
        /// </summary>
        private ILogger log = new Logger();

        /// <summary>
        /// The data repository.
        /// </summary>
        private DataRepository data = new DataRepository();

        /// <summary>
        /// The label orientation.
        /// </summary>
        private PrintOrientation orientation = PrintOrientation.Portrait;

        #endregion

        #region constructor

        private PrintManager()
        {
            this.log.LogInfo(this.GetType(), "Setting up printer..");
            this.products = this.data.GetProducts();
            this.partners = this.data.GetBusinessPartners();
            var localLabel = this.data.GetLabel("PalletLabel");
            if (localLabel != null)
            {
                try
                {
                    this.orientation = (PrintOrientation)Enum.Parse(typeof(PrintOrientation), localLabel.Orientation);
                }
                catch
                {
                    this.orientation = PrintOrientation.Portrait;
                }
               
                this.Label = new ThermalLabel();
                this.Label.LoadXmlTemplate(localLabel.LabelFile);
            }

            this.SetPrinterSettings();
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the label to print.
        /// </summary>
        public ThermalLabel Label { get; set; }

        /// <summary>
        /// Gets or sets the printer settings.
        /// </summary>
        public PrinterSettings PrinterSettings { get; set; }

        /// <summary>
        /// Gets or sets the printer settings.
        /// </summary>
        public PrinterSettings Printer2Settings { get; set; }

        /// <summary>
        /// Gets or sets the printer settings.
        /// </summary>
        public PrinterSettings Printer3Settings { get; set; }

        /// <summary>
        /// Gets the singleton.
        /// </summary>
        public static PrintManager Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Process the print operation.
        /// </summary>
        /// <param name="customerId">The customer id to search for.</param>
        /// <param name="customerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <param name="mode">The station we are printng at.</param>
        /// <param name="labelType">The label type required.</param>
        /// <param name="stockTransactionId">The transaction id record to retrieve.</param>
        /// <param name="numCopies">The number of labels required.</param>
        /// <returns>A collection of the label ids printed.</returns>
        public Tuple<List<int>, List<byte[]>, int> ProcessPrinting(
            int? customerId,
            int? customerGroupId,
            int? productId,
            int? productGroupId,
            LabelType labelType,
            int stockTransactionId,
            int numCopies = 1,
            Process process = null,
            int? printer = null,
            int? warehouseId = null,
            int? nouTransactionTypeId = null,
            int? deviceId = null)
        {
            var labelids = new List<int>();
            var printerNo = 1;
            var storedLabels = new List<byte[]>();

            var localProduct = this.products.FirstOrDefault(x => x.INMasterID == productId);
            if (localProduct != null)
            {
                productGroupId = localProduct.INGroupID;
            }

            if (customerId.HasValue)
            {
                var localCustomer = this.partners.FirstOrDefault(x => x.BPMasterID == customerId);
                if (localCustomer != null)
                {
                    customerGroupId = localCustomer.BPGroupID;
                }
            }

            var localLabels = new List<LabelData>();
            var lab = LabelManager.Instance.GetLabels(customerId, customerGroupId, productId, productGroupId,
                labelType, process, warehouseId);

            if (lab != null)
            {
                localLabels = lab.ToList();
            }
            else
            {
                this.log.LogError(this.GetType(), "Label match not found");
                throw new Exception(Message.NoLabelFound);
            }

            foreach (var localLabelData in localLabels)
            {
                var localLabel = localLabelData.Label;
                var labelQty = localLabelData.LabelQty;
                var localPrinter = localLabelData.Printer;

                if (numCopies > labelQty)
                {
                    // piece label set to the default 1 in the association, so we use the product pieces value
                    labelQty = numCopies;
                }

                if (localLabel != null)
                {
                    this.Label = new ThermalLabel();
                    this.Label.LoadXmlTemplate(localLabel.LabelFile);
                    this.log.LogInfo(this.GetType(), "Label loaded");

                    System.Data.DataTable dataSource = null;
                    if (nouTransactionTypeId == 2)
                    {
                        dataSource = this.data.GetIntakeLabelData(stockTransactionId);
                    }
                    else if (nouTransactionTypeId == 4)
                    {
                        dataSource = this.data.GetOutOfProductionLabelData(stockTransactionId);
                    }
                    else if (nouTransactionTypeId == 7)
                    {
                        dataSource = this.data.GetDispatchLabelData(stockTransactionId);
                    }
                    else if (nouTransactionTypeId == 8)
                    {
                        dataSource = this.data.GetPalletLabelData(stockTransactionId);
                    }

                    this.Label.DataSource = dataSource;
                    this.SetMultiLineText(dataSource);

                    this.orientation = PrintOrientation.Portrait;
                    if (!string.IsNullOrEmpty(localLabel.Orientation))
                    {
                        this.orientation = (PrintOrientation)Enum.Parse(typeof(PrintOrientation), localLabel.Orientation);
                    }

                    if (printer.HasValue)
                    {
                        printerNo = printer.ToInt();
                    }
                    else if (localPrinter == null || localPrinter.PrinterNumber == 1)
                    {
                        printerNo = 1;
                    }
                    else
                    {
                        printerNo = localPrinter.PrinterNumber;
                    }

                    if (nouTransactionTypeId == 8)
                    {
                        printerNo = 2;

                        if (deviceId != null && deviceId == ApplicationSettings.PrinterPallet2Device)
                        {
                            printerNo = 3;
                        }
                    }

                    var label = this.Print(labelQty, printerNo);
                    if (!string.IsNullOrEmpty(label))
                    {
                        var convertedLabel = label.ToBytes();
                        if (convertedLabel != null)
                        {
                            storedLabels.Add(convertedLabel);
                        }
                    }

                    labelids.Add(localLabel.LabelID);
                }
                else
                {
                    this.log.LogError(this.GetType(), "Label match not found");
                    throw new Exception(Message.NoLabelFound);
                }
            }

            return Tuple.Create(labelids, storedLabels, printerNo);
        }

        /// <summary>
        /// Sets the printer settings.
        /// </summary>
        private void SetPrinterSettings()
        {
            this.PrinterSettings = new PrinterSettings();
            this.PrinterSettings.Communication.CommunicationType = CommunicationType.Network;
            this.PrinterSettings.Dpi = ApplicationSettings.PrinterDPI;
            this.PrinterSettings.Communication.NetworkPort = 9100;
            this.PrinterSettings.Communication.NetworkIPAddress = IPAddress.Parse(ApplicationSettings.PrinterIP);
            this.log.LogInfo(this.GetType(), string.Format("Setting up printer: Printer ip:{0}, DPI:{1}", ApplicationSettings.PrinterIP, ApplicationSettings.PrinterDPI));

            if (!string.IsNullOrEmpty(ApplicationSettings.PrinterPalletIP))
            {
                this.Printer2Settings = new PrinterSettings();
                this.Printer2Settings.Communication.CommunicationType = CommunicationType.Network;
                this.Printer2Settings.Dpi = ApplicationSettings.PrinterDPI;
                this.Printer2Settings.Communication.NetworkPort = 9100;
                this.Printer2Settings.Communication.NetworkIPAddress = IPAddress.Parse(ApplicationSettings.PrinterPalletIP);
                this.log.LogInfo(this.GetType(), string.Format("Setting up printer 2: Printer ip:{0}, DPI:{1}", ApplicationSettings.PrinterPalletIP, ApplicationSettings.PrinterDPI));
            }

            if (!string.IsNullOrEmpty(ApplicationSettings.PrinterPalletIP2))
            {
                this.Printer3Settings = new PrinterSettings();
                this.Printer3Settings.Communication.CommunicationType = CommunicationType.Network;
                this.Printer3Settings.Dpi = ApplicationSettings.PrinterDPI;
                this.Printer3Settings.Communication.NetworkPort = 9100;
                this.Printer3Settings.Communication.NetworkIPAddress = IPAddress.Parse(ApplicationSettings.PrinterPalletIP2);
                this.log.LogInfo(this.GetType(), string.Format("Setting up printer 3: Printer ip:{0}, DPI:{1}", ApplicationSettings.PrinterPalletIP2, ApplicationSettings.PrinterDPI));
            }
        }

        /// <summary>
        /// Sets the multi line text.
        /// </summary>
        /// <param name="dataSource">The datasource.</param>
        private void SetMultiLineText(DataTable dataSource)
        {
            foreach (var item in this.Label.Items)
            {
                if (item.DataField.Equals("#MultiLine Text1"))
                {
                    if (item is TextItem && dataSource != null && dataSource.Rows.Count > 0)
                    {
                        (item as TextItem).Text = dataSource.Rows[0]["#MultiLine Text1"].ToString();
                        (item as TextItem).DataField = string.Empty;
                    }

                    continue;
                }

                if (item.DataField.Equals("#MultiLine Text2"))
                {
                    if (item is TextItem && dataSource != null && dataSource.Rows.Count > 0)
                    {
                        (item as TextItem).Text = dataSource.Rows[0]["#MultiLine Text2"].ToString();
                        (item as TextItem).DataField = string.Empty;
                    }
                }

                if (item.DataField.Equals("#Text1"))
                {
                    if (item is TextItem && dataSource != null && dataSource.Rows.Count > 0)
                    {
                        (item as TextItem).Text = dataSource.Rows[0]["#Text1"].ToString();
                        (item as TextItem).DataField = string.Empty;
                    }
                }
            }
        }

        /// <summary> Prints the label.
        /// </summary>
        /// <param name="numCopies">The number of labels to print.</param>
        public string Print(int numCopies = 1, int printerNo = 1)
        {
            var localPrinterSettings = this.PrinterSettings;
            if (printerNo == 2)
            {
                localPrinterSettings = this.Printer2Settings;
            }
            else if (printerNo == 3)
            {
                localPrinterSettings = this.Printer3Settings;
            }

            try
            {
                using (var printJob = new PrintJob(localPrinterSettings))
                {
                    for (int i = 0; i < numCopies; i++)
                    {
                        printJob.ThermalLabel = this.Label;
                        printJob.PrintOrientation = this.orientation;

                        #region licence failsafe

                        ThermalLabel.LicenseOwner = "nouvem limited-Ultimate Edition-Developer License";
                        ThermalLabel.LicenseKey = "TGYVQ5YUM3758TPAJS26FKM5ZERNVWKQQ4J8FTDTTAHB4UUGBZ5Q";

                        #endregion

                        printJob.Print();
                        this.log.LogInfo(this.GetType(), "Label Printed");
                        if (i + 1 == numCopies)
                        {
                            return printJob.GetNativePrinterCommands();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Ex:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return string.Empty;
        }

        public IList<PalletStock> GetStockToReprint()
        {
            return this.data.GetStockToReprint();
        }

        public void ConsumeStockToReprint()
        {
            this.data.ConsumeStockToReprint();
        }

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <param name="labelIds">The label ids.</param>
        /// <param name="images">The label images.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public bool AddLabelToTransaction(int transId, string labelIds, List<byte[]> images, int printerNo = 1)
        {
            var storedLabel = new StoredLabel();
            storedLabel.PrinterNo = printerNo;
            var imageCount = images.Count;
            if (imageCount > 0)
            {
                storedLabel.Label1 = images.ElementAt(0);
            }

            if (imageCount > 1)
            {
                storedLabel.Label2 = images.ElementAt(1);
            }

            if (imageCount > 2)
            {
                storedLabel.Label3 = images.ElementAt(2);
            }

            if (imageCount > 3)
            {
                storedLabel.Label4 = images.ElementAt(3);
            }

            if (imageCount > 4)
            {
                storedLabel.Label5 = images.ElementAt(4);
            }

            if (imageCount > 5)
            {
                storedLabel.Label6 = images.ElementAt(5);
            }

            if (imageCount > 6)
            {
                storedLabel.Label7 = images.ElementAt(6);
            }

            if (imageCount > 7)
            {
                storedLabel.Label8 = images.ElementAt(7);
            }

            return this.data.AddLabelToTransaction(transId, labelIds, storedLabel);
        }

        #endregion
    }
}

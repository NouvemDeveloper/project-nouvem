﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neodynamic.SDK.Printing;
using Nouvem.Logging;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Scheduler.Model.Enum;
using Nouvem.Scheduler.Model.Repository;
using Nouvem.Shared;

namespace Nouvem.Scheduler.BusinessLogic
{
    public class LabelManager
    {
        #region field

        /// <summary>
        /// The manager singleton.
        /// </summary>
        private static LabelManager instance = new LabelManager();

        /// <summary>
        /// The app logger.
        /// </summary>
        private ILogger log = new Logger();

        /// <summary>
        /// The data repository.
        /// </summary>
        private DataRepository data = new DataRepository();

        /// <summary>
        /// The application product groups.
        /// </summary>
        private IList<INGroup> productGroups = new List<INGroup>();

        #endregion

        #region constructor

        private LabelManager()
        {
            this.GetProductGroups();
            this.GetLabels();
            this.GetLabelAssociations();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets the label manager singleton.
        /// </summary>
        public static LabelManager Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// The application labels.
        /// </summary>
        private IList<Label> labels = new List<Label>();

        /// <summary>
        /// The label associations.
        /// </summary>
        private IList<LabelsAssociation> labelAssociations = new List<LabelsAssociation>();

        #endregion

        #region method

        /// <summary>
        /// Gets the application labels.
        /// </summary>
        public void GetLabels()
        {
            this.labels = this.data.GetLabels();
        }

        /// <summary>
        /// Gets the label associations.
        /// </summary>
        private void GetLabelAssociations()
        {
            this.labelAssociations = this.data.GetLabelAssociations().OrderBy(x => x.WarehouseID).ToList();
        }

        /// <summary>
        /// Gets the current label association.
        /// </summary>
        /// <param name="customerId">The customer id to search for.</param>
        /// <param name="customerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <returns>The current associated label(s).</returns>
        public IList<LabelData> GetLabels(int? customerId, int? customerGroupId, int? productId, int? productGroupId, LabelType labelType, Process process = null, int? wareHouseId = null)
        {
            var labelAssoc = this.GetAssociatedLabel(customerId, customerGroupId, productId, productGroupId, process, wareHouseId: wareHouseId);

            if (labelAssoc == null)
            {
                this.log.LogInfo(this.GetType(), "No matching label found to print");
                return null;
            }

            var localLabelDatas = new List<LabelData>();
            List<Label> localLabels;
            var labelQty = 1;
            Printer localPrinter = null;
            switch (labelType)
            {
                case LabelType.Box:
                    localLabels = labelAssoc.BoxLabels.ToList();
                    labelQty = labelAssoc.BoxLabelQty.IsNullOrZero() ? 1 : labelAssoc.BoxLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterBox;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                case LabelType.Item:
                    localLabels = labelAssoc.ItemLabels.ToList();
                    labelQty = labelAssoc.ItemLabelQty.IsNullOrZero() ? 1 : labelAssoc.ItemLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterItem;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                case LabelType.Piece:
                    localLabels = labelAssoc.PieceLabels.ToList();
                    labelQty = labelAssoc.PieceLabelQty.IsNullOrZero() ? 1 : labelAssoc.PieceLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterItem;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                case LabelType.Pallet:
                    localLabels = labelAssoc.PalletLabels.ToList();
                    labelQty = labelAssoc.PalletLabelQty.IsNullOrZero() ? 1 : labelAssoc.PalletLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterPallet;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                default:
                    localLabels = labelAssoc.ShippingLabels.ToList();
                    labelQty = labelAssoc.ShippingLabelQty.IsNullOrZero() ? 1 : labelAssoc.ShippingLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterShipping;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;
            }

            return localLabelDatas;
        }

        /// <summary>
        /// Gets the current label association.
        /// </summary>
        /// <param name="partnerId">The customer id to search for.</param>
        /// <param name="partnerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <returns>The current label association.</returns>
        private LabelsAssociation GetAssociatedLabel(int? partnerId, int? partnerGroupId, int? productId, int? productGroupId, Process process = null, int? wareHouseId = null)
        {
            this.log.LogInfo(this.GetType(), "GetAssociatedLabel(): processing..");

            //if (process != null)
            //{
            //    return this.labelAssociations.FirstOrDefault(x => x.LabelProcesses.Select(proc => proc.ProcessID).Contains(process.ProcessID));
            //}

            IList<int> hierarchicalProductGroupIds = new List<int>();
            if (!productGroupId.IsNullOrZero())
            {
                // get the product group and it's parent ids (if any)
                hierarchicalProductGroupIds = this.GetParentGroups(productGroupId.ToInt());
            }

            var localLabelAssociations = this.labelAssociations;
            if (wareHouseId.HasValue)
            {
                localLabelAssociations = this.labelAssociations.Where(x => x.WarehouseID == wareHouseId).ToList();
            }

            // check product and partner match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && partnerId == assoc.BPMasterID)
                {
                   return assoc;
                }
            }

            // check product and partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && partnerGroupId == assoc.BPGroupID)
                {
                    return assoc;
                }
            }

            // check product and all partner/partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && (assoc.BPMasterID == -1 || assoc.BPGroupID == -1))
                {
                    return assoc;
                }
            }

            // check product group and partner match
            foreach (var assoc in localLabelAssociations)
            {
                if (partnerId == assoc.BPMasterID && hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if (partnerId == localAssoc.BPMasterID &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // check product group and partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (partnerGroupId == assoc.BPGroupID && hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if (partnerGroupId == localAssoc.BPGroupID &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return assoc;
                            }
                        }
                    }
                }
            }

            // check product group all partner/partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()) && (assoc.BPMasterID == -1 || assoc.BPGroupID == -1))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if ((localAssoc.BPMasterID == -1 || localAssoc.BPGroupID == -1) &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return assoc;
                            }
                        }
                    }
                }
            }

            // get the all products/groups associations
            var nonProductAssociations =
                localLabelAssociations.Where(assoc => assoc.INMasterID < 0 && assoc.INGroupID < 0).ToList();

            // check partner match against all products.
            foreach (var assoc in nonProductAssociations)
            {
                if (partnerId == assoc.BPMasterID)
                {
                    return assoc;
                }
            }

            // check partner group match against all products.
            foreach (var assoc in nonProductAssociations)
            {
                if (partnerGroupId == assoc.BPGroupID)
                {
                   return assoc;
                }
            }

            // return default 
            var defaultLabelAssoc = localLabelAssociations.FirstOrDefault(x => x.BPMasterID < 0 && x.BPGroupID < 0 && x.INMasterID < 0 && x.INGroupID < 0 && x.WarehouseID == wareHouseId);
            return defaultLabelAssoc;
        }

        /// <summary>
        /// Retrieves the product groups.
        /// </summary>
        private void GetProductGroups()
        {
            this.productGroups = this.data.GetInventoryGroups();
        }

        /// <summary>
        /// Gets all the parent ids in the group hierarchy.
        /// </summary>
        /// <param name="groupId">The group id to check.</param>
        /// <returns>Returns all the parent ids in the group hierarchy.</returns>
        private IList<int> GetParentGroups(int groupId)
        {
            int? childsParentId = null;
            var childsGroup = this.productGroups.FirstOrDefault(x => x.INGroupID == groupId);
            if (childsGroup != null)
            {
                childsParentId = childsGroup.ParentInGroupID;
            }

            var hierarchyIds = new List<int> { groupId };

            while (childsParentId != null)
            {
                var parent = this.productGroups.FirstOrDefault(x => x.INGroupID == childsParentId);
                if (parent != null)
                {
                    hierarchyIds.Add(parent.INGroupID);
                    childsParentId = parent.ParentInGroupID;
                }
                else
                {
                    childsParentId = null;
                }
            }

            return hierarchyIds;
        }

        #endregion
    }
}

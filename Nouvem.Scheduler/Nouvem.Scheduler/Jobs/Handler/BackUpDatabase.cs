﻿// -----------------------------------------------------------------------
// <copyright file="BackUpDatabase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Entity;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using Quartz;

    public class BackUpDatabase : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), string.Format("Back up of database starting..Time:{0}", DateTime.Now));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Database.CommandTimeout = 1800;
                    entities.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "exec BackUpDatabase");
                    this.log.LogInfo(this.GetType(), string.Format("Database back up complete..Time:{0}", DateTime.Now));
                    entities.Database.CommandTimeout = 30;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

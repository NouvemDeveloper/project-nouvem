﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Shared;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class ExportCarcassPrices : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var payments = Data.GetCarcassPrices();
                if (payments == null)
                {
                    return;
                }

                var sb = new StringBuilder();
                foreach (var x in payments)
                {
                    var line = string.Format("{0}|{1}|{2}",x.CarcassNumber, x.KillDate, x.Price);
                    sb.AppendLine(line);
                }

                var filename = Path.Combine(ApplicationSettings.ExportCarcassPricesPath, string.Format("CarcassPrices{0}.csv", DateTime.Now.ToString("ddMMyyyyHHmmss")));
                File.WriteAllText(filename, sb.ToString());

                var mailAttachments = new List<string>();
                mailAttachments.Add(filename);
                EmailManager.Instance.SendEmail("Dillons Carcass Prices File", "Please copy attachment into Nouvem import folder",
                    ApplicationSettings.ExportCarcassPricesEmail, mailAttachments);
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

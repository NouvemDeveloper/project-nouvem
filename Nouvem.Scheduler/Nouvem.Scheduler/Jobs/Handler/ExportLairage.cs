﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Shared;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class ExportLairage : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var customerData = new Dictionary<int,string>();
            var customerData1 = ApplicationSettings.CheckForLairageExportCustomer1.Split(',');
            var customerData2 = ApplicationSettings.CheckForLairageExportCustomer2.Split(',');
            var customerData3 = ApplicationSettings.CheckForLairageExportCustomer3.Split(',');
            if (customerData1.Length == 2)
            {
                customerData.Add(customerData1[0].ToInt(),customerData1[1]);
            }

            if (customerData2.Length == 2)
            {
                customerData.Add(customerData2[0].ToInt(), customerData2[1]);
            }

            if (customerData3.Length == 2)
            {
                customerData.Add(customerData3[0].ToInt(), customerData3[1]);
            }

            var intakes = new HashSet<int>();
            var data = Data.GetLairgesForExport().GroupBy(x => x.Customer);
            var sleep = ApplicationSettings.CheckForLairageExportSleep == 0
                ? 2000
                : ApplicationSettings.CheckForLairageExportSleep;
            var attachments = new List<Tuple<string,string>>();

            try
            {
                foreach (var lairages in data)
                {
                    System.Threading.Thread.Sleep(sleep);

                    foreach (var lairage in lairages)
                    {
                        intakes.Add(lairage.APGoodsReceiptID);
                    }
               
                    var sb = new StringBuilder();
                    foreach (var x in lairages)
                    {
                        var line = string.Format(
                            "{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}|{23}" +
                            "|{24}|{25}|{26}|{27}|{28}|{29}|{30}|{31}|{32}|{33}|{34}|{35}|{36}|{37}|{38}|{39}|{40}|{41}|{42}|{43}|{44}|{45}|{46}|{47}" +
                            "|{48}|{49}|{50}|{51}|{52}|{53}|{54}|{55}|{56}|{57}|{58}|{59}|{60}|{61}|{62}|{63}|{64}|{65}|{66}|{67}|{68}|{69}|{70}|{71}" +
                            "|{72}|{73}|{74}|{75}|{76}|{77}|{78}|{79}|{80}|{81}|{82}|{83}|{84}|{85}|{86}|{87}|{88}|{89}|{90}|{91}|{92}|{93}|{94}|{95}" +
                            "|{96}|{97}|{98}|{99}|{100}|{101}|{102}|{103}|{104}|{105}|{106}|{107}|{108}|{109}|{110}|{111}|{112}|{113}|{114}|{115}|{116}|{117}|{118}|{119}" +
                            "|{120}|{121}|{122}|{123}|{124}|{125}|{126}|{127}|{128}|{129}",
                            x.APGoodsReceiptID,
                            x.APOrderID,
                            x.DocumentNumberingID,
                            x.Number,
                            x.BPMasterSnapshotID_Supplier,
                            x.BPMasterSnapshotID_Haulier,
                            x.BPAddressSnapshotID_Invoice,
                            x.BPAddressSnapshotID_Delivery,
                            x.DeliveryDate,
                            x.GoodsReceiptDate,
                            x.DocumentDate,
                            x.CreationDate,
                            x.Printed,
                            x.NouDocStatusID,
                            x.Remarks,
                            x.TotalExVAT,
                            x.VAT,
                            x.SubTotalExVAT,
                            x.DiscountIncVAT,
                            x.DiscountPercentage,
                            x.GrandTotalIncVAT,
                            x.BPMasterID_Haulier,
                            x.BPMasterID_Supplier,
                            x.BPAddressID_Delivery,
                            x.BPAddressID_Invoice,
                            x.UserMasterID_Buyer,
                            x.UserMasterID,
                            x.BPContactID,
                            x.BaseDocumentReferenceID,
                            x.Deleted,
                            x.EditDate,
                            x.NouKillTypeID,
                            x.HerdQAS,
                            x.ProposedKillDate,
                            x.KillDate,
                            x.MoveResponse,
                            x.Reference,
                            x.KillType,
                            x.StockTransactionID,
                            x.MasterTableID,
                            x.BatchNumberID,
                            x.Serial,
                            x.TransactionDate,
                            x.NouTransactionTypeID,
                            x.TransactionQTY,
                            x.TransactionWeight,
                            x.GrossWeight,
                            x.Tare,
                            x.Alibi,
                            x.Pieces,
                            x.WarehouseID,
                            x.LocationID,
                            x.INMasterID,
                            x.ContainerID,
                            x.Consumed,
                            x.ManualWeight,
                            x.DeviceMasterID,
                            x.IsBox,
                            x.StockTransactionID_Container,
                            x.LabelID,
                            x.CanAddToBox,
                            x.InLocation,
                            x.Comments,
                            x.StoredLabelID,
                            x.BPMasterID,
                            x.Price,
                            x.AttributeID,
                            x.BatchID_Base,
                            x.SplitID,
                            x.ProcessID,
                            x.StockTransactionID_Pallet,
                            x.StockTransactionID_StockMove,
                            x.MissingDate,
                            x.Eartag,
                            x.Herd,
                            x.Name,
                            x.DOB,
                            x.Category,
                            x.Sex,
                            x.Customer,
                            x.FarmAssured,
                            x.AgeInMonths,
                            x.Clipped,
                            x.Casualty,
                            x.Lame,
                            x.Cleanliness,
                            x.NoOfMoves,
                            x.CurrentResidency,
                            x.TotalResidency,
                            x.PreviousResidency,
                            x.DateOfLastMove,
                            x.Generic1,
                            x.Generic2,
                            x.Identigen,
                            x.KillNumber,
                            x.CarcassNumber,
                            x.SequencedDate,
                            x.GradingDate,
                            x.Condemned,
                            x.Detained,
                            x.NotInsured,
                            x.TBYes,
                            x.BurstBelly,
                            x.fatColour,
                            x.CarcassSide,
                            x.ColdWeight,
                            x.Grade,
                            x.Side1Weight,
                            x.Side2Weight,
                            x.CountryOfOrigin,
                            x.RearedIn,
                            x.HoldingNumber,
                            x.IntakeProposedKillDate,
                            x.supplierID,
                            x.Attribute220,
                            x.Attribute230,
                            x.Attribute231,
                            x.Attribute232,
                            x.Attribute233,
                            x.Attribute234,
                            x.Attribute235,
                            x.Attribute236,
                            x.Attribute237,
                            x.Attribute238,
                            x.SupplierName,
                            x.Code,
                            x.AddressLine1,
                            x.AddressLine2,
                            x.AddressLine3,
                            x.AddressLine4);

                        sb.AppendLine(line);
                    }

                    try
                    {
                        Data.MarkIntakesAsExported(intakes);
                        var filename = Path.Combine(ApplicationSettings.CheckForLairageExportPath, string.Format("LairageExport{0}.csv", DateTime.Now.ToString("ddMMyyyyHHmmss")));
                        File.WriteAllText(filename, sb.ToString());

                        var customerId = lairages.First().Customer.ToInt();
                        var emailAddresses = customerData[customerId];
                        attachments.Add(Tuple.Create(filename, emailAddresses));
                        
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                        //Data.SendEmail($"(Test) Scheduler Issue: {ApplicationSettings.Customer} Lairge File export ERROR", 
                        //    $"Issue time:{DateTime.Now} : Error:{e.Message}, inner:{e.InnerException}", ApplicationSettings.NouvemEmailAddresses);
                    }
                }


                var allAttachments = attachments.GroupBy(x => x.Item2);
                var mailAttachments = new List<string>();
                foreach (var localAttachments in allAttachments)
                {
                    foreach (var localAttachment in localAttachments)
                    {
                        mailAttachments.Add(localAttachment.Item1);
                    }

                    var emailAddresses = localAttachments.Key;
                    EmailManager.Instance.SendEmail("DMP Foods Lairge File", "Please copy attachments into Nouvem import folder", emailAddresses, mailAttachments);
                    mailAttachments.Clear();
                    System.Threading.Thread.Sleep(sleep);
                }
            }
            catch (Exception e)
            {
                this.log.LogError(this.GetType(), $"{e.Message}: inner:{e.InnerException}");
                //Data.SendEmail($"(Test) Scheduler Issue: {ApplicationSettings.Customer} Lairge File export ERROR",
                //    $"Issue time:{DateTime.Now} : Error:{e.Message}, inner:{e.InnerException}", ApplicationSettings.NouvemEmailAddresses);
            }
        }
    }
}

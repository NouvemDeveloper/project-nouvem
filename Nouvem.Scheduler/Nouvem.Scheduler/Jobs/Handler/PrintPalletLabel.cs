﻿// -----------------------------------------------------------------------
// <copyright file=PrintPalletLabel .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using Neodynamic.SDK.Printing;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Scheduler.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using Quartz;

    public class PrintPalletLabel : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {              
                var stock = Data.GetPalletLabelData();
                if (stock == null || !stock.StockTransactionID_Pallet.HasValue)
                {
                    return;
                }

                var printer = PrintManager.Instance;

                try
                {
                    var labelType = LabelType.Pallet;
                    var labels = printer.ProcessPrinting(
                        stock.Customer,
                        null,
                        stock.ProductID,
                        null,
                        labelType,
                        stock.StockTransactionID_Pallet.ToInt(),
                        1,
                        null,
                        null,
                        null,
                        8,
                        stock.DeviceID);

                    if (labels.Item1.Any())
                    {
                        printer.AddLabelToTransaction(stock.StockTransactionID_Pallet.ToInt(),
                            string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                    }
                }
                catch (Exception e)
                {
                    this.log.LogError(this.GetType(), e.Message);
                } 
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

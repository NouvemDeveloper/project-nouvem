﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Shared;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class ExportDispatches : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var dispatch = Data.GetDispatchStock();
                var doc = new XDocument(new XDeclaration("1.0", "utf-8", null));

                var header =
                    new XElement("carcaseTransaction",
                        new XElement("transactiontype", "POP"),
                        new XElement("abp_plant_name", ""),
                        new XElement("abp_plant_no", ""),
                        new XElement("supplier_name", ApplicationSettings.Customer),
                        new XElement("abp_supplier_code", ""),
                        new XElement("order_no", "")
                    );

                if (dispatch == null || !dispatch.StockDetails.Any())
                {
                    return;
                }

                var product = dispatch.StockDetails.First().Product;
                var iteration = 2;
                if (product.CompareIgnoringCase("Fore") || product.CompareIgnoringCase("Hind"))
                {
                    iteration = 1;
                }

                foreach (var detail in dispatch.StockDetails)
                {
                    for (int i = 0; i < iteration; i++)
                    {
                        header.Add(
                            new XElement("carcase_data",
                                new XElement("kill_date", detail.GradingDate),
                                new XElement("carcase_no", detail.CarcassNumber),
                                new XElement("eur_code", detail.Grade),
                                new XElement("mpqas", detail.FarmAssured.ToBool().ToString()),
                                new XElement("tot_bqas_residency", detail.TotalResidency),
                                new XElement("num_residencies", detail.CurrentResidency),
                                new XElement("cold_weight", detail.ColdWeight),
                                new XElement("breed", detail.BreedName),
                                new XElement("dob_date", detail.DOB),
                                new XElement("dolm_date", detail.DateOfLastMove),
                                new XElement("herd_no", detail.HerdNo),
                                new XElement("barcode", detail.Barcode),
                                new XElement("cat_code", detail.CatName),
                                new XElement("eartag", detail.Eartag),
                                new XElement("Product", detail.Product),
                                new XElement("Reference1", detail.Attribute100),
                                new XElement("Reference2", detail.Attribute101),
                                new XElement("Reference3", detail.Attribute102),
                                new XElement("Reference4", detail.Attribute103),
                                new XElement("Reference5", detail.Attribute104),
                                new XElement("Reference6", detail.Attribute105),
                                new XElement("Reference7", detail.Attribute106),
                                new XElement("Reference8", detail.Attribute107),
                                new XElement("Reference9", detail.Attribute108),
                                new XElement("Reference10", detail.Attribute109)
                            ));
                    }
                }

                doc.Add(header);
                var filename = Path.Combine(ApplicationSettings.DispatchExportFilePath, string.Format("{0}.xml", DateTime.Now.ToString("ddMMyyyyHHmmss")));
                File.WriteAllText(filename, doc.ToString());

                var mailAttachments = new List<string>();
                mailAttachments.Add(filename);
                EmailManager.Instance.SendEmail("DMP Foods Export File", "Please copy attachment into Nouvem import folder", ApplicationSettings.DispatchExportEmailAddress, mailAttachments);
            }
            catch (Exception e)
            {
                this.log.LogError(this.GetType(), $"{e.Message}: inner:{e.InnerException}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class UpdateAveragePriceList : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    Data.UpdateAveragePriceList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

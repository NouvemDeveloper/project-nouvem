﻿using System;
using System.Data.Entity;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class DBJob2 : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Database.CommandTimeout = 1800;
                    entities.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "exec Scheduler_DBJob2");
                    entities.Database.CommandTimeout = 30;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.Reporting.WinForms;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class EmailJob2 : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var manager = ReportManager.Instance;
                var reportPath = string.Empty;
                var jobData = Data.GetEmailReportJob2Data();
                foreach (var data in jobData)
                {
                    var report = manager.Reports.FirstOrDefault(x => x.Name == ApplicationSettings.EmailReportJob2ReportName);
                    if (report != null)
                    {
                        reportPath = report.Path;
                    }

                    try
                    {
                        var reportParam = new List<ReportParameter>();
                        if (data.Param1 != "n/a")
                        {
                            reportParam.Add(new ReportParameter { Name = ApplicationSettings.EmailReportJob2Param1, Values = { data.Param1 } });
                        }

                        if (data.Param2 != "n/a")
                        {
                            reportParam.Add(new ReportParameter { Name = ApplicationSettings.EmailReportJob2Param2, Values = { data.Param2 } });
                        }

                        if (data.Param3 != "n/a")
                        {
                            reportParam.Add(new ReportParameter { Name = ApplicationSettings.EmailReportJob2Param3, Values = { data.Param3 } });
                        }

                        if (data.Param4 != "n/a")
                        {
                            reportParam.Add(new ReportParameter { Name = ApplicationSettings.EmailReportJob2Param4, Values = { data.Param4 } });
                        }

                        if (data.Param5 != "n/a")
                        {
                            reportParam.Add(new ReportParameter { Name = ApplicationSettings.EmailReportJob2Param5, Values = { data.Param5 } });
                        }

                        if (data.Param6 != "n/a")
                        {
                            reportParam.Add(new ReportParameter { Name = ApplicationSettings.EmailReportJob2Param6, Values = { data.Param6 } });
                        }

                        var localReport = new ReportData
                        {
                            Name = ApplicationSettings.EmailReportJob2ReportName,
                            Path = reportPath,
                            ReportParameters = reportParam
                        };

                        this.log.LogInfo(this.GetType(), $"EmailJob2 - sending report {localReport.Name} to {data.EmailAddress}");
                        ReportManager.Instance.GenerateAndEmailReport(localReport, ApplicationSettings.EmailReportJob2Path, data.EmailAddress);
                        System.Threading.Thread.Sleep(2000);
                    }
                    catch (Exception ex)
                    {
                        this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}


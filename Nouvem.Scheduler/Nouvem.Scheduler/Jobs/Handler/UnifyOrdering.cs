﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents.DocumentStructures;
using Newtonsoft.Json.Linq;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class UnifyOrdering : HandlerBase, IJob
    {

        public UnifyOrdering()
        {

        }

        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "Checking for Unify orders");
            //string myJson = "{\n\t\"email\":\"test_api@unifyordering.com\",\n\t\"password\":\"test_api@unifyordering.com\",\n\t\"type\":\"1\"\n}";
            var request = HttpWebRequest.Create(@"https://www.unifyordering.com/admin/api/user/login/");
            request.ContentType = "application/json";
            request.Method = "POST";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    email = "test_api@unifyordering.com",
                    password = "test_api@unifyordering.com",
                    type=1
                });

                streamWriter.Write(json);
            }

            string token = string.Empty;
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var error = $"Cannot connect to Unify. Error fetching data. Server returned status code: {response.StatusCode}";
                        this.log.LogError(this.GetType(), error);
                        EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                        System.Threading.Thread.Sleep(2000);
                        return;
                    }

                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        var content = reader.ReadToEnd();
                        if (string.IsNullOrWhiteSpace(content))
                        {
                            var error = "Cannot connect to Unify. Response contained empty body...";
                            this.log.LogError(this.GetType(), error);
                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                            System.Threading.Thread.Sleep(2000);
                            return;
                        }
                        else
                        {
                            JObject o = JObject.Parse(content);
                            token = "b40a28be00b7438b7669d35d73fca63d"; // (string)o["user"]["token"];
                        }
                    }
                }

                request = HttpWebRequest.Create(@"https://www.unifyordering.com/admin/api/orders/?status=1");
                request.ContentType = "application/json";
                request.Method = "GET";
                request.Headers.Add("X-Auth-Token", token);
                var saleOrderIds = new List<string>();
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var error = $"Error fetching data. Server returned status code: {response.StatusCode}";
                        this.log.LogError(this.GetType(), error);
                        EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                        System.Threading.Thread.Sleep(2000);
                        return;
                    }
                        
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        var content = reader.ReadToEnd();
                        if (string.IsNullOrWhiteSpace(content))
                        {
                            var error = "Response contained empty body...";
                            this.log.LogError(this.GetType(), error);
                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                            System.Threading.Thread.Sleep(2000);
                            return;
                        }
                        else
                        {
                            JObject data = JObject.Parse(content);
                            var orders = data["orders"];
                            foreach (var order in orders)
                            {
                                var localOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderHeader>(order.ToString());
                                if (true)//localOrder.status == "1")
                                {
                                    saleOrderIds.Add(localOrder.id);
                                }
                            }
                        }
                    }
                }
                
                var ordersToImport = new List<RootObject>();
                foreach (var saleOrderId in saleOrderIds)
                {
                    var url = $"https://www.unifyordering.com/admin/api/orders/{saleOrderId}/";
                    request = HttpWebRequest.Create(url);
                    request.ContentType = "application/json";
                    request.Method = "GET";
                    request.Headers.Add("X-Auth-Token", token);
            
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            var error = $"Error fetching data for Unify Order No:{saleOrderId}. Server returned status code: {response.StatusCode}";
                            this.log.LogError(this.GetType(), error);
                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                            System.Threading.Thread.Sleep(2000);
                            continue;
                        }

                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            var content = reader.ReadToEnd();
                            if (string.IsNullOrWhiteSpace(content))
                            {
                                var error = $"Response contained empty body...Unify Order No:{saleOrderId}";
                                this.log.LogError(this.GetType(), error);
                                EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                                System.Threading.Thread.Sleep(2000);
                                return;
                            }
                            else
                            {
                               ordersToImport.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(content));
                            }
                        }
                    }
                }

                this.log.LogInfo(this.GetType(), $"{ordersToImport.Count} orders found");
                var ordersRead = Data.AddOrUpdateUnifyOrders(ordersToImport);
                foreach (var orderRead in ordersRead)
                {
                    var url = $"https://www.unifyordering.com/admin/api/orders/{orderRead}/confirm/";
                    request = HttpWebRequest.Create(url);
                    request.ContentType = "application/json";
                    request.Method = "PUT";
                    request.Headers.Add("X-Auth-Token", token);

                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            var error = $"Error marking order:{orderRead} as confirmed. Server returned status code: {response.StatusCode}";
                            this.log.LogError(this.GetType(), error);
                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                            System.Threading.Thread.Sleep(2000);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }

    public class Organisation
    {
        public string name { get; set; }
        public string image { get; set; }
        public string noimg_colour { get; set; }
    }

    public class OrderActivity
    {
        public string changed { get; set; }
        public string cartid { get; set; }
        public string user { get; set; }
        public string createddate { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string ts { get; set; }
        public string message { get; set; }
        public string date { get; set; }
    }

    public class OrderItem
    {
        public string orderid { get; set; }
        public string order_quantity { get; set; }
        public string size_display { get; set; }
        public string id { get; set; }
        public string code { get; set; }
        public string cat_name { get; set; }
        public object subcat_name { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public string quantity { get; set; }
        public string unit_display { get; set; }
    }

    public class Customer
    {
        public string account_id { get; set; }
        public string name { get; set; }
        public string image { get; set; }
        public string noimg_colour { get; set; }
    }

    public class OrderHeader
    {
        public string id { get; set; }
        public string status { get; set; }
        public string cartid { get; set; }
        public string ts { get; set; }
        public string currency { get; set; }
        public Organisation organisation { get; set; }
        public string createddate_formated { get; set; }
        public string fordate_formated { get; set; }
        public string fordate { get; set; }
        public string status_desktop_name { get; set; }
        public IList<OrderItem> order_items { get; set; }
        public double total { get; set; }
        public int product_count { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public string fordate { get; set; }
        public List<OrderActivity> order_activity { get; set; }
        public List<OrderItem> order_items { get; set; }
        public string s { get; set; }
        public string status_name { get; set; }
        public bool can_cancel { get; set; }
        public Customer customer { get; set; }
    }

    public class RootObject
    {
        public string status { get; set; }
        public Order order { get; set; }
    }

    public class Example
    {
        public string status { get; set; }
        public IList<Order> orders { get; set; }
        public string nav { get; set; }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="BackUpDatabase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using Neodynamic.SDK.Printing;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Scheduler.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using Quartz;

    public class ReprintLabel : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var printer = PrintManager.Instance;
                var stockToReprint = printer.GetStockToReprint();
                if (!stockToReprint.Any())
                {
                    return;
                }

                foreach (var stock in stockToReprint)
                {
                    try
                    {
                        var labelType = stock.IsBox.ToBool() ? LabelType.Box : LabelType.Item;
                        var labels = printer.ProcessPrinting(
                            stock.BPMasterID,
                            null,
                            stock.INMasterID,
                            null,
                            labelType,
                            stock.StockTransactionID,
                            1,
                            null,
                            null,
                            stock.WarehouseID,
                            stock.NouTransactionTypeID);

                        if (labels.Item1.Any())
                        {
                            printer.AddLabelToTransaction(stock.StockTransactionID,
                                string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                        }
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                    }
                }

                printer.ConsumeStockToReprint();
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

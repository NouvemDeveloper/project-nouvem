﻿// -----------------------------------------------------------------------
// <copyright file="AutoSaleOrderProcessor.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Nouvem.Scheduler.Model.BusinessObject;
    using Nouvem.Scheduler.Properties;
    using Nouvem.Shared;
    using Quartz;

    public class AutoSaleOrderProcessor : HandlerBase, IJob
    {
        #region field

        /// <summary>
        /// The prices.
        /// </summary>
        private IList<PriceListDetail> prices = new List<PriceListDetail>();

        /// <summary>
        /// The del date.
        /// </summary>
        private DateTime? deliveryDate;

        /// <summary>
        /// The customer del address.
        /// </summary>
        private string customerDeliveryAddress;
        
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSaleOrderProcessor"/> class.
        /// </summary>
        public AutoSaleOrderProcessor()
        {
            this.GetUserId();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the device id.
        /// </summary>
        public int DeviceId { get; set; }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Gets the user id.
        /// </summary>
        private void GetUserId()
        {
            this.UserId = Data.GetUserId();
            this.DeviceId = Data.GetDeviceId();
        }

        #region import

        /// <summary>
        /// Determine if there are po's ready to import, and dispatches ready to export, letting the user know.
        /// </summary>
        private void ImportOrders()
        {
            var directory = Settings.Default.SharedFilePath;

            if (!Directory.Exists(directory))
            {
                this.log.LogError(this.GetType(),"Invalid shared directory path. Please check that your shared directory path is valid");
                return;
            }

            // process the import po files into sale orders.
            this.ProcessFiles();
        }


        /// <summary>
        /// Parses the import po files, creating sale orders, and automatically adding them to the db.
        /// </summary>
        /// <returns>The new created sale orders, or null if an issue.</returns>
        public void ProcessFiles()
        {
            var saleOrders = new List<ARDispatch>();
            foreach (var file in Directory.EnumerateFiles(Settings.Default.SharedFilePath))
            {
                var identifier = file.Substring(file.Length - 6, 2);

                if (identifier.Equals(Constant.IM) || identifier.Equals(Constant.GR))
                {
                    // file is being, or has been, processed
                    continue;
                }

                var saleOrderItems = new List<DispatchDocketItem>();
                using (var localFile = new StreamReader(file))
                {
                    while (!localFile.EndOfStream)
                    {
                        var line = localFile.ReadLine();

                        if (!string.IsNullOrEmpty(line) && line.StartsWith("T"))
                        {
                            break;
                        }

                        var saleOrderItemData = this.ParseSaleOrderItems(line);
                        var error = saleOrderItemData.Item1;
                        var saleOrderItem = saleOrderItemData.Item2;

                        if (error != string.Empty)
                        {
                            this.log.LogError(this.GetType(), string.Format("ProcessFiles():{0}", error));
                            return;
                        }

                        saleOrderItems.Add(saleOrderItem);
                    }
        
                    var groupedSaleOrderItems = saleOrderItems.GroupBy(x => x.CustomerSaleOrderNumber);

                    foreach (var items in groupedSaleOrderItems)
                    {
                        var dispatchItems = new List<ARDispatchDetail>();
                        var soNumber = items.ElementAt(0).CustomerSaleOrderNumber;
                        var delAddress = items.ElementAt(0).DeliveryAddress;

                        var dbAddressId = Data.GetDeliveryAddress(delAddress);

                        // create the sale order header
                        var number = Data.SetDocumentNumber(1001);
                        var saleOrder = new ARDispatch
                        {
                            DeliveryDate = this.deliveryDate,
                            DocumentDate = DateTime.Today,
                            CreationDate = DateTime.Now,
                            NouDocStatusID = 1,
                            DeviceID = this.DeviceId,
                            BPAddressID_Invoice = 5375,
                            DocumentNumberingID = 1001,
                            Number = number,
                            BPMasterID_Customer = 411,
                            CustomerPOReference = soNumber,
                            BPAddressID_Delivery = dbAddressId
                        };

                        foreach (var item in items)
                        {
                            dispatchItems.Add(new ARDispatchDetail
                            {
                                INMasterID = item.INMasterID,
                                QuantityOrdered = item.Quantity,
                                WeightOrdered = item.Weight,
                                UnitPrice = item.UnitPrice,
                                PriceListID = item.PriceListID
                            });
                        }

                        saleOrder.ARDispatchDetails = dispatchItems;
                        saleOrders.Add(saleOrder);
                    }
                }

                // mark the file as having been imported
                var newFilename = file.Replace(Constant.PO, Constant.IM);

                if (!file.Equals(newFilename))
                {
                    // mark the file as having been processed.
                    File.Move(file, newFilename);
                    this.log.LogInfo(this.GetType(), string.Format("File {0} processed, and marked IM.", newFilename));
                }
                else
                {
                    this.log.LogError(this.GetType(), string.Format("Cannot mark file name as imported (IM), as a file with the same name already exists. File:{0}", newFilename));
                }
            }

            Data.ImportOrders(saleOrders);
        }

        /// <summary>
        /// Parses an individual csv line into a sale order item.
        /// </summary>
        /// <param name="line">The csv line to parse.</param>
        /// <returns>A sale order item.</returns>
        private Tuple<string, DispatchDocketItem> ParseSaleOrderItems(string line)
        {
            this.log.LogInfo(this.GetType(), string.Format("ParseSaleOrderItems():{0}", line));
            var data = line.Split(',');

            this.log.LogInfo(this.GetType(), string.Format("Line Count:{0}", data.Count()));

            for (int i = 0; i < data.Count(); i++)
            {
                this.log.LogInfo(this.GetType(), string.Format("Element Pos:{0}, Element Value:{1}", i, data[i]));
            }

            var externalCustomerCode = data.ElementAt(2).Trim();
            this.customerDeliveryAddress = data.ElementAt(3).Trim();
            var poNumber = data.ElementAt(4).Trim();
            var customerSaleOrderNumber = data.ElementAt(5).Trim();
            this.deliveryDate = data.ElementAt(7).Trim().ToDate();
            var productCode = data.ElementAt(8).Trim();
            var quantity = data.ElementAt(12).Trim().ToNullableInt();
            var weight = data.ElementAt(13).Trim().ToDecimal();
            var notes = data.ElementAt(14).Trim();

            // get the product
            var product = Data.GetProduct(productCode);

            if (product == null)
            {
                var error = string.Format("Product code:{0} does not exist in the system", productCode);
                this.log.LogError(this.GetType(), error);
                return Tuple.Create(error, new DispatchDocketItem());
            }

            var localPrice = this.prices.FirstOrDefault(x => x.INMasterID == product.INMasterID);
            var price = 0m;
            if (localPrice != null)
            {
                price = localPrice.Price;
            }

            // create the sale order item (Note - using batchcode to put the customer so number into..it's cleared down when finished processing)
            var saleOrderItem = new DispatchDocketItem
            {
                INMasterID = product.INMasterID,
                Quantity = quantity,
                Weight = weight,
                UnitPrice = price,
                PriceListID = localPrice.PriceListID,
                CustomerSaleOrderNumber = customerSaleOrderNumber,
                DeliveryAddress = this.customerDeliveryAddress
            };

            return Tuple.Create(string.Empty, saleOrderItem);
        }

        #endregion

        #region export

        /// <summary>
        /// Gets the non exported dispatch orders.
        /// </summary>
        private void ExportDispatchOrders()
        {
            var dispatchDockets = new List<DispatchDocket>(Data.GetDispatchDockets(411));

            if (!dispatchDockets.Any())
            {
                return;
            }

            this.ProcessDispatchDockets(dispatchDockets);
        }

        /// <summary>
        /// Processes the customer dispath dockets.
        /// </summary>
        /// <param name="dockets">The dockets to process.</param>
        /// <returns>A flag indicating a successful process of the non exported dispatch dockets.</returns>
        private bool ProcessDispatchDockets(IList<Model.BusinessObject.DispatchDocket> dockets)
        {
            this.log.LogInfo(this.GetType(), string.Format("ProcessDispatchDockets(): Processing {0} dispatch dockets", dockets.Count));
            try
            {
                var files =
               Directory.EnumerateFiles(Settings.Default.SharedFilePath)
                   .Where(x => x.Substring(x.Length - 6, 2).Equals(Constant.IM)).ToList();

                foreach (var dispatchDocket in dockets)
                {
                    #region logging

                    //this.log.LogInfo(this.GetType(), string.Format("ProcessDispatchDockets(): Processing dispatch dockets:{0}, external ref:{1}, SaleOrderId:{2}, Items:{3}", dispatchDocket.DispatchDocketId, externalRef, dispatchDocket.SaleOrderId, dispatchDocket.GetDispatchItemsCount));

                    //foreach (var item in dispatchDocket.DispatchDocketItems)
                    //{
                    //    this.log.LogInfo(
                    //        this.GetType(),
                    //        string.Format("DispatchItemId:{0}," +
                    //                      "DispatchDocketId:{1}," +
                    //                      "Product Code:{2}," +
                    //                      "External Ref:{3}," +
                    //                      "Price Per Unit:{4}," +
                    //                      "Quantity:{5}," +
                    //                      "Weight:{6}," +
                    //                      "Total Price:{7}",
                    //        item.DispatchDocketItemId,
                    //        item.DispatchDocketId,
                    //        item.ProductCode,
                    //        item.ExternalRef,
                    //        item.PricePerUnit,
                    //        item.Quantity,
                    //        item.Weight,
                    //        item.TotalPrice));
                    //}

                    #endregion

                    // get the docket items
                    var items = dispatchDocket.DispatchDocketItems;
                    var externalRef = dispatchDocket.CustomerSaleOrderId;
                    var number = dispatchDocket.DispatchDocketNumber;

                    foreach (var file in files)
                    {
                        var newFile = new StringBuilder();
                        using (var localFile = new StreamReader(file))
                        {
                            while (!localFile.EndOfStream)
                            {
                                var line = localFile.ReadLine();

                                if (!string.IsNullOrEmpty(line) && line.StartsWith("T"))
                                {
                                    // Terminator line
                                    newFile.Append(line + Environment.NewLine);
                                    break;
                                }

                                if (string.IsNullOrEmpty(line))
                                {
                                    break;
                                }

                                // parse the line
                                var parsedLine = this.ParseDispatchItems(line, externalRef, items, number);
                                newFile.Append(parsedLine + Environment.NewLine);
                            }
                        }

                        // overwrite the file with the appended data.
                        File.WriteAllText(file, newFile.ToString());
                    }
                }

                files.ForEach(file =>
                {
                    var fullyProcessed = true;
                    const int valuesInUnprocessedLine = 15;
                    using (var localFile = new StreamReader(file))
                    {
                        while (!localFile.EndOfStream)
                        {
                            var line = localFile.ReadLine();

                            if (!string.IsNullOrEmpty(line) && line.StartsWith("T"))
                            {
                                break;
                            }

                            /* get the line data count. If it matches the unprocessed line data count then 
                             * this file hasn't been fully processed */
                            if (line != null && line.Split(',').Count() <= valuesInUnprocessedLine)
                            {
                                fullyProcessed = false;
                                break;
                            }
                        }
                    }

                    if (fullyProcessed)
                    {
                        var newFilename = file.Replace(Constant.IM, Constant.GR);
                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                            this.log.LogInfo(this.GetType(), string.Format("File {0} processed, and marked GR.", newFilename));
                        }
                        else
                        {
                            this.log.LogError(this.GetType(), string.Format("Cannot mark file name as processed (GR), as a file with the same name already exists. File:{0}", newFilename));
                        }
                    }
                });

                // mark the dockets as exported in the db.
                return Data.MarkAsExported(dockets);
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Parses the file line, attempting to match it against the 
        /// corresponding dispatch item, appending the dispatch item details if matched.
        /// </summary>
        /// <param name="line">The file line to parse.</param>
        /// <param name="externalRef">The external dispatch docket customer reference.</param>
        /// <param name="items">The docket items.</param>
        private string ParseDispatchItems(string line, string externalRef, IList<DispatchDocketItem> items, int number)
        {
            var data = line.Split(',');
            var externalCustomerRef = data.ElementAt(5).Trim();
            var productCode = data.ElementAt(8).Trim();

            this.log.LogInfo(this.GetType(),
                string.Format("ParseDispatchItems(): " +
                              "line:{0}, " +
                              "externalRef:{1}, " +
                              "externalCustomerRef:{2}, " +
                              "Product code:{3}",
                              line,
                              externalRef,
                              externalCustomerRef,
                              productCode));

            if (!externalCustomerRef.Equals(externalRef))
            {
                this.log.LogInfo(this.GetType(), "Returning line. External refs don't match");
                return line;
            }

            var matches = items.Where(x => x.ProductCode.Trim().Equals(productCode)).ToList();
            this.log.LogInfo(this.GetType(), string.Format("{0} dispatch items for external ref:{1}, product code:{2}", matches.Count, externalRef, productCode));

            if (matches.Any())
            {
                var sharedData = matches.ElementAt(0);
                var unitPrice = Math.Round(sharedData.UnitPrice.ToDecimal(), 2).ToString();

                var docketNo = number.ToString().PadLeft(10, ' ');
                decimal? localQuantity = 0;
                decimal? localWeight = 0;
                decimal? localTotal = 0M;

                foreach (var dispatchDocketItem in matches)
                {
                    localQuantity += dispatchDocketItem.Quantity;
                    localWeight += dispatchDocketItem.Weight;
                    localTotal += dispatchDocketItem.TotalPrice;
                }

                var quantity = localQuantity.ToString();
                var weight = Math.Round(localWeight.ToDecimal(), 3).ToString();
                var totalPrice = Math.Round(localTotal.ToDecimal(), 2).ToString();

                var foundItemLine = string.Format("{0},{1},{2},{3},{4},{5}", line, quantity, weight, unitPrice, totalPrice, docketNo);
                this.log.LogInfo(this.GetType(), string.Format("Item found. {0}", foundItemLine));

                return foundItemLine;
            }

            this.log.LogInfo(this.GetType(), "Item not found");

            // default line, in case the item wasn't available.
            return string.Format("{0},{1},{2},{3},{4},{5}", line, "0", "0", "0", "0", "0");
        }

        #endregion

        #endregion

        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), string.Format("Shared file path:{0}", Settings.Default.SharedFilePath));
            this.prices = Data.GetPrices();

            try
            {
                this.ImportOrders();
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Import Error:{0}", ex.Message));
            }

            try
            {
                this.ExportDispatchOrders();
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Export Error:{0}", ex.Message));
            }
        }
    }
}

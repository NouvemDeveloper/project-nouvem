﻿// -----------------------------------------------------------------------
// <copyright file="BackUpDatabase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Xml.Linq;
using Neodynamic.SDK.Printing;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Jobs.Trigger;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Scheduler.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using Quartz;

    public class IntakeImport : HandlerBase, IJob
    {
        private IList<ImportAttributeLookup> lookups;

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                this.lookups = Data.GetAttributeLookups();
                if (SchedulerGlobal.Categories == null)
                {
                    SchedulerGlobal.Categories = Data.GetCategories();
                }

                if (SchedulerGlobal.Breeds == null)
                {
                    SchedulerGlobal.Breeds = Data.GetBreeds();
                }

                var files = Directory.EnumerateFiles(ApplicationSettings.CheckForIntakeImportPath).Where(x => x.EndsWithIgnoringCase("xml"));

                #region new
                foreach (var file in files)
                {
                    try
                    {
                        var doc = XDocument.Load(file);
                        var supplier = doc.Root.Element("supplier_name").Value;
                        var plantCode = doc.Root.Element("abp_supplier_code").Value;
                        var poNumber = doc.Root.Element("order_no").Value;

                        var jobDescendants = doc.Descendants("carcase_data");
                        if (!jobDescendants.Any())
                        {
                            var newFilename = string.Format("{0}_Ignored_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                            if (!file.Equals(newFilename))
                            {
                                // mark the file as having been processed.
                                File.Move(file, newFilename);
                            }

                            continue;
                        }

                        var detailsLocal = new List<StockDetail>();
                        foreach (var itemJob in jobDescendants)
                        {
                            var localBarcode = itemJob.FindInElement("barcode");
                            var product = itemJob.FindInElement("Product");
                            var productID = 0;
                            if (product.CompareIgnoringCase("Hind"))
                            {
                                productID = ApplicationSettings.DispatchExportHindID;
                            }
                            else if (product.CompareIgnoringCase("Fore"))
                            {
                                productID = ApplicationSettings.DispatchExportForeID;
                            }

                            var stock = new StockDetail
                            {
                                StockTransaction = new StockTransaction
                                {
                                    TransactionDate = DateTime.Now,
                                    NouTransactionTypeID = 2,
                                    TransactionQTY = 1,
                                    TransactionWeight = itemJob.FindInElement("cold_weight").ToDecimal(),
                                    Pieces = 1,
                                    WarehouseID = ApplicationSettings.CheckForIntakeImportWarehouse,
                                    INMasterID = productID == 0 ? this.GetKillProductByCategory(itemJob.FindInElement("cat_code")) : productID,
                                    Comments = localBarcode
                                },

                                Attribute = new Scheduler.Attribute()
                            };

                            this.SetAttribute(stock.Attribute, "carcase_no", itemJob.FindInElement("carcase_no"));
                            this.SetAttribute(stock.Attribute, "eur_code", itemJob.FindInElement("eur_code"));
                            this.SetAttribute(stock.Attribute, "mpqas", itemJob.FindInElement("mpqas").ToBool().BoolToYesNo());
                            this.SetAttribute(stock.Attribute, "tot_bqas_residency", itemJob.FindInElement("tot_bqas_residency"));
                            this.SetAttribute(stock.Attribute, "num_residencies", itemJob.FindInElement("num_residencies"));
                            this.SetAttribute(stock.Attribute, "cold_weight", itemJob.FindInElement("cold_weight"));
                            this.SetAttribute(stock.Attribute, "herd_no", itemJob.FindInElement("herd_no"));
                            this.SetAttribute(stock.Attribute, "eartag", itemJob.FindInElement("eartag"));
                            this.SetAttribute(stock.Attribute, "lot_no", itemJob.FindInElement("lot_no"));
                            this.SetAttribute(stock.Attribute, "carcase_no", itemJob.FindInElement("carcase_no"));
                            this.SetAttribute(stock.Attribute, "Reference1", itemJob.FindInElement("Reference1"));
                            this.SetAttribute(stock.Attribute, "Reference2", itemJob.FindInElement("Reference2"));
                            this.SetAttribute(stock.Attribute, "Reference3", itemJob.FindInElement("Reference3"));
                            this.SetAttribute(stock.Attribute, "Reference4", itemJob.FindInElement("Reference4"));
                            this.SetAttribute(stock.Attribute, "Reference5", itemJob.FindInElement("Reference5"));
                            this.SetAttribute(stock.Attribute, "Reference6", itemJob.FindInElement("Reference6"));
                            this.SetAttribute(stock.Attribute, "Reference7", itemJob.FindInElement("Reference7"));
                            this.SetAttribute(stock.Attribute, "Reference8", itemJob.FindInElement("Reference8"));
                            this.SetAttribute(stock.Attribute, "Reference9", itemJob.FindInElement("Reference9"));
                            this.SetAttribute(stock.Attribute, "Reference10", itemJob.FindInElement("Reference10"));

                            if (localBarcode.IsNumeric() && localBarcode.Length < 18)
                            {
                                stock.StockTransaction.Serial = localBarcode.ToInt();
                            }

                            var localKill = itemJob.FindInElement("kill_date");
                            if (localKill != string.Empty && localKill.Length >= 8)
                            {
                                var convert = localKill.Replace("-", "").Substring(0, 8).Trim().ToDate();
                                if (convert > DateTime.Today.AddYears(-100))
                                {

                                    this.SetAttribute(stock.Attribute, "kill_date", convert.ToShortDateString());
                                }
                            }

                            var localDOB = itemJob.FindInElement("dob_date");
                            if (localDOB != string.Empty && localDOB.Length >= 8)
                            {
                                var convert = localDOB.Replace("-", "").Substring(0, 8).Trim().ToDate();
                                if (convert > DateTime.Today.AddYears(-100))
                                {
                                    this.SetAttribute(stock.Attribute, "dob_date", convert.ToShortDateString());
                                }
                            }

                            var localDOLM = itemJob.FindInElement("dolm_date");
                            if (localDOLM != string.Empty && localDOLM.Length >= 8)
                            {
                                var convert = localDOLM.Replace("-", "").Substring(0, 8).Trim().ToDate();
                                if (convert > DateTime.Today.AddYears(-100))
                                {
                                    this.SetAttribute(stock.Attribute, "dolm_date", convert.ToShortDateString());
                                }
                            }

                            var localCategory = SchedulerGlobal.Categories.FirstOrDefault(x =>
                                x.CAT.CompareIgnoringCase(itemJob.FindInElement("cat_code")));
                            if (localCategory != null)
                            {
                                this.SetAttribute(stock.Attribute, "cat_code", localCategory.CAT);
                            }

                            var localBreed = itemJob.FindInElement("breed");
                            if (localBreed != null)
                            {
                                this.SetAttribute(stock.Attribute, "breed", localBreed);
                            }

                            detailsLocal.Add(stock);
                        }

                        if (Data.AddIntakeTransactions(detailsLocal))
                        {
                            var newFilename = string.Format("{0}_Imported_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));

                            if (!file.Equals(newFilename))
                            {
                                // mark the file as having been processed.
                                File.Move(file, newFilename);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                        var newFilename = string.Format("{0}_Error_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                }

                #endregion

                #region old
                //foreach (var file in files)
                //{
                //    try
                //    {
                //        var doc = XDocument.Load(file);
                //        var supplier = doc.Root.Element("supplier_name").Value;
                //        var plantCode = doc.Root.Element("abp_supplier_code").Value;
                //        var poNumber = doc.Root.Element("order_no").Value;

                //        var jobDescendants = doc.Descendants("carcase_data");
                //        if (!jobDescendants.Any())
                //        {
                //            var newFilename = string.Format("{0}_Ignored_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                //            if (!file.Equals(newFilename))
                //            {
                //                // mark the file as having been processed.
                //                File.Move(file, newFilename);
                //            }

                //            continue;
                //        }

                //        var detailsLocal = new List<StockDetail>();
                //        foreach (var itemJob in jobDescendants)
                //        {
                //            var localBarcode = itemJob.FindInElement("barcode");
                //            var stock = new StockDetail
                //            {
                //                StockTransaction = new StockTransaction
                //                {
                //                    TransactionDate = DateTime.Now,
                //                    NouTransactionTypeID = 2,
                //                    TransactionQTY = 1,
                //                    TransactionWeight = itemJob.FindInElement("cold_weight").ToDecimal(),
                //                    Pieces = 1,
                //                    WarehouseID = ApplicationSettings.CheckForIntakeImportWarehouse,
                //                    INMasterID = this.GetKillProductByCategory(itemJob.FindInElement("cat_code")),
                //                    Comments = localBarcode
                //                },

                //                Attribute = new Scheduler.Attribute
                //                {
                //                    Attribute10 = itemJob.FindInElement("carcase_no"),
                //                    Attribute11 = itemJob.FindInElement("eur_code"),
                //                    Attribute8 = itemJob.FindInElement("mpqas").ToBool().BoolToYesNo(),
                //                    Attribute12 = itemJob.FindInElement("tot_bqas_residency"),
                //                    Attribute13 = itemJob.FindInElement("num_residencies"),
                //                    Attribute14 = itemJob.FindInElement("cold_weight"),
                //                    Attribute15 = itemJob.FindInElement("herd_no"),
                //                    Attribute16 = itemJob.FindInElement("eartag"),
                //                    Attribute24 = itemJob.FindInElement("lot_no"),
                //                    Attribute17 = supplier,
                //                    Attribute18 = poNumber,
                //                    Attribute4 = plantCode
                //                }
                //            };

                //            if (localBarcode.IsNumeric())
                //            {
                //                stock.StockTransaction.Serial = localBarcode.ToInt();
                //            }

                //            var localKill = itemJob.FindInElement("kill_date");
                //            if (localKill != string.Empty && localKill.Length >= 8)
                //            {
                //                var convert = localKill.Replace("-", "").Substring(0, 8).Trim().ToDate();
                //                if (convert > DateTime.Today.AddYears(-100))
                //                {
                //                    stock.Attribute.Attribute7 = convert.ToShortDateString();
                //                }
                //            }

                //            var localDOB = itemJob.FindInElement("dob_date");
                //            if (localDOB != string.Empty && localDOB.Length >= 8)
                //            {
                //                var convert = localDOB.Replace("-", "").Substring(0, 8).Trim().ToDate();
                //                if (convert > DateTime.Today.AddYears(-100))
                //                {
                //                    stock.Attribute.Attribute19 = convert.ToShortDateString();
                //                }
                //            }

                //            var localDOLM = itemJob.FindInElement("dolm_date");
                //            if (localDOLM != string.Empty && localDOLM.Length >= 8)
                //            {
                //                var convert = localDOLM.Replace("-", "").Substring(0, 8).Trim().ToDate();
                //                if (convert > DateTime.Today.AddYears(-100))
                //                {
                //                    stock.Attribute.Attribute20 = convert.ToShortDateString();
                //                }
                //            }

                //            var localCategory = SchedulerGlobal.Categories.FirstOrDefault(x =>
                //                x.CAT.CompareIgnoringCase(itemJob.FindInElement("cat_code")));
                //            if (localCategory != null)
                //            {
                //                stock.Attribute.Attribute21 = localCategory.CAT;
                //            }

                //            var localBreed = itemJob.FindInElement("breed");
                //            if (localBreed != null)
                //            {
                //                stock.Attribute.Attribute22 = localBreed;
                //            }

                //            stock.Attribute.Attribute5 = ApplicationSettings.CheckForIntakeImportCountryOfOrigin;
                //            stock.Attribute.Attribute3 = ApplicationSettings.CheckForIntakeImportCountryOfOrigin;

                //            detailsLocal.Add(stock);
                //        }

                //        if (Data.AddIntakeTransactions(detailsLocal))
                //        {
                //            var newFilename = string.Format("{0}_Imported_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));

                //            if (!file.Equals(newFilename))
                //            {
                //                // mark the file as having been processed.
                //                File.Move(file, newFilename);
                //            }
                //        }
                //    }
                //    catch (Exception e)
                //    {
                //        this.log.LogError(this.GetType(), e.Message);
                //        var newFilename = string.Format("{0}_Error_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                //        if (!file.Equals(newFilename))
                //        {
                //            // mark the file as having been processed.
                //            File.Move(file, newFilename);
                //        }
                //    }
                //}

                #endregion
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        private void SetAttribute(Nouvem.Scheduler.Attribute attribute, string lookup, string value)
        {
            var lookUpAttribute = this.lookups.FirstOrDefault(x => x.Lookup.CompareIgnoringCase(lookup));
            if (lookUpAttribute != null)
            {
                attribute.SetAttributeValue(lookUpAttribute.Attribute,value);
            }
        }

        ///// <summary>
        ///// Locates the input attributeat runtime, and assigns it the attribute value.
        ///// </summary>
        ///// <param name="name">The attribute to look for.</param>
        ///// <param name="attributeValue">The value to assign.</param>
        //public void SetAttributeValue(string name, string attributeValue)
        //{
        //    try
        //    {
        //        var typeSource = this.GetType();

        //        // Get all the properties of source object type
        //         var propertyInfo = typeSource.GetProperties(System.Reflection.BindingFlags.Public |
        //            System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

        //        foreach (var property in propertyInfo)
        //        {
        //            var fieldName = property.Name;
        //            var match = name.Equals(fieldName);

        //            if (match)
        //            {
        //                property.SetValue(this, attributeValue);
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //}

        /// <summary>
        /// Gets the associated category product.
        /// </summary>
        /// <param name="category">The category to check.</param>
        /// <returns>The associated category product</returns>
        private int GetKillProductByCategory(string category)
        {
            if (ApplicationSettings.CheckForIntakeImportProduct > 0)
            {
                 return ApplicationSettings.CheckForIntakeImportProduct;
            }

            if (category.CompareIgnoringCase("A"))
            {
                return ApplicationSettings.ProductIdYoungBull;
            }

            if (category.CompareIgnoringCase("B"))
            {
                return ApplicationSettings.ProductIdBull;
            }

            if (category.CompareIgnoringCase("C"))
            {
                return ApplicationSettings.ProductIdSteer;
            }

            if (category.CompareIgnoringCase("D"))
            {
                return ApplicationSettings.ProductIdCow;
            }

            if (category.CompareIgnoringCase("V"))
            {
                return ApplicationSettings.ProductIdVealYoung;
            }

            if (category.CompareIgnoringCase("Z"))
            {
                return ApplicationSettings.ProductIdVealOld;
            }

            return ApplicationSettings.ProductIdHeifer;
        }
    }
}


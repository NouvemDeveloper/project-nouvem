﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Shared;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class LairageImport : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var files = Directory.EnumerateFiles(ApplicationSettings.CheckForIntakeImportPath).Where(x => x.EndsWithIgnoringCase("anl"));
                foreach (var file in files)
                {
                    var details = new List<StockDetail>();
                    try
                    {
                        var intake = new StockDetail();
                        var count = 1;
                         
                        foreach (string line in File.ReadLines(file, Encoding.UTF8))
                        {
                            if (line.StartsWithIgnoringCase("EOF"))
                            {
                                details.Add(intake);
                                break;
                            }

                            if (count == 5)
                            {
                                details.Add(intake);
                                intake = new StockDetail();
                                count = 1;
                            }

                            if (count == 1)
                            {
                                intake.Attribute.Eartag = line.Substring(1, 20).Trim();
                                intake.Attribute.Attribute1 = line.Substring(31, 5).Trim();
                                //if (string.IsNullOrWhiteSpace(intake.Attribute.Attribute1))
                                //{
                                //    // no aphais number so ignore file.
                                //    break;
                                //}

                                intake.Attribute.DOB = line.Substring(36, 8).Trim().ToDate();
                                intake.Attribute.Attribute2 = line.Substring(44, 3).Trim(); // breed
                                intake.Attribute.Sex = line.Substring(50, 1);
                                intake.Attribute.Attribute3 = line.Substring(51, 1); // fa
                                intake.Attribute.CountryOfOrigin = line.Substring(61, 20).Trim();
                                intake.Attribute.NoOfMoves = line.Substring(81, 3).ToInt();
                                intake.Attribute.DateOfLastMove = line.Substring(84, 8).ToDate();
                                intake.Attribute.Attribute229 = line.Substring(111, 8);
                                intake.Attribute.Attribute249 = line.Substring(103, 2);
                            }
                            else if (count == 2)
                            {
                                intake.Attribute.Herd = line.Substring(0, 10).Trim();
                                intake.Attribute.Attribute230 = line.Substring(10, 10).Trim();
                                intake.Attribute.Attribute231 = line.Substring(20, 10).Trim();
                                intake.Attribute.Attribute232 = line.Substring(30, 10).Trim();
                                intake.Attribute.Attribute233 = line.Substring(40, 10).Trim();
                                intake.Attribute.Attribute234 = line.Substring(50, 10).Trim();
                                intake.Attribute.Attribute235 = line.Substring(60, 10).Trim();
                                intake.Attribute.Attribute236 = line.Substring(70, 10).Trim();
                                intake.Attribute.Attribute237 = line.Substring(80, 10).Trim();
                            }
                            else if (count == 3)
                            {
                                intake.Attribute.Attribute4 = line.Substring(0, 25).Trim();
                                intake.Attribute.Attribute5 = line.Substring(25, 25).Trim();
                                intake.Attribute.Attribute6 = line.Substring(50, 128).Trim();
                                intake.Attribute.Attribute7 = line.Substring(178, 10).Trim();
                            }
                            else
                            {
                                // ignore line 4
                            }

                            count++;
                        }

                        details = details.Where(x => !string.IsNullOrWhiteSpace(x.Attribute.Attribute1)).ToList();

                        if (details.Count == 0)
                        {
                            var newFilename = string.Format("{0}_Ignored_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));

                            if (!file.Equals(newFilename))
                            {
                                // mark the file as having been processed.
                                File.Move(file, newFilename);
                            }
                        }
                        else if (Data.AddLairageTransactions(details))
                        {
                            var newFilename = string.Format("{0}_Imported_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));

                            if (!file.Equals(newFilename))
                            {
                                // mark the file as having been processed.
                                File.Move(file, newFilename);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                        var newFilename = string.Format("{0}_Error_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }

                        //Data.SendEmail($"(Test) Scheduler Issue: {ApplicationSettings.Customer} AFAIS Lairge File import ERROR",
                        //    $"Issue time:{DateTime.Now} : Error:{e.Message}, inner:{e.InnerException}", ApplicationSettings.NouvemEmailAddresses);
                    }
                }
            }
            catch (Exception e)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", e.Message, e.InnerException));
                //Data.SendEmail($"(Test) Scheduler Issue: {ApplicationSettings.Customer} AFAIS Lairge File import ERROR",
                //    $"Issue time:{DateTime.Now} : Error:{e.Message}, inner:{e.InnerException}", ApplicationSettings.NouvemEmailAddresses);
            }
        }
    }
}

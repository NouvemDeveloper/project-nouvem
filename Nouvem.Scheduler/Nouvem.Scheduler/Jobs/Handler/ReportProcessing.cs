﻿// -----------------------------------------------------------------------
// <copyright file="ReportProcessing.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Scheduler.Properties;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Scheduler.BusinessLogic;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Model.BusinessObject;
    using Quartz;

    public class ReportProcessing : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var dispatchReportsToPrint = new List<int>();

            var reportPath = string.Empty;
            var manager = ReportManager.Instance;

            if (ApplicationSettings.IntakeDetailsReportConnect)
            {
                var invoiceDocket = manager.Reports.FirstOrDefault(x => x.Name == ReportName.IntakeDetails);
                if (invoiceDocket != null)
                {
                    reportPath = invoiceDocket.Path;
                }

                try
                {
                    var reportsToPrint = Data.CheckForUnprintedIntakeDockets();
                    foreach (var intakeId in reportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "orderID", Values = { intakeId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.IntakeReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.IntakeDetails, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.IntakeReportPrinter1, numOfCopies: ApplicationSettings.IntakeReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.IntakeReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.IntakeDetails, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.IntakeReportPrinter2, numOfCopies: ApplicationSettings.IntakeReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.IntakeReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.IntakeDetails, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.IntakeReportPrinter3, numOfCopies: ApplicationSettings.IntakeReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.IntakeReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.IntakeDetails, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.IntakeReportPrinter4, numOfCopies: ApplicationSettings.IntakeReportPrinterCopies);
                        }
                    }

                    Data.UpdateIntakeDockets(reportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }
      
            if (ApplicationSettings.InvoiceReportConnect)
            {
                var invoiceDocket = manager.Reports.FirstOrDefault(x => x.Name == ApplicationSettings.InvoiceName);
                if (invoiceDocket != null)
                {
                    reportPath = invoiceDocket.Path;
                }

                try
                {
                    var reportsToPrint = Data.CheckForUnprintedInvoiceDockets();
                    foreach (var invoiceId in reportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { invoiceId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.InvoiceName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter1, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.InvoiceName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter2, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.InvoiceName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter3, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.InvoiceReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.InvoiceName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.InvoiceReportPrinter4, numOfCopies:ApplicationSettings.InvoiceReportPrinterCopies);
                        }
                    }

                    Data.UpdateInvoiceDockets(reportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }

            if (ApplicationSettings.DispatchReportConnect)
            {
                var dispatchDocket = manager.Reports.FirstOrDefault(x => x.Name == ReportName.BloorsDispatchDocket);
                if (dispatchDocket != null)
                {
                    reportPath = dispatchDocket.Path;
                }

                try
                {
                    dispatchReportsToPrint = Data.CheckForUnprintedDispatchDockets();
                    foreach (var ardispatchId in dispatchReportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { ardispatchId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter1, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter2, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter3, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.DispatchDocket, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchReportPrinter4, numOfCopies: ApplicationSettings.DispatchReportPrinterCopies);
                        }
                    }

                    Data.UpdateDispatchDockets(dispatchReportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }

            if (ApplicationSettings.DispatchSummaryReportConnect)
            {
                var dispatchSummaryDocket = manager.Reports.FirstOrDefault(x => x.Name == ReportName.BloorsDispatchSummary);
                if (dispatchSummaryDocket != null)
                {
                    reportPath = dispatchSummaryDocket.Path;
                }

                try
                {
                    if (!dispatchReportsToPrint.Any())
                    {
                        dispatchReportsToPrint = Data.CheckForUnprintedDispatchDockets();
                    }
           
                    foreach (var ardispatchId in dispatchReportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Dispatchid", Values = { ardispatchId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter1, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter2, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter3, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.DispatchSummaryReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ReportName.BloorsDispatchSummary, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.DispatchSummaryReportPrinter4, numOfCopies: ApplicationSettings.DispatchSummaryReportPrinterCopies);
                        }
                    }

                    Data.UpdateDispatchDockets(dispatchReportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }

            if (ApplicationSettings.SaleOrderReportConnect)
            {
                var saleOrder = manager.Reports.FirstOrDefault(x => x.Name == ApplicationSettings.SaleOrderName);
                if (saleOrder != null)
                {
                    reportPath = saleOrder.Path;
                }

                try
                {
                    var reportsToPrint = Data.CheckForUnprintedSaleOrderDockets();
                    foreach (var saleOrderId in reportsToPrint)
                    {
                        var reportParam = new List<ReportParameter> { new ReportParameter { Name = ApplicationSettings.SaleOrderParameter, Values = { saleOrderId.ToString() } } };

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter1))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.SaleOrderName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter1, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter2))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.SaleOrderName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter2, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter3))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.SaleOrderName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter3, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }

                        if (!string.IsNullOrWhiteSpace(ApplicationSettings.SaleOrderReportPrinter4))
                        {
                            ReportManager.Instance.PrintReport(new ReportData { Name = ApplicationSettings.SaleOrderName, Path = reportPath, ReportParameters = reportParam }, ApplicationSettings.SaleOrderReportPrinter4, numOfCopies: ApplicationSettings.SaleOrderReportPrinterCopies);
                        }
                    }

                    Data.UpdateSaleOrderDockets(reportsToPrint);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            }
        }
    }
}
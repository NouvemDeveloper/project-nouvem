﻿// -----------------------------------------------------------------------
// <copyright file="DataRepository.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using Microsoft.Office.Interop.Outlook;
using Nouvem.AccountsIntegration.BusinessObject;
using Nouvem.Email;
using Nouvem.Logging;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Jobs.Handler;
using Nouvem.Scheduler.Model.BusinessObject;
using BusinessPartner = Nouvem.Scheduler.Model.BusinessObject.BusinessPartner;

namespace Nouvem.Scheduler.Model.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Properties;
    using Nouvem.Shared;

    public class DataRepository
    {
        private ILogger log = new Logger();

        #region broadcast

        /// <summary>
        /// Checks the dispatch dockets, updating them if they dont tally.
        /// </summary>
        /// <returns>A flag, indicating a business partner change or not.</returns>
        public bool CheckBusinessPartners(DateTime lastCheckTime)
        {
            this.log.LogInfo(this.GetType(), "CheckBusinessPartners. Checking for partner changes");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BPMasters.Any(x => x.EditDate != null && x.EditDate >= lastCheckTime);
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        #region auto sale order processor

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        public int GetDeliveryAddress(string address)
        {
           var localAddress = address.ToLower().Trim();
           try
            {
                using (var entities = new NouvemEntities())
                {
                    // update
                    var dbAddress =
                        entities.BPAddresses.FirstOrDefault(x => x.AddressLine1.ToLower().Trim().Equals(localAddress));

                    if (dbAddress == null)
                    {
                        var add = new BPAddress
                        {
                            BPMasterID = 411,
                            AddressLine1 = address,
                            AddressLine2 = string.Empty,
                            AddressLine3 = string.Empty,
                            AddressLine4 = string.Empty,
                            Shipping = true,
                            PostCode = string.Empty,
                            Deleted = false
                        };

                        entities.BPAddresses.Add(add);
                        entities.SaveChanges();
                        return add.BPAddressID;
                    }

                    return dbAddress.BPAddressID;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        public int SetDocumentNumber(int docNumberId)
        {
            this.log.LogInfo(this.GetType(), string.Format("SetDocumentNumber(): Updating doc number ID:{0}", docNumberId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // update
                    var dbNumber =
                        entities.DocumentNumberings.FirstOrDefault(
                            number => number.DocumentNumberingID == docNumberId);

                    if (dbNumber != null)
                    {
                        var currentNo = dbNumber.NextNumber;
                        var nextNo = dbNumber.NextNumber + 1;
                        if (nextNo > dbNumber.LastNumber)
                        {
                            // Last number reached, so reset back to first number.
                            nextNo = dbNumber.FirstNumber;
                        }

                        dbNumber.NextNumber = nextNo;
                        entities.SaveChanges();
                        this.log.LogInfo(this.GetType(), "Document number updated");

                        return dbNumber.NextNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return 0;
            }

            return 0;
        }

        /// <summary>
        /// Gets the product associated with the product code.
        /// </summary>
        /// <param name="code">The code to serach for.</param>
        /// <returns>An associated product.</returns>
        public bool ImportOrders(IList<ARDispatch> orders)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var order in orders)
                    {
                        var details = order.ARDispatchDetails;
                        order.ARDispatchDetails = null;
                        entities.ARDispatches.Add(order);
                        entities.SaveChanges();

                        foreach (var arDispatchDetail in details)
                        {
                            // Add to the master snapshot table first
                            var snapshot = new INMasterSnapshot
                            {
                                INMasterID = arDispatchDetail.INMasterID,
                                Code = string.Empty,
                                Name = string.Empty
                            };

                            entities.INMasterSnapshots.Add(snapshot);

                            arDispatchDetail.INMasterSnapshotID = snapshot.INMasterSnapshotID;
                            arDispatchDetail.ARDispatchID = order.ARDispatchID;
                            entities.ARDispatchDetails.Add(arDispatchDetail);
                            entities.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the product associated with the product code.
        /// </summary>
        /// <param name="code">The code to serach for.</param>
        /// <returns>An associated product.</returns>
        public IList<PriceListDetail> GetPrices()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var prices = new List<PriceListDetail>();
                    var ndPrices = entities.PriceListDetails.Where(x => x.PriceListID == 5254 && !x.Deleted).ToList();
                    var basePrices = entities.PriceListDetails.Where(x => x.PriceListID == 5086 && !x.Deleted).ToList();

                    var ndProductIds = ndPrices.Select(x => x.INMasterID).ToList();
                    foreach (var price in basePrices)
                    {
                        if (!ndProductIds.Contains(price.INMasterID))
                        {
                            prices.Add(price);
                        }
                    }

                    return prices.Concat(ndPrices).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns></returns>
        public IList<INMaster> GetProducts()
        {
            var products = new List<INMaster>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.INMasters.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return products;
        }

        /// <summary>
        /// Gets the product associated with the product code.
        /// </summary>
        /// <param name="code">The code to serach for.</param>
        /// <returns>An associated product.</returns>
        public INMaster GetProduct(string code)
        {
            this.log.LogInfo(this.GetType(), string.Format("Attempting to get the product for product code:{0}", code));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var product = entities.INMasters.FirstOrDefault(x => x.Code.Trim().Equals(code.Trim()));

                    if (product != null)
                    {
                        this.log.LogInfo(this.GetType(), string.Format("Product Id:{0} found", product.INMasterID));
                        return product;
                    }

                    this.log.LogError(this.GetType(), "Product not found");
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Marks the processed dispatch dockets as exported.
        /// </summary>
        /// <param name="dockets">The dockets to mark as exported.</param>
        /// <returns>A flag, indicating a successful group export.</returns>
        public bool MarkAsExported(IList<DispatchDocket> dockets)
        {
            this.log.LogInfo(this.GetType(), string.Format("Attempting to mark {0} dispatch dockets as exported", dockets.Count));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var dispatchDocket in dockets)
                    {
                        var dbDocket =
                            entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == dispatchDocket.DispatchDocketId);

                        if (dbDocket != null)
                        {
                            dbDocket.Exported = true;
                            this.log.LogInfo(this.GetType(), string.Format("Dispatch docket id:{0} found and marked as exported", dbDocket.ARDispatchID));
                        }
                        else
                        {
                            this.log.LogError(this.GetType(), string.Format("Dispatch docket id:{0} could not be found", dispatchDocket.DispatchDocketId));
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non exported dispatch dockets for the selected customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of non exported dispatch dockets for the selected customer.</returns>
        public IList<DispatchDocket> GetDispatchDockets(int customerId)
        {
            //this.log.LogInfo(this.GetType(), string.Format("Attempting to retrieve the non exported dispatch dockets for customer id:{0}", customerId));
            var dispatchDockets = new List<DispatchDocket>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    dispatchDockets = (from docket in entities.ARDispatches
                                       where docket.BPMasterID_Customer == customerId
                                       && (docket.Exported == null || docket.Exported == false)
                                       && docket.Deleted == null
                                       && docket.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID
                                       select new DispatchDocket
                                       {
                                           DispatchDocketId = docket.ARDispatchID,
                                           SaleOrderId = docket.BaseDocumentReferenceID,
                                           DispatchDocketDate = docket.DeliveryDate,
                                           CustomerSaleOrderId = docket.CustomerPOReference,
                                           DispatchDocketNumber = docket.Number,
                                           DispatchDocketItems = (from item in docket.ARDispatchDetails.Where(x => x.Deleted == null)
                                                                  select new DispatchDocketItem
                                                                  {
                                                                      DispatchDocketItemId = item.ARDispatchDetailID,
                                                                      DispatchDocketId = item.ARDispatchID,
                                                                      Quantity = item.QuantityDelivered,
                                                                      Weight = item.WeightDelivered,
                                                                      UnitPrice = item.UnitPrice,
                                                                      TotalPrice = item.TotalExclVAT,
                                                                      //ExternalRef = item.SaleOrderItem != null ? item.SaleOrderItem.ExternalRef : string.Empty,
                                                                      ProductCode = item.INMaster.Code
                                                                  }).ToList()
                                       }).ToList();

                    //this.log.LogInfo(this.GetType(), string.Format("{0} non exported dispatch dockets retrieved", dispatchDockets.Count));
                }
            }
            catch (Exception ex)
            {
                //this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dispatchDockets;
        }

        /// <summary>
        /// Retrieves a default user id.
        /// </summary>
        /// <returns>A default user id.</returns>
        public int GetUserId()
        {
            //this.log.LogInfo(this.GetType(), "Attempting to get a user id");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var user = entities.UserMasters.FirstOrDefault();

                    if (user != null)
                    {
                        //this.log.LogInfo(this.GetType(), string.Format("User id :{0} returned", user.ID));
                        return user.UserMasterID;
                    }
                }
            }
            catch (Exception ex)
            {
                //this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            //this.log.LogError(this.GetType(), "No users found");
            return 0;
        }

        /// <summary>
        /// Retrieves a default user id.
        /// </summary>
        /// <returns>A default user id.</returns>
        public int GetDeviceId()
        {
            //this.log.LogInfo(this.GetType(), "Attempting to get a user id");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var device = entities.DeviceMasters.FirstOrDefault();

                    if (device != null)
                    {
                        //this.log.LogInfo(this.GetType(), string.Format("User id :{0} returned", user.ID));
                        return device.DeviceID;
                    }
                }
            }
            catch (Exception ex)
            {
                //this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            //this.log.LogError(this.GetType(), "No users found");
            return 0;
        }

        #endregion

        #region data integration

        ///// <summary>
        ///// Returns the group matching the name.
        ///// </summary>
        ///// <param name="name">The name to serach for.</param>
        ///// <returns>The group matching the name.</returns>
        //public IList<App_GetSage200Partners_Result> GetPartners()
        //{

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            return entities.App_GetSage200Partners().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return null;
        //}

        /// <summary>
        /// Returns the group matching the name.
        /// </summary>
        /// <param name="name">The name to serach for.</param>
        /// <returns>The group matching the name.</returns>
        public IList<BPGroup> GetPartnerGroups()
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BPGroups.ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the group matching the name.
        /// </summary>
        /// <param name="name">The name to serach for.</param>
        /// <returns>The group matching the name.</returns>
        public INGroup GetGroup(string name)
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.INGroups.FirstOrDefault(x => !x.Deleted && x.Name == name);
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds a new collection of products to the db.
        /// </summary>
        /// <param name="products">The products to add.</param>
        public void AddOrUpdateProducts(IList<INMaster> products)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var inMaster in products)
                    {
                        try
                        {
                            var dbProduct = entities.INMasters.FirstOrDefault(x => x.Code == inMaster.Code);
                            if (dbProduct != null)
                            {
                                if (ApplicationSettings.UpdateProducts && dbProduct.Name.Trim() != inMaster.Name.Trim())
                                {
                                    dbProduct.Name = inMaster.Name.Trim();
                                    dbProduct.EditDate = DateTime.Now;
                                    this.log.LogInfo(this.GetType(), string.Format("{0} product edited", inMaster.Code));
                                }
                            }
                            else
                            {
                                entities.INMasters.Add(inMaster);
                                entities.SaveChanges();
                                this.log.LogInfo(this.GetType(), string.Format("{0} product added", inMaster.Code));
                            }
                        }
                        catch (Exception ex)
                        {
                            this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}, Product:{2}", ex.Message, ex.InnerException, inMaster.Code));
                        }
                    }
     
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Adds a new collection of partners to the db.
        /// </summary>
        /// <param name="partners">The partners to add.</param>
        public void AddOrUpdatePartners(IList<BusinessPartner> partners)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var partner in partners)
                    {
                        try
                        {
                            var dbPartner =
                           entities.BPMasters.FirstOrDefault(
                               x => x.Code.Equals(partner.BpMaster.Code) && x.Deleted == null);
                            if (dbPartner != null)
                            {
                                if (dbPartner.OnHold != partner.BpMaster.OnHold
                                    || dbPartner.CreditLimit != partner.BpMaster.CreditLimit
                                    || dbPartner.Balance != partner.BpMaster.Balance
                                    || (!string.IsNullOrEmpty(partner.BpMaster.Name) && dbPartner.Name != partner.BpMaster.Name))
                                {
                                    if (!string.IsNullOrEmpty(partner.BpMaster.Name))
                                    {
                                        dbPartner.Name = partner.BpMaster.Name;
                                    }

                                    dbPartner.OnHold = partner.BpMaster.OnHold;
                                    //dbPartner.InActiveFrom = partner.BpMaster.InActiveFrom;
                                    //dbPartner.InActiveTo = partner.BpMaster.InActiveTo;
                                    dbPartner.CreditLimit = partner.BpMaster.CreditLimit;
                                    dbPartner.Balance = partner.BpMaster.Balance;
                                    dbPartner.EditDate = DateTime.Now;
                                    entities.SaveChanges();
                                    this.log.LogInfo(this.GetType(), string.Format("{0} partner edited", dbPartner.Code));
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(partner.PriceListName))
                                {
                                    var dbPriceList = entities.PriceLists.FirstOrDefault(x =>
                                        x.CurrentPriceListName == partner.PriceListName);
                                    if (dbPriceList != null)
                                    {
                                        partner.BpMaster.PriceListID = dbPriceList.PriceListID;
                                    }
                                }

                                if (partner.BpMaster.PriceListID.IsNullOrZero())
                                {
                                    entities.PriceLists.Add(partner.PriceList);
                                    entities.SaveChanges();
                                    partner.BpMaster.PriceListID = partner.PriceList.PriceListID;
                                }
                               
                                this.log.LogInfo(this.GetType(), string.Format("{0} partner added", partner.BpMaster.Code));

                                entities.BPMasters.Add(partner.BpMaster);
                                entities.SaveChanges();

                                foreach (var address in partner.Addresses)
                                {
                                    address.BPMasterID = partner.BpMaster.BPMasterID;
                                    entities.BPAddresses.Add(address);
                                    entities.SaveChanges();
                                }

                                if (partner.Contact != null)
                                {
                                    partner.Contact.BPMasterID = partner.BpMaster.BPMasterID;
                                    entities.BPContacts.Add(partner.Contact);
                                    entities.SaveChanges();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}, Partner:{2}", ex.Message, ex.InnerException, partner.BpMaster.Code));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Adds a new collection of partners to the db.
        /// </summary>
        /// <param name="partners">The partners to add.</param>
        public void AddOrUpdatePrices(IList<Nouvem.AccountsIntegration.BusinessObject.Prices> prices)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var price in prices)
                    {
                        try
                        {
                            entities.App_UpdatePrices(price.Price, price.Code, price.PriceBand);
                        }
                        catch (Exception ex)
                        {
                            this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}, Partner:{2}", ex.Message, ex.InnerException, price.Code));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        #endregion

        #region stock

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <param name="labelIds">The label ids.</param>
        /// <param name="storedLabel">The label images.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public IList<PalletStock> GetStockToReprint()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.PalletStocks.Where(x => x.Consumed == null && x.NouTransactionTypeID != null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Consumes reprinted stock.
        /// </summary>
        public void ConsumeStockToReprint()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_ConsumeReprintedStock();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        #endregion

        #region label/print

        /// <summary>
        /// Checks the dispatch dockets, updating them if they dont tally.
        /// </summary>
        /// <returns></returns>
        public string CheckDispatchDockets()
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.CheckDispatchDockets().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                //this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedDispatchDockets()
        {
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.ARDispatches
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var arDispatch in unprintedDockets)
                    {
                        unprintedDocketIds.Add(arDispatch.ARDispatchID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedInvoiceDockets()
        {
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.ARInvoices
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var invoice in unprintedDockets)
                    {
                        unprintedDocketIds.Add(invoice.ARInvoiceID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedIntakeDockets()
        {
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.APGoodsReceipts
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var order in unprintedDockets)
                    {
                        unprintedDocketIds.Add(order.APGoodsReceiptID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedSaleOrderDockets()
        {
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.AROrders
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var order in unprintedDockets)
                    {
                        unprintedDocketIds.Add(order.AROrderID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateInvoiceDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.ARInvoices
                    .Where(x => docketIds.Contains(x.ARInvoiceID));

                foreach (var invoice in unprintedDockets)
                {
                    invoice.Printed = true;
                }

                entities.SaveChanges();
            }
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateIntakeDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.APGoodsReceipts
                    .Where(x => docketIds.Contains(x.APGoodsReceiptID));

                foreach (var invoice in unprintedDockets)
                {
                    invoice.Printed = true;
                }

                entities.SaveChanges();
            }
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateSaleOrderDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.AROrders
                    .Where(x => docketIds.Contains(x.AROrderID));

                foreach (var docket in unprintedDockets)
                {
                    docket.Printed = true;
                }

                entities.SaveChanges();
            }
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateDispatchDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.ARDispatches
                    .Where(x => docketIds.Contains(x.ARDispatchID));

                foreach (var docket in unprintedDockets)
                {
                    docket.Printed = true;
                }

                entities.SaveChanges();
            }
        }


        /// <summary>
        /// Retrieves the label associations.
        /// </summary>
        /// <returns>A collection of label associations.</returns>
        public IList<LabelsAssociation> GetLabelAssociations()
        {
            this.log.LogInfo(this.GetType(), "GetLabelAssociations(): Attempting to retrieve the label associations");
            var labAssoc = new List<LabelsAssociation>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var labelAssociations = entities.LabelAssociations.Where(x => x.Deleted == null);

                    foreach (var assoc in labelAssociations)
                    {
                        var localAssoc = new LabelsAssociation
                        {
                            LabelAssociationID = assoc.LabelAssociationID,
                            UsingCustomerGroup = assoc.UsingCustomerGroup,
                            UsingProductGroup = assoc.UsingProductGroup,
                            PieceLabelQty = assoc.PieceLabelQty ?? 1,
                            ItemLabelQty = assoc.ItemLabelQty ?? 1,
                            BoxLabelQty = assoc.BoxLabelQty ?? 1,
                            WarehouseID = assoc.WarehouseID,
                            PalletLabelQty = assoc.PalletLabelQty ?? 1,
                            ShippingLabelQty = assoc.ShippingLabelQty ?? 1,
                            PrinterIDItem = assoc.PrinterID_Item,
                            PrinterIDBox = assoc.PrinterID_Box,
                            PrinterIDPallet = assoc.PrinterID_Pallet,
                            PrinterIDShipping = assoc.PrinterID_Shipping,
                            PrinterItem = assoc.PrinterItem,
                            PrinterBox = assoc.PrinterBox,
                            PrinterPallet = assoc.PrinterPallet,
                            PrinterShipping = assoc.PrinterShipping,
                            BPMaster = assoc.BPMaster,
                            INMaster = assoc.INMaster,
                            BPMasterID = assoc.BPMasterID != null ? assoc.BPMasterID : assoc.UsingCustomerGroup != true ? -1 : -2,
                            IsCustomerEnabled = assoc.BPMasterID != null || assoc.UsingCustomerGroup != true,
                            BPGroupID = assoc.BPGroupID != null ? assoc.BPGroupID : assoc.UsingCustomerGroup == true ? -1 : -2,
                            IsCustomerGroupEnabled = assoc.BPGroupID != null || assoc.UsingCustomerGroup == true,
                            INMasterID = assoc.INMasterID != null ? assoc.INMasterID : assoc.UsingProductGroup != true ? -1 : -2,
                            IsProductEnabled = assoc.INMasterID != null || assoc.UsingProductGroup != true,
                            INGroupID = assoc.INGroupID != null ? assoc.INGroupID : assoc.UsingProductGroup == true ? -1 : -2,
                            IsProductGroupEnabled = assoc.INGroupID != null || assoc.UsingProductGroup == true,
                            LabelProcesses = entities.LabelProcesses.Where(x => x.Deleted == null && x.LabelAssociationID == assoc.LabelAssociationID).ToList()
                        };

                        foreach (var process in localAssoc.LabelProcesses)
                        {
                            localAssoc.LabelProcessesForCombo.Add(process.ProcessID.ToInt());
                        }

                        if (assoc.LabelAssociationLabels != null)
                        {
                            var boxLabels = assoc.LabelAssociationLabels.Where(x => x.BoxLabel == true && x.Deleted == null);
                            foreach (var localLabel in boxLabels)
                            {
                                localAssoc.BoxLabels.Add(localLabel.Label);
                                localAssoc.BoxLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var pieceLabels = assoc.LabelAssociationLabels.Where(x => x.PieceLabel == true && x.Deleted == null);
                            foreach (var localLabel in pieceLabels)
                            {
                                localAssoc.PieceLabels.Add(localLabel.Label);
                                localAssoc.PieceLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var itemLabels = assoc.LabelAssociationLabels.Where(x => x.ItemLabel == true && x.Deleted == null);
                            foreach (var localLabel in itemLabels)
                            {
                                localAssoc.ItemLabels.Add(localLabel.Label);
                                localAssoc.ItemLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var palletLabels = assoc.LabelAssociationLabels.Where(x => x.PalletLabel == true && x.Deleted == null);
                            foreach (var localLabel in palletLabels)
                            {
                                localAssoc.PalletLabels.Add(localLabel.Label);
                                localAssoc.PalletLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var shippingLabels = assoc.LabelAssociationLabels.Where(x => x.ShippingLabel == true && x.Deleted == null);
                            foreach (var localLabel in shippingLabels)
                            {
                                localAssoc.ShippingLabels.Add(localLabel.Label);
                                localAssoc.ShippingLabelsForCombo.Add(localLabel.Label.LabelID);
                            }
                        }

                        labAssoc.Add(localAssoc);
                    }

                    this.log.LogInfo(this.GetType(), string.Format("{0} label associations retrieved", labAssoc.Count));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labAssoc;
        }

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<Label> GetLabels()
        {
            this.log.LogInfo(this.GetType(), "GetLabels(): Attempting to retrieve all the labels");
            var labels = new List<Label>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    labels = entities.Labels.Where(x => !x.Deleted).ToList();
                    this.log.LogInfo(this.GetType(), string.Format("{0} labels retrieved", labels.Count()));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labels;
        }

        /// <summary>
        /// Gets a label corresponding to the input name.
        /// </summary>
        /// <param name="name">The name pof the label.</param>
        /// <returns>A matching label.</returns>
        public Label GetLabel(string name)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.Labels.FirstOrDefault(x => x.Name.Equals(name));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the pallet label transaction to print (if any).
        /// </summary>
        /// <returns>A matching label.</returns>
        public int? GetPalletLabel()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetPalletStock().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the pallet label transaction to print (if any).
        /// </summary>
        /// <returns>A matching label.</returns>
        public App_GetPalletStockData_Result1 GetPalletLabelData()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetPalletStockData().FirstOrDefault();                                
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <param name="labelIds">The label ids.</param>
        /// <param name="storedLabel">The label images.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public bool AddLabelToTransaction(int transId, string labelIds, StoredLabel storedLabel)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == transId);
                    if (trans != null)
                    {
                        entities.StoredLabels.Add(storedLabel);
                        trans.LabelID = labelIds;
                        trans.StoredLabelID = storedLabel.StoredLabelID;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetIntakeLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelIntake", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the out of production data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetOutOfProductionLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelOutOfProd", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the dispatch data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetDispatchLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelDispatch", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the dispatch data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelPallet", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        #endregion

        #region products

        /// <summary>
        /// Method which returns the non deleted inventory groups.
        /// </summary>
        /// <returns>A collection of non deleted inventory groups.</returns>
        public IList<INGroup> GetInventoryGroups()
        {
            this.log.LogInfo(this.GetType(), "GetInventoryGroups(): Attempting to retrieve all the inventory groups");
            var groups = new List<INGroup>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    groups = entities.INGroups.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.log.LogInfo(this.GetType(), string.Format("GetBPProperties(): {0} inventory groups properties successfully retrieved.", groups.Count));
            return groups;
        }

        #endregion

        #region device/settings

        /// <summary>
        /// Gets the local device.
        /// </summary>
        /// <returns>The local device.</returns>
        public DeviceMaster GetDevice()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localDevice =
                        entities.DeviceMasters.FirstOrDefault(x => x.DeviceName.Equals(Constant.SchedulerPC));

                    return localDevice;
                }
            }
            catch (Exception ex)
            {
                //this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the local device settings.
        /// </summary>
        /// <returns>The local device settings.</returns>
        public IList<DeviceSetting> GetDeviceSettings()
        {
            this.log.LogInfo(this.GetType(), "GetDeviceSettings(). Retrieving device settings");
            var deviceSettings = new List<DeviceSetting>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localDevice =
                        entities.DeviceMasters.FirstOrDefault(x => x.DeviceName.Equals(Constant.SchedulerPC));

                    if (localDevice != null)
                    {
                        deviceSettings =
                            entities.DeviceSettings.Where(
                                x => x.DeviceMasterID == localDevice.DeviceID && x.Deleted == null).ToList();
                        this.log.LogInfo(this.GetType(), "GetDeviceSettings(). Device settings retrieved");
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return deviceSettings;
        }

        /// <summary>
        /// Gets the local device settings.
        /// </summary>
        /// <returns>The local device settings.</returns>
        public IList<EmailReportJob> GetEmailReportJobs()
        {
            var jobs = new List<EmailReportJob>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    jobs = entities.EmailReportJobs.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return jobs;
        }

        /// <summary>
        /// Gets the email job 1 data.
        /// </summary>
        /// <returns>The email job 1 data.</returns>
        public IList<Scheduler_EmailReportJob1_Result> GetEmailReportJob1Data()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.Scheduler_EmailReportJob1().ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the email job 1 data.
        /// </summary>
        /// <returns>The email job 1 data.</returns>
        public IList<Scheduler_EmailReportJob2_Result> GetEmailReportJob2Data()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.Scheduler_EmailReportJob2().ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Updates the current device settings.
        /// </summary>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDeviceSettings(IList<DeviceSetting> settings)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var setting in settings)
                    {
                        var dbSettings =
                                entities.DeviceSettings.FirstOrDefault(
                                    x => x.Name.Trim() == setting.Name.Trim() && x.DeviceMasterID == SchedulerGlobal.Device.DeviceID);

                        if (dbSettings == null)
                        {
                            entities.DeviceSettings.Add(setting);
                        }
                        else
                        {
                            dbSettings.Name = setting.Name;
                            dbSettings.Value1 = setting.Value1;
                            dbSettings.Value2 = setting.Value2;
                            dbSettings.Value3 = setting.Value3;
                            dbSettings.Value4 = setting.Value4;
                            dbSettings.Value5 = setting.Value5;
                            dbSettings.Value6 = setting.Value6;
                            dbSettings.Value7 = setting.Value7;
                            dbSettings.Value8 = setting.Value8;
                            dbSettings.Deleted = setting.Deleted;
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }

            return true;
        }

        #endregion

        #region pricing

        /// <summary>
        /// Retrieve all the special prices.
        /// </summary>
        /// <returns>A collection of special prices.</returns>
        public bool ApplySpecialPrices()
        {
            this.log.LogInfo(this.GetType(), "ApplySpecialPrices(): Attempting to retrieve all the special prices");
            var specialPrices = new List<SpecialPrice>();
            var today = DateTime.Today;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var specialPriceList = entities.SpecialPriceLists
                        .Where(x => x.Deleted == null && DbFunctions.TruncateTime(x.StartDate) == today && x.EndDate >= today).ToList();
                    specialPrices = (from price in specialPriceList
                                     select new SpecialPrice
                                     {
                                         SpecialPriceID = price.SpecialPriceListID,
                                         BPGroupID = price.BPGroupID,
                                         BPMasterID = price.BPMasterID,
                                         INMasterID = price.INMasterID,
                                         Price = price.Price,
                                         StartDate = price.StartDate,
                                         EndDate = price.EndDate
                                     }).ToList();

                    this.log.LogInfo(this.GetType(), string.Format("{0} special prices retrieved", specialPrices.Count()));

                    if (!specialPrices.Any())
                    {
                        this.log.LogInfo(this.GetType(), "No special prices for today found.");
                        return true;
                    }

                    var statuses = this.GetNouDocStatuses();
                    var statusActive = statuses.First(x => x.Value.Equals("Active"));
                    var statusComplete = statuses.First(x => x.Value.Equals("Complete"));
                    var statusCancelled = statuses.First(x => x.Value.Equals("Cancelled"));

                    var partnerIds = specialPrices.Select(x => x.BPMasterID).ToList();

                    // get the unexported invoices
                    var invoices = entities.ARInvoices
                           .Where(x => x.NouDocStatusID == statusActive.NouDocStatusID
                                  || x.NouDocStatusID == statusComplete.NouDocStatusID && partnerIds.Contains(x.BPMasterID_Customer) && x.Deleted == null
                                       ).ToList();

                    foreach (var arInvoice in invoices)
                    {
                        var details = arInvoice.ARInvoiceDetails.Where(x => x.Deleted == null).ToList();
                        var updating = false;
                        foreach (var arInvoiceDetail in details)
                        {
                            var priceList =
                                specialPriceList.FirstOrDefault(
                                    x =>
                                        x.INMasterID == arInvoiceDetail.INMasterID &&
                                        x.BPMasterID == arInvoice.BPMasterID_Customer);

                            if (priceList != null)
                            {
                                updating = true;
                                arInvoiceDetail.UnitPrice = priceList.Price;
                                arInvoiceDetail.UnitPriceAfterDiscount = priceList.Price;

                                var priceMethod =
                                    entities.PriceListDetails.FirstOrDefault(
                                        x =>
                                            x.PriceListID == arInvoiceDetail.PriceListID &&
                                            x.INMasterID == arInvoiceDetail.INMasterID && !x.Deleted);

                                if (priceMethod != null && priceMethod.NouPriceMethod != null)
                                {
                                    if (priceMethod.NouPriceMethod.Name.Equals(Constant.PriceByWgt))
                                    {
                                        var total =
                                            decimal.Round(
                                                arInvoiceDetail.WeightDelivered.ToDecimal() *
                                                arInvoiceDetail.UnitPrice.ToDecimal(), 2);
                                        arInvoiceDetail.TotalExclVAT = total;
                                        arInvoiceDetail.TotalIncVAT = total;
                                    }
                                    else if (priceMethod.NouPriceMethod.Name.Equals(Constant.PriceByQty))
                                    {
                                        var total =
                                            decimal.Round(
                                                arInvoiceDetail.QuantityDelivered.ToDecimal() *
                                                arInvoiceDetail.UnitPrice.ToDecimal(), 2);
                                        arInvoiceDetail.TotalExclVAT = total;
                                        arInvoiceDetail.TotalIncVAT = total;
                                    }
                                    else if (arInvoiceDetail.INMaster != null)
                                    {
                                        // typical weight.
                                        var total =
                                            decimal.Round(
                                                (arInvoiceDetail.QuantityDelivered.ToDecimal() *
                                                 arInvoiceDetail.INMaster.NominalWeight.ToDecimal())
                                                * arInvoiceDetail.UnitPrice.ToDecimal(), 2);
                                        arInvoiceDetail.TotalExclVAT = total;
                                        arInvoiceDetail.TotalIncVAT = total;
                                    }
                                }
                            }
                        }

                        if (updating)
                        {
                            var orderDetailsTotal = details.Sum(x => x.TotalExclVAT).ToDecimal();
                            arInvoice.SubTotalExVAT = orderDetailsTotal;
                            arInvoice.GrandTotalIncVAT = orderDetailsTotal;
                            entities.SaveChanges();
                        }
                    }

                    var dispatches = entities.ARDispatches
                           .Where(x => partnerIds.Contains(x.BPMasterID_Customer) && x.Deleted == null
                               && x.NouDocStatusID != statusCancelled.NouDocStatusID && x.NouDocStatusID != statusComplete.NouDocStatusID
                                       ).ToList();

                    foreach (var arDispatch in dispatches)
                    {
                        var details = arDispatch.ARDispatchDetails.Where(x => x.Deleted == null).ToList();
                        var updating = false;
                        foreach (var arDispatchDetail in details)
                        {
                            var priceList =
                                specialPriceList.FirstOrDefault(
                                    x =>
                                        x.INMasterID == arDispatchDetail.INMasterID &&
                                        x.BPMasterID == arDispatch.BPMasterID_Customer);

                            if (priceList != null)
                            {
                                updating = true;
                                arDispatchDetail.UnitPrice = priceList.Price;
                                arDispatchDetail.UnitPriceAfterDiscount = priceList.Price;

                                var priceMethod =
                                    entities.PriceListDetails.FirstOrDefault(
                                        x =>
                                            x.PriceListID == arDispatchDetail.PriceListID &&
                                            x.INMasterID == arDispatchDetail.INMasterID && !x.Deleted);

                                if (priceMethod != null && priceMethod.NouPriceMethod != null)
                                {
                                    if (priceMethod.NouPriceMethod.Name.Equals(Constant.PriceByWgt))
                                    {
                                        var total =
                                            decimal.Round(
                                                arDispatchDetail.WeightDelivered.ToDecimal() *
                                                arDispatchDetail.UnitPrice.ToDecimal(), 2);
                                        arDispatchDetail.TotalExclVAT = total;
                                        arDispatchDetail.TotalIncVAT = total;
                                    }
                                    else if (priceMethod.NouPriceMethod.Name.Equals(Constant.PriceByQty))
                                    {
                                        var total =
                                            decimal.Round(
                                                arDispatchDetail.QuantityDelivered.ToDecimal() *
                                                arDispatchDetail.UnitPrice.ToDecimal(), 2);
                                        arDispatchDetail.TotalExclVAT = total;
                                        arDispatchDetail.TotalIncVAT = total;
                                    }
                                    else if (arDispatchDetail.INMaster != null)
                                    {
                                        // typical weight.
                                        var total =
                                            decimal.Round(
                                                (arDispatchDetail.QuantityDelivered.ToDecimal() *
                                                 arDispatchDetail.INMaster.NominalWeight.ToDecimal())
                                                * arDispatchDetail.UnitPrice.ToDecimal(), 2);
                                        arDispatchDetail.TotalExclVAT = total;
                                        arDispatchDetail.TotalIncVAT = total;
                                    }
                                }
                            }
                        }

                        if (updating)
                        {
                            var orderDetailsTotal = details.Sum(x => x.TotalExclVAT).ToDecimal();
                            arDispatch.SubTotalExVAT = orderDetailsTotal;
                            arDispatch.GrandTotalIncVAT = orderDetailsTotal;
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }

        /// <summary>
        /// Handles the price change to a future list.
        /// </summary>
        /// <param name="partners">The affected partners.</param>
        public void HandlePriceChanges(IList<BPMaster> partners)
        {
            var docketsInvoiced = new List<int>();
            var saleOrdersOnDockets = new List<int>();
            var priceMethods = this.GetPriceMethods();
            var priceByQty = priceMethods.First(x => x.Name == Constant.PriceByQty);
            var priceByWgt = priceMethods.First(x => x.Name == Constant.PriceByWgt);
            var priceListDetails = this.GetPriceListDetails();
            var statuses = this.GetNouDocStatuses();
            var statusActive = statuses.First(x => x.Value.Equals("Active"));
            var statusComplete = statuses.First(x => x.Value.Equals("Complete"));
            var statusCancelled = statuses.First(x => x.Value.Equals("Cancelled"));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var partner in partners)
                    {
                        #region invoice

                        var newPriceListId = partner.PriceListID;
                        var newPriceListDetails =
                            priceListDetails.Where(x => x.PriceListID == newPriceListId).ToList();
                        var partnerId = partner.BPMasterID;
                        var invoices = entities.ARInvoices
                            .Where(x => x.NouDocStatusID == statusActive.NouDocStatusID
                                   || x.NouDocStatusID == statusComplete.NouDocStatusID
                                        && x.BPMasterID_Customer == partnerId).ToList();

                        foreach (var invoice in invoices)
                        {
                            var docketChange = false;
                            var invoiceDetails = invoice.ARInvoiceDetails.Where(x => x.Deleted == null).ToList();
                            foreach (var invoiceDetail in invoiceDetails)
                            {
                                var currentpriceId = invoiceDetail.PriceListID;
                                var currentPriceDetail = priceListDetails
                                    .FirstOrDefault(x => x.PriceListID == currentpriceId && x.INMasterID == invoiceDetail.INMasterID);
                                if (currentPriceDetail != null)
                                {
                                    var currentPrice = currentPriceDetail.Price;
                                    var newPriceDetail =
                                        newPriceListDetails.FirstOrDefault(x => x.INMasterID == invoiceDetail.INMasterID);
                                    if (newPriceDetail != null)
                                    {
                                        if (newPriceDetail.Price != currentPrice)
                                        {
                                            docketChange = true;
                                            invoiceDetail.PriceListID = newPriceListId;
                                            var priceMethodID = newPriceDetail.NouPriceMethodID;
                                            invoiceDetail.UnitPrice = newPriceDetail.Price;
                                            invoiceDetail.UnitPriceAfterDiscount = newPriceDetail.Price;
                                            if (priceMethodID == priceByQty.NouPriceMethodID)
                                            {
                                                invoiceDetail.TotalExclVAT =
                                                    invoiceDetail.QuantityDelivered.ToDecimal() * newPriceDetail.Price;
                                                invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //if (invoiceDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //}
                                            }
                                            else if (priceMethodID == priceByWgt.NouPriceMethodID)
                                            {
                                                invoiceDetail.TotalExclVAT = invoiceDetail.WeightDelivered.ToDecimal() * newPriceDetail.Price;
                                                invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //if (invoiceDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //}
                                            }
                                            else
                                            {
                                                var typicalWgt = invoiceDetail.INMaster.NominalWeight;
                                                var qtyDelivered = invoiceDetail.QuantityDelivered;
                                                invoiceDetail.TotalExclVAT = (typicalWgt.ToDecimal() *
                                                                             qtyDelivered.ToDecimal()) *
                                                                             newPriceDetail.Price;
                                                invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;

                                                //if (invoiceDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }

                            if (docketChange)
                            {
                                entities.SaveChanges();

                                var totalExVat = invoiceDetails.Sum(x => x.TotalExclVAT);
                                var totalIncVat = invoiceDetails.Sum(x => x.TotalIncVAT);

                                invoice.SubTotalExVAT = totalExVat;
                                invoice.GrandTotalIncVAT = totalIncVat;
                                entities.SaveChanges();

                                docketsInvoiced.Add(invoice.BaseDocumentReferenceID.ToInt());
                            }
                        }

                        #endregion

                        #region dispatch

                        var dockets = entities.ARDispatches
                            .Where(x => ((x.NouDocStatusID != statusComplete.NouDocStatusID
                                   && x.NouDocStatusID != statusCancelled.NouDocStatusID
                                   && x.Deleted == null) || docketsInvoiced.Contains(x.ARDispatchID))
                                        && x.BPMasterID_Customer == partnerId).ToList();

                        foreach (var docket in dockets)
                        {
                            var docketChange = false;
                            var docketDetails = docket.ARDispatchDetails.Where(x => x.Deleted == null).ToList();
                            foreach (var docketDetail in docketDetails)
                            {
                                var currentpriceId = docketDetail.PriceListID;
                                var currentPriceDetail = priceListDetails
                                    .FirstOrDefault(x => x.PriceListID == currentpriceId && x.INMasterID == docketDetail.INMasterID);
                                if (currentPriceDetail != null)
                                {
                                    var currentPrice = currentPriceDetail.Price;
                                    var newPriceDetail =
                                        newPriceListDetails.FirstOrDefault(x => x.INMasterID == docketDetail.INMasterID);
                                    if (newPriceDetail != null)
                                    {
                                        if (newPriceDetail.Price != currentPrice)
                                        {
                                            docketChange = true;
                                            docketDetail.PriceListID = newPriceListId;
                                            docketDetail.UnitPrice = newPriceDetail.Price;
                                            docketDetail.UnitPriceAfterDiscount = newPriceDetail.Price;

                                            var priceMethodID = newPriceDetail.NouPriceMethodID;
                                            if (priceMethodID == priceByQty.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT =
                                                    docketDetail.QuantityDelivered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else if (priceMethodID == priceByWgt.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT = docketDetail.WeightDelivered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else
                                            {
                                                var typicalWgt = docketDetail.INMaster.NominalWeight;
                                                var qtyDelivered = docketDetail.QuantityDelivered;
                                                docketDetail.TotalExclVAT = (typicalWgt.ToDecimal() *
                                                                             qtyDelivered.ToDecimal()) *
                                                                             newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;

                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }

                            if (docketChange)
                            {
                                entities.SaveChanges();

                                var totalExVat = docketDetails.Sum(x => x.TotalExclVAT);
                                var totalIncVat = docketDetails.Sum(x => x.TotalIncVAT);

                                docket.SubTotalExVAT = totalExVat;
                                docket.GrandTotalIncVAT = totalIncVat;
                                entities.SaveChanges();

                                saleOrdersOnDockets.Add(docket.BaseDocumentReferenceID.ToInt());
                            }
                        }

                        #endregion

                        #region Order

                        var saleOrders = entities.AROrders
                            .Where(x => ((x.NouDocStatusID != statusComplete.NouDocStatusID
                                   && x.NouDocStatusID != statusCancelled.NouDocStatusID
                                   && x.Deleted == null) || saleOrdersOnDockets.Contains(x.AROrderID))
                                        && x.BPMasterID_Customer == partnerId).ToList();

                        foreach (var docket in saleOrders)
                        {
                            var docketChange = false;
                            var docketDetails = docket.AROrderDetails.Where(x => x.Deleted == null).ToList();
                            foreach (var docketDetail in docketDetails)
                            {
                                var currentpriceId = docketDetail.PriceListID;
                                var currentPriceDetail = priceListDetails
                                    .FirstOrDefault(x => x.PriceListID == currentpriceId && x.INMasterID == docketDetail.INMasterID);
                                if (currentPriceDetail != null)
                                {
                                    var currentPrice = currentPriceDetail.Price;
                                    var newPriceDetail =
                                        newPriceListDetails.FirstOrDefault(x => x.INMasterID == docketDetail.INMasterID);
                                    if (newPriceDetail != null)
                                    {
                                        if (newPriceDetail.Price != currentPrice)
                                        {
                                            docketChange = true;
                                            docketDetail.PriceListID = newPriceListId;
                                            docketDetail.UnitPrice = newPriceDetail.Price;
                                            docketDetail.UnitPriceAfterDiscount = newPriceDetail.Price;

                                            var priceMethodID = newPriceDetail.NouPriceMethodID;
                                            if (priceMethodID == priceByQty.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT =
                                                    docketDetail.QuantityOrdered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else if (priceMethodID == priceByWgt.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT = docketDetail.WeightOrdered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else
                                            {
                                                var typicalWgt = docketDetail.INMaster.NominalWeight;
                                                var qtyOrdered = docketDetail.QuantityOrdered;
                                                docketDetail.TotalExclVAT = (typicalWgt.ToDecimal() *
                                                                             qtyOrdered.ToDecimal()) *
                                                                             newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;

                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }

                            if (docketChange)
                            {
                                entities.SaveChanges();

                                var totalExVat = docketDetails.Sum(x => x.TotalExclVAT);
                                var totalIncVat = docketDetails.Sum(x => x.TotalIncVAT);

                                docket.SubTotalExVAT = totalExVat;
                                docket.GrandTotalIncVAT = totalIncVat;
                                entities.SaveChanges();
                            }
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Method that updates a price list.
        /// </summary>
        /// <param name="priceListsToUpdate">The price list to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdatePriceMasters(IList<PriceList> priceListsToUpdate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var priceList in priceListsToUpdate)
                    {
                        var dbList =
                            entities.PriceLists.FirstOrDefault(
                                list => list.PriceListID == priceList.PriceListID);

                        if (dbList != null)
                        {
                            dbList.EndDate = DateTime.Today;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        private IList<NouPriceMethod> GetPriceMethods()
        {
            var priceMethods = new List<NouPriceMethod>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceMethods = entities.NouPriceMethods.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return priceMethods;
        }

        /// <summary>
        /// Gets the price lists.
        /// </summary>
        /// <returns>The price lists.</returns>
        public IList<PriceList> GetPriceLists()
        {
            try
            {
                var entities = new NouvemEntities();
                return entities.PriceLists.Where(x => !x.Deleted).ToList();

            }
            catch (Exception ex)
            {
            }

            return null;
        }

        /// <summary>
        /// Gets the price list details.
        /// </summary>
        /// <returns>The price list details.</returns>
        public IList<PriceListDetail> GetPriceListDetails()
        {
            try
            {
                var entities = new NouvemEntities();
                return entities.PriceListDetails.Where(x => !x.Deleted).ToList();

            }
            catch (Exception ex)
            {
            }

            return null;
        }

        #endregion

        #region business partners

        /// <summary>
        /// Gets the partners.
        /// </summary>
        /// <returns>The partners.</returns>
        public IList<BPMaster> GetBusinessPartners()
        {
            var partners = new List<BPMaster>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BPMasters.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return partners;
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnersToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateBusinessPartnerPricing(IList<BPMaster> partnersToUpdate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var partnerToUpdate in partnersToUpdate)
                    {
                        var dbPartner =
                           entities.BPMasters.FirstOrDefault(
                             partner => partner.BPMasterID == partnerToUpdate.BPMasterID);

                        if (dbPartner != null)
                        {
                            dbPartner.PriceListID = partnerToUpdate.PriceListID;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        #endregion

        #region doc status

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        public IList<NouDocStatu> GetNouDocStatuses()
        {
            var statuses = new List<NouDocStatu>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    statuses = entities.NouDocStatus.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return statuses;
        }

        #endregion

        #region production

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public bool RemoveBatches()
        {
            this.log.LogInfo(this.GetType(), "RemoveBatches(): Removing batches..");
            var date = DateTime.Today;
            var completeDate = date.AddMonths(-ApplicationSettings.RemoveBatchesOverMonths);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batches = entities.PROrders.Where(x => x.NouDocStatusID == 1 && x.CreationDate < completeDate);
                    foreach (var prOrder in batches)
                    {
                        prOrder.NouDocStatusID = 3;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        #region attributes

        /// <summary>
        /// Gets the application categories.
        /// </summary>
        /// <returns></returns>
        public IList<NouCategory> GetCategories()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouCategories.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the application breeds.
        /// </summary>
        /// <returns></returns>
        public IList<NouBreed> GetBreeds()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouBreeds.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the application breeds.
        /// </summary>
        /// <returns></returns>
        public IList<ImportAttributeLookup> GetAttributeLookups()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.ImportAttributeLookups.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }


        #endregion

        #region intake

        /// <summary>
        /// Adds the intake transactions ahead of delivery.
        /// </summary>
        /// <param name="details">The data to add.</param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public bool AddIntakeTransactions(IList<StockDetail> details)
        {
            this.log.LogInfo(this.GetType(), string.Format("Adding {0} intake transactions", details.Count));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        this.log.LogInfo(this.GetType(), string.Format("Adding barcode {0}", stockDetail.StockTransaction.Comments));
                        entities.Attributes.Add(stockDetail.Attribute);
                        entities.SaveChanges();
                        stockDetail.StockTransaction.AttributeID = stockDetail.Attribute.AttributeID;

                        entities.StockTransactions.Add(stockDetail.StockTransaction);
                        entities.SaveChanges();
                        this.log.LogInfo(this.GetType(), string.Format("Barcode {0} added", stockDetail.StockTransaction.Comments));
                    }

                    this.log.LogInfo(this.GetType(), "Intake complete");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }


            return false;
        }

        /// <summary>
        /// Retrieves the payments carcass prices.
        /// </summary>
        /// <returns></returns>
        public IList<App_GetCarcassPrices_Result1> GetCarcassPrices()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetCarcassPrices(0).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            
            return null;
        }

        /// <summary>
        /// Retrieves the payments carcass prices.
        /// </summary>
        /// <returns></returns>
        public bool UpdateAveragePriceList()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    entities.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "exec App_UpdateAveragePriceList");
                    entities.Database.CommandTimeout = 30;
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the payments carcass prices.
        /// </summary>
        /// <returns></returns>
        public bool ImportCarcassPrice(IList<StockDetail> carcasses)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in carcasses)
                    {
                        try
                        {
                            entities.App_ImportCarcassPrice(stockDetail.Carcass, stockDetail.KillDate, stockDetail.Price);
                        }
                        catch (Exception ex)
                        {
                            this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                        }
            
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds the intake transactions ahead of delivery.
        /// </summary>
        /// <param name="details">The data to add.</param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public bool MarkIntakesAsExported(HashSet<int> intakes)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var intakeId in intakes)
                    {
                        var intake = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == intakeId);
                        if (intake != null)
                        {
                            intake.ReferenceID = 2;
                            entities.SaveChanges();
                        }
                    }

                    this.log.LogInfo(this.GetType(), "Intake complete");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }


            return false;
        }

        /// <summary>
        /// Adds the intake transactions ahead of delivery.
        /// </summary>
        /// <param name="details">The data to add.</param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public string AddImportLairage(int apGoodsReceiptId,int number,string documentdate,string remarks,string killdate,string moveresponse,string reference,
        int serial,string transactiondate,decimal transactionqty, decimal transactionwgt, decimal tare, int inmasterid,string isbox,string eartag,string herd,string dob,
            int category,string sex,string farmassured,int ageinmonths,string clipped, string casualty,string lame,string cleanliness,int noofmoves,int currentresidency,
            int totalresidency,string dateoflastmove,string generic1, string generic2, int killno,int carcassno,string sequenceddate, string gradingdate,string condemned,string detained,string notinsured,
            string tbyes,string burstbelly,string fatcolour,int carcassside,decimal coldwgt,string grade,decimal side1wgt,decimal side2wgt,string countryoforigin, string rearedin,
            int supplierid,string attribute220, string attribute230, string attribute231, string attribute232, string attribute233, string attribute234, string attribute235, 
            string attribute236, string attribute237, string attribute238, string suppliername, string suppliercode, string add1, string add2, string add3, string add4)
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_ImportLairage(apGoodsReceiptId, number, documentdate, remarks, killdate, moveresponse,
                        reference, serial, transactiondate, transactionqty, transactionwgt,
                        tare, inmasterid, isbox, eartag, herd, dob, category, sex, farmassured, ageinmonths, clipped,
                        casualty, lame, cleanliness, noofmoves, currentresidency, totalresidency,
                        dateoflastmove, generic1, generic2, killno, carcassno, sequenceddate, gradingdate, condemned,
                        detained, notinsured, tbyes, burstbelly, fatcolour, carcassside,
                        coldwgt, grade, side1wgt, side2wgt, countryoforigin, rearedin, supplierid, attribute220,
                        attribute230, attribute231, attribute232, attribute233,
                        attribute234, attribute235, attribute236, attribute237, attribute238, suppliername,
                        suppliercode, add1, add2, add3, add4);

                    this.log.LogInfo(this.GetType(), "Intake complete");
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return ex.Message;
            }
        }

        /// <summary>
        /// Adds the intake transactions ahead of delivery.
        /// </summary>
        /// <param name="details">The data to add.</param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public bool AddLairageTransactions(IList<StockDetail> details)
        {
            this.log.LogInfo(this.GetType(), string.Format("Adding {0} intake transactions", details.Count));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details.GroupBy(x => x.Attribute.Herd))
                    {
                        var outputAddParam = new System.Data.Entity.Core.Objects.ObjectParameter("MasterTableID", typeof(int));
                        var lairageData = stockDetail.First();
                        try
                        {
                            entities.App_AddLairage(lairageData.Attribute.Attribute4,
                                lairageData.Attribute.Attribute5,
                                lairageData.Attribute.Attribute6,
                                lairageData.Attribute.Attribute7,
                                lairageData.Attribute.Herd,
                                outputAddParam);
                        }
                        catch (Exception e)
                        {
                            this.log.LogError(this.GetType(), $"Add Lairage:{e.Message}, {lairageData.Attribute.Attribute5}");
                        }

                        var masterTableId = outputAddParam.Value.ToInt();
                        if (masterTableId == 0)
                        {
                            continue;
                        }

                        foreach (var detail in stockDetail)
                        {
                            try
                            {
                                entities.App_UpdateLairage( 
                                    detail.Attribute.Eartag,
                                    detail.Attribute.Attribute1,
                                    detail.Attribute.DOB,
                                    detail.Attribute.Attribute2,
                                    detail.Attribute.Herd,
                                    detail.Attribute.Sex,
                                    detail.Attribute.Attribute3,
                                    detail.Attribute.CountryOfOrigin,
                                    detail.Attribute.NoOfMoves,
                                    detail.Attribute.DateOfLastMove,
                                    masterTableId,
                                    detail.Attribute.Attribute230,
                                    detail.Attribute.Attribute231,
                                    detail.Attribute.Attribute232,
                                    detail.Attribute.Attribute233,
                                    detail.Attribute.Attribute234,
                                    detail.Attribute.Attribute235,
                                    detail.Attribute.Attribute236,
                                    detail.Attribute.Attribute237,
                                    detail.Attribute.Attribute229,
                                    detail.Attribute.Attribute249);
                            }
                            catch (Exception e)
                            {
                                this.log.LogError(this.GetType(), $"{e.Message}, {detail.Attribute.Eartag}");
                            }  
                        }
                    }

                    this.log.LogInfo(this.GetType(), "Intake complete");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the dispatch carcass data.
        /// </summary>
        /// <returns>The dispatch carcass data.</returns>
        public Sale GetDispatchStock()
        {
            var dispatch = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    dispatch.StockDetails = (from stock in entities.App_DispatchExportData(0)
                                             select new StockDetail
                                             {
                                                 Number = stock.Number,
                                                 Barcode = stock.Serial,
                                                 TransactionWeight = stock.ColdWeight,
                                                 Eartag = stock.Eartag,
                                                 TotalResidency = stock.TotalResidency,
                                                 CurrentResidency = stock.CurrentResidency,
                                                 Grade = stock.Grade,
                                                 GradingDate = stock.GradingDate,
                                                 CarcassNumber = stock.CarcassNumber,
                                                 FarmAssured = stock.FarmAssured,
                                                 DOB = stock.DOB,
                                                 DateOfLastMove = stock.DateOfLastMove,
                                                 HerdNo = stock.Herd,
                                                 CatName = stock.CAT,
                                                 BreedName = stock.Breed,
                                                 Product = stock.Product,
                                                 Attribute100 = stock.Reference1,
                                                 Attribute101 = stock.Reference2,
                                                 Attribute102 = stock.Reference3,
                                                 Attribute103 = stock.Reference4,
                                                 Attribute104 = stock.Reference5,
                                                 Attribute105 = stock.Reference6,
                                                 Attribute106 = stock.Reference7,
                                                 Attribute107 = stock.Reference8,
                                                 Attribute108 = stock.Reference9,
                                                 Attribute109 = stock.Reference10

                                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dispatch;
        }

        /// <summary>
        /// Adds the intake transactions ahead of delivery.
        /// </summary>
        /// <param name="details">The data to add.</param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public IList<App_GetLairagesForExport_Result3> GetLairgesForExport()
        {
           try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetLairagesForExport().ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds the intake transactions ahead of delivery.
        /// </summary>
        /// <param name="details">The data to add.</param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public IList<string> AddOrUpdateUnifyOrders(IList<RootObject> details)
        {
            var ordersRead = new List<string>();
            this.log.LogInfo(this.GetType(), string.Format("Adding {0} intake transactions", details.Count));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in details)
                    {
                        var error = string.Empty;
                        var unifyId = stockDetail.order.id;
                        var delDate = stockDetail.order.fordate;
                        var customerId = stockDetail.order.customer.account_id;

                        var outputAddParam = new System.Data.Entity.Core.Objects.ObjectParameter("Error", typeof(string));

                        entities.App_AddUnifyOrder(
                            unifyId,
                            delDate,
                            customerId,
                            outputAddParam);

                        error = outputAddParam.Value.ToString();
                        if (error != string.Empty)
                        {
                            var order =
                                entities.AROrders.FirstOrDefault(x => x.OtherReference == unifyId);
                            if (order != null)
                            {
                                order.NouDocStatusID = 2;
                                order.Deleted = DateTime.Now;
                                entities.SaveChanges();
                            }

                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                            System.Threading.Thread.Sleep(2000);
                            break;
                        }

                        try
                        {
                            foreach (var orderItem in stockDetail.order.order_items)
                            {
                                outputAddParam = new System.Data.Entity.Core.Objects.ObjectParameter("Error", typeof(string));
                                entities.App_AddOrUpdateUnifyOrder(
                                    unifyId,
                                    delDate,
                                    customerId,
                                    orderItem.price,
                                    orderItem.code,
                                    orderItem.order_quantity,
                                    outputAddParam);

                                error = outputAddParam.Value.ToString();
                                if (error != string.Empty)
                                {
                                    var order =
                                        entities.AROrders.FirstOrDefault(x => x.OtherReference == unifyId);
                                    if (order != null)
                                    {
                                        order.NouDocStatusID = 2;
                                        order.Deleted = DateTime.Now;
                                        entities.SaveChanges();
                                    }

                                    EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                                    System.Threading.Thread.Sleep(2000);
                                    break;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            error = string.Format("Exception:{0} Inner:{1}", e.Message, e.InnerException);
                            var order =
                                entities.AROrders.FirstOrDefault(x => x.OtherReference == unifyId);
                            if (order != null)
                            {
                                order.NouDocStatusID = 2;
                                order.Deleted = DateTime.Now;
                                entities.SaveChanges();
                            }

                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", e.Message, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
                        }

                        if (error == string.Empty)
                        {
                            ordersRead.Add(unifyId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return ordersRead;
        }

        ///// <summary>
        ///// Adds the intake transactions ahead of delivery.
        ///// </summary>
        ///// <param name="details">The data to add.</param>
        ///// <returns>Flag, as to successfull add or not.</returns>
        //public IList<string> AddOrUpdateUnifyOrders(IList<RootObject> details)
        //{
        //    var ordersRead = new List<string>();
        //    this.log.LogInfo(this.GetType(), string.Format("Adding {0} intake transactions", details.Count));
        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            foreach (var stockDetail in details)
        //            {
        //                var error = string.Empty;
        //                var unifyId = stockDetail.order.id;
        //                var delDate = stockDetail.order.fordate;
        //                var customerId = stockDetail.order.customer.account_id;
        //                var outputAddParam = new System.Data.Entity.Core.Objects.ObjectParameter("Error", typeof(string));
        //                try
        //                {
        //                    foreach (var orderItem in stockDetail.order.order_items)
        //                    {
        //                        entities.App_AddOrUpdateUnifyOrder(
        //                            unifyId,
        //                            delDate,
        //                            customerId,
        //                            orderItem.price,
        //                            orderItem.code,
        //                            orderItem.order_quantity,
        //                            outputAddParam);

        //                        error = outputAddParam.Value.ToString();
        //                        if (error != string.Empty)
        //                        {
        //                            var order =
        //                                entities.AROrders.FirstOrDefault(x => x.OtherReference == unifyId);
        //                            if (order != null)
        //                            {
        //                                order.NouDocStatusID = 2;
        //                                order.Deleted = DateTime.Now;
        //                                entities.SaveChanges();
        //                            }

        //                            EmailManager.Instance.SendEmail("Unify Order Import Issue!", error, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
        //                            System.Threading.Thread.Sleep(2000);
        //                            break;
        //                        }
        //                    }
        //                }
        //                catch (Exception e)
        //                {
        //                    error = string.Format("Exception:{0} Inner:{1}", e.Message, e.InnerException);
        //                    var order =
        //                        entities.AROrders.FirstOrDefault(x => x.OtherReference == unifyId);
        //                    if (order != null)
        //                    {
        //                        order.NouDocStatusID = 2;
        //                        order.Deleted = DateTime.Now;
        //                        entities.SaveChanges();
        //                    }

        //                    EmailManager.Instance.SendEmail("Unify Order Import Issue!", e.Message, ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses);
        //                }

        //                if (error == string.Empty)
        //                {
        //                    ordersRead.Add(unifyId);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return ordersRead;
        //}

        #endregion
    }
}

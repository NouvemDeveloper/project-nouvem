﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Scheduler.Model.BusinessObject
{
    public class BusinessPartner
    {
        /// <summary>
        /// The partner.
        /// </summary>
        public BPMaster BpMaster { get; set; }

        /// <summary>
        /// The partner addresses.
        /// </summary>
        public IList<BPAddress> Addresses { get; set; }

        /// <summary>
        /// The partner contact.
        /// </summary>
        public BPContact Contact { get; set; }

        /// <summary>
        /// The partner price book.
        /// </summary>
        public PriceList PriceList{ get; set; }

        /// <summary>
        /// The partner price book.
        /// </summary>
        public string PriceListName { get; set; }
    }
}

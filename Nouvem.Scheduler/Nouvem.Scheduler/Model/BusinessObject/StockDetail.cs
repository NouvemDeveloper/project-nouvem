﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Model.BusinessObject
{
    public class StockDetail
    {
        public StockDetail()
        {
            this.StockTransaction = new StockTransaction();
            this.Attribute = new Attribute();
        }

        /// <summary>
        /// Gets or sets the transaction.
        /// </summary>
        public StockTransaction StockTransaction { get; set; }

        /// <summary>
        /// Gets or sets the associated attribute.
        /// </summary>
        public Attribute Attribute { get; set; }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public int? Serial { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// Gets or sets the transaction weight.
        /// </summary>
        public decimal? TransactionWeight { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Grade { get; set; }

        /// <summary>
        /// Gets or sets the current residency.
        /// </summary>
        public int? CurrentResidency { get; set; }

        /// <summary>
        /// Gets or sets the current residency.
        /// </summary>
        public int? TotalResidency { get; set; }

        /// <summary>
        /// Gets or sets the grading date.
        /// </summary>
        public DateTime? GradingDate { get; set; }

        /// <summary>
        /// Gets or sets the carcass no.
        /// </summary>
        public int? CarcassNumber { get; set; }

        /// <summary>
        /// Gets or sets the carcass no.
        /// </summary>
        public string Carcass { get; set; }

        /// <summary>
        /// Gets or sets the carcass no.
        /// </summary>
        public string KillDate { get; set; }

        /// <summary>
        /// Gets or sets the carcass no.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public bool? FarmAssured { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public DateTime? DOB { get; set; }

        /// <summary>
        /// Gets or sets the sex.
        /// </summary>
        public DateTime? DateOfLastMove { get; set; }

        /// <summary>
        /// Gets or sets the herd.
        /// </summary>
        public string HerdNo { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public string CatName { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string BreedName { get; set; }
        public string Product { get; set; }

        public string Attribute100 { get; set; }

        public string Attribute101 { get; set; }
        public string Attribute102 { get; set; }
        public string Attribute103 { get; set; }
        public string Attribute104 { get; set; }
        public string Attribute105 { get; set; }
        public string Attribute106 { get; set; }
        public string Attribute107 { get; set; }
        public string Attribute108 { get; set; }
        public string Attribute109 { get; set; }

        /// <summary>
        /// Gets or sets the cold weight.
        /// </summary>
        public decimal ColdWeight
        {
            get
            {
                return Math.Round(this.TransactionWeight.ToDecimal() * 0.98m, 1);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Scheduler.Model.BusinessObject
{
    public class Sale
    {
        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public int SaleID { get; set; }

        /// <summary>
        /// Gets or sets the stock details.
        /// </summary>
        public IList<StockDetail> StockDetails { get; set; }
    }
}

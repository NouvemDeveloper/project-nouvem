﻿// -----------------------------------------------------------------------
// <copyright file="SaleOrder.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;

    public class SaleOrder
    {
        /// <summary>
        /// Gets or sets the sale order id.
        /// </summary>
        public long SaleOrderId { get; set; }

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// Gets or sets purchase order number.
        /// </summary>
        public string PurchaseOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets currency id.
        /// </summary>
        public int? CurrencyId { get; set; }

        /// <summary>
        /// Gets or sets currency rate.
        /// </summary>
        public decimal? CurrencyRate { get; set; }

        /// <summary>
        /// Gets or sets the delivery address.
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        /// Gets or sets the other reference.
        /// </summary>
        public string OtherRef { get; set; }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        public DateTime? TimeDate { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// Gets or sets the order status.
        /// </summary>
        public int OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the order is filled or not.
        /// </summary>
        public bool OrderFilled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the order has been sent to the scales.
        /// </summary>
        public bool SentToScales { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the order has been printed.
        /// </summary>
        public bool? Printed { get; set; }

        /// <summary>
        /// Gets or sets the standing order template.
        /// </summary>
        public int StandingOrderTemplate { get; set; }

        /// <summary>
        /// Gets or sets the standing order.
        /// </summary>
        public int StandingOrder { get; set; }

        /// <summary>
        /// Gets or sets the hh edit state.
        /// </summary>
        public int HHEditState { get; set; }

        /// <summary>
        /// Gets or sets the carriage charge.
        /// </summary>
        public decimal? CarriageCharge { get; set; }

        /// <summary>
        /// Gets or sets the chef order.
        /// </summary>
        public int? ChefOrder { get; set; }

        /// <summary>
        /// Gets or sets the dispatch notes flag.
        /// </summary>
        public long? ShowDispatchNotes { get; set; }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the sale order items.
        /// </summary>
        public IList<ARDispatchDetail> SaleOrderItems { get; set; }

        /// <summary>
        /// Gets the sale order items count. </summary>
        public int SaleOrderItemsCount
        {
            get
            {
                return this.SaleOrderItems.Count;
            }
        }
    }
}


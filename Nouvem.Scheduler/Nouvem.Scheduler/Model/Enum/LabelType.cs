﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Scheduler.Model.Enum
{
    public enum LabelType
    {
        /// <summary>
        /// The box label type.
        /// </summary>
        Box,

        /// <summary>
        /// The item label type.
        /// </summary>
        Item,

        /// <summary>
        /// The piece label type.
        /// </summary>
        Piece,

        /// <summary>
        /// The pallet label type.
        /// </summary>
        Pallet,

        /// <summary>
        /// The shipping label type.
        /// </summary>
        Shipping
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="HandlerBase.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Logging;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using Nouvem.Scheduler.Model.Repository;

    public class HandlerBase
    {
        private static readonly DataRepository DataRepository = new DataRepository();

        /// <summary>
        /// Gets the application logger.
        /// </summary>
        protected ILogger log = new Logger();

        /// <summary>
        /// Gets the data manager singleton.
        /// </summary>
        public static DataRepository Data
        {
            get
            {
                return DataRepository;
            }
        }
    }
}

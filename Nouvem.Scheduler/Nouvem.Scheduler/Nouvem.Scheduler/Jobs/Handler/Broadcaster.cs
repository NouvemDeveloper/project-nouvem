﻿// -----------------------------------------------------------------------
// <copyright file="Broadcaster.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using Nouvem.Scheduler.Global;
    using Quartz;

    public class Broadcaster : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "broadcasting check..");
            this.CheckForDatabaseChanges();
        }

        /// <summary>
        /// Checks for any db changes.
        /// </summary>
        private void CheckForDatabaseChanges()
        {
            var localTime = SchedulerGlobal.LastDatabaseCheckTime;
            SchedulerGlobal.LastDatabaseCheckTime = DateTime.Now;
            if (Data.CheckBusinessPartners(localTime))
            {
                this.BroadcastBusinessPartnerChange();
            }
        }

        /// <summary>
        /// Broadcasts a message letting any observers know that the businesss partners have updated.
        /// </summary>
        private void BroadcastBusinessPartnerChange()
        {
            this.log.LogInfo(this.GetType(), "Broadcasting business partner change");
            var client = new UdpClient();
            var ip = new IPEndPoint(IPAddress.Broadcast, 15000);
            var messageStream = Encoding.ASCII.GetBytes("Update Partners");
            client.Send(messageStream, messageStream.Length, ip);
            client.Close();
        }
    }
}

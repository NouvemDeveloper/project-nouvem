﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPrices.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using Quartz;

    public class RemoveOldBatches : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Data.RemoveBatches();
        }
    }
}


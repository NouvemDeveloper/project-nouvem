﻿// -----------------------------------------------------------------------
// <copyright file="Broadcaster.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using Nouvem.Scheduler.Global;
    using Quartz;
    using Nouvem.AccountsIntegration;

    public class DataIntegration : HandlerBase, IJob
    {
        private Nouvem.AccountsIntegration.Exchequer accounts = new Exchequer();

        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "Checking data integration..");
            this.GetPartners();
            this.GetProducts();
        }

        /// <summary>
        /// Gets the newly added partners, and adds to the db.
        /// </summary>
        private void GetPartners()
        {
            var data = this.accounts.GetPartners(ApplicationSettings.AccountsDirectory);
            var partners = data.Item1;
            var error = data.Item2;

            if (error != string.Empty)
            {
                this.log.LogError(this.GetType(), string.Format("Scheduler: GetPartners(): {0}", error));
            }

            var partnerToAdd = new List<BusinessPartner>();
            this.log.LogInfo(this.GetType(), string.Format("{0} partners found to process", partners.Count));
            foreach (var partner in partners)
            {
                var bpPartner = new BusinessPartner();
                
                bpPartner.BpMaster = new BPMaster
                {
                    NouBPTypeID = 1,
                    Code = partner.Code,
                    Name = partner.Name,
                    BPGroupID = 3,
                    Tel = partner.Phone,
                    FAX = partner.Fax,
                    Email = partner.Email,
                    Notes = partner.CustomerNotes,
                    CreditLimit = partner.CreditLimit,
                    Balance = partner.AccountBalance,
                    CreationDate = DateTime.Now,
                    EditDate = DateTime.Now
                };

                if (partner.AccountStatus <= 1)
                {
                    bpPartner.BpMaster.OnHold = false;
                    bpPartner.BpMaster.InActiveFrom = null;
                    bpPartner.BpMaster.InActiveTo = null;
                }
                else if (partner.AccountStatus == 2)
                {
                    bpPartner.BpMaster.OnHold = true;
                }
                else if (partner.AccountStatus == 3)
                {
                    bpPartner.BpMaster.InActiveFrom = DateTime.Today;
                    bpPartner.BpMaster.InActiveTo = DateTime.Today.Date.AddYears(100);
                }

                if (!string.IsNullOrEmpty(partner.ContactName))
                {
                    bpPartner.Contact = new BPContact
                    {
                        FirstName = partner.ContactName,
                        Phone = string.Empty,
                        Mobile = string.Empty,
                        Email = string.Empty,
                        Deleted = false
                    };
                }

                if (partner.Currency.StartsWithIgnoringCase("UK") || partner.Currency.StartsWithIgnoringCase("GB") || partner.Currency.StartsWithIgnoringCase("£"))
                {
                    bpPartner.BpMaster.BPCurrencyID = 3;
                }
                else
                {
                    bpPartner.BpMaster.BPCurrencyID = 2;
                }

                var addresses = new List<BPAddress>();
                var localAddress = partner.AddressLine1.Split(new [] { "<CR>" }, StringSplitOptions.None);
                if (localAddress.Any())
                {
                    var address = new BPAddress
                    {
                        AddressLine1 = localAddress.ElementAt(0),
                        AddressLine2 = localAddress.ElementAt(1),
                        AddressLine3 = localAddress.ElementAt(2),
                        AddressLine4 = localAddress.ElementAt(3),
                        PostCode = localAddress.ElementAt(4),
                        Billing = true
                    };

                    addresses.Add(address);
                }

                var localDelAddress = partner.AddressLine2.Split(new[] { "<CR>" }, StringSplitOptions.None);
                if (localDelAddress.Any())
                {
                    var address = new BPAddress
                    {
                        AddressLine1 = localDelAddress.ElementAt(0),
                        AddressLine2 = localDelAddress.ElementAt(1),
                        AddressLine3 = localDelAddress.ElementAt(2),
                        AddressLine4 = localDelAddress.ElementAt(3),
                        PostCode = localDelAddress.ElementAt(4),
                        Shipping = true
                    };

                    addresses.Add(address);
                }

                bpPartner.Addresses = addresses;

                bpPartner.PriceList = new PriceList
                {
                    CurrentPriceListName = bpPartner.BpMaster.Name,
                    Factor = 0,
                    NouRoundingOptionsID = 1,
                    NouRoundingRulesID = 3,
                    BPCurrencyID = bpPartner.BpMaster.BPCurrencyID != null ? bpPartner.BpMaster.BPCurrencyID.ToInt() : 3,
                    CreationDate = DateTime.Now,
                    EditDate = DateTime.Now,
                    Deleted = false,
                };

                partnerToAdd.Add(bpPartner);
            }

            if (partnerToAdd.Any())
            {
                Data.AddOrUpdatePartners(partnerToAdd);
            }
        }

        /// <summary>
        /// Gets the newly added partners, and adds to the db.
        /// </summary>
        private void GetProducts()
        {
            var data = this.accounts.GetProducts(ApplicationSettings.AccountsDirectory);
            var products = data.Item1;
            var error = data.Item2;

            if (error != string.Empty)
            {
                this.log.LogError(this.GetType(), string.Format("Scheduler: GetProducts(): {0}", error));
            }

            var productsToAdd = new List<INMaster>();
            this.log.LogInfo(this.GetType(), string.Format("{0} products found to process", products.Count));
            foreach (var product in products)
            {
                var inMaster = new INMaster
                {
                    Code = product.Code,
                    Name = product.Name,
                    TraceabilityTemplateNameID = ApplicationSettings.TraceabilityTemplateId,
                    DateTemplateNameID = ApplicationSettings.DateTemplateId,
                    AttributeTemplateID = ApplicationSettings.AttributeTemplateId,
                    SalesItem = true,
                    PurchaseItem = true,
                    VATCodeID = ApplicationSettings.VatCodeId,
                    MinWeight = 0,
                    MaxWeight = 0,
                    StockItem = false,
                    FixedAsset = false,
                    NominalWeight = 0,
                    TypicalPieces = 0,
                    ProductionProduct = true,
                    CreationDate = DateTime.Now,
                    EditDate = DateTime.Now,
                    PrintPieceLabels = false
                };

                inMaster.INGroupID = ApplicationSettings.ProductGroupId;
                if (!string.IsNullOrEmpty(product.GroupName))
                {
                    var group = Data.GetGroup(product.GroupName);
                    if (group != null)
                    {
                        inMaster.INGroupID = group.INGroupID;
                    }
                }

                productsToAdd.Add(inMaster);
            }

            if (productsToAdd.Any())
            {
                Data.AddOrUpdateProducts(productsToAdd);
            }
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Model.BusinessObject
{
    using System;

    public class SpecialPrice
    {
        /// <summary>
        /// Gets or sets the special price id.
        /// </summary>
        public int SpecialPriceID { get; set; }

        /// <summary>
        /// Gets or sets the special price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the in master id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the bp master id.
        /// </summary>
        public int? BPMasterID { get; set; }

        /// <summary>
        /// Gets or sets the in master id.
        /// </summary>
        public int? BPGroupID { get; set; }
       
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime Deleted { get; set; }
    }
}

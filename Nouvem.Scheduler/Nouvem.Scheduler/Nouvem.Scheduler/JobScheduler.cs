﻿// -----------------------------------------------------------------------
// <copyright file="JobScheduler.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Scheduler.Jobs.Trigger;

namespace Nouvem.Scheduler
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading.Tasks;

    public partial class JobScheduler : ServiceBase
    {
        private Data job = new Data();

        public static bool DeviceSettingsRetrieved { get; set; }

        public JobScheduler()
        {
            this.InitializeComponent();
        }

        public void OnDebug()
        {
            this.OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            this.job.SchedulerStart();
        }

        protected override void OnStop()
        {
            this.job.SaveSettings();
        }
    }
}

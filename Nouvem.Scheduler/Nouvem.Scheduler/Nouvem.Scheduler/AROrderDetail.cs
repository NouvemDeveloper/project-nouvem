//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Scheduler
{
    using System;
    using System.Collections.Generic;
    
    public partial class AROrderDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AROrderDetail()
        {
            this.ARDispatchDetails = new HashSet<ARDispatchDetail>();
        }
    
        public int AROrderDetailID { get; set; }
        public int AROrderID { get; set; }
        public int INMasterID { get; set; }
        public int INMasterSnapshotID { get; set; }
        public Nullable<double> QuantityOrdered { get; set; }
        public Nullable<double> WeightOrdered { get; set; }
        public Nullable<double> QuantityDelivered { get; set; }
        public Nullable<double> WeightDelivered { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<double> DiscountPercentage { get; set; }
        public Nullable<decimal> DiscountAmount { get; set; }
        public Nullable<decimal> DiscountPrice { get; set; }
        public Nullable<decimal> LineDiscountPercentage { get; set; }
        public Nullable<decimal> LineDiscountAmount { get; set; }
        public Nullable<decimal> UnitPriceAfterDiscount { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> TotalIncVAT { get; set; }
        public Nullable<decimal> TotalExclVAT { get; set; }
        public Nullable<int> VATCodeID { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public Nullable<int> PriceListID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARDispatchDetail> ARDispatchDetails { get; set; }
        public virtual AROrder AROrder { get; set; }
        public virtual INMaster INMaster { get; set; }
        public virtual PriceList PriceList { get; set; }
        public virtual INMasterSnapshot INMasterSnapshot { get; set; }
    }
}

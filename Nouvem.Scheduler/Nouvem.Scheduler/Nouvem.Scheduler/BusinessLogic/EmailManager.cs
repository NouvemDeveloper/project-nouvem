﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Email;

namespace Nouvem.Scheduler.BusinessLogic
{
    public class EmailManager
    {
        #region field

        private static EmailManager instance = new EmailManager();

        #endregion

        #region public interface

        /// <summary>
        /// Gets the singleton.
        /// </summary>
        public static EmailManager Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Send email notification.
        /// </summary>
        /// <param name="addresses">The email addresses.</param>
        /// <param name="content">The message content.</param>
        public void SendEmail(IList<string> addresses, string content)
        {
            var address = addresses.First();
            addresses.RemoveAt(0);
            var ccAddressList = addresses;

            var ccAddresses = (from cc in ccAddressList
                select new MailAddress(cc)).ToList();

            try
            {
                var mail = new Email.Email();
                mail.SendEmail(new EmailSettings
                {
                    FromAddress = "Nouvem.TaskScheduler@gmail.com",
                    ToAddress = address,
                    FromMailPassword = "Nouv1234",
                    Subject = "Task Scheduler Notification Alert !",
                    Body = content,
                    CCAddresses = ccAddresses
                });
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(@"C:\Nouvem\Test.txt", ex.Message);
            }
        }

        #endregion
    }
}

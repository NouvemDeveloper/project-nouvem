﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace Nouvem.Scheduler
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            this.InitializeComponent();
            this.AfterInstall += this.serviceInstaller1_AfterInstall;
        }

        private void serviceInstaller1_AfterUninstall(object sender, InstallEventArgs e)
        {
            //new ServiceController(this.serviceInstaller1.ServiceName).Start();
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            //using (ServiceController sc = new ServiceController(this.serviceInstaller1.ServiceName))
            //{
            //    sc.Start();
            //}
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Scheduler
{
    using System;
    using System.Collections.Generic;
    
    public partial class Label
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Label()
        {
            this.LabelAssociationLabels = new HashSet<LabelAssociationLabel>();
        }
    
        public int LabelID { get; set; }
        public string Name { get; set; }
        public Nullable<int> LabelGroupID { get; set; }
        public string LabelFile { get; set; }
        public byte[] LabelImage { get; set; }
        public string DataSource { get; set; }
        public string Orientation { get; set; }
        public bool Deleted { get; set; }
        public byte[] ExternalLabelFile { get; set; }
        public string Notes { get; set; }
        public Nullable<int> UserMasterID_ModifiedBy { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LabelAssociationLabel> LabelAssociationLabels { get; set; }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="BusinessPartnerAddress.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace ParseAddress
{
    using System;


    public class BusinessPartnerAddress
    {
        /// <summary>
        /// Gets or sets the address details.
        /// </summary>
        public BPAddress Details { get; set; }

       

        /// <summary>
        /// Gets the partner full address.
        /// </summary>
        public string FullAddress
        {
            get
            {
                return this.Details != null ? string.Format("{0} {1} {2} {3}",
                    this.Details.AddressLine1 ?? string.Empty,
                    this.Details.AddressLine2 ?? string.Empty,
                    this.Details.AddressLine3 ?? string.Empty,
                    this.Details.AddressLine4 ?? string.Empty)
                    : string.Empty;
            }
        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Nouvem.Webservice.Controllers
{
    public class LairageController : ApiController
    {
        public IHttpActionResult GetAllStudents()
        {
            IList<APGoodsReceipt> lairages = null;

            using (var entities = new DBEntities())
            {
                lairages = entities.APGoodsReceipts.ToList();
            }

            if (lairages.Count == 0)
            {
                return NotFound();
            }

            return Ok(lairages);
        }
    }
}

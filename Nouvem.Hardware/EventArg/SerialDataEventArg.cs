﻿// -----------------------------------------------------------------------
// <copyright file="SerialDataReceivedArgs.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Hardware.EventArg
{
    using System;

    /// <summary>
    /// Event arguments class for the serial received event.
    /// </summary>
    public class SerialDataReceivedArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SerialDataReceivedArgs "/> class.
        /// </summary>
        /// <param name="serialData">The serial value to broadcast.</param>
        public SerialDataReceivedArgs(string serialData)
        {
            this.SerialData = serialData;
        }

        /// <summary>
        /// Gets or sets the serial data,
        /// </summary>
        public string SerialData { get; set; }
    }
}




﻿// -----------------------------------------------------------------------
// <copyright file="Printer.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Hardware
{
    using System;
    using Nouvem.Hardware.EventArg;

    public class Printer : SerialBase
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Scanner"/> class.
        /// </summary>
        public Printer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scanner"/> class with arguments.
        /// </summary>
        /// <param name="port">The name of the com port.</param>
        /// <param name="baudRate">The baud rate setting.</param>
        /// <param name="parity">The parity setting.</param>
        /// <param name="stopBits">The stop bits value.</param>
        /// <param name="dataBits">The data bits value.</param>
        public Printer(
            string port,
            string baudRate,
            string parity,
            string stopBits,
            string dataBits)
            : base(port, baudRate, parity, stopBits, dataBits)
        {
        }

        #endregion

        #region event

        /// <summary>
        /// Event used to pass on the incoming serial data.
        /// </summary>
        public event EventHandler<SerialDataReceivedArgs> DataReceived;

        #endregion

        #region public interface

        /// <summary>
        /// Reads, and parses, the incoming serial data string.
        /// </summary>
        public void WriteToPort(string data)
        {
            this.SerialPort.Write(data);
        }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the incoming serial data reader, and adds indicator string parsing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected override void SerialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
        }

        protected override void ReadSerialData()
        {
            // mot implemented
        }
       
        #endregion

        #region private

        #endregion
    }
}


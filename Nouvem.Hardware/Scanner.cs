﻿// -----------------------------------------------------------------------
// <copyright file="Scanner.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Hardware
{
    using System;
    using Nouvem.Hardware.EventArg;

    public class Scanner : SerialBase
    {
        /// <summary>
        /// The buffer to hold the incoming data stream.
        /// </summary>
        private static string DataBuffer;

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Scanner"/> class.
        /// </summary>

        public Scanner()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scanner"/> class with arguments.
        /// </summary>
        /// <param name="port">The name of the com port.</param>
        /// <param name="baudRate">The baud rate setting.</param>
        /// <param name="parity">The parity setting.</param>
        /// <param name="stopBits">The stop bits value.</param>
        /// <param name="dataBits">The data bits value.</param>
        /// <param name="waitForPortDataTime">The time the thread sleeps in milliseconds waiting for the data read.</param>
        public Scanner(
            string port,
            string baudRate,
            string parity,
            string stopBits,
            string dataBits,
            int waitForPortDataTime = 100)
            : base(port, baudRate, parity, stopBits, dataBits)
        {
            this.WaitForPortDataTime = waitForPortDataTime;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scanner"/> class with arguments.
        /// </summary>
        /// <param name="port">The name of the com port.</param>
        /// <param name="baudRate">The baud rate setting.</param>
        /// <param name="parity">The parity setting.</param>
        /// <param name="stopBits">The stop bits value.</param>
        /// <param name="dataBits">The data bits value.</param>
        /// <param name="waitForPortDataTime">The time the thread sleeps in milliseconds waiting for the data read.</param>
        public Scanner(
            string port,
            int waitForPortDataTime = 100)
            : base(port)
        {
            this.WaitForPortDataTime = waitForPortDataTime;
        }

        #endregion

        #region event

        /// <summary>
        /// Event used to pass on the incoming serial data.
        /// </summary>
        public event EventHandler<SerialDataReceivedArgs> DataReceived;

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the incoming serial data reader, and adds indicator string parsing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected override void SerialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            base.SerialPort_DataReceived(sender, e);
            this.ReadSerialData();

            try
            {
                if (this.SerialPort.IsOpen)
                {
                    this.SerialPort.DiscardInBuffer();
                }
            }
            catch 
            {
            }
        }

        /// <summary>
        /// Reads, and parses, the incoming serial data string.
        /// </summary>
        protected override void ReadSerialData()
        {
            try
            {
                // remove the carriage return
                var data = this.PortData.Replace("\r\n", "");

                // Broadcast our scanner data to all interested subscribers.
                this.OnSerialDataReceived(new SerialDataReceivedArgs(data));
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Creates a new event, to broadcast the incoming serial data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnSerialDataReceived(SerialDataReceivedArgs e)
        {
            if (this.DataReceived != null)
            {
                this.DataReceived(this, e);
            }
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="Product.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration.BusinessObject
{
    public class Product
    {
        /// <summary>
        /// The product code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The product group name.
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// The product group name.
        /// </summary>
        public string Status { get; set; }
    }
}



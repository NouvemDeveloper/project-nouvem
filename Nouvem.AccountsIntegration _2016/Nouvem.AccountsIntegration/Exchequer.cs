﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.AccountsIntegration.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.AccountsIntegration
{
    public class Exchequer : IAccountsIntegration
    {
        /// <summary>
        /// Create accounts db connection.
        /// </summary>
        /// <param name="path">The db path.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string Connect(string path, string userName, string password)
        {
            return string.Empty;
        }

        /// <summary>
        /// Close session and release resource.
        /// </summary>
        public void Logout()
        {
            // not implemented
        }

        /// <summary>
        /// Retrieves the newly created accountrs partners.
        /// </summary>
        /// <param name="directory">The file directory</param>
        /// <returns>A list of new partners.</returns>
        public Tuple<List<BusinessPartner>, string> GetPartners(string directory)
        {
            var partners = new List<BusinessPartner>();
            try
            {
                if (!Directory.Exists(directory))
                {
                    return Tuple.Create(new List<BusinessPartner>(), "Invalid directory");
                }

                foreach (var file in Directory.EnumerateFiles(directory).Where(x => !x.EndsWith("Imported")))
                {
                    var processed = false;
                    using (var localFile = new StreamReader(file))
                    {
                        while (!localFile.EndOfStream)
                        {
                            var line = localFile.ReadLine();

                            if (string.IsNullOrEmpty(line))
                            {
                                break;
                            }

                            var data = line.Split('|');

                            if (!data.Any())
                            {
                                break;
                            }

                            if (data.ElementAt(0) != "CR")
                            {
                                break;
                            }

                            var partner = new BusinessPartner();
                            partner.Code = data.ElementAt(1);
                            partner.Name = data.ElementAt(2);
                            partner.ContactName = data.ElementAt(4);
                            partner.AddressLine1 = data.ElementAt(5);
                            partner.Phone = data.ElementAt(6);
                            partner.Fax = data.ElementAt(8);
                            partner.Email = data.ElementAt(9);
                            partner.CustomerNotes = data.ElementAt(11);
                            partner.Currency = data.ElementAt(12);
                            partner.AddressLine2 = data.ElementAt(14);
                            partner.AccountStatus = data.ElementAt(19).ToInt();
                            partners.Add(partner);
                            processed = true;
                        }
                    }

                    if (processed)
                    {
                        // mark the file as having been imported
                        var newFilename = string.Format("{0}_Imported", file);

                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Tuple.Create(new List<BusinessPartner>(), ex.Message);
            }

            return Tuple.Create(partners, string.Empty);
        }

        /// <summary>
        /// Retrieves the newly created account products.
        /// </summary>
        /// <param name="directory">The file directory</param>
        /// <returns>A list of new products.</returns>
        public Tuple<List<Product>, string> GetProducts(string directory)
        {
            var products = new List<Product>();
            try
            {
                if (!Directory.Exists(directory))
                {
                    return Tuple.Create(new List<Product>(), "Invalid directory");
                }

                foreach (var file in Directory.EnumerateFiles(directory).Where(x => !x.EndsWith("Imported")))
                {
                    var processed = false;
                    using (var localFile = new StreamReader(file))
                    {
                        while (!localFile.EndOfStream)
                        {
                            var line = localFile.ReadLine();

                            if (string.IsNullOrEmpty(line))
                            {
                                break;
                            }

                            var data = line.Split('|');

                            if (!data.Any())
                            {
                                break;
                            }

                            if (data.ElementAt(0) != "PR")
                            {
                                break;
                            }

                            var productName = string.Empty;
                            var localName = data.ElementAt(2).Split(new[] { "<CR>" }, StringSplitOptions.None);
                            if (localName.Any())
                            {
                                productName = localName.First();
                            }

                            var product = new Product();
                            product.Code = data.ElementAt(1);
                            product.Name = productName;
                            product.GroupName = data.ElementAt(3);
                            product.Status = data.ElementAt(27);
                            products.Add(product);
                            processed = true;
                        }
                    }

                    if (processed)
                    {
                        // mark the file as having been imported
                        var newFilename = string.Format("{0}_Imported", file);

                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Tuple.Create(new List<Product>(), ex.Message);
            }

            return Tuple.Create(products, string.Empty);
        }

        /// <summary>
        /// Posts the sale order invoice.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSalesInvoice(BusinessObject.InvoiceHeader header)
        {
            try
            {
                var fixedName = "SI";
                var invoiceNo = header.InvoiceNo;
                var invoiceDate = header.InvoiceDate.ToString("ddMMyy");
                var customerCode = header.TradingPartnerCode;
                var currencyRate = string.Empty;
                var currencySymbol = string.Empty;
                var poRef = header.CustomerOrderNo;
                var delDate = header.InvoiceDate.ToString("ddMMyy"); 
                var labelPrice = string.Empty;

                var fs = new FileStream(header.FilePath, FileMode.Append, FileAccess.Write);
                using (var localFile = new StreamWriter(fs))
                {
                    foreach (var invoiceDetail in header.Details)
                    {
                        var invoiceitemNo = string.Empty;
                        var qty = invoiceDetail.QuantityDelivered;
                        var wgt = invoiceDetail.WeightDelivered;
                        var unitsSold = invoiceDetail.Quantity;
                        var pricePerUnit = invoiceDetail.UnitPrice;
                        var productCode = invoiceDetail.ProductCode;
                        var productDescription = invoiceDetail.Description;

                        var line = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}",
                            fixedName,
                            invoiceNo,
                            invoiceDate,
                            customerCode,
                            currencyRate,
                            currencySymbol,
                            poRef,
                            delDate,
                            labelPrice,
                            invoiceitemNo,
                            qty,
                            wgt,unitsSold,
                            pricePerUnit,
                            productCode,
                            productDescription);

                        localFile.WriteLine(line);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }

        public string PostSaleOrder(BusinessObject.InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<BusinessObject.BusinessPartner>, string> GetPartners()
        {
            throw new NotImplementedException();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Sage50.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.AccountsIntegration.BusinessObject;
    using Nouvem.AccountsIntegration.Global;
    using Nouvem.Shared;
    using SageDataObject220;

    public class Sage50 : IAccountsIntegration
    {
        #region field

        /// <summary>
        /// The workspace reference.
        /// </summary>
        private WorkSpace workSpace;

        #endregion

        #region constructor

        #endregion

        /// <summary>
        /// Create accounts db connection.
        /// </summary>
        /// <param name="path">The db path.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string Connect(string path, string userName, string password)
        {
            var dataObjectEngine = new SDOEngine();
            this.workSpace = (WorkSpace)dataObjectEngine.Workspaces.Add("Nouvem2");
            var dataPath = dataObjectEngine.SelectCompany(path);

            try
            {
                //Leaving the username and password blank generates a login dialog
                this.workSpace.Connect(dataPath, userName, password, "Nouvem2");
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Close session and release resource.
        /// </summary>
        public void Logout()
        {
            this.workSpace.Disconnect();
        }

        /// <summary>
        /// Retrieves the partners.
        /// </summary>
        /// <returns>The accounts partners.</returns>
        public Tuple<List<BusinessPartner>, string> GetPartners(string path)
        {
            var localPartners = new List<BusinessPartner>();
            var error = string.Empty;
            var salesRecord = (SalesRecord)this.workSpace.CreateObject("SalesRecord");
            var purchaseRecord = (PurchaseRecord)this.workSpace.CreateObject("PurchaseRecord");

            try
            {
                for (int i = 0; i <= salesRecord.Count; i++)
                {
                    var partner = new BusinessPartner();
                    partner.Name = SDOHelper.Read(salesRecord, "NAME").ToString();
                    partner.Code = SDOHelper.Read(salesRecord, "ACCOUNT_REF").ToString();
                    partner.AddressLine1 = SDOHelper.Read(salesRecord, "ADDRESS_1").ToString();
                    partner.AddressLine2 = SDOHelper.Read(salesRecord, "ADDRESS_2").ToString();
                    partner.AddressLine3 = SDOHelper.Read(salesRecord, "ADDRESS_3").ToString();
                    partner.AddressLine4 = SDOHelper.Read(salesRecord, "ADDRESS_4").ToString();
                    partner.PostCode = SDOHelper.Read(salesRecord, "ADDRESS_5").ToString();
                    partner.ContactName = SDOHelper.Read(salesRecord, "CONTACT_NAME").ToString();
                    partner.Phone = SDOHelper.Read(salesRecord, "TELEPHONE").ToString();
                    partner.Fax = SDOHelper.Read(salesRecord, "FAX").ToString();
                    partner.Terms = SDOHelper.Read(salesRecord, "TERMS").ToString();
                    partner.IsCustomer = true;

                    localPartners.Add(partner);
                }

                for (int i = 0; i <= purchaseRecord.Count; i++)
                {
                    var partnerRef = SDOHelper.Read(salesRecord, "ACCOUNT_REF").ToString();
                    var customer = localPartners.FirstOrDefault(x => x.Code.Equals(partnerRef));
                    if (customer != null)
                    {
                        customer.IsSupplier = true;
                        continue;
                    }

                    var partner = new BusinessPartner();
                    partner.Name = SDOHelper.Read(salesRecord, "NAME").ToString();
                    partner.Code = SDOHelper.Read(salesRecord, "ACCOUNT_REF").ToString();
                    partner.AddressLine1 = SDOHelper.Read(salesRecord, "ADDRESS_1").ToString();
                    partner.AddressLine2 = SDOHelper.Read(salesRecord, "ADDRESS_2").ToString();
                    partner.AddressLine3 = SDOHelper.Read(salesRecord, "ADDRESS_3").ToString();
                    partner.AddressLine4 = SDOHelper.Read(salesRecord, "ADDRESS_4").ToString();
                    partner.PostCode = SDOHelper.Read(salesRecord, "ADDRESS_5").ToString();
                    partner.ContactName = SDOHelper.Read(salesRecord, "CONTACT_NAME").ToString();
                    partner.Phone = SDOHelper.Read(salesRecord, "TELEPHONE").ToString();
                    partner.Fax = SDOHelper.Read(salesRecord, "FAX").ToString();
                    partner.Terms = SDOHelper.Read(salesRecord, "TERMS").ToString();
                    partner.IsSupplier = true;

                    localPartners.Add(partner);
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return Tuple.Create(localPartners, error);
        }

        /// <summary>
        /// Posts the sale order invoice.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSalesInvoice(BusinessObject.InvoiceHeader header)
        {
            InvoicePost invoicePost;
            InvoiceItem invoiceItem;
            SalesRecord salesRecord;

            //Try a connection, will throw an exception if it fails
            try
            {
                //Instantiate objects
                salesRecord = (SalesRecord) this.workSpace.CreateObject("SalesRecord");
                invoicePost = (InvoicePost) this.workSpace.CreateObject("InvoicePost");

                //Set the invoice type
                invoicePost.Type = (InvoiceType) LedgerType.sdoLedgerInvoice;

                //Read the first customer record and use to populate the invoice fields
                salesRecord.MoveFirst();
                SDOHelper.Write(invoicePost.Header, "ACCOUNT_REF", header.TradingPartnerCode);
                SDOHelper.Write(invoicePost.Header, "NAME", header.TradingPartnerName);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_1", header.AddressLine1);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_2", header.AddressLine2);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_3", header.AddressLine3);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_4", header.AddressLine4);
                SDOHelper.Write(invoicePost.Header, "ADDRESS_5", header.PostCode);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_1",header.DeliveryAddressLine1);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_2", header.DeliveryAddressLine2);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_3", header.DeliveryAddressLine3);
                SDOHelper.Write(invoicePost.Header, "DEL_ADDRESS_4", header.DeliveryAddressLine4);
                SDOHelper.Write(invoicePost.Header, "INVOICE_DATE", header.InvoiceDate);
                SDOHelper.Write(invoicePost.Header, "ORDER_NUMBER", header.SaleOrderNo);
                SDOHelper.Write(invoicePost.Header, "CUST_ORDER_NUMBER", header.CustomerOrderNo);
                SDOHelper.Write(invoicePost.Header, "INVOICE_NUMBER", header.InvoiceNo);
                SDOHelper.Write(invoicePost.Header, "NOTES_3", header.DispatchDocketNo);
      
                // If anything is entered in the GLOBAL_NOM_CODE, all of the updated invoice’s splits will have this nominal code and
                // also this willforce anything entered in the GLOBAL_DETAILS field into the all the splits details field. 
                SDOHelper.Write(invoicePost.Header, "GLOBAL_NOM_CODE", "");
                SDOHelper.Write(invoicePost.Header, "GLOBAL_DETAILS", "");
                SDOHelper.Write(invoicePost.Header, "INVOICE_TYPE_CODE",(Byte) InvoiceType.sdoProductInvoice);

                foreach (var detail in header.Details)
                {
                    invoiceItem = (InvoiceItem)SDOHelper.Add(invoicePost.Items);
                    SDOHelper.Write(invoiceItem, "STOCK_CODE", detail.ProductCode);
                    SDOHelper.Write(invoiceItem, "DESCRIPTION", detail.Description);
                    SDOHelper.Write(invoiceItem, "QTY_ORDER", detail.Quantity);
                    SDOHelper.Write(invoiceItem, "UNIT_PRICE", detail.UnitPrice.ToDouble());
                    SDOHelper.Write(invoiceItem, "NET_AMOUNT", detail.TotalExVat.ToDouble());
                    SDOHelper.Write(invoiceItem, "TAX_AMOUNT", detail.Vat.ToDouble());
                    SDOHelper.Write(invoiceItem, "COMMENT_1", "");
                    SDOHelper.Write(invoiceItem, "COMMENT_2", "");
                    SDOHelper.Write(invoiceItem, "UNIT_OF_SALE", detail.UnitOfSale);
                    SDOHelper.Write(invoiceItem, "FULL_NET_AMOUNT", detail.TotalIncVat.ToDouble());
                    SDOHelper.Write(invoiceItem, "TAX_RATE", 0);
                    SDOHelper.Write(invoiceItem, "NOMINAL_CODE", detail.NominalCode);
                }

                //Update the invoice
                if (!invoicePost.Update())
                {
                    return "Error";
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
           
            return string.Empty;
        }

        /// <summary>
        /// Posts the sale order .
        /// </summary>
        /// <param name="header">The sale order data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSaleOrder(BusinessObject.InvoiceHeader header)
        {
            // not implemented
            return "Error";
        }
    }
}

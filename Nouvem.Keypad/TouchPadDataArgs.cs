﻿// -----------------------------------------------------------------------
// <copyright file="TouchPadDataArgs.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Keypad
{
    using System;

    /// <summary>
    /// Event arguments class for the touch pad data..
    /// </summary>
    public class TouchPadDataArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TouchPadDataArgs"/> class.
        /// </summary>
        /// <param name="Data">The weight value to broadcast.</param>
        public TouchPadDataArgs(string data, bool isChecked)
        {
            this.Data = data;
            this.IsChecked = isChecked;
        }

        /// <summary>
        /// Gets or sets the serial data,
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Gets or sets the serial data,
        /// </summary>
        public bool IsChecked { get; set; }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Keypad.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Keypad
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.ComponentModel;

    /// <summary>
    /// Interaction logic for Keypad.xaml
    /// </summary>
    public partial class KeypadSides : Window, INotifyPropertyChanged
    {
        #region event

        /// <summary>
        /// Event used to broadcast the touch pad data.
        /// </summary>
        public event EventHandler<TouchPadDataArgs> DataReceived;

        /// <summary>
        /// Event used to broadcast the touch pad positin data.
        /// </summary>
        public event EventHandler<TouchPadPositionArgs> PositionDataReceived;

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region field

        /// <summary>
        /// The touch pad value.
        /// </summary>
        private string touchPadValue;

        private bool isChecked;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Keypad"/> class.
        /// </summary>
        public KeypadSides(bool isChecked = false, bool showHold = false)
        {
            this.InitializeComponent();
            this.DataContext = this;
            this.isChecked = isChecked;
        
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Keypad"/> class with parameters.
        /// </summary>
        /// <param name="owner">The window owner.</param>
        public KeypadSides(Window owner)
        {
            this.InitializeComponent();
            this.Owner = owner;
            this.DataContext = this;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Keypad"/> class with parameters.
        /// </summary>
        /// <param name="top">The top position.</param>
        /// <param name="left">The left position.</param>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        /// <param name="isScanner">The running on a handheld flag..</param>
        public KeypadSides(double top, double left, double height, double width, bool isScanner, bool isEmulator = false, bool isChecked = false, bool showHold = false, string displayMessage = "")
        {
            this.InitializeComponent();
            this.DataContext = this;
            this.Top = top;
            this.Left = left;
            this.Height = height;
            this.Width = width;
            this.isChecked = isChecked;
  
            this.WindowState = WindowState.Maximized;
            this.TextBoxQuestion.Height = 0;

            if (displayMessage != string.Empty)
            {
                this.TextBoxQuestion.Text = displayMessage;
                this.TextBoxQuestion.Height = 90;
            }

            if (isScanner)
            {
                this.button16.Content = "Ent";
                this.LabelEntry.FontSize = 22;

                if (isEmulator)
                {
                    this.WindowState = WindowState.Normal;
                    this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Keypad"/> class with parameters.
        /// </summary>
        /// <param name="owner">The window owner.</param>
        /// <param name="top">The top position.</param>
        /// <param name="left">The left position.</param>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        /// <param name="isScanner">The running on a handheld flag..</param>
        public KeypadSides(Window owner, double top, double left, double height, double width, bool isScanner, bool isChecked = false, bool showHold = false)
        {
            this.InitializeComponent();
            this.DataContext = this;
            this.Owner = owner;
            this.Top = top;
            this.Left = left;
            this.Height = height;
            this.Width = width;

          

            if (isScanner)
            {
                this.button16.Content = "Ent";
                this.LabelEntry.FontSize = 22;
            }
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the touch pad value.
        /// </summary>
        public string TouchPadValue
        {
            get
            {
                return this.touchPadValue;
            }

            private set
            {
                this.touchPadValue = value;
                this.OnPropertyChanged("TouchPadValue");
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Handles a touch pad entry.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            if (button == null)
            {
                return;
            }

            switch (button.CommandParameter.ToString())
            {
                case "ESC":

                    // Broadcast our cleared data to all interested subscribers.
                    this.OnDataReceived(new TouchPadDataArgs(string.Empty, this.isChecked));
                    this.Close();
                    break;

                case "RETURN":

                    // Broadcast our data to all interested subscribers.
                    this.OnDataReceived(new TouchPadDataArgs(this.TouchPadValue, this.isChecked));

                    // Broadcast our position data to all interested subscribers.
                    this.OnPositionDataReceived(new TouchPadPositionArgs(this.Top, this.Left, this.Width, this.Height));
                    this.Close();
                    break;

                case "BACK":

                    if (!string.IsNullOrEmpty(this.TouchPadValue))
                    {
                        this.TouchPadValue = this.TouchPadValue.Remove(this.TouchPadValue.Length - 1);
                    }

                    break;

                case "-":

                    this.TouchPadValue = string.Format("-{0}", this.touchPadValue);
                    break;

                default:

                    this.TouchPadValue += button.Content.ToString();
                    break;
            }
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="info">The property name.</param>
        private void OnPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Creates a new event, to broadcast the touch pad data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnDataReceived(TouchPadDataArgs e)
        {
            if (this.DataReceived != null)
            {
                this.DataReceived(this, e);
            }
        }

        /// <summary>
        /// Creates a new event, to broadcast the touchpad position data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnPositionDataReceived(TouchPadPositionArgs e)
        {
            if (this.PositionDataReceived != null)
            {
                this.PositionDataReceived(this, e);
            }
        }

        #endregion

        private void buttonCheck_Checked(object sender, RoutedEventArgs e)
        {
            this.isChecked = true;
        }

        private void buttonCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            this.isChecked = false;
        }
    }
}


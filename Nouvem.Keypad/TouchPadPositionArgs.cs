﻿// -----------------------------------------------------------------------
// <copyright file="TouchPadPositionArgs.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Keypad
{
    using System;

    /// <summary>
    /// Event arguments class for the touch pad Position data..
    /// </summary>
    public class TouchPadPositionArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TouchPadPositionArgs"/> class.
        /// </summary>
        /// <param name="top">The top position.</param>
        /// <param name="left">The left position.</param>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        public TouchPadPositionArgs(double top, double left, double height, double width)
        {
            this.Top = top;
            this.Left = left;
            this.Height = height;
            this.Width = width;
        }

        /// <summary>
        /// Gets or sets the top position.
        /// </summary>
        public double Top { get; set; }

        /// <summary>
        /// Gets or sets the left position.
        /// </summary>
        public double Left { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public double Width { get; set; }
    }
}

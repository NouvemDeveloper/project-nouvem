﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.TextBoxPath.Text = @"C:\Nouvem\Barcodes To Upload 2.csv";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(this.TextBoxPath.Text))
            {
                MessageBox.Show("No file at specified path");
                return;
            }

            using (var localFile = new StreamReader(this.TextBoxPath.Text))
            {
                while (!localFile.EndOfStream)
                {
                    var line = localFile.ReadLine();
                    if (line != null)
                    {
                        var data = line.Split(',');
                        var barcode = data.ElementAt(0).ToString();
                        var codeLine = data.ElementAt(1).ToString();

                        var codes = codeLine.Split('-');
                        if (codes.Any())
                        {
                            var code = codes[0].ToLower().Trim();

                            using (var entities = new NouvemTSBloorEntities())
                            {
                                var product =
                                    entities.INMasters.FirstOrDefault(
                                        x => x.Deleted == null && x.Code.ToLower().Trim() == code);
                                if (product != null)
                                {
                                    if (product.ProductLabelFieldID == null)
                                    {
                                        var prodLabelField = new ProductLabelField
                                        {
                                            Barcode1 = barcode.ToString()
                                        };

                                        entities.ProductLabelFields.Add(prodLabelField);
                                        entities.SaveChanges();
                                        product.ProductLabelFieldID = prodLabelField.ProductLabelFieldID;
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        var proLabField =
                                            entities.ProductLabelFields.FirstOrDefault(
                                                x => x.ProductLabelFieldID == product.ProductLabelFieldID);
                                        if (proLabField != null)
                                        {
                                            proLabField.Barcode1 = barcode.ToString();
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            MessageBox.Show("Import sucessful");
        }
    }
}

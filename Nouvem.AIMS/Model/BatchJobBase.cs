﻿// -----------------------------------------------------------------------
// <copyright file="BatchJobBase.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    /// <summary>
    /// Class which models the common attributes of the batchJob node.
    /// </summary>
    public class BatchJobBase
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchJobBase"/> class.
        /// </summary>
        protected BatchJobBase()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets or sets the date on which the request will be sent.
        /// </summary>
        public string DateGen { get; set; }

        /// <summary>
        /// Gets or sets the name of the file in which the request will be stored.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the number of transactions which the xml request will have.
        /// </summary>
        public string NumTrans { get; set; }

        #endregion
    }
}


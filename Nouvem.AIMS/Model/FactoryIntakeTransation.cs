﻿// -----------------------------------------------------------------------
// <copyright file="FactoryIntakeTransation.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.AIMS.BusinessLogic;

namespace Nouvem.AIMS.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// Class which model the batchTrans elements in the request. 
    /// </summary>
    public class FactoryIntakeTransation : BatchTransBase
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FactoryIntakeTransation"/> class.
        /// </summary>
        public FactoryIntakeTransation()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets or sets the movement request to be submitted.
        /// </summary>
        public IList<MovementRequest> MovementRequest { get; set; }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="MovementHistoryResponse.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.AIMS.Model
{
    /// <summary>
    /// Class which models an animal movement tag.
    /// </summary>
    public class MovementTag
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MovementTag"/> class.
        /// </summary>
        public MovementTag()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the the tag number.
        /// </summary>
        public string TagNum { get; set; }

        /// <summary>
        /// Gets or sets the animal action, for example 'I'.
        /// </summary>
        public string AnimalAction { get; set; }

        /// <summary>
        /// The recorded carcass primary key
        /// </summary>
        public string SoftwareAnimalId { get; set; }

        /// <summary>
        /// The recorded carcass primary key
        /// </summary>
        public string ScannedIndicator { get; set; }

        /// <summary>
        /// Gets or sets the the tag number.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the the tag number.
        /// </summary>
        public string Cleanliness { get; set; }

        /// <summary>
        /// Gets or sets the the tag number.
        /// </summary>
        public string Location { get; set; }

        #endregion
    }
}

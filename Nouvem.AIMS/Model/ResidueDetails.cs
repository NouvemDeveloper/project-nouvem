﻿// -----------------------------------------------------------------------
// <copyright file="ResidueDetails.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class that holds the residue details data results stored in the AIMS response file
    /// </summary>
    public class ResidueDetails
    {
        /// <summary>
        /// The residue code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The associated value
        /// </summary>
        public string Value { get; set; }
    }
}


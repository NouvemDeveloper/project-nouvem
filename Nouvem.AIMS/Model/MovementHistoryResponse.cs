﻿// -----------------------------------------------------------------------
// <copyright file="MovementHistoryResponse.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    using System.Collections.Generic;
    using Nouvem.AIMS.Model;

    /// <summary>
    /// Class that records an animals previous movements as what appear in the AIMS response file 
    /// to a herd movement request.
    /// </summary>
    public class MovementHistoryResponse
    {

        #region properties

        /// <summary>
        /// The animals tag number
        /// </summary>
        public string TagNum { get; set; }

        /// <summary>
        /// The animals herd of origin
        /// </summary>
        public string HerdOfOrigin { get; set; }

        /// <summary>
        /// Collection that holds all the movements recorded in the AIMS response file
        /// </summary>
        public List<Movement> Movements { get; set; }

        #endregion

        #region methods

        /// <summary>
        /// method that adds a Movement object to our list
        /// </summary>
        /// <param name="moveFromId"></param>
        /// <param name="moveToId"></param>
        /// <param name="movementDate"></param>
        public void AddMovements(string moveFromId, string moveToId, string movementDate)
        {
            Movements.Add(new Movement { MoveFromID = moveFromId, MoveToID = moveToId, MovementDate = movementDate });

        }

        #endregion
    }
}

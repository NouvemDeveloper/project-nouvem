﻿// -----------------------------------------------------------------------
// <copyright file="MovementHistoryResponse.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Nouvem.AIMS.Service;
using Nouvem.Shared;

namespace Nouvem.AIMS.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class that records the move tag details of an animal whose details are returned
    /// in an AIMS response to a movement request. (Multiple move tag responses can be returned in a movement response file)
    /// </summary>
    public class MoveTagResponse
    {

        #region properties

        public string TagNum { get; set; }
        public string DOB { get; set; }
        public string Breed { get; set; }
        public string Sex { get; set; }
        public string HerdOfOrigin { get; set; }
        public string DamEvent { get; set; }
        public string TagNumData { get; set; }
        public string TseTestInd { get; set; }
        public string MovementAnimalId { get; set; }
        public string SoftwareAnimalId { get; set; }
        public string AnimalStatus { get; set; }
        public string CurrentLocation { get; set; }
        public string Error { get; set; }

        public string AimsError
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Error))
                {
                    return Errors.GetErrors()
                               .Where(x => x.Key == this.Error.RemoveNonIntegers().ToString())
                               .Select(x => x.Value).FirstOrDefault();
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Each move tag response can contain multiple MoveTagTestData elements which need
        /// to be stored.
        /// </summary>
        public List<MoveTagTestData> TagTestData { get; set; }

        /// <summary> 
        /// Each move tag response will contain 1 movement history response element which needs
        /// to be stored.
        /// </summary>
        public MovementHistoryResponse MovementHistory { get; set; }

        #endregion

        #region methods

        /// <summary>
        /// method that adds a MoveTestData object to our list
        /// </summary>
        /// <param name="testTypeCode"></param>
        /// <param name="testDate"></param>
        public void AddMoveTagTestData(string testTypeCode, string testDate)
        {
            TagTestData.Add(new MoveTagTestData { TestTypeCode = testTypeCode, TestDate = testDate });
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="HerdDesignator.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.BusinessLogic
{
    /// <summary>
    /// Class which models an AIMs herd designator request and response.
    /// </summary>
    public class HerdDesignator
    {
        #region Public Interface

        /// <summary>
        /// Gets or sets the name of the file in which the request will be stored.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the date on which the request will be sent.
        /// </summary>
        public string DateGen { get; set; }

        /// <summary>
        /// Gets or sets the number of transactions which the xml request will have.
        /// </summary>
        public string NumTrans { get; set; }

        /// <summary>
        /// Gets or sets the transaction type. MOV is a movement transaction request, for example.
        /// </summary>
        public string TransType { get; set; }

        /// <summary>
        /// Gets or sets the species of the animal for which the request is being processed. 
        /// </summary>
        public string SpeciesId { get; set; }

        /// <summary>
        /// Gets or sets the status which the herd request which is being processed. 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the business id or herd number.
        /// </summary>
        public string BusinessId { get; set; }

        /// <summary>
        /// Gets or sets the region identifier.
        /// </summary>
        public string RegionId { get; set; }

        /// <summary>
        /// Gets or sets the region identifier for the herd.
        /// </summary>
        public string HerdWithinRegionId { get; set; }

        /// <summary>
        /// Gets or sets the error message relating to a request.
        /// </summary>
        public string Error { get; set; }

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="HerdDesignator"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="HerdDesignator"/> object.</returns>
        public static HerdDesignator CreateNewHerdDesignator()
        {
            return new HerdDesignator();
        }

        /// <summary>
        /// Create a new <see cref="HerdDesignator"/> with specific parameters.
        /// </summary>
        /// <param name="fileName">The name of the file in which the request will be stored.</param>
        /// <param name="dateGen">The date on which the request will be sent.</param>
        /// <param name="numTrans">The number of transactions which the xml request will have.</param>
        /// <param name="transType">The transaction type. MOV is a movement transaction request, for example.</param>
        /// <param name="speciesId">The species of the animal for which the request is being processed.</param>
        /// <param name="businessId">The business id or herd number.</param>
        /// <param name="status">The status of the herd according to aims.</param>
        /// <param name="error">Any error relating to the transaction</param>
        /// <param name="regionId">The region identifier.</param>
        /// <param name="herdWithinRegionId">The region identifier for the herd.</param>
        /// <returns>A new <see cref="HerdDesignator"/> object with the parameters specified.</returns>
        public static HerdDesignator CreateHerdDesignator(
            string fileName,
            string dateGen,
            string numTrans,
            string transType,
            string speciesId,
            string businessId,
            string status = null,
            string error = null,
            string regionId = null,
            string herdWithinRegionId = null)
        {
            return new HerdDesignator
            {
                FileName = fileName,
                DateGen = dateGen,
                NumTrans = numTrans,
                TransType = transType,
                SpeciesId = speciesId,
                BusinessId = businessId,
                Status = status ?? string.Empty,
                Error = error ?? string.Empty,
                RegionId = regionId ?? string.Empty,
                HerdWithinRegionId = herdWithinRegionId ?? string.Empty
            };
        }

        #endregion

        #endregion
    }
}


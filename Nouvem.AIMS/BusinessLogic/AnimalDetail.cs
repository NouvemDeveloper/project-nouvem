﻿// -----------------------------------------------------------------------
// <copyright file="AnimalDetail.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.BusinessLogic
{
    using System.Collections.Generic;
    using Nouvem.AIMS.Model;

    /// <summary>
    /// Class which models an AIMs animal designator request and response.
    /// </summary>
    public class AnimalDetail : BatchJobBase
    {
        #region Public Properties

        // Gets or sets a transaction type
        public string TransType { get; set; }

        // Gets or sets a species id
        public string SpeciesID { get; set; }

        // Gets or sets a tag number
        public string TagNum { get; set; }

        // Gets or sets a status value
        public string Status { get; set; }

        // Gets or sets the lot number
        public string LotNum { get; set; }

        // Gets or sets the lot number
        public string UserName { get; set; }

        // Gets or sets the lot number
        public string UserPassword { get; set; }

        // Gets or sets the inwardMovementId
        public string InwardMovementRefID { get; set; }

        // Gets or sets the date of birth
        public string DOB { get; set; }

        // Gets or sets the breed
        public string Breed { get; set; }

        // Gets or sets the dam date of birth
        public string DamDOB { get; set; }

        // Gets or sets the dam breed
        public string DamBreed { get; set; }

        // Gets or sets the sex
        public string Sex { get; set; }

        // Gets or sets the dam event
        public string DamEvent { get; set; }

        // Gets or sets the dam tag number 
        public string TagNumDam { get; set; }

        // Gets or sets the tse test ind
        public string TseTestInd { get; set; }

        // Gets or sets the movement id
        public string MovementAnimalID { get; set; }

        // Gets or sets the test type code
        public string TestTypeCode { get; set; }

        // Gets or sets the test date
        public string TestDate { get; set; }

        // Gets or sets the herd of origin
        public string HerdOfOrigin { get; set; }

        // Gets or sets the moved from id
        public string MovedFromID { get; set; }

        // Gets or sets the moved to Id
        public string MovedToID { get; set; }

        // Gets or sets the movement date
        public string MovementDate { get; set; }

        // Gets or sets the second moved from id
        public string MovedFromID2 { get; set; }

        // Gets or sets the moved to id
        public string MovedToID2 { get; set; }

        // Gets or sets the movemenet date
        public string MovementDate2 { get; set; }

        /// <summary>
        /// Gets or sets the error message relating to a request.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// 
        /// Gets or sets the collection of movements associated with each animal.
        /// </summary>
        public List<Movement> AnimalMovements { get; set; }

        /// <summary>
        /// Gets or sets the collection of move tag test data associated with each animal
        /// </summary>
        public List<MoveTagTestData> TagTestData { get; set; }

        #endregion

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="AnimalDetail"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="AnimalDetail"/> object.</returns>
        public static AnimalDetail CreateNewAnimalDetails()
        {
            return new AnimalDetail();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dateGen"></param>
        /// <param name="numTrans"></param>
        /// <param name="transType"></param>
        /// <param name="speciesId"></param>
        /// <param name="tagNum"></param>
        /// <param name="status"></param>
        /// <param name="lotNum"></param>
        /// <param name="inwardMovementRefID"></param>
        /// <param name="dOB"></param>
        /// <param name="breed"></param>
        /// <param name="sex"></param>
        /// <param name="damEvent"></param>
        /// <param name="tagNumDam"></param>
        /// <param name="tseTestInd"></param>
        /// <param name="movementAnimalID"></param>
        /// <param name="testTypeCode"></param>
        /// <param name="testDate"></param>
        /// <param name="herdOfOrigon"></param>
        /// <param name="movedFromID"></param>
        /// <param name="movedToID"></param>
        /// <param name="movementDate"></param>
        /// <param name="movedFromID2"></param>
        /// <param name="movedToID2"></param>
        /// <param name="movementDate2"></param>
        /// <returns></returns>
        public static AnimalDetail CreateAnimalDetails(
            string fileName,
            string dateGen,
            string numTrans,
            string transType,
            string speciesId,
            string tagNum,
            string error = null,
        string status = null,
        string lotNum = null,
        string inwardMovementRefID = null,
        string dOB = null,
        string breed = null,
        string sex = null,
        string damEvent = null,
        string tagNumDam = null,
        string tseTestInd = null,
        string movementAnimalID = null,
        string damBreed = null,
        string damDOB = null,
        string testTypeCode = null,
        string testDate = null,
        string herdOfOrigon = null,
        string movedFromID = null,
        string movedToID = null,
        string movementDate = null,
        string movedFromID2 = null,
        string movedToID2 = null,
        string movementDate2 = null,
        string userName = "",
        string userPassword = ""
        )
        {
            return new AnimalDetail
            {
                FileName = fileName,
                DateGen = dateGen,
                NumTrans = numTrans,
                TransType = transType,
                SpeciesID = speciesId,
                TagNum = tagNum,
                Error = error ?? string.Empty,
                Status = status ?? string.Empty,
                LotNum = lotNum ?? string.Empty,
                InwardMovementRefID = inwardMovementRefID ?? string.Empty,
                DOB = dOB ?? string.Empty,
                Breed = breed ?? string.Empty,
                Sex = sex ?? string.Empty,
                DamEvent = damEvent ?? string.Empty,
                TagNumDam = tagNumDam ?? string.Empty,
                TseTestInd = tseTestInd ?? string.Empty,
                MovementAnimalID = movementAnimalID ?? string.Empty,
                DamBreed = damBreed ?? string.Empty,
                DamDOB = damDOB ?? string.Empty,
                TestTypeCode = testTypeCode ?? string.Empty,
                TestDate = testDate ?? string.Empty,
                HerdOfOrigin = herdOfOrigon ?? string.Empty,
                MovedFromID = movedFromID ?? string.Empty,
                MovedToID = movedToID ?? string.Empty,
                MovementDate = movementDate ?? string.Empty,
                MovedFromID2 = movedFromID2 ?? string.Empty,
                MovedToID2 = movedToID2 ?? string.Empty,
                MovementDate2 = movementDate2 ?? string.Empty,
                UserName = userName,
                UserPassword = userPassword,

                AnimalMovements = new List<Movement>(),
                TagTestData = new List<MoveTagTestData>()

            };
        }

        #region public interface

        /// <summary>
        /// Method that adds an animals previous movement to the list.
        /// </summary>
        /// <param name="moveFromID"></param>
        /// <param name="moveToID"></param>
        /// <param name="movementDate"></param>
        public void AddMovements(string moveFromID, string moveToID, string movementDate)
        {
            AnimalMovements.Add(new Movement { MoveFromID = moveFromID, MoveToID = moveToID, MovementDate = movementDate });

        }

        /// <summary>
        /// Method that adds an animals move tag test data to the list
        /// </summary>
        /// <param name="testTypeCode"></param>
        /// <param name="testDate"></param>
        public void AddMoveTagTestData(string testTypeCode, string testDate)
        {
            TagTestData.Add(new MoveTagTestData { TestTypeCode = testTypeCode, TestDate = testDate });

        }


        #endregion

        #endregion
    }
}


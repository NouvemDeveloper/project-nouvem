﻿// -----------------------------------------------------------------------
// <copyright file="FactoryPriceDetail.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class which models an AIMs factory price details request and response.
    /// </summary>
    public class FactoryPriceDetail
    {
        /// <summary>
        /// Gets or sets the name of the file in which the request will be stored.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the date on which the request will be sent.
        /// </summary>
        public string DateGen { get; set; }

        /// <summary>
        /// Gets or sets the number of transactions which the xml request will have.
        /// </summary>
        public string NumTrans { get; set; }

        /// <summary>
        /// Gets or sets the transaction type. MOV is a movement transaction request, for example.
        /// </summary>
        public string TransType { get; set; }

        /// <summary>
        /// Gets or sets the species id.
        /// </summary>
        public string SpeciesID { get; set; }

        /// <summary>
        /// Gets or sets the officer code.
        /// </summary>
        public string OfficerCode { get; set; }

        /// <summary>
        /// Gets or sets the carcass number.
        /// </summary>
        public string CarcassNumber { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the conformation.
        /// </summary>
        public string Conformation { get; set; }

        /// <summary>
        /// Gets or sets the fat score.
        /// </summary>
        public string fat { get; set; }

        /// <summary>
        /// Gets or sets the side 1 weight.
        /// </summary>
        public string WeightSide1 { get; set; }

        /// <summary>
        /// Gets or sets the side 2 weight.
        /// </summary>
        public string WeightSide2 { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string TagNumber { get; set; }

        /// <summary>
        /// Gets or sets the carcass description.
        /// </summary>
        public string CarcassDescription { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// Gets or sets the factory ownership id.
        /// </summary>
        public string FactoryOwnershipID { get; set; }

        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Create a new empty <see cref="FactoryPriceDetail"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="FactoryPriceDetail"/> object.</returns>
        public static FactoryPriceDetail CreateNewfactoryPriceDetail()
        {
            return new FactoryPriceDetail(); ;
        }

        /// <summary>
        /// Create a new empty <see cref="FactoryPriceDetail"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="FactoryPriceDetail"/> object.</returns>
        public static FactoryPriceDetail CreatefactoryPriceDetail(
            string fileName,
            string dateGen,
            string numTrans,
            string transType,
            string speciesID,
            string officerCode,
            string carcassNumber,
            string category,
            string conformation,
            string fat,
            string weightside1,
            string weightSide2,
            string tagNumber,
            string carcassDescription,
            string price,
            string factoryOwnershipID,
            string status = "",
            string error = "")
        {
            return new FactoryPriceDetail
            {
                FileName = fileName,
                DateGen = dateGen,
                NumTrans = numTrans,
                TransType = transType,
                SpeciesID = speciesID,
                OfficerCode = officerCode,
                CarcassNumber = carcassNumber,
                Category = category,
                Conformation = conformation,
                fat = fat,
                WeightSide1 = weightside1,
                WeightSide2 = weightSide2,
                TagNumber = tagNumber,
                CarcassDescription = carcassDescription,
                Price = price,
                FactoryOwnershipID = factoryOwnershipID,
                Status = status,
                Error = error
            };
        }
    }
}


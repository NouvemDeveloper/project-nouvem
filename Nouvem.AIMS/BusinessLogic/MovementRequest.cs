﻿// -----------------------------------------------------------------------
// <copyright file="FactoryIntakeTransation.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using Nouvem.AIMS.Model;

    /// <summary>
    /// Class which models the movement request to be submitted.
    /// </summary>
    public class MovementRequest : BatchJobBase
    {
        #region fields

        /// <summary>
        /// The list of attached residue details objects
        /// </summary>
        private List<ResidueDetails> residueDetails;

        /// <summary>
        /// The list of country elibibility objects.
        /// </summary>
        private List<CountryEligibility> countryEligibility = new List<CountryEligibility>();

        /// <summary>
        /// The list of attached movement tag responses objects.
        /// </summary>
        private List<MoveTagResponse> movementTagResponse;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MovementRequest"/> class.
        /// </summary>
        public static MovementRequest CreateNewMovementRequest()
        {
            return new MovementRequest();

        }

        /// <summary>
        /// Intiialises a new instance of movementRequest with parameters
        /// </summary>
        /// <param name="fileName">the file name</param>
        /// <param name="dateGen">the date of generation</param>
        /// <param name="numTrans">the transaction number</param>
        /// <param name="senderId">the sender id</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="status">the status</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="lotNum">the lot number</param>
        /// <param name="inwardMovementRefId">the inward movement reference id</param>
        /// <param name="softwareId">the software id</param>
        /// <param name="headerStatus">the header status</param>
        /// <param name="numTags">the number of tags</param>
        /// <param name="countryCode">the comma delimited country codes string</param>
        /// <param name="countryCodeValue"></param>
        /// <param name="residueDeatils">the list of residue details</param>
        /// <param name="movementTag">the list of movement tags</param>
        /// <param name="movementTagResponse">the list of movement tag responses</param>
        /// <returns>a movement request object</returns>
        public static MovementRequest CreateMovementRequest(string fileName, string dateGen, string numTrans, string senderId,
                                                            string transType, string speciesId, string moveFromId, string moveDate,
                                                            string lotNum, string softwareId, string countryCode, string numTags,
                                                            string lotAction, string status = null,
                                                            string inwardMovementRefId = null,
                                                            string headerStatus = null,
                                                            List<ResidueDetails> residueDetails = null,
                                                            List<MovementTag> movementTag = null,
                                                            List<MoveTagResponse> movementTagResponse = null,
                                                            string userName = "",
                                                            string password = ""
                                                            )
        {
            {
                return new MovementRequest
                {
                    FileName = fileName,
                    DateGen = dateGen,
                    NumTrans = numTrans,
                    SenderId = senderId,
                    TransType = transType,
                    SpeciesId = speciesId,
                    MovedFromId = moveFromId,
                    MoveDate = moveDate,
                    LotNum = lotNum,
                    SoftwareId = softwareId,
                    CountryCode = countryCode,
                    NumTags = numTags,
                    LotAction = lotAction,
                    MovementTag = movementTag,
                    Status = status ?? string.Empty,
                    InwardMovementRefId = inwardMovementRefId ?? string.Empty,
                    HeaderStatus = headerStatus ?? string.Empty,
                    residueDetails = new List<ResidueDetails>(),
                    movementTagResponse = new List<MoveTagResponse>(),
                    UserName = userName,
                    UserPassword = password
                };
            }
        }

        /// <summary>
        /// Intiialises a new instance of movementRequest with parameters
        /// </summary>
        /// <param name="fileName">the file name</param>
        /// <param name="dateGen">the date of generation</param>
        /// <param name="numTrans">the transaction number</param>
        /// <param name="senderId">the sender id</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="status">the status</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="lotNum">the lot number</param>
        /// <param name="inwardMovementRefId">the inward movement reference id</param>
        /// <param name="softwareId">the software id</param>
        /// <param name="headerStatus">the header status</param>
        /// <param name="numTags">the number of tags</param>
        /// <param name="countryCode">the comma delimited country codes string</param>
        /// <param name="countryCodeValue"></param>
        /// <param name="residueDeatils">the list of residue details</param>
        /// <param name="movementTag">the list of movement tags</param>
        /// <param name="movementTagResponse">the list of movement tag responses</param>
        /// <returns>a movement request object</returns>
        public static MovementRequest CreateSheepMovementRequest(string fileName, string dateGen, string numTrans, string senderId,
                                                            string transType, string speciesId, string moveFromId, string moveDate,
                                                            string lotNum, string softwareId, string numTags,
                                                            string lotAction, string status = null,
                                                            string inwardMovementRefId = null,
                                                            string headerStatus = null,
                                                            List<ResidueDetails> residueDetails = null,
                                                            List<MovementTag> movementTag = null,
                                                            List<MoveTagResponse> movementTagResponse = null, 
                                                            string permitNo = "", 
                                                            int numOfTagsDead = 0, 
                                                            int numOfTagsC = 0, 
                                                            int numOfTagsB = 0
                                                            )
        {
            {
                return new MovementRequest
                {
                    FileName = fileName,
                    DateGen = dateGen,
                    NumTrans = numTrans,
                    SenderId = senderId,
                    TransType = transType,
                    SpeciesId = speciesId,
                    MovedFromId = moveFromId,
                    MoveDate = moveDate,
                    LotNum = lotNum,
                    SoftwareId = softwareId,
                    NumTags = numTags,
                    LotAction = lotAction,
                    MovementTag = movementTag,
                    Status = status ?? string.Empty,
                    InwardMovementRefId = inwardMovementRefId ?? string.Empty,
                    HeaderStatus = headerStatus ?? string.Empty,
                    residueDetails = new List<ResidueDetails>(),
                    movementTagResponse = new List<MoveTagResponse>(),
                    PermitNo = permitNo,
                    NumTagsDead = numOfTagsDead,
                    NumTagsB = numOfTagsB,
                    NumTagsC = numOfTagsC
                };
            }
        }

        #endregion

        #region Public Interface

        public string SenderId { get; set; }
        /// <summary>
        /// Gets or sets the species which is being queried.
        /// </summary>
        public string SpeciesId { get; set; }

        /// <summary>
        /// Gets or sets the id from which the animal is being moved.
        /// </summary>
        public string MovedFromId { get; set; }

        /// <summary>
        /// Gets or sets the date on which the animal is being moved.
        /// </summary>
        public string MoveDate { get; set; }

        /// <summary>
        /// Gets or sets the lot number associated with the animal.
        /// </summary>
        public string LotNum { get; set; }

        public string UserName { get; set; }

        public string UserPassword { get; set; }

        /// <summary>
        /// Gets or sets the software id associated with the request.
        /// </summary>
        public string SoftwareId { get; set; }

        /// <summary>
        /// Gets or sets the software id associated with the request.
        /// </summary>
        public string PermitNo { get; set; }

        /// <summary>
        /// Gets or sets the country code delimited string for the request.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the numbers of tags for the request.
        /// </summary>
        public string NumTags { get; set; }

        /// <summary>
        /// Gets or sets the numbers of tags for the request.
        /// </summary>
        public int NumTagsDead { get; set; }

        /// <summary>
        /// Gets or sets the numbers of tags for the request.
        /// </summary>
        public int NumTagsB { get; set; }

        /// <summary>
        /// Gets or sets the numbers of tags for the request.
        /// </summary>
        public int NumTagsC { get; set; }

        /// <summary>
        /// Gets or sets the lot action for the request.
        /// </summary>
        public string LotAction { get; set; }

        /// <summary>
        /// Gets or sets the trans type
        /// </summary>
        public string TransType { get; set; }

        /// <summary>
        /// Gets or sets the errors
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the inwardmovement ref id
        /// </summary>
        public string InwardMovementRefId { get; set; }

        /// <summary>
        /// Gets or sets the header status.
        /// </summary>
        public string HeaderStatus { get; set; }

        /// <summary>
        /// Gets or sets the header status.
        /// </summary>
        public IList<Tuple<string,string>> ValidationErrors { get; set; }

        /// <summary>
        /// Gets or sets the residue details for the request
        /// </summary>
        public List<ResidueDetails> ResidueDetails
        {
            get
            {
                return residueDetails;
            }
        }

        /// <summary>
        /// Gets the country eligibility collection.
        /// </summary>
        public List<CountryEligibility> CountryEligibility
        {
            get
            {
                return this.countryEligibility;
            }
        }

        /// <summary>
        /// Gets or sets the movement tag details for the request.
        /// </summary>
        public List<MovementTag> MovementTag { get; set; }

        public List<MoveTagResponse> MovementTagResponse
        {
            get { return movementTagResponse; }

        }

        #endregion

        #region methods

        /// <summary>
        /// method that adds a new MovementTag object to our list
        /// </summary>
        /// <param name="tagNum"></param>
        /// <param name="animalAction"></param>
        public void AddMovementTag(string tagNum, string animalAction, string softwareAnimalId)
        {

            MovementTag.Add(new MovementTag { TagNum = tagNum, AnimalAction = animalAction, SoftwareAnimalId = softwareAnimalId });
        }

        /// <summary>
        /// Method that adds a MoveTagResponse
        /// </summary>
        /// <param name="tagNum">the tag number</param>
        /// <param name="dob">the animal date of birth</param>
        /// <param name="breed">the animal breed</param>
        /// <param name="sex">the animal sex</param>
        /// <param name="herdOfOrigin">The herd of origin</param>
        /// <param name="damEvent">the dam event</param>
        /// <param name="tagNumData">the tag number data</param>
        /// <param name="tseTestInd"the test ind></param>
        /// <param name="movementAnimalId">the movement animal id</param>
        /// <param name="softwareAnimalId">the software animal id</param>
        /// <param name="animalStatus">the animal status</param>
        /// <param name="currentLocation">the current location</param>
        /// <param name="moveTagTestData">the list of move tag data</param>
        /// <param name="movementHistoryResponse">the list of movement history</param>
        public void AddMoveTagResponse(string tagNum, string dob, string breed, string sex, string herdOfOrigin, string damEvent, string tagNumData, string tseTestInd,
                                       string movementAnimalId, string softwareAnimalId, string animalStatus, string currentLocation, List<MoveTagTestData> moveTagTestData,
                                       MovementHistoryResponse movementHistoryResponse, string error = "")
        {
            try
            {
                movementTagResponse.Add(new MoveTagResponse
                {
                    TagNum = tagNum ?? string.Empty,
                    DOB = dob ?? string.Empty,
                    Breed = breed ?? string.Empty,
                    Sex = sex ?? string.Empty,
                    HerdOfOrigin = herdOfOrigin ?? string.Empty,
                    DamEvent = damEvent ?? string.Empty,
                    TagNumData = tagNumData ?? string.Empty,
                    TseTestInd = tseTestInd ?? string.Empty,
                    MovementAnimalId = movementAnimalId ?? string.Empty,
                    SoftwareAnimalId = softwareAnimalId ?? string.Empty,
                    AnimalStatus = animalStatus ?? string.Empty,
                    CurrentLocation = currentLocation ?? string.Empty,
                    TagTestData = moveTagTestData,
                    MovementHistory = movementHistoryResponse,
                    Error = error
                });
            }
            catch (Exception ex)
            {
                Error += ex.ToString();
            }
        }

        /// <summary>
        /// Method that adds a residue deatils object to our collection
        /// </summary>
        /// <param name="code">the code</param>
        /// <param name="value">the value</param>
        public void AddResidueDetails(string code, string value)
        {
            try
            {
                residueDetails.Add(new ResidueDetails { Code = code ?? string.Empty, Value = value ?? string.Empty });
            }
            catch (Exception ex)
            {
                Error += ex.ToString();
            }
        }

        /// <summary>
        /// Method that adds a country eligibility object to our collection
        /// </summary>
        /// <param name="countryCode">the country code</param>
        /// <param name="value">the value</param>
        public void AddCountryEligibility(string countryCode, string value)
        {
            try
            {
                countryEligibility.Add(new CountryEligibility { CountryCode = countryCode ?? string.Empty, Value = value ?? string.Empty });
            }
            catch (Exception ex)
            {
                Error += ex.ToString();
            }
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="AnimalDetail.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading;
using System.Web.Services.Protocols;
using Nouvem.AIMS.Model;
using Nouvem.AIMS.Service;
using Nouvem.AIMS.SheepAssignBatchNo;
using Nouvem.AIMS.SheepToAbbatoir;
using Nouvem.AIMS.SheepToLairage;
using Nouvem.AIMS.uk.gov.dardni.reference;
using Nouvem.Shared;
using SheepBatches = Nouvem.AIMS.SheepToLairage.SheepBatches;

namespace Nouvem.AIMS.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
  
    using System.Net;

    /// <summary>
    /// Wrapper class for the web service calls.
    /// </summary>
    public class WSWrapper
    {
        #region Fields

        /// <summary>
        /// Instance of the service caller used to call the AIMs service.
        /// </summary>
        private Service.WebClient webClient;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WSWrapper"/> class.
        /// </summary>
        public WSWrapper()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the service caller used to check if a web address is available.
        /// </summary>
        public Service.WebClient WebClient
        {
            get
            {
                if (this.webClient == null)
                {
                    var url = String.Empty; //Settings.Default.TestUrl ?? string.Empty;
                    this.webClient = new Service.WebClient(url);
                }

                return this.webClient;
            }

            set
            {
                this.webClient = value;
            }
        }

        #endregion

        #region Public Interface

        #region Herd Designator

        /// <summary>
        /// Submit an herd designator request to the AIMs service.
        /// </summary>
        /// <param name="fileName">The name of the file in which the request will be stored.</param>
        /// <param name="dateGen">The date on which the request will be sent.</param>
        /// <param name="numTrans">The number of transactions which the xml request will have.</param>
        /// <param name="transType">The transaction type. MOV is a movement transaction request, for example.</param>
        /// <param name="speciesId">The species of the animal for which the request is being processed.</param>
        /// <param name="herdNo">The business id or herd number.</param>
        /// <param name="serviceUrl">The url of the service.</param>
        /// <param name="certPath">The path at which the security cert is stored.</param>
        /// <param name="password">The password needed to unlock the certificate used in communicating to AIM.</param>
        /// <param name="canSave">Flag to indicate if the file is to be saved to disk.</param>
        /// <param name="logPath">The path at which the file is to be saved.</param>
        /// <returns>An object describing the response from the request submitted.</returns>
        public HerdDesignator QueryHerdDesignator(string fileName, string dateGen, string numTrans, string transType, string speciesId, string herdNo, string serviceUrl, string certPath, string password, bool canSave = false, string logPath = "")
        {
            var designator = HerdDesignator.CreateNewHerdDesignator();
         
            try
            {
                // Create the designator based on the parameters passed.
                designator = HerdDesignator.CreateHerdDesignator(fileName, dateGen, numTrans, transType, speciesId, herdNo);

                // Create the xml request to be sent.
                var helper = new HerdDesignatorXmlHelper(designator);
                var xmlRequest = helper.CreateXmlRequest(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest.ToString()))
                {
                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) || !string.IsNullOrEmpty(password))
                    {
                        var client = new Service.WebClient(serviceUrl);

                        System.Net.ServicePointManager.Expect100Continue = true;
                        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                        // Load the security cert.
                        var securityCert = client.FindSecurityCert(certPath, password);

                        // Post the transaction to the service.
                        var serviceResponse = client.PostXMLTransaction(xmlRequest, securityCert, timeout: 20000);

                        // Extract the designator object from the response.
                        designator = helper.ParseXmlResponse(serviceResponse.InnerXml, fileName, canSave, logPath);
                    }
                    else
                    {
                        throw new ApplicationException("One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception queryHerdDesignatorEx)
            {
                designator.Error += queryHerdDesignatorEx.ToString();
            }

            return designator;
        }

        #endregion

        #region animal details

        /// <summary>
        /// Creates an md5 hash from the input string.
        /// </summary>
        /// <param name="input">The string to hash.</param>
        /// <returns>A md5 hash of the input string.</returns>
        public static string CreateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            // Convert byte array to hex string
            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// The aims animal details request
        /// </summary>
        /// <param name="fileName">the xml file name</param>
        /// <param name="dateGen">the date generated</param>
        /// <param name="numTrans">the number of transactions</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="tagNum">the ear tag number</param>
        /// <returns>a new animal detail response</returns>
        public AnimalDetail QueryUKAnimalDetails(
            string fileName, 
            string dateGen, 
            string numTrans, 
            string transType, 
            string speciesId, 
            string tagNum, 
            string serviceUrl, 
            string certPath, 
            string password, 
            bool canSave = false, 
            string logPath = "",
            string userName = "",
            string userPassword = "")
        {
            var details = AnimalDetail.CreateNewAnimalDetails();

            try
            {
                // Create the designator based on the parameters passed.
                details = AnimalDetail.CreateAnimalDetails(fileName, dateGen, numTrans, transType, speciesId, tagNum,userName:userName, userPassword:userPassword);

                // Create the xml request to be sent.
                var helper = new AnimalDetailsXmlHelper(details);
                var xmlRequest = helper.CreateXmlRequestBCMS(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest))
                {
                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) || !string.IsNullOrEmpty(password))
                    {
                        var data = new uk.gov.defra.ddts.secure.webservice.t1.DefraDataTransferPublicNWSE();
                        data.Url = serviceUrl;
                   
                        var toEncodeAsBytes = Encoding.UTF8.GetBytes(xmlRequest);
                        var encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
                        var md5Password = CreateMd5Hash("Sn0w5how3rs");
                        var moveResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS", encodedAs64, "Get_Animal_Details-V1-0");
                        
                        var base64EncodedBytes = System.Convert.FromBase64String(moveResponse);
                        var xmlResponse = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                        if (xmlResponse.StartsWithIgnoringCase("CTWS System Exception"))
                        {
                            throw new ApplicationException(xmlResponse);
                        }

                        details = helper.ParseXmlBCMSResponse(xmlResponse,logPath:logPath,fileName:fileName, canSave:true);
                    }
                    else
                    {
                        throw new ApplicationException("One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception ex)
            {
                details.Error += ex.ToString();
            }

            return details;
        }


        /// <summary>
        /// The aims animal details request
        /// </summary>
        /// <param name="fileName">the xml file name</param>
        /// <param name="dateGen">the date generated</param>
        /// <param name="numTrans">the number of transactions</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="tagNum">the ear tag number</param>
        /// <returns>a new animal detail response</returns>
        public AnimalDetail QueryNIAnimalDetails(
            string fileName,
            string dateGen,
            string numTrans,
            string transType,
            string speciesId,
            string tagNum,
            string serviceUrl,
            string certPath,
            string password,
            bool canSave = false,
            string logPath = "",
            string userName = "",
            string userPassword = "")
        {
            var details = AnimalDetail.CreateNewAnimalDetails();

            try
            {
                var loginService = new uk.gov.dardni.reference1.Gateway();
                loginService.CookieContainer = new CookieContainer();
                var mylogin = loginService.Login("905470711841", "demoroom123");
                //var mylogin = loginService.Login("527172574776", "Winter01");

                if (mylogin)
                {
                    System.Net.CookieCollection cookie = loginService.CookieContainer.GetCookies(new Uri("https://reference.dardni.gov.uk/GatewayWebServices/Gateway.asmx"));
                    var data = new AphisViewAnimalAOL();
                    data.CookieContainer = new CookieContainer();
                    data.CookieContainer.Add(cookie);
                    data.PreAuthenticate = true;
                    //data.Proxy = new WebProxy("proxy-lb", 8080);
                    //data.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                    var result = data.ViewAnimalAOL(139051, "570433", "UK 9 650384 0293 1");
                    var c = result;
                    //var result = data.ViewAnimalAOL(259110, "062840", "UK 9 060379 0624 5");
                }

            }
            catch (Exception ex)
            {
                details.Error += ex.ToString();
            }

            return details;
        }

        /// <summary>
        /// The aims animal details request
        /// </summary>
        /// <param name="fileName">the xml file name</param>
        /// <param name="dateGen">the date generated</param>
        /// <param name="numTrans">the number of transactions</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="tagNum">the ear tag number</param>
        /// <returns>a new animal detail response</returns>
        public AnimalDetail QueryAnimalDetails(string fileName, string dateGen, string numTrans, string transType, string speciesId, string tagNum, string serviceUrl, string certPath, string password, bool canSave = false, string logPath = "")
        {
            var details = AnimalDetail.CreateNewAnimalDetails();

            try
            {
                // Create the designator based on the parameters passed.
                details = AnimalDetail.CreateAnimalDetails(fileName, dateGen, numTrans, transType, speciesId, tagNum);

                // Create the xml request to be sent.
                var helper = new AnimalDetailsXmlHelper(details);
                var xmlRequest = helper.CreateXmlRequest(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest.ToString()))
                {
                    //var url = !string.IsNullOrEmpty(Properties.Settings.Default.serviceUrl) ? Properties.Settings.Default.serviceUrl : string.Empty;
                    //var certPath = !string.IsNullOrEmpty(Properties.Settings.Default.serviceUrl) ? Properties.Settings.Default.certPath : string.Empty;
                    //var password = !string.IsNullOrEmpty(Properties.Settings.Default.serviceUrl) ? Properties.Settings.Default.certPassword : string.Empty;

                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) || !string.IsNullOrEmpty(password))
                    {
                        var client = new Service.WebClient(serviceUrl);

                        // Load the security cert.
                        var securityCert = client.FindSecurityCert(certPath, password);

                        // Post the transaction to the service.
                        var serviceResponse = client.PostXMLTransaction(xmlRequest, securityCert, timeout: 20000);

                        // Extract the details object from the response.
                        details = helper.ParseXmlResponse(serviceResponse.InnerXml, fileName, canSave, logPath);
                    }
                    else
                    {
                        throw new ApplicationException("One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception ex)
            {
                details.Error += ex.ToString();
            }

            return details;
        }

        /// <summary>
        /// Moves a wand herd into lairage.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepMoveToLairageOutput MoveSheepToLairageTest(SheepToLairage.SheepMoveToLairageInput sheepMoveData)
        {
            var lairageCall = new SheepToLairage.WS_SheepMoveToLairage();
            return lairageCall.CallWS_SheepMoveToLairage(sheepMoveData);
        }

        /// <summary>
        /// Moves a wand herd into the abbatoir.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepMoveToAbatOutput MoveSheepToAbbatoirTest(SheepToAbbatoir.SheepMoveToAbatInput sheepMoveData)
        {
            var abbatoirCall = new SheepToAbbatoir.WS_SheepMoveToAbat();
            return abbatoirCall.CallWS_SheepMoveToAbat(sheepMoveData);
        }

        /// <summary>
        /// Moves a wand herd into lairage.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepMoveToLairageLive.SheepMoveToLairageOutput MoveSheepToLairage(SheepMoveToLairageLive.SheepMoveToLairageInput sheepMoveData)
        {
            var lairageCall = new SheepMoveToLairageLive.WS_SheepMoveToLairage();
            return lairageCall.CallWS_SheepMoveToLairage(sheepMoveData);
        }

        /// <summary>
        /// Moves a wand herd into the abbatoir.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepMoveToAbbatoirLive.SheepMoveToAbatOutput MoveSheepToAbbatoir(SheepMoveToAbbatoirLive.SheepMoveToAbatInput sheepMoveData)
        {
            var abbatoirCall = new SheepMoveToAbbatoirLive.WS_SheepMoveToAbat();
            return abbatoirCall.CallWS_SheepMoveToAbat(sheepMoveData);
        }

        /// <summary>
        /// Moves a wand herd into the abbatoir.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepAssignBatchNo.SheepAssignBatchNoOutput SheepAssignBatchNumber(SheepAssignBatchNo.SheepAssignBatchNoInput sheepMoveData)
        {
            var abbatoirCall = new SheepAssignBatchNo.WS_SheepAssignBatchNo();
            return abbatoirCall.CallWS_SheepAssignBatchNo(sheepMoveData);
        }

        /// <summary>
        /// Moves a wand herd into the abbatoir.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepAssignBatchNoLive.SheepAssignBatchNoOutput SheepAssignBatchNumberLive(SheepAssignBatchNoLive.SheepAssignBatchNoInput sheepMoveData)
        {
            var abbatoirCall = new SheepAssignBatchNoLive.WS_SheepAssignBatchNo();
            return abbatoirCall.CallWS_SheepAssignBatchNo(sheepMoveData);
        }

        /// <summary>
        /// Moves a wand herd into the abbatoir.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepEIDCheck.SheepEidCheckOutput SheepEIDCheck(SheepEIDCheck.SheepEidCheckInput sheepData)
        {
            var abbatoirCall = new SheepEIDCheck.WS_SheepEIDCheck();
            return abbatoirCall.CallWS_SheepEIDCheck(sheepData);
        }

        /// <summary>
        /// Moves a wand herd into the abbatoir.
        /// </summary>
        /// <param name="sheepMoveData">The move data.</param>
        /// <returns>The move result returned by the web service.</returns>
        public SheepEIDCheckLive.SheepEidCheckOutput SheepEIDCheckLive(SheepEIDCheckLive.SheepEidCheckInput sheepData)
        {
            var abbatoirCall = new SheepEIDCheckLive.WS_SheepEIDCheck();
            return abbatoirCall.CallWS_SheepEIDCheck(sheepData);
        }

        #region test data

        public void BCMSTestDataDefra()
        {
            //var client = new Service.WebClient(serviceUrl);



            // **************************Movements***********************************

            var data = new uk.gov.defra.ddts.secure.webservice.t1.DefraDataTransferPublicNWSE();
            data.Url = @"https://webservice.secure.ddts.defra.gov.uk/DefraDataTransferPublicNWSE.asmx";
            //var data = new uk.gov.defra.ddts.secure.webservice.t1.DefraDataTransferPublicNWSE();
            //data.Url = @"https://webservice.secure.ddts.defra.gov.uk/DefraDataTransferPublicNWSE.asmx";

            //var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //          "<RegMovs SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2017-12-21T15:15:47-05:00\">" +
            //          "  <Authentication>" +
            //          "   <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\" />" +
            //          "  </Authentication>" +
            //          "     <Moves TxnId=\"156844892277388\">" +
            //          "       <Mov RowNum=\"1\" Etg=\"UK200203100013\" Loc=\"03/003/0003\" SLoc=\"09\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"2\" Etg=\"UK590066500055\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"off\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"3\" Etg=\"UK590066600434\" Loc=\"20/999/0003\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"4\" Etg=\"UK590066200626\" Loc=\"01/001/0003\"  MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"5\" Etg=\"UK240001500772\" Loc=\"11/001/1231\" SLoc=\"04\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"6\" Etg=\"UK590066101010\" Loc=\"21/021/0023\"  MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"7\" Etg=\"UK720202600250\" Loc=\"21/021/0021\"  MType=\"off\" MDate=\"1999-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "        <Mov RowNum=\"8\" Etg=\"A0123  4567\" Loc=\"01/001/0002\"                MType=\"off\" MDate=\"2005-04-29\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "       <Mov RowNum=\"9\" Etg=\"UK720001700066\" Loc=\"01/001/0002\" SLoc=\"03\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //           "       <Mov RowNum=\"10\" Etg=\"UK100111700133\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //          "    </Moves>" +
            //          "</RegMovs>";

            var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                     "<RegMovs SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2017-12-21T15:17:22-05:00\">" +
                     "  <Authentication>" +
                     "   <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\" />" +
                     "  </Authentication>" +
                     "     <Moves TxnId=\"168924227388\">" +
                       "       <Mov RowNum=\"1\" Etg=\"UK200203100013\" Loc=\"03/003/0003\" SLoc=\"09\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
                     "       <Mov RowNum=\"2\" Etg=\"UK590066101010\" Loc=\"21/021/0023\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
                     "       <Mov RowNum=\"3\" Etg=\"UK720001700066\" Loc=\"01/001/0002\" SLoc=\"03\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
                     "       <Mov RowNum=\"4\" Etg=\"UK720202300058\" Loc=\"01/001/0003\" SLoc=\"02\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
                     "       <Mov RowNum=\"5\" Etg=\"UK720202600250\" Loc=\"21/021/0021\" MType=\"off\" MDate=\"1999-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
                     "       <Mov RowNum=\"6\" Etg=\"UK590066500818\" Loc=\"1234567890\" MType=\"off\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
                     "       <Mov RowNum=\"7\" Etg=\"UK590066300242\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"off\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
                     "       <Mov RowNum=\"8\" Etg=\"A123  4567\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"on\" MDate=\"2005-04-29\" REFNUM=\"1\" IWarn=\"n\" /> " +
                     "       <Mov RowNum=\"9\" Etg=\"FUZZY1\" Loc=\"11/111/1113\"           MType=\"off\" MDate=\"2005-04-29\" REFNUM=\"1\" IWarn=\"n\" /> " +
                     "       <Mov RowNum=\"10\" Etg=\"UK100111700133\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"on\" MDate=\"2099-12-31\" REFNUM=\"1\" IWarn=\"n\" /> " +
                     "    </Moves>" +
                     "</RegMovs>";

            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                      "   <GetResults SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\"  RequestTimeStamp=\"2017-12-21T15:28:14-05:00\" TestScenarioId=\"112221\">" +
                      "    <Authentication>  " +
                      "      <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/></Authentication> " +
                      "   <Receipt Num=\"440417\"/> " +
                      "</GetResults>";


            byte[] toEncodeAsBytes = Encoding.UTF8.GetBytes(xml);
            string encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
            var md5Password = Nouvem.Security.Security.CreateMd5Hash("fl0wers0");
           
            try
            {
                var moveResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS-FULL-PROVING", encodedAs64, "Get_Register_Movements_Validation_Results-V1-0");
                //var moveResponse = data.TransferDataHex("DDTS.CTWS.Vendor", md5Password, "DEFRA-CTWS-FULL-PROVING", encodedAs64, "Register_Movements_Asynchronous-V1-0");
                //var moveResponse = data.TransferDataHex("DDTS.CTWS.Vendor", md5Password, "DEFRA-CTWS-FULL-PROVING", encodedAs64, "Get_Register_Movements_Validation_Results-V1-0");
                //var moveResponse = data.TransferDataHex("DDTS.CTWS.Vendor", md5Password, "DEFRA-CTWS-FULL-PROVING", encodedAs64, "Get_Animal_Details-V1-0");
                //var moveResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS", encodedAs64, "Get_Animal_Details-V1-0");
                var base64EncodedBytes = System.Convert.FromBase64String(moveResponse);
                var xmlResponse = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                var result = xmlResponse;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            // **************************************************************************************







            //****************** Births Get Results **************************************************


            //                        var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //                                  "<RegBirths SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2018-02-27T11:56:57-05:00\" >" +
            //                                  "<Authentication>" +
            //                                  "  <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/>" +
            //                                  "</Authentication>" +
            //                                  "<Births TxnId=\"17787477729919\">" +
            ////"<Birth RowNum=\"1\" Etg=\"UK590066101017\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"21/021/0021\"              IWarn=\"y\"/>" +
            ////"<Birth RowNum=\"2\" Etg=\"UK240001300777\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK560002400063\"                                                   BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            ////"<Birth RowNum=\"3\" Etg=\"UK520202500138\" Dob=\"2004-03-01\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK560002400063\"                                                   BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            ////"<Birth RowNum=\"4\" Etg=\"UK200203500213\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"21/021/0021\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            ////"<Birth RowNum=\"5\" Etg=\"UK590066701401\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"21/021/0023\"              IWarn=\"y\"/>" +
            //"<Birth RowNum=\"6\" Etg=\"UK200203400205\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"m\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //"<Birth RowNum=\"7\" Etg=\"UK363636200155\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"DAISY1\"         SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"8\" Etg=\"UK590066401209\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"m\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0002\" PSLoc=\"03\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"9\" Etg=\"UK100111400137\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"01/001/0002\" BSLoc=\"03\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"10\" Etg=\"UK200203200021\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"21/021/0023\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"11\" Etg=\"UK590066100247\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK H4001 00033\"                          SiEtg=\"UK560002400063\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"12\" Etg=\"UK720001700073\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"08/088/5888\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"13\" Etg=\"UK590066101017\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"21/021/0021\"              IWarn=\"y\"/>" +
            //"<Birth RowNum=\"14\" Etg=\"UK240001200097\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKH4776 00269\"                           SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\"              IWarn=\"y\"/>" +
            //"<Birth RowNum=\"15\" Etg=\"UK320001500164\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00012\" SuEtg=\"UKAA223300012\"                           BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"16\" Etg=\"UK590066700057\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK590066300249\"                          SiEtg=\"UK H3796 00006\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"17\" Etg=\"UK200203400205\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UKH4001 00172\"  SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"18\" Etg=\"UK320001400163\" Dob=\"2020-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //"<Birth RowNum=\"19\" Etg=\"UK H4001 00172\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //"<Birth RowNum=\"20\" Etg=\"UK530303300080\" Dob=\"2004-03-09\" Brd=\"ZZ\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //                                  "</Births>" +
            //                                  "</RegBirths>";




            //                        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //                                  "   <GetResults SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2018-02-27T12:12:12-05:00\" TestScenarioId=\"112220\">" +
            //                                  "    <Authentication>  " +
            //                                  "      <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/></Authentication> " +
            //                                  "   <Receipt Num=\"447336\"/> " +
            //                                  "</GetResults>";

            //var data = new uk.gov.defra.ddts.secure.webservice.t1.DefraDataTransferPublicNWSE();
            //data.Url = @"https://webservice.secure.ddts.defra.gov.uk/DefraDataTransferPublicNWSE.asmx";
            //xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //          "<RegMovs SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2018-04-30T11:56:47-05:00\">" +
            //          "  <Authentication>" +
            //          "      <CTS_OL_User Usr=\"128-813-939\" Pwd=\"6YE8YY6\"/>" +
            //          "  </Authentication> " +
            //          " <Eartags> " +
            //          "   <Eartag_Id>UK740072300479</Eartag_Id>" +
            //          " </Eartags>" +
            //          "</RegMovs>";


            //xml =
            //    "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <RegMovs RequestTimeStamp=\"18-05-03T17:36:28\" ProgramVersion=\"17.158\" ProgramName=\"nouvem\" SchemaVersion=\"1.0\"> " +
            //    "   <Authentication>  " +
            //    "       <CTS_OL_User Usr=\"128-813-939\" Pwd=\"6YE8YY6\" />  " +
            //    "   </Authentication>" +
            //    "   <Eartags>" +
            //    "       <Eartag_Id>UK740072300479</Eartag_Id>" +
            //    "   </Eartags>" +
            //    " </RegMovs>";

            try
            {
                //var toEncodeAsBytes = Encoding.UTF8.GetBytes(xml);
                //var encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
                //var md5Password = CreateMd5Hash("Sn0w5how3rs");
                //var moveResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS", encodedAs64, "Get_Animal_Details-V1-0");

                //var base64EncodedBytes = System.Convert.FromBase64String(moveResponse);
                //var xmlResponse = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                //var result = xmlResponse;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void BCMSTestData()
        {
            #region NI
            //var serviceCall = new WS_SheepMoveToLairage();
            //var genericInput = new SheepToLairage.GenericInput
            //{
            //    callingLocation = "Meat",
            //    channel = "Meat",
            //    clntRefNo_BusId = "999994",
            //    environment = "Test",
            //    herdNo = "A06007",
            //    username = "LAKE01"
            //};

            //var sheepDetails = new SheepDetails
            //{
            //    lotNumber = 111,
            //    noOfAnimals = 1,
            //    noOfAnimalsSpecified = true,
            //    type = "LAMBS",
            //    eidList = new string[]
            //    {
            //        "UK 1710235 01701"
            //    } // 
            //};

            //SheepDetails[] details = { sheepDetails };

            //var batch = new SheepBatches
            //{
            //    batchNo = "122",
            //    noOfAnimals = 1,
            //    noOfAnimalsSpecified = true
            //};

            //SheepBatches[] batches = { batch };

            //var testResult = serviceCall.CallWS_SheepMoveToLairage(new SheepMoveToLairageInput
            //{
            //    genericInput = genericInput,
            //    pinNo = 8,
            //    pinNoSpecified = true,
            //    batchNo = "122",
            //    fromFlockNo = "710235",
            //    movementType = "EID",
            //    noOfAnimals = 1,
            //    noOfAnimalsSpecified = true,
            //    permitNo = "F002248935",
            //    sheepDetailList = details,
            //    sheepBatchList = batches
            //});

            //var yy = testResult;
            //return;

            //var serviceCall = new SheepToLairage.WS_SheepMoveToLairage();
            //var genericInput = new Nouvem.AIMS.SheepToLairage.GenericInput
            //{
            //    callingLocation = "Meat",
            //    channel = "Meat",
            //    clntRefNo_BusId = "999994",
            //    environment = "Test",
            //    herdNo = "A06007",
            //    username = "LAKE01"
            //};

            //var sheepDetails = new SheepDetails
            //{
            //    lotNumber = 0,
            //    noOfAnimals = 2,
            //    type = "LAMBS",
            //    eidList = new string[] { "UK 1710235 01701" }
            //};

            //SheepDetails[] details = { sheepDetails };

            //var batch = new SheepBatches
            //{
            //    batchNo = "1",
            //    noOfAnimals = 1
            //};

            //SheepBatches[] batches = { batch };

            //var testResult = serviceCall.CallWS_SheepMoveToLairage(new SheepMoveToLairageInput
            //{
            //    genericInput = genericInput,
            //    pinNo = 8,
            //    batchNo = "1",
            //    fromFlockNo = "710235",
            //    movementType = "EID",
            //    noOfAnimals = 1,
            //    permitNo = "F002248935",
            //    sheepDetailList = details,
            //    sheepBatchList = batches
            //});

            //var yy = testResult;


            //var tt = new SheepToLairage.WS_SheepMoveToLairage();
            ////tt.Url = @"https://ws-pptref.dardni.gov.uk/AphisMeatServicesTest/services/WS_SheepMoveToLairage";
            //var genericInput = new Nouvem.AIMS.SheepToLairage.GenericInput
            //{
            //    callingLocation = "Meat",
            //    channel = "Meat",
            //    clntRefNo_BusId = "922230",
            //    environment = "Test",
            //    herdNo = "A012345",
            //    username = "USER01",
            //    //password = "Winter01"
            //};

            //var batch = new SheepBatches
            //{
            //    batchNo = "123",
            //    noOfAnimals = 1,
            //    lotNo = "111"
            //};

            //var sheepDetails = new SheepDetails
            //{
            //    lotNumber = 1,
            //    noOfAnimals = 2,
            //    type = "LAMBS",
            //    eidList = new string[] { "UK 1780903 02074", "UK 1780903 02083" }
            //};
            //SheepDetails[] details = { sheepDetails };
            //SheepBatches[] batches = { batch };

            //var resulttt = tt.CallWS_SheepMoveToLairage(new SheepMoveToLairageInput
            //{
            //    batchNo = "1",
            //    fromFlockNo = "710235",
            //    movementType = "EID",
            //    noOfAnimals = 1,
            //    permitNo = "F002248937",
            //    pinNo = 6,
            //    sheepDetailList = details,
            //    genericInput = genericInput,
            //    //sheepBatchList = batches
            //});

            //var yy = resulttt;


            //var genericInput = new Nouvem.AIMS.SheepToAbbatoir.GenericInput
            //{
            //    callingLocation = "Meat",
            //    channel = "Meat",
            //    clntRefNo_BusId = "999994",
            //    environment = "Test",
            //    herdNo = "A06007",
            //    username = "LAKE01",
            //    password = "Winter01"
            //};

            //var sheepMoveData = new SheepMoveToAbatInput
            //{
            //    batchNo = "10101",
            //    pinNo = 8,
            //    genericInput = genericInput
            //};

            //var abbatoirCall = new WS_SheepMoveToAbat();
            //var resultt = abbatoirCall.CallWS_SheepMoveToAbat(sheepMoveData);
            //var tt = resultt;

            //var tt = new SheepToLairage.WS_SheepMoveToLairage();
            ////tt.Url = @"https://ws-pptref.dardni.gov.uk/AphisMeatServicesTest/services/WS_SheepMoveToLairage";
            //var genericInput = new Nouvem.AIMS.SheepToLairage.GenericInput
            //{
            //    callingLocation = "Meat",
            //    channel = "Meat",
            //    clntRefNo_BusId = "657666",
            //    environment = "Test",
            //    herdNo = "06007S",
            //    username = "LAKE01",
            //    //password = "Winter01"
            //};

            //var batch = new SheepBatches
            //{
            //    batchNo = "123",
            //    noOfAnimals = 1,
            //    lotNo = "111"
            //};

            //var sheepDetails = new SheepDetails
            //{
            //    lotNumber = 1,
            //    noOfAnimals = 1,
            //    type = "EWES",
            //    eidList = new string[] { "0826172069804060" }
            //};
            //SheepDetails[] details = { sheepDetails };
            //SheepBatches[] batches = { batch };

            //var resulttt = tt.CallWS_SheepMoveToLairage(new SheepMoveToLairageInput
            //{
            //    batchNo = "123",
            //    fromFlockNo = "760827",
            //    movementType = "EID",
            //    noOfAnimals = 1,
            //    permitNo = "A000009760",
            //    pinNo = 123,
            //    sheepDetailList = details,
            //    genericInput = genericInput,
            //    sheepBatchList = batches
            //});

            //var yy = resulttt;

            //var a = new SheepToAbbatoir.WS_SheepMoveToAbat();
            //a.CallWS_SheepMoveToAbat(new SheepMoveToAbatInput { });

            //var client = new Service.WebClient(serviceUrl);

            #endregion



            // **************************Movements***********************************

            ////var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            ////          "<RegMovs SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2017-12-21T15:15:47-05:00\">" +
            ////          "  <Authentication>" +
            ////          "   <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\" />" +
            ////          "  </Authentication>" +
            ////          "     <Moves TxnId=\"156844892277388\">" +
            ////          "       <Mov RowNum=\"1\" Etg=\"UK200203100013\" Loc=\"03/003/0003\" SLoc=\"09\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"2\" Etg=\"UK590066500055\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"off\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"3\" Etg=\"UK590066600434\" Loc=\"20/999/0003\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"4\" Etg=\"UK590066200626\" Loc=\"01/001/0003\"  MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"5\" Etg=\"UK240001500772\" Loc=\"11/001/1231\" SLoc=\"04\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"6\" Etg=\"UK590066101010\" Loc=\"21/021/0023\"  MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"7\" Etg=\"UK720202600250\" Loc=\"21/021/0021\"  MType=\"off\" MDate=\"1999-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////         "        <Mov RowNum=\"8\" Etg=\"A0123  4567\" Loc=\"01/001/0002\"                MType=\"off\" MDate=\"2005-04-29\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "       <Mov RowNum=\"9\" Etg=\"UK720001700066\" Loc=\"01/001/0002\" SLoc=\"03\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////           "       <Mov RowNum=\"10\" Etg=\"UK100111700133\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            ////          "    </Moves>" +
            ////          "</RegMovs>";

            //var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //         "<RegMovs SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2017-12-21T15:17:22-05:00\">" +
            //         "  <Authentication>" +
            //         "   <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\" />" +
            //         "  </Authentication>" +
            //         "     <Moves TxnId=\"168924227388\">" +
            //           "       <Mov RowNum=\"1\" Etg=\"UK200203100013\" Loc=\"03/003/0003\" SLoc=\"09\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "       <Mov RowNum=\"2\" Etg=\"UK590066101010\" Loc=\"21/021/0023\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
            //         "       <Mov RowNum=\"3\" Etg=\"UK720001700066\" Loc=\"01/001/0002\" SLoc=\"03\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
            //         "       <Mov RowNum=\"4\" Etg=\"UK720202300058\" Loc=\"01/001/0003\" SLoc=\"02\" MType=\"on\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
            //         "       <Mov RowNum=\"5\" Etg=\"UK720202600250\" Loc=\"21/021/0021\" MType=\"off\" MDate=\"1999-05-01\" REFNUM=\"1\" IWarn=\"y\" /> " +
            //         "       <Mov RowNum=\"6\" Etg=\"UK590066500818\" Loc=\"1234567890\" MType=\"off\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "       <Mov RowNum=\"7\" Etg=\"UK590066300242\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"off\" MDate=\"2005-05-01\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "       <Mov RowNum=\"8\" Etg=\"A123  4567\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"on\" MDate=\"2005-04-29\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "       <Mov RowNum=\"9\" Etg=\"FUZZY1\" Loc=\"11/111/1113\"           MType=\"off\" MDate=\"2005-04-29\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "       <Mov RowNum=\"10\" Etg=\"UK100111700133\" Loc=\"01/001/0001\" SLoc=\"04\" MType=\"on\" MDate=\"2099-12-31\" REFNUM=\"1\" IWarn=\"n\" /> " +
            //         "    </Moves>" +
            //         "</RegMovs>";

            //xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //          "   <GetResults SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\"  RequestTimeStamp=\"2017-12-21T15:28:14-05:00\" TestScenarioId=\"112221\">" +
            //          "    <Authentication>  " +
            //          "      <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/></Authentication> " +
            //          "   <Receipt Num=\"440417\"/> " +
            //          "</GetResults>";

            //byte[] toEncodeAsBytes = Encoding.UTF8.GetBytes(xml);
            //string encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
            //var md5Password = Nouvem.Security.Security.CreateMd5Hash("fl0wers0");
            ////var moveResponse = data.TransferDataHex("DDTS.CTWS.Vendor", md5Password, "DEFRA-CTWS-FULL-PROVING", encodedAs64, "Register_Movements_Asynchronous-V1-0");
            //var moveResponse = data.TransferDataHex("DDTS.CTWS.Vendor", md5Password, "DEFRA-CTWS-FULL-PROVING", encodedAs64, "Get_Register_Movements_Validation_Results-V1-0");


            // **************************************************************************************







            //****************** Births Get Results **************************************************


            //                        var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //                                  "<RegBirths SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2018-02-27T11:56:57-05:00\" >" +
            //                                  "<Authentication>" +
            //                                  "  <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/>" +
            //                                  "</Authentication>" +
            //                                  "<Births TxnId=\"17787477729919\">" +
            ////"<Birth RowNum=\"1\" Etg=\"UK590066101017\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"21/021/0021\"              IWarn=\"y\"/>" +
            ////"<Birth RowNum=\"2\" Etg=\"UK240001300777\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK560002400063\"                                                   BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            ////"<Birth RowNum=\"3\" Etg=\"UK520202500138\" Dob=\"2004-03-01\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK560002400063\"                                                   BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            ////"<Birth RowNum=\"4\" Etg=\"UK200203500213\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"21/021/0021\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            ////"<Birth RowNum=\"5\" Etg=\"UK590066701401\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"21/021/0023\"              IWarn=\"y\"/>" +
            //"<Birth RowNum=\"6\" Etg=\"UK200203400205\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"m\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //"<Birth RowNum=\"7\" Etg=\"UK363636200155\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"DAISY1\"         SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"8\" Etg=\"UK590066401209\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"m\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0002\" PSLoc=\"03\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"9\" Etg=\"UK100111400137\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"01/001/0002\" BSLoc=\"03\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"10\" Etg=\"UK200203200021\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"21/021/0023\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"11\" Etg=\"UK590066100247\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK H4001 00033\"                          SiEtg=\"UK560002400063\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"12\" Etg=\"UK720001700073\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"08/088/5888\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"13\" Etg=\"UK590066101017\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"21/021/0021\"              IWarn=\"y\"/>" +
            //"<Birth RowNum=\"14\" Etg=\"UK240001200097\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKH4776 00269\"                           SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\"              IWarn=\"y\"/>" +
            //"<Birth RowNum=\"15\" Etg=\"UK320001500164\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00012\" SuEtg=\"UKAA223300012\"                           BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"16\" Etg=\"UK590066700057\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UK590066300249\"                          SiEtg=\"UK H3796 00006\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"17\" Etg=\"UK200203400205\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UKH4001 00172\"  SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"y\"/>" +
            //"<Birth RowNum=\"18\" Etg=\"UK320001400163\" Dob=\"2020-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //"<Birth RowNum=\"19\" Etg=\"UK H4001 00172\" Dob=\"2004-03-09\" Brd=\"HF\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //"<Birth RowNum=\"20\" Etg=\"UK530303300080\" Dob=\"2004-03-09\" Brd=\"ZZ\" Sex=\"f\" EId=\"111222333\" GdEtg=\"UKAA2233 00009\" SuEtg=\"UK560002400063\" SiEtg=\"UK A1213 00003\" BLoc=\"20/002/0001\" PLoc=\"01/001/0001\" PSLoc=\"04\" IWarn=\"n\"/>" +
            //                                  "</Births>" +
            //                                  "</RegBirths>";




            //                        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
            //                                  "   <GetResults SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2018-02-27T12:12:12-05:00\" TestScenarioId=\"112220\">" +
            //                                  "    <Authentication>  " +
            //                                  "      <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/></Authentication> " +
            //                                  "   <Receipt Num=\"447336\"/> " +
            //
            //


            //SecureString theSecureString = new NetworkCredential("", "Winter01").SecurePassword;


            //var toEncodeAsBytes = Encoding.UTF8.GetBytes("527172574776");
            //var username = Convert.ToBase64String(toEncodeAsBytes);

            //var toEncodeAsBytes1 = Encoding.UTF8.GetBytes("Winter01");
            //var localPassword = Convert.ToBase64String(toEncodeAsBytes1);

            //var loginService = new uk.gov.dardni.reference1.Gateway();
            //loginService.CookieContainer = new CookieContainer();

            ////loginService.Proxy = new WebProxy("proxy-lb", 8080);

            ////loginService.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;



            //var mylogin = loginService.Login("905470711841", "demoroom123");

            ////var mylogin = loginService.Login("527172574776", "Winter01");



            //if (mylogin)
            //{
            //    System.Net.CookieCollection cookie = loginService.CookieContainer.GetCookies(new Uri("https://reference.dardni.gov.uk/GatewayWebServices/Gateway.asmx"));
            //    var data = new AphisViewAnimalAOL();
            //    data.CookieContainer = new CookieContainer();
            //    data.CookieContainer.Add(cookie);
            //    data.PreAuthenticate = true;

            //    //data.Proxy = new WebProxy("proxy-lb", 8080);

            //    //data.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

            //    var result = data.ViewAnimalAOL(139051, "570433", "UK 9 650384 0293 1");
            //    var c = result;
            //    //var result = data.ViewAnimalAOL(259110, "062840", "UK 9 060379 0624 5");

            //}
            ////var data = new uk.gov.dardni.reference.AphisViewAnimalAOL();
          
            //////data.SoapVersion = SoapProtocolVersion.Soap12;
            ////data.Credentials = new System.Net.NetworkCredential("527172574776", "Winter01");

            //try
            //{
            //    //var localResult = data.ViewAnimalAOL(247840, "062840", "UK906037906245");
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}

          

            //return;


            var data = new uk.gov.defra.ddts.secure.webservice.t1.DefraDataTransferPublicNWSE();
            data.Url = @"https://webservice.secure.ddts.defra.gov.uk/DefraDataTransferPublicNWSE.asmx";

            var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                      "<RegMovs SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\" RequestTimeStamp=\"2018-04-30T11:56:47-05:00\">" +
                      "  <Authentication>" +
                      "      <CTS_OL_User Usr=\"128-813-939\" Pwd=\"6YE8YY6\"/>" +
                      "  </Authentication> " +
                      " <Eartags> " +
                      "   <Eartag_Id>UK740072300479</Eartag_Id>" +
                      " </Eartags>" +
                      "</RegMovs>";

                   //xml =
                   //    "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <RegMovs RequestTimeStamp=\"18-05-03T17:36:28\" ProgramVersion=\"17.158\" ProgramName=\"nouvem\" SchemaVersion=\"1.0\"> " +
                   //    "   <Authentication>  " +
                   //    "       <CTS_OL_User Usr=\"128-813-939\" Pwd=\"6YE8YY6\" />  " +
                   //    "   </Authentication>" +
                   //    "   <Eartags>" +
                   //    "       <Eartag_Id>UK740072300479</Eartag_Id>" +
                   //    "   </Eartags>" +
                   //    " </RegMovs>";

                   xml =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                "   <ViewAnimalAOL  xmlns = \"http://www.dardni.gov.uk/\">  " +
                "       <customerId>527172574776</customerId>" +
                "       <herdNo>062840</herdNo>" +
                "       <AnimalNo>UK740072300479</AnimalNo>" +
                "   </ViewAnimalAOL>";

           
         
               

            try
            {
                var toEncodeAsBytes = Encoding.UTF8.GetBytes(xml);
                var encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
                var md5Password = CreateMd5Hash("Sn0w5how3rs");
                var moveResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS", encodedAs64, "Get_Animal_Details-V1-0");

                var base64EncodedBytes = System.Convert.FromBase64String(moveResponse);
                var xmlResponse = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                var result = xmlResponse;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void DataOnViewAnimalAolCompleted(object sender, ViewAnimalAOLCompletedEventArgs e)
        {
            var t = e;
            var c = t;
        }

        #endregion

        #endregion

        #region movement request

        /// <summary>
        /// Aim query request for the herd movement into the factory
        /// </summary>
        /// <param name="fileName">the xml file name</param>
        /// <param name="dateGen">the date the request was generated</param>
        /// <param name="numTrans">the number of transactions</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="movedFromId">the moved fromm herd id</param>
        /// <param name="moveDate">the movement date</param>
        /// <param name="lotNum">the lot number</param>
        /// <param name="softwareId">the software id</param>
        /// <param name="countryCode">the country code</param>
        /// <param name="numTags">the number of tags</param>
        /// <param name="lotAction">the lot action</param>
        /// <param name="movementTags">the collection of movement tags</param>
        /// <returns>a movement request</returns>
        public MovementRequest QueryUKMovementRequest(string fileName, string dateGen, string numTrans, string senderId,
                                                            string transType, string speciesId, string moveFromId, string moveDate,
                                                            string lotNum, string softwareId, string countryCode, string numTags,
                                                            string lotAction, List<MovementTag> movementTag, string serviceUrl, 
                                                            string certPath, string password, bool canSave = false, string logPath = "",string userName = "")
        {
            // create the movementRequest object based on the passed parameters
            var movementRequest = MovementRequest.CreateNewMovementRequest();

            try
            {
                movementRequest = MovementRequest.CreateMovementRequest(fileName, dateGen, numTrans, senderId, transType, speciesId, moveFromId, moveDate,
                                                                       lotNum, softwareId, countryCode, numTags, lotAction, string.Empty,
                                                                       string.Empty, string.Empty, null, movementTag, null,userName,password);


                // Create the xml request to be sent.
                var helper = new MovementRequestXmlHelper(movementRequest);
                var xmlRequest = helper.CreateXmlRequestBCMS(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest))
                {
                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) || !string.IsNullOrEmpty(password))
                    {
                        var data = new uk.gov.defra.ddts.secure.webservice.t1.DefraDataTransferPublicNWSE();
                        data.Url = serviceUrl;

                        var toEncodeAsBytes = Encoding.UTF8.GetBytes(xmlRequest);
                        var encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
                        var md5Password = CreateMd5Hash("Sn0w5how3rs");
                        var moveResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS", encodedAs64, "Register_Movements_Asynchronous-V1-0");

                        var base64EncodedBytes = System.Convert.FromBase64String(moveResponse);
                        var xmlResponse = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                        if (xmlResponse.StartsWithIgnoringCase("CTWS System Exception"))
                        {
                            throw new ApplicationException(xmlResponse);
                        }

                        var details = helper.ParseXmlBCMSResponse(xmlResponse, logPath: logPath, fileName: fileName, canSave: true);
                        if (!string.IsNullOrEmpty(details.InwardMovementRefId))
                        {
                            var xmlValidation = helper.CreateXmlValidationRequestBCMS(details.InwardMovementRefId);
                            toEncodeAsBytes = Encoding.UTF8.GetBytes(xmlValidation);
                            encodedAs64 = Convert.ToBase64String(toEncodeAsBytes);
                            md5Password = CreateMd5Hash("Sn0w5how3rs");
                            var validationResponse = data.TransferDataHex("DDTS.CTWS.BMNouvem", md5Password, "DEFRA-CTWS", encodedAs64, "Get_Register_Movements_Validation_Results-V1-0");

                            base64EncodedBytes = System.Convert.FromBase64String(validationResponse);
                            xmlResponse = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                            if (xmlResponse.StartsWithIgnoringCase("CTWS System Exception"))
                            {
                                throw new ApplicationException(xmlValidation);
                            }

                            var validationDetails = helper.ParseXmlBCMSValidationResponse(xmlResponse, logPath: logPath, fileName: fileName, canSave: true);
                            if (validationDetails.ValidationErrors != null && validationDetails.ValidationErrors.Any())
                            {
                                movementRequest.Error = "Validation errors have been found with the movement request";
                                movementRequest.ValidationErrors = validationDetails.ValidationErrors;
                            }
                        }
                        else
                        {
                            movementRequest.Error = details.Error;
                        }
                    }
                    else
                    {
                        throw new ApplicationException("One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception ex)
            {
                movementRequest.Error += ex.ToString();
            }

            return movementRequest;
        }

        /// <summary>
        /// Aim query request for the herd movement into the factory
        /// </summary>
        /// <param name="fileName">the xml file name</param>
        /// <param name="dateGen">the date the request was generated</param>
        /// <param name="numTrans">the number of transactions</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="movedFromId">the moved fromm herd id</param>
        /// <param name="moveDate">the movement date</param>
        /// <param name="lotNum">the lot number</param>
        /// <param name="softwareId">the software id</param>
        /// <param name="countryCode">the country code</param>
        /// <param name="numTags">the number of tags</param>
        /// <param name="lotAction">the lot action</param>
        /// <param name="movementTags">the collection of movement tags</param>
        /// <returns>a movement request</returns>
        public MovementRequest QueryMovementRequest(string fileName, string dateGen, string numTrans, string senderId,
                                                            string transType, string speciesId, string moveFromId, string moveDate,
                                                            string lotNum, string softwareId, string countryCode, string numTags,
                                                            string lotAction, List<MovementTag> movementTag, string serviceUrl, string certPath, string password, bool canSave = false, string logPath = "")
        {
            // create the movementRequest object based on the passed parameters
            var movementRequest = MovementRequest.CreateNewMovementRequest();

            try
            {
                movementRequest = MovementRequest.CreateMovementRequest(fileName, dateGen, numTrans, senderId, transType, speciesId, moveFromId, moveDate,
                                                                       lotNum, softwareId, countryCode, numTags, lotAction, string.Empty,
                                                                       string.Empty, string.Empty, null, movementTag, null);


                // Create the xml request to be sent.
                var helper = new MovementRequestXmlHelper(movementRequest);
                var xmlRequest = helper.CreateXmlRequest(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest.ToString()))
                {
                    //System.Net.ServicePointManager.Expect100Continue = true;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) || !string.IsNullOrEmpty(password))
                    {
                        var client = new Service.WebClient(serviceUrl);

                        // Load the security cert.
                        var securityCert = client.FindSecurityCert(certPath, password);

                        // Post the transaction to the service.
                        var serviceResponse = client.PostXMLTransaction(xmlRequest, securityCert);

                        // Extract the designator object from the response.
                        movementRequest = helper.ParseXmlResponse(serviceResponse.InnerXml, fileName, canSave, logPath);
                    }
                    else
                    {
                        throw new ApplicationException("One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception ex)
            {
                movementRequest.Error += ex.ToString();
            }

            return movementRequest;
        }

        /// <summary>
        /// Aim query request for the herd movement into the factory
        /// </summary>
        /// <param name="fileName">the xml file name</param>
        /// <param name="dateGen">the date the request was generated</param>
        /// <param name="numTrans">the number of transactions</param>
        /// <param name="transType">the transaction type</param>
        /// <param name="speciesId">the species id</param>
        /// <param name="movedFromId">the moved fromm herd id</param>
        /// <param name="moveDate">the movement date</param>
        /// <param name="lotNum">the lot number</param>
        /// <param name="softwareId">the software id</param>
        /// <param name="countryCode">the country code</param>
        /// <param name="numTags">the number of tags</param>
        /// <param name="lotAction">the lot action</param>
        /// <param name="movementTags">the collection of movement tags</param>
        /// <returns>a movement request</returns>
        public MovementRequest QuerySheepMovementRequest(string fileName, string dateGen, string numTrans, string senderId,
                                                            string transType, string speciesId, string moveFromId, string moveDate,
                                                            string lotNum, string softwareId,  string numTags,
                                                            string lotAction, List<MovementTag> movementTag, string serviceUrl, string certPath, 
                                                            string password, bool canSave = false, string logPath = "", string permitNo = "", 
                                                            int noTagsDead = 0, int noTagsC = 0, int noTagsB = 0)
        {
            // create the movementRequest object based on the passed parameters
            var movementRequest = MovementRequest.CreateNewMovementRequest();

            try
            {
                movementRequest = MovementRequest.CreateSheepMovementRequest(fileName, dateGen, numTrans, senderId, transType, speciesId, moveFromId, moveDate,
                                                                       lotNum, softwareId, numTags, lotAction, string.Empty,
                                                                       string.Empty, string.Empty, null, movementTag, null, permitNo, noTagsDead, noTagsC, noTagsB);


                // Create the xml request to be sent.
                var helper = new MovementRequestXmlHelper(movementRequest);
                var xmlRequest = helper.CreateSheepXmlRequest(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest.ToString()))
                {
                    //System.Net.ServicePointManager.Expect100Continue = true;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) || !string.IsNullOrEmpty(password))
                    {
                        var client = new Service.WebClient(serviceUrl);

                        // Load the security cert.
                        var securityCert = client.FindSecurityCert(certPath, password);

                        // Post the transaction to the service.
                        var serviceResponse = client.PostXMLTransaction(xmlRequest, securityCert);

                        //movementRequest = helper.ParseXmlSheepResponse("", fileName, canSave, logPath);

                        // Extract the designator object from the response.
                        movementRequest = helper.ParseXmlSheepResponse(serviceResponse.InnerXml, fileName, canSave, logPath);
                    }
                    else
                    {
                        throw new ApplicationException("One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception ex)
            {
                movementRequest.Error += ex.ToString();
            }

            return movementRequest;
        }

        #endregion

        #region factory price details

        /// <summary>
        /// Method that querries the factory price detail service.
        /// </summary>
        /// <param name="fileName">The name of the file in which the request will be stored.</param>
        /// <param name="dateGen">The date on which the request will be sent.</param>
        /// <param name="numTrans">The number of transactions which the xml request will have.</param>
        /// <param name="transType">The transaction type. MOV is a movement transaction request, for example.</param>
        /// <param name="speciesID">The species id.</param>
        /// <param name="officerCode">The sending officer code.</param>
        /// <param name="carcassNumber">The carcass number.</param>
        /// <param name="category">The animal category.</param>
        /// <param name="conformation">The animal conformation score.</param>
        /// <param name="fat">The animal fat score.</param>
        /// <param name="weightside1">The side 1 weight.</param>
        /// <param name="weightSide2">The side 2 weight.</param>
        /// <param name="tagNumber">The eartag.</param>
        /// <param name="carcassDescription">The carcass description.</param>
        /// <param name="price">The carcass price.</param>
        /// <param name="factoryOwnershipID">The factory owner id.</param>
        /// <param name="serviceUrl">The url of the service.</param>
        /// <param name="certPath">The path at which the security cert is stored.</param>
        /// <param name="password">The password needed to unlock the certificate used in communicating to AIM.</param>
        /// <param name="canSave">Flag to indicate if the file is to be saved to disk.</param>
        /// <param name="logPath">The path at which the file is to be saved.</param>
        /// <returns>A factory price object.</returns>
        public FactoryPriceDetail QueryFactoryPriceDetail(
            string fileName,
            string dateGen,
            string numTrans,
            string transType,
            string speciesID,
            string officerCode,
            string carcassNumber,
            string category,
            string conformation,
            string fat,
            string weightside1,
            string weightSide2,
            string tagNumber,
            string carcassDescription,
            string price,
            string factoryOwnershipID,
            string serviceUrl,
            string certPath,
            string password,
            bool canSave = false,
            string logPath = "")
        {
            var priceDetail = FactoryPriceDetail.CreatefactoryPriceDetail(
                fileName,
                dateGen,
                numTrans,
                transType,
                speciesID,
                officerCode,
                carcassNumber,
                category,
                conformation,
                fat,
                weightside1,
                weightSide2,
                tagNumber,
                carcassDescription,
                price,
                factoryOwnershipID);

            try
            {
                var helper = new FactoryPriceDetailXmlHelper(priceDetail);
                var xmlRequest = helper.CreateXmlRequest(fileName, canSave, logPath);

                if (!string.IsNullOrEmpty(xmlRequest.ToString()))
                {
                    if (!string.IsNullOrEmpty(serviceUrl) || !string.IsNullOrEmpty(certPath) ||
                        !string.IsNullOrEmpty(password))
                    {
                        var client = new Service.WebClient(serviceUrl);

                        System.Net.ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

                        // Load the security cert.
                        var securityCert = client.FindSecurityCert(certPath, password);

                        // Post the transaction to the service.
                        var serviceResponse = client.PostXMLTransaction(xmlRequest, securityCert, timeout: 20000);

                        // Extract the designator object from the response.
                        priceDetail = helper.ParseXmlResponse(serviceResponse.InnerXml, fileName, canSave, logPath);
                    }
                    else
                    {
                        throw new ApplicationException(
                            "One or more of the service details (url, certificate path, password) supplied in the configuration are incorrect.");
                    }
                }
                else
                {
                    throw new ApplicationException("Xml request generation failed.");
                }
            }
            catch (Exception ex)
            {
                priceDetail.Error += ex.ToString();
            }

            return priceDetail;
        }

        #endregion

        #endregion
    }
}


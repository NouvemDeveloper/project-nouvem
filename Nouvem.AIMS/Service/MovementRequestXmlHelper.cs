﻿// -----------------------------------------------------------------------
// <copyright file="Errors.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.CodeDom;
using Nouvem.AIMS.SheepMoveToAbbatoirLive;

namespace Nouvem.AIMS.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using Nouvem.AIMS.BusinessLogic;
    using Nouvem.AIMS.Model;
    using Nouvem.Shared;

    /// <summary>
    /// Helper class for the web service calls which is used to generate and parse xml animal details requests.
    /// </summary>
    public class MovementRequestXmlHelper
    {
        #region Fields

        /// <summary>
        /// Instance of the movement request object.
        /// </summary>
        private MovementRequest movementRequest;

        /// <summary>
        /// The version of the xml being sent.
        /// </summary>
        private string version;

        /// <summary>
        /// The encoding of the xml being sent.
        /// </summary>
        private string encoding;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MovementRequestXmlHelper"/> class.
        /// </summary>
        /// <param name="request">The <see cref="AnimalDetailsXmlHelper"/> to be processed for transmission to AIMs.</param>
        /// <param name="version">The version of the xml file which defaults to 1.0.</param>
        /// <param name="encoding">The encoding of the xml file which defaults to UTF-8.</param>
        public MovementRequestXmlHelper(MovementRequest request, string version = "1.0", string encoding = "UTF-8")
        {
            this.movementRequest = request;
            this.version = version;
            this.encoding = encoding;
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Extract the AnimalDetails object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A AnimalDetails object detailing the service call response.</returns>
        public MovementRequest ParseXmlBCMSValidationResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var detailsLocal = MovementRequest.CreateNewMovementRequest();
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                //detailsLocal.AnimalMovements = new List<Movement>();
                var doc = XDocument.Parse(xmlResponse);

                var rejects = doc.Descendants().Where(p => p.Name.LocalName == "Reject").ToList();
                if (!rejects.Any())
                {
                    return detailsLocal;
                }

                detailsLocal.ValidationErrors = new List<Tuple<string, string>>();
                foreach (var xElement in rejects)
                {
                    var eartag = string.Empty;
                    var reason = string.Empty;
                    var tag = xElement.Descendants().FirstOrDefault(x => x.Name.LocalName == "Mov");
                    eartag = tag.FindInElement("Etg");
                    var causes = xElement.Descendants().FirstOrDefault(x => x.Name.LocalName == "Causes");

                    if (causes != null)
                    {
                        var cause = causes.Descendants().FirstOrDefault(x => x.Name.LocalName == "Cause");
                        reason = cause.FindInElement("Desc");
                    }
                   
                    detailsLocal.ValidationErrors.Add(Tuple.Create(eartag,reason));
                }

                if (canSave)
                {
                    var fromAimsPath = System.IO.Path.Combine(logPath, "fromAIMS");
                    if (Directory.Exists(fromAimsPath))
                    {
                        var fullPath = System.IO.Path.Combine(fromAimsPath, fileName);
                        doc.Save(fullPath);
                    }
                }
            }

            return detailsLocal;
        }

        /// <summary>
        /// Extract the AnimalDetails object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A AnimalDetails object detailing the service call response.</returns>
        public MovementRequest ParseXmlBCMSResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var detailsLocal = MovementRequest.CreateNewMovementRequest();
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                //detailsLocal.AnimalMovements = new List<Movement>();
                var doc = XDocument.Parse(xmlResponse);

                var error = doc.Descendants().FirstOrDefault(x => x.Name.LocalName == "SystemException");
                if (error != null)
                {
                    detailsLocal.Error = error.FindInElement("ExMsg");
                }
                else
                {
                    var move = doc.Descendants().FirstOrDefault(x => x.Name.LocalName == "Receipt");
                    if (move != null)
                    {
                        detailsLocal.InwardMovementRefId = move.FindInElement("Num");
                    }
                }

                if (canSave)
                {
                    var fromAimsPath = System.IO.Path.Combine(logPath, "fromAIMS");
                    if (Directory.Exists(fromAimsPath))
                    {
                        var fullPath = System.IO.Path.Combine(fromAimsPath, fileName);
                        doc.Save(fullPath);
                    }
                }
            }

            return detailsLocal;
        }
        
        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public string CreateXmlRequestBCMS(string fileName = "", bool canSave = false, string logPath = "")
        {
            var xmlDocument = new XDocument();

            if (this.movementRequest != null)
            {
                var userName = this.movementRequest.UserName;
                var userPassword = this.movementRequest.UserPassword;
                var date = DateTime.Today.ToString("yy-MM-dd");
                var time = DateTime.Now.ToString("HH:mm:ss");
                var docDate = string.Format("{0}T{1}", date, time);

                var moves = new XElement("Moves",new XAttribute("TxnId", this.movementRequest.LotNum + Guid.NewGuid()));
                var rowNum = 1;
                foreach (var tag in this.movementRequest.MovementTag)
                {
                    moves.Add(new XElement("Mov",
                        new XAttribute("RowNum", rowNum),
                        new XAttribute("Etg", tag.TagNum),
                        new XAttribute("Loc", tag.Location),
                        new XAttribute("MType", "on"),
                        new XAttribute("MDate", DateTime.Today.ToString("yyyy-MM-dd")),
                        new XAttribute("IWarn", "n")
                        ));
                    rowNum++;
                }

                xmlDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "true"));
                var regMoves = new XElement(
                    "RegMovs",
                    new XAttribute("RequestTimeStamp", docDate),
                    new XAttribute("ProgramVersion", "17.158"),
                    new XAttribute("ProgramName", "nouvem"),
                    new XAttribute("SchemaVersion", "1.0"));

                var auth = new XElement("Authentication",
                    new XElement("CTS_OL_User", new XAttribute("Usr", userName), new XAttribute("Pwd", userPassword)));

                regMoves.Add(auth);
                regMoves.Add(moves);
                xmlDocument.Add(regMoves);
            }

            if (canSave)
            {
                string toAimsPath = System.IO.Path.Combine(logPath, "toAIMS");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            var xmlString = new StringWriter();
            xmlDocument.Save(xmlString);

            return xmlString.ToString();
        }

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public string CreateXmlValidationRequestBCMS(string receiptNo)
        {
            var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                  "   <GetResults SchemaVersion=\"1.0\" ProgramName=\"nouvem\" ProgramVersion=\"17.158\"  RequestTimeStamp=\"2017-12-21T15:28:14-05:00\" TestScenarioId=\"112221\">" +
                  "    <Authentication>  " +
                  "      <CTS_OL_User Usr=\"111-111-111\" Pwd=\"m0nster\"/></Authentication> " +
                  "   <Receipt Num=\"440417\"/> " +
                  "</GetResults>";
            var xmlDocument = new XDocument();

            if (this.movementRequest != null)
            {
                var userName = this.movementRequest.UserName;
                var userPassword = this.movementRequest.UserPassword;
                var date = DateTime.Today.ToString("yy-MM-dd");
                var time = DateTime.Now.ToString("HH:mm:ss");
                var docDate = string.Format("{0}T{1}", date, time);

                xmlDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "true"));
                var regMoves = new XElement(
                    "GetResults",
                    new XAttribute("RequestTimeStamp", docDate),
                    new XAttribute("ProgramVersion", "17.158"),
                    new XAttribute("ProgramName", "nouvem"),
                    new XAttribute("SchemaVersion", "1.0"));

                var auth = new XElement("Authentication",
                    new XElement("CTS_OL_User", new XAttribute("Usr", userName), new XAttribute("Pwd", userPassword)));

                var receipt = new XElement("Receipt",new XAttribute("Num", receiptNo));

                regMoves.Add(auth);
                regMoves.Add(receipt);
                xmlDocument.Add(regMoves);
            }

            var xmlString = new StringWriter();
            xmlDocument.Save(xmlString);

            return xmlString.ToString();
        }

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public XmlDocument CreateXmlRequest(string fileName = "", bool canSave = false, string logPath = "")
        {
            var xmlDocument = new XDocument();
            var aimCountryCodes = new List<string>();

            if (this.movementRequest != null)
            {
                // parse the country codes string
                if (this.movementRequest.CountryCode != null)
                {
                    string[] codes = this.movementRequest.CountryCode.Split(',');

                    if (codes.Length == 3)
                    {
                        aimCountryCodes.Add(codes[0]);
                        aimCountryCodes.Add(codes[1]);
                        aimCountryCodes.Add(codes[2]);
                    }
                    else if (codes.Length == 2)
                    {
                        aimCountryCodes.Add(codes[0]);
                        aimCountryCodes.Add(codes[1]);
                    }
                    else if (codes.Length == 1)
                    {
                        aimCountryCodes.Add(codes[0]);
                    }
                }


                xmlDocument = new XDocument(
                new XDeclaration(this.version, this.encoding, "true"),
                new XElement(
                    "batchJob",
                        new XAttribute("senderId", this.movementRequest.SenderId != null ? this.movementRequest.SenderId : string.Empty),
                        new XAttribute("fileName", this.movementRequest.FileName != null ? this.movementRequest.FileName : string.Empty),
                        new XAttribute("dateGen", this.movementRequest.DateGen != null ? this.movementRequest.DateGen : string.Empty),
                        new XAttribute("numTrans", this.movementRequest.NumTrans != null ? this.movementRequest.NumTrans : string.Empty)));

                if (xmlDocument != null)
                {
                    var newNode = new XElement(
                        "batchTrans",
                        new XAttribute("transType", this.movementRequest.TransType != null ? this.movementRequest.TransType : string.Empty),
                        new XElement(
                            "movementRequest",
                            new XElement("speciesId", this.movementRequest.SpeciesId != null ? this.movementRequest.SpeciesId : string.Empty),
                            new XElement("movedFromId", this.movementRequest.MovedFromId != null ? this.movementRequest.MovedFromId : string.Empty),
                            new XElement("moveDate", this.movementRequest.MoveDate != null ? this.movementRequest.MoveDate : string.Empty),
                            new XElement("lotNum", this.movementRequest.LotNum != null ? this.movementRequest.LotNum : string.Empty),
                            new XElement("softwareId", this.movementRequest.SoftwareId != null ? this.movementRequest.SoftwareId : string.Empty)
                           ));

                    // Add in the country codes.
                    foreach (var code in aimCountryCodes)
                    {
                        newNode.Element("movementRequest").Add(new XElement("countryCode", code));
                    }

                    // Add in the remaining tags.
                    newNode.Element("movementRequest").Add(new XElement("numTags", this.movementRequest.NumTags != null ? this.movementRequest.NumTags : string.Empty));
                    newNode.Element("movementRequest").Add(new XElement("lotAction", this.movementRequest.LotAction != null ? this.movementRequest.LotAction : string.Empty));

                    if (this.movementRequest.MovementTag != null)
                    {
                        // we add all the movements for this request
                        foreach (MovementTag tag in this.movementRequest.MovementTag)
                        {
                            var tagNode = new XElement(
                                "movementTag",
                                new XElement("tagNum", tag.TagNum != null ? tag.TagNum : string.Empty),
                                new XElement("softwareAnimalId", tag.SoftwareAnimalId != null ? tag.SoftwareAnimalId : string.Empty),
                                new XElement("animalAction", tag.TagNum != null ? tag.AnimalAction : string.Empty));

                            newNode.Element("movementRequest").Add(tagNode);
                        }
                    }

                    xmlDocument.Element("batchJob").Add(newNode);
                }
            }

            if (canSave)
            {
                string toAimsPath = System.IO.Path.Combine(logPath, "toAims");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            return xmlDocument.ToXmlDocument();
        }

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public XmlDocument CreateSheepXmlRequest(string fileName = "", bool canSave = false, string logPath = "")
        {
            var xmlDocument = new XDocument();
            if (this.movementRequest != null)
            {
                xmlDocument = new XDocument(
                new XDeclaration(this.version, this.encoding, "true"),
                new XElement(
                    "batchJob",
                        new XAttribute("senderId", this.movementRequest.SenderId != null ? this.movementRequest.SenderId : string.Empty),
                        new XAttribute("fileName", this.movementRequest.FileName != null ? this.movementRequest.FileName : string.Empty),
                        new XAttribute("dateGen", this.movementRequest.DateGen != null ? this.movementRequest.DateGen : string.Empty),
                        new XAttribute("numTrans", this.movementRequest.NumTrans != null ? this.movementRequest.NumTrans : string.Empty)));

                if (xmlDocument != null)
                {
                    var newNode = new XElement(
                        "batchTrans",
                        new XAttribute("transType", this.movementRequest.TransType != null ? this.movementRequest.TransType : string.Empty),
                        new XElement(
                            "movementRequest",
                            new XElement("speciesId", this.movementRequest.SpeciesId != null ? this.movementRequest.SpeciesId : string.Empty),
                            new XElement("movedFromId", this.movementRequest.MovedFromId != null ? this.movementRequest.MovedFromId : string.Empty),
                            new XElement("moveDate", this.movementRequest.MoveDate != null ? this.movementRequest.MoveDate : string.Empty),
                            new XElement("lotNum", this.movementRequest.LotNum != null ? this.movementRequest.LotNum : string.Empty),
                            new XElement("numTags", this.movementRequest.NumTags != null ? this.movementRequest.NumTags : string.Empty),
                            new XElement("numTagsDead", this.movementRequest.NumTagsDead.ToString()),
                            new XElement("numTagsClpCatB", this.movementRequest.NumTagsB.ToString()),
                            new XElement("numTagsClpCatC", this.movementRequest.NumTagsC.ToString()),
                            new XElement("lotAction", this.movementRequest.LotAction != null ? this.movementRequest.LotAction : string.Empty),
                            new XElement("dispatchDocSerialNum", this.movementRequest.PermitNo != null ? this.movementRequest.PermitNo : string.Empty)
                           ));

                    if (this.movementRequest.MovementTag != null)
                    {
                        // we add all the movements for this request
                        foreach (MovementTag tag in this.movementRequest.MovementTag)
                        {
                            var scanIndicator = string.IsNullOrEmpty(tag.ScannedIndicator) ? "N" : tag.ScannedIndicator;
                            var tagNode = new XElement(
                                "movementTag",
                                new XElement("tagNum", tag.TagNum != null ? tag.TagNum : string.Empty),
                                new XElement("animalAction", tag.TagNum != null ? tag.AnimalAction : string.Empty),
                                new XElement("scannedInd", scanIndicator)
                                );

                            if (scanIndicator == "N")
                            {
                                var indicator = new XElement("nonEidTagInd",3);
                                tagNode.Add(indicator);
                            }

                            newNode.Element("movementRequest").Add(tagNode);
                        }
                    }

                    xmlDocument.Element("batchJob").Add(newNode);
                }
            }

            if (canSave)
            {
                string toAimsPath = System.IO.Path.Combine(logPath, "toAims");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            return xmlDocument.ToXmlDocument();
        }


        /// <summary>
        /// Extract the MovementRequest object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A MovementRequest object detailing the service call response.</returns>
        public MovementRequest ParseXmlResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var movementsLocal = MovementRequest.CreateNewMovementRequest();

            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var linqToXmlDocument = XDocument.Parse(xmlResponse);

                if (linqToXmlDocument != null && this.movementRequest != null)
                {
                    // Load the individual sections as the return format varies depending on the response.
                    var jobDescendants = linqToXmlDocument.Descendants("batchJobResponse");
                    var transDescendants = linqToXmlDocument.Descendants("batchTransResponse");
                    var movementResponseDescendants = linqToXmlDocument.Descendants("movementResponse");
                    var moveTagResponseDescendants = linqToXmlDocument.Descendants("moveTagResponse");
                    // var countryEligibilityDescendants = linqToXmlDocument.Descendants("countryEligibility");
                    // var residueDetailsDescendants = linqToXmlDocument.Descendants("residueDetails");

                    if (jobDescendants.Any() && transDescendants.Any() && movementResponseDescendants.Any() && moveTagResponseDescendants.Any())
                    {
                        movementsLocal = (from itemJob in jobDescendants
                                          from itemTrans in transDescendants
                                          from itemMovementResponse in movementResponseDescendants
                                          select MovementRequest.CreateMovementRequest(
                                             itemJob.FindInElement("fileName"),
                                             itemJob.FindInElement("dateGen"),
                                             itemJob.FindInElement("numTrans"),
                                             itemJob.FindInElement("senderId"),
                                             itemTrans.FindInElement("transType"),
                                             itemMovementResponse.FindInElement("speciesId"),
                                             string.Empty,
                                             string.Empty,
                                             itemMovementResponse.FindInElement("lotNum"),
                                             itemMovementResponse.FindInElement("softwareId"),
                                             string.Empty,
                                             itemMovementResponse.FindInElement("numTags"),
                                             string.Empty,
                                             itemTrans.FindInElement("status"),
                                             itemMovementResponse.FindInElement("inwardMovementRefId"),
                                             itemMovementResponse.FindInElement("headerStatus")
                                             )).First<MovementRequest>();

                        // get the list of country eligibility in the response file.
                        var countryEligibilityElements = (from XElement element in transDescendants.Descendants()
                                                          where (element.Name.LocalName.ToString() == "countryEligibility")
                                                          select element).ToList();

                        if (countryEligibilityElements != null && countryEligibilityElements.Count > 0)
                        {
                            for (int i = 0; i < countryEligibilityElements.Count; i++)
                            {
                                var element = countryEligibilityElements.ElementAt(i);
                                movementsLocal.AddCountryEligibility(element.FindInElement("countryCode"), element.FindInElement("value"));
                            }
                        }

                        // get the list of residue details in the response file.
                        var residueDetailsElements = (from XElement element in movementResponseDescendants.Descendants()
                                                      where (element.Name.LocalName.ToString() == "residue")
                                                      select element).ToList();

                        if (residueDetailsElements != null && residueDetailsElements.Count > 0)
                        {
                            for (int i = 0; i < residueDetailsElements.Count; i++)
                            {
                                var element = residueDetailsElements.ElementAt(i);
                                movementsLocal.AddResidueDetails(element.FindInElement("code"), element.FindInElement("value"));
                            }
                        }

                        // get the list of movement elements in the response file
                        var moveTagElements = (from XElement element in movementResponseDescendants.Descendants()
                                               where (element.Name.LocalName.ToString() == "moveTagResponse")
                                               select element).ToList();

                        if (moveTagElements != null && moveTagElements.Count > 0)
                        {
                            for (int i = 0; i < moveTagElements.Count; i++)
                            {
                                var moveTagTestData = new List<MoveTagTestData>();
                                var movementsData = new List<Movement>();
                                MovementHistoryResponse movementHistoryResponse;

                                // we have our moveTagElement
                                var moveTagElement = moveTagElements.ElementAt(i);

                                // get the list of MoveTagTestData objects within the MoveTagElement
                                var moveTagTestDataElements = (from XElement element in movementResponseDescendants.Descendants()
                                                               where (element.Name.LocalName.ToString() == "moveTagTestData")
                                                               select element).ToList();

                                for (int j = 0; j < moveTagTestDataElements.Count; j++)
                                {
                                    var moveTagTestDataElement = moveTagTestDataElements.ElementAt(j);
                                    moveTagTestData.Add(new MoveTagTestData { TestTypeCode = moveTagTestDataElement.FindInElement("testTypeCode"), TestDate = moveTagTestDataElement.FindInElement("TestDate") });
                                }

                                var movementHistoryResponseDescendants = moveTagElement.Descendants();

                                // get the list of movement elements in the response file
                                var movementsDataElements = (from XElement element in movementHistoryResponseDescendants.Descendants()
                                                             where (element.Name.LocalName.ToString() == "movementsData")
                                                             select element).ToList();

                                for (int xx = 0; xx < movementsDataElements.Count; xx++)
                                {
                                    var movementDataElement = movementsDataElements.ElementAt(xx);
                                    movementsData.Add(new Movement
                                    {
                                        MoveFromID = movementDataElement.FindInElement("movedFromId"),
                                        MoveToID = movementDataElement.FindInElement("movedToId")
                                                                                       ,
                                        MovementDate = movementDataElement.FindInElement("movementDate")
                                    });
                                }

                                // get the MovementHistoryResponse object associated with this MoveTagResponse element
                                movementHistoryResponse = new MovementHistoryResponse
                                {
                                    TagNum = moveTagElement.FindInElement("tagNum"),
                                    HerdOfOrigin = moveTagElement.FindInElement("herdOfOrigin"),
                                    Movements = movementsData
                                };

                                movementsLocal.AddMoveTagResponse(
                                                                  moveTagElement.FindInElement("tagNum"),
                                                                  moveTagElement.FindInElement("dob"),
                                                                  moveTagElement.FindInElement("breed"),
                                                                  moveTagElement.FindInElement("sex"),
                                                                  moveTagElement.FindInElement("herdOfOrigin"),
                                                                  moveTagElement.FindInElement("damEvent"),
                                                                  moveTagElement.FindInElement("tagNumDam"),
                                                                  moveTagElement.FindInElement("tseTestInd"),
                                                                  moveTagElement.FindInElement("movementAnimalId"),
                                                                  moveTagElement.FindInElement("softwareAnimalId"),
                                                                  moveTagElement.FindInElement("animalStatus"),
                                                                  moveTagElement.FindInElement("currentLocation"),
                                                                  moveTagTestData,
                                                                  movementHistoryResponse);
                            }
                        }

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            movementsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // get, and decipher, the ams error/s
                        var aimsErrors = linqToXmlDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            movementsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                    else
                    {
                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            movementsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // get, and decipher, the ams error/s
                        var aimsErrors = linqToXmlDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            movementsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                    }
                }

                if (canSave)
                {
                    string toAimsPath = System.IO.Path.Combine(logPath, "fromAims");

                    if (Directory.Exists(toAimsPath))
                    {
                        string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                        linqToXmlDocument.Save(fullPath);
                    }
                }
            }

            return movementsLocal;
        }

        /// <summary>
        /// Extract the MovementRequest object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A MovementRequest object detailing the service call response.</returns>
        public MovementRequest ParseXmlSheepResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var movementsLocal = MovementRequest.CreateNewMovementRequest();

            //xmlResponse =
            //    "<batchJobResponse dateGen=\"2019-02-25\" fileName=\"1551111451\" numTrans=\"1\">" +
            //    "<batchTransResponse transType=\"MOV\">" +
            //    "<status>E</status>" +
            //    "<movementResponse>" +
            //    "<lotNum>55014</lotNum>" +
            //    "<inwardMovementRefId>572326238</inwardMovementRefId>" +
            //    "<headerStatus>E</headerStatus>" +
            //    "<dispatchDocSerialNum>60470-0127</dispatchDocSerialNum>" +
            //    "<moveTagResponse>" +
            //    "<tagNum>372036047000156</tagNum>" +
            //    "<animalStatus>E</animalStatus>" +
            //    "<amsError>548</amsError>" +
            //    "</moveTagResponse>" +
            //    "</movementResponse>" +
            //    "</batchTransResponse>" +
            //    "</batchJobResponse> ";

            //xmlResponse =
            //    "<batchJobResponse dateGen=\"2019-05-31\"fileName=\"MOV-F268-0190531-00009488.xml\" numTrans=\"1\" senderId=\"F268\">" +
            //    "<batchTransResponse transType =\"MOV\">" +
            //    "<status>A</status>" +
            //    "<movementResponse>" +
            //    "<speciesId>4</speciesId>"+
            //    "<lotNum>176742</lotNum>"+
            //    "<inwardMovementRefId>572326238</inwardMovementRefId>"+
            //    "<headerStatus>M</headerStatus>"+
            //    "<numTags>22</numTags>"+
            //    "<numTagsDead>0</numTagsDead>"+
            //    "<numTagsClpCatB>0</numTagsClpCatB>"+
            //    "<numTagsClpCatC>0</numTagsClpCatC>"+
            //    "<dispatchDocSerialNum>28989-0052</dispatchDocSerialNum>"+
            //    "<moveTagResponse>"+
            //    "<tagNum>372042898902160</tagNum>"+
            //    "<movementAnimalId>6424</movementAnimalId>"+
            //    "<animalStatus>A</animalStatus>"+
            //    "<scannedInd>Y</scannedInd>"+
            //    "</moveTagResponse>"+
            //    "<moveTagResponse>"+
            //    "<tagNum>372042898903008</tagNum>"+
            //    "<movementAnimalId>6425</movementAnimalId>"+
            //    "<animalStatus>A</animalStatus>"+
            //    "<scannedInd>N</scannedInd>"+
            //    "<nonEidTagInd>3</nonEidTagInd>"+
            //    "</moveTagResponse>"+
            //    "</movementResponse>"+
            //    "</batchTransResponse>"+
            //    "</batchJobResponse>";

           // xmlResponse =
              //  "<batchJobResponse dateGen=\"2019-05-31\" fileName=\"MOV-F268-20190531-00009488.xml\" numTrans=\"1\" senderId=\"F268\"><batchTransResponse transType=\"MOV\"><status>A</status><movementResponse><speciesId>4</speciesId><lotNum>176742</lotNum><inwardMovementRefId>572326238</inwardMovementRefId><headerStatus>M</headerStatus><numTags>22</numTags><numTagsDead>0</numTagsDead><numTagsClpCatB>0</numTagsClpCatB><numTagsClpCatC>0</numTagsClpCatC><dispatchDocSerialNum>28989-0052</dispatchDocSerialNum><moveTagResponse><tagNum>372042898902160</tagNum><movementAnimalId>6424</movementAnimalId><animalStatus>A</animalStatus><scannedInd>Y</scannedInd></moveTagResponse><moveTagResponse><tagNum>372042898903008</tagNum><movementAnimalId>6425</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903023</tagNum><movementAnimalId>6426</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903026</tagNum><movementAnimalId>6427</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903018</tagNum><movementAnimalId>6428</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903019</tagNum><movementAnimalId>6429</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903027</tagNum><movementAnimalId>6430</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903024</tagNum><movementAnimalId>6431</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903025</tagNum><movementAnimalId>6432</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903021</tagNum><movementAnimalId>6433</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903034</tagNum><movementAnimalId>6434</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903033</tagNum><movementAnimalId>6435</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903020</tagNum><movementAnimalId>6436</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903022</tagNum><movementAnimalId>6437</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903030</tagNum><movementAnimalId>6438</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903029</tagNum><movementAnimalId>6439</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903037</tagNum><movementAnimalId>6440</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903031</tagNum><movementAnimalId>6441</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903035</tagNum><movementAnimalId>6442</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903036</tagNum><movementAnimalId>6443</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903032</tagNum><movementAnimalId>6444</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse><moveTagResponse><tagNum>372042898903028</tagNum><movementAnimalId>6445</movementAnimalId><animalStatus>A</animalStatus><scannedInd>N</scannedInd><nonEidTagInd>3</nonEidTagInd></moveTagResponse></movementResponse></batchTransResponse></batchJobResponse>\r\n";



            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var linqToXmlDocument = XDocument.Parse(xmlResponse);

                if (linqToXmlDocument != null && this.movementRequest != null)
                {
                    // Load the individual sections as the return format varies depending on the response.
                    var jobDescendants = linqToXmlDocument.Descendants("batchJobResponse");
                    var transDescendants = linqToXmlDocument.Descendants("batchTransResponse");
                    var movementResponseDescendants = linqToXmlDocument.Descendants("movementResponse");
                    var moveTagResponseDescendants = linqToXmlDocument.Descendants("moveTagResponse");

                    if (jobDescendants.Any() && transDescendants.Any() && movementResponseDescendants.Any() && moveTagResponseDescendants.Any())
                    {
                        movementsLocal = (from itemJob in jobDescendants
                                          from itemTrans in transDescendants
                                          from itemMovementResponse in movementResponseDescendants
                                          select MovementRequest.CreateMovementRequest(
                                             itemJob.FindInElement("fileName"),
                                             itemJob.FindInElement("dateGen"),
                                             itemJob.FindInElement("numTrans"),
                                             itemJob.FindInElement("senderId"),
                                             itemTrans.FindInElement("transType"),
                                             string.Empty,
                                             string.Empty,
                                             string.Empty,
                                             itemMovementResponse.FindInElement("lotNum"),
                                             string.Empty,
                                             string.Empty,
                                             itemMovementResponse.FindInElement("numTags"),
                                             string.Empty,
                                             itemTrans.FindInElement("status"),
                                             itemMovementResponse.FindInElement("inwardMovementRefId"),
                                             itemMovementResponse.FindInElement("headerStatus")
                                             )).First<MovementRequest>();

                        // get the list of movement elements in the response file
                        var moveTagElements = (from XElement element in movementResponseDescendants.Descendants()
                                               where (element.Name.LocalName.ToString() == "moveTagResponse")
                                               select element).ToList();

                        if (moveTagElements != null && moveTagElements.Count > 0)
                        {
                            for (int i = 0; i < moveTagElements.Count; i++)
                            {
                                var moveTagTestData = new List<MoveTagTestData>();

                                // we have our moveTagElement
                                var moveTagElement = moveTagElements.ElementAt(i);
                                movementsLocal.AddMoveTagResponse(
                                                                  moveTagElement.FindInElement("tagNum"),
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  string.Empty,
                                                                  moveTagElement.FindInElement("animalStatus"),
                                                                  string.Empty,
                                                                  moveTagTestData,
                                                                  new MovementHistoryResponse(),
                                                                  moveTagElement.FindInElement("amsError"));
                            }
                        }
                    }
                    else
                    {
                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            movementsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // get, and decipher, the ams error/s
                        var aimsErrors = linqToXmlDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            movementsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item.RemoveNonIntegers().ToString())
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                    }
                }

                if (canSave)
                {
                    string toAimsPath = System.IO.Path.Combine(logPath, "fromAims");

                    if (Directory.Exists(toAimsPath))
                    {
                        string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                        linqToXmlDocument.Save(fullPath);
                    }
                }
            }

            return movementsLocal;
        }

        #endregion
    }
}                    


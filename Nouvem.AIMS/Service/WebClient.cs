﻿// -----------------------------------------------------------------------
// <copyright file="WebClient.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Service
{
    using System;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Xml;

    /// <summary>
    /// Implementation class used to call the AIMs service.
    /// </summary>
    public class WebClient 
    {
        #region Fields

        /// <summary>
        /// The url to which the request will be sent.
        /// </summary>
        private string url;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WebClient"/> class.
        /// </summary>
        /// <param name="url">The address to which the request will be sent.</param>
        public WebClient(string url)
        {
            this.url = url;
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Submit an xml web request.
        /// </summary>
        /// <param name="xmlMessage">The xml message to transmit.</param>
        /// <param name="certificate">The security certificate required to post the message to the url.</param>
        /// <param name="contentType">The content type of the message, "text/xml; encoding='utf-8'" by default.</param>
        /// <returns>A response from the host in the form of an XmlDocument object.</returns>
        public XmlDocument PostXMLTransaction(XmlDocument xmlMessage, X509Certificate certificate = null, string contentType = "text/xml; encoding='utf-8'", int timeout = 40000)
        {
            // Declare XMLResponse document.
            XmlDocument xmlResponse = null;

            // Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebRequest httpWebRequest;

            // Declare an HTTP-specific implementation of the WebResponse class
            HttpWebResponse httpWebResponse = null;

            // Declare a generic view of a sequence of bytes
            Stream requestStream = null;
            Stream responseStream = null;

            // Declare XMLReader
            XmlTextReader xmlReader;

            try
            {
                // Check that the url is not null or empty. Note we cannot load this in a web client.
                if (string.IsNullOrEmpty(this.url))
                {
                    throw new ArgumentNullException("Argument url cannot be null or empty.");
                }

                // Check that we have a valid xml doc first.
                if (xmlMessage == null)
                {
                    throw new ArgumentNullException("Argument xmlMessage cannot be null.");
                }

                // Creates an HttpWebRequest for the specified URL and add the certificate.
                httpWebRequest = (HttpWebRequest)WebRequest.Create(this.url);

                // If we have a certificate add it.
                if (certificate != null)
                {
                    httpWebRequest.ClientCertificates.Add(certificate);
                }

                // Set HttpWebRequest properties, method will always be 'Post'.
                var bytes = System.Text.Encoding.ASCII.GetBytes(xmlMessage.InnerXml);
                httpWebRequest.Method = "Post";
                httpWebRequest.ContentLength = bytes.Length;
                httpWebRequest.ContentType = contentType;
                httpWebRequest.Timeout = timeout;
                ServicePointManager.Expect100Continue = true;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
          
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                // Get Stream object
                requestStream = httpWebRequest.GetRequestStream();

                // Writes a sequence of bytes to the current stream
                requestStream.Write(bytes, 0, bytes.Length);

                // Close stream
                requestStream.Close();

                // Sends the HttpWebRequest, and waits for a response.
                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                // Start HttpResponse
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    // Get response stream
                    responseStream = httpWebResponse.GetResponseStream();

                    // Load response stream into XMLReader
                    xmlReader = new XmlTextReader(responseStream);

                    // Declare XMLDocument
                    var xmldoc = new XmlDocument();
                    xmldoc.Load(xmlReader);

                    // Set XMLResponse object returned from XMLReader
                    xmlResponse = xmldoc;

                    // Close XMLReader
                    xmlReader.Close();
                }

                // Close HttpWebResponse
                httpWebResponse.Close();
            }
            catch (Exception postXMLTransactionEx)
            {
                throw new Exception(postXMLTransactionEx.ToString());
            }
            finally
            {
                // Close connections
                if (requestStream != null)
                {
                    requestStream.Close();
                }

                if (responseStream != null)
                {
                    responseStream.Close();
                }

                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                }

                // Release objects
                xmlReader = null;
                requestStream = null;
                responseStream = null;
                httpWebResponse = null;
                httpWebRequest = null;
            }

            return xmlResponse;
        }

        public Stream StringToStream(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public byte[] ReadFully(Stream stream, int initialLength = -1)
        {
            // If we've been passed an unhelpful initial length, just
            // use 32K.
            if (initialLength < 1)
            {
                initialLength = 32768;
            }

            byte[] buffer = new byte[initialLength];
            int read = 0;

            int chunk;
            while ((chunk = stream.Read(buffer, read, buffer.Length - read)) > 0)
            {
                read += chunk;

                // If we've reached the end of our buffer, check to see if there's
                // any more information
                if (read == buffer.Length)
                {
                    int nextByte = stream.ReadByte();

                    // End of stream? If so, we're done
                    if (nextByte == -1)
                    {
                        return buffer;
                    }

                    // Nope. Resize the buffer, put in the byte we've just
                    // read, and continue
                    byte[] newBuffer = new byte[buffer.Length * 2];
                    Array.Copy(buffer, newBuffer, buffer.Length);
                    newBuffer[read] = (byte)nextByte;
                    buffer = newBuffer;
                    read++;
                }
            }
            // Buffer is now too big. Shrink it.
            byte[] ret = new byte[read];
            Array.Copy(buffer, ret, read);
            return ret;
        }

        public String EncodeData(byte[] data)
        {
            return Convert.ToBase64String(data);
        }

        /// <summary>
        /// Retrieve a security cert which may be required for posting the transaction.
        /// </summary>
        /// <param name="cerificatetPath">The path at which the certificate file is stored on the file system.</param>
        /// <param name="password">The password required to open the certificate file.</param>
        /// <returns>An X509Certificate to be used for http post operations.</returns>
        public X509Certificate FindSecurityCert(string cerificatetPath, string password)
        {
            var certificate = new X509Certificate();

            // Read in the certificate file from the path as a byte array.
            var rawCertificateData = File.ReadAllBytes(cerificatetPath);

            // Import the certificate into the object, if we have data.
            if (rawCertificateData.Length != 0)
            {
                certificate.Import(rawCertificateData, password, X509KeyStorageFlags.PersistKeySet);
            }

            return certificate;
        }

        #endregion Public Methods
    }
}
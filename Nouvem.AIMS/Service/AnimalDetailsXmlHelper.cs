﻿// -----------------------------------------------------------------------
// <copyright file="Errors.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.AIMS.Model;

namespace Nouvem.AIMS.Service
{
    using System;
    using System.Linq;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using Nouvem.AIMS.BusinessLogic;
    using Nouvem.Shared;

    /// <summary>
    /// Helper class for the web service calls which is used to generate and parse xml animal details requests.
    /// </summary>
    public class AnimalDetailsXmlHelper
    {
        #region Fields

        /// <summary>
        /// Instance of the designator object.
        /// </summary>
        private AnimalDetail designator;

        /// <summary>
        /// The version of the xml being sent.
        /// </summary>
        private string version;

        /// <summary>
        /// The encoding of the xml being sent.
        /// </summary>
        private string encoding;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimalDetailsXmlHelper"/> class.
        /// </summary>
        /// <param name="designator">The <see cref="AnimalDetailsXmlHelper"/> to be processed for transmission to AIMs.</param>
        /// <param name="version">The version of the xml file which defaults to 1.0.</param>
        /// <param name="encoding">The encoding of the xml file which defaults to UTF-8.</param>
        public AnimalDetailsXmlHelper(AnimalDetail designator, string version = "1.0", string encoding = "UTF-8")
        {
            this.designator = designator;
            this.version = version;
            this.encoding = encoding;
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public string CreateXmlRequestBCMS(string fileName = "", bool canSave = false, string logPath = "")
        {
            var xmlDocument = new XDocument();

            if (this.designator != null)
            {
                var eartag = this.designator.TagNum;
                var userName = this.designator.UserName;
                var userPassword = this.designator.UserPassword;
                var date = DateTime.Today.ToString("yy-MM-dd");
                var time = DateTime.Now.ToString("HH:mm:ss");
                var docDate = string.Format("{0}T{1}", date, time);

                xmlDocument = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "true"),
                    new XElement(
                        "RegMovs",
                        new XAttribute("RequestTimeStamp", docDate),
                        new XAttribute("ProgramVersion", "17.158"),
                        new XAttribute("ProgramName", "nouvem"),
                        new XAttribute("SchemaVersion", "1.0"),
                        new XElement("Authentication", 
                            new XElement("CTS_OL_User", new XAttribute("Usr", userName),new XAttribute("Pwd", userPassword))),
                        new XElement("Eartags", 
                            new XElement("Eartag_Id",eartag))
                    )
                );
            }

            if (canSave)
            {
                string toAimsPath = System.IO.Path.Combine(logPath, "toAIMS");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            var xmlString = new StringWriter();
            xmlDocument.Save(xmlString);

            return xmlString.ToString(); 
        }

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public XmlDocument CreateXmlRequest(string fileName = "", bool canSave = false, string logPath = "")
        {
            var xmlDocument = new XDocument();

            if (this.designator != null)
            {
                xmlDocument = new XDocument(
                new XDeclaration(this.version, this.encoding, "true"),
                new XElement(
                    "batchJob",
                        new XAttribute("fileName", this.designator.FileName != null ? this.designator.FileName : string.Empty),
                        new XAttribute("dateGen", this.designator.DateGen != null ? this.designator.DateGen : string.Empty),
                        new XAttribute("numTrans", this.designator.NumTrans != null ? this.designator.NumTrans : string.Empty)));

                if (xmlDocument != null)
                {
                    var newNode = new XElement(
                        "batchTrans",
                        new XAttribute("transType", this.designator.TransType != null ? this.designator.TransType : string.Empty),
                        new XElement(
                            "animalDetailsRequest",
                            new XElement("speciesId", this.designator.SpeciesID != null ? this.designator.SpeciesID : string.Empty),
                            new XElement("tagNum", this.designator.TagNum != null ? this.designator.TagNum : string.Empty)));

                    xmlDocument.Element("batchJob").Add(newNode);
                }
            }

            if (canSave)
            {
                string toAimsPath = System.IO.Path.Combine(logPath, "toAims");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            return xmlDocument.ToXmlDocument();
        }

        /// <summary>
        /// Extract the AnimalDetails object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A AnimalDetails object detailing the service call response.</returns>
        public AnimalDetail ParseXmlResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var detailsLocal = AnimalDetail.CreateNewAnimalDetails();

            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var xDocument = XDocument.Parse(xmlResponse);

                if (xDocument != null && this.designator != null)
                {
                    // Load the individual sections as the return format varies depending on the response.
                    var jobDescendants = xDocument.Descendants("batchJobResponse");
                    var transDescendants = xDocument.Descendants("batchTransResponse");
                    var animalDescendants = xDocument.Descendants("animalDetailsResponse");
                    var moveTagDescendants = xDocument.Descendants("moveTagResponse");
                    var movementHistoryDescendants = xDocument.Descendants("movementHistoryResponse");
                    //var movementDataDescendants = xDocument.Descendants("movementsData");

                    if (jobDescendants.Any() && transDescendants.Any() && animalDescendants.Any() && moveTagDescendants.Any() && movementHistoryDescendants.Any())
                    {
                        detailsLocal = (from itemJob in jobDescendants
                                        from itemTrans in transDescendants
                                        from itemAnimal in animalDescendants
                                        from itemMoveTag in moveTagDescendants
                                        from itemMovementHistory in movementHistoryDescendants
                                        select AnimalDetail.CreateAnimalDetails(
                                           itemJob.FindInElement("fileName"),
                                           itemJob.FindInElement("dateGen"),
                                           itemJob.FindInElement("numTrans"),
                                           itemTrans.FindInElement("transType"),
                                           string.Empty,
                                           itemMoveTag.FindInElement("tagNum"),
                                           itemTrans.FindInElement("batchError"),
                                           itemTrans.FindInElement("status"),
                                           itemAnimal.FindInElement("lotNum"),
                                           itemAnimal.FindInElement("inwardMovementRefID"),
                                           itemMoveTag.FindInElement("dob"),
                                           itemMoveTag.FindInElement("breed"),
                                           itemMoveTag.FindInElement("sex"),
                                           itemMoveTag.FindInElement("damEvent"),
                                           itemMoveTag.FindInElement("tagNumDam"),
                                           itemMoveTag.FindInElement("tseTestInd"),
                                           itemMoveTag.FindInElement("movementAnimalID"),
                                           itemMoveTag.FindInElement("damBreed"),
                                           itemMoveTag.FindInElement("damDateOfBirth"),
                                            //itemMoveTag.FindInElement("herdOfOrigin"),
                                           itemJob.FindInElement("batchError"))).First<AnimalDetail>();

                        // Get, and decipher, the batch errors.
                        var batchErrors = xDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            detailsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // get, and decipher, the ams error/s
                        var aimsErrors = xDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            detailsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        var query = from herdElement in xDocument.Descendants("herdOfOrigin") select herdElement.Value;
                        foreach (var item in query)
                        {
                            // there'll only ever be 1 herd of origin value item.
                            detailsLocal.HerdOfOrigin = item;
                        }

                        //get the list of movement elements in the response file
                        var elems = (from XElement element in movementHistoryDescendants.Descendants()
                                     where (element.Name.LocalName.ToString() == "movementsData")
                                     select element).ToList();

                        if (elems.Count > 0)
                        {
                            for (int i = 0; i < elems.Count; i++)
                            {
                                var element = elems.ElementAt(i);
                                detailsLocal.AddMovements(element.FindInElement("movedFromId"), element.FindInElement("movedToId"), element.FindInElement("movementDate"));
                            }
                        }


                        //get the list of moveTagTestData elements in the response file
                        var tagTestElems = (from XElement element in moveTagDescendants.Descendants()
                                            where (element.Name.LocalName.ToString() == "moveTagTestData")
                                            select element).ToList();

                        if (tagTestElems.Count > 0)
                        {
                            for (int i = 0; i < tagTestElems.Count; i++)
                            {
                                var element = tagTestElems.ElementAt(i);
                                detailsLocal.AddMoveTagTestData(element.FindInElement("testTypeCode"), element.FindInElement("TestDate"));
                            }
                        }

                        // Gather the error/s.
                        detailsLocal.Error = string.Join(",", jobDescendants.Elements("batchError").Select(x => x.Value).ToList());

                    }
                    else if (jobDescendants.Any() && transDescendants.Any() && !animalDescendants.Any())
                    {
                        //catch the batch error
                        detailsLocal = detailsLocal = (from itemJob in jobDescendants
                                                       select AnimalDetail.CreateAnimalDetails(
                                                       itemJob.FindInElement("fileName"),
                                                       itemJob.FindInElement("dateGen"),
                                                       itemJob.FindInElement("numTrans"),
                                                       string.Empty,
                                                       string.Empty,
                                                       string.Empty,
                                                       itemJob.FindInElement("batchError"))).First<AnimalDetail>();

                        // Gather the error/s.
                        detailsLocal.Error = string.Join(",", jobDescendants.Elements("batchError").Select(x => x.Value).ToList());

                    }
                    //else if (jobDescendants.Any())
                    //{
                    //    //catch the batch error
                    //    detailsLocal = detailsLocal = (from itemJob in jobDescendants
                    //                                   select AnimalDetail.CreateAnimalDetails(
                    //                                   itemJob.FindInElement("fileName"),
                    //                                   itemJob.FindInElement("dateGen"),
                    //                                   itemJob.FindInElement("numTrans"),
                    //                                   string.Empty,
                    //                                   string.Empty,
                    //                                   string.Empty,
                    //                                   itemJob.FindInElement("batchError"))).First<AnimalDetail>();

                    //    // Gather the error/s.
                    //    detailsLocal.Error = string.Join(",", jobDescendants.Elements("batchError").Select(x => x.Value).ToList());

                    //}
                    else
                    {
                        // Get, and decipher, the batch errors.
                        var batchErrors = xDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            detailsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // get, and decipher, the ams error/s
                        var aimsErrors = xDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            detailsLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                }

                if (canSave)
                {
                    string toAimsPath = System.IO.Path.Combine(logPath, "fromAims");

                    if (Directory.Exists(toAimsPath))
                    {
                        string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                        xDocument.Save(fullPath);
                    }
                }
            }

            return detailsLocal;
        }

        /// <summary>
        /// Extract the AnimalDetails object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A AnimalDetails object detailing the service call response.</returns>
        public AnimalDetail ParseXmlBCMSResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var detailsLocal = AnimalDetail.CreateNewAnimalDetails();

            if (!string.IsNullOrEmpty(xmlResponse))
            {
                detailsLocal.AnimalMovements = new List<Movement>();
                var doc = XDocument.Parse(xmlResponse);

                var animal = doc.Descendants().FirstOrDefault(x => x.Name.LocalName == "AnimalDetails");
                if (animal == null)
                {
                    throw new Exception("Animal not found");
                }

                detailsLocal.DOB = animal.FindInElement("Dob");
                detailsLocal.Breed = animal.FindInElement("Brd");
                detailsLocal.Sex = animal.FindInElement("Sex");
                detailsLocal.TagNum = animal.FindInElement("Etg");
                detailsLocal.TagNumDam = animal.FindInElement("GdEtg");

                var movements = doc.Descendants().Where(x => x.Name.LocalName == "MovementDetails");

                foreach (var element in movements)
                {
                    var moveOn = element.Descendants().FirstOrDefault(x => x.Name.LocalName == "OnMovmt");
                    var moveOff = element.Descendants().FirstOrDefault(x => x.Name.LocalName == "OffMovmt");
                    detailsLocal.AnimalMovements.Add(new Movement
                    {
                        MoveFromID = moveOn != null ? moveOn.FindInElement("Loc") : string.Empty,
                        MoveToID = moveOff != null ? moveOff.FindInElement("Loc") : string.Empty,
                        MovementDate = moveOn != null ? moveOn.FindInElement("MDate") : string.Empty,
                        MovementOffDate = moveOff != null ? moveOff.FindInElement("MDate") : string.Empty,
                        SubLocation = moveOn != null ? moveOn.FindInElement("SLoc") : string.Empty
                    });
                }

                if (canSave)
                {
                    var fromAimsPath = System.IO.Path.Combine(logPath, "fromAIMS");
                    if (Directory.Exists(fromAimsPath))
                    {
                        var fullPath = System.IO.Path.Combine(fromAimsPath, fileName);
                        doc.Save(fullPath);
                    }
                }
            }

            return detailsLocal;
        }

        #endregion
    }
}
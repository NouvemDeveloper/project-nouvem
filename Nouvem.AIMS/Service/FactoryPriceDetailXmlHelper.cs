﻿// -----------------------------------------------------------------------
// <copyright file="FactoryPriceDetailXmlHelper.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Service
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using Nouvem.AIMS.BusinessLogic;
    using Nouvem.Shared;

    /// <summary>
    /// Helper class for the web service calls which is used to generate and parse xml factory price detail requests.
    /// </summary>
    public class FactoryPriceDetailXmlHelper
    {
        #region Fields

        /// <summary>
        /// Instance of the price detail object.
        /// </summary>
        private FactoryPriceDetail priceDetail;

        /// <summary>
        /// The version of the xml being sent.
        /// </summary>
        private string version;

        /// <summary>
        /// The encoding of the xml being sent.
        /// </summary>
        private string encoding;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FactoryPriceDetailXmlHelper"/> class.
        /// </summary>
        /// <param name="priceDetail">The <see cref="FactoryPriceDetailXmlHelper"/> to be processed for transmission to AIMs.</param>
        /// <param name="version">The version of the xml file which defaults to 1.0.</param>
        /// <param name="encoding">The encoding of the xml file which defaults to UTF-8.</param>
        public FactoryPriceDetailXmlHelper(FactoryPriceDetail priceDetail, string version = "1.0", string encoding = "UTF-8")
        {
            this.priceDetail = priceDetail;
            this.version = version;
            this.encoding = encoding;
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public XmlDocument CreateXmlRequest(string fileName = "", bool canSave = false, string logPath = null)
        {
            var xmlDocument = new XDocument();

            if (this.priceDetail != null)
            {
                xmlDocument = new XDocument(
                new XDeclaration(this.version, this.encoding, "true"),
                new XElement(
                    "batchJob",
                        new XAttribute("fileName", this.priceDetail.FileName != null ? this.priceDetail.FileName : string.Empty),
                        new XAttribute("dateGen", this.priceDetail.DateGen != null ? this.priceDetail.DateGen : string.Empty),
                        new XAttribute("numTrans", this.priceDetail.NumTrans != null ? this.priceDetail.NumTrans : string.Empty)));

                if (xmlDocument != null)
                {
                    var newNode = new XElement("batchTrans",
                                         new XAttribute("transType", this.priceDetail.TransType != null ? this.priceDetail.TransType : string.Empty),
                                         new XElement("factoryPriceDetailsRequest",
                                             new XElement("speciesId", this.priceDetail.SpeciesID != null ? this.priceDetail.SpeciesID : string.Empty),
                                             new XElement("officerCode", this.priceDetail.OfficerCode != null ? this.priceDetail.OfficerCode : string.Empty),
                                             new XElement("carcaseNum", this.priceDetail.CarcassNumber != null ? this.priceDetail.CarcassNumber : string.Empty),
                                             new XElement("category", this.priceDetail.Category != null ? this.priceDetail.Category : string.Empty),
                                             new XElement("conformation", this.priceDetail.Conformation != null ? this.priceDetail.Conformation : string.Empty),
                                             new XElement("fat", this.priceDetail.fat != null ? this.priceDetail.fat : string.Empty),
                                             new XElement("weight1", this.priceDetail.WeightSide1 != null ? this.priceDetail.WeightSide1 : string.Empty),
                                             new XElement("weight2", this.priceDetail.WeightSide2 != null ? this.priceDetail.WeightSide2 : string.Empty),
                                             new XElement("tagNum", this.priceDetail.TagNumber != null ? this.priceDetail.TagNumber : string.Empty),
                                             new XElement("carcaseDesc", this.priceDetail.CarcassDescription != null ? this.priceDetail.CarcassDescription : string.Empty),
                                             new XElement("price", this.priceDetail.Price != null ? this.priceDetail.Price : string.Empty),
                                             new XElement("factoryOwnershipInd", this.priceDetail.FactoryOwnershipID != null ? this.priceDetail.FactoryOwnershipID : string.Empty)
                                             ));

                    xmlDocument.Element("batchJob").Add(newNode);
                }
            }

            if (canSave)
            {
                // Save save if specified to do so
                string toAimsPath = System.IO.Path.Combine(logPath, "toAims");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            return xmlDocument.ToXmlDocument();
        }

        /// <summary>
        /// Extract the FactoryPriceDetail object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A HerdpriceDetail object detailing the service call response.</returns>
        public FactoryPriceDetail ParseXmlResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var priceDetailLocal = FactoryPriceDetail.CreateNewfactoryPriceDetail();

            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var linqToXmlDocument = XDocument.Parse(xmlResponse);

                if (linqToXmlDocument != null && this.priceDetail != null)
                {
                    // Load the individual sections as the return format varies depending on the response.
                    var jobDescendants = linqToXmlDocument.Descendants("batchJobResponse");
                    var transDescendants = linqToXmlDocument.Descendants("batchTransResponse");
                    var priceDetailDescendants = linqToXmlDocument.Descendants("factoryPriceDetailsResponse");

                    if (transDescendants.Any() && priceDetailDescendants.Any())
                    {
                        priceDetailLocal = (from itemJob in linqToXmlDocument.Descendants("batchJobResponse")
                                            select FactoryPriceDetail.CreatefactoryPriceDetail(
                                               itemJob.FindInElement("fileName"),
                                               itemJob.FindInElement("dateGen"),
                                               itemJob.FindInElement("numTrans"),
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               itemJob.FindInElement("tagNum"),
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               itemJob.FindInElement("status"),
                                               itemJob.FindInElement("batchError"))).FirstOrDefault();

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            priceDetailLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // Get, and decipher, the aims errors.
                        var aimsErrors = linqToXmlDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            priceDetailLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                    else
                    {
                        priceDetailLocal = (from itemJob in linqToXmlDocument.Descendants("batchJobResponse")
                                            select FactoryPriceDetail.CreatefactoryPriceDetail(
                                               itemJob.FindInElement("fileName"),
                                               itemJob.FindInElement("dateGen"),
                                               itemJob.FindInElement("numTrans"),
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               string.Empty,
                                               itemJob.FindInElement("batchError"))).FirstOrDefault();

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            priceDetailLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                        // Get, and decipher, the aims errors.
                        var aimsErrors = linqToXmlDocument.Descendants("amsError").Select(x => x.Value);

                        foreach (var item in aimsErrors)
                        {
                            priceDetailLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                }

                if (canSave)
                {
                    string toAimsPath = System.IO.Path.Combine(logPath, "fromAims");

                    if (Directory.Exists(toAimsPath))
                    {
                        string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                        linqToXmlDocument.Save(fullPath);
                    }
                }
            }

            return priceDetailLocal;
        }

        #endregion
    }
}

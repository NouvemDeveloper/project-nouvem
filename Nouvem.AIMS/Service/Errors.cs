﻿// -----------------------------------------------------------------------
// <copyright file="Errors.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Static class that holds the aims error codes and their respective meanings.
    /// </summary>
    public static class Errors
    {
        /// <summary>
        /// The collection of error codes and meanings.
        /// </summary>
        private static Dictionary<string, string> errorList = new Dictionary<string, string>();

        static Errors()
        {
            errorList.Add("098", "Invalid File Format");
            errorList.Add("099", "Unexpected Error");
            errorList.Add("056", "Filename cannot be >30 Characters long");
            errorList.Add("055", "Filename is already on file and a response has already been issued");
            errorList.Add("020", "Business Id in the Client Certificate is not recognised by the Department");
            errorList.Add("030", "Business Id in the Client Certificate is invalid for notifying transaction to AIM");
            errorList.Add("040", "Business Id in the Client Certificate is not currently valid for notifying transactions to AIM");
            errorList.Add("090", "The number of transaction exceeds the maximum allowed");
            errorList.Add("100", "The actual number of transaction in the file is not the same as the number of transactions specified");
            errorList.Add("070", "Date Generated must be less or equal to the current date");
            errorList.Add("150", "Invalid transType");
            errorList.Add("152", "Business Id in the Client CertierrorList.Addficate is not valid for notifying the input transType to AIM");
            errorList.Add("155", "Invalid Details for the transType");
            errorList.Add("115", "Invalid movement type");
            errorList.Add("119", "Species Id must  be and may only be provded where Lot Action = 'I'");
            errorList.Add("120", "Invalid Species Id");
            errorList.Add("140", "Business Id in the client certifcate is not authorised to use the AIM webservice for the quoted species");
            errorList.Add("E120", "Invalid Species Id");
            errorList.Add("E140", "Business Id in the client certifcate is not authorised to use the AIM webservice for the quoted species");
            errorList.Add("1000", "Lot Action where provided, must be equal to 'I'");
            errorList.Add("1002", "Animal Details must be provided for ovine EID intakes into a CPR; otherwise they cannot be provided");
            errorList.Add("1010", "Inward Movement Ref Id must be provided where Lot Action is other than 'I'");
            errorList.Add("1012", "Inward Movement Ref Id cannot be filled where Lot Action = 'I' and an intake movement is being created");
            errorList.Add("1022", "Moved From Id may only be provided where Lot Action = 'I'");
            errorList.Add("1024", "Moved From Id must be filled where Lot Action = 'I'");
            errorList.Add("1025", "Moved From Id, if filled, must be alphanumeric and 2-8 long");
            errorList.Add("1044", "Third Party Id can only be entered where Lot Action = 'l'");
            errorList.Add("1045", "Third Party Id, if entered, must be alphanumeric 2-8 long");
            errorList.Add("1046", "Third Party Id may not be the same as Moved From Id or Client Cert Sender Id");
            errorList.Add("1050", "Movement Date must be entered where Lot Action = 'I'");
            errorList.Add("1052", "Movement Date may only be entered where Lot Action = 'I'");
            errorList.Add("1054", "Movement Date provided is later than the Date generated in the file");
            errorList.Add("1055", "Movement Date, where provided must be >= date From which the Factory was set up on AIM for the species");
            errorList.Add("1056", "Inward Movement Date is outside the permitted number of days in the past for which a movement may be notified through the Webservice");
            errorList.Add("1060", "Lot Num imust be provided wher Lot Action = 'I'");
            errorList.Add("1062", "Lot Num may only be entered where Lot Action = 'I'");
            errorList.Add("1070", "Num Tags must be entered and > 0 wher Lot Action = 'I'");
            errorList.Add("1080", "No details found for Moved From Id on the Departments Corporate Customer System (CCS)");
            errorList.Add("1082", "Moved From ID provided is invalid for the movement of animals of this species");
            errorList.Add("1100", "No details found for Third Party Id on the Departments Corporate Customer System (CCS)");
            errorList.Add("1102", "Third Party Id provided is invalid for the movement of animals of this species");
            errorList.Add("1110", "InwardMoveRefId provided is not on file");
            errorList.Add("1111", "Lot Action if filled, must be 'I'");
            errorList.Add("1112", "InwardMoveRefId provided does not refer to this factory");
            errorList.Add("1113", "Record was not created by factory using the Webservice");
            errorList.Add("1115", "Animal may not be added to a lot that has been validated by the Department Vet. The Vet should be asked to unassign the lot or the movement should be recorded using a different Lot NUmber");
            errorList.Add("1130", "There is no approved movement rule on file for role of the Client Certificate Sender Id, Species, and Movement Date");
            errorList.Add("1138", "Lot Number is already on file for the factory and Movement Date");
            errorList.Add("545", "Tag Num must not be null, be an empty string or greater than 14 characters");
            errorList.Add("546", "Tag Number may not be duplicated within the lot");
            errorList.Add("547", "Tag Number may not be previously submitted on the same file date");
            errorList.Add("548", "Tag Number format must be valid for the bovine species");
            errorList.Add("E545", "Tag Num must not be null, be an empty string or greater than 14 characters");
            errorList.Add("E546", "Tag Number may not be duplicated within the lot");
            errorList.Add("E547", "Tag Number may not be previously submitted on the same file date");
            errorList.Add("E548", "Tag Number format must be valid for the bovine species");
            errorList.Add("1150", "Electronic Id cannot be filled");
            errorList.Add("1151", "AnimalAction must be equal to 'I'");
            errorList.Add("180", "Business Id if filled must bo on CCS");
            errorList.Add("181", "Business Id must have a valid AIM role for Movements/Species");
            errorList.Add("183", "Business Id must exist on CCS for the role of herd keeper/herd owner");
            errorList.Add("184", "Business Id must exist on the Animal Health Computer System (AHCS) for the species provided");
            errorList.Add("1600", "Region Id/Herd Within Region may not be filled where Business Id is filled");
            errorList.Add("1602", "If Region Id is filled then Herd within Region must be filled (and vice versa)");
            errorList.Add("1604", "Region Id if filled must be between 1-99");
            errorList.Add("1606", "Herd within region if provided must be between 1 and 99999");
            errorList.Add("1608", "Business Id, if filled, must be a valid herd number");
            errorList.Add("1614", "There is no designator on file for the business id and species provided");
            errorList.Add("1616", "There are no details on file for the Designator and Species provided");
            errorList.Add("049", "If File Name already on file, the Client Cert Sender Id must be the same as the Sende Id on file");
            errorList.Add("050", "If Filename is already on file and File Date is within the permitted number of days in the past for which a movement may be notified, then there should be a response for the file");
            errorList.Add("052", "If filename is already on file, the the File Date must be withini the permitted number of days in the past for which a movement may be notified");
            errorList.Add("1068", "Country code(s) may only be entered where Lot Action = 'I' or 'U'");
            errorList.Add("1072", "NumTags may not be entered where Lot Action = 'D'");
            errorList.Add("1073", "Numtags, if entered, must be greater than zero");
            errorList.Add("1083", "Moved From must be on CCS for an AIM role of Herd Keeper, Herd OWner, Dealer or factory.");
            errorList.Add("1136", "Where NumTags is not null, NumTags must be qual to the number of MovementTagNum quoted");
            errorList.Add("1141", "At least one of Moved From Id, Third Party Id, Lot Number or Movement Date must be provided where Lot Action = 'U' (Note: Null Third Party on its own meets teh requirement)");
            errorList.Add("1139", "Dispatch Document Serial Number must be provided for ovine intakes into a CPR registered factory; otherwise it cannot be provided");
            errorList.Add("1152", "AnimalAction must be filled and must be one of 'I', 'U' of 'D'");
            errorList.Add("1147", "Dispatch Document Serial Number format must be valid");
            errorList.Add("1153", "AnimalAction must be the same within a transaction");
            errorList.Add("1156", "If Lot Action = 'I', Animalaction must be 'I'");
            errorList.Add("1158", "If Lot Action = 'I', MovementAnimalId cannot be provided");
            errorList.Add("1162", "If AnimalAction = 'U' or 'D' MovementAnimalId must be provided");
            errorList.Add("1164", "If AnimalAction = 'U', tag number must be provided");
            errorList.Add("1166", "If AnimalAction = 'D', the only animal field that can be provided is MovementAnimalId");
            errorList.Add("1182", "MovementAnimalId if filled must be on file");
            errorList.Add("1184", "MovementAnimalId must be associated with the provided InwardsrefID on the database");
            errorList.Add("1194", "If AnimalAction = 'I', the Tag Number cannot be already on file for the Movement Lot being proccessed");
            errorList.Add("1196", "If AnimalAction = 'U' the Tag Number cannot be already on file for the MOvement Lot being processes as abother MOvementAnimalRefId");
            errorList.Add("552", "Tag number not present on AIM");
            errorList.Add("145", "Business Id in the client certificate is not registered for this transaction type and species");
            errorList.Add("1900", "Classification Officer Code must be numberic, cannot be zero and must be less than or equal to three characters");
            errorList.Add("1902", "Carcase number must be numeric");
            errorList.Add("1904", "Category must be one of 'A', 'B', 'C', 'D', 'E', 'V' or 'Z'. Must be B or D for non IE12 tag numbers");
            errorList.Add("1906", "Conformation must be one of 'E', 'U', 'R', 'O', 'P' optionally followed by a '+', '-' or '=' sign (max length 2 characters");
            errorList.Add("1908", "Fat must be an Integer in the range of 1-5 optionally followed by 'H' or 'L' (max length of 2 characters");
            errorList.Add("1910", "Weight1 must be numeric");
            errorList.Add("6880", "The sum of the number of CLP Category B and CLP Category C cannot be greater than the number of animals in the lot ");
            errorList.Add("6885", "Dispatch Document Serial Number, if filled must be unique on AIM for the specified period");
            errorList.Add("Weight2 must be numeric", "1912");
            errorList.Add("1914", "Where length of tag number <14, Category must be 'B' od 'D'");
            errorList.Add("1916", "Animal Carcase Desciption must be one of 'N', 'X', 'Y', 'Z'");
            errorList.Add("1918", "Price must be nmeric and less than or equal to four characters");
            errorList.Add("1920", "Factory Ownership Indicator, if filled must be 'Y' or 'N'");
            errorList.Add("1921", "Where Factory Ownership Indicator = 'Y', and the Moved From <> Sender ID however the Moved From is not a herd keeper");
            errorList.Add("1922", "Moved From is the same as the Sender Id - Factory Ownership Indicator must be 'Y'");
            errorList.Add("1923", "No movement details to the factory for the animal");
            errorList.Add("1924", "The animal has not been recorded as slaughtered in this factory");
            errorList.Add("1926", "Historic record that may not be updated using the webservice");
            errorList.Add("1927", "Pricing details previously received for this tag number");
            errorList.Add("060", "Invalid cert");

        }

        /// <summary>
        /// Method that returns the errors collection
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetErrors()
        {
            return errorList;
        }
    }
}


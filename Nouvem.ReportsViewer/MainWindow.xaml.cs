﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using Nouvem.ReportsViewer.ReportService;

namespace Nouvem.ReportsViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();

            this.ReportViewer.ProcessingMode = ProcessingMode.Remote;
            this.ReportViewer.ServerReport.ReportServerUrl = new Uri("http://localhost/reportserver");
            this.ReportViewer.ServerReport.ReportPath = "/Nouvem.Reports/Report1";
            this.ReportViewer.RefreshReport();


            //var rs = new ReportingService2010();
            //rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            ////rs.Url = "http://brian-pc/ReportServer/";
           
            //var items = rs.ListChildren("/Nouvem.Reports", false);

            //foreach (var item in items)
            //{
            //    if (item.TypeName.Equals("Report"))
            //    {
            //        //this.listBox1.Items.Add(item.Name);
            //    }
            //}
        }
    }
}

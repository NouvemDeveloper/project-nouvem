﻿
-- =============================================
-- Author:		brian murray
-- Create date: 07/04/2016
-- Description:	Gets the batch production data
-- =============================================
CREATE PROCEDURE [dbo].[GetProductionData]
	-- Add the parameters for the stored procedure here
    @BatchId AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.BatchNumberID, 
	       b.Number AS 'BatchNo', 
		   SUM(s.TransactionQTY) AS 'TransactionQty', 
		   SUM(s.TransactionWeight) AS 'TransactionWeight', 
		   SUM(1) AS 'BoxCount', 
		   im.Name, 
		   ntt.AddToStock, 
		   ntt.SubtractFromStock, 
		   SUM((CASE WHEN ntt.SubtractFromStock = 1 THEN s.TransactionWeight ELSE 0 END)) AS InputWgt, 
		   SUM((CASE WHEN ntt.AddToStock = 1 THEN s.TransactionWeight ELSE 0 END)) AS OutputWgt, 
		   p.PROrderID, 
		   s.INMasterID

    FROM   PROrder p 
	       INNER JOIN BatchNumber b ON p.BatchNumberID = b.BatchNumberID
		   INNER JOIN StockTransaction s ON s.MasterTableID = p.PROrderID 
		   INNER JOIN NouTransactionType ntt ON s.NouTransactionTypeID = ntt.NouTransactionTypeID 
		   INNER JOIN INMaster im ON im.INMasterID = s.INMasterID

	WHERE (s.Deleted IS NULL) AND (b.BatchNumberID= @BatchId)
	GROUP BY im.Name, p.Number, b.BatchNumberID, b.Number, ntt.AddToStock, ntt.SubtractFromStock, p.PROrderID, s.INMasterID

END


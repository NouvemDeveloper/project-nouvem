﻿






-- =============================================
-- Author:		brian murray
-- Create date: 28/03/16
-- Description:	Fields used for the label designer data.
-- =============================================
CREATE PROCEDURE [dbo].[LabelData]


AS
BEGIN    

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TOP 1 
	       t.BatchLotNumber AS 'Batch/Lot Number',
	       t.SerialNumber AS 'Serial Number',
		   t.Pieces,
		   t.ProductNetWeightInkg AS 'Product Net Weight in kg',
		   t.Tare AS 'Tare',		
		   t.GrossWgt AS 'Gross Wgt',
		   t.TransactionQty,
		   t.ProductCode AS 'Product Code',
		   t.ProductName as 'Product Name',
		   t.MultiLineField1 as '#MultiLine Text1',
		   t.MultiLineField2 as '#MultiLine Text2',
		   t.PartnerName as 'Partner Name',
		   t.DeliveryAddress1 as 'Delivery Address 1',
		   t.DeliveryAddress2 as 'Delivery Address 2',
		   t.DeliveryAddress3 as 'Delivery Address 3',
		   t.DeliveryAddress4 as 'Delivery Address 4',
		   t.InvoiceAddress1 as 'Invoice Address 1',
		   t.InvoiceAddress2 as 'Invoice Address 2',
		   t.InvoiceAddress3 as 'Invoice Address 3',
		   t.InvoiceAddress4 as 'Invoice Address 4',
		   t.DeliveryAddressFull AS 'Delivery Address Full',
		   t.InvoiceAddressFull AS 'Invoice Address Full',
		   t.PartnerDetails AS 'Partner Details',
		   t.Text1 as '#Text1',
		   t.Text2 as '#Text2',
		   t.Text3 as '#Text3',
		   t.Text4 as '#Text4',
		   t.Text5 as '#Text5',
		   t.Text6 as '#Text6',
		   t.Text7 as '#Text7',
		   t.Text8 as '#Text8',
		   t.Text9 as '#Text9',
		   t.Text10 as '#Text10',
		   t.Text11 as '#Text11',
		   t.Text12 as '#Text12',
		   t.Text13 as '#Text13',
		   t.Text14 as '#Text14',
		   t.Text15 as '#Text15',
		   t.Text16 as '#Text16',
		   t.Text17 as '#Text17',
		   t.Text18 as '#Text18',
		   t.Text19 as '#Text19',
		   t.Text20 as '#Text20',
		   t.Text21 as '#Text21',
		   t.Text22 as '#Text22',
		   t.Text23 as '#Text23',
		   t.Text24 as '#Text24',
		   t.Text25 as '#Text25',
		   t.Text26 as '#Text26',
		   t.Text27 as '#Text27',
		   t.Text28 as '#Text28',
		   t.Text29 as '#Text29',
		   t.Text30 as '#Text30', 		   
	       t.Barcode_Serial AS 'Barcode_Serial',
		   t.KillDate AS 'Kill Date',
		   t.BornIn AS 'Born In',
           t.RearedIn AS 'Reared In',
           t.FattenedIn AS 'Fattened In',
		   t.SlaughteredIn AS 'Slaughtered In',
           t.CutIn AS 'Cut In',
           t.BestBefore AS 'Best Before Date',            
		   t.CountryOfOrigin AS 'Country Of Origin',
           t.PackedOnDate AS 'Packed On Date',
           t.UseByDate AS 'Use By Date',											
		   t.Tel AS 'Partner Tel'		


	FROM TestLabelData t

END




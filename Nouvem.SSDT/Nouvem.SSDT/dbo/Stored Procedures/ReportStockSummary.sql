﻿



-- =============================================
-- Author:		brian murray
-- Create date: 05/04/2016
-- Description:	Stock level summary.
-- =============================================
CREATE PROCEDURE [dbo].[ReportStockSummary] 

 @CutOffDate DATE,
 @CutOffTime NVARCHAR(20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ExactCutOffTime AS DATETIME = CONVERT(DATETIME, @CutOffDate) + CONVERT(TIME, @CutOffTime)

	SELECT g.INGroupID,
	       g.Name AS 'GroupName',
	       m.Name, 
	       SUM(CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END) AS 'BoxCount', 
		   SUM(s.TransactionWeight) AS 'Weight',
		   SUM(s.Pieces) AS 'Pieces',
		   s.WarehouseID,	
		   w.Name AS 'WareHouseName'
	FROM INMaster m INNER JOIN StockTransaction s 
	                           ON s.INMasterID = m.INMasterID
					INNER JOIN INGroup g
					           ON m.INGroupID = g.INGroupID
					INNER JOIN NouTransactionType nt
					           ON s.NouTransactionTypeID = nt.NouTransactionTypeID
				    INNER JOIN Warehouse w 
					           ON s.WarehouseID = w.WarehouseID

	WHERE nt.AddToStock = 1 AND s.Deleted is null AND s.Consumed is null  AND s.TransactionDate <= @ExactCutOffTime		

	GROUP BY g.INGroupID, g.Name, m.Name, s.WarehouseID, w.Name

END





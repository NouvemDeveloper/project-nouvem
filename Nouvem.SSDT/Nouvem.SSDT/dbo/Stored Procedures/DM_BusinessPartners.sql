﻿
-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the partner data from the DEMLocal db to the Nouvem partners tables.
-- =============================================
CREATE PROCEDURE [dbo].[DM_BusinessPartners]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @CustomerName varchar(254),
	 @CustomerCode varchar(50),
	 @ContactName varchar(254),
	 @CustomerNotes varchar(1024),
	 @CustomerFax varchar(254),
	 @CustomerEmail varchar(254),
	 @CustomerPhone varchar(254),
	 @CustomerAddress varchar(254),
	 @CustomerDelAddress varchar(254),
	 @CustomerPostCode varchar(50),
	 @CustomerDelPostCode varchar(50)

	 DECLARE PARTNER_CURSOR CURSOR STATIC FOR
     SELECT c.CompanyName,
	        c.CustomerCode,
			c.ContactName,
			c.Notes,
			c.Fax,
			c.Email,
			c.Phone,
			c.[Address],
			c.DeliveryAddress,
			c.PostCode,
			c.DeliveryPostCode
 
     FROM [DEMLocal].[dbo].[Customers] c
      
 
     OPEN PARTNER_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @CustomerName,
		   @CustomerCode,
	       @ContactName,
	       @CustomerNotes,
	       @CustomerFax,
	       @CustomerEmail,
	       @CustomerPhone,
	       @CustomerAddress,
		   @CustomerDelAddress,
		   @CustomerPostCode,
		   @CustomerDelPostCode

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	 
	    DECLARE @AddressMatch INT
		DECLARE @BPMasterID int =  (SELECT TOP 1 BPMasterID FROM BPMaster
								   WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@CustomerCode)) AND RTRIM(LTRIM(Name)) = RTRIM(LTRIM(@CustomerName)))
		
								  
			IF (@BPMasterID > 0)
		    BEGIN

			    UPDATE BPMaster SET Notes = @CustomerNotes, Tel = @CustomerPhone, FAX = @CustomerFax, Email = @CustomerEmail WHERE BPMasterID = @BPMasterID

				-- if the addresses match, or there's no delivery address then mark the billing address as the shipping address too.
				SET @AddressMatch = (CASE WHEN @CustomerDelAddress IS NULL OR @CustomerDelAddress = '' OR @CustomerAddress = @CustomerDelAddress THEN 1 ELSE 0 END)

				IF (@CustomerAddress IS NOT NULL)
				BEGIN
				    INSERT iNTO BPAddress VALUES (@BPMasterID, @CustomerAddress, '', '', '', ISNULL(@CustomerPostCode, ''), 1, @AddressMatch, 0, NULL)
				END

				IF (@AddressMatch = 0 AND @CustomerDelAddress IS NOT NULL AND @CustomerDelAddress <> '')
				BEGIN
					  INSERT iNTO BPAddress VALUES (@BPMasterID, @CustomerDelAddress, '', '', '', ISNULL(@CustomerDelPostCode, ''), 0, 1, 0, NULL)	
							    
				END
				
				IF (@ContactName IS NOT NULL AND @ContactName <> '')
				BEGIN
				    INSERT iNTO BPContact VALUES (@BPMasterID, '', @ContactName, NULL,NULL,'','','',1, 0)
				END	

				
		    END		

	 FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @CustomerName,
		   @CustomerCode,
	       @ContactName,
	       @CustomerNotes,
	       @CustomerFax,
	       @CustomerEmail,
	       @CustomerPhone,
	       @CustomerAddress,
		   @CustomerDelAddress,
		   @CustomerPostCode,
		   @CustomerDelPostCode
	 END
	 
	 CLOSE PARTNER_CURSOR;
     DEALLOCATE PARTNER_CURSOR;
END


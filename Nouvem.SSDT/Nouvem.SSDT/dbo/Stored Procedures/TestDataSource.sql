﻿


-- =============================================
-- Author:		brian murray
-- Create date: 21/08/2015
-- Description:	Test proc for the label design.
-- =============================================
CREATE PROCEDURE [dbo].[TestDataSource]
	-- Add the parameters for the stored procedure here
@PartnerID as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if @PartnerID = 0 
	  BEGIN
	    SELECT Name, Deleted,
	           '150808' AS Code,
	           (SELECT CASE WHEN OnHold = 0 THEN 'no' ELSE 'yes' END) AS OnHold,
	           '' AS Barcode,
			   '' AS Barcode2
	    FROM BPMaster 
	    WHERE Deleted = '2001-01-01'
	  END
	ELSE
	  BEGIN
	    SELECT Name, Deleted,
	           '150808' AS Code,
	           (SELECT CASE WHEN OnHold = 0 THEN 'no' ELSE 'yes' END) AS OnHold,
	           '' AS Barcode,
			   '' AS Barcode2
	    FROM BPMaster 
	    WHERE BPMasterID = @PartnerID
	END
END




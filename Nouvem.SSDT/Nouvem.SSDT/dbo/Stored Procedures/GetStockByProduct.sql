﻿

-- =============================================
-- Author:		brian murray
-- Create date: 25-01-2016
-- Description:	Gets all the product stock by input product.
-- =============================================
CREATE PROCEDURE [dbo].[GetStockByProduct]
	-- Add the parameters for the stored procedure here
	@ProductID AS INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT m.Name AS Product, w.Name AS WarehouseName, w.WarehouseID,
       SUM(CASE WHEN n.AddToStock = 1 THEN s.TransactionWeight ELSE -s.TransactionWeight END) AS Wgt,
	   SUM(CASE WHEN n.AddToStock = 1 THEN s.TransactionQTY ELSE -s.TransactionQTY END) AS Qty
    FROM INMaster m 
       INNER JOIN StockTransaction s 
            ON m.INMasterID = s.INMasterID
	    INNER JOIN NouTransactionType n
		    ON s.NouTransactionTypeID = n.NouTransactionTypeID
	    INNER JOIN Warehouse w
		    ON w.WarehouseID = s.WarehouseID
    WHERE s.INMasterID = @ProductID AND s.Deleted is null AND s.Consumed is NULL AND w.StockLocation = 1
    GROUP BY w.	WarehouseID, w.Name, m.Name
END


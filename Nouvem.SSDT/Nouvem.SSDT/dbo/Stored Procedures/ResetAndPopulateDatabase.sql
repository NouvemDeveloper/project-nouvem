﻿

-- =============================================
-- Author:		brian murray
-- Create date: 12-06-16
-- Description:	Resets the db, populating it with default data.
-- =============================================
CREATE PROCEDURE [dbo].[ResetAndPopulateDatabase] 	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- reset the database
	exec ResetDatabase
	
	-- add default data --
	INSERT INTO [UserGroup ] Values ('Administrator', 0)
    DECLARE @UserGroupID INT = Ident_Current('UserGroup')
	INSERT INTO UserMaster Values ('Admin', 'Admin', '','',@UserGroupID,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
	DECLARE @UserMasterID INT = Ident_Current('UserMaster')

	DECLARE @NouRoundingOptionsID INT = (SELECT TOP 1 NouRoundingOptionsID FROM NouRoundingOptions WHERE [Description] = 'To Nearest 0.01')
	DECLARE @NouRoundingRulesID INT = (SELECT TOP 1 NouRoundingRulesID FROM NouRoundingRules WHERE [Description] = 'Round Up')
	DECLARE @BPCurrencyID INT = (SELECT TOP 1 BPCurrencyID FROM BPCurrency WHERE Name = 'Euro')
	INSERT INTO PriceList VALUES('Base Price List', NULL,0,@NouRoundingOptionsID, @NouRoundingRulesID, @BPCurrencyID, 0, @UserMasterID, GETDATE())
	DECLARE @PriceListID INT = Ident_Current('PriceListID')


	INSERT INTO BPGroup Values ('No Group', 0)
	DECLARE @BPGroupID INT = Ident_Current('BPGroup')
	DECLARE @NouBPTypeID INT = (SELECT TOP 1 NouBPTypeID FROM NouBPType WHERE [Type] = 'CustomerAndSupplier')
	
	INSERT INTO BPMaster Values(@NouBPTypeID, 'NOU123', 'TestCompany', @BPCurrencyID, @BPGroupID,NULL,@PriceListID,'30 days credit','','',
	                            '+353 45787664', '+353 4545789986', 'www.TestCompany.com', 'IE4564', '82167', NULL,NULL,0,
								1824.12, NULL,0,NULL,NULL,NULL,NULL, NULL,@UserMasterID, GETDATE(), 'testcompany@gmail.com')

    DECLARE @BPMasterID INT = Ident_Current('BPMaster')
	INSERT INTO BPAddress Values (@BPMasterID, 'Unit 8', 'Ballymany Ind Estate', 'Swords', 'Co. Dublin', 'ATY564', 1,0,0,NULL)
	INSERT INTO BPAddress Values (@BPMasterID, 'Unit A8', 'Jamestown Business Park', 'Naas', 'Co. Kildare', 'BYT789', 0,1,0,NULL)
	INSERT INTO BPContact Values(@BPMasterID, 'mr', 'John', 'Cosgrave', 'MD', '045 785567', '0876788995', 'johncosgrave@testcompany.com', 1,0)
    INSERT INTO BPContact Values(@BPMasterID, 'mrs', 'Sharon', 'Jones', 'Office Supervisor', '045 785563', '0876866751', 'sharonjones@testcompany.com', 0,0)
    
	INSERT INTO INGroup Values('BEEF', NULL,NULL,NULL,NULL,0)
	DECLARE @INGroupID INT = Ident_Current('INGroup')


	INSERT INTO VATCode Values('No VAT', '', 0, 0)
	DECLARE @VatCodeID INT = Ident_Current('VATCode')
	INSERT INTO INMaster Values('TC103', 'BEEF HINDS', @INGroupID, NULL,NULL,NULL,0,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,
	                            NULL,NULL,NULL,NULL,NULL,1,@VatCodeID,'',NULL,NULL,NULL,NULL,NULL,NULL,@UserMasterID,GETDATE(),
								NULL,1,NULL,NULL)
END



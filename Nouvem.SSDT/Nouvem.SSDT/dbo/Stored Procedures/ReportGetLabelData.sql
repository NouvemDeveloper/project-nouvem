﻿

-- =============================================
-- Author:		brian murray
-- Create date: 05/04/2016
-- Description:	Retrieves the products
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetLabelData]
	@LabelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT s.StockTransactionID,
	       s.Serial,
		   s.TransactionWeight,
		   s.TransactionQty,
		   s.Tare,
		   s.GrossWeight,
		   s.TransactionDate,
		   m.Name,
		   m.Code,
		   bn.Number,
		   (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'KillDate') AS 'Kill Date',
		   (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'BornIn') AS 'Born In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'RearedIn') AS 'Reared In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'FattenedIn') AS 'Fattened In',
		    (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SlaughteredIn') AS 'Slaughtered In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'CutIn') AS 'Cut In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'BestBefore') AS 'Best Before Date',
		   (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'CountryOfOrigin') AS 'Country Of Origin'


	FROM StockTransaction s INNER JOIN INMaster m
	                                   ON s.INMasterID = m.INMasterID
		                    LEFT JOIN  BatchNumber bn
							           ON bn.BatchNumberID = s.BatchNumberID
							

	WHERE s.StockTransactionID = @LabelID
	     
END



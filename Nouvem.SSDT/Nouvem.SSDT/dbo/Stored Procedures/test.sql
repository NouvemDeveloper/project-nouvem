﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[test] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  d.ARDispatchID,
        d.TotalExVAT,
		d.SubTotalExVAT,	
		d.DeliveryDate,
		d.DocumentDate,
		d.Number,
		d.CustomerPOReference,
        b.Name,
		b.Code,
		a.AddressLine1,
		a.AddressLine2,
		a.AddressLine3,
		a.AddressLine4,
		invoice.AddressLine1 AS 'InvoiceAddressLine1',
		invoice.AddressLine2 AS 'InvoiceAddressLine2',
		invoice.AddressLine3 AS 'InvoiceAddressLine3',
		invoice.AddressLine4 AS 'InvoiceAddressLine4',
		dd.WeightDelivered,
		dd.QuantityDelivered,
		dd.UnitPriceAfterDiscount,
		dd.TotalExclVAT,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName'
    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Customer = b.BPMasterID
       LEFT JOIN BPAddress a
          ON d.BPAddressID_Delivery = a.BPAddressID
       LEFT JOIN BPAddress invoice
          ON d.BPAddressID_Invoice = invoice.BPAddressID
     WHERE dd.Deleted IS NULL
     END


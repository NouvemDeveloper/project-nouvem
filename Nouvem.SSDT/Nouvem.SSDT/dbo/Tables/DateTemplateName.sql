﻿CREATE TABLE [dbo].[DateTemplateName] (
    [DateTemplateNameID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (50) NOT NULL,
    [Deleted]            BIT           NOT NULL,
    CONSTRAINT [PK_INDatesTemp1] PRIMARY KEY CLUSTERED ([DateTemplateNameID] ASC)
);


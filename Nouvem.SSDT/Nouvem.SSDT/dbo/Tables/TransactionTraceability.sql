﻿CREATE TABLE [dbo].[TransactionTraceability] (
    [TransactionTraceabilityID]    INT            IDENTITY (1, 1) NOT NULL,
    [StockTransactionID]           INT            NOT NULL,
    [NouTraceabilityMethodID]      INT            NOT NULL,
    [Value]                        NVARCHAR (100) NOT NULL,
    [Deleted]                      DATETIME       NULL,
    [TraceabilityTemplateMasterID] INT            NULL,
    [DateMasterID]                 INT            NULL,
    [QualityMasterID]              INT            NULL,
    [DynamicName]                  NVARCHAR (20)  NULL,
    CONSTRAINT [PK_TraceabilityValue] PRIMARY KEY CLUSTERED ([TransactionTraceabilityID] ASC),
    CONSTRAINT [FK_TraceabilityValue_NouTraceabilityMethod] FOREIGN KEY ([NouTraceabilityMethodID]) REFERENCES [dbo].[NouTraceabilityMethod] ([NouTraceabilityMethodID]),
    CONSTRAINT [FK_TraceabilityValue_StockTransaction] FOREIGN KEY ([StockTransactionID]) REFERENCES [dbo].[StockTransaction] ([StockTransactionID]),
    CONSTRAINT [FK_TransactionTraceability_DateMaster] FOREIGN KEY ([DateMasterID]) REFERENCES [dbo].[DateMaster] ([DateMasterID]),
    CONSTRAINT [FK_TransactionTraceability_QualityMaster] FOREIGN KEY ([QualityMasterID]) REFERENCES [dbo].[QualityMaster] ([QualityMasterID]),
    CONSTRAINT [FK_TransactionTraceability_TraceabilityTemplateMaster] FOREIGN KEY ([TraceabilityTemplateMasterID]) REFERENCES [dbo].[TraceabilityTemplateMaster] ([TraceabilityTemplatesMasterID])
);


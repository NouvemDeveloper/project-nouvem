﻿CREATE TABLE [dbo].[NouStockMode] (
    [NouStockModeID] INT        IDENTITY (1, 1) NOT NULL,
    [Name]           NCHAR (30) NOT NULL,
    [Number]         INT        NOT NULL,
    [Deleted]        DATETIME   NULL,
    CONSTRAINT [PK_NouStockMode] PRIMARY KEY CLUSTERED ([NouStockModeID] ASC)
);


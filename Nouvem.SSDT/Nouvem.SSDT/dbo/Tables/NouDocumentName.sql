﻿CREATE TABLE [dbo].[NouDocumentName] (
    [NouDocumentNameID] INT           IDENTITY (1, 1) NOT NULL,
    [DocumentName]      NVARCHAR (50) NOT NULL,
    [Deleted]           DATETIME      NULL,
    CONSTRAINT [PK_NouDocumentNames] PRIMARY KEY CLUSTERED ([NouDocumentNameID] ASC)
);


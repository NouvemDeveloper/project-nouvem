﻿CREATE TABLE [dbo].[BatchNumber] (
    [BatchNumberID] INT            IDENTITY (1, 1) NOT NULL,
    [Number]        NVARCHAR (100) NOT NULL,
    [Deleted]       DATETIME       NULL,
    [CreatedDate]   DATETIME       CONSTRAINT [DF__BatchNumb__Creat__619C4B9F] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BatchNumber] PRIMARY KEY CLUSTERED ([BatchNumberID] ASC)
);


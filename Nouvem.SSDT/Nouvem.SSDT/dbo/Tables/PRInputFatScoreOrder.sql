﻿CREATE TABLE [dbo].[PRInputFatScoreOrder] (
    [PRInputFatScoreOrderID] INT      IDENTITY (1, 1) NOT NULL,
    [PROrderID]              INT      NOT NULL,
    [NouFatScoreID]          INT      NOT NULL,
    [Selected]               BIT      NOT NULL,
    [Deleted]                DATETIME NULL,
    CONSTRAINT [PK_PRInputFatScoreOrder] PRIMARY KEY CLUSTERED ([PRInputFatScoreOrderID] ASC),
    CONSTRAINT [FK_PRInputFatScoreOrder_PROrder] FOREIGN KEY ([PROrderID]) REFERENCES [dbo].[PROrder] ([PROrderID])
);


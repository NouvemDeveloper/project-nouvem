﻿CREATE TABLE [dbo].[QualityTemplateAllocation] (
    [QualityTemplateAllocationID] INT IDENTITY (1, 1) NOT NULL,
    [QualityTemplateNameID]       INT NOT NULL,
    [QualityMasterID]             INT NOT NULL,
    [Batch]                       BIT NULL,
    [Transaction]                 BIT NULL,
    [Deleted]                     BIT NOT NULL,
    CONSTRAINT [PK_INQualitysTempDetail] PRIMARY KEY CLUSTERED ([QualityTemplateAllocationID] ASC),
    CONSTRAINT [FK_QualityTemplateAllocation_QualityTemplateName] FOREIGN KEY ([QualityTemplateNameID]) REFERENCES [dbo].[QualityTemplateName] ([QualityTemplateNameID]),
    CONSTRAINT [FK_QualityTemplateAlocation_QualityMaster] FOREIGN KEY ([QualityMasterID]) REFERENCES [dbo].[QualityMaster] ([QualityMasterID])
);


﻿CREATE TABLE [dbo].[TraceabilityTemplateAllocation] (
    [TraceabilityTemplateAllocationID] INT IDENTITY (1, 1) NOT NULL,
    [TraceabilityTemplateNameID]       INT NOT NULL,
    [TraceabilityTemplateMasterID]     INT NOT NULL,
    [Batch]                            BIT NULL,
    [Transaction]                      BIT NULL,
    [Deleted]                          BIT NOT NULL,
    CONSTRAINT [PK_TracebilityTemplatesAllocation] PRIMARY KEY CLUSTERED ([TraceabilityTemplateAllocationID] ASC),
    CONSTRAINT [FK_TraceabilityTemplatesAllocation_TraceabilityTemplateName] FOREIGN KEY ([TraceabilityTemplateNameID]) REFERENCES [dbo].[TraceabilityTemplateName] ([TraceabilityTemplateNameID]),
    CONSTRAINT [FK_TracebilityTemplatesAllocation_TracebilityTemplatesMaster] FOREIGN KEY ([TraceabilityTemplateMasterID]) REFERENCES [dbo].[TraceabilityTemplateMaster] ([TraceabilityTemplatesMasterID])
);


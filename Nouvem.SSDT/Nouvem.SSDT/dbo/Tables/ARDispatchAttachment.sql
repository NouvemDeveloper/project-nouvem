﻿CREATE TABLE [dbo].[ARDispatchAttachment] (
    [ARDispatchAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [ARDispatchID]           INT             NOT NULL,
    [FilePath]               NVARCHAR (50)   NULL,
    [FileName]               NVARCHAR (50)   NOT NULL,
    [AttachmentDate]         DATE            NOT NULL,
    [File]                   VARBINARY (MAX) NOT NULL,
    [Deleted]                DATETIME        NULL,
    CONSTRAINT [PK_ARDispatchAttachment] PRIMARY KEY CLUSTERED ([ARDispatchAttachmentID] ASC),
    CONSTRAINT [FK_ARDispatchAttachment_ARDispatch] FOREIGN KEY ([ARDispatchID]) REFERENCES [dbo].[ARDispatch] ([ARDispatchID])
);


﻿CREATE TABLE [dbo].[DateTemplateAllocation] (
    [DateTemplateAllocationID] INT IDENTITY (1, 1) NOT NULL,
    [DateTemplateNameID]       INT NOT NULL,
    [DateMasterID]             INT NOT NULL,
    [Batch]                    BIT NULL,
    [Transaction]              BIT NULL,
    [Deleted]                  BIT NOT NULL,
    CONSTRAINT [PK_INDatesTempDetail] PRIMARY KEY CLUSTERED ([DateTemplateAllocationID] ASC),
    CONSTRAINT [FK_DateTemplateAlocation_DateMaster] FOREIGN KEY ([DateMasterID]) REFERENCES [dbo].[DateMaster] ([DateMasterID]),
    CONSTRAINT [FK_INDatesTempDetail_INDatesTemp] FOREIGN KEY ([DateTemplateNameID]) REFERENCES [dbo].[DateTemplateName] ([DateTemplateNameID])
);


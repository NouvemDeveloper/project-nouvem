﻿CREATE TABLE [dbo].[Label] (
    [LabelID]      INT             IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50)   NOT NULL,
    [LabelGroupID] INT             NULL,
    [LabelFile]    NVARCHAR (MAX)  NOT NULL,
    [Deleted]      BIT             NOT NULL,
    [LabelImage]   VARBINARY (MAX) NULL,
    [DataSource]   NVARCHAR (30)   NULL,
    CONSTRAINT [PK_Label] PRIMARY KEY CLUSTERED ([LabelID] ASC),
    CONSTRAINT [FK_Label_LabelGroup] FOREIGN KEY ([LabelGroupID]) REFERENCES [dbo].[LabelGroup] ([LabelGroupID])
);


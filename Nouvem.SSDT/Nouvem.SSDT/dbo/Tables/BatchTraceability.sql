﻿CREATE TABLE [dbo].[BatchTraceability] (
    [BatchTraceabilityID]          INT            IDENTITY (1, 1) NOT NULL,
    [BatchNumberID]                INT            NOT NULL,
    [NouTraceabilityMethodID]      INT            NOT NULL,
    [TraceabilityTemplateMasterID] INT            NULL,
    [DateMasterID]                 INT            NULL,
    [QualityMasterID]              INT            NULL,
    [Value]                        NVARCHAR (100) NOT NULL,
    [Deleted]                      DATETIME       NULL,
    CONSTRAINT [PK_BatchTraceability] PRIMARY KEY CLUSTERED ([BatchTraceabilityID] ASC),
    CONSTRAINT [FK_BatchTraceability_BatchNumber] FOREIGN KEY ([BatchNumberID]) REFERENCES [dbo].[BatchNumber] ([BatchNumberID]),
    CONSTRAINT [FK_BatchTraceability_DateMaster] FOREIGN KEY ([DateMasterID]) REFERENCES [dbo].[DateMaster] ([DateMasterID]),
    CONSTRAINT [FK_BatchTraceability_NouTraceabilityMethod] FOREIGN KEY ([NouTraceabilityMethodID]) REFERENCES [dbo].[NouTraceabilityMethod] ([NouTraceabilityMethodID]),
    CONSTRAINT [FK_BatchTraceability_QualityMaster] FOREIGN KEY ([QualityMasterID]) REFERENCES [dbo].[QualityMaster] ([QualityMasterID]),
    CONSTRAINT [FK_BatchTraceability_TraceabilityTemplateMaster] FOREIGN KEY ([TraceabilityTemplateMasterID]) REFERENCES [dbo].[TraceabilityTemplateMaster] ([TraceabilityTemplatesMasterID])
);


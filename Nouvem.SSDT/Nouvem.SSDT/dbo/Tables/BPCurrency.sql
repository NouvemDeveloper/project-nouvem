﻿CREATE TABLE [dbo].[BPCurrency] (
    [BPCurrencyID] INT             IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (20)    NOT NULL,
    [RateToBase]   DECIMAL (18, 2) NOT NULL,
    [Symbol]       NVARCHAR (5)    NOT NULL,
    [Deleted]      BIT             NOT NULL,
    CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED ([BPCurrencyID] ASC)
);


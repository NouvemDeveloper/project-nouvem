﻿CREATE TABLE [dbo].[APQuoteAttachment] (
    [APQuoteAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [APQuoteID]           INT             NOT NULL,
    [FilePath]            NVARCHAR (50)   NULL,
    [FileName]            NVARCHAR (50)   NOT NULL,
    [AttachmentDate]      DATE            NOT NULL,
    [File]                VARBINARY (MAX) NOT NULL,
    [Deleted]             DATETIME        NULL,
    CONSTRAINT [PK_APSaleAttachment] PRIMARY KEY CLUSTERED ([APQuoteAttachmentID] ASC),
    CONSTRAINT [FK_APQuoteAttachment_APQuote] FOREIGN KEY ([APQuoteID]) REFERENCES [dbo].[APQuote] ([APQuoteID])
);


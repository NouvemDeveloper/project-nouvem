﻿CREATE TABLE [dbo].[ContainerType] (
    [ContainerTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (50) NOT NULL,
    [Deleted]         BIT           NOT NULL,
    CONSTRAINT [PK_ContainerType] PRIMARY KEY CLUSTERED ([ContainerTypeID] ASC)
);


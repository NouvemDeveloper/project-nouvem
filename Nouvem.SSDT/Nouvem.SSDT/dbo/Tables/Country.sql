﻿CREATE TABLE [dbo].[Country] (
    [CountryID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]      NVARCHAR (50) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [Deleted]   BIT           NOT NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);


﻿// -----------------------------------------------------------------------
// <copyright file="InformationType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.Enum
{
    /// <summary>
    /// Enumeration of the possible information types.
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// The background message type.
        /// </summary>
        Background,

        /// <summary>
        /// The priority message type.
        /// </summary>
        Priority,

        /// <summary>
        /// The issue message type.
        /// </summary>
        Issue
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="PrintMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.Enum
{
    /// <summary>
    /// Models the printing modes.
    /// </summary>
    public enum PrintMode
    {
        /// <summary>
        /// The usb mode
        /// </summary>
        USB,

        /// <summary>
        /// The serial mode
        /// </summary>
        Serial,

        /// <summary>
        /// The parallel mode
        /// </summary>
        Parallel,

        /// <summary>
        /// The ethernet mode
        /// </summary>
        Network,

        /// <summary>
        /// The print to image mode.
        /// </summary>
        Image
    }
}

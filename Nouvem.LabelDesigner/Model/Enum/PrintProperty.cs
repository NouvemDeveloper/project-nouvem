﻿// -----------------------------------------------------------------------
// <copyright file="PrintProperty.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.Enum
{
    /// <summary>
    /// Models the print propertiers.
    /// </summary>
    public enum PrintProperty
    {
        /// <summary>
        /// The com port.
        /// </summary>
        ComPort,

        /// <summary>
        /// The baud rate.
        /// </summary>
        BaudRate,


        /// <summary>
        /// The parity.
        /// </summary>
        Parity,


        /// <summary>
        /// The data bits.
        /// </summary>
        DataBits,


        /// <summary>
        /// The stop bits.
        /// </summary>
        StopBits,


        /// <summary>
        /// The handshake.
        /// </summary>
        Handshake,


        /// <summary>
        /// The ip host.
        /// </summary>
        IPHost,


        /// <summary>
        /// The port.
        /// </summary>
        Port,


        /// <summary>
        /// The parallel port.
        /// </summary>
        ParallelPort,

        /// <summary>
        /// The printer name.
        /// </summary>
        PrinterName,

        /// <summary>
        /// The printer language.
        /// </summary>
        PrinterLanguage,

        /// <summary>
        /// The print oriention.
        /// </summary>
        PrintOrientation,

        /// <summary>
        /// The dpi resolution.
        /// </summary>
        DPI,

        /// <summary>
        /// The number of copies.
        /// </summary>
        Copies
    }
}

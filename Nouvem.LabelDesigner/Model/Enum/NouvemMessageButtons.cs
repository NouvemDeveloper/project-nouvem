﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBoxButtons.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.Enum
{
    /// <summary>
    /// The message box buttons enumerations
    /// </summary>
    public enum NouvemMessageBoxButtons
    {
        /// <summary>
        /// The OK cancel selection
        /// </summary>
        OKCancel,

        /// <summary>
        /// The yes no selection
        /// </summary>
        YesNo,

        /// <summary>
        /// The OK cancel selection
        /// </summary>
        WorkflowStandard,

        /// <summary>
        /// The ok selection
        /// </summary>
        OK
    }
}

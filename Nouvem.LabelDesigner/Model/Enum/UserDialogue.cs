﻿// -----------------------------------------------------------------------
// <copyright file="UserDialogue.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.Enum
{
    /// <summary>
    /// Enumeration that models the message box user selections.
    /// </summary>
    public enum UserDialogue
    {
        /// <summary>
        /// The OK result
        /// </summary>
        OK,

        /// <summary>
        /// The cancel result
        /// </summary>
        Cancel,

        /// <summary>
        /// The Yesresult
        /// </summary>
        Yes,

        /// <summary>
        /// The Noresult
        /// </summary>
        No,

        Workflow,
        Standard
    }
}


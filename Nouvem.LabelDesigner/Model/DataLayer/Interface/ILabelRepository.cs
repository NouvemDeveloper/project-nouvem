﻿// -----------------------------------------------------------------------
// <copyright file="ILabelRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;

namespace Nouvem.LabelDesigner.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface ILabelRepository
    {
        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        IList<Label> GetLabels();

        /// <summary>
        /// Retrieve all the label fonts.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        IList<LabelFont> GetLabelFonts();

        /// <summary>
        /// Saves a label.
        /// </summary>
        /// <param name="label">The label to save.</param>
        /// <returns>A flag, indicating a successful save or not.</returns>
        bool SaveLabel(Label label);

        /// <summary>
        /// Retrieve all the label groups.
        /// </summary>
        /// <returns>A collection of abel groups.</returns>
        IList<LabelGroup> GetLabelGroups();

        /// <summary>
        /// Add or updates the label groups list.
        /// </summary>
        /// <param name="labelGroups">The label groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateLabelGroups(IList<LabelGroup> labelGroups);

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        IList<Gs1AIMaster> GetGs1Master();

        /// <summary>
        /// Retrieves the label data schema.
        /// </summary>
        /// <param name="name">The sp name.</param>
        /// <returns>A data table.(Schema only).</returns>
        DataTable GetLabelData(string name);

        /// <summary>
        /// Retrieves the label sp names.
        /// </summary>
        /// <returns>A collection of sp label names.</returns>
        IList<string> GetLabelSpNames();

        /// <summary>
        /// Retrieves the label data result row.
        /// </summary>
        DataTable GetTestLabelData();

        /// <summary>
        /// Updates the test data.
        /// </summary>
        void UpdateTestLabelData(DataTable dt);

        /// <summary>
        /// Deletes the label.
        /// </summary>
        /// <param name="label">The label to delete</param>
        bool DeleteLabel(Label label);
    }
}

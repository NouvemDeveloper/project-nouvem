﻿// -----------------------------------------------------------------------
// <copyright file="SystemMessageRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.DataLayer
{
    using System.Collections.Generic;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.Enum;

    /// <summary>
    /// Class which handles the system message functions.
    /// </summary>
    public class SystemMessageRepository
    {
        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="SystemMessageRepository"/> class from being created.
        /// </summary>
        public SystemMessageRepository()
        {
            this.SystemMessages = new List<Information>();
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets the system messages.
        /// </summary>
        public IList<Information> SystemMessages { get; private set; }

        /// <summary>
        /// Add a new message.
        /// </summary>
        /// <param name="category">The category of the message (eg: error)</param>
        /// <param name="message">The information message to log.</param>
        public void Write(MessageType category, string message)
        {
            // just keep the last 100 messages, otherwise could cause a memory leak
            if (this.SystemMessages.Count > 100)
            {
                this.SystemMessages.RemoveAt(this.SystemMessages.Count - 1);
            }

            this.SystemMessages.Insert(0, Information.CreateInformation(category, message));
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="LabelItemData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.BusinessObject
{
    public class LabelItemData
    {
        /// <summary>
        /// Gets or sets the label item name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the label item value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the label item assigned variable.
        /// </summary>
        public string AssignedVariable { get; set; }
    }
}

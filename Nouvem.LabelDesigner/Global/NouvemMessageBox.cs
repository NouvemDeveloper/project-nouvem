﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBox.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Global
{
    using System;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesigner.Model.Enum;

    /// <summary>
    /// Static handler for the message box.
    /// </summary>
    public static class NouvemMessageBox
    {
        #region private

        /// <summary>
        /// The user selection reference.
        /// </summary>
        private static UserDialogue userSelection;

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the user dialogue selection.
        /// </summary>
        public static UserDialogue UserSelection
        {
            get
            {
                return userSelection;
            }

            set
            {
                userSelection = value;

                // selection made, so close the message box.
                Messenger.Default.Send(Token.Message, Token.CloseMessageBoxWindow);
            }
        }
        /// <summary>
        /// Set, and display the nouvem message box.
        /// </summary>
        /// <param name="displayMessage">The message to display.</param>
        /// <param name="buttonSelection">The buttons configuration settings.</param>
        public static void Show(string displayMessage, NouvemMessageBoxButtons buttonSelection = NouvemMessageBoxButtons.OK)
        {
            // Display the message, set the ui button configuration, and open the message box.
            Messenger.Default.Send(Tuple.Create(displayMessage, buttonSelection));
        }

        #endregion
    }
}


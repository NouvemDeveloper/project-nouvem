﻿// -----------------------------------------------------------------------
// <copyright file="CommandConverter.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Global.Converter
{
    using Neodynamic.Windows.WPF.ThermalLabelEditor;
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class CommandConverter : IMultiValueConverter
    {
   
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var tuple = new Tuple<ThermalLabelEditor, string>(
            (ThermalLabelEditor)values[0], (string)values[1]);

            return (object)tuple;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

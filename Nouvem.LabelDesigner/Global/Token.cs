﻿// -----------------------------------------------------------------------
// <copyright file="Token.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Global
{
    static class Token
    {
        /// <summary>
        /// Token used to as the first parameter of the messenger construct, used when a standard generic message is only required.
        /// </summary>
        public const string Message = "";

        /// <summary>
        /// Token used to inform observers of a selected item change.
        /// </summary>
        public const string SelectedItemChange = "SELECTED_ITEM_CHANGE";

        /// <summary>
        /// Token used to inform observers of a selected item update.
        /// </summary>
        public const string SelectedItemUpdate = "SELECTED_ITEM_UPDATE";

        /// <summary>
        /// Token used to inform observers to save a label.
        /// </summary>
        public const string Savelabel = "SAVE_LABEL";

        /// <summary>
        /// Token used to set font size.
        /// </summary>
        public const string SetFontSize = "SET_FONT_SIZE";

        /// <summary>
        /// Token used to set font name.
        /// </summary>
        public const string SetFontName = "SET_FONT_NAME";

        /// <summary>
        /// Token used to set the font size.
        /// </summary>
        public const string SaveAs = "SAVE_AS";

        /// <summary>
        /// Token used to set the font size.
        /// </summary>
        public const string Delete = "DELETE";

        /// <summary>
        /// Token used to send the save as editor.
        /// </summary>
        public const string SaveAsEditor = "SAVE_AS_EDITOR";

        /// <summary>
        /// Token used to inform observers to load a label.
        /// </summary>
        public const string LoadLabel = "LOAD_LABEL";

        /// <summary>
        /// Token used to inform observers to open a font view.
        /// </summary>
        public const string OpenFont = "OPEN_FONT";

        /// <summary>
        /// Token used to inform observers to set a font.
        /// </summary>
        public const string SetFont = "SET_FONT";

        /// <summary>
        /// Token used to inform observers that no item is currently selected on the label.
        /// </summary>
        public const string NoLabelItemSelected = "NO_LABEL_ITEM_SELECTED";

        /// <summary>
        /// Token used to inform the message box to close.
        /// </summary>
        public const string CloseMessageBoxWindow = "CLOSE_MESSAGEBOX_WINDOW";

         /// <summary>
        /// Token used to inform the message box to open.
        /// </summary>
        public const string OpenMessageBox = "OPEN_MESSAGEBOX_WINDOW";

        /// <summary>
        /// Token used to inform the editor to create a new label.
        /// </summary>
        public const string NewLabel = "NEW_LABEL";

        /// <summary>
        /// Token used to inform the recipient to update it's string builder.
        /// </summary>
        public const string StringBuilder = "STRINGBUILDER";

        /// <summary>
        /// Token used to inform the recipient to update it's gs1 selection.
        /// </summary>
        public const string Gs1Selected = "GS1SELECTED";

         /// <summary>
        /// Token used to inform the recipient to update it's barcode code property.
        /// </summary>
        public const string UpdateBarcodeCode = "UPDATE_BARCODE_CODE";

        /// <summary>
        /// Token used to inform the recipient that a label item has been added/removed.
        /// </summary>
        public const string LabelItemChange = "LABEL_ITEM_CHANGE";

        /// <summary>
        /// Token used to send the grid gap value change.
        /// </summary>
        public const string GridGapValue = "GRID_GAP_VALUE";

        /// <summary>
        /// Token used to send the the editor.
        /// </summary>
        public const string SendLabelEditor = "SEND_LABEL_EDITOR";

        /// <summary>
        /// Token used to receive the editor.
        /// </summary>
        public const string LabelEditorSent = "LABEL_EDITOR_SENT";

         /// <summary>
        /// Token used to inform the recipient to update it's barcode data field property.
        /// </summary>
        public const string UpdateBarcodeDataField = "UPDATE_BARCODE_DATA_FIELD";
    }
}

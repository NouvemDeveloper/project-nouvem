﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesignerGlobal.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.LabelDesigner.BusinessLogic;
using Nouvem.Logging;

namespace Nouvem.LabelDesigner.Global
{
    using System.Collections.Generic;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.DataLayer;

    public static class LabelDesignerGlobal
    {
        /// <summary>
        /// The application logger reference.
        /// </summary>
        private static readonly ILogger Log = new Logger();

        /// <summary>
        /// Gets or sets the application label groups.
        /// </summary>
        public static LabelGroup LabelGroups;

        /// <summary>
        /// Gets or sets the application selected label data.
        /// </summary>
        public static IList<LabelData> SelectedLabelData { get; set; }

        /// <summary>
        /// Gets or sets the selected label data source.
        /// </summary>
        public static string DataSource { get; set; }

        /// <summary>
        /// Gets or sets the fonts.
        /// </summary>
        public static IList<LabelFont> LabelFonts;

        static LabelDesignerGlobal()
        {
            RegisterUnhandledExceptionHandler();
            LabelFonts = DataManager.Instance.GetLabelFonts();
        }

        /// <summary>
        /// Method that registers for any unhandled exceptions. 
        /// </summary>
        private static void RegisterUnhandledExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                var ex = e.ExceptionObject as Exception;

                // log the unhandled exception.
                Log.LogError(
                    typeof(LabelDesignerGlobal),
                    string.Format("RegisterUnhandledExceptionHandler(): Message:{0}  Inner Exception:{1}  Stack Trace:{2}", ex.Message, ex.InnerException, ex.StackTrace));

                throw ex;
            };
        }
    }
}

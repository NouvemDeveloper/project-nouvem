﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesigner.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows;
using Nouvem.LabelDesigner.Properties;
using Nouvem.LabelDesigner.View.Designer;

namespace Nouvem.LabelDesigner.Global.Start
{
    using Nouvem.LabelDesigner.View;

    /// <summary>
    /// Entry point for any external application call.
    /// </summary>
    public static class LabelDesigner
    {
        ///// <summary>
        ///// Start the label designer.
        ///// </summary>
        //public static void Start(string loggedInUser)
        //{
        //    Settings.Default.LoggedInUser = loggedInUser;
        //    Settings.Default.Save();

        //    var app = new Application();
        //    app.Run(new DesignerContainerView());
        //    //var label = new DesignerContainerView();
        //    //label.ShowDialog();
        //}

    }
}

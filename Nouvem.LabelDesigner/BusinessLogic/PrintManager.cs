﻿// -----------------------------------------------------------------------
// <copyright file="PrintManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.LabelDesigner.Model.DataLayer;
using Nouvem.LabelDesigner.Properties;

namespace Nouvem.LabelDesigner.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing.Printing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Neodynamic.SDK.Printing;
    using Neodynamic.Windows.WPF.ThermalLabelEditor;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Logging;
    using Nouvem.Shared.Localisation;

    public class PrintManager
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly PrintManager Manager = new PrintManager();

        /// <summary>
        /// The logger reference.
        /// </summary>
        private Logger Log = new Logger();

        /// <summary>
        /// The data manager reference.
        /// </summary>
        private DataManager dataManager = DataManager.Instance;

        /// <summary>
        /// The printer settings.
        /// </summary>
        private Neodynamic.SDK.Printing.PrinterSettings printerSettings = new Neodynamic.SDK.Printing.PrinterSettings();

        /// <summary>
        /// The number of copies.
        /// </summary>
        private int copies = 1;

        /// <summary>
        /// The orientation reference.
        /// </summary>
        private PrintOrientation printOrientation = PrintOrientation.Portrait;

        /// <summary>
        /// The thermal label editor reference.
        /// </summary>
        private ThermalLabelEditor labelEditor = new ThermalLabelEditor();

        #endregion

        #region constructor

        private PrintManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the print manager singleton.
        /// </summary>
        public static PrintManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Prints the current editor label.
        /// </summary>
        /// <returns>A string, indicating whether the label was printed or not.</returns>
        public string Print()
        {
            return PrintEngine.Print();
        }

        /// <summary>
        /// Sets the label to print to that of the input label.
        /// </summary>
        /// <param name="label">The label to store for printing.</param>
        public void SetLabel(ThermalLabel label)
        {
            PrintEngine.Label = label;
        }

        /// <summary>
        /// Sets the label data source.
        /// </summary>
        /// <param name="dataSource">The label data source.</param>
        public void SetDataSource(string dataSource)
        {
            PrintEngine.DataSource = dataSource;
        }

        /// <summary>
        /// Sets the printer settings.
        /// </summary>
        public void SetPrinterSettings()
        {
            PrintEngine.SetSettings();
        }

        /// <summary>
        /// Exports a label to image.
        /// </summary>
        /// <param name="label">The label to export.</param>
        /// <param name="print">Flag, which indicates whether the label is to be printed.</param>
        public string ExportToImage(ThermalLabel label, bool print)
        {
            return PrintEngine.ExportToImage(label, print);
        }

        /// <summary>
        /// Exports a label to pdf.
        /// </summary>
        /// <param name="label">The label to export.</param>
        public string ExportToPdf(ThermalLabel label)
        {
            return PrintEngine.ExportToPdf(label);
        }

        /// <summary>
        /// Exports a label to xml.
        /// </summary>
        /// <param name="label">The label to export.</param>
        public string ExportToXml(ThermalLabel label)
        {
            return PrintEngine.ExportToXml(label);
        }

        #endregion

        #endregion
    }
}

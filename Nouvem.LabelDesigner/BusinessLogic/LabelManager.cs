﻿// -----------------------------------------------------------------------
// <copyright file="LabelManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.LabelDesigner.ViewModel;

namespace Nouvem.LabelDesigner.BusinessLogic
{
    using System;
    using System.IO;
    using Neodynamic.SDK.Printing;
    using Neodynamic.Windows.WPF.ThermalLabelEditor;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.DataLayer;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Logging;

    public class LabelManager
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly LabelManager Manager = new LabelManager();

        /// <summary>
        /// Gets the singleton data manager reference.
        /// </summary>
        protected DataManager DataManager = DataManager.Instance;

        /// <summary>
        /// The logger reference.
        /// </summary>
        private Logger Log = new Logger();

        #endregion

        #region constructor

        private LabelManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the label manager singleton.
        /// </summary>
        public static LabelManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Saves a newly created label to persistence.
        /// </summary>
        /// <param name="editor">The editor.</param>
        /// <param name="labelName">The label name.</param>
        /// <param name="labelGroupID">The label group id</param>
        /// <returns>A newly created label, or null if there was an issue.</returns>
        public Label SaveLabel(ThermalLabelEditor editor, string labelName, int? labelGroupID)
        {
            try
            {
                ProgressBar.SetUp(min:1, max:4);
                ProgressBar.Run();
                // save the label xml (.tl)
                editor.Save(Settings.Default.CurrentLabel);

                // export the image
                var label = editor.CreateThermalLabel();

                var dataSource = this.DataManager.GetTestLabelData();
                label.DataSource = dataSource;

                var labelImageStream = new MemoryStream();
                using (var printJob = new PrintJob())
                {
                    ProgressBar.Run();
                    printJob.ThermalLabel = label;
                    //printJob.ExportToPdf(label, @"C:\Nouvem\Label.pdf", 300);
                   
                    //printJob.ExportToImage(Settings.Default.LabelImage, new ImageSettings(ImageFormat.Png), 203);
                    printJob.ExportToImage(labelImageStream, new ImageSettings(ImageFormat.Png), 203);
                    ProgressBar.Run(); 
                }

                //var labelImageStream = File.ReadAllBytes(Settings.Default.LabelImage);
                var labelFile = File.ReadAllText(Settings.Default.CurrentLabel);

                var localLabel = new Label
                {
                    Name = labelName,
                    LabelGroupID = labelGroupID,
                    LabelFile = labelFile,
                    LabelImage = labelImageStream.ToArray(),
                    DataSource = LabelDesignerGlobal.DataSource,
                    Orientation = ViewModelLocator.PrinterSetUpStatic.PrintOrientation,
                    Deleted = false
                };

                // save to persistence
                if (this.DataManager.SaveLabel(localLabel))
                {
                    ProgressBar.Run();
                    return localLabel;
                }

                return null;
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        #endregion

        #endregion
    }
}

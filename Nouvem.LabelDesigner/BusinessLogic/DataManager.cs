﻿// -----------------------------------------------------------------------
// <copyright file="DataManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.LabelDesigner.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.DataLayer;
    using Nouvem.LabelDesigner.Model.DataLayer.Repository;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;

    /// <summary>
    /// Manager class for the data logic.
    /// </summary>
    public sealed class DataManager 
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly DataManager Manager = new DataManager();

        /// <summary>
        /// The system message repository reference.
        /// </summary>
        private readonly SystemMessageRepository systemMessageRepository = new SystemMessageRepository();

        /// <summary>
        /// The label repository reference.
        /// </summary>
        private readonly LabelRepository labelRepository = new LabelRepository();
      
        #endregion

        #region constructor

        private DataManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the data manager singleton.
        /// </summary>
        public static DataManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets or sets the test data.
        /// </summary>
        public IList<LabelData> TestData { get; set; }

        #endregion

        #region method

        #region system message

        /// <summary>
        /// Add a new message.
        /// </summary>
        /// <param name="category">The category of the message (eg: error)</param>
        /// <param name="message">The information message to log.</param>
        public void WriteMessage(MessageType category, string message)
        {
            this.systemMessageRepository.Write(category, message);
        }

        /// <summary>
        /// Gets the system messages.
        /// </summary>
        /// <returns>A collection of system messages.</returns>
        public IList<Information> GetMessages()
        {
            return this.systemMessageRepository.SystemMessages;
        }

        #endregion

        #region label

        /// <summary>
        /// Updates the test data..
        /// </summary>
        public void UpdateTestLabelData(DataTable dt)
        {
            this.labelRepository.UpdateTestLabelData(dt);
        }

        /// <summary>
        /// Retrieves the label data result row.
        /// </summary>
        public DataTable GetTestLabelData()
        {
            return this.labelRepository.GetTestLabelData();
        }

        /// <summary>
        /// Deletes the label.
        /// </summary>
        /// <param name="label">The label to delete</param>
        public bool DeleteLabel(Label label)
        {
            return this.labelRepository.DeleteLabel(label);
        }

        /// <summary>
        /// Retrieves the test label data.
        /// </summary>
        /// <returns>The test label data.</returns>
        public IList<LabelData> GetLabelDataFields()
        {
            var labelData = new List<LabelData>();
            var dt = this.labelRepository.GetTestLabelData();
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var localLabelData = new LabelData();
                    localLabelData.Field = column.ColumnName;
                    localLabelData.SampleData = row[column].ToString();
                    localLabelData.Identifier = "LabelData";
                    labelData.Add(localLabelData);

                    if (localLabelData.Field.ContainsIgnoringCase("GS1128") && string.IsNullOrEmpty(localLabelData.SampleData))
                    {
                        localLabelData.SampleData = "0253915205640553310310.87";
                    }
                }
            }

            var data = labelData.Where(x => !x.Field.StartsWith("#")).OrderBy(x => x.Field);
            var productDataBarcode = labelData.Where(x => x.Field.StartsWith("#Barcode"));
            var productDataMulti = labelData.Where(x => x.Field.StartsWith("#MultiLine")).OrderBy(x => x.Field);
            var productDataText = labelData.Where(x => x.Field.StartsWith("#Text")).OrderBy(x => x.Field.FindNumericSequence().ToInt());
            var partnertDataText = labelData.Where(x => x.Field.StartsWith("#PartnerText")).OrderBy(x => x.Field.FindNumericSequence().ToInt());
            var dataToReturn = data.Concat(productDataBarcode).Concat(productDataMulti).Concat(productDataText).Concat(partnertDataText).ToList();

            return dataToReturn;
        }

        ///// <summary>
        ///// Retrieves the intake label data schema.
        ///// </summary>
        ///// <returns>An intake label data table.(Schema only).</returns>
        //public IList<LabelData> GetLabelDataFields()
        //{
        //    var spNames = this.labelRepository.GetLabelSpNames();
        //    var labelData = new List<LabelData>();

        //    foreach (var name in spNames)
        //    {
        //        var schema = this.labelRepository.GetLabelData(name);
        //        foreach (DataRow dr in schema.Rows)
        //        {
        //            var localLabelData = new LabelData { Field = dr["ColumnName"].ToString(), Identifier = name };
        //            var dataType = dr["DataType"].ToString();
        //            switch (dataType)
        //            {
        //                case "System.String":
        //                    localLabelData.SampleData = "Sample Text";
        //                    break;

        //                case "System.Boolean":
        //                    localLabelData.SampleData = "Yes";
        //                    break;

        //                case "System.Int32":
        //                case "System.Int16:":
        //                    localLabelData.SampleData = "45";
        //                    break;

        //                case "System.Date":
        //                case "System.DateTime":
        //                    localLabelData.SampleData = DateTime.Today.ToShortDateString();
        //                    break;

        //                default:
        //                    localLabelData.SampleData = "55.88";
        //                    break;
        //            }

        //            labelData.Add(localLabelData);
        //        }}
            

        //    var data = labelData.Where(x => !x.Field.StartsWith("#")).OrderBy(x => x.Field);
        //    var productDataMulti = labelData.Where(x => x.Field.StartsWith("#MultiLine")).OrderBy(x => x.Field);
        //    var productDataText = labelData.Where(x => x.Field.StartsWith("#Text")).OrderBy(x => x.Field.FindNumericSequence().ToInt());
        //    var dataToReturn = data.Concat(productDataMulti).Concat(productDataText).ToList();

        //    return dataToReturn;
        //}

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<Label> GetLabels()
        {
            return this.labelRepository.GetLabels();
        }

        /// <summary>
        /// Retrieve all the label fonts.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<LabelFont> GetLabelFonts()
        {
            return this.labelRepository.GetLabelFonts();
        }

        /// <summary>
        /// Saves a label.
        /// </summary>
        /// <param name="label">The label to save.</param>
        /// <returns>A flag, indicating a successful save or not.</returns>
        public bool SaveLabel(Label label)
        {
            return this.labelRepository.SaveLabel(label);
        }

        /// <summary>
        /// Retrieve all the label groups.
        /// </summary>
        /// <returns>A collection of abel groups.</returns>
        public IList<LabelGroup> GetLabelGroups()
        {
            return this.labelRepository.GetLabelGroups();
        }

        /// <summary>
        /// Add or updates the label groups list.
        /// </summary>
        /// <param name="labelGroups">The label groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateLabelGroups(IList<LabelGroup> labelGroups)
        {
            return this.labelRepository.AddOrUpdateLabelGroups(labelGroups);
        }

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        public IList<Gs1Data> GetGs1Master()
        {
            return (from gs1 in this.labelRepository.GetGs1Master()
                   select new Gs1Data
                   {
                       AI = gs1.AI,
                       Description = gs1.OfficialDescription,
                       DataLength = gs1.DataLength,
                       IsFixedLength = gs1.IsFixed
                   }).ToList();
        }

        #endregion

        #endregion

        #endregion

        #region private

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ParallelView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Print
{
    using System.Threading;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ParallelView.xaml
    /// </summary>
    public partial class ParallelView : UserControl
    {
        public ParallelView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxParallel.Dispatcher.Invoke(() => this.TextBoxParallel.Focus()));
        }
    }
}

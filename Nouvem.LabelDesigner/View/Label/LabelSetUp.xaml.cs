﻿// -----------------------------------------------------------------------
// <copyright file="LabelSetUp.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Label
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LabelSetUp.xaml
    /// </summary>
    public partial class LabelSetUp : UserControl
    {
        public LabelSetUp()
        {
            this.InitializeComponent();
        }
    }
}

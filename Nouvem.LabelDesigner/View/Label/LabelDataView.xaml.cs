﻿// -----------------------------------------------------------------------
// <copyright file="LabelDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Label
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for GeneralView.xaml
    /// </summary>
    public partial class LabelDataView : UserControl
    {
        public LabelDataView()
        {
            this.InitializeComponent();
        }
    }
}

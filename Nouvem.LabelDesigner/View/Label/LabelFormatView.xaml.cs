﻿// -----------------------------------------------------------------------
// <copyright file="LabelSetUp.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Label
{
    using System.Threading;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LabelFormatView.xaml
    /// </summary>
    public partial class LabelFormatView : UserControl
    {
        public LabelFormatView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxName.Dispatcher.Invoke(() => this.TextBoxName.Focus()));
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DesignerView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Neodynamic.Windows.WPF.ThermalLabelEditor;
using Nouvem.LabelDesigner.View.Data;
using Nouvem.LabelDesigner.View.Utility;
using Nouvem.Shared;

namespace Nouvem.LabelDesigner.View.Designer
{
    using System;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Neodynamic.SDK.Printing;
    using Neodynamic.Windows.ThermalLabelEditor;
    using Nouvem.LabelDesigner.BusinessLogic;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.LabelDesigner.View.UserInput;
    using Nouvem.Shared.Localisation;
    using Microsoft.Windows.Controls.Ribbon;
    using Microsoft.Win32;

    /// <summary>
    /// Interaction logic for DesignerView.xaml
    /// </summary>
    public partial class DesignerView
    {
        #region field

        /// <summary>
        /// The text item counter
        /// </summary>
        private static int textItemCounter = 1;

        /// <summary>
        /// The barcode item counter
        /// </summary>
        private static int barcodeItemCounter = 1;

        /// <summary>
        /// The rectangle item counter
        /// </summary>
        private static int rectangleItemCounter = 1;

        /// <summary>
        /// The ellipse item counter
        /// </summary>
        private static int ellipseItemCounter = 1;

        /// <summary>
        /// The literal item counter
        /// </summary>
        private static int literalItemCounter = 1;

        /// <summary>
        /// The line item counter
        /// </summary>
        private static int lineItemCounter = 1;

        /// <summary>
        /// The image item counter
        /// </summary>
        private static int imageItemCounter = 1;

        /// <summary>
        /// The rfid item counter
        /// </summary>
        private static int rfidItemCounter = 1;

        #endregion

        #region constructor

        public DesignerView()
        {
            this.InitializeComponent();
            this.RegisterMessages();
            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);

            this.ThermalLabelEditor.Loaded += (sender, args) =>
            {
                //this.ButtonShowGrid.Label = string.IsNullOrEmpty(Settings.Default.ShowGridOnOff)
                //    ? Strings.ShowGridOn
                //    : Settings.Default.ShowGridOnOff;
                this.ButtonShowGrid.Label = Strings.ShowGridOff;
                //this.ThermalLabelEditor.ShowGrid = Settings.Default.ShowGridOnOff == Strings.ShowGridOn;

                //this.ButtonSnap.Label = string.IsNullOrEmpty(Settings.Default.SnapToPixelOnOff)
                //    ? Strings.SnapToGridOn
                //    : Settings.Default.SnapToPixelOnOff;
                this.ButtonSnap.Label = Strings.SnapToGridOff;
                this.ThermalLabelEditor.GridSize = 0.1;

                //this.ThermalLabelEditor.SnapToGrid = Settings.Default.SnapToPixelOnOff == Strings.SnapToGridOn;
            };
        }

        #endregion

        #region item selection

        /// <summary>
        /// Sets the ActiveTool to the rectangle.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonRectangle_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Rectangle
            this.ThermalLabelEditor.ActiveTool = EditorTool.Rectangle;

            //Create and set the ActiveToolItem i.e. a RectangleShapeItem
            var rectItem = new RectangleShapeItem();
            rectItem.Name = string.Format("{0}{1}", Constant.RectangleItem, rectangleItemCounter++);
            rectItem.UseCache = false;
            rectItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);
            this.ThermalLabelEditor.ActiveToolItem = rectItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, rectItem.Name, string.Empty, string.Empty), Token.LabelItemChange);
        }

        /// <summary>
        /// Sets the ActiveTool to the ellipse.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonEllipse_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Ellipse
            this.ThermalLabelEditor.ActiveTool = EditorTool.Ellipse;

            //Create and set the ActiveToolItem i.e. a EllipseShapeItem
            var ellItem = new EllipseShapeItem();
            ellItem.Name = string.Format("{0}{1}", Constant.EllipseItem, ellipseItemCounter++);
            ellItem.UseCache = false;
            ellItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = ellItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, ellItem.Name, string.Empty, string.Empty), Token.LabelItemChange);
        }

        /// <summary>
        /// Sets the ActiveTool to the literal.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonLiteral_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Literal
            this.ThermalLabelEditor.ActiveTool = EditorTool.Literal;

            //Create and set the ActiveToolItem i.e. a LineShapeItem
            var literalItem = new LiteralItem();
            literalItem.Name = string.Format("{0}{1}", Constant.LiteralItem, literalItemCounter++);
            literalItem.UseCache = false;
            literalItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = literalItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, literalItem.Name, string.Empty, string.Empty), Token.LabelItemChange);
        }

        /// <summary>
        /// Sets the ActiveTool to the line.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonLine_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Line
            this.ThermalLabelEditor.ActiveTool = EditorTool.Line;

            //Create and set the ActiveToolItem i.e. a LineShapeItem
            var lineItem = new LineShapeItem();
            lineItem.Name = string.Format("{0}{1}", Constant.LineItem, lineItemCounter++);
            lineItem.UseCache = false;
            lineItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = lineItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, lineItem.Name, string.Empty, string.Empty), Token.LabelItemChange);
        }

        /// <summary>
        /// Sets the ActiveTool to the image.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonImage_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (this.ThermalLabelEditor.LabelDocument == null)
                {
                    // no label on the surface
                    return;
                }

                //Set the ActiveTool to Image
                this.ThermalLabelEditor.ActiveTool = EditorTool.Image;

                var dialog = new OpenFileDialog();
                if (dialog.ShowDialog() == true)
                {
                    var imgItem = new ImageItem();
                    imgItem.Name = string.Format("{0}{1}", Constant.ImageItem, imageItemCounter++);
                    imgItem.UseCache = true;
                    imgItem.CacheItemId = "1";
                    imgItem.CacheItemId = imageItemCounter.ToString();
                    imgItem.SourceFile = dialog.FileName;
                    this.ThermalLabelEditor.ActiveToolItem = imgItem;

                    // Notify the label designer that a new item has been added.
                    Messenger.Default.Send(Tuple.Create(Constant.Add, imgItem.Name, string.Empty, string.Empty),
                        Token.LabelItemChange);
                }
                else
                {
                    var imgItem = new ImageItem();
                    imgItem.UseCache = true;
                    this.ThermalLabelEditor.ActiveToolItem = imgItem;
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Sets the ActiveTool to the text.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonText_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Text
            this.ThermalLabelEditor.ActiveTool = EditorTool.Text;

            //Create and set the ActiveToolItem i.e. a TextItem
            var textItem = new TextItem();
            //textItem.Font.Name = Font.NativePrinterFontB;
            textItem.Font.Name = "Segoe UI";
            textItem.Font.NameAtPrinterStorage = "E:SEGOEUI";
            //textItem.Font.Name = Font.NativePrinterFontA;
            textItem.Font.Size = 14;
            textItem.Text = Strings.Sample;
            textItem.Name = string.Format("{0}{1}", Constant.TextItem, textItemCounter++);
            textItem.UseCache = false;
            //textItem.PrintAsGraphic

            textItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = textItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, textItem.Name, textItem.Text, textItem.DataField), Token.LabelItemChange);
        }

        /// <summary>
        /// Sets the ActiveTool to the RFID tag.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonRFID_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Text
            this.ThermalLabelEditor.ActiveTool = EditorTool.RFIDTag;

            //Create and set the ActiveToolItem i.e. a TextItem
            var rfidTagItem = new RFIDTagItem();
            rfidTagItem.Name = string.Format("{0}{1}", Constant.RfidItem, rfidItemCounter++);
            rfidTagItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);
            this.ThermalLabelEditor.ActiveToolItem = rfidTagItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, rfidTagItem.Name, string.Empty, string.Empty), Token.LabelItemChange);
        }

        /// <summary>
        /// Sets the ActiveTool to the barcode.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonBarcode_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Barcode
            this.ThermalLabelEditor.ActiveTool = EditorTool.Barcode;

            var barcodeItem = new BarcodeItem();
            barcodeItem.Symbology = BarcodeSymbology.Code128;
            barcodeItem.Code = BarcodeItemUtils.GenerateSampleCode(barcodeItem.Symbology);
            barcodeItem.Font.Name = Font.NativePrinterFontA;
            barcodeItem.Font.Size = 5;
            barcodeItem.BarcodeAlignment = BarcodeAlignment.MiddleCenter;
            barcodeItem.Name = string.Format("{0}{1}", Constant.BarcodeItem, barcodeItemCounter++);
            barcodeItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = barcodeItem;

            // Notify the label designer that a new item has been added.
            Messenger.Default.Send(Tuple.Create(Constant.Add, barcodeItem.Name, barcodeItem.Text, barcodeItem.DataField), Token.LabelItemChange);
        }

        #endregion

        #region export

        /// <summary>
        /// Gets the current label, exports it, and prints.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonExportToImage_Click(object sender, RoutedEventArgs e)
        {
            var label = this.ThermalLabelEditor.CreateThermalLabel();
            var manager = PrintManager.Instance;
            if (manager.ExportToImage(label, false) == string.Empty)
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelExported);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelExportError);
            }
        }

        /// <summary>
        /// Gets the current label, exports it to pdf format.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonExportToPdf_Click(object sender, RoutedEventArgs e)
        {
            var label = this.ThermalLabelEditor.CreateThermalLabel();
            var manager = PrintManager.Instance;
            if (manager.ExportToPdf(label) == string.Empty)
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelExported);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelExportError);
            }
        }

        /// <summary>
        /// Gets the current label, exports it to xml format.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonExportToXml_Click(object sender, RoutedEventArgs e)
        {
            var label = this.ThermalLabelEditor.CreateThermalLabel();
            var manager = PrintManager.Instance;
            if (manager.ExportToXml(label) == string.Empty)
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelExported);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelExportError);
            }
        }

        #endregion

        #region message registration

        /// <summary>
        /// Register for incoming messages.
        /// </summary>
        private void RegisterMessages()
        {
            Messenger.Default.Register<ThermalLabel>(this, label =>
            {
                if (this.ThermalLabelEditor.LabelDocument == null)
                {
                    this.ThermalLabelEditor.LoadThermalLabel(label);
                    this.ThermalLabelEditor.LabelDocument.NumOfFractionalDigits = 4;
                    return;
                }

                this.ThermalLabelEditor.UpdateLabelDocument(label.UnitType, label.Width, label.Height, 4, label.GapLength, label.MarkLength, label.OffsetLength, label.LabelsPerRow, label.LabelsHorizontalGapLength, label.IsContinuous);
                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<string>(this, Token.NewLabel, label =>
            {
                this.ThermalLabelEditor.DeleteAll();
                this.ThermalLabelEditor.UpdateLabelDocument(UnitType.Inch, 4, 3, 4, 0, 0, 0, 1, 0, false);

                // reset the item counters
                this.ResetItemCounters();
                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<ThermalLabel>(this, Token.LoadLabel, label =>{
                this.ThermalLabelEditor.LoadThermalLabel(label);
                this.ThermalLabelEditor.LabelDocument.NumOfFractionalDigits = 4;
                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<Item>(this, Token.SelectedItemUpdate, item =>
            {
                try
                {
                    var currentItem = this.ThermalLabelEditor.CurrentSelection;
                    if (currentItem == null)
                    {
                        return;
                    }

                    if (currentItem is ShapeItem && item is ShapeItem)
                    {
                        (currentItem as ShapeItem).StrokeThickness = (item as ShapeItem).StrokeThickness;

                        if (currentItem is LineShapeItem && item is LineShapeItem)
                        {
                            (currentItem as LineShapeItem).Orientation = (item as LineShapeItem).Orientation;
                        }
                    }
                    else if (currentItem.GetType() == item.GetType())
                    {
                        if (currentItem is TextItem)
                        {
                            this.ThermalLabelEditor.TextItemEditModeEnabled = false;
                            currentItem.UpdateFrom(item);
                            this.ThermalLabelEditor.TextItemEditModeEnabled = true;
                        }
                        else
                        {
                            currentItem.UpdateFrom(item);
                        }
                    }

                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }
                finally
                {
                   
                }
              
                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<string>(this, Token.SelectedItemUpdate, text =>
            {
                //this.ThermalLabelEditor.TextItemEditModeEnabled = false;
                var currentItem = this.ThermalLabelEditor.CurrentSelection as TextItem;

                if (currentItem != null)
                {
                    // change the font to the incoming font
                    currentItem.Text = text;

                    // update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }

                //this.ThermalLabelEditor.TextItemEditModeEnabled = true;
                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<int>(this, Token.SelectedItemUpdate, i =>
            {
                //this.ThermalLabelEditor.TextItemEditModeEnabled = false;
                var currentItem = this.ThermalLabelEditor.CurrentSelection as TextItem;

                if (currentItem != null)
                {
                    // change the rotation to the incoming font
                    currentItem.RotationAngle = i;

                    // update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }

                //this.ThermalLabelEditor.TextItemEditModeEnabled = true;
                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<Neodynamic.SDK.Printing.TextAlignment>(this, Token.SelectedItemUpdate, value =>
            {
                var currentItem = this.ThermalLabelEditor.CurrentSelection as TextItem;

                if (currentItem != null)
                {
                    // change the allignment value to the incoming allignment
                    currentItem.TextAlignment = value;

                    // update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }

                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<string>(this, Token.OpenFont, x =>
            {
                if (this.ThermalLabelEditor.CurrentSelection is TextItem)
                {
                    var currentItem = this.ThermalLabelEditor.CurrentSelection as TextItem;

                    // Send the text item details to the Font view
                    Messenger.Default.Send(Tuple.Create(currentItem.Font, currentItem.Text, currentItem.BackColor, currentItem.ForeColor), Token.OpenFont);
                    return;
                }

                if (this.ThermalLabelEditor.CurrentSelection is BarcodeItem)
                {
                    var currentItem = this.ThermalLabelEditor.CurrentSelection as BarcodeItem;

                    // Send the barcode item details to the Font view
                    Messenger.Default.Send(Tuple.Create(currentItem.Font, currentItem.Code, currentItem.BackColor, currentItem.ForeColor), Token.OpenFont);
                }

                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            // Register for the changing of a font.
            Messenger.Default.Register<Tuple<Font, Tuple<Color, Color>>>(this, Token.SetFont, x =>
            {
                if (this.ThermalLabelEditor.CurrentSelection is TextItem)
                {
                    var currentItem = this.ThermalLabelEditor.CurrentSelection as TextItem;

                    // change the font to the incoming font
                    currentItem.Font.UpdateFrom(x.Item1);

                    // Set the colors
                    currentItem.BackColor = x.Item2.Item1;
                    currentItem.ForeColor = x.Item2.Item2;

                    // update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                    return;
                }

                if (this.ThermalLabelEditor.CurrentSelection is BarcodeItem)
                {
                    var currentItem = this.ThermalLabelEditor.CurrentSelection as BarcodeItem;

                    // change the font to the incoming font
                    currentItem.Font.UpdateFrom(x.Item1);

                    // Set the colors
                    currentItem.BackColor = x.Item2.Item1;
                    currentItem.ForeColor = x.Item2.Item2;

                    // update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                    return;
                }

                Messenger.Default.Send(Token.Message, Token.LabelItemChange);
            });

            Messenger.Default.Register<string>(this, Token.OpenMessageBox, x =>
            {
                var messageBox = new NouvemMessageBoxView();
                messageBox.ShowDialog();
                messageBox.Focus();
            });

            Messenger.Default.Register<decimal>(this, Token.GridGapValue, d => this.ThermalLabelEditor.GridSize = d.ToDouble());

            Messenger.Default.Register<string>(this, Token.SendLabelEditor, s => Messenger.Default.Send(this.ThermalLabelEditor, Token.LabelEditorSent));
        }

        #endregion

        #region clipboard

        /// <summary>
        /// Handles the clipboard or context menu selections.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ButtonClipboard_Click(object sender, RoutedEventArgs e)
        {
            var command = string.Empty;
            if (sender is MenuItem)
            {
                command = (sender as MenuItem).Header.ToString();
            }

            if (sender is RibbonButton)
            {
                command = (sender as RibbonButton).Label;
            }

            if (command == string.Empty)
            {
                return;
            }

            if (command.Equals(Strings.Cut))
            {
                if (this.ThermalLabelEditor.CanCut)
                {
                    this.ThermalLabelEditor.Cut();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLabelItemSelected);
                }
            }
            else if (command.Equals(Strings.Copy))
            {
                if (this.ThermalLabelEditor.CanCopy)
                {
                    this.ThermalLabelEditor.Copy();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLabelItemSelected);
                }
            }
            else if (command.Equals(Strings.Paste))
            {
                if (this.ThermalLabelEditor.CanPaste)
                {
                    this.ThermalLabelEditor.Paste();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLabelItemSelected);
                }
            }
            else if (command.Equals(Strings.Undo))
            {
                if (this.ThermalLabelEditor.CanUndo)
                {
                    this.ThermalLabelEditor.Undo();
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.UndoRedoLimitReached, Strings.Undo));
                }
            }
            else if (command.Equals(Strings.Redo))
            {
                if (this.ThermalLabelEditor.CanRedo)
                {
                    this.ThermalLabelEditor.Redo();
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.UndoRedoLimitReached, Strings.Redo));
                }
            }
            else if (command.Equals(Strings.DeleteSelected))
            {
                this.ThermalLabelEditor.DeleteSelectedItems();

                if (this.ThermalLabelEditor.ActiveToolItem != null)
                {
                    var activeItemType = this.ThermalLabelEditor.ActiveToolItem.GetType();
                    if (activeItemType == typeof (BarcodeItem))
                    {
                        if (this.ThermalLabelEditor.ActiveToolItem.Name.Equals("BarcodeItem1"))
                        {
                            barcodeItemCounter = 1;
                        }
                        else
                        {
                            barcodeItemCounter = 2;
                        }
                    }

                    // Notify the label designer that an item has been removed.
                    Messenger.Default.Send(Tuple.Create(Constant.Remove, this.ThermalLabelEditor.ActiveToolItem.Name, string.Empty, string.Empty), Token.LabelItemChange);
                }
            }
            else if (command.Equals(Strings.DeleteAll))
            {
                this.ThermalLabelEditor.DeleteAll();
                this.ResetItemCounters();

                // Notify the label designer that all items have been removed. 
                Messenger.Default.Send(Tuple.Create(Constant.RemoveAll, string.Empty, string.Empty, string.Empty), Token.LabelItemChange);
            }
        }

        #endregion

        #region reset counters

        /// <summary>
        /// Resets the item counters.
        /// </summary>
        private void ResetItemCounters()
        { 
            textItemCounter = 1;
            barcodeItemCounter = 1;
            ellipseItemCounter = 1;
            rectangleItemCounter = 1;
            rfidItemCounter = 1;
            literalItemCounter = 1;
            lineItemCounter = 1;
            imageItemCounter = 1;
        }

        #endregion

        private void UploadFont_Click(object sender, RoutedEventArgs e)
        {
            var ttfToPrinter = new TTFtoPrinterStorageDialog();
            
            ttfToPrinter.ShowDialog();
        }

        private void ManageFonts_Click(object sender, RoutedEventArgs e)
        {
            var printerFonts = new PrinterFontsDialog();
            printerFonts.ShowDialog();
        }

        private void ButtonShowGrid_Click(object sender, RoutedEventArgs e)
        {
            this.ThermalLabelEditor.ShowGrid = !this.ThermalLabelEditor.ShowGrid;
            this.ThermalLabelEditor.ShowGridLines = true;
            this.ButtonShowGrid.Label = this.ThermalLabelEditor.ShowGrid ? Strings.ShowGridOn : Strings.ShowGridOff;
            Settings.Default.ShowGridOnOff = this.ButtonShowGrid.Label;
            SystemMessage.Write(MessageType.Priority, string.Format(Message.ShowGridOnOff, this.ThermalLabelEditor.ShowGrid.BoolToOnOff().ToLower()));
        }

        private void ButtonSnap_Click(object sender, RoutedEventArgs e)
        {
            this.ThermalLabelEditor.SnapToGrid = !this.ThermalLabelEditor.SnapToGrid;
            this.ButtonSnap.Label = this.ThermalLabelEditor.SnapToGrid ? Strings.SnapToGridOn : Strings.SnapToGridOff;
            Settings.Default.SnapToPixelOnOff = this.ButtonSnap.Label;
            SystemMessage.Write(MessageType.Priority, string.Format(Message.SnapToPixelsOnOff, this.ThermalLabelEditor.SnapToGrid.BoolToOnOff().ToLower()));
        }

        private void ButtonGridGap_Click(object sender, RoutedEventArgs e)
        {
            var gridGap = new GridGapView();
            gridGap.ShowDialog();
            gridGap.Focus();
        }

        private void ButtonLock_Click(object sender, RoutedEventArgs e)
        {
            this.ThermalLabelEditor.LockSelectedItems();
        }

        private void ButtonUnlock_Click(object sender, RoutedEventArgs e)
        {
            this.ThermalLabelEditor.UnlockSelectedItems();
        }

        private void ButtonSaveAs_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.ThermalLabelEditor, Token.SaveAs);
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.ThermalLabelEditor, Token.Delete);
        }
    }
}

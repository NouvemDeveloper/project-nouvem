﻿// -----------------------------------------------------------------------
// <copyright file="DesignerContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Designer
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Properties;

    /// <summary>
    /// Interaction logic for DesignerContainerView.xaml
    /// </summary>
    public partial class DesignerContainerView : Window
    {
        public DesignerContainerView()
        {
            this.InitializeComponent();
            this.Closing += (sender, args) => Messenger.Default.Send(Token.Message, Token.SendLabelEditor);
            this.Closed += (sender, args) => Settings.Default.Save();

            //Messenger.Default.Register<Tuple<Font, string>>(this, Token.OpenFont, font =>
            //{
            //    var fontView = new FontView(font) { Owner = this };
            //    fontView.ShowDialog();
            //    fontView.Focus();
            //});
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using GalaSoft.MvvmLight.Messaging;
using Nouvem.LabelDesigner.Global;
using Nouvem.LabelDesigner.Properties;
using Nouvem.Shared;

namespace Nouvem.LabelDesigner.View.Utility
{
    /// <summary>
    /// Interaction logic for GridGapView.xaml
    /// </summary>
    public partial class GridGapView : Window
    {
        public GridGapView()
        {
            this.InitializeComponent();
            this.Closing += (sender, args) => Settings.Default.Save();
            this.Loaded += (sender, args) =>
            {
                this.SpinEditGap.Text = "0.1";
            };
        }

        private void SpinEditGap_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            if (this.SpinEditGap.Value <= 0)
            {
                this.SpinEditGap.Value = 0.3M;
            }
            else
            {
                // Settings.Default.LabelGap = this.SpinEditGap.Text;
                Messenger.Default.Send(this.SpinEditGap.Value, Token.GridGapValue);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

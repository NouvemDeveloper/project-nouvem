﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Neodynamic.SDK.Printing;
using System.Printing;
using Nouvem.LabelDesigner.ViewModel;

namespace Nouvem.LabelDesigner.View.Utility
{
    /// <summary>
    /// Interaction logic for PrintJobDialog.xaml
    /// </summary>
    public partial class PrintJobDialog : Window
    {

        PrinterSettings _printerSettings = new PrinterSettings();
        int _copies = 1;
        PrintOrientation _printOrientation = PrintOrientation.Portrait;


        public PrintJobDialog()
        {
            InitializeComponent();

            this.Init();
        }

        private void Init()
        {
            this.cboProgLang.SelectedIndex = 0;
            this.cboPrintOrientation.SelectedIndex = 0;

            //Load installed printers...
            LocalPrintServer printServer = new LocalPrintServer();
            PrintQueueCollection printQueuesOnLocalServer = printServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local });
            List<string> installedPrinters = new List<string>();
            foreach (PrintQueue printer in printQueuesOnLocalServer)
            {
                installedPrinters.Add(printer.Name);
            }

            this.cboPrinters.DataContext = installedPrinters;

            //Load Serial Comm settings...
            this.cboSerialPorts.DataContext = System.IO.Ports.SerialPort.GetPortNames();
            this.cboParity.DataContext = Enum.GetNames(typeof(System.IO.Ports.Parity));
            this.cboStopBits.DataContext = Enum.GetNames(typeof(System.IO.Ports.StopBits));
            this.cboFlowControl.DataContext = Enum.GetNames(typeof(System.IO.Ports.Handshake));

            this.txtIPAddress.Text = ViewModelLocator.PrinterSetUpStatic.IpAddress;
            this.txtIPPort.Text = ViewModelLocator.PrinterSetUpStatic.Port.ToString();
            this.cboSerialPorts.SelectedItem = ViewModelLocator.PrinterSetUpStatic.ComPort;
            this.cboParity.SelectedItem = ViewModelLocator.PrinterSetUpStatic.Parity;
            this.cboFlowControl.SelectedItem = ViewModelLocator.PrinterSetUpStatic.Handshake;
            //this.txtDataBits.Text = ViewModelLocator.PrinterSetUpStatic.Te;


        }

        public PrinterSettings PrinterSettings
        {
            get { return _printerSettings; }
        }

        public int Copies
        {
            get { return _copies; }
        }

        public PrintOrientation PrintOrientation
        {
            get { return _printOrientation; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            try
            {
                //Update printer comm object...
                if (this.tabPrintComm.SelectedIndex == 0)
                {
                    //USB
                    _printerSettings.Communication.CommunicationType = CommunicationType.USB;
                    _printerSettings.PrinterName = cboPrinters.SelectedItem.ToString();
                }
                else if (this.tabPrintComm.SelectedIndex == 1)
                {
                    //Parallel
                    _printerSettings.Communication.CommunicationType = CommunicationType.Parallel;
                    _printerSettings.Communication.ParallelPortName = this.txtParallelPort.Text;
                }
                else if (this.tabPrintComm.SelectedIndex == 2)
                {
                    //Serial
                    _printerSettings.Communication.CommunicationType = CommunicationType.Serial;
                    _printerSettings.Communication.SerialPortName = cboSerialPorts.SelectedItem.ToString();
                    _printerSettings.Communication.SerialPortBaudRate = int.Parse(this.txtBaudRate.Text);
                    _printerSettings.Communication.SerialPortDataBits = int.Parse(this.txtDataBits.Text);
                    _printerSettings.Communication.SerialPortFlowControl = (System.IO.Ports.Handshake)Enum.Parse(typeof(System.IO.Ports.Handshake), cboFlowControl.SelectedItem.ToString());
                    _printerSettings.Communication.SerialPortParity = (System.IO.Ports.Parity)Enum.Parse(typeof(System.IO.Ports.Parity), cboParity.SelectedItem.ToString());
                    _printerSettings.Communication.SerialPortStopBits = (System.IO.Ports.StopBits)Enum.Parse(typeof(System.IO.Ports.StopBits), cboStopBits.SelectedItem.ToString());
                }
                else if (this.tabPrintComm.SelectedIndex == 3)
                {
                    //Network
                    _printerSettings.Communication.CommunicationType = CommunicationType.Network;
                    System.Net.IPAddress ipAddress = System.Net.IPAddress.None;

                    try
                    {
                        ipAddress = System.Net.IPAddress.Parse(this.txtIPAddress.Text);
                    }
                    catch
                    { }

                    if (ipAddress != System.Net.IPAddress.None) //use IP
                        _printerSettings.Communication.NetworkIPAddress = ipAddress;
                    else //try Host Name
                        _printerSettings.PrinterName = this.txtIPAddress.Text;

                    _printerSettings.Communication.NetworkPort = int.Parse(this.txtIPPort.Text);

                }

                _printerSettings.Dpi = double.Parse(this.txtDpi.Text);
                _printerSettings.ProgrammingLanguage = (ProgrammingLanguage)Enum.Parse(typeof(ProgrammingLanguage), ((ComboBoxItem)cboProgLang.SelectedItem).Content.ToString());


                _copies = int.Parse(this.txtCopies.Text);
                _printOrientation = (PrintOrientation)Enum.Parse(typeof(PrintOrientation), ((ComboBoxItem)cboPrintOrientation.SelectedItem).Content.ToString());


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.DialogResult = false;
            }

        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TextItemView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TextItemView.xaml
    /// </summary>
    public partial class TextItemView : UserControl
    {
        public TextItemView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TextItemView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TextItemFormatView.xaml
    /// </summary>
    public partial class TextItemFormatView : UserControl
    {
        public TextItemFormatView()
        {
            this.InitializeComponent();
        }
    }
}

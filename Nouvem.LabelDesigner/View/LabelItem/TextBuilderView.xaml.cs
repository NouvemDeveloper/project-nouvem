﻿// -----------------------------------------------------------------------
// <copyright file="TextBuilderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TextBuilderView.xaml
    /// </summary>
    public partial class TextBuilderView : UserControl
    {
        public TextBuilderView()
        {
            this.InitializeComponent();
        }
    }
}

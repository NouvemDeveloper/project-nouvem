﻿// -----------------------------------------------------------------------
// <copyright file="BarcodeBuilderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BarcodeBuilderView.xaml
    /// </summary>
    public partial class BarcodeBuilderView : UserControl
    {
        public BarcodeBuilderView()
        {
            this.InitializeComponent();
        }
    }
}

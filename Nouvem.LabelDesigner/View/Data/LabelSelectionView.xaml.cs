﻿// -----------------------------------------------------------------------
// <copyright file="LabelSelectionView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Data
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LabelSelectionView.xaml
    /// </summary>
    public partial class LabelSelectionView : UserControl
    {
        public LabelSelectionView()
        {
            this.InitializeComponent();
            this.GridControlLabels.View.ShowSearchPanel(true);
        }
    }
}

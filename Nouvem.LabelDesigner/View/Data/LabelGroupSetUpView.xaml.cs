﻿// -----------------------------------------------------------------------
// <copyright file="LabelGroupSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Data
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LabelGroupSetUpView.xaml
    /// </summary>
    public partial class LabelGroupSetUpView : UserControl
    {
        public LabelGroupSetUpView()
        {
            this.InitializeComponent();
        }
    }
}

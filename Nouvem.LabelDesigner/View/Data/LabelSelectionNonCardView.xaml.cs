﻿// -----------------------------------------------------------------------
// <copyright file="LabelSelectionNonCardView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Data
{
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for LabelSelectionNonCardView.xaml
    /// </summary>
    public partial class LabelSelectionNonCardView : UserControl
    {
        public LabelSelectionNonCardView()
        {
            this.InitializeComponent();
        }

        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }
    }
}

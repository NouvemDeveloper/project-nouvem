﻿// -----------------------------------------------------------------------
// <copyright file="PrinterSetUpViewModel .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel.Print
{
    using System;
    using System.Collections.Generic;
    using System.IO.Ports;
    using System.Linq;
    using System.Printing;
    using System.Windows.Forms;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Neodynamic.SDK.Printing;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Shared;
    using Shared = Nouvem.Shared.Localisation;
    using PrintMode = Nouvem.LabelDesigner.Model.Enum.PrintMode;

    public class PrinterSetUpViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The com port field.
        /// </summary>
        private string comPort;

        /// <summary>
        /// The baud rate field.
        /// </summary>
        private string baudRate;

        /// <summary>
        /// The parity field.
        /// </summary>
        private string parity;

        /// <summary>
        /// The stopbits field.
        /// </summary>
        private string stopBits;

        /// <summary>
        /// The databits field.
        /// </summary>
        private string dataBits;

        /// <summary>
        /// The handshake field.
        /// </summary>
        private string handshake;

        /// <summary>
        /// The usb selection flag.
        /// </summary>
        private bool usb;

        /// <summary>
        /// The serial selection flag.
        /// </summary>
        private bool serial;

        /// <summary>
        /// The ethernet selection flag.
        /// </summary>
        private bool ethernet;

        /// <summary>
        /// The parallel selection flag.
        /// </summary>
        private bool parallel;

        /// <summary>
        /// The image selection flag.
        /// </summary>
        private bool image;

        /// <summary>
        /// The ip address field.
        /// </summary>
        private string ipAddress;

        /// <summary>
        /// The port number.
        /// </summary>
        private int port;

        /// <summary>
        /// The parallel port name field.
        /// </summary>
        private string parallelPortName;

        /// <summary>
        /// The printer name selected field.
        /// </summary>
        private string printerName;

        /// <summary>
        /// The dpi resolution field.
        /// </summary>
        private int dpi;

        /// <summary>
        /// The number of copies field.
        /// </summary>
        private int copies;

        /// <summary>
        /// The printer language selected field.
        /// </summary>
        private string printerLanguage;

        /// <summary>
        /// The printer orientation selected field.
        /// </summary>
        private string printOrientation;

        /// <summary>
        /// The image types.
        /// </summary>
        private string[] imageTypes;

        /// <summary>
        /// The selected image type.
        /// </summary>
        private string selectedImageType;

        /// <summary>
        /// The export image path.
        /// </summary>
        private string exportImagePath;

        /// <summary>
        /// The use default printer flag.
        /// </summary>
        private bool useDefaultPrinter;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the PrinterSetUpViewModel class.
        /// </summary>
        public PrinterSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // handle the form unload event.
            this.OnUnloadedCommand = new RelayCommand(Settings.Default.Save);

            // handle animage path change.
            this.ImagePathCommand = new RelayCommand(ImagePathCommandExecute);

            #endregion

            this.SetProperties();
            this.GetPrinters();
            this.GetParallelPorts();
            this.GetPrinterLanguages();
            this.GetPrintOrientations();
            this.GetData();
            this.GetImageTypes();

            this.SelectedImageType = string.IsNullOrEmpty(Settings.Default.SelectedImageType)
              ? ImageFormat.Png.ToString()
              : Settings.Default.SelectedImageType;

            this.ExportImagePath = Settings.Default.ExportImagePath;
            this.UseDefaultPrinter = Settings.Default.UseDefaultPrinter;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether we are using the deafult printer.
        /// </summary>
        public bool UseDefaultPrinter
        {
            get
            {
                return this.useDefaultPrinter;
            }

            set
            {
                this.useDefaultPrinter = value;
                this.RaisePropertyChanged();
                Settings.Default.UseDefaultPrinter = value;
                Settings.Default.Save();
            }
        }
       
        /// <summary>
        /// Gets or sets the export image path.
        /// </summary>
        public string ExportImagePath
        {
            get
            {
                return this.exportImagePath;
            }

            set
            {
                this.exportImagePath = value;
                this.RaisePropertyChanged();
                Settings.Default.ExportImagePath = value;
            }
        }

        /// <summary>
        /// Gets or sets the com ports.
        /// </summary>
        public string[] ComPorts { get; set; }

        /// <summary>
        /// Gets or sets the baud rate values.
        /// </summary>
        public string[] BaudRates { get; set; }

        /// <summary>
        /// Gets or sets the data bit values.
        /// </summary>
        public string[] DataBitsValues { get; set; }

        /// <summary>
        /// Gets or sets the parity values.
        /// </summary>
        public string[] Paritys { get; set; }

        /// <summary>
        /// Gets or sets the handshake values.
        /// </summary>
        public string[] Handshakes { get; set; }

        /// <summary>
        /// Gets or sets the stop bits values.
        /// </summary>
        public string[] StopbitsValues { get; set; }

        /// <summary>
        /// Gets or sets the parallel ports.
        /// </summary>
        public string[] ParallelPorts{ get; set; }

        /// <summary>
        /// Gets or sets the printer names.
        /// </summary>
        public IList<string> PrinterNames { get; set; }

        /// <summary>
        /// Gets or sets the printer languages.
        /// </summary>
        public string[] PrinterLanguages { get; set; }

        /// <summary>
        /// Gets or sets the print orientations.
        /// </summary>
        public string[] PrintOrientations { get; set; }

        /// <summary>
        /// Gets or sets the image types.
        /// </summary>
        public string[] ImageTypes
        {
            get
            {
                return this.imageTypes;
            }

            set
            {
                this.imageTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected image type.
        /// </summary>
        public string SelectedImageType
        {
            get
            {
                return this.selectedImageType;
            }

            set
            {
                this.selectedImageType = value;
                this.RaisePropertyChanged();
                Settings.Default.SelectedImageType = value;
            }
        }

        /// <summary>
        /// Gets or sets the com port.
        /// </summary>
        public string ComPort
        {
            get
            {
                return this.comPort;
            }

            set
            {
                this.comPort = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.ComPort, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the baud rate.
        /// </summary>
        public string BaudRate
        {
            get
            {
                return this.baudRate;
            }

            set
            {
                this.baudRate = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.BaudRate, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the parity.
        /// </summary>
        public string Parity
        {
            get
            {
                return this.parity;
            }

            set
            {
                this.parity = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.Parity, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the stop bits.
        /// </summary>
        public string StopBits
        {
            get
            {
                return this.stopBits;
            }

            set
            {
                this.stopBits = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.StopBits, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the databits.
        /// </summary>
        public string DataBits
        {
            get
            {
                return this.dataBits;
            }

            set
            {
                this.dataBits = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.DataBits, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the handshake.
        /// </summary>
        public string Handshake
        {
            get
            {
                return this.handshake;
            }

            set
            {
                this.handshake = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.Handshake, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether usb is the selected print mode.
        /// </summary>
        public bool USB
        {
            get
            {
                return this.usb;
            }

            set
            {
                this.usb = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SavePrintMode(PrintMode.USB);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether serial is the selected print mode.
        /// </summary>
        public bool Serial
        {
            get
            {
                return this.serial;
            }

            set
            {
                this.serial = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SavePrintMode(PrintMode.Serial);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ethernet is the selected print mode.
        /// </summary>
        public bool Ethernet
        {
            get
            {
                return this.ethernet;
            }

            set
            {
                this.ethernet = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SavePrintMode(PrintMode.Network);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether parallel is the selected print mode.
        /// </summary>
        public bool Parallel
        {
            get
            {
                return this.parallel;
            }

            set
            {
                this.parallel = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SavePrintMode(PrintMode.Parallel);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether usb is the selected print mode.
        /// </summary>
        public bool Image
        {
            get
            {
                return this.image;
            }

            set
            {
                this.image = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SavePrintMode(PrintMode.Image);
                }
            }
        }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        public string IpAddress
        {
            get
            {
                return this.ipAddress;
            }

            set
            {
                this.ipAddress = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.IPHost, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port
        {
            get
            {
                return this.port;
            }

            set
            {
                this.port = value;
                this.RaisePropertyChanged();

                this.SaveProperty(PrintProperty.Port, value);
            }
        }

        /// <summary>
        /// Gets or sets the parallel port name.
        /// </summary>
        public string ParallelPortName
        {
            get
            {
                return this.parallelPortName;
            }

            set
            {
                this.parallelPortName = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.ParallelPort, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the usb printer name.
        /// </summary>
        public string PrinterName
        {
            get
            {
                return this.printerName;
            }

            set
            {
                this.printerName = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.PrinterName, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the printer language.
        /// </summary>
        public string PrinterLanguage
        {
            get
            {
                return this.printerLanguage;
            }

            set
            {
                this.printerLanguage = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.PrinterLanguage, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the print oriention.
        /// </summary>
        public string PrintOrientation
        {
            get
            {
                return this.printOrientation;
            }

            set
            {
                this.printOrientation= value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SaveProperty(PrintProperty.PrintOrientation, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the print resolution.
        /// </summary>
        public int DPI
        {
            get
            {
                return this.dpi;
            }

            set
            {
                this.dpi = value;
                this.RaisePropertyChanged();
                this.SaveProperty(PrintProperty.DPI, value);
            }
        }

        /// <summary>
        /// Gets or sets the print number of copies.
        /// </summary>
        public int Copies
        {
            get
            {
                return this.copies;
            }

            set
            {
                this.copies= value;
                this.RaisePropertyChanged();
                this.SaveProperty(PrintProperty.Copies, value);
            }
        }
      
        #endregion

        #region command
        
        /// <summary>
        /// Command to handle an image path change.
        /// </summary>
        public ICommand ImagePathCommand { get; set; }

        /// <summary>
        /// Command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; set; }

        /// <summary>
        /// Command to handle the form unload.
        /// </summary>
        public ICommand OnUnloadedCommand { get; set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command

        /// <summary>
        /// Opens a folder browser for the user to select the image path.
        ///  </summary>
        private void ImagePathCommandExecute()
        {
            var openFileDialog = new FolderBrowserDialog();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.ExportImagePath = openFileDialog.SelectedPath;
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Saves the selected print mode.
        /// </summary>
        /// <param name="mode">The mode to save.</param>
        private void SavePrintMode(PrintMode mode)
        {
            Settings.Default.PrintMode = mode.ToString();
            Settings.Default.Save();
            this.PrintManager.SetPrinterSettings();
            SystemMessage.Write(MessageType.Priority, string.Format(Shared.Message.PrintModeChanged, mode));
        }

        /// <summary>
        /// Saves the selected setting value.
        /// </summary>
        /// <param name="property">The property to change.</param>
        /// <param name="value">The new value.</param>
        private void SaveProperty(PrintProperty property, object value)
        {
            switch (property)
            {
                case PrintProperty.ComPort:
                    Settings.Default.ComPort = value as string;
                    break;

                case PrintProperty.BaudRate:
                    Settings.Default.BaudRate = value as string;
                    break;

                case PrintProperty.Parity:
                    Settings.Default.Parity = value as string;
                    break;

                case PrintProperty.DataBits:
                    Settings.Default.DataBits = value as string;
                    break;

                case PrintProperty.StopBits:
                    Settings.Default.StopBits = value as string;
                    break;

                case PrintProperty.Handshake:
                    Settings.Default.Handshake = value as string;
                    break;

                case PrintProperty.IPHost:
                    Settings.Default.IPHost = value as string;
                    break;

                case PrintProperty.Port:
                    Settings.Default.Port = (int)value;
                    break;

                case PrintProperty.ParallelPort:
                    Settings.Default.ParallelPortName = value as string;
                    break;

                case PrintProperty.PrinterName:
                    Settings.Default.PrinterName = value as string;
                    break;

                case PrintProperty.PrinterLanguage:
                    Settings.Default.PrinterLanguage = value as string;
                    break;

                case PrintProperty.PrintOrientation:
                    Settings.Default.PrintOrientation = value as string;
                    break;

                case PrintProperty.DPI:
                    Settings.Default.DPI = (int)value;
                    break;

                case PrintProperty.Copies:
                    Settings.Default.Copies = (int)value;
                    break;
            }

            Settings.Default.Save();
            this.PrintManager.SetPrinterSettings();
        }

        /// <summary>
        /// Get the serial data enumeration values.
        /// </summary>
        private void GetData()
        {
            this.ComPorts = SerialPort.GetPortNames();
            this.Paritys = Enum.GetNames(typeof(Parity));
            this.Handshakes = Enum.GetNames(typeof(Handshake));
            this.StopbitsValues = Enum.GetNames(typeof(StopBits));
            this.DataBitsValues = new [] { "7", "8"};
            this.BaudRates = new[] {"2400", "9600", "19200", "38400", "57600", "115400"};
        }

        /// <summary>
        /// Sets the default communication settings.
        /// </summary>
        private void SetProperties()
        {
            // print mode
            if (!string.IsNullOrEmpty(Settings.Default.PrintMode))
            {
                var printMode = (PrintMode) Enum.Parse(typeof (PrintMode), Settings.Default.PrintMode);

                switch (printMode)
                {
                    case PrintMode.USB:
                        this.USB = true;
                        break;

                    case PrintMode.Serial:
                        this.Serial = true;
                        break;

                    case PrintMode.Network:
                        this.Ethernet = true;
                        break;

                    case PrintMode.Parallel:
                        this.Parallel = true;
                        break;

                    case PrintMode.Image:
                        this.Image = true;
                        break;
                }
            }

            // general
            this.PrinterLanguage = !string.IsNullOrEmpty(Settings.Default.PrinterLanguage) ? Settings.Default.PrinterLanguage : Constant.DefaultPrinterLanguage;
            this.PrintOrientation = !string.IsNullOrEmpty(Settings.Default.PrintOrientation) ? Settings.Default.PrintOrientation : Constant.DefaultPrintOriention;
            this.DPI = Settings.Default.DPI != 0 ? Settings.Default.DPI : Constant.DefaultDPI.ToInt();
            this.Copies = Settings.Default.Copies != 0 ? Settings.Default.Copies : Constant.DefaultCopies.ToInt();

            // serial
            this.ComPort = Settings.Default.ComPort;
            this.BaudRate = !string.IsNullOrEmpty(Settings.Default.BaudRate) ? Settings.Default.BaudRate : Constant.DefaultBaudRate;
            this.Parity = !string.IsNullOrEmpty(Settings.Default.Parity) ? Settings.Default.Parity : Constant.None;
            this.DataBits = !string.IsNullOrEmpty(Settings.Default.DataBits) ? Settings.Default.DataBits : Constant.DefaultDataBits;
            this.StopBits = !string.IsNullOrEmpty(Settings.Default.StopBits) ? Settings.Default.StopBits : Constant.DefaultStopBits;
            this.Handshake = !string.IsNullOrEmpty(Settings.Default.Handshake) ? Settings.Default.Handshake : Constant.None;

            // ethernet
            this.IpAddress = !string.IsNullOrEmpty(Settings.Default.IPHost) ? Settings.Default.IPHost : Constant.DefaultIp;
            this.Port = Settings.Default.Port;

            // parallel
            this.parallelPortName = !string.IsNullOrEmpty(Settings.Default.ParallelPortName) ? Settings.Default.ParallelPortName : Constant.DefaultParallel;

            // usb
            this.PrinterName = Settings.Default.PrinterName;

            this.PrintManager.SetPrinterSettings();
        }

        /// <summary>
        /// Retrieves the image types.
        /// </summary>
        private void GetImageTypes()
        {
            this.ImageTypes = Enum.GetNames(typeof (ImageFormat));
        }

        /// <summary>
        /// Retrieves the host printers.
        /// </summary>
        private void GetPrinters()
        {
            var printServer = new LocalPrintServer();
            var printQueuesOnLocalServer = printServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local });

            if (printQueuesOnLocalServer.Any())
            {
                this.PrinterNames = new List<string>();
                printQueuesOnLocalServer.ToList().ForEach(printer => this.PrinterNames.Add(printer.Name));

                if (string.IsNullOrEmpty(Settings.Default.PrinterName))
                {
                    this.PrinterName = this.PrinterNames.First();
                }
            }
        }

        /// <summary>
        /// Sets the parallel ports.
        /// </summary>
        private void GetParallelPorts()
        {
            this.ParallelPorts = new[] {"LPT1", "LPT2", "LPT3"};
        }

        /// <summary>
        /// Sets the printer languages.
        /// </summary>
        private void GetPrinterLanguages()
        {
            this.PrinterLanguages = Enum.GetNames(typeof(ProgrammingLanguage));
        }

        /// <summary>
        /// Sets the print orientations.
        /// </summary>
        private void GetPrintOrientations()
        {
            this.PrintOrientations = Enum.GetNames(typeof(PrintOrientation));
        }

        #endregion

        #endregion
    }
}

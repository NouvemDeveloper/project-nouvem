﻿// -----------------------------------------------------------------------
// <copyright file="UtilityViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Drawing.Text;
using DevExpress.Data.Helpers;
using DevExpress.XtraPrinting.Shape;
using GalaSoft.MvvmLight.Messaging;
using Neodynamic.SDK.Printing;
using Nouvem.LabelDesigner.Global;

namespace Nouvem.LabelDesigner.ViewModel.LabelItem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UtilityViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The utility item stroke thickness value.
        /// </summary>
        private double strokeThickness;

        /// <summary>
        /// The current shape item.
        /// </summary>
        private ShapeItem currentItem;

        /// <summary> The line direction.
        /// </summary>
        private string selectedLineDirection;

        /// <summary>
        /// Corner radius value for the rectangle item.
        /// </summary>
        private double cornerRadius;

        /// <summary>
        /// The corner radius visibility flag.
        /// </summary>
        private bool cornerRadiusVisibility;

        /// <summary>
        /// The thickness visibility flag.
        /// </summary>
        private bool thicknessVisibility;

        /// <summary>
        /// The direction visibility flag.
        /// </summary>
        private bool directionVisibility;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UtilityViewModel"/> class.
        /// </summary>
        public UtilityViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // register for a selected label item change.
            Messenger.Default.Register<Item>(this, Token.SelectedItemChange, item =>
            {
                this.DirectionVisibility = false;
                this.CornerRadiusVisibility = false;
                this.ThicknessVisibility = false;

                if (!(item is BarcodeItem) && !(item is TextItem))
                {
                    if (item is RectangleShapeItem)
                    {
                        this.StrokeThickness = (item as RectangleShapeItem).StrokeThickness;
                        this.ThicknessVisibility = true;
                        this.CornerRadiusVisibility = true;
                    }
                    else if (item is LineShapeItem)
                    {
                        this.StrokeThickness = (item as LineShapeItem).StrokeThickness;
                        this.ThicknessVisibility = true;
                        this.DirectionVisibility = true;
                    }
                    else if (item is EllipseShapeItem)
                    {
                        this.StrokeThickness = (item as EllipseShapeItem).StrokeThickness;
                        this.ThicknessVisibility = true;
                    }
                    else if (item is ShapeItem)
                    {
                        this.StrokeThickness = (item as ShapeItem).StrokeThickness;
                        this.ThicknessVisibility = true;
                    }

                    this.currentItem = item as ShapeItem;
                }
            });

            #endregion

            #region command handler

          


            #endregion

            this.GetLineDirections();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the corner radius controls are visible.
        /// </summary>
        public bool CornerRadiusVisibility
        {
            get
            {
                return this.cornerRadiusVisibility;
            }

            set
            {
                this.cornerRadiusVisibility = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the direction controls are visible.
        /// </summary>
        public bool DirectionVisibility
        {
            get
            {
                return this.directionVisibility;
            }

            set
            {
                this.directionVisibility = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the thickness controls are visible.
        /// </summary>
        public bool ThicknessVisibility
        {
            get
            {
                return this.thicknessVisibility;
            }

            set
            {
                this.thicknessVisibility = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the rectangle item corner radius.
        /// </summary>
        public double CornerRadius
        {
            get
            {
                return this.cornerRadius;
            }

            set
            {
                if (this.currentItem is RectangleShapeItem)
                {
                    (this.currentItem as RectangleShapeItem).CornerRadius = new RectangleCornerRadius(value);

                    this.UpdateItem();

                    this.cornerRadius = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the line orientations.
        /// </summary>
        public string[] LineOrientations { get; set; }

        /// <summary>
        /// Gets or sets the line direction.
        /// </summary>
        public string SelectedLineDirection
        {
            get
            {
                return this.selectedLineDirection;
            }

            set
            {
                if (this.currentItem is LineShapeItem)
                {
                    //this.currentItem = this.currentItem as LineShapeItem;
                    (this.currentItem as LineShapeItem).Orientation = (LineOrientation)Enum.Parse(typeof(LineOrientation), value);
                    
                    this.UpdateItem();

                    this.selectedLineDirection = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the stroke thickness.
        /// </summary>
        public double StrokeThickness
        {
            get
            {
                return this.strokeThickness;
            }

            set
            {
                if (value < 0)
                {
                    return;
                }
                
                this.strokeThickness = value;
                this.RaisePropertyChanged();

                if (this.currentItem != null)
                {
                    this.currentItem.StrokeThickness = value;
                    this.UpdateItem();
                }
            }
        }

        #endregion

        #region command



        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution



        #endregion

        #region helper

        private void ParseItem(RectangleShapeItem item)
        {
            this.StrokeThickness = item.StrokeThickness;
        }

        /// <summary>
        /// Notifies the editor of a change to the barcode item.
        /// </summary>
        private void UpdateItem()
        {
            if (this.currentItem == null)
            {
                return;
            }
           
            Messenger.Default.Send<Item>(this.currentItem, Token.SelectedItemUpdate);
        }

        /// <summary>
        /// Gets the application line orientations.
        /// </summary>
        private void GetLineDirections()
        {
            this.LineOrientations = Enum.GetNames(typeof (LineOrientation));

            if (this.LineOrientations.Any())
            {
                this.SelectedLineDirection = this.LineOrientations[0];
            }
        }

        #endregion

        #endregion
    }
}
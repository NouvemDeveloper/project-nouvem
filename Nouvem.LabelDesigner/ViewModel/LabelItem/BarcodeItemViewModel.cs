﻿// -----------------------------------------------------------------------
// <copyright file="BarcodeItemViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Neodynamic.Windows.ThermalLabelEditor;

namespace Nouvem.LabelDesigner.ViewModel.LabelItem
{
    using System;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Neodynamic.SDK.Printing;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Properties;

    public class BarcodeItemViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The selected symbology.
        /// </summary>
        private string selectedBarcodeSymbology;
        
        /// <summary>
        /// The selected code allignment.
        /// </summary>
        private string selectedCodeAllignment;

        /// <summary>
        /// The barcode item reference.
        /// </summary>
        private BarcodeItem barcodeItem;

        /// <summary>
        /// The bar item bars width.
        /// </summary>
        private double barWidth;

        /// <summary>
        /// The bar item bars height.
        /// </summary>
        private double barHeight;

        /// <summary>
        /// The barcodes used in the application.
        /// </summary>
        private IList<string> applicationBarcodes = new List<string>(); 
      
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the BarcodeItemViewModel class.
        /// </summary>
        public BarcodeItemViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Item>(this, Token.SelectedItemChange, item =>
            {
                if (item is BarcodeItem)
                {
                    this.BarcodeItem = item as BarcodeItem;
                }
            });

            Messenger.Default.Register<string>(this, Token.UpdateBarcodeCode, x =>
            {
                if (this.BarcodeItem != null)
                {
                    this.BarcodeItem.Code = x;
                    this.BarcodeItem.AddChecksum = true;
                    this.UpdateItem();
                }
            });

            Messenger.Default.Register<string>(this, Token.UpdateBarcodeDataField, x =>
            {
                var field = x;

                if (this.BarcodeItem != null)
                {
                    this.BarcodeItem.DataField = field;
                    this.UpdateItem();
                }
            });

            #endregion

            #region command

            // Set the barcode to a GS1128 default.
            this.OnLoadingCommand = new RelayCommand(() =>
            {
                if (this.SelectedBarcodeSymbology == null)
                {
                    this.SelectedBarcodeSymbology = ApplicationBarcode.Code128;
                }
            });

            // Clean up.
            //this.OnClosingCommand = new RelayCommand(ViewModelLocator.ClearBarcodeItem);

            #endregion

            this.GetApplicationBarcodes();
            this.GetBarcodeSymbologies();
            this.GetCodeAllignments();
        }

        #endregion

        #region public interface

        #region property

        public BarcodeItem BarcodeItem
        {
            get
            {
                return this.barcodeItem;
            }

            set
            {
                this.barcodeItem = value;
                this.ParseBarcodeItem();
            }
        }

        /// <summary>
        /// Gets or sets the barcode symbologies.
        /// </summary>
        public string[] BarcodeSymbologies { get; set; }

        /// <summary>
        /// Gets or sets the barcode code allignments.
        /// </summary>
        public string[] CodeAllignments { get; set; }

        /// <summary>
        /// Gets or sets the selected barcode symbology.
        /// </summary>
        public string SelectedBarcodeSymbology
        {
            get
            {
                return this.selectedBarcodeSymbology;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.selectedBarcodeSymbology = value;
                    this.RaisePropertyChanged();

                    if (this.barcodeItem != null)
                    {
                        this.barcodeItem.Symbology = (BarcodeSymbology)Enum.Parse(typeof(BarcodeSymbology), value);
                        this.barcodeItem.Code = BarcodeItemUtils.GenerateSampleCode(this.barcodeItem.Symbology);
                        this.UpdateItem();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected code allignment.
        /// </summary>
        public string SelectedCodeAllignment
        {
            get
            {
                return this.selectedCodeAllignment;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.selectedCodeAllignment = value;
                    this.RaisePropertyChanged();

                    if (this.barcodeItem != null)
                    {
                        this.barcodeItem.CodeAlignment = (BarcodeTextAlignment)Enum.Parse(typeof(BarcodeTextAlignment), value);
                        this.UpdateItem();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected barcode bar height.
        /// </summary>
        public double BarHeight
        {
            get
            {
                return this.barHeight;
            }

            set
            {
                this.barHeight = value;
                this.RaisePropertyChanged();

                if (barcodeItem != null)
                {
                    this.barcodeItem.BarHeight = value;
                    this.UpdateItem();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected barcode bar width.
        /// </summary>
        public double BarWidth
        {
            get
            {
                return this.barWidth;
            }

            set
            {
                this.barWidth = value;
                this.RaisePropertyChanged();

                if (barcodeItem != null)
                {
                    this.barcodeItem.BarWidth = value;
                    this.UpdateItem();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the ui load event command.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the ui unload event command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Determines if the current barcode is GS1 standard.
        /// </summary>
        /// <returns>A flag, as to whether the current barcode is GS1 standard.</returns>
        public bool IsGs1Barcode()
        {
            return this.selectedBarcodeSymbology.StartsWith(Constant.GS1) 
                || this.selectedBarcodeSymbology.StartsWith(Constant.Code128);
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Gets the application barcodes.
        /// </summary>
        /// <remarks>This list will expand upon user request.</remarks>
        private void GetApplicationBarcodes()
        {
            this.applicationBarcodes.Add(ApplicationBarcode.GS1128);
            this.applicationBarcodes.Add(ApplicationBarcode.Code128);
            this.applicationBarcodes.Add(ApplicationBarcode.Ean13);
        }

        /// <summary>
        /// Gets the barcode symbologies.
        /// </summary>
        private void GetBarcodeSymbologies()
        {
            this.BarcodeSymbologies = Enum.GetNames(typeof(BarcodeSymbology)).Where(x => this.applicationBarcodes.Contains(x)).ToArray();
            Array.Sort(this.BarcodeSymbologies);
        }

        /// <summary>
        /// Gets the code allignments.
        /// </summary>
        private void GetCodeAllignments()
        {
            this.CodeAllignments = Enum.GetNames(typeof(BarcodeTextAlignment));
        }

        /// <summary>
        /// Notifies the editor of a change to the barcode item.
        /// </summary>
        private void UpdateItem()
        {
            Messenger.Default.Send<Item>(this.barcodeItem, Token.SelectedItemUpdate);
        }

        /// <summary>
        /// Parses the incoming barcode item.
        /// </summary>
        private void ParseBarcodeItem()
        {
            this.BarHeight = this.BarcodeItem.BarHeight;
            this.BarWidth = this.BarcodeItem.BarWidth;
            this.SelectedCodeAllignment = this.BarcodeItem.CodeAlignment.ToString();
            this.SelectedBarcodeSymbology = this.BarcodeSymbologies.FirstOrDefault(x => x == this.BarcodeItem.Symbology.ToString());
            //this.Locator.BarcodeBuilder.ParseData(this.barcodeItem.Tag, this.barcodeItem.Comments);
        }
     
        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }
    }
}

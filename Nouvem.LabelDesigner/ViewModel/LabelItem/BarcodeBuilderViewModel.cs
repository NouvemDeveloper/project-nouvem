﻿// -----------------------------------------------------------------------
// <copyright file="BarcodeBuilderViewModel .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Grid;
using Neodynamic.SDK.Printing;
using Nouvem.LabelDesigner.Properties;
using Nouvem.Shared;

namespace Nouvem.LabelDesigner.ViewModel.LabelItem
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class BarcodeBuilderViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The barcode data (fields and application identifiers)
        /// </summary>
        private readonly IDictionary<Gs1Data, LabelData> barcode;

        /// <summary>
        /// The data string holding the barcode format (eg (01)CodeField(02)NameField )
        /// </summary>
        private readonly StringBuilder barcodeDataString = new StringBuilder();

        /// <summary>
        /// The data source items.
        /// </summary>
        private ObservableCollection<LabelData> dataSourceFields;

        /// <summary>
        /// All the data source items.
        /// </summary>
        private IList<LabelData> allLabelData;

        /// <summary>
        /// The selected label data.
        /// </summary>
        private LabelData selectedLabelData;

        /// <summary>
        /// The selected gs1.
        /// </summary>
        private Gs1Data selectedGs1;

        /// <summary>
        /// The data source items. </summary>
        private ObservableCollection<Gs1Data> gs1s;

        /// <summary>
        /// The barcode string being built.
        /// </summary>
        private StringBuilder barcodeString = new StringBuilder();

        /// <summary>
        /// The bindable barcode string.
        /// </summary>
        private string barcodeData;

        /// <summary>
        /// The current label field selected.
        /// </summary>
        private string currentLabelField;

        /// <summary>
        /// The current label field flag.
        /// </summary>
        private bool currentLabelFieldValue;

        /// <summary>
        /// Flag, as to whether the current selected barcode is of GS1 type (Gs1/Ean)
        /// </summary>
        private bool isGs1Barcode;

        /// <summary>
        /// The current gs1 vm.
        /// </summary>
        private LabelDesignerViewModelBase currentGs1ViewModel;

        /// <summary>
        /// Flag, as to whether a label data source item is being deselected.
        /// </summary>
        private bool removeLabelItem;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BarcodeBuilderViewModel"/> class.
        /// </summary>
        public BarcodeBuilderViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Item>(this, Token.SelectedItemChange, item =>
            {
                if (item is BarcodeItem)
                {
                    if (LabelDesignerGlobal.SelectedLabelData == null)
                    {
                        return;
                    }

                    if (this.DataSourceFields == null)
                    {
                        this.DataSourceFields = new ObservableCollection<LabelData>(LabelDesignerGlobal.SelectedLabelData.Where(x => x.Field.ToLower().StartsWith(Constant.Barcode.ToLower())));
                        this.DataSourceFields.Add(new LabelData { Field = "#Barcode1" });
                        this.DataSourceFields.Add(new LabelData { Field = "#Barcode2" });
                    }

                    var barcodeItem = item as BarcodeItem;
                    if (!string.IsNullOrEmpty(barcodeItem.DataField))
                    {
                        this.SelectedLabelData =
                          this.DataSourceFields.FirstOrDefault(x => x.Field.Equals(barcodeItem.DataField));
                        if (this.SelectedLabelData != null)
                        {
                            foreach (var data in this.DataSourceFields)
                            {
                                data.IsSelected = false;
                            }

                            this.SelectedLabelData.IsSelected = true;
                        }
                    }
                }
            });

            // register for gsi selection
            Messenger.Default.Register<bool>(this, Token.Gs1Selected, this.Gs1SelectionHandler);

            #endregion

            #region command

            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                #region validation

                if (e == null || e.Row == null)
                {
                    return;
                }

                #endregion

                if (e.Row is LabelData)
                {
                    var isSelected = (bool)e.Value;
                    //Messenger.Default.Send(isSelected, Token.StringBuilder);
                    this.AssignDataVariable(isSelected);
                }
            });

            this.UpdateGs1Command = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                #region validation

                if (e == null || e.Row == null)
                {
                    return;
                }

                #endregion

                if (e.Row is Gs1Data)
                {
                    var isSelected = (bool)e.Value;
                    //Messenger.Default.Send(isSelected, Token.StringBuilder);
                    this.Gs1SelectionHandler(isSelected);
                }
            });

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                // register for data change
                //Messenger.Default.Register<bool>(this, Token.StringBuilder, this.AssignDataVariable);

                //if (LabelDesignerGlobal.SelectedLabelData != null)
                //{
                //    this.DataSourceFields = new ObservableCollection<LabelData>(LabelDesignerGlobal.SelectedLabelData.Where(x => x.Field.ToLower().StartsWith(Constant.Barcode.ToLower())));
                //    this.DataSourceFields.Add(new LabelData { Field = "#Barcode1"});
                //    this.DataSourceFields.Add(new LabelData { Field = "#Barcode2" });
                //}

                // determine if a gs1 standard barcode has been selected.
                this.IsGs1Barcode = this.Locator.BarcodeItem.IsGs1Barcode();
            });

            this.OnUnloadedCommand = new RelayCommand(() => Messenger.Default.Unregister<bool>(this, Token.StringBuilder, this.AssignDataVariable));

            #endregion

            this.barcode = new Dictionary<Gs1Data, LabelData>();
            this.GetLabelData();
            this.GetGs1s();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public LabelDesignerViewModelBase CurrentGs1ViewModel
        {
            get
            {
                return this.currentGs1ViewModel;
            }

            set
            {
                this.currentGs1ViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the barcode string.
        /// </summary>
        public string BarcodeData
        {
            get
            {
                return this.barcodeData;
            }

            set
            {
                this.barcodeData = value;
                this.RaisePropertyChanged();

                if (!this.labelItemLoading)
                {
                    Messenger.Default.Send(value, Token.UpdateBarcodeCode);
                }
            }
        }

        /// <summary>
        /// Gets or sets the data sources names.
        /// </summary>
        public IList<string> DataSources { get; set; }

        /// <summary>
        /// Gets or sets the label data.
        /// </summary>
        public ObservableCollection<LabelData> DataSourceFields
        {
            get
            {
                return this.dataSourceFields;
            }

            set
            {
                this.dataSourceFields = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ObservableCollection<Gs1Data> GS1s
        {
            get
            {
                return this.gs1s;
            }

            set
            {
                this.gs1s = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected label data.
        /// </summary>
        public LabelData SelectedLabelData
        {
            get
            {
                return this.selectedLabelData;
            }

            set
            {
                this.selectedLabelData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected gs1 item.
        /// </summary>
        public Gs1Data SelectedGs1
        {
            get
            {
                return this.selectedGs1;
            }

            set
            {
                this.selectedGs1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the current barcode type is GS1.
        /// </summary>
        public bool IsGs1Barcode
        {
            get
            {
                return this.isGs1Barcode;
            }

            set
            {
                this.isGs1Barcode = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.CurrentGs1ViewModel = this.Locator.BarcodeBuilder;
                }
                else
                {
                    this.CurrentGs1ViewModel = this.Locator.Gs1Compressed;
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the grid changes.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the gs1 grid changes.
        /// </summary>
        public ICommand UpdateGs1Command { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Parses the newly selected/opened barcode item, setting the grid selections to match it's stored code data.
        /// </summary>
        /// <param name="data">The stored data format (AI's, and variable fields).</param>
        /// <param name="sampleData">The stored sample text data.</param>
        public void ParseData(string data, string sampleData)
        {
            #region validation

            if (this.DataSourceFields == null || this.GS1s == null)
            {
                return;
            }

            #endregion
           
            try
            {
                this.labelItemLoading = true;

                // clear the grids, and fields.
                this.GS1s.ToList().ForEach(x => x.IsSelected = false);
                this.DataSourceFields.ToList().ForEach(x => x.IsSelected = false);
                this.BarcodeData = sampleData;
                this.barcodeString = new StringBuilder();
                this.currentLabelField = string.Empty;
                this.currentLabelFieldValue = false;
                this.removeLabelItem = false;

                if (data == string.Empty)
                {
                    // new barcode, so nothing more to do.
                    return;
                }

                var localDataArray = data.Split('|');
                foreach (var localData in localDataArray)
                {
                    if (localData.StartsWith("("))
                    {
                        // application identifier
                        foreach (var localGs1 in this.GS1s)
                        {
                            // check the AI, if it matches the stored A1
                            localGs1.IsSelected = string.Format("({0})", localGs1.AI).Equals(localData);
                        }

                        continue;
                    }

                    // it's a data source variable item
                    foreach (var labelData in this.DataSourceFields)
                    {
                        // check the data source field, if it matches the store barcode field.
                        labelData.IsSelected = labelData.Field.Equals(localData);
                    }
                }
            }
            finally
            {
                this.labelItemLoading = false;
            }
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Handler for a gs1 application identifier selection.
        /// </summary>
        /// <param name="b">The selected/deselected flag.</param>
        private void Gs1SelectionHandler(bool b)
        {
            #region validation

            //if (!removeLabelItem)
            //{
            //    // TODO The Gs1Data IsSelected field is being hit twice (same value). This takes care of that problem, but need to fix this issue properly.
            //    if (this.selectedLabelData != null && b == this.currentLabelFieldValue && this.currentLabelField.Equals(this.selectedLabelData.Field))
            //    {
            //        return;
            //    }
            //}

            if (this.labelItemLoading)
            {
                return;
            }

            #endregion

            var dataLength = this.selectedGs1.DataLength;
            var dummyData = this.selectedLabelData != null && this.selectedLabelData.SampleData != null ? this.selectedLabelData.SampleData : string.Empty;
            dummyData = dummyData.PadRight(dataLength, 'x');

            var gsiData = string.Format("({0}){1}",this.selectedGs1.AI, dummyData);
            var localBarcode = string.Empty;
            if (this.selectedLabelData != null)
            {
                localBarcode = string.Format("({0})|{1}|", this.selectedGs1.AI, this.selectedLabelData.Field);
            }

            if (b)
            {
                if (this.selectedLabelData == null || !this.selectedLabelData.IsSelected)
                {
                    this.SelectedGs1.IsSelected = false;
                    SystemMessage.Write(MessageType.Issue, Message.NoDataFieldSelected);
                    return;
                }
            
                this.barcodeDataString.Append(localBarcode);
                this.barcodeString.Append(gsiData);
                
                if (this.barcode.ContainsKey(this.selectedGs1))
                {
                    this.barcode.Remove(this.selectedGs1);
                }

                this.barcode.Add(this.selectedGs1, this.selectedLabelData);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.BarcodeVariableAssignedGs1, this.selectedGs1.AI, this.selectedLabelData.Field));
            }
            else
            {
                if (!string.IsNullOrEmpty(gsiData))
                {
                    this.barcodeString.Replace(gsiData, string.Empty);
                }

                if (!string.IsNullOrEmpty(localBarcode))
                {
                    this.barcodeDataString.Replace(localBarcode, string.Empty);
                }
               
                if (this.barcode.ContainsKey(this.selectedGs1))
                {
                    LabelData localLabelData;
                    var value = this.barcode.TryGetValue(this.selectedGs1, out localLabelData);

                    if (localLabelData != null)
                    {
                        this.labelItemLoading = true;
                        this.SelectedLabelData = localLabelData;
                        localLabelData.IsSelected = false;
                        this.labelItemLoading = false;
                    }

                    this.barcode.Remove(this.selectedGs1);
                }

                if (this.selectedLabelData != null)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.BarcodeVariableRemovedGs1, this.selectedGs1.AI, this.selectedLabelData.Field));
                }
            }

            if (this.selectedLabelData != null)
            {
                this.currentLabelField = this.selectedLabelData.Field;
            }

            this.currentLabelFieldValue = b;
        
            this.BarcodeData = this.barcodeString.ToString();
            this.removeLabelItem = false;

            Messenger.Default.Send(Tuple.Create(this.barcodeDataString.ToString(), this.BarcodeData), Token.UpdateBarcodeDataField);
        }

        ///// <summary>
        ///// Handler for the data source variable field selection.(Only applicable to non-gs1 barcodes)
        ///// </summary>
        ///// <param name="b">The selection/deselection flag.</param>
        //private void AssignDataVariable(bool b)
        //{
        //    if (this.IsGs1Barcode)
        //    {
        //        this.AssignDataVariableGs1(b);
        //    }
        //    else
        //    {
        //        this.AssignDataVariableNonGs1(b);
        //    }
        //}

        ///// <summary>
        ///// Handler for the data source variable field selection.(Only applicable to gs1 barcodes)
        ///// </summary>
        ///// <param name="b">The selection/deselection flag.</param>
        //private void AssignDataVariableGs1(bool b)
        //{
        //    #region validation

        //    if (this.labelItemLoading)
        //    {
        //        return;
        //    }

        //    #endregion

        //    if (b)
        //    {
                
        //    }
        //    else
        //    {
        //        var localValue = this.barcode.FirstOrDefault(x => x.Value.Equals(this.selectedLabelData));

        //        if (localValue.Key != null)
        //        {
        //            this.selectedGs1 = localValue.Key;
        //            localValue.Key.IsSelected = false;
        //        }
        //    }
        //}

        /// <summary>
        /// Handler for the data source variable field selection.(Only applicable to non-gs1 barcodes)
        /// </summary>
        /// <param name="b">The selection/deselection flag.</param>
        private void AssignDataVariable(bool b)
        {
            #region validation

            // gs1 barcode logic is handled in the Gs1SelectionHandler()
            if (this.selectedLabelData == null || this.labelItemLoading)
            {
                return;
            }

            #endregion

            //var localBarcode = string.Format("{0}|", this.selectedLabelData.Field);
            var barcodeField = string.Empty;

            if (b)
            {
                //this.barcodeDataString.Append(localBarcode);
                barcodeField = this.selectedLabelData.Field;
                SystemMessage.Write(MessageType.Priority, string.Format(Message.BarcodeVariableAssigned, this.selectedLabelData.Field));
            }
            else
            {
                //this.barcodeDataString.Replace(localBarcode, string.Empty);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.BarcodeVariableRemoved, this.selectedLabelData.Field));
                
            }
            
            Messenger.Default.Send(barcodeField, Token.UpdateBarcodeDataField);
        }

        /// <summary>
        /// Gets all the data source data.
        /// </summary>
        private void GetLabelData()
        {
            this.allLabelData = new List<LabelData>();
            this.DataSources = new List<string>();

            this.DataManager.GetLabelDataFields().Where(x => x.Field.StartsWith(Constant.Barcode)).ToList().ForEach(x =>
            {
                this.allLabelData.Add(x);
                if (!this.DataSources.Contains(x.Identifier))
                {
                    this.DataSources.Add(x.Identifier);
                }
            });
        }

        /// <summary>
        /// Gets all the gsi identifiers.
        /// </summary>
        private void GetGs1s()
        {
            this.GS1s = new ObservableCollection<Gs1Data>(this.DataManager.GetGs1Master());
        }

        #endregion
    }
}

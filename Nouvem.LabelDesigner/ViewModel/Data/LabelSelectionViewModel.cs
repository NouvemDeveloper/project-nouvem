﻿// -----------------------------------------------------------------------
// <copyright file="LabelSelectionViewModel.cs" company="Nouvem Limited"
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Nouvem.LabelDesigner.ViewModel.Data
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.DataLayer;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class LabelSelectionViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The selected label reference.
        /// </summary>
        private Label selectedLabel;

        /// <summary>
        /// The show deleted label flag.
        /// </summary>
        private bool showDeletedLabels;

        /// <summary>
        /// The labels reference.
        /// </summary>
        private ObservableCollection<Label> labels;

        /// <summary>
        /// The labels reference.
        /// </summary>
        private IList<Label> allLabels = new List<Label>();

        /// <summary>
        /// The ui loaded flag.
        /// </summary>
        private bool isFormLoaded;

        #endregion

         /// <summary>
        /// Initializes a new instance of the LabelSelectionViewModel class.
        /// </summary>
        public LabelSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Move back without selection.
            this.MoveBackCommand = new RelayCommand(() =>  this.SwitchView(this.Locator.LabelSetUp));

            // Handle the  ui load.
            this.OnLoadedCommand = new GalaSoft.MvvmLight.Command.RelayCommand(() => this.isFormLoaded = true);

            // Handle the ui unload.
            this.OnUnloadedCommand = new GalaSoft.MvvmLight.Command.RelayCommand(() => this.isFormLoaded = false);

            #endregion

            #region instantiation

            this.Labels = new ObservableCollection<Label>();

            #endregion

            this.GetLabels();
        }

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whethe we are showing the deleted labels.
        /// </summary>
        public bool ShowDeletedLabels
        {
            get
            {
                return this.showDeletedLabels;
            }

            set
            {
                this.showDeletedLabels = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.Labels = new ObservableCollection<Label>(this.allLabels.Where(x => x.Deleted));
                }
                else
                {
                    this.Labels = new ObservableCollection<Label>(this.allLabels.Where(x => !x.Deleted));
                }
            }
        }

        /// <summary>
        /// Gets or sets the application labels.
        /// </summary>
        public ObservableCollection<Label> Labels
        {
            get
            {
                return this.labels;
            }

            set
            {
                this.labels = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected label.
        /// </summary>
        public Label SelectedLabel
        {
            get
            {
                return this.selectedLabel;
            }

            set
            {
                this.selectedLabel = value;
                this.RaisePropertyChanged();

                if (value != null && this.isFormLoaded)
                {
                    this.SwitchView(this.Locator.LabelSetUp);

                    if (!string.IsNullOrEmpty(this.selectedLabel.DataSource))
                    {
                        this.SetDataSource(this.selectedLabel.DataSource);
                    }

                    // load the label file
                    this.Locator.LabelSetUp.OpenLabel(this.selectedLabel);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a non selection.
        /// </summary>
        public ICommand MoveBackCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the ui load.
        /// </summary>
        public ICommand OnLoadedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the ui unload.
        /// </summary>
        public ICommand OnUnloadedCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Refreshes the labels.
        /// </summary>
        public void Refresh()
        {
            this.Labels.Clear();
            this.GetLabels();
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Set the application data source to that of the newly opened label.
        /// </summary>
        /// <param name="source">The label data source.</param>
        private void SetDataSource(string source)
        {
            this.Locator.DataSource.SelectedLabelDataSource = source;
        }

        /// <summary>
        /// Gets the application labels.
        /// </summary>
        private void GetLabels()
        {
            this.allLabels = this.DataManager.GetLabels();
            if (!this.allLabels.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.NoLabels);
                this.SwitchView(this.Locator.LabelSetUp);
                return;
            }

            foreach (var label in this.allLabels.Where(x => !x.Deleted))
            {
                this.Labels.Add(label);
            }
        }

        #endregion
    }
}

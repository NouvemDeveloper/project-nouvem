﻿// -----------------------------------------------------------------------
// <copyright file="LabelSelectionViewModel.cs" company="Nouvem Limited"
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Reflection;
using GalaSoft.MvvmLight.CommandWpf;
using Nouvem.LabelDesigner.Properties;

namespace Nouvem.LabelDesigner.ViewModel.Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.DataLayer;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class DataSourceViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The selected label data source.
        /// </summary>
        private string selectedLabelDataSource;

        /// <summary>
        /// The data source items.
        /// </summary>
        private ObservableCollection<LabelData> dataSourceFields;

        /// <summary>
        /// The data source items to update.
        /// </summary>
        private IList<LabelData> dataSourceFieldsToUpdate = new List<LabelData>();

        /// <summary>
        /// All the data source items.
        /// </summary>
        private IList<LabelData> allLabelData;

        /// <summary>
        /// The add/update button content.
        /// </summary>
        private string controlButtonText;

        #endregion

        /// <summary>
        /// Initializes a new instance of the DataSourceViewModel class.
        /// </summary>
        public DataSourceViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.OnLoadedCommand = new RelayCommand(() => this.ControlButtonText = Strings.OK);
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.ControlButtonText = Strings.Update;
                if (this.SelectedDataSourceField != null)
                {
                    this.dataSourceFieldsToUpdate.Add(this.SelectedDataSourceField);
                }
            });

            this.ControlSelectionCommand = new RelayCommand(this.ControlSelectionCommandExecute);

            this.GetLabelData();
        }

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected data source field.
        /// </summary>
        public LabelData SelectedDataSourceField { get; set; }

        /// <summary>
        /// Gets or sets the add/update content.
        /// </summary>
        public string ControlButtonText
        {
            get
            {
                return this.controlButtonText;
            }

            set
            {
                this.controlButtonText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the data sources names.
        /// </summary>
        public IList<string> DataSources { get; set; }

        /// <summary>
        /// Gets or sets the label data.
        /// </summary>
        public ObservableCollection<LabelData> DataSourceFields
        {
            get
            {
                return this.dataSourceFields;
            }

            set
            {
                this.dataSourceFields = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected label data source.
        /// </summary>
        public string SelectedLabelDataSource
        {
            get
            {
                return this.selectedLabelDataSource;
            }

            set
            {
                this.selectedLabelDataSource = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrEmpty(value))
                {
                    this.DataSourceFields = new ObservableCollection<LabelData>(this.allLabelData.Where(x => x.Identifier.Equals(value)));
                    LabelDesignerGlobal.SelectedLabelData = this.DataSourceFields;
                    LabelDesignerGlobal.DataSource = value;
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelDataSourceSelected, value));
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle ui load.
        /// </summary>
        public ICommand OnLoadedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle ui load.
        /// </summary>
        public ICommand ControlSelectionCommand { get; set; }

        /// <summary>
        /// Gets the command to handle ui update.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a label selection.
        /// </summary>
        public ICommand LabelSelectedCommand { get; set; }

        #endregion

        #endregion

        #region protected override

        protected override void ControlSelectionCommandExecute()
        {
            if (this.ControlButtonText.Equals(Strings.Update))
            {
                var dt = this.DataManager.GetTestLabelData();

                foreach (var field in this.dataSourceFieldsToUpdate)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (DataColumn column in dt.Columns)
                        {
                            var localColumn = column.ColumnName;
                           
                            if (localColumn.Equals(field.Field))
                            {
                                row[column] = field.SampleData;
                            }
                        }
                    }
                }

                try
                {
                    this.DataManager.UpdateTestLabelData(dt);
                    SystemMessage.Write(MessageType.Priority, Message.LabelDataUpdated);
                    this.ControlButtonText = Strings.OK;
                    this.GetLabelData();
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }
            }

        //    if (this.ControlButtonText.Equals(Strings.Update))
        //    {
        //        var testData = new TestLabelData();
        //        var typeSource = testData.GetType();

               

        //        // Get all the properties of source object type
        //        var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        //        foreach (var field in this.dataSourceFieldsToUpdate)
        //        {
        //            // loop through the row fields
        //            foreach (var property in propertyInfo)
        //            {
        //                // determine if the current field is one of the traceability fields
        //                var match = field.Field.Equals(property.Name);

        //                if (match)
        //                {
        //                    // it is, so get it's value and add it to our corresponding traceability result.
        //                    property.SetValue(testData, field.SampleData);
        //                }
        //            }
        //        }

        //        //if (this.DataManager.UpdateTestLabelData(testData))
        //        //{

        //        //}
        //        //else
        //        //{
                    
        //        //}
        //    }
        }

        protected override void CancelSelectionCommandExecute()
        {
           
        }

        #endregion

        #region private

        /// <summary>
        /// Gets all the data source data.
        /// </summary>
        private void GetLabelData()
        {
            this.allLabelData = new List<LabelData>();
            this.DataSources = new List<string>();

            this.DataManager.GetLabelDataFields().ToList().ForEach(x =>
            {
                this.allLabelData.Add(x);
                if (!this.DataSources.Contains(x.Identifier))
                {
                    this.DataSources.Add(x.Identifier);
                }
            });

            this.SelectedLabelDataSource = this.DataSources.FirstOrDefault(x => x.Equals("LabelData"));
        }

        #endregion
    }
}

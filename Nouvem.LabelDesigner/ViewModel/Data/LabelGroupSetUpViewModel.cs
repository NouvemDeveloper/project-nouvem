﻿// -----------------------------------------------------------------------
// <copyright file="CountryMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel.Data
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.DataLayer;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class LabelGroupSetUpViewModel : LabelDesignerViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the LabelGroupSetUpViewModel class.
        /// </summary>
        public LabelGroupSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.LabelGroups = new ObservableCollection<LabelGroup>();

            #endregion

            this.GetLabelGroups();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the label groups.
        /// </summary>
        public ObservableCollection<LabelGroup> LabelGroups { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the label groups.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateLabelGroups();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the label groups.
        /// </summary>
        private void UpdateLabelGroups()
        {
            try
            {
                if (this.DataManager.AddOrUpdateLabelGroups(this.LabelGroups))
                {
                    SystemMessage.Write(MessageType.Priority, Message.LabelGroupUpdated);
                    this.Locator.LabelSetUp.RefreshGroups();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.LabelGroupNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.LabelGroupNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearLabelGroupSetUp();
            this.SwitchView(this.Locator.LabelSetUp);
        }

        /// <summary>
        /// Gets the application label groups.
        /// </summary>
        private void GetLabelGroups()
        {
            this.LabelGroups.Clear();
            foreach (var group in this.DataManager.GetLabelGroups())
            {
                this.LabelGroups.Add(group);
            }
        }
      
        #endregion
    }
}

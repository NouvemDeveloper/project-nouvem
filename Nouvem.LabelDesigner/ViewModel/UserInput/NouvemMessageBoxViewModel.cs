﻿// -----------------------------------------------------------------------
// <copyright file="MessageBoxViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel.UserInput
{
    using System;
    using System.Windows.Input;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Shared.Localisation;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class that handles the message box functionality.
    /// </summary>
    public class NouvemMessageBoxViewModel : ViewModelBase
    {
        #region fields

        /// <summary>
        /// The left button text.
        /// </summary>
        private string buttonLeft;

        /// <summary>
        /// The right button text.
        /// </summary>
        private string buttonRight;

        /// <summary>
        /// flag, as to whether the left button is visible or not.
        /// </summary>
        private bool buttonLeftVisibile;

        /// <summary>
        /// The message text to display.
        /// </summary>
        private string displayMessage;

        /// <summary>
        /// The message box buttons selection field
        /// </summary>
        private NouvemMessageBoxButtons buttonSelection;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NouvemMessageBoxViewModel"/> class.
        /// </summary>
        public NouvemMessageBoxViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the nouvem message box handler message, and buttons configuration.
            Messenger.Default.Register <Tuple<string, NouvemMessageBoxButtons>>(this, x =>
            {
                this.DisplayMessage = x.Item1;
                this.SetUpButtons(x.Item2);
                Messenger.Default.Send(Token.Message, Token.OpenMessageBox);
            });

            #endregion

            #region command registration

            this.ButtonLeftCommand = new RelayCommand(this.ButtonLeftCommandExecute);
            this.ButtonRightCommand = new RelayCommand(this.ButtonRightCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the left buttons text.
        /// </summary>
        public string ButtonLeft
        {
            get
            {
                return this.buttonLeft;
            }

            set
            {
                this.buttonLeft = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the right buttons text.
        /// </summary>
        public string ButtonRight
        {
            get
            {
                return this.buttonRight;
            }

            set
            {
                this.buttonRight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the message box message.
        /// </summary>
        public string DisplayMessage
        {
            get
            {
                return this.displayMessage;
            }

            set
            {
                this.displayMessage = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the left button is visible or not.
        /// </summary>
        public bool ButtonLeftVisible
        {
            get
            {
                return this.buttonLeftVisibile;
            }

            set
            {
                this.buttonLeftVisibile = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the left button command.
        /// </summary>
        public ICommand ButtonLeftCommand { get; private set; }

        /// <summary>
        /// Gets the right button command.
        /// </summary>
        public ICommand ButtonRightCommand { get; private set; }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the left button selection.
        /// </summary>
        private void ButtonLeftCommandExecute()
        {
            switch (this.buttonSelection)
            {
                case NouvemMessageBoxButtons.OKCancel:
                    NouvemMessageBox.UserSelection = UserDialogue.OK;
                    break;

                case NouvemMessageBoxButtons.YesNo:
                    NouvemMessageBox.UserSelection = UserDialogue.Yes;
                    break;

                case NouvemMessageBoxButtons.WorkflowStandard:
                    NouvemMessageBox.UserSelection = UserDialogue.Workflow;
                    break;

                default:
                    NouvemMessageBox.UserSelection = UserDialogue.OK;
                    break;
            }
        }

        /// <summary>
        /// Handle the right button selection.
        /// </summary>
        private void ButtonRightCommandExecute()
        {
            switch (this.buttonSelection)
            {
                case NouvemMessageBoxButtons.OKCancel:
                    NouvemMessageBox.UserSelection = UserDialogue.Cancel;
                    break;

                case NouvemMessageBoxButtons.YesNo:
                    NouvemMessageBox.UserSelection = UserDialogue.No;
                    break;

                case NouvemMessageBoxButtons.WorkflowStandard:
                    NouvemMessageBox.UserSelection = UserDialogue.Standard;
                    break;

                default:
                    NouvemMessageBox.UserSelection = UserDialogue.OK;
                    break;
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Set the buttons to display on the ui.
        /// </summary>
        /// <param name="buttonsSelection">The button selection enumeration value.</param>
        private void SetUpButtons(NouvemMessageBoxButtons buttonsSelection)
        {
            switch (buttonsSelection)
            {
                case NouvemMessageBoxButtons.OKCancel:
                    this.ButtonLeft = Strings.OK;
                    this.ButtonRight = Strings.Cancel;
                    this.ButtonLeftVisible = true;
                    break;

                case NouvemMessageBoxButtons.YesNo:
                    this.ButtonLeft = Strings.Yes;
                    this.ButtonRight = Strings.No;
                    this.ButtonLeftVisible = true;
                    break;

                case NouvemMessageBoxButtons.WorkflowStandard:
                    this.ButtonLeft = Strings.Workflow;
                    this.ButtonRight = Strings.Standard;
                    this.ButtonLeftVisible = true;
                    break;

                case NouvemMessageBoxButtons.OK:
                    this.ButtonLeftVisible = false;
                    this.ButtonRight = Strings.OK;
                    break;
            }

            this.buttonSelection = buttonsSelection;
        }

        #endregion

        #endregion
    }
}


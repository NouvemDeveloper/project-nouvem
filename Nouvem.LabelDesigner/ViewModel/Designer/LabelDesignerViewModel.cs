﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesignerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;
using Nouvem.LabelDesigner.View.Utility;

namespace Nouvem.LabelDesigner.ViewModel.Designer
{
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Neodynamic.SDK.Printing;
    using Neodynamic.Windows.WPF.ThermalLabelEditor;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Shared.Localisation;

    public class LabelDesignerViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The current label item.
        /// </summary>
        private Item currentItem;

        /// <summary>
        /// The currently selected view model reference.
        /// </summary>
        private LabelDesignerViewModelBase currentViewModel;

        /// <summary>
        /// The thermal label editor zoom value.
        /// </summary>
        private double zoomValue;

        /// <summary>
        /// The selected area x value.
        /// </summary>
        private double top;

        /// <summary>
        /// The selected area y value.
        /// </summary>
        private double left;

        /// <summary>
        /// The selected area width value.
        /// </summary>
        private double width;

        /// <summary>
        /// The selected area height value.
        /// </summary>
        private double height;

        /// <summary>
        /// Flag, as to whether a label has changed in any way.
        /// </summary>
        public bool LabelHasChanged;

        /// <summary>
        /// Flag, as to whether the curre4nt label has been printed.(This relates to caching. We don't want to recreate a label on every print, as we'll lose any caching benefits)
        /// </summary>
        private bool firstLabelPrinted;

        /// <summary>
        /// The add/open type.
        /// </summary>
        private string menuType = string.Empty;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the LabelDesignerViewModel class.
        /// </summary>
        public LabelDesignerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            //// Register for a label item change.
            //Messenger.Default.Register<string>(this, Token.LabelItemChange, s => this.labelHasChanged = true);

            Messenger.Default.Register<ThermalLabelEditor>(this, Token.LabelEditorSent, x => this.HandleChangedLabelSave(x));

            Messenger.Default.Register<ThermalLabelEditor>(this, Token.SaveAs, x =>
            {
                this.LabelHasChanged = false;
                var editor = x;
                if (editor.LabelDocument == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLabelCreated);
                    return;
                }

                var saveAs = new SaveAsView(editor);
                saveAs.ShowDialog();
                saveAs.Focus();
            });

            #endregion

            #region command

            // Handle the ui view swap.
            this.SwapViewCommand = new RelayCommand(() =>
            {
                if (Settings.Default.LabelSelectionGridSelected)
                {
                    this.SwitchView(this.Locator.LabelSelection);
                }
                else
                {
                    this.SwitchView(this.Locator.LabelSelectionGrid);
                }

                Settings.Default.LabelSelectionGridSelected = !Settings.Default.LabelSelectionGridSelected;
                Settings.Default.Save();
            });

            // Handler to print a label.
            this.PrintLabelCommand = new RelayCommand<ThermalLabelEditor>(this.PrintLabelCommandExecute);

            // Handler for a toolbar selection.
            this.ToolBarCommand = new RelayCommand<object>(this.ToolBarCommandExecute);

            // Handler for the label item change.
            this.SelectedItemChangedCommand = new RelayCommand<ThermalLabelEditor>(this.SelectedItemChangedCommandExecute);

            // Handler for the label item resize/move.
            this.SelectionAreaChangedCommand = new RelayCommand<ThermalLabelEditor>(this.SelectionAreaChangedCommandExecute);

            // Handle the label item change.
            this.OnSelectionItemPropertyChangedCommand = new RelayCommand(this.OnSelectionItemPropertyChangedCommandExecute);

            // Handler for the close application command.
            this.CloseApplicationCommand = new RelayCommand(this.CloseApplicationCommandExecute);

            #endregion

            this.Locator = new ViewModelLocator();
            this.ZoomValue = 100;
            Task.Factory.StartNew(this.UpdateProgressbar).ContinueWith((s) => SystemMessage.Write(MessageType.Priority, Message.ApplicationLoaded));
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the ViewModelLocator reference.
        /// </summary>
        protected ViewModelLocator Locator { get; private set; }

        /// <summary>
        /// Gets or sets the thermal label editor zoom value.
        /// </summary>
        public double ZoomValue
        {
            get
            {
                return this.zoomValue;
            }

            set
            {
                this.zoomValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current view model to be displayed in the content area.
        /// </summary>
        public LabelDesignerViewModelBase CurrentViewModel
        {
            get
            {
                return this.currentViewModel;
            }

            set
            {
                if (this.currentViewModel == value)
                {
                    return;
                }
           
                this.currentViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected barcode bar height.
        /// </summary>
        public double Height
        {
            get
            {
                return this.height;
            }

            set
            {
                this.height = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected barcode bar width.
        /// </summary>
        public double Width
        {
            get
            {
                return this.width;
            }

            set
            {
                this.width = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected barcode bar width.
        /// </summary>
        public double Top
        {
            get
            {
                return this.top;
            }

            set
            {
                this.top = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected barcode bar width.
        /// </summary>
        public double Left
        {
            get
            {
                return this.left;
            }

            set
            {
                this.left = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Swap the ui view.
        /// </summary>
        public ICommand SwapViewCommand { get; private set; }

        /// <summary>
        /// Gets the toolbar command.
        /// </summary>
        public ICommand ToolBarCommand { get; private set; }

        /// <summary>
        /// Gets the editor item changed command.
        /// </summary>
        public ICommand OnSelectionItemPropertyChangedCommand{ get; private set; }

        /// <summary>
        /// Gets the editor item changed command.
        /// </summary>
        public ICommand SelectedItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the selection area changed command.
        /// </summary>
        public ICommand SelectionAreaChangedCommand { get; private set; }

        /// <summary>
        /// Gets the print label command.
        /// </summary>
        public ICommand PrintLabelCommand { get; private set; }

        /// <summary>
        /// Gets the close application command.
        /// </summary>
        public ICommand CloseApplicationCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        protected override void ControlSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Handle a ui change application close with outstanding label changes.
        /// </summary>
        private void HandleChangedLabelSave(ThermalLabelEditor editor)
        {
            try
            {
                if (this.menuType.Equals(Constant.New))
                {
                    this.Locator.LabelSetUp.SaveLabel(editor, false);
                    this.firstLabelPrinted = false;
                    ViewModelLocator.Cleanup();
                    this.LabelHasChanged = false;
                    this.SwitchView(this.Locator.LabelSetUp);
                    this.Locator.LabelSetUp.LabelName = string.Empty;
                    Messenger.Default.Send(Token.Message, Token.NewLabel);
                    return;
                }

                if (this.menuType.Equals(Constant.Open))
                {
                    this.Locator.LabelSetUp.SaveLabel(editor, false);
                    this.firstLabelPrinted = false;
                    ViewModelLocator.ClearLabelSetUp();
                    this.LabelHasChanged = false;
                    if (Settings.Default.LabelSelectionGridSelected)
                    {
                        this.SwitchView(this.Locator.LabelSelectionGrid);
                    }
                    else
                    {
                        this.SwitchView(this.Locator.LabelSelection);
                    }

                    return;
                }

                // application closing check.
                if (this.LabelHasChanged)
                {
                    NouvemMessageBox.Show(Message.SaveLabelConfirmation, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        this.Locator.LabelSetUp.SaveLabel(editor, false);
                    }
                }
            }
            finally
            {
                this.menuType = string.Empty;
            }
        }

        /// <summary>
        /// THe tool bar selection handler.
        /// </summary>
        /// <param name="item">The command parameter.</param>
        private void ToolBarCommandExecute(object item)
        {
            if (item is ThermalLabelEditor)
            {
                // save label
                this.LabelHasChanged = false;
                var editor = item as ThermalLabelEditor;
                if (editor.LabelDocument == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLabelCreated);
                    return;
                }

                // send to the label set up view for processing
                this.Locator.LabelSetUp.SaveLabel(editor);
                return;
            }

            if (item.Equals(Constant.New))
            {
                if (this.LabelHasChanged)
                {
                    NouvemMessageBox.Show(Message.SaveLabelConfirmation, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        this.menuType = Constant.New;
                        Messenger.Default.Send(Token.LabelEditorSent, Token.SendLabelEditor);
                        return;
                    }
                }

                this.firstLabelPrinted = false;
                ViewModelLocator.Cleanup();
                this.LabelHasChanged = false;
                this.SwitchView(this.Locator.LabelSetUp);
                this.Locator.LabelSetUp.LabelName = string.Empty;
                Messenger.Default.Send(Token.Message, Token.NewLabel);
                return;
            }

            if (item.Equals(Constant.Open))
            {
                if (this.LabelHasChanged)
                {
                    NouvemMessageBox.Show(Message.SaveLabelConfirmation, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        this.menuType = Constant.Open;
                        Messenger.Default.Send(Token.LabelEditorSent, Token.SendLabelEditor);
                        return;
                    }
                }

                this.firstLabelPrinted = false;
                ViewModelLocator.ClearLabelSetUp();
                this.LabelHasChanged = false;
                if (Settings.Default.LabelSelectionGridSelected)
                {
                    this.SwitchView(this.Locator.LabelSelectionGrid);
                }
                else
                {
                    this.SwitchView(this.Locator.LabelSelection);
                }
      
                return;
            }

            if (item.Equals(Constant.Shape))
            {
                // ViewModelLocator.ClearLabelSetUp();
                this.SwitchView(this.Locator.UtilityItem);
                return;
            }

            if (item.Equals(Constant.Ellipse))
            {
                //ViewModelLocator.ClearLabelSetUp();
                this.SwitchView(this.Locator.UtilityItem);
                return;
            }

            if (item.Equals(Constant.Rectangle))
            {
                // ViewModelLocator.ClearLabelSetUp();
                this.SwitchView(this.Locator.UtilityItem);
                return;
            }

            if (item.Equals(Constant.Line))
            {
                //ViewModelLocator.ClearLabelSetUp();
                this.SwitchView(this.Locator.UtilityItem);
                return;
            }

            if (item.Equals(Constant.RFID))
            {
                // ViewModelLocator.ClearLabelSetUp();
                this.SwitchView(this.Locator.UtilityItem);
                return;
            }

            if (item.Equals(Constant.Barcode))
            {
                return;
            }

            if (item.Equals(Constant.ZoomOut))
            {
                if (this.ZoomValue >= 20)
                {
                    this.ZoomValue -= 10;
                }

                return;
            }

            if (item.Equals(Constant.ZoomIn))
            {
                this.ZoomValue += 10;
                return;
            }

            if (item.Equals(Constant.ZoomOriginal))
            {
                this.ZoomValue = 100;
                return;
            }

            if (item.Equals(Constant.ExportToImage))
            {
            }

            if (item.Equals(Constant.PrinterSetUp))
            {
                //ViewModelLocator.ClearLabelSetUp();
                this.SwitchView(this.Locator.PrinterSetUp);
                return;
            }

            if (item.Equals(Constant.DataSource))
            {
                this.SwitchView(this.Locator.DataSource);
                return;
            }

            if (item.Equals(Constant.Print))
            {
                // print the current editor label
                var result = this.PrintManager.Print();
                if (result != string.Empty)
                {
                    SystemMessage.Write(MessageType.Issue, result);
                }

                //this.SwitchView(this.Locator.Print);
            }
        }

        /// <summary>
        /// Prints the current label.
        /// </summary>
        /// <param name="editor">The ui editor.</param>
        private void PrintLabelCommandExecute(ThermalLabelEditor editor)
        {
            /* If a label hasn't been printed, then we create and store it.(recreating and storing it will lose any caching benefits)
             * If the label has changed, the we obviously need to recreate and store it */
            //if (!this.firstLabelPrinted || this.labelHasChanged)
            //{
            //    var label = editor.CreateThermalLabel();
            //    this.PrintManager.SetLabel(label);
            //    this.PrintManager.SetDataSource(LabelDesignerGlobal.DataSource);
            //}

            var label = editor.CreateThermalLabel();
            this.PrintManager.SetLabel(label);
            this.PrintManager.SetDataSource(LabelDesignerGlobal.DataSource);
           
            var result = this.PrintManager.Print();
            if (result != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, result);
            }

            // reset our flags.
            this.firstLabelPrinted = true;
            this.LabelHasChanged = false;
        }

        /// <summary>
        /// Handles the label change.
        /// </summary>
        private void OnSelectionItemPropertyChangedCommandExecute()
        {
            this.LabelHasChanged = true;
        }

        /// <summary>
        /// Handles the co-ordibate changes of a selected editor control item.
        /// </summary>
        /// <param name="editor">The editor reference.</param>
        private void SelectionAreaChangedCommandExecute(ThermalLabelEditor editor)
        {
            var area = editor.CurrentSelectionArea;
            this.Left = area.X;
            this.Top = area.Y;
            this.Width = area.Width;
            this.Height = area.Height;
            //this.labelHasChanged = true;
        }

        /// <summary>
        /// Handles the change of editor item selection.
        /// </summary>
        /// <param name="editor">The editor.</param>
        private void SelectedItemChangedCommandExecute(ThermalLabelEditor editor)
        {
            this.currentItem = editor.CurrentSelection;

            if (this.currentItem == null )
            {
                // selection of non item on label made, so diplay the general label set up view 
                this.SwitchView(this.Locator.LabelSetUp);
            }
            else if (this.currentItem is BarcodeItem)
            {
                this.SwitchView(this.Locator.BarcodeItem);
            }
            else if (this.currentItem is TextItem)
            {
                var fontSize = (this.currentItem as TextItem).Font.Size;
                var font = (this.currentItem as TextItem).Font.Name;
                var localRotation = (this.currentItem as TextItem).RotationAngle;
                var localAllignment = (this.currentItem as TextItem).TextAlignment;
                this.SwitchView(this.Locator.TextItem);
                Messenger.Default.Send(fontSize, Token.SetFontSize);
                Messenger.Default.Send(font, Token.SetFontName);
                if (this.currentItem is TextItem)
                {
                    (this.currentItem as TextItem).RotationAngle = localRotation;
                    (this.currentItem as TextItem).TextAlignment = localAllignment;
                }
                
            }
            else 
            {
                if (this.currentItem is ImageItem)
                {
                    (this.currentItem as ImageItem).UseCache = true;
                    (this.currentItem as ImageItem).CacheItemId = "1";
                }

                this.SwitchView(this.Locator.UtilityItem);
                if (this.currentItem is ImageItem)
                {
                    (this.currentItem as ImageItem).UseCache = true;
                    (this.currentItem as ImageItem).CacheItemId = "1";
                }
            }

            // Inform all label item view models of an item change.
            Messenger.Default.Send(this.currentItem, Token.SelectedItemChange);
        }

        /// <summary>
        /// Close the application.
        /// </summary>
        private void CloseApplicationCommandExecute()
        {
            System.Windows.Application.Current.Shutdown();
            Process.GetCurrentProcess().Kill();
        }

        /// <summary>
        /// Updates the progress bar. 
        /// </summary
        /// <remarks>Note: this is currently just for visual effect i.e. it's not linked to any processing.</remarks>
        private void UpdateProgressbar()
        {
            ProgressBar.SetUp();
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    System.Threading.Thread.Sleep(10);
                    ProgressBar.Run();
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SaveAsViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using GalaSoft.MvvmLight.Messaging;
using Neodynamic.Windows.WPF.ThermalLabelEditor;
using Nouvem.LabelDesigner.Global;
using Nouvem.LabelDesigner.Model.Enum;
using Nouvem.Shared.Localisation;

namespace Nouvem.LabelDesigner.ViewModel.Utility
{
    using System;

    public class SaveAsViewModel : LabelDesignerViewModelBase
    {
        #region private

        /// <summary>
        /// The label name field.
        /// </summary>
        private string labelName;

        /// <summary>
        /// The label editor.
        /// </summary>
        private ThermalLabelEditor editor;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveAsViewModel"/> class.
        /// </summary>
        public SaveAsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            Messenger.Default.Register<ThermalLabelEditor>(this, Token.SaveAsEditor, e => this.editor = e);
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the label name.
        /// </summary>
        public string LabelName
        {
            get
            {
                return this.labelName;
            }

            set
            {
                this.labelName = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region protected

        /// <summary>
        /// Send the save as name.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            if (string.IsNullOrEmpty(this.LabelName))
            {
                Global.SystemMessage.Write(MessageType.Issue, Message.NoLabelNameEntered);
                return;
            }

            this.Locator.LabelSetUp.LabelName = this.LabelName;
            this.Locator.LabelSetUp.SaveLabel(this.editor, saveAs:true);
            this.Close();
        }

        /// <summary>
        /// Clean up and close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearSaveAs();
            Messenger.Default.Send(Token.Message, Token.CloseMessageBoxWindow);
        }

        #endregion
    }
}


/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Nouvem.LabelDesigner"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using DevExpress.Xpf.Core.HandleDecorator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Nouvem.LabelDesigner.ViewModel.Data;
using Nouvem.LabelDesigner.ViewModel.Designer;
using Nouvem.LabelDesigner.ViewModel.Label;
using Nouvem.LabelDesigner.ViewModel.LabelItem;
using Nouvem.LabelDesigner.ViewModel.Print;
using Nouvem.LabelDesigner.ViewModel.SystemMessage;
using Nouvem.LabelDesigner.ViewModel.UserInput;
using Nouvem.LabelDesigner.ViewModel.Utility;

namespace Nouvem.LabelDesigner.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// The main menu view model reference.
        /// </summary>
        private static MenuViewModel menuViewModel;

        /// <summary>
        /// The main menu view model reference.
        /// </summary>
        private static LabelDesignerViewModel labelDesignerViewModel;

        /// <summary>
        /// The barcode item view model reference.
        /// </summary>
        private static BarcodeItemViewModel barcodeItemViewModel;

        /// <summary>
        /// The text item view model reference.
        /// </summary>
        private static TextItemViewModel textItemViewModel;

        /// <summary>
        /// The font view model reference.
        /// </summary>
        private static FontViewModel fontViewModel;

        /// <summary>
        /// The printer set up view model reference.
        /// </summary>
        private static PrinterSetUpViewModel printerSetUpViewModel = new PrinterSetUpViewModel();

        /// <summary>
        /// The barcode item view model reference.
        /// </summary>
        private static LabelSetUpViewModel labelSetUpViewModel = new LabelSetUpViewModel();

        /// <summary>
        /// The barcode item view model reference.
        /// </summary>
        private static SystemMessageViewModel systemMessageViewModel;

        /// <summary>
        /// The label group set up view model reference.
        /// </summary>
        private static LabelGroupSetUpViewModel labelGroupSetUpViewModel;

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static LabelSelectionViewModel labelSelectionViewModel = new LabelSelectionViewModel();

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static LabelSelectionGridViewModel labelSelectionGridViewModel = new LabelSelectionGridViewModel();

        /// <summary>
        /// The message box view model reference.
        /// </summary>
        private static NouvemMessageBoxViewModel messageBox = new NouvemMessageBoxViewModel();

        /// <summary>
        /// The line item view model reference.
        /// </summary>
        private static UtilityViewModel utilityItemViewModel;

        /// <summary>
        /// The save as view model reference.
        /// </summary>
        private static SaveAsViewModel saveAsViewModel;

        /// <summary>
        /// The ellipse item view model reference.
        /// </summary>
        private static EllipseItemViewModel ellipseItemViewModel;

        /// <summary>
        /// The rfid item view model reference.
        /// </summary>
        private static RFIDItemViewModel rfidItemViewModel;

        /// <summary>
        /// The rectangle item view model reference.
        /// </summary>
        private static RectangleItemViewModel rectangleItemViewModel;

        /// <summary>
        /// The shape item view model reference.
        /// </summary>
        private static ShapeItemViewModel shapeItemViewModel;

        /// <summary>
        /// The barcode builder view model reference.
        /// </summary>
        private static BarcodeBuilderViewModel barcodeBuilderViewModel = new BarcodeBuilderViewModel();

        /// <summary>
        /// The data source view model reference.
        /// </summary>
        private static DataSourceViewModel dataSourceViewModel = new DataSourceViewModel();

        /// <summary>
        /// The gs1 compressed view model reference.
        /// </summary>
        private static Gs1CompressedViewModel gs1CompressedViewModel = new Gs1CompressedViewModel();

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
           //ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    // Create design time view services and models
            //    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            //}
            //else
            //{
            //    // Create run time view services and models
            //    SimpleIoc.Default.Register<IDataService, DataService>();
            //}

            //SimpleIoc.Default.Register<MenuViewModel>();
        }

        #region Menu

        /// <summary>
        /// Gets the Menu property.
        /// </summary>
        public static MenuViewModel MenuStatic
        {
            get
            {
                if (menuViewModel == null)
                {
                    CreateMenu();
                }
     
                return menuViewModel;
            }
        }

        /// <summary>
        /// Gets the Menu property.
        /// </summary>
        public MenuViewModel Menu
        {
            get
            {
                return MenuStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Menu property.
        /// </summary>
        public static void ClearMenu()
        {
            if (menuViewModel != null)
            {
                menuViewModel.Cleanup();
                menuViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Menu property.
        /// </summary>
        public static void CreateMenu()
        {
            if (menuViewModel == null)
            {
                menuViewModel = new MenuViewModel();
            }
        }

        #endregion        

        #region LabelDesigner

        /// <summary>
        /// Gets the LabelDesigner property.
        /// </summary>
        public static LabelDesignerViewModel LabelDesignerStatic
        {
            get
            {
                if (labelDesignerViewModel == null)
                {
                    CreateLabelDesigner();
                }

                return labelDesignerViewModel;
            }
        }

        /// <summary>
        /// Gets the LabelDesigner property.
        /// </summary>
        public LabelDesignerViewModel LabelDesigner
        {
            get
            {
                return LabelDesignerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelDesigner property.
        /// </summary>
        public static void ClearLabelDesigner()
        {
            if (labelDesignerViewModel != null)
            {
                labelDesignerViewModel.Cleanup();
                labelDesignerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelDesigner property.
        /// </summary>
        public static void CreateLabelDesigner()
        {
            if (labelDesignerViewModel == null)
            {
                labelDesignerViewModel = new LabelDesignerViewModel();
            }
        }

        #endregion        

        #region BarcodeItem

        /// <summary>
        /// Gets the BarcodeItem property.
        /// </summary>
        public static BarcodeItemViewModel BarcodeItemStatic
        {
            get
            {
                if (barcodeItemViewModel == null)
                {
                    CreateBarcodeItem();
                }

                return barcodeItemViewModel;
            }
        }

        /// <summary>
        /// Gets the BarcodeItem property.
        /// </summary>
        public BarcodeItemViewModel BarcodeItem
        {
            get
            {
                return BarcodeItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BarcodeItem property.
        /// </summary>
        public static void ClearBarcodeItem()
        {
            if (barcodeItemViewModel != null)
            {
                barcodeItemViewModel.Cleanup();
                barcodeItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BarcodeItem property.
        /// </summary>
        public static void CreateBarcodeItem()
        {
            if (barcodeItemViewModel == null)
            {
                barcodeItemViewModel = new BarcodeItemViewModel();
            }
        }

        #endregion        

        #region TextItem

        /// <summary>
        /// Gets the TextItem property.
        /// </summary>
        public static TextItemViewModel TextItemStatic
        {
            get
            {
                if (textItemViewModel == null)
                {
                    CreateTextItem();
                }

                return textItemViewModel;
            }
        }

        /// <summary>
        /// Gets the TextItem property.
        /// </summary>
        public TextItemViewModel TextItem
        {
            get
            {
                return TextItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TextItem property.
        /// </summary>
        public static void ClearTextItem()
        {
            if (textItemViewModel != null)
            {
                textItemViewModel.Cleanup();
                textItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TextItem property.
        /// </summary>
        public static void CreateTextItem()
        {
            if (textItemViewModel == null)
            {
                textItemViewModel = new TextItemViewModel();
            }
        }

        #endregion        

        #region Font

        /// <summary>
        /// Gets the Font property.
        /// </summary>
        public static FontViewModel FontStatic
        {
            get
            {
                if (fontViewModel == null)
                {
                    CreateFont();
                }

                return fontViewModel;
            }
        }

        /// <summary>
        /// Gets the Font property.
        /// </summary>
        public FontViewModel Font
        {
            get
            {
                return FontStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Font property.
        /// </summary>
        public static void ClearFont()
        {
            if (fontViewModel != null)
            {
                fontViewModel.Cleanup();
                fontViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Font property.
        /// </summary>
        public static void CreateFont()
        {
            if (fontViewModel == null)
            {
                fontViewModel = new FontViewModel();
            }
        }

        #endregion        

        #region LabelSetUp

        /// <summary>
        /// Gets the LabelSetUp property.
        /// </summary>
        public static LabelSetUpViewModel LabelSetUpStatic
        {
            get
            {
                if (labelSetUpViewModel == null)
                {
                    CreateLabelSetUp();
                }

                return labelSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the LabelSetUp property.
        /// </summary>
        public LabelSetUpViewModel LabelSetUp
        {
            get
            {
                return LabelSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelSetUp property.
        /// </summary>
        public static void ClearLabelSetUp()
        {
            if (labelSetUpViewModel != null)
            {
                labelSetUpViewModel.Cleanup();
                labelSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelSetUp property.
        /// </summary>
        public static void CreateLabelSetUp()
        {
            if (labelSetUpViewModel == null)
            {
                labelSetUpViewModel = new LabelSetUpViewModel();
            }
        }

        #endregion        

        #region SaveAs

        /// <summary>
        /// Gets the SaveAs property.
        /// </summary>
        public static SaveAsViewModel SaveAsStatic
        {
            get
            {
                if (saveAsViewModel == null)
                {
                    CreateSaveAs();
                }

                return saveAsViewModel;
            }
        }

        /// <summary>
        /// Gets the SaveAs property.
        /// </summary>
        public SaveAsViewModel SaveAs
        {
            get
            {
                return SaveAsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SaveAs property.
        /// </summary>
        public static void ClearSaveAs()
        {
            if (saveAsViewModel != null)
            {
                saveAsViewModel.Cleanup();
                saveAsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SaveAs property.
        /// </summary>
        public static void CreateSaveAs()
        {
            if (saveAsViewModel == null)
            {
                saveAsViewModel = new SaveAsViewModel();
            }
        }

        #endregion        

        #region LabelSelection

        /// <summary>
        /// Gets the LabelSelection property.
        /// </summary>
        public static LabelSelectionViewModel LabelSelectionStatic
        {
            get
            {
                if (labelSelectionViewModel == null)
                {
                    CreateLabelSelection();
                }

                return labelSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the LabelSelection property.
        /// </summary>
        public LabelSelectionViewModel LabelSelection
        {
            get
            {
                return LabelSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelSelection property.
        /// </summary>
        public static void ClearLabelSelection()
        {
            if (labelSelectionViewModel != null)
            {
                labelSelectionViewModel.Cleanup();
                labelSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelSelection property.
        /// </summary>
        public static void CreateLabelSelection()
        {
            if (labelSelectionViewModel == null)
            {
                labelSelectionViewModel = new LabelSelectionViewModel();
            }
        }

        #endregion        

        #region LabelSelectionGridGrid

        /// <summary>
        /// Gets the LabelSelectionGrid property.
        /// </summary>
        public static LabelSelectionGridViewModel LabelSelectionGridStatic
        {
            get
            {
                if (labelSelectionGridViewModel == null)
                {
                    CreateLabelSelectionGrid();
                }

                return labelSelectionGridViewModel;
            }
        }

        /// <summary>
        /// Gets the LabelSelectionGrid property.
        /// </summary>
        public LabelSelectionGridViewModel LabelSelectionGrid
        {
            get
            {
                return LabelSelectionGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelSelectionGrid property.
        /// </summary>
        public static void ClearLabelSelectionGrid()
        {
            if (labelSelectionGridViewModel != null)
            {
                labelSelectionGridViewModel.Cleanup();
                labelSelectionGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelSelectionGrid property.
        /// </summary>
        public static void CreateLabelSelectionGrid()
        {
            if (labelSelectionGridViewModel == null)
            {
                labelSelectionGridViewModel = new LabelSelectionGridViewModel();
            }
        }

        #endregion        

        #region PrinterSetUp

        /// <summary>
        /// Gets the PrinterSetUp property.
        /// </summary>
        public static PrinterSetUpViewModel PrinterSetUpStatic
        {
            get
            {
                if (printerSetUpViewModel == null)
                {
                    CreatePrinterSetUp();
                }

                return printerSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the PrinterSetUp property.
        /// </summary>
        public PrinterSetUpViewModel PrinterSetUp
        {
            get
            {
                return PrinterSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PrinterSetUp property.
        /// </summary>
        public static void ClearPrinterSetUp()
        {
            if (printerSetUpViewModel != null)
            {
                printerSetUpViewModel.Cleanup();
                printerSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PrinterSetUp property.
        /// </summary>
        public static void CreatePrinterSetUp()
        {
            if (printerSetUpViewModel == null)
            {
                printerSetUpViewModel = new PrinterSetUpViewModel();
            }
        }

        #endregion        

        #region UtilityItem

        /// <summary>
        /// Gets the UtilityItem property.
        /// </summary>
        public static UtilityViewModel UtilityItemStatic
        {
            get
            {
                if (utilityItemViewModel == null)
                {
                    CreateUtilityItem();
                }

                return utilityItemViewModel;
            }
        }

        /// <summary>
        /// Gets the UtilityItem property.
        /// </summary>
        public UtilityViewModel UtilityItem
        {
            get
            {
                return UtilityItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UtilityItem property.
        /// </summary>
        public static void ClearUtilityItem()
        {
            if (utilityItemViewModel != null)
            {
                utilityItemViewModel.Cleanup();
                utilityItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UtilityItem property.
        /// </summary>
        public static void CreateUtilityItem()
        {
            if (utilityItemViewModel == null)
            {
                utilityItemViewModel = new UtilityViewModel();
            }
        }

        #endregion        

        #region ShapeItem

        /// <summary>
        /// Gets the ShapeItem property.
        /// </summary>
        public static ShapeItemViewModel ShapeItemStatic
        {
            get
            {
                if (shapeItemViewModel == null)
                {
                    CreateShapeItem();
                }

                return shapeItemViewModel;
            }
        }

        /// <summary>
        /// Gets the ShapeItem property.
        /// </summary>
        public ShapeItemViewModel ShapeItem
        {
            get
            {
                return ShapeItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ShapeItem property.
        /// </summary>
        public static void ClearShapeItem()
        {
            if (shapeItemViewModel != null)
            {
                shapeItemViewModel.Cleanup();
                shapeItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ShapeItem property.
        /// </summary>
        public static void CreateShapeItem()
        {
            if (shapeItemViewModel == null)
            {
                shapeItemViewModel = new ShapeItemViewModel();
            }
        }

        #endregion        

        #region EllipseItem

        /// <summary>
        /// Gets the EllipseItem property.
        /// </summary>
        public static EllipseItemViewModel EllipseItemStatic
        {
            get
            {
                if (ellipseItemViewModel == null)
                {
                    CreateEllipseItem();
                }

                return ellipseItemViewModel;
            }
        }

        /// <summary>
        /// Gets the EllipseItem property.
        /// </summary>
        public EllipseItemViewModel EllipseItem
        {
            get
            {
                return EllipseItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the EllipseItem property.
        /// </summary>
        public static void ClearEllipseItem()
        {
            if (ellipseItemViewModel != null)
            {
                ellipseItemViewModel.Cleanup();
                ellipseItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the EllipseItem property.
        /// </summary>
        public static void CreateEllipseItem()
        {
            if (ellipseItemViewModel == null)
            {
                ellipseItemViewModel = new EllipseItemViewModel();
            }
        }

        #endregion        

        #region RectangleItem

        /// <summary>
        /// Gets the RectangleItem property.
        /// </summary>
        public static RectangleItemViewModel RectangleItemStatic
        {
            get
            {
                if (rectangleItemViewModel == null)
                {
                    CreateRectangleItem();
                }

                return rectangleItemViewModel;
            }
        }

        /// <summary>
        /// Gets the RectangleItem property.
        /// </summary>
        public RectangleItemViewModel RectangleItem
        {
            get
            {
                return RectangleItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the RectangleItem property.
        /// </summary>
        public static void ClearRectangleItem()
        {
            if (rectangleItemViewModel != null)
            {
                rectangleItemViewModel.Cleanup();
                rectangleItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the RectangleItem property.
        /// </summary>
        public static void CreateRectangleItem()
        {
            if (rectangleItemViewModel == null)
            {
                rectangleItemViewModel = new RectangleItemViewModel();
            }
        }

        #endregion        

        #region RFIDItem

        /// <summary>
        /// Gets the RFIDItem property.
        /// </summary>
        public static RFIDItemViewModel RFIDItemStatic
        {
            get
            {
                if (rfidItemViewModel == null)
                {
                    CreateRFIDItem();
                }

                return rfidItemViewModel;
            }
        }

        /// <summary>
        /// Gets the RFIDItem property.
        /// </summary>
        public RFIDItemViewModel RFIDItem
        {
            get
            {
                return RFIDItemStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the RFIDItem property.
        /// </summary>
        public static void ClearRFIDItem()
        {
            if (rfidItemViewModel != null)
            {
                rfidItemViewModel.Cleanup();
                rfidItemViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the RFIDItem property.
        /// </summary>
        public static void CreateRFIDItem()
        {
            if (rfidItemViewModel == null)
            {
                rfidItemViewModel = new RFIDItemViewModel();
            }
        }

        #endregion        

        #region LabelGroupSetUp

        /// <summary>
        /// Gets the LabelGroupSetUp property.
        /// </summary>
        public static LabelGroupSetUpViewModel LabelGroupSetUpStatic
        {
            get
            {
                if (labelGroupSetUpViewModel == null)
                {
                    CreateLabelGroupSetUp();
                }

                return labelGroupSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the LabelGroupSetUp property.
        /// </summary>
        public LabelGroupSetUpViewModel LabelGroupSetUp
        {
            get
            {
                return LabelGroupSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelGroupSetUp property.
        /// </summary>
        public static void ClearLabelGroupSetUp()
        {
            if (labelGroupSetUpViewModel != null)
            {
                labelGroupSetUpViewModel.Cleanup();
                labelGroupSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelGroupSetUp property.
        /// </summary>
        public static void CreateLabelGroupSetUp()
        {
            if (labelGroupSetUpViewModel == null)
            {
                labelGroupSetUpViewModel = new LabelGroupSetUpViewModel();
            }
        }

        #endregion        

        #region BarcodeBuilder

        /// <summary>
        /// Gets the BarcodeBuilder property.
        /// </summary>
        public static BarcodeBuilderViewModel BarcodeBuilderStatic
        {
            get
            {
                if (barcodeBuilderViewModel == null)
                {
                    CreateBarcodeBuilder();
                }

                return barcodeBuilderViewModel;
            }
        }

        /// <summary>
        /// Gets the BarcodeBuilder property.
        /// </summary>
        public BarcodeBuilderViewModel BarcodeBuilder
        {
            get
            {
                return BarcodeBuilderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BarcodeBuilder property.
        /// </summary>
        public static void ClearBarcodeBuilder()
        {
            if (barcodeBuilderViewModel != null)
            {
                barcodeBuilderViewModel.Cleanup();
                barcodeBuilderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BarcodeBuilder property.
        /// </summary>
        public static void CreateBarcodeBuilder()
        {
            if (barcodeBuilderViewModel == null)
            {
                barcodeBuilderViewModel = new BarcodeBuilderViewModel();
            }
        }

        #endregion        

        #region SystemMessage

        /// <summary>
        /// Gets the system message property.
        /// </summary>
        public static SystemMessageViewModel SystemMessageStatic
        {
            get
            {
                if (systemMessageViewModel == null)
                {
                    CreateSystemMessage();
                }

                return systemMessageViewModel;
            }
        }

        /// <summary>
        /// Gets the systemMessage property.
        /// </summary>
        public SystemMessageViewModel SystemMessage
        {
            get
            {
                return SystemMessageStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the systemMessage property.
        /// </summary>
        public static void ClearInformationBar()
        {
            if (systemMessageViewModel != null)
            {
                systemMessageViewModel.Cleanup();
                systemMessageViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the systemMessage property.
        /// </summary>
        public static void CreateSystemMessage()
        {
            if (systemMessageViewModel == null)
            {
                systemMessageViewModel = new SystemMessageViewModel();
            }
        }

        #endregion

        #region MessageBox

        /// <summary>
        /// Gets the MessageBox property.
        /// </summary>
        public static NouvemMessageBoxViewModel MessageBoxStatic
        {
            get
            {
                if (messageBox == null)
                {
                    CreateMessageBox();
                }

                return messageBox;
            }
        }

        /// <summary>
        /// Gets the MessageBox property.
        /// </summary>
        public NouvemMessageBoxViewModel MessageBox
        {
            get
            {
                return MessageBoxStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the MessageBox property.
        /// </summary>
        public static void ClearMessageBox()
        {
            if (messageBox != null)
            {
                messageBox.Cleanup();
                messageBox = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the MessageBox property.
        /// </summary>
        public static void CreateMessageBox()
        {
            if (messageBox == null)
            {
                messageBox = new NouvemMessageBoxViewModel();
            }
        }

        #endregion

        #region DataSource

        /// <summary>
        /// Gets the DataSource property.
        /// </summary>
        public static DataSourceViewModel DataSourceStatic
        {
            get
            {
                if (dataSourceViewModel == null)
                {
                    CreateDataSource();
                }

                return dataSourceViewModel;
            }
        }

        /// <summary>
        /// Gets the DataSource property.
        /// </summary>
        public DataSourceViewModel DataSource
        {
            get
            {
                return DataSourceStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DataSource property.
        /// </summary>
        public static void ClearDataSource()
        {
            if (dataSourceViewModel != null)
            {
                dataSourceViewModel.Cleanup();
                dataSourceViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DataSource property.
        /// </summary>
        public static void CreateDataSource()
        {
            if (dataSourceViewModel == null)
            {
                dataSourceViewModel = new DataSourceViewModel();
            }
        }

        #endregion        

        #region Gs1Compressed

        /// <summary>
        /// Gets the Gs1Compressed property.
        /// </summary>
        public static Gs1CompressedViewModel Gs1CompressedStatic
        {
            get
            {
                if (gs1CompressedViewModel == null)
                {
                    CreateGs1Compressed();
                }

                return gs1CompressedViewModel;
            }
        }

        /// <summary>
        /// Gets the Gs1Compressed property.
        /// </summary>
        public Gs1CompressedViewModel Gs1Compressed
        {
            get
            {
                return Gs1CompressedStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Gs1Compressed property.
        /// </summary>
        public static void ClearGs1Compressed()
        {
            if (gs1CompressedViewModel != null)
            {
                gs1CompressedViewModel.Cleanup();
                gs1CompressedViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Gs1Compressed property.
        /// </summary>
        public static void CreateGs1Compressed()
        {
            if (gs1CompressedViewModel == null)
            {
                gs1CompressedViewModel = new Gs1CompressedViewModel();
            }
        }

        #endregion        
        
        /// <summary>
        /// Clean up the view models.
        /// </summary>
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
            ClearLabelSetUp();
            ClearBarcodeItem();
            ClearTextItem();
            ClearRectangleItem();
            ClearRFIDItem();
            ClearEllipseItem();
            ClearUtilityItem();
            ClearBarcodeBuilder();
        }
    }
}
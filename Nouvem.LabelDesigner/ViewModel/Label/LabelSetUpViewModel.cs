﻿// -----------------------------------------------------------------------
// <copyright file="LabelSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel.Label
{
    using System;
    using System.Collections.ObjectModel;
    using GalaSoft.MvvmLight.Messaging;
    using System.Linq;
    using Neodynamic.SDK.Printing;
    using Neodynamic.Windows.WPF.ThermalLabelEditor;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.DataLayer;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Shared.Localisation;

    public class LabelSetUpViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The current label.
        /// </summary>
        private Label currentLabel;

        /// <summary>
        /// The label name.
        /// </summary>
        private string labelName;

        /// <summary>
        /// The selected unit of measure field.
        /// </summary>
        private string selectedUnitType;

        /// <summary>
        /// The label width.
        /// </summary>
        private double labelWidth;

        /// <summary>
        /// The label height.
        /// </summary>
        private double labelHeight;

        /// <summary>
        /// The label gap length.
        /// </summary>
        private double labelGapLength;

        /// <summary>
        /// The label mark length.
        /// </summary>
        private double labelMarkLength;

        /// <summary>
        /// The labels per row number.
        /// </summary>
        private int labelsPerRow;

        /// <summary>
        /// The horizontal gap length between labels when the media roll contains more than one label per row.
        /// </summary>
        private double labelsHorizontalGapLength;

        /// <summary>
        /// The offset length or thickness between labels. The unit of measurement is specified by UnitType property
        /// </summary>
        private double offsetLength;

        /// <summary>
        /// The label continuous flag.
        /// </summary>
        private bool continuous;

        /// <summary>
        /// The selected label group
        /// </summary>
        private LabelGroup selectedlabelGroup;

        /// <summary>
        /// The label details reference.
        /// </summary>
        private ObservableCollection<LabelItemData> labelDetails;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the LabelSetUpViewModel class.
        /// </summary>
        public LabelSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<ThermalLabelEditor>(this, Token.Delete, x =>
            {
                var editor = x;
                if (editor.LabelDocument == null)
                {
                    return;
                }

                NouvemMessageBox.Show(Message.LabelDeletionConfirmation, NouvemMessageBoxButtons.YesNo);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    if (this.DataManager.DeleteLabel(this.currentLabel))
                    {
                        SystemMessage.Write(MessageType.Priority, Message.LabelDeleted);
                        this.SwitchView(this.Locator.LabelSetUp);
                        this.Locator.LabelSetUp.LabelName = string.Empty;
                        Messenger.Default.Send(Token.Message, Token.NewLabel);
                        this.Locator.LabelSelection.Refresh();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.LabelNotDeleted);
                    }
                }
            });

            // Register to handle a label item add/removal i.e. added or removed from the label.
            Messenger.Default.Register<Tuple<string, string, string, string>>(this, Token.LabelItemChange, this.HandleLabelItemChange);

            // Register to handle a label item update
            Messenger.Default.Register<Item>(this, Token.SelectedItemUpdate, this.HandleLabelItemUpdate);

            #endregion

            #region instantiation

            this.LabelDetails = new ObservableCollection<LabelItemData>();

            #endregion

            if (this.LabelGroups == null)
            {
                this.GetLabelGroups();
            }
        
            this.SetLabelProperties();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected unit type.
        /// </summary>
        public ObservableCollection<LabelItemData> LabelDetails 
        {
            get
            {
                return this.labelDetails;
            }

            set
            {
                this.labelDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the label groups.
        /// </summary>
        public ObservableCollection<LabelGroup> LabelGroups { get; set; }

        /// <summary>
        /// Gets or sets the selected unit type.
        /// </summary>
        public string SelectedUnitType
        {
            get
            {
                return this.selectedUnitType;
            }

            set
            {
                this.selectedUnitType = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the label name.
        /// </summary>
        public string LabelName
        {
            get
            {
                return this.labelName;
            }

            set
            {
                this.labelName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected label group.
        /// </summary>
        public LabelGroup SelectedLabelGroup
        {
            get
            {
                return this.selectedlabelGroup;
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                this.selectedlabelGroup = value;
                this.RaisePropertyChanged();

                if (value.Name.Equals(Strings.DefineNew))
                {
                    // create a new label group
                    this.SwitchView(this.Locator.LabelGroupSetUp);
                }
            }
        }

        /// <summary>
        /// Gets or sets the label height.
        /// </summary>
        public double LabelHeight
        {
            get
            {
                return this.labelHeight;
            }

            set
            {
                this.labelHeight = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the label width.
        /// </summary>
        public double LabelWidth
        {
            get
            {
                return this.labelWidth;
            }

            set
            {
                this.labelWidth = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the label gap length.
        /// </summary>
        public double LabelGapLength
        {
            get
            {
                return this.labelGapLength;
            }

            set
            {
                this.labelGapLength = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the label mark length.
        /// </summary>
        public double LabelMarkLength
        {
            get
            {
                return this.labelMarkLength;
            }

            set
            {
                this.labelMarkLength = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the labels per row.
        /// </summary>
        public int LabelsPerRow
        {
            get
            {
                return this.labelsPerRow;
            }

            set
            {
                this.labelsPerRow = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the horizontal gap length between labels when the media roll contains more than one label per row.
        /// </summary>
        public double LabelsHorizontalGapLength
        {
            get
            {
                return this.labelsHorizontalGapLength;
            }

            set
            {
                this.labelsHorizontalGapLength = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }
     
        /// <summary>
        ///   Gets or sets the offset length or thickness between labels. The unit of measurement is specified by UnitType property.
        /// </summary>
        public double OffsetLength
        {
            get
            {
                return this.offsetLength;
            }

            set
            {
                this.offsetLength = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }
      
        /// <summary>
        /// Gets or sets a value indicating whether the label is continuous or not.
        /// </summary>
        public bool Continuous
        {
            get
            {
                return this.continuous;
            }

            set
            {
                this.continuous = value;
                this.RaisePropertyChanged();
                this.SetUpLabel();
            }
        }

        /// <summary>
        /// Gets or sets the label unit types.
        /// </summary>
        public string[] UnitTypes { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Opens a stored label.
        /// </summary>
        /// <param name="labelData">The path of the label to open.</param>
        public void OpenLabel(Label labelData)
        {
            this.currentLabel = labelData;

            try
            {
                //Create a ThermalLabel obj from a Template
                var label = ThermalLabel.CreateFromXmlTemplate(labelData.LabelFile);
                this.SelectedUnitType = label.UnitType.ToString();
                this.LabelsPerRow = label.LabelsPerRow;
                this.LabelHeight = label.Height;
                this.LabelWidth = label.Width;
                this.LabelGapLength = label.GapLength;
                this.LabelMarkLength = label.MarkLength;
                this.Continuous = label.IsContinuous;
                this.LabelsHorizontalGapLength = label.LabelsHorizontalGapLength;
                this.OffsetLength = label.OffsetLength;
                this.LabelName = labelData.Name;
                this.Locator.PrinterSetUp.PrintOrientation = !string.IsNullOrEmpty(labelData.Orientation) ? labelData.Orientation : "Portrait";

                //load it on the editor surface
                Messenger.Default.Send(label, Token.LoadLabel);

                this.Locator.LabelDesigner.LabelHasChanged = false;

                // Store it, ready for printing
                this.PrintManager.SetLabel(label);
                
                SystemMessage.Write(MessageType.Background, Message.LoadLabelSuccess);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.LoadLabelFailure);
            }
        }

        #endregion

        #endregion

        #region internal

        /// <summary>
        /// Refreshes the label groups, selecting the most recently added.
        /// </summary>
        internal void RefreshGroups()
        {
            this.GetLabelGroups();
            if (this.LabelGroups.Count >= 2)
            {
                // a new group has been added, so select it ('define new' is the last, so it's the group preceding that)
                this.SelectedLabelGroup = this.LabelGroups.ElementAt(this.LabelGroups.Count - 2);
            }
        }

        /// <summary>
        /// Saves the current editor label.
        /// </summary>
        /// <param name="editor">The editor reference.</param>
        /// <param name="checkForOverwrite">Flag, as to whether we are checking for an overwrite and asking the user.</param>
        internal void SaveLabel(ThermalLabelEditor editor, bool checkForOverwrite = true, bool saveAs = false)
        {
            #region validation

            if (string.IsNullOrWhiteSpace(this.LabelName))
            {
                if (!checkForOverwrite)
                {
                    if (this.LabelName == null)
                    {
                        this.LabelName = string.Empty;
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.SelectLabelName);
                    return;
                }
            }

            if (!saveAs && checkForOverwrite && this.DoesLabelExist())
            {
                // label name already stored with a label, so inform the user that they are about to overwrite it.
                NouvemMessageBox.Show(Message.OverwriteLabel, NouvemMessageBoxButtons.YesNo);

                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    return;
                }
            }

            #endregion

            try
            {
                int? groupID = null;
                if (this.selectedlabelGroup != null && this.selectedlabelGroup.Name != Strings.DefineNew)
                {
                    groupID = this.selectedlabelGroup.LabelGroupID;
                }

                var localLabel = this.LabelManager.SaveLabel(editor, this.labelName, groupID);
                if (localLabel != null)
                {
                    SystemMessage.Write(MessageType.Priority, Message.LabelSaved);

                    // add it to the label selection labels.
                    this.Locator.LabelSelection.Refresh();
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.LabelSaveFailure);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        #endregion

        #region protected override

        /// <summary>
        /// Creates a new label, or resets to a default label.
        /// </summary>
        /// <param name="controlSelection">The control mode (OK/Cancel).</param>
        protected override void ControlButtonSelectedCommandExecute(string controlSelection)
        {
            base.ControlButtonSelectedCommandExecute(controlSelection);

            if (controlSelection.Equals(Constant.OK))
            {
                this.SetUpLabel();
                return;
            }

            // Cancel, so reset values to default
            this.SetLabelProperties();
            this.SetUpLabel();
        }

        #endregion

        #region private

        /// <summary>
        /// Determines if a label name exists.
        /// </summary>
        /// <returns>A flag, that determines if a label name existys.</returns>
        private bool DoesLabelExist()
        {
            try
            {
                foreach (var label in this.Locator.LabelSelection.Labels)
                {
                    if (label.Name.Equals(this.labelName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Gets the application label groups.
        /// </summary>
        private void GetLabelGroups()
        {
            this.LabelGroups = new ObservableCollection<LabelGroup>(this.DataManager.GetLabelGroups());
            this.LabelGroups.Add(new LabelGroup { Name = Strings.DefineNew });
        }

        /// <summary>
        /// Sets up, and sends a label to the editor.
        /// </summary>
        private void SetUpLabel()
        {
            if (this.selectedUnitType == null)
            {
                return;
            }

            var label = new ThermalLabel
            {
                Height = this.LabelHeight,
                Width = this.LabelWidth,
                GapLength = this.LabelGapLength,
                MarkLength = this.LabelMarkLength,
                IsContinuous = this.Continuous,
                LabelsPerRow = this.LabelsPerRow,
                LabelsHorizontalGapLength = this.LabelsHorizontalGapLength,
                OffsetLength = this.OffsetLength,
                UnitType = (UnitType)Enum.Parse(typeof(UnitType), this.selectedUnitType),
            };

            // Store it, ready for printing.
            this.PrintManager.SetLabel(label);

            // Send to the view to be displayed on the editor.
            Messenger.Default.Send(label);}

        /// <summary>
        /// Handles a label item removal or addition, by adding/removing it from the general grid.
        /// </summary>
        /// <param name="data">The item and add/removal instruction.</param>
        private void HandleLabelItemChange(Tuple<string, string, string, string> data)
        {
            var instruction = data.Item1;
            var labelItemName = data.Item2;
            var labelItemValue = data.Item3;
            var labelItemVariable = data.Item4;

            // add item
            if (instruction.Equals(Constant.Add))
            {
                this.LabelDetails.Add(new LabelItemData { Name = labelItemName, Value = labelItemValue, AssignedVariable = labelItemVariable});
                return;
            }

            // remove item
            if (instruction.Equals(Constant.Remove))
            {
                var itemToRemove = this.LabelDetails.FirstOrDefault(item => item.Name.Equals(labelItemName));
                if (itemToRemove != null)
                {
                    this.LabelDetails.Remove(itemToRemove);
                }
                
                return;
            }

            // remove everything
            if (instruction.Equals(Constant.RemoveAll))
            {
                this.LabelDetails.Clear();
            }
        }

        /// <summary>
        /// Handles a label item update, reflecting the changes in the general grid.
        /// </summary>
        /// <param name="labelItem">The label item updated.</param>
        private void HandleLabelItemUpdate(Item labelItem)
        {
            var itemToUpdate = this.LabelDetails.FirstOrDefault(item => item.Name.Equals(labelItem.Name));
            if (itemToUpdate != null)
            {
                if (labelItem is TextItem)
                {
                    itemToUpdate.Value = (labelItem as TextItem).Text;
                    itemToUpdate.AssignedVariable = (labelItem as TextItem).DataField;
                }
            }
        }

        /// <summary>
        /// Sets the label properties to their default values.
        /// </summary>
        private void SetLabelProperties()
        {
            this.UnitTypes = Enum.GetNames(typeof(UnitType));
            this.LabelHeight = 3;
            this.LabelWidth = 4;
            this.LabelsPerRow = 1;
            this.OffsetLength = 0;
            this.labelGapLength = 0;
            this.LabelMarkLength = 0;
            this.LabelsHorizontalGapLength = 0;
            this.continuous = false;
        }
      
        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }
    }
}

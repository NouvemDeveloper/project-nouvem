﻿CREATE TABLE [dbo].[NouBPType] (
    [NouBPTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [Type]        VARCHAR (30) NOT NULL,
    [Deleted]     BIT          NOT NULL,
    CONSTRAINT [PK_BusinessPartnerType] PRIMARY KEY CLUSTERED ([NouBPTypeID] ASC)
);


﻿CREATE TABLE [dbo].[WarehouseLocation] (
    [WarehouseLocationID]  INT            IDENTITY (1, 1) NOT NULL,
    [WarehouseID]          INT            NOT NULL,
    [Code]                 NCHAR (10)     NULL,
    [Name]                 NVARCHAR (30)  NOT NULL,
    [Barcode]              NVARCHAR (100) NULL,
    [MultiProductLocation] BIT            NOT NULL,
    [Deleted]              DATETIME       NULL,
    CONSTRAINT [PK_WarehouseLocation] PRIMARY KEY CLUSTERED ([WarehouseLocationID] ASC),
    CONSTRAINT [FK_WarehouseLocation_Warehouse] FOREIGN KEY ([WarehouseID]) REFERENCES [dbo].[Warehouse] ([WarehouseID])
);


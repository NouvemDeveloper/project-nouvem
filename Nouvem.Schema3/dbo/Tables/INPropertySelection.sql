﻿CREATE TABLE [dbo].[INPropertySelection] (
    [INPropertySelectionID] INT IDENTITY (1, 1) NOT NULL,
    [INMasterID]            INT NOT NULL,
    [INPropertyID]          INT NOT NULL,
    [Deleted]               INT NOT NULL,
    CONSTRAINT [PK_INPropertySelection] PRIMARY KEY CLUSTERED ([INPropertySelectionID] ASC),
    CONSTRAINT [FK_INPropertySelection_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_INPropertySelection_INProperty] FOREIGN KEY ([INPropertyID]) REFERENCES [dbo].[INProperty] ([INPropertyID])
);


﻿CREATE TABLE [dbo].[NouAuthorisationGroupName] (
    [NouAuthorisationGroupNamesID] INT           IDENTITY (1, 1) NOT NULL,
    [GroupName]                    NVARCHAR (50) NOT NULL,
    [Deleted]                      BIT           NOT NULL,
    CONSTRAINT [PK_AuthorisationGroupNames] PRIMARY KEY CLUSTERED ([NouAuthorisationGroupNamesID] ASC)
);


﻿CREATE TABLE [dbo].[PROrderOutput] (
    [PROrderOutputID] INT      IDENTITY (1, 1) NOT NULL,
    [PROrderID]       INT      NOT NULL,
    [INMasterID]      INT      NOT NULL,
    [Selected]        BIT      NOT NULL,
    [Deleted]         DATETIME NULL,
    CONSTRAINT [PK_PROutputOrder] PRIMARY KEY CLUSTERED ([PROrderOutputID] ASC),
    CONSTRAINT [FK_PROutputOrder_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_PROutputOrder_PROrder] FOREIGN KEY ([PROrderID]) REFERENCES [dbo].[PROrder] ([PROrderID])
);


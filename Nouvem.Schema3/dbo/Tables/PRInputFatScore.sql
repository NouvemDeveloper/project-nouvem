﻿CREATE TABLE [dbo].[PRInputFatScore] (
    [PRInputFatScoreID] INT      IDENTITY (1, 1) NOT NULL,
    [PRSpecID]          INT      NOT NULL,
    [NouFatScoreID]     INT      NOT NULL,
    [Selected]          BIT      NOT NULL,
    [Deleted]           DATETIME NULL,
    CONSTRAINT [PK_PRInputFatScore] PRIMARY KEY CLUSTERED ([PRInputFatScoreID] ASC),
    CONSTRAINT [FK_PRInputFatScore_PRSpec] FOREIGN KEY ([PRSpecID]) REFERENCES [dbo].[PRSpec] ([PRSpecID])
);


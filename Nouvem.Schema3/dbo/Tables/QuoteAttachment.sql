﻿CREATE TABLE [dbo].[QuoteAttachment] (
    [QuoteAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [ARQuoteID]         INT             NOT NULL,
    [FilePath]          NVARCHAR (50)   NULL,
    [FileName]          NVARCHAR (50)   NOT NULL,
    [AttachmentDate]    DATE            NOT NULL,
    [File]              VARBINARY (MAX) NOT NULL,
    [Deleted]           DATETIME        NULL,
    CONSTRAINT [PK_SaleAttachment] PRIMARY KEY CLUSTERED ([QuoteAttachmentID] ASC),
    CONSTRAINT [FK_QuoteAttachment_ARQuote] FOREIGN KEY ([ARQuoteID]) REFERENCES [dbo].[ARQuote] ([ARQuoteID])
);


﻿CREATE TABLE [dbo].[NouOrderMethod] (
    [NouOrderMethodID] INT          IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (30) NOT NULL,
    [Number]           INT          NOT NULL,
    [Deleted]          BIT          NOT NULL,
    CONSTRAINT [PK_NouOrderMethod] PRIMARY KEY CLUSTERED ([NouOrderMethodID] ASC)
);


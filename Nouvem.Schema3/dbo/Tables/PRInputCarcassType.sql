﻿CREATE TABLE [dbo].[PRInputCarcassType] (
    [PRInputCarcassTypeID] INT      IDENTITY (1, 1) NOT NULL,
    [PRSpecID]             INT      NOT NULL,
    [CarcassTypeID]        INT      NOT NULL,
    [Selected]             BIT      NOT NULL,
    [Deleted]              DATETIME NULL,
    CONSTRAINT [PK_PRInputCarcassType] PRIMARY KEY CLUSTERED ([PRInputCarcassTypeID] ASC),
    CONSTRAINT [FK_PRInputCarcassType_PRSpec] FOREIGN KEY ([PRSpecID]) REFERENCES [dbo].[PRSpec] ([PRSpecID])
);


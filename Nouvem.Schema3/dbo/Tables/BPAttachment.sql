﻿CREATE TABLE [dbo].[BPAttachment] (
    [BPAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [BPMasterID]     INT             NOT NULL,
    [FilePath]       NVARCHAR (50)   NULL,
    [FileName]       NVARCHAR (50)   NOT NULL,
    [AttachmentDate] DATE            NOT NULL,
    [Deleted]        BIT             NOT NULL,
    [File]           VARBINARY (MAX) CONSTRAINT [DF__BPAttachme__File__05F8DC4F] DEFAULT (0x01) NOT NULL,
    CONSTRAINT [PK_BPAttachments] PRIMARY KEY CLUSTERED ([BPAttachmentID] ASC),
    CONSTRAINT [FK_BPAttachments_BPMaster] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID])
);


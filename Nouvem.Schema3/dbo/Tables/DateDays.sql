﻿CREATE TABLE [dbo].[DateDays] (
    [DateDaysID]   INT      IDENTITY (1, 1) NOT NULL,
    [DateMasterID] INT      NOT NULL,
    [INMasterID]   INT      NOT NULL,
    [Days]         INT      NULL,
    [Deleted]      DATETIME NULL,
    CONSTRAINT [PK_DateDays] PRIMARY KEY CLUSTERED ([DateDaysID] ASC),
    CONSTRAINT [FK_DateDays_DateMaster] FOREIGN KEY ([DateMasterID]) REFERENCES [dbo].[DateMaster] ([DateMasterID]),
    CONSTRAINT [FK_DateDays_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID])
);


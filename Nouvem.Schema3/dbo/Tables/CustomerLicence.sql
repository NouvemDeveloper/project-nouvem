﻿CREATE TABLE [dbo].[CustomerLicence] (
    [CustomerLicenceID] INT             IDENTITY (1, 1) NOT NULL,
    [LicenceKey]        NVARCHAR (100)  NOT NULL,
    [Customer]          NVARCHAR (30)   NULL,
    [Email]             NVARCHAR (30)   NULL,
    [PrivateKey]        NVARCHAR (MAX)  NOT NULL,
    [PublicKey]         NVARCHAR (MAX)  NOT NULL,
    [LicenceFile]       VARBINARY (MAX) NOT NULL,
    [Deleted]           BIT             NOT NULL,
    [IssueDate]         DATE            NULL,
    [UpgradeDate]       DATE            NULL,
    CONSTRAINT [PK_CustomerLicence] PRIMARY KEY CLUSTERED ([CustomerLicenceID] ASC)
);


﻿CREATE TABLE [dbo].[NouConfirmation] (
    [NouConfirmationID] INT       IDENTITY (1, 1) NOT NULL,
    [Name]              NCHAR (1) NOT NULL,
    [deleted]           DATETIME  NULL,
    CONSTRAINT [PK_NouConfirmation] PRIMARY KEY CLUSTERED ([NouConfirmationID] ASC)
);


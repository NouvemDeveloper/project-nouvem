﻿CREATE TABLE [dbo].[LabelAssociationLabel] (
    [LabelAssociationLabelID] INT      IDENTITY (1, 1) NOT NULL,
    [LabelAssociationID]      INT      NOT NULL,
    [LabelID]                 INT      NOT NULL,
    [CreationDate]            DATETIME NULL,
    [UserMasterID]            INT      NULL,
    [ItemLabel]               BIT      NULL,
    [BoxLabel]                BIT      NULL,
    [ShippingLabel]           BIT      NULL,
    [PalletLabel]             BIT      NULL,
    [Deleted]                 DATETIME NULL,
    CONSTRAINT [PK_LabelAssociationLabel] PRIMARY KEY CLUSTERED ([LabelAssociationLabelID] ASC),
    CONSTRAINT [FK_LabelAssociationLabel_Label] FOREIGN KEY ([LabelID]) REFERENCES [dbo].[Label] ([LabelID]),
    CONSTRAINT [FK_LabelAssociationLabel_LabelAssociation] FOREIGN KEY ([LabelAssociationID]) REFERENCES [dbo].[LabelAssociation] ([LabelAssociationID]),
    CONSTRAINT [FK_LabelAssociationLabel_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);


﻿CREATE TABLE [dbo].[RegionMaster] (
    [RegionID]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [RegionNumber] INT           NULL,
    [Remark]       NVARCHAR (50) NULL,
    [Deleted]      DATETIME      NULL,
    CONSTRAINT [PK_RegonMaster] PRIMARY KEY CLUSTERED ([RegionID] ASC)
);


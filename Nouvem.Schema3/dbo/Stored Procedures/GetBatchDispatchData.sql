﻿
-- =============================================
-- Author:		brian murray
-- Create date: 24/08/2016
-- Description:	Returns the remaining in stock, batch stock.
-- =============================================
CREATE PROCEDURE [dbo].[GetBatchDispatchData]
	-- Add the parameters for the stored procedure here
	@BatchNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select d.Number, b.Name, s.Product, s.Serial, s.TransactionQTY, s.TransactionWeight, s.Pieces
	  FROM ARDispatch d
	    INNER JOIN ArdispatchDetail dd
		  ON d.ardispatchid = dd.ArdispatchId
		INNER JOIN ViewTRansaction s
		  ON s.MasterTableid = dd.ARDispatchDetailID
		INNER JOIN BPMaster b
		  ON b.BPMasterid = d.BPMasterID_Customer

	 WHERE d.Deleted is null and dd.Deleted is null and s.Deleted is null
	       --AND s.BatchNo = 18384
		   and s.Stocktransactionid in (select stocktransactionid from viewtransaction where batchno = @BatchNo and consumed is null and deleted is null and name = 'Dispatch')

	 order by s.Product
END
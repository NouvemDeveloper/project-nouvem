﻿


-- =============================================
-- Author:		brian murray
-- Create date: 17/06/16
-- Description:	Retrieves the stock transaction data bwtween the input dates.
-- =============================================
CREATE PROCEDURE [dbo].[ReportStockTransactionData] 
	-- Add the parameters for the stored procedure here
	@CustomerID INT,
	@Start DATETIME,
	@End DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF @CustomerID = 0
	BEGIN
	 SELECT 
	   d.ARDispatchID,
	   d.Number as 'DocketNumber',
	   bm.Name as Customer,
	   ing.Name as 'ProductGroup',
	   inm.Code, 
	   inm.Name, 
	   s.Serial,	
	   CONVERT(DECIMAL(18,2),s.Tare) AS Tare,
	   CONVERT(DECIMAL(18,2),s.TransactionWeight) AS TransactionWgt,
	   CONVERT(DECIMAL(18,0),s.TransactionQTY) AS TransactionQty,
	   s.Pieces,
	   (CASE WHEN s.IsBox = 1 THEN 'Yes' ELSE 'No' END) as IsBox,
	   s.TransactionDate,

	   CONVERT(DECIMAL(18,2),dd.UnitPriceAfterDiscount) as Price,
	   bn.Number as 'BatchNumber',
	   (SELECT CONVERT(DECIMAL(18,2),dd.TotalExclVAT / 
	     (SELECT COUNT (*) FROM ARDispatchDetail 
		    INNER JOIN StockTransaction ON ARDispatchDetail.ARDispatchDetailID = StockTransaction.MasterTableID
			 WHERE StockTransaction.MasterTableID = dd.ARDispatchDetailID AND StockTransaction.Deleted IS NULL))) AS Total

	   FROM ARDispatch d
	   INNER JOIN ARDispatchDetail dd ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN StockTransaction s on s.MasterTableID = dd.ARDispatchDetailID
	   INNER JOIN BPMaster bm ON bm.BPMasterID = d.BPMasterID_Customer
	   INNER JOIN INMaster inm ON inm.INMasterID = dd.INMasterID
	   INNER JOIN INGroup ing ON ing.INGroupID = inm.INGroupID	  
	   INNER JOIN NouDocStatus nds ON nds.NouDocStatusID = d.NouDocStatusID
	   LEFT JOIN BatchNumber bn on bn.BatchNumberID = s.BatchNumberID

	   WHERE CONVERT(DATE, d.CreationDate) BETWEEN CONVERT(DATE, @Start) AND CONVERT(DATE,  @End)  
	         AND s.Deleted IS NULL AND nds.Value = 'Complete'

	END
	ELSE
	BEGIN
	SELECT 
	   d.ARDispatchID,
	   d.Number as 'DocketNumber',
	   bm.Name as Customer,
	   ing.Name as 'ProductGroup',
	   inm.Code, 
	   inm.Name, 
	   s.Serial,	
	   CONVERT(DECIMAL(18,2),s.Tare) AS Tare,
	   CONVERT(DECIMAL(18,2),s.TransactionWeight) AS TransactionWgt,
	   CONVERT(DECIMAL(18,0),s.TransactionQTY) AS TransactionQty,
	   s.Pieces,
	   (CASE WHEN s.IsBox = 1 THEN 'Yes' ELSE 'No' END) as IsBox,
	   s.TransactionDate,	   
	   CONVERT(DECIMAL(18,2),dd.UnitPriceAfterDiscount) as Price,
	   bn.Number as 'BatchNumber',
	   (SELECT CONVERT(DECIMAL(18,2),dd.TotalExclVAT / 
	     (SELECT COUNT (*) FROM ARDispatchDetail 
		    INNER JOIN StockTransaction ON ARDispatchDetail.ARDispatchDetailID = StockTransaction.MasterTableID
			 WHERE StockTransaction.MasterTableID = dd.ARDispatchDetailID 
			 AND StockTransaction.Deleted IS NULL))) AS Total


	   FROM ARDispatch d
	   INNER JOIN ARDispatchDetail dd ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN StockTransaction s on s.MasterTableID = dd.ARDispatchDetailID
	   INNER JOIN BPMaster bm ON bm.BPMasterID = d.BPMasterID_Customer
	   INNER JOIN INMaster inm ON inm.INMasterID = dd.INMasterID
	   INNER JOIN INGroup ing ON ing.INGroupID = inm.INGroupID	  
	   INNER JOIN NouDocStatus nds ON nds.NouDocStatusID = d.NouDocStatusID
	   LEFT JOIN BatchNumber bn on bn.BatchNumberID = s.BatchNumberID

	   WHERE CONVERT(DATE, d.CreationDate) BETWEEN CONVERT(DATE, @Start) AND CONVERT(DATE,  @End)  
	         AND bm.BPMasterID = @CustomerID AND s.Deleted IS NULL AND nds.Value = 'Complete'

	END
	  

END
﻿

-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the dispatch summary data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportDispatchDocketSummary] 
	-- Add the parameters for the stored procedure here
	@Start AS DateTime,
	@End AS DateTime,
	@CustomerId as int,
	@StartTime NVARCHAR(20),
	@EndTime NVARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ExactStartTime AS DATETIME = CONVERT(DATETIME, @Start) + CONVERT(TIME, @StartTime)
	DECLARE @ExactEndTime AS DATETIME = CONVERT(DATETIME, @End) + CONVERT(TIME, @EndTime)
	DECLARE @BasePriceListId int = (SELECT PriceListID FROM PriceList WHERE CurrentPriceListName = 'Base Price List')

	If (@CustomerId = 0)
	BEGIN
	  SELECT        
				       
		SUM(dd.WeightDelivered) as WeightDelivered,
		SUM(dd.QuantityDelivered) as Pieces,
		MAX(dd.UnitPriceAfterDiscount) as UnitPriceAfterDiscount,
		AVG(dd.UnitPrice) as UnitPrice,
		SUM(dd.TotalExclVAT) as TotalExclVAT,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		i.INGroupID,
		g.Name AS 'GroupName',		
		CASE WHEN (SELECT NouPriceMethodID FROM PriceListDetail WHERE INMasterID = MAX(i.INMasterID) AND PriceListDetail.PriceListID = @BasePriceListId) = 1 
		THEN 
		CASE WHEN SUM(dd.QuantityDelivered) = 0 THEN 0 ELSE SUM(dd.TotalExclVAT) / SUM(dd.QuantityDelivered) END 
		ELSE 
		CASE WHEN SUM(dd.WeightDelivered) = 0 THEN 0 ELSE SUM(dd.TotalExclVAT) / SUM(dd.WeightDelivered) END 
		END AS 'Avg Price',
	   ((SELECT COUNT(*) 
	     FROM StockTransaction where StockTransaction.MasterTableID in 
	       (select ARDispatchDetailID from ardispatchdetail where ardispatchid in
           (select ARDispatchID from ARDispatch where CONVERT(date,DocumentDate) BETWEEN CONVERT(date,@Start) and CONVERT(date,@End)
            and BPMasterID_Customer = max(d.BPMasterID_Customer) and inmasterid = max(dd.INMasterID))))) as 'Qty',
        ((SELECT SUM(CASE WHEN StockTransaction.IsBox = 1 THEN 1 ELSE 0 END)
	     FROM StockTransaction where StockTransaction.MasterTableID in 
	       (select ARDispatchDetailID from ardispatchdetail where ardispatchid in
           (select ARDispatchID from ARDispatch where CONVERT(date,DocumentDate) BETWEEN CONVERT(date,@Start) and CONVERT(date,@End)
            and BPMasterID_Customer = max(d.BPMasterID_Customer) and inmasterid = max(dd.INMasterID))))) as 'Box Qty'

    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN BPMaster b
	      ON d.BPMasterID_Customer = b.BPMasterID
	   INNER JOIN NouDocStatus nds
	      ON nds.NouDocStatusID = d.NouDocStatusID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID	
	   INNER JOIN INGroup g
	      ON g.INGroupID = i.INGroupID
       
     WHERE nds.Value = 'Complete' 
	   AND d.CreationDate BETWEEN @ExactStartTime AND @ExactEndTime  
	    AND dd.Deleted IS NULL

	   GROUP BY i.Code,i.Name,i.INGroupID,g.Name

    END
	ELSE
	BEGIN
	SELECT        
				       
		SUM(dd.WeightDelivered) as WeightDelivered,
		SUM(dd.QuantityDelivered) as Pieces,
		MAX(dd.UnitPriceAfterDiscount) as UnitPriceAfterDiscount,
		AVG(dd.UnitPrice) as UnitPrice,
		SUM(dd.TotalExclVAT) as TotalExclVAT,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		i.INGroupID,
		g.Name AS 'GroupName',
		b.BPMasterID,
		b.Name,
       CASE WHEN (SELECT NouPriceMethodID FROM PriceListDetail WHERE INMasterID = MAX(i.INMasterID) AND PriceListDetail.PriceListID = @BasePriceListId) = 1 
		THEN 
		CASE WHEN SUM(dd.QuantityDelivered) = 0 THEN 0 ELSE SUM(dd.TotalExclVAT) / SUM(dd.QuantityDelivered) END 
		ELSE 
		CASE WHEN SUM(dd.WeightDelivered) = 0 THEN 0 ELSE SUM(dd.TotalExclVAT) / SUM(dd.WeightDelivered) END 
		END AS 'Avg Price',
	   ((SELECT COUNT(*) 
	     FROM StockTransaction where StockTransaction.MasterTableID in 
	       (select ARDispatchDetailID from ardispatchdetail where ardispatchid in
           (select ARDispatchID from ARDispatch where CONVERT(date,DocumentDate) BETWEEN CONVERT(date,@Start) and CONVERT(date,@End)
            and BPMasterID_Customer = max(d.BPMasterID_Customer) and inmasterid = max(dd.INMasterID))))) as 'Qty',
        ((SELECT SUM(CASE WHEN StockTransaction.IsBox = 1 THEN 1 ELSE 0 END)
	     FROM StockTransaction where StockTransaction.MasterTableID in 
	       (select ARDispatchDetailID from ardispatchdetail where ardispatchid in
           (select ARDispatchID from ARDispatch where CONVERT(date,DocumentDate) BETWEEN CONVERT(date,@Start) and CONVERT(date,@End)
            and BPMasterID_Customer = max(d.BPMasterID_Customer) and inmasterid = max(dd.INMasterID))))) as 'Box Qty'

    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN BPMaster b
	      ON d.BPMasterID_Customer = b.BPMasterID
	   INNER JOIN NouDocStatus nds
	      ON nds.NouDocStatusID = d.NouDocStatusID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID	
	   INNER JOIN INGroup g
	      ON g.INGroupID = i.INGroupID
       
     WHERE nds.Value = 'Complete' 
	   AND d.CreationDate BETWEEN @ExactStartTime AND @ExactEndTime
	   AND b.BPMasterID = @CustomerId
	 
	   AND dd.Deleted IS NULL

	   GROUP BY i.Code,i.Name,i.INGroupID,g.Name,b.BPMasterID, b.Name

	END

    

     END
﻿-- =============================================
-- Author:		brian murray
-- Create date: 12/04/2016
-- Description:	Populate the default spec with all products.
-- =============================================
CREATE PROCEDURE PopulateDefaultSpec
	-- Add the parameters for the stored procedure here
	@PRSpecID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT *
	INTO   #Temp
	FROM  INMaster

	DECLARE @Id INT

	WHILE EXISTS(SELECT * FROM #Temp)
	Begin

		Select Top 1 @Id = INMasterID From #Temp

		--Do some processing here
		INSERT INTO PRSpecOutput VALUES(@PRSpecID,@Id, 1,NULL)
		DELETE #Temp WHERE INMasterID = @Id
    END
END

﻿
-- =============================================
-- Author:		<James Harrison>
-- Create date: <11/10/2016>
-- Description:	<Deleted Label Report>
-- =============================================
CREATE PROCEDURE [dbo].[ReportDeletedLabel]
	-- Add the parameters for the stored procedure here
	@startdate date,
	@enddate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 
 st.StockTransactionID,
 um.FullName as 'Operator Name',
 inm.Name,
 st.deleted,
 st.pieces,
 st.TransactionQTY,
 st.TransactionWeight,
 pro.Reference,
 lbld.UserMasterID,
 lblum.FullName,
 ISNULL(st.StockTransactionID_Container,0) as StockTransactionID_Container,
 case when st.IsBox = 1 then st.StockTransactionID else ISNULL(st.StockTransactionID_Container,0) end as 'GroupId'


 from LabelDeletion lbld 
 inner join StockTransaction st on lbld.StockTransactionID = st.StockTransactionID
 inner join INMaster inm on st.INMasterID = inm.INMasterID
 inner join PROrder pro on st.BatchNumberID = pro.BatchNumberID
 left join UserMaster lblum on lbld.UserMasterID = lblum.UserMasterID
 left join UserMaster um on st.UserMasterID = um.UserMasterID

 where st.Deleted is not null and CONVERT (date,lbld.DeletionTime) between @startdate and @enddate

END
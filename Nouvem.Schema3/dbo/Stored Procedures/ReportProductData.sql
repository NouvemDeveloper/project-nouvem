﻿

-- =============================================
-- Author:		brian murray
-- Create date: 17/06/16
-- Description:	Retrieves the product data bwtween the input dates.
-- =============================================
CREATE PROCEDURE [dbo].[ReportProductData] 
	-- Add the parameters for the stored procedure here
	@CustomerID INT,
	@Start DATETIME,
	@End DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @CustomerID = 0
	BEGIN
	 SELECT 
	   
	   bm.Name as Customer,
	   ing.Name as 'Group',
	   inm.Code, 
	   inm.Name, 
	   SUM(dd.QuantityDelivered) as QuantityDelivered, 
	   SUM(dd.WeightDelivered) as WeightDelivered, 
	   SUM(dd.TotalExclVAT) as Total,
	   CONVERT(DECIMAL(18,2),AVG(dd.UnitPriceAfterDiscount)) as AveragePrice

	   FROM ARDispatch d
	   INNER JOIN ARDispatchDetail dd ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN BPMaster bm ON bm.BPMasterID = d.BPMasterID_Customer
	   INNER JOIN INMaster inm ON inm.INMasterID = dd.INMasterID
	   INNER JOIN INGroup ing ON ing.INGroupID = inm.INGroupID	
	   INNER JOIN NouDocStatus nds ON nds.NouDocStatusID = d.NouDocStatusID

	   WHERE CONVERT(DATE, d.DeliveryDate) BETWEEN CONVERT(DATE, @Start) AND CONVERT(DATE,  @End)
	         AND nds.Value = 'Complete'
	         

	   GROUP BY bm.Name, ing.Name, inm.Code, inm.Name
	   

	END
	ELSE
	BEGIN
	SELECT 
	   
	   bm.Name as Customer,
	   ing.Name as 'Group',
	   inm.Code, 
	   inm.Name, 
	   SUM(dd.QuantityDelivered) as QuantityDelivered, 
	   SUM(dd.WeightDelivered) as WeightDelivered, 
	   SUM(dd.TotalExclVAT) as Total,
	   CONVERT(DECIMAL(18,2),AVG(dd.UnitPriceAfterDiscount))

	   FROM ARDispatch d
	   INNER JOIN ARDispatchDetail dd ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN BPMaster bm ON bm.BPMasterID = d.BPMasterID_Customer
	   INNER JOIN INMaster inm ON inm.INMasterID = dd.INMasterID
	   INNER JOIN INGroup ing ON ing.INGroupID = inm.INGroupID	  
	   INNER JOIN NouDocStatus nds ON nds.NouDocStatusID = d.NouDocStatusID

	   WHERE CONVERT(DATE, d.DeliveryDate) 
	         BETWEEN CONVERT(DATE, @Start) AND CONVERT(DATE,  @End)
			 AND bm.BPMasterID = @CustomerID AND nds.Value = 'Complete'

	   GROUP BY bm.Name, ing.Name, inm.Code, inm.Name

	END
	  

END
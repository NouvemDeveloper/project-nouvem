﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[SplitPlant] 
(
	-- Add the parameters for the function here
	@Plant nvarchar(100)
)
RETURNS nvarchar(100)
AS
BEGIN

 DECLARE @Result nvarchar(100) 
 DECLARE @NewLine CHAR(2) = CHAR(13) + CHAR(10)
Set @Result = SUBSTRING(@Plant,CHARINDEX(@NewLine,@Plant)+2,LEN(@Plant))

	-- Return the result of the function
	RETURN @Result

END
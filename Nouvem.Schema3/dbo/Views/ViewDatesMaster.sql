﻿






 CREATE view [dbo].[ViewDatesMaster]
  as
  SELECT
  d.DateMasterID, 
  d.DateCode, 
  d.DateDescription, 
  n.NouDateTypeID,
  n.DateType, 
  g.GS1AIMasterID,
  ISNULL(g.AI, '') as Identifier, 
  ISNULL(g.OfficialDescription, '') as OfficialDescription, 
  ISNULL(g.RelatedDescription, '') as RelatedDescription,
  ISNULL(g.DataLength, '') as [DataLength],
  ISNULL(g.AI, '') + '  ' + ISNULL(g.OfficialDescription, '') as VisualDisplay,
  CONVERT(BIT, 0) as Batch,
  CONVERT(BIT, 0) as [Transaction],
  CONVERT(BIT, 0) as [Required],
  CONVERT(BIT, 0) as [Reset]


  FROM DateMaster d 
      inner join NouDateType n on n.NouDateTypeID = d.NouDateTypeID 
	  left join Gs1AIMaster g on g.GS1AIMasterID = d.GS1AIMasterID 

  WHERE d.Deleted = 0
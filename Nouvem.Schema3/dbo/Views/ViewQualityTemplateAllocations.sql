﻿


  CREATE View [dbo].[ViewQualityTemplateAllocations]
  AS

  SELECT 
         qtm.Name, 
		 qt.QualityTemplateNameID, 
		 qt.QualityMasterID, 
		 qt.Batch,
		 qt.[Transaction],
		 qt.[Required],
		 qt.[Reset],
		 qm.QualityCode, 
		 qm.QualityDescription,
		 n.QualityType
  
  FROM QualityTemplateAllocation qt 
      inner join QualityMaster qm on qt.QualityMasterID = qm.QualityMasterID
	  inner join QualityTemplateName qtm on qt.QualityTemplateNameID = qtm.QualityTemplateNameID
	  Inner join NouQualityType n on qm.NouQualityTypeID = n.NouQualityTypeID

  WHERE qt.Deleted = 0
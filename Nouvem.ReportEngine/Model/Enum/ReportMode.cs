﻿// -----------------------------------------------------------------------
// <copyright file="ReportMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ReportEngine.Model.Enum
{
    public enum ReportMode
    {
        /// <summary>
        /// The print mode.
        /// </summary>
        Print,

        /// <summary>
        /// The preview mode.
        /// </summary>
        Preview
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ReportType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ReportEngine.Model.Enum
{
    public enum ReportType
    {
        /// <summary>
        /// The business partners report.
        /// </summary>
        BusinessPartners
    }
}

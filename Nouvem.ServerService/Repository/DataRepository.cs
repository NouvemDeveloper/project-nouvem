﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.ServerService.Repository
{
    public class DataRepository
    {
        public string CheckDispatchDockets()
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.CheckDispatchDockets().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw;
                //this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            //return string.Empty;
        }
    }
}

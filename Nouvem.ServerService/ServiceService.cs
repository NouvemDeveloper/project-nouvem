﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Nouvem.ServerService.Repository;


namespace Nouvem.ServerService
{
    public partial class ServiceService : ServiceBase
    {
        private DataRepository repository = new DataRepository();

        public ServiceService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var result = this.repository.CheckDispatchDockets();
            File.WriteAllText(@"C:\Nouvem\Test.txt", result);
        }

        protected override void OnStop()
        {
        }
    }
}

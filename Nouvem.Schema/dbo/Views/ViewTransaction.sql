﻿




CREATE VIEW [dbo].[ViewTransaction]
AS
SELECT         dbo.StockTransaction.TransactionDate,dbo.StockTransaction.MasterTableID, dbo.StockTransaction.StockTransactionID, dbo.StockTransaction.InLocation, dbo.StockTransaction.StockTransactionID_Container, dbo.BatchNumber.Number AS BatchNo, dbo.StockTransaction.Serial, dbo.Warehouse.Name, dbo.Warehouse.StockLocation, dbo.INMaster.Name AS Product, 
                         dbo.NouTransactionType.Description, dbo.StockTransaction.TransactionQTY, dbo.StockTransaction.TransactionWeight, dbo.StockTransaction.Tare, dbo.StockTransaction.Pieces, dbo.StockTransaction.Consumed, 
                         dbo.StockTransaction.Deleted, dbo.StockTransaction.IsBox, dbo.StockTransaction.StockTransactionID_Container AS 'BoxContainerID',dbo.StockTransaction.WarehouseID
FROM            dbo.StockTransaction INNER JOIN
                         dbo.Warehouse ON dbo.StockTransaction.WarehouseID = dbo.Warehouse.WarehouseID INNER JOIN
                         dbo.NouTransactionType ON dbo.StockTransaction.NouTransactionTypeID = dbo.NouTransactionType.NouTransactionTypeID INNER JOIN
                         dbo.BatchNumber ON dbo.StockTransaction.BatchNumberID = dbo.BatchNumber.BatchNumberID INNER JOIN
                         dbo.INMaster ON dbo.StockTransaction.INMasterID = dbo.INMaster.INMasterID


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[56] 4[5] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "StockTransaction"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 211
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 15
         End
         Begin Table = "Warehouse"
            Begin Extent = 
               Top = 6
               Left = 287
               Bottom = 136
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "NouTransactionType"
            Begin Extent = 
               Top = 6
               Left = 500
               Bottom = 136
               Right = 711
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "BatchNumber"
            Begin Extent = 
               Top = 6
               Left = 749
               Bottom = 136
               Right = 967
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "INMaster"
            Begin Extent = 
               Top = 138
               Left = 287
               Bottom = 268
               Right = 530
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or =', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ViewTransaction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ViewTransaction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ViewTransaction';


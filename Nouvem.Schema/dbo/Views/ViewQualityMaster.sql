﻿

CREATE view [dbo].[ViewQualityMaster]
  as
  SELECT
  q.QualityMasterID, 
  q.QualityCode, 
  q.QualityDescription, 
  q.SQL,
  q.Collection,
  n.NouQualityTypeID,
  n.QualityType, 
  g.GS1AIMasterID,
  ISNULL(g.AI, '') as Identifier, 
  ISNULL(g.OfficialDescription, '') as OfficialDescription, 
  ISNULL(g.RelatedDescription, '') as RelatedDescription,
  ISNULL(g.DataLength, '') as [DataLength],
  ISNULL(g.AI, '') + '  ' + ISNULL(g.OfficialDescription, '') as VisualDisplay,
  CONVERT(BIT, 0) as Batch,
  CONVERT(BIT, 0) as [Transaction],
  CONVERT(BIT, 0) as [Required],
  CONVERT(BIT, 0) as [Reset]

  

  FROM QualityMaster q
      inner join NouQualityType n on n.NouQualityTypeID = q.NouQualityTypeID 
	  left join Gs1AIMaster g on g.GS1AIMasterID = q.GS1AIMasterID 

  WHERE q.Deleted = 0





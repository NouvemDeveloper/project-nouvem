﻿CREATE TABLE [dbo].[BPMaster] (
    [BPMasterID]     INT             IDENTITY (1, 1) NOT NULL,
    [NouBPTypeID]    INT             NOT NULL,
    [Code]           VARCHAR (30)    NOT NULL,
    [Name]           NVARCHAR (100)  NULL,
    [BPCurrencyID]   INT             NULL,
    [BPGroupID]      INT             NULL,
    [PLMasterID]     INT             NULL,
    [PriceListID]    INT             NULL,
    [Remarks]        NVARCHAR (500)  NULL,
    [Notes]          NVARCHAR (250)  NULL,
    [PopUpNotes]     NVARCHAR (250)  NULL,
    [Tel]            NVARCHAR (50)   NULL,
    [FAX]            NVARCHAR (50)   NULL,
    [Web]            NVARCHAR (50)   NULL,
    [VATNo]          NVARCHAR (50)   NULL,
    [CompanyNo]      NVARCHAR (50)   NULL,
    [Uplift]         INT             NULL,
    [CreditLimit]    INT             NULL,
    [PORequired]     BIT             NULL,
    [Balance]        DECIMAL (18, 2) NULL,
    [RouteMasterID]  INT             NULL,
    [OnHold]         BIT             NULL,
    [ActiveFrom]     DATE            NULL,
    [ActiveTo]       DATE            NULL,
    [InActiveFrom]   DATE            NULL,
    [InActiveTo]     DATE            NULL,
    [Deleted]        DATETIME        NULL,
    [UserMasterID]   INT             NULL,
    [CreationDate]   DATETIME        NULL,
    [Email]          NVARCHAR (50)   NULL,
    [BPLabelFieldID] INT             NULL,
    [EditDate]       DATETIME        NULL,
    [DeviceID]       INT             NULL,
    CONSTRAINT [PK_BusinessPartner] PRIMARY KEY CLUSTERED ([BPMasterID] ASC),
    CONSTRAINT [FK_BPMaster_BPLabelField] FOREIGN KEY ([BPLabelFieldID]) REFERENCES [dbo].[BPLabelField] ([BPLabelFieldID]),
    CONSTRAINT [FK_BPMaster_DeviceMaster] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_BPMaster_PriceList] FOREIGN KEY ([PriceListID]) REFERENCES [dbo].[PriceList] ([PriceListID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_BPMaster_RouteMaster] FOREIGN KEY ([RouteMasterID]) REFERENCES [dbo].[RouteMaster] ([RouteID]),
    CONSTRAINT [FK_BPMaster_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_BPPropertyNo_BPPropertyNo] FOREIGN KEY ([NouBPTypeID]) REFERENCES [dbo].[NouBPType] ([NouBPTypeID]),
    CONSTRAINT [FK_BusinessPartner_BPGroup] FOREIGN KEY ([BPGroupID]) REFERENCES [dbo].[BPGroup] ([BPGroupID]),
    CONSTRAINT [FK_BusinessPartner_Currency] FOREIGN KEY ([BPCurrencyID]) REFERENCES [dbo].[BPCurrency] ([BPCurrencyID]),
    CONSTRAINT [IX_BPMaster] UNIQUE NONCLUSTERED ([Code] ASC)
);












GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FK_BPPropertyNo_BPPropertyNo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BPMaster', @level2type = N'CONSTRAINT', @level2name = N'FK_BPPropertyNo_BPPropertyNo';


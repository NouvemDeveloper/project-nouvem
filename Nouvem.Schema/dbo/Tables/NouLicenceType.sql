﻿CREATE TABLE [dbo].[NouLicenceType] (
    [NouLicenceTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [LicenceName]      NVARCHAR (50) NOT NULL,
    [Deleted]          BIT           NOT NULL,
    [IsDevice]         BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LicenceType] PRIMARY KEY CLUSTERED ([NouLicenceTypeID] ASC)
);


﻿CREATE TABLE [dbo].[APOrder] (
    [APOrderID]                    INT             IDENTITY (1, 1) NOT NULL,
    [DocumentNumberingID]          INT             NOT NULL,
    [Number]                       INT             NOT NULL,
    [BPMasterSnapshotID_Supplier]  INT             NULL,
    [BPMasterSnapshotID_Haulier]   INT             NULL,
    [BPAddressSnapshotID_Invoice]  INT             NULL,
    [BPAddressSnapshotID_Delivery] INT             NULL,
    [DeliveryDate]                 DATETIME        NULL,
    [DocumentDate]                 DATETIME        NULL,
    [CreationDate]                 DATETIME        NULL,
    [Printed]                      BIT             NULL,
    [NouDocStatusID]               INT             NULL,
    [Remarks]                      NCHAR (10)      NULL,
    [TotalExVAT]                   DECIMAL (18, 2) NULL,
    [VAT]                          DECIMAL (18, 2) NULL,
    [SubTotalExVAT]                DECIMAL (18, 2) NULL,
    [DiscountIncVAT]               DECIMAL (18, 2) NULL,
    [DiscountPercentage]           FLOAT (53)      NULL,
    [GrandTotalIncVAT]             DECIMAL (18, 2) NULL,
    [UserMasterID]                 INT             NULL,
    [BPAddressID_Delivery]         INT             NULL,
    [BPAddressID_Invoice]          INT             NULL,
    [BPMasterID_Haulier]           INT             NULL,
    [BPMasterID_Supplier]          INT             NULL,
    [QuoteValidDate]               DATETIME        NULL,
    [BPContactID]                  INT             NULL,
    [Deleted]                      DATETIME        NULL,
    [BaseDocumentReferenceID]      INT             NULL,
    [DeviceID]                     INT             NULL,
    [EditDate]                     DATETIME        NULL,
    CONSTRAINT [PK_APOrder] PRIMARY KEY CLUSTERED ([APOrderID] ASC),
    CONSTRAINT [FK_APOrder_BPAddress] FOREIGN KEY ([BPAddressID_Delivery]) REFERENCES [dbo].[BPAddress] ([BPAddressID]),
    CONSTRAINT [FK_APOrder_BPAddress1] FOREIGN KEY ([BPAddressID_Invoice]) REFERENCES [dbo].[BPAddress] ([BPAddressID]),
    CONSTRAINT [FK_APOrder_BPAddressSnapshot] FOREIGN KEY ([BPAddressSnapshotID_Delivery]) REFERENCES [dbo].[BPAddressSnapshot] ([BPAddressSnapshotID]),
    CONSTRAINT [FK_APOrder_BPAddressSnapshot1] FOREIGN KEY ([BPAddressSnapshotID_Invoice]) REFERENCES [dbo].[BPAddressSnapshot] ([BPAddressSnapshotID]),
    CONSTRAINT [FK_APOrder_BPContact] FOREIGN KEY ([BPContactID]) REFERENCES [dbo].[BPContact] ([BPContactID]),
    CONSTRAINT [FK_APOrder_BPMaster] FOREIGN KEY ([BPMasterID_Supplier]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_APOrder_BPMaster1] FOREIGN KEY ([BPMasterID_Haulier]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_APOrder_BPMasterSnapshot] FOREIGN KEY ([BPMasterSnapshotID_Supplier]) REFERENCES [dbo].[BPMasterSnapshot] ([BPMasterSnapshotID]),
    CONSTRAINT [FK_APOrder_BPMasterSnapshot1] FOREIGN KEY ([BPMasterSnapshotID_Haulier]) REFERENCES [dbo].[BPMasterSnapshot] ([BPMasterSnapshotID]),
    CONSTRAINT [FK_APOrder_DeviceMaster] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_APOrder_DocumentNumbering] FOREIGN KEY ([DocumentNumberingID]) REFERENCES [dbo].[DocumentNumbering] ([DocumentNumberingID]),
    CONSTRAINT [FK_APOrder_NouDocStatus] FOREIGN KEY ([NouDocStatusID]) REFERENCES [dbo].[NouDocStatus] ([NouDocStatusID]),
    CONSTRAINT [FK_APOrder_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);








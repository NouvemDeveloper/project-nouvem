﻿CREATE TABLE [dbo].[Concession] (
    [ConcessionID]       INT            IDENTITY (1, 1) NOT NULL,
    [ARDispatchDetailID] INT            NOT NULL,
    [INMasterID]         INT            NOT NULL,
    [Reason]             NVARCHAR (500) NOT NULL,
    [UserMasterID]       INT            NULL,
    [DeviceMasterID]     INT            NULL,
    [Deleted]            DATETIME       NULL,
    [ConcessionTime]     DATETIME       NULL,
    [Reference]          NVARCHAR (100) NULL,
    CONSTRAINT [PK_Concession] PRIMARY KEY CLUSTERED ([ConcessionID] ASC),
    CONSTRAINT [FK_Concession_ARDispatchDetail] FOREIGN KEY ([ARDispatchDetailID]) REFERENCES [dbo].[ARDispatchDetail] ([ARDispatchDetailID])
);






﻿CREATE TABLE [dbo].[BPLabelField] (
    [BPLabelFieldID] INT            IDENTITY (1, 1) NOT NULL,
    [Field1]         NVARCHAR (100) NULL,
    [Field2]         NVARCHAR (100) NULL,
    [Field3]         NVARCHAR (100) NULL,
    [Field4]         NVARCHAR (100) NULL,
    [Field5]         NVARCHAR (100) NULL,
    [Field6]         NVARCHAR (100) NULL,
    [Field7]         NVARCHAR (100) NULL,
    [Field8]         NVARCHAR (100) NULL,
    [Field9]         NVARCHAR (100) NULL,
    [Field10]        NVARCHAR (100) NULL,
    [Field11]        NVARCHAR (100) NULL,
    [Field12]        NVARCHAR (100) NULL,
    [Field13]        NVARCHAR (100) NULL,
    [Field14]        NVARCHAR (100) NULL,
    [Field15]        NVARCHAR (100) NULL,
    [Deleted]        DATETIME       NULL,
    CONSTRAINT [PK_BPLabelField] PRIMARY KEY CLUSTERED ([BPLabelFieldID] ASC)
);


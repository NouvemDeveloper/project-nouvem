CREATE TABLE [dbo].[ARDispatchDetail] (
    [ARDispatchDetailID]     INT             IDENTITY (1, 1) NOT NULL,
    [ARDispatchID]           INT             NOT NULL,
    [INMasterID]             INT             NOT NULL,
    [INMasterSnapshotID]     INT             NOT NULL,
    [QuantityOrdered]        FLOAT (53)      NULL,
    [WeightOrdered]          FLOAT (53)      NULL,
    [QuantityDelivered]      FLOAT (53)      NULL,
    [WeightDelivered]        FLOAT (53)      NULL,
    [UnitPrice]              DECIMAL (18, 2) NULL,
    [DiscountPercentage]     FLOAT (53)      NULL,
    [DiscountAmount]         DECIMAL (18, 2) NULL,
    [DiscountPrice]          DECIMAL (18, 2) NULL,
    [LineDiscountPercentage] DECIMAL (18, 2) NULL,
    [LineDiscountAmount]     DECIMAL (18, 2) NULL,
    [UnitPriceAfterDiscount] DECIMAL (18, 2) NULL,
    [VAT]                    DECIMAL (18, 2) NULL,
    [TotalIncVAT]            DECIMAL (18, 2) NULL,
    [TotalExclVAT]           DECIMAL (18, 2) NULL,
    [VATCodeID]              INT             NULL,
    [AROrderDetailID]        INT             NULL,
    [BatchNumberID]          INT             NULL,
    [Deleted]                DATETIME        NULL,
    [PriceListID]            INT             NULL,
    [Comments]               NVARCHAR (500)  NULL,
    CONSTRAINT [PK_ARDispatchDetail] PRIMARY KEY CLUSTERED ([ARDispatchDetailID] ASC),
    CONSTRAINT [FK_ARDispatchDetail_ARDispatch] FOREIGN KEY ([ARDispatchID]) REFERENCES [dbo].[ARDispatch] ([ARDispatchID]),
    CONSTRAINT [FK_ARDispatchDetail_ARDispatchDetail] FOREIGN KEY ([ARDispatchDetailID]) REFERENCES [dbo].[ARDispatchDetail] ([ARDispatchDetailID]),
    CONSTRAINT [FK_ARDispatchDetail_AROrderDetail] FOREIGN KEY ([AROrderDetailID]) REFERENCES [dbo].[AROrderDetail] ([AROrderDetailID]),
    CONSTRAINT [FK_ARDispatchDetail_BatchNumber] FOREIGN KEY ([BatchNumberID]) REFERENCES [dbo].[BatchNumber] ([BatchNumberID]),
    CONSTRAINT [FK_ARDispatchDetail_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_ARDispatchDetail_INMasterSnapshot] FOREIGN KEY ([INMasterSnapshotID]) REFERENCES [dbo].[INMasterSnapshot] ([INMasterSnapshotID]),
    CONSTRAINT [FK_ARDispatchDetail_PriceList] FOREIGN KEY ([PriceListID]) REFERENCES [dbo].[PriceList] ([PriceListID]),
    CONSTRAINT [FK_ARDispatchDetail_VATCode] FOREIGN KEY ([VATCodeID]) REFERENCES [dbo].[VATCode] ([VATCodeID])
);










GO

-- =============================================
-- Author:		brian murray
-- Create date: 09/09/16
-- Description:	Fail safe, to ensure the line and header totals match,
-- =============================================
CREATE TRIGGER [dbo].[Trigger_ARDispatchDetail_ForInsertUpdate]
   ON [dbo].[ARDispatchDetail]
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @HeaderTotal DECIMAL(18,2) 
	DECLARE @LinesTotal DECIMAL(18,2)
    DECLARE @ARDispatchID int
	DECLARE @DeviceID int

	SELECT @ARDispatchID = ARDispatchID FROM inserted
	SELECT @HeaderTotal = SubTotalExVat, @DeviceID = DeviceID FROM ARDispatch WHERE ARDispatchID = @ARDispatchID
	SELECT @LinesTotal = (SELECT SUM(TotalExclVAT) FROM ARDispatchDetail WHERE ARDispatchID = @ARDispatchID AND Deleted IS NULL)

	IF (@HeaderTotal <> @LinesTotal)
	BEGIN
	UPDATE ARDispatch SET SubTotalExVAT = @LinesTotal, GrandTotalIncVAT = @LinesTotal WHERE ARDispatchID = @ARDispatchID
	INSERT INTO Log_DispatchError VALUES(@ARDispatchID,@DeviceID,@HeaderTotal, @LinesTotal,GETDATE(),NULL)
	END

END
﻿CREATE TABLE [dbo].[INMasterSnapshot] (
    [INMasterSnapshotID] INT             IDENTITY (1, 1) NOT NULL,
    [INMasterID]         INT             NOT NULL,
    [Code]               NVARCHAR (30)   NOT NULL,
    [Name]               NVARCHAR (100)  NOT NULL,
    [MinWeight]          DECIMAL (18, 5) NULL,
    [MaxWeight]          DECIMAL (18, 5) NULL,
    [NominalWeight]      DECIMAL (18, 5) NULL,
    [TypicalPieces]      INT             NULL,
    CONSTRAINT [PK_InvoiceDetailINMasterID] PRIMARY KEY CLUSTERED ([INMasterSnapshotID] ASC)
);














﻿CREATE TABLE [dbo].[Log_DispatchError] (
    [Log_DispatchErrorID] INT             IDENTITY (1, 1) NOT NULL,
    [ARDispatchID]        INT             NULL,
    [DeviceID]            INT             NULL,
    [BeforeTotal]         DECIMAL (18, 2) NULL,
    [AfterTotal]          DECIMAL (18, 2) NULL,
    [ErrorDate]           DATETIME        NULL,
    [Deleted]             DATETIME        NULL,
    CONSTRAINT [PK_Log_DispatchError] PRIMARY KEY CLUSTERED ([Log_DispatchErrorID] ASC)
);


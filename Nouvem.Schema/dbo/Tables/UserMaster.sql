﻿CREATE TABLE [dbo].[UserMaster] (
    [UserMasterID]         INT             IDENTITY (1, 1) NOT NULL,
    [UserName]             NVARCHAR (50)   NOT NULL,
    [FullName]             VARCHAR (50)    NOT NULL,
    [Email]                VARCHAR (50)    NULL,
    [Mobile]               NVARCHAR (50)   NULL,
    [UserGroupID]          INT             NOT NULL,
    [ChangeAtNextLogin]    BIT             NULL,
    [PasswordNeverExpires] BIT             NULL,
    [SuspendUser]          BIT             NULL,
    [ActiveFrom]           DATE            NULL,
    [ActiveTo]             DATE            NULL,
    [InActiveFrom]         DATE            NULL,
    [InActiveTo]           DATE            NULL,
    [LicenceDetailID]      INT             NULL,
    [Password]             VARBINARY (50)  NULL,
    [Photo]                VARBINARY (MAX) NULL,
    [UserSettingsID]       INT             NULL,
    [EditDate]             DATETIME        NULL,
    [DeviceID]             INT             NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserMasterID] ASC),
    CONSTRAINT [FK_UserMaster_DeviceMaster] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_UserMaster_LicenceDetail] FOREIGN KEY ([LicenceDetailID]) REFERENCES [dbo].[LicenceDetail] ([LicenceDetailID]),
    CONSTRAINT [FK_UserMaster_UserGroup ] FOREIGN KEY ([UserGroupID]) REFERENCES [dbo].[UserGroup ] ([UserGroupID])
);








GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_UserName]
    ON [dbo].[UserMaster]([UserName] ASC);


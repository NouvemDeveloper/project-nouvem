﻿CREATE TABLE [dbo].[UserGroupRule] (
    [UserGroupRuleID]         INT IDENTITY (1, 1) NOT NULL,
    [UserGroupID]             INT NOT NULL,
    [NouAuthorisationListtID] INT NOT NULL,
    [NouAuthorisationValueID] INT NOT NULL,
    [Deleted]                 BIT NOT NULL,
    CONSTRAINT [PK_UserGroupMaster] PRIMARY KEY CLUSTERED ([UserGroupRuleID] ASC),
    CONSTRAINT [FK_UserGroupRule_SysAutherisationValue1] FOREIGN KEY ([NouAuthorisationValueID]) REFERENCES [dbo].[NouAuthorisationValue] ([NouAuthorisationValueID]),
    CONSTRAINT [FK_UserGroupRule_UserGroup ] FOREIGN KEY ([UserGroupID]) REFERENCES [dbo].[UserGroup ] ([UserGroupID]),
    CONSTRAINT [FK_UserGroupRules_SysAuthorisationList] FOREIGN KEY ([NouAuthorisationListtID]) REFERENCES [dbo].[NouAuthorisation] ([NouAuthorisationID])
);


﻿CREATE TABLE [dbo].[StockTransaction] (
    [StockTransactionID]           INT             IDENTITY (1, 1) NOT NULL,
    [MasterTableID]                INT             NULL,
    [BatchNumberID]                INT             NULL,
    [Serial]                       INT             NULL,
    [TransactionDate]              DATETIME2 (7)   NOT NULL,
    [NouTransactionTypeID]         INT             NOT NULL,
    [TransactionQTY]               DECIMAL (18, 5) NULL,
    [TransactionWeight]            DECIMAL (18, 5) NULL,
    [GrossWeight]                  DECIMAL (18, 5) NULL,
    [Tare]                         DECIMAL (18, 5) NULL,
    [Alibi]                        INT             NULL,
    [Pieces]                       INT             NULL,
    [WarehouseID]                  INT             NOT NULL,
    [LocationID]                   INT             NULL,
    [INMasterID]                   INT             NOT NULL,
    [ContainerID]                  INT             NULL,
    [Consumed]                     DATETIME        NULL,
    [ManualWeight]                 BIT             NOT NULL,
    [DeviceMasterID]               INT             CONSTRAINT [DF__StockTran__UserM__195694DD] DEFAULT ((1)) NULL,
    [UserMasterID]                 INT             NULL,
    [IsBox]                        BIT             NULL,
    [StockTransactionID_Container] INT             NULL,
    [LabelID]                      VARCHAR (100)   NULL,
    [Reference]                    INT             NULL,
    [CanAddToBox]                  BIT             NULL,
    [Deleted]                      DATETIME        NULL,
    [InLocation]                   BIT             NULL,
    [Comments]                     NVARCHAR (500)  NULL,
    [StoredLabelID]                INT             NULL,
    [BPMasterID]                   INT             NULL,
    CONSTRAINT [PK_StockITransaction] PRIMARY KEY CLUSTERED ([StockTransactionID] ASC),
    CONSTRAINT [FK_StockTransaction_BatchNumber] FOREIGN KEY ([BatchNumberID]) REFERENCES [dbo].[BatchNumber] ([BatchNumberID]),
    CONSTRAINT [FK_StockTransaction_DeviceMaster] FOREIGN KEY ([DeviceMasterID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_StockTransaction_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_StockTransaction_StockTransaction] FOREIGN KEY ([StockTransactionID_Container]) REFERENCES [dbo].[StockTransaction] ([StockTransactionID]),
    CONSTRAINT [FK_StockTransaction_StockTransaction1] FOREIGN KEY ([StockTransactionID]) REFERENCES [dbo].[StockTransaction] ([StockTransactionID]),
    CONSTRAINT [FK_StockTransaction_StoredLabel] FOREIGN KEY ([StoredLabelID]) REFERENCES [dbo].[StoredLabel] ([StoredLabelID]),
    CONSTRAINT [FK_StockTransaction_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID]),
    CONSTRAINT [FK_StockTransaction_Warehouse] FOREIGN KEY ([WarehouseID]) REFERENCES [dbo].[Warehouse] ([WarehouseID]),
    CONSTRAINT [FK_StockTransaction_WarehouseLocation] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[WarehouseLocation] ([WarehouseLocationID])
);
























GO



GO
CREATE NONCLUSTERED INDEX [IX_Serial]
    ON [dbo].[StockTransaction]([Serial] ASC);


GO



GO



GO



GO



GO
CREATE NONCLUSTERED INDEX [IX_StockTransaction_Container]
    ON [dbo].[StockTransaction]([StockTransactionID_Container] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MasterTableD]
    ON [dbo].[StockTransaction]([MasterTableID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BatchNumber]
    ON [dbo].[StockTransaction]([BatchNumberID] ASC);


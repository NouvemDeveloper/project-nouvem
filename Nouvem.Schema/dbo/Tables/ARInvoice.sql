﻿CREATE TABLE [dbo].[ARInvoice] (
    [ARInvoiceID]             INT             IDENTITY (1, 1) NOT NULL,
    [DocumentNumberingID]     INT             NOT NULL,
    [Number]                  INT             NOT NULL,
    [BPContactID]             INT             NULL,
    [BPContactID_Delivery]    INT             NULL,
    [RouteID]                 INT             NULL,
    [DeliveryDate]            DATETIME        NULL,
    [DocumentDate]            DATETIME        NULL,
    [CreationDate]            DATETIME        NULL,
    [Printed]                 BIT             NULL,
    [NouDocStatusID]          INT             NULL,
    [Remarks]                 NVARCHAR (MAX)  NULL,
    [InvoiceNotePopUp]        BIT             NULL,
    [InvoiceNote]             NVARCHAR (MAX)  NULL,
    [TotalExVAT]              DECIMAL (18, 2) NULL,
    [VAT]                     DECIMAL (18, 2) NULL,
    [SubTotalExVAT]           DECIMAL (18, 2) NULL,
    [DiscountIncVAT]          DECIMAL (18, 2) NULL,
    [DiscountPercentage]      FLOAT (53)      NULL,
    [GrandTotalIncVAT]        DECIMAL (18, 2) NULL,
    [OtherReference]          NVARCHAR (50)   NULL,
    [UserMasterID]            INT             NULL,
    [CustomerPOReference]     NVARCHAR (50)   NULL,
    [BaseDocumentReferenceID] INT             NULL,
    [BPAddressID_Delivery]    INT             NULL,
    [BPAddressID_Invoice]     INT             NULL,
    [BPMasterID_Haulier]      INT             NULL,
    [BPMasterID_Customer]     INT             NULL,
    [PORequired]              BIT             NULL,
    [DeviceID]                INT             NULL,
    [Deleted]                 DATETIME        NULL,
    [EditDate]                DATETIME        NULL,
    CONSTRAINT [PK_ARInvoice] PRIMARY KEY CLUSTERED ([ARInvoiceID] ASC),
    CONSTRAINT [FK_ARInvoice_BPAddress] FOREIGN KEY ([BPAddressID_Delivery]) REFERENCES [dbo].[BPAddress] ([BPAddressID]),
    CONSTRAINT [FK_ARInvoice_BPAddress1] FOREIGN KEY ([BPAddressID_Invoice]) REFERENCES [dbo].[BPAddress] ([BPAddressID]),
    CONSTRAINT [FK_ARInvoice_BPContact] FOREIGN KEY ([BPContactID]) REFERENCES [dbo].[BPContact] ([BPContactID]),
    CONSTRAINT [FK_ARInvoice_BPMaster] FOREIGN KEY ([BPMasterID_Haulier]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_ARInvoice_BPMaster1] FOREIGN KEY ([BPMasterID_Customer]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_ARInvoice_DeviceMaster] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_ARInvoice_DocumentNumbering] FOREIGN KEY ([DocumentNumberingID]) REFERENCES [dbo].[DocumentNumbering] ([DocumentNumberingID]),
    CONSTRAINT [FK_ARInvoice_NouDocStatus] FOREIGN KEY ([NouDocStatusID]) REFERENCES [dbo].[NouDocStatus] ([NouDocStatusID]),
    CONSTRAINT [FK_ARInvoice_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID]),
    CONSTRAINT [FK_ARInvoice1_BPContact] FOREIGN KEY ([BPContactID_Delivery]) REFERENCES [dbo].[BPContact] ([BPContactID])
);








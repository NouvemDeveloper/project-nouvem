﻿CREATE TABLE [dbo].[PROrder] (
    [PROrderID]                   INT           IDENTITY (1, 1) NOT NULL,
    [DocumentNumberingID]         INT           NOT NULL,
    [Number]                      INT           NOT NULL,
    [NouDocStatusID]              INT           NOT NULL,
    [ReleaseDateTime]             DATE          NULL,
    [PRTypeID]                    INT           NULL,
    [Reference]                   NVARCHAR (50) NULL,
    [CreationDate]                DATETIME      NOT NULL,
    [PRSpecID]                    INT           NULL,
    [SpecificationNameIfModified] NVARCHAR (50) NULL,
    [BPContactID]                 INT           NULL,
    [UserID]                      INT           NOT NULL,
    [DeviceID]                    INT           NOT NULL,
    [BatchNumberID]               INT           NOT NULL,
    [ScheduledDate]               DATE          NULL,
    [BPMasterID]                  INT           NULL,
    [Deleted]                     DATETIME      NULL,
    CONSTRAINT [PK_ProductionOrder] PRIMARY KEY CLUSTERED ([PROrderID] ASC),
    CONSTRAINT [FK_PROrder_BatchNumber] FOREIGN KEY ([BatchNumberID]) REFERENCES [dbo].[BatchNumber] ([BatchNumberID]),
    CONSTRAINT [FK_PROrder_BPContact] FOREIGN KEY ([BPContactID]) REFERENCES [dbo].[BPContact] ([BPContactID]),
    CONSTRAINT [fk_PROrder_BPMaster] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_PROrder_DeviceMaster] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_PROrder_DocumentNumbering] FOREIGN KEY ([DocumentNumberingID]) REFERENCES [dbo].[DocumentNumbering] ([DocumentNumberingID]),
    CONSTRAINT [FK_PROrder_NouDocStatus] FOREIGN KEY ([NouDocStatusID]) REFERENCES [dbo].[NouDocStatus] ([NouDocStatusID]),
    CONSTRAINT [FK_PROrder_PRSpec] FOREIGN KEY ([PRSpecID]) REFERENCES [dbo].[PRSpec] ([PRSpecID]),
    CONSTRAINT [FK_PROrder_UserMaster] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);




GO
CREATE NONCLUSTERED INDEX [IX_BatchNumberID]
    ON [dbo].[PROrder]([BatchNumberID] ASC);


﻿CREATE TABLE [dbo].[Gs1AIMaster] (
    [GS1AIMasterID]       INT           IDENTITY (1, 1) NOT NULL,
    [AI]                  NVARCHAR (10) NOT NULL,
    [OfficialDescription] NVARCHAR (50) NOT NULL,
    [RelatedDescription]  NVARCHAR (50) NULL,
    [DataLength]          INT           NOT NULL,
    [Deleted]             BIT           NOT NULL,
    [IsFixed]             BIT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Gs1AI] PRIMARY KEY CLUSTERED ([GS1AIMasterID] ASC)
);


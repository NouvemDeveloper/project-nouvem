﻿CREATE TABLE [dbo].[StockTakeDetail] (
    [StockTakeDetailID]   INT             IDENTITY (1, 1) NOT NULL,
    [StockTakeID]         INT             NOT NULL,
    [INMasterID]          INT             NULL,
    [OpeningQty]          DECIMAL (18, 2) NULL,
    [OpeningWgt]          DECIMAL (18, 5) NULL,
    [ClosingQty]          DECIMAL (18, 2) NULL,
    [ClosingWgt]          DECIMAL (18, 5) NULL,
    [Barcode]             NVARCHAR (100)  NULL,
    [StockTransactionID]  INT             NULL,
    [UserID]              INT             NULL,
    [WarehouseID]         INT             NULL,
    [WarehouseID_Current] INT             NULL,
    [Qty]                 DECIMAL (18, 5) NULL,
    [Wgt]                 DECIMAL (18, 5) NULL,
    [StockDeletedDate]    DATETIME        NULL,
    [Deleted]             DATETIME        NULL,
    [CreationDate]        DATETIME        NULL,
    [WarehouseLocationID] INT             NULL,
    CONSTRAINT [PK_StockTakeDetail] PRIMARY KEY CLUSTERED ([StockTakeDetailID] ASC),
    CONSTRAINT [FK_StockTakeDetail_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_StockTakeDetail_StockTake] FOREIGN KEY ([StockTakeID]) REFERENCES [dbo].[StockTake] ([StockTakeID]),
    CONSTRAINT [FK_StockTakeDetail_StockTransaction] FOREIGN KEY ([StockTransactionID]) REFERENCES [dbo].[StockTransaction] ([StockTransactionID]),
    CONSTRAINT [FK_StockTakeDetail_UserMaster] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserMaster] ([UserMasterID]),
    CONSTRAINT [FK_StockTakeDetail_Warehouse] FOREIGN KEY ([WarehouseID]) REFERENCES [dbo].[Warehouse] ([WarehouseID]),
    CONSTRAINT [FK_StockTakeDetail_Warehouse1] FOREIGN KEY ([WarehouseID_Current]) REFERENCES [dbo].[Warehouse] ([WarehouseID])
);










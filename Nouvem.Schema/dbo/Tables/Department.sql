﻿CREATE TABLE [dbo].[Department] (
    [DepartmentID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]         NVARCHAR (50) NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [Deleted]      BIT           NOT NULL,
    CONSTRAINT [PK_Departement] PRIMARY KEY CLUSTERED ([DepartmentID] ASC)
);


﻿CREATE TABLE [dbo].[DocumentNumbering] (
    [DocumentNumberingID]     INT      IDENTITY (1, 1) NOT NULL,
    [NouDocumentNameID]       INT      NOT NULL,
    [DocumentNumberingTypeID] INT      NOT NULL,
    [FirstNumber]             INT      NOT NULL,
    [LastNumber]              INT      NOT NULL,
    [NextNumber]              INT      NOT NULL,
    [Deleted]                 DATETIME NULL,
    CONSTRAINT [PK_DocumentNumbering] PRIMARY KEY CLUSTERED ([DocumentNumberingID] ASC),
    CONSTRAINT [FK_DocumentNumbering_DocumentNumberingType] FOREIGN KEY ([DocumentNumberingTypeID]) REFERENCES [dbo].[DocumentNumberingType] ([DocumentNumberingTypeID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_DocumentNumbering_NouDocumentName] FOREIGN KEY ([NouDocumentNameID]) REFERENCES [dbo].[NouDocumentName] ([NouDocumentNameID])
);


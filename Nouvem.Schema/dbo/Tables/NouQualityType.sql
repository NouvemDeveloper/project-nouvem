﻿CREATE TABLE [dbo].[NouQualityType] (
    [NouQualityTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [QualityType]      VARCHAR (20) NOT NULL,
    [Deleted]          BIT          NOT NULL,
    CONSTRAINT [PK_NouQualityType] PRIMARY KEY CLUSTERED ([NouQualityTypeID] ASC)
);


﻿CREATE TABLE [dbo].[INMaster] (
    [INMasterID]                 INT             IDENTITY (1, 1) NOT NULL,
    [Code]                       VARCHAR (30)    NOT NULL,
    [Name]                       VARCHAR (100)   NOT NULL,
    [INGroupID]                  INT             NOT NULL,
    [TraceabilityTemplateNameID] INT             NULL,
    [DateTemplateNameID]         INT             NULL,
    [QualityTemplateNameID]      INT             NULL,
    [StockItem]                  BIT             NULL,
    [SalesItem]                  BIT             NULL,
    [PurchaseItem]               BIT             NULL,
    [FixedAsset]                 BIT             NULL,
    [BoxTareContainerID]         INT             NULL,
    [PiecesTareContainerID]      INT             NULL,
    [PalletTareContainerID]      INT             NULL,
    [MinWeight]                  DECIMAL (18, 2) NULL,
    [MaxWeight]                  DECIMAL (18, 2) NULL,
    [NominalWeight]              DECIMAL (18, 5) NULL,
    [SalesNominalCode]           NVARCHAR (50)   NULL,
    [SalesNominalDeptCode]       NVARCHAR (50)   NULL,
    [PurchaseNominalCode]        NVARCHAR (50)   NULL,
    [PurchaseNominalDeptCode]    NVARCHAR (50)   NULL,
    [DepartmentID]               INT             NULL,
    [TypicalPieces]              INT             NULL,
    [VATCodeID]                  INT             NULL,
    [Remarks]                    NVARCHAR (500)  NULL,
    [ActiveFrom]                 DATE            NULL,
    [ActiveTo]                   DATE            NULL,
    [InActiveFrom]               DATE            NULL,
    [InActiveTo]                 DATE            NULL,
    [ItemImage]                  VARBINARY (MAX) NULL,
    [Deleted]                    DATETIME        NULL,
    [UserMasterID]               INT             NULL,
    [CreationDate]               DATETIME        NULL,
    [NouStockModeID]             INT             NULL,
    [ProductionProduct]          BIT             NULL,
    [ProductLabelFieldID]        INT             NULL,
    [PricePerUnit]               DECIMAL (18, 2) NULL,
    [QtyPerBox]                  INT             NULL,
    [EditDate]                   DATETIME        NULL,
    [DeviceID]                   INT             NULL,
    CONSTRAINT [PK_INMaster] PRIMARY KEY CLUSTERED ([INMasterID] ASC),
    CONSTRAINT [FK_INMaster_Container] FOREIGN KEY ([BoxTareContainerID]) REFERENCES [dbo].[Container] ([ContainerID]),
    CONSTRAINT [FK_INMaster_Container1] FOREIGN KEY ([PiecesTareContainerID]) REFERENCES [dbo].[Container] ([ContainerID]),
    CONSTRAINT [FK_INMaster_Container2] FOREIGN KEY ([PalletTareContainerID]) REFERENCES [dbo].[Container] ([ContainerID]),
    CONSTRAINT [FK_INMaster_DateTemplateName] FOREIGN KEY ([DateTemplateNameID]) REFERENCES [dbo].[DateTemplateName] ([DateTemplateNameID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_INMaster_Departement] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID]),
    CONSTRAINT [FK_INMaster_DeviceMaster] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_INMaster_INGroup] FOREIGN KEY ([INGroupID]) REFERENCES [dbo].[INGroup] ([INGroupID]),
    CONSTRAINT [FK_INMaster_NouStockMode] FOREIGN KEY ([NouStockModeID]) REFERENCES [dbo].[NouStockMode] ([NouStockModeID]),
    CONSTRAINT [FK_INMaster_ProductLabelField] FOREIGN KEY ([ProductLabelFieldID]) REFERENCES [dbo].[ProductLabelField] ([ProductLabelFieldID]),
    CONSTRAINT [FK_INMaster_QualityTemplateName] FOREIGN KEY ([QualityTemplateNameID]) REFERENCES [dbo].[QualityTemplateName] ([QualityTemplateNameID]),
    CONSTRAINT [FK_INMaster_TraceabilityTemplateName] FOREIGN KEY ([TraceabilityTemplateNameID]) REFERENCES [dbo].[TraceabilityTemplateName] ([TraceabilityTemplateNameID]),
    CONSTRAINT [FK_INMaster_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID]),
    CONSTRAINT [FK_INMaster_VATCode] FOREIGN KEY ([VATCodeID]) REFERENCES [dbo].[VATCode] ([VATCodeID]) ON DELETE CASCADE ON UPDATE CASCADE
);
















﻿CREATE TABLE [dbo].[DocumentNumberingType] (
    [DocumentNumberingTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                    NVARCHAR (50) NOT NULL,
    [Deleted]                 DATETIME      NULL,
    CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED ([DocumentNumberingTypeID] ASC)
);


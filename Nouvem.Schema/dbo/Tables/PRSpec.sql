﻿CREATE TABLE [dbo].[PRSpec] (
    [PRSpecID]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [CreationDate] DATETIME      NOT NULL,
    [Deleted]      DATETIME      NULL,
    CONSTRAINT [PK_ProductionSpec] PRIMARY KEY CLUSTERED ([PRSpecID] ASC)
);


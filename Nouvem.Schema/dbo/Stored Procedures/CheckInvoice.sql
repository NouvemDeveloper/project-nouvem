﻿





-- =============================================
-- Author:		brian murray
-- Create date: 19/07/16
-- Description:	Checks the dispatch docket totals against the line totals.
-- =============================================
CREATE PROCEDURE [dbo].[CheckInvoice] 
	-- Add the parameters for the stored procedure here
	@num int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DECLARE 
	 @ArdispatchDetailId int,
	 @PriceListId int,
	 @InmasterId int,
	 @QtyDelivered float,
	 @WeightDelivered float,
	 @Total decimal(18,2),
	 @UnitPrice decimal(18,2)

	


    declare @ardispatchid int =(
    select TOP 1 BaseDocumentReferenceID from arinvoice where Number = @num)

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT a.ARDispatchDetailID,
	        a.PriceListID,
			a.INMasterID,
			a.QuantityDelivered,
			a.WeightDelivered,
			a.TotalExclVAT,
			a.UnitPrice
				
     FROM  ardispatchdetail a
	 WHERE a.Deleted is null and a.ARDispatchID = @ardispatchid
 
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO 
	    @ArdispatchDetailId,
		 @PriceListId,
		 @InmasterId,
		 @QtyDelivered,
		 @WeightDelivered,
		 @Total,
		 @UnitPrice

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
        BEGIN 	   
	   
	      DECLARE @LineTotal decimal(18,2)
	      declare @NouPriceMethodId int = (
	      select top 1 noupricemethodid from pricelistdetail where inmasterid = @inmasterid and pricelistid = @pricelistid)
		  
		  if (@NouPriceMethodId is not null)
		  begin

		   if (@NouPriceMethodId = 1)
		   begin		    
			SET @LineTotal = @QtyDelivered * @UnitPrice
		   end

		    if (@NouPriceMethodId = 2)
		   begin		
		    declare @nominalWeight decimal(18,5) = (SELECT top 1 NominalWeight FROM INmaster where inmasterid = @INMasterId)    
			SET @LineTotal = (@QtyDelivered * @NominalWeight) * @UnitPrice
		   end  

		    if (@NouPriceMethodId = 3)
		   begin		    
			SET @LineTotal = @WeightDelivered * @UnitPrice
		   end

		   if (@LineTotal <> @Total)
		   print convert(nvarchar(100),@LineTotal) + '    ' + Convert(nvarchar(100), @Total) + '      ' + convert(nvarchar(100), @Ardispatchid)

		  end

	 FETCH NEXT FROM PRICE_CURSOR
		INTO 
	    @ArdispatchDetailId,
		 @PriceListId,
		 @InmasterId,
		 @QtyDelivered,
		 @WeightDelivered,
		 @Total,
		 @UnitPrice
	 END	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
	
END
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE ReportProducts
	-- Add the parameters for the stored procedure here
@Code nvarchar(20)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	code,name,TypicalPieces,ActiveTo,
	(select 'ts bloor') as company
	
	 from inmaster where code = @code
END
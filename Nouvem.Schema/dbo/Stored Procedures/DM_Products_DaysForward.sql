﻿



-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Bring the days forward accross.
-- =============================================
CREATE PROCEDURE [dbo].[DM_Products_DaysForward]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 	
	 @ProductCode varchar(30),
	 @DaysForward int

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.ProductCode,
			p.BestBeforeDays	

     FROM  [SERVER2\sqlexpress].[DEMLocal].[dbo].[Products]  p
 
     OPEN PRICE_CURSOR;
  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO 
		  @ProductCode,
		  @DaysForward
		

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	 
		DECLARE @INMasterID int = (SELECT TOP 1 INMasterID FROM INMaster WHERE
								   RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@ProductCode)))
								
								  
			IF (@INMasterID > 0)
		    BEGIN
			    INSERT INTO DateDays VALUES(9, @INMasterID, @DaysForward, NULL)
		    END		
	

	 FETCH NEXT FROM PRICE_CURSOR
		 INTO  @ProductCode,
		       @DaysForward
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END




﻿







-- =============================================
-- Author:		brian murray
-- Create date: 06/07/2016
-- Description:	Gets the dispatch docket transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportBatchDetail_OutOfButchery] 
	-- Add the parameters for the stored procedure here
	@BatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	    w.StockLocation,
		w.Name as 'Warehouse',        
		d.Number,	
        b.Name,
		b.Code,		
		s.TransactionWeight,
		s.TransactionQTY,		
		s.Serial,
		s.Pieces,
		s.TransactionDate,
		i.INMasterID,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		bn.Number AS 'BatchNo',		
		
		  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'Batch Ref') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'Batch Ref') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'Batch Ref')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'Batch Ref') 
				      END)) AS 'Supplier Batch'

    FROM APGoodsReceipt d
       INNER JOIN APGoodsReceiptDetail dd
          ON d.APGoodsReceiptID = dd.APGoodsReceiptID
	   INNER JOIN StockTransaction s
	      ON s.MasterTableID = dd.APGoodsReceiptDetailID
	   INNER JOIN BatchNumber bn
	      ON bn.BatchNumberID = s.BatchNumberID
	   INNER JOIN Warehouse w
	      ON w.WarehouseID = s.WarehouseID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Supplier = b.BPMasterID
	   
      
         
     WHERE bn.BatchNumberID = @BatchID AND dd.Deleted IS NULL AND s.Deleted IS NULL and s.NouTransactionTypeID = 2
     END
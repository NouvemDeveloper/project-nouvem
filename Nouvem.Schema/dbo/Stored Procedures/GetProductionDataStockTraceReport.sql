﻿




-- Create date: 15/11/2016
-- Description:	Gets the batch production data
-- =============================================
CREATE PROCEDURE [dbo].[GetProductionDataStockTraceReport]
	-- Add the parameters for the stored procedure here
   @LabelId AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Batchid int = (select top 1 BatchNumberid from StockTransaction where StockTransactionID = @LabelID)
	DECLARE @TransProductionIssue int = (SELECT TOP 1 NouTransactionTypeID FROM NouTransactionType WHERE [Description] = 'ProductionIssue')
	DECLARE @TransProductionReceipt int = (SELECT TOP 1 NouTransactionTypeID FROM NouTransactionType WHERE [Description] = 'ProductionReceipt')

    -- Insert statements for procedure here
	SELECT b.BatchNumberID, 
	       b.Number AS 'BatchNo',
		   P.reference,
		   SUM(s.TransactionQTY) AS 'TransactionQty', 
		   SUM(s.TransactionWeight) AS 'TransactionWeight',
		   SUM(CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END) AS 'BoxCount',
		   im.Name, 
		   ntt.AddToStock, 
		   ntt.SubtractFromStock, 
		   SUM((CASE WHEN ntt.SubtractFromStock = 1 AND s.StockTransactionID_Container IS NULL THEN s.TransactionQty ELSE 0 END)) AS InputQty, 
	       SUM((CASE WHEN ntt.AddToStock = 1 AND s.StockTransactionID_Container IS NULL THEN s.Pieces ELSE 0 END)) AS OutputQty, 
		   SUM((CASE WHEN ntt.SubtractFromStock = 1 AND s.StockTransactionID_Container IS NULL THEN s.TransactionWeight ELSE 0 END)) AS InputWgt, 
	       SUM((CASE WHEN ntt.AddToStock = 1 AND s.StockTransactionID_Container IS NULL THEN s.TransactionWeight ELSE 0 END)) AS OutputWgt, 
		   p.PROrderID, 
		   s.INMasterID

    FROM   PROrder p 
	       INNER JOIN BatchNumber b ON p.BatchNumberID = b.BatchNumberID
		   INNER JOIN StockTransaction s ON s.MasterTableID = p.PROrderID 
		   INNER JOIN NouTransactionType ntt ON s.NouTransactionTypeID = ntt.NouTransactionTypeID 
		   INNER JOIN INMaster im ON im.INMasterID = s.INMasterID



	WHERE (s.Deleted IS NULL) AND (b.BatchNumberID= @batchid) and (s.NouTransactionTypeID = @TransProductionIssue or s.NouTransactionTypeID = @TransProductionReceipt)
	GROUP BY im.Name, p.Number, b.BatchNumberID,P.reference, b.Number, ntt.AddToStock, ntt.SubtractFromStock, p.PROrderID, s.INMasterID

END
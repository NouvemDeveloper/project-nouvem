﻿
-- =============================================
-- Author:		brian murray
-- Create date: 21/08/2015
-- Description:	Test proc for the label design.
-- =============================================
CREATE PROCEDURE [dbo].[TestDataSourceInventory]
	-- Add the parameters for the stored procedure here
@ID as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @ID = 0
	   BEGIN
	      SELECT Name, Code, Deleted,
	             REPLACE(FORMAT(NominalWeight, '0000.00'), '.', '')  as NominalNetWeight,		
	             '' as Barcode,
	             '' as Barcode2,
	             Right(Year(getDate()),2) + FORMAT(DATEPART(mm, GETDATE()), '00') + FORMAT(DATEPART(dd, GETDATE()), '00') AS OrderDate
	      FROM INMaster
	      WHERE Deleted = '2001-01-01'
        END
		ELSE
		BEGIN
		   SELECT Name, Code, Deleted,
	             REPLACE(FORMAT(NominalWeight, '0000.00'), '.', '')  as NominalNetWeight,		
	             '' as Barcode,
	             '' as Barcode2,
	             Right(Year(getDate()),2) + FORMAT(DATEPART(mm, GETDATE()), '00') + FORMAT(DATEPART(dd, GETDATE()), '00') AS OrderDate
	      FROM INMaster
	      WHERE INMasterID = @ID
		END   
END



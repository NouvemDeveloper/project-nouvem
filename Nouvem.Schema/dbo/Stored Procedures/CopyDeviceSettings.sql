﻿





-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Copies the device settings from one device to another.
-- =============================================
CREATE PROCEDURE [dbo].[CopyDeviceSettings]	
@CopyFromDeviceId int,
@CopyToDeviceId int
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 	 


	 @Name nvarchar(100),
	 @Value1 nvarchar(100),
	 @Value2 nvarchar(100),
	 @Value3 nvarchar(100),
	 @Value4 nvarchar(100),
	 @Value5 nvarchar(100),
	 @Value6 nvarchar(100),
	 @Value7 nvarchar(100),
	 @Value8 nvarchar(100),
	 @Value9 nvarchar(100),
	 @Value10 nvarchar(100),
	 @Value11 nvarchar(100),
	 @Value12 nvarchar(100),
	 @Value13 nvarchar(100),
	 @Value14 nvarchar(100)

	 DECLARE SETTINGS_CURSOR CURSOR STATIC FOR
     SELECT Name,
			Value1,	
			Value2,
			Value3,
			Value4,
			Value5,
			Value6,
			Value7,
			Value8,
			Value9,
			Value10,
			Value11,
			Value12,
			Value13,
			Value14

     FROM  DeviceSettings Where DeviceMasterID = @CopyFromDeviceId
 
     OPEN SETTINGS_CURSOR;
  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM SETTINGS_CURSOR
	 INTO 
		 @Name,
		 @Value1,
		 @Value2,
		 @Value3,
		 @Value4,
		 @Value5,
		 @Value6,
		 @Value7,
		 @Value8,
		 @Value9,
		 @Value10,
		 @Value11,
		 @Value12,
		 @Value13,
		 @Value14
		

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	  
	     DECLARE @CopyToName nvarchar(100) =
		 (SELECT Name FROM DeviceSettings Where Name = @Name AND DeviceMasterID = @CopyToDeviceId)	

		 IF @CopyToName IS NULL
		   INSERT INTO DeviceSettings VALUES (@Name, @Value1, @Value2, @Value3, @Value4, @Value5, @Value6, @Value7, @Value8, @Value9, @Value10,@Value11, @Value12,@Value13,@Value14, NULL, @CopyToDeviceId)
		 ELSE
		   UPDATE DeviceSettings 
		   SET Value1 = @Value1, Value2 = @Value2, Value3 = @Value3, Value4 = @Value4, Value5 = @Value5, Value6 = @Value6, Value7 = @Value7, 
		   Value8 = @Value8, Value9 = @Value9, Value10 = @Value10, Value11 = @Value11, Value12 = @Value12, Value13=@Value13, Value14=@Value14
		   WHERE DeviceMasterID = @CopyToDeviceId and Name = @Name

	 FETCH NEXT FROM SETTINGS_CURSOR
		 INTO  
		 @Name,
		 @Value1,
		 @Value2,
		 @Value3,
		 @Value4,
		 @Value5,
		 @Value6,
		 @Value7,
		 @Value8,
		 @Value9,
		 @Value10,
		 @Value11,
		 @Value12,
		 @Value13,
		 @Value14
	 END
	 
	 CLOSE SETTINGS_CURSOR;
     DEALLOCATE SETTINGS_CURSOR;
END
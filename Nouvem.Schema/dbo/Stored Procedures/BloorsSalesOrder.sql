﻿
-- =============================================
-- Author:		<James Harrison>
-- Create date: <24/08/2016>
-- Description:	<Sales Order (Order Outstanding)>
-- =============================================
CREATE PROCEDURE [dbo].[BloorsSalesOrder] 
	-- Add the parameters for the stored procedure here
 @SalesID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT
       bpm.Name 'CustomerName',
       RTRIM(LTRIM(bpa.AddressLine1)) as 'AddressLine1',
       RTRIM(LTRIM(bpa.AddressLine2)) as 'AddressLine2',
       RTRIM(LTRIM(bpa.addressline3)) as 'AddressLine3',
       RTRIM(LTRIM(bpa.addressline4)) as 'AddressLine4',
       RTRIM(LTRIM(bpa.PostCode)) as 'PostCode',
       aro.DispatchNote1,
	   aro.PopUpNote,
       aro.DeliveryDate,
       aro.arorderid,
       aro.CustomerPOReference,
       RTRIM(LTRIM(inm.code)) as 'Code',
       RTRIM(LTRIM(inm.Name)) as 'Name',
	   um.UserName,
       arod.QuantityOrdered,
       arod.WeightOrdered,
       arod.UnitPrice,
	   inm.qtyperbox,
	   aro.Number,
	   hbpm.Name as 'HaulierName',
	   


	  (SELECT arod.WeightOrdered -
	  (SELECT TOP 1 WeightDelivered FROM ARDispatchDetail WHERE ARDispatchID = ard.ARDispatchID AND INMasterID = inm.INMasterID)) AS 'WeightOutstanding',

	  (SELECT arod.QuantityOrdered -
	  (SELECT TOP 1 QuantityDelivered FROM ARDispatchDetail WHERE ARDispatchID = ard.ARDispatchID AND INMasterID = inm.INMasterID)) AS 'QtyOutstanding' 
	  
	  
FROM AROrder aro inner join AROrderDetail arod on aro.AROrderID = arod.AROrderID
       inner join BPMaster bpm on bpm.BPMasterID = aro.BPMasterID_Customer
       inner join BPAddress bpa on bpm.BPMasterID = bpa.BPMasterID
       inner join INMaster inm on inm.INMasterID = arod.INMasterID
	  left join ARDispatch ard on ard.BaseDocumentReferenceID = arod.AROrderID	   
       left join UserMaster um on aro.UserMasterID_SalesEmployee = um.UserMasterID
	   left join BPMaster hbpm on hbpm.BPMasterID = aro.BPMasterID_Haulier

       where aro.AROrderID = @SalesID and bpa.Shipping = 1 and ard.NouDocStatusID <>2 and arod.Deleted is null






END
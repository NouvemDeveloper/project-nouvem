﻿








-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the dispatch docket transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportDispatchDocketDetail] 
	-- Add the parameters for the stored procedure here
	@DispatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @NouTransactionTypeDispatch int = (SELECT TOP 1 NouTransactionTypeID FROM NouTransactionType WHERE [Description] = 'GoodsDelivery')

    -- Insert statements for procedure here
	SELECT  d.ARDispatchID,       
		d.DeliveryDate,
		d.DocumentDate,
		d.Number,
		d.CustomerPOReference,
        b.Name,
		b.Code,
		a.AddressLine1,
		a.AddressLine2,
		a.AddressLine3,
		a.AddressLine4,
		invoice.AddressLine1 AS 'InvoiceAddressLine1',
		invoice.AddressLine2 AS 'InvoiceAddressLine2',
		invoice.AddressLine3 AS 'InvoiceAddressLine3',
		invoice.AddressLine4 AS 'InvoiceAddressLine4',
		s.TransactionWeight,
		s.TransactionQTY,
		dd.QuantityDelivered,
		s.Serial,
		s.Pieces,
		(CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END) as Box,
		i.INMasterID,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		bn.Number AS 'BatchNo',
		dd.ARDispatchDetailID,

		(SELECT 
		 CASE 
		 WHEN  (SELECT COUNT (*) FROM ARDispatchDetail 
		 INNER JOIN StockTransaction ON ARDispatchDetail.ARDispatchDetailID = StockTransaction.MasterTableID
		 WHERE StockTransaction.MasterTableID = dd.ARDispatchDetailID AND StockTransaction.Deleted IS NULL) = 0 
		 THEN 0
		 ELSE		
		CONVERT(DECIMAL(18,2),dd.TotalExclVAT / 
	    (SELECT COUNT (*) FROM ARDispatchDetail 
		 INNER JOIN StockTransaction ON ARDispatchDetail.ARDispatchDetailID = StockTransaction.MasterTableID
		 WHERE StockTransaction.MasterTableID = dd.ARDispatchDetailID AND StockTransaction.Deleted IS NULL)) END) AS Total,

		  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'UseBy') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = i.INMasterID and ms.TraceabilityCode = 'UseBy') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'UseBy')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'UseBy') 
				      END)) AS 'Use By'
    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN StockTransaction s
	      ON s.MasterTableID = dd.ARDispatchDetailID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Customer = b.BPMasterID
	   LEFT JOIN BatchNumber bn
	      ON bn.BatchNumberID = s.BatchNumberID
       LEFT JOIN BPAddress a
          ON d.BPAddressID_Delivery = a.BPAddressID
       LEFT JOIN BPAddress invoice
          ON d.BPAddressID_Invoice = invoice.BPAddressID
     WHERE d.ARDispatchID = @DispatchID AND dd.Deleted IS NULL AND s.Deleted is null and s.NouTransactionTypeID = @NouTransactionTypeDispatch
     END

﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BloorProductList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT inm.code,
	inm.name,
	ISNULL(ing.Name, '') as 'Product Group',
		ISNULL(ping.Name, '') as 'Parent Product Group',
		plf.Field1 as 'Label 1',
		plf.field2 as 'Label 2',
		plf.field3 as 'Label 3'
		
	


	from INMaster inm inner join INGroup ing on inm.INGroupID = ing.INGroupID
	left join INGroup ping on ing.ParentInGroupID = ping.INGroupID
	left join ProductLabelField plf on inm.ProductLabelFieldID = plf.ProductLabelFieldID


	where inm.deleted is null


	
END
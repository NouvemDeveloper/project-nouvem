﻿-- =============================================
-- Author:		brian murray
-- Create date: 05/09/2015
-- Description:	Removes the business partners
-- =============================================
CREATE PROCEDURE TestRemovePartners
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from BPAttachment
	delete from BPPropertySelection
	delete from BPProperty
	delete from BPAddress
	delete from BPAddressSnapshot
	delete from BPContact
	delete from BPMaster
END

﻿




-- =============================================
-- Author:		brian murray
-- Create date: 05/04/2016
-- Description:	Retrieves the products
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetLabelData]
	@LabelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT s.StockTransactionID,
	       s.Serial,
		   s.TransactionWeight,
		   s.TransactionQty,
		   s.Tare,
		   s.GrossWeight,
		   s.TransactionDate,
		   m.Name,
		   m.Code,
		   bn.Number,
		 
                    (SELECT [dbo].[SplitPlant](CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierCutIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierCutIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierCutIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierCutIn') 
				      END)) AS 'Supplier Cut In',

		    (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'Grade') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'Grade') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'Grade')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'Grade') 
				      END)) AS 'Grade',
		   
		   	 (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'KillNumber') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'KillNumber') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber') 
				      END)) AS 'Kill Number',

					   (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierBatch') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SupplierBatch') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch') 
				      END)) AS 'Supplier Batch',
		 
	         (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'BornIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'BornIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'BornIn')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'BornIn') 
				      END)) AS 'Born In',

                  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'RearedIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'RearedIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'RearedIn')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'RearedIn') 
				      END)) AS 'Reared In',

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'FattenedIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'FattenedIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'FattenedIn')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'FattenedIn') 
				      END)) AS 'Fattened In',

					  (SELECT [dbo].[SplitPlant](CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SlaughteredIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'SlaughteredIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'SlaughteredIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SlaughteredIn') 
				      END)) AS 'Slaughtered In',

					  (SELECT [dbo].[SplitPlant](CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CutIn') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CutIn') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'CutIn')
				      ELSE  (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'CutIn') 
				      END)) AS 'Cut In',				
		

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'KillDate') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'KillDate') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'KillDate')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'KillDate') 
				      END)) AS 'Kill Date',

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'PackedOnDate') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'PackedOnDate') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'PackedOnDate')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'PackedOnDate') 
				      END)) AS 'Packed On Date',

					 (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'UseBy') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join DateTemplateName tn on inm.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateTemplateAllocation a on a.DateTemplateNameID = tn.DateTemplateNameID 
                                 inner join DateMaster ms on a.DateMasterID = ms.DateMasterID where INMasterID = m.INMasterID and ms.DateCode = 'UseBy') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND DateMaster.DateCode = 'UseBy')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'UseBy') 
				      END)) AS 'Use By Date',

					  (SELECT (CASE WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CountryOfOrigin') IS NULL
					  THEN ''
					  WHEN (select a.Batch from INMaster inm  
			                     inner join TraceabilityTemplateName tn on inm.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateAllocation a on a.TraceabilityTemplateNameID = tn.TraceabilityTemplateNameID 
                                 inner join TraceabilityTemplateMaster ms on a.TraceabilityTemplateMasterID = ms.TraceabilityTemplatesMasterID where INMasterID = m.INMasterID and ms.TraceabilityCode = 'CountryOfOrigin') = 1
				      THEN  (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
											    AND TraceabilityTemplateMaster.TraceabilityCode = 'CountryOfOrigin')
				      ELSE  (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'CountryOfOrigin') 
				      END)) AS 'Country Of Origin'



	FROM StockTransaction s INNER JOIN INMaster m
	                                   ON s.INMasterID = m.INMasterID
		                    LEFT JOIN  BatchNumber bn
							           ON bn.BatchNumberID = s.BatchNumberID
							

	WHERE s.StockTransactionID = @LabelId
	     
END



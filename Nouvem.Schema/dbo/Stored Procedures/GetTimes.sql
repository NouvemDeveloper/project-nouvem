﻿

-- =============================================
-- Author:		brian murray
-- Create date: 03/05/2016
-- Description:	Gets the system times incrementing every half hour.
-- =============================================
CREATE PROCEDURE [dbo].[GetTimes]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH CTE(N)
	AS
	(
	SELECT 30
	UNION ALL
	SELECT N+30
	FROM CTE
	WHERE N+5<24*60   
	)

	
	SELECT '23:59'
	UNION ALL
	SELECT CONVERT(varchar(5),DATEADD(minute,N,0) ,108) 
	
	FROM CTE
	OPTION (MAXRECURSION 0)
	

	

	
END


﻿



CREATE PROCEDURE [dbo].[ReportGetBatchYieldDetail] 
	@PROrderID int,
	@INMasterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TransProductionIssue int = (SELECT TOP 1 NouTransactionTypeID FROM NouTransactionType WHERE [Description] = 'ProductionIssue')
	DECLARE @TransProductionReceipt int = (SELECT TOP 1 NouTransactionTypeID FROM NouTransactionType WHERE [Description] = 'ProductionReceipt')

    SELECT m.Name, s.Serial, w.StockLocation, s.TransactionWeight, s.TransactionQTY, 
	CASE WHEN s.Pieces is null or s.Pieces = 0 THEN 1 ELSE s.Pieces END AS Pieces,	
	CASE WHEN s.IsBox = 1 THEN 1 ELSE 0 END as 'BoxCount', s.InLocation,
	s.MasterTableID as 'PROrderID', s.INMasterID,
   (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'KillDate' and w.StockLocation <> 1) AS 'Kill Date',
   (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber' and w.StockLocation <> 1) AS 'Kill Number'

	FROM StockTransaction s 
	INNER JOIN Warehouse w ON s.WarehouseID = w.WarehouseID
	INNER JOIN INMaster m ON s.INMasterID = m.INMasterID
    WHERE s.MasterTableID = @PROrderID and s.INMasterID = @INMasterID AND s.Deleted is null AND s.StockTransactionID_Container IS NULL 
	      and (s.NouTransactionTypeID = @TransProductionIssue or s.NouTransactionTypeID = @TransProductionReceipt)
END



﻿


-- =============================================
-- Author:		brian murray
-- Create date: 19/07/16
-- Description:	Sets the 0 cost prices, using the cost price - @percentage
-- =============================================
CREATE PROCEDURE [dbo].[SetCostPrices] 
	-- Add the parameters for the stored procedure here
	@Percentage INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DECLARE 
	 @PriceListDetailId int,
	 @INMasterId int,	
	 @CostPriceListId INT = (SELECT PriceListID FROM PriceList WHERE CurrentPriceListName = 'Cost Price List'),
	 @BasePriceListId INT = (SELECT PriceListID FROM PriceList WHERE CurrentPriceListName = 'Base Price List'),
	 @Value FLOAT = CONVERT(FLOAT,(100 - @Percentage)) / CONVERT(FLOAT,100)

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.PriceListDetailID,
			p.INMasterID			
     FROM PriceListDetail p where p.PriceListID = @CostPriceListId
 
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @PriceListDetailId,
		  @INMasterId
		 

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	   DECLARE @BasePrice FLOAT
	   DECLARE @Price Decimal(18,2) = (SELECT Price FROM PriceListDetail where PriceListDetailID = @PriceListDetailId)
	   

	   if (@Price = 0)
	   BEGIN
	
	     SET @BasePrice = CONVERT(FLOAT,(SELECT Price FROM PriceListDetail where INMasterId = @INMasterId AND PriceListID = @BasePriceListId))

		 if (@BasePrice is not null and @BasePrice > 0)
		 BEGIN	
		    PRINT @BasePrice
		    Set @BasePrice = @BasePrice * @Value
			PRINT 'New Price' + CONVERT(VARCHAR(20), @BasePrice)
			Update PriceListDetail SET Price = @BasePrice WHERE PriceListDetailID = @PriceListDetailId
			Print 'Updated'
		 END
	   END		   
	   

	 FETCH NEXT FROM PRICE_CURSOR
		INTO @PriceListDetailId,
		  @INMasterId
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
	
END
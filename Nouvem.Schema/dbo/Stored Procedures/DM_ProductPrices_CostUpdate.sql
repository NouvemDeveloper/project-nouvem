﻿
-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Error in DM_ProductPrices. Correcting
-- =============================================
Create PROCEDURE [dbo].[DM_ProductPrices_CostUpdate]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @ProductCode varchar(30),		
	 @CostPricePerUnit decimal(18,2)


	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.ProductCode,		
			p.CostPricePerUnit
				
 
     FROM [win-coldstore\sqlexpress].[DEMLocal].[dbo].[Products]  p 
	
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @ProductCode,	
		  @CostPricePerUnit


     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 	 
	
		    DECLARE @INMasterID INT = (SELECT TOP 1 INMasterID FROM INMaster
								  WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@ProductCode)))

			DECLARE @PriceListDetailId INT = (SELECT TOP 1 PriceListDetailID FROM PriceListDetail WHERE INMasterID = @INMasterID AND PriceListID = 5087)      
								  
			IF (@INMasterID > 0)
		    BEGIN			   
			   UPDATE PriceListDetail SET Price = @CostPricePerUnit WHERE PriceListDetailID = @PriceListDetailId		 
			   		
		    END		
	

	 FETCH NEXT FROM PRICE_CURSOR
		  INTO 
		  @ProductCode,				
		  @CostPricePerUnit
		 
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END
﻿

-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the partner data from the DEMLocal db to the Nouvem partners tables.
-- =============================================
CREATE PROCEDURE [dbo].[DM_RemoveDuplicateBatches]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @Id int


	 DECLARE PARTNER_CURSOR CURSOR STATIC FOR
         WITH CTE AS(
			SELECT PROrderID,
				 RN = ROW_NUMBER()OVER(PARTITION BY Reference ORDER BY Reference)
			 FROM PROrder where deleted is null	)
			 select PROrderID from CTE where RN>1      
 
     OPEN PARTNER_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @Id
		  

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	    
		
	 FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @Id
		  
		  declare @Count int = (select Count(*) from stocktransaction where NouTransactionTypeID = 7 and mastertableid = @id and deleted is null)
		  if (@Count = 0 and @ID < 10430)
		  begin
		    update PROrder set deleted = getdate() where PROrderID = @Id
		  end

	 END
	 
	 CLOSE PARTNER_CURSOR;
     DEALLOCATE PARTNER_CURSOR;
END
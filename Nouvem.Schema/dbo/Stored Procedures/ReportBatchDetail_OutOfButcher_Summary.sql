﻿







-- =============================================
-- Author:		brian murray
-- Create date: 06/07/2016
-- Description:	Gets the dispatch docket transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportBatchDetail_OutOfButcher_Summary] 
	-- Add the parameters for the stored procedure here
	@BatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  

		w.Name as 'Warehouse',    	      	
		SUM(s.TransactionWeight) as 'Wgt',
		SUM(s.TransactionQTY) as 'Qty',	
		SUM(s.Pieces) as 'Pieces',	
		i.INMasterID,
		i.Name AS 'ProductName'		
		 

    FROM APGoodsReceipt d
       INNER JOIN APGoodsReceiptDetail dd
          ON d.APGoodsReceiptID = dd.APGoodsReceiptID
	   INNER JOIN StockTransaction s
	      ON s.MasterTableID = dd.APGoodsReceiptDetailID
	   INNER JOIN BatchNumber bn
	      ON bn.BatchNumberID = s.BatchNumberID
	   INNER JOIN Warehouse w
	      ON w.WarehouseID = s.WarehouseID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Supplier = b.BPMasterID
	   
      
         
     WHERE bn.BatchNumberID = @BatchID AND dd.Deleted IS NULL AND s.Deleted IS NULL and s.NouTransactionTypeID = 2
	  GROUP BY w.Name, i.INMasterID, i.Name
     END
﻿
-- =============================================
-- Author:		brian murray
-- Create date: 13-10-16
-- Description:	Retrieves the label trace data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportLabelTrace]
	-- Add the parameters for the stored procedure here
	@FromId int,
	@ToId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	s.StockTransactionID,
	inm.Name,
	inm.Code,
	u.FullName as 'User',
	s.Serial as 'Barcode',
	s.TransactionDate

	FROM StockTransaction s
	INNER JOIN INMaster inm 
	  ON s.INMasterID = inm.INMasterID
	LEFT JOIN UserMaster u
	  ON s.UserMasterID = u.UserMasterID

	WHERE s.StockTransactionID BETWEEN @FromId AND @ToId
END
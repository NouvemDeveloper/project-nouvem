﻿



-- =============================================
-- Author:		brian murray
-- Create date: 11/04/2016
-- Description:	Resets the db, removing all non essentail data.
-- *************************************************************

-- =============================================================
-- =============================================================
--  **** DO NOT USE THIS SP WITHOUT EXPRESS PERMISSION ***
-- =============================================================
-- =============================================================


-- =============================================
CREATE PROCEDURE [dbo].[ResetDatabase] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  
    DELETE FROM TransactionTraceability
	DELETE FROM StockTransaction	
	DELETE FROM PROrder	
	DELETE FROM APGoodsReceiptDetail
	DELETE FROM APGoodsReceipt
	DELETE FROM APGoodsReceiptAttachment
	DELETE FROM APOrderDetail
	DELETE FROM APOrder
	DELETE FROM APOrderAttachment
	DELETE FROM APQuoteDetail
	DELETE FROM APQuote
	DELETE FROM APQuoteAttachment
	DELETE FROM ARDispatchDetail
	DELETE FROM ARDispatch
	DELETE FROM ARDispatchAttachment
	DELETE FROM AROrderDetail
	DELETE FROM AROrder
	DELETE FROM OrderAttachment
	DELETE FROM ARQuoteDetail
	DELETE FROM ARQuote
	DELETE FROM QuoteAttachment
	DELETE FROM [Audit]
	DELETE FROM BPAddressSnapshot
	UPDATE INMaster SET TraceabilityTemplateNameID = null, DateTemplateNameID = null, QualityTemplateNameID = null
	UPDATE INGroup SET TraceabilityTemplateNameID = null, DateTemplateNameID = null, QualityTemplateNameID = null
	DELETE FROM DateDays
	DELETE FROM DateTemplateAllocation
	DELETE FROM DateMaster
	DELETE FROM DateTemplateName
	DELETE FROM DeviceSettings	
	DELETE FROM DeviceMaster
	DELETE FROM INAttachment
	DELETE FROM INMasterSnapshot
	DELETE FROM LabelAssociationLabel
    DELETE FROM Label
	DELETE FROM LabelGroup
	DELETE FROM LabelAssociation
	DELETE FROM ARInvoiceDetail
	DELETE FROM ARInvoiceAttachment
	DELETE FROM ARInvoice
	DELETE FROM UserGroupRule
	DELETE FROM UserLoggedOn
	DELETE FROM WindowSettings
	UPDATE INMaster SET UserMasterID = null
	DELETE FROM UserMaster
	DELETE FROM [UserGroup ]
	DELETE FROM NouLicenceTypeRule
	DELETE FROM LicenceDetail
	DELETE FROM Licence
	DELETE FROM Plant
	DELETE FROM PRBatchOutput
	DELETE FROM PRInputCarcassType
	DELETE FROM PRInputCarcassTypeOrder
	DELETE FROM PRInputConfirmation
	DELETE FROM PRInputConfirmationOrder
	DELETE FROM PRInputFatScore
	DELETE FROM PRInputFatScoreOrder
	DELETE FROM PROrderOutput
	DELETE FROM QualityTemplateAllocation
	DELETE FROM QualityMaster
	DELETE FROM QualityTemplateName
	DELETE FROM DeviceSettings
	UPDATE BPMaster SET RouteMasterID = null
	DELETE FROM RegionMaster
	DELETE FROM RouteMaster
	DELETE FROM PRSpecOutput
	DELETE FROM StockTakeDetail
	DELETE FROM INMaster
	DELETE FROM INGroup
	DELETE FROM BPContact
	DELETE FROM BPAddressSnapshot
	DELETE FROM BPAddress
	DELETE FROM BPPropertySelection
	DELETE FROM BPProperty
	DELETE FROM BPAttachment
	DELETE FROM BPMaster
	DELETE FROM BPGroup
	DELETE FROM BatchTraceability
	DELETE FROM BatchNumber
	DELETE FROM PriceListDetail
	DELETE FROM PriceList
	DELETE FROM VATCode


	-- reseed --
	DBCC CHECKIDENT ('INMaster', reseed, 0)
	DBCC CHECKIDENT ('INGroup', reseed, 0)
	DBCC CHECKIDENT ('BPMaster', reseed, 0)
	DBCC CHECKIDENT ('BPGroup', reseed, 0)

	-- BPPartner, iNMaster, PRSpec, and price list tables left in for now

END




﻿




-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the partner data from the DEMLocal db to the Nouvem partners tables.
-- =============================================
CREATE PROCEDURE [dbo].[DM_ProductionBatches]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @DeliveryDate datetime,
	 @ProductCode varchar(50),
	 @Qty int,
	 @Wgt money,
	 @SuppliersBatch char(20),
	 @IntakeBatch varchar(50),
	 @BestBefore datetime,
	 @Country nvarchar(50),
	 @SupplierCode nvarchar(10),
	
	 @NextNumber int = 1,
	 @CurrentBatchNo int = 1,
	 @PRNextNumber int = 1


	 DECLARE PARTNER_CURSOR CURSOR STATIC FOR
         SELECT i.ReceivedDate,
		        p.ProductCode,
				ii.IntakeQuantity,
				ii.IntakeWeight,
				st.SuppliersBatch,
				ii.IntakeBatch,
				st.BestBeforeDate,
				c.[Description],
				s.SupplierCode

         FROM [SERVER2\sqlexpress].[DEMLocal].[dbo].[IntakeDockets] i
         INNER JOIN [SERVER2\sqlexpress].[DEMLocal].[dbo].[IntakeDocketItems] ii 
		   ON i.ID = ii.IntakeID
		 INNER JOIN [SERVER2\sqlexpress].[DEMLocal].[dbo].[Suppliers] s
		   ON s.ID	= i.SupplierID
		 INNER JOIN [SERVER2\sqlexpress].[DEMLocal].[dbo].[Stock] st
		   ON st.Id = ii.StockID
		 INNER JOIN [SERVER2\sqlexpress].[DEMLocal].[dbo].[Products] p
		   ON p.ID = st.ProductID
		 LEFT JOIN [SERVER2\sqlexpress].[DEMLocal].[dbo].[Countries] c
		   ON c.ID = st.Origin
         WHERE CONVERT(Date, i.ReceivedDate) > DATEADD(day,-30, CONVERT(date,GETdate()))   
      
 
     OPEN PARTNER_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @DeliveryDate,
		   @ProductCode,
	       @Qty,
	       @Wgt,
	       @SuppliersBatch,
	       @IntakeBatch,
		   @BestBefore,
		   @Country,
		   @SupplierCode
	      

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	    
		-- apgoodsrecipt
	     update DocumentNumbering set NextNumber = @NextNumber where DocumentNumberingID = 25

		 DECLARE @BPMasterID int = (SELECT TOP 1 BPMasterID FROM BPMaster WHERE rtrim(ltrim(code)) = rtrim(ltrim(@SupplierCode)))
		
		insert into APGoodsReceipt Values(NULL,25,@NextNumber,NULL,NULL,NULL,NULL, @DeliveryDate, @DeliveryDate, Getdate(),getdate(),0,1,'',0,null,0,0,0,0,null,@BPMasterID,null,null,null,null,null,null,null,null,null)
		DECLARE @APGoodsReceiptId int = Ident_Current('APGoodsReceipt')


		DECLARE @LocalProductCode nvarchar(50) = ltrim(rtrim(@ProductCode))
		    if (@LocalProductCode = 'BTL1')
			BEGIN 
			 SET @LOcalProductCode = 'BITL1'
			END

			if (@LocalProductCode = 'GP1')
			BEGIN 
			 SET @LOcalProductCode = 'GPN1'
			END

			if (@LocalProductCode = 'TC1')
			BEGIN 
			 SET @LOcalProductCode = 'BITC1'
			END

			if (@LocalProductCode = 'TC1')
			BEGIN 
			 SET @LOcalProductCode = 'BITC1'
			END

			if (@LocalProductCode = 'TC2')
			BEGIN 
			 SET @LOcalProductCode = 'BITC2'
			END

			if (@LocalProductCode = 'FAG1')
			BEGIN 
			 SET @LOcalProductCode = 'UKG1'
			END

			if (@LocalProductCode = 'PSH1')
			BEGIN 
			 SET @LOcalProductCode = 'HOCK1SMP'
			END

			if (@LocalProductCode = 'TLGF2')
			BEGIN 
			 SET @LOcalProductCode = 'SLBITL5'
			END

			if (@LocalProductCode = 'PWBPSHV1')
			BEGIN 
			 SET @LOcalProductCode = 'BP1'
			END

			if (@LocalProductCode = 'BSB1')
			BEGIN 
			 SET @LOcalProductCode = 'BS1'
			END


			if (@LocalProductCode = 'GFS1')
			BEGIN 
			 SET @LOcalProductCode = 'GCN1SM'
			END

			if (@LocalProductCode = 'PWBPSHV1')
			BEGIN 
			 SET @LOcalProductCode = 'BP1'
			END

			if (@LocalProductCode = 'RAWUKWHOCK')
			BEGIN 
			 SET @LOcalProductCode = 'UKWHOCK1P'
			END

		-- apgoodsreceiptdetail
		declare @Inmasterid int = (select top 1 INMasterid  from inmaster where ltrim(rtrim(Code)) = ltrim(rtrim(@LocalProductCode)))


		if (@Inmasterid is not null)
		BEGIN
		   
		    declare @BatchNo nvarchar(100) = CONVERT(varchar(100), @CurrentBatchNo)
		    insert into BatchNumber values (@BatchNo, NULL, GETDATE(), @SuppliersBatch)
		    DECLARE @BatchNumberID int = Ident_Current('BatchNumber')

			insert into APGoodsReceiptDetail values (@APGoodsReceiptId, @Inmasterid, 75033, null,null,@Qty, @Wgt, NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,@BatchNumberID,NULL,NULL,NULL,'')

			 DECLARE @APGoodsReceiptDetailID int = Ident_Current('APGoodsReceiptDetail')

			 insert into stocktransaction values(@APGoodsReceiptDetailId,@BatchNumberID,NULL,GETDATE(), 2, @Qty, @Wgt, @Wgt,null,null,@Qty,1002,null,@Inmasterid,null,null,1,null,null,null,null,null,null,null,null,null,'',null)
			 DECLARE @StocktransactionID int = Ident_Current('Stocktransaction')
			 update StockTransaction set serial = StockTransactionID where StockTransactionID = @StocktransactionID


			DECLARE @UseBy nvarchar(10)  = CONVERT(nvarchar(10), @BestBefore,103)
			insert into TransactionTraceability values (@StocktransactionID,2,@UseBy,null, null,12,null,null)

			insert into BatchTraceability values (@BatchNumberID,1,15,null,null,' ',null)
			insert into BatchTraceability values (@BatchNumberID,1,18,null,null,' ',null)
			insert into BatchTraceability values (@BatchNumberID,1,19,null,null,@Country,null)

			update DocumentNumbering set NextNumber = @PRNextNumber where DocumentNumberingID = 1003
			insert into PROrder values (1003,@PRNextNumber,1,NULL,NULL,@IntakeBatch,GETDATE(),NULL,'',null,9022,4018,@BatchNumberId, GETDATE(),NULL,NULL)
			DECLARE @PROrderID int = Ident_Current('PROrder')
			insert into PROrderInput values (@PROrderID, @Inmasterid, 0,null)
		END

		  
      SET @NextNumber = @NextNumber + 1
	  SET @CurrentBatchNo = @CurrentBatchNo + 1	 
	  SET @PRNextNumber = @PRNextNumber + 1
	 FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @DeliveryDate,
		   @ProductCode,
	       @Qty,
	       @Wgt,
	       @SuppliersBatch,
	       @IntakeBatch,
		   @BestBefore,
		   @Country,
		   @SupplierCode
	 END
	 
	 CLOSE PARTNER_CURSOR;
     DEALLOCATE PARTNER_CURSOR;
END
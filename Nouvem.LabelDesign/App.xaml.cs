﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.LabelDesign.Global;
using Nouvem.LabelDesign.View.UserInput;

namespace Nouvem.LabelDesign
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Overriden base startup.
        /// </summary>
        /// <param name="e">Start up event argument.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Messenger.Default.Register<string>(
              this,
              Token.OpenMessageBox,
              x =>
              {
                  var messageBox = new NouvemMessageBoxView();
                  messageBox.Dispatcher.Invoke(new Action(() => messageBox.ShowDialog()), DispatcherPriority.SystemIdle);
              });
        }
    }
}

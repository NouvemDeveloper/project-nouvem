﻿// -----------------------------------------------------------------------
// <copyright file="Token.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesign.Global
{
    internal static class Token
    {
        /// <summary>
        /// Token used to as the first parameter of the messenger construct, used when a standard generic message is only required.
        /// </summary>
        public const string Message = "";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseMessageBoxWindow = "CLOSE_MESSAGEBOX_WINDOW";

        /// <summary>
        /// Token used to inform the message box to open. 
        /// </summary>
        public const string OpenMessageBox = "OPEN_MESSAGEBOX";
    }
}

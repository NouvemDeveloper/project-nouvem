﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesign.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesign.Global
{
    using Nouvem.LabelDesign.View;

    /// <summary>
    /// Entry point for any external application call.
    /// </summary>
    public static class LabelDesign
    {
        /// <summary>
        /// Start the label designer.
        /// </summary>
        public static void Start()
        {
            var label = new LabelDesignView();
            label.ShowDialog();
        }
    }
}

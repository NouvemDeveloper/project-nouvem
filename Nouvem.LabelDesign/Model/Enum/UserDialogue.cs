﻿// -----------------------------------------------------------------------
// <copyright file="UserDialogue.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesign.Model.Enum
{
    /// <summary>
    /// Enumeration that models the message box user selections.
    /// </summary>
    internal enum UserDialogue
    {
        /// <summary>
        /// The OK result
        /// </summary>
        OK,

        /// <summary>
        /// The cancel result
        /// </summary>
        Cancel,

        /// <summary>
        /// The Yes result
        /// </summary>
        Yes,

        /// <summary>
        /// The No result
        /// </summary>
        No
    }
}


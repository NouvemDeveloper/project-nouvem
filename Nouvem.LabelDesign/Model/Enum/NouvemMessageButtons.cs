﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBoxButtons.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesign.Model.Enum
{
    /// <summary>
    /// The message box buttons enumerations
    /// </summary>
    public enum NouvemMessageBoxButtons
    {
        /// <summary>
        /// The OK cancel selection
        /// </summary>
        OKCancel,

        /// <summary>
        /// The yes no selection
        /// </summary>
        YesNo,

        /// <summary>
        /// The ok selection
        /// </summary>
        OK
    }
}


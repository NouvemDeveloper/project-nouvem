﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nouvem.LabelDesign.Utility
{
    /// <summary>
    /// Interaction logic for PositionView.xaml
    /// </summary>
    public partial class PositionView : UserControl
    {
        public PositionView()
        {
            InitializeComponent();
        }

        public double ItemX
        {
            get
            {
                try
                {
                    return double.Parse(txtItemX.Text);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                txtItemX.Text = value.ToString();
            }
        }

        public double ItemY
        {
            get
            {
                try
                {
                    return double.Parse(txtItemY.Text);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                txtItemY.Text = value.ToString();
            }
        }

    }

}

    
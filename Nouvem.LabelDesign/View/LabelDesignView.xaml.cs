﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesignView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesign.View
{
    using System;
    using System.Windows;
    using Microsoft.Win32;
    using Neodynamic.SDK.Printing;
    using Neodynamic.Windows;
    using Neodynamic.Windows.ThermalLabelEditor;
    using Nouvem.LabelDesign.Dialogue;
    using Nouvem.LabelDesign.Global;
    using Nouvem.LabelDesign.Utility;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LabelDesignView : Window
    {

        #region private

        #region label item

        /// <summary>
        /// Sets the ActiveTool to the Pointer
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbPointer_Click(object sender, RoutedEventArgs e)
        {
            //Set the ActiveTool to Pointer
            this.ThermalLabelEditor.ActiveTool = EditorTool.Pointer;
        }

        /// <summary>
        /// Sets the ActiveTool to the rectangle.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbRect_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Rectangle
            this.ThermalLabelEditor.ActiveTool = EditorTool.Rectangle;

            //Create and set the ActiveToolItem i.e. a RectangleShapeItem
            var rectItem = new RectangleShapeItem();
            rectItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = rectItem;
        }

        /// <summary>
        /// Sets the ActiveTool to the ellipse.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbEllipse_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Ellipse
            this.ThermalLabelEditor.ActiveTool = EditorTool.Ellipse;

            //Create and set the ActiveToolItem i.e. a EllipseShapeItem
            var ellItem = new EllipseShapeItem();
            ellItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = ellItem;
        }

        /// <summary>
        /// Sets the ActiveTool to the line.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbLine_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Line
            this.ThermalLabelEditor.ActiveTool = EditorTool.Line;

            //Create and set the ActiveToolItem i.e. a LineShapeItem
            var lineItem = new LineShapeItem();
            lineItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = lineItem;
        }

        /// <summary>
        /// Sets the ActiveTool to the text.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbText_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Text
            this.ThermalLabelEditor.ActiveTool = EditorTool.Text;

            //Create and set the ActiveToolItem i.e. a TextItem
            var txtItem = new TextItem();
            txtItem.Font.Name = Font.NativePrinterFontA;
            txtItem.Font.Size = 10;
            txtItem.Text = Strings.TypeHere;

            txtItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = txtItem;
        }

        /// <summary>
        /// Sets the ActiveTool to the image.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbImage_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Image
            this.ThermalLabelEditor.ActiveTool = EditorTool.Image;

            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                var imgItem = new ImageItem();
                imgItem.SourceFile = dialog.FileName;
                this.ThermalLabelEditor.ActiveToolItem = imgItem;
            }
        }

        /// <summary>
        /// Sets the ActiveTool to the image.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbBarcode_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set the ActiveTool to Barcode
            this.ThermalLabelEditor.ActiveTool = EditorTool.Barcode;

            //Create and set the ActiveToolItem i.e. a BarcodeItem
            //HERE YOU COULD CHANGE THE DEFAULT BARCODE TO BE DISPLAYED
            //OR OPEN A BARCODE DIALOG FOR SETTINGS, ETC.
            var barcodeItem = new BarcodeItem();
            barcodeItem.Symbology = BarcodeSymbology.Code39;
            barcodeItem.AddChecksum = false;
            barcodeItem.Code = BarcodeItemUtils.GenerateSampleCode(barcodeItem.Symbology);
            barcodeItem.Font.Name = Font.NativePrinterFontA;
            barcodeItem.Font.Size = 5;
            barcodeItem.BarcodeAlignment = BarcodeAlignment.MiddleCenter;
            barcodeItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = barcodeItem;
        }

        /// <summary>
        /// Sets the ActiveTool to the rfid.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbRFID_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                // no label on the surface
                return;
            }

            //Set image, logo or icon you'd like to show for RFID tag!
            //this.ThermalLabelEditor.RFIDTagImageFileName = @"C:\Images\rfid.png";


            //Set the ActiveTool to RFID Tag
            this.ThermalLabelEditor.ActiveTool = EditorTool.RFIDTag;

            //Create and set the ActiveToolItem i.e. a RFIDTagItem
            var rfidTagItem = new RFIDTagItem();
            rfidTagItem.ConvertToUnit(this.ThermalLabelEditor.LabelDocument.UnitType);

            this.ThermalLabelEditor.ActiveToolItem = rfidTagItem;
        }

        #endregion

        #region new/open/save/update

        /// <summary>
        /// Open the new label set up screen.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void menuNew_Click(object sender, RoutedEventArgs e)
        {
            //Create a new 'document'
            var labelSetup = new LabelSetUpView
            {
                LabelUnit = UnitType.Inch,
                LabelWidth = 4,
                LabelHeight = 3,
                LabelGapLength = 0,
                LabelMarkLength = 0
            };
            
            if (labelSetup.ShowDialog() == true)
            {
                //Create a ThermalLabel object based on the dialog box info
                //NOTE: In that dialog you should also ask for the "GapLength" 
                //and other properties which may be relevant to the creation
                //of the ThermalLabel object!
                var label = new ThermalLabel
                {
                    Width = labelSetup.LabelWidth,
                    Height = labelSetup.LabelHeight,
                    UnitType = labelSetup.LabelUnit,
                    IsContinuous = labelSetup.LabelIsContinuous,
                    GapLength = labelSetup.LabelGapLength,
                    MarkLength = labelSetup.LabelMarkLength
                };

                //load it on the editor surface
                this.ThermalLabelEditor.LoadThermalLabel(label);
            }
        }

        /// <summary>
        /// Open a label path
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void menuOpen_Click(object sender, RoutedEventArgs e)
        {
            //open a thermal label template
            //NOTE: *.tl extension is just that! The content of such files should be a ThermalLabel XML Template
            var dialog = new OpenFileDialog();
            dialog.Filter = "ThermalLabel Template (*.xml, *.tl)|*.xml;*.tl";
            if (dialog.ShowDialog() == true)
            {
                try
                {
                    //Create a ThermalLabel obj from a Template
                    var label = ThermalLabel.CreateFromXmlTemplate(System.IO.File.ReadAllText(dialog.FileName));

                    //load it on the editor surface
                    this.ThermalLabelEditor.LoadThermalLabel(label);
                }
                catch (Exception ex)
                {
                    NouvemMessageBox.Show(Message.Error);
                }
            }
        }

        /// <summary>
        /// Save the label.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void menuSave_Click(object sender, RoutedEventArgs e)
        {
            if (this.ThermalLabelEditor.LabelDocument == null)
            {
                return;
            }

            //open save dialog...
            //NOTE: we have used *.tl file extension for ThermalLabel XML templates
            //BUT you can change it to whatever you want
            var dialog = new SaveFileDialog();
            dialog.DefaultExt = ".tl";
            dialog.Filter = "ThermalLabel XML Template (.tl)|*.tl";
            if (dialog.ShowDialog() == true)
            {
                try
                {
                    //save ThermalLabel template
                    this.ThermalLabelEditor.Save(dialog.FileName);
                }
                catch (Exception ex)
                {
                    NouvemMessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        private void menuLabelSetup_Click(object sender, RoutedEventArgs e)
        {
            //is there any label on the editor's surface...
            if (ThermalLabelEditor.LabelDocument != null)
            {
                //Open dialog for label document setup
                LabelSetUpView labelSetup = new LabelSetUpView();
                labelSetup.LabelUnit = ThermalLabelEditor.LabelDocument.UnitType;
                labelSetup.LabelWidth = ThermalLabelEditor.LabelDocument.Width;
                labelSetup.LabelHeight = ThermalLabelEditor.LabelDocument.Height;
                labelSetup.LabelIsContinuous = ThermalLabelEditor.LabelDocument.IsContinuous;
                labelSetup.LabelGapLength = ThermalLabelEditor.LabelDocument.GapLength;
                labelSetup.LabelMarkLength = ThermalLabelEditor.LabelDocument.MarkLength;

                labelSetup.Owner = this;

                if (labelSetup.ShowDialog() == true)
                {
                    //Incoke UpdateLabelDocument method for updating the label document inside the editor
                    ThermalLabelEditor.UpdateLabelDocument(labelSetup.LabelUnit, labelSetup.LabelWidth, labelSetup.LabelHeight, ThermalLabelEditor.LabelDocument.NumOfFractionalDigits);
                }
            }
        }

        #endregion

        #region print label

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void menuPrint_Click(object sender, RoutedEventArgs e)
        {
            // Create the ThermalLabel obj from the editor
            var thermalLabel = this.ThermalLabelEditor.CreateThermalLabel();

            if (thermalLabel == null)
            {
                return;
            }

            //Display Print Job dialog...           
            var frmPrintJob = new PrintJobDialog { Owner = this };

            if (frmPrintJob.ShowDialog() == true)
            {
                //create a PrintJob object
                using (var printJob = new PrintJob(frmPrintJob.PrinterSettings))
                {
                    printJob.Copies = frmPrintJob.Copies;
                    printJob.PrintOrientation = frmPrintJob.PrintOrientation;
                    printJob.ThermalLabel = thermalLabel;
                    printJob.Print();
                }
            }
        }

        #endregion

        #region about

        /// <summary>
        /// Displays support details.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void menuAbout_Click(object sender, RoutedEventArgs e)
        {
            NouvemMessageBox.Show(Message.LabelSupportInfo);
        }

        #endregion

        #region zoom

        /// <summary>
        /// Sets the zoom.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void tbbZoom100_Click(object sender, RoutedEventArgs e)
        {
            //Set up zoom to 100%
            this.sldZoom.Value = 100;
        }

        #endregion

        #region editor item movement

        /// <summary>
        /// Handles the item movement.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ThermalLabelEditor_SelectionChanged(object sender, EventArgs e)
        {
            /* The items selection has changed...
             * For simplicity, we will not show any dialog if there's a multiple item selection */
            tbbProp.IsEnabled = false;
            if (this.ThermalLabelEditor.CurrentSelection != null)
            {
                tbbProp.IsEnabled = !(this.ThermalLabelEditor.CurrentSelection is MultipleSelectionItem);
            }
        }

        /// <summary>
        /// Gets and displays the label editor items dynamic position.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ThermalLabelEditor_SelectionAreaChanged(object sender, EventArgs e)
        {
            /* Show in the 'status bar' the dimensions of the selected area
             * we're going to format it including the unit */
            var area = this.ThermalLabelEditor.CurrentSelectionArea;

            if (!(area.Width > 0 && area.Height > 0))
            {
                this.txtSelectionInfo.Text = string.Empty;
                return;
            }

            var unitOfMeasure = string.Empty;
            switch (this.ThermalLabelEditor.LabelDocument.UnitType)
            {
                case UnitType.Inch:
                    unitOfMeasure = Strings.Inch;
                    break;

                case UnitType.DotsPerInch:
                    unitOfMeasure = Strings.DotsPerInch;
                    break;

                case UnitType.Pica:
                    unitOfMeasure = Strings.Pica;
                    break;

                case UnitType.Point:
                    unitOfMeasure = Strings.Point;
                    break;

                default:
                    unitOfMeasure = this.ThermalLabelEditor.LabelDocument.UnitType.ToString().ToLower();
                    break;
            }

            var data = new object[]{unitOfMeasure,
                                         area.X,
                                         area.Y,
                                         area.Width,
                                         area.Height};

            var decimals = "0".PadRight(this.ThermalLabelEditor.LabelDocument.NumOfFractionalDigits, '0');

            this.txtSelectionInfo.Text = string.Format("X: {1:0." + decimals + "}{0}   Y: {2:0." + decimals + "}{0}   Width: {3:0." + decimals + "}{0}   Height: {4:0." + decimals + "}{0}", data);
        }

        #endregion

        #endregion

       

        /// <summary>
        /// Handle the window load event.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //THIS IS A LIST OF PROPERTIES FOR CUSTOMIZING THE EDITOR UI
            //this.ThermalLabelEditor.RulerBackground = Brushes.Tomato;
            //this.ThermalLabelEditor.RulerLinesColor = Colors.Gold;
            //this.ThermalLabelEditor.RulerForeColor = Colors.White;
            //this.ThermalLabelEditor.RulerSelectionColor = Colors.Purple;

            //LinearGradientBrush lgb = new LinearGradientBrush(Colors.LightGray, Colors.White, 45);
            //this.ThermalLabelEditor.LabelDocumentFrameBackground = lgb;
            //this.ThermalLabelEditor.LabelDocumentFrameBorderColor = Colors.Black;
            //this.ThermalLabelEditor.LabelDocumentFrameBorderThickness = 3;
            //this.ThermalLabelEditor.LabelDocumentFrameCornerRadius = 0;

            //this.ThermalLabelEditor.NoImageFileName = @"c:\noimage.jpg";


            //this.ThermalLabelEditor.AdornerHandlerBackColor = Colors.Yellow;
            //this.ThermalLabelEditor.AdornerHandlerHoverBackColor = Colors.DarkCyan;
            //this.ThermalLabelEditor.AdornerHandlerBorderColor = Colors.Gray;
            //this.ThermalLabelEditor.AdornerFrameBorderColor = Colors.Gray;

            //this.ThermalLabelEditor.AdornerSelectionBackColor = System.Windows.Media.Color.FromArgb(128, 255, 0, 128);
            //this.ThermalLabelEditor.AdornerSelectionBorderColor = System.Windows.Media.Color.FromArgb(128, 255, 0, 255);

            //this.ThermalLabelEditor.AngleSnap = 45;

            //this.ThermalLabelEditor.ImageProcessingDpi = 300;

            //this.ThermalLabelEditor.TextItemEditModeEnabled = false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void editorContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //Disable/Enable context menu items based on the selected items on the editor's surface
            Item selectedItem = this.ThermalLabelEditor.CurrentSelection;

            //Font option is available for TextItem and BarcodeItem only
            if (selectedItem is TextItem || selectedItem is BarcodeItem)
            {
                this.cmFont.Visibility = Visibility.Visible;
            }
            else
            {
                this.cmFont.Visibility = Visibility.Collapsed;
            }

            this.Sep1.Visibility = cmFont.Visibility;

            //update "format" name based on the type of the selected Item
            this.cmFormat.Visibility = Visibility.Visible;

            if (selectedItem is TextItem)
            {
                this.cmFormat.Header = "Format Text...";
            }
            else if (selectedItem is BarcodeItem)
            {
                this.cmFormat.Header = "Format Barcode...";
            }
            else if (selectedItem is RectangleShapeItem)
            {
                this.cmFormat.Header = "Format Rectangle...";
            }
            else if (selectedItem is EllipseShapeItem)
            {
                this.cmFormat.Header = "Format Ellipse...";
            }
            else if (selectedItem is LineShapeItem)
            {
                this.cmFormat.Header = "Format Line...";
            }
            else if (selectedItem is ImageItem)
            {
                this.cmFormat.Header = "Format Picture...";
            }
            else if (selectedItem is RFIDTagItem)
            {
                this.cmFormat.Header = "Format RFID Tag...";
            }
            else
            {
                this.cmFormat.Visibility = Visibility.Collapsed;
            }

            Sep3.Visibility = this.cmFormat.Visibility;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void cmFont_Click(object sender, RoutedEventArgs e)
        {
            //get current selection
            Item selectedItem = this.ThermalLabelEditor.CurrentSelection;

            //show Font dialog only for TextItem or BarcodeItem objects
            if (selectedItem is TextItem || selectedItem is BarcodeItem)
            {
                //Get current Font of selected item
                Font itemFont;
                if (selectedItem is TextItem)
                {
                    itemFont = ((TextItem) selectedItem).Font;
                }
                else
                {
                    itemFont = ((BarcodeItem)selectedItem).Font;
                }

                //create and open a FontDialog
                var fontDialog = new FontDialogueView
                {
                    Font = itemFont,
                    Owner = this
                };

                if (fontDialog.ShowDialog() == true)
                {
                    //get new Font settings
                    itemFont = fontDialog.Font;

                    //update font settings on item
                    if (selectedItem is TextItem)
                    {
                        ((TextItem)selectedItem).Font.UpdateFrom(itemFont);
                    }
                    else
                    {
                        ((BarcodeItem)selectedItem).Font.UpdateFrom(itemFont);
                    }

                    //update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void cmFormat_Click(object sender, RoutedEventArgs e)
        {
            //get current selection
            Item selectedItem = this.ThermalLabelEditor.CurrentSelection;

            if (selectedItem is ImageItem)
            {
                //create and open ImageItemDialog
                ImageDialogueView imgItemDialog = new ImageDialogueView();
                imgItemDialog.Owner = this;

                //set current ImageItem to dialog
                ImageItem curImgItem = selectedItem as ImageItem;

                imgItemDialog.ImageItem = curImgItem;
                if (imgItemDialog.ShowDialog() == true)
                {
                    //update ImageItem based on dialog result
                    curImgItem.UpdateFrom(imgItemDialog.ImageItem);

                    //update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }
            }
            //else if (selectedItem is ShapeItem)
            //{
            //    //create and open ShapeItemDialog
            //    ShapeDialogueView shapeItemDialog = new ShapeDialogueView();
            //    shapeItemDialog.Owner = this;
            //    //set current ShapeItem to dialog
            //    ShapeItem curShapeItem = selectedItem as ShapeItem;
            //    shapeItemDialog.ShapeItem = curShapeItem;
            //    //customize dialog title
            //    shapeItemDialog.Title = curShapeItem.ToString().Replace("Neodynamic.SDK.Printing.", "").Replace("ShapeItem", "");

            //    if (shapeItemDialog.ShowDialog() == true)
            //    {
            //        //update ShapeItem based on dialog result
            //        curShapeItem.UpdateFrom(shapeItemDialog.ShapeItem);

            //        //update editor's surface
            //        this.ThermalLabelEditor.UpdateSelectionItemsProperties();
            //    }
            //}
            //else if (selectedItem is TextItem)
            //{
            //    //create and open TextItemDialog
            //    TextDialogueView textItemDialog = new TextDialogueView();
            //    textItemDialog.Owner = this;
            //    //set current TextItem to dialog
            //    TextItem curTextItem = selectedItem as TextItem;
            //    textItemDialog.TextItem = curTextItem;

            //    if (textItemDialog.ShowDialog() == true)
            //    {
            //        //update TextItem based on dialog result
            //        curTextItem.UpdateFrom(textItemDialog.TextItem);

            //        //update editor's surface
            //        this.ThermalLabelEditor.UpdateSelectionItemsProperties();
            //    }
            //}
            else if (selectedItem is BarcodeItem)
            {
                //create and open BarcodeItemDialog
                BarcodeDialogueView bcItemDialog = new BarcodeDialogueView();
                bcItemDialog.Owner = this;
                //set current BarcodeItem to dialog
                BarcodeItem curBarcodeItem = selectedItem as BarcodeItem;
                bcItemDialog.BarcodeItem = curBarcodeItem;

                if (bcItemDialog.ShowDialog() == true)
                {
                    //update BarcodeItem based on dialog result
                    curBarcodeItem.UpdateFrom(bcItemDialog.BarcodeItem);

                    //update editor's surface
                    this.ThermalLabelEditor.UpdateSelectionItemsProperties();
                }
            }
            //else if (selectedItem is RFIDTagItem)
            //{
            //    //create and open RFIDTagItemDialog
            //    RFIDTagItemDialog rfidTagItemDialog = new RFIDTagItemDialog();
            //    rfidTagItemDialog.Owner = this;
            //    //set current RFIDTagItem to dialog
            //    RFIDTagItem curRFIDTagItem = selectedItem as RFIDTagItem;
            //    rfidTagItemDialog.RFIDTagItem = curRFIDTagItem;

            //    if (rfidTagItemDialog.ShowDialog() == true)
            //    {
            //        //update RFIDTagItem based on dialog result
            //        curRFIDTagItem.UpdateFrom(rfidTagItemDialog.RFIDTagItem);

            //        //update editor's surface
            //        this.ThermalLabelEditor.UpdateSelectionItemsProperties();
            //    }
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void menuOptions_Click(object sender, RoutedEventArgs e)
        {
            //is there any label on the editor's surface...
            if (this.ThermalLabelEditor.LabelDocument != null)
            {
                //Open dialog for View settings
                OptionsDialogueView viewOpt = new OptionsDialogueView();
                viewOpt.ShowGrid = this.ThermalLabelEditor.ShowGrid;
                viewOpt.GridSize = this.ThermalLabelEditor.GridSize;
                viewOpt.SnapToGrid = this.ThermalLabelEditor.SnapToGrid;
                viewOpt.AngleSnap = this.ThermalLabelEditor.AngleSnap;
                viewOpt.ArrowKeysShortStep = this.ThermalLabelEditor.ArrowKeysShortStep;
                viewOpt.ArrowKeysLargeStep = this.ThermalLabelEditor.ArrowKeysLargeStep;

                viewOpt.SetUnitLegends(this.ThermalLabelEditor.LabelDocument.UnitType);

                viewOpt.Owner = this;

                if (viewOpt.ShowDialog() == true)
                {
                    this.ThermalLabelEditor.ShowGrid = viewOpt.ShowGrid;
                    this.ThermalLabelEditor.GridSize = viewOpt.GridSize;
                    this.ThermalLabelEditor.SnapToGrid = viewOpt.SnapToGrid;
                    this.ThermalLabelEditor.AngleSnap = viewOpt.AngleSnap;
                    this.ThermalLabelEditor.ArrowKeysShortStep = viewOpt.ArrowKeysShortStep;
                    this.ThermalLabelEditor.ArrowKeysLargeStep = viewOpt.ArrowKeysLargeStep;
                }
            }
        }

     
        private void menuPdf_Click(object sender, RoutedEventArgs e)
        {
            //Create the ThermalLabel obj from the editor
            ThermalLabel tLabel = this.ThermalLabelEditor.CreateThermalLabel();

            if (tLabel != null)
            {
                //open save dialog...
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".pdf";
                dlg.Filter = "Adobe PDF (.pdf)|*.pdf";
                if (dlg.ShowDialog() == true)
                {
                    try
                    {
                        //export ThermalLabel to PDF
                        using (PrintJob pj = new PrintJob())
                        {
                            pj.ThermalLabel = tLabel;
                            pj.ExportToPdf(dlg.FileName, 96);
                        }
                    }
                    catch (Exception ex)
                    {
                        NouvemMessageBox.Show(Message.Error);
                    }
                }
            }
        }

        private void menuUploadTTFToPrinterStorage_Click(object sender, RoutedEventArgs e)
        {
            TTFToPrinterStorageView ttfToPrinter = new TTFToPrinterStorageView();
            ttfToPrinter.Owner = this;
            ttfToPrinter.ShowDialog();
        }

        private void menuManageFontsInPrinterStorage_Click(object sender, RoutedEventArgs e)
        {
            PrinterFontsDialogueView printerFonts = new PrinterFontsDialogueView();
            printerFonts.Owner = this;
            printerFonts.ShowDialog();
        }













    }
}

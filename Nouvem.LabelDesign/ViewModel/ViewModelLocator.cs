﻿// -----------------------------------------------------------------------
// <copyright file="ViewModelLocator.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.LabelDesign.ViewModel.UserInput;

namespace Nouvem.LabelDesign.ViewModel
{
    using GalaSoft.MvvmLight;
  

    public class ViewModelLocator
    {
        /// <summary>
        /// The message box view model reference.
        /// </summary>
        private static NouvemMessageBoxViewModel messageBox = new NouvemMessageBoxViewModel();

        public ViewModelLocator()
        {
            
        }

        #region MessageBox

        /// <summary>
        /// Gets the MessageBox property.
        /// </summary>
        public static NouvemMessageBoxViewModel MessageBoxStatic
        {
            get
            {
                if (messageBox == null)
                {
                    CreateMessageBox();
                }

                return messageBox;
            }
        }

        /// <summary>
        /// Gets the MessageBox property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NouvemMessageBoxViewModel MessageBox
        {
            get
            {
                return MessageBoxStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the MessageBox property.
        /// </summary>
        public static void ClearMessageBox()
        {
            if (messageBox != null)
            {
                messageBox.Cleanup();
                messageBox = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the MessageBox property.
        /// </summary>
        public static void CreateMessageBox()
        {
            if (messageBox == null)
            {
                messageBox = new NouvemMessageBoxViewModel();
            }
        }

        #endregion
    }
}

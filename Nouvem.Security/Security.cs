﻿// -----------------------------------------------------------------------
// <copyright file="Security.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Security
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Class responsible for all security/cryptography services.
    /// </summary>
    public static class Security
    {
        #region field

        /// <summary>
        /// The encryption key.
        /// </summary>
        private static readonly byte[] EncryptionKey = { 44, 67, 11, 24, 107, 111, 33, 244, 14, 3, 167, 34, 18, 198, 4, 8, 78, 88, 28, 77, 99, 28, 23, 18, 108, 96, 78, 93, 133, 22, 109, 93 };

        /// <summary>
        /// The initialisation vector.
        /// </summary>
        private static readonly byte[] InitialisationVector = { 82, 21, 88, 189, 177, 109, 152, 16, 87, 66, 102, 15, 109, 109, 33, 22 };

        #endregion

        #region public interface

        #region checksum

        /// <summary>
        /// Generates a hexidecimal string checksum over the input file.
        /// </summary>
        /// <param name="file">The file to generate a checksum value for.</param>
        /// <returns>A hexidecimal string checksum value.</returns>
        public static string GenerateChecksum(string file)
        {
            var md5Crypto = new MD5CryptoServiceProvider();
            var checksumBuilder = new StringBuilder();

            if (File.Exists(file))
            {
                // convert the file string to a byte array
                using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    // create the hash
                    md5Crypto.ComputeHash(stream);

                    // create the checksum string
                    md5Crypto.Hash.ToList().ForEach(x => checksumBuilder.Append(string.Format("{0:X2}", x)));
                }
            }

            return checksumBuilder.ToString();
        }

        /// <summary>
        /// Generates a hexidecimal string checksum over the input file.
        /// </summary>
        /// <param name="s">The string to generate a checksum value for.</param>
        /// <returns>A hexidecimal string checksum value.</returns>
        public static string GenerateStringChecksum(string s)
        {
            var md5Crypto = new MD5CryptoServiceProvider();
            var checksumBuilder = new StringBuilder();
            var stream = new UTF8Encoding().GetBytes(s);

            // create the hash
            md5Crypto.ComputeHash(stream);

            // create the checksum string
            md5Crypto.Hash.ToList().ForEach(x => checksumBuilder.Append(string.Format("{0:X2}", x)));
            return checksumBuilder.ToString();
        }

        #endregion

        #region encryption

        /// <summary>
        /// Encrpts the input file.
        /// </summary>
        /// <param name="xDoc">The xml document to encrypt.</param>
        /// <param name="filePath">The document file path.</param>
        public static void EncryptXmlFile(XDocument xDoc, string filePath)
        {
            using (var rijndael = new RijndaelManaged())
            {
                // Create the symmetric encryptor object with the Key and initialization vector
                rijndael.Key = EncryptionKey;
                rijndael.IV = InitialisationVector;
                var encryptor = rijndael.CreateEncryptor(rijndael.Key, rijndael.IV);

                using (var ws = File.Open(filePath, FileMode.Create))
                {
                    // write the encrytion to file
                    using (var stream = new CryptoStream(ws, encryptor, CryptoStreamMode.Write))
                    {
                        // save the encrypted file
                        xDoc.Save(stream);
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts an xml document.
        /// </summary>
        /// <param name="xDoc">The xml document to decrypt.</param>
        /// <param name="filePath">The file path of the document.</param>
        public static void DecryptXmlFile(ref XDocument xDoc, string filePath)
        {
            using (var rijndael = new RijndaelManaged())
            {
                rijndael.Key = EncryptionKey;
                rijndael.IV = InitialisationVector;
                var decryptor = rijndael.CreateDecryptor(rijndael.Key, rijndael.IV);

                using (var filestream = File.OpenRead(filePath))
                {
                    // write the encrytion to file
                    using (var stream = new CryptoStream(filestream, decryptor, CryptoStreamMode.Read))
                    {
                        using (var streamReader = new System.Xml.XmlTextReader(stream))
                        {
                            xDoc = XDocument.Load(streamReader);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Encrypts a string to stream.
        /// </summary>
        /// <param name="stringToEncrypt">The string to encrypt.</param>
        /// <returns>An encrypted stream of data.</returns>
        public static byte[] EncryptString(string stringToEncrypt)
        {
            var encoding = new UnicodeEncoding();
            var key = InitialisationVector;

            using (var rijndael = new RijndaelManaged())
            using (var stream = new MemoryStream())
            using (var encryptor = rijndael.CreateEncryptor(key, key))
            using (var crypto = new CryptoStream(stream, encryptor, CryptoStreamMode.Write))
            {
                var encryptedString = encoding.GetBytes(stringToEncrypt);
                crypto.Write(encryptedString, 0, encryptedString.Length);
                crypto.FlushFinalBlock();
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Decrypts an encrypted steam to it's original string.
        /// </summary>
        /// <param name="streamToDecrypt">The steam to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string DecryptString(byte[] streamToDecrypt)
        {
            if (streamToDecrypt == null)
            {
                return string.Empty;
            }

            var encoding = new UnicodeEncoding();
            var key = InitialisationVector;

            using (var rijndeal = new RijndaelManaged())
            using (var stream = new MemoryStream())
            using (var decryptor = rijndeal.CreateDecryptor(key, key))
            using (var crypto = new CryptoStream(stream, decryptor, CryptoStreamMode.Write))
            {
                crypto.Write(streamToDecrypt, 0, streamToDecrypt.Length);
                crypto.FlushFinalBlock();
                return encoding.GetString(stream.ToArray());
            }
        }

        /// <summary>
        /// Creates an md5 hash from the input string.
        /// </summary>
        /// <param name="input">The string to hash.</param>
        /// <returns>A md5 hash of the input string.</returns>
        public static string CreateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            // Convert byte array to hex string
            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        #endregion

        #region password

        /// <summary>
        /// Generates a 4 digit monthy password.
        /// </summary>
        /// <returns>A 4 digit monthly password.</returns>
        public static string GetNouvemPassword()
        {
            var year = DateTime.Now.Year;
            var month = DateTime.Now.Month;
            var temp = year % 2 == 0 ? (year / month) / 4 : (year / month) / 7;
            var temp2 = month % 2 == 0 ? (temp * 93) : (temp * 54);

            return new string(temp2.ToString().ToCharArray().Reverse().ToArray()).PadRight(4, '9').Substring(0, 4);
        }

        #endregion

        #endregion
    }
}


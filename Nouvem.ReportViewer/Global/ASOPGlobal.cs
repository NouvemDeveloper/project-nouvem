﻿// -----------------------------------------------------------------------
// <copyright file="ASOPGlobal.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Global
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using AutoSaleOrderProcessor.BusinessLogic;
    using AutoSaleOrderProcessor.Model.DataLayer;
    using AutoSaleOrderProcessor.Properties;
    using Nouvem.Logging;

    static class ASOPGlobal
    {
        /// <summary>
        /// The application logger reference.
        /// </summary>
        private static Logger Log = new Logger();

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        internal static Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the product look up.
        /// </summary>
        internal static IList<Product> ProductLookUp { get; set; }

        /// <summary>
        /// Global data processing.
        /// </summary>
        internal static void ApplicationStart()
        {
           // Customer = DataManager.Instance.GetCustomer(Settings.Default.CustomerCode) ?? new Customer();
            //ProductLookUp = new List<Product>();
            RegisterUnhandledExceptionHandler();
            //CheckDatabaseConnection();
        }

        /// <summary>
        /// Method that registers for any unhandled exceptions. 
        /// </summary>
        private static void RegisterUnhandledExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                var ex = e.ExceptionObject as Exception;

                // log the unhandled exception.
                Log.LogError(
                    typeof(ASOPGlobal),
                    string.Format("RegisterUnhandledExceptionHandler(): Message:{0}  Inner Exception:{1}  Stack Trace:{2}", ex.Message, ex.InnerException, ex.StackTrace));

                throw ex;
            };
        }

        /// <summary>
        /// Checks the db connectivity.
        /// </summary>
        private static void CheckDatabaseConnection()
        {
            var manager = DataManager.Instance;
            if (!manager.CheckDatabaseConnection())
            {
                System.Windows.MessageBox.Show("Connection to the database failed. Please check your settings");
            }
        }
    }
}

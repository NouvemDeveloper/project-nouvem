﻿// -----------------------------------------------------------------------
// <copyright file="TabSizeConverter.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Global.Converter
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Class which converts tab sizes to fill the parent control width.
    /// </summary>
    public class TabSizeConverter : IMultiValueConverter
    {
        /// <summary>
        /// Gets the width for each tab control item, so as to collectively fill the the entire tab control.
        /// </summary>
        /// <param name="values">The tab control items.</param>
        /// <param name="targetType">The double target type</param>
        /// <param name="parameter">null parameter</param>
        /// <param name="culture">Localisation culture</param>
        /// <returns>The width to be assigned to each tab control item.</returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var tabControl = values[0] as TabControl;
            var width = tabControl.ActualWidth / tabControl.Items.Count;

            // Subtract 1, otherwise we could overflow to two rows.
            return (width <= 1) ? 0 : (width - 1);
        }

        /// <summary>
        /// Gets the width for each tab control item, so as to collectively fill the the entire tab control.
        /// </summary>
        /// <param name="value">The tab control items.</param>
        /// <param name="targetTypes">The double target type</param>
        /// <param name="parameter">null parameter</param>
        /// <param name="culture">Localisation culture</param>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}


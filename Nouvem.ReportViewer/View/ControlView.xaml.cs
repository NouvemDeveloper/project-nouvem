﻿// -----------------------------------------------------------------------
// <copyright file="ControlView.cs="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.View
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for CrudView.xaml
    /// </summary>
    public partial class ControlView : UserControl
    {
        public ControlView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="MainView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Net;
using AutoSaleOrderProcessor.Model.BusinessObject;

namespace AutoSaleOrderProcessor.View
{
    using System;
    using System.Windows.Controls;
    using AutoSaleOrderProcessor.Properties;
    using GalaSoft.MvvmLight.Messaging;


    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            
        }
    }
}

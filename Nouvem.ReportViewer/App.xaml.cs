﻿// -----------------------------------------------------------------------
// <copyright file="App.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor
{
    using System.Windows;
    using System.Windows.Threading;
    using AutoSaleOrderProcessor.Global;
    using Nouvem.Logging;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// The application logging reference.
        /// </summary>
        private readonly Logger log = new Logger();

        /// <summary>
        /// Overriden base startup.
        /// </summary>
        /// <param name="e">Start up event argument.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ASOPGlobal.ApplicationStart();
        }

        /// <summary>
        /// Handler for the application unhandled exceptions.
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">The event args</param>
        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // log it
            this.log.LogError(this.GetType(), string.Format("OnDispatcherUnhandledException()  Message: {0}, Inner Exception: {1},  Stack Trace: {2}", e.Exception.Message, e.Exception.InnerException, e.Exception.StackTrace));
        }
    }
}

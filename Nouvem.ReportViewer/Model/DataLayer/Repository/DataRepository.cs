﻿// -----------------------------------------------------------------------
// <copyright file="DataRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoSaleOrderProcessor.Global;
    using Nouvem.Logging;

    public class DataRepository
    {
        /// <summary>
        /// The application logger.
        /// </summary>
        private readonly Logger log = new Logger();

        /// <summary>
        /// Checks the connectivity to the db.
        /// </summary>
        /// <returns>A falg, indicating a successful connection or not.</returns>
        public bool CheckDatabaseConnection()
        {
            this.log.LogInfo(this.GetType(), "Attempting to connect to the db");
            try
            {
                using (var entities = new ASOPEntities())
                {
                    var test = entities.Customers.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            this.log.LogInfo(this.GetType(), "Connection successful");
            return true;
        }
    }
}

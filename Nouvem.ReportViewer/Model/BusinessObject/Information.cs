﻿// -----------------------------------------------------------------------
// <copyright file="Information.cs" company="Nouvem Limited">
// Copyright (c)Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Model.BusinessObject
{
    using System;
    using AutoSaleOrderProcessor.Model.Enum;
    using AutoSaleOrderProcessor.ViewModel;

    /// <summary>
    /// Information item from the status bar.
    /// </summary>
    public class Information
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Information"/> class.
        /// </summary>
        protected Information()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets or sets the date and time stamp for the entry.
        /// </summary>
        public DateTime EntryDateTime { get; set; }

        /// <summary>
        /// Gets or sets the category of the message.
        /// </summary>
        public MessageType Category { get; set; }

        /// <summary>
        /// Gets or sets the text message of the entry.
        /// </summary>
        public string Text { get; set; }

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="Information"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="Information"/> object.</returns>
        public static Information CreateNewInformation()
        {
            return new Information();
        }

        /// <summary>
        /// Create a new empty <see cref="Information"/> with the parameters specified.
        /// </summary>
        /// <param name="category">The category of the message (error, information).</param>
        /// <param name="information">The information to be inserted.</param>
        /// <returns>A new <see cref="Information"/> object with the parameters specified.</returns>
        public static Information CreateInformation(MessageType category, string information)
        {
            return new Information
            {
                EntryDateTime = DateTime.Now,
                Category = category,
                Text = information
            };
        }

        #endregion

        #endregion
    }
}

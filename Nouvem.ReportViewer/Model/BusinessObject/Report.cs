﻿// -----------------------------------------------------------------------
// <copyright file="Report.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Model.BusinessObject
{
    using System;

    public class Report
    {
        /// <summary>
        /// The parent folder id.
        /// </summary>
        public int ParentID { get; set; }

        /// <summary>
        /// The node report id.
        /// </summary>
        public int NodeID { get; set; }

        /// <summary>
        /// The report name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The report path.
        /// </summary>
        public string Path { get; set; }
    }
}

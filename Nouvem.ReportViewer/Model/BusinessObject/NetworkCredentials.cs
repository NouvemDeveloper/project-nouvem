﻿// -----------------------------------------------------------------------
// <copyright file="Report.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Net;
using System.Security.Principal;


namespace AutoSaleOrderProcessor.Model.BusinessObject
{
    using System;

    public class NetworkReportServerCredentials 
    {
        private string _userName;
        private string _password;
        private string _domain;

        public NetworkReportServerCredentials(string userName, string password, string domain)
        {
            _userName = userName;
            _password = password;
            _domain = domain;
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;
            return false;
        }

        public WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public ICredentials NetworkCredentials
        {
            get { return new NetworkCredential(_userName, _password, _domain); }
        }
    }
}

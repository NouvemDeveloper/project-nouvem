﻿// -----------------------------------------------------------------------
// <copyright file="SystemMessageViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using AutoSaleOrderProcessor.Global;
    using AutoSaleOrderProcessor.Model.Enum;
    using AutoSaleOrderProcessor.Properties;
    using Nouvem.Logging;

    /// <summary>
    /// Class which models the system message view.
    /// </summary>
    public class SystemMessageViewModel : ViewModelBase
    {
        #region Fields

        /// <summary>
        /// The application logger.
        /// </summary>
        private Logger log = new Logger();

        /// <summary>
        /// The information to be displayed in the information/status bar.
        /// </summary>
        private ObservableCollection<SystemMessageViewModel> informationBar =
            new ObservableCollection<SystemMessageViewModel>();

        /// <summary>
        /// The time of the message.
        /// </summary>
        private string messageTime;

        /// <summary>
        /// The category of the entry.
        /// </summary>
        private MessageType category;

        /// <summary>
        /// The display message.
        /// </summary>
        private string messageToDisplay;

        /// <summary>
        /// the last record field.
        /// </summary>
        private SystemMessageViewModel lastRecord;

        /// <summary>
        /// The progress bar value.
        /// </summary>
        private int progressBarValue;

        /// <summary>
        /// The progress bar minimum value.
        /// </summary>
        private int progressBarMinValue;

        /// <summary>
        /// The progress bar maximum value.
        /// </summary>
        private int progressBarMaxValue;

        /// <summary>
        /// The progress bar error state.
        /// </summary>
        private bool progressBarError;

        /// <summary>
        /// The current time reference.
        /// </summary>
        private string currentTime;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMessageViewModel"/> class.
        /// </summary>
        public SystemMessageViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMessageViewModel"/> class.
        /// </summary>
        /// <param name="category">The category for the entry</param>
        /// <param name="messageTime">The message time.</param>
        /// <param name="message">The message to display.</param>
        public SystemMessageViewModel(DateTime messageTime, MessageType category, string message)
        {
            this.MessageTime = messageTime.ToString("HH:mm:ss");
            this.Category = category;
            this.MessageToDisplay = message;
        }

        #endregion

        #region Public Interface

        #region property

        /// <summary>
        /// Gets or sets the current time.
        /// </summary>
        public string CurrentTime
        {
            get
            {
                return this.currentTime;
            }

            set
            {
                this.currentTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the application version number.
        /// </summary>
        public string VersionNumber
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
      
        /// <summary>
        /// Gets or sets the last message.
        /// </summary>
        public SystemMessageViewModel LastRecord
        {
            get
            {
                return this.lastRecord;
            }

            set
            {
                this.lastRecord = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the records to be displayed on the information bar.
        /// </summary>
        public ObservableCollection<SystemMessageViewModel> StatusBarInformation
        {
            get
            {
                return this.informationBar;
            }

            set
            {
                if (value != null)
                {
                    this.informationBar = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Retrieve the system messages.
        /// </summary>
        /// <returns>Return the system messages.</returns>
        public ObservableCollection<SystemMessageViewModel> GetSystemMessages()
        {
            try
            {
                this.LastRecord = null;
                ObservableCollection<SystemMessageViewModel> records = null;

                var messages = from message in SystemMessage.GetSystemMessages()
                               select new SystemMessageViewModel(message.EntryDateTime, message.Category, message.Text);

                records = new ObservableCollection<SystemMessageViewModel>(messages);
                this.LastRecord = records.FirstOrDefault();
                return records;
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the message type to display.
        /// </summary>
        public string DisplayMessage
        {
            get
            {
                return this.Category == MessageType.Background
                       || this.Category == MessageType.Priority
                           ? Constant.Message
                           : Constant.Error;
            }
        }

        /// <summary>
        /// Gets or sets the time of the message.
        /// </summary>
        public string MessageTime
        {
            get
            {
                return this.messageTime;
            }

            set
            {
                this.messageTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the category for the message. For example, error.
        /// </summary>
        public MessageType Category
        {
            get
            {
                return this.category;
            }

            set
            {
                this.category = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the message to be displayed.
        /// </summary>
        public string MessageToDisplay
        {
            get
            {
                return this.messageToDisplay;
            }

            set
            {
                this.messageToDisplay = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to show the message history.
        /// </summary>
        public ICommand ShowMessageHistoryCommand { get; private set; }

        #endregion

        #endregion
    }
}



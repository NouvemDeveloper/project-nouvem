﻿// -----------------------------------------------------------------------
// <copyright file="ViewModelLocator.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Nouvem"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

namespace AutoSaleOrderProcessor.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        #region field

        /// <summary>
        /// The contact view model reference.
        /// </summary>
        private static MasterViewModel masterViewModel;

        /// <summary>
        /// The main view model reference.
        /// </summary>
        private static MainViewModel mainViewModel;

        /// <summary>
        /// The system message view model reference.
        /// </summary>
        private static SettingsViewModel settingsViewModel;
      
        /// <summary>
        /// The system message view model reference.
        /// </summary>
        private static SystemMessageViewModel systemMessageViewModel;
      
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            //ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            //SimpleIoc.Default.Register<MasterViewModel>();
        }

        #endregion

        #region property

        #region Master

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public static MasterViewModel MasterStatic
        {
            get
            {
                if (masterViewModel == null)
                {
                    CreateMaster();
                }

                //currentViewModel = masterViewModel;
                return masterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MasterViewModel Master
        {
            get
            {
                return MasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Master property.
        /// </summary>
        public static void ClearMaster()
        {
            if (masterViewModel != null)
            {
                masterViewModel.Cleanup();
                masterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Master property.
        /// </summary>
        public static void CreateMaster()
        {
            if (masterViewModel == null)
            {
                masterViewModel = new MasterViewModel();
            }
        }

        #endregion

        #region Main

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public static MainViewModel MainStatic
        {
            get
            {
                if (mainViewModel == null)
                {
                    CreateMain();
                }

                //currentViewModel = mainViewModel;
                return mainViewModel;
            }
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return MainStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Main property.
        /// </summary>
        public static void ClearMain()
        {
            if (mainViewModel != null)
            {
                mainViewModel.Cleanup();
                mainViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Main property.
        /// </summary>
        public static void CreateMain()
        {
            if (mainViewModel == null)
            {
                mainViewModel = new MainViewModel();
            }
        }

        #endregion

        #region Settings

        /// <summary>
        /// Gets the Settings property.
        /// </summary>
        public static SettingsViewModel SettingsStatic
        {
            get
            {
                if (settingsViewModel == null)
                {
                    CreateSettings();
                }

                //currentViewModel = settingsViewModel;
                return settingsViewModel;
            }
        }

        /// <summary>
        /// Gets the Settings property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public SettingsViewModel Settings
        {
            get
            {
                return SettingsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Settings property.
        /// </summary>
        public static void ClearSettings()
        {
            if (settingsViewModel != null)
            {
                settingsViewModel.Cleanup();
                settingsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Settings property.
        /// </summary>
        public static void CreateSettings()
        {
            if (settingsViewModel == null)
            {
                settingsViewModel = new SettingsViewModel();
            }
        }

        #endregion

        #region SystemMessage

        /// <summary>
        /// Gets the system message property.
        /// </summary>
        public static SystemMessageViewModel SystemMessageStatic
        {
            get
            {
                if (systemMessageViewModel == null)
                {
                    CreateSystemMessage();
                }

                return systemMessageViewModel;
            }
        }

        /// <summary>
        /// Gets the systemMessage property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public SystemMessageViewModel SystemMessage
        {
            get
            {
                return SystemMessageStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the systemMessage property.
        /// </summary>
        public static void ClearInformationBar()
        {
            if (systemMessageViewModel != null)
            {
                systemMessageViewModel.Cleanup();
                systemMessageViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the systemMessage property.
        /// </summary>
        public static void CreateSystemMessage()
        {
            if (systemMessageViewModel == null)
            {
                systemMessageViewModel = new SystemMessageViewModel();
            }
        }

        #endregion

        #endregion

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}
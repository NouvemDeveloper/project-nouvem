﻿// -----------------------------------------------------------------------
// <copyright file="SettingsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.ViewModel
{
    using System.Windows.Forms;
    using AutoSaleOrderProcessor.BusinessLogic;
    using AutoSaleOrderProcessor.Global;
    using AutoSaleOrderProcessor.Model.Enum;
    using AutoSaleOrderProcessor.Properties;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;

    public class SettingsViewModel : ViewModelBase
    {
        #region field

        /// <summary>
        /// The data manager.
        /// </summary>
        private DataManager dataManager = DataManager.Instance;

        /// <summary>
        /// The reporting services url.
        /// </summary>
        private string webServiceURL;

        /// <summary>
        /// The report server path.
        /// </summary>
        private string reportServerPath;

        /// <summary>
        /// The web serviceurl at load.
        /// </summary>
        private string webServiceURLOnLoad;

        /// <summary>
        /// The import file path at load.
        /// </summary>
        private string sharedFilePathOnLoad;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsViewModel"/> class.
        /// </summary>
        public SettingsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // Handle the control button selection.
            this.ControlButtonSelectedCommand = new RelayCommand<string>(this.ControlButtonSelectedCommandExecute);
            
            // Handle the load event.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            #endregion
        }

        #endregion

        #region public interface
      
        /// <summary>
        /// Gets or sets the reporting services url.
        /// </summary>
        public string WebServiceURL
        {
            get
            {
                return this.webServiceURL;
            }

            set
            {
                this.webServiceURL = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the report server path.
        /// </summary>
        public string ReportServerPath
        {
            get
            {
                return this.reportServerPath;
            }

            set
            {
                this.reportServerPath = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the control button command.
        /// </summary>
        public RelayCommand<string> ControlButtonSelectedCommand { get; set; }

        /// <summary>
        /// Gets the ui load event command.
        /// </summary>
        public RelayCommand OnLoadingCommand { get; set; }

        #endregion

        #region private

        /// <summary>
        /// Handler for the control button selection.
        /// </summary>
        /// <param name="mode">THe button selected (OK or Cancel).</param>
        private void ControlButtonSelectedCommandExecute(string mode)
        {
            if (mode.Equals(Constant.OK))
            {
                Settings.Default.WebServiceURL = this.webServiceURL ?? string.Empty;
                Settings.Default.ReportServerPath = this.reportServerPath ?? string.Empty;
                Settings.Default.Save();
                SystemMessage.Write(MessageType.Priority, "Settings have been saved");
                return;
            }

            // cancel
            this.WebServiceURL = this.webServiceURL;
            this.ReportServerPath = this.sharedFilePathOnLoad;
            SystemMessage.Write(MessageType.Priority, "Settings changes have been cancelled");
        }

        /// <summary>
        /// Handles the ui load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.WebServiceURL = Settings.Default.WebServiceURL;
            this.ReportServerPath = Settings.Default.ReportServerPath;
            this.webServiceURLOnLoad = this.webServiceURL;
            this.sharedFilePathOnLoad = this.reportServerPath;
        }

        #endregion
    }
}

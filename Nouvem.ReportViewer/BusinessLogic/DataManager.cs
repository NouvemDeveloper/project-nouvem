﻿// -----------------------------------------------------------------------
// <copyright file="DataManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using AutoSaleOrderProcessor.ViewModel;

namespace AutoSaleOrderProcessor.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using AutoSaleOrderProcessor.Global;
    using AutoSaleOrderProcessor.Model.BusinessObject;
    using AutoSaleOrderProcessor.Model.DataLayer;
    using AutoSaleOrderProcessor.Model.DataLayer.Repository;
    using AutoSaleOrderProcessor.Model.Enum;
    using AutoSaleOrderProcessor.Properties;
    using Nouvem.Logging;
    using Nouvem.Shared;

    /// <summary>
    /// Manager class for the data logic.
    /// </summary>
    public sealed class DataManager 
    {
        #region field

        /// <summary>
        /// The application logger reference.
        /// </summary>
        private Logger log = new Logger();

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly DataManager Manager = new DataManager();

        /// <summary>
        /// The system message repository.
        /// </summary>
        private readonly SystemMessageRepository systemMessageRepository = new SystemMessageRepository();

        /// <summary>
        /// The data repository.
        /// </summary>
        private readonly DataRepository dataRepository = new DataRepository();

        #endregion

        #region constructor

        private DataManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the data manager singleton.
        /// </summary>
        public static DataManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method
       
        /// <summary>
        /// Add a new message.
        /// </summary>
        /// <param name="category">The category of the message (eg: error)</param>
        /// <param name="message">The information message to log.</param>
        public void WriteMessage(MessageType category, string message)
        {
            this.systemMessageRepository.Write(category, message);
        }

        /// <summary>
        /// Gets the system messages.
        /// </summary>
        /// <returns>A collection of system messages.</returns>
        public IList<Information> GetMessages()
        {
            return this.systemMessageRepository.SystemMessages;
        }
       
        /// <summary>
        /// Checks the connectivity to the db.
        /// </summary>
        /// <returns>A falg, indicating a successful connection or not.</returns>
        public bool CheckDatabaseConnection()
        {
            return this.dataRepository.CheckDatabaseConnection();
        }

        #endregion

        #endregion

        #region private
      
        #endregion
    }
}


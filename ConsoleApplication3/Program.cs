﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using Nouvem.Identigen;
using Nouvem.AccountsIntegration;
using Nouvem.AIMS.BusinessLogic;
using Nouvem.AIMS.Service;
using Nouvem.Shared;
using Nouvem.FTrace;


namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //var dd = new List<int>();
            //dd.Add(1);

            //var cc = dd.Skip(2);
            //var xx = dd.Take(4);
            //var a = "Te st";
            //var c = a.IndexOf(" w");
            //var cw = a;

            //var reference= "WOOD001 LAM002";

            //var data = reference.Split(' ');
            //if (data.Length == 3)
            //{

            //}
            //else
            //{

            //}
            //var t = Convert.ToInt32("11011", 2);
            //var yg = t;
            //var decString = "0372042480406544";

            //var hexstring = "82005D09E407900F";
            //string strBinary = String.Join(String.Empty,
            //    hexstring.Select(
            //        c => Convert.ToString(Convert.ToInt64(c.ToString(), 16), 2).PadLeft(4, '0')
            //    )
            //);

            //var strCountry = Convert.ToInt64(strBinary.Substring(16,10),2).ToString();
            //if (strCountry.Length > 4)
            //{
            //    strCountry = strCountry.Substring(strCountry.Length - 4, 4);
            //}
            //else
            //{
            //    strCountry = strCountry.PadLeft(4, '0');
            //}


            //var strAnimal= Convert.ToInt64(strBinary.Substring(26, 38),2).ToString();
            //if (strAnimal.Length > 12)
            //{
            //    strAnimal = strAnimal.Substring(strAnimal.Length - 12, 12);
            //}
            //else
            //{
            //    strAnimal = strAnimal.PadLeft(12, '0');
            //}

            //var strDecimalEID = strCountry + strAnimal;
            //var strDecimalEIDISO = strCountry.Substring(strCountry.Length - 3, 3) + " " + strAnimal;
            //var lngRetagCount = Convert.ToInt64(strBinary.Substring(1, 3),2).ToString();
            //var lngSpecies = Convert.ToInt64(strBinary.Substring(4, 5),2).ToString();

            //var strFullISOEID = strDecimalEIDISO + " " +
            //                    lngRetagCount.PadLeft(1, '0') + " " +
            //                    lngSpecies.PadLeft(2, '0') + " " +
            //                    Convert.ToInt64(strBinary.Substring(9, 6),2).ToString().PadLeft(2, '0') + strBinary.Substring(15, 1);

            //var a = strFullISOEID;




            //decimal Debitvalue = 1156.5m;
            //decimal DEBITAMT = Convert.ToDecimal(string.Format("{0:F2}", Debitvalue));

            //decimal d = 100.1M;
            //decimal dc = decimal.Round(d, 2);
            ////decimal? a = 0.1m;
            //var dc = a.ToNullableDecimalFormatted(2);
            //var dddd = dc;
            //var aims = new Nouvem.AIMS.BusinessLogic.WSWrapper();
            //var t = new Nouvem.AIMS.Service.MovementRequestXmlHelper(new MovementRequest(),"","");
            //t.ParseXmlSheepResponse("", "");
            //aims.BCMSTestDataDefra();
            //var tt = new MovementRequestXmlHelper(new MovementRequest(),"","");
            //tt.ParseXmlBCMSValidationResponse("", "", true);
            //Console.ReadKey();
            //return;
            //var someString = "5637391C062211-1";

            //var t = new List<int?>();
            //t.Add(null);
            //t.Add(1);
            //t.Add(null);

            //var d = t.OrderBy(x => x).ToList();
            //var g = d;


            //var a = new FTraceConnect("","","","");
            //a.SendFile("");
            //a.PostXMLData("", "");

            //var aa = "-4.5";
            //var tt = aa.ToDecimal();
            //var ff = Math.Round(tt,0);
            //var sss = ff;
            //FindCore200();
            //var con = new Sage200();
            //con.Logout();
            //con.Connect("", "", "");
            //con.GetPartners("");
            //MyClass CC = null;
            //var d = CC?.Test;

            //var a = d;
            // var test = 1;
            // var test4 = "Yes";
            // var c = $"{test4} is not an issue. {test}";
            // var d = c;

            //Nouvem.AIMS.Model.Movement ct = null;
            // var yy = ct?.MoveFromID ?? string.Empty;

            //var aims = new Nouvem.AIMS.BusinessLogic.WSWrapper();
            //aims.QueryUKAnimalDetails("1", "1", "1", "1", "1", "1", "1", "1", "1");

            //decimal a = 0.01m;
            //var b = Math.Round(0.26, 2);

            // var c = b;

            //var ind = new Indicator("","","","","",Indicator.IndicatorModel.CSW20,"C:\\Nouvem\\Indicator\\Alibi", verifyChecksum:true);

            var sb = new StringBuilder();
            sb.AppendLine();

            var c = 2;
            var i = 1;

            while (i <= 30)
            {
                var line = string.Format("batchData.Attribute{0},", i);

                sb.AppendLine(line);
                i++;
                c++;
            }


            var filename = @"C:\Nouvem\DB\attributes.txt";
            File.WriteAllText(filename, sb.ToString());
        }

        public class MyClass
        {
            public int? Test { get; set; }

        }



        #region constants
        private const string REG_PATH = @"Software\Sage\MMS";
        private const string REGKEY_VALUE = "ClientInstallLocation";
        private const string DEFAULT_ASSEMBLY = "Sage.Common.dll";
        private const string ASSEMBLY_RESOLVER = "Sage.Common.Utilities.AssemblyResolver";
        private const string RESOLVER_METHOD = "GetResolver";
        #endregion constants


        /// <summary>
        /// Locates and invokes assemblies from the client folder at runtime.
        /// </summary>
        static void FindCore200()
        {
            // get registry info for Sage 200 server path
            string path = string.Empty;
            RegistryKey root = Registry.CurrentUser;
            RegistryKey key = root.OpenSubKey(REG_PATH);

            if (key != null)
            {
                object value = key.GetValue(REGKEY_VALUE);
                if (value != null)
                    path = value as string;
            }

            // refer to all installed assemblies based on location of default one
            if (string.IsNullOrEmpty(path) == false)
            {
                string commonDllAssemblyName = System.IO.Path.Combine(path, DEFAULT_ASSEMBLY);

                if (System.IO.File.Exists(commonDllAssemblyName))
                {
                    System.Reflection.Assembly defaultAssembly = System.Reflection.Assembly.LoadFrom(commonDllAssemblyName);
                    Type type = defaultAssembly.GetType(ASSEMBLY_RESOLVER);
                    MethodInfo method = type.GetMethod(RESOLVER_METHOD);
                    method.Invoke(null, null);
                }
            }
        }

        /// <summary>
        /// Launch the application's main code
        /// </summary>
        static void LaunchApplication()
        {
            // NOTE: Entering this function, .Net will attempt to load sage assemblies into the AppDomain.
            // Assembly resolution MUST be configured before entering this function.

            // -- Replace this code with your application startup code --
            // |                                                        |
            // V                                                        V

            //var app = new ClassReferencingSageObjects();
            //app.Run();

            // ^                                                        ^
            // |                                                        |
            // -- Replace this code with your application startup code --
        }


    }
}

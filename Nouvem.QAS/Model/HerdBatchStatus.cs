﻿// -----------------------------------------------------------------------
// <copyright file="HerdBatchStatus.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.QAS.Model
{
    using System;

    /// <summary>
    /// Class which represents the HerdStatus result returned from the WS batch validation call.
    /// </summary>
    public class HerdBatchStatus
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HerdBatchStatus"/> class.
        /// </summary>
        protected HerdBatchStatus()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets a value indicating the Herd No. submitted for checking.
        /// </summary>
        public string HerdNo { get; internal set; }

        /// <summary>
        /// Gets a value indicating the date of expiry of the producers certificate.
        /// </summary>
        public DateTime? CertValidUntil { get; internal set; }

        /// <summary>
        /// Gets a value indicating any error which has resulted from querying the WS.
        /// </summary>
        public string Error { get; internal set; }

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="HerdBatchStatus"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="HerdBatchStatus"/> object.</returns>
        public static HerdBatchStatus CreateNewHerdStatus()
        {
            return new HerdBatchStatus();
        }

        /// <summary>
        /// Create a new <see cref="HerdBatchStatus"/> with specific parameters.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="certValidUntil">The date of expiry of the producers certificate.</param>
        /// <param name="error">Any error which has resulted from querying the WS.</param>
        /// <returns>A new <see cref="HerdBatchStatus"/> object with the parameters specified.</returns>
        public static HerdBatchStatus CreateHerdStatus(
            string herdNo,
            DateTime? certValidUntil,
            string error)
        {
            return new HerdBatchStatus
            {
                HerdNo = herdNo,
                CertValidUntil = certValidUntil,
                Error = error
            };
        }

        #endregion

        #endregion
    }
}


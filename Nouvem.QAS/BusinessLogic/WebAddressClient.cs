﻿using System;

namespace Nouvem.QAS.BusinessLogic
{
    using System.Net;

    /// <summary>
    /// Class used to validate the web address.
    /// </summary>
    public class WebAddressClient 
    {
        #region Public Interface

        /// <summary>
        /// Method which validates whether or not a WS is available for a call.
        /// </summary>
        /// <param name="address">The address which will be called.</param>
        /// <returns>Returns a value which indicates whether or not the service is available.</returns>
        public bool IsAddressAvailable(string address)
        {
            try
            {
                var client = new WebClient();
                client.DownloadData(address);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}

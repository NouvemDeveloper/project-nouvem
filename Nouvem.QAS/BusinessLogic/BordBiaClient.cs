﻿using System;
using System.CodeDom;
using CASI;
using Nouvem.QAS.ie.bordbia.qas;
using Nouvem.QAS.Model;
using Nouvem.QAS.uk.co.everysite.casi;


namespace Nouvem.QAS.BusinessLogic
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Client class for calling the services.
    /// </summary>
    public class BordBiaClient
    {
        #region private

        private const string BeefAssurance = "0349EQH8NH.16LGK2UGGKG4I";
        private const string LambAssurance = "0349EQH8NH.16LGK2V42K04K";
        private const string BeefAndLambCollectionCenterAssurance = "2RRVTHNXTS.ARRVICUTDSG1D";
        private const string BeefAndLambMarketAssurance = "2RRVTHNXTS.ARRVHE8MUFH1B";
        private const string PigCollectionCenterAssurance = "2RRVTHNXTS.ARRVIN79J011E";
        private const string PigMarketAssurance = "2RRVTHNXTS.ARRVI6NA2E01C";
        private const string LivestockAssurance = "000HK277ZW.A5DCIL63164N";

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BordBiaClient"/> class.
        /// </summary>
        public BordBiaClient()
        {
        }

        #endregion

        #region Public Methods

        #region herd status

        /// <summary>
        /// Call the beef web service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <returns>An object describing the status of the herd.</returns>
        public HerdStatus CallUKQueryBeefWS(string username, string password, string herdNo, DateTime? checkDate)
        {
            // format must be xx/xxx/xxxx, so remove any BCMS sub holding if exists e.g. 24/527/0004-01
            if (herdNo.Length > 11)
            {
                herdNo = herdNo.Substring(0, 11);
            }

            if (!checkDate.HasValue)
            {
                checkDate = DateTime.Today;
            }

            var error = string.Empty;
            var status = string.Empty;
            DateTime? certValidUntil = null;

            var reqCol = new CASI.Remote.Tracking.ObjectCollection();
            reqCol.Add(new CASI.Remote.Tracking.Authentication { Username = username, Password = password });

            var request = new CASI.Remote.Tracking.Request();
            request.Tagged = false;
            request.Scheme = new ObjectReference("0349EQH8NH.16LGK2F6Z1C3E", ObjectType.Scheme);
            request.MembershipIdentifier = herdNo;
            request.CheckDate = (DateTime)checkDate;
            reqCol.Add(request);

            var response = new CASI.Remote.Tracking.ObjectCollection();
            reqCol.Fill(ref response);

            /* note - there may be occassions when 2 results are returned. This would happen if a supplier changed type 
             * e.g. changed from beef to pig. Therefore we need to check the validity against the type.*/
            foreach (var dbData in response)
            {
                if (dbData is CASI.Tracking.Response)
                {
                    var deptResponse = (dbData as CASI.Tracking.Response);
                    status = deptResponse.CertificateStatus;
                    if (status.Equals("VALID") || status.Replace(" ","").Equals("VALID(GRACE)"))
                    {
                        if (deptResponse.Enterprises.Contains(BeefAssurance) ||
                            deptResponse.Enterprises.Contains(LambAssurance))
                        {
                            error = string.Empty;
                            certValidUntil = deptResponse.ExpiryDate;
                            break;
                        }

                        error = "Invalid supplier enterprise";
                    }
                }
                else if (dbData is CASI.Tracking.ResponseError)
                {
                    error = (dbData as CASI.Tracking.ResponseError).Message;
                }
            }

            return HerdStatus.CreateHerdStatus(herdNo, string.Empty, certValidUntil, false, error, string.Empty);
        }

        /// <summary>
        /// Call the beef web service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <returns>An object describing the status of the herd.</returns>
        public HerdStatus CallQueryBeefWS(string username, string password, string herdNo)
        {
            var service = new ie.bordbia.qas.CheckHerdStatus();
            var result = new BlqasCertificationStatus();
            var localRetry = 0;

            // BM: TODO Remove hard coding
            while (localRetry < 3)
            {
                try
                {
                    result = service.QueryBeef(username, password, herdNo);
                    break;
                }
                catch (Exception ex)
                {
                    localRetry++;

                    if (localRetry >= 3)
                    {
                        result.Error =
                            string.Format("callQueryBeefWS(): Message:{0}, Exception: {1}, InnerException: {2}, StackTrace: :{3}",
                                           ex.Message, ex, ex.InnerException, ex.StackTrace);
                    }
                }
            }

            return HerdStatus.CreateHerdStatus(result.HerdNo, result.HerdOwner, result.CertValidUntil, result.NominatedByYourMeatPlant, result.Error, result.Scope);
        }

        /// <summary>
        /// Call the beef web service, passing in a batch of herdNo's.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <returns>An object describing the status of the herd.</returns>
        public IList<HerdBatchStatus> CallQueryBeefBatchWS(string username, string password, string herdNo)
        {
            var result = new List<HerdBatchStatus>();
            var service = new ie.bordbia.qas.CheckHerdStatus();
            var serviceResult = service.QueryBeefBatch(username, password, herdNo);

            if (serviceResult != null && serviceResult.Count() > 0)
            {
                result = (from herd in serviceResult
                          select HerdBatchStatus.CreateHerdStatus(herd.HerdNo, herd.CertValidUntil, string.Empty)).ToList();
            }

            return result;
        }

        /// <summary>
        /// Call the lamb web service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <returns>An object describing the status of the herd.</returns>
        public HerdStatus CallQueryLambWS(string username, string password, string herdNo)
        {
            var service = new ie.bordbia.qas.CheckHerdStatus();

            var result = service.QueryLamb(username, password, herdNo);

            return HerdStatus.CreateHerdStatus(result.HerdNo, result.HerdOwner, result.CertValidUntil, result.NominatedByYourMeatPlant, result.Error, result.Scope);
        }

        /// <summary>
        /// Call the lamb web service, passing in a batch of herdNo's.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <returns>An object describing the status of the herd.</returns>
        public IList<HerdBatchStatus> CallQueryLambBatchWS(string username, string password, string herdNo)
        {
            var result = new List<HerdBatchStatus>();
            var service = new ie.bordbia.qas.CheckHerdStatus();
            var serviceResult = service.QueryLambBatch(username, password, herdNo);

            if (serviceResult != null && serviceResult.Count() > 0)
            {
                result = (from herd in serviceResult
                          select HerdBatchStatus.CreateHerdStatus(herd.HerdNo, herd.CertValidUntil, string.Empty)).ToList();
            }

            return result;
        }

        #endregion

      

        #endregion
    }
}


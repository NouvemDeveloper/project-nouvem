﻿
using System.Data;
using CASI;
using Nouvem.QAS.BusinessLogic;
using Nouvem.QAS.Model;
using Nouvem.QAS.Properties;

namespace Nouvem.QAS.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;

    /// <summary>
    /// Wrapper class for the web service call.
    /// </summary>
    public class WSWrapper
    {
        #region Fields

        /// <summary>
        /// Instance of the service caller.
        /// </summary>
        private BordBiaClient webClientWrapper;

        /// <summary>
        /// Instance of the service caller used to check if a web address is available.
        /// </summary>
        private WebAddressClient webAddressWrapper;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WSWrapper"/> class.
        /// </summary>
        public WSWrapper()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the service caller used.
        /// </summary>
        public BordBiaClient WebClientWrapper
        {
            get
            {
                if (this.webClientWrapper == null)
                {
                    this.webClientWrapper = new BordBiaClient();
                }

                return this.webClientWrapper;
            }

            set
            {
                this.webClientWrapper = value;
            }
        }

        /// <summary>
        /// Gets or sets the service caller used to check if a web address is available.
        /// </summary>
        public WebAddressClient WebAddressWrapper
        {
            get
            {
                if (this.webAddressWrapper == null)
                {
                    this.webAddressWrapper = new WebAddressClient();
                }

                return this.webAddressWrapper;
            }

            set
            {
                this.webAddressWrapper = value;
            }
        }

        #endregion

        #region Public Interface

        #region Herd Status

        /// <summary>
        /// Method to query the beef service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <param name="fileName">The output path of the response stored as xml.</param>
        /// <param name="writeTofile">Flag to indicate if the output file should be generated.</param>
        /// <returns>HerdStatus object detailing the results of the WS query.</returns>
        public HerdStatus QueryUKBeefService(string username, string password, string herdNo, string fileName, DateTime? checkDate, bool writeTofile = true)
        {
            var herdStatus = HerdStatus.CreateNewHerdStatus();

            try
            {
                var url = Settings.Default.Nouvem_QAS_uk_co_everysite_casi_CASITrackingWebService;
                if (this.WebAddressWrapper.IsAddressAvailable(url))
                {
                    herdStatus = this.WebClientWrapper.CallUKQueryBeefWS(username, password, herdNo, checkDate);

                    if (writeTofile)
                    {
                        this.WriteToXml(herdStatus, fileName);
                    }
                }
                else
                {
                    herdStatus.HerdNo = herdNo;
                    herdStatus.Error = string.Format("The web service {0} is not available for querying.", url);
                }
            }
            catch (Exception ex)
            {
                herdStatus.HerdNo = herdNo;
                herdStatus.Error = string.Format("QueryBeefService(): {0}", ex.ToString());
            }

            return herdStatus;
        }


        /// <summary>
        /// Method to query the beef service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <param name="fileName">The output path of the response stored as xml.</param>
        /// <param name="writeTofile">Flag to indicate if the output file should be generated.</param>
        /// <returns>HerdStatus object detailing the results of the WS query.</returns>
        public HerdStatus QueryBeefService(string username, string password, string herdNo, string fileName, bool writeTofile = true)
        {
            var herdStatus = HerdStatus.CreateNewHerdStatus();


            try
            {
                var url = Settings.Default.Nouvem_QAS_ie_bordbia_qas_CheckHerdStatus;

                if (this.WebAddressWrapper.IsAddressAvailable(url))
                {
                    herdStatus = this.WebClientWrapper.CallQueryBeefWS(username, password, herdNo);

                    if (writeTofile)
                    {
                        this.WriteToXml(herdStatus, fileName);
                    }
                }
                else
                {
                    herdStatus.HerdNo = herdNo;
                    herdStatus.Error = string.Format("The web service {0} is not available for querying.", url);
                }
            }
            catch (Exception ex)
            {
                herdStatus.HerdNo = herdNo;
                herdStatus.Error = string.Format("QueryBeefService(): {0}", ex.ToString());
            }

            return herdStatus;
        }

        /// <summary>
        /// Method to query the beef (batch) service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNos">The herd numbers for which you want to run the check.</param>
        /// <returns>HerdStatus object detailing the results of the WS query.</returns>
        /// <remarks>Herd No and CertValidUntil are the only values returned from the web service for this call.</remarks>
        public IList<HerdBatchStatus> QueryBeefBatchService(string username, string password, string herdNos)
        {
            IList<HerdBatchStatus> batchHerdStatus = new List<HerdBatchStatus>();

            try
            {
                var url = Settings.Default.Nouvem_QAS_ie_bordbia_qas_CheckHerdStatus;

                if (this.WebAddressWrapper.IsAddressAvailable(url))
                {
                    batchHerdStatus = this.WebClientWrapper.CallQueryBeefBatchWS(username, password, herdNos);
                }
                else
                {
                    batchHerdStatus = (from herdNolocal in herdNos.Split(',').ToList()
                                       select HerdBatchStatus.CreateHerdStatus(herdNolocal, null, string.Format("Unable to validate herdNo {0} as the service is not accessible.", herdNolocal))).ToList();
                }
            }
            catch (Exception queryBeefBatchServiceEx)
            {
                batchHerdStatus = (from herdNolocal in herdNos.Split(',').ToList()
                                   select HerdBatchStatus.CreateHerdStatus(herdNolocal, null, queryBeefBatchServiceEx.ToString())).ToList();
            }

            return batchHerdStatus;
        }

        /// <summary>
        /// Method to query the lamb service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNo">The herd for which you want to run the check.</param>
        /// <param name="fileName">The output path of the response stored as xml.</param>
        /// <param name="writeTofile">Flag to indicate if the output file should be generated.</param>
        /// <returns>HerdStatus object detailing the results of the WS query.</returns>
        public HerdStatus QueryLambService(string username, string password, string herdNo, string fileName = null, bool writeTofile = true)
        {
            var herdStatus = HerdStatus.CreateNewHerdStatus();

            try
            {
                var url = Settings.Default.Nouvem_QAS_ie_bordbia_qas_CheckHerdStatus;

                if (this.WebAddressWrapper.IsAddressAvailable(url))
                {
                    herdStatus = this.WebClientWrapper.CallQueryLambWS(username, password, herdNo);

                    if (writeTofile)
                    {
                        this.WriteToXml(herdStatus, fileName);
                    }
                }
                else
                {
                    herdStatus.HerdNo = herdNo;
                    herdStatus.Error = string.Format("The web service {0} is not available for querying.", url);
                }
            }
            catch (Exception queryLambServiceEx)
            {
                herdStatus.HerdNo = herdNo;
                herdStatus.Error = queryLambServiceEx.ToString();
            }

            return herdStatus;
        }

        /// <summary>
        /// Method to query the lamb (batch) service.
        /// </summary>
        /// <param name="username">The username for accessing the service.</param>
        /// <param name="password">The password for accessing the service.</param>
        /// <param name="herdNos">The herd numbers for which you want to run the check.</param>
        /// <returns>HerdStatus object detailing the results of the WS query.</returns>
        /// <remarks>Herd No and CertValidUntil are the only values returned from the web service for this call.</remarks>
        public IList<HerdBatchStatus> QueryLambBatchService(string username, string password, string herdNos)
        {
            IList<HerdBatchStatus> batchHerdStatus = new List<HerdBatchStatus>();

            try
            {
                var url = Settings.Default.Nouvem_QAS_ie_bordbia_qas_CheckHerdStatus;

                if (this.WebAddressWrapper.IsAddressAvailable(url))
                {
                    batchHerdStatus = this.WebClientWrapper.CallQueryLambBatchWS(username, password, herdNos);
                }
                else
                {
                    batchHerdStatus = (from herdNolocal in herdNos.Split(',').ToList()
                                       select HerdBatchStatus.CreateHerdStatus(herdNolocal, null, string.Format("Unable to validate herdNo {0} as the service is not accessible.", herdNolocal))).ToList();
                }
            }
            catch (Exception queryLambBatchServiceEx)
            {
                batchHerdStatus = (from herdNolocal in herdNos.Split(',').ToList()
                                   select HerdBatchStatus.CreateHerdStatus(herdNolocal, null, queryLambBatchServiceEx.ToString())).ToList();
            }

            return batchHerdStatus;
        }

        #endregion

        #region QA Dispatch

      


        #endregion

        #endregion

        #region XmlSerializer

        /// <summary>
        /// Write the response out to an xml file.
        /// </summary>
        /// <param name="herdStatus">The herd status object to write to xml.</param>
        /// <param name="fileName">The name of the output file.</param>
        /// <returns>Flag to indicate if the write was successful.</returns>
        private bool WriteToXml(HerdStatus herdStatus, string fileName)
        {
            bool isWriteSuccessful = false;

            try
            {
                // Write the file to the file system if necessary.
                if (!File.Exists(fileName) && herdStatus != null)
                {
                    var serializer = new XmlSerializer(herdStatus.GetType());
                    using (var stream = new FileStream(fileName, FileMode.Create))
                    {
                        serializer.Serialize(stream, herdStatus);
                        isWriteSuccessful = true;
                    }
                }
            }
            catch (Exception writeToXmlEx)
            {
                //herdStatus.Error = writeToXmlEx.ToString();
            }

            return isWriteSuccessful;
        }

        #endregion
    }
}

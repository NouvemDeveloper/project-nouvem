﻿// -----------------------------------------------------------------------
// <copyright file="LicenseValidator.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Licencing
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using Nouvem.Licencing.Properties;
    using Portable.Licensing;
    using Portable.Licensing.Validation;

    /// <summary>
    /// Class which contains the licence parsing and validation functionality.
    /// </summary>
    public class LicenseValidator
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseValidator"/> class.
        /// </summary>
        protected LicenseValidator()
        {
        }
      
        #endregion

        #region public interface

        #region creation

        /// <summary>
        /// Method that creates a new license validator.
        /// </summary>
        /// <returns>A new license validator.</returns>
        public static LicenseValidator CreateNew()
        {
            return new LicenseValidator();
        }

        #endregion

        #region method

        /// <summary>
        /// Method that validates a license.
        /// </summary>
        /// <param name="licenseFile">The path to the license file.</param>
        /// <param name="standardLicense">The type of license file to check.</param>
        /// <returns>A license detail object.</returns>
        /// <remarks>If the returned LicenseDetail ValidationErrors collection is not empty, then invalidate the licence client side.</remarks>
        public LicenseDetail ImportLicence(string licenseFile, bool standardLicense = true)
        {
            var publicKey = string.Empty;
            var licenceDetail = this.ReadLicence(licenseFile, ref publicKey, standardLicense);

            if (licenceDetail.ValidationErrors.Any())
            {
                return licenceDetail;
            }

            foreach (var error in this.ValidateLicence(licenseFile, publicKey, standardLicense))
            {
                licenceDetail.ValidationErrors.Add(error);
            }

            return licenceDetail;
        }

        #endregion

        #region private

        /// <summary>
        /// Read and parse the licence contents.
        /// </summary>
        /// <param name="licenseFile">The licence file to read.</param>
        /// <param name="publicKey">The licence public key.</param>
        /// <param name="standardLicense">The licence type.</param>
        /// <returns>A parsed licence details object.</returns>
        private LicenseDetail ReadLicence(string licenseFile, ref string publicKey, bool standardLicense)
        {
            var detail = new LicenseDetail();
            var licenseModules = new Dictionary<string, string>();

            try
            {
                var localLicense = File.ReadAllText(licenseFile);
                var xDoc = XDocument.Parse(localLicense);

                var itemLicenseElements = xDoc.Descendants("License");
                detail = (from itemLicense in itemLicenseElements
                          select new LicenseDetail
                          {
                              LicenseKey = itemLicense.Element("Id").Value,
                              Type = itemLicense.Element("Type").Value,
                              ExpiryDate = itemLicense.Element("Expiration").Value,
                              Quantity = itemLicense.Element("Quantity").Value,
                              Customer = itemLicense.Element("Customer").Element("Name").Value,
                              Email = itemLicense.Element("Customer").Element("Email").Value,
                              Signature = itemLicense.Element("Signature").Value
                          }).FirstOrDefault();

                // add the modules.
                var moduleElements = itemLicenseElements.Descendants("ProductFeatures").Elements();
                moduleElements.ToList().ForEach(x => licenseModules.Add(x.FirstAttribute.ToString(), x.Value));

                // get the non-authorisations first.
                foreach (var module in licenseModules.Where(x => !x.Key.Contains(Constant.Authorisation) && !x.Key.Contains(Constant.Type)))
                {
                    if (module.Key == Constant.ValidFrom)
                    {
                        detail.ValidFrom = this.ToDate(module.Value);
                        continue;
                    }

                    if (module.Key == Constant.ValidTo)
                    {
                        detail.ValidTo = this.ToDate(module.Value);
                        continue;
                    }

                    if (module.Key == Constant.NoOfDays)
                    {
                        detail.NoOfDays = Convert.ToInt32(module.Value);
                        continue;
                    }

                    if (module.Key == Constant.IssueDate)
                    {
                        detail.IssueDate = this.ToDate(module.Value);
                        continue;
                    }

                    if (module.Key == Constant.MessageContent)
                    {
                        detail.MessageContent = module.Value;
                        continue;
                    }

                    if (module.Key == Constant.PublicKey)
                    {
                        publicKey = module.Value;
                        continue;
                    }

                    if (module.Key == Constant.Upgrade)
                    {
                        detail.UpgradedFromLicenceKey = module.Value;
                    }
                }

                // get the types.
                foreach (var module in licenseModules.Where(x => x.Key.Contains(Constant.Type)))
                {
                    detail.Types.Add(this.ParseType(module.Value));
                }

                // get the authorisations.
                foreach (var module in licenseModules.Where(x => x.Key.Contains(Constant.Authorisation)))
                {
                   detail.Authorisations.Add(this.ParseAuthorisation(module.Value));
                }
            }
            catch (Exception ex)
            {
                detail.ValidationErrors.Add(ex.Message, "Please contact your administrator for further assistance");
            }

            return detail;
        }

        /// <summary>
        /// Validates the licence file. Any tampering with it will throw a validation error here.
        /// </summary>
        /// <param name="licenceFile">The licence file to validate.</param>
        /// <param name="publicKey">The public licence key.</param>
        /// <param name="standardLicence">The licence type.</param>
        /// <returns>A collection of validation issues, or an empty collection if validation ok.</returns>
        private IEnumerable<KeyValuePair<string, string>> ValidateLicence(string licenceFile, string publicKey, bool standardLicence)
        {
            var failures = new Dictionary<string, string>();
            var licenceType = standardLicence ? LicenseType.Standard : LicenseType.Trial;

            try
            {
                var license = License.Load(File.ReadAllText(licenceFile));

                var validationFailures = license.Validate()
                    .ExpirationDate()
                    .When(lic => lic.Type == licenceType)
                    .And()
                    .Signature(publicKey)
                    .AssertValidLicense();

                foreach (var validationfailure in validationFailures)
                {
                    failures.Add(validationfailure.Message, validationfailure.HowToResolve);
                }
            }
            catch (Exception ex)
            {
                failures.Add(ex.Message, Message.ContactNouvem);
            }

            return failures;
        }

        /// <summary>
        /// Convert a string date to datetime format.
        /// </summary>
        /// <param name="source">The string date to convert.</param>
        /// <returns>A formatted date.</returns>
        private DateTime ToDate(string source)
        {
            DateTime date;

            string[] formats =
                {
                    "dd/MM/yyyy",
                    "dd/M/yyyy", 
                    "d/M/yyyy", 
                    "d/MM/yyyy", 
                    "dd/MM/yy", 
                    "dd/M/yy", 
                    "d/M/yy", 
                    "d/MM/yy", 
                    "yyyy-MM-dd", 
                    "M/d/yyyy h:mm:ss tt", 
                    "dd/MM/yyyy h:mm:ss", 
                    "d/M/yyyy hh:mm:ss", 
                    "dd/MM/yyyy hh:mm:ss", 
                    "dd/MM/yyyy HH:mm:ss", 
                    "yyyyMMdd"
                };

            if (DateTime.TryParseExact(source, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                return date;
            }

            return date;
        }

        /// <summary>
        /// Parses a string containing an authorisation and it's value.
        /// </summary>
        /// <param name="authorisation">The authorisation/value to parse.</param>
        /// <returns>A parsed tuple(authorisation and value).</returns>
        private Tuple<string, string, string> ParseAuthorisation(string authorisation)
        {
            Tuple<string, string, string> localAuthorisation = null;
            var parsedAuthorisation = authorisation.Split('|');
            if (parsedAuthorisation.Count() == 3)
            {
                localAuthorisation = Tuple.Create(parsedAuthorisation[0], parsedAuthorisation[1], parsedAuthorisation[2]);
            }

            return localAuthorisation;
        }

        /// <summary>
        /// Parses a string containing an licence type and it's number of licences.
        /// </summary>
        /// <param name="type">The licence type/number to parse.</param>
        /// <returns>A parsed keyvaluepair(type and number).</returns>
        private KeyValuePair<string, int> ParseType(string type)
        {
            var keyValue = new KeyValuePair<string, int>();
            var parsedType = type.Split('|');
            if (parsedType.Count() == 2)
            {
                keyValue = new KeyValuePair<string, int>(parsedType[0], Convert.ToInt32(parsedType[1]));
            }

            return keyValue;
        }

        #endregion

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="LicenceAuthorisation.cs" company="Nouvem Technology">
// Copyright (c) Nouvem technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace LicenceGenerator
{
    /// <summary>
    /// Class which models the licence authorisations.
    /// </summary>
    public class LicenceAuthorisation
    {
        /// <summary>
        /// The authorisation entity reference.
        /// </summary>
        //public NouAuthorisation Authorisation { get; set; }

        /// <summary>
        /// Full access flag.
        /// </summary>
        public bool FullAccess { get; set; }
        
        /// <summary>
        /// No access flag.
        /// </summary>
        public bool NoAccess { get; set; }

        /// <summary>
        /// Read only access flag.
        /// </summary>
        public bool ReadOnly { get; set; }
    }
}

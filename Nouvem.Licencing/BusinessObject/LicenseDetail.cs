﻿// -----------------------------------------------------------------------
// <copyright file="LicenseDetail.cs" company="DEM Machines Limited">
// Copyright (c) Nouvem Technology Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Licencing
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class which models the license data.
    /// </summary>
    public class LicenseDetail
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseDetail"/> class.
        /// </summary>
        public LicenseDetail()
        {
            this.Authorisations = new List<Tuple<string, string, string>>();
            this.Types = new Dictionary<string, int>();
            this.ValidationErrors = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets the license id.
        /// </summary>
        public string LicenseKey { get; set; }

        /// <summary>
        /// Gets or sets the license type.
        /// </summary>
        public string Type{ get; set; }

        /// <summary>
        /// Gets or sets the license expiry date.
        /// </summary>
        public string ExpiryDate{ get; set; }

        /// <summary>
        /// Gets or sets the license id.
        /// </summary>
        public string Quantity { get; set; }

        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string Customer { get; set; }

        /// <summary>
        /// Gets or sets the client email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the license signature.
        /// </summary>
        public string Signature { get; set; }

        /// <summary>
        /// Gets or sets the license message content.
        /// </summary>
        public string MessageContent { get; set; }

        /// <summary>
        /// Gets or sets the license valid from date.
        /// </summary>
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// Gets or sets the license valid to date.
        /// </summary>
        public DateTime ValidTo { get; set; }

        /// <summary>
        /// Gets or sets the license issue date.
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// Gets or sets the license import date.
        /// </summary>
        public DateTime ImportDate { get; set; }

        /// <summary>
        /// Gets or sets the license number of days.
        /// </summary>
        public int NoOfDays { get; set; }

        /// <summary>
        /// Gets or sets the public key.
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// Gets or sets the private key.
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets the licence key that is being upgraded from.
        /// </summary>
        public string UpgradedFromLicenceKey { get; set; }

        /// <summary>
        /// Gets or sets the licence file.
        /// </summary>
        public Byte[] File { get; set; }

        /// <summary>
        /// Gets or sets the product authorisations.
        /// </summary>
        public IList<Tuple<string, string, string>> Authorisations { get; set; }

        /// <summary>
        /// Gets or sets the licence types.
        /// </summary>
        public IDictionary<string, int> Types { get; set; }

        /// <summary>
        /// Gets or sets the license validation errors.
        /// </summary>
        public IDictionary<string, string> ValidationErrors { get; set; }
    }
}

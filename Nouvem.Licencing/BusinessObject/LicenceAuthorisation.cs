﻿// -----------------------------------------------------------------------
// <copyright file="LicenceAuthorisation.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Licencing.BusinessObject
{
    public class LicenceAuthorisation
    {
        /// <summary>
        /// The authorisation rule.
        /// </summary>
        public string Authorisation { get; set; }

        /// <summary>
        /// The authorisation rule value.
        /// </summary>
        public string AuthorisationValue { get; set; }
    }
}

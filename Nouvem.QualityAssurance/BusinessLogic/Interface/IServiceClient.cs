﻿// -----------------------------------------------------------------------
// <copyright file="IServiceClient.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.QualityAssurance.BusinessLogic.Interface
{
    using System.Collections.Generic;
    using Nouvem.QualityAssurance.Model.BusinessObject;

    /// <summary>
    /// Interface which defines the methods which will interact with quality assurance validation service for a herd.
    /// </summary>
    public interface IServiceClient
    {
        /// <summary>
        /// Validate the herd number using the quality assurance service.
        /// </summary>
        /// <param name="herdNumber">The herd number to validate.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="herdCertificateHistory">The list of certificate approvals found locally for the herd number.</param>
        /// <returns>The certification detail for the herd number.</returns>
        /// <remarks>TODO: Set username and password in the configuration.</remarks>
        BordBiaCertification ValidateRedTractorHerdNumber(string herdNumber, string username, string password,
            DateTime? dateToCheck, int retryAttempts = 3, bool serviceDisabled = false);

        /// <summary>
        /// Validate the herd number using the quality assurance service.
        /// </summary>
        /// <param name="herdNumber">The herd number to validate.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="herdCertificateHistory">The list of certificate approvals found locally for the herd number.</param>
        /// <returns>The certification detail for the herd number.</returns>
        /// <remarks>TODO: Set username and password in the configuration.</remarks>
        BordBiaCertification ValidateHerdNumber(string herdNumber, string username, string password, IList<CertificateHistory> herdCertificateHistory, int retryAttempts = 3, bool serviceDisabled = false);

        /// <summary>
        /// Validate the herd number only using the quality assurance service.
        /// </summary>
        /// <param name="herdNumber">The herd number to validate.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="herdCertificateHistory">The list of certificate approvals found locally for the herd number.</param>
        /// <returns>The certification detail for the herd number.</returns>
        /// <remarks>TODO: Set username and password in the configuration.</remarks>
        Nouvem.QualityAssurance.ie.bordbia.qas.BlqasCertificationStatus ValidateHerdNumberOnly(string herdNumber, string username, string password, IList<CertificateHistory> herdCertificateHistory);
    }
}


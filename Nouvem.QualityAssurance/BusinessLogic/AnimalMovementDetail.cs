﻿// -----------------------------------------------------------------------
// <copyright file="AnimalMovementDetail.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.QualityAssurance.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.QualityAssurance.BusinessLogic.Interface;
    using Nouvem.QualityAssurance.Model.BusinessObject;

    /// <summary>
    /// Class which models the animal movements it's farm residency.
    /// </summary>
    public class AnimalMovementDetail : IAnimalResidency
    {
        #region Fields

        /// <summary>
        /// The default collection of animal movements.
        /// </summary>
        private IList<Movement> movements = new List<Movement>();

        /// <summary>
        /// The default collection of certificate histories.
        /// </summary>
        private IList<CertificateHistory> herdCertificateHistory = new List<CertificateHistory>();

        /// <summary>
        /// The caller for the service.
        /// </summary>
        private IServiceClient bordBiaServiceClient = new BordBiaCertification();

        /// <summary>
        /// The current farm certificate
        /// </summary>
        private BordBiaCertification currentFarmCertification = new BordBiaCertification();

        /// <summary>
        /// The factory herd certificate(usually the same as the presenting herd, but not always)
        /// </summary>
        private BordBiaCertification lairageHerdCertification = new BordBiaCertification();

        /// <summary>
        /// The global residency result.
        /// </summary>
        private ResidencyResult globalResidencyResult = new ResidencyResult();

        /// <summary>
        /// The previous farm certificate
        /// </summary>
        private BordBiaCertification previousFarmCertification = new BordBiaCertification();

        /// <summary>
        /// The farm certificate of the host farm from 3+ movements back
        /// </summary>
        private BordBiaCertification multipleFarmsToFarmCertification = new BordBiaCertification();

        /// <summary>
        /// The web service tome out error message
        /// </summary>
        private const string WebServiceTimeOut = "Web service time out";

        /// <summary>
        /// the service disabled field
        /// </summary>
        private bool bordBiaDisabled;

        public bool UKCheck { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimalMovementDetail"/> class.
        /// </summary>
        /// <param name="movements">The collection of movements for the animal.</param>
        /// <param name="herdCertificateHistory">The certification history for the herd number.</param>
        /// <param name="lotHerdNo">The herd number of the lot.</param>
        /// <param name="dateOfBirth">The date of birth of the animal.</param>
        /// <param name="eartag">The tag of the animal for which the residency is being calculated.</param>
        /// <param name="bordBiaUsername">The username used to access the quality approval service.</param>
        /// <param name="bordBiaPassword">The password used to access the quality approval service.</param>
        public AnimalMovementDetail(IList<Movement> movements, IList<CertificateHistory> herdCertificateHistory, string lotHerdNo, DateTime dateOfBirth, string eartag, string bordBiaUsername, string bordBiaPassword, bool bordBiaDisabled = false, bool usingUK = false)
        {
            this.UKCheck = usingUK;
            this.Movements = movements;
            this.HerdCertificateHistory = herdCertificateHistory;
            this.LotHerdNo = lotHerdNo;
            this.DateOfBirth = dateOfBirth;
            this.Eartag = eartag;
            this.BordBiaUsername = bordBiaUsername;
            this.BordBiaPassword = bordBiaPassword;
            this.bordBiaDisabled = bordBiaDisabled;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimalMovementDetail"/> class.
        /// </summary>
        protected AnimalMovementDetail()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the service caller used to check if a web address is available.
        /// </summary>
        public IServiceClient WebClient
        {
            get
            {
                if (this.bordBiaServiceClient == null)
                {
                    this.bordBiaServiceClient = new BordBiaCertification();
                }

                return this.bordBiaServiceClient;
            }

            set
            {
                this.bordBiaServiceClient = value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the collection of movements for the animal.
        /// </summary>
        public IList<Movement> Movements
        {
            get
            {
                return this.FilterMovements();
            }

            set
            {
                this.movements = value;
            }
        }

        /// <summary>
        /// Gets or sets the certification history for the herd number which has been stored locally.
        /// </summary>
        public IList<CertificateHistory> HerdCertificateHistory
        {
            get
            {
                return this.herdCertificateHistory;
            }

            set
            {
                this.herdCertificateHistory = value;
            }
        }

        /// <summary>
        /// Gets or sets the tag associated with the movements.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the herd number for the lot.
        /// </summary>
        public string LotHerdNo { get; set; }

        /// <summary>
        /// Gets or sets the date of birth of the animal.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the username used to access the quality approval service.
        /// </summary>
        public string BordBiaUsername { get; set; }

        /// <summary>
        /// Gets or sets the password used to access the quality approval service.
        /// </summary>
        public string BordBiaPassword { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Find the residency in days on the current farm.
        /// </summary>
        /// <returns>An object representation of the number of days the animal spent on the current farm which have been approved by the quality assurance service.</returns>
        public virtual ResidencyResult FindCurrentUKFarmResidency(string herdOfOrigin = "", string presentingHerdNo = "")
        {
            this.LotHerdNo = presentingHerdNo;
            var residencyResult = ResidencyResult.CreateNewResidencyResult();
            var residencyCalculation = ResidencyCalculation.CreateNewResidencyCalculation();

            var animalMovement = this.Movements
                .OrderByDescending(item => item.MovementDate)
                .ThenByDescending(x => x.Index)
                .FirstOrDefault();

            if (animalMovement != null)
            {
                var id = animalMovement.ToId;
                if (string.IsNullOrEmpty(id))
                {
                    id = animalMovement.FromId;
                }

                this.currentFarmCertification = this.WebClient.ValidateRedTractorHerdNumber(id, this.BordBiaUsername,
                    this.BordBiaPassword, DateTime.Today, serviceDisabled: this.bordBiaDisabled);

                if (!string.IsNullOrEmpty(this.currentFarmCertification.Error))
                {
                    return ResidencyResult.CreateResidencyResult(this.currentFarmCertification.Error, 0, null, this.currentFarmCertification.Error);
                }

                // If there was a web service time out, then invalidate the current process.
                    if (this.CheckForWebServiceTimeOut(this.currentFarmCertification))
                {
                    return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                }

                /* if the cert is valid then we can use for residency calculations. 
                 * Otherwise, we use the calculations for UI display purposes only */
                var canUseForCalculations = this.currentFarmCertification.IsValid;

                // For ui display purposes, ensure that the cert of the presenting herd is recorded.
                var herdHistory = this.currentFarmCertification.HerdCertificateHistory;
                DateTime? herdHistoryFirstCertStartDate = null;
                DateTime? herdHistoryFirstCertExpiryDate = null;

                if (herdHistory.Any())
                {
                    herdHistoryFirstCertStartDate = herdHistory[0].CertStartDate;
                    herdHistoryFirstCertExpiryDate = herdHistory[0].CertExpiryDate;
                }

                residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                       id,
                       "Factory",
                       animalMovement.MovementDate,
                       DateTime.Now,
                       herdHistoryFirstCertStartDate,
                       herdHistoryFirstCertExpiryDate,
                       false,
                       false,
                       DateTime.Now.Subtract(animalMovement.MovementDate).Days,
                       string.Empty,
                       canUseForCalculations);

                residencyResult = ResidencyResult.CreateResidencyResult(this.Eartag, residencyCalculation.DaysAllocated, new List<ResidencyCalculation> { residencyCalculation });
                this.globalResidencyResult = new ResidencyResult
                {
                    Eartag = residencyResult.Eartag,
                    ResidencyCalculations = residencyResult.ResidencyCalculations,
                    TotalDaysResidency = residencyResult.TotalDaysResidency
                };

                return residencyResult;
            }

            //this.globalResidencyResult = residencyResult;
            return residencyResult;
        }

        /// <summary>
        /// Find the residency in days on the previous farm.
        /// </summary>
        /// <returns>An object representation of the number of days the animal spent on the previous farm which have been approved by the quality assurance service.</returns>
        public virtual ResidencyResult FindPreviousUKFarmResidency()
        {
            var residencyResult = ResidencyResult.CreateNewResidencyResult();
            var residencyCalculation = ResidencyCalculation.CreateNewResidencyCalculation();
            var daysResidency = 0;

            if (this.Movements.Count < 2)
            {
                residencyResult.Eartag = string.Empty;
                return residencyResult;
            }

            // Get the last movement.
            var animalMovement = this.Movements.ElementAtOrDefault(this.Movements.Count - 1);

            // Get the second last movement. If we only have one movement take the birth date.
            var animalMovementPreviousFarm = this.Movements.ElementAtOrDefault(this.Movements.Count - 2);

            if (animalMovement != null)
            {
                var certificationCurrentHerdNo = this.currentFarmCertification;
                this.previousFarmCertification = this.WebClient.ValidateRedTractorHerdNumber(
                    animalMovementPreviousFarm.ToId, 
                    this.BordBiaUsername, 
                    this.BordBiaPassword,
                    animalMovement.MovementOffDate, 
                    serviceDisabled: this.bordBiaDisabled);

                // If there was a web service time out, then invalidate the current process.
                if (this.CheckForWebServiceTimeOut(this.previousFarmCertification))
                {
                    return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                }

                if (!string.IsNullOrEmpty(previousFarmCertification.Error))
                {
                    return ResidencyResult.CreateResidencyResult(previousFarmCertification.Error, 0, null, previousFarmCertification.Error);
                }

                var certificationPreviousHerdNo = this.previousFarmCertification;

                // Determine if we can use this for residency calculations, or merely store the results for UI auditing purposes.
                bool presentingAndCurrentHerdsValid = false;

                if (certificationCurrentHerdNo.IsValid)
                {
                    presentingAndCurrentHerdsValid = this.lairageHerdCertification.IsValid;
                }
                // var canUseForCalculations = certificationPreviousHerdNo.IsValid && certificationCurrentHerdNo.IsValid;
                var canUseForCalculations = certificationPreviousHerdNo.IsValid && presentingAndCurrentHerdsValid;
                daysResidency = animalMovement.MovementDate.Subtract(animalMovementPreviousFarm.MovementDate).Days;

                DateTime? certFirstStartDate = null;
                DateTime? certFirstExpiryDate = null;

                if (certificationPreviousHerdNo.HerdCertificateHistory.Any())
                {
                    certFirstStartDate = certificationPreviousHerdNo.HerdCertificateHistory[0].CertStartDate;
                    certFirstExpiryDate = certificationPreviousHerdNo.HerdCertificateHistory[0].CertExpiryDate;
                }

                residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                        animalMovementPreviousFarm.ToId,
                        animalMovement.ToId,
                        animalMovementPreviousFarm.MovementDate,
                        animalMovement.MovementDate,
                        certFirstStartDate,
                        certFirstExpiryDate,
                        false,
                        false,
                        daysResidency,
                        string.Empty,
                        canUseForCalculations);

                return ResidencyResult.CreateResidencyResult(this.Eartag, daysResidency, new List<ResidencyCalculation> { residencyCalculation });
            }

            return residencyResult;
        }

        /// <summary>
        /// Find the residency in days on all farms.
        /// </summary>
        /// <param name="farmCount">The number of farms to search through when calculating residency. -1 indicates that all farms are checked.</param>
        /// <returns>The number of days the animal spent of all farms which have been approved by the quality assurance service.</returns>
        public virtual ResidencyResult FindTotalUKFarmResidency(int farmCount = -1)
        {
            var residencyResult = ResidencyResult.CreateNewResidencyResult();
            var residencyCalculation = ResidencyCalculation.CreateNewResidencyCalculation();

            /* We have to create a shallow copy. If we simply assign this.globalResidencyResult to residencyResult
             * then all the residency calculations will propogate out to currentFarmResidency */
            residencyResult.Eartag = Eartag;
            residencyResult.TotalDaysResidency = this.globalResidencyResult.TotalDaysResidency;
            foreach (var calculation in this.globalResidencyResult.ResidencyCalculations)
            {
                residencyResult.ResidencyCalculations.Add(calculation);
            }

            // Set the default variables. 
            var totalResidency = 0;
            var farmCheckedCount = 0;
            var noBreakInQASFarms = this.currentFarmCertification.IsValid;

            // If the residency count is 0, exit.
            if (this.Movements.Count == 0)
            {
                return residencyResult;
            }

            // Add the current residency to the total count.
            totalResidency = residencyResult.TotalDaysResidency;

            // The last move is now.
            var lastMove = Movement.CreateMovement(residencyResult.ResidencyCalculations[0].MovingFromFarmId,
                residencyResult.ResidencyCalculations[0].MovingFromFarmId, 
                residencyResult.ResidencyCalculations[0].MovedToFarmDate,
                moveOff: residencyResult.ResidencyCalculations[0].MovingFromFarmDate);
          
            var movementList = this.Movements.ToList();

            // Remove the last movement if it is the current residency.
            farmCheckedCount = 1;
            movementList.RemoveAt(movementList.Count - 1);

            var localCount = 2;
            foreach (var animalMovement in movementList.OrderByDescending(movement => movement.MovementDate).ThenByDescending(x => x.Index))
            {
                BordBiaCertification fromFarmCertification;
                BordBiaCertification toFarmCertification;

                /* BM: Rather than make repetitive web service calls, we can use the previous web service results.
                   The result of this change means that the average 3/4 movents check has been reduced from 9/10 seconds to 3/4 seconds.*/
                if (localCount == 1)
                {
                    // both fromfarm and toFarm are the stored currentFarmCertification, so no need to hit the department.
                    fromFarmCertification = this.currentFarmCertification;
                    toFarmCertification = this.currentFarmCertification;
                }
                else if (localCount == 2)
                {
                    // the toFarm is the currentFarmCertification, and the fromFarm is the previousFarmCertification, so no need to hit the department.
                    fromFarmCertification = this.previousFarmCertification;
                    toFarmCertification = this.currentFarmCertification;
                }
                else if (localCount == 3)
                {
                    // The to farm is the stored previousFarmCertification, but the from farm has not been previously stored, so we hit the department for this.
                    fromFarmCertification = this.WebClient.ValidateRedTractorHerdNumber(animalMovement.ToId, this.BordBiaUsername, this.BordBiaPassword, animalMovement.MovementOffDate, serviceDisabled: this.bordBiaDisabled);

                    // If there was a web service time out, then invalidate the current process.
                    if (this.CheckForWebServiceTimeOut(fromFarmCertification))
                    {
                        return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                    }

                    if (!string.IsNullOrEmpty(fromFarmCertification.Error))
                    {
                        return ResidencyResult.CreateResidencyResult(fromFarmCertification.Error, 0, null, fromFarmCertification.Error);
                    }

                    toFarmCertification = this.previousFarmCertification;

                    // store this, as it will be the to farm on the next iteration.
                    this.multipleFarmsToFarmCertification = fromFarmCertification;
                }
                else
                {
                    // The to farm is the stored multipleFarmsToFarmCertification, but the from farm has not been previously stored, so we hit the department for this.
                    fromFarmCertification = this.WebClient.ValidateRedTractorHerdNumber(animalMovement.ToId, this.BordBiaUsername, this.BordBiaPassword, animalMovement.MovementOffDate, serviceDisabled: this.bordBiaDisabled);

                    // If there was a web service time out, then invalidate the current process.
                    if (this.CheckForWebServiceTimeOut(fromFarmCertification))
                    {
                        return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                    }

                    if (!string.IsNullOrEmpty(fromFarmCertification.Error))
                    {
                        return ResidencyResult.CreateResidencyResult(fromFarmCertification.Error, 0, null, fromFarmCertification.Error);
                    }

                    toFarmCertification = this.multipleFarmsToFarmCertification;

                    // store this, as it will be the to farm on the next iteration.
                    this.multipleFarmsToFarmCertification = fromFarmCertification;
                }

                /* If the from certificate is valid we record its qas and residency. (this will need to be displayed on the UI for auditing purposes, regardless as to
                 * whether it's used in the residency calculations)
                 * If the to farm is valid, and there has not been a break in farm validity, then we add the residency to the total */
                if (farmCount == -1 || farmCount > farmCheckedCount)
                {
                    var daysResidency = 0;

                    daysResidency = lastMove.MovementDate.Subtract(animalMovement.MovementDate).Days;

                    if (animalMovement.MovementDate != lastMove.MovementDate)
                    {
                        /* determine if we are just storing the days of residency for UI display purposes (audit trail)
                           or using it as part of the total residency calculations. (this is determined further down the line, so we need to record this here) */
                        var localMovementValid = fromFarmCertification.IsValid && toFarmCertification.IsValid;

                        if (!localMovementValid)
                        {
                            noBreakInQASFarms = false;
                        }

                        var useForCalculations = noBreakInQASFarms && localMovementValid;

                        DateTime? fromFarmFirstHerdCertStartDate = null;
                        DateTime? fromFarmFirstHerdCertExpiryDate = null;

                        if (fromFarmCertification.HerdCertificateHistory.Any())
                        {
                            fromFarmFirstHerdCertStartDate = fromFarmCertification.HerdCertificateHistory[0].CertStartDate;
                            fromFarmFirstHerdCertExpiryDate = fromFarmCertification.HerdCertificateHistory[0].CertExpiryDate;
                        }

                        // Add the calculation for iteration to the collection.
                        residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                            animalMovement.ToId,
                            lastMove.ToId,
                            animalMovement.MovementDate,
                            lastMove.MovementDate,
                            fromFarmFirstHerdCertStartDate,
                            fromFarmFirstHerdCertExpiryDate,
                            false,
                            false,
                            daysResidency,
                            string.Empty,
                            useForCalculations);

                        residencyResult.ResidencyCalculations.Add(residencyCalculation);

                        if (useForCalculations)
                        {
                            totalResidency += daysResidency;
                        }
                    }
                }

                lastMove = animalMovement;
                farmCheckedCount++;
                localCount++;
            }

            residencyResult.Eartag = this.Eartag;
            residencyResult.TotalDaysResidency = totalResidency;

            this.ClearResidency();

            return residencyResult;
        }

        /// <summary>
        /// Find the residency in days on the current farm.
        /// </summary>
        /// <returns>An object representation of the number of days the animal spent on the current farm which have been approved by the quality assurance service.</returns>
        public virtual ResidencyResult FindCurrentFarmResidency(string herdOfOrigin = "", string presentingHerdNo = "")
        {
            this.LotHerdNo = presentingHerdNo;
            var residencyResult = ResidencyResult.CreateNewResidencyResult();
            var residencyCalculation = ResidencyCalculation.CreateNewResidencyCalculation();

            // The animal was born on the current farm.
            if (this.Movements.Count == 0)
            {
                var localHerd = string.IsNullOrEmpty(herdOfOrigin) ? this.LotHerdNo : herdOfOrigin;
                this.currentFarmCertification = this.WebClient.ValidateHerdNumber(localHerd, this.BordBiaUsername, this.BordBiaPassword, this.HerdCertificateHistory, serviceDisabled: this.bordBiaDisabled);

                // If there was a web service time out, then invalidate the current process.
                if (this.CheckForWebServiceTimeOut(this.currentFarmCertification))
                {
                    return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                }

                if (!this.currentFarmCertification.IsValid)
                {
                    residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                        localHerd,
                        "Factory",
                        this.DateOfBirth,
                        DateTime.Now,
                        null,
                        null,
                        false,
                        false,
                        DateTime.Now.Subtract(this.DateOfBirth).Days,
                        "The certificate is not valid.");

                    residencyResult = ResidencyResult.CreateResidencyResult(this.Eartag, 0, new List<ResidencyCalculation> { residencyCalculation });

                    return residencyResult;
                }

                // The start date of the residency days calculation is dependent on whether the cert start date is before or after the date of birth.
                residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                    localHerd,
                    "Factory",
                    this.DateOfBirth,
                    DateTime.Now,
                    currentFarmCertification.HerdCertificateHistory[0].CertStartDate,
                    currentFarmCertification.HerdCertificateHistory[0].CertExpiryDate,
                    false,
                    false,
                    DateTime.Now.Subtract(this.DateOfBirth).Days,
                    string.Empty);

                residencyResult = ResidencyResult.CreateResidencyResult(this.Eartag, DateTime.Now.Subtract(this.DateOfBirth).Days, new List<ResidencyCalculation> { residencyCalculation });

                this.globalResidencyResult = residencyResult;

                return residencyResult;
            }

            /* Otherwise, look at the movements. (the reason we do a secondary order by index
             * is in the event of duplicate dates, we ensure that the movements are in the correct order) */
            var animalMovement = this.Movements
                .OrderByDescending(item => item.MovementDate)
                .ThenByDescending(x => x.Index)
                .FirstOrDefault();

            if (animalMovement != null)
            {
                // Check if the aim animal herd and lairage herd are valid. If both are valid, it's approved.
                var localCertification = new BordBiaCertification();

                this.currentFarmCertification = this.WebClient.ValidateHerdNumber(animalMovement.ToId, this.BordBiaUsername, this.BordBiaPassword, this.HerdCertificateHistory, serviceDisabled: this.bordBiaDisabled);

                // If there was a web service time out, then invalidate the current process.
                if (this.CheckForWebServiceTimeOut(this.currentFarmCertification))
                {
                    return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                }

                if (this.currentFarmCertification.IsValid)
                {
                    if (animalMovement.ToId.Equals(this.LotHerdNo))
                    {
                        lairageHerdCertification = this.currentFarmCertification;
                    }
                    else
                    {
                        // presenting herd is valid, but we still need to check the factory herd as it's not the same as the presenting herd.
                        this.lairageHerdCertification = this.WebClient.ValidateHerdNumber(this.LotHerdNo, this.BordBiaUsername, this.BordBiaPassword, this.HerdCertificateHistory, serviceDisabled: bordBiaDisabled);
                    }

                    localCertification = this.lairageHerdCertification;

                    // If there was a web service time out, then invalidate the current process.
                    if (this.CheckForWebServiceTimeOut(localCertification))
                    {
                        return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                    }
                }
                else
                {
                    // no need to check the factory herd as the presenting herd is not valid
                    localCertification = this.currentFarmCertification;
                }

                /* if the cert is valid then we can use for residency calculations. 
                 * Otherwise, we use the calculations for UI display purposes only */
                var canUseForCalculations = localCertification.IsValid;

                // For ui display purposes, ensure that the cert of the presenting herd is recorded.
                var herdHistory = this.currentFarmCertification.HerdCertificateHistory;
                DateTime? herdHistoryFirstCertStartDate = null;
                DateTime? herdHistoryFirstCertExpiryDate = null;

                if (herdHistory.Any())
                {
                    herdHistoryFirstCertStartDate = herdHistory[0].CertStartDate;
                    herdHistoryFirstCertExpiryDate = herdHistory[0].CertExpiryDate;
                }

                residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                       animalMovement.ToId,
                       "Factory",
                       animalMovement.MovementDate,
                       DateTime.Now,
                       herdHistoryFirstCertStartDate,
                       herdHistoryFirstCertExpiryDate,
                       false,
                       false,
                       DateTime.Now.Subtract(animalMovement.MovementDate).Days,
                       string.Empty,
                       canUseForCalculations);

                residencyResult = ResidencyResult.CreateResidencyResult(this.Eartag, residencyCalculation.DaysAllocated, new List<ResidencyCalculation> { residencyCalculation });
                this.globalResidencyResult = new ResidencyResult
                {
                    Eartag = residencyResult.Eartag,
                    ResidencyCalculations = residencyResult.ResidencyCalculations,
                    TotalDaysResidency = residencyResult.TotalDaysResidency
                };

                return residencyResult;
            }

            //this.globalResidencyResult = residencyResult;
            return residencyResult;
        }

        /// <summary>
        /// Find the residency in days on the previous farm.
        /// </summary>
        /// <returns>An object representation of the number of days the animal spent on the previous farm which have been approved by the quality assurance service.</returns>
        public virtual ResidencyResult FindPreviousFarmResidency()
        {
            var residencyResult = ResidencyResult.CreateNewResidencyResult();
            var residencyCalculation = ResidencyCalculation.CreateNewResidencyCalculation();
            var daysResidency = 0;

            // Check if the animal was born on the farm.
            if (this.Movements.Count == 0)
            {
                residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                    this.LotHerdNo,
                    string.Empty,
                    this.DateOfBirth,
                    DateTime.Now,
                    null,
                    null,
                    false,
                    false,
                    0,
                    "No movements present for the animal.");

                return ResidencyResult.CreateResidencyResult(this.Eartag, 0, new List<ResidencyCalculation> { residencyCalculation });
            }

            // Get the last movement.
            var animalMovement = this.Movements.ElementAtOrDefault(this.Movements.Count - 1);

            // Get the second last movement. If we only have one movement take the birth date.
            var animalMovementPreviousFarm = this.Movements.Count == 1
                ? Movement.CreateMovement(animalMovement.FromId, animalMovement.FromId, this.DateOfBirth, moveOff: animalMovement.MovementOffDate)
                : this.Movements.ElementAtOrDefault(this.Movements.Count - 2);

            if (animalMovement != null)
            {
                var certificationCurrentHerdNo = this.currentFarmCertification;
                this.previousFarmCertification = this.WebClient.ValidateHerdNumber(animalMovementPreviousFarm.ToId, this.BordBiaUsername, this.BordBiaPassword, this.HerdCertificateHistory, serviceDisabled: this.bordBiaDisabled);

                // If there was a web service time out, then invalidate the current process.
                if (this.CheckForWebServiceTimeOut(this.previousFarmCertification))
                {
                    return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                }

                var certificationPreviousHerdNo = this.previousFarmCertification;

                // Determine if we can use this for residency calculations, or merely store the results for UI auditing purposes.
                bool presentingAndCurrentHerdsValid = false;

                if (certificationCurrentHerdNo.IsValid)
                {
                    presentingAndCurrentHerdsValid = this.lairageHerdCertification.IsValid;
                }
                // var canUseForCalculations = certificationPreviousHerdNo.IsValid && certificationCurrentHerdNo.IsValid;
                var canUseForCalculations = certificationPreviousHerdNo.IsValid && presentingAndCurrentHerdsValid;
                daysResidency = animalMovement.MovementDate.Subtract(animalMovementPreviousFarm.MovementDate).Days;

                DateTime? certFirstStartDate = null;
                DateTime? certFirstExpiryDate = null;

                if (certificationPreviousHerdNo.HerdCertificateHistory.Any())
                {
                    certFirstStartDate = certificationPreviousHerdNo.HerdCertificateHistory[0].CertStartDate;
                    certFirstExpiryDate = certificationPreviousHerdNo.HerdCertificateHistory[0].CertExpiryDate;
                }

                residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                        animalMovementPreviousFarm.ToId,
                        animalMovement.ToId,
                        animalMovementPreviousFarm.MovementDate,
                        animalMovement.MovementDate,
                        certFirstStartDate,
                        certFirstExpiryDate,
                        false,
                        false,
                        daysResidency,
                        string.Empty,
                        canUseForCalculations);

                return ResidencyResult.CreateResidencyResult(this.Eartag, daysResidency, new List<ResidencyCalculation> { residencyCalculation });
            }

            return residencyResult;
        }

        /// <summary>
        /// Find the residency in days on all farms.
        /// </summary>
        /// <param name="farmCount">The number of farms to search through when calculating residency. -1 indicates that all farms are checked.</param>
        /// <returns>The number of days the animal spent of all farms which have been approved by the quality assurance service.</returns>
        public virtual ResidencyResult FindTotalFarmResidency(int farmCount = -1)
        {
            var residencyResult = ResidencyResult.CreateNewResidencyResult();
            var residencyCalculation = ResidencyCalculation.CreateNewResidencyCalculation();

            /* We have to create a shallow copy. If we simply assign this.globalResidencyResult to residencyResult
             * then all the residency calculations will propogate out to currentFarmResidency */
            residencyResult.Eartag = Eartag;
            residencyResult.TotalDaysResidency = this.globalResidencyResult.TotalDaysResidency;
            foreach (var calculation in this.globalResidencyResult.ResidencyCalculations)
            {
                residencyResult.ResidencyCalculations.Add(calculation);
            }

            // Set the default variables. 
            var totalResidency = 0;
            var farmCheckedCount = 0;
            var noBreakInQASFarms = false;

            if (this.currentFarmCertification.IsValid)
            {
                noBreakInQASFarms = this.lairageHerdCertification.IsValid;
            }

            // No movements indicates that the animal was only present on the current farm.
            //residencyResult = this.FindCurrentFarmResidency();

            // If the residency count is 0, exit.
            if (this.Movements.Count == 0)
            {
                return residencyResult;
            }

            // Add the current residency to the total count.
            totalResidency = residencyResult.TotalDaysResidency;

            // The last move is now.
            var lastMove = Movement.CreateMovement(residencyResult.ResidencyCalculations[0].MovingFromFarmId, residencyResult.ResidencyCalculations[0].MovingFromFarmId, residencyResult.ResidencyCalculations[0].MovedToFarmDate);

            // Add in the residency for the period on which the animal was on the farm it was born.
            var birthHerd = this.Movements[0];
            var movementList = this.Movements.ToList();
            movementList.Insert(0, Movement.CreateMovement(birthHerd.FromId, birthHerd.FromId, this.DateOfBirth));

            // Remove the last movement if it is the current residency.
            if (!(this.movements.Last().ToId.StartsWith("F") || this.movements.Last().ToId.StartsWith("M")) && this.movements.Last().ToId.Length == 4)
            {
                farmCheckedCount = 1;
                movementList.RemoveAt(movementList.Count - 1);
            }

            var localCount = 1;
            foreach (var animalMovement in movementList.OrderByDescending(movement => movement.MovementDate).ThenByDescending(x => x.Index))
            {
                BordBiaCertification fromFarmCertification;
                BordBiaCertification toFarmCertification;

                /* BM: Rather than make repetitive web service calls, we can use the previous web service results.
                   The result of this change means that the average 3/4 movents check has been reduced from 9/10 seconds to 3/4 seconds.*/
                if (localCount == 1)
                {
                    // both fromfarm and toFarm are the stored currentFarmCertification, so no need to hit the department.
                    fromFarmCertification = this.currentFarmCertification;
                    toFarmCertification = this.currentFarmCertification;
                }
                else if (localCount == 2)
                {
                    // the toFarm is the currentFarmCertification, and the fromFarm is the previousFarmCertification, so no need to hit the department.
                    fromFarmCertification = this.previousFarmCertification;
                    toFarmCertification = this.currentFarmCertification;
                }
                else if (localCount == 3)
                {
                    // The to farm is the stored previousFarmCertification, but the from farm has not been previously stored, so we hit the department for this.
                    fromFarmCertification = this.WebClient.ValidateHerdNumber(animalMovement.ToId, this.BordBiaUsername, this.BordBiaPassword, this.HerdCertificateHistory, serviceDisabled: this.bordBiaDisabled);

                    // If there was a web service time out, then invalidate the current process.
                    if (this.CheckForWebServiceTimeOut(fromFarmCertification))
                    {
                        return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                    }

                    toFarmCertification = this.previousFarmCertification;

                    // store this, as it will be the to farm on the next iteration.
                    this.multipleFarmsToFarmCertification = fromFarmCertification;
                }
                else
                {
                    // The to farm is the stored multipleFarmsToFarmCertification, but the from farm has not been previously stored, so we hit the department for this.
                    fromFarmCertification = this.WebClient.ValidateHerdNumber(animalMovement.ToId, this.BordBiaUsername, this.BordBiaPassword, this.HerdCertificateHistory, serviceDisabled: this.bordBiaDisabled);

                    // If there was a web service time out, then invalidate the current process.
                    if (this.CheckForWebServiceTimeOut(fromFarmCertification))
                    {
                        return ResidencyResult.CreateResidencyResult(WebServiceTimeOut, 0, null);
                    }

                    toFarmCertification = this.multipleFarmsToFarmCertification;

                    // store this, as it will be the to farm on the next iteration.
                    this.multipleFarmsToFarmCertification = fromFarmCertification;
                }

                /* If the from certificate is valid we record its qas and residency. (this will need to be displayed on the UI for auditing purposes, regardless as to
                 * whether it's used in the residency calculations)
                 * If the to farm is valid, and there has not been a break in farm validity, then we add the residency to the total */
                if (farmCount == -1 || farmCount > farmCheckedCount)
                {
                    var daysResidency = 0;

                    daysResidency = lastMove.MovementDate.Subtract(animalMovement.MovementDate).Days;

                    if (animalMovement.MovementDate != lastMove.MovementDate)
                    {
                        /* determine if we are just storing the days of residency for UI display purposes (audit trail)
                           or using it as part of the total residency calculations. (this is determined further down the line, so we need to record this here) */
                        var localMovementValid = fromFarmCertification.IsValid && toFarmCertification.IsValid;

                        if (!localMovementValid)
                        {
                            noBreakInQASFarms = false;
                        }

                        var useForCalculations = noBreakInQASFarms && localMovementValid;

                        DateTime? fromFarmFirstHerdCertStartDate = null;
                        DateTime? fromFarmFirstHerdCertExpiryDate = null;

                        if (fromFarmCertification.HerdCertificateHistory.Any())
                        {
                            fromFarmFirstHerdCertStartDate = fromFarmCertification.HerdCertificateHistory[0].CertStartDate;
                            fromFarmFirstHerdCertExpiryDate = fromFarmCertification.HerdCertificateHistory[0].CertExpiryDate;
                        }

                        // Add the calculation for iteration to the collection.
                        residencyCalculation = ResidencyCalculation.CreateResidencyCalculation(
                            animalMovement.ToId,
                            lastMove.ToId,
                            animalMovement.MovementDate,
                            lastMove.MovementDate,
                            fromFarmFirstHerdCertStartDate,
                            fromFarmFirstHerdCertExpiryDate,
                            false,
                            false,
                            daysResidency,
                            string.Empty,
                            useForCalculations);

                        residencyResult.ResidencyCalculations.Add(residencyCalculation);

                        if (useForCalculations)
                        {
                            totalResidency += daysResidency;
                        }
                    }
                }

                lastMove = animalMovement;
                farmCheckedCount++;
                localCount++;
            }

            residencyResult.Eartag = this.Eartag;
            residencyResult.TotalDaysResidency = totalResidency;

            this.ClearResidency();

            return residencyResult;
        }

        /// <summary>
        /// Remove the mart and factory movements from the movements of the animal.
        /// </summary>
        /// <returns>The collection of movements with the mart and factory moves removed.</returns>
        public virtual IList<Movement> FilterMovements()
        {
            var filteredMoves = new List<Movement>();
            var validMove = Movement.CreateNewMovement();
            var index = 1;

            if (!this.UKCheck)
            {
                foreach (var movement in this.movements.OrderBy(item => item.MovementDate))
                {
                    if (!((movement.FromId.StartsWith("M") && movement.FromId.Length == 4) || (movement.FromId.StartsWith("F") && movement.FromId.Length == 4) || (movement.FromId.StartsWith("D") && movement.FromId.Length == 4)))
                    {
                        validMove.FromId = movement.FromId;
                        validMove.MovementDate = movement.MovementDate;
                    }

                    if (!string.IsNullOrEmpty(validMove.FromId) && (movement.ToId != validMove.FromId) && !((movement.ToId.StartsWith("M") && movement.ToId.Length == 4) || (movement.ToId.StartsWith("F") && movement.ToId.Length == 4) || (movement.ToId.StartsWith("D") && movement.ToId.Length == 4)))
                    {
                        validMove.ToId = movement.ToId;
                        validMove.Index = index;
                        filteredMoves.Add(validMove);
                        index++;

                        validMove = Movement.CreateNewMovement();
                    }
                }
            }
            else
            {
                foreach (var movement in this.movements.OrderBy(item => item.MovementDate))
                {
                    if (movement.FromId.Length == 4 || movement.FromId.Length == 4 || movement.IsMart)
                    {
                        continue;
                    }

                    validMove.FromId = movement.FromId;
                    validMove.MovementDate = movement.MovementDate;
                    validMove.MovementOffDate = movement.MovementOffDate;
                    validMove.SubLocation = movement.SubLocation;
                    validMove.ToId = movement.ToId;
                    validMove.Index = index;
                    filteredMoves.Add(validMove);
                    index++;

                    validMove = Movement.CreateNewMovement();
                }
            }
            
            return filteredMoves;
        }

        /// <summary>
        /// Find the last date on which the animal moved.
        /// </summary>
        /// <returns>The on which the animal was last moved.</returns>
        public virtual DateTime? FindLastMovementDate()
        {
            var lastMovement =
                this.Movements
                    .OrderByDescending(item => item.MovementDate)
                    .FirstOrDefault();

            if (lastMovement != null)
            {
                return lastMovement.MovementDate;
            }

            return null;
        }

        /// <summary>
        /// Check to see if the animal has been imported through the department of agriculture.
        /// </summary>
        /// <returns>A boolean flag to indicate if the animal has been imported through the department of agriculture.</returns>
        public virtual bool IsAnimalImported()
        {
            return this.Movements.Any(movement => movement.FromId == "E999" || movement.ToId == "I9990016");
        }

        /// <summary>
        /// Find the date on which the animal is marked as imported by the department of agriculture.
        /// </summary>
        /// <returns>The date, if present, on which the animal was imported as noted by the department of agriculture.</returns>
        public virtual DateTime? FindImportDate()
        {
            return
                this.Movements.Where(movement => movement.FromId == "E999" || movement.ToId == "I9990016")
                    .Select(movement => movement.MovementDate).FirstOrDefault();
        }

        /// <summary>
        /// Method that checks for a web time out error
        /// </summary>
        /// <param name="certification">the returned certification result to check</param>
        /// <returns>a flag, indicating a web time out error or not</returns>
        private bool CheckForWebServiceTimeOut(BordBiaCertification certification)
        {
            return !string.IsNullOrEmpty(certification.Error) && certification.Error.StartsWith(WebServiceTimeOut);
        }

        /// <summary>
        /// Method that clears our global results fields.
        /// </summary>
        private void ClearResidency()
        {
            this.previousFarmCertification = new BordBiaCertification();
            this.currentFarmCertification = new BordBiaCertification();
            this.lairageHerdCertification = new BordBiaCertification();
            this.multipleFarmsToFarmCertification = new BordBiaCertification();
            this.globalResidencyResult = new ResidencyResult();
        }

        /// <summary>
        /// Find the date from which residency can be allocated.
        /// </summary>
        /// <param name="certificateHistory">The list of certificate approvals for the herd number.</param>
        /// <param name="moveDate">The date on which the movement occurred.</param>
        /// <returns>The date from which residency can be calculated for the herd number.</returns>
        private static DateTime FindResidencyStartDate(IList<CertificateHistory> certificateHistory, DateTime moveDate)
        {
            DateTime residencyStartDate = DateTime.Now.Date;
            var previousCertHistory = CertificateHistory.CreateNewCertificateHistory();

            foreach (var history in certificateHistory)
            {
                // Return the residency start date if the history is older than it.
                // Also, breaks in the certificate history are not allowed. Certificate approvals must be continuous.
                if (moveDate > history.CertExpiryDate || (!string.IsNullOrEmpty(previousCertHistory.HerdOwner) && (previousCertHistory.CertStartDate != history.CertExpiryDate)))
                {
                    return residencyStartDate;
                }

                // If the move date is before the cert start date, the date from the cert start date can only be assigned on this iteration.
                // The certificate history on the next iteration may allow for the inclusion of the extra days.
                residencyStartDate = history.CertStartDate < moveDate
                    ? moveDate
                    : history.CertStartDate;

                previousCertHistory = history;
            }

            return residencyStartDate;
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="BordBiaCertification.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.QualityAssurance.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.QualityAssurance.BusinessLogic.Interface;
    using Nouvem.QualityAssurance.ie.bordbia.qas;
    using Nouvem.QualityAssurance.Model.BusinessObject;
    using Nouvem.QualityAssurance.Properties;

    /// <summary>
    /// Class which models the quality assurance status of a herd.
    /// </summary>
    public class BordBiaCertification : IServiceClient
    {
        #region Fields

        /// <summary>
        /// The collection of certificate approvals taken from the service or the database archive.
        /// </summary>
        private IList<CertificateHistory> herdCertificateHistory = new List<CertificateHistory>();

        /// <summary>
        /// The web service retry attempts count
        /// </summary>
        private int localRetryAttempts = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the herd number associated associated with the certificate.
        /// </summary>
        public string HerdNo { get; set; }

        /// <summary>
        /// Gets or sets the collection of certificate approvals taken from the service or the database archive.
        /// </summary>
        public IList<CertificateHistory> HerdCertificateHistory
        {
            get
            {
                return this.herdCertificateHistory;
            }

            set
            {
                this.herdCertificateHistory = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the quality assurance check ran without an error.
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets the error message relating to the certification.
        /// </summary>
        public string Error { get; internal set; }

        #endregion

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="BordBiaCertification"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="BordBiaCertification"/> object.</returns>
        public static BordBiaCertification CreateNewBordBiaCertification()
        {
            return new BordBiaCertification();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validate the herd number using the quality assurance service.
        /// </summary>
        /// <param name="herdNumber">The herd number to validate.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="herdCertificateHistory">The list of certificate approvals found locally for the herd number.</param>
        /// <returns>The certification detail for the herd number.</returns>
        /// <remarks>TODO: Set username and password in the configuration.</remarks>
        public BordBiaCertification ValidateHerdNumber(string herdNumber, string username, string password, IList<CertificateHistory> herdCertificateHistory, int retryAttempts = 3, bool serviceDisabled = false)
        {
            var certificate = new BordBiaCertification();
            var service = new Nouvem.QualityAssurance.ie.bordbia.qas.CheckHerdStatus();
            var localRetry = 0;
            var qasResponse = new BlqasCertificationStatus();

            try
            {
                if (serviceDisabled)
                {
                    // bord bia has been disabled, so we check the stored qas records.
                    var localHerdHistory =
                        herdCertificateHistory.Where(cert => cert.HerdNumber.Equals(herdNumber) && cert.CertExpiryDate >= DateTime.Today).ToList();

                    if (localHerdHistory.Any())
                    {
                        var latestStoredCert =
                            localHerdHistory.OrderByDescending(cert => cert.CertExpiryDate).FirstOrDefault();

                        if (latestStoredCert != null)
                        {
                            qasResponse = new BlqasCertificationStatus
                            {
                                CertValidUntil = latestStoredCert.CertExpiryDate,
                                Error = string.Empty,
                                HerdNo = latestStoredCert.HerdNumber,
                                HerdOwner = latestStoredCert.HerdOwner,
                                NominatedByYourMeatPlant = false,
                                Scope = string.Empty
                            };
                        }
                    }
                    else
                    {
                        certificate.IsValid = false;
                        return certificate;
                    }
                }
                else
                {
                    while (localRetry < retryAttempts)
                    {
                        try
                        {
                            qasResponse = service.QueryBeef(username, password, herdNumber);

                            // we have a response, so break out
                            break;
                        }
                        catch (Exception)
                        {
                            // If a web timeout exception occurs, retry.
                            localRetry++;

                            if (localRetry >= retryAttempts)
                            {
                                certificate.IsValid = false;
                                certificate.Error = string.Format("Web service time out. Unable to retrieve the certification detail for herd number {0}", herdNumber);
                                return certificate;
                            }
                        }
                    }
                }

                certificate.HerdNo = herdNumber;

                // If the herd is not valid now then the herd is taken to have been invalid from day one.
                if (qasResponse.CertValidUntil.HasValue && qasResponse.CertValidUntil >= DateTime.Today)
                {
                    certificate.IsValid = true;
                    certificate.HerdCertificateHistory.Add(CertificateHistory.CreateCertificateHistory(qasResponse.HerdNo, qasResponse.HerdOwner, qasResponse.CertValidUntil.Value, Settings.Default.CertDuration));
                }
                else
                {
                    certificate.IsValid = false;
                }

                // Include the historical movements as well.
                foreach (var history in herdCertificateHistory.Where(item => item.HerdNumber == herdNumber))
                {
                    certificate.HerdCertificateHistory.Add(CertificateHistory.CreateCertificateHistory(history.HerdNumber, history.HerdOwner, history.CertExpiryDate, history.CertDuration));
                }
            }
            catch (Exception ex)
            {
                certificate.Error = string.Format("Error: {0}", ex.Message);
                return certificate;
            }

            return certificate;
        }

        /// <summary>
        /// Validate the herd number using the quality assurance service.
        /// </summary>
        /// <param name="herdNumber">The herd number to validate.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>The certification detail for the herd number.</returns>
        /// <remarks>TODO: Set username and password in the configuration.</remarks>
        public BordBiaCertification ValidateRedTractorHerdNumber(string herdNumber, string username, string password, DateTime? dateToCheck, int retryAttempts = 3, bool serviceDisabled = false)
        {
            var certificate = new BordBiaCertification();
            var service = new Nouvem.QAS.BusinessLogic.WSWrapper();
            var localRetry = 0;
            var qasResponse = QAS.Model.HerdStatus.CreateNewHerdStatus();

            try
            {
                if (serviceDisabled)
                {
                    // bord bia has been disabled, so we check the stored qas records.
                    certificate.IsValid = false;
                    return certificate;
                }
                else
                {
                    while (localRetry < retryAttempts)
                    {
                        try
                        {
                            qasResponse = service.QueryUKBeefService(username, password, herdNumber,"",dateToCheck,false);

                            if (this.IsQaError(qasResponse.Error))
                            {
                                certificate.Error = qasResponse.Error;
                            }
                         
                            // we have a response, so break out
                            break;
                        }
                        catch (Exception)
                        {
                            // If a web timeout exception occurs, retry.
                            localRetry++;

                            if (localRetry >= retryAttempts)
                            {
                                certificate.IsValid = false;
                                certificate.Error = string.Format("Web service time out. Unable to retrieve the certification detail for herd number {0}", herdNumber);
                                return certificate;
                            }
                        }
                    }
                }

                certificate.HerdNo = herdNumber;
                if (qasResponse.CertValidUntil.HasValue && qasResponse.CertValidUntil >= dateToCheck)
                {
                    certificate.IsValid = true;
                    certificate.HerdCertificateHistory.Add(CertificateHistory.CreateCertificateHistory(qasResponse.HerdNo, qasResponse.HerdOwner, qasResponse.CertValidUntil.Value, Settings.Default.CertDuration));
                }
                else
                {
                    certificate.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                certificate.Error = string.Format("Error: {0}", ex.Message);
                return certificate;
            }

            return certificate;
        }

        /// <summary>
        /// Validate the herd number only using the quality assurance service.
        /// </summary>
        /// <param name="herdNumber">The herd number to validate.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="herdCertificateHistory">The list of certificate approvals found locally for the herd number.</param>
        /// <returns>The certification detail for the herd number.</returns>
        /// <remarks>TODO: Set username and password in the configuration.</remarks>
        public Nouvem.QualityAssurance.ie.bordbia.qas.BlqasCertificationStatus ValidateHerdNumberOnly(string herdNumber, string username, string password, IList<CertificateHistory> herdCertificateHistory)
        {
            var certificate = new BordBiaCertification();
            var service = new Nouvem.QualityAssurance.ie.bordbia.qas.CheckHerdStatus();

            try
            {
                var qasResponse = service.QueryBeef(username, password, herdNumber);
                return qasResponse;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool IsQaError(string error)
        {
            return !string.IsNullOrEmpty(error) && !(error.StartsWith("Identifier") && error.EndsWith("not found."));
        }

        #endregion
    }
}

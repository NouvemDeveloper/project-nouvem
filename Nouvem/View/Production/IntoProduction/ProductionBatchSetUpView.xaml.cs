﻿// -----------------------------------------------------------------------
// <copyright file="ProductionBatchSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------


using System.Windows;
using Nouvem.ViewModel;

namespace Nouvem.View.Production.IntoProduction
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ProductionBatchSetUpView.xaml
    /// </summary>
    public partial class ProductionBatchSetUpView : UserControl
    {
        public ProductionBatchSetUpView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.OpenCalender, b =>
            {
                if (b)
                {
                    //this.DatePickerDocDate.IsDropDownOpen = true;
                }
                else
                {
                    this.DatePickerReleaseDate.IsDropDownOpen = true;
                }
               
            });

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
            if (ApplicationSettings.CreatingTouchscreenRecipes)
            {
                this.DataContext = ViewModelLocator.BatchSetUpStatic;
                this.LabelRecipe.Visibility = Visibility.Visible;
                this.ComboBoxRecipe.Visibility = Visibility.Visible;
                this.LabelQty.Visibility = Visibility.Visible;
                this.TextBoxQty.Visibility = Visibility.Visible;
            }
            else
            {
                this.DataContext = ViewModelLocator.ProductionBatchSetUpStatic;
                this.LabelRecipe.Visibility = Visibility.Hidden;
                this.ComboBoxRecipe.Visibility = Visibility.Hidden;
                this.LabelQty.Visibility = Visibility.Hidden;
                this.TextBoxQty.Visibility = Visibility.Hidden;
            }
        }
    }
}

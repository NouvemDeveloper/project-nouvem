﻿// -----------------------------------------------------------------------
// <copyright file="CarcassSplitView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.Model.Enum;


namespace Nouvem.View.Production.IntoProduction
{
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.ViewModel;

    /// <summary>
    /// Interaction logic for CarcassSplitView.xaml
    /// </summary>
    public partial class CarcassSplitView : Window
    {
        private ViewType mode;
        private bool disallowSelection;

        public CarcassSplitView(IList<int> productsToDisplay, ViewType mode = ViewType.IntoProduction)
        {
            this.InitializeComponent();
            if (!ApplicationSettings.UseFullCarcassProduct)
            {
                this.ButtonFull.MinWidth = 0;
                this.ButtonFull.Visibility = Visibility.Collapsed;
            }

            if (mode == ViewType.ARDispatch)
            {
                this.Topmost = true;
            }

            Messenger.Default.Register<string>(this, Token.CloseCarcassSplitWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.AllowSplitScanSelection, s => this.disallowSelection = false);
            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
            this.TextBoxMessage.Visibility = Visibility.Collapsed;
            this.mode = mode;

            if (productsToDisplay != null)
            {
                if (productsToDisplay.Contains(ApplicationSettings.ForeProduct))
                {
                    this.ButtonFore.Visibility = Visibility.Hidden;
                }

                if (productsToDisplay.Contains(ApplicationSettings.HindProduct))
                {
                    this.ButtonHind.Visibility = Visibility.Hidden;
                }

                if (productsToDisplay.Count >= 2)
                {
                    this.TextBoxMessage.Visibility = Visibility.Visible;
                }
            }
        }

        private void Button_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.disallowSelection)
            {
                return;
            }

            if (this.mode == ViewType.ARDispatch)
            {
                ViewModelLocator.ARDispatchTouchscreenStatic.SplitProductId = ApplicationSettings.HindProduct;
                this.Close();
                return;
            }

            if (this.mode == ViewType.APReceipt)
            {
                this.disallowSelection = true;
                ViewModelLocator.APReceiptTouchscreenStatic.CarcassSplitProductSelected(true,false);
        
                return;
            }

            this.disallowSelection = true;
            ViewModelLocator.IntoProductionStatic.CarcassSplitProductSelected(true,false);
            //this.Close();
        }

        private void Button_PreviewMouseUp_1(object sender, MouseButtonEventArgs e)
        {
            if (this.disallowSelection)
            {
                return;
            }

            if (this.mode == ViewType.ARDispatch)
            {
                ViewModelLocator.ARDispatchTouchscreenStatic.SplitProductId = ApplicationSettings.ForeProduct;
                this.Close();
                return;
            }

            if (this.mode == ViewType.APReceipt)
            {
                this.disallowSelection = true;
                ViewModelLocator.APReceiptTouchscreenStatic.CarcassSplitProductSelected(false,false);
                return;
            }

            this.disallowSelection = true;
            ViewModelLocator.IntoProductionStatic.CarcassSplitProductSelected(false,false);
            //this.Close();
        }

        private void Button_PreviewMouseUp_3(object sender, MouseButtonEventArgs e)
        {
            if (this.disallowSelection)
            {
                return;
            }

            if (this.mode == ViewType.ARDispatch)
            {
                ViewModelLocator.ARDispatchTouchscreenStatic.SplitProductId = ApplicationSettings.FullCarcassProduct;
                this.Close();
                return;
            }

            if (this.mode == ViewType.APReceipt)
            {
                this.disallowSelection = true;
                ViewModelLocator.APReceiptTouchscreenStatic.CarcassSplitProductSelected(false, true);
                return;
            }

            this.disallowSelection = true;
            ViewModelLocator.IntoProductionStatic.CarcassSplitProductSelected(false, true);
            //this.Close();
        }

        private void Button_PreviewMouseUp_2(object sender, MouseButtonEventArgs e)
        {
            if (this.mode == ViewType.ARDispatch)
            {
                ViewModelLocator.ARDispatchTouchscreenStatic.SplitProductId = 0;
            }
            if (this.mode == ViewType.APReceipt)
            {
                ViewModelLocator.APReceiptTouchscreenStatic.StocktransactionId = null;
            }

            this.Close();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenPalletisationView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Production
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenPalletisationView.xaml
    /// </summary>
    public partial class TouchscreenPalletisationView : Window
    {
        public TouchscreenPalletisationView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.ClosePalletWindow, s => this.Close());
        }
    }
}

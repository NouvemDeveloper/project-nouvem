﻿// -----------------------------------------------------------------------
// <copyright file="ProductionSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;

namespace Nouvem.View.Production
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenProductionSearchDataView.xaml
    /// </summary>
    public partial class TouchscreenProductionSearchDataView : Window
    {
        public TouchscreenProductionSearchDataView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            //Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.SetFocus());
            this.GridControlSearchData.View.ShowSearchPanel(true);

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
                Settings.Default.Save();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(Settings.Default.TouchscreenProductionSearchData);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.TouchscreenProductionSearchData))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(Settings.Default.TouchscreenProductionSearchData);
                }

                //this.TableView.Focus();
                //this.TableView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
                //this.TableView.FocusedColumn = this.GridColumnBatchNo;
                //this.TableView.ShowEditor();

                Messenger.Default.Send(Token.Message, Token.TouchscreenMode);


                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void DatePicker_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is DatePicker)
            {
                (sender as DatePicker).IsDropDownOpen = true;
            }
        }

        private void DatePicker_PreviewMouseUp_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is DatePicker)
            {
                (sender as DatePicker).IsDropDownOpen = true;
            }
        }

        private void SetFocus()
        {
            this.TableView.Focus();
            this.TableView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
            this.TableView.FocusedColumn = this.GridColumnBatchNo;
            this.TableView.ShowEditor();

            //this.TableView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
            //this.TableView.ShowEditor();
        }
    }
}


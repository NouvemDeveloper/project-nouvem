﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Production;

namespace Nouvem.View.Production
{
    /// <summary>
    /// Interaction logic for BatchEditorView.xaml
    /// </summary>
    public partial class BatchEditorView : Window
    {
        private readonly string gridPathIn;
        private readonly string gridPathOut;
        private readonly string gridPathTransIn;
        private readonly string gridPathTransOut;
        public BatchEditorView()
        {
            this.InitializeComponent();
            this.DataContext = new BatchEditorViewModel();
            Messenger.Default.Register<CollectionData>(this, Token.BatchAttributes, this.HandleMacro);
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseBatchEditorWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.gridPathIn =
                Settings.Default.BatchEditorInGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.gridPathOut =
                Settings.Default.BatchEditorOutGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += this.OnClosing;
            this.Loaded += this.OnLoaded;

            this.WindowState = WindowSettings.BatchEditorMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized
                ? ApplicationSettings.SystemMessageBoxHeight
                : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPathIn))
            {
                try
                {
                    this.GridControlIn.RestoreLayoutFromXml(this.gridPathIn);
                }
                catch (Exception e){}
            }

            if (File.Exists(this.gridPathOut))
            {
                try
                {
                    this.GridControlOut.RestoreLayoutFromXml(this.gridPathOut);
                }
                catch (Exception e) { }
            }
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.BatchEditorHeight = this.Height;
            WindowSettings.BatchEditorLeft = this.Left;
            WindowSettings.BatchEditorTop = this.Top;
            WindowSettings.BatchEditorWidth = this.Width;
            WindowSettings.BatchEditorMaximised = this.WindowState.ToBool();
            this.GridControlIn.FilterCriteria = null;
            this.GridControlOut.FilterCriteria = null;
            Messenger.Default.Send(false, Token.WindowStatus);
            this.GridControlIn.View.SearchString = string.Empty;
            this.GridControlOut.View.SearchString = string.Empty;
            this.GridControlIn.SaveLayoutToXml(this.gridPathIn);
            this.GridControlOut.SaveLayoutToXml(this.gridPathOut);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }

        private void HandleMacro(CollectionData data)
        {
            this.HideColumns();
            if (!data.RowData.Any())
            {
                return;
            }

            foreach (var localData in data.RowData)
            {
                if (localData.Item1.CompareIgnoringCase("Attribute1")) { this.GridColumn1.Header = localData.Item2; this.GridColumn1.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute2")) { this.GridColumn2.Header = localData.Item2; this.GridColumn2.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute3")) { this.GridColumn3.Header = localData.Item2; this.GridColumn3.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute4")) { this.GridColumn4.Header = localData.Item2; this.GridColumn4.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute5")) { this.GridColumn5.Header = localData.Item2; this.GridColumn5.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute6")) { this.GridColumn6.Header = localData.Item2; this.GridColumn6.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute7")) { this.GridColumn7.Header = localData.Item2; this.GridColumn7.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute8")) { this.GridColumn8.Header = localData.Item2; this.GridColumn8.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute9")) { this.GridColumn9.Header = localData.Item2; this.GridColumn9.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute10")) { this.GridColumn10.Header = localData.Item2; this.GridColumn10.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute11")) { this.GridColumn11.Header = localData.Item2; this.GridColumn11.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute12")) { this.GridColumn12.Header = localData.Item2; this.GridColumn12.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute13")) { this.GridColumn13.Header = localData.Item2; this.GridColumn13.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute14")) { this.GridColumn14.Header = localData.Item2; this.GridColumn14.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute15")) { this.GridColumn15.Header = localData.Item2; this.GridColumn15.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute16")) { this.GridColumn16.Header = localData.Item2; this.GridColumn16.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute17")) { this.GridColumn17.Header = localData.Item2; this.GridColumn17.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute18")) { this.GridColumn18.Header = localData.Item2; this.GridColumn18.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute19")) { this.GridColumn19.Header = localData.Item2; this.GridColumn19.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute20")) { this.GridColumn20.Header = localData.Item2; this.GridColumn20.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute21")) { this.GridColumn21.Header = localData.Item2; this.GridColumn21.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute22")) { this.GridColumn22.Header = localData.Item2; this.GridColumn22.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute23")) { this.GridColumn23.Header = localData.Item2; this.GridColumn23.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute24")) { this.GridColumn24.Header = localData.Item2; this.GridColumn24.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute25")) { this.GridColumn25.Header = localData.Item2; this.GridColumn25.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute26")) { this.GridColumn26.Header = localData.Item2; this.GridColumn26.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute27")) { this.GridColumn27.Header = localData.Item2; this.GridColumn27.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute28")) { this.GridColumn28.Header = localData.Item2; this.GridColumn28.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute29")) { this.GridColumn29.Header = localData.Item2; this.GridColumn29.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute30")) { this.GridColumn30.Header = localData.Item2; this.GridColumn30.Visible = true; }
            }
        }

        private void HideColumns()
        {
            this.GridColumn1.Visible = false;
            this.GridColumn2.Visible = false;
            this.GridColumn3.Visible = false;
            this.GridColumn4.Visible = false;
            this.GridColumn5.Visible = false;
            this.GridColumn6.Visible = false;
            this.GridColumn7.Visible = false;
            this.GridColumn8.Visible = false;
            this.GridColumn9.Visible = false;
            this.GridColumn10.Visible = false;
            this.GridColumn11.Visible = false;
            this.GridColumn12.Visible = false;
            this.GridColumn13.Visible = false;
            this.GridColumn14.Visible = false;
            this.GridColumn15.Visible = false;
            this.GridColumn16.Visible = false;
            this.GridColumn17.Visible = false;
            this.GridColumn18.Visible = false;
            this.GridColumn19.Visible = false;
            this.GridColumn20.Visible = false;
            this.GridColumn21.Visible = false;
            this.GridColumn22.Visible = false;
            this.GridColumn23.Visible = false;
            this.GridColumn24.Visible = false;
            this.GridColumn25.Visible = false;
            this.GridColumn26.Visible = false;
            this.GridColumn27.Visible = false;
            this.GridColumn28.Visible = false;
            this.GridColumn29.Visible = false;
            this.GridColumn30.Visible = false;
        }
    }
}

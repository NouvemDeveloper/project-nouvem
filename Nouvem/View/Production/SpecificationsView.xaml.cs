﻿// -----------------------------------------------------------------------
// <copyright file="SpecificationsView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Production
{
    using System.ComponentModel;
    using System.Windows;
    using System.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SpecificationsView.xaml
    /// </summary>
    public partial class SpecificationsView : Window
    {
        private readonly string gridPath;
        private readonly string gridSecondaryPath;

        public SpecificationsView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseSpecificationsWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            ThreadPool.QueueUserWorkItem(x => this.TextBoxSpec.Dispatcher.Invoke(() => this.TextBoxSpec.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedInventoryControl, x => this.ComboBoxSpecs.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxSpec.Focus());
            Messenger.Default.Register<string>(this, Token.CollapseGroups, s => this.TreeListViewData.CollapseAllNodes());

            this.gridPath = Settings.Default.SpecMainGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.gridSecondaryPath = Settings.Default.SpecSecondaryGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            
            this.Loaded += this.OnLoaded;
            this.Closing += this.OnClosing;
            Messenger.Default.Send(true, Token.WindowStatus);

            this.WindowState = WindowSettings.SpecificationsMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

        }

        private void OnLoaded(object o, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControlProducts.RestoreLayoutFromXml(this.gridPath);
            }

            if (File.Exists(this.gridSecondaryPath))
            {
                this.GridControlSelectedProducts.RestoreLayoutFromXml(this.gridSecondaryPath);
            }
        }

        private void OnClosing(object o, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.SpecificationsHeight = this.Height;
            WindowSettings.SpecificationsLeft = this.Left;
            WindowSettings.SpecificationsTop = this.Top;
            WindowSettings.SpecificationsWidth = this.Width;
            WindowSettings.SpecificationsMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.GridControlProducts.SaveLayoutToXml(this.gridPath);
            this.GridControlSelectedProducts.SaveLayoutToXml(this.gridSecondaryPath);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }

        private void CheckBoxExpand_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TreeListViewData.ExpandAllNodes();
        }

        private void CheckBoxExpand_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TreeListViewData.CollapseAllNodes();
        }
    }
}

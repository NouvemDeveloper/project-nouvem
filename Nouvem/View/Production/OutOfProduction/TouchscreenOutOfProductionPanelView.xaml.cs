﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenOutOfProductionPanelView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Production.OutOfProduction
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TouchscreenOutOfProductionPanelView.xaml
    /// </summary>
    public partial class TouchscreenOutOfProductionPanelView : UserControl
    {
        public TouchscreenOutOfProductionPanelView()
        {
            this.InitializeComponent();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Production.Receipe;

namespace Nouvem.View.Production.Receipe
{
    /// <summary>
    /// Interaction logic for RecipeSearchView.xaml
    /// </summary>
    public partial class RecipeSearchView : Window
    {
        private readonly string gridPath;

        public RecipeSearchView()
        {
            InitializeComponent();
            this.DataContext = new RecipeSearchViewModel();
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseRecipeSearchWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.gridPath = Settings.Default.RecipeSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += this.OnClosing;
            this.Loaded += this.OnLoaded;

            this.WindowState = WindowSettings.RecipeSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControlSearchData.RestoreLayoutFromXml(this.gridPath);
            }

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableView);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.Show();
                }
                catch
                {
                }
            });
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.RecipeSearchHeight = this.Height;
            WindowSettings.RecipeSearchLeft = this.Left;
            WindowSettings.RecipeSearchTop = this.Top;
            WindowSettings.RecipeSearchWidth = this.Width;
            WindowSettings.RecipeSearchMaximised = this.WindowState.ToBool();
            this.GridControlSearchData.FilterCriteria = null;
            Messenger.Default.Send(false, Token.WindowStatus);
            this.GridControlSearchData.View.SearchString = string.Empty;
            this.GridControlSearchData.SaveLayoutToXml(this.gridPath);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }
    }
}


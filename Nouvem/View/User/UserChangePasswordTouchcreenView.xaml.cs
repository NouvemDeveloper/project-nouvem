﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.User
{
    /// <summary>
    /// Interaction logic for UserChangePasswordTouchcreenView.xaml
    /// </summary>
    public partial class UserChangePasswordTouchcreenView : Window
    {
        public UserChangePasswordTouchcreenView()
        {
            InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseUserChangePasswordWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Loaded += (sender, args) =>
            {
                this.PasswordBoxEditPassword.Focus();
                this.Keyboard.IsOpen = true;
            };
        }
    }
}

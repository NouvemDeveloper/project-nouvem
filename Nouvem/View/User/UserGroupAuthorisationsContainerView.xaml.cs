﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Nouvem.Shared;

// -----------------------------------------------------------------------
// <copyright file="UserGroupAuthorisationContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.User
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for UserGroupAuthorisationsContainerView.xaml
    /// </summary>
    public partial class UserGroupAuthorisationsContainerView : Window
    {
        public UserGroupAuthorisationsContainerView()
        {
            this.InitializeComponent();

            var gridPath = Settings.Default.AuthorisationsGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            Messenger.Default.Register<string>(this, Token.CloseUserGroupAuthorisationWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.AuthorisationsMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlAuthorisations.RestoreLayoutFromXml(gridPath);
                }
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlAuthorisations.SaveLayoutToXml(gridPath);
                WindowSettings.AuthorisationsHeight = this.Height;
                WindowSettings.AuthorisationsLeft = this.Left;
                WindowSettings.AuthorisationsTop = this.Top;
                WindowSettings.AuthorisationsWidth = this.Width;
                WindowSettings.AuthorisationsMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };
        }
    }
}

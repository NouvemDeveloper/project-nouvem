﻿// -----------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.User
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for UserChangePasswordView.xaml
    /// </summary>
    public partial class UserChangePasswordView : UserControl
    {
        public UserChangePasswordView()
        {
            this.InitializeComponent();
            this.PasswordBoxEditPassword.Focus();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="LairageSearchView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.View.KillLine.Lairage
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for SaleSearchDataView.xaml
    /// </summary>
    public partial class LairageSearchView : Window
    {
        public LairageSearchView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.LairageSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.RequestLairages, s => this.GetFilteredLairages());
            this.GridControlSearchData.View.ShowSearchPanel(true);

            var gridPath = Settings.Default.LairageSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += (sender, args) =>
            {
                WindowSettings.LairageSearchHeight = this.Height;
                WindowSettings.LairageSearchLeft = this.Left;
                WindowSettings.LairageSearchTop = this.Top;
                WindowSettings.LairageSearchWidth = this.Width;
                WindowSettings.LairageSearchMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void GetFilteredLairages()
        {
            var lairages = new List<Sale>();
            for (int i = 0; i < this.GridControlSearchData.VisibleRowCount; i++)
            {
                var dataRow = this.GridControlSearchData.GetRow(this.GridControlSearchData.GetRowHandleByVisibleIndex(i));
                if (dataRow is Sale)
                {
                    lairages.Add((Sale)dataRow);
                }
            }

            Messenger.Default.Send(lairages, Token.SendingLairages);
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="LairageOrderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using DevExpress.Xpf.Grid;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.KillLine.Lairage
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LairageOrderView.xaml
    /// </summary>
    public partial class LairageOrderView : Window
    {
        public LairageOrderView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseLairageOrderWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.LairageOrderMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.LairageOrderHeight = this.Height;
                WindowSettings.LairageOrderLeft = this.Left;
                WindowSettings.LairageOrderTop = this.Top;
                WindowSettings.LairageOrderWidth = this.Width;
                WindowSettings.LairageOrderMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void TableViewData_ValidateRow(object sender, GridRowValidationEventArgs e)
        {
            try
            {
                var inMasterid = (e.Row as SaleDetail).INMasterID;
                if (inMasterid == 0)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.NoProductSelectedOnOrderLine;
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelectedOnOrderLine);
                    return;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}



﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.KillLine.Grader
{
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for GraderPanelView.xaml
    /// </summary>
    public partial class GraderPanelView : UserControl
    {
        public GraderPanelView()
        {
            InitializeComponent();
            this.LabelScanner.Visibility = Visibility.Visible;
            this.LabelScannerText.Visibility = Visibility.Visible;
            this.LabelOrders.Visibility = Visibility.Collapsed;
            Messenger.Default.Register<bool>(this, Token.GraderPanelLabel, b =>
                {
                    if (!b)
                    {
                        this.LabelScanner.Visibility = Visibility.Collapsed;
                        this.LabelScannerText.Visibility = Visibility.Collapsed;
                        this.LabelOrders.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        this.LabelScanner.Visibility = Visibility.Visible;
                        this.LabelScannerText.Visibility = Visibility.Visible;
                        this.LabelOrders.Visibility = Visibility.Collapsed;
                    }
                });
        }
    }
}

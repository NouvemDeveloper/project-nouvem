﻿// -----------------------------------------------------------------------
// <copyright file="SequencerPanelView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.KillLine.Sequencer
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SequencerPanelView.xaml
    /// </summary>
    public partial class SequencerPanelView : UserControl
    {
        public SequencerPanelView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.FocusToButton, s => this.ButtonAddToQueue.Focus());
        }
    }
}

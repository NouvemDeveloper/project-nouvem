﻿// -----------------------------------------------------------------------
// <copyright file="SequencerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.Logging;

namespace Nouvem.View.KillLine.Sequencer
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for SequencerView.xaml
    /// </summary>
    public partial class SequencerView : UserControl
    {
        public SequencerView()
        {
            this.InitializeComponent();

            var gridPath = Settings.Default.SequencerGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Unloaded += (sender, args) =>
            {
                try
                {
                    this.GridControlStockDetails.SaveLayoutToXml(gridPath);
                }
                catch (Exception e)
                {
                    var log = new Logger();
                    log.LogError(this.GetType(), e.Message);
                }
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlStockDetails.RestoreLayoutFromXml(gridPath);
                    }
                    catch (Exception e)
                    {
                        var log = new Logger();
                        log.LogError(this.GetType(), e.Message);
                    }
                }
            };
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="WeightBandSetUpView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Data;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Payments
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WeightBandSetUpView.xaml
    /// </summary>
    public partial class WeightBandSetUpView : Window
    {
        private readonly string gridPath;

        public WeightBandSetUpView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWeightBandWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.gridPath = Settings.Default.WeightBandGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.WindowState = WindowSettings.WeightBandMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Loaded += this.OnLoaded;
            this.Closing += this.OnClosing;
            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControl.RestoreLayoutFromXml(this.gridPath);
            }

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableViewData);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.ShowDialog();
                }
                catch
                {
                }
            });
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.WeightBandHeight = this.Height;
            WindowSettings.WeightBandLeft = this.Left;
            WindowSettings.WeightBandTop = this.Top;
            WindowSettings.WeightBandWidth = this.Width;
            WindowSettings.WeightBandMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.GridControl.SaveLayoutToXml(this.gridPath);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
            BindingOperations.ClearAllBindings(this);
        }
    }
}

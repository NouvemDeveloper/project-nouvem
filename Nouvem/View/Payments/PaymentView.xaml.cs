﻿// -----------------------------------------------------------------------
// <copyright file="PaymentView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Payments
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for PaymentView.xaml
    /// </summary>
    public partial class PaymentView : Window
    {
        public PaymentView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.ClosePaymentWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<Sale>(this, Token.PaymentProposalSelected, o =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });
         
            this.WindowState = WindowSettings.PaymentMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.PaymentDetailsGridpath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlStockDetails.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewStockData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlStockDetails.SaveLayoutToXml(gridPath);
                WindowSettings.PaymentHeight = this.Height;
                WindowSettings.PaymentLeft = this.Left;
                WindowSettings.PaymentTop = this.Top;
                WindowSettings.PaymentWidth = this.Width;
                WindowSettings.PaymentMaximised = this.WindowState == WindowState.Maximized;
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
        
        private void GridColumn_Validate(object sender, GridCellValidationEventArgs e)
        {
            
        }

        private void TableViewDeductions_HiddenEditor(object sender, EditorEventArgs e)
        {
            //try
            //{
            //    var deduction = (e.Row as PaymentDeductionItem);

            //    if (deduction == null)
            //    {
            //        return;
            //    }

            //    if (deduction.LoadingDeductionItem)
            //    {
            //        return;
            //    }

            //    if (e.Column.Name != "Price")
            //    {
            //        return;
            //    }

            //    //if (!deduction.AllowPriceEdit)
            //    //{
            //    //    e.IsValid = false;
            //    //    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
            //    //    e.ErrorContent = Message.EditPriceNotAllowed;
            //    //    SystemMessage.Write(MessageType.Issue, Message.EditPriceNotAllowed);
            //    //    return;
            //    //}

            //    if (!deduction.AllowZeroPrice)
            //    {
            //        var value = (decimal)e.Value;
            //        if (value == 0)
            //        {
            //            SystemMessage.Write(MessageType.Issue, Message.ZeroPriceNotAllowed);
            //            this.TableViewDeductions.CommitEditing(false);
            //            return;
            //        }

            //        this.TableViewDeductions.CommitEditing();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    var a = ex.Message;
            //    var b = a;
            //}
        }

        private void TableViewDeductions_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            try
            {
                var deduction = (e.Row as PaymentDeductionItem);

                if (deduction == null)
                {
                    return;
                }

                if (deduction.LoadingDeductionItem)
                {
                    return;
                }

                if (e.Column.FieldName != "Price")
                {
                    return;
                }

                //if (!deduction.AllowPriceEdit)
                //{
                //    var value = e.Cell.Value.ToDecimal();

                //    SystemMessage.Write(MessageType.Issue, Message.EditPriceNotAllowed);
                //    var oldValue = e.OldValue.ToDecimal();
                //    e.Cell.Value = oldValue.ToDecimal();
                //    return;
                //}

                if (!deduction.AllowZeroPrice)
                {
                    var value = e.Cell.Value.ToDecimal();

                    if (value == 0)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.ZeroPriceNotAllowed);
                        var oldValue = e.OldValue.ToDecimal();
                        (e.Row as PaymentDeductionItem).Price = oldValue.ToDecimal();
                        //deduction.Price = oldValue.ToDecimal();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                var b = a;
            }
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowHandle = this.TableViewStockData.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlStockDetails.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlStockDetails.CurrentColumn == this.GridControlStockDetails.Columns["Notes"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowPaymentNotesWindow);
                }
            }
        }
    }
}




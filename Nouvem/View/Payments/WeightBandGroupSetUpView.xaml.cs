﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Windows.Data;
using Nouvem.Shared;

namespace Nouvem.View.Payments
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WeightBandGroupSetUpView.xaml
    /// </summary>
    public partial class WeightBandGroupSetUpView : Window
    {
        public WeightBandGroupSetUpView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWeightBandGroupWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.WeightBandGroupMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += this.OnClosing;
            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.WeightBandGroupHeight = this.Height;
            WindowSettings.WeightBandGroupLeft = this.Left;
            WindowSettings.WeightBandGroupTop = this.Top;
            WindowSettings.WeightBandGroupWidth = this.Width;
            WindowSettings.WeightBandGroupMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
            BindingOperations.ClearAllBindings(this);
        }
    }
}

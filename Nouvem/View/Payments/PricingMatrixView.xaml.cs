﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Payments;

namespace Nouvem.View.Payments
{
    /// <summary>
    /// Interaction logic for PricingMatrixView.xaml
    /// </summary>
    public partial class PricingMatrixView : Window
    {
        public PricingMatrixView()
        {
            InitializeComponent();
            this.DataContext = new PricingMatrixViewModel();
            Messenger.Default.Register<string>(this, Token.ClosePricingMatrixWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.PricingMatrixMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.PricingMatrixHeight = this.Height;
                WindowSettings.PricingMatrixLeft = this.Left;
                WindowSettings.PricingMatrixTop = this.Top;
                WindowSettings.PricingMatrixWidth = this.Width;
                WindowSettings.PricingMatrixMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


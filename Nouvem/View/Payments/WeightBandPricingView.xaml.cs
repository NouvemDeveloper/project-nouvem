﻿// -----------------------------------------------------------------------
// <copyright file="WeightBandPricingView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Payments
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Data;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WeightBandPricingView.xaml
    /// </summary>
    public partial class WeightBandPricingView : Window
    {
        private readonly string gridPath;

        public WeightBandPricingView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWeightBandPricingWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.gridPath = Settings.Default.WeightBandPricingGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Loaded += this.OnLoaded;
            this.Closing += this.OnClosing;
            Messenger.Default.Send(true, Token.WindowStatus);

            this.WindowState = WindowSettings.WeightBandPricingMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControl.RestoreLayoutFromXml(this.gridPath);
            }

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableViewData);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.ShowDialog();
                }
                catch
                {
                }
            });
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.WeightBandPricingHeight = this.Height;
            WindowSettings.WeightBandPricingLeft = this.Left;
            WindowSettings.WeightBandPricingTop = this.Top;
            WindowSettings.WeightBandPricingWidth = this.Width;
            WindowSettings.WeightBandPricingMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            this.GridControl.SaveLayoutToXml(this.gridPath);
            Messenger.Default.Unregister(this);
            this.Loaded -= this.OnLoaded;
            this.Closing -= this.OnClosing;
            BindingOperations.ClearAllBindings(this);
        }
    }
}


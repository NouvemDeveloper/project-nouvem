﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.MRP;

namespace Nouvem.View.MRP
{
    /// <summary>
    /// Interaction logic for MRPView.xaml
    /// </summary>
    public partial class MRPView : Window
    {
        public MRPView()
        {
            InitializeComponent();
            this.DataContext = new MRPViewModel();

            this.WindowState = WindowSettings.MRPMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<int?>(this, Token.StockModeChange, this.SetStockModeColumns);
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseMRPWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            //this.GridControlSearchData.View.ShowSearchPanel(true);
            var gridPathOrders = Settings.Default.MRPOrdersGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathData = Settings.Default.MRPDataGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Closing += (sender, args) =>
            {
                WindowSettings.MRPHeight = this.Height;
                WindowSettings.MRPLeft = this.Left;
                WindowSettings.MRPTop = this.Top;
                WindowSettings.MRPWidth = this.Width;
                WindowSettings.MRPMaximised = this.WindowState.ToBool();
                //this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
               // this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlMRPOrders.SaveLayoutToXml(gridPathOrders);
                this.GridControlMRPData.SaveLayoutToXml(gridPathData);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPathOrders))
                {
                    this.GridControlMRPOrders.RestoreLayoutFromXml(gridPathOrders);
                }

                if (File.Exists(gridPathData))
                {
                    this.GridControlMRPData.RestoreLayoutFromXml(gridPathData);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        //var preview = new DocumentPreviewWindow();
                        //var link = new PrintableControlLink(this.TableView);
                        //link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        //var model = new LinkPreviewModel(link);
                        //preview.Model = model;
                        //link.CreateDocument(true);
                        //preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void SetStockModeColumns(int? i)
        {
            this.ColumnBatch.Visible = true;
           
            this.ColumnOpenWgt.Visible = true;
            this.ColumnClosingWgt.Visible = true;
            this.ColumnAllocatedWgt.Visible = true;
            this.ColumnDueInWgt.Visible = true;
            this.ColumnOpenQty.Visible = true;
            this.ColumnClosingQty.Visible = true;
            this.ColumnAllocatedQty.Visible = true;
            this.ColumnDueInQty.Visible = true;
            if (i.IsNullOrZeroOrOne() || i == 6)
            {
                this.ColumnBatch.Visible = true;
                //this.ColumnBatchReference.Visible = true;
               
                this.ColumnOpenWgt.Visible = true;
                this.ColumnClosingWgt.Visible = true;
                this.ColumnAllocatedWgt.Visible = true;
                this.ColumnDueInWgt.Visible = true;
                this.ColumnOpenQty.Visible = true;
                this.ColumnClosingQty.Visible = true;
                this.ColumnAllocatedQty.Visible = true;
                this.ColumnDueInQty.Visible = true;
                //this.ColumnCommittedWgt.Visible = true;
                //this.ColumnAvailableWgt.Visible = true;
                //this.ColumnCommittedQty.Visible = true;
                //this.ColumnAvailableQty.Visible = true;
            }
            else if (i == 4)
            {
                this.ColumnBatch.Visible = true;
                //this.ColumnBatchReference.Visible = true;
               
                this.ColumnOpenWgt.Visible = false;
                this.ColumnClosingWgt.Visible = false;
                this.ColumnAllocatedWgt.Visible = false;
                this.ColumnDueInWgt.Visible = false;
                this.ColumnOpenQty.Visible = true;
                this.ColumnClosingQty.Visible = true;
                this.ColumnAllocatedQty.Visible = true;
                this.ColumnDueInQty.Visible = true;
                //this.ColumnCommittedWgt.Visible = false;
                //this.ColumnAvailableWgt.Visible = false;
                //this.ColumnCommittedQty.Visible = true;
                //this.ColumnAvailableQty.Visible = true;
            }
            else if (i == 5)
            {
                this.ColumnBatch.Visible = true;
                //this.ColumnBatchReference.Visible = true;
                
                this.ColumnOpenWgt.Visible = true;
                this.ColumnClosingWgt.Visible = true;
                this.ColumnAllocatedWgt.Visible = true;
                this.ColumnDueInWgt.Visible = true;
                this.ColumnOpenQty.Visible = false;
                this.ColumnClosingQty.Visible = false;
                this.ColumnAllocatedQty.Visible = false;
                this.ColumnDueInQty.Visible = false;
                //this.ColumnCommittedWgt.Visible = true;
                //this.ColumnAvailableWgt.Visible = true;
                //this.ColumnCommittedQty.Visible = false;
                //this.ColumnAvailableQty.Visible = false;
            }
            else if (i == 7)
            {
                this.ColumnBatch.Visible = false;
                //this.ColumnBatchReference.Visible = false;
               
                this.ColumnOpenWgt.Visible = false;
                this.ColumnClosingWgt.Visible = false;
                this.ColumnAllocatedWgt.Visible = false;
                this.ColumnDueInWgt.Visible = false;
                this.ColumnOpenQty.Visible = true;
                this.ColumnClosingQty.Visible = true;
                this.ColumnAllocatedQty.Visible = true;
                this.ColumnDueInQty.Visible = true;
                //this.ColumnCommittedWgt.Visible = false;
                //this.ColumnAvailableWgt.Visible = false;
                //this.ColumnCommittedQty.Visible = true;
                //this.ColumnAvailableQty.Visible = true;
            }
            else if (i == 8)
            {
                this.ColumnBatch.Visible = false;
                //this.ColumnBatchReference.Visible = false;
              
                this.ColumnOpenWgt.Visible = true;
                this.ColumnClosingWgt.Visible = true;
                this.ColumnAllocatedWgt.Visible = true;
                this.ColumnDueInWgt.Visible = true;
                this.ColumnOpenQty.Visible = false;
                this.ColumnClosingQty.Visible = false;
                this.ColumnAllocatedQty.Visible = false;
                this.ColumnDueInQty.Visible = false;
                //this.ColumnCommittedWgt.Visible = true;
                //this.ColumnAvailableWgt.Visible = true;
                //this.ColumnCommittedQty.Visible = false;
                //this.ColumnAvailableQty.Visible = false;
            }
            else
            {
                this.ColumnBatch.Visible = false;
                //this.ColumnBatchReference.Visible = false;
              
                this.ColumnOpenWgt.Visible = true;
                this.ColumnClosingWgt.Visible = true;
                this.ColumnAllocatedWgt.Visible = true;
                this.ColumnDueInWgt.Visible = true;
                this.ColumnOpenQty.Visible = true;
                this.ColumnClosingQty.Visible = true;
                this.ColumnAllocatedQty.Visible = true;
                this.ColumnDueInQty.Visible = true;
                //this.ColumnCommittedWgt.Visible = true;
                //this.ColumnAvailableWgt.Visible = true;
                //this.ColumnCommittedQty.Visible = true;
                //this.ColumnAvailableQty.Visible = true;
            }
        }

    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APReceipt
{
    using System.Globalization;
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for APQuoteView.xaml
    /// </summary>
    public partial class APReceiptView : UserControl
    {
        public APReceiptView()
        {
            this.InitializeComponent();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
            ThreadPool.QueueUserWorkItem(x => this.ComboBoxEditCode.Dispatcher.Invoke(() => this.ComboBoxEditCode.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.ComboBoxEditCode.Focus());
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenReceiptPanelView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------


namespace Nouvem.View.Purchases.APReceipt
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TouchscreenReceiptPanelView.xaml
    /// </summary>
    public partial class TouchscreenReceiptPanelView : UserControl
    {
        public TouchscreenReceiptPanelView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenSuppliersView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APReceipt
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenSuppliersView.xaml
    /// </summary>
    public partial class TouchscreenSuppliersView : Window
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public TouchscreenSuppliersView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseHiddenWindows, s => this.Close());
            #if !DEBUG
                this.Topmost = true;
            #endif

            Messenger.Default.Register<bool>(this, Token.CloseTouchscreenPartnersWindow, b =>
            {
                if (b)
                {
                    this.Visibility = Visibility.Collapsed;
                    this.Topmost = false;
                    if (this.Keyboard != null)
                    {
                        this.Keyboard.IsOpen = false;
                        if (this.GridSearchBox != null)
                        {
                            this.GridSearchBox.Height = 0;
                        }
                    }
                }
                else
                {
                    this.Visibility = Visibility.Visible;
                #if !DEBUG
                this.Topmost = true;
                #endif
                }
            });

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                ScrollViewer = null;
            };

            this.Loaded += (sender, args) =>
            {
                this.GridSearchBox.Height = 0;

                #region registration

                Messenger.Default.Register<string>(this, Token.DisplayKeyboard, s => this.DisplayKeyboard());

                #region scrolling

                Messenger.Default.Register<string>(this, Token.ScrollSuppliers, s =>
                {
                    if (ScrollViewer == null)
                    {
                        ScrollViewer = GetScrollViewer(this.GridControlSuppliers) as ScrollViewer;
                    }

                    if (ScrollViewer == null)
                    {
                        return;
                    }

                    if (s.Equals(Constant.Up))
                    {
                        // scroll up
                        ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - ApplicationSettings.TouchScreenSuppliersVerticalOffset);
                        return;
                    }

                    // scoll down
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + ApplicationSettings.TouchScreenSuppliersVerticalOffset);
                });

                #endregion

                #endregion
            };
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
         public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Open the keyboard.
        /// </summary>
        private void DisplayKeyboard()
        {
            this.GridSearchBox.Height = 50;
            this.Keyboard.IsOpen = true;
            this.TextBoxSearch.Focus();
        }

        /// <summary>
        /// Needed for immediate selection of the group selection within the data template.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }

        /// <summary>
        /// Needed for immediate selection of the group selection within the data template.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        /// <summary>
        /// Workaround, to enable the grid settings storage when auto populate columns is on.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        /// <remarks>TODO: No need for this from version 15.1 up. (Remove).</remarks>
        private void GridControlSuppliers_AutoGeneratedColumns(object sender, RoutedEventArgs e)
        {
            //for (var i = 0; i < this.GridControlSuppliers.Columns.Count; i++)
            //{
            //    this.GridControlSuppliers.Columns[i].Name = string.Format("Column{0}", i);
            //}

            //if (File.Exists(Settings.Default.SuppliersDataGrid))
            //{
            //    this.GridControlSuppliers.RestoreLayoutFromXml(Settings.Default.SuppliersDataGrid);
            //}
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            this.GridSearchBox.Height = 0;
        }
    }
}

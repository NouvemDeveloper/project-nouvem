﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenReceiptProductiontPanel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APReceipt
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TouchscreenReceipProductiontPanel.xaml
    /// </summary>
    public partial class TouchscreenReceiptProductionPanel : UserControl
    {
        public TouchscreenReceiptProductionPanel()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenOrdersView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Grid;

namespace Nouvem.View.Purchases.APReceipt
{
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenOrdersView.xaml
    /// </summary>
    public partial class TouchscreenTransactionsManagerView : UserControl
    {
        public TouchscreenTransactionsManagerView()
        {
            this.InitializeComponent();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");

            this.Unloaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                //this.GridControlTransactions.SaveLayoutToXml(Settings.Default.TransactionsDataGrid);
            };

            this.Loaded += (sender, args) =>
            {
                #region registration

                Messenger.Default.Register<string>(this, Token.DisplayKeyboard, s => this.DisplayKeyboard());

                Messenger.Default.Register<string>(this, Token.FilterTransactions, s =>
                {
                    var filter = string.Format("([Serial] LIKE '{0}%')", s);
                    //this.GridControlTransactions.FilterString = filter;
                });

                // open/close the calender.
                Messenger.Default.Register<bool>(this, Token.DisplayCalender, b => this.DatePickerOrderReceived.IsDropDownOpen = b);

                #endregion

                // TODO Reimplement when we purchase 15.1
                //if (File.Exists(Settings.Default.SuppliersDataGrid))
                //{
                //    this.GridControlSuppliers.RestoreLayoutFromXml(Settings.Default.SuppliersDataGrid);
                //}
            };
        }

        /// <summary>
        /// Workaround, to enable the grid settings storage when auto populate columns is on.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        /// <remarks>TODO: No need for this from version 15.1 up. (Remove).</remarks>
        private void GridControlTransactions_AutoGeneratedColumns(object sender, RoutedEventArgs e)
        {
            //for (var i = 0; i < this.GridControlTransactions.Columns.Count; i++)
            //{
            //    this.GridControlTransactions.Columns[i].Name = string.Format("Column{0}", i);
            //}

            if (File.Exists(Settings.Default.TransactionsDataGrid))
            {
                //this.GridControlTransactions.RestoreLayoutFromXml(Settings.Default.TransactionsDataGrid);
            }
        }

        /// <summary>
        /// Open the keyboard.
        /// </summary>
        private void DisplayKeyboard()
        {
            //this.GridControlTransactions.View.ShowSearchPanel(true);
            this.Keyboard.IsOpen = true;
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            //this.GridControlTransactions.View.HideSearchPanel();
            //this.GridControlTransactions.View.ShowFilterPanelMode = ShowFilterPanelMode.Never;
        }
    }
}


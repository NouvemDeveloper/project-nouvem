﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenOrdersView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows.Input;
using DevExpress.Data.Linq;
using Nouvem.Model.BusinessObject;

namespace Nouvem.View.Purchases.APReceipt
{
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenOrdersView.xaml
    /// </summary>
    public partial class TouchscreenOrdersView : Window
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public TouchscreenOrdersView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseHiddenWindows, s => this.Close());
            Messenger.Default.Register<string>(this, Token.GetOrders, s =>
            {
                var orders = new List<Sale>();
                foreach (var order in this.GridControlOrders.SelectedItems)
                {
                    if (order != null && order is Sale)
                    {
                        orders.Add((Sale)order);
                    }
                }

                Messenger.Default.Send(orders, Token.SendOrders);
            });

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
            #if !DEBUG
            this.Topmost = true;
            #endif

            Messenger.Default.Register<bool>(this, Token.CloseTouchscreenOrdersWindow, b =>
            {
                if (b)
                {
                    this.Visibility = Visibility.Collapsed;
                    this.Topmost = false;
                    if (this.Keyboard != null)
                    {
                        this.Keyboard.IsOpen = false;
                        if (this.GridSearchBox != null)
                        {
                            this.GridSearchBox.Height = 0;
                        }
                    }
                }
                else
                {
                    this.Visibility = Visibility.Visible;
                    #if !DEBUG
                    this.Topmost = true;
                    #endif
                }
            });

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                //this.GridControlOrders.SaveLayoutToXml(Settings.Default.OrdersDataGrid);
                ScrollViewer = null;
            };

            this.Loaded += (sender, args) =>
            {
                #region registration

                #region scolling

                Messenger.Default.Register<string>(this, Token.ScrollOrders, s =>
                {
                    if (ScrollViewer == null)
                    {
                        ScrollViewer = GetScrollViewer(this.GridControlOrders) as ScrollViewer;
                    }

                    if (ScrollViewer == null)
                    {
                        return;
                    }

                    if (s.Equals(Constant.Up))
                    {
                        // scroll up
                        ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - ApplicationSettings.TouchScreenOrdersVerticalOffset);
                        return;
                    }

                    // scoll down
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + ApplicationSettings.TouchScreenOrdersVerticalOffset);
                });

                #endregion

                this.GridSearchBox.Height = 0;

                // open/close the calender.
                Messenger.Default.Register<bool>(this, Token.DisplayCalender, b => this.DatePickerOrderReceived.IsDropDownOpen = b);

                Messenger.Default.Register<string>(this, Token.DisplayOrderKeyboard, s => this.DisplayKeyboard());

                #endregion

                // TODO Reimplement when we purchase 15.1
                //if (File.Exists(Settings.Default.SuppliersDataGrid))
                //{
                //    this.GridControlSuppliers.RestoreLayoutFromXml(Settings.Default.SuppliersDataGrid);
                //}
            };
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }

        ///// <summary>
        ///// Workaround, to enable the grid settings storage when auto populate columns is on.
        ///// </summary>
        ///// <param name="sender">The sender object.</param>
        ///// <param name="e">The event args.</param>
        ///// <remarks>TODO: No need for this from version 15.1 up. (Remove).</remarks>
        private void GridControlOrders_AutoGeneratedColumns(object sender, RoutedEventArgs e)
        {
            //for (var i = 0; i < this.GridControlOrders.Columns.Count; i++)
            //{
            //    this.GridControlOrders.Columns[i].Name = string.Format("Column{0}", i);
            //}

            //if (File.Exists(Settings.Default.OrdersDataGrid))
            //{
            //    this.GridControlOrders.RestoreLayoutFromXml(Settings.Default.OrdersDataGrid);
            //}
        }

        /// <summary>
        /// Open the keyboard.
        /// </summary>
        private void DisplayKeyboard()
        {
            this.GridSearchBox.Height = 50;
            this.Keyboard.IsOpen = true;
            this.TextBoxSearch.Focus();
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            this.GridSearchBox.Height = 0;
        }
    }
}

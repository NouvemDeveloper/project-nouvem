﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptItemsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Input;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using Nouvem.Shared;

namespace Nouvem.View.Purchases.APReceipt
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for APReceiptItemsView.xaml
    /// </summary>
    public partial class APReceiptItemsView : UserControl
    {
        private int rowHandle;

        public APReceiptItemsView()
        {
            this.InitializeComponent();
            var gridPath = Settings.Default.ReceiptItemsPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Unloaded += (sender, args) =>
            {
                this.GridControlSaleDetails.SaveLayoutToXml(gridPath);
            };

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                this.GridControlSaleDetails.SetCellValue(this.rowHandle, "Notes", s);
            });

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlSaleDetails.RestoreLayoutFromXml(gridPath);
                    }
                    catch
                    {
                    }
                }
            };
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            if (this.rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlSaleDetails.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlSaleDetails.CurrentColumn == this.GridControlSaleDetails.Columns["Notes"])
                {
                    var value = this.GridControlSaleDetails.GetCellValue(this.rowHandle, "Notes");
                    var localValue = value == null ? string.Empty : value.ToString();
                    Messenger.Default.Send(localValue, Token.DisplayNotes);
                }
            }
        }

        private void TableViewData_ShownEditor(object sender, DevExpress.Xpf.Grid.EditorEventArgs e)
        {
            try
            {
                if (this.TableViewData.ActiveEditor is TextEdit)
                {
                    (this.TableViewData.ActiveEditor as TextEdit).GotFocus += (o, args) => (o as TextEdit).SelectAll();
                }
            }
            catch (Exception ex)
            {
                var log = new Logging.Logger();
                log.LogError(this.GetType(), ex.Message);
            }
        }

        void OnInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var view = (TableView)sender;
            Dispatcher.BeginInvoke(new Action(
                () => {
                    view.FocusedRowHandle = GridControl.InvalidRowHandle;
                    var newItemRowVisibleIndex = view.Grid.GetRowVisibleIndexByHandle(GridControl.NewItemRowHandle);
                    var editedRowVisibleIndex = newItemRowVisibleIndex;
                    switch (view.NewItemRowPosition)
                    {
                        case NewItemRowPosition.Bottom: editedRowVisibleIndex -= 1; break;
                        case NewItemRowPosition.Top: editedRowVisibleIndex += 1; break;
                    }
                    var editedRowHandle = view.Grid.GetRowHandleByVisibleIndex(editedRowVisibleIndex);
                    view.FocusedRowHandle = editedRowHandle;
                    view.ShowEditor();
                }
                ));
        }

        private void TableViewData_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var hitInfo = this.TableViewData.CalcHitInfo(e.OriginalSource as DependencyObject);

                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.FieldName;
                Messenger.Default.Send(currentColumnHeader, Token.GridColumn);
            }
            catch
            {
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Purchases.APInvoice;

namespace Nouvem.View.Purchases.APInvoice
{
    /// <summary>
    /// Interaction logic for APInvoiceView.xaml
    /// </summary>
    public partial class APInvoiceView : Window
    {
        public APInvoiceView()
        {
            InitializeComponent();

            this.WindowState = WindowSettings.APInvoiceMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseInvoiceWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.Closing += (sender, args) =>
            {
                WindowSettings.APInvoiceHeight = this.Height;
                WindowSettings.APInvoiceLeft = this.Left;
                WindowSettings.APInvoiceTop = this.Top;
                WindowSettings.APInvoiceWidth = this.Width;
                WindowSettings.APInvoiceMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SaleContentsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Input;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;

namespace Nouvem.View.Purchases
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for PurchaseContentsView.xaml
    /// </summary>
    public partial class PurchaseContentsView : UserControl
    {
        private int rowHandle;

        public PurchaseContentsView()
        {
            this.InitializeComponent();
            this.Unloaded += (sender, args) =>
            {
                this.GridControlPurchaseDetails.SaveLayoutToXml(Settings.Default.PurchaseDetailsPath);
                //Messenger.Default.Unregister(this);

            };

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                this.GridControlPurchaseDetails.SetCellValue(this.rowHandle, "Notes", s);
            });

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.PurchaseDetailsPath))
                {
                    this.GridControlPurchaseDetails.RestoreLayoutFromXml(Settings.Default.PurchaseDetailsPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        void OnInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var view = (TableView)sender;
            Dispatcher.BeginInvoke(new Action(
                () => {
                    view.FocusedRowHandle = GridControl.InvalidRowHandle;
                    var newItemRowVisibleIndex = view.Grid.GetRowVisibleIndexByHandle(GridControl.NewItemRowHandle);
                    var editedRowVisibleIndex = newItemRowVisibleIndex;
                    switch (view.NewItemRowPosition)
                    {
                        case NewItemRowPosition.Bottom: editedRowVisibleIndex -= 1; break;
                        case NewItemRowPosition.Top: editedRowVisibleIndex += 1; break;
                    }
                    var editedRowHandle = view.Grid.GetRowHandleByVisibleIndex(editedRowVisibleIndex);
                    view.FocusedRowHandle = editedRowHandle;
                    view.ShowEditor();
                }
                ));
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            if (this.rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlPurchaseDetails.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlPurchaseDetails.CurrentColumn == this.GridControlPurchaseDetails.Columns["Notes"])
                {
                    var value = this.GridControlPurchaseDetails.GetCellValue(this.rowHandle, "Notes");
                    var localValue = value == null ? string.Empty : value.ToString();
                    Messenger.Default.Send(localValue, Token.DisplayNotes);
                }
            }
        }

        private void TableViewData_ShownEditor(object sender, DevExpress.Xpf.Grid.EditorEventArgs e)
        {
            try
            {
                if (this.TableViewData.ActiveEditor is TextEdit)
                {
                    (this.TableViewData.ActiveEditor as TextEdit).GotFocus += (o, args) => (o as TextEdit).SelectAll();
                }
            }
            catch (Exception ex)
            {
                var log = new Logging.Logger();
                log.LogError(this.GetType(), ex.Message);
            }
        }

        private void TableViewData_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var hitInfo = this.TableViewData.CalcHitInfo(e.OriginalSource as DependencyObject);

                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.FieldName;
                Messenger.Default.Send(currentColumnHeader, Token.GridColumn);
            }
            catch
            {
            }
        }
    }
}

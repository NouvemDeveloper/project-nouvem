﻿// -----------------------------------------------------------------------
// <copyright file="APOrderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APOrder
{
    using System.Globalization;
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for APOrderView.xaml
    /// </summary>
    public partial class APOrderView : UserControl
    {
        public APOrderView()
        {
            this.InitializeComponent();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
            ThreadPool.QueueUserWorkItem(x => this.ComboBoxEditCode.Dispatcher.Invoke(() => this.ComboBoxEditCode.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.ComboBoxEditCode.Focus());
        }
    }
}

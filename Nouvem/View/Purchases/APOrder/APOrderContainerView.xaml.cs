﻿// -----------------------------------------------------------------------
// <copyright file="APOrderContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Purchases.APOrder
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for APOrderContainerView.xaml
    /// </summary>
    public partial class APOrderContainerView : Window
    {
        private bool canClose;
        public APOrderContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.CanClosePurchaseOrderWindow, b => this.canClose = b);
            Messenger.Default.Register<string>(this, Token.CloseAPOrderWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.WindowState = WindowSettings.IntakeOrderMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                if (!this.canClose)
                {
                    args.Cancel = true;
                    Messenger.Default.Send(Token.Message, Token.ClosingPurchaseOrderWindow);
                    return;
                }

                WindowSettings.IntakeOrderHeight = this.Height;
                WindowSettings.IntakeOrderLeft = this.Left;
                WindowSettings.IntakeOrderTop = this.Top;
                WindowSettings.IntakeOrderWidth = this.Width;
                WindowSettings.IntakeOrderMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

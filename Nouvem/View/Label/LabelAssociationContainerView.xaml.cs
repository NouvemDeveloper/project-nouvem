﻿// -----------------------------------------------------------------------
// <copyright file="LabelAssociationContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.Label
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LabelAssociationContainerView.xaml
    /// </summary>
    public partial class LabelAssociationContainerView : Window
    {
        public LabelAssociationContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseLabelAssociationWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            var gridPath = Settings.Default.LabelAssociationGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.WindowState = WindowSettings.LabelAssociationMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSaleDetails.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlSaleDetails.FilterCriteria = null;
                this.GridControlSaleDetails.SaveLayoutToXml(gridPath);
                WindowSettings.LabelAssociationHeight = this.Height;
                WindowSettings.LabelAssociationLeft = this.Left;
                WindowSettings.LabelAssociationTop = this.Top;
                WindowSettings.LabelAssociationWidth = this.Width;
                WindowSettings.LabelAssociationMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


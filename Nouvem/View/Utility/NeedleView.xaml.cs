﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Gauges;
using Nouvem.Global;

namespace Nouvem.View.Utility
{
    /// <summary>
    /// Interaction logic for NeedleView.xaml
    /// </summary>
    public partial class NeedleView : UserControl
    {
        public NeedleView()
        {
            InitializeComponent();
            if (ApplicationSettings.UseNeedleEcoTheme)
            {
                this.CircularGaugeControl.Model = new CircularEcoModel();
            }
            else
            {
                this.CircularGaugeControl.Model = new CircularClassicModel();
            }
        }
    }
}

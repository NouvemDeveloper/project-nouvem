﻿// -----------------------------------------------------------------------
// <copyright file="TareCalculatorView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Utility
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TareCalculatorView.xaml
    /// </summary>
    public partial class TareCalculatorView : Window
    {
        public TareCalculatorView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseTareCalculatorWindow, s => this.Close());
            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SqlGeneratorContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Utility
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for SqlGeneratoContainerView.xaml
    /// </summary>
    public partial class SqlGeneratorContainerView : Window
    {
        public SqlGeneratorContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseSqlGeneratorWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


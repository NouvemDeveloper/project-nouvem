﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Logging;
using Nouvem.Shared;

namespace Nouvem.View.Utility
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : Window
    {
        private string imagePath;
        public ImageView()
        {
            this.InitializeComponent();
            
            this.Closing += (sender, args) =>
            {
                WindowSettings.ImageViewHeight = this.Height;
                WindowSettings.ImageViewLeft = this.Left;
                WindowSettings.ImageViewTop = this.Top;
                WindowSettings.ImageViewWidth = this.Width;
                WindowSettings.ImageViewMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(Token.Message,Token.LabelViewerClosed);
            };
        }

        public string ImagePath
        {
            get { return this.imagePath; }
            set
            {
                this.imagePath = value;
                try
                {
                    var uriSource = new Uri(value, UriKind.Absolute);
                    this.Image.Source = new BitmapImage(uriSource);
                }
                catch (Exception e)
                {
                    var log = new Logger();
                    log.LogError(this.GetType(), e.Message);
                }
            }
        }
    }
}

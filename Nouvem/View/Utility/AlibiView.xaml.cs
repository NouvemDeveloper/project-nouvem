﻿// -----------------------------------------------------------------------
// <copyright file="AlibiView.cs" company="Nouvem TLimited">
// Copyright (c) Nouvem Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Utility
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for AlibiView.xaml
    /// </summary>
    public partial class AlibiView : UserControl
    {
        public AlibiView()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) => this.TextBoxAlibi.Focus();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Utility
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SystemInformationView.xaml
    /// </summary>
    public partial class SystemInformationView : Window
    {
        public SystemInformationView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseSystemInformationWindow, s => this.Close());
        }
    }
}

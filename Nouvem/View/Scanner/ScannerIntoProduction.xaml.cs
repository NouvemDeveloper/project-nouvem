﻿// -----------------------------------------------------------------------
// <copyright file="ScannerIntoProduction.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Controls;
using DevExpress.Xpf.Editors;
using Nouvem.Model.Enum;

namespace Nouvem.View.Scanner
{
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerIntoProduction.xaml
    /// </summary>
    public partial class ScannerIntoProduction : Window
    {
        public ScannerIntoProduction()
        {
            this.InitializeComponent();

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            Messenger.Default.Register<string>(this, Token.CloseScannerIntoProduction, s => this.Close());
            ThreadPool.QueueUserWorkItem(x => this.TextBoxBarcode.Dispatcher.Invoke(() => this.TextBoxBarcode.Focus()));
            this.Loaded += (sender, args) => this.TextBoxBarcode.Focus();
            Messenger.Default.Register<string>(Token.Message, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }

        private void ComboBoxEditOrders_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ComboBoxEdit)
            {
                var localCombo = (sender as ComboBoxEdit);
                localCombo.ShowPopup();
            }

            //this.TextBoxBarcode.Focus();
        }

        private void ComboBoxEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }

        private void ComboBoxEditOrders_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Controls;
using System.Windows.Input;

namespace Nouvem.View.Scanner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerProductionbatchSetUpView.xaml
    /// </summary>
    public partial class ScannerProductionbatchSetUpView : Window
    {
        public ScannerProductionbatchSetUpView()
        {
            this.InitializeComponent();
            this.ComboBoxSpecs.Focus();

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            Messenger.Default.Register<string>(Token.Message, Token.CloseScannerBatchSetUp, s => this.Close());
           this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
        }

        private void ComboBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ComboBox)
            {
                var localCombo = (sender as ComboBox);
                localCombo.IsDropDownOpen = true;
            }

            //this.TextBoxBarcode.Focus();
        }
    }
}

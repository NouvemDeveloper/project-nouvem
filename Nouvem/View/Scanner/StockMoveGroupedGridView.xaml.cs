﻿// -----------------------------------------------------------------------
// <copyright file="StockMoveGroupedGridView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for StockMoveGroupedGrid.xaml
    /// </summary>
    public partial class StockMoveGroupedGridView : UserControl
    {
        public StockMoveGroupedGridView()
        {
            this.InitializeComponent();
        }

        private void GridControlDetails_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }
    }
}

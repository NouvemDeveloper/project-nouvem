﻿// -----------------------------------------------------------------------
// <copyright file="ScannerOrdersView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ScannerOrdersView.xaml
    /// </summary>
    public partial class ScannerOrdersView : UserControl
    {
        public ScannerOrdersView()
        {
            this.InitializeComponent();
        }
    }
}

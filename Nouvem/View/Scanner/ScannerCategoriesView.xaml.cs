﻿// -----------------------------------------------------------------------
// <copyright file="SequencerCategoriesView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SequencerCategoriesView.xaml
    /// </summary>
    public partial class SequencerCategoriesView : Window
    {
        public SequencerCategoriesView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseScannerCategoryWindow, s =>
            {
                Messenger.Default.Unregister(this);
                this.Close();
            });

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                //this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
                this.Left = ApplicationSettings.ScannerEmulatorLeft;
                this.Top = ApplicationSettings.ScannerEmulatorTop;
                this.MinHeight = 300;
                this.MinWidth = 300;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }
    }
}


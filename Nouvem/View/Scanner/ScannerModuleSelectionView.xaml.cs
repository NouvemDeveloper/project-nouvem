﻿// -----------------------------------------------------------------------
// <copyright file="ScannerModuleSelectionView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Nouvem.ViewModel;

namespace Nouvem.View.Scanner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerModuleSelectionView.xaml
    /// </summary>
    public partial class ScannerModuleSelectionView : Window
    {
        public ScannerModuleSelectionView()
        {
            this.InitializeComponent();
            this.LabelVersion.Content = string.Format("Version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
            if (ApplicationSettings.ScannerEmulatorMode)
            {
                //this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
                this.Left = ApplicationSettings.ScannerEmulatorLeft;
                this.Top = ApplicationSettings.ScannerEmulatorTop;
                this.MinHeight = 300;
                this.MinWidth = 300;
                this.WindowStyle = WindowStyle.None;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetScannerSize, x =>
            {
                ApplicationSettings.ScannerEmulatorHeight = this.Height;
                ApplicationSettings.ScannerEmulatorWidth = this.Width;
                ApplicationSettings.ScannerEmulatorLeft = this.Left;
                ApplicationSettings.ScannerEmulatorTop = this.Top;
            });

            this.Loaded += (sender, args) =>
            {
                var localModules = ViewModelLocator.MasterStatic.AllowedHandheldModules;
                if (localModules != null)
                {
                    var count = localModules.Count + 1;
                    var margins = (count * 5);
                    var localHeight = (this.Grid.ActualHeight - margins) / count;
                    if (!localModules.Any(x => x.UserGroupID == 133))
                    {
                        this.ButtonDispatch.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonDispatch.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 134))
                    {
                        this.ButtonStockTake.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonStockTake.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 135))
                    {
                        this.ButtonStockMove.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonStockMove.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 136))
                    {
                        this.ButtonIntoProd.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonIntoProd.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 137))
                    {
                        this.ButtonPallet.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonPallet.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 138))
                    {
                        this.ButtonSequencer.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonSequencer.Height = localHeight;
                    }

                    this.ButtonClose.Height = localHeight;
                }
                else
                {
                    var count = 8;
                    var margins = (count * 5);
                    var localHeight = (this.Grid.ActualHeight - margins) / count;
                    this.ButtonDispatch.Height = localHeight;
                    this.ButtonStockTake.Height = localHeight;
                    this.ButtonStockMove.Height = localHeight;
                    this.ButtonIntoProd.Height = localHeight;
                    this.ButtonPallet.Height = localHeight;
                    this.ButtonSequencer.Height = localHeight;
                    this.ButtonClose.Height = localHeight;
                }
            };

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                ApplicationSettings.ScannerEmulatorHeight = this.Height;
                ApplicationSettings.ScannerEmulatorWidth = this.Width;
                ApplicationSettings.ScannerEmulatorLeft = this.Left;
                ApplicationSettings.ScannerEmulatorTop = this.Top;
            };

            Messenger.Default.Register<string>(this, Token.CloseScannerModuleSelection, x => this.Close());
           
            //this.WaitIndicator.Visibility = Visibility.Visible;
        }
    }
}

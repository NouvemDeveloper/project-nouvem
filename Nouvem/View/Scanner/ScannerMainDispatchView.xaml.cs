﻿// -----------------------------------------------------------------------
// <copyright file="ScannerMainDispatchView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;

namespace Nouvem.View.Scanner
{
    using System.Windows.Controls;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerMainDispatchView.xaml
    /// </summary>
    public partial class ScannerMainDispatchView : UserControl
    {
        public ScannerMainDispatchView()
        {
            this.InitializeComponent();

            if (ApplicationSettings.OnlyShowDispatchedWeightsAtScannerDispatch)
            {
                this.GridControlBandOrdered.Visible = false;
                this.GridControlBandOutstanding.Visible = false;
                this.GridColumnProduct.MinWidth = 150;
            }

            if (ApplicationSettings.ShowBoxCountAtScannerDispatch)
            {
                this.GridControlBandBox.Visible = true;
            }

            if (!ApplicationSettings.ShowBoxCountAtScannerDispatch)
            {
                this.GridColumnBoxCount.Width = 0;
                this.GridColumnBoxCount.Visible = false;
                this.GridControlBandBox.Width = 0;
                this.GridControlBandBox.Visible = false;
            }
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        private void TextBox_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }
    }
}

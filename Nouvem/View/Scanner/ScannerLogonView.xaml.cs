﻿// -----------------------------------------------------------------------
// <copyright file="ScannerLogonView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerLogonView.xaml
    /// </summary>
    public partial class ScannerLogonView : Window
    {
        public ScannerLogonView()
        {
            this.InitializeComponent();

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxUserId.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToPasswordBox, x => this.PasswordBoxPassword.Focus());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseLogin, x => this.Close());

            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }

        private void PasswordBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxPassword.Password, Token.UserPassword);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Nouvem.View.Scanner
{
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerPalletisationView.xaml
    /// </summary>
    public partial class ScannerPalletisationView : Window
    {
        public ScannerPalletisationView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWindow, s=> this.Close());
            Messenger.Default.Register<string>(this, Token.ClosePalletWindow, s => this.Close());


#if DEBUG
            this.TextBoxBarcode.Height = 30;
            this.TextBoxBarcode.Width = 30;
#endif

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                //this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
                this.Left = ApplicationSettings.ScannerEmulatorLeft;
                this.Top = ApplicationSettings.ScannerEmulatorTop;
                this.MinHeight = 300;
                this.MinWidth = 300;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            ThreadPool.QueueUserWorkItem(x => this.TextBoxBarcode.Dispatcher.Invoke(() => this.TextBoxBarcode.Focus()));
            this.Loaded += (sender, args) => this.TextBoxBarcode.Focus();
            Messenger.Default.Register<string>(Token.Message, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }

        private void GridControlSaleDetails_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}

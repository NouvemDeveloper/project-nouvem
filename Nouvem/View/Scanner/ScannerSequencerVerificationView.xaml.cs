﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Scanner
{
    /// <summary>
    /// Interaction logic for ScannerSequencerVerificationView.xaml
    /// </summary>
    public partial class ScannerSequencerVerificationView : UserControl
    {
        public ScannerSequencerVerificationView()
        {
            InitializeComponent();
        }

        private void UserControl_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.FocusToButton);
        }
    }
}

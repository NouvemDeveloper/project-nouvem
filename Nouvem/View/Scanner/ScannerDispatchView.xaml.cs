﻿// -----------------------------------------------------------------------
// <copyright file="ScannerDispatchView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;

namespace Nouvem.View.Scanner
{
    using System.Windows.Controls;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerDispatchView.xaml
    /// </summary>
    public partial class ScannerDispatchView : UserControl
    {
        public ScannerDispatchView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxBarcode.Dispatcher.Invoke(() => this.TextBoxBarcode.Focus()));
            this.Loaded += (sender, args) => this.TextBoxBarcode.Focus();
            Messenger.Default.Register<string>(Token.Message, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());

#if DEBUG
            this.TextBoxBarcode.Height = 30;
            this.TextBoxBarcode.Width = 30;
#endif
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }

        private void TextBoxBarcode_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}

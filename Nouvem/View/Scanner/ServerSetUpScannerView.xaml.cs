﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Scanner
{
    /// <summary>
    /// Interaction logic for ServerSetUpScannerView.xaml
    /// </summary>
    public partial class ServerSetUpScannerView : Window
    {
        public ServerSetUpScannerView()
        {
            InitializeComponent();
            this.WindowState = WindowState.Maximized;
            Messenger.Default.Register<string>(this, Token.CloseServerSetUpWindow, x => this.Close());
            System.Threading.ThreadPool.QueueUserWorkItem(
                (a) =>
                {
                    System.Threading.Thread.Sleep(100);
                    this.TextBoxCompanyName.Dispatcher.Invoke(() => this.TextBoxCompanyName.Focus());
                });

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxCompanyName.Focus());
            Messenger.Default.Register<string>(this, Token.DBStoredPassword, s =>
            {
                this.PasswordBoxPassword.Password = s;
                this.TextBoxCompanyName.Focus();
            });
        }

        private void PasswordBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxPassword.Password, Token.DBPassword);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ServerSetUpContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Logic
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ServerSetUpContainerView.xaml
    /// </summary>
    public partial class ServerSetUpContainerView : Window
    {
        public ServerSetUpContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseServerSetUpWindow, x => this.Close());
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="WarehouseLocationContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Plant
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for WarehouseLocationContainerView.xaml
    /// </summary>
    public partial class WarehouseLocationContainerView : Window
    {
        public WarehouseLocationContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseWarehouseLocationWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TelesalesSearchView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Telesales
{
    using System.IO;
    using System.Windows;
    using DevExpress.Xpf.Printing;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for WorkflowSearchView.xaml
    /// </summary>
    public partial class TelesalesSearchView : Window
    {
        private string gridPath;

        public TelesalesSearchView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.TelesalesSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.GridControlSearchData.View.ShowSearchPanel(true);

            this.gridPath = Settings.Default.TelesalesSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += this.OnClosing;
            this.Loaded += this.OnLoaded;       
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists(gridPath))
            {
                this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
            }

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableView);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.ShowDialog();
                }
                catch
                {
                }
            });
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WindowSettings.TelesalesSearchHeight = this.Height;
            WindowSettings.TelesalesSearchLeft = this.Left;
            WindowSettings.TelesalesSearchTop = this.Top;
            WindowSettings.TelesalesSearchWidth = this.Width;
            WindowSettings.TelesalesSearchMaximised = this.WindowState.ToBool();
            this.GridControlSearchData.FilterCriteria = null;
            Messenger.Default.Send(false, Token.WindowStatus);
            this.GridControlSearchData.View.SearchString = string.Empty;
            this.GridControlSearchData.SaveLayoutToXml(gridPath);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }
    }
}


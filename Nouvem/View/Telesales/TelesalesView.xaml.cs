﻿// -----------------------------------------------------------------------
// <copyright file="TelesalesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;

namespace Nouvem.View.Telesales
{
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using DevExpress.Xpf.Editors;
    using DevExpress.Xpf.Printing;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TelesalesView.xaml
    /// </summary>
    public partial class TelesalesView : Window
    {
        private readonly string gridPath;

        public TelesalesView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseTelesalesWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.Loaded += this.OnLoaded;
            this.Closing += this.OnClosing;
            this.gridPath = Settings.Default.TelesalesGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.WindowState = WindowSettings.TelesalesMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            ThreadPool.QueueUserWorkItem(x => this.ButtonOrder.Dispatcher.Invoke(() => this.ButtonOrder.Focus()));

            Messenger.Default.Register<string>(this, Token.FocusToButton, s =>
            {
                ThreadPool.QueueUserWorkItem(x => this.ButtonOrder.Dispatcher.Invoke(() => this.ButtonOrder.Focus()));
            });
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControlSearchData.RestoreLayoutFromXml(this.gridPath);
            }

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableView);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.Show();
                }
                catch
                {
                }
            });
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            this.GridControlSearchData.FilterCriteria = null;
            this.GridControlSearchData.View.SearchString = string.Empty;
            this.GridControlSearchData.SaveLayoutToXml(this.gridPath);
            WindowSettings.TelesalesHeight = this.Height;
            WindowSettings.TelesalesLeft = this.Left;
            WindowSettings.TelesalesTop = this.Top;
            WindowSettings.TelesalesWidth = this.Width;
            WindowSettings.TelesalesMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }

        private void ComboBoxEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            if (e.CloseMode == PopupCloseMode.Normal)
            {
                var users = this.ComboBoxEditUsers.SelectedItems;
                Messenger.Default.Send(users, Token.UpdateUsers);
            }
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="TelesalesSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Telesales
{
    using System.ComponentModel;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TelesalesSetUpView.xaml
    /// </summary>
    public partial class TelesalesSetUpView : Window
    {
        public TelesalesSetUpView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseTelesalesSetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.Closing += this.OnClosing;

            this.WindowState = WindowSettings.TelesalesSetUpMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.TelesalesSetUpHeight = this.Height;
            WindowSettings.TelesalesSetUpLeft = this.Left;
            WindowSettings.TelesalesSetUpTop = this.Top;
            WindowSettings.TelesalesSetUpWidth = this.Width;
            WindowSettings.TelesalesSetUpMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
        }
    }
}

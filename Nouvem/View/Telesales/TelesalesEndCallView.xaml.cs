﻿// -----------------------------------------------------------------------
// <copyright file="TelesalesEndCallView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;

namespace Nouvem.View.Telesales
{
    using System.Threading;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TelesalesEndCallView.xaml
    /// </summary>
    public partial class TelesalesEndCallView : Window
    {
        public TelesalesEndCallView()
        {
            this.InitializeComponent();
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseTelesalesWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseTelesalesEndCallWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            ThreadPool.QueueUserWorkItem(x => this.ButtonUpdate.Dispatcher.Invoke(() => this.ButtonUpdate.Focus()));
            this.Closing += this.OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.TelesalesEndCallHeight = this.Height;
            WindowSettings.TelesalesEndCallLeft = this.Left;
            WindowSettings.TelesalesEndCallTop = this.Top;
            WindowSettings.TelesalesEndCallWidth = this.Width;
            this.Closing -= this.OnClosing;
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenLoginView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.StartUp
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenLoginView.xaml
    /// </summary>
    public partial class TouchscreenLoginView : Window
    {
        public TouchscreenLoginView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.SetTopMost, b => this.Topmost = b);
            Messenger.Default.Register<string>(this, Token.CloseLogin, s =>
            {
                this.Close();
            });
        }

        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }
    }
}

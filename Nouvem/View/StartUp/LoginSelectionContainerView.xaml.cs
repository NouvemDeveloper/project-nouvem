﻿// -----------------------------------------------------------------------
// <copyright file="LoginSelectionContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.StartUp
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LoginSelectionView.xaml
    /// </summary>
    public partial class LoginSelectionContainerView : Window
    {
        public LoginSelectionContainerView()
        {
            this.InitializeComponent();
            
            Messenger.Default.Register<string>(this, Token.CloseLoginSelection, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Unregister(this);
            };
        }
    }
}

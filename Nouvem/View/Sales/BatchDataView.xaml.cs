﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Sales;

namespace Nouvem.View.Sales
{
    /// <summary>
    /// Interaction logic for BatchDataView.xaml
    /// </summary>
    public partial class BatchDataView : Window
    {
        public BatchDataView(Tuple<int?, IList<App_GetOrderBatchData_Result>> productData)
        {
            this.InitializeComponent();
            this.DataContext = new BatchDataViewModel(productData);
            this.WindowState = WindowSettings.BatchDataMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseBatchDataWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
         
            var gridPath = Settings.Default.BatchDataGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Closing += (sender, args) =>
            {
                WindowSettings.BatchDataHeight = this.Height;
                WindowSettings.BatchDataLeft = this.Left;
                WindowSettings.BatchDataTop = this.Top;
                WindowSettings.BatchDataWidth = this.Width;
                WindowSettings.BatchDataMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                    }
                    catch (Exception e)
                    {
                    }
                }
            };
        }
    }
}

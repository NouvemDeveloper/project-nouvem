﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceItemsView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.Sales.ARInvoice
{
    using System.IO;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for InvoiceItemsView.xaml
    /// </summary>
    public partial class InvoiceItemsView : UserControl
    {
        public InvoiceItemsView()
        {
            this.InitializeComponent();
            var gridPath = Settings.Default.ReceiptItemsPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Unloaded += (sender, args) =>
            {
                this.GridControlSaleDetails.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSaleDetails.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }
    }
}

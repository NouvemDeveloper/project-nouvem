﻿// -----------------------------------------------------------------------
// <copyright file="Order2View .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Sales.AROrder
{
    using System.Globalization;
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for OrderView.xaml
    /// </summary>
    public partial class Order2View : UserControl
    {
        public Order2View()
        {
            this.InitializeComponent();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-IE");
            ThreadPool.QueueUserWorkItem(x => this.ComboBoxEditCode.Dispatcher.Invoke(() => this.ComboBoxEditCode.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.ComboBoxEditCode.Focus());
        }
    }
}

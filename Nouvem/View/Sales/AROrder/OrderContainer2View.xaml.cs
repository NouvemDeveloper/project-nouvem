﻿// -----------------------------------------------------------------------
// <copyright file="OrderContainer2View.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Sales.AROrder
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for OrderContainer2View.xaml
    /// </summary>
    public partial class OrderContainer2View : Window
    {
        private bool canClose;
        public OrderContainer2View()
        {
            this.InitializeComponent();

            Messenger.Default.Register<bool>(this, Token.CanCloseOrder2Window, b => this.canClose = b);
            this.WindowState = WindowSettings.Order2Maximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseOrder2Window, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.Closing += (sender, args) =>
            {
                if (!this.canClose)
                {
                    args.Cancel = true;
                    Messenger.Default.Send(Token.Message, Token.ClosingSaleOrder2Window);
                    return;
                }

                WindowSettings.Order2Height = this.Height;
                WindowSettings.Order2Left = this.Left;
                WindowSettings.Order2Top = this.Top;
                WindowSettings.Order2Width = this.Width;
                WindowSettings.Order2Maximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


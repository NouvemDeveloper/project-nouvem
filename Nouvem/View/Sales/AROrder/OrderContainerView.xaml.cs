﻿// -----------------------------------------------------------------------
// <copyright file="OrderContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Sales.AROrder
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for OrderContainerView.xaml
    /// </summary>
    public partial class OrderContainerView : Window
    {
        private bool canClose;
        public OrderContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<bool>(this, Token.CanCloseOrderWindow, b => this.canClose = b);
            Messenger.Default.Register<string>(this, Token.CloseOrderWindow, x =>
            {
                this.Close();
            }); 

            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.WindowState = WindowSettings.OrderMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };
            
            this.Closing += (sender, args) =>
            {
                //NouvemMessageBox.Show(Message.CloseWindowPrompt, NouvemMessageBoxButtons.YesNo);
                //if (NouvemMessageBox.UserSelection == UserDialogue.No)
                //{
                //    args.Cancel = true;
                //    return;
                //}

                if (!this.canClose)
                {
                    args.Cancel = true;
                    Messenger.Default.Send(Token.Message, Token.ClosingSaleOrderWindow);
                    return;
                }
                else
                {
                    args.Cancel = false;
                }

                WindowSettings.OrderHeight = this.Height;
                WindowSettings.OrderLeft = this.Left;
                WindowSettings.OrderTop = this.Top;
                WindowSettings.OrderWidth = this.Width;
                WindowSettings.OrderMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="SaleContentsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Logging;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Model.Event;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.View.Utility;

namespace Nouvem.View.Sales
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for SaleContentsView.xaml
    /// </summary>
    public partial class SaleContentsView : UserControl
    {
        private int rowHandle;
        private int gridRowHandle;
        private Logging.Logger log = new Logger();
        private NotesView notesView;

        public SaleContentsView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.FocusToGrid, s => this.SetFocus());

            var gridPath = Settings.Default.SaleDetailsPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Unloaded += (sender, args) =>
            {
                this.GridControlSaleDetails.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
                if (this.notesView != null)
                {
                    this.notesView.DataReceived -= NotesViewOnDataReceived;
                    this.notesView = null;
                }
            };

            //Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            //{
            //    this.log.LogInfo(this.GetType(), $"Row Handle:{this.rowHandle}, GridRowHandle:{this.gridRowHandle}, Note:{s}");
            //    if (this.gridRowHandle < 0)
            //    {
            //        this.gridRowHandle = this.GridControlSaleDetails.VisibleRowCount - 2;
            //    }

            //    this.GridControlSaleDetails.SetCellValue(this.gridRowHandle, "Notes", s);
            //});

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlSaleDetails.RestoreLayoutFromXml(gridPath);
                    }
                    catch
                    {
                    }
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });

                Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.GridControlSaleDetails.Focus());
            };
        }

        private void TableViewData_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var hitInfo = this.TableViewData.CalcHitInfo(e.OriginalSource as DependencyObject);

                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.FieldName;
                Messenger.Default.Send(currentColumnHeader, Token.GridColumn);
            }
            catch
            {
            }
        }

        private void TableViewData_ShownEditor(object sender, DevExpress.Xpf.Grid.EditorEventArgs e)
        {
            try
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    try
                    {
                        if (sender is TableView && ((TableView)sender).ActiveEditor != null)
                        {
                            ((TableView)sender).ActiveEditor.SelectAll();
                        }
                    }
                    catch (Exception)
                    {
                    }
                }));
            }
            catch (Exception ex)
            {
                var log = new Logging.Logger();
                log.LogError(this.GetType(), ex.Message);
            }
        }

        private void SetFocus()
        {
            //if (this.GridControlSaleDetails.VisibleRowCount == 0)
            //{
            //    return;
            //}
            try
            {
                //if (this.GridControlSaleDetails.IsValidRowHandle(rowHandle))
                //{
                this.GridControlSaleDetails.CurrentColumn = this.GridControlSaleDetails.Columns["INMasterID"];
                this.TableViewData.FocusedRowHandle = 1;
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.GridControlSaleDetails.Focus();
                    this.TableViewData.ShowEditor();
                }), System.Windows.Threading.DispatcherPriority.Render);
            }
            catch (Exception)
            {
            }
           
            //}
        }

        private void GridControlSaleDetails_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue)
                {
                    this.GridControlSaleDetails.CurrentColumn = this.GridControlSaleDetails.Columns[0];
                    this.TableViewData.ShowEditor();
                }
            }
            catch (Exception)
            {
            }
        }

        private void GridColumn_Validate(object sender, GridCellValidationEventArgs e)
        {
            if (ApplicationSettings.AllowDuplicateProductOnOrder)
            {
                return;
            }

            try
            {
                var inMaster = (int)e.Value;
                var count = 0;

                foreach (var item in (this.GridControlSaleDetails.ItemsSource as IEnumerable<object>))
                {
                    if (count > 1)
                    {
                        break;
                    }

                    var sale = item as SaleDetail;
                    if (sale.INMasterID == inMaster)
                    {
                        count++;
                    }
                }

                if (count > 1)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.ProductExistsOnOrder;
                    SystemMessage.Write(MessageType.Issue, Message.ProductExistsOnOrder);
                }
            }
            catch (Exception)
            {
            }
        }

        private void CreateNotes(string note)
        {
            this.notesView = new NotesView();
            this.notesView.DataReceived += NotesViewOnDataReceived;
            this.notesView.SetNote(note);
            this.notesView.Show();
        }

        private void NotesViewOnDataReceived(object sender, StringArgs e)
        {
            this.log.LogInfo(this.GetType(), $"Row Handle:{this.rowHandle}, GridRowHandle:{this.gridRowHandle}, Note:{e.Data}");
            if (this.gridRowHandle < 0)
            {
                this.gridRowHandle = this.GridControlSaleDetails.VisibleRowCount - 2;
            }

            this.GridControlSaleDetails.SetCellValue(this.gridRowHandle, "Notes", e.Data);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            //this.rowHandle = this.TableViewData.FocusedRowHandle;
            //if (this.rowHandle < 0)
            //{
            //    return;
            //}

            if (this.GridControlSaleDetails.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlSaleDetails.CurrentColumn == this.GridControlSaleDetails.Columns["Notes"])
                {
                    this.gridRowHandle = this.rowHandle;
                    this.log.LogInfo(this.GetType(), $"Calling Notes - grid row handle:{this.gridRowHandle}");
                    var value = this.GridControlSaleDetails.GetCellValue(this.gridRowHandle, "Notes");
                    var localValue = value == null ? string.Empty : value.ToString();
                    this.CreateNotes(localValue);
                    //Messenger.Default.Send(localValue, Token.DisplayNotes);
                }
            }
        }

        private void TableViewData_ValidateRow(object sender, GridRowValidationEventArgs e)
        {
            try
            {
                var price = (e.Row as SaleDetail).PriceListID;
                var inMasterid = (e.Row as SaleDetail).INMasterID;
                //var count = 0;

                //foreach (var item in (this.GridControlSaleDetails.ItemsSource as IEnumerable<object>))
                //{
                //    if (count > 1)
                //    {
                //        break;
                //    }

                //    var sale = item as SaleDetail;
                //    if (sale.INMasterID == inMasterid)
                //    {
                //        count++;
                //    }
                //}

                //if (count > 1)
                //{
                //    e.IsValid = false;
                //    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                //    e.ErrorContent = Message.ProductExistsOnOrder;
                //    SystemMessage.Write(MessageType.Issue, Message.ProductExistsOnOrder);
                //    return;
                //}

                if (inMasterid == 0)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.NoProductSelectedOnOrderLine;
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelectedOnOrderLine);
                    return;
                }

                if (price == 0)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.NoPriceBookFound;
                    SystemMessage.Write(MessageType.Issue, Message.NoPriceBookFound);
                    return;
                }

                if (ApplicationSettings.QtyMustBeEnteredOnSaleOrderLine)
                {
                    var qty = (e.Row as SaleDetail).QuantityOrdered;
                    var wgt = (e.Row as SaleDetail).WeightOrdered;
                    if ((qty == null || qty == 0) && (wgt == null || wgt == 0))
                    {
                        e.IsValid = false;
                        e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                        e.ErrorContent = Message.NoQtyEntered;
                        SystemMessage.Write(MessageType.Issue, "No qty or wgt has been entered on this order line");
                        return;
                    }
                }

                if (ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarning)
                {
                    var unitPrice = (e.Row as SaleDetail).UnitPrice;
                    if (unitPrice == null || unitPrice == 0)
                    {
                        //e.IsValid = false;
                        e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning;
                        e.ErrorContent = Message.NoPriceEntered;
                        NouvemMessageBox.Show(Message.NoPriceEntered);
                        return;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        void OnInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var view = (TableView)sender;
            Dispatcher.BeginInvoke(new Action(
                () => {
                    view.FocusedRowHandle = GridControl.InvalidRowHandle;
                    var newItemRowVisibleIndex = view.Grid.GetRowVisibleIndexByHandle(GridControl.NewItemRowHandle);
                    var editedRowVisibleIndex = newItemRowVisibleIndex;
                    switch (view.NewItemRowPosition)
                    {
                        case NewItemRowPosition.Bottom: editedRowVisibleIndex -= 1; break;
                        case NewItemRowPosition.Top: editedRowVisibleIndex += 1; break;
                    }
                    var editedRowHandle = view.Grid.GetRowHandleByVisibleIndex(editedRowVisibleIndex);
                    view.FocusedRowHandle = editedRowHandle;
                    view.ShowEditor();
                }
                ));
        }

        //private void GridControlSaleDetails_KeyDown(object sender, KeyEventArgs e)
        //{
        //    this.TableViewData.AddNewRow();
        //    e.Handled = true;
        //}
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SalesRecentOrdersView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;

namespace Nouvem.View.Sales
{
    using System;
    using Nouvem.Global;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for SalesRecentOrdersView.xaml
    /// </summary>
    public partial class SalesRecentOrdersView : UserControl
    {
        public SalesRecentOrdersView()
        {
            this.InitializeComponent();

            this.GridControlRecentOrders.PreviewMouseDown += this.Grid_PreviewMouseDown;
            this.TableViewData.MouseLeftButtonUp += this.TableView_MouseLeftButtonUp;

            this.GridControlRecentOrderItems.PreviewMouseDown += this.Grid1_PreviewMouseDown;
            this.TableViewDataOrders.MouseLeftButtonUp += this.TableView1_MouseLeftButtonUp;

            Messenger.Default.Register<string>(this, Token.SetRecentOrdersFocus, s => this.SetFocus());
            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.RecentOrdersGridPath))
                {
                    this.GridControlRecentOrders.RestoreLayoutFromXml(Settings.Default.RecentOrdersGridPath);
                }

                this.SetFocus();
                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) =>
            {
                this.GridControlRecentOrders.SaveLayoutToXml(Settings.Default.RecentOrdersGridPath);
                Messenger.Default.Unregister(this);
            };
        }

        private void SetFocus()
        {
            if (this.GridControlRecentOrders.VisibleRowCount == 0)
            {
                return;
            }

            var rowHandle = 0;

            if (this.GridControlRecentOrders.IsValidRowHandle(rowHandle))
            {
                this.GridControlRecentOrders.CurrentColumn = this.GridControlRecentOrders.Columns["Ordered"];
                this.TableViewData.FocusedRowHandle = rowHandle;
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.GridControlRecentOrders.Focus();
                    this.TableViewData.ShowEditor();
                }), System.Windows.Threading.DispatcherPriority.Render);
            }
        }

        private void Grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var hitInfo = ((TableView)this.GridControlRecentOrders.View).CalcHitInfo(e.OriginalSource as DependencyObject);
                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.ActualColumnChooserHeaderCaption.ToString().Trim();
                if (currentColumnHeader.Equals("Product") || currentColumnHeader.Equals("Ordered") || currentColumnHeader.Equals(string.Empty))
                {
                    return;
                }

                if (hitInfo.HitTest == TableViewHitTest.ColumnHeader || hitInfo.HitTest == TableViewHitTest.ColumnHeaderFilterButton)
                {
                    this.GridControlRecentOrders.UnselectAll();

                    if (this.GridControlRecentOrders.View is TableView)
                    {
                        (this.GridControlRecentOrders.View as TableView)
                       .SelectCells(0, currentColumn, this.GridControlRecentOrders.VisibleRowCount - 1, currentColumn);

                        // Let the vm know that a previous order has been selected.
                        Messenger.Default.Send(currentColumnHeader, Token.RecentOrderItemSelected);
                    }
                }
            }
            catch
            {
            }
        }


        private void TableView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var tableView = sender as TableView;
            var column = this.GridControlRecentOrders.CurrentColumn.ActualColumnChooserHeaderCaption;

            if (column.Equals("Product") || column.Equals("Ordered") || column.Equals(string.Empty))
            {
                return;
            }

            if (tableView != null)
            {
                try
                {
                    var hitInfo = tableView.CalcHitInfo(e.OriginalSource as DependencyObject);
                    if (hitInfo.InRowCell)
                    {
                        var selectedCellValue = tableView.Grid.GetCellValue(hitInfo.RowHandle, hitInfo.Column).ToString();

                        // Let the vm know that a previous order item has been selected.
                        Messenger.Default.Send(selectedCellValue, Token.RecentOrderItemSelected);
                    }
                }
                catch 
                {
                }
            }
        }

        private void Grid1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var hitInfo = ((TableView)this.GridControlRecentOrderItems.View).CalcHitInfo(e.OriginalSource as DependencyObject);
                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.ActualColumnChooserHeaderCaption.ToString().Trim();
                if (currentColumnHeader.Equals("Product") || currentColumnHeader.Equals("Ordered") || currentColumnHeader.Equals(string.Empty))
                {
                    return;
                }

                if (hitInfo.HitTest == TableViewHitTest.ColumnHeader || hitInfo.HitTest == TableViewHitTest.ColumnHeaderFilterButton)
                {
                    this.GridControlRecentOrderItems.UnselectAll();

                    if (this.GridControlRecentOrderItems.View is TableView)
                    {
                        (this.GridControlRecentOrderItems.View as TableView)
                       .SelectCells(0, currentColumn, this.GridControlRecentOrderItems.VisibleRowCount - 1, currentColumn);

                        // Let the vm know that a previous order has been selected.
                        Messenger.Default.Send(currentColumnHeader, Token.RecentOrderItemSelected);
                    }
                }
            }
            catch
            {
            }
        }


        private void TableView1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var tableView = sender as TableView;
            var column = this.GridControlRecentOrderItems.CurrentColumn.ActualColumnChooserHeaderCaption;

            if (column.Equals("Product") || column.Equals("Ordered") || column.Equals(string.Empty))
            {
                return;
            }

            if (tableView != null)
            {
                try
                {
                    var hitInfo = tableView.CalcHitInfo(e.OriginalSource as DependencyObject);
                    if (hitInfo.InRowCell)
                    {
                        var selectedCellValue = tableView.Grid.GetCellValue(hitInfo.RowHandle, hitInfo.Column).ToString();

                        // Let the vm know that a previous order item has been selected.
                        Messenger.Default.Send(selectedCellValue, Token.RecentOrderItemSelected);
                    }
                }
                catch
                {
                }
            }
        }
    }
}

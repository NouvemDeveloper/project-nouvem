﻿// -----------------------------------------------------------------------
// <copyright file="AttributesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;
using System.Windows;
using Nouvem.Model.Enum;

namespace Nouvem.View.Sales
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for AttributesView.xaml
    /// </summary>
    public partial class AttributesView : UserControl
    {
        public AttributesView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s =>
            {
                ThreadPool.QueueUserWorkItem(x => this.TextBoxEartag.Dispatcher.Invoke(() => this.TextBoxEartag.Focus()));
            });

            Messenger.Default.Register<string>(this, Token.FocusToHerdNo, s =>
            {
                ThreadPool.QueueUserWorkItem(x => this.TextBoxEartag.Dispatcher.Invoke(() => this.TextBoxEartag.Focus()));
            });

            Messenger.Default.Register<string>(this, Token.FocusToButton, s =>
            {
                //this.TextBoxBornIn.Focus();
            });

            if (ApplicationSettings.UsingUKDepartmentServiceQueries)
            {
                this.StackPanelThirdParty.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.StackPanelHoldingNo.Visibility = Visibility.Collapsed;
            }

            if (ApplicationSettings.LairageType == LairageType.NI)
            {
                this.StackPanelNumber.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.StackPanelAfais.Visibility = Visibility.Collapsed;
            }

            if (ApplicationSettings.ShowFreezerTypeAtLairage)
            {
                this.StackPanelRisk.Visibility = Visibility.Collapsed;
                this.StackPanelNoEartag.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.StackPanelFreezertype.Visibility = Visibility.Collapsed;
                this.StackPanelNo.Visibility = Visibility.Collapsed;
            }
        }
    }
}

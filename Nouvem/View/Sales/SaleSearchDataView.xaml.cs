﻿// -----------------------------------------------------------------------
// <copyright file="SaleSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;
using Nouvem.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.View.Sales
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for SaleSearchDataView.xaml
    /// </summary>
    public partial class SaleSearchDataView : Window
    {
        public SaleSearchDataView(ViewType module = ViewType.Audit)
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<bool>(this, Token.SetTopMost, b => this.Topmost = b);
            this.GridControlSearchData.View.ShowSearchPanel(true);

            this.WindowState = WindowSettings.SaleSearchDataMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            string gridPath;
            if (module == ViewType.ARDispatch)
            {
                gridPath = Settings.Default.SalesSearchGridDispatchPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.Order)
            {
                gridPath = Settings.Default.SalesSearchGridSaleOrderPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.Invoice)
            {
                gridPath = Settings.Default.SalesSearchGridInvoicePath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.APReceipt)
            { 
                gridPath = Settings.Default.SalesSearchGridAPReceiptPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.APOrder)
            {
                gridPath = Settings.Default.SalesSearchGridAPOrderPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.LairageOrder)
            {
                gridPath = Settings.Default.SalesSearchGridLairageOrderPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.LairageIntake)
            {
                gridPath = Settings.Default.SalesSearchGridLairagePath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.APQuote)
            {
                gridPath = Settings.Default.SalesSearchGridAPQuotePath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else if (module == ViewType.Quote)
            {
                gridPath = Settings.Default.SalesSearchGridQuotePath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }
            else
            {
                gridPath = Settings.Default.SalesSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            }

            this.Closing += (sender, args) =>
            {
                WindowSettings.SaleSearchDataHeight = this.Height;
                WindowSettings.SaleSearchDataLeft = this.Left;
                WindowSettings.SaleSearchDataTop = this.Top;
                WindowSettings.SaleSearchDataWidth = this.Width;
                WindowSettings.SaleSearchDataMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void TableView_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!ApplicationSettings.TouchScreenModeOnly)
            {
                return;
            }

            Messenger.Default.Send(Token.Message, Token.DisplaySearchKeyboard);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Sales.ARReturn
{
    /// <summary>
    /// Interaction logic for CreditNoteCreationView.xaml
    /// </summary>
    public partial class CreditNoteCreationView : Window
    {
        public CreditNoteCreationView()
        {
            InitializeComponent();

            this.WindowState = WindowSettings.CreditNoteCreationMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseCreditNoteCreationWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            var gridPath = Settings.Default.CreditNoteCreationGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Closing += (sender, args) =>
            {
                WindowSettings.CreditNoteCreationHeight = this.Height;
                WindowSettings.CreditNoteCreationLeft = this.Left;
                WindowSettings.CreditNoteCreationTop = this.Top;
                WindowSettings.CreditNoteCreationWidth = this.Width;
                WindowSettings.CreditNoteCreationMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlSearchData.SelectAll();
        }
    }
}

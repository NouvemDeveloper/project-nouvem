﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Editors;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.ViewModel;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Sales
{
    /// <summary>
    /// Interaction logic for TouchscreenAttributesView.xaml
    /// </summary>
    public partial class TouchscreenAttributesView : UserControl
    {
        private ComboBox selectedComboBox = new ComboBox();

        public TouchscreenAttributesView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<bool>(this, Token.EnableEartagEntryOnGrader, b =>
                {
                    this.TextBoxEartag.IsHitTestVisible = b;
                });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals(Constant.TouchscreenTraceability) && this.selectedComboBox != null)
                {
                    this.selectedComboBox.SelectedValue = c.Data;
                }
            });

            Messenger.Default.Register<ViewType>(this, Token.SetDataContext, v =>
            {
                if (v == ViewType.LairageIntake)
                {
                    this.DataContext = ViewModelLocator.SequencerStatic;
                    this.TextBoxEartag.Visibility = Visibility.Visible;
                    this.TextBoxHerd.Visibility = Visibility.Collapsed;
                    this.datePickerDOB.Visibility = Visibility.Visible;
                    this.ComboBoxBreeds.Visibility = Visibility.Visible;
                    this.ComboBoxCasualty.Visibility = Visibility.Collapsed;
                    this.ComboBoxCleanliness.Visibility = Visibility.Collapsed;
                    this.ComboBoxCustomers.Visibility = Visibility.Collapsed;
                    this.ComboBoxFA.Visibility = Visibility.Visible;
                    this.ComboBoxImported.Visibility = Visibility.Collapsed;
                    this.ComboBoxYesNo.Visibility = Visibility.Collapsed;
                    this.ComboBoxLame.Visibility = Visibility.Collapsed;
                    this.TextBoxAgeInDays.Visibility = Visibility.Collapsed;
                    this.TextBoxAgeInMonths.Visibility = Visibility.Visible;
                    this.ComboBoxSexes.Visibility = Visibility.Visible;
                    this.TabItemDates.Visibility = Visibility.Collapsed;
                    this.TabItemQty.Visibility = Visibility.Collapsed;
                    this.ComboBoxDestinations.Visibility = Visibility.Collapsed;
                    this.TextBoxIdentigen.Visibility = Visibility.Collapsed;
                    this.ComboBoxRearedIn.Visibility = Visibility.Collapsed;
                    this.ComboBoxHalal.Visibility = Visibility.Collapsed;
                    this.ComboBoxDressSpec.Visibility = Visibility.Collapsed;
                    this.ComboBoxPrivate.Visibility = Visibility.Collapsed;
                    this.ComboBoxKillType.Visibility = Visibility.Collapsed;
                    this.ComboBoxOrigin.Visibility = Visibility.Visible;
                    this.ComboBoxOrigin.IsHitTestVisible = true;
                    this.ComboBoxInternalGrades.Visibility = Visibility.Collapsed;
                    return;
                }

                if (v == ViewType.Sequencer)
                {
                    this.DataContext = ViewModelLocator.SequencerStatic;
                    if (ApplicationSettings.HideAllKillTouchscreenAttributes)
                    {
                        this.ComboBoxCategory.Visibility = Visibility.Collapsed;
                        return;
                    }

                    this.TextBoxEartag.Visibility = Visibility.Collapsed;
                    this.TextBoxHerd.Visibility = Visibility.Collapsed;
                    this.datePickerDOB.Visibility = Visibility.Collapsed;
                    this.ComboBoxBreeds.Visibility = Visibility.Collapsed;
                    this.ComboBoxCasualty.Visibility = Visibility.Collapsed;
                    this.ComboBoxCleanliness.Visibility = Visibility.Collapsed;
                    this.ComboBoxCustomers.Visibility = Visibility.Collapsed;
                    this.ComboBoxFA.Visibility = Visibility.Collapsed;
                    this.ComboBoxImported.Visibility = Visibility.Collapsed;
                    this.ComboBoxYesNo.Visibility = Visibility.Collapsed;
                    this.ComboBoxLame.Visibility = Visibility.Collapsed;
                    this.TextBoxAgeInDays.Visibility = Visibility.Collapsed;
                    this.TextBoxAgeInMonths.Visibility = Visibility.Collapsed;
                    this.ComboBoxSexes.Visibility = Visibility.Collapsed;
                    this.TabItemDates.Visibility = Visibility.Collapsed;
                    this.TabItemQty.Visibility = Visibility.Collapsed;
                    this.ComboBoxDestinations.Visibility = Visibility.Collapsed;
                    this.TextBoxIdentigen.Visibility = Visibility.Collapsed;
                    this.ComboBoxRearedIn.Visibility = Visibility.Collapsed;
                    this.ComboBoxHalal.Visibility = Visibility.Visible;
                    this.ComboBoxDressSpec.Visibility = Visibility.Collapsed;
                    this.ComboBoxPrivate.Visibility = Visibility.Collapsed;
                    this.ComboBoxKillType.Visibility = Visibility.Collapsed;
                    this.ComboBoxInternalGrades.Visibility = Visibility.Collapsed;
                    return;
                }

                if (v == ViewType.Grader)
                {
                    this.DataContext = ViewModelLocator.GraderStatic;
                    if (ApplicationSettings.HideAllKillTouchscreenAttributes)
                    {
                        this.ComboBoxCategory.Visibility = Visibility.Collapsed;
                        this.ComboBoxInternalGrades.Visibility = Visibility.Visible;
                        return;
                    }

                    this.TextBoxEartag.Visibility = Visibility.Visible;
                    this.TextBoxEartag.IsHitTestVisible = false;
                    //this.TextBoxHerd.Visibility = Visibility.Collapsed;
                    this.datePickerDOB.Visibility = Visibility.Visible;
                    this.datePickerDOB.IsHitTestVisible = true;
                    this.ComboBoxBreeds.Visibility = Visibility.Visible;
                    this.ComboBoxBreeds.IsHitTestVisible = false;
                    this.ComboBoxCasualty.Visibility = Visibility.Collapsed;
                    this.ComboBoxInternalGrades.Visibility = Visibility.Collapsed;
                    this.ComboBoxCleanliness.Visibility = Visibility.Collapsed;
                    this.ComboBoxCustomers.Visibility = Visibility.Collapsed;
                    this.ComboBoxCustomers.IsHitTestVisible = false;
                    this.ComboBoxFA.Visibility = Visibility.Visible;
                    this.ComboBoxFA.IsHitTestVisible = true;
                    this.ComboBoxImported.Visibility = Visibility.Collapsed;
                    this.ComboBoxYesNo.Visibility = Visibility.Collapsed;
                    this.ComboBoxLame.Visibility = Visibility.Collapsed;
                    this.ComboBoxDestinations.Visibility = ApplicationSettings.ShowDestinationsAtGrader ? Visibility.Visible : Visibility.Collapsed;
                    this.TextBoxAgeInDays.Visibility = Visibility.Collapsed;
                    this.TextBoxAgeInMonths.Visibility = Visibility.Visible;
                    this.TextBoxAgeInMonths.IsHitTestVisible = false;
                    this.ComboBoxSexes.Visibility = Visibility.Visible;
                    this.ComboBoxRearedIn.Visibility = Visibility.Visible;
                    this.TabItemDates.Visibility = Visibility.Collapsed;
                    this.TabItemQty.Visibility = Visibility.Collapsed;
                    this.ComboBoxOrigin.Visibility = Visibility.Visible;
                    this.ComboBoxOrigin.IsHitTestVisible = true;
                    this.TextBoxIdentigen.Visibility = ApplicationSettings.ShowIdentigenAtGrader ? Visibility.Visible : Visibility.Collapsed;
                    this.TextBoxIdentigen.IsHitTestVisible = false;
                    this.ComboBoxHalal.Visibility = Visibility.Collapsed;
                    this.ComboBoxDressSpec.Visibility = ApplicationSettings.ShowDressSpecAtGrader ? Visibility.Visible: Visibility.Collapsed;
                    this.ComboBoxPrivate.Visibility = ApplicationSettings.ShowPrivateAtGrader ? Visibility.Visible : Visibility.Collapsed;
                    this.ComboBoxKillType.Visibility = ApplicationSettings.ShowKillTypeAtGrader ? Visibility.Visible : Visibility.Collapsed;

                    if (ApplicationSettings.DisableCategoryChangeAtGrader)
                    {
                        this.ComboBoxSexes.IsHitTestVisible = false;
                        this.ComboCategory.IsHitTestVisible = false;
                    }

                    return;
                }
            });

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);

            this.TextBoxEartag.Visibility = Visibility.Collapsed;
            this.TextBoxHerd.Visibility = Visibility.Collapsed;
            this.datePickerDOB.Visibility = Visibility.Collapsed;
            this.ComboBoxBreeds.Visibility = Visibility.Collapsed;
            this.ComboBoxCasualty.Visibility = Visibility.Collapsed;
            this.ComboBoxCleanliness.Visibility = Visibility.Collapsed;
            this.ComboBoxHalal.Visibility = Visibility.Collapsed;
            this.ComboBoxCustomers.Visibility = Visibility.Collapsed;
            this.ComboBoxFA.Visibility = Visibility.Collapsed;
            this.ComboBoxImported.Visibility = Visibility.Collapsed;
            this.ComboBoxYesNo.Visibility = Visibility.Collapsed;
            this.ComboBoxLame.Visibility = Visibility.Collapsed;
            this.TextBoxAgeInDays.Visibility = Visibility.Collapsed;
            this.TextBoxAgeInMonths.Visibility = Visibility.Collapsed;
            this.ComboBoxSexes.Visibility = Visibility.Collapsed;
            this.ComboBoxRearedIn.Visibility = Visibility.Collapsed;
            this.ComboBoxDestinations.Visibility = Visibility.Collapsed;
            this.ComboBoxOrigin.Visibility = Visibility.Collapsed;
            this.ComboBoxCustomers.Visibility = Visibility.Collapsed;
            this.TabItemDates.Visibility = Visibility.Hidden;
            this.TabItemQty.Visibility = Visibility.Hidden;
            this.TextBoxIdentigen.Visibility = Visibility.Collapsed;
            this.ComboBoxDressSpec.Visibility = Visibility.Collapsed;
            this.ComboBoxPrivate.Visibility = Visibility.Collapsed;
            this.ComboBoxKillType.Visibility = Visibility.Collapsed;
            this.ComboBoxInternalGrades.Visibility = Visibility.Collapsed;
        }

        private void Control_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is DatePicker)
            {
                (sender as DatePicker).IsDropDownOpen = true;
            }

            if (sender is TextBox)
            {
                Messenger.Default.Send(ViewType.Keyboard);
            }

            if (sender is ComboBox)
            {
                //var items = new List<object>();
                var items = new List<CollectionData>();
                var stringItems = new List<object>();
                var localCombo = (sender as ComboBox);
                localCombo.IsDropDownOpen = false;
                this.selectedComboBox = localCombo;

                if (localCombo.Name.CompareIgnoringCase("ComboCategory"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = string.Format("{0} - {1}", (item as NouCategory).CAT,(item as NouCategory).Name), ID = (item as NouCategory).CategoryID, Identifier = Constant.TraceabilityAttribute });
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboInternalGrades"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = (item as InternalGrade).Description, ID = (item as InternalGrade).InternalGradeID, Identifier = Constant.InternalGrade });
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboDestinations"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        stringItems.Add(item);
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(stringItems, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboSexes"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        stringItems.Add(item);
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(stringItems, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboBreeds"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = (item as NouBreed).Name, ID = (item as NouBreed).NouBreedID, Identifier = Constant.Breed });
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboDressSpec"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add((CollectionData)item);
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboKillType"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add((CollectionData)item);
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboFA"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = item.ToString(), Identifier = Constant.FA });
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboPrivate"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = item.ToString(), Identifier = Constant.Private });
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboHalal"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        stringItems.Add(item);
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(stringItems, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboRearedIn"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = (item as Model.DataLayer.Country).Name, ID = (item as Model.DataLayer.Country).CountryID, Identifier = Constant.RearedIn});
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                if (localCombo.Name.CompareIgnoringCase("ComboOrigin"))
                {
                    foreach (var item in localCombo.Items)
                    {
                        items.Add(new CollectionData { Data = (item as Model.DataLayer.Country).Name, ID = (item as Model.DataLayer.Country).CountryID, Identifier = Constant.Origin });
                    }

                    // Send our combo collection to the display module.
                    Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DispatchContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for DispatchContainerView.xaml
    /// </summary>
    public partial class DispatchContainerView : Window
    {
        public DispatchContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseDispatchContainerWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.DispatchContainerMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.DispatchContainerHeight = this.Height;
                WindowSettings.DispatchContainerLeft = this.Left;
                WindowSettings.DispatchContainerTop = this.Top;
                WindowSettings.DispatchContainerWidth = this.Width;
                WindowSettings.DispatchContainerMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

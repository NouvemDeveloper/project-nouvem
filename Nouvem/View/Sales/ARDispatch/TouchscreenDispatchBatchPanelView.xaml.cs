﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nouvem.Global;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for TouchscreenDispatchBatchPanelView.xaml
    /// </summary>
    public partial class TouchscreenDispatchBatchPanelView : UserControl
    {
        public TouchscreenDispatchBatchPanelView()
        {
            this.InitializeComponent();
            if (ApplicationSettings.ShowPriceAtDispatch)
            {
                this.LabelDocket.Visibility = Visibility.Collapsed;
                this.TextBoxDocket.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.TextBoxPrice.Visibility = Visibility.Collapsed;
            }

            if (ApplicationSettings.ShowBoxingAtDispatch)
            {
                this.LabelShipping.Visibility = Visibility.Collapsed;
                this.LabelShippingMain.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.LabelBox.Visibility = Visibility.Collapsed;
                this.LabelBoxMain.Visibility = Visibility.Collapsed;
            }
        }
    }
}

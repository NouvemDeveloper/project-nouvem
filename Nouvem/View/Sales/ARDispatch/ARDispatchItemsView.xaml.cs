﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchItemsView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows.Input;
using DevExpress.Utils;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Sales.ARDispatch
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ARDispatchItemsView .xaml
    /// </summary>
    public partial class ARDispatchItemsView : UserControl
    {
        private int rowHandle;

        public ARDispatchItemsView()
        {
            this.InitializeComponent();
            if (ApplicationSettings.PriceInvoiceDocket)
            {
                this.GridColumnUnitPrice.ReadOnly = true;
            }

            //this.GridColumnDisPrice.AllowSorting = DefaultBoolean.False;
            this.GridColumnQty.AllowSorting = DefaultBoolean.False;
            this.GridColumnWgt.AllowSorting = DefaultBoolean.False;
            this.GridColumnQtyDel.AllowSorting = DefaultBoolean.False;
            this.GridColumnWgtDel.AllowSorting = DefaultBoolean.False;
            this.GridColumnItemDesc.AllowSorting = DefaultBoolean.False;
            this.GridColumnNameItemNo.AllowSorting = DefaultBoolean.False;
            //his.GridColumnPercentage.AllowSorting = DefaultBoolean.False;
            this.GridColumnNameDisabled.AllowSorting = DefaultBoolean.False;
            this.GridColumnPriceList.AllowSorting = DefaultBoolean.False;
            this.TableViewData.AllowSorting = false;
            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                this.GridControlSaleDetails.SetCellValue(this.rowHandle, "Notes", s);
            });

            var gridPath = Settings.Default.DispatchItemsGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Unloaded += (sender, args) =>
            {
                this.GridControlSaleDetails.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlSaleDetails.RestoreLayoutFromXml(gridPath);
                    }
                    catch 
                    {
                    }
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });

                Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.GridControlSaleDetails.Focus());
            };
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            if (this.rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlSaleDetails.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlSaleDetails.CurrentColumn == this.GridControlSaleDetails.Columns["Notes"])
                {
                    var value = this.GridControlSaleDetails.GetCellValue(this.rowHandle, "Notes");
                    var localValue = value == null ? string.Empty : value.ToString();
                    Messenger.Default.Send(localValue, Token.DisplayNotes);
                }
            }
        }

        void OnInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var view = (TableView)sender;
            Dispatcher.BeginInvoke(new Action(
                () => {
                    view.FocusedRowHandle = GridControl.InvalidRowHandle;
                    var newItemRowVisibleIndex = view.Grid.GetRowVisibleIndexByHandle(GridControl.NewItemRowHandle);
                    var editedRowVisibleIndex = newItemRowVisibleIndex;
                    switch (view.NewItemRowPosition)
                    {
                        case NewItemRowPosition.Bottom: editedRowVisibleIndex -= 1; break;
                        case NewItemRowPosition.Top: editedRowVisibleIndex += 1; break;
                    }
                    var editedRowHandle = view.Grid.GetRowHandleByVisibleIndex(editedRowVisibleIndex);
                    view.FocusedRowHandle = editedRowHandle;
                    view.ShowEditor();
                }
                ));
        }

        private void TableViewData_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var hitInfo = this.TableViewData.CalcHitInfo(e.OriginalSource as DependencyObject);

                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.FieldName;
                Messenger.Default.Send(currentColumnHeader, Token.GridColumn);
            }
            catch
            {
            }
        }

        private void GridColumn_Validate(object sender, GridCellValidationEventArgs e)
        {
            if (ApplicationSettings.AllowDuplicateProductOnOrder)
            {
                return;
            }

            try
            {
                var inMaster = (int)e.Value;
                var count = 0;

                foreach (var item in (this.GridControlSaleDetails.ItemsSource as IEnumerable<object>))
                {
                    if (count > 1)
                    {
                        break;
                    }

                    var sale = item as SaleDetail;
                    if (sale.INMasterID == inMaster)
                    {
                        count++;
                    }
                }

                if (count > 1)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.ProductExistsOnOrder;
                    SystemMessage.Write(MessageType.Issue, Message.ProductExistsOnOrder);
                }
            }
            catch (Exception)
            {
            }
        }


        private void TableViewData_ValidateRow(object sender, GridRowValidationEventArgs e)
        {
            try
            {
                var price = (e.Row as SaleDetail).PriceListID;
                var inMasterid = (e.Row as SaleDetail).INMasterID;

                //var count = 0;

                //foreach (var item in (this.GridControlSaleDetails.ItemsSource as IEnumerable<object>))
                //{
                //    if (count > 1)
                //    {
                //        break;
                //    }

                //    var sale = item as SaleDetail;
                //    if (sale.INMasterID == inMasterid)
                //    {
                //        count++;
                //    }
                //}

                //if (count > 1)
                //{
                //    e.IsValid = false;
                //    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                //    e.ErrorContent = Message.ProductExistsOnOrder;
                //    SystemMessage.Write(MessageType.Issue, Message.ProductExistsOnOrder);
                //    return;
                //}

                if (inMasterid == 0)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.NoProductSelectedOnOrderLine;
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelectedOnOrderLine);
                    return;
                }

                if (price == 0)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.NoPriceBookFound;
                    SystemMessage.Write(MessageType.Issue, Message.NoPriceBookFound);
                    return;
                }

                if (ApplicationSettings.QtyMustBeEnteredOnSaleOrderLine)
                {
                    var qty = (e.Row as SaleDetail).QuantityOrdered;
                    var wgt = (e.Row as SaleDetail).WeightOrdered;
                    if ((qty == null || qty == 0) && (wgt == null || wgt == 0))
                    {
                        e.IsValid = false;
                        e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                        e.ErrorContent = Message.NoQtyEntered;
                        SystemMessage.Write(MessageType.Issue, "No qty or wgt has been entered on this order line");
                        return;
                    }
                }

                if (ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarning)
                {
                    var unitPrice = (e.Row as SaleDetail).UnitPrice;
                    if (unitPrice == null || unitPrice == 0)
                    {
                        //e.IsValid = false;
                        e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning;
                        e.ErrorContent = Message.NoPriceEntered;
                        NouvemMessageBox.Show(Message.NoPriceEntered);
                        return;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void TableViewData_ShownEditor(object sender, DevExpress.Xpf.Grid.EditorEventArgs e)
        {
            try
            {
                (this.TableViewData.ActiveEditor as TextEdit).GotFocus += (o, args) => (o as TextEdit).SelectAll();
            }
            catch (Exception)
            {
            }
        }

        private void TableViewData_Loaded(object sender, RoutedEventArgs e)
        {
            //SaleDetail.PriceChangingOnDesktop = true;
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ARDispatchContainerView.xaml
    /// </summary>
    public partial class ARDispatchContainerView : Window
    {
        private bool canClose;
        public ARDispatchContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.CanCloseDispatchWindow, b => this.canClose = b);
            Messenger.Default.Register<string>(this, Token.CloseDispatchWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.WindowState = WindowSettings.DispatchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                if (!this.canClose)
                {
                    args.Cancel = true;
                    Messenger.Default.Send(Token.Message, Token.ClosingDispatchWindow);
                    return;
                }

                WindowSettings.DispatchHeight = this.Height;
                WindowSettings.DispatchLeft = this.Left;
                WindowSettings.DispatchTop = this.Top;
                WindowSettings.DispatchWidth = this.Width;
                WindowSettings.DispatchMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

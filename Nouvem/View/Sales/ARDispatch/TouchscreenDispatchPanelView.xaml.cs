﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenDispatchPanelView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TouchscreenDispatchPanelView.xaml
    /// </summary>
    public partial class TouchscreenDispatchPanelView : UserControl
    {
        public TouchscreenDispatchPanelView()
        {
            this.InitializeComponent();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.ViewModel.Sales.ARDispatch;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for FTraceView.xaml
    /// </summary>
    public partial class FTraceView : Window
    {
        public FTraceView()
        {
            InitializeComponent();
            this.DataContext = new FTraceViewModel();
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, s => this.Close());
        }
    }
}

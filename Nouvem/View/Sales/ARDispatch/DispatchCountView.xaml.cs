﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nouvem.Global;
using Nouvem.ViewModel;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for DispatchCountView.xaml
    /// </summary>
    public partial class DispatchCountView : UserControl
    {
        public DispatchCountView()
        {
            this.InitializeComponent();
            if (ApplicationSettings.ScannerMode)
            {
                this.DataContext = ViewModelLocator.ScannerMainDispatchStatic;
                this.TextBoxCount.FontSize = ApplicationSettings.DispatchBoxCountHandheldFontSize;
            }
            else
            {
                this.DataContext = ViewModelLocator.ARDispatchTouchscreenStatic;
                this.TextBoxCount.FontSize = ApplicationSettings.DispatchBoxCountFontSize;
            }
        }
    }
}

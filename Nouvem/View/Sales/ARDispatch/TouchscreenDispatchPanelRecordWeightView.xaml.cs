﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenDispatchPanelRecordWeightView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TouchscreenDispatchPanelRecordWeightView.xaml
    /// </summary>
    public partial class TouchscreenDispatchPanelRecordWeightView : UserControl
    {
        public TouchscreenDispatchPanelRecordWeightView()
        {
            this.InitializeComponent();
        }
    }
}


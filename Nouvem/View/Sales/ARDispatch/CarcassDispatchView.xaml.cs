﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Sales.ARDispatch;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for CarcassDispatchView.xaml
    /// </summary>
    public partial class CarcassDispatchView : Window
    {
        public CarcassDispatchView()
        {
            this.InitializeComponent();
            this.DataContext = new CarcassDispatchViewModel();
            Messenger.Default.Register<string>(this, Token.CloseCarcassDispatchWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlStockDetails.FilterCriteria = null;
            this.GridControlStockDetails.View.SearchString = string.Empty;

            this.WindowState = WindowSettings.CarcassDispatchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.CarcassDispatchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlStockDetails.RestoreLayoutFromXml(gridPath);
                    }
                    catch (Exception)
                    {
                       
                    }
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewStockData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlStockDetails.FilterCriteria = null;
                this.GridControlStockDetails.SaveLayoutToXml(gridPath);
                WindowSettings.CarcassDispatchHeight = this.Height;
                WindowSettings.CarcassDispatchLeft = this.Left;
                WindowSettings.CarcassDispatchTop = this.Top;
                WindowSettings.CarcassDispatchWidth = this.Width;
                WindowSettings.CarcassDispatchMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void TabItem_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlStockDetails.SelectAll();
        }
    }
}


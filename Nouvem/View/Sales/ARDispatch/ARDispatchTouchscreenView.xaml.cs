﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchTouchscreenView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using Nouvem.Global;
using Nouvem.Model.Enum;
using Nouvem.Properties;

namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ARDispatchTouchscreenView.xaml
    /// </summary>
    public partial class ARDispatchTouchscreenView : UserControl
    {
        public ARDispatchTouchscreenView()
        {
            this.InitializeComponent();
            if (ApplicationSettings.ShowPOAtDispatch)
            {
                this.GridDeiveryDate.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.GridDeiveryDateAndPo.Visibility = Visibility.Collapsed;
            }
            //if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight || ApplicationSettings.ScanOrWeighAtDispatch)
            //{
            //    this.GridColumnCode.Width = 35;
            //}
            //else
            //{
            //    this.GridColumnCode.Width = 0;
            //}

            //if (ApplicationSettings.Customer == Customer.Woolleys)
            //{
            //    this.GridColumnPrice.Width = 35;
            //    this.GridColumnCode.Width = 0;
            //    this.GridColumnCode.Visible = false;
            //    this.GridColumnCode.IsEnabled = false;
            //}
            //else
            //{
            //    this.GridColumnCode.Width = 35;
            //    this.GridColumnPrice.Width = 0;
            //    this.GridColumnPrice.Visible = false;
            //    this.GridColumnPrice.IsEnabled = false;
            //}
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchContainer2View.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ARDispatchContainer2View.xaml
    /// </summary>
    public partial class ARDispatchContainer2View : Window
    {
        private bool canClose;
        public ARDispatchContainer2View()
        {
            this.InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.CanCloseDispatch2Window, b => this.canClose = b);
            this.WindowState = WindowSettings.Dispatch2Maximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseDispatch2Window, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.Closing += (sender, args) =>
            {
                if (!this.canClose)
                {
                    args.Cancel = true;
                    Messenger.Default.Send(Token.Message, Token.ClosingDispatch2Window);
                    return;
                }

                WindowSettings.Dispatch2Height = this.Height;
                WindowSettings.Dispatch2Left = this.Left;
                WindowSettings.Dispatch2Top = this.Top;
                WindowSettings.Dispatch2Width = this.Width;
                WindowSettings.Dispatch2Maximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


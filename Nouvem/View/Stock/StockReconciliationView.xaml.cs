﻿// -----------------------------------------------------------------------
// <copyright file="StockReconciliationView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Stock
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for StockReconciliationView.xaml
    /// </summary>
    public partial class StockReconciliationView : Window
    {
        public StockReconciliationView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseStockReconciliationWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.WindowState = WindowSettings.StockReconciliationMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.MissingStockGridPath))
                {
                    this.GridControlMissing.RestoreLayoutFromXml(Settings.Default.MissingStockGridPath);
                }

                if (File.Exists(Settings.Default.RecoveredStockGridPath))
                {
                    this.GridControlRecovered.RestoreLayoutFromXml(Settings.Default.RecoveredStockGridPath);
                }

                if (File.Exists(Settings.Default.UnknownStockGridPath))
                {
                    this.GridControlUnknown.RestoreLayoutFromXml(Settings.Default.UnknownStockGridPath);
                }

                if (File.Exists(Settings.Default.AccountedForStockGridPath))
                {
                    this.GridControlAccountedFor.RestoreLayoutFromXml(Settings.Default.AccountedForStockGridPath);
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.StockReconciliationHeight = this.Height;
                WindowSettings.StockReconciliationLeft = this.Left;
                WindowSettings.StockReconciliationTop = this.Top;
                WindowSettings.StockReconciliationWidth = this.Width;
                WindowSettings.StockReconciliationMaximised = this.WindowState.ToBool();
                this.GridControlMissing.SaveLayoutToXml(Settings.Default.MissingStockGridPath);
                this.GridControlRecovered.SaveLayoutToXml(Settings.Default.RecoveredStockGridPath);
                this.GridControlUnknown.SaveLayoutToXml(Settings.Default.UnknownStockGridPath);
                this.GridControlAccountedFor.SaveLayoutToXml(Settings.Default.AccountedForStockGridPath);
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="StocktakeView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Stock
{
    using System.Threading;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for StocktakeView.xaml
    /// </summary>
    public partial class StocktakeView : Window
    {
        public StocktakeView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseStockStockTakeWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.Loaded += (sender, args) => this.TextBoxName.Focus();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxName.Dispatcher.Invoke(() => this.TextBoxName.Focus()));

            this.WindowState = WindowSettings.StockTakeMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.StockTakeHeight = this.Height;
                WindowSettings.StockTakeLeft = this.Left;
                WindowSettings.StockTakeTop = this.Top;
                WindowSettings.StockTakeWidth = this.Width;
                WindowSettings.StockTakeMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

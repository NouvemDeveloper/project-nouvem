﻿// -----------------------------------------------------------------------
// <copyright file="ReportCarcassSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.View.Report
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ReportCarcassSearchDataView.xaml
    /// </summary>
    public partial class ReportCarcassSearchDataView : Window
    {
        public ReportCarcassSearchDataView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.GetCarcassSales, s => this.GetCarcassSales());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            ApplicationSettings.CarcassSearchScreenLoaded = true;

            var gridPath = Settings.Default.ReportCarcassSearch.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                ApplicationSettings.CarcassSearchScreenLoaded = false;
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                    }
                    catch
                    {
                    }
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

        }

        private void GetCarcassSales()
        {
            var sales = new List<Sale>();
            for (int i = 0; i < this.GridControlSearchData.VisibleRowCount; i++)
            {
                var dataRow = this.GridControlSearchData.GetRow(this.GridControlSearchData.GetRowHandleByVisibleIndex(i));
                if (dataRow is Sale)
                {
                    var localSale = (Sale) dataRow;
                    sales.Add(localSale);
                }
            }

            Messenger.Default.Send(sales, Token.SendCarcassSales);
        }
    }
}



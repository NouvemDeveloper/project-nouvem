﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenReportView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using Nouvem.BusinessLogic;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Report
{
    using System;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Microsoft.Office.Interop.Outlook;
    using Nouvem.Global;
    using Nouvem.Logging;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Interaction logic for ReportViewer.xaml
    /// </summary>
    public partial class TouchscreenReportView : Window
    {
        /// <summary>
        /// The application logger.
        /// </summary>
        private Logger log = new Logger();

        /// <summary>
        /// Count when displaying multiple reports.
        /// </summary>
        private int reportCount;

        /// <summary>
        /// The current report being displayed.
        /// </summary>
        private ReportData currentReport;

        public TouchscreenReportView()
        {
            this.InitializeComponent();
            this.TextBoxCopies.Text = ApplicationSettings.TouchscreenReportsCopiesToPrint.ToString();

            Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);

            Messenger.Default.Register<string>(this, Token.CloseReportViewer, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.AttachReportToEmail, s => this.AttachReportToEmail());
            Messenger.Default.Register<string>(this, Token.AttachReportsToEmail, s => this.AttachReportsToEmail());

            this.ReportingViewer.ProcessingMode = ProcessingMode.Remote;
            this.ReportingViewer.ServerReport.ReportServerCredentials.NetworkCredentials
            = new System.Net.NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);

            this.Loaded += (sender, args) =>
            {
                this.ReportingViewer.ServerReport.ReportServerUrl = new Uri(ApplicationSettings.ReportServerPath);
            };

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            // Register for the incoming report to display.
            Messenger.Default.Register<Model.BusinessObject.ReportData>(this, Token.SetTouchscreenReportPath, r =>
            {
                this.currentReport = r;

                try
                {
                    var reportPath = r.Path;
                    var reportParameters = r.ReportParameters;
                    this.ReportingViewer.ServerReport.ReportPath = reportPath;

                    if (reportParameters != null)
                    {
                        // We are programmically setting the parameters.
                        this.ReportingViewer.ServerReport.SetParameters(reportParameters);
                    }

                    this.ReportingViewer.RefreshReport();
                    this.log.LogDebug(this.GetType(), "Report Printed");
                }
                catch (System.Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            });

            // Register for the incoming report to display.
            Messenger.Default.Register<Model.BusinessObject.ReportData>(this, Token.SetReportPathAndExport, r =>
            {
                try
                {
                    var reportPath = r.Path;
                    var reportParameters = r.ReportParameters;
                    this.ReportingViewer.ServerReport.ReportPath = reportPath;

                    if (reportParameters != null)
                    {
                        // We are programmically setting the parameters.
                        this.ReportingViewer.ServerReport.SetParameters(reportParameters);
                    }

                    this.ReportingViewer.RefreshReport();
                    this.ExportReports();
                    this.log.LogDebug(this.GetType(), "Report Printed");
                }
                catch (System.Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            });
        }

        /// <summary>
        /// Exports the current report out to a pdf file
        /// </summary>
        /// <returns>Path to the file that was generated</returns>
        private string ExportReport()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            //Render the report to a byte array
            byte[] bytes;
            if (this.ReportingViewer.ProcessingMode == ProcessingMode.Local)
            {
                bytes = this.ReportingViewer.LocalReport.Render("PDF", null, out mimeType,
                    out encoding, out filenameExtension, out streamids, out warnings);
            }
            else
            {
                bytes = this.ReportingViewer.ServerReport.Render("PDF", null, out mimeType,
                   out encoding, out filenameExtension, out streamids, out warnings);
            }

            //Write report out to temporary PDF file
            var filename = Path.Combine(Settings.Default.ExportReportPath, "Report.pdf");
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            //return path to saved file
            return filename;
        }

        /// <summary>
        /// Attach report to the selected email account.
        /// </summary>
        private void AttachReportToEmail()
        {
            try
            {
                //Write report out to temporary PDF file
                var reportFilename = this.ExportReport();

                this.log.LogDebug(this.GetType(), string.Format("AttachReportToEmail(): Type:{0} To:{1}, Subject:{2}, Body:{3}",
                                                                ApplicationSettings.SSRSEmailType, ApplicationSettings.SSRSEmailTo,
                                                                ApplicationSettings.SSRSEmailSubject, ApplicationSettings.SSRSEmailBody));

                if (ApplicationSettings.SSRSEmailType.Equals("Outlook"))
                {
                    Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                    var mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
                    mailItem.To = ApplicationSettings.SSRSEmailTo;
                    mailItem.Subject = ApplicationSettings.SSRSEmailSubject;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = ApplicationSettings.SSRSEmailBody;

                    mailItem.Attachments.Add(reportFilename, OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                    mailItem.Display(true);
                }

                //// Create a new email using outlook
                //ApplicationClass outlookApp = new ApplicationClass();
                //MailItem mailItem = (MailItem)outlookApp.CreateItem(OlItemType.olMailItem);
                //mailItem.To = "mail@yourdomain.com";
                //mailItem.Subject = "Your Report";
                //mailItem.Body = "Please find your report attached";
                //mailItem.Attachments.Add(reportFilename, (int)OlAttachmentType.olByValue, 1, reportFilename);

                //Remove the temp file once attached
                Array.ForEach(Directory.GetFiles(Path.Combine("@", Settings.Default.ExportReportPath)), File.Delete);

                // Display the window
                //mailItem.Display(false);
            }
            catch (System.Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Exports the current report out to a pdf file
        /// </summary>
        /// <returns>Path to the file that was generated</returns>
        private string ExportReports()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            //Render the report to a byte array
            byte[] bytes;
            if (this.ReportingViewer.ProcessingMode == ProcessingMode.Local)
            {
                bytes = this.ReportingViewer.LocalReport.Render("PDF", null, out mimeType,
                    out encoding, out filenameExtension, out streamids, out warnings);
            }
            else
            {
                bytes = this.ReportingViewer.ServerReport.Render("PDF", null, out mimeType,
                   out encoding, out filenameExtension, out streamids, out warnings);
            }

            //Write report out to temporary PDF file
            var reportSuffix = string.Format("Report{0}.pdf", this.reportCount);
            var filename = Path.Combine(Settings.Default.ExportReportPath, reportSuffix);
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            this.reportCount++;

            //return path to saved file
            return filename;
        }

        /// <summary>
        /// Attach reports to the selected email account.
        /// </summary>
        private void AttachReportsToEmail()
        {
            try
            {
                if (ApplicationSettings.SSRSEmailType.Equals("Outlook"))
                {
                    Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                    var mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
                    mailItem.To = ApplicationSettings.SSRSEmailTo;
                    mailItem.Subject = ApplicationSettings.SSRSEmailSubject;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = ApplicationSettings.SSRSEmailBody;

                    foreach (var file in Directory.GetFiles(Settings.Default.ExportReportPath, "*.pdf"))
                    {
                        mailItem.Attachments.Add(file, OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                    }

                    mailItem.Display(true);
                }

                //// Create a new email using outlook
                //ApplicationClass outlookApp = new ApplicationClass();
                //MailItem mailItem = (MailItem)outlookApp.CreateItem(OlItemType.olMailItem);
                //mailItem.To = "mail@yourdomain.com";
                //mailItem.Subject = "Your Report";
                //mailItem.Body = "Please find your report attached";
                //mailItem.Attachments.Add(reportFilename, (int)OlAttachmentType.olByValue, 1, reportFilename);

                //Remove the temp file once attached
                Array.ForEach(Directory.GetFiles(Path.Combine("@", Settings.Default.ExportReportPath)), File.Delete);

                // Display the window
                //mailItem.Display(false);
            }
            catch (System.Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.currentReport != null)
            {
                for (int i = 0; i < this.TextBoxCopies.Text.ToInt(); i++)
                {
                    ReportManager.Instance.PrintReport(this.currentReport);
                }
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var copies = this.TextBoxCopies.Text.ToInt();
            copies++;
            this.TextBoxCopies.Text = copies.ToString();
            Settings.Default.TouchscreenReportsCopiesToPrint = copies;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var copies = this.TextBoxCopies.Text.ToInt();
            if (copies == 1)
            {
                return;
            }

            copies--;
            this.TextBoxCopies.Text = copies.ToString();
            Settings.Default.TouchscreenReportsCopiesToPrint = copies;
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="ReportSearch.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevExpress.Xpf.Grid;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Report;

namespace Nouvem.View.Report
{
    using System.IO;
    using System.Windows;
    using DevExpress.Xpf.Printing;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for ReportSearchView.xaml
    /// </summary>
    public partial class ReportTouchscreenSearchView : Window
    {
        public ReportTouchscreenSearchView(ReportData report)
        {
            this.InitializeComponent();
            this.DataContext = new ReportSearchTouchscreenViewModel(report);
            this.GridControlSearchData.SelectionMode = report.MultiSelect ? MultiSelectMode.Row : MultiSelectMode.None;
            this.Title = string.Format("{0}: {1}", Strings.Report, string.IsNullOrWhiteSpace(report.Alias) ? report.Name : report.Alias);
            Messenger.Default.Register<bool>(this, Token.SearchMultiSelect, b => this.GridControlSearchData.SelectionMode = b ? MultiSelectMode.Row : MultiSelectMode.None);
            Messenger.Default.Register<string>(this, Token.RequestReportRows, s => this.GetFilteredData());

            this.WindowState = WindowState.Maximized;
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;

            Messenger.Default.Register<CollectionData>(this, Token.Macro, this.HandleMacro);

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseReportSearchWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            var gridPath = string.Format("C:\\Nouvem\\GridLayout\\report_{0}{1}.xml", report.Name, NouvemGlobal.UserId.ToInt());

            this.Closing += (sender, args) =>
            {
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                try
                {
                    this.GridControlSearchData.SaveLayoutToXml(gridPath);
                }
                catch (Exception)
                {
                }

                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void GetFilteredData()
        {
            var rows = new List<CollectionData>();
            for (int i = 0; i < this.GridControlSearchData.VisibleRowCount; i++)
            {
                var dataRow = this.GridControlSearchData.GetRow(this.GridControlSearchData.GetRowHandleByVisibleIndex(i));
                if (dataRow is CollectionData)
                {
                    rows.Add((CollectionData)dataRow);
                }
            }

            Messenger.Default.Send(rows, Token.SendingReportRows);
        }

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlSearchData.SelectAll();
        }

        private void HandleMacro(CollectionData data)
        {
            if (!data.RowData.Any())
            {
                return;
            }

            var rowsToShow = new List<string>();
            foreach (var rowData in data.RowData)
            {
                if (rowData.Item1 == "Item1") { this.GridColumn1.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item2") { this.GridColumn2.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item3") { this.GridColumn3.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item4") { this.GridColumn4.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item5") { this.GridColumn5.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item6") { this.GridColumn6.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item7") { this.GridColumn7.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item8") { this.GridColumn8.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item9") { this.GridColumn9.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item10") { this.GridColumn10.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item11") { this.GridColumn11.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item12") { this.GridColumn12.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item13") { this.GridColumn13.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item14") { this.GridColumn14.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item15") { this.GridColumn15.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item16") { this.GridColumn16.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item17") { this.GridColumn17.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item18") { this.GridColumn18.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item19") { this.GridColumn19.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item20") { this.GridColumn20.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item21") { this.GridColumn21.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item22") { this.GridColumn22.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item23") { this.GridColumn23.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item24") { this.GridColumn24.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item25") { this.GridColumn25.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item26") { this.GridColumn26.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item27") { this.GridColumn27.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item28") { this.GridColumn28.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item29") { this.GridColumn29.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
                if (rowData.Item1 == "Item30") { this.GridColumn30.Header = rowData.Item2; rowsToShow.Add(rowData.Item1); continue; }
            }

            this.HideRemainingColumns(rowsToShow);
        }

        private void HideRemainingColumns(IList<string> rowsToShow)
        {
            if (!rowsToShow.Contains("Item1")) { this.GridColumn1.Visible = false; }
            if (!rowsToShow.Contains("Item2")) { this.GridColumn2.Visible = false; }
            if (!rowsToShow.Contains("Item3")) { this.GridColumn3.Visible = false; }
            if (!rowsToShow.Contains("Item4")) { this.GridColumn4.Visible = false; }
            if (!rowsToShow.Contains("Item5")) { this.GridColumn5.Visible = false; }
            if (!rowsToShow.Contains("Item6")) { this.GridColumn6.Visible = false; }
            if (!rowsToShow.Contains("Item7")) { this.GridColumn7.Visible = false; }
            if (!rowsToShow.Contains("Item8")) { this.GridColumn8.Visible = false; }
            if (!rowsToShow.Contains("Item9")) { this.GridColumn9.Visible = false; }
            if (!rowsToShow.Contains("Item10")) { this.GridColumn10.Visible = false; }
            if (!rowsToShow.Contains("Item11")) { this.GridColumn11.Visible = false; }
            if (!rowsToShow.Contains("Item12")) { this.GridColumn12.Visible = false; }
            if (!rowsToShow.Contains("Item13")) { this.GridColumn13.Visible = false; }
            if (!rowsToShow.Contains("Item14")) { this.GridColumn14.Visible = false; }
            if (!rowsToShow.Contains("Item15")) { this.GridColumn15.Visible = false; }
            if (!rowsToShow.Contains("Item16")) { this.GridColumn16.Visible = false; }
            if (!rowsToShow.Contains("Item17")) { this.GridColumn17.Visible = false; }
            if (!rowsToShow.Contains("Item18")) { this.GridColumn18.Visible = false; }
            if (!rowsToShow.Contains("Item19")) { this.GridColumn19.Visible = false; }
            if (!rowsToShow.Contains("Item20")) { this.GridColumn20.Visible = false; }
            if (!rowsToShow.Contains("Item21")) { this.GridColumn21.Visible = false; }
            if (!rowsToShow.Contains("Item22")) { this.GridColumn22.Visible = false; }
            if (!rowsToShow.Contains("Item23")) { this.GridColumn23.Visible = false; }
            if (!rowsToShow.Contains("Item24")) { this.GridColumn24.Visible = false; }
            if (!rowsToShow.Contains("Item25")) { this.GridColumn25.Visible = false; }
            if (!rowsToShow.Contains("Item26")) { this.GridColumn26.Visible = false; }
            if (!rowsToShow.Contains("Item27")) { this.GridColumn27.Visible = false; }
            if (!rowsToShow.Contains("Item28")) { this.GridColumn28.Visible = false; }
            if (!rowsToShow.Contains("Item29")) { this.GridColumn29.Visible = false; }
            if (!rowsToShow.Contains("Item30")) { this.GridColumn30.Visible = false; }
        }
    }
}




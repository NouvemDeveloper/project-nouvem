﻿// -----------------------------------------------------------------------
// <copyright file="SaleSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.Report
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ReportSaleSearchDataView.xaml
    /// </summary>
    public partial class ReportSaleSearchDataView : Window
    {
        public ReportSaleSearchDataView()
        {
            this.InitializeComponent();

            //if (Settings.Default.TouchScreenMode)
            //{
            //    this.WindowState = WindowState.Maximized;
            //}
            //else
            //{
            //    this.WindowState = WindowState.Normal;
            //}

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            //this.CheckBoxExpand.Checked += (o, e) => this.GridControlSearchData.ExpandAllGroups();
            //this.CheckBoxExpand.Unchecked += (o, e) => this.GridControlSearchData.CollapseAllGroups();

            var gridPath = Settings.Default.ReportSalesSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.Show();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void TableView_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!ApplicationSettings.TouchScreenModeOnly)
            {
                var rowHandle = this.TableView.GetRowHandleByMouseEventArgs(e);
                if (rowHandle == DevExpress.Xpf.Grid.GridControl.InvalidRowHandle)
                {
                    return;
                }

                if (this.GridControlSearchData.IsValidRowHandle(rowHandle))
                {
                    if (this.GridControlSearchData.CurrentColumn == this.GridControlSearchData.Columns["TechnicalNotes"])
                    {
                        Messenger.Default.Send(Token.Message, Token.ShowReportNotesWindow);
                    }
                }

                return;
            }

            Messenger.Default.Send(Token.Message, Token.DisplaySearchKeyboard);
        }
    }
}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Shared;

namespace Nouvem.View.Attribute
{
    /// <summary>
    /// Interaction logic for TemplateGroupView.xaml
    /// </summary>
    public partial class TemplateGroupView : Window
    {
        public TemplateGroupView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseTraceabilityGroupWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                WindowSettings.TemplateGroupHeight = this.Height;
                WindowSettings.TemplateGroupLeft = this.Left;
                WindowSettings.TemplateGroupTop = this.Top;
                WindowSettings.TemplateGroupWidth = this.Width;
                WindowSettings.TemplateGroupMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(Token.Message, Token.TurnDefineNewGroupOff);
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            this.WindowState = WindowSettings.TemplateGroupMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


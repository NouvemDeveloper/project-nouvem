﻿// -----------------------------------------------------------------------
// <copyright file="AttributeResponseView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Attribute
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for AttributeResponseView.xaml
    /// </summary>
    public partial class AttributeResponseView : Window
    {
        public AttributeResponseView()
        {
            this.InitializeComponent();
            this.Topmost = true;
            Messenger.Default.Register<string>(this, Token.CloseWorkflow, s => this.Close());
            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}


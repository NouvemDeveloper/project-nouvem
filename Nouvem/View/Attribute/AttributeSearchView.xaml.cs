﻿// -----------------------------------------------------------------------
// <copyright file="AttributeSearchView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using DevExpress.Xpf.Grid;

namespace Nouvem.View.Attribute
{
    using System.IO;
    using System.Windows;
    using DevExpress.Xpf.Printing;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for AttributeSearchView.xaml
    /// </summary>
    public partial class AttributeSearchView : Window
    {
        public AttributeSearchView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.AttributeSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            var gridPath = Settings.Default.AttributeSearchGrid.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.Show();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlSearchData.FilterCriteria = null;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                WindowSettings.AttributeSearchHeight = this.Height;
                WindowSettings.AttributeSearchLeft = this.Left;
                WindowSettings.AttributeSearchTop = this.Top;
                WindowSettings.AttributeSearchWidth = this.Width;
                WindowSettings.AttributeSearchMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        //private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    var rowHandle = this.TableView.GetRowHandleByMouseEventArgs(e);
        //    if (rowHandle == GridControl.InvalidRowHandle)
        //    {
        //        return;
        //    }

        //    if (this.GridControlSearchData.IsValidRowHandle(rowHandle))
        //    {
        //        if (this.GridControlSearchData.CurrentColumn == this.GridControlSearchData.Columns["PopUpNote"])
        //        {
        //            Messenger.Default.Send(Token.Message, Token.ShowDesktopNotesWindow);
        //        }
        //    }
        //}
    }
}


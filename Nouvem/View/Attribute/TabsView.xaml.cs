﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Shared;
using Nouvem.ViewModel.Attribute;

namespace Nouvem.View.Attribute
{
    /// <summary>
    /// Interaction logic for TabsView.xaml
    /// </summary>
    public partial class TabsView : Window
    {
        public TabsView()
        {
            this.InitializeComponent();
            this.DataContext = new TabsViewModel();

            this.WindowState = WindowSettings.TabsMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseTabsSetUpWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
           
            this.Closing += (sender, args) =>
            {
                WindowSettings.TabsHeight = this.Height;
                WindowSettings.TabsLeft = this.Left;
                WindowSettings.TabsTop = this.Top;
                WindowSettings.TabsWidth = this.Width;
                WindowSettings.TabsMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

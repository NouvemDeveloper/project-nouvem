﻿// -----------------------------------------------------------------------
// <copyright file="KeyboardContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Interaction logic for KeyboardContainerView.xaml
    /// </summary>
    public partial class KeyboardSmallContainerView : Window
    {
        public KeyboardSmallContainerView(ViewType target)
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x => this.Close());
            this.Loaded += (sender, args) => this.TextBoxInput.Focus();
            this.Keyboard.Closed += (sender, args) =>
            {
                if (target == ViewType.ScannerIntoProduction)
                {
                    Messenger.Default.Send(this.TextBoxInput.Text, Token.KeyboardValueEntered);
                }

                Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            };
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ProcessSelectionView.xaml
    /// </summary>
    public partial class ProcessSelectionView : Window
    {
        public ProcessSelectionView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseProcess, s => this.Close());
        }
    }
}

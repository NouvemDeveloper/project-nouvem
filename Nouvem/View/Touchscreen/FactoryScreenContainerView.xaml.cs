﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;
using Nouvem.BusinessLogic;
using Nouvem.Logging;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for FactoryScreenContainerView.xaml
    /// </summary>
    public partial class FactoryScreenContainerView : Window
    {
        private DispatcherTimer applicationIdleTimer;
        private LowLevelKeyboardListener listener;
        private Logging.Logger log = new Logger();
        private StringBuilder scannerString;

        public FactoryScreenContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseFactoryWindow, s =>
            {
                this.Close();
            });

            this.WindowStyle = WindowStyle.None;
            if (ApplicationSettings.AllowTouchScreenResize)
            {
                this.WindowStyle = WindowStyle.SingleBorderWindow;
            }

            if (ApplicationSettings.LogOutOnApplicationIdle)
            {
                Messenger.Default.Register<string>(this, Token.Scanner, s => this.Reset());
                this.applicationIdleTimer = new DispatcherTimer();
                this.applicationIdleTimer.Interval = TimeSpan.FromMinutes(ApplicationSettings.LogOutOnApplicationIdleTime);
                this.applicationIdleTimer.Start();
                this.applicationIdleTimer.Tick += (sender, args) =>
                {
                    if (!ViewModelLocator.IsWorkflowNull() && ViewModelLocator.WorkflowStatic.IsFormLoaded)
                    {
                        return;
                    }

                    this.LogOut();
                };
            }

            this.Loaded += (sender, args) =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
                Messenger.Default.Send(Token.Message, Token.CloseLogin);
                ThreadPool.QueueUserWorkItem(x => this.Dispatcher.Invoke(() => this.Focus()));

                if (ApplicationSettings.UseKeyboardWedgeAtTouchscreen)
                {
                    this.scannerString = new StringBuilder();
                    this.listener = new LowLevelKeyboardListener();
                    this.listener.OnKeyPressed += listener_OnKeyPressed;
                    this.listener.HookKeyboard();
                }
            };
            
            this.Closing += (sender, args) =>
            {
                if (this.applicationIdleTimer != null)
                {
                    this.applicationIdleTimer.Stop();
                }

                if (this.listener != null)
                {
                    this.listener.UnHookKeyboard();
                }
           
                Messenger.Default.Unregister(this);
            };
        }

        void listener_OnKeyPressed(object sender, KeyPressedArgs e)
        {
            if (e.KeyPressed == Key.X)
            {
                Messenger.Default.Send(this.scannerString.ToString(), Token.Scanner);
                this.scannerString.Clear();
                return;
            }

            this.scannerString.Append(e.KeyPressed.ToScannerString());
        }

        private void Window_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Reset();
        }

        /// <summary>
        /// Resets the timer.
        /// </summary>
        private void Reset()
        {
            if (ApplicationSettings.LogOutOnApplicationIdle)
            {
                this.applicationIdleTimer.Stop();
                this.applicationIdleTimer.Start();
            }
        }

        private void LogOut()
        {
            Settings.Default.AllowMultipleInstancesOfApplication = true;
            Settings.Default.Save();
            //ViewModelLocator.CleanUpTouchscreens();
            var data = DataManager.Instance;
            data.LogOut();
            //this.DataManager.SaveDeviceSettings();
            System.Windows.Forms.Application.Restart();
            //Environment.Exit(0);
            //Application.Current.Dispatcher.Invoke(Application.Current.Shutdown);
            Process.GetCurrentProcess().Kill();
        }
    }
}

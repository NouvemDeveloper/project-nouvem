﻿// -----------------------------------------------------------------------
// <copyright file="CollectionDisplayContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for CollectionDisplayContainerView.xaml
    /// </summary>
    public partial class CollectionDisplayContainerView : Window
    {
        public CollectionDisplayContainerView(bool desktop = false)
        {
            this.InitializeComponent();
#if !DEBUG
            this.Topmost = true;
#endif

            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x =>
            {
                this.Close();
            });


            if (desktop)
            {
                this.WindowState = WindowState.Normal;
                this.WindowStyle = WindowStyle.None;
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Width = 700;
                this.Height = 700;
            }
        }
    }
}

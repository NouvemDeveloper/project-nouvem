﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Editors;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.ViewModel.Touchscreen;

namespace Nouvem.View.Touchscreen
{
    /// <summary>
    /// Interaction logic for TouchscreenStockTakeView.xaml
    /// </summary>
    public partial class TouchscreenStockTakeView : Window
    {
        public TouchscreenStockTakeView()
        {
            this.InitializeComponent();
            this.DataContext = new TouchscreenStockTakeViewModel();

            ThreadPool.QueueUserWorkItem(x => this.TextBoxBarcode.Dispatcher.Invoke(() => this.TextBoxBarcode.Focus()));

            Messenger.Default.Register<string>(this, Token.CloseTouchscreenStockTake, s => this.Close());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());
            this.Unloaded += (sender, args) =>
            {
                this.GridControlDetails.SaveLayoutToXml(Settings.Default.ScannerStockTakePath);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.ScannerStockTakePath))
                {
                    this.GridControlDetails.RestoreLayoutFromXml(Settings.Default.ScannerStockTakePath);
                }

                this.TextBoxBarcode.Focus();
            };
        }

        private void UserControl_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            this.TextBoxBarcode.Focus();
        }

        private void ComboBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ComboBoxEdit)
            {
                var localCombo = (sender as ComboBoxEdit);
                localCombo.ShowPopup();
            }

            //this.TextBoxBarcode.Focus();
        }

        private void ComboBoxEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }

        private void GridControlDetails_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}


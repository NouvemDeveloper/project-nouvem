﻿// -----------------------------------------------------------------------
// <copyright file="CollectionDisplayView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for CollectionDisplayView.xaml
    /// </summary>
    public partial class CollectionDisplayView : UserControl
    {
        public CollectionDisplayView()
        {
            this.InitializeComponent();
            this.TextBoxQuestion.Height = 0;
            this.GridSearchBox.Height = 0;

            Messenger.Default.Register<string>(this, Token.DisplayCollectionSearchKeyboard, s => this.DisplayKeyboard());
            Messenger.Default.Register<string>(this, Token.ShowAttributeQuestion, s =>
            {
                this.TextBoxQuestion.Text = s;
                this.TextBoxQuestion.Height = 90;
            });

            this.Unloaded += this.OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Messenger.Default.Unregister(this);
            BindingOperations.ClearAllBindings(this);
            this.Unloaded -= this.OnUnloaded;
        }

        /// <summary>
        /// Open the keyboard.
        /// </summary>
        private void DisplayKeyboard()
        {
            this.GridSearchBox.Height = 50;
            this.Keyboard.IsOpen = true;
            this.TextBoxSearch.Focus();
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            this.GridSearchBox.Height = 0;
        }
    }
}

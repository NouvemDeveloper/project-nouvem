﻿// -----------------------------------------------------------------------
// <copyright file="MenuContainerView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for MenuContainerView.xaml
    /// </summary>
    public partial class MenuContainerView : Window
    {
        public MenuContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseMenuWindow, s => this.Close());
            this.Topmost = true;
        }
    }
}

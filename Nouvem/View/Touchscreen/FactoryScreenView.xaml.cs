﻿// -----------------------------------------------------------------------
// <copyright file="FactoryScreenView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Media.Imaging;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for FactoryScreenView.xaml
    /// </summary>
    public partial class FactoryScreenView : UserControl
    {
        public FactoryScreenView()
        {
            this.InitializeComponent();

            this.SetImage(this.ExpanderAttributes.IsExpanded);

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, this.SetImage);
            Messenger.Default.Register<bool>(this, Token.DisableLegacyAttributesView, b =>
            {
                if (b)
                {
                    this.TouchscreenTraceabilityView.IsEnabled = false;
                    this.TouchscreenTraceabilityView.IsHitTestVisible = false;
                    this.TouchscreenTraceabilityView.Visibility = Visibility.Collapsed;

                    this.AttributesView.IsEnabled = true;
                    this.AttributesView.IsHitTestVisible = true;
                    this.AttributesView.Visibility = Visibility.Visible;
                }
                else
                {
                    this.TouchscreenTraceabilityView.IsEnabled = true;
                    this.TouchscreenTraceabilityView.IsHitTestVisible = true;
                    this.TouchscreenTraceabilityView.Visibility = Visibility.Visible;

                    this.AttributesView.IsEnabled = false;
                    this.AttributesView.IsHitTestVisible = false;
                    this.AttributesView.Visibility = Visibility.Collapsed;
                }
            });
        }

        private void SetImage(bool b)
        {
            if (!b)
            {
                this.ImageExpander.Source = new BitmapImage(
                    new Uri("pack://application:,,,/Design/Image/ArrowVUp.png"));
            }
            else
            {
                this.ImageExpander.Source = new BitmapImage(
                    new Uri("pack://application:,,,/Design/Image/ArrowVDown.png"));
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="Touchscreen.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Nouvem.ViewModel;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SwitchTouchscreenModuleView.xaml
    /// </summary>
    public partial class SwitchTouchscreenModuleView : Window
    {
        public SwitchTouchscreenModuleView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseModuleSelection, s => this.Close());

            this.Loaded += (sender, args) =>
            {
                var localModules = ViewModelLocator.MasterStatic.AllowedTouchscreenModules;
                if (localModules != null)
                {
                    var count = localModules.Count;
                    var margins = (count * 20);
                    var localHeight = (this.Grid.ActualHeight - margins) / count;
                    if (!localModules.Any(x => x.UserGroupID == 123))
                    {
                        this.ButtonLairage.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonLairage.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 124))
                    {
                        this.ButtonSequencer.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonSequencer.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 125))
                    {
                        this.ButtonGrader.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonGrader.Height = localHeight;
                    }


                    if (!localModules.Any(x => x.UserGroupID == 126))
                    {
                        this.ButtonGoodsIn.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonGoodsIn.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 127))
                    {
                        this.ButtonIntoProd.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonIntoProd.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 128))
                    {
                        this.ButtonOutOfProd.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonOutOfProd.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 129))
                    {
                        this.ButtonDispatch.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonDispatch.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 130))
                    {
                        this.ButtonPallet.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonPallet.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 131))
                    {
                        this.ButtonWorkflow.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonWorkflow.Height = localHeight;
                    }

                    if (!localModules.Any(x => x.UserGroupID == 145))
                    {
                        this.ButtonReturns.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        this.ButtonReturns.Height = localHeight;
                    }
                }
                else
                {
                    var count = 9;
                    var margins = (count * 20);
                    var localHeight = (this.Grid.ActualHeight - margins) / count;
                    this.ButtonLairage.Height = localHeight;
                    this.ButtonSequencer.Height = localHeight;
                    this.ButtonGrader.Height = localHeight;
                    this.ButtonGoodsIn.Height = localHeight;
                    this.ButtonIntoProd.Height = localHeight;
                    this.ButtonOutOfProd.Height = localHeight;
                    this.ButtonDispatch.Height = localHeight;
                    this.ButtonPallet.Height = localHeight;
                    this.ButtonWorkflow.Height = localHeight;
                    this.ButtonReturns.Height = localHeight;
                }
            };
        }
    }
}

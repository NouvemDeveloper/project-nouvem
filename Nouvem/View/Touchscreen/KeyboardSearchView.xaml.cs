﻿// -----------------------------------------------------------------------
// <copyright file="KeyboardSearchView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for KeyboardSearchView.xaml
    /// </summary>
    public partial class KeyboardSearchView : Window
    {
        public KeyboardSearchView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x => this.Close());
            this.Closing += (sender, args) => this.Keyboard.IsOpen = false;
            this.Keyboard.Closed += (sender, args) => this.Close();
            this.WindowStyle = WindowStyle.None;

            this.Loaded += (sender, args) => Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        private void Window_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.WindowStyle = WindowStyle.SingleBorderWindow;
        }
    }
} 

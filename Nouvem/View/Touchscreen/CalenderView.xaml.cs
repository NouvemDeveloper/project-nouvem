﻿// -----------------------------------------------------------------------
// <copyright file="CalenderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;

namespace Nouvem.View.Touchscreen
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for CalenderView.xaml
    /// </summary>
    public partial class CalenderView : Window
    {
        private string mode;
        public CalenderView(string mode = "")
        {
            this.InitializeComponent();
            this.mode = mode;
            this.Calendar.DisplayDate = DateTime.Today;
            this.TextBoxQuestion.Height = 0;
            Messenger.Default.Register<string>(this, Token.ShowAttributeQuestion, s =>
            {
                this.TextBoxQuestion.Text = s;
                this.TextBoxQuestion.Height = 90;
            });

            this.Closing += this.OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
        }

        private void Button_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Calendar_SelectedDatesChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var token = this.mode == string.Empty ? Token.CalenderDisplayValue : Token.CalenderDisplayValueDispatch;
            var date = this.Calendar.SelectedDate.ToDate().ToShortDateString();
            Messenger.Default.Send(date, token);
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Touchscreen
{
    /// <summary>
    /// Interaction logic for ReportDisplayView.xaml
    /// </summary>
    public partial class ReportDisplayView : Window
    {
        public ReportDisplayView()
        {
            InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x => this.Close());
        }
    }
}

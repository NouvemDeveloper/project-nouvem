﻿// -----------------------------------------------------------------------
// <copyright file="LabelSelectionView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Controls;
using System.Windows.Input;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for LabelSelection.xaml
    /// </summary>
    public partial class LabelSelectionView : Window
    {
        public LabelSelectionView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseLabelSelectionWindow, s => this.Close());
        }

        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }
    }
}

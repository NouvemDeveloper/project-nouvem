﻿// -----------------------------------------------------------------------
// <copyright file="DateTemplateNameContainerView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Date
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for DateTemplateNameContainerView.xaml
    /// </summary>
    public partial class DateTemplateNameContainerView : Window
    {
        public DateTemplateNameContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseDateTemplateNameWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

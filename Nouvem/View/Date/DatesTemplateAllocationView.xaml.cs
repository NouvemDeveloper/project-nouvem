﻿// -----------------------------------------------------------------------
// <copyright file="DatesTemplateAllocationView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Date
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for DatesTemplateAllocationView.xaml
    /// </summary>
    public partial class DatesTemplateAllocationView : UserControl
    {
        public DatesTemplateAllocationView()
        {
            this.InitializeComponent();
        }
    }
}

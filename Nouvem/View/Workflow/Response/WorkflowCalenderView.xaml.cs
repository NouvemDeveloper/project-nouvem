﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowCalenderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Workflow.Response
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for WorkflowCalenderView.xaml
    /// </summary>
    public partial class WorkflowCalenderView : UserControl
    {
        public WorkflowCalenderView()
        {
            this.InitializeComponent();
        }
    }
}

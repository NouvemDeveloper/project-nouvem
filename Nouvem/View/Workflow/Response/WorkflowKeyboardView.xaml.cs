﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowKeyboardView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
using System.Threading;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Workflow.Response
{
    /// <summary>
    /// Interaction logic for WorkflowKeyboard.xaml
    /// </summary>
    public partial class WorkflowKeyboardView : UserControl
    {
        public WorkflowKeyboardView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.DisplayWorkflowKeyboard, s =>
            {
                this.TextBoxKeyboard.Text = s;
                ThreadPool.QueueUserWorkItem(x => this.TextBoxKeyboard.Dispatcher.Invoke(() => this.TextBoxKeyboard.Focus()));
                this.TextBoxKeyboard.Focus();
            });
        }

        private void Keyboard_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.Keyboard.EnterSelected)
            {
                this.Keyboard.EnterSelected = false;
                Messenger.Default.Send("Forward", Token.EnterSelected);
            }
        }
    }
}


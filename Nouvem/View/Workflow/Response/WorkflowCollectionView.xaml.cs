﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowCollectionView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;

namespace Nouvem.View.Workflow.Response
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WorkflowCollectionView.xaml
    /// </summary>
    public partial class WorkflowCollectionView : UserControl
    {
        public WorkflowCollectionView()
        {
            this.InitializeComponent();
        }
    }
}

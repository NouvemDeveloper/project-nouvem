﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowSearchView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using DevExpress.Xpf.Grid;

namespace Nouvem.View.Workflow
{
    using System.IO;
    using System.Windows;
    using DevExpress.Xpf.Printing;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for WorkflowSearchView.xaml
    /// </summary>
    public partial class WorkflowSearchView : Window
    {
        public WorkflowSearchView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.WorkflowSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.GridControlSearchData.View.ShowSearchPanel(true);

            var gridPath = Settings.Default.WorkflowSearchGrid.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += (sender, args) =>
            {
                WindowSettings.WorkflowSearchHeight = this.Height;
                WindowSettings.WorkflowSearchLeft = this.Left;
                WindowSettings.WorkflowSearchTop = this.Top;
                WindowSettings.WorkflowSearchWidth = this.Width;
                WindowSettings.WorkflowSearchMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.Show();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowHandle = this.TableView.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlSearchData.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlSearchData.CurrentColumn == this.GridControlSearchData.Columns["PopUpNote"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowDesktopNotesWindow);
                }
            }
        }
    }
}

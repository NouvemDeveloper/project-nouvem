﻿// -----------------------------------------------------------------------
// <copyright file="AlertUsersSetUpView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Workflow
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for AlertUsersSetUpView.xaml
    /// </summary>
    public partial class AlertUsersSetUpView : Window
    {
        public AlertUsersSetUpView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.AlertUsersSetUpMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseAlertUserSetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.Closing += (sender, args) =>
            {
                WindowSettings.AlertUsersSetUpHeight = this.Height;
                WindowSettings.AlertUsersSetUpLeft = this.Left;
                WindowSettings.AlertUsersSetUpTop = this.Top;
                WindowSettings.AlertUsersSetUpWidth = this.Width;
                WindowSettings.AlertUsersSetUpMaximised = this.WindowState.ToBool();
                Messenger.Default.Unregister(this);
            };
        }
    }
}

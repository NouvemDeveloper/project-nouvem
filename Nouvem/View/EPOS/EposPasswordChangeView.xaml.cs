﻿// -----------------------------------------------------------------------
// <copyright file="EposPasswordChangeView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Properties;

namespace Nouvem.View.EPOS
{
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for EposPasswordChangeView.xaml
    /// </summary>
    public partial class EposPasswordChangeView : UserControl
    {
        public EposPasswordChangeView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.PasswordBoxEditPassword.Dispatcher.Invoke(() => this.PasswordBoxEditPassword.Focus()));

            Messenger.Default.Register<string>(this, Token.MovePasswordFocus, s =>
            {
                if (s.Equals(Constant.Up))
                {
                    this.PasswordBoxEditPassword.Focus();
                    return;
                }

                this.PasswordBoxEditConfirmPassword.Focus();
            });

            this.Unloaded += (sender, args) => Messenger.Default.Send(Token.Message, Token.CloseUserChangePasswordWindow);
        }
    }
}

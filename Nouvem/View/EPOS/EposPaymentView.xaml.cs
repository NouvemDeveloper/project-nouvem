﻿// -----------------------------------------------------------------------
// <copyright file="EposPaymentView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EposPaymentView.xaml
    /// </summary>
    public partial class EposPaymentView : UserControl
    {
        public EposPaymentView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="EposUnpaidSalesView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EposUnpaidSalesView.xaml
    /// </summary>
    public partial class EposUnpaidSalesView : UserControl
    {
        public EposUnpaidSalesView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="EposView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EposView.xaml
    /// </summary>
    public partial class EposView : UserControl
    {
        public EposView()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.TableViewProducts.ExportToXlsx(@"c:\Nouvem\grid_export.xlsx");
        }
    }
}

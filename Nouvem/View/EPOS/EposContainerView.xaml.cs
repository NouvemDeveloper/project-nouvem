﻿// -----------------------------------------------------------------------
// <copyright file="EposContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for EposContainerView.xaml
    /// </summary>
    public partial class EposContainerView : Window
    {
        public EposContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseEposWindow, x => this.Close());
        
            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="EposStockAdjustView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for EposStockAdjustView.xaml
    /// </summary>
    public partial class EposStockAdjustView : UserControl
    {
        public EposStockAdjustView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<bool>(this, Token.DisplayKeyboard, b =>
            {
                this.Keyboard.IsOpen = b;

                if (b)
                {
                    this.TextBoxStockAmount.Focus();
                }
                else
                {
                    this.ButtonStockAdjust.Focus();
                }
            });

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxStockAmount.Focus());}
    }
}

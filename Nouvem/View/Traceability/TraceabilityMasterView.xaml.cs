﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityMasterContainerView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.Traceability
{
    using System.Windows.Controls;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TraceabilityMasterView.xaml
    /// </summary>
    public partial class TraceabilityMasterView : UserControl
    {
        public TraceabilityMasterView()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) =>
            {
                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlMaster.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlMaster.CurrentColumn == this.GridControlMaster.Columns["SQL"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowSqlWindow);
                }
                else if (this.GridControlMaster.CurrentColumn == this.GridControlMaster.Columns["Collection"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowColWindow);
                }
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityTemplateAllocationView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Traceability
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for TraceabilityTemplateAllocationView.xaml
    /// </summary>
    public partial class TraceabilityTemplateAllocationView : UserControl
    {
        public TraceabilityTemplateAllocationView()
        {
            this.InitializeComponent();
        }
    }
}

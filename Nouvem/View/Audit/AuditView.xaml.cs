﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
// -----------------------------------------------------------------------
// <copyright file="AuditView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;

namespace Nouvem.View.Audit
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for AuditView.xaml
    /// </summary>
    public partial class AuditView : UserControl
    {
        public AuditView()
        {
            this.InitializeComponent();

            this.Unloaded += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlAudit.View.SearchString = string.Empty;
                this.GridControlAudit.SaveLayoutToXml(Settings.Default.AuditGridPath);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.AuditGridPath))
                {
                    this.GridControlAudit.RestoreLayoutFromXml(Settings.Default.AuditGridPath);
                }
            };
        }
    }
}

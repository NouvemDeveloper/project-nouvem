﻿// -----------------------------------------------------------------------
// <copyright file="ContainerContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Container
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using DevExpress.Xpf.Printing;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ContainerContainerView.xaml
    /// </summary>
    public partial class ContainerContainerView : Window
    {
        public ContainerContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableView);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.ShowDialog();
                }
                catch
                {
                }
            });

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

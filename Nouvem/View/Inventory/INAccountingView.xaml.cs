﻿// -----------------------------------------------------------------------
// <copyright file="INAccountingView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for INAccountingView.xaml
    /// </summary>
    public partial class INAccountingView : UserControl
    {
        public INAccountingView()
        {
            this.InitializeComponent();
        }
    }
}

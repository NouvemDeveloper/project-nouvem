﻿// -----------------------------------------------------------------------
// <copyright file="INLabelDetailsView .cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for INLabelDetailsView.xaml
    /// </summary>
    public partial class INLabelDetailsView : UserControl
    {
        public INLabelDetailsView()
        {
            this.InitializeComponent();
        }
    }
}

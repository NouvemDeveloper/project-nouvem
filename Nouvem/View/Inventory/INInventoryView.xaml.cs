﻿// -----------------------------------------------------------------------
// <copyright file="INInventoryView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.Inventory
{
    using System;
    using System.IO;
    using System.Windows.Controls;
    using DevExpress.Data;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for INInventory.xaml
    /// </summary>
    public partial class INInventoryView : UserControl
    {
        public INInventoryView()
        {
            this.InitializeComponent();
            this.ColumnBatch.Visible = true;
            this.ColumnWgt.Visible = true;
            this.ColumnQty.Visible = true;
            this.ColumnOpenWgt.Visible = true;
            this.ColumnClosingWgt.Visible = true;
            this.ColumnAllocatedWgt.Visible = true;
            this.ColumnDueInWgt.Visible = true;
            this.ColumnOpenQty.Visible = true;
            this.ColumnClosingQty.Visible = true;
            this.ColumnAllocatedQty.Visible = true;
            this.ColumnDueInQty.Visible = true;
            Messenger.Default.Register<int?>(this, Token.StockModeChange, i =>
            {
                if (i.IsNullOrZeroOrOne() || i == 6)
                {
                    this.ColumnBatch.Visible = true;
                    //this.ColumnBatchReference.Visible = true;
                    this.ColumnWgt.Visible = true;
                    this.ColumnQty.Visible = true;
                    this.ColumnOpenWgt.Visible = true;
                    this.ColumnClosingWgt.Visible = true;
                    this.ColumnAllocatedWgt.Visible = true;
                    this.ColumnDueInWgt.Visible = true;
                    this.ColumnOpenQty.Visible = true;
                    this.ColumnClosingQty.Visible = true;
                    this.ColumnAllocatedQty.Visible = true;
                    this.ColumnDueInQty.Visible = true;
                    //this.ColumnCommittedWgt.Visible = true;
                    //this.ColumnAvailableWgt.Visible = true;
                    //this.ColumnCommittedQty.Visible = true;
                    //this.ColumnAvailableQty.Visible = true;
                }
                else if (i == 4)
                {
                    this.ColumnBatch.Visible = true;
                    //this.ColumnBatchReference.Visible = true;
                    this.ColumnWgt.Visible = false;
                    this.ColumnQty.Visible = true;
                    this.ColumnOpenWgt.Visible = false;
                    this.ColumnClosingWgt.Visible = false;
                    this.ColumnAllocatedWgt.Visible = false;
                    this.ColumnDueInWgt.Visible = false;
                    this.ColumnOpenQty.Visible = true;
                    this.ColumnClosingQty.Visible = true;
                    this.ColumnAllocatedQty.Visible = true;
                    this.ColumnDueInQty.Visible = true;
                    //this.ColumnCommittedWgt.Visible = false;
                    //this.ColumnAvailableWgt.Visible = false;
                    //this.ColumnCommittedQty.Visible = true;
                    //this.ColumnAvailableQty.Visible = true;
                }
                else if (i == 5)
                {
                    this.ColumnBatch.Visible = true;
                    //this.ColumnBatchReference.Visible = true;
                    this.ColumnWgt.Visible = true;
                    this.ColumnQty.Visible = false;
                    this.ColumnOpenWgt.Visible = true;
                    this.ColumnClosingWgt.Visible = true;
                    this.ColumnAllocatedWgt.Visible = true;
                    this.ColumnDueInWgt.Visible = true;
                    this.ColumnOpenQty.Visible = false;
                    this.ColumnClosingQty.Visible = false;
                    this.ColumnAllocatedQty.Visible = false;
                    this.ColumnDueInQty.Visible = false;
                    //this.ColumnCommittedWgt.Visible = true;
                    //this.ColumnAvailableWgt.Visible = true;
                    //this.ColumnCommittedQty.Visible = false;
                    //this.ColumnAvailableQty.Visible = false;
                }
                else if (i == 7)
                {
                    this.ColumnBatch.Visible = false;
                    //this.ColumnBatchReference.Visible = false;
                    this.ColumnWgt.Visible = false;
                    this.ColumnQty.Visible = true;
                    this.ColumnOpenWgt.Visible = false;
                    this.ColumnClosingWgt.Visible = false;
                    this.ColumnAllocatedWgt.Visible = false;
                    this.ColumnDueInWgt.Visible = false;
                    this.ColumnOpenQty.Visible = true;
                    this.ColumnClosingQty.Visible = true;
                    this.ColumnAllocatedQty.Visible = true;
                    this.ColumnDueInQty.Visible = true;
                    //this.ColumnCommittedWgt.Visible = false;
                    //this.ColumnAvailableWgt.Visible = false;
                    //this.ColumnCommittedQty.Visible = true;
                    //this.ColumnAvailableQty.Visible = true;
                }
                else if (i == 8)
                {
                    this.ColumnBatch.Visible = false;
                    //this.ColumnBatchReference.Visible = false;
                    this.ColumnWgt.Visible = true;
                    this.ColumnQty.Visible = false;
                    this.ColumnOpenWgt.Visible = true;
                    this.ColumnClosingWgt.Visible = true;
                    this.ColumnAllocatedWgt.Visible = true;
                    this.ColumnDueInWgt.Visible = true;
                    this.ColumnOpenQty.Visible = false;
                    this.ColumnClosingQty.Visible = false;
                    this.ColumnAllocatedQty.Visible = false;
                    this.ColumnDueInQty.Visible = false;
                    //this.ColumnCommittedWgt.Visible = true;
                    //this.ColumnAvailableWgt.Visible = true;
                    //this.ColumnCommittedQty.Visible = false;
                    //this.ColumnAvailableQty.Visible = false;
                }
                else
                {
                    this.ColumnBatch.Visible = false;
                    //this.ColumnBatchReference.Visible = false;
                    this.ColumnWgt.Visible = true;
                    this.ColumnQty.Visible = true;
                    this.ColumnOpenWgt.Visible = true;
                    this.ColumnClosingWgt.Visible = true;
                    this.ColumnAllocatedWgt.Visible = true;
                    this.ColumnDueInWgt.Visible = true;
                    this.ColumnOpenQty.Visible = true;
                    this.ColumnClosingQty.Visible = true;
                    this.ColumnAllocatedQty.Visible = true;
                    this.ColumnDueInQty.Visible = true;
                    //this.ColumnCommittedWgt.Visible = true;
                    //this.ColumnAvailableWgt.Visible = true;
                    //this.ColumnCommittedQty.Visible = true;
                    //this.ColumnAvailableQty.Visible = true;
                }
            });

            this.Loaded += (sender, args) =>
            {
                //if (File.Exists(Settings.Default.ProductStockGridPath))
                //{
                //    this.GridControlInventory.RestoreLayoutFromXml(Settings.Default.ProductStockGridPath);
                //}

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewProduct);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) =>
            {
                //this.GridControlInventory.SaveLayoutToXml(Settings.Default.ProductStockGridPath);
                Messenger.Default.Unregister(this);
            };
        }

        private void GridControlInventory_CustomSummary(object sender, CustomSummaryEventArgs e)
        {
            try
            {
                if (e.IsTotalSummary)
                {
                    if (((GridSummaryItem)e.Item).ShowInColumn == "CommittedWgt")
                    {
                        e.TotalValue = ProductStockData.CommittedStockWgt;
                        return;
                    }

                    if (((GridSummaryItem)e.Item).ShowInColumn == "CommittedQty")
                    {
                        e.TotalValue = ProductStockData.CommittedStockQty;
                        return;
                    }

                    if (((GridSummaryItem)e.Item).ShowInColumn == "AvailableWgt")
                    {
                        //var weight = this.GridControlInventory.GetTotalSummaryValue(this.GridSummaryItemWgt);
                        //if (weight != null)
                        //{
                        //    e.TotalValue = (decimal)weight - ProductStockData.CommittedStockWgt.ToDecimal();
                        //}

                        e.TotalValue = ProductStockData.StockWgt - ProductStockData.CommittedStockWgt.ToDecimal();
                    
                        return;
                    }

                    if (((GridSummaryItem)e.Item).ShowInColumn == "AvailableQty")
                    {
                        //var weight = this.GridControlInventory.GetTotalSummaryValue(this.GridSummaryItemQty);
                        //if (weight != null)
                        //{
                        //    e.TotalValue = (decimal)weight - ProductStockData.CommittedStockQty.ToDecimal();
                        //}

                        e.TotalValue = ProductStockData.StockQty - ProductStockData.CommittedStockQty.ToDecimal();
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="INRemarks.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for INRemarks.xaml
    /// </summary>
    public partial class INRemarks : UserControl
    {
        public INRemarks()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ItemMasterPropertySetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ItemMasterPropertySetUpView.xaml
    /// </summary>
    public partial class ItemMasterPropertySetUpView : UserControl
    {
        public ItemMasterPropertySetUpView()
        {
            this.InitializeComponent();
        }
    }
}

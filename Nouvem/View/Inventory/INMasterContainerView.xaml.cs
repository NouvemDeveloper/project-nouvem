﻿// -----------------------------------------------------------------------
// <copyright file="INMasterContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Inventory
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for INMasterContainerView.xaml
    /// </summary>
    public partial class INMasterContainerView : Window
    {
        public INMasterContainerView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.INMasterMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseINMasterWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.Closing += (sender, args) =>
            {
                WindowSettings.INMasterHeight = this.Height;
                WindowSettings.INMasterLeft = this.Left;
                WindowSettings.INMasterTop = this.Top;
                WindowSettings.INMasterWidth = this.Width;
                WindowSettings.INMasterMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                this.Topmost = true;
                System.Threading.Thread.Sleep(100);
                this.Topmost = false;
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


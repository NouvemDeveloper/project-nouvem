﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Inventory;

namespace Nouvem.View.Inventory
{
    /// <summary>
    /// Interaction logic for TouchscreenSpecsView.xaml
    /// </summary>
    public partial class TouchscreenSpecsView : Window
    {
        public TouchscreenSpecsView()
        {
            InitializeComponent();
            this.DataContext = new SpecsViewModel();
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSpecWindow, x => this.Close());
            this.Closing += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
            };
        }
    }
}

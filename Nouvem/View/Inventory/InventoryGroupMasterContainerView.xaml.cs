﻿// <copyright file="BPMasterWindowView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Inventory
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for InventoryGroupMasterContainerView.xaml
    /// </summary>
    public partial class InventoryGroupMasterContainerView : Window
    {
        public InventoryGroupMasterContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseInventoryGroupMasterWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.INGroupMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {

                WindowSettings.INGroupHeight = this.Height;
                WindowSettings.INGroupLeft = this.Left;
                WindowSettings.INGroupTop = this.Top;
                WindowSettings.INGroupWidth = this.Width;
                WindowSettings.INGroupMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
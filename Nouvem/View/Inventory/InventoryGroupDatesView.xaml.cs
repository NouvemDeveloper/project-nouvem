﻿// <copyright file="InventoryGroupDatesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for InventoryGroupDatesView.xaml
    /// </summary>
    public partial class InventoryGroupDatesView : UserControl
    {
        public InventoryGroupDatesView()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) =>
            {
                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}

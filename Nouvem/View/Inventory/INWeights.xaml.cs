﻿// -----------------------------------------------------------------------
// <copyright file="INWeights.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for INWeights.xaml
    /// </summary>
    public partial class INWeights : UserControl
    {
        public INWeights()
        {
            this.InitializeComponent();
        }
    }
}

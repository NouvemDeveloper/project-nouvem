﻿// -----------------------------------------------------------------------
// <copyright file="TransactionTraceabilitiesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Transaction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Logging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.DataLayer.Repository;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for TouchscreenTraceabilityView.xaml
    /// </summary>
    public partial class TransactionTraceabilitiesView : UserControl
    {
        #region field

        /// <summary>
        /// The batch control contents.
        /// </summary>
        private static IList<StackPanel> BatchContentControls = new List<StackPanel>();

        /// <summary>
        /// The traceability data holder.
        /// </summary>
        private IList<Tuple<string, bool, TraceabilityData>> traceabilityData = new List<Tuple<string, bool, TraceabilityData>>();

        /// <summary>
        /// The logging reference.
        /// </summary>
        private ILogger log = new Logger();

        /// <summary>
        /// The current traceability combo box.
        /// </summary>
        private static ComboBox selectedComboBox;

        /// <summary>
        /// The current traceability text box.
        /// </summary>
        private static TextBox selectedTextBox;

        /// <summary>
        /// The data respository.
        /// </summary>
        private IDynamicRepository repository = new DynamicRepository();

        /// <summary>
        /// The current batch number.
        /// </summary>
        private string batchNo;

        /// <summary>
        /// The free text box control count.
        /// </summary>
        private int freeTextCount = 1;

        /// <summary>
        /// The collection control count.
        /// </summary>
        private int dataCount = 1;

        /// <summary>
        /// The date picker control count.
        /// </summary>
        private int dateCount = 1;

        #endregion

        #region constructor

        public TransactionTraceabilitiesView()
        {
            this.InitializeComponent();

            // Register for a request to send the current items traceability data.
            Messenger.Default.Register<string>(this, Token.GetEditTraceabilityData, s => this.GetTraceabilityData());

            // Register for the incoming batch traceability data.
            Messenger.Default.Register<List<TraceabilityData>>(this, Token.CreateEditDynamicControls, this.CreateBatchControls);

            // Register for the incoming serial traceability data.
            Messenger.Default.Register<List<TraceabilityData>>(this, Token.CreateEditDynamicSerialControls, this.CreateSerialControls);

            this.Unloaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
            };
        }

        #endregion

        #region private

        #region create controls

        #region batch

        /// <summary>
        /// Creates the dynamic serial traceability controls.
        /// </summary>
        /// <param name="traceData">The control data.</param>
        private void CreateBatchControls(List<TraceabilityData> traceData)
        {
            this.TabItemBatchTraceability.Visibility = Visibility.Collapsed;
            this.TabItemBatchQuality.Visibility = Visibility.Collapsed;
            this.TabItemBatchDates.Visibility = Visibility.Collapsed;

            this.log.LogDebug(this.GetType(), "CreateBatchControls(): Creating batch controls.");
            
            this.TabControlTraceability.SelectedIndex = 0;
            this.batchNo = traceData.First().BatchNumber.Number;
            var dataGroups = traceData.GroupBy(x => x.TraceabilityMethod);

            foreach (var dataGroup in dataGroups)
            {
                var traceabilityMethod = dataGroup.Key;
                var masterName = string.Format("StackPanelBatch{0}{1}", traceabilityMethod, this.batchNo);
                this.log.LogDebug(this.GetType(), string.Format("Master Name:{0}", masterName));

                var masterStackPanel = new StackPanel { Name = masterName, Orientation = Orientation.Horizontal };
                var stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                var controlCount = 0;

                foreach (var data in dataGroup)
                {
                    var description = string.Format("{0}:", data.TraceabilityMasterDescription);
                    this.log.LogDebug(this.GetType(), string.Format("Traceability method:{0}", description));

                    if (data.TraceabilityType.Equals(Constant.ManualEntry))
                    {
                        var value = data.TraceabilityValue;

                        // free text
                        var controlName = string.Format("TextBox{0}", this.freeTextCount).Replace(" ", "");
                        var textBox = new TextBox { Name = controlName, Width = 140, Height = 30, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                  
                        if (!string.IsNullOrEmpty(value))
                        {
                            textBox.Text = value;
                        }

                        this.RegisterName(controlName, textBox);

                        var stackPanelFreeText = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelFreeText.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelFreeText.Children.Add(textBox);

                        stackPanel.Children.Add(stackPanelFreeText);

                        this.traceabilityData.Add(Tuple.Create(controlName, true, data));
                        this.freeTextCount++;
                    }
                    else if (data.TraceabilityType.Equals(Constant.Calender))
                    {
                        // datetime
                        var datePicker = new DatePicker { SelectedDate = DateTime.Today, Width = 140, Height = 30, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                        }

                        var controlName = string.Format("DatePicker{0}", this.dateCount).Replace(" ", "");
                        this.RegisterName(controlName, datePicker);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelCalender.Children.Add(datePicker);
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, true, data));
                        this.dateCount++;
                    }
                    else if (data.TraceabilityType.Equals(Constant.Collection))
                    {
                        // collection
                        var collectionData = data.TraceabilityMasterCollection.Split(',')
                            .Select(x => x.ToString())
                            .ToList();

                        var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                        var combo = new ComboBox { Name = controlName, Width = 140, Height = 30, ItemsSource = collectionData, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
             
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            combo.Text = data.TraceabilityValue;
                        }

                        this.RegisterName(controlName, combo);
                      
                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelCalender.Children.Add(combo);
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, true, data));
                        this.dataCount++;
                    }
                    else
                    {
                        // sql
                        var script = data.TraceabilityMasterSQL;
                        var commandData = this.repository.GetData(script).ToList().Select(x => x.ToString());

                        var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                        var combo = new ComboBox { Name = controlName, ItemsSource = commandData, Width = 140, Height = 30, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                     
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            combo.Text = data.TraceabilityValue;
                        }

                        this.RegisterName(controlName, combo);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelCalender.Children.Add(combo);
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, true, data));
                        this.dataCount++;
                    }

                    controlCount++;
                    if (controlCount == 2)
                    {
                        masterStackPanel.Children.Add(stackPanel);
                        stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                        controlCount = 0;
                    }
                }

                if (controlCount > 0)
                {
                    masterStackPanel.Children.Add(stackPanel);
                }

                //this.RegisterName(masterName, masterStackPanel);

                if (traceabilityMethod.Equals(Constant.Traceability))
                {
                    this.ContentControlTraceability.Content = masterStackPanel;
                    this.TabItemBatchTraceability.Visibility = Visibility.Visible;
                }
                else if (traceabilityMethod.Equals(Constant.Quality))
                {
                    this.ContentControlQuality.Content = masterStackPanel;
                    this.TabItemBatchQuality.Visibility = Visibility.Visible;
                }
                else
                {
                    this.ContentControlDates.Content = masterStackPanel;
                    this.TabItemBatchDates.Visibility = Visibility.Visible;
                }

                BatchContentControls.Add(masterStackPanel);
            }
        }

        #endregion

        #region serial

        /// <summary>
        /// Creates the dynamic serial traceability controls.
        /// </summary>
        /// <param name="traceData">The control data.</param>
        private void CreateSerialControls(List<TraceabilityData> traceData)
        {
            this.log.LogDebug(this.GetType(), "CreateSerialControls(): Creating serial controls.");
            this.TabItemSerialTraceability.Visibility = Visibility.Collapsed;
            this.TabItemSerialQuality.Visibility = Visibility.Collapsed;
            this.TabItemSerialDates.Visibility = Visibility.Collapsed;

            var batchNo = traceData.First().BatchNumber.Number;

            var dataGroups = traceData.GroupBy(x => x.TraceabilityMethod);

            var freeTextCounter = 1;
            var dataCounter = 1;
            var dateCounter = 1;
            var collectionCounter = 1;

            foreach (var dataGroup in dataGroups)
            {
                var traceabilityMethod = dataGroup.Key;
                var masterName = string.Format("StackPanelSerial{0}{1}", traceabilityMethod, batchNo);
                this.log.LogDebug(this.GetType(), string.Format("Master Name:{0}", masterName));

                var masterStackPanel = new StackPanel { Name = masterName, Orientation = Orientation.Horizontal };
                var stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                var controlCount = 0;

                foreach (var data in dataGroup)
                {
                    var description = string.Format("{0}:", data.TraceabilityMasterDescription);
                    this.log.LogDebug(this.GetType(), string.Format("Traceability method:{0}", description));

                    if (data.TraceabilityType.Equals(Constant.ManualEntry))
                    {
                        var value = data.TraceabilityValue;

                        // free text
                        var controlName = string.Format("TextBox{0}", this.freeTextCount).Replace(" ", "");
                        var textBox = new TextBox { Name = controlName, Width = 140, Height = 30, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                        
                        if (!string.IsNullOrEmpty(value))
                        {
                            textBox.Text = value;
                        }

                        data.DynamicColumnName = string.Format("FreeText{0}", freeTextCounter);
                        this.RegisterName(controlName, textBox);

                        var stackPanelFreeText = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelFreeText.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelFreeText.Children.Add(textBox);

                        stackPanel.Children.Add(stackPanelFreeText);

                        this.traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.freeTextCount++;
                        freeTextCounter++;
                    }
                    else if (data.TraceabilityType.Equals(Constant.Calender))
                    {
                        // datetime
                        var datePicker = new DatePicker { SelectedDate = DateTime.Today, Width = 140, Height = 30, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                        }

                        var controlName = string.Format("DatePicker{0}", this.dateCount).Replace(" ", "");
                        data.DynamicColumnName = string.Format("Date{0}", dateCounter);
                        this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));
                        this.RegisterName(controlName, datePicker);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelCalender.Children.Add(datePicker);
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.dateCount++;
                        dateCounter++;
                    }
                    else if (data.TraceabilityType.Equals(Constant.Collection))
                    {
                        // collection
                        var collectionData = data.TraceabilityMasterCollection.Split(',')
                            .Select(x => x.ToString())
                            .ToList();

                        var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                        var combo = new ComboBox { Name = controlName, Width = 140, Height = 30, ItemsSource = collectionData, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                   
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            combo.Text = data.TraceabilityValue;
                        }

                        data.DynamicColumnName = string.Format("Collection{0}", collectionCounter);
                        this.RegisterName(controlName, combo);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelCalender.Children.Add(combo);
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.dataCount++;
                        collectionCounter++;
                    }
                    else
                    {
                        // sql
                        var script = data.TraceabilityMasterSQL;
                        var commandData = this.repository.GetData(script).ToList().Select(x => x.ToString());

                        var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                        var combo = new ComboBox { Name = controlName, ItemsSource = commandData, Width = 140, Height = 30, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 1, 0, 1) };
                   
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            combo.Text = data.TraceabilityValue;
                        }

                        data.DynamicColumnName = string.Format("Data{0}", dataCounter);
                        this.RegisterName(controlName, combo);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label { Content = description, Width = 140, Height = 30 });
                        stackPanelCalender.Children.Add(combo);
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.dataCount++;
                        dataCounter++;
                    }

                    controlCount++;

                    if (controlCount == 2)
                    {
                        masterStackPanel.Children.Add(stackPanel);
                        stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                        controlCount = 0;
                    }
                }

                if (controlCount > 0)
                {
                    masterStackPanel.Children.Add(stackPanel);
                }

                if (traceabilityMethod.Equals(Constant.Traceability))
                {
                    this.ContentControlSerialTraceability.Content = masterStackPanel;
                    this.TabItemSerialTraceability.Visibility = Visibility.Visible;
                }
                else if (traceabilityMethod.Equals(Constant.Quality))
                {
                    this.ContentControlSerialQuality.Content = masterStackPanel;
                    this.TabItemSerialQuality.Visibility = Visibility.Visible;
                }
                else
                {
                    this.ContentControlSerialDates.Content = masterStackPanel;
                    this.TabItemSerialDates.Visibility = Visibility.Visible;
                }
            }
        }

        #endregion

        #endregion

        #region get traceability data

        /// <summary>
        /// Retrieves the batch traceability data.
        /// </summary>
        private void GetTraceabilityData()
        {
            this.log.LogDebug(this.GetType(), "GetTraceabilityData(): Retrieving data");
            var error = string.Empty;
            var batchTraceabilityResults = new List<TraceabilityResult>();

            foreach (var data in this.traceabilityData)
            {
                string value;
                var controlName = data.Item1;
                var batchAttribute = data.Item2;
                var localTraceabilityData = data.Item3;
                var traceabilityId = data.Item3.TraceabilityId;

                this.log.LogDebug(this.GetType(), string.Format("Control Name:{0}", controlName));

                var control = this.FindName(controlName);
                if (control == null)
                {
                    continue;
                }

                var traceabilityMasterId = localTraceabilityData.TraceabilityMethod.Equals(Constant.Traceability)
                   ? localTraceabilityData.TraceabilityMasterID
                   : (int?)null;

                var dateMasterId = localTraceabilityData.TraceabilityMethod.Equals(Constant.Date)
                   ? localTraceabilityData.TraceabilityMasterID
                   : (int?)null;

                var qualityMasterId = localTraceabilityData.TraceabilityMethod.Equals(Constant.Quality)
                   ? localTraceabilityData.TraceabilityMasterID
                   : (int?)null;

                if (control is TextBox)
                {
                    value = (control as TextBox).Text;
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        batchTraceabilityResults.Clear();
                        error = "error";
                    }

                    var result = new TraceabilityResult
                    {
                        FieldName = batchAttribute ? controlName : localTraceabilityData.DynamicColumnName,
                        NouTraceabilityMethod = localTraceabilityData.TraceabilityMethod,
                        TraceabilityMasterID = traceabilityMasterId,
                        DatemasterID = dateMasterId,
                        QualityMasterID = qualityMasterId,
                        TraceabilityValue = value,
                        BatchAttribute = batchAttribute,
                        TraceabilityId = traceabilityId
                    };

                    batchTraceabilityResults.Add(result);
                    this.log.LogDebug(this.GetType(), string.Format("Value found:{0}", value));
                    continue;
                }

                if (control is DatePicker)
                {
                    value = (control as DatePicker).SelectedDate.ToString();
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        batchTraceabilityResults.Clear();
                        error = "error";
                    }

                    var result = new TraceabilityResult
                    {
                        FieldName = batchAttribute ? controlName : localTraceabilityData.DynamicColumnName,
                        NouTraceabilityMethod = localTraceabilityData.TraceabilityMethod,
                        TraceabilityValue = value,
                        TraceabilityMasterID = traceabilityMasterId,
                        DatemasterID = dateMasterId,
                        QualityMasterID = qualityMasterId,
                        BatchAttribute = batchAttribute,
                        TraceabilityId = traceabilityId
                    };

                    batchTraceabilityResults.Add(result);
                    this.log.LogDebug(this.GetType(), string.Format("Value found:{0}", value));
                    continue;
                }

                if (control is ComboBox)
                {
                    value = (control as ComboBox).Text;
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        batchTraceabilityResults.Clear();
                        error = "error";
                    }

                    var result = new TraceabilityResult
                    {
                        FieldName = batchAttribute ? controlName : localTraceabilityData.DynamicColumnName,
                        NouTraceabilityMethod = localTraceabilityData.TraceabilityMethod,
                        TraceabilityValue = value,
                        TraceabilityMasterID = traceabilityMasterId,
                        DatemasterID = dateMasterId,
                        QualityMasterID = qualityMasterId,
                        BatchAttribute = batchAttribute,
                        TraceabilityId = traceabilityId
                    };

                    this.log.LogDebug(this.GetType(), string.Format("Value found:{0}", value));
                    batchTraceabilityResults.Add(result);
                }
            }

            this.log.LogDebug(this.GetType(), string.Format("Sending {0} traceability results", batchTraceabilityResults.Count));
            Messenger.Default.Send(Tuple.Create(error, batchTraceabilityResults), Token.TraceabilityEditValues);
        }

        /// <summary>
        /// Retieves the stored panel.
        /// </summary>
        /// <param name="name">The panel name to search for.</param>
        /// <returns>A stored stackpanel.</returns>
        private StackPanel FindStackPanel(string name)
        {
            return BatchContentControls.FirstOrDefault(x => x.Name.Equals(name));
        }

        #endregion
    }

        #endregion
}


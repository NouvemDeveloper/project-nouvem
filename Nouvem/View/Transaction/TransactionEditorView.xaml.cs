﻿// -----------------------------------------------------------------------
// <copyright file="EditTransactionView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Transaction
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for EditTransactionView.xaml
    /// </summary>
    public partial class TransactionEditorView : Window
    {
        public TransactionEditorView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseEditTransactionWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            var gridPath = Settings.Default.TransactionEditorGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.WindowState = WindowSettings.TransactionEditorMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.TransactionEditorHeight = this.Height;
                WindowSettings.TransactionEditorLeft = this.Left;
                WindowSettings.TransactionEditorTop = this.Top;
                WindowSettings.TransactionEditorWidth = this.Width;
                WindowSettings.TransactionEditorMaximised = this.WindowState.ToBool();
                this.GridControlTransactions.FilterCriteria = null;
                this.GridControlTransactions.View.SearchString = string.Empty;
                this.GridControlTransactions.SaveLayoutToXml(gridPath);
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlTransactions.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.Show();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowHandle = this.TableView.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlTransactions.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlTransactions.CurrentColumn == this.GridControlTransactions.Columns["Notes"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowTransactionEditNotesWindow);
                }
            }
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenTransactionSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.Transaction
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenTransactionSearchDataView.xaml
    /// </summary>
    public partial class TouchscreenTransactionSearchDataView : Window
    {
        public TouchscreenTransactionSearchDataView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(Settings.Default.TouchscreenTransactionDataGridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.TouchscreenTransactionDataGridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(Settings.Default.TouchscreenTransactionDataGridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.DisplaySearchKeyboard);
        }
    }
}



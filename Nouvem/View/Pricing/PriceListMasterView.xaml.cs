﻿// -----------------------------------------------------------------------
// <copyright file="PriceListMasterView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.LabelDesigner.ViewModel;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Pricing
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for PriceListMasterView.xaml
    /// </summary>
    public partial class PriceListMasterView : UserControl
    {
        public PriceListMasterView()
        {
            this.InitializeComponent();

            var gridPath = Settings.Default.PriceListMasterPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Unloaded += (sender, args) =>
            {
                this.GridControlPriceMaster.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlPriceMaster.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }
    }
}

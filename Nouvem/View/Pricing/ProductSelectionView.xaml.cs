﻿// -----------------------------------------------------------------------
// <copyright file="ProductSelectionView " company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Scheduler.Drawing;

namespace Nouvem.View.Pricing
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ProductSelectionView.xaml
    /// </summary>
    public partial class ProductSelectionView : UserControl
    {
        public ProductSelectionView()
        {
            this.InitializeComponent();
        }

        private void CheckBoxExpand_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TreeListViewData.ExpandAllNodes();
        }

        private void CheckBoxExpand_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TreeListViewData.CollapseAllNodes();
        }
    }﻿ 
}

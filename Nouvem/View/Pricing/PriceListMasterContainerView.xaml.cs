﻿// -----------------------------------------------------------------------
// <copyright file="PriceListMasterContainerView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.Pricing
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for PriceListMasterContainerView.xaml
    /// </summary>
    public partial class PriceListMasterContainerView : Window
    {
        public PriceListMasterContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.ClosePriceListMasterWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.PriceListMasterMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.PriceListMasterPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlPriceMaster.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlPriceMaster.FilterCriteria = null;
                this.GridControlPriceMaster.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
                WindowSettings.PriceListMasterHeight = this.Height;
                WindowSettings.PriceListMasterLeft = this.Left;
                WindowSettings.PriceListMasterTop = this.Top;
                WindowSettings.PriceListMasterWidth = this.Width;
                WindowSettings.PriceListMasterMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

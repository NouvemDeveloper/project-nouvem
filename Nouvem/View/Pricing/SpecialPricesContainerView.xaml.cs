﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPricesContainerView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Pricing
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SpecialPricesContainerView.xaml
    /// </summary>
    public partial class SpecialPricesContainerView : Window
    {
        public SpecialPricesContainerView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.SpecialPricesMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseSpecialPricesWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                WindowSettings.SpecialPricesHeight = this.Height;
                WindowSettings.SpecialPricesLeft = this.Left;
                WindowSettings.SpecialPricesTop = this.Top;
                WindowSettings.SpecialPricesWidth = this.Width;
                WindowSettings.SpecialPricesMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPricesView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Pricing
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SpecialPricesView.xaml
    /// </summary>
    public partial class SpecialPricesView : UserControl
    {
        public SpecialPricesView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="PriceListMasterContainerView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Pricing
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ProductSelectionContainerView.xaml
    /// </summary>
    public partial class ProductSelectionContainerView : Window
    {
        public ProductSelectionContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseProductSelectionWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.Loaded += (sender, args) =>
            {
                this.Topmost = true;
                this.Topmost = false;
            };

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

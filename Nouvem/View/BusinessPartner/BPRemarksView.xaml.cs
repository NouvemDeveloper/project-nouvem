﻿// -----------------------------------------------------------------------
// <copyright file="BPPropertiesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BPRemarksView.xaml
    /// </summary>
    public partial class BPRemarksView : UserControl
    {
        public BPRemarksView()
        {
            this.InitializeComponent();
        }
    }
}

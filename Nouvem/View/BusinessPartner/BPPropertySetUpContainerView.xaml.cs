﻿using System;
// -----------------------------------------------------------------------
// <copyright file="BPPropertySetUpContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for BPPropertySetUpContainerView.xaml
    /// </summary>
    public partial class BPPropertySetUpContainerView : Window
    {
        public BPPropertySetUpContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseBPPropertySetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="BPPaymentTerms.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BPPaymentTerms.xaml
    /// </summary>
    public partial class BPPaymentTerms : UserControl
    {
        public BPPaymentTerms()
        {
            this.InitializeComponent();
        }
    }
}

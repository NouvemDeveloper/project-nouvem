﻿// -----------------------------------------------------------------------
// <copyright file="BPMasterView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for BPMasterView.xaml
    /// </summary>
    public partial class BPMasterView : UserControl
    {
        public BPMasterView()
        {
            this.InitializeComponent();
           
            Messenger.Default.Register<string>(this, Token.FocusToDataUpdate, x => this.TextBoxCode.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedPartnerControl, x => this.ComboBoxPartnerCode.Focus());
            ThreadPool.QueueUserWorkItem(x => this.TextBoxCode.Dispatcher.Invoke(() => this.ComboBoxPartnerCode.Focus()));
        }
    }
}

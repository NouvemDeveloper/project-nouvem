﻿// -----------------------------------------------------------------------
// <copyright file="BPGroupSetUpContainerView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for BPGroupSetUpContainerView .xaml
    /// </summary>
    public partial class BPGroupSetUpContainerView : Window
    {
        public BPGroupSetUpContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseBPGroupSetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
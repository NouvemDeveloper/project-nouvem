﻿// -----------------------------------------------------------------------
// <copyright file="BPLabelDetails.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BPLabelDetails.xaml
    /// </summary>
    public partial class BPLabelDetails : UserControl
    {
        public BPLabelDetails()
        {
            this.InitializeComponent();
        }
    }
}

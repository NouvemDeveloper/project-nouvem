﻿// -----------------------------------------------------------------------
// <copyright file="BPGeneralView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BPGeneralView.xaml
    /// </summary>
    public partial class BPGeneralView : UserControl
    {
        public BPGeneralView()
        {
            this.InitializeComponent();
        }
    }
}

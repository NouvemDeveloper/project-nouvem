﻿// -----------------------------------------------------------------------
// <copyright file="BPAttachmentsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for BPAttachmentsView.xaml
    /// </summary>
    public partial class BPAttachmentsView : UserControl
    {
        public BPAttachmentsView()
        {
            this.InitializeComponent();
            this.Unloaded += (sender, args) => this.GridControlAttachments.SaveLayoutToXml(Settings.Default.AttachmentsGridPath);
            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.AttachmentsGridPath))
                {
                    this.GridControlAttachments.RestoreLayoutFromXml(Settings.Default.AttachmentsGridPath);
                }
            };
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="BPPropertySetUpContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.BusinessPartner
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BPPropertySetUpView.xaml
    /// </summary>
    public partial class BPPropertySetUpView : UserControl
    {
        public BPPropertySetUpView()
        {
            this.InitializeComponent();
        }
    }
}

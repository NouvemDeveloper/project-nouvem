﻿// -----------------------------------------------------------------------
// <copyright file="BPAddressesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.BusinessPartner
{
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for BPAddressesView.xaml
    /// </summary>
    public partial class BPAddressesView : UserControl
    {
        public BPAddressesView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxAddressOne.Focus());
            ThreadPool.QueueUserWorkItem(x => this.TextBoxAddressOne.Dispatcher.Invoke(() => this.TextBoxAddressOne.Focus()));

            this.Loaded += (sender, args) =>
            {
                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}

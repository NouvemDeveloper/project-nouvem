﻿// -----------------------------------------------------------------------
// <copyright file="MasterModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Windows.Controls.Ribbon;
using Nouvem.BusinessLogic;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Master
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for MasterView.xaml
    /// </summary>
    public partial class MasterView 
    {
        public MasterView()
        {
            this.InitializeComponent();
            Messenger.Default.Send(Token.Message, Token.CloseWindow);
            this.SetUserShortcuts(NouvemGlobal.LoggedInUser);
            //this.ShortCutReport1.Label = "Test";
            //this.ShortCutReport1.CommandParameter = "Business Partner Report";

            //RibbonButton btn = new RibbonButton();
            //btn.Name = "btnBrowse";
            //TextBlock btnText = new TextBlock();
            //btnText.Text = "Browse";
            //btn.Content = btnText;

            //btn.Click += this.Btn_Click;
            //this.GroupShortCuts.Items.Add(btn);

            #region shortcuts

            Messenger.Default.Register<Tuple<ApplicationMenu,bool>>(this, Token.AddMenuToShortcut, o =>
            {
                var s = o.Item1;
                var action = o.Item2;
                var localAction = Visibility.Visible;
                if (!action)
                {
                    localAction = Visibility.Collapsed;
                }

                if (s.IsReport)
                {
                    if (!s.IsHistoric.ToBool())
                    {
                        if (action)
                        {
                            if (this.ShortCutReport1.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport1.Visibility = localAction;
                                this.ShortCutReport1.Tag = s.DisplayName;
                                this.ShortCutReport1.Label = s.TreeNodeText;
                                this.ShortCutReport1.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport1.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport2.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport2.Visibility = localAction;
                                this.ShortCutReport2.Tag = s.DisplayName;
                                this.ShortCutReport2.Label = s.TreeNodeText;
                                this.ShortCutReport2.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport2.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport3.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport3.Visibility = localAction;
                                this.ShortCutReport3.Tag = s.DisplayName;
                                this.ShortCutReport3.Label = s.TreeNodeText;
                                this.ShortCutReport3.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport3.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport4.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport4.Visibility = localAction;
                                this.ShortCutReport4.Tag = s.DisplayName;
                                this.ShortCutReport4.Label = s.TreeNodeText;
                                this.ShortCutReport4.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport4.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport5.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport5.Visibility = localAction;
                                this.ShortCutReport5.Tag = s.DisplayName;
                                this.ShortCutReport5.Label = s.TreeNodeText;
                                this.ShortCutReport5.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport5.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport6.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport6.Visibility = localAction;
                                this.ShortCutReport6.Tag = s.DisplayName;
                                this.ShortCutReport6.Label = s.TreeNodeText;
                                this.ShortCutReport6.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport6.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport7.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport7.Visibility = localAction;
                                this.ShortCutReport7.Tag = s.DisplayName;
                                this.ShortCutReport7.Label = s.TreeNodeText;
                                this.ShortCutReport7.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport7.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport8.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport8.Visibility = localAction;
                                this.ShortCutReport8.Tag = s.DisplayName;
                                this.ShortCutReport8.Label = s.TreeNodeText;
                                this.ShortCutReport8.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport8.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport9.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport9.Visibility = localAction;
                                this.ShortCutReport9.Tag = s.DisplayName;
                                this.ShortCutReport9.Label = s.TreeNodeText;
                                this.ShortCutReport9.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport9.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }
                            else if (this.ShortCutReport10.Visibility == Visibility.Collapsed)
                            {
                                this.ShortCutReport10.Visibility = localAction;
                                this.ShortCutReport10.Tag = s.DisplayName;
                                this.ShortCutReport10.Label = s.TreeNodeText;
                                this.ShortCutReport10.CommandParameter = s.DisplayName;
                                var reports = ReportManager.Instance.Reports;
                                if (reports != null)
                                {
                                    var localReport =
                                        reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(s.TreeNodeText));
                                    if (localReport != null)
                                    {
                                        this.ShortCutReport10.ToolTipDescription = localReport.ReportID.ToString();
                                    }
                                }
                            }

                            RibbonButton btn = new RibbonButton();
                            btn.Name = "btnBrowse";
                            TextBlock btnText = new TextBlock();
                            btnText.Text = "Browse";
                            btn.Content = btnText;

                            btn.Click += this.Btn_Click;
                            this.GroupShortCuts.Items.Add(btn);
                        }
                        else
                        {
                            var userShortcut =
                                NouvemGlobal.LoggedInUser.UserShortcuts.FirstOrDefault(x =>
                                    x.Name.CompareIgnoringCase(s.DisplayName));
                            if (userShortcut != null)
                            {
                                var controlId = userShortcut.ControlID;
                                if (controlId == 1)
                                {
                                    this.ShortCutReport1.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 2)
                                {
                                    this.ShortCutReport2.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 3)
                                {
                                    this.ShortCutReport3.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 4)
                                {
                                    this.ShortCutReport4.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 5)
                                {
                                    this.ShortCutReport5.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 6)
                                {
                                    this.ShortCutReport6.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 7)
                                {
                                    this.ShortCutReport7.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 8)
                                {
                                    this.ShortCutReport8.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 9)
                                {
                                    this.ShortCutReport9.Visibility = Visibility.Collapsed;
                                }
                                else if (controlId == 10)
                                {
                                    this.ShortCutReport10.Visibility = Visibility.Collapsed;
                                }
                            }
                        }
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Yield Detail"))
                    {
                        this.ShortCutReportBatchYieldDetail.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Costings"))
                    {
                        this.ShortCutReportBatchCostings.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Beef Kill Report"))
                    {
                        this.ShortCutReportBeefKill.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Carcass Dispatch Export"))
                    {
                        this.ShortCutReportCarcassDispatchExport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Cheque Payment Report"))
                    {
                        this.ShortCutReportChequePaymentReport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Daily Kill Summary"))
                    {
                        this.ShortCutReportDailyKillSummary.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("End Of Day Report"))
                    {
                        this.ShortCutReportEndOfDay.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Kill report By Supplier"))
                    {
                        this.ShortCutReportKillReportBySupplier.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Kill Report ScotBeef"))
                    {
                        this.ShortCutReportKillReportScotBeef.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Kill Report"))
                    {
                        this.ShortCutReportKillReport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Label Kill Information"))
                    {
                        this.ShortCutReportLabelKillInformation.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("RPA Export"))
                    {
                        this.ShortCutReportRPAExport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Stock Trace Report"))
                    {
                        this.ShortCutReportStockTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Batch Details Report"))
                    {
                        this.ShortCutReportWoolleysBatchDetails.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Batch Details Trace Report"))
                    {
                        this.ShortCutReportWoolleysBatchDetailsTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Batch Yield"))
                    {
                        this.ShortCutReportWoolleysBatchYield.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Carcass Export"))
                    {
                        this.ShortCutReportWoolleysCarcassExport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Dispatch Docket Detail"))
                    {
                        this.ShortCutReportWoolleysDispatchDocketDetail.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Dispatch Docket"))
                    {
                        this.ShortCutReportWoolleysDispatchDocket.Visibility = localAction;
                    }else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Invoice"))
                    {
                        this.ShortCutReportWoolleysInvoice.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Stock Report(Location)"))
                    {
                        this.ShortCutReportWoolleysStockReportLocation.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Woolleys - Stock Take"))
                    {
                        this.ShortCutReportWoolleysStockTake.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Yield (Priced)"))
                    {
                        this.ShortCutReportBatchYieldPriced.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Yield"))
                    {
                        this.ShortCutReportBatchYield.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Price List (Product Group)"))
                    {
                        this.ShortCutReportPriceListProductGroup.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Details Trace"))
                    {
                        this.ShortCutReportBatchDetailsTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Details"))
                    {
                        this.ShortCutReportBatchDetails.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Detail"))
                    {
                        this.ShortCutReportBatchDetail.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Label Association"))
                    {
                        this.ShortCutReportLabelAssociation.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Deleted Label"))
                    {
                        this.ShortCutReportDeletedLabel.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Input"))
                    {
                        this.ShortCutReportBatchInput.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Batch Output"))
                    {
                        this.ShortCutReportBatchOutput.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Concession"))
                    {
                        this.ShortCutReportConcession.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch OP"))
                    {
                        this.ShortCutReportDispatchOP.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Terminal"))
                    {
                        this.ShortCutReportDispatchTerminal.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Docket Detail"))
                    {
                        this.ShortCutReportDispatchDocketDetail.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Docket (NorthDown)"))
                    {
                        this.ShortCutReportDispatchDocketNorthDown.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Docket(No Price)"))
                    {
                        this.ShortCutReportDispatchDocketNoPrice.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Docket inc VAT"))
                    {
                        this.ShortCutReportDispatchDocketincVAT.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Docket"))
                    {
                        this.ShortCutReportDispatchDocket.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("CMR"))
                    {
                        this.ShortCutReportCMR.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Carcass Dispatch"))
                    {
                        this.ShortCutReportCarcassDispatch.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Carcass Export"))
                    {
                        this.ShortCutReportCarcassExport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Invoice Detail"))
                    {
                        this.ShortCutReportInvoiceDetail.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Invoice"))
                    {
                        this.ShortCutReportInvoice.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Intake Docket"))
                    {
                        this.ShortCutReportIntakeDocket.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Intake Report(Kill No)"))
                    {
                        this.ShortCutReportIntakeReportKill.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Intake Report(Product)"))
                    {
                        this.ShortCutReportIntakeReportProduct.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Label Data"))
                    {
                        this.ShortCutReportLabelData.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Label Trace"))
                    {
                        this.ShortCutReportLabelTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Stock Trace"))
                    {
                        this.ShortCutReportStockTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Dispatch Summary"))
                    {
                        this.ShortCutReportDispatchSummary.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Slicing Production List By Customer"))
                    {
                        this.ShortCutReportSlicingProdByCustomer.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Slicing Production List By Product"))
                    {
                        this.ShortCutReportSlicingProdByProduct.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Joints Production List By Customer"))
                    {
                        this.ShortCutReportJointsProdByCustomer.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Carcass Trace"))
                    {
                        this.ShortCutReportCarcassTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Customer List"))
                    {
                        this.ShortCutReportCustomerList.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Stock Report(Location)"))
                    {
                        this.ShortCutReportStockReportLocation.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Stock Report"))
                    {
                        this.ShortCutReportStockReport.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Stock Take"))
                    {
                        this.ShortCutReportStockTake.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Stock Trace"))
                    {
                        this.ShortCutReportStockTrace.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Product List"))
                    {
                        this.ShortCutReportProductList.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Price Lists"))
                    {
                        this.ShortCutReportPriceLists.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Price History"))
                    {
                        this.ShortCutReportPriceHistory.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Traceability"))
                    {
                        this.ShortCutReportTraceability.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Products"))
                    {
                        this.ShortCutReportProducts.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Price List Details"))
                    {
                        this.ShortCutReportPriceListDetails.Visibility = localAction;
                    }
                    else if (s.TreeNodeText.ContainsIgnoringCase("Sales Order"))
                    {
                        this.ShortCutReportSaleOrder.Visibility = localAction;
                    }

                    this.SaveShortcuts(s.ReportPath);
                    return;
                }

                if (s.TreeNodeText.Equals(Strings.Authorisations))
                {
                    this.ShortCutAuthorisation.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Specifications))
                {
                    this.ShortCutSpecifications.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.BatchSetUp))
                {
                    this.ShortCutBatchSetUp.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("Purchase Invoice"))
                {
                    this.ShortCutPurchaseInvoice.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Handheld))
                {
                    this.ShortCutHandheld.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.TelesalesSetUp))
                {
                    this.ShortCutTelesalesSetUp.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Group))
                {
                    this.ShortCutBPGroup.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.HACCPSearch))
                {
                    this.ShortCutWorkflowSearch.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Alerts))
                {
                    this.ShortCutAlerts.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.AnimalMovements))
                {
                    this.ShortCutAnimalMovements.Visibility = localAction;
                }
                else if (s.TreeNodeText.ContainsIgnoringCase(Strings.Telesales))
                {
                    this.ShortCutTelesales.Visibility = localAction;
                }
                else if (s.TreeNodeText.ContainsIgnoringCase(Strings.QuickOrder))
                {
                    this.ShortCutQuickOrder.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.LairageOrder))
                {
                    this.ShortCutLairageOrder.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.LairageDetails))
                {
                    this.ShortCutLairageDetails.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.KillDetails))
                {
                    this.ShortCutKillDetails.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.LairageIntake))
                {
                    this.ShortCutLairageIntake.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.CreateBatchPaymentPaymentProposal))
                {
                    this.ShortCutCreatePaymentPropoal.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.PaymentProposal))
                {
                    this.ShortCutPaymentProposal.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Accounts))
                {
                    this.ShortCutAccounts.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.SpecialPrices))
                {
                    this.ShortCutSpecialPrices.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("Master"))
                {
                    this.ShortCutBPMaster.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("INMaster"))
                {
                    this.ShortCutInventoryMaster.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.EditTransactions))
                {
                    this.ShortCutEditTransactions.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.CreateInvoice))
                {
                    this.ShortCutCreateInvoice.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Group))
                {
                    this.ShortCutBPGroup.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Import))
                {
                    this.ShortCutImportLicence.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.LabelAssociation))
                {
                    this.ShortCutLabelAssociation.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.LicenseAdmin))
                {
                    this.ShortCutLicencingAdmin.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Map))
                {
                    this.ShortCutMap.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Properties))
                {
                    this.ShortCutProperties.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.SwitchUser))
                {
                    this.ShortCutSwitchUser.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.StockMove))
                {
                    this.ShortCutStockMove.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.StockTake))
                {
                    this.ShortCutStockTake.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Reconciliation))
                {
                    this.ShortCutReconciliation.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Container))
                {
                    this.ShortCutDispatchContainer.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("APQuote"))
                {
                    this.ShortCutAPQuote.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("APOrder"))
                {
                    this.ShortCutAPOrder.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("SalesSearchData"))
                {
                    this.ShortCutSalesSearchData.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("SalesSearchDataDispatch"))
                {
                    this.ShortCutSalesSearchDataDispatch.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("SalesSearchDataSaleInvoice"))
                {
                    this.ShortCutSalesSearchDataSaleInvoice.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("SalesSearchDataPurchaseOrder"))
                {
                    this.ShortCutSalesSearchDataPurchaseOrder.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("SalesSearchDataIntake"))
                {
                    this.ShortCutSalesSearchDataIntake.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("SalesSearchDataPurchaseInvoice"))
                {
                    this.ShortCutSalesSearchDataPurchaseInvoice.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Invoice))
                {
                    this.ShortCutInvoice.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("APReceiptDesktop"))
                {
                    this.ShortCutAPReceiptDesktop.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("APReceiptTouchscreen"))
                {
                    this.ShortCutAPReceiptTouchscreen.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.IntoProduction))
                {
                    this.ShortCutIntoProduction.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.OutOfProduction))
                {
                    this.ShortCutOutOfProduction.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("ARDispatchDesktop"))
                {
                    this.ShortCutARDispatchDesktop.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("ARDispatchTouchscreen"))
                {
                    this.ShortCutARDispatchTouchscreen.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Quote))
                {
                    this.ShortCutARQuote.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.Order))
                {
                    this.ShortCutAROrder.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals("ARDispatchTouchscreen"))
                {
                    this.ShortCutARDispatchTouchscreen.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.PriceLists))
                {
                    this.ShortCutPriceLists.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.AllPrices))
                {
                    this.ShortCutAllPrices.Visibility = localAction;
                }
                else if (s.TreeNodeText.Equals(Strings.SpecialPrices))
                {
                    this.ShortCutSpecialPrices.Visibility = localAction;
                }
              
                this.SaveShortcuts();
            });

            #endregion
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void SaveShortcuts(string reportPath = "")
        {
            var shortcuts = new List<Nouvem.Model.DataLayer.UserShortcut>();

            if (this.ShortCutReport1.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 1, Alias = this.ShortCutReport1.Label, Name = this.ShortCutReport1.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport1.ToolTipDescription.ToInt()});
            }

            if (this.ShortCutReport2.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 2, Alias = this.ShortCutReport2.Label, Name = this.ShortCutReport2.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport2.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport3.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 3, Alias = this.ShortCutReport3.Label, Name = this.ShortCutReport3.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport3.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport4.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 4, Alias = this.ShortCutReport4.Label, Name = this.ShortCutReport4.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport4.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport5.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 5, Alias = this.ShortCutReport5.Label, Name = this.ShortCutReport5.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport5.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport6.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 6, Alias = this.ShortCutReport6.Label, Name = this.ShortCutReport6.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport6.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport7.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 7, Alias = this.ShortCutReport7.Label, Name = this.ShortCutReport7.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport7.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport8.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 8, Alias = this.ShortCutReport8.Label, Name = this.ShortCutReport8.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport8.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport9.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 9, Alias = this.ShortCutReport9.Label, Name = this.ShortCutReport9.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport9.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutReport10.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { ControlID = 10, Alias = this.ShortCutReport10.Label, Name = this.ShortCutReport10.Tag.ToString(), ReportPath = reportPath, ShowShortcut = true, ReportID = this.ShortCutReport10.ToolTipDescription.ToInt() });
            }

            if (this.ShortCutPriceLists.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.PriceLists, ShowShortcut = true });
            }

            if (this.ShortCutSpecifications.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Specifications, ShowShortcut = true });
            }

            if (this.ShortCutBatchSetUp.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.BatchSetUp, ShowShortcut = true });
            }

            if (this.ShortCutStockMove.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.StockMove, ShowShortcut = true });
            }

            if (this.ShortCutQuickOrder.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.QuickOrder, ShowShortcut = true });
            }

            if (this.ShortCutHandheld.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Handheld, ShowShortcut = true });
            }

            if (this.ShortCutTelesalesSetUp.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.TelesalesSetUp, ShowShortcut = true });
            }

            if (this.ShortCutTelesales.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Telesales, ShowShortcut = true });
            }

            if (this.ShortCutLairageDetails.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.LairageDetails, ShowShortcut = true });
            }

            if (this.ShortCutWorkflowSearch.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.HACCPSearch, ShowShortcut = true });
            }

            if (this.ShortCutAlerts.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Alerts, ShowShortcut = true });
            }

            if (this.ShortCutAnimalMovements.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.AnimalMovements, ShowShortcut = true });
            }

            if (this.ShortCutKillDetails.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.KillDetails, ShowShortcut = true });
            }

            if (this.ShortCutLairageOrder.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.LairageOrder, ShowShortcut = true });
            }

            if (this.ShortCutLairageIntake.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.LairageIntake, ShowShortcut = true });
            }

            if (this.ShortCutCreatePaymentPropoal.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.CreateBatchPaymentPaymentProposal, ShowShortcut = true });
            }

            if (this.ShortCutPaymentProposal.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.PaymentProposal, ShowShortcut = true });
            }

            if (this.ShortCutReportConcession.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Concession", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportPriceListProductGroup.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Price List (Product Group)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchYieldPriced.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Yield (Priced)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportPriceLists.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Price Lists", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportPriceHistory.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Price History", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportTraceability.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Traceability", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDispatchOP.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch OP", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDispatchTerminal.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Terminal", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchDetail.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Detail", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportLabelAssociation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Label Association", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDeletedLabel.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Deleted Label", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportProducts.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Products", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDispatchDocketNorthDown.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Docket (NorthDown)", ReportPath = reportPath, ShowShortcut = true });
            }


            if (this.ShortCutReportDispatchDocketNoPrice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Docket(No Price)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDispatchDocketincVAT.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Docket inc VAT", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportProductList.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Product List", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportPriceListDetails.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Price List Details", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportStockTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Stock Trace", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportStockReport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Stock Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportStockReportLocation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Stock Report(Location)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportStockTake.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Stock Take", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportIntakeDocket.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Intake Docket", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportSlicingProdByCustomer.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Slicing Production List By Customer", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportSlicingProdByProduct.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Slicing Production List By Product", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportJointsProdByCustomer.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Joints Production List By Customer", ReportPath = reportPath, ShowShortcut = true });
            }
            if (this.ShortCutReportDispatchSummary.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Summary", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportSaleOrder.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Sales Order", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportIntakeReportKill.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Intake Report(Kill No)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportIntakeReportProduct.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Intake Report(Product)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportLabelData.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Label Data", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportLabelTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Label Trace", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportStockTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Stock Trace", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Invoice", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportInvoiceDetail.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Invoice Detail", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportCarcassDispatch.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Carcass Dispatch", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportCarcassTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Carcass Trace", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportCarcassExport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Carcass Export", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportCMR.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "CMR", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDispatchDocket.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Docket", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDispatchDocketDetail.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Dispatch Docket Detail", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchYieldDetail.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Yield Detail", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchInput.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Input", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchOutput.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Output", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchYield.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Yield", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchDetails.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Details", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBatchDetailsTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Details Trace", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutSpecialPrices.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.SpecialPrices, ShowShortcut = true });
            }

            if (this.ShortCutAccounts.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Accounts, ShowShortcut = true });
            }

            if (this.ShortCutAllPrices.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.AllPrices, ShowShortcut = true });
            }

            if (this.ShortCutCreateInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.CreateInvoice, ShowShortcut = true });
            }

            if (this.ShortCutSpecialPrices.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.SpecialPrices, ShowShortcut = true });
            }

            if (this.ShortCutInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Invoice, ShowShortcut = true });
            }

            if (this.ShortCutInventoryMaster.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "INMaster", ShowShortcut = true });
            }

            if (this.ShortCutPurchaseInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Purchase Invoice", ShowShortcut = true });
            }

            if (this.ShortCutAROrder.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Order, ShowShortcut = true });
            }

            if (this.ShortCutARQuote.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Quote, ShowShortcut = true });
            }

            if (this.ShortCutBPMaster.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Master", ShowShortcut = true });
            }

            if (this.ShortCutInventoryGroup.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "INGroup", ShowShortcut = true });
            }

            if (this.ShortCutAPQuote.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "APQuote", ShowShortcut = true });
            }

            if (this.ShortCutARDispatchDesktop.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "ARDispatchDesktop", ShowShortcut = true });
            }

            if (this.ShortCutARDispatchTouchscreen.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "ARDispatchTouchscreen", ShowShortcut = true });
            }

            if (this.ShortCutIntoProduction.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.IntoProduction, ShowShortcut = true });
            }

            if (this.ShortCutOutOfProduction.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.OutOfProduction, ShowShortcut = true });
            }

            if (this.ShortCutAPOrder.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "APOrder", ShowShortcut = true });
            }

            if (this.ShortCutSalesSearchData.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "SalesSearchData", ShowShortcut = true });
            }

            if (this.ShortCutSalesSearchDataDispatch.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "SalesSearchDataDispatch", ShowShortcut = true });
            }

            if (this.ShortCutSalesSearchDataSaleInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "SalesSearchDataSaleInvoice", ShowShortcut = true });
            }

            if (this.ShortCutSalesSearchDataPurchaseOrder.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "SalesSearchDataPurchaseOrder", ShowShortcut = true });
            }

            if (this.ShortCutSalesSearchDataIntake.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "SalesSearchDataIntake", ShowShortcut = true });
            }

            if (this.ShortCutSalesSearchDataPurchaseInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "SalesSearchDataPurchaseInvoice", ShowShortcut = true });
            }

            if (this.ShortCutAPReceiptDesktop.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "APReceiptDesktop", ShowShortcut = true });
            }

            if (this.ShortCutAPReceiptTouchscreen.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "APReceiptTouchscreen", ShowShortcut = true });
            }

            if (this.ShortCutStockTake.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.StockTake, ShowShortcut = true });
            }

            if (this.ShortCutDispatchContainer.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Container, ShowShortcut = true });
            }

            if (this.ShortCutReconciliation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Reconciliation, ShowShortcut = true });
            }

            if (this.ShortCutAuthorisation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Authorisations, ShowShortcut = true });
            }

            if (this.ShortCutBPGroup.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Group, ShowShortcut = true });
            }

            if (this.ShortCutBPMaster.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Master, ShowShortcut = true });
            }

            if (this.ShortCutProperties.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Properties, ShowShortcut = true });
            }

            if (this.ShortCutEditTransactions.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.EditTransactions, ShowShortcut = true });
            }

            if (this.ShortCutImportLicence.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Import, ShowShortcut = true });
            }

            if (this.ShortCutLabelAssociation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.LabelAssociation, ShowShortcut = true });
            }

            if (this.ShortCutLicencingAdmin.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Admin, ShowShortcut = true });
            }

            if (this.ShortCutMap.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.Map, ShowShortcut = true });
            }

            if (this.ShortCutSwitchUser.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = Strings.SwitchUser, ShowShortcut = true });
            }





            if (this.ShortCutReportBatchCostings.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Batch Costings", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportBeefKill.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Beef kill Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportCarcassDispatchExport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Carcass Dispatch Export", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportChequePaymentReport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Cheque Payment Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportDailyKillSummary.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Daily Kill Summary", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportEndOfDay.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "End Of Day Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportKillReport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Kill Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportKillReportBySupplier.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Kill Report By Supplier", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportKillReportScotBeef.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Kill Report ScotBeef", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportLabelKillInformation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Label Kill Information", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportRPAExport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "RPA Export", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportStockTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Stock Trace Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysBatchDetails.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Batch Details Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysBatchDetailsTrace.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Batch Details Trace Report", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysBatchYield.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Batch Yield", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysCarcassExport.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Carcass Export", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysDispatchDocket.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Dispatch Docket", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysDispatchDocketDetail.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Dispatch Docket Detail", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysInvoice.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Invoice", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysStockReportLocation.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Stock Report(Location)", ReportPath = reportPath, ShowShortcut = true });
            }

            if (this.ShortCutReportWoolleysStockTake.Visibility == Visibility.Visible)
            {
                shortcuts.Add(new UserShortcut { Name = "Woolleys - Stock Take", ReportPath = reportPath, ShowShortcut = true });
            }

            NouvemGlobal.LoggedInUser.UserShortcuts = shortcuts;
            DataManager.Instance.SaveUserShortcuts(NouvemGlobal.LoggedInUser);
        }

        private void SetUserShortcuts(Model.BusinessObject.User user)
        {
            #region collapse shortcuts

            this.ShortCutReport1.Visibility = Visibility.Collapsed;
            this.ShortCutReport2.Visibility = Visibility.Collapsed;
            this.ShortCutReport3.Visibility = Visibility.Collapsed;
            this.ShortCutReport4.Visibility = Visibility.Collapsed;
            this.ShortCutReport5.Visibility = Visibility.Collapsed;
            this.ShortCutReport6.Visibility = Visibility.Collapsed;
            this.ShortCutReport7.Visibility = Visibility.Collapsed;
            this.ShortCutReport8.Visibility = Visibility.Collapsed;
            this.ShortCutReport9.Visibility = Visibility.Collapsed;
            this.ShortCutReport10.Visibility = Visibility.Collapsed;
            this.ShortCutStockMove.Visibility = Visibility.Collapsed;
            this.ShortCutSpecifications.Visibility = Visibility.Collapsed;
            this.ShortCutBatchSetUp.Visibility = Visibility.Collapsed;
            this.ShortCutWorkflowSearch.Visibility = Visibility.Collapsed;
            this.ShortCutAlerts.Visibility = Visibility.Collapsed;
            this.ShortCutHandheld.Visibility = Visibility.Collapsed;
            this.ShortCutTelesalesSetUp.Visibility = Visibility.Collapsed;
            this.ShortCutTelesales.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockTraceReport.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchCostings.Visibility = Visibility.Collapsed;
            this.ShortCutReportBeefKill.Visibility = Visibility.Collapsed;
            this.ShortCutReportCarcassDispatchExport.Visibility = Visibility.Collapsed;
            this.ShortCutPurchaseInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutReportChequePaymentReport.Visibility = Visibility.Collapsed;
            this.ShortCutReportDailyKillSummary.Visibility = Visibility.Collapsed;
            this.ShortCutReportEndOfDay.Visibility = Visibility.Collapsed;
            this.ShortCutReportKillReport.Visibility = Visibility.Collapsed;
            this.ShortCutReportKillReportBySupplier.Visibility = Visibility.Collapsed;
            this.ShortCutReportKillReportScotBeef.Visibility = Visibility.Collapsed;
            this.ShortCutReportLabelKillInformation.Visibility = Visibility.Collapsed;
            this.ShortCutReportRPAExport.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysBatchDetails.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysBatchDetailsTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysBatchYield.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysCarcassExport.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysDispatchDocket.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysDispatchDocketDetail.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysStockReportLocation.Visibility = Visibility.Collapsed;
            this.ShortCutReportWoolleysStockTake.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchDocketNorthDown.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchDocketNoPrice.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchDocketincVAT.Visibility = Visibility.Collapsed;
            this.ShortCutAnimalMovements.Visibility = Visibility.Collapsed;
            this.ShortCutUserSetUp.Visibility = Visibility.Collapsed;
            this.ShortCutAuthorisation.Visibility = Visibility.Collapsed;
            this.ShortCutBPGroup.Visibility = Visibility.Collapsed;
            this.ShortCutBPMaster.Visibility = Visibility.Collapsed;
            this.ShortCutEditTransactions.Visibility = Visibility.Collapsed;
            this.ShortCutGroup.Visibility = Visibility.Collapsed;
            this.ShortCutImportLicence.Visibility = Visibility.Collapsed;
            this.ShortCutLabelAssociation.Visibility = Visibility.Collapsed;
            this.ShortCutLicencingAdmin.Visibility = Visibility.Collapsed;
            this.ShortCutMap.Visibility = Visibility.Collapsed;
            this.ShortCutProperties.Visibility = Visibility.Collapsed;
            this.ShortCutSwitchUser.Visibility = Visibility.Collapsed;
            this.ShortCutARDispatchDesktop.Visibility = Visibility.Collapsed;
            this.ShortCutARDispatchTouchscreen.Visibility = Visibility.Collapsed;
            this.ShortCutAPQuote.Visibility = Visibility.Collapsed;
            this.ShortCutAPOrder.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchData.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchDataDispatch.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchDataSaleInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchDataPurchaseOrder.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchDataIntake.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchDataPurchaseInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutSalesSearchData.Visibility = Visibility.Collapsed;
            this.ShortCutAPReceiptDesktop.Visibility = Visibility.Collapsed;
            this.ShortCutAPReceiptTouchscreen.Visibility = Visibility.Collapsed;
            this.ShortCutPriceLists.Visibility = Visibility.Collapsed;
            this.ShortCutAllPrices.Visibility = Visibility.Collapsed;
            this.ShortCutSpecialPrices.Visibility = Visibility.Collapsed;
            this.ShortCutReconciliation.Visibility = Visibility.Collapsed;
            this.ShortCutStockTake.Visibility = Visibility.Collapsed;
            this.ShortCutIntoProduction.Visibility = Visibility.Collapsed;
            this.ShortCutOutOfProduction.Visibility = Visibility.Collapsed;
            this.ShortCutDispatchContainer.Visibility = Visibility.Collapsed;
            this.ShortCutInventoryGroup.Visibility = Visibility.Collapsed;
            this.ShortCutInventoryMaster.Visibility = Visibility.Collapsed;
            this.ShortCutARQuote.Visibility = Visibility.Collapsed;
            this.ShortCutAROrder.Visibility = Visibility.Collapsed;
            this.ShortCutInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutQuickOrder.Visibility = Visibility.Collapsed;
            this.ShortCutCreateInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutSpecialPrices.Visibility = Visibility.Collapsed;
            this.ShortCutAccounts.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchYieldDetail.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchYield.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchDetailsTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchInput.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchOutput.Visibility = Visibility.Collapsed;
            this.ShortCutReportCMR.Visibility = Visibility.Collapsed;
            this.ShortCutReportCarcassDispatch.Visibility = Visibility.Collapsed;
            this.ShortCutReportCarcassExport.Visibility = Visibility.Collapsed;
            this.ShortCutReportCarcassTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportCustomerList.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchDocket.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchDocketDetail.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchSummary.Visibility = Visibility.Collapsed;
            this.ShortCutReportIntakeDocket.Visibility = Visibility.Collapsed;
            this.ShortCutReportIntakeReportKill.Visibility = Visibility.Collapsed;
            this.ShortCutReportIntakeReportProduct.Visibility = Visibility.Collapsed;
            this.ShortCutReportInvoice.Visibility = Visibility.Collapsed;
            this.ShortCutReportInvoiceDetail.Visibility = Visibility.Collapsed;
            this.ShortCutReportJointsProdByCustomer.Visibility = Visibility.Collapsed;
            this.ShortCutReportLabelData.Visibility = Visibility.Collapsed;
            this.ShortCutReportLabelTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchDetails.Visibility = Visibility.Collapsed;
            this.ShortCutReportProductList.Visibility = Visibility.Collapsed;
            this.ShortCutReportPriceListDetails.Visibility = Visibility.Collapsed;
            this.ShortCutReportSaleOrder.Visibility = Visibility.Collapsed;
            this.ShortCutReportSlicingProdByCustomer.Visibility = Visibility.Collapsed;
            this.ShortCutReportSlicingProdByProduct.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockReport.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockReportLocation.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockTrace.Visibility = Visibility.Collapsed;
            this.ShortCutReportStockTake.Visibility = Visibility.Collapsed;
            this.ShortCutReportProducts.Visibility = Visibility.Collapsed;
            this.ShortCutReportDeletedLabel.Visibility = Visibility.Collapsed;
            this.ShortCutReportLabelAssociation.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchDetail.Visibility = Visibility.Collapsed;
            this.ShortCutReportConcession.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchOP.Visibility = Visibility.Collapsed;
            this.ShortCutReportDispatchTerminal.Visibility = Visibility.Collapsed;
            this.ShortCutReportPriceLists.Visibility = Visibility.Collapsed;
            this.ShortCutReportPriceHistory.Visibility = Visibility.Collapsed;
            this.ShortCutReportTraceability.Visibility = Visibility.Collapsed;
            this.ShortCutReportPriceListProductGroup.Visibility = Visibility.Collapsed;
            this.ShortCutReportBatchYieldPriced.Visibility = Visibility.Collapsed;
            this.ShortCutLairageOrder.Visibility = Visibility.Collapsed;
            this.ShortCutLairageIntake.Visibility = Visibility.Collapsed;
            this.ShortCutCreatePaymentPropoal.Visibility = Visibility.Collapsed;
            this.ShortCutPaymentProposal.Visibility = Visibility.Collapsed;
            this.ShortCutLairageDetails.Visibility = Visibility.Collapsed;
            this.ShortCutKillDetails.Visibility = Visibility.Collapsed;

            #endregion

            if (user == null || user.UserShortcuts == null)
            {
                return;
            }

            foreach (var shortcut in user.UserShortcuts)
            {
                if (shortcut.ControlID.HasValue)
                {
                    if (shortcut.ControlID == 1)
                    {
                        this.ShortCutReport1.Visibility = Visibility.Visible;
                        this.ShortCutReport1.CommandParameter = shortcut.Name;
                        this.ShortCutReport1.Label = shortcut.Alias;
                        this.ShortCutReport1.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 2)
                    {
                        this.ShortCutReport2.Visibility = Visibility.Visible;
                        this.ShortCutReport2.CommandParameter = shortcut.Name;
                        this.ShortCutReport2.Label = shortcut.Alias;
                        this.ShortCutReport2.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 3)
                    {
                        this.ShortCutReport3.Visibility = Visibility.Visible;
                        this.ShortCutReport3.CommandParameter = shortcut.Name;
                        this.ShortCutReport3.Label = shortcut.Alias;
                        this.ShortCutReport3.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 4)
                    {
                        this.ShortCutReport4.Visibility = Visibility.Visible;
                        this.ShortCutReport4.CommandParameter = shortcut.Name;
                        this.ShortCutReport4.Label = shortcut.Alias;
                        this.ShortCutReport4.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 5)
                    {
                        this.ShortCutReport5.Visibility = Visibility.Visible;
                        this.ShortCutReport5.CommandParameter = shortcut.Name;
                        this.ShortCutReport5.Label = shortcut.Alias;
                        this.ShortCutReport5.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 6)
                    {
                        this.ShortCutReport6.Visibility = Visibility.Visible;
                        this.ShortCutReport6.CommandParameter = shortcut.Name;
                        this.ShortCutReport6.Label = shortcut.Alias;
                        this.ShortCutReport6.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 7)
                    {
                        this.ShortCutReport7.Visibility = Visibility.Visible;
                        this.ShortCutReport7.CommandParameter = shortcut.Name;
                        this.ShortCutReport7.Label = shortcut.Alias;
                        this.ShortCutReport7.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 8)
                    {
                        this.ShortCutReport8.Visibility = Visibility.Visible;
                        this.ShortCutReport8.CommandParameter = shortcut.Name;
                        this.ShortCutReport8.Label = shortcut.Alias;
                        this.ShortCutReport8.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 9)
                    {
                        this.ShortCutReport9.Visibility = Visibility.Visible;
                        this.ShortCutReport9.CommandParameter = shortcut.Name;
                        this.ShortCutReport9.Label = shortcut.Alias;
                        this.ShortCutReport9.Tag = shortcut.Name;
                    }
                    else if (shortcut.ControlID == 10)
                    {
                        this.ShortCutReport10.Visibility = Visibility.Visible;
                        this.ShortCutReport10.CommandParameter = shortcut.Name;
                        this.ShortCutReport10.Label = shortcut.Alias;
                        this.ShortCutReport10.Tag = shortcut.Name;
                    }
                }
                else if (shortcut.Name.Equals(Strings.Authorisations))
                {
                    this.ShortCutAuthorisation.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Group))
                {
                    this.ShortCutBPGroup.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.StockMove))
                {
                    this.ShortCutStockMove.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Specifications))
                {
                    this.ShortCutSpecifications.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.BatchSetUp))
                {
                    this.ShortCutBatchSetUp.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.TelesalesSetUp))
                {
                    this.ShortCutTelesalesSetUp.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Telesales))
                {
                    this.ShortCutTelesales.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Handheld))
                {
                    this.ShortCutHandheld.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.HACCPSearch))
                {
                    this.ShortCutWorkflowSearch.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Alerts))
                {
                    this.ShortCutAlerts.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.AnimalMovements))
                {
                    this.ShortCutAnimalMovements.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.LairageOrder))
                {
                    this.ShortCutLairageOrder.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Purchase Invoice"))
                {
                    this.ShortCutPurchaseInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.LairageDetails))
                {
                    this.ShortCutLairageDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.QuickOrder))
                {
                    this.ShortCutQuickOrder.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.KillDetails))
                {
                    this.ShortCutKillDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.LairageIntake))
                {
                    this.ShortCutLairageIntake.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.CreateBatchPaymentPaymentProposal))
                {
                    this.ShortCutCreatePaymentPropoal.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.PaymentProposal))
                {
                    this.ShortCutPaymentProposal.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Joints Production List By Customer"))
                {
                    this.ShortCutReportJointsProdByCustomer.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Slicing Production List By Customer"))
                {
                    this.ShortCutReportSlicingProdByCustomer.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Slicing Production List By Product"))
                {
                    this.ShortCutReportSlicingProdByProduct.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Intake Docket"))
                {
                    this.ShortCutReportIntakeDocket.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Concession"))
                {
                    this.ShortCutReportConcession.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Price Lists"))
                {
                    this.ShortCutReportPriceLists.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Yield (Priced)"))
                {
                    this.ShortCutReportBatchDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Price List (Product Group)"))
                {
                    this.ShortCutReportPriceListProductGroup.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Price History"))
                {
                    this.ShortCutReportPriceHistory.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Traceability"))
                {
                    this.ShortCutReportTraceability.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch OP"))
                {
                    this.ShortCutReportDispatchOP.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Terminal"))
                {
                    this.ShortCutReportDispatchTerminal.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Detail"))
                {
                    this.ShortCutReportBatchDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Label Association"))
                {
                    this.ShortCutReportLabelAssociation.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Deleted Label"))
                {
                    this.ShortCutReportDeletedLabel.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Products"))
                {
                    this.ShortCutReportProducts.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Product List"))
                {
                    this.ShortCutReportProductList.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Price List Details"))
                {
                    this.ShortCutReportPriceListDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Stock Report"))
                {
                    this.ShortCutReportStockReport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Stock Report(Location)"))
                {
                    this.ShortCutReportStockReportLocation.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Stock Trace"))
                {
                    this.ShortCutReportStockTrace.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Stock Take"))
                {
                    this.ShortCutReportStockTake.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Price List Details"))
                {
                    this.ShortCutReportPriceListDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Sales Order"))
                {
                    this.ShortCutReportSaleOrder.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Label Data"))
                {
                    this.ShortCutReportLabelData.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Label Trace"))
                {
                    this.ShortCutReportLabelTrace.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Details"))
                {
                    this.ShortCutReportBatchDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Intake Report(Kill No)"))
                {
                    this.ShortCutReportIntakeReportKill.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Intake Report(Product)"))
                {
                    this.ShortCutReportIntakeReportProduct.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Invoice"))
                {
                    this.ShortCutReportInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Invoice Detail"))
                {
                    this.ShortCutReportInvoiceDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Carcass Dispatch"))
                {
                    this.ShortCutReportCarcassDispatch.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Carcass Export"))
                {
                    this.ShortCutReportCarcassExport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Carcass Trace"))
                {
                    this.ShortCutReportCarcassTrace.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("CMR"))
                {
                    this.ShortCutReportCMR.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Yield"))
                {
                    this.ShortCutReportBatchYield.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Details"))
                {
                    this.ShortCutReportBatchDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Details Trace"))
                {
                    this.ShortCutReportBatchDetailsTrace.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Input"))
                {
                    this.ShortCutReportBatchInput.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Output"))
                {
                    this.ShortCutReportBatchOutput.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Yield Detail"))
                {
                    this.ShortCutReportBatchYieldDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Docket (NorthDown)"))
                {
                    this.ShortCutReportDispatchDocketNorthDown.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Docket(No Price)"))
                {
                    this.ShortCutReportDispatchDocketNoPrice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Docket inc VAT"))
                {
                    this.ShortCutReportDispatchDocketincVAT.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Docket"))
                {
                    this.ShortCutReportDispatchDocket.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Docket Detail"))
                {
                    this.ShortCutReportDispatchDocketDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Dispatch Summary"))
                {
                    this.ShortCutReportDispatchSummary.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Customer List"))
                {
                    this.ShortCutReportCustomerList.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("SalesSearchData"))
                {
                    this.ShortCutSalesSearchData.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("SalesSearchDataDispatch"))
                {
                    this.ShortCutSalesSearchDataDispatch.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("SalesSearchDataSaleInvoice"))
                {
                    this.ShortCutSalesSearchDataSaleInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("SalesSearchDataPurchaseOrder"))
                {
                    this.ShortCutSalesSearchDataPurchaseOrder.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("SalesSearchDataIntake"))
                {
                    this.ShortCutSalesSearchDataIntake.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("SalesSearchDataPurchaseInvoice"))
                {
                    this.ShortCutSalesSearchDataPurchaseInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Yield Detail"))
                {
                    this.ShortCutReportBatchYieldDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Accounts))
                {
                    this.ShortCutAccounts.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.SpecialPrices))
                {
                    this.ShortCutSpecialPrices.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.CreateInvoice))
                {
                    this.ShortCutCreateInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Invoice))
                {
                    this.ShortCutInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Master"))
                {
                    this.ShortCutBPMaster.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Quote))
                {
                    this.ShortCutARQuote.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Order))
                {
                    this.ShortCutAROrder.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("INMaster"))
                {
                    this.ShortCutInventoryMaster.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.EditTransactions))
                {
                    this.ShortCutEditTransactions.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Group))
                {
                    this.ShortCutBPGroup.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Import))
                {
                    this.ShortCutImportLicence.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.LabelAssociation))
                {
                    this.ShortCutLabelAssociation.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.LicenseAdmin))
                {
                    this.ShortCutLicencingAdmin.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Map))
                {
                    this.ShortCutMap.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Properties))
                {
                    this.ShortCutProperties.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.SwitchUser))
                {
                    this.ShortCutSwitchUser.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("ARDispatchDesktop"))
                {
                    this.ShortCutARDispatchDesktop.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("APReceiptDesktop"))
                {
                    this.ShortCutAPReceiptDesktop.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals(Strings.Container))
                {
                    this.ShortCutDispatchContainer.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Batch Costings"))
                {
                    this.ShortCutReportBatchCostings.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Beef Kill Report"))
                {
                    this.ShortCutReportBeefKill.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Carcass Dispatch Export"))
                {
                    this.ShortCutReportCarcassDispatchExport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Cheque Payment Report"))
                {
                    this.ShortCutReportChequePaymentReport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Daily Kill Summary"))
                {
                    this.ShortCutReportDailyKillSummary.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("End Of Day Report"))
                {
                    this.ShortCutReportEndOfDay.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Kill Report"))
                {
                    this.ShortCutReportKillReport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Kill Report By Supplier"))
                {
                    this.ShortCutReportKillReportBySupplier.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Kill Report ScotBeef"))
                {
                    this.ShortCutReportKillReportScotBeef.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Label Kill Information"))
                {
                    this.ShortCutReportLabelKillInformation.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("RPA Export"))
                {
                    this.ShortCutReportRPAExport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Stock Trace Report"))
                {
                    this.ShortCutReportStockTrace.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Batch Details Report"))
                {
                    this.ShortCutReportWoolleysBatchDetails.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Batch Details Trace Report"))
                {
                    this.ShortCutReportWoolleysBatchDetailsTrace.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Batch Yield"))
                {
                    this.ShortCutReportWoolleysBatchYield.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Carcass Export"))
                {
                    this.ShortCutReportWoolleysCarcassExport.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Dispatch Docket"))
                {
                    this.ShortCutReportWoolleysDispatchDocket.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Dispatch Docket Detail"))
                {
                    this.ShortCutReportWoolleysDispatchDocketDetail.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Invoice"))
                {
                    this.ShortCutReportWoolleysInvoice.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Stock Report(Location)"))
                {
                    this.ShortCutReportWoolleysStockReportLocation.Visibility = Visibility.Visible;
                }
                else if (shortcut.Name.Equals("Woolleys - Stock Take"))
                {
                    this.ShortCutReportWoolleysStockTake.Visibility = Visibility.Visible;
                }
            }
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="MasterWindowView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Threading;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.View.Account;
using Nouvem.View.Attribute;
using Nouvem.View.Audit;
using Nouvem.View.Container;
using Nouvem.View.Country;
using Nouvem.View.Currency;
using Nouvem.View.Date;
using Nouvem.View.Department;
using Nouvem.View.Document;
using Nouvem.View.EPOS;
using Nouvem.View.Inventory;
using Nouvem.View.KillLine.Lairage;
using Nouvem.View.Map;
using Nouvem.View.MRP;
using Nouvem.View.Payments;
using Nouvem.View.Purchases.APOrder;
using Nouvem.View.Purchases.APQuote;
using Nouvem.View.Purchases.APReceipt;
using Nouvem.View.Quality;
using Nouvem.View.Sales;
using Nouvem.View.Sales.AROrder;
using Nouvem.View.Sales.ARQuote;
using Nouvem.View.Plant;
using Nouvem.View.Pricing;
using Nouvem.View.Production;
using Nouvem.View.Production.Receipe;
using Nouvem.View.Purchases.APInvoice;
using Nouvem.View.Region;
using Nouvem.View.Report;
using Nouvem.View.Route;
using Nouvem.View.Sales.ARDispatch;
using Nouvem.View.Sales.ARInvoice;
using Nouvem.View.Sales.ARReturn;
using Nouvem.View.Scanner;
using Nouvem.View.Scanner.Emulator;
using Nouvem.View.Stock;
using Nouvem.View.Touchscreen;
using Nouvem.View.Traceability;
using Nouvem.View.Transaction;
using Nouvem.View.UOM;
using Nouvem.View.UserInput;
using Nouvem.View.Utility;
using Nouvem.View.Workflow;
using Nouvem.ViewModel.Sales;

namespace Nouvem.View.Master
{
    using System.Windows;
    using Nouvem.View.Logic;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.View.BusinessPartner;
    using Nouvem.View.Label;
    using Nouvem.View.License;
    using Nouvem.View.StartUp;
    using Nouvem.View.User;
    using Nouvem.ViewModel;

    /// <summary>
    /// Interaction logic for MasterWindowView.xaml
    /// </summary>
    public partial class MasterWindowView : Window
    {
        /// <summary>
        /// The receipt details window.
        /// </summary>
        ///private Window receipt;

        /// <summary>
        /// The dispatch details window.
        /// </summary>
        private Window dispatch;

        public MasterWindowView()
        {
            this.InitializeComponent();

            // ensure any log in windows are closed
            Messenger.Default.Send(Token.Message, Token.CloseAllWindows);

            Messenger.Default.Register<string>(Token.Message, Token.BringToFront, s => this.Activate());

            // Create the bp master window.
            Messenger.Default.Register<ViewType>(
                this,
                view =>
                {
                    if (view == ViewType.BPMaster)
                    {
                        if (ViewModelLocator.BPMasterStatic.IsFormLoaded)
                        {
                            // don't allow multiple instances of the bp master window.
                            return;
                        }

                        var bpMaster = new BPMasterContainerView { Owner = GetWindow(this) };
                        bpMaster.Show();
                        bpMaster.Focus();
                        return;
                    }

                    if (view == ViewType.BPPropertySetUp)
                    {
                        var bpMaster = new BPPropertySetUpContainerView { Owner = GetWindow(this) };
                        bpMaster.Show();
                        bpMaster.Focus();
                        return;
                    }

                    if (view == ViewType.UserSetUp)
                    {
                        if (ViewModelLocator.UserMasterStatic.IsFormLoaded)
                        {
                            // don't allow multiple instances of the user set up window.
                            return;
                        }

                        var userSetUp = new UserSetUpContainerView { Owner = GetWindow(this) };
                        userSetUp.Show();
                        userSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.UserSearchData)
                    {
                        var userSearch = new UserSearchDataView { Owner = GetWindow(this) };
                        userSearch.Show();
                        userSearch.Focus();
                        return;
                    }

                    if (view == ViewType.MRP)
                    {
                        var userSearch = new MRPView { Owner = GetWindow(this) };
                        userSearch.Show();
                        userSearch.Focus();
                        return;
                    }

                    if (view == ViewType.PricingMatrix)
                    {
                        var userSearch = new PricingMatrixView { Owner = GetWindow(this) };
                        userSearch.Show();
                        userSearch.Focus();
                        return;
                    }

                    if (view == ViewType.UserGroup)
                    {
                        if (ViewModelLocator.UserGroupStatic.IsFormLoaded)
                        {
                            // don't allow multiple instances of the user group window.
                            return;
                        }

                        var userGroup = new UserGroupContainerView { Owner = GetWindow(this) };
                        userGroup.Show();
                        userGroup.Focus();
                        return;
                    }

                    if (view == ViewType.ReportSetUp)
                    {
                        var bpGroup = new ReportSetUpView { Owner = GetWindow(this) };
                        bpGroup.Show();
                        bpGroup.Focus();
                        return;
                    }

                    if (view == ViewType.SpecsSearch)
                    {
                        var bpGroup = new SpecsSearchView { Owner = GetWindow(this) };
                        bpGroup.Show();
                        bpGroup.Focus();
                        return;
                    }

                    if (view == ViewType.ReportFolders)
                    {
                        var bpGroup = new ReportFoldersView { Owner = GetWindow(this) };
                        bpGroup.Show();
                        bpGroup.Focus();
                        return;
                    }

                    if (view == ViewType.Department)
                    {
                        var bpGroup = new DepartmentView { Owner = GetWindow(this) };
                        bpGroup.Show();
                        bpGroup.Focus();
                        return;
                    }

                    if (view == ViewType.ReportProcess)
                    {
                        var bpGroup = new ReportProcessView { Owner = GetWindow(this) };
                        bpGroup.Show();
                        bpGroup.Focus();
                        return;
                    }

                    if (view == ViewType.BPGroupSetUp)
                    {
                        var bpGroup = new BPGroupSetUpContainerView { Owner = GetWindow(this) };
                        bpGroup.Show();
                        bpGroup.Focus();
                        return;
                    }

                    if (view == ViewType.SyncPartners)
                    {
                        var accounts = new SyncPartnersView { Owner = GetWindow(this) };
                        accounts.Show();
                        accounts.Focus();
                        return;
                    }

                    if (view == ViewType.SyncPrices)
                    {
                        var accounts = new SyncPricesView { Owner = GetWindow(this) };
                        accounts.Show();
                        accounts.Focus();
                        return;
                    }

                    if (view == ViewType.SyncProducts)
                    {
                        var accounts = new SyncProductsView { Owner = GetWindow(this) };
                        accounts.Show();
                        accounts.Focus();
                        return;
                    }

                    if (view == ViewType.Accounts)
                    {
                        var accounts = new AccountsView { Owner = GetWindow(this) };
                        accounts.Show();
                        accounts.Focus();
                        return;
                    }

                    if (view == ViewType.ReturnsAccounts)
                    {
                        var accounts = new AccountsCreditReturnsView { Owner = GetWindow(this) };
                        accounts.Show();
                        accounts.Focus();
                        return;
                    }

                    if (view == ViewType.PurchasesAccounts)
                    {
                        var accounts = new AccountsPurchasesView { Owner = GetWindow(this) };
                        accounts.Show();
                        accounts.Focus();
                        return;
                    }

                    if (view == ViewType.Currency)
                    {
                        var currency = new CurrencyContainerView { Owner = GetWindow(this) };
                        currency.Show();
                        currency.Focus();
                        return;
                    }

                    if (view == ViewType.QualityAssuranceView)
                    {
                        var qa= new QualityAssuranceView { Owner = GetWindow(this) };
                        qa.Show();
                        qa.Focus();
                        return;
                    }

                    if (view == ViewType.Invoice)
                    {
                        if (ViewModelLocator.InvoiceStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var invoice = new InvoiceContainerView { Owner = GetWindow(this) };
                        invoice.Show();
                        invoice.Focus();
                        return;
                    }

                    if (view == ViewType.PurchaseInvoice)
                    {
                        if (ViewModelLocator.APInvoiceStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var invoice = new APInvoiceView { Owner = GetWindow(this) };
                        invoice.Show();
                        invoice.Focus();
                        return;
                    }

                    if (view == ViewType.Map)
                    {
                        var map = new MapContainerView { Owner = GetWindow(this) };
                        map.Show();
                        map.Focus();
                        return;
                    }

                    if (view == ViewType.MessageHistory)
                    {
                        var history = new MessageHistoryView { Owner = GetWindow(this) };
                        history.ShowDialog();
                        history.Focus();
                        return;
                    }

                    if (view == ViewType.PaymentDeductions)
                    {
                        var pay = new PaymentDeductionsView { Owner = GetWindow(this) };
                        pay.ShowDialog();
                        pay.Focus();
                        return;
                    }

                    if (view == ViewType.PaymentProposalCreation)
                    {
                        var pay = new PaymentProposalView { Owner = GetWindow(this) };
                        pay.Show();
                        pay.Focus();
                        return;
                    }

                    if (view == ViewType.PaymentProposalOpen)
                    {
                        var pay = new PaymentProposalSearchView { Owner = GetWindow(this) };
                        pay.Show();
                        pay.Focus();
                        return;
                    }

                    if (view == ViewType.LairageAnimalsSearch)
                    {
                        var pay = new LairageAnimalsSearchView { Owner = GetWindow(this) };
                        pay.Show();
                        pay.Focus();
                        return;
                    }

                    if (view == ViewType.Payment)
                    {
                        if (ViewModelLocator.PaymentStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var pay = new PaymentView { Owner = GetWindow(this) };
                        pay.Show();
                        pay.Focus();
                        return;
                    }

                    if (view == ViewType.Login)
                    {
                        ApplicationSettings.UserLoggedIn = true;
                        Settings.Default.Save();
                        var login = new LoginContainerView { Owner = GetWindow(this) };
                        login.Show();
                        login.Focus();
                        return;
                    }

                    if (view == ViewType.ReportViewer)
                    {
                        //if (ViewModelLocator.ReportViewerStatic.IsFormLoaded)
                        //{
                        //    return;
                        //}

                        var report = new Report.ReportViewer { Owner = GetWindow(this) };
                        report.Show();
                        report.Focus();
                        return;
                    }

                    if (view == ViewType.LoginSelection)
                    {
                        var loginSelection = new LoginSelectionContainerView { Owner = GetWindow(this) };
                        loginSelection.ShowDialog();
                        loginSelection.Focus();
                        return;
                    }

                    if (view == ViewType.DispatchContainer)
                    {
                        var loginSelection = new DispatchContainerView { Owner = GetWindow(this) };
                        loginSelection.Show();
                        loginSelection.Focus();
                        return;
                    }

                    if (view == ViewType.FTrace)
                    {
                        var loginSelection = new FTraceView { Owner = GetWindow(this) };
                        loginSelection.Show();
                        loginSelection.Focus();
                        return;
                    }

                    if (view == ViewType.LabelAssociation)
                    {
                        var assoc = new LabelAssociationContainerView { Owner = GetWindow(this) };
                        assoc.Show();
                        assoc.Focus();
                        return;
                    }

                    if (view == ViewType.ServerSetUp)
                    {
                        var serverSetUp = new ServerSetUpContainerView { Owner = GetWindow(this) };
                        serverSetUp.ShowDialog();
                        serverSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.Warehouse)
                    {
                        var warehouseSetUp = new WarehouseContainerView { Owner = GetWindow(this) };
                        warehouseSetUp.Show();
                        warehouseSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.Authorisations)
                    {
                        var authorisations = new UserGroupAuthorisationsContainerView { Owner = GetWindow(this) };
                        authorisations.Show();
                        authorisations.Focus();
                        return;
                    }

                    if (view == ViewType.DeviceSetUp)
                    {
                        var deviceSetUp = new DeviceSetUpContainerView { Owner = GetWindow(this) };
                        deviceSetUp.Show();
                        deviceSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.DeviceSettings)
                    {
                        var deviceSetUp = new DeviceSettingsView { Owner = GetWindow(this) };
                        deviceSetUp.Show();
                        deviceSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.DeviceSearch)
                    {
                        var deviceSearch = new DeviceSearchView { Owner = GetWindow(this) };
                        deviceSearch.Show();
                        deviceSearch.Focus();
                        return;
                    }

                    if (view == ViewType.LicenseAdmin)
                    {
                        var admin = new LicenseAdminContainerView { Owner = GetWindow(this) };
                        admin.Show();
                        admin.Focus();
                        return;
                    }

                    if (view == ViewType.ProductionSearchData)
                    {
                        var search = new ProductionSearchDataView { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.Specifications)
                    {
                        var search = new SpecificationsView { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.Recipe)
                    {
                        var search = new ReceipeView(0) { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.QuickOrder)
                    {
                        var search = new QuickOrderView { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.CarcassDispatch)
                    {
                        var search = new CarcassDispatchView { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.BatchSetUp)
                    {
                        var search = new BatchSetUpView { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.LicenseImport)
                    {
                        var import = new LicenseImportContainerView { Owner = GetWindow(this) };
                        import.Show();
                        import.Focus();
                        return;
                    }

                    if (view == ViewType.GS1AI)
                    {
                        var gS1AI = new GS1AIContainerView { Owner = GetWindow(this) };
                        gS1AI.Show();
                        gS1AI.Focus();
                        return;
                    }

                    if (view == ViewType.Date)
                    {
                        var date = new DateContainerView { Owner = GetWindow(this) };
                        date.Show();
                        date.Focus();
                        return;
                    }

                    if (view == ViewType.DateTemplateName)
                    {
                        var date = new DateTemplateNameContainerView() { Owner = GetWindow(this) };
                        date.Show();
                        date.Focus();
                        return;
                    }

                    if (view == ViewType.DateTemplateAllocation)
                    {
                        var date = new DatesTemplateAllocationContainerView { Owner = GetWindow(this) };
                        date.Show();
                        date.Focus();
                        return;
                    }

                    if (view == ViewType.AttributeTemplateName)
                    {
                        if (ViewModelLocator.TemplateNameStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var traceability = new TemplateNameView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.AlertUsersSetUp)
                    {
                        var traceability = new AlertUsersSetUpView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.Traceability)
                    {
                        var traceability = new TraceabilityMasterContainerView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.ARDispatch)
                    {
                        if (ViewModelLocator.ARDispatchStatic.IsFormLoaded)
                        {
                            if (ViewModelLocator.ARDispatch2Static.IsFormLoaded)
                            {
                                return;
                            }

                            var dispatch2 = new ARDispatchContainer2View { Owner = GetWindow(this) };
                            dispatch2.Show();
                            dispatch2.Focus();
                            return;
                        }

                        var dispatch = new ARDispatchContainerView { Owner = GetWindow(this) };
                        dispatch.Show();
                        dispatch.Focus();
                        return;
                    }

                    if (view == ViewType.ARDispatchDetails)
                    {
                        //if (this.dispatch == null)
                        //{
                        //    this.dispatch = new ARDispatchDetailsView { Owner = GetWindow(this) };
                        //}
                        this.dispatch = new ARDispatchDetailsView { Owner = GetWindow(this) };
                        this.dispatch.Show();
                        this.dispatch.Focus();
                        return;
                    }

                    if (view == ViewType.TemplateAllocation)
                    {
                        var traceability = new TemplateAllocationView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.Tabs)
                    {
                        var traceability = new TabsView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.TraceabilityTemplateName)
                    {
                        var traceability = new TraceabilityTemplateNameContainerView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.TraceabilityTemplateAllocation)
                    {
                        var traceability = new TraceabilityTemplateAllocationContainerView { Owner = GetWindow(this) };
                        traceability.Show();
                        traceability.Focus();
                        return;
                    }

                    if (view == ViewType.CountrySetUp)
                    {
                        var country = new CountryMasterContainerView { Owner = GetWindow(this) };
                        country.Show();
                        country.Focus();
                        return;
                    }

                    if (view == ViewType.Specs)
                    {
                        var country = new SpecsView { Owner = GetWindow(this) };
                        country.Show();
                        country.Focus();
                        return;
                    }

                    if (view == ViewType.Region)
                    {
                        var region = new RegionContainerView { Owner = GetWindow(this) };
                        region.Show();
                        region.Focus();
                        return;
                    }

                    if (view == ViewType.APQuote)
                    {
                        if (ViewModelLocator.APQuoteStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var quote = new APQuoteContainerView { Owner = GetWindow(this) };
                        quote.Show();
                        quote.Focus();
                        return;
                    }

                    if (view == ViewType.SpecialPrices)
                    {
                        if (ViewModelLocator.SpecialPricesStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var price = new SpecialPricesContainerView { Owner = GetWindow(this) };
                        price.Show();
                        price.Focus();
                        return;
                    }

                    if (view == ViewType.APOrder)
                    {
                        if (ViewModelLocator.APOrderStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var order = new APOrderContainerView { Owner = GetWindow(this) };
                        order.Show();
                        order.Focus();
                        return;
                    }

                    if (view == ViewType.Route)
                    {
                        var route = new RouteContainerView { Owner = GetWindow(this) };
                        route.Show();
                        route.Focus();
                        return;
                    }

                    if (view == ViewType.ApplyPrice)
                    {
                        var route = new ApplyPriceView { Owner = GetWindow(this) };
                        route.Show();
                        route.Focus();
                        return;
                    }

                    if (view == ViewType.ARReturns)
                    {
                        if (ViewModelLocator.ARReturnStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var route = new ARReturnView { Owner = GetWindow(this) };
                        route.Show();
                        route.Focus();
                        return;
                    }

                    if (view == ViewType.AttributeSearch)
                    {
                        var route = new AttributeSearchView { Owner = GetWindow(this) };
                        route.Show();
                        route.Focus();
                        return;
                    }

                    if (view == ViewType.APReceipt)
                    {
                        if (ViewModelLocator.APReceiptStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var receipt = new APReceiptContainerView { Owner = GetWindow(this) };
                        receipt.Show();
                        receipt.Focus();
                        return;
                    }

                    if (view == ViewType.ScannerDispatch)
                    {
                        var scanner = new ScannerModuleSelectionView();
                        scanner.Show();
                        scanner.Focus();
                    }

                    //if (view == ViewType.CollectionDisplay)
                    //{
                    //    var display = new CollectionDisplayContainerView { Owner = GetWindow(this) };
                    //    display.ShowDialog();
                    //    display.Focus();
                    //    return;
                    //}

                    //if (view == ViewType.Keyboard)
                    //{
                    //    var keyboard = new KeyboardContainerView { Owner = GetWindow(this) };
                    //    keyboard.ShowDialog();
                    //    keyboard.Focus();
                    //    return;
                    //}

                    if (view == ViewType.TransactionEditor)
                    {
                        var edit = new TransactionEditorView { Owner = GetWindow(this) };
                        edit.ShowDialog();
                        edit.Focus();
                        return;
                    }

                    if (view == ViewType.WorkflowSearch)
                    {
                        var edit = new WorkflowSearchView { Owner = GetWindow(this) };
                        edit.Show();
                        edit.Focus();
                        return;
                    }

                    if (view == ViewType.FactoryTouchScreen)
                    {
                        try
                        {
                            var factory = new FactoryScreenContainerView { Owner = GetWindow(this) };
                            factory.Show();
                            factory.Focus();
                            ViewModelLocator.FactoryScreenStatic.SetMainViewModel(ViewModelLocator.TouchscreenStatic);
                        }
                        catch 
                        {
                         
                        }
                       
                        return;
                    }

                    if (view == ViewType.DocumentTrail)
                    {
                        var doc = new DocumentTrailView { Owner = GetWindow(this) };
                        doc.ShowDialog();
                        doc.Focus();
                        return;
                    }

                    if (view == ViewType.APReceiptDetails)
                    {
                        var receipt = new APReceiptDetailsContainerView { Owner = GetWindow(this) };
                        receipt.Show();
                        receipt.Focus();
                        return;
                    }

                    if (view == ViewType.BatchEdit)
                    {
                        var batchEdit = new BatchEditView {Owner = GetWindow(this)};
                        batchEdit.Show();
                        batchEdit.Focus();
                        return;
                    }

                    if (view == ViewType.BatchEditor)
                    {
                        var batchEdit = new BatchEditorView { Owner = GetWindow(this) };
                        batchEdit.Show();
                        batchEdit.Focus();
                        return;
                    }

                    if (view == ViewType.RecipeSearch)
                    {
                        var batchEdit = new RecipeSearchView { Owner = GetWindow(this) };
                        batchEdit.Show();
                        batchEdit.Focus();
                        return;
                    }

                    if (view == ViewType.ContainerSetUp)
                    {
                        var container = new ContainerContainerView { Owner = GetWindow(this) };
                        container.Show();
                        container.Focus();
                        return;
                    }

                    if (view == ViewType.ContainerSetUp)
                    {
                        var container = new ContainerContainerView { Owner = GetWindow(this) };
                        container.Show();
                        container.Focus();
                        return;
                    }

                    if (view == ViewType.AttributeTemplateGroup)
                    {
                        var group = new TemplateGroupView { Owner = GetWindow(this) };
                        group.Show();
                        group.Focus();
                        return;
                    }

                    if (view == ViewType.ContainerAccountsView)
                    {
                        var account = new ContainerAccountsView { Owner = GetWindow(this) };
                        account.Show();
                        account.Focus();
                        return;
                    }


                    if (view == ViewType.IMPropertySetUp)
                    {
                        var propertySetUp = new ItemMasterPropertySetUpContainerView { Owner = GetWindow(this) };
                        propertySetUp.Show();
                        propertySetUp.Focus();
                        return;
                    }

                    if (view == ViewType.Plant)
                    {
                        var plantSetUp = new PlantContainerView { Owner = GetWindow(this) };
                        plantSetUp.Show();
                        plantSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.ReportProductData)
                    {
                        var data = new ReportProductDataView { Owner = GetWindow(this) };
                        data.Show();
                        data.Focus();
                        return;
                    }

                    if (view == ViewType.ReportTransactionData)
                    {
                        var data = new ReportTransactionDataView { Owner = GetWindow(this) };
                        data.Show();
                        data.Focus();
                        return;
                    }

                    if (view == ViewType.TransactionDetails)
                    {
                        var trans = new TransactionDetailsContainerView { Owner = GetWindow(this) };
                        trans.Show();
                        trans.Focus();
                        return;
                    }

                    if (view == ViewType.Order)
                    {
                        if (ViewModelLocator.OrderStatic.IsFormLoaded)
                        {
                            if (ViewModelLocator.Order2Static.IsFormLoaded)
                            {
                                return;
                            }

                            var order2 = new OrderContainer2View { Owner = GetWindow(this) };
                            order2.Show();
                            order2.Focus();
                            return;
                        }

                        var order = new OrderContainerView { Owner = GetWindow(this) };
                        order.Show();
                        order.Focus();
                        return;
                    }

                    if (view == ViewType.UOM)
                    {
                        var uomSetUp = new UOMContainerView { Owner = GetWindow(this) };
                        uomSetUp.Show();
                        uomSetUp.Focus();
                        return;
                    }

                    if (view == ViewType.PricingMaster)
                    {
                        var pricing = new PriceListMasterContainerView{ Owner = GetWindow(this) };
                        pricing.Show();
                        pricing.Focus();
                        return;
                    }

                    if (view == ViewType.WeightBandPricing)
                    {
                        var pricing = new WeightBandPricingView { Owner = GetWindow(this) };
                        pricing.Show();
                        pricing.Focus();
                        return;
                    }

                    if (view == ViewType.AllPrices)
                    {
                        var pricing = new AllPricesView { Owner = GetWindow(this) };
                        pricing.Show();
                        pricing.Focus();
                        return;
                    }

                    if (view == ViewType.TelesalesSearch)
                    {
                        var pricing = new Telesales.TelesalesSearchView { Owner = GetWindow(this) };
                        pricing.Show();
                        pricing.Focus();
                        return;
                    }

                    if (view == ViewType.WorkflowAlerts)
                    {
                        var flow = new WorkflowAlertsView { Owner = GetWindow(this) };
                        flow.Show();
                        flow.Focus();
                        return;
                    }

                    if (view == ViewType.StockMove)
                    {
                        var flow = new StockMoveView { Owner = GetWindow(this) };
                        flow.Show();
                        flow.Focus();
                        return;
                    }

                    if (view == ViewType.PriceListDetail)
                    {
                        if (ViewModelLocator.PriceListDetailStatic.IsFormLoaded)
                        {
                            Messenger.Default.Send(Token.Message, Token.RefreshPriceDetails);
                            return;
                        }

                        var pricing = new PriceListDetailContainerView { Owner = GetWindow(this) };
                        pricing.Show();
                        pricing.Focus();
                        return;
                    }

                    if (view == ViewType.TelesalesSetUp)
                    {
                        var sales = new Telesales.TelesalesSetUpView { Owner = GetWindow(this) };
                        sales.Show();
                        sales.Focus();
                        return;
                    }

                    if (view == ViewType.TelesalesEndCall)
                    {
                        var sales = new Telesales.TelesalesEndCallView { Owner = GetWindow(this) };
                        sales.ShowDialog();
                        sales.Focus();
                        return;
                    }

                    if (view == ViewType.Telesales)
                    {
                        var sales = new Telesales.TelesalesView { Owner = GetWindow(this) };
                        sales.Show();
                        sales.Focus();
                        return;
                    }

                    if (view == ViewType.ProductSelection)
                    {
                        var product = new ProductSelectionContainerView { Owner = GetWindow(this) };
                        product.Show();
                        product.Focus();
                        return;
                    }

                    if (view == ViewType.WarehouseLocation)
                    {
                        var location = new WarehouseLocationContainerView { Owner = GetWindow(this) };
                        location.Show();
                        location.Focus();
                        return;
                    }

                    if (view == ViewType.AttributeMasterSetUp)
                    {
                        var location = new AttributeMasterSetUpView { Owner = GetWindow(this) };
                        location.Show();
                        location.Focus();
                        return;
                    }

                    if (view == ViewType.InventoryGroup)
                    {
                        var group = new InventoryGroupMasterContainerView { Owner = GetWindow(this) };
                        group.Show();
                        group.Focus();
                        return;
                    }

                    if (view == ViewType.WeightBandGroup)
                    {
                        var group = new WeightBandGroupSetUpView { Owner = GetWindow(this) };
                        group.Show();
                        group.Focus();
                        return;
                    }

                    if (view == ViewType.WeightBand)
                    {
                        var group = new WeightBandSetUpView { Owner = GetWindow(this) };
                        group.Show();
                        group.Focus();
                        return;
                    }

                    if (view == ViewType.LairageOrder)
                    {
                        var order = new LairageOrderView { Owner = GetWindow(this) };
                        order.Show();
                        order.Focus();
                        return;
                    }

                    if (view == ViewType.LairageIntake)
                    {
                        if (ViewModelLocator.LairageStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var order = new LairageIntakeView { Owner = GetWindow(this) };
                        order.Show();
                        order.Focus();
                        return;
                    }

                    if (view == ViewType.LairageSearch)
                    {
                        if (ViewModelLocator.LairageSearchStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var order = new LairageSearchView { Owner = GetWindow(this) };
                        order.Show();
                        order.Focus();
                        return;
                    }

                    if (view == ViewType.Quote)
                    {
                        if (ViewModelLocator.QuoteStatic.IsFormLoaded)
                        {
                            return;
                        }

                        var quote = new QuoteContainerView { Owner = GetWindow(this) };
                        quote.Show();
                        quote.Focus();
                        return;
                    }

                    if (view == ViewType.Quality)
                    {
                        var quality = new QualityContainerView { Owner = GetWindow(this) };
                        quality.Show();
                        quality.Focus();
                        return;
                    }

                    if (view == ViewType.StockAdjust)
                    {
                        var stock = new StockAdjustView{ Owner = GetWindow(this) };
                        stock.Show();
                        stock.Focus();
                        return;
                    }

                    if (view == ViewType.StockTake)
                    {
                        var stock = new StocktakeView{ Owner = GetWindow(this) };
                        stock.Show();
                        stock.Focus();
                        return;
                    }

                    if (view == ViewType.StockReconciliation)
                    {
                        var stock = new StockReconciliationView { Owner = GetWindow(this) };
                        stock.Show();
                        stock.Focus();
                        return;
                    }

                    if (view == ViewType.ScannerFactory)
                    {
                        var scanner = new ScannerEmulatorFactoryView { Owner = GetWindow(this) };
                        scanner.Show();
                        scanner.Focus();
                        return;
                    }

                    if (view == ViewType.QualityTemplateName)
                    {
                        var quality = new QualityTemplateNameContainerView { Owner = GetWindow(this) };
                        quality.Show();
                        quality.Focus();
                        return;
                    }

                    if (view == ViewType.QualityTemplateAllocation)
                    {
                        var quality = new QualityTemplateAllocationContainerView { Owner = GetWindow(this) };
                        quality.Show();
                        quality.Focus();
                        return;
                    }

                    if (view == ViewType.DocumentType)
                    {
                        var type = new DocumentTypeContainerView { Owner = GetWindow(this) };
                        type.Show();
                        type.Focus();
                        return;
                    }

                    if (view == ViewType.DocumentNumber)
                    {
                        var number = new DocumentNumberContainerView { Owner = GetWindow(this) };
                        number.Show();
                        number.Focus();
                        return;
                    }

                    if (view == ViewType.EposSettings)
                    {
                        var settings = new EPOSSettingsContainerView { Owner = GetWindow(this) };
                        settings.Show();
                        settings.Focus();
                        return;
                    }

                    if (view == ViewType.InventoryGroupSetUp)
                    {
                        var group = new INGroupSetUpContainerView { Owner = GetWindow(this) };
                        group.ShowDialog();
                        group.Focus();
                        return;
                    }

                    if (view == ViewType.InventoryMaster)
                    {
                        if (ViewModelLocator.InventoryMasterStatic.IsFormLoaded)
                        {
                            // don't allow multiple instances of the item window.
                            return;
                        }

                        var master = new INMasterContainerView { Owner = GetWindow(this) };
                        master.Show();
                        master.Focus();
                        return;
                    }

                    if (view == ViewType.InventorySearchData)
                    {
                        if (ViewModelLocator.InventorySearchDataStatic.IsFormLoaded)
                        {
                            // don't allow multiple instances of the item search window.
                            return;
                        }

                        var search = new INSearchDataView { Owner = GetWindow(this) };
                        search.Show();
                        search.Focus();
                        return;
                    }

                    if (view == ViewType.Epos)
                    {
                        var epos = new EposContainerView { Owner = GetWindow(this) };
                        epos.Show();
                        epos.Focus();
                        return;
                    }

                    if (view == ViewType.VatSetUp)
                    {
                        var setUp = new VatSetUpContainerView { Owner = GetWindow(this) };
                        setUp.Show();
                        setUp.Focus();
                        return;
                    }
                });

            Messenger.Default.Register<Tuple<string, int>>(this, Token.AuditData, tuple =>
            {
                var auditSetUp = new AuditContainerView(tuple) { Owner = GetWindow(this) };
                auditSetUp.Show();
                auditSetUp.Focus();
            });

            // Create the bp search screen.
            Messenger.Default.Register<ViewType>(
                this,
                Token.SearchForBusinessPartner,
                searchType =>
                {
                    if (ViewModelLocator.BPSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(searchType, Token.SearchTerm);

                        // don't allow multiple instances of the searchwindow.
                        return;
                    }

                    if (searchType == ViewType.BPSearchData)
                    {
                        // open the bp search screen
                        var searchDataScreen = new BPSearchDataView();
                        searchDataScreen.Owner = GetWindow(this);
                        searchDataScreen.Show();
                        searchDataScreen.Focus();
                    }
                    else
                    {
                        // open the sales customer search screeen
                        var searchDataScreen = new SalesCustomerSearchView();
                        searchDataScreen.Owner = GetWindow(this);
                        searchDataScreen.Show();
                        searchDataScreen.Focus();
                    }
                 
                    // let the shared vm know which is the current master search window.
                    Messenger.Default.Send(searchType, Token.SearchTerm);
                });

            Messenger.Default.Register<string>(
                this,
                Token.DisplayNotes,
                s =>
                {
                    var notes = new NotesView(s) { Owner = GetWindow(this) };
                    notes.ShowDialog();
                    notes.Focus();
                });

            // Create the sales search screen.
            Messenger.Default.Register<Tuple<IList<Sale>,ViewType>>(
                this,
                Token.SearchForSale,
                sales =>
                {
                    if (NouvemGlobal.SalesSearchScreenOpen)
                    {
                        Messenger.Default.Send(sales, Token.DisplayCustomerSales);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new SaleSearchDataView(sales.Item2);
                    searchDataScreen.Owner = GetWindow(this);
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.SalesSearchDataStatic.SetDisplaySales(sales.Item1);
                    //Messenger.Default.Send(sales, Token.DisplayCustomerSales);
                });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForSaleMultiSelect,
                sales =>
                {
                    if (ViewModelLocator.SalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayCustomerSales);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new SalesSearchDataMultiSelectView();
                    searchDataScreen.Owner = GetWindow(this);
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.SalesSearchDataStatic.SetDisplaySales(sales);
                    //Messenger.Default.Send(sales, Token.DisplayCustomerSales);
                });


            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForReportSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayReportData);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new ReportSaleSearchDataView();
                    searchDataScreen.Owner = GetWindow(this);
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                });

            // Create the sales search screen.
            Messenger.Default.Register<List<Sale>>(
                this,
                Token.CreateInvoice,
                sales =>
                {
                    var invoice = new InvoiceCreationView {Owner = this};
                    invoice.Show();
                    invoice.Focus();
                });

            Messenger.Default.Register<List<Sale>>(
                this,
                Token.CreateAPInvoice,
                sales =>
                {
                    var invoice = new APInvoiceCreationView { Owner = this };
                    invoice.Show();
                    invoice.Focus();
                });

            Messenger.Default.Register<List<Sale>>(
                this,
                Token.CreateCreditNote,
                sales =>
                {
                    var invoice = new CreditNoteCreationView { Owner = this };
                    invoice.Show();
                    invoice.Focus();
                });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForProductionSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayCustomerSales);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new ProductionSearchDataView();
                    searchDataScreen.Owner = GetWindow(this);
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.ShowAllOrders = true;
                    ViewModelLocator.ReportSalesSearchDataStatic.DontFilterByDates = true;
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                    //Messenger.Default.Send(sales, Token.DisplayReportData);
                });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForTransactionSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayCustomerSales);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new TransactionSearchDataView();
                    searchDataScreen.Owner = GetWindow(this);
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForCarcassSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayReportData);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new ReportCarcassSearchDataView();
                    searchDataScreen.Owner = GetWindow(this);
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                });

            Messenger.Default.Register<string>(
            this, 
            Token.DisplaySql, 
            s =>
            {
                var sql = new SqlGeneratorContainerView { Owner = GetWindow(this) };
                if (!string.IsNullOrWhiteSpace(s))
                {
                    Messenger.Default.Send(s, Token.SetSql);
                }

                sql.ShowDialog();
                sql.Focus();
            });

            Messenger.Default.Register<int>(
                this,
                Token.ShowReceipe,
                i =>
                {
                    var receipe = new ReceipeView(i);
                    receipe.ShowDialog();
                    receipe.Focus();
                });

            Messenger.Default.Register<string>(
            this,
            Token.DisplayCol,
            s =>
            {
                var data = new DataGeneratorContainerView { Owner = GetWindow(this) };
                if (!string.IsNullOrWhiteSpace(s))
                {
                    Messenger.Default.Send(s, Token.SetData);
                }

                data.ShowDialog();
                data.Focus();
            });

            Messenger.Default.Register<ReportData>(this, ViewType.ReportSearch, o =>
            {
                var bpGroup = new ReportSearchView(o) { Owner = GetWindow(this) };
                bpGroup.Show();
                bpGroup.Focus();
                return;
            });

            Messenger.Default.Register<ReportData>(this, Token.MultiReport, r =>
            {
                var report = new Report.ReportViewer(r) { Owner = GetWindow(this) } ;
                report.Show();
                report.Focus();
                return;
            });


            Messenger.Default.Register<Tuple<int?,IList<App_GetOrderBatchData_Result>>>(this, ViewType.BatchData, r =>
            {
                var report = new BatchDataView(r) { Owner = GetWindow(this) };
                report.Show();
                report.Focus();
                return;
            });

            Messenger.Default.Register<ViewType>(
            this,
            Token.DisplaySaleSearchScreen,
            v =>
            {
                if (v == ViewType.ARDispatch)
                {
                    if (ViewModelLocator.ARDispatchStatic.IsFormLoaded)
                    {
                        return;
                    }

                    var localDispatch = new ARDispatchContainerView { Owner = GetWindow(this) };
                    localDispatch.Show();
                    localDispatch.Focus();
                    return;
                }

                if (v == ViewType.ARDispatch2)
                {
                    if (ViewModelLocator.ARDispatch2Static.IsFormLoaded)
                    {
                        return;
                    }

                    var dispatch2 = new ARDispatchContainer2View { Owner = GetWindow(this) };
                    dispatch2.Show();
                    dispatch2.Focus();
                    return;
                }

                if (v == ViewType.Order)
                {
                    if (ViewModelLocator.OrderStatic.IsFormLoaded)
                    {
                        return;
                    }

                    var order = new OrderContainerView { Owner = GetWindow(this) };
                    order.Show();
                    order.Focus();
                    return;
                }

                if (v == ViewType.Order2)
                {
                    if (ViewModelLocator.Order2Static.IsFormLoaded)
                    {
                        return;
                    }

                    var order2 = new OrderContainer2View { Owner = GetWindow(this) };
                    order2.Show();
                    order2.Focus();
                    return;
                }
               
            });
        }
    }
}

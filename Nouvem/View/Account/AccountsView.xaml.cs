﻿// -----------------------------------------------------------------------
// <copyright file="AccountsView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.Account
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for AccountsView.xaml
    /// </summary>
    public partial class AccountsView : Window
    {
        public AccountsView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.AccountsMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseAccountsWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            var gridPath = Settings.Default.AccountsGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Closing += (sender, args) =>
            {
                WindowSettings.AccountsHeight = this.Height;
                WindowSettings.AccountsLeft = this.Left;
                WindowSettings.AccountsTop = this.Top;
                WindowSettings.AccountsWidth = this.Width;
                WindowSettings.AccountsMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlSearchData.SelectAll();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.GridControlSearchData.IsGroupRowHandle(this.TableView.FocusedRowHandle))
            {
                object obj = this.GridControlSearchData.GetGroupRowValue(this.TableView.FocusedRowHandle);

                var c = obj;

                if (c.ToString() == "45")
                {
                    MessageBox.Show(c.ToString());
                }
            }
        }}
}

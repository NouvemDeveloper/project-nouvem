﻿// -----------------------------------------------------------------------
// <copyright file="AccountsView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.Account
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ContainerAccountsView.xaml
    /// </summary>
    public partial class ContainerAccountsView : Window
    {
        public ContainerAccountsView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseAccountsWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(Settings.Default.ContainerAccountsPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.ContainerAccountsPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(Settings.Default.ContainerAccountsPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.GridControlSearchData.IsGroupRowHandle(this.TableView.FocusedRowHandle))
            {
                object obj = this.GridControlSearchData.GetGroupRowValue(this.TableView.FocusedRowHandle);

                var c = obj;

                if (c.ToString() == "45")
                {
                    MessageBox.Show(c.ToString());
                }
            }
        }
    }
}

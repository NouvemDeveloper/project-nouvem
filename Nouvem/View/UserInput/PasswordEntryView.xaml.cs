﻿// -----------------------------------------------------------------------
// <copyright file="PasswordEntryView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.UserInput
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.ViewModel;

    /// <summary>
    /// Interaction logic for PasswordEntryView.xaml
    /// </summary>
    public partial class PasswordEntryView : Window
    {
        /// <summary>
        /// Initializes a new instance of the PasswordEntry class.
        /// </summary>
        /// <param name="checkType">The password type to check.</param>
        public PasswordEntryView(PasswordCheckType checkType)
        {
            this.InitializeComponent();
            ViewModelLocator.PasswordEntryStatic.CheckType = checkType;
            Messenger.Default.Register<string>(this, Token.ClosePasswordEntryWindow, x => this.Close());
            this.PasswordBoxEdit.Focus();
        }
    }
}

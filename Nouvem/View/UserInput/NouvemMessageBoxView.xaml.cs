﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBoxView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Logging;

namespace Nouvem.View.UserInput
{
    using System;
    using System.Windows;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for NouvemMessageBoxView.xaml
    /// </summary>
    public partial class NouvemMessageBoxView
    {
        /// <summary>
        /// timer used for the flash message
        /// </summary>
        private DispatcherTimer timer;

        public NouvemMessageBoxView(Tuple<bool, bool, bool, bool, bool, bool> instructions)
        {
            try
            {
                this.InitializeComponent();

                var isTouchScreen = instructions.Item1;
                var flashMessage = instructions.Item2;
                var isScanner = instructions.Item3;
                var isLeft = instructions.Item4;
                var showCounter = instructions.Item5;
                var isPopUp = instructions.Item6;
                this.GridCounter.Visibility = Visibility.Collapsed;
                this.CheckboxDispatch.Visibility = Visibility.Collapsed;
                this.CheckboxDispatchDetail.Visibility = Visibility.Collapsed;
                this.CheckboxInvoice.Visibility = Visibility.Collapsed;

                this.Height = ApplicationSettings.MessageBoxHeight <= 0 ? 300 : ApplicationSettings.MessageBoxHeight;
                this.Width = ApplicationSettings.MessageBoxWidth <= 0 ? 900 : ApplicationSettings.MessageBoxWidth;
                this.TextBlockMessage.FontSize = ApplicationSettings.MessageBoxFontSize <= 0 ? 28 : ApplicationSettings.MessageBoxFontSize;

                if (ApplicationSettings.TouchscreenMessageBoxFontSize <= 0)
                {
                    ApplicationSettings.TouchscreenMessageBoxFontSize = 28;
                }

                if (isLeft && ApplicationSettings.AllowLeftMessageBox)
                {
                    this.WindowStartupLocation = WindowStartupLocation.Manual;
                    this.Left = ApplicationSettings.MessageBoxLeft <= 0 ? 50 : ApplicationSettings.MessageBoxLeft;
                    this.Top = ApplicationSettings.MessageBoxTop <= 0 ? 50 : ApplicationSettings.MessageBoxTop;
                }
                else
                {
                    this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                }

                if (isScanner)
                {
                    this.Height = ApplicationSettings.ScannerMessageBoxHeight <= 0 ? 180 : ApplicationSettings.ScannerMessageBoxHeight;
                    this.Width = ApplicationSettings.ScannerMessageBoxWidth <= 0 ? 230 : ApplicationSettings.ScannerMessageBoxWidth;
                    this.ButtonLeft.Height = 30;
                    this.ButtonRight.Height = 30;
                    this.ButtonSpare.Height = 30;
                    this.TextBlockMessage.FontSize = ApplicationSettings.ScannerMessageBoxFontSize <= 0 ? 12 : ApplicationSettings.ScannerMessageBoxFontSize;
                }
                else if (isTouchScreen)
                {
                    this.Height = ApplicationSettings.TouchscreenMessageBoxHeight <= 0 ? 300 : ApplicationSettings.TouchscreenMessageBoxHeight;
                    this.Width = ApplicationSettings.TouchscreenMessageBoxWidth <= 0 ? 900 : ApplicationSettings.TouchscreenMessageBoxWidth;
                    this.ButtonLeft.Height = 60;
                    this.ButtonRight.Height = 60;
                    this.ButtonSpare.Height = 60;
                    this.TextBlockMessage.FontSize = ApplicationSettings.TouchscreenMessageBoxFontSize;
                }

                if (isPopUp)
                {
                    this.Height = ApplicationSettings.PopUpNoteHeight <= 0 ? 500 : ApplicationSettings.PopUpNoteHeight;
                    this.Width = ApplicationSettings.PopUpNoteWidth <= 0 ? 300 : ApplicationSettings.PopUpNoteWidth;
                    this.ButtonLeft.Height = 60;
                    this.ButtonRight.Height = 60;
                    this.ButtonSpare.Height = 60;
                    this.TextBlockMessage.FontSize = ApplicationSettings.PopUpNoteFontSize <= 0 ? 28 : ApplicationSettings.PopUpNoteFontSize;
                }

                if (flashMessage)
                {
                    this.Height = ApplicationSettings.TouchscreenMessageBoxHeight <= 0 ? 300 : ApplicationSettings.TouchscreenMessageBoxHeight;
                    this.Width = ApplicationSettings.TouchscreenMessageBoxWidth <= 0 ? 900 : ApplicationSettings.TouchscreenMessageBoxWidth;
                    this.ButtonLeft.Height = 60;
                    this.ButtonRight.Height = 60;
                    this.ButtonSpare.Height = 60;
                    this.TextBlockMessage.FontSize = ApplicationSettings.TouchscreenMessageBoxFontSize;
                    this.ButtonLeft.Visibility = Visibility.Hidden;
                    this.ButtonRight.Visibility = Visibility.Hidden;
                    this.timer = new DispatcherTimer();
                    this.timer.Interval = TimeSpan.FromSeconds(ApplicationSettings.MessageBoxFlashTime);
                    this.timer.Tick += this.TimerOnTick;
                    this.timer.Start();
                }

                if (showCounter)
                {
                    this.GridCounter.Visibility = Visibility.Visible;
                    this.CheckboxDispatch.Visibility = Visibility.Visible;
                    this.CheckboxDispatchDetail.Visibility = Visibility.Visible;
                    this.CheckboxInvoice.Visibility = Visibility.Visible;
                    this.CheckboxDispatch.IsChecked = ApplicationSettings.AutoPrintDispatchReport;
                    this.CheckboxDispatchDetail.IsChecked = ApplicationSettings.AutoPrintDispatchDetailReport;
                    this.CheckboxInvoice.IsChecked = ApplicationSettings.AutoPrintInvoiceReport;
                }

                this.StateChanged += (sender, args) =>
                {
                    if (this.WindowState != WindowState.Normal)
                    {
                        this.WindowState = WindowState.Normal;
                        this.Activate();
                        this.Topmost = true;
                    }
                };

                //this.Loaded += (sender, args) =>
                //{
                //    this.Activate();
                //    this.Topmost = false;
                //    this.Topmost = true;
                //};

                this.Closing += (sender, args) =>
                {
                    try
                    {
                        Messenger.Default.Unregister(this);
                        if (this.timer != null)
                        {
                            this.timer.Stop();
                            this.timer.Tick -= this.TimerOnTick;
                            this.timer = null;
                        }
                    }
                    catch (Exception e)
                    {
                        var log = new Logger();
                        log.LogError(this.GetType(), e.Message);
                    }

                };

                Messenger.Default.Register<string>(this, Token.CloseMessageBoxWindow, x => this.Close());
            }
            catch (Exception e)
            {
                var log = new Logger();
                log.LogError(this.GetType(), e.Message);
            }
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            this.Close();
        }
    }
}






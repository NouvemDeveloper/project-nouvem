﻿// -----------------------------------------------------------------------
// <copyright file="ControlView.cs="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.UserInput
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for CrudView.xaml
    /// </summary>
    public partial class ControlView : UserControl
    {
        public ControlView()
        {
            this.InitializeComponent();
        }
   }
 }


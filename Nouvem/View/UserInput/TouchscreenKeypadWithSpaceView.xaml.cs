﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenKeypadWithSpaceView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.UserInput
{
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenKeypadView.xaml
    /// </summary>
    public partial class TouchscreenKeypadWithSpaceView : UserControl
    {
        public TouchscreenKeypadWithSpaceView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles a touch pad entry.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;

            if (button == null)
            {
                return;
            }

            Messenger.Default.Send(button.CommandParameter.ToString(), Token.TouchscreenKeyPadEntry);
        }
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="NavigationButtonsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.UserInput
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for NavigationButtonsView.xaml
    /// </summary>
    public partial class NavigationButtonsView : UserControl
    {
        public NavigationButtonsView()
        {
            this.InitializeComponent();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DeviceSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.License
{
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for DeviceSetUpView.xaml
    /// </summary>
    public partial class DeviceSetUpView : UserControl
    {
        public DeviceSetUpView()
        {
            this.InitializeComponent();

            ThreadPool.QueueUserWorkItem(x => this.TextBoxMac.Dispatcher.Invoke(() => this.TextBoxMac.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxMac.Focus());
            
        }
    }
}

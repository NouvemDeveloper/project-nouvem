﻿// -----------------------------------------------------------------------
// <copyright file="LicenseAdminView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.License
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LicenseImportView.xaml
    /// </summary>
    public partial class LicenseImportView : UserControl
    {
        public LicenseImportView()
        {
            this.InitializeComponent();
            this.Unloaded += (sender, args) => this.GridControlLicenses.SaveLayoutToXml(Settings.Default.LicensesGridPath);

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.LicensesGridPath))
                {
                    this.GridControlLicenses.RestoreLayoutFromXml(Settings.Default.LicensesGridPath);
                }
            };
        }
    }
}

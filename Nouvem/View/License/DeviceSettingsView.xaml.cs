﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.License;

namespace Nouvem.View.License
{
    /// <summary>
    /// Interaction logic for DeviceSettingsView.xaml
    /// </summary>
    public partial class DeviceSettingsView : Window
    {
        public DeviceSettingsView()
        {
            this.InitializeComponent();
            this.DataContext = new DeviceSettingsViewModel();
            this.WindowState = WindowSettings.DeviceSettingsMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseDeviceSettingsWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.Closing += (sender, args) =>
            {
                WindowSettings.DeviceSettingsHeight = this.Height;
                WindowSettings.DeviceSettingsLeft = this.Left;
                WindowSettings.DeviceSettingsTop = this.Top;
                WindowSettings.DeviceSettingsWidth = this.Width;
                WindowSettings.DeviceSettingsMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="App.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using DevExpress.Xpf.Core;
using Microsoft.Win32;
using Nouvem.Model.BusinessObject;
using Nouvem.View.Attribute;
using Nouvem.View.Inventory;
using Nouvem.View.KillLine.Grader;
using Nouvem.View.Plant;
using Nouvem.View.Production;
using Nouvem.View.Purchases.APReceipt;
using Nouvem.View.Report;
using Nouvem.View.Sales;
using Nouvem.View.Sales.ARDispatch;
using Nouvem.View.Sales.ARInvoice;
using Nouvem.View.Stock;
using Nouvem.View.Touchscreen;
using Nouvem.View.Transaction;
using Nouvem.View.Utility;
using Nouvem.View.Workflow;

namespace Nouvem
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Global;
    using Nouvem.Logging;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.View.Logic;
    using Nouvem.View.Master;
    using Nouvem.View.Scanner;
    using Nouvem.View.StartUp;
    using Nouvem.View.User;
    using Nouvem.View.UserInput;
    using ViewModelLocator = Nouvem.ViewModel.ViewModelLocator;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Mutex reference used to detect multiple instances of Nouvem.
        /// </summary>
        static readonly Mutex singleton = new Mutex(true, "Nouvem");

        /// <summary>
        /// The application logging reference.
        /// </summary>
        private readonly Logger log = new Logger();

        /// <summary>
        /// The receipt details window.
        /// </summary>
        private Window orders;

        /// <summary>
        /// Overriden base startup.
        /// </summary>
        /// <param name="e">Start up event argument.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            var terminal = new TerminalServicesManager();
            terminal.Start();

            //if (SingleApplicationDetector.IsRunning())
            //{
            //    return;
            //}
            //SingleApplicationDetector.Close();

            //if (!Settings.Default.AllowMultipleInstancesOfApplication && !Settings.Default.OverrideStartUpCheck)
            //{
            //    var nouvemProcessName = Process.GetCurrentProcess().ProcessName;
            //    this.Log.LogDebug(this.GetType(), string.Format("Checking if nouvem is already running: Process name:{0}", nouvemProcessName));

            //    if (Process.GetProcesses().Count(p => p.ProcessName.Equals(nouvemProcessName)) > 1)
            //    {
            //        if (NouvemGlobal.IsRDPConnection)
            //        {
            //            if (alreadyLoggedInOverRdp)
            //            {
            //                this.Log.LogDebug(this.GetType(), "Process already running over rdp. Shutting down.");
            //                Current.Shutdown();
            //                Process.GetCurrentProcess().Kill();
            //                return;
            //            }
            //        }
            //        else
            //        {
            //            this.Log.LogDebug(this.GetType(), "Process already running. Shutting down.");
            //            Current.Shutdown();
            //            Process.GetCurrentProcess().Kill();
            //            return;
            //        }
            //    }
            //}
            //else
            //{
            //    Settings.Default.AllowMultipleInstancesOfApplication = false;
            //    Settings.Default.Save();
            //}

            var splash = new ApplicationLoadingView();
            splash.Show();
            base.OnStartup(e);

            //var worker = new BackgroundWorker();
            //worker.DoWork += (sender, args) =>
            //Task.Factory.StartNew(() =>
            //{
                this.RegisterForUICreation();
                this.GenerateNouvemPassword();
                EntityConnection.GetConnectionSettings();
                if (this.CheckDatabaseConnection())
                {
                    //Task.Factory.StartNew(() => EntityConnection.CreateConnectionString());
                    this.Dispatcher.Invoke(() =>
                    {
                        this.BeginDataProcessing();
                    });
                }

                //if (Settings.Default.EposMode)
                //{
                //    this.StartupUri = new Uri("View/EPOS/EposContainerView.xaml", UriKind.Relative);
                //} else
                if (ApplicationSettings.TouchScreenModeOnly)
                {
                    //this.StartupUri = new Uri("View/StartUp/TouchscreenLoginView.xaml", UriKind.Relative);
                    this.Dispatcher.Invoke(() =>
                    {
                        var start = new TouchscreenLoginView();
                        start.Show();
                    });
                }
                else if (ApplicationSettings.ScannerMode)
                {
                    //this.StartupUri = new Uri("View/Scanner/ScannerLogonView.xaml", UriKind.Relative);
                    this.Dispatcher.Invoke(() =>
                    {
                        var start = new ScannerLogonView();
                        start.Show();
                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        var start = new LoginContainerView();
                        start.Show();
                    });
                   
                    //this.StartupUri = new Uri("View/StartUp/LoginContainerView.xaml", UriKind.Relative);
                }
            //});

            //worker.RunWorkerAsync();
        }

        /// <summary>
        /// Handler for the application unhandled exceptions.
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">The event args</param>
        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // log it
            this.log.LogError(this.GetType(), string.Format("OnDispatcherUnhandledException()  Message: {0}, Inner Exception: {1},  Stack Trace: {2}", e.Exception.Message, e.Exception.InnerException, e.Exception.StackTrace));
            try
            {
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            }
            catch (ConfigurationErrorsException exception)
            {
                this.log.LogError(this.GetType(), "Config settings corrupt");
                try
                {
                    File.Delete(exception.Filename);
                }
                catch (Exception ex)
                {
                    this.log.LogError(this.GetType(), "Config settings corrupt");
                }
           
            }

            Application.Current.Shutdown();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        /// <summary>
        /// Register for the entry ui views creation.
        /// </summary>
        private void RegisterForUICreation()
        {
            this.CreateSplashScreen();
            this.CreateMaster();
            this.CreateLoginSelection();
            this.CreatePasswordView();
            this.CreateServerSetUp();
            this.UserChangePassword();
            this.CreateMessageBoxView();
            this.CreateScannerModuleSelection();
            this.CreateScannerFactory();
            this.CreateTouchscreenModuleOnlyModule();
            this.CreateTouchScreenControl();
            this.CreateTouchscreenModuleSelection();
            this.CreateLabelDisplay();
            this.CreateGenericDisplay();
            this.CreateTouchscreenOrdersView();
            this.CreateTouchscreenLogin();
            this.RegisterForTouchscreenReportSearch();
            this.CreateTouchscreenUsers();
            this.CreateTareCalculator();
        }

        /// <summary>
        /// Message registration handler, for the master ui creation.
        /// </summary>
        private void CreateSplashScreen()
        {
            Messenger.Default.Register<ReportData>(this, Token.MultiReportTouchscreen, r =>
            {
                var report = new View.Report.ReportViewer(r);
                report.Show();
                report.Focus();
                return;
            });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateSequencerCategory,
                x =>
                {
                    var splash = new SequencerCategoriesView();
                    splash.ShowDialog();
                    splash.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerFactorySequencer,
                x =>
                {
                    var scanner = new ScannerSequencerView();
                    scanner.ShowDialog();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateSplashScreen,
                x =>
                {
                    var splash = new NouvemSplashScreenView();
                    splash.ShowDialog();
                    splash.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.ShowStockTake,
                i =>
                {
                    var stock = new TouchscreenStockTakeView();
                    stock.Show();
                    stock.Focus();
                    stock.WindowState = WindowState.Maximized;
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateSpecs,
                x =>
                {
                    var splash = new TouchscreenSpecsView();
                    splash.Show();
                    splash.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateIndicator,
                x =>
                {
                    var splash = new TouchscreenIndicatorView();
                    splash.Show();
                    splash.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateCalender,
                x =>
                {
                    var splash = new CalenderView(x);
                    splash.Show();
                    splash.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateAttributeResponse,
                x =>
                {
                    var splash = new AttributeResponseView();
                    splash.Show();
                    splash.Focus();
                });

            Messenger.Default.Register<string>(
               this,
               Token.CreateWorkflow,
               x =>
               {
                   var flow = new WorkflowView();
                   flow.ShowDialog();
                   flow.Focus();
               });

            Messenger.Default.Register<string>(
               this,
               Token.CreateWorkflowTemplateSelection,
               x =>
               {
                   var flow = new WorkflowTemplateSelectionView();
                   flow.Show();
                   flow.Focus();
               });

            Messenger.Default.Register<string>(
               this,
               Token.CreateWorkflowSelection,
               x =>
               {
                   var flow = new WorkflowSelectionView();
                   flow.Show();
                   flow.Focus();
               });

            Messenger.Default.Register<string>(
               this,
               Token.CreateSystemInformation,
               x =>
               {
                   var flow = new SystemInformationView();
                   flow.Show();
                   flow.Focus();
               });

            Messenger.Default.Register<string>(
               this,
               Token.CreateMenu,
               x =>
               {
                   var flow = new MenuContainerView();
                   flow.Show();
                   flow.Focus();
               });
        }

        /// <summary>
        /// Message registration handler, for the master ui creation.
        /// </summary>
        private void CreateMaster()
        {
            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateMasterView,
                x =>
                {
                    if (ViewModelLocator.MasterStatic.IsFormLoaded)
                    {
                        // don't allow multiple instances of the master window (this will only be an issue when in epos mode)
                        return;
                    }

                    var master = new MasterWindowView();
                    master.Show();
                    master.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for the scanner module selection ui creation.
        /// </summary>
        private void CreateScannerModuleSelection()
        {
            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerModuleSelection,
                x =>
                {
                    var scanner = new ScannerModuleSelectionView();
                    if (ApplicationSettings.ScannerEmulatorMode)
                    {
                        scanner.ShowDialog();
                    }
                    else
                    {
                        scanner.Show();
                    }

                    scanner.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for the scanner module selection ui creation.
        /// </summary>
        private void CreateTouchscreenModuleOnlyModule()
        {
            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenModuleOnlyMode,
                x =>
                {
                    var factory = new FactoryScreenContainerView();
                    factory.ShowDialog();
                    factory.Focus();
                    return;
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenWarehouse,
                x =>
                {
                    var module = new TouchscreenWarehouseView();
                    module.Show();
                    module.Focus();
                });

            Messenger.Default.Register<string>(
               this,
               Token.CreatePrinterSelection,
               x =>
               {
                   var factory = new PrinterSelectionView();
                   factory.Show();
                   factory.Focus();
                   return;
               });
        }

        /// <summary>
        /// Creates one of the touchscreen traceability selection controls.
        /// </summary>
        private void CreateTouchScreenControl()
        {
            Messenger.Default.Register<ViewType>(this, v =>
            {
                if (v == ViewType.CollectionDisplay)
                {
                    var display = new CollectionDisplayContainerView();
                    display.ShowDialog();
                    display.Focus();
                    return;
                }

                if (v == ViewType.RoutesDisplay)
                {
                    var display = new RoutesView();
                    display.ShowDialog();
                    display.Focus();
                    return;
                }

                if (v == ViewType.CollectionDisplayDesktop)
                {
                    var display = new CollectionDisplayContainerView(true);
                    display.ShowDialog();
                    display.Focus();
                    return;
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowNotesWindow, v =>
            {
                var display = new TouchscreenNotesView();
                display.Show();
                display.Focus();
                return;
            });

            Messenger.Default.Register<string>(this, Token.ShowScannerNotesWindow, v =>
            {
                var display = new TouchscreenScannerNotesView();
                display.Show();
                display.Focus();
                return;
            });

            Messenger.Default.Register<string>(this, Token.ShowDetainCondemn, s =>
            {
                var display = new DetainCondemnView();
                display.Show();
                display.Focus();
                return;
            });

            Messenger.Default.Register<ViewType>(this, v =>
            {
                if (v == ViewType.ReportDisplay)
                {
                    var display = new ReportDisplayView();
                    display.Show();
                    display.Focus();
                    return;
                }
            });

            Messenger.Default.Register<ViewType>(this, v =>
            {
                if (v == ViewType.Keyboard)
                {
                    var keyboard = new KeyboardContainerView(ViewType.TouchscreenTraceability);
                    keyboard.ShowDialog();
                    keyboard.Focus();
                    return;
                }

                if (v == ViewType.KeyboardSmall)
                {
                    var keyboard = new KeyboardSmallContainerView(ViewType.ScannerIntoProduction);
                    keyboard.ShowDialog();
                    keyboard.Focus();
                    return;
                }
            });

            Messenger.Default.Register<Tuple<ViewType, ViewType>>(this, v =>
            {
                if (v.Item1 == ViewType.Keyboard)
                {
                    var keyboard = new KeyboardContainerView(v.Item2);
                    keyboard.ShowDialog();
                    keyboard.Focus();
                    return;
                }

                if (v.Item1 == ViewType.KeyboardPassword)
                {
                    var keyboard = new KeyboardContainerView(v.Item2, true);
                    keyboard.ShowDialog();
                    keyboard.Focus();
                    return;
                }
            });
        }

        /// <summary>
        /// Creates a scanner factory.
        /// </summary>
        private void CreateScannerFactory()
        {
            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerFactory,
                x =>
                {
                    if (ViewModelLocator.FactoryScreenStatic.IsFormLoaded)
                    {
                        ViewModelLocator.FactoryScreenStatic.MainDisplayViewModel = ViewModelLocator.ScannerTouchscreenStatic;
                        ViewModelLocator.ScannerTouchscreenStatic.CoreDisplayViewModel = ViewModelLocator.ScannerMainDispatchStatic;
                        return;
                    }

                    ViewModelLocator.FactoryScreenStatic.MainDisplayViewModel = ViewModelLocator.ScannerTouchscreenStatic;
                    ViewModelLocator.ScannerTouchscreenStatic.CoreDisplayViewModel = ViewModelLocator.ScannerMainDispatchStatic;
                    var scanner = new ScannerFactoryView();
                    scanner.Show();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerFactoryStockTake,
                x =>
                {
                    if (ViewModelLocator.FactoryScreenStatic.IsFormLoaded)
                    {
                        ViewModelLocator.FactoryScreenStatic.MainDisplayViewModel = ViewModelLocator.StockTakeStatic;
                        return;
                    }

                    var scanner = new ScannerFactoryView();
                    ViewModelLocator.FactoryScreenStatic.MainDisplayViewModel = ViewModelLocator.StockTakeStatic;
                    //ViewModelLocator.ScannerTouchscreenStatic.CoreDisplayViewModel = ViewModelLocator.StockTakeStatic;
                    scanner.Show();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerBatchSetUp,
                x =>
                {
                    var scanner = new ScannerProductionbatchSetUpView();
                    scanner.ShowDialog();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerStockMovement,
                x =>
                {
                    if (ViewModelLocator.FactoryScreenStatic.IsFormLoaded)
                    {
                        ViewModelLocator.FactoryScreenStatic.MainDisplayViewModel = ViewModelLocator.StockMovementStatic;
                        return;
                    }

                    var scanner = new ScannerFactoryView();
                    ViewModelLocator.FactoryScreenStatic.MainDisplayViewModel = ViewModelLocator.StockMovementStatic;
                    //ViewModelLocator.ScannerTouchscreenStatic.CoreDisplayViewModel = ViewModelLocator.StockTakeStatic;
                    scanner.Show();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreatePalletisation,
                x =>
                {
                    var scanner = new ScannerPalletisationView();
                    scanner.Show();
                    scanner.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenPalletisation,
                x =>
                {
                    var scanner = new TouchscreenPalletisationView();
                    scanner.Show();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerIntoProduction,
                x =>
                {
                    var scanner = new ScannerIntoProduction();
                    scanner.Show();
                    scanner.Focus();
                });

            // Create the main window (Splash screen has unloaded)
            Messenger.Default.Register<string>(
                this,
                Token.CreateScannerStockMoveOrders,
                x =>
                {
                    var scanner = new ScannerStockMoveOrdersView();
                    scanner.Show();
                    scanner.Focus();
                });
        }

        /// <summary>
        /// Creates a label display window.
        /// </summary>
        private void CreateLabelDisplay()
        {
            Messenger.Default.Register<string>(
                this,
                Token.DisplayLabelSelection,
                x =>
                {
                    var label = new LabelSelectionView();
                    label.Show();
                    label.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.DisplayProcessSelection,
                x =>
                {
                    var label = new ProcessSelectionView();
                    label.Show();
                    label.Focus();
                });
        }

        /// <summary>
        /// Creates a label display window.
        /// </summary>
        private void CreateGenericDisplay()
        {
            Messenger.Default.Register<string>(
                this,
                Token.DisplayGenericSelection,
                x =>
                {
                    var label = new WarehouseSelectionView();
                    label.Show();
                    label.Focus();
                });
        }

        /// <summary>
        /// Creates a label display window.
        /// </summary>
        private void CreateTouchscreenUsers()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenUsers,
                x =>
                {
                    var users = new TouchscreenUsers();
                    users.Show();
                    users.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for the login selection creation.
        ///  </summary>
        private void CreateLoginSelection()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateLoginSelectionView,
                x =>
                {

                    var login = new LoginSelectionContainerView();
                    login.ShowDialog();
                    login.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for the server set up creation.
        /// </summary>
        private void CreateServerSetUp()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateServerSetUp,
                x =>
                {

                    var setup = new ServerSetUpContainerView();
                    setup.Show();
                    setup.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateServerSetUpScanner,
                x =>
                {

                    var setup = new ServerSetUpScannerView();
                    setup.Show();
                    setup.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for the server set up creation.
        /// </summary>
        private void CreateTareCalculator()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateTareCalculator,
                x =>
                {

                    var setup = new TareCalculatorView();
                    setup.Show();
                    setup.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for ttouchscreen log in view call.
        /// </summary>
        private void CreateTouchscreenLogin()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenLogin,
                x =>
                {

                    var login = new TouchscreenLoginView();
                    login.Show();
                    login.Focus();
                    Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                });
        }

        /// <summary>
        /// Message registration handler, for the server set up creation.
        /// </summary>
        private void CreateTouchscreenModuleSelection()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenPartners,
                x =>
                {
                    var module = new TouchscreenSuppliersView();
                    module.Show();
                    module.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenOrder,
                x =>
                {
                    var module = new TouchscreenOrdersView();
                    module.Show();
                    module.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenProductionOrder,
                x =>
                {
                    var module = new TouchscreenProductionOrdersView();
                    module.Show();
                    module.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenIntakeBatches,
                x =>
                {
                    var module = new TouchscreenIntakeBatchesView();
                    module.Show();
                    module.Focus();
                });

            Messenger.Default.Register<string>(
                this,
                Token.CreateTouchscreenModuleSelection,
                x =>
                {

                    var module = new SwitchTouchscreenModuleView();
                    module.Show();
                    module.Focus();
                });

            // Create the sales search screen.
            Messenger.Default.Register<string>(
                this,
                Token.DisplaySearchKeyboard,
                s =>
                {
                    if (ViewModelLocator.KeyboardStatic.IsFormLoaded)
                    {
                        return;
                    }

                    var key = new KeyboardSearchView();
                    key.Show();
                    key.Focus();
                });

            // Create the touchscreen report viewer.
            Messenger.Default.Register<string>(
                this,
                Token.DisplayTouchscreenReportViewer,
                s =>
                {
                    var key = new TouchscreenReportView();
                    key.Show();
                    key.Focus();
                });
        }

        /// <summary>
        /// Message registration handler, for the user change password functionality.
        /// </summary>
        private void UserChangePassword()
        {
            Messenger.Default.Register<string>(
                this,
                Token.CreateUserPasswordChange,
                x =>
                {
                    if (ApplicationSettings.TouchScreenModeOnly)
                    {
                        var change = new UserChangePasswordTouchcreenView();
                        change.ShowDialog();
                        change.Focus();
                    }
                    else
                    {
                        var change = new UserChangePasswordContainerView();
                        change.ShowDialog();
                        change.Focus();
                    }
                });
        }

        /// <summary>
        /// Message registration handler, for the pssword box creation.
        /// </summary>
        private void CreatePasswordView()
        {
            Messenger.Default.Register<PasswordCheckType>(
                this,
                x =>
                {

                    var password = new View.UserInput.PasswordEntryView(x);
                    password.ShowDialog();
                    password.Focus();
                });
        }

        private bool processing;
        /// <summary>
        /// Message registration handler, for the password box creation.
        /// </summary>
        private void CreateMessageBoxView()
        {
            Messenger.Default.Register<Tuple<bool, bool, bool, bool, bool, bool>>(
                this,
                Token.OpenMessageBox,
                x =>
                {
                    if (this.processing)
                    {
                        return;
                    }

                    try
                    {
                        this.processing = true;
                        var scannerMode = x.Item3;
                        var flash = x.Item2;

                        if (!scannerMode)
                        {
                            var messageBox = new NouvemMessageBoxView(x);
                            messageBox.Dispatcher.Invoke(() =>
                            {
                                try
                                {
                                    messageBox.ShowDialog();
                                }
                                catch (Exception ex)
                                {
                                    this.log.LogError(this.GetType(), ex.Message);
                                }
                            });
                        }
                        else
                        {
                            var messageBox = new NouvemMessageBoxSmallView(flash);
                            messageBox.Dispatcher.Invoke(() =>
                            {
                                try
                                {
                                    messageBox.ShowDialog();
                                }
                                catch (Exception ex)
                                {
                                    this.log.LogError(this.GetType(), ex.Message);
                                }
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        this.log.LogError(this.GetType(), ex.Message);
                    }
                    finally
                    {
                        this.processing = false;
                    }
                });
        }

        private void CreateTouchscreenOrdersView()
        {
            Messenger.Default.Register<string>(this, Token.CreateTouchscreenOrdersView, s =>
            {
                if (this.orders == null)
                {
                    this.orders = new TouchscreenOrdersContainerView();
                }

                this.orders.Show();
                this.orders.Focus();
                return;
            });

        }

        /// <summary>
        /// Begin the data loading.
        /// </summary>
        private void BeginDataProcessing()
        {
            NouvemGlobal.ApplicationStart();
        }

        /// <summary>
        /// Test the database connection.
        /// </summary>
        /// <returns>Flag, as to whether a connection could be made to the db or not.</returns>
        private bool CheckDatabaseConnection()
        {
            var dataManager = DataManager.Instance;
            try
            {
                return dataManager.TestDatabase();
            }
            catch (Exception)
            {
                MessageBox.Show(Message.DatabaseConnectionFailure);
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RegisterForTouchscreenReportSearch()
        {


            //// Create the sales search screen.
            //Messenger.Default.Register<ViewType>(
            //    this,
            //    view =>
            //    {
            //        if (view == ViewType.ReportViewer)
            //        {
            //            if (ViewModelLocator.ReportViewerStatic.IsFormLoaded)
            //            {
            //                return;
            //            }

            //            var report = new Nouvem.View.Report.ReportViewer();
            //            report.Show();
            //            report.Focus();
            //            return;
            //        }
            //    });

            // Create the sales search screen.
            //Messenger.Default.Register<IList<Sale>>(
            //    this,
            //    Token.SearchForSale,
            //    sales =>
            //    {
            //        if (ViewModelLocator.SalesSearchDataStatic.IsFormLoaded)
            //        {
            //            Messenger.Default.Send(sales, Token.DisplayCustomerSales);

            //            // don't allow multiple instances of the user master window.
            //            return;
            //        }

            //        var searchDataScreen = new SaleSearchDataView();
            //        searchDataScreen.Show();
            //        searchDataScreen.Focus();
            //        ViewModelLocator.SalesSearchDataStatic.SetDisplaySales(sales);
            //        //Messenger.Default.Send(sales, Token.DisplayCustomerSales);
            //    });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForTouchscreenReportSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayReportData);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new TouchscreenReportSaleSearchDataView();
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                });

            Messenger.Default.Register<ReportData>(this, ViewType.ReportTouchscreenSearch, o =>
            {
                var bpGroup = new ReportTouchscreenSearchView(o) { };
                bpGroup.Show();
                bpGroup.Focus();
                return;
            });

            // Create the sales search screen.
            //Messenger.Default.Register<List<Sale>>(
            //    this,
            //    Token.CreateInvoice,
            //    sales =>
            //    {
            //        var invoice = new InvoiceCreationView ();
            //        invoice.Show();
            //        invoice.Focus();
            //    });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForTouchscreenProductionSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayCustomerSales);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new TouchscreenProductionSearchDataView();
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.DontFilterByDates = true;
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                    //Messenger.Default.Send(sales, Token.DisplayReportData);
                });

            // Create the sales search screen.
            Messenger.Default.Register<IList<Sale>>(
                this,
                Token.SearchForTouchscreenTransactionSale,
                sales =>
                {
                    if (ViewModelLocator.ReportSalesSearchDataStatic.IsFormLoaded)
                    {
                        Messenger.Default.Send(sales, Token.DisplayCustomerSales);

                        // don't allow multiple instances of the user master window.
                        return;
                    }

                    var searchDataScreen = new TouchscreenTransactionSearchDataView();
                    searchDataScreen.Show();
                    searchDataScreen.Focus();
                    ViewModelLocator.ReportSalesSearchDataStatic.SetIncomingReportSales(sales);
                });
        }

        /// <summary>
        /// Retrieve the nouvem log in password.
        /// </summary>
        private void GenerateNouvemPassword()
        {
            ApplicationSettings.NouvemPassword = Security.Security.GetNouvemPassword();
        }

        private void DisplayLoadingScreen()
        {
            var loading = new ApplicationLoadingView();
            loading.Show();
        }

        /// <summary>
        /// Check for licence deallocation.
        /// </summary>
        private void BeginPolling()
        {
            Current.Dispatcher.Invoke(() =>
            {
                var pollingTimer = new DispatcherTimer();
                var dataManager = DataManager.Instance;
                var localTime =
                    ApplicationSettings.DatabasePollingInterval == 0 ? 10 : ApplicationSettings.DatabasePollingInterval;
                pollingTimer.Interval = TimeSpan.FromMinutes(localTime);
                pollingTimer.IsEnabled = true;
                pollingTimer.Start();
                pollingTimer.Tick += (sender, args) =>
                {
                    //if (dataManager.CheckForLicenceDeallocation())
                    //{
                    //    Messenger.Default.Send(Token.Message, Token.CloseAllWindows);
                    //    NouvemMessageBox.Show(Message.NoLicenceDetected);
                    //}

                    //if (dataManager.CheckForForcedLogOff())
                    //{
                    //    Messenger.Default.Send(Token.Message, Token.CloseAllWindows);
                    //    NouvemGlobal.Licensee.LicenseType = null;
                    //    NouvemMessageBox.Show(Message.UserForcedLoggedOut);
                    //}

                    //NouvemGlobal.UpdateBusinessPartners();
                    Application.Current.Dispatcher.BeginInvoke(
                        DispatcherPriority.Background,
                        new Action(NouvemGlobal.RefreshData));
                };
            });
        }
    }
}
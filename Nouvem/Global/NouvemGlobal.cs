﻿// -----------------------------------------------------------------------
// <copyright file="NouvemGlobal.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.Shared.Localisation;

namespace Nouvem.Global
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Threading;
    using System.Threading.Tasks;
    using Nouvem.BusinessLogic;
    using Nouvem.Logging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Security;
    using Nouvem.Shared;
    using Nouvem.ViewModel;

    /// <summary>
    /// Static class that contains all the application global operations/data.
    /// </summary>
    public static class NouvemGlobal
    {
        #region field

        /// <summary>
        /// The application logger reference.
        /// </summary>
        private static readonly ILogger Log = new Logger();

        /// <summary>
        /// The business partner repository reference.
        /// </summary>
        private static readonly DataManager DataManager = DataManager.Instance;

        #endregion

        #region internal interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the application has loaded..
        /// </summary>
        internal static bool ApplicationLoaded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the application has loaded without error.
        /// </summary>
        internal static string ApplicationStartUpError { get; set; }

        /// <summary>
        /// Gets or sets the application business partners.
        /// </summary>
        internal static IList<BusinessPartner> BusinessPartners { get; set; }

        /// <summary>
        /// Gets or sets the application business partners.
        /// </summary>
        internal static IList<SupplierHerd> SupplierHerds { get; set; }

        /// <summary>
        /// Gets or sets the application customer business partners.
        /// </summary>
        internal static IList<BusinessPartner> CustomerPartners { get; set; }

        /// <summary>
        /// Gets or sets the application customer business partners.
        /// </summary>
        internal static IList<AttributeAllocationData> AttributeAllocationData { get; set; }

        /// <summary>
        /// Gets or sets the application supplier business partners.
        /// </summary>
        internal static bool RetrieveProductStockSummary { get; set; }

        /// <summary>
        /// Gets or sets the application supplier business partners.
        /// </summary>
        internal static IList<BusinessPartner> SupplierPartners { get; set; }

        /// <summary>
        /// Gets or sets the special prices.
        /// </summary>
        internal static IList<SpecialPrice> SpecialPrices { get; set; }

        /// <summary>
        /// Gets or sets the application routes.
        /// </summary>
        internal static IList<Route> Routes{ get; set; }

        /// <summary>
        /// Gets or sets the application routes.
        /// </summary>
        internal static DateTime LastPriceListDetailsUpdate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are selecting and weighing a product at dispatch.
        /// </summary>
        internal static bool SelectingAndWeighingProductAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets the application business partners currencies.
        /// </summary>
        internal static IList<BPCurrency> BusinessPartnerCurrencies { get; set; }

        /// <summary>
        /// Gets or sets the application business partners groups.
        /// </summary>
        internal static IList<BPGroup> BusinessPartnerGroups { get; set; }

        /// <summary>
        /// Gets or sets the application customer business partners.
        /// </summary>
        internal static IList<NouKillType> NouKillTypes { get; set; }

        /// <summary>
        /// Gets or sets the application business partners types.
        /// </summary>
        internal static IList<NouBPType> BusinessPartnerTypes { get; set; }

        /// <summary> Gets or sets the application business partners contacts.
        /// </summary>
        internal static IList<BusinessPartnerContact> BusinessPartnerContacts { get; set; }

        /// <summary>
        /// Gets or sets the application business partners properties.
        /// </summary>
        internal static IList<BPProperty> BusinessPartnerProperties { get; set; }

        /// <summary>
        /// Gets or sets the application business partners properties.
        /// </summary>
        internal static IList<BPPropertySelection> BusinessPartnerPropertySelections { get; set; }

        /// <summary>
        /// Gets or sets the servers.
        /// </summary>
        internal static IList<Server> Servers { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        internal static IList<User> Users { get; set; }

        /// <summary>
        /// Gets or sets the group authorisations.
        /// </summary>
        internal static IList<ViewGroupRule> GroupAuthorisations { get; set; }

        /// <summary>
        /// Gets or sets the application devices.
        /// </summary>
        internal static IList<DeviceMaster> Devices { get; set; }

        /// <summary>
        /// Gets or sets the local machine device.
        /// </summary>
        internal static DeviceMaster LocalDevice { get; set; }

        /// <summary>
        /// Gets or sets the application licensees.
        /// </summary>
        internal static IList<Licensee> Licensees { get; set; }

        /// <summary>
        /// Gets or sets the application licensees.
        /// </summary>
        internal static int? TransactionTypeStockMoveId { get; set; }

        /// <summary>
        /// Gets or sets the local licensee (user or device).
        /// </summary>
        internal static Licensee Licensee { get; set; }

        /// <summary>
        /// Gets or sets the logged in user.
        /// </summary>
        internal static PriceMaster ReceipePriceList { get; set; }

        /// <summary>
        /// Gets or sets the logged in user.
        /// </summary>
        internal static User LoggedInUser { get; set; }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        internal static IList<PriceList> PriceLists { get; set; }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        internal static IList<PriceMaster> PriceListMasters { get; set; }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        internal static PriceMaster BasePriceList
        {
            get
            {
                return PriceListMasters != null
                    ? PriceListMasters.FirstOrDefault(
                        x => x.CurrentPriceListName.CompareIgnoringCase(Strings.BasePriceList))
                    : null;
            }
        }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        internal static PriceMaster CostPriceList
        {
            get
            {
                return PriceListMasters != null
                    ? PriceListMasters.FirstOrDefault(
                        x => x.CurrentPriceListName.CompareIgnoringCase(Strings.CostPriceList))
                    : null;
            }
        }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        internal static PriceMaster SpecialPriceList
        {
            get
            {
                return PriceListMasters != null
                    ? PriceListMasters.FirstOrDefault(
                        x => x.CurrentPriceListName.CompareIgnoringCase(Strings.SpecialPrices))
                    : null;
            }
        }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        internal static PriceMaster AveragePriceList
        {
            get
            {
                return PriceListMasters != null
                    ? PriceListMasters.FirstOrDefault(
                        x => x.CurrentPriceListName.CompareIgnoringCase(Strings.AveragePriceList))
                    : null;
            }
        }

        /// <summary>
        /// Gets or sets the inventory items.
        /// </summary>
        internal static IList<InventoryItem> InventoryItems { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the batch traceability values have been set.
        /// </summary>
        internal static Dictionary<string, bool> BatchValuesSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we need to check if the batch traceability values have been set.
        /// </summary>
        internal static bool CheckBatchValuesSet { get; set; }

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        internal static IList<InventoryItem> SaleItems { get; set; }

        /// <summary>
        /// Gets or sets the inventory purchase items.
        /// </summary>
        internal static IList<InventoryItem> PurchaseItems { get; set; }

        /// <summary>
        /// Gets or sets the production items.
        /// </summary>
        internal static IList<InventoryItem> ProductionItems { get; set; }

        /// <summary>
        /// Gets or sets the inventory attachments.
        /// </summary>
        internal static IList<INAttachment> InventoryAttachments { get; set; }

        /// <summary>
        /// Gets or sets the inventory property selections.
        /// </summary>
        internal static IList<INPropertySelection> INPropertySelections { get; set; }

        /// <summary>
        /// Gets or sets the traceability data.
        /// </summary>
        internal static Dictionary<Tuple<int,string>, List<TraceabilityData>> TraceabilityData { get; set; }

        /// <summary>
        /// Gets or sets the inventory properties.
        /// </summary>
        internal static IList<INProperty> INProperties { get; set; }

        /// <summary>
        /// Gets or sets the system traceability methods.
        /// </summary>
        internal static IList<NouTraceabilityMethod> TraceabilityMethods { get; set; }

        /// <summary>
        /// Gets or sets the system traceability types.
        /// </summary>
        internal static IList<NouTraceabilityType> TraceabilityTypes { get; set; }

        /// <summary>
        /// Gets or sets the system quality types.
        /// </summary>
        internal static IList<NouQualityType> QualityTypes { get; set; }

        /// <summary>
        /// Gets or sets the system date types.
        /// </summary>
        internal static IList<NouDateType> DateTypes { get; set; }

        /// <summary>
        /// Gets or sets the application transaction types.
        /// </summary>
        internal static IList<NouTransactionType> TransactionTypes { get; set; }

        /// <summary>
        /// Gets or sets the transaction type goods receipt id.
        /// </summary>
        internal static int TransactionTypeGoodsReturnId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type goods receipt id.
        /// </summary>
        internal static int TransactionTypeGoodsReceiptId { get; set; }
        /// <summary>
        /// Gets or sets the transaction type lairage id.
        /// </summary>
        internal static int TransactionTypeLairageId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type goods delivery id.
        /// </summary>
        internal static int TransactionTypeGoodsDeliveryId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type production issue id.
        /// </summary>
        internal static int TransactionTypeProductionIssueId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type production receipt id.
        /// </summary>
        internal static int TransactionTypeProductionReceiptId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type epos sale id.
        /// </summary>
        internal static int TransactionTypeSaleEposId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type epos sale id.
        /// </summary>
        internal static int TransactionTypeStockAdjustIdId { get; set; }

        /// <summary>
        /// Gets or sets the transaction type edit id.
        /// </summary>
        internal static int TransactionTypeEditId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse locations.
        /// </summary>
        internal static IList<Warehouse> WarehouseLocations { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseReturnsId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseIntakeId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseButcheryId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseInjectionId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseOutOfButcheryId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseOutOfInjectionId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseIntoSlicingId { get; set; }

        /// <summary>
        /// Gets or sets the warehouse intake id.
        /// </summary>
        internal static int WarehouseIntoJointingId { get; set; }

        /// <summary>
        /// Gets or sets the dispatch id.
        /// </summary>
        internal static int DispatchId { get; set; }

        /// <summary>
        /// Gets or sets the terminal services session identifier.
        /// </summary>
        internal static string RDPIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the terminal services session identifier.
        /// </summary>
        internal static bool IsRDPConnection { get; set; }

        /// <summary>
        /// Gets or sets the production id.
        /// </summary>
        internal static int ProductionId { get; set; }

        /// <summary>
        /// Gets or sets the out of production id.
        /// </summary>
        internal static int OutOfProductionId { get; set; }

        /// <summary>
        /// Gets or sets the out of production id.
        /// </summary>
        internal static int PalletLocationId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are scanning stock at dispatch.
        /// </summary>
        internal static bool ScanningAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets the audit data.
        /// </summary>
        internal static IList<Audit> AuditData { get; set; }

        /// <summary>
        /// Gets or sets the audit data.
        /// </summary>
        internal static bool SalesSearchScreenOpen { get; set; }

        /// <summary>
        /// Gets or sets the system document status items.
        /// </summary>
        internal static IList<NouDocStatu> NouDocStatusItems { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status active item.
        /// </summary>
        internal static NouDocStatu NouDocStatusActive { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status active item.
        /// </summary>
        internal static NouDocStatu NouDocStatusActiveEdit { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status in progress item.
        /// </summary>
        internal static NouDocStatu NouDocStatusInProgress { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status cancelled item.
        /// </summary>
        internal static NouDocStatu NouDocStatusCancelled { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status filled item.
        /// </summary>
        internal static NouDocStatu NouDocStatusFilled { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status complete item.
        /// </summary>
        internal static NouDocStatu NouDocStatusComplete { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status exported item.
        /// </summary>
        internal static NouDocStatu NouDocStatusExported { get; set; }

        /// <summary>
        /// Gets or sets the nou doc status exported item.
        /// </summary>
        internal static NouDocStatu NouDocStatusMoved { get; set; }

        /// <summary>
        /// Gets or sets the system containers.
        /// </summary>
        internal static IList<Container> Containers { get; set; }

        /// <summary>
        /// Gets or sets the system container types.
        /// </summary>
        internal static IList<ContainerType> ContainerTypes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the licensee is a nouvem representative.
        /// </summary>
        internal static bool IsNouvemUser
        {
            get
            {
                return LoggedInUser?.UserMaster?.UserName != null && LoggedInUser.UserMaster.UserName.CompareIgnoringCase(Constant.NouvemLogin);
            }
        }

        /// <summary>
        /// Gets the logged in user id.
        /// </summary>
        internal static int? UserId
        {
            get
            {
                return LoggedInUser != null && LoggedInUser.UserMaster != null ? LoggedInUser.UserMaster.UserMasterID : (int?)null;
            }
        }

        /// <summary>
        /// Gets the logged in device id.
        /// </summary>
        public static int? DeviceId
        {
            get
            {
                return LocalDevice != null ? LocalDevice.DeviceID : (int?)null;
            }
        }

        /// <summary>
        /// Gets the logged in device id.
        /// </summary>
        public static string DeviceIdString
        {
            get
            {
                return LocalDevice != null ? string.Format("{0}:{1}",Strings.Device, LocalDevice.DeviceID) : string.Empty;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// The global data processor.
        /// </summary>
        public static void ApplicationStart()
        {
            //ParseAddresses();
            RegisterUnhandledExceptionHandler();
            CreateFolders();
            CheckForLogDeletion();
            GetRoutes();
            SetLocale();
            GetDevices();
            SetDeviceSettings();
            GetBusinessPartners();
            
            GetPriceLists();
            Task.Factory.StartNew(GetGroupAuthorisations);

            var inventory = Task.Factory.StartNew(GetInventoryItems);
            inventory.Wait();

            var users = Task.Factory.StartNew(() => GetUsers());
            users.Wait();

            Task.Factory.StartNew(StartScannerProcessing);
            Task.Factory.StartNew(GetTransactionTypes);
            Task.Factory.StartNew(GetWarehouses);
            //Task.Factory.StartNew(GetAuditData);
            Task.Factory.StartNew(GetDocStatusValues);
            Task.Factory.StartNew(CleanUpDatabase);
            Task.Factory.StartNew(GetTraceabilityData);
            Task.Factory.StartNew(GetTraceabilityMethods);
            Task.Factory.StartNew(GetTraceabilityTypes);
            Task.Factory.StartNew(GetQualityTypes);
            Task.Factory.StartNew(GetDateTypes);
            Task.Factory.StartNew(GetSpecialPrices);
            Task.Factory.StartNew(GetContainers).ContinueWith(s => ApplicationLoaded = true);
            GetKillTypes();
            SetApplicationUpdates();
            ScheduleJobs();
            LastPriceListDetailsUpdate = DateTime.Now;
            SendPrinterCommand();
            GetAttributeAllocationData();
        }

        /// <summary>
        /// The global data refresh processor.
        /// </summary>
        public static void RefreshData()
        {
            GetBusinessPartners();
           
            GetPriceLists();
            var inventory = Task.Factory.StartNew(GetInventoryItems);
            inventory.Wait();
            var users = Task.Factory.StartNew(() => GetUsers());
            users.Wait();

            Task.Factory.StartNew(GetSpecialPrices);
            Task.Factory.StartNew(GetTraceabilityData);
            Task.Factory.StartNew(GetTraceabilityMethods);
            Task.Factory.StartNew(GetTraceabilityTypes);
            Task.Factory.StartNew(GetQualityTypes);
            Task.Factory.StartNew(GetDateTypes);
            Task.Factory.StartNew(GetContainers).ContinueWith(s => ApplicationLoaded = true);
        }

        public static void AddInventoryItemByID(int id)
        {
            var item = DataManager.GetInventoryItemById(id);
            if (item != null)
            {
                InventoryAttachments = DataManager.GetInventoryAttachments();
                INPropertySelections = DataManager.GetINPropertySelection();
                INProperties = DataManager.GetINProperties();

                var localItem = InventoryItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (localItem != null)
                {
                    InventoryItems.Remove(localItem);
                }

                var localSalesItem = SaleItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (localSalesItem != null)
                {
                    SaleItems.Remove(localSalesItem);
                }

                var localPurchaseItem = PurchaseItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (localPurchaseItem != null)
                {
                    PurchaseItems.Remove(localPurchaseItem);
                }

                var localProductionItem = ProductionItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (localProductionItem != null)
                {
                    ProductionItems.Remove(localProductionItem);
                }

                AddInventoryItem(item);
            }
        }

        /// <summary>
        /// Gets the current special prices.
        /// </summary>
        public static void SendPrinterCommand()
        {
            if (ApplicationSettings.SendPrinterCommandOnStartUp)
            {
                PrintManager.Instance.SendCommand(ApplicationSettings.PrinterCommand, true);
            }
        }

        /// <summary>
        /// Gets the current special prices.
        /// </summary>
        public static void GetSpecialPrices()
        {
            SpecialPrices = DataManager.GetSpecialPrices().Where(x => x.StartDate <= DateTime.Today && x.EndDate >= DateTime.Today).ToList();
        }

        /// <summary>
        /// Gets the current special prices.
        /// </summary>
        public static void GetKillTypes()
        {
            NouKillTypes = DataManager.GetKillTypes();
        }

        /// <summary>
        /// Refresh the partners.
        /// </summary>
        public static void RefreshPartners()
        {
            GetBusinessPartners();
        }

        /// <summary>
        /// Refresh the products.
        /// </summary>
        public static void RefreshProducts()
        {
            GetInventoryItems();
        }

        /// <summary>
        /// Adds a newly created inventory item to the relevant collections.
        /// </summary>
        /// <param name="item">The item to add.</param>
        internal static void AddInventoryItem(InventoryItem item)
        {
            InventoryItems.Add(item);
            if (item.Master.SalesItem == true)
            {
                SaleItems.Add(item);
            }

            if (item.Master.PurchaseItem == true)
            {
                PurchaseItems.Add(item);
            }

            if (item.Master.ProductionProduct == true)
            {
                ProductionItems.Add(item);
            }
        }

        /// <summary>
        /// Method that gets the application inventory items.
        /// </summary>
        internal static void GetInventoryItems()
        {
            try
            {
                InventoryItems = DataManager.GetInventoryItems();
                InventoryAttachments = new List<INAttachment>(); //DataManager.GetInventoryAttachments();
                INPropertySelections = DataManager.GetINPropertySelection();
                INProperties = DataManager.GetINProperties();
                SaleItems = InventoryItems.Where(x => x.Master.SalesItem == true).ToList();
                PurchaseItems = InventoryItems.Where(x => x.Master.PurchaseItem == true).ToList();
                ProductionItems = InventoryItems.Where(x => x.Master.ProductionProduct == true).ToList();
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }


        /// <summary>
        /// Updates inventory item to the relevant collections.
        /// </summary>
        /// <param name="item">The item to update.</param>
        internal static void UpdateInventoryItem(InventoryItem item)
        {
            var localItem = InventoryItems.FirstOrDefault(x => x.Master.INMasterID == item.Master.INMasterID);
            if (localItem != null)
            {
                var localSaleItem = SaleItems.FirstOrDefault(x => x.Master.INMasterID == item.Master.INMasterID);
                if (localSaleItem == null)
                {
                    if (item.Master.SalesItem == true)
                    {
                        SaleItems.Add(item);
                    }
                }
                else
                {
                    if (item.Master.SalesItem != true)
                    {
                        SaleItems.Remove(item);
                    }
                }

                var localPurchaseItem = PurchaseItems.FirstOrDefault(x => x.Master.INMasterID == item.Master.INMasterID);
                if (localPurchaseItem == null)
                {
                    if (item.Master.PurchaseItem == true)
                    {
                        PurchaseItems.Add(item);
                    }
                }
                else
                {
                    if (item.Master.PurchaseItem != true)
                    {
                        PurchaseItems.Remove(item);
                    }
                }

                var localProductionItem = ProductionItems.FirstOrDefault(x => x.Master.INMasterID == item.Master.INMasterID);
                if (localProductionItem == null)
                {
                    if (item.Master.ProductionProduct == true)
                    {
                        ProductionItems.Add(item);
                    }
                }
                else
                {
                    if (item.Master.ProductionProduct != true)
                    {
                        ProductionItems.Remove(item);
                    }
                }
            }
        }

        ///// <summary>
        ///// Check an individual authorisation.
        ///// </summary>
        ///// <param name="authorisationToCheck">The individual authorisation to check.</param>
        ///// <returns>A flag, indicating the authorisation level.</returns>
        //internal static string CheckAuthorisation(string authorisationToCheck)
        //{
        //    Log.LogInfo(typeof(NouvemGlobal), string.Format("CheckAuthorisation(): Checking authorisation for {0}", authorisationToCheck));
        //    var companyAuthorisationValue = Constant.NoAccess;

        //    try
        //    {
        //        if (IsNouvemUser)
        //        {
        //            return Constant.FullAccess;
        //        }

        //        if (Licensee == null)
        //        {
        //            Log.LogInfo(typeof(NouvemGlobal), "Authorisation denied. No licence");
        //            return Constant.NoAccess;
        //        }

        //        if (LoggedInUser == null)
        //        {
        //            Log.LogInfo(typeof(NouvemGlobal), "Authorisation denied. No logged in user");
        //            return Constant.NoAccess;
        //        }

        //        if ((!Licensee.IsDevice && Licensee.LicenceStatus != LicenceStatus.Valid))
        //        {
        //            Log.LogInfo(typeof(NouvemGlobal), $"Invalid user licence:{Licensee.Name}");
        //            return Constant.NoAccess;
        //        }

        //        Log.LogInfo(typeof(NouvemGlobal), $"Checking authorisation for licencee:{Licensee.Name}");

        //        // get the overriding licence authorisation.
        //        var authorisations = Licensee.Authorisations;

        //        if (authorisations == null)
        //        {
        //            Log.LogInfo(typeof(NouvemGlobal), "No authorisations found");
        //            return Constant.NoAccess;
        //        }

        //        var localAuthorisation = authorisations.FirstOrDefault(x => x.Key.Equals(authorisationToCheck));

        //        if (localAuthorisation.Key == null)
        //        {
        //            // authorisation doesn't exist for current customer, so allow.
        //            Log.LogInfo(typeof(NouvemGlobal), "Authorisation not part of licencees authorisations, so allowing");
        //            return Constant.FullAccess;
        //        }

        //        if (localAuthorisation.Value == null)
        //        {
        //            Log.LogInfo(typeof(NouvemGlobal), "Authorisation found. No value");
        //            return Constant.NoAccess;
        //        }

        //        if (localAuthorisation.Value.Equals(Constant.NoAccess))
        //        {
        //            // licence says no access, so no need to check the company authorisations.
        //            Log.LogInfo(typeof(NouvemGlobal), "Authorisation found. No access");
        //            return Constant.NoAccess;
        //        }

        //        // get the company authorisation value.
        //        var companyAuthorisation =
        //               GroupAuthorisations.FirstOrDefault(x => x.AuthorisationListDescription.Equals(authorisationToCheck) && x.UserGroupID == LoggedInUser.Group.UserGroupID);
                
        //        if (companyAuthorisation != null)
        //        {
        //            Log.LogInfo(typeof(NouvemGlobal), $"Group Authorisation found:{companyAuthorisation.AuthorisationValueDescription}");
        //            companyAuthorisationValue = companyAuthorisation.AuthorisationValueDescription;
        //        }

        //        if (localAuthorisation.Value.Equals(Constant.ReadOnly))
        //        {
        //            // licence says read only, so we need to check the company authorisations for a no access.
        //            return companyAuthorisationValue.Equals(Constant.NoAccess) ? Constant.NoAccess : Constant.ReadOnly;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.LogError(typeof(NouvemGlobal), ex.Message);
        //        SystemMessage.Write(MessageType.Issue, ex.Message);
        //    }

        //    // license is full access, so return the company authorisation value.
        //    Log.LogInfo(typeof(NouvemGlobal), $"Group Authorisation found:{companyAuthorisationValue}");
        //    return companyAuthorisationValue;
        //}

        /// <summary>
        /// Gets the local ip address.
        /// </summary>
        /// <returns>The local ip address.</returns>
        internal static string GetIpAddress()
        {
            try
            {
                var hostName = Dns.GetHostName();

                // Get the IP
                return Dns.GetHostEntry(hostName).AddressList[2].ToString();
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }
   
        /// <summary>
        /// Gets the local device unique mac address.
        /// </summary>
        /// <returns>The local mac address.</returns>
        internal static string GetMacAddress()
        {
            if (!string.IsNullOrWhiteSpace(RDPIdentifier))
            {
                Log.LogInfo(typeof(NouvemGlobal), string.Format("GetMacAddress(): using rdp identifier as profile name:{0}", RDPIdentifier));
                return RDPIdentifier;
            }

            //var nics = NetworkInterface.GetAllNetworkInterfaces();
            //var macAddress = string.Empty;

            //foreach (NetworkInterface adapter in nics)
            //{
            //    if (macAddress == String.Empty) 
            //    {
            //        // only return MAC Address from first card 
            //        macAddress = adapter.GetPhysicalAddress().ToString();
            //    }
            //}

            var macAddress = System.Environment.MachineName;
            Log.LogInfo(typeof(NouvemGlobal), string.Format("GetMacAddress(): mac:{0}", macAddress));
            return macAddress;
        }

        /// <summary>
        /// Method that updates the application business partners.
        /// </summary>
        internal static void UpdateBusinessPartners()
        {
            try
            {
                var partnerData = DataManager.GetUpdatedBusinessPartners();
                if (partnerData == null)
                {
                    return;
                }

                var newBusinessPartners = partnerData.Item1;
                newBusinessPartners.ForEach(recentPartner =>
                {
                    var addPartner = !BusinessPartners.Any(x => x.Details.BPMasterID == recentPartner.Details.BPMasterID);

                    if (addPartner)
                    {
                        BusinessPartners.Add(recentPartner);
                        if (recentPartner.PartnerType.Type.Equals(BusinessPartnerType.Customer) ||
                            recentPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock) ||
                            recentPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier))
                        {
                            CustomerPartners.Add(recentPartner);
                        }

                        if (recentPartner.PartnerType.Type.Equals(BusinessPartnerType.Supplier) ||
                            recentPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock)  ||
                            recentPartner.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier) ||
                            recentPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier))
                        {
                            SupplierPartners.Add(recentPartner);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Method that updates the inventory items.
        /// </summary>
        internal static void UpdateInventoryItems()
        {
            try
            {
                var items = DataManager.GetRecentInventoryItems();

                items.ToList().ForEach(recentItem =>
                {
                    var addItem = !InventoryItems.Any(x => x.Master.INMasterID == recentItem.Master.INMasterID);

                    if (addItem)
                    {
                       AddInventoryItem(recentItem); 
                    }
                });
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Gets the attribute data.
        /// </summary>
        internal static void GetAttributeAllocationData()
        {
            AttributeAllocationData = DataManager.GetAttributeAllocations();
        }

        /// <summary>
        /// Method that gets the application price list data.
        /// </summary>
        internal static void GetPriceLists()
        {
            try
            {
                Log.LogInfo(typeof(NouvemGlobal), "GetPriceLists()...starting");
                PriceLists = DataManager.GetAllPriceLists();
                //PriceListDetails = DataManager.GetPriceListDetails();
                PriceListMasters = DataManager.GetPriceLists();
                Log.LogInfo(typeof(NouvemGlobal), "GetPriceLists()...complete");

                if (BasePriceList == null)
                {
                    var currency = BusinessPartnerCurrencies.FirstOrDefault(x => x.Name.Equals(ApplicationSettings.DefaultCurrency));
                    if (currency == null)
                    {
                        return;
                    }

                    var baseList = new PriceMaster();
                    baseList.CurrentPriceListName = Strings.BasePriceList;
                    baseList.Factor = 0;
                    baseList.NouRoundingOptionsID = 1;
                    baseList.NouRoundingRulesID = 3;
                    baseList.BPCurrencyID = currency.BPCurrencyID;
                    if (DataManager.AddOrUpdatePriceMasters(new List<PriceMaster>{baseList}))
                    {
                        PriceListMasters = DataManager.GetPriceLists();
                    }
                }

                if (CostPriceList == null)
                {
                    var currency = BusinessPartnerCurrencies.FirstOrDefault(x => x.Name.Equals(ApplicationSettings.DefaultCurrency));
                    if (currency == null)
                    {
                        return;
                    }

                    var baseList = new PriceMaster();
                    baseList.CurrentPriceListName = Strings.CostPriceList;
                    baseList.Factor = 0;
                    baseList.NouRoundingOptionsID = 1;
                    baseList.NouRoundingRulesID = 3;
                    baseList.BPCurrencyID = currency.BPCurrencyID;
                    if (DataManager.AddOrUpdatePriceMasters(new List<PriceMaster> { baseList }))
                    {
                        PriceListMasters = DataManager.GetPriceLists();
                    }
                }

                if (SpecialPriceList == null)
                {
                    var currency = BusinessPartnerCurrencies.FirstOrDefault();
                    if (currency == null)
                    {
                        return;
                    }

                    var baseList = new PriceMaster();
                    baseList.CurrentPriceListName = Strings.SpecialPrices;
                    baseList.Factor = 0;
                    baseList.NouRoundingOptionsID = 1;
                    baseList.NouRoundingRulesID = 3;
                    baseList.BPCurrencyID = currency.BPCurrencyID;
                    if (DataManager.AddOrUpdatePriceMasters(new List<PriceMaster> { baseList }))
                    {
                        PriceListMasters = DataManager.GetPriceLists();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Method that gets the application users.
        /// </summary>
        internal static IList<User> GetUsers()
        {
            //if (Users != null)
            //{
            //    return Users;
            //}

            try
            {
                Users = DataManager.GetUsers().Where(x =>
                        (x.UserMaster.InActiveFrom == null && x.UserMaster.InActiveTo == null)
                        || !(DateTime.Today >= x.UserMaster.InActiveFrom && DateTime.Today < x.UserMaster.InActiveTo))
                    .ToList();
                //Users.Add(new User
                //{
                //    UserMaster = new UserMaster { UserName = Constant.NouvemLogin, FullName = Constant.NouvemLogin, Password = Security.EncryptString(ApplicationSettings.NouvemPassword) },
                //    Group = new UserGroup_()
                //});
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));

                if (Users == null)
                {
                    Users = new List<User>();
                }

                Users.Add(new User
                {
                    UserMaster = new UserMaster { UserName = Constant.NouvemLogin, FullName = Constant.NouvemLogin, Password = Security.EncryptString(ApplicationSettings.NouvemPassword) },
                    Group = new UserGroup_()
                });
            }

            return Users;
        }

        /// <summary>
        /// Adds a newly created partner to the relevant collections.
        /// </summary>
        /// <param name="newPartner">The newly created partner.</param>
        internal static void AddNewPartner(BusinessPartner newPartner)
        {
            if (newPartner?.PartnerType == null)
            {
                return;
            }

            BusinessPartners.Add(newPartner);
            if (newPartner.PartnerType.Type.Equals(BusinessPartnerType.Customer)
                || newPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock) 
                || newPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier))
            {
                CustomerPartners.Add(newPartner);
            }

            if (newPartner.PartnerType.Type.Equals(BusinessPartnerType.Supplier)
                || newPartner.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier)
                  || newPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock) 
                || newPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier))
            {
                SupplierPartners.Add(newPartner);
            }
        }

        /// <summary>
        /// Updates the partner in the relevant collections.
        /// </summary>
        /// <param name="updatedPartner">The newly updated partner.</param>
        internal static void UpdatePartner(BusinessPartner updatedPartner)
        {
            var customerPartner = 
                CustomerPartners.FirstOrDefault(x => x.Details.BPMasterID == updatedPartner.Details.BPMasterID);
            var supplierPartner =
                    SupplierPartners.FirstOrDefault(x => x.Details.BPMasterID == updatedPartner.Details.BPMasterID);

            if (updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.Customer)
                  || updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock) 
                || updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier))
            {
                if (customerPartner == null)
                {
                    CustomerPartners.Add(updatedPartner);
                }

                if (supplierPartner != null && updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.Customer))
                {
                    // customer only. Remove from suppliers
                    SupplierPartners.Remove(supplierPartner);
                }
            }

            if (updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.Supplier)
                  || updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock) 
                || updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier)
               || updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier))
            {
                if (supplierPartner == null)
                {
                    SupplierPartners.Add(updatedPartner);
                }

                if (customerPartner != null && updatedPartner.PartnerType.Type.Equals(BusinessPartnerType.Supplier))
                {
                    // supplier only. Remove from customers
                    CustomerPartners.Remove(supplierPartner);
                }
            }
        }

        /// <summary>
        /// Updates the property selections.
        /// </summary>
        internal static void UpdatePropertySelections()
        {
            INPropertySelections = DataManager.GetINPropertySelection();
        }

        /// <summary>
        /// Updates the inventory attachments.
        /// </summary>
        internal static void UpdateInventoryAttachments()
        {
            InventoryAttachments = DataManager.GetInventoryAttachments();
        }

        /// <summary>
        /// Updates the price lists.
        /// </summary>
        internal static void UpdatePriceLists()
        {
            GetPriceLists();
        }

        /// <summary>
        /// Method that gets the application audit data.
        /// </summary>
        internal static void GetAuditData()
        {
            AuditData = DataManager.GetAuditDetails();
        }

        /// <summary>
        /// Method that gets the document status values.
        /// </summary>
        internal static void GetDocStatusValues()
        {
            var localItems = DataManager.GetNouDocStatusItems();
            NouDocStatusFilled = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.Filled));
            NouDocStatusComplete = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.Complete));
            NouDocStatusActive = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.Active));
            NouDocStatusActiveEdit = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.ActiveEdit));
            NouDocStatusCancelled = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.Cancelled));
            NouDocStatusInProgress = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.InProgress));
            NouDocStatusExported = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.Exported));
            NouDocStatusMoved = localItems.First(x => x.Value.CompareIgnoringCase(OrderStatus.Moved));

            NouDocStatusItems = new List<NouDocStatu>();
            NouDocStatusItems.Add(NouDocStatusActive);
            NouDocStatusItems.Add(NouDocStatusActiveEdit);
            NouDocStatusItems.Add(NouDocStatusMoved);
            NouDocStatusItems.Add(NouDocStatusInProgress);
            NouDocStatusItems.Add(NouDocStatusFilled);
            NouDocStatusItems.Add(NouDocStatusComplete);
            NouDocStatusItems.Add(NouDocStatusCancelled);
            NouDocStatusItems.Add(NouDocStatusExported);
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Updates the application updates.
        /// </summary>
        private static void SetApplicationUpdates()
        {
            DataManager.SetApplicationUpdates();
        }

        private static void ParseAddresses()
        {
            var addresses = DataManager.GetBusinessPartnerAddresses();
            foreach (var address in addresses)
            {
                var localAddress = address.Details.AddressLine1;
                var splitAddress = localAddress.Split(Environment.NewLine.ToCharArray()).Where(x => !string.IsNullOrEmpty(x)).ToList();
                //var splitAddress = localAddress.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToList();
                if (splitAddress.Count() == 2)
                {
                    address.Details.AddressLine1 = splitAddress.ElementAt(0);
                    address.Details.AddressLine2 = splitAddress.ElementAt(1);
                }
                else if (splitAddress.Count == 3)
                {
                    address.Details.AddressLine1 = splitAddress.ElementAt(0);
                    address.Details.AddressLine2 = splitAddress.ElementAt(1);
                    address.Details.AddressLine3 = splitAddress.ElementAt(2);
                }
                else if (splitAddress.Count == 4)
                {
                    address.Details.AddressLine1 = splitAddress.ElementAt(0);
                    address.Details.AddressLine2 = splitAddress.ElementAt(1);
                    address.Details.AddressLine3 = splitAddress.ElementAt(2);
                    address.Details.AddressLine4 = splitAddress.ElementAt(3);
                }
                else if (splitAddress.Count > 4)
                {
                    address.Details.AddressLine1 = splitAddress.ElementAt(0);
                    address.Details.AddressLine2 = splitAddress.ElementAt(1);
                    address.Details.AddressLine3 = splitAddress.ElementAt(2);

                    var longAddress = string.Empty;
                    for (int i = 0; i < splitAddress.Count; i++)
                    {
                        if (i >= 3)
                        {
                            longAddress += splitAddress.ElementAt(i) + " ";
                        }
                    }

                    if (longAddress.Length > 40)
                    {
                        longAddress = longAddress.Substring(0, 39);
                    }

                    address.Details.AddressLine4 = longAddress;
                }
            }

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var businessPartnerAddress in addresses)
                    {
                        var addy = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == businessPartnerAddress.Details.BPAddressID);
                       if (addy != null)
                       {
                           addy.AddressLine1 = businessPartnerAddress.Details.AddressLine1;
                           addy.AddressLine2 = businessPartnerAddress.Details.AddressLine2.Length <= 40 ? businessPartnerAddress.Details.AddressLine2 : businessPartnerAddress.Details.AddressLine2.Substring(0, 39);
                           addy.AddressLine3 = businessPartnerAddress.Details.AddressLine3.Length <= 40 ? businessPartnerAddress.Details.AddressLine3 : businessPartnerAddress.Details.AddressLine3.Substring(0, 39);
                           addy.AddressLine4 = businessPartnerAddress.Details.AddressLine4;
                       }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Method that registers for any unhandled exceptions. 
        /// </summary>
        private static void RegisterUnhandledExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                var ex = e.ExceptionObject as Exception;

                // log the unhandled exception.
                Log.LogError(
                    typeof(NouvemGlobal),
                    string.Format("RegisterUnhandledExceptionHandler(): Message:{0}  Inner Exception:{1}  Stack Trace:{2}", ex.Message, ex.InnerException, ex.StackTrace));

                Application.Current.Shutdown();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            };
        }

        /// <summary>
        /// Gets the local machine device details.
        /// </summary>
        public static void GetDevices()
        {
            Devices = DataManager.GetDevices();
            var localMac = GetMacAddress();
            Log.LogInfo(typeof(NouvemGlobal), string.Format("Local profile name:{0}", localMac.Trim()));
            LocalDevice = Devices.FirstOrDefault(x => x.DeviceMAC.CompareIgnoringCase(localMac.Trim()));

            if (LocalDevice != null)
            {
                Log.LogInfo(typeof(NouvemGlobal), string.Format("GetDevices(): Device match. Device ID:{0}", LocalDevice.DeviceID));
                var versionNumber = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                if (LocalDevice.CurrentVersion != versionNumber)
                {
                    LocalDevice.PreviousVersion = LocalDevice.CurrentVersion;
                    LocalDevice.CurrentVersion = versionNumber;
                    DataManager.Instance.AddOrUpdateDevice(LocalDevice,null);
                }
            }
            else
            {
                Log.LogError(typeof(NouvemGlobal), "GetDevices(): No device found matching profile name");
                ApplicationStartUpError = string.Format(Message.NoMatchingDeviceFound, localMac.Trim());
            }
        }

        /// <summary>
        /// Creates the application folders structure.
        /// </summary>
        private static void CreateFolders()
        {
            try
            {
                if (!Directory.Exists(@"C:\Nouvem"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem");
                }

                if (!Directory.Exists(@"C:\Nouvem\ConnectionString"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\ConnectionString");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\Aims"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\Aims");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\AimsSheep"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\AimsSheep");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\AimsSheep\toAims"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\AimsSheep\toAims");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\AimsSheep\fromAims"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\AimsSheep\fromAims");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\Aims\Cert"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\Aims\Cert");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\Aims\FromAims"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\Aims\FromAims");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\Aims\ToAims"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\Aims\ToAims");
                }

                if (!Directory.Exists(@"C:\Nouvem\Departments\BordBia"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Departments\BordBia");
                }

                if (!Directory.Exists(@"C:\Nouvem\Indicator"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Indicator");
                }

                if (!Directory.Exists(@"C:\Nouvem\Report"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Report");
                }

                if (!Directory.Exists(@"C:\Nouvem\Indicator\Checksum"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Indicator\Checksum");
                }

                if (!Directory.Exists(@"C:\Nouvem\Indicator\Alibi"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Indicator\Alibi");
                }

                if (!Directory.Exists(@"C:\Nouvem\GridLayout"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\GridLayout");
                }

                if (!Directory.Exists(@"C:\Nouvem\Label"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Label");
                }

                if (!Directory.Exists(@"C:\Nouvem\Log"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\Log");
                }

                if (!Directory.Exists(@"C:\Nouvem\SSRSReports"))
                {
                    Directory.CreateDirectory(@"C:\Nouvem\SSRSReports");
                }
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("{0},{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Gets the application traceability data.
        /// </summary>
        private static void GetTraceabilityData()
        {
            TraceabilityData = DataManager.GetTraceabilityData();
            BatchValuesSet = new Dictionary<string, bool>();
        }

        /// <summary>
        /// Gets the application traceability methods.
        /// </summary>
        private static void GetTraceabilityMethods()
        {
            TraceabilityMethods = DataManager.GetTraceabilityMethods();
        }

        /// <summary>
        /// Gets the application traceability types.
        /// </summary>
        private static void GetTraceabilityTypes()
        {
            TraceabilityTypes = DataManager.GetTraceabilityTypes();
        }

        /// <summary>
        /// Gets the application quality types.
        /// </summary>
        private static void GetQualityTypes()
        {
            QualityTypes = DataManager.GetQualityTypes();
        }

        /// <summary>
        /// Gets the application date types.
        /// </summary>
        private static void GetDateTypes()
        {
            DateTypes = DataManager.GetDateTypes();
        }

        /// <summary>
        /// Gets the application containers/types.
        /// </summary>
        private static void GetContainers()
        {
            Containers = DataManager.GetContainers();
            ContainerTypes = DataManager.GetContainerTypes();
        }

        /// <summary>
        /// Sets the locale.
        /// </summary>
        private static void SetLocale()
        {
            // TODO capture locale from user set up when complete.
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
        }

        /// <summary>
        /// Gets the user group authorisations.
        /// </summary>
        public static void GetGroupAuthorisations()
        {
            GroupAuthorisations = DataManager.GetGroupRules();
        }

        /// <summary>
        /// Gets the routes.
        /// </summary>
        private static void GetRoutes()
        {
            Routes = DataManager.GetRoutes();
        }

        /// <summary>
        /// Sets the device settings.
        /// </summary>
        private static void SetDeviceSettings()
        {
            DataManager.SetDeviceSettings();
            Task.Factory.StartNew(() =>
            {
                ReportManager.Instance.SetSettings();
                PrintManager.Instance.SetUpPrinter();
                PrintManager.Instance.OpenPrinterPort();
            });

            var accounts = AccountsManager.Instance;

            Log.LogInfo(typeof(NouvemGlobal), "SetDeviceSettings...connect to accounts");
            accounts.GetAccountsPackage();
        }

        /// <summary>
        /// Method that gets the application business partners.
        /// </summary>
        private static void GetBusinessPartners()
        {
            if (BusinessPartners != null)
            {
                return;
            }

            try
            {
                var partnerData = DataManager.GetBusinessPartners();
                BusinessPartners = partnerData.Item1;
                BusinessPartnerGroups = partnerData.Item2;
                BusinessPartnerCurrencies = partnerData.Item3;
                BusinessPartnerTypes = partnerData.Item4;
                //BusinessPartnerContacts = partnerData.Item5;
                //BusinessPartnerProperties = partnerData.Item6;
                //BusinessPartnerPropertySelections = partnerData.Item7;

                if (ApplicationSettings.ShowOrHideGroupPartners.HasValue)
                {
                    if (ApplicationSettings.ShowOrHideGroupPartners == 1)
                    {
                        var localPartners = BusinessPartners.Where(x => x.PartnerGroup.BPGroupID == ApplicationSettings.ShowOrHidePartnerGroupID).ToList();
                        foreach (var businessPartner in localPartners)
                        {
                            BusinessPartners.Remove(businessPartner);
                        }

                        var localGroups = BusinessPartnerGroups.Where(x => x.BPGroupID == ApplicationSettings.ShowOrHidePartnerGroupID).ToList();
                        foreach (var group in localGroups)
                        {
                            BusinessPartnerGroups.Remove(group);
                        }
                    }
                    else
                    {
                        var localPartners = BusinessPartners.Where(x => x.PartnerGroup.BPGroupID != ApplicationSettings.ShowOrHidePartnerGroupID).ToList();
                        foreach (var businessPartner in localPartners)
                        {
                            BusinessPartners.Remove(businessPartner);
                        }

                        var localGroups = BusinessPartnerGroups.Where(x => x.BPGroupID != ApplicationSettings.ShowOrHidePartnerGroupID).ToList();
                        foreach (var group in localGroups)
                        {
                            BusinessPartnerGroups.Remove(group);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            SetPartners();
        }
     
        /// <summary>
        /// Method that gets the application transaction types.
        /// </summary>
        private static void GetTransactionTypes()
        {
            TransactionTypes = DataManager.GetTransactionTypes();

            var lairage = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.Lairage.ToString()));
            if (lairage != null)
            {
                TransactionTypeLairageId = lairage.NouTransactionTypeID;
            }

            var receipt = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.GoodsReceipt.ToString()));
            if (receipt != null)
            {
                TransactionTypeGoodsReceiptId = receipt.NouTransactionTypeID;
            }

            var saleEpos = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.SaleEpos.ToString()));
            if (saleEpos != null)
            {
                TransactionTypeSaleEposId = saleEpos.NouTransactionTypeID;
            }

            var stockAdjust = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.StockAdjust.ToString()));
            if (stockAdjust != null)
            {
                TransactionTypeStockAdjustIdId = stockAdjust.NouTransactionTypeID;
            }

            var edit = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.Edit.ToString()));
            if (edit != null)
            {
                TransactionTypeEditId = edit.NouTransactionTypeID;
            }

            var delivery = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.GoodsDelivery.ToString()));
            if (delivery != null)
            {
                TransactionTypeGoodsDeliveryId = delivery.NouTransactionTypeID;
            }

            var productionIssue = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.ProductionIssue.ToString()));
            if (productionIssue!= null)
            {
                TransactionTypeProductionIssueId = productionIssue.NouTransactionTypeID;
            }

            var productionReceipt = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.ProductionReceipt.ToString()));
            if (productionReceipt != null)
            {
                TransactionTypeProductionReceiptId = productionReceipt.NouTransactionTypeID;
            }

            var goodsReturn = TransactionTypes.FirstOrDefault(x => x.Description.CompareIgnoringCase(TransactionType.GoodsReturn.ToString()));
            if (goodsReturn != null)
            {
                TransactionTypeGoodsReturnId = goodsReturn.NouTransactionTypeID;
            }
        }

        /// <summary>
        /// Method that gets the application warehouse locations.
        /// </summary>
        private static void GetWarehouses()
        {
            // TODO Temporary - need to match up in application.
            WarehouseLocations = DataManager.GetWarehouses();
            var intake =
                WarehouseLocations.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Intake));

            if (ApplicationSettings.DefaultIntakeLocationId > 0)
            {
                WarehouseIntakeId = ApplicationSettings.DefaultIntakeLocationId;
            }
            else if (intake != null)
            {
                WarehouseIntakeId = intake.WarehouseID;
            }

            var returns =
                WarehouseLocations.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Returns));

            if (ApplicationSettings.DefaultReturnsLocationId > 0)
            {
                WarehouseIntakeId = ApplicationSettings.DefaultReturnsLocationId;
            }
            else if (returns != null)
            {
                WarehouseReturnsId = returns.WarehouseID;
            }

            var dispatch =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.Dispatch.ToLower()));

            if (dispatch != null)
            {
                DispatchId = dispatch.WarehouseID;
            }
            else
            {
                DispatchId = ApplicationSettings.DefaultDispatchLocationId;
            }

            var production =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.Production.ToLower()));

            if (production != null)
            {
                ProductionId = production.WarehouseID;
            }
            else
            {
                ProductionId = ApplicationSettings.DefaultIntoProductionLocationId;
            }
            
            var outOfProduction =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.FinishedGoodsChill.ToLower()));

            if (outOfProduction != null)
            {
                OutOfProductionId = outOfProduction.WarehouseID;
            }
            else
            {
                OutOfProductionId = ApplicationSettings.DefaultOutOfProductionLocationId;
            }

            var intoSlicing =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.IntoSlicing.ToLower()));

            if (intoSlicing != null)
            {
                WarehouseIntoSlicingId = intoSlicing.WarehouseID;
            }

            var intoJointing =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.IntoJointing.ToLower()));

            if (intoJointing != null)
            {
                WarehouseIntoJointingId = intoJointing.WarehouseID;
            }

            var butchery =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Equals(Constant.Butchery.ToLower()));

            if (butchery != null)
            {
                WarehouseButcheryId = butchery.WarehouseID;
            }

            var outOfButchery =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.OutOfButchery.ToLower()));

            if (outOfButchery != null)
            {
                WarehouseOutOfButcheryId = outOfButchery.WarehouseID;
            }

            var injection =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.Injection.ToLower()));

            if (injection != null)
            {
                WarehouseInjectionId = injection.WarehouseID;
            }

            var outOfInjection =
                WarehouseLocations.FirstOrDefault(x => x.Name.ToLower().Contains(Constant.OutOfInjection.ToLower()));

            if (outOfInjection != null)
            {
                WarehouseOutOfInjectionId = outOfInjection.WarehouseID;
            }

            PalletLocationId = ApplicationSettings.DefaultPalletLocationId == 0
                ? OutOfProductionId
                : ApplicationSettings.DefaultPalletLocationId;
        }
        
        /// <summary>
        /// Performs any db clean up required.
        /// </summary>
        private static void CleanUpDatabase()
        {
            DataManager.RemoveOldLogins();
        }

        /// <summary>
        /// Divide the partners up into suppliers and customers.
        /// </summary>
        private static void SetPartners()
        {
            if (BusinessPartners == null)
            {
                return;
            }
            
            CustomerPartners = new List<BusinessPartner>(BusinessPartners
                .Where(x => x.PartnerType.Type.Equals(BusinessPartnerType.Customer)
                    || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock)
                    || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier)));
            SupplierPartners = new List<BusinessPartner>(BusinessPartners
                 .Where(x => x.PartnerType.Type.Equals(BusinessPartnerType.Supplier)
                     || x.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier)
                     || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock)
                     || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndSupplier)));
           
        }

        /// <summary>
        /// Method that checks if the files have to be deleted.
        /// </summary>
        private static void CheckForLogDeletion()
        {
            // get the deletion  date
            var deleteDate = Settings.Default.DeleteLogsDate < DateTime.Today ? DateTime.Today : Settings.Default.DeleteLogsDate;

            if (deleteDate <= DateTime.Today)
            {
                DeleteLogFiles();

                // reset the next deletion date.
                var timeframe = Settings.Default.DeleteLogsTimeframe == 0 ? 30 : Settings.Default.DeleteLogsTimeframe;
                Settings.Default.DeleteLogsDate = deleteDate.AddDays(timeframe);
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// We begin the scanner processing here for responsiveness.
        /// </summary>
        private static void StartScannerProcessing()
        {
            if (ApplicationSettings.ScannerMode)
            {
                // Run on a background thread, and marshall back on to the main ui thread.
                //Task.Factory.StartNew(() =>
                //{
                    //ViewModelLocator.CreateScannerTouchscreen();
                    //ViewModelLocator.CreateScannerMainDispatch();
                    //ViewModelLocator.CreateTouchscreenOrders();
                //});
            }
        }

        /// <summary>
        /// Deletes the log files.
        /// </summary>
        private static void DeleteLogFiles()
        {
            Log.LogInfo(typeof(NouvemGlobal), "Attempting to delete the log files.");
            try
            {
                Array.ForEach(Directory.GetFiles(Path.Combine("@", Settings.Default.LogFilesPath)), File.Delete);
                Log.LogInfo(typeof(NouvemGlobal), "Log files deleted.");
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(NouvemGlobal), ex.Message);
            }
        }

        /// <summary>
        /// Schedules the jobs to process.
        /// </summary>
        private static void ScheduleJobs()
        {
            //var broadcast = BroadcastManager.Instance;
            //broadcast.StartListening();

            SchedulerManager.Instance.RestartApplication();
        }

        #endregion
    }
}

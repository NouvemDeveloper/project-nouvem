﻿// -----------------------------------------------------------------------
// <copyright file="EntityConnection.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.Win32;
using Nouvem.Model.BusinessObject;
using System.Collections.Generic;
using System.IO;
using Nouvem.BusinessLogic;
using Nouvem.Shared;

namespace Nouvem.Global
{
    using Nouvem.Logging;
    using Nouvem.Model.DataLayer.Repository;
    using Nouvem.Properties;
    using System;
    using System.Data.Entity.Core.EntityClient;
    using System.Data.SqlClient;

    /// <summary>
    /// Class responsible for the entity connection string creation.
    /// </summary>
    public static class EntityConnection
    {
        /// <summary>
        /// The logging reference.
        /// </summary>
        private static readonly ILogger Log = new Logger();

        /// <summary>
        /// Create the dynamic entity connection string.
        /// </summary>
        internal static bool CreateConnectionString()
        {
            // Specify the provider name, server and database.
            var providerName = ApplicationSettings.ProviderName;
            var serverName = ApplicationSettings.ServerName;
            var databaseName = ApplicationSettings.DatabaseName;
            var userid = ApplicationSettings.UserId;
            var password = ApplicationSettings.UserPassword;
            Settings.Default.Backup = password;

            // Initialize the connection string builder for the underlying provider.
            var sqlBuilder = new SqlConnectionStringBuilder();

            if (string.IsNullOrEmpty(userid) || string.IsNullOrEmpty(password))
            {
                // Set the properties for the data source.
                sqlBuilder.DataSource = serverName;
                sqlBuilder.InitialCatalog = databaseName;
                sqlBuilder.IntegratedSecurity = true;
            }
            else
            {
                sqlBuilder.DataSource = serverName;
                sqlBuilder.InitialCatalog = databaseName;
                sqlBuilder.UserID = userid;
                sqlBuilder.Password = password;
            }

            // Build the SqlConnection connection string.
            var providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            var entityBuilder = new EntityConnectionStringBuilder
            {
                Provider = providerName,
                ProviderConnectionString = providerString,
                Metadata = Settings.Default.EntityMetadata
            };

            // Store the entity connection string (called from the dbContext.cs file), and the standard connection string
            ApplicationSettings.ConnectionString = entityBuilder.ToString();
            ApplicationSettings.ApplicationConnectionString = providerString;
        
            var key = Registry.CurrentUser.CreateSubKey(@"Nouvem\Settings");

            if (key != null)
            {
                //storing the values  
                var connectionstring = Security.Security.EncryptString(ApplicationSettings.ConnectionString ?? string.Empty);
                var appConnectionstring = Security.Security.EncryptString(ApplicationSettings.ApplicationConnectionString ?? string.Empty);
                var localPassword = Security.Security.EncryptString(password ?? string.Empty);
                key.SetValue(Constant.ConnectionString, connectionstring);
                key.SetValue(Constant.Provider, providerName ?? string.Empty);
                key.SetValue(Constant.Server, serverName ?? string.Empty);
                key.SetValue(Constant.Database, databaseName ?? string.Empty);
                key.SetValue(Constant.UserID, userid ?? string.Empty);
                key.SetValue(Constant.Password, localPassword);
                key.SetValue(Constant.Company, ApplicationSettings.CompanyName ?? string.Empty);
                key.SetValue(Constant.ApplicationConnString, appConnectionstring);
                key.SetValue(Constant.LoggedInUser, ApplicationSettings.LoggedInUserIdentifier ?? string.Empty);
                key.Close();
            }

            Log.LogInfo(typeof(EntityConnection), string.Format("CreateConnectionString(): Entity string created - {0}", ApplicationSettings.ConnectionString));
            return true;
        }

        /// <summary>
        /// Uses the back up file connection string.
        /// </summary>
        /// <returns>A flag, indicating whether a connection can be made to the database.</returns>
        internal static bool GetConnectionSettings()
        {
            Log.LogInfo(typeof(EntityConnection), "GetConnectionSettings(): Attempting to retrieve the connection settings");
            var resetConnectionData = false;
            try
            {
                var key = Registry.CurrentUser.OpenSubKey(@"Nouvem\Settings");

                //if it does exist, retrieve the stored values  
                if (key != null)
                {
                    var conn = key.GetValue(Constant.ConnectionString);
                    var provider = key.GetValue(Constant.Provider);
                    var server = key.GetValue(Constant.Server);
                    var database = key.GetValue(Constant.Database);
                    var userId = key.GetValue(Constant.UserID);
                    var password = key.GetValue(Constant.Password);
                    var company = key.GetValue(Constant.Company);
                    var appConnString = key.GetValue(Constant.ApplicationConnString);
                    var user = key.GetValue(Constant.LoggedInUser);
                    key.Close();

                    ApplicationSettings.ConnectionString = conn != null ? Security.Security.DecryptString((byte[])conn) : string.Empty;
                    ApplicationSettings.ProviderName =  provider != null ? provider.ToString() : "System.Data.SqlClient";
                    ApplicationSettings.ServerName = server != null ? server.ToString() : string.Empty;
                    ApplicationSettings.DatabaseName = database != null ? database.ToString() : string.Empty;
                    ApplicationSettings.UserId = userId != null ? userId.ToString() : string.Empty;
                    try
                    {
                        ApplicationSettings.UserPassword = password != null ? Security.Security.DecryptString((byte[])password) : string.Empty;
                        Settings.Default.Backup = ApplicationSettings.UserPassword;
                    }
                    catch (Exception e)
                    {
                        Log.LogError(typeof(EntityConnection), $"Password Error:{e.Message}");
                        ApplicationSettings.UserPassword = Settings.Default.Backup;
                        resetConnectionData = true;
                    }
                   
                    ApplicationSettings.CompanyName = company != null ? company.ToString() : string.Empty;

                    try
                    {
                        ApplicationSettings.ApplicationConnectionString = appConnString != null ? Security.Security.DecryptString((byte[])appConnString) : string.Empty;
                    }
                    catch (Exception e)
                    {
                        Log.LogError(typeof(EntityConnection), $"ApplicationConnectionString Error:{e.Message}. Attempting to fix...");
                        var sqlBuilder = new SqlConnectionStringBuilder();
                        if (string.IsNullOrEmpty(ApplicationSettings.UserId) || string.IsNullOrEmpty(ApplicationSettings.UserPassword))
                        {
                            // Set the properties for the data source.
                            sqlBuilder.DataSource = ApplicationSettings.ServerName;
                            sqlBuilder.InitialCatalog = ApplicationSettings.DatabaseName;
                            sqlBuilder.IntegratedSecurity = true;
                        }
                        else
                        {
                            sqlBuilder.DataSource = ApplicationSettings.ServerName;
                            sqlBuilder.InitialCatalog = ApplicationSettings.DatabaseName;
                            sqlBuilder.UserID = ApplicationSettings.UserId;
                            sqlBuilder.Password = ApplicationSettings.UserPassword;
                        }

                        // Build the SqlConnection connection string.
                        var providerString = sqlBuilder.ToString();
                        ApplicationSettings.ApplicationConnectionString = providerString;
                        if (!string.IsNullOrEmpty(ApplicationSettings.ApplicationConnectionString))
                        {
                            Log.LogInfo(typeof(EntityConnection), "ApplicationConnectionString Error Fixed");
                        }

                        resetConnectionData = true;
                    }
                    
                    ApplicationSettings.LoggedInUserIdentifier = user != null ? user.ToString() : string.Empty;

                    Log.LogInfo(typeof(EntityConnection), "Connection settings retrieved");
                    if (resetConnectionData)
                    {
                        CreateConnectionString();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.LogInfo(typeof(EntityConnection), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Create the dynamic entity connection string.
        /// </summary>
        internal static bool SaveConnectionString(Server data)
        {
            var key = Registry.CurrentUser.CreateSubKey(@"Nouvem\Settings");

            if (key != null)
            {
                var dbCredentials = Security.Security.EncryptString(data.Credentials ?? string.Empty);
                key.SetValue($"{Constant.Database}{data.ConnectionNo}", dbCredentials);
                key.Close();
            }
          
            return true;
        }

        /// <summary>
        /// Retrieve all the servers.
        /// </summary>
        /// <returns>A collection of server details.</returns>
        internal static IList<Server> GetServers()
        {
            var localServers = new List<Server>();

            try
            {
                var key = Registry.CurrentUser.OpenSubKey(@"Nouvem\Settings");

                //if it does exist, retrieve the stored values  
                if (key != null)
                {
                    var dbCredentials1  = key.GetValue($"{Constant.Database}1");
                    var dbCredentials2 = key.GetValue($"{Constant.Database}2");
                    var dbCredentials3 = key.GetValue($"{Constant.Database}3");
                    var dbCredentials4 = key.GetValue($"{Constant.Database}4");
                    var dbCredentials5 = key.GetValue($"{Constant.Database}5");
                    var dbCredentials6 = key.GetValue($"{Constant.Database}6");
                    var dbCredentials7 = key.GetValue($"{Constant.Database}7");
                    var dbCredentials8 = key.GetValue($"{Constant.Database}8");
                    var dbCredentials9 = key.GetValue($"{Constant.Database}9");
                    var dbCredentials10 = key.GetValue($"{Constant.Database}10");
                    var dbCredentials11 = key.GetValue($"{Constant.Database}11");
                    var dbCredentials12 = key.GetValue($"{Constant.Database}12");
                    var dbCredentials13 = key.GetValue($"{Constant.Database}13");
                    var dbCredentials14 = key.GetValue($"{Constant.Database}14");
                    var dbCredentials15 = key.GetValue($"{Constant.Database}15");
                    var dbCredentials16 = key.GetValue($"{Constant.Database}16");
                    var dbCredentials17 = key.GetValue($"{Constant.Database}17");
                    var dbCredentials18 = key.GetValue($"{Constant.Database}18");
                    var dbCredentials19 = key.GetValue($"{Constant.Database}19");
                    var dbCredentials20 = key.GetValue($"{Constant.Database}20");

                    key.Close();

                    if (dbCredentials1 != null)
                    {
                        var server = Security.Security.DecryptString((byte[]) dbCredentials1).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 1, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 1, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials2 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials2).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 2, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 2, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials3 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials3).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 3, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 3, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials4 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials4).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 4, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 4, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials5 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials5).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 5, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 5, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials6 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials6).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 6, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 6, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials7 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials7).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 7, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 7, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials8 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials8).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 8, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 8, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials9 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials9).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 9, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 9, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials10 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials10).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 10, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 10, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials11 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials11).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 11, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 11, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials12 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials12).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 12, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 12, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials13 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials13).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 13, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 13, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials14 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials14).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 14, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 14, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials15 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials15).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 15, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 15, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials16 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials16).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 16, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 16, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials17 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials17).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 17, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 17, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials18 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials18).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 18, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 18, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials19 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials19).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 19, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 19, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }

                    if (dbCredentials20 != null)
                    {
                        var server = Security.Security.DecryptString((byte[])dbCredentials20).Split('|');
                        if (server.Length == 5)
                        {
                            // sql server authentication
                            localServers.Add(new Server { ConnectionNo = 20, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2], UserId = server[3], Password = server[4] });
                        }
                        else
                        {
                            // windows authentication
                            localServers.Add(new Server { ConnectionNo = 20, CompanyName = server[0], ServerName = server[1], DefaultDatabaseName = server[2] });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogInfo(typeof(EntityConnection), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            
            return localServers;
        }

        /// <summary>
        /// Creates a back up connection string, stored and encrypted to an xml file.
        /// </summary>
        /// <param name="data">The entity connection data to store.</param>
        internal static void StoreConnectionString(Dictionary<string, string> data)
        {
            try
            {
                var xmlConn = XmlHelper.CreateXml(data);
                if (!Directory.Exists(Settings.Default.ConnectionStringPath))
                {
                    Directory.CreateDirectory(Settings.Default.ConnectionStringPath);
                }

                var filePath = Path.Combine(Settings.Default.ConnectionStringPath, "conn.xml");
                Security.Security.EncryptXmlFile(xmlConn, filePath);
            }
            catch (Exception ex)
            {
                Log.LogError(typeof(EntityConnection), ex.Message);
            }
        }

        /// <summary>
        /// Tests the db connection.
        /// </summary>
        /// <returns>A flag, indicating a successful connection, or not.</returns>
        internal static bool TestConnection()
        {
            var testRepository = new UserRepository();
            Log.LogError(typeof(EntityConnection), string.Format("TestConnection(): Attempting to connect to the database with the following entity string - {0}", ApplicationSettings.ConnectionString));

            try
            {
                return testRepository.TestDatabase();
                Log.LogError(typeof(EntityConnection), "Connection to database successful");
            }
            catch (Exception)
            {
                Log.LogError(typeof(EntityConnection), string.Format("Connection to database failed - {0}", ApplicationSettings.ConnectionString));
                return false;
            }
        }
    }
}

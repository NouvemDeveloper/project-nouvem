﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationSettings.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using System.Windows.Media;
using Neodynamic.SDK.Printing;

namespace Nouvem.Global
{
    using System;
    using DevExpress.Xpf.Grid;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;

    public static class ApplicationSettings
    {
        /// <summary>
        /// Gets the dispatch mode.
        /// </summary>
        public static DispatchMode DispatchMode { get; set; }

        /// <summary>
        /// Gets the dispatch mode.
        /// </summary>
        public static bool ChangePriceOnDispatchTouchscreen { get; set; }

        public static bool ShowBatchDispatchTouchscreenLine { get; set; }

        public static bool ShowSupplierDispatchTouchscreenLine { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static string LabelImagePath { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool AutoUpdateRecipePrice { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool AlwaysManuallySelectBatchOnMix1 { get; set; }

        /// <summary>
        /// Gets or sets the Nouvem user password.
        /// </summary>
        public static string NouvemPassword { get; set; }

        public static LairageType LairageType { get; set; }

        public static string ProviderName { get; set; } = "System.Data.SqlClient";
        public static string ServerName { get; set; }
        public static string DatabaseName { get; set; }
        public static string EntityMetaData { get; set; } = "res://*/Model.DataLayer.NouvemEntities.csdl|res://*/Model.DataLayer.NouvemEntities.ssdl|res://*/Model.DataLayer.NouvemEntities.msl";
        public static string CompanyName { get; set; }
        public static string ApplicationConnectionString { get; set; }
        public static string ConnectionString { get; set; }
        public static string UserId { get; set; }
        public static string UserPassword { get; set; }
        public static string DBPassword { get; set; }
        public static int BeefIntakeID { get; set; }

        public static int TileDeliveryAddressLength { get; set; }

        /// <summary>
        /// Gets the dispatch mode.
        /// </summary>
        public static bool OnlyShowLivestockSuppliersAtBusinessPartner { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static string FTracePath { get; set; }

        public static string FTraceURL { get; set; }

        public static string FTracePassword { get; set; }

        public static string FTraceUserName { get; set; }
        public static int BordBiaQAResidency { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static string KeypakReport { get; set; }

        /// <summary>
        /// Gets the dispatch mode.
        /// </summary>
        public static bool ClearBatchOnDispatchProductChange { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static string Gs1Identifier { get; set; }

        public static bool AutoSplitCarcassAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static string ExportReportPath { get; set; }

        public static double RecipeScreenOffset { get; set; } = 20;

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool PrintReportAtPalletisation { get; set; }

        public static string WindowsCalculatorPath { get; set; }

        /// <summary>
        /// Gets or sets the department pricing path.
        /// </summary>
        public static string DeptAnimalPricingPath { get; set; }

        /// <summary>
        /// Gets or sets the department pricing file format.
        /// </summary>
        public static string DeptFileFormat { get; set; }

        /// <summary>
        /// Gets or sets the department pricing file format.
        /// </summary>
        public static string DeptFactoryCode { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool DontCreateCSVForPalletCard { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static int Gs1IdentifierLength { get; set; }

        /// <summary>
        /// Gets the intake mode.
        /// </summary>
        public static IntakeMode IntakeMode { get; set; }
        public static IntakeMode DefaultIntakeMode { get; set; }

        public static bool UseDisplayPassword { get; set; }
        public static bool SelectIntakeBatchesAtDispatch { get; set; }

        public static bool PasswordMustBeUnique { get; set; }
        public static bool GetBoxCountAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static string WelshDestination { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static string OrganicDestination { get; set; }

        public static string StandardDestination { get; set; }
        public static string SelectFarmDestination { get; set; }
        public static string ContinentalDestination { get; set; }

        public static bool UsingKillLineQueueNumbers { get; set; }

        /// <summary>
        /// Gets the intake mode.
        /// </summary>
        public static SequencerMode SequencerMode { get; set; }

        /// <summary>
        /// Gets the device indicator communication method..
        /// </summary>
        public static string Indicator2CommunicationType { get; set; }

        /// <summary>
        /// Gets the device indicator communication method..
        /// </summary>
        public static string Indicator3CommunicationType { get; set; }

        /// <summary>
        /// Gets or sets the lot number to be sent to AIMs as part of a movement request.
        /// </summary>
        internal static bool ReprintStockMoveItem { get; set; }

        /// <summary>
        /// Gets or sets the lot number to be sent to AIMs as part of a movement request.
        /// </summary>
        internal static bool DisallowProductRemovalAtDispatch{ get; set; }

        internal static bool ShowProductRemarksAtSaleOrder { get; set; }

        /// <summary>
        /// Gets or sets the lot number to be sent to AIMs as part of a movement request.
        /// </summary>
        internal static bool ReprintStockMoveBoxItem { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static bool DeliveryDateCannotBeInThePast { get; set; }

        /// <summary>
        /// Gets a value indicating whether we go to the db at dispatch processing to retrieve order.
        /// </summary>
        public static bool OnlyShowDispatchedWeightsAtScannerDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether we go to the db at dispatch processing to retrieve order.
        /// </summary>
        public static bool ShowBoxCountAtScannerDispatch { get; set; }

        /// <summary>
        /// Gets the plant code.
        /// </summary>
        public static string StockMoveGrid { get; set; }

        /// <summary>
        /// Gets the device indicator model.
        /// </summary>
        public static string Indicator2Model { get; set; }

        /// <summary>
        /// Gets the device indicator alibi files settings.
        /// </summary>
        public static string IndicatorAlibiPath2 { get; set; }

        /// <summary>
        /// Gets the device indicator alibi files settings.
        /// </summary>
        public static string IndicatorAlibiPath3 { get; set; }

        /// <summary>
        /// Gets a value indicating whether the indicator is in use.
        /// </summary>
        public static bool ConnectToIndicator2 { get; set; }

        /// <summary>
        /// Gets a value indicating whether the indicator is in use.
        /// </summary>
        public static bool ConnectToIndicator3 { get; set; }

        /// <summary>
        /// Gets the indicator font size.
        /// </summary>
        public static double IndicatorFontSize2 { get; set; }

        /// <summary>
        /// Gets the indicator font size.
        /// </summary>
        public static double IndicatorFontSize3 { get; set; }

        /// <summary>
        /// Gets the indicator stable reads count.
        /// </summary>
        public static int IndicatorStableReads2 { get; set; }

        /// <summary>
        /// Gets the indicator stable reads count.
        /// </summary>
        public static int IndicatorStableReads3 { get; set; }

        /// <summary>
        /// Gets the indicator string variables.
        /// </summary>
        public static string IndicatorStringVariables2 { get; set; }

        /// <summary>
        /// Gets the indicator string variables.
        /// </summary>
        public static string IndicatorStringVariables3 { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using stable reads/tolerance for motion.
        /// </summary>
        public static bool UseStableReadsForMotion2 { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using stable reads/tolerance for motion.
        /// </summary>
        public static bool UseStableReadsForMotion3 { get; set; }

        /// <summary>
        /// Gets a value indicating whether the no movement on scales warning is to be ignored.
        /// </summary>
        public static bool IgnoreNoMovementmentOnScalesWarning2 { get; set; }

        /// <summary>
        /// Gets a value indicating whether the no movement on scales warning is to be ignored.
        /// </summary>
        public static bool IgnoreNoMovementmentOnScalesWarning3 { get; set; }

        /// <summary>
        /// Gets a value indicating whether the no movement on scales warning is to be ignored.
        /// </summary>
        public static bool UseHistoricDispatchMode { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static decimal DisturbBy2 { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static decimal DisturbBy3 { get; set; }

        /// <summary>
        /// Gets the indicator stable reads count.
        /// </summary>
        public static int ImportWarehouseId { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static string ZeroScalesCommand2 { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static string ZeroScalesCommand3 { get; set; }

        /// <summary>
        /// Gets a value indicating whether a manual weight can be recorded by the touchscreen operator.
        /// </summary>
        public static bool CanManuallyEnterWeight2 { get; set; }

        /// <summary>
        /// Gets the dispatch tolerance weight allowed.
        /// </summary>
        public static decimal ToleranceWeight2 { get; set; }

        /// <summary>
        /// Gets the dispatch tolerance weight allowed.
        /// </summary>
        public static decimal ToleranceWeight3 { get; set; }


        /// <summary>
        /// Gets a value indicating whether a manual weight can be recorded by the touchscreen operator.
        /// </summary>
        public static bool CanManuallyEnterWeight3 { get; set; }

        /// <summary>
        /// Gets the indicator small font size.
        /// </summary>
        public static double IndicatorFontSizeSmall2 { get; set; }

        /// <summary>
        /// Gets the indicator small font size.
        /// </summary>
        public static double IndicatorFontSizeSmall3 { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static int MinWeightInDivisions2 { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static int MinWeightInDivisions3 { get; set; }

        /// <summary>
        /// Gets the device indicator alibi files settings.
        /// </summary>
        public static int IndicatorWaitTime2 { get; set; }

        /// <summary>
        /// Gets the device indicator alibi files settings.
        /// </summary>
        public static int IndicatorWaitTime3 { get; set; }

        /// <summary>
        /// Gets the application weights precision value.
        /// </summary>
        public static decimal ScalesDivision2 { get; set; }

        /// <summary>
        /// Gets the application weights precision value.
        /// </summary>
        public static decimal ScalesDivision3 { get; set; }

        /// <summary>
        /// Gets a value the scales must drop below in order for a weight to be recorded.
        /// </summary>
        public static decimal? DropBelow2 { get; set; }

        /// <summary>
        /// Gets a value the scales must drop below in order for a weight to be recorded.
        /// </summary>
        public static decimal? DropBelow3 { get; set; }

        /// <summary>
        /// Gets the device indicator settings.
        /// </summary>
        public static string Indicator2Settings { get; set; }

        /// <summary>
        /// Gets the device indicator settings.
        /// </summary>
        public static string Indicator3Settings { get; set; }

        /// <summary>
        /// Gets the device indicator model.
        /// </summary>
        public static string Indicator3Model { get; set; }

        /// <summary>
        /// Gets a value indicating whether a manual weight can be recorded by the touchscreen operator.
        /// </summary>
        public static int CurrentIndicator { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static bool ResettingVealCategoriesAtSequencer { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int YoungVealAge { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int OldVealAge { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static bool WarnIfDuplicateProductionReference { get; set; }

        public static bool DisallowSlashesInProductionName { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static bool CheckDispatchBatchAgainstProduct { get; set; }

        public static bool CheckPartnerForLiveOrder { get; set; }

        /// <summary>
        /// Gets the intake mode.
        /// </summary>
        public static GraderMode GraderMode { get; set; }

        /// <summary>
        /// Gets the intake mode.
        /// </summary>
        public static bool DisplayUTMOTMMessage { get; set; }

        /// <summary>
        /// Gets the intake mode.
        /// </summary>
        public static bool ShowFarmAssuredAtSequencer { get; set; }
        public static bool ShowIdentigenAtGrader { get; set; }
        public static bool ShowKillTypeAtGrader { get; set; }
        public static bool ShowPrivateAtGrader { get; set; }
        public static bool ShowDressSpecAtGrader { get; set; }
        public static bool ShowDestinationsAtGrader { get; set; }
        public static bool ShowPriceAtDispatch { get; set; }

        /// <summary>
        /// Gets value indicating whether we are manually entering the batch numbers.
        /// </summary>
        public static int DefaultQty { get; set; }

        /// <summary>
        /// Gets a value indicating whether standard print is disabled at intake.
        /// </summary>
        public static bool DisablePrintingAtIntake { get; set; }

        /// <summary>
        /// Gets the number of labels to print before a forced slow down is needed for external printers.
        /// </summary>
        public static int ExternalLabelsToPrintBeforeSlowDown { get; set; }

        /// <summary>
        /// Gets the forced slow down time needed for external printers.
        /// </summary>
        public static int ExternalLabelsSlowDownTime { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sequencer queue is auto refreshed.
        /// </summary>
        public static bool RefreshSequencerQueue { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sequencer queue is auto refreshed.
        /// </summary>
        public static int ForeProduct { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sequencer queue is auto refreshed.
        /// </summary>
        public static int HindProduct { get; set; }

        public static int FullCarcassProduct { get; set; }
        public static bool UseFullCarcassProduct { get; set; }

        public static bool PrintSequencerLabel { get; set; }

        /// <summary>
        /// Gets a value indicating whether standard print is disabled at production.
        /// </summary>
        public static bool DisablePrintingAtOutOfProduction { get; set; }

        public static bool UsingItemLabelsForPieceLabelsAtOutOfProduction { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static IntoProductionMode IntoProductionMode { get; set; }

        public static bool CarcassSplitReweighing { get; set; }

        public static bool AutoCarcassSplitReweighingAtDispatch { get; set; }

        public static int MinQtyForRecipeFinishedProductManualComplete { get; set; }

        public static string RecipeNegativeBatchStockMessage { get; set; }

        public static int AutoTypicalWeightSetWait { get; set; }

        public static bool UseHistoricProductionBatchNo { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static bool AlwaysShowAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static double AttributesExpanderHeight { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static int ManualBatchDefaultLocationID { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static bool CheckForUnsavedBatchAttributes { get; set; }

        /// <summary>
        /// Gets the application weights precision value.
        /// </summary>
        public static bool RecallBatchAtIntakeScan { get; set; }
        public static bool RecallProductAtScan { get; set; }
        public static int RecallProductAtScanStartPos { get; set; }
        public static int RecallProductAtScanLength { get; set; }

        public static bool ZeroTareOutsideOfDispatch { get; set; }
        public static bool DisableContainerTareSelection { get; set; }
        public static bool DisableTouchscreenGenericSelection { get; set; }

         /// <summary>
        /// Gets a value indicating whether the sales search screen is to be kept open.
        /// </summary>
        public static string DeptOfAgricultureImportCode { get; set; }

        /// <summary>
        /// Gets a value indicating whether a batch number can be manually created by the user.
        /// </summary>
        public static bool PrintBatchLabelOnItakeCompletion { get; set; }

        /// <summary>
        /// Gets a value indicating whether a batch number can be manually created by the user.
        /// </summary>
        public static bool BatchAppendDDMM { get; set; }
        
         /// <summary>
        /// Gets a value indicating whether the sales search screen is to be kept open.
        /// </summary>
        public static string DeptOfAgricultureImportFarm { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are pricing items y the piece rather than qty.
        /// </summary>
        public static bool IntakeItemByThePiece { get; set; }


        /// <summary>
        /// Gets a value indicating whether a qa label is to be printed after the first label in the batch is printed.
        /// </summary>
        public static bool ResetLabelOnProductSelection { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sales search screen is to be kept open.
        /// </summary>
        public static bool DefaultShippingDateToPreviousDay { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static bool CarcassSearchScreenLoaded { get; set; }

        /// <summary>
        /// Gets a value indicating whether the min/max product weights are checked at dispatch.
        /// </summary>
        public static bool CheckWeightBoundaryAtCarcassDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sheep are manually sequenced.
        /// </summary>
        public static bool ManuallySequenceSheep { get; set; }

        /// <summary>
        /// Gets or sets the lot number to be sent to AIMs as part of a movement request.
        /// </summary>
        internal static int CurrentMOVLotNo { get; set; }

        /// <summary>
        /// Gets or sets the lot number to be sent to AIMs as part of a movement request.
        /// </summary>
        internal static string AIMMovementCountryCodes { get; set; }

        /// <summary>
        /// Gets or sets the lot number to be sent to AIMs as part of a movement request.
        /// </summary>
        internal static bool AllowPrinterSelectionAtGraderReprint { get; set; }

        /// <summary>
        /// Gets a value indicating whether the min/max product weights are checked at dispatch.
        /// </summary>
        public static bool SyncPartnersLoadedForFirstTime { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static OutOfProductionMode OutOfProductionMode { get; set; }

        /// <summary>
        /// Gets a value indicating whether the min/max product weights are checked at dispatch.
        /// </summary>
        public static SolidColorBrush TouchscreenAlternateRowColour { get; set; }

        /// <summary>
        /// Gets a value indicating whether we check at dispatch for a linked db record when scanning.
        /// </summary>
        public static double TouchscreenKeyboardHeight { get; set; }

        /// <summary>
        /// Gets a value indicating whether we check at dispatch for a linked db record when scanning.
        /// </summary>
        public static double TouchscreenKeyboardWidth { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static DispatchPanel DispatchPanelMode { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static OutOfProductionPanel OutOfProductionPanelMode { get; set; }

        /// <summary>
        /// Gets the messae box top value.
        /// </summary>
        public static double MessageBoxTop { get; set; }

        /// <summary>
        /// Gets the messae box top value.
        /// </summary>
        public static double MessageBoxLeft { get; set; }

        /// <summary>
        /// Gets the plant code.
        /// </summary>
        public static string PlantCode { get; set; }

        /// <summary>
        /// Gets the plant code.
        /// </summary>
        public static string DefaultAnimalCountry { get; set; }

        /// <summary>
        /// Gets the plant code.
        /// </summary>
        public static int ScotBeefSupplierID { get; set; }

        /// <summary>
        /// Gets the plant code.
        /// </summary>
        public static bool ScotBeefEnforceIdentigen{ get; set; }

          /// <summary>
        /// Gets into prod bottom group menu buttons width.
        /// </summary>
        public static double TouchscreenOutOfProductionGroupMenuWidth { get; set; }
    
        /// <summary>
        /// Gets a value indicating whether we check at dispatch for a linked db record when scanning.
        /// </summary>
        public static bool CheckForLinkedDatabaseScannedDispatchSerial { get; set; }

        /// <summary>
        /// Gets the linked db at dispatch scan identity insert start value.
        /// </summary>
        public static int LinkedDatabaseScannedDispatchSerialIdentity { get; set; }
    
        /// <summary>
        /// Gets the linked db at dispatch scan identity insert start value.
        /// </summary>
        public static int LinkedDatabaseScannedDispatchSerialStart { get; set; }

        /// <summary>
        /// Gets the linked db at dispatch scan identity insert start value.
        /// </summary>
        public static int GraderLabelUOMAge { get; set; }

        /// <summary>
        /// Gets the linked db at dispatch scan identity insert start value.
        /// </summary>
        public static int MinSequencerEartagLength { get; set; }
        public static bool SearchByEartagAndKillNoAtSequencer { get; set; }
        public static bool UsingHideStationAtSequencer { get; set; }
        public static bool HidePullTerminalAtSequencer { get; set; }
        public static int LabelsToPrintAtSequencer { get; set; }
        public static bool UsingSheepWand { get; set; }
        public static bool ShowFreezerTypeAtLairage { get; set; }
        public static bool DressSpecRequiredAtGrader{ get; set; }
        public static bool DisableCategoryChangeAtGrader { get; set; }

        /// <summary>
        /// Gets the linked db at dispatch scan identity insert start value.
        /// </summary>
        public static int MinSequencerHerdNoLength { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sales search screen is to be kept open.
        /// </summary>
        public static bool SalesSearchKeepVisible { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are pricing items y the piece rather than qty.
        /// </summary>
        public static bool PricingItemByThePiece { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are pricing items y the piece rather than qty.
        /// </summary>
        public static bool RetrieveLivePriceOnRecentOrders { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are pricing items y the piece rather than qty.
        /// </summary>
        public static bool BoxQtyAlwaysOne { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are pricing items y the piece rather than qty.
        /// </summary>
        public static bool PriceTo5DecimalPlaces { get; set; }

        /// <summary>
        /// Gets a value indicating whether default templates are applied to a group on set up.
        /// </summary>
        public static bool SetDefaultGroupTemplates { get; set; }

        /// <summary>
        /// Gets the default traceability template.
        /// </summary>
        public static int TraceabilityDefaultTemplateId { get; set; }

        /// <summary>
        /// Gets the default quality template.
        /// </summary>
        public static int QualityDefaultTemplateId { get; set; }

        /// <summary>
        /// Gets the default dates template.
        /// </summary>
        public static int DatesDefaultTemplateId { get; set; }

        /// <summary>
        /// Gets a value indicating whether a customer price book is auto created when the customer is created.
        /// </summary>
        public static bool AutoCreateCustomerPriceList { get; set; }

        /// <summary>
        /// Gets a value indicating whether a customer price book is auto created when the customer is created.
        /// </summary>
        public static string ComboBoxSearchType { get; set; }

        /// <summary>
        /// Gets a value indicating whether a customer price book is auto created when the customer is created.
        /// </summary>
        public static bool UseNeedleEcoTheme { get; set; }

        /// <summary>
        /// Gets a value indicating whether the reports are available on the touchscreens.
        /// </summary>
        public static bool DisplayReportsOnTouchscreen { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch docket is printed on order commpleton.
        /// </summary>
        public static bool PrintDispatchDocketOnCompletion { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are seraching using the 'includes' criteria.
        /// </summary>
        public static bool TouchscreenPartnerSearchByIncludes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the trim groups are ignored for concessions.
        /// </summary>
        public static bool IgnoreTrimGroupsForConcessions { get; set; }

        /// <summary>
        /// Gets a value indicating whether the customers can be selected at dispatch.
        /// </summary>
        public static bool DisallowCustomerSelectionAtDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the customers can be selected at dispatch.
        /// </summary>
        public static bool AllowCustomerChangeOnOrders { get; set; }

        public static bool CheckAvailableStockOnSaleOrders { get; set; }

        public static bool CheckForImportedLabelAtStockTake { get; set; }

        public static int DisableManualWeighingAtIntake { get; set; }

        /// <summary>
        /// Gets a value indicating whether the moules can be changed on the touchscreens.
        /// </summary>
        public static bool DisallowTouchscreenModuleChange { get; set; }

        /// <summary>
        /// Gets a value indicating whether the touchscreen orders filter is retained when loading the screen.
        /// </summary>
        public static bool RetainTouchscreenOrdersFilter { get; set; }

        /// <summary>
        /// Gets the comma delimited list of reports to be displayed on the touchscreen.
        /// </summary>
        public static string ReportsOnTouchscreen { get; set; }

        /// <summary>
        /// Gets a value indicating whether a warning is displayed to the user when trying to complete an unfilled dispatch order.
        /// </summary>
        public static bool DispatchOrderNotFilledWarning { get; set; }

        /// <summary>
        /// Gets a value indicating whether an authorisation check is required for a user attempting to complete an unfilled order.
        /// </summary>
        public static bool UnfilledDispatchCompletionAuthorisationCheck { get; set; }
      
        /// <summary>
        /// Gets a value indicating whether a route must be selected on an order.
        /// </summary>
        public static bool RouteMustBeSetOnOrder { get; set; }

        /// <summary>
        /// Gets a value indicating whether there is a check to be made for part boxes on completion.
        /// </summary>
        public static bool CheckForPartBoxesAtDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether there is a check to be made for part boxes on completion.
        /// </summary>
        public static bool CheckForOutstandingOrders { get; set; }

        /// <summary>
        /// Gets a value indicating whether the min/max product weights are checked at dispatch.
        /// </summary>
        public static bool CheckWeightBoundaryAtDispatch { get; set; }

        private static DateTime salesSearchFromDate { get; set; }
        private static DateTime salesSearchToDate { get; set; }

        public static string SalesSearchDateDispatch { get; set; }
        public static string SalesSearchDateInvoice { get; set; }
        public static string SalesSearchDateSaleOrder { get; set; }

        /// <summary>
        /// Gets the search from date on the search screen.
        /// </summary>
        public static DateTime SalesSearchFromDate
        {
            get
            {
                return salesSearchFromDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    salesSearchFromDate = DateTime.Today;
                }
                else
                {
                    salesSearchFromDate = value;
                }
            }
        }

        /// <summary>
        /// Get or sets the from purchases date.
        /// </summary>
        public static DateTime FromPurchasesDate { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool FromPurchasesShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool FromCreditReturnsShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static DateTime PaymentProposalFromDate { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool PaymentProposalShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static DateTime PaymentProposalCreateFromDate { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool PaymentProposalCreateShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool PaymentProposalKeepVisible { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static DateTime LairageSearchFromDate { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static DateTime LairageSearchToDate { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool LairageSearchShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool LairageSearchShowFAOnly { get; set; }

        /// <summary>
        /// Gets or sets the Lairage search from date.
        /// </summary>
        public static bool LairageSearchShowKeepVisible { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static DateTime AnimalsSearchFromDate { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static DateTime AnimalsSearchToDate { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool AnimalsSearchShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool AnimalsSearchShowKeepVisible{ get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static DateTime AlertsFromDate { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool AlertsShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool GenerateDispatchFile { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static string DispatchFilePath { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static DateTime WorkflowSearchFromDate { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool WorkflowSearchShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static DateTime BatchSetUpFromDate { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool BatchSetupShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool ShowAlertsForAllUsers { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool AlertsKeepVisible { get; set; }

        public static bool ShowMessageBoxOnSaleOrderAdd { get; set; }

        /// <summary>
        /// Gets or sets the animals search from date.
        /// </summary>
        public static bool DisableChequeReportAfterFirstPrint { get; set; }
        public static int InsuranceAge { get; set; }

        /// <summary>
        /// Gets the search from date on the search screen.
        /// </summary>
        public static DateTime SalesSearchToDate
        {
            get
            {
                return salesSearchToDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    salesSearchToDate = DateTime.Today;
                }
                else
                {
                    salesSearchToDate = value;
                }
            }
        }

        public static bool CanOnlyBoxOffProductQtyAtProduction { get; set; }

        /// <summary>
        /// Gets a value indicating whether a qa label is to be printed after the first label in the batch is printed.
        /// </summary>
        public static bool PrintQALabelAfterFirstLabelInBatch { get; set; }

         /// <summary>
        /// Gets a value indicating whether we are displaying all orders on the search screen.
        /// </summary>
        public static bool ReportSalesSearchDataKeepVisible { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are displaying all orders on the search screen.
        /// </summary>
        public static bool ReportSalesSearchShowAllOrders { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are displaying all orders on the search screen.
        /// </summary>
        public static bool SalesSearchShowAllOrders { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are displaying all orders on the search screen.
        /// </summary>
        public static bool ShowAllStock { get; set; }

        public static bool ShowPOAtDispatch { get; set; }

        public static bool ShowPartnerPopUpNoteAtDesktopModules { get; set; }

        /// <summary>
        /// Gets the device printer communication method..
        /// </summary>
        public static string PrinterCommunicationType { get; set; }

        /// <summary>
        /// Gets the device printer communication method..
        /// </summary>
        public static string Printer2CommunicationType { get; set; }

        /// <summary>
        /// Gets the device printer communication method..
        /// </summary>
        public static string Printer3CommunicationType { get; set; }

        /// <summary>
        /// Gets the customer default currency.
        /// </summary>
        public static string CustomerCurrency { get; set; }

        /// <summary>
        /// Gets the customer default currency.
        /// </summary>
        public static string LoggedInUserNameUI
        {
            get
            {
                return string.Format("Logged in: {0}", LoggedInUserName);
            }
        }

        /// <summary>
        /// Gets the device printer settings.
        /// </summary>
        public static string PrinterSettings { get; set; }

        /// <summary>
        /// Gets the device printer settings.
        /// </summary>
        public static string Printer2Settings { get; set; }

        /// <summary>
        /// Gets the device printer settings.
        /// </summary>
        public static string Printer3Settings { get; set; }

        /// <summary>
        /// Gets a value indicating whether the scanner is in use.
        /// </summary>
        public static bool ConnectToScanner { get; set; }

        /// <summary>
        /// Gets a value indicating whether the wand is in use.
        /// </summary>
        public static bool ConnectToWand { get; set; }

        /// <summary>
        /// Gets or sets the handheld font size.
        /// </summary>
        public static double HandheldFontSize { get; set; }

        public static double HandheldOrderCountFontSize { get; set; }

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static double HandheldGridFontSize { get; set; }

        /// <summary>
        /// Gets or sets the handheld font size.
        /// </summary>
        public static double HandheldDispatchGridRowSize { get; set; }

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static string TouchscreenQtyMask { get; set; }

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static string TouchscreenWgtMask { get; set; }

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static string TouchscreenPricingMask { get; set; }

        public static string TouchscreenPricingMask1 { get; set; } = "0:N5";

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static int TouchscreenPricingMaskNo { get; set; }

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static int TouchscreenQtyMaskNo { get; set; }

        /// <summary>
        /// Gets or sets the handheld grid font size.
        /// </summary>
        public static int TouchscreenWgtMaskNo { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are recalling the last associated customer from a product.
        /// </summary>
        public static bool RecallCustomerFromProductAtOutOfProduction { get; set; }

        private static DateTime reportsSearchDate;
        /// <summary>
        /// Gets a value indicating whether the scanner is in use.
        /// </summary>
        public static DateTime ReportsSearchFromDate
        {
            get
            {
                return reportsSearchDate;
            }

            set
            {
                if (value < DateTime.Now.AddDays(-10000))
                {
                    value = DateTime.Today;
                }

                reportsSearchDate = value;
            }
        }

        private static DateTime reportsSearchDateTo;
        /// <summary>
        /// Gets a value indicating whether the scanner is in use.
        /// </summary>
        public static DateTime ReportsSearchToDate
        {
            get
            {
                return reportsSearchDateTo;
            }

            set
            {
                if (value < DateTime.Now.AddDays(-10000))
                {
                    value = DateTime.Today;
                }

                reportsSearchDateTo = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the indicator is in use.
        /// </summary>
        public static bool ConnectToIndicator { get; set; }

        /// <summary>
        /// Gets a value indicating whether the indicator is in use.
        /// </summary>
        public static bool DontAutoFilterProductionBatchSearch { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using an rdp identifier.
        /// </summary>
        public static bool UseRDPIdentifierForSettings { get; set; }
       
        /// <summary>
        /// Gets a value indicating whether the application is to be restarted.
        /// </summary>
        public static bool RestartApplication { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application is to be restarted when idle.
        /// </summary>
        public static bool LogOutOnApplicationIdle { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application is to be restarted when idle.
        /// </summary>
        public static int LogOutOnApplicationIdleTime { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application is to be restarted when idle.
        /// </summary>
        public static bool LogOutOnWorkflowIdle { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application is to be restarted when idle.
        /// </summary>
        public static int LogOutOnWorkflowTime { get; set; }

        public static bool DisableAuthorisationCheck { get; set; }

        /// <summary>
        /// Gets the restart application hour.
        /// </summary>
        public static int RestartApplicationHour { get; set; }

        /// <summary>
        /// Gets the printer language.
        /// </summary>
        public static ProgrammingLanguage PrinterLanguage { get; set; }

        /// <summary>
        /// Gets the printer language.
        /// </summary>
        public static ProgrammingLanguage Printer2Language { get; set; }

        /// <summary>
        /// Gets the printer language.
        /// </summary>
        public static ProgrammingLanguage Printer3Language { get; set; }

        /// <summary>
        /// Gets the restart application hour.
        /// </summary>
        public static bool ScanningExternalCarcass { get; set; }

        public static bool ScanningForBatchAtProductionOrders { get; set; }

        /// <summary>
        /// Gets the restart application minute.
        /// </summary>
        public static int RestartApplicationMin { get; set; }

        /// <summary>
        /// Gets a value indicating whether a check is to be made for excess carcass split weighings.
        /// </summary>
        public static bool CheckForExcessCarcassSplitWeighings { get; set; }

        /// <summary>
        /// Gets the scroll bar orders offset.
        /// </summary>
        public static double TouchScreenOrdersVerticalOffset { get; set; }

        /// <summary>
        /// Gets the scroll bar products offset.
        /// </summary>
        public static double TouchScreenProductsVerticalOffset { get; set; }

        /// <summary>
        /// Gets the scroll bar out of production products offset.
        /// </summary>
        public static double TouchScreenProductionProductsVerticalOffset { get; set; }

        /// <summary>
        /// Gets the scroll bar out of production products offset.
        /// </summary>
        public static double TouchScreenWorkflowSelectionVerticalOffset { get; set; }

        /// <summary>
        /// Gets the scroll bar suppliers offset.
        /// </summary>
        public static double TouchScreenSuppliersVerticalOffset { get; set; }

        /// <summary>
        /// Gets a value indicating whether the printer is in use.
        /// </summary>
        public static bool IsPrinterInUse { get; set; }

        /// <summary>
        /// Gets a value indicating whether the printer is in use.
        /// </summary>
        public static bool IsPrinter2InUse { get; set; }

        /// <summary>
        /// Gets a value indicating whether the printer is in use.
        /// </summary>
        public static bool IsPrinter3InUse { get; set; }
      
        /// <summary>
        /// Gets a value indicating whether the we auto weigh at intake.
        /// </summary>
        public static bool AutoWeightAtIntake { get; set; }

        /// <summary>
        /// Gets a value indicating whether we go to the db at intake processing to retrieve order.
        /// </summary>
        public static bool GetLiveOrderStatusIntake { get; set; }

        /// <summary>
        /// Gets a value indicating whether we go to the db at dispatch processing to retrieve order.
        /// </summary>
        public static bool GetLiveOrderStatusDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether we go to the db at dispatch processing to retrieve order.
        /// </summary>
        public static bool DisplayOldGrades { get; set; }

        /// <summary>
        /// Gets a value indicating whether we go to the db at dispatch processing to retrieve order.
        /// </summary>
        public static GradeSystem GradeSystem { get; set; }

        /// <summary>
        /// Gets a value indicating whether the serial attributes are reset only by the operator choosing.
        /// </summary>
        public static bool ResetSerialAttributesManually { get; set; }
       
        /// <summary>
        /// Gets a value indicating whether the we auto weigh at oop.
        /// </summary>
        public static bool AutoWeighAtOutOfProduction { get; set; }
       
        /// <summary>
        /// Gets a value indicating whether the we auto weigh at dispatch.
        /// </summary>
        public static bool AutoWeightAtDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating the scanner port thread wait time.
        /// </summary>
        public static int ScannerPortDataWaitTime { get; set; }

        /// <summary>
        /// Gets a value indicating the scanner port thread wait time.
        /// </summary>
        public static int WandPortDataWaitTime { get; set; }

        /// <summary>
        /// Gets a value indicating the scanner port thread wait time.
        /// </summary>
        public static string WandPortSendString{ get; set; }

        public static string WandPortEraseString { get; set; }

        /// <summary>
        /// Gets a value indicating the scanner port thread wait time.
        /// </summary>
        public static int WandPortTimeOut { get; set; }

        /// <summary>
        /// Gets a value indicating the handheld scanner text wait time.
        /// </summary>
        public static int HandheldScannerDataWaitTime { get; set; }

        /// <summary>
        /// Gets a value indicating the handheld scanner text wait time.
        /// </summary>
        public static string KeyboardWedgeTerminator { get; set; }

        /// <summary>
        /// Gets a value indicating whether the selected label is reset on every print.
        /// </summary>
        public static bool ResetLabelOnEveryPrint { get; set; }

        /// <summary>
        /// Gets a value indicating whether the selected label is reset on every print.
        /// </summary>
        public static bool IgnoreCustomerWithNoPriceListCheck { get; set; }

        /// <summary>
        /// Gets a value indicating whether the shipping date is the primary date used (as opposed to the delivery date).
        /// </summary>
        public static bool UseShippingDate { get; set; }

        /// <summary>
        /// Gets a value indicating whether the shipping date is the primary date used (as opposed to the delivery date).
        /// </summary>
        public static string DispatchOrdersDateType { get; set; }

        /// <summary>
        /// Gets a value indicating whether the shipping date is the primary date used (as opposed to the delivery date).
        /// </summary>
        public static string DispatchOrdersDateSort { get; set; }

        /// <summary>
        /// Gets a value indicating what batch n umber type is being used.
        /// </summary>
        public static BatchNumberType BatchNumberType { get; set; }

        /// <summary>
        /// Gets a value indicating whether the shipping date is the primary date used (as opposed to the delivery date).
        /// </summary>
        public static bool ShowRoutesAtTouchscreenDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the shipping date is the primary date used (as opposed to the delivery date).
        /// </summary>
        public static bool CreateProductionStockAtDispatch { get; set; }

        /// <summary>
        /// Gets the report email to.
        /// </summary>
        public static int SSRSZoom { get; set; }
      
        /// <summary>
        /// Gets the report email type.
        /// </summary>
        public static string SSRSEmailType { get; set; }

        /// <summary>
        /// Gets the report email to.
        /// </summary>
        public static string SSRSEmailTo { get; set; }
      
        /// <summary>
        /// Gets the report email body
        /// </summary>
        public static string SSRSEmailBody { get; set; }

        /// <summary>
        /// Gets the report email type
        /// </summary>
        public static string SSRSEmailSubject { get; set; }

        /// <summary>
        /// Gets the report email type
        /// </summary>
        public static string SSRSEmailExportFormat { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the default printer(USB mode only).
        /// </summary>
        public static bool UseDefaultPrinter { get; set; }
       
        /// <summary>
        /// Gets the printer dpi.
        /// </summary>
        public static double PrinterDPI { get; set; }
       
        /// <summary>
        /// Gets the printer dpi.
        /// </summary>
        public static double Printer2DPI { get; set; }

        /// <summary>
        /// Gets the printer dpi.
        /// </summary>
        public static bool PrintingTrailLabelAtGrader { get; set; }

        /// <summary>
        /// Gets the printer dpi.
        /// </summary>
        public static bool UseRiskToDeterminePrinterAtGrader { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a lairage selected customer is applied to all the lot aniamls.
        /// </summary>
        public static bool ApplySelectedCustomerToAllLairageAnimals { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether only completed lot animals can be sequenced.
        /// </summary>
        public static bool OnlyAllowCompletedLotAnimalsToBeSequenced { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether the lairage web service options are shown.
        /// </summary>
        public static bool DontShowWebServiceOption { get; set; }

        /// <summary>
        /// Gets the printer dpi.
        /// </summary>
        public static double Printer3DPI { get; set; }

        /// <summary>
        /// Gets a value indicating whether the a connection is to be made to the eport server/web service.
        /// </summary>
        public static bool ConnectToSSRS { get; set; }

        /// <summary>
        /// Gets a value that determines if the application checks for new alerts.
        /// </summary>
        public static bool CheckForAlerts { get; set; }

        /// <summary>
        /// Gets a value that determines the periodical interval time used if the application checks for new alerts.
        /// </summary>
        public static int CheckForAlertsTime { get; set; }
       
        /// <summary>
        /// Gets a value that determines the flash message box time to flash for.
        /// </summary>
        public static double MessageBoxFlashTime { get; set; }

        /// <summary>
        /// Gets a value that determines the time to wait for the scales to stable befoe voiding the current weight.
        /// </summary>
        public static int WaitForScalesToStable { get; set; }

        /// <summary>
        /// Gets the out of production products home menu width.
        /// </summary>
        public static double OutOfProductionProductsHomeMenuWidth { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether a manual weight can be recorded by the touchscreen operator.
        /// </summary>
        public static bool CanManuallyEnterWeight { get; set; }

        /// <summary>
        /// Gets a value indicating whether a actegory is set to null, enforcing selection, at the sequencer
        /// </summary>
        public static bool ResetCategoryAtSequencer{ get; set; }

        /// <summary>
        /// Gets a value indicating whether a actegory is set to null, enforcing selection, at the sequencer
        /// </summary>
        public static int MaxLairageBeefIntake { get; set; }

        /// <summary>
        /// Gets a value indicating whether an agent must be selected at lairage.
        /// </summary>
        public static bool MustSelectAgentAtLairage { get; set; }

        /// <summary>
        /// Gets a value indicating whether an agent must be selected at lairage.
        /// </summary>
        public static bool UsingUKDepartmentServiceQueries { get; set; }

        /// <summary>
        /// Gets a value indicating whether an agent must be selected at lairage.
        /// </summary>
        public static string TrackingGroupGuid { get; set; }

        /// <summary>
        /// Gets the touchscreen traceability controls width.
        /// </summary>
        public static double TouchscreenTraceabilityControlsWidth { get; set; }

        /// <summary>
        /// Gets the touchscreen traceability controls width.
        /// </summary>
        public static double TouchscreenTraceabilityControlsLabelWidth { get; set; }
       
        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static double TouchscreenCardsFontSize { get; set; }

        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static bool DisplayNewAlertsMessageUntilAlertOpened { get; set; }

        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static bool WorkflowProceedOnSelection { get; set; }

        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static bool WorkflowDisableBackArrow { get; set; }

        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static bool WorkflowDisableForwardArrow { get; set; }

        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static bool DisallowStandardAttributeChange { get; set; }

        /// <summary>
        /// Gets the touchscreen card view cards font size.
        /// </summary>
        public static bool UsingWorkflowForStandardAttributes { get; set; }
        
        /// <summary>
        /// Gets the touchscreen menu button width.
        /// </summary>
        public static double TouchscreenMenuButtonWidth { get; set; }
        
        /// <summary>
        /// Gets the touchscreen menu button height.
        /// </summary>
        public static double TouchscreenMenuButtonHeight { get; set; }

        /// <summary>
        /// Gets the default currency.
        /// </summary>
        public static string DefaultCurrency { get; set; }

        public static bool EnforceAutoBoxing { get; set; }
        public static int AutoBoxProductGroups { get; set; }

        /// <summary>
        /// Gets the default currency.
        /// </summary>
        public static bool OnlyShowSupplierProductsAtIntake { get; set; }

        /// <summary>
        /// Gets the touchscreen menu button font size.
        /// </summary>
        public static int TouchscreenMenuButtonFontSize { get; set; }
        public static int TouchscreenMenuButtonFontSizeCollection { get; set; }

        public static int TouchscreenMenuButtonFontSizeLogIn { get; set; }
        public static int TouchscreenLogInWidth { get; set; }
        public static int TouchscreenLogInHeight { get; set; }

        /// <summary>
        /// Gets the touchscreen menu button font size.
        /// </summary>
        public static int DispatchBoxCountFontSize { get; set; }

        /// <summary>
        /// Gets the touchscreen menu button font size.
        /// </summary>
        public static int DispatchBoxCountHandheldFontSize { get; set; }

        /// <summary>
        /// Gets the indicator font size.
        /// </summary>
        public static double IndicatorFontSize { get; set; }

        /// <summary>
        /// Gets thevalue indicating whether we are selecting a product in carcass reweigh mode.
        /// </summary>
        public static bool SelectingProductInReweighMode { get; set; }
       
        /// <summary>
        /// Gets the indicator font size.
        /// </summary>
        public static bool PromptForReweighAtDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the no movement on scales warning is to be ignored.
        /// </summary>
        public static bool IgnoreNoMovementmentOnScalesWarning { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static decimal DisturbBy { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static string ZeroScalesCommand { get; set; }

        /// <summary>
        /// Gets a value the scales must drop below in order for a weight to be recorded.
        /// </summary>
        public static decimal? DropBelow { get; set; }

        /// <summary>
        /// Gets a value the scales must drop below in order for a weight to be recorded.
        /// </summary>
        public static decimal Divisions { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static string PrinterCommand { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static bool SendPrinterCommandOnStartUp { get; set; }

        /// <summary>
        /// Gets the indicator font size.
        /// </summary>
        public static bool StartDispatchInReweighMode { get; set; }

        /// <summary>
        /// Gets the carcass side weight difference allowed before a warning at the grader.
        /// </summary>
        public static int CarcassSideWeightDifference { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int YoungBullMaxAge { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int UOMAddDays { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static bool UseCustomerForGraderLabel { get; set; }

        /// <summary>
        /// Gets the identigen user name.
        /// </summary>
        public static string IdentigenUserName { get; set; }

        /// <summary>
        /// Gets the identigen password.
        /// </summary>
        public static string IdentigenPassword { get; set; }

        /// <summary>
        /// Gets the identigen location.
        /// </summary>
        public static string IdentigenLocation { get; set; }

        /// <summary>
        /// Gets the identigen location.
        /// </summary>
        public static bool WaitForMotionAtGrader { get; set; }

        /// <summary>
        /// Gets the identigen location.
        /// </summary>
        public static bool LairageGraderCombo { get; set; }

        /// <summary>
        /// Gets the identigen location.
        /// </summary>
        public static bool AccumulateWeightToTareAtGrader { get; set; }

        /// <summary>
        /// Gets a value indicating whether swap view is enabled at the grader.
        /// </summary>
        public static bool AllowGradeViewChange { get; set; }

        /// <summary>
        /// Gets a value indicating whether swap view is enabled at the sequencer.
        /// </summary>
        public static bool AllowSequencerViewChange { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static bool UsingHistoricGrades { get; set; }
        
        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdEwe { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdLamb { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdYSpringLamb { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdHogget { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdRam { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdGoat { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int GraderRefreshTime { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdYoungBull { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdBull { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdSteer { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdVealYoung { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdVealOld { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdCow { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdHeifer { get; set; }

        /// <summary>
        /// Gets a value indicating whether at into prod select product mode, we are scanning and weighing separately.
        /// </summary>
        public static bool IntoProductionSelectProductScanAndWeighSeparately { get; set; }

        /// <summary>
        /// Gets the indicator font size.
        /// </summary>
        public static bool UserLoggedIn { get; set; }

        /// <summary>
        /// Gets the indicator small font size.
        /// </summary>
        public static double IndicatorFontSizeSmall { get; set; }

        /// <summary>
        /// Gets the touchscreen traceability controls height.
        /// </summary>
        public static double TouchscreenTraceabilityControlsHeight { get; set; }

        /// <summary>
        /// Gets the touchscreen traceability controls font size.
        /// </summary>
        public static double TouchscreenTraceabilityControlsFontSize { get; set; }

        /// <summary>
        /// Gets the price list default currency.
        /// </summary>
        public static int PriceListDefaultCurrency { get; set; }

        private static int orderAmountToTake;

        /// <summary>
        /// Gets the price list default currency.
        /// </summary>
        public static int OrderAmountToTake
        {
            get
            {
                return orderAmountToTake;
            }

            set
            {
                if (value == 0)
                {
                    value = 100;
                }

                orderAmountToTake = value;
            }
        }

        /// <summary>
        /// Gets the recent orders amt to take.
        /// </summary>
        public static int RecentOrdersAmtToTake { get; set; }

        /// <summary>
        /// Gets the recent orders amt to take.
        /// </summary>
        public static bool RecentOrdersAtDispatchByDelivered { get; set; }

        /// <summary>
        /// Gets the recent orders amt to take.
        /// </summary>
        public static bool UseDefaultProductAtOutOfProduction { get; set; }

        /// <summary>
        /// Gets the recent orders amt to take.
        /// </summary>
        public static int DefaultProductAtOutOfProductionID { get; set; }

        /// <summary>
        /// Gets the recent orders amt to take.
        /// </summary>
        public static bool DefaultToFindAtDesktopDispatch { get; set; }

        /// <summary>
        /// Gets the price list default rounding.
        /// </summary>
        public static int PriceListDefaultRounding { get; set; }

        /// <summary>
        /// Gets the price list default rounding option.
        /// </summary>
        public static int PriceListDefaultRoundingOption { get; set; }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        public static string Customer { get; set; }

        /// <summary>
        /// Gets a value indicating whether the kill line is enabled.
        /// </summary>
        public static bool ModuleEnableKillLine { get; set; }

        /// <summary>
        /// Gets the application accounts package being used.
        /// </summary>
        public static AccountsPackage AccountsPackage { get; set; }

        /// <summary>
        /// Gets the application accounts package sales person code.
        /// </summary>
        public static string AccountsSalesPersonCode { get; set; }
       
        /// <summary>
        /// Gets the application accounts package database path.
        /// </summary>
        public static string AccountsDbPath { get; set; }
       
        /// <summary>
        /// Gets the application accounts package database user name.
        /// </summary>
        public static string AccountsDbUserName { get; set; }
       
        /// <summary>
        /// Gets the application accounts package database password.
        /// </summary>
        public static string AccountsDbPassword { get; set; }

        /// <summary>
        /// Gets the application accounts package database password.
        /// </summary>
        public static string AccountsCustomer { get; set; }

        /// <summary>
        /// Gets the application accounts package database password.
        /// </summary>
        public static string AccountsFilePath{ get; set; }
        public static bool UseProposedKillDateForPurchaseInvoice { get; set; }

        /// <summary>
        /// Gets the application accounts package database password.
        /// </summary>
        public static string AccountsDirectory { get; set; }

        public static bool UseLairagesForPurchaseAccounts { get; set; }
        public static bool PostToPurchaseInvoice { get; set; }
        public static bool UseDeliveryDateForPurchaseOrder { get; set; }
        public static bool PartPostPurchaseOrder { get; set; }
        public static bool UseSagePurchaseOrderNumbers { get; set; }

        public static bool PriceInvoiceDocket { get; set; }
        public static bool PricePurchaseInvoiceDocket { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static bool UseDeliveryDateForInvoiceDocumentDate { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using the dispatch delivery date as the invoice doc date.
        /// </summary>
        public static bool ShippingDateMustBeAfterDeliveryDate { get; set; }

        public static bool ShowBoxingAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the invoice is to be pre posted.
        /// </summary>
        public static bool PrePostInvoice { get; set; }

        /// <summary>
        /// Gets the indicator stable reads count.
        /// </summary>
        public static int IndicatorStableReads { get; set; }

        /// <summary>
        /// Gets the touchscreen traceability controls stacked on each other number.
        /// </summary>
        public static int TouchscreenTraceabilityControlsStackedNumber { get; set; }

        /// <summary>
        /// Gets the device indicator communication method..
        /// </summary>
        public static string IndicatorCommunicationType { get; set; }

        /// <summary>
        /// Gets the device indicator settings.
        /// </summary>
        public static string IndicatorSettings { get; set; }

        /// <summary>
        /// Gets the device indicator alibi files settings.
        /// </summary>
        public static string IndicatorAlibiPath { get; set; }
       
        /// <summary>
        /// Gets the device indicator alibi files settings.
        /// </summary>
        public static int IndicatorWaitTime { get; set; }
        
        /// <summary>
        /// Gets the device indicator model.
        /// </summary>
        public static string IndicatorModel { get; set; }
       
        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static bool TouchScreenModeOnly { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static bool EposMode { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string LairageIntakeSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string PaymentCreationSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string LairageReportSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string LairageKillInformationSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string BeefReportSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string IdentigenSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string KillDetailsSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string RPAReportSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string AnimalMovermentsSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string AnimalPricingSearchType { get; set; }

        /// <summary>
        /// Gets or sets the module search type.
        /// </summary>
        public static string PaymentsSearchType { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static bool AllowTouchScreenResize { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static int DefaultIntakeLocationId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static int DefaultReturnsLocationId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static int DefaultOutOfProductionLocationId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen.
        /// </summary>
        public static int DefaultIntoProductionLocationId { get; set; }

        public static int DefaultDispatchLocationId { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using stable reads/tolerance for motion.
        /// </summary>
        public static bool UseStableReadsForMotion { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using external labels.
        /// </summary>
        public static bool UseExternalLabels { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using external labels.
        /// </summary>
        public static bool UseExternalLabels2 { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are using external labels.
        /// </summary>
        public static bool UseExternalLabels3 { get; set; }

        /// <summary>
        /// Gets a value indicating whether we can select a customer at out of production.
        /// </summary>
        public static bool CanSelectCustomersAtOutOfProduction { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen intake.
        /// </summary>
        public static bool TouchScreenModeOnlyIntake { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen intake.
        /// </summary>
        public static bool TouchScreenModeOnlySequencer { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen intake.
        /// </summary>
        public static bool TouchScreenModeOnlyGrader { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen intake.
        /// </summary>
        public static bool TouchScreenModeOnlyWorkflow { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application will use keyboard wedge for scanning at the touchscreen.
        /// </summary>
        public static bool UseKeyboardWedgeAtTouchscreen { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen into production.
        /// </summary>
        public static bool TouchScreenModeOnlyIntoProduction { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen out of production.
        /// </summary>
        public static bool TouchScreenModeOnlyOutOfProduction { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application allows all products out of production.
        /// </summary>
        public static bool AllowAllProductsOutOfProduction { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static bool TouchScreenModeOnlyDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static bool CreatingTouchscreenRecipes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static bool ScanningForExternalBarcodeReadOnlyAtDispatch { get; set; }

        public static bool ScanningForIntakeBatchesAtDispatch { get; set; }

        public static bool RetrieveSerialNoFromBarcode { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static bool ScanningForScotBeefBarcodeReadAtDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the application launches directly to touchscreen dispatch.
        /// </summary>
        public static bool ScanningForExternalBarcodeReadOnlyAtIntoProduction { get; set; }

        /// <summary>
        /// Gets the device printer label orientation.
        /// </summary>
        public static string PrinterLabelOrientation { get; set; }

        /// <summary>
        /// Gets the label history transactions number to retrieve from the db.
        /// </summary>
        public static int LabelHistoryTransactionsNumberToRetrieve { get; set; }

        /// <summary>
        /// Gets the goods in label to print amount.
        /// </summary>
        public static int GoodsInLabelsToPrint { get; set; }
        
        /// <summary>
        /// Gets the dispatch label to print amount.
        /// </summary>
        public static int DispatchLabelsToPrint { get; set; }

        /// <summary>
        /// Gets the dispatch label to print amount.
        /// </summary>
        public static bool DisablePrintLabelAtDispatch { get; set; }

        public static string AllowBoxesOnlyToBeSelectedAtDispatchGroups { get; set; }

        /// <summary>
        /// Gets the dispatch label to print amount.
        /// </summary>
        public static bool AllowBoxesOnlyToBeSelectedAtDispatch { get; set; }

        /// <summary>
        /// Gets the dispatch label to print amount.
        /// </summary>
        public static bool AllowBatchSelectionForSerialStockAtDispatch { get; set; }

        /// <summary>
        /// Gets the into production to print amount.
        /// </summary>
        public static int IntoProductionLabelsToPrint { get; set; }

        /// <summary>
        /// Gets the out of production to print amount.
        /// </summary>
        public static int OutOfProductionLabelsToPrint { get; set; }

        /// <summary>
        /// Gets the bord bia user name.
        /// </summary>
        public static string BordBiaUserName { get; set; }

        /// <summary>
        /// Gets the bord bia password.
        /// </summary>
        public static string BordBiaPassword { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string BordBiaResponsePath { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static bool UsingBordBia { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string AimsCertPath { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string AimsLogPath { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string AimsSheepLogPath { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string AimSenderId { get; set; }

        /// <summary>
        /// Gets the required days an animal must reside on the current farm.
        /// </summary>
        public static int? RequiredDaysOnCurrentFarm { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string AimsURL { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static string AimsPassword { get; set; }

        public static string AfaisPassword { get; set; }
        public static string AfaisUserName { get; set; }
        public static string AfaisBusinessId { get; set; }
        public static string AfaisEnvironment { get; set; }
        public static int AfaisPinNo { get; set; }
        public static string AfaisAbbatoirCode { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static bool UsingAims { get; set; }

        /// <summary>
        /// Gets the bord bia response path.
        /// </summary>
        public static bool HideAllKillTouchscreenAttributes { get; set; }

        public static bool DontAllowLairageMove { get; set; }

        /// <summary>
        /// Gets the standard touchscreen button font size.
        /// </summary>
        public static double TouchscreenStaticButtonFontSize { get; set; }

        /// <summary>
        /// Gets the transactions to retrieve for the transactions search grid.
        /// </summary>
        public static int TransactionsToTakeForReportSearch { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static decimal MinWeightAllowedIntake { get; set; }

        /// <summary>
        /// Gets the out of production min weight allowed.
        /// </summary>
        public static decimal MinWeightAllowedOutOfProduction { get; set; }

        /// <summary>
        /// Gets the into production min weight allowed.
        /// </summary>
        public static decimal MinWeightAllowedIntoProduction { get; set; }

        /// <summary>
        /// Gets the dispatch min weight allowed.
        /// </summary>
        public static decimal MinWeightAllowedDispatch { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static int MinWeightInDivisions { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static int? PollScalesForStringInterval { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static int? PollScalesForStringInterval2 { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static int? PollScalesForStringInterval3 { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static string PollScalesString { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static string PollScalesString2 { get; set; }

        /// <summary>
        /// Gets the intake min weight allowed.
        /// </summary>
        public static string PollScalesString3 { get; set; }

        /// <summary>
        /// Gets the dispatch tolerance weight allowed.
        /// </summary>
        public static decimal ToleranceWeight { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to a goods in receipt.
        /// </summary>
        public static bool AutoCopyToGoodsIn { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to a goods in receipt.
        /// </summary>
        public static bool AlwaysCopyToGoodsIn { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an intake order item update message box is to be displayed.
        /// </summary>
        public static bool DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox { get; set; }

        /// <summary>
        /// Gets a value indicating whether we a dispatch order item update message box is to be displayed.
        /// </summary>
        public static bool DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether we an intake order can receive greater amounts than on the order.
        /// </summary>
        public static bool AllowIntakeOverAmount { get; set; }

        /// <summary>
        /// Gets a value indicating whether we a dispatch order can receive greater amounts than on the order.
        /// </summary>
        public static bool AllowDispatchOverAmount { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether we the production orders are displayed on entry.
        /// </summary>
        public static bool ClearProductionOrdersOnEntry { get; set; }

        public static bool ResetRecipeQtyOnOrdersScreen { get; set; }

        /// <summary>
        /// Gets a value indicating whether we the production orders are displayed on entry.
        /// </summary>
        public static bool ClearPartnersOnEntry { get; set; }

        /// <summary>
        /// Gets a value indicating whether we the production orders are displayed on entry.
        /// </summary>
        public static bool DisableSaleOrderDiscounts { get; set; }

        /// <summary>
        /// Gets the filter count for partners to be displayed at any one time on the touchscreen search.
        /// </summary>
        public static int FilterPartnerCountOnSearchScreen { get; set; }

        public static bool FilterProductionOrdersByDepartment { get; set; }

        /// <summary>
        /// Gets a value indicating whether we a dispatch order over the amount is hard stopped or not.
        /// </summary>
        public static bool DispatchOverAmountHardStop { get; set; }

        /// <summary>
        /// Gets a value indicating whether a product not on an order can be dispatched.
        /// </summary>
        public static bool AllowDispatchProductNotOnOrder { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether an invoice is to be auto created on dispatch completion.
        /// </summary>
        public static bool AutoCreateInvoiceOnOrderCompletion { get; set; }

        /// <summary>
        /// Gets a value indicating whether an invoice is to be auto created on dispatch completion.
        /// </summary>
        public static bool MarkCompletedDispatchesAsActiveEdit { get; set; }

        public static int? NouProrderTypeID { get; set; }

        public static bool UseLegacyUseByAttribute { get; set; }

        /// <summary>
        /// Gets the scanner settings.
        /// </summary>
        public static string ScannerSettings { get; set; }
        public static string WandSettings { get; set; }

        /// <summary>
        /// Gets the ssrs selected report path.
        /// </summary>
        public static int SSRSCompanyID { get; set; }

        /// <summary>
        /// Gets the ssrs selected report path.
        /// </summary>
        public static string ReportServerPath { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to a goods in receipt.
        /// </summary>
        public static bool AutoCopyToDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to a goods in receipt.
        /// </summary>
        public static bool AutoCopyToLairage { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to a goods in receipt.
        /// </summary>
        public static bool IgnoreCompleteDispatchCheck { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to a goods in receipt.
        /// </summary>
        public static bool AlwaysCopyToDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the qty is a required entry on a sale order line.
        /// </summary>
        public static bool QtyMustBeEnteredOnSaleOrderLine { get; set; }

        /// <summary>
        /// Gets a value indicating whether the qty is a required entry on a sale order line.
        /// </summary>
        public static bool NoPriceEnteredOnSaleOrderLineWarning { get; set; }

        /// <summary>
        /// Gets a value indicating whether the qty is a required entry on a sale order line.
        /// </summary>
        public static bool NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate { get; set; }

        public static bool DisablePriceChecksOnSaleOrders { get; set; }

        /// <summary>
        /// Gets a value indicating whether a duplicate product is allowed on the order.
        /// </summary>
        public static bool AllowDuplicateProductOnOrder { get; set; }

        /// <summary>
        /// Gets a value indicating whether we an order is to be auto copied to an invoice.
        /// </summary>
        public static bool AutoCopyToInvoice { get; set; }

        /// <summary>
        /// Gets the batch creation mode value.
        /// </summary>
        public static BatchCreationMode BatchCreationMode { get; set; }

        /// <summary>
        /// Gets a value indicating whether the scanner is in emulator mode (non rdp).
        /// </summary>
        public static bool ScannerEmulatorMode { get; set; }

        /// <summary>
        /// Gets the emulator height.
        /// </summary>
        public static double ScannerEmulatorHeight { get; set; }

        /// <summary>
        /// Gets the emulator width.
        /// </summary>
        public static double ScannerEmulatorWidth { get; set; }

        /// <summary>
        /// Gets the emulator width.
        /// </summary>
        public static double ScannerEmulatorTop { get; set; }

        /// <summary>
        /// Gets the emulator width.
        /// </summary>
        public static double ScannerEmulatorLeft { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are in scanner mode.
        /// </summary>
        public static bool ScannerMode { get; set; }

        public static bool UseKeypadSidesAtDispatch { get; set; }
        public static bool UseKeypadSidesAtIntake { get; set; }
        public static bool UseKeypadSidesAtIntoProduction { get; set; }
        public static bool UseNewScanFunctionalityAtDispatch { get; set; }

        public static bool OnlyAllowAuthorisedAnimalsAtSequencer { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are in scanner mode.
        /// </summary>
        public static bool RetrieveAttributeOnDispatchScan { get; set; }

        /// <summary>
        /// Gets a value indicating whether a report is auto printed at dispatch.
        /// </summary>
        public static bool PrintReportOnCompletion { get; set; }

        /// <summary>
        /// Gets a value indicating whether a report is auto printed at dispatch.
        /// </summary>
        public static bool AutoPrintDispatchReport { get; set; }

        /// <summary>
        /// Gets a value indicating whether a report is auto printed at dispatch.
        /// </summary>
        public static bool AutoPrintDispatchDetailReport { get; set; }

        /// <summary>
        /// Gets a value indicating whether a report is auto printed at dispatch.
        /// </summary>
        public static bool AutoPrintInvoiceReport { get; set; }

        /// <summary>
        /// Gets a value indicating whether a report is auto printed at dispatch.
        /// </summary>
        public static bool AlwaysShowButcheryBatch { get; set; }

        /// <summary>
        /// Gets the sale order days forward for the delivery date.
        /// </summary>
        public static int SaleOrderDeliveryDaysForward { get; set; }

        /// <summary>
        /// Gets the sale order days forward for the delivery date.
        /// </summary>
        public static int ProposedKillDateDaysForward { get; set; }

        /// <summary>
        /// Gets the sale order days forward for the delivery date.
        /// </summary>
        public static int LairageDeliveryDaysForward { get; set; }

        /// <summary>
        /// Gets the valid until days forward for the delivery date.
        /// </summary>
        public static int SaleOrderValidUntilDaysForward { get; set; }

        /// <summary>
        /// Gets the reporting services web service url.
        /// </summary>
        public static string SSRSWebServiceURL { get; set; }
        
        /// <summary>
        /// Gets the reporting services network credentials user name.
        /// </summary>
        public static string SSRSUserName { get; set; }

        /// <summary>
        /// Gets the reporting services network credentials password.
        /// </summary>
        public static string SSRSPassword { get; set; }

        /// <summary>
        /// Gets the reporting services network credentials domain name.
        /// </summary>
        public static string SSRSDomain { get; set; }

        /// <summary>
        /// Gets the application weights precision value.
        /// </summary>
        public static decimal ScalesDivision { get; set; }

        /// <summary>
        /// Gets the card view grid card width.
        /// </summary>
        public static double CardViewCardWidth { get; set; }

        /// <summary>
        /// Gets the card view grid card width.
        /// </summary>
        public static double CardViewCardWidthIntakeBatches { get; set; }

        /// <summary>
        /// Gets the card view grid narrow card width.
        /// </summary>
        public static double CardViewNarrowCardWidth { get; set; }

        /// <summary>
        /// Gets the intake card display data.
        /// </summary>
        public static string DisplayIntakeData { get; set; }

        /// <summary>
        /// Gets the dispatch card display data.
        /// </summary>
        public static string DisplayDispatchData { get; set; }

        /// <summary>
        /// Gets the dispatch card display data.
        /// </summary>
        public static string DisplayWorkflowData { get; set; }

        /// <summary>
        /// Gets the production card display data.
        /// </summary>
        public static string DisplayProductionData { get; set; }

        /// <summary>
        /// Gets the production card display data.
        /// </summary>
        public static string DisplayProductionBatchData { get; set; }

        /// <summary>
        /// Gets the production card display data.
        /// </summary>
        public static string DisplayLabelData { get; set; }

        /// <summary>
        /// Gets the production card display data.
        /// </summary>
        public static string DisplayStockProductionData { get; set; }

        /// <summary>
        /// Gets the production card display data.
        /// </summary>
        public static string DisplayRoutesData { get; set; }

        /// <summary>
        /// Gets the out of production card display data.
        /// </summary>
        public static string DisplayOutOfProductionData { get; set; }

        /// <summary>
        /// Gets the card view grid card height.
        /// </summary>
        public static double CardViewCardHeight { get; set; }

        /// <summary>
        /// Gets the card view grid card height.
        /// </summary>
        public static double CardViewCardHeightIntakeBatches { get; set; }

        /// <summary>
        /// Gets the db polling interval.
        /// </summary>
        public static int DatabasePollingInterval { get; set; }

        /// <summary>
        /// Gets the cop amount of reports to print on the touchscreen.
        /// </summary>
        public static int TouchscreenReportsCopiesToPrint { get; set; }

        /// <summary>
        /// Gets the card view grid narrow card height.
        /// </summary>
        public static double CardViewPartnersCardHeight { get; set; }

        /// <summary>
        /// Gets the card view menu grid card height.
        /// </summary>
        public static double CardViewPartnersCardWidth { get; set; }

        /// <summary>
        /// Gets the card view grid narrow card height.
        /// </summary>
        public static double CardViewNarrowCardHeight { get; set; }

        /// <summary>
        /// Gets the card view menu grid card height.
        /// </summary>
        public static double CardViewMenuCardWidth { get; set; }

        /// <summary>
        /// Gets the card view menu grid card height.
        /// </summary>
        public static double CardViewMenuCardHeightItem { get; set; }
       
        /// <summary>
        /// Gets the keyboard height.
        /// </summary>
        public static double KeyboardSearchHeight { get; set; }

        /// <summary>
        /// Gets the keyboard height.
        /// </summary>
        public static double KeyboardSearchWidth { get; set; }

        /// <summary>
        /// Gets the keyboard height.
        /// </summary>
        public static double KeyboardSearchTop { get; set; }

        /// <summary>
        /// Gets the keyboard height.
        /// </summary>
        public static double KeyboardSearchLeft { get; set; }

        /// <summary>
        /// Gets the card view menu grid card height.
        /// </summary>
        public static double CardViewMenuCardWidthItem { get; set; }

        /// <summary>
        /// Gets the card view menu grid card height.
        /// </summary>
        public static double CardViewMenuCardHeight { get; set; }

        public static double CardViewMenuCardHeightCollection { get; set; }
        public static double CardViewMenuCardWidthCollection { get; set; }

        public static double CardViewMenuCardHeightRoutes { get; set; }
        public static double CardViewMenuCardWidthRoutes { get; set; }
        public static double CardViewMenuCardRoutesFontSize { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static string LoggedInUser { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static bool TestMode { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static decimal TestModeMaxIncrement { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static double TestModeIncrementTime { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static string LoggedInUserName { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are in touchscreen mode.
        /// </summary>
        public static bool TouchScreenMode { get; set; }

        public static bool CheckForExtraDispatchLines { get; set; }
        public static bool ShowSpecsAtDispatch { get; set; }
        public static double SpecHeight { get; set; }
        public static double SpecWidth { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static string LoggedInUserIdentifier { get; set; }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public static bool UserPasswordMustBeUnique{ get; set; }

        static ApplicationSettings()
        {
            GenerateApplicationSettings();
        }

        /// <summary>
        /// Gets value indicating whether we are manually entering the batch numbers.
        /// </summary>
        public static bool ManuallyEnterBatchNumber { get; set; }

        /// <summary>
        /// Gets a value indicating whether the serial attribute values are reset after every transaction.
        /// </summary>
        public static bool ResetSerialAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether a batch number can be manually created by the user.
        /// </summary>
        public static bool AllowManualBatchCreation { get; set; }

        public static bool ResetQtyToZeroAtIntake { get; set; }

        /// <summary>
        /// Gets a value indicating whether the intake attributes are displayed (expanded).
        /// </summary>
        public static bool ShowIntakeAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the into production attributes are displayed (expanded).
        /// </summary>
        public static bool ShowIntoProductionAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the out of production attributes are displayed (expanded).
        /// </summary>
        public static bool ShowOutOfProductionAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static bool ShowDispatchAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static bool ShowSequencerAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static bool ShowLairageAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch attributes are displayed (expanded).
        /// </summary>
        public static bool ShowGraderAttributes { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch container must be selected before user can work on it.
        /// </summary>
        public static bool MustSelectContainerForDispatch { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch container must be selected before user can work on it.
        /// </summary>
        public static bool CheckForShippingContainerDateMatch { get; set; }

        /// <summary>
        /// Gets the indicator string variables.
        /// </summary>
        public static string IndicatorStringVariables { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdPig { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static int ProductIdPiglet { get; set; }

        public static int ProductIdSheepFull { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static bool PrintFreezerLabelAtGrader { get; set; }

        /// <summary>
        /// Gets the young bull maximum age.
        /// </summary>
        public static bool SelectingCategoryAtLairage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static bool AllowDuplicateSheepEartagsAtSequencer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static bool PrintKillReportAtPayments { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static bool AutoCompletePaymentOnReportPrint { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether product codes are to be automated.
        /// </summary>
        public static bool AutoCreateProductCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether product codes are to be automated.
        /// </summary>
        public static bool AutoCompleteRecipeMix { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether product codes are to be automated.
        /// </summary>
        public static bool AutoTickScrapie { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static bool DoNotResetLairageAgent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static int? LairageRadioButtonOptionsPartnerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static int? LairageRadioButtonOptionsPartnerId1 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static int? LairageRadioButtonOptionsPartnerId2 { get; set; }

        public static int? LairageRadioButtonOptionsPartnerId3 { get; set; }

        public static int? LairageRadioButtonOptionsPartnerId4 { get; set; }

        public static bool SortMode { get; set; }

        public static bool SortGroupMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether cat b animals are always non qa.
        /// </summary>
        public static bool InvalidateCatBFarmAssurance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static int? LairageAgentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static double? EweTare { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static double? LambTare { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static int? EweLabelsToPrint { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we can toggle to beef at grader.
        /// </summary>
        public static bool AllowGraderBeefToggle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we can toggle to sheep at grader.
        /// </summary>
        public static bool AllowGraderSheepToggle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we can toggle to pig at grader.
        /// </summary>
        public static bool AllowGraderPigToggle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether duplicate eartags are allowed in a lot at the sequencer.
        /// </summary>
        public static int GraderAnimalsToRetrieve { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are using the tare calculator.
        /// </summary>
        public static bool UsingTareCalculator { get; set; }

        public static bool CheckRoutesAtOrder { get; set; }

        public static bool DisallowInvoiceReOpening { get; set; }
        public static bool GradesNotRequiredForVeal { get; set; }

        public static bool ShowUnallocatedCustomersOnly { get; set; }

        public static bool CanEditBatches { get; set; }
        public static bool AccumulateNotesAtBatchEdit { get; set; }

        public static int KilledSheepToAppearAtTopOfGrid { get; set; }

        /// <summary>
        /// Gets or sets the needle font size (Target weight in center of needle).
        /// </summary>
        public static double NeedleTargetWeightSize { get; set; }

        /// <summary>
        /// Gets a value indicating whether a customer price book is auto created when the customer is created.
        /// </summary>
        public static int TestDeviceId { get; set; }

        /// <summary>
        /// Gets the transactions to retrieve for the transactions search grid.
        /// </summary>
        public static int CheckSequencedAnimalsCount { get; set; }

        /// <summary>
        /// Gets or sets the BatchEdit search from date.
        /// </summary>
        public static DateTime BatchEditSearchFromDate { get; set; }

        /// <summary>
        /// Gets or sets the BatchEdit search from date.
        /// </summary>
        public static DateTime BatchEditSearchToDate { get; set; }

        /// <summary>
        /// Gets or sets the BatchEdit search from date.
        /// </summary>
        public static bool BatchEditSearchShowAllOrders { get; set; }

        /// <summary>
        /// Gets or sets the BatchEdit search from date.
        /// </summary>
        public static string AlibiChecksum { get; set; }

        /// <summary>
        /// Gets or sets the BatchEdit search from date.
        /// </summary>
        public static string AlibiVersionNo { get; set; }

        /// <summary>
        /// Gets or sets the BatchEdit search from date.
        /// </summary>
        public static string AlibiCertNo { get; set; }

        /// <summary>
        /// Gets or sets the out of production id.
        /// </summary>
        internal static int DefaultPalletLocationId { get; set; }

        /// <summary>
        /// Gets or sets the indicator serial no.
        /// </summary>
        public static string NAWISerialNo { get; set; }

        public static string ProductGroupName { get; set; }
        public static bool MustEnterPalletNumberAtIntake { get; set; }
        public static bool ResetLocationsAtStockMovement { get; set; }
        public static bool SearchProductsBycode { get; set; }

        /// <summary>
        /// Gets a value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public static bool GradeVealAsWholeAnimal { get; set; }

        public static bool GradeVealAsWholeAnimalWithZ { get; set; }

        public static bool ShowExpandedProductGroups { get; set; }

        /// <summary>
        /// Gets a value indicating whether a batch number can be manually created by the user.
        /// </summary>
        public static bool DisplayReportPrintMessageOnCopyTemplate { get; set; }

        /// <summary>
        /// Gets a value indicating whether a batch number can be manually created by the user.
        /// </summary>
        public static bool UseDeliveryDateForIntakeReportSearch { get; set; }

        public static bool SaveBatchAttributesOnEntry { get; set; }

        public static int? ShowOrHideGroupPartners { get; set; }
        public static int ShowOrHidePartnerGroupID { get; set; }
        public static string DispatchDocketNumberType { get; set; }
        public static string IntakeDocketNumberType { get; set; }
        public static int PreviousWorkflowTimeCheck { get; set; }
        public static double PopUpNoteHeight { get; set; }
        public static double PopUpNoteWidth { get; set; }
        public static double MessageBoxHeight { get; set; }
        public static double MessageBoxWidth { get; set; }
        public static double MessageBoxFontSize { get; set; }
        public static bool AllowLeftMessageBox { get; set; }
        public static double TouchscreenMessageBoxHeight { get; set; }
        public static double TouchscreenMessageBoxWidth { get; set; }
        public static double TouchscreenMessageBoxFontSize { get; set; }
        public static double ScannerMessageBoxHeight { get; set; }
        public static double ScannerMessageBoxWidth { get; set; }
        public static double ScannerMessageBoxFontSize { get; set; }
        public static double SystemMessageBoxHeight { get; set; }
        public static double PopUpNoteFontSize { get; set; }
        public static bool PasswordNotRequired { get; set; }
        public static bool HideProcessSelection { get; set; }

        public static bool SearchIntakeByReference { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static string DefaultIntakeModes { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static string DefaultIntoProductionModes { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static string DefaultStockMovementModes { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static string DefaultOutOfProductionModes { get; set; }

        /// <summary>
        /// Gets the IntoProductionMode mode.
        /// </summary>
        public static string DefaultDispatchModes { get; set; }

        /// <summary>
        /// Gets the StockMovementMode mode.
        /// </summary>
        public static StockMovementMode StockMovementMode { get; set; }

        /// <summary>
        /// Gets the touchscreen menu button height.
        /// </summary>
        public static double TouchscreenMessageBarHeight { get; set; }

        /// <summary>
        /// Gets the touchscreen menu button height.
        /// </summary>
        public static double TouchscreenMessageBarFontSize { get; set; }
        
        /// <summary>
        /// TODO: Implement settings in db.
        /// </summary>
        private static void GenerateApplicationSettings()
        {
           // BatchGenerationMode = BatchGenerationMode.Sequential;
        }
    }
}

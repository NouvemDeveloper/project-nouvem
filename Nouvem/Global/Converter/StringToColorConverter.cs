﻿// -----------------------------------------------------------------------
// <copyright file="StringToColorConverter.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Global.Converter
{
    using System;
    using System.Windows;
    using System.Windows.Data;
    using Nouvem.Properties;

    public class StringToColorConverter : IValueConverter
    {
        /// <summary>
        /// Converts a card colour to that of a sale detail status.
        /// </summary>
        /// <param name="value">The tab control items.</param>
        /// <param name="targetType">The double target type</param>
        /// <param name="parameter">null parameter</param>
        /// <param name="culture">Localisation culture</param> 
        /// <returns>The converted colour.</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var orderProgress = value as string;
            return orderProgress.Equals(OrderStatus.Active)
                ? Application.Current.FindResource("GradientBrushCard") : orderProgress.Equals(OrderStatus.InProgress)
                ? Application.Current.FindResource("GradientBrushInProgress") : orderProgress.Equals(OrderStatus.Filled)
                ? Application.Current.FindResource("GradientBrushOrderComplete") : "Black";
        }

        /// <summary>
        /// Converts a card colour to that of a sale detail status.
        /// </summary>
        /// <param name="value">The tab control items.</param>
        /// <param name="targetType">The double target type</param>
        /// <param name="parameter">null parameter</param>
        /// <param name="culture">Localisation culture</param>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

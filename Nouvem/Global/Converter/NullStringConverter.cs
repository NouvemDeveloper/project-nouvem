﻿// -----------------------------------------------------------------------
// <copyright file="NegateConverter.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.Global.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class NullStringConverter : IValueConverter
    {
        /// <summary>
        /// Inverts a boolean.
        /// </summary>
        /// <param name="value">The boolean value.</param>
        /// <param name="targetType">The target type</param>
        /// <param name="parameter">null parameter</param>
        /// <param name="culture">Localisation culture</param> 
        /// <returns>The converted colour.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var name = (value as INGroup).Name;

            return string.Format("({0})", name);
        }

        /// <summary>
        /// Inverts a boolean back.
        /// </summary>
        /// <param name="value">The boolean value.</param>
        /// <param name="targetType">The target type</param>
        /// <param name="parameter">null parameter</param>
        /// <param name="culture">Localisation culture</param> 
        /// <returns>The converted colour.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

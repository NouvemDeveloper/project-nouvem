﻿// -----------------------------------------------------------------------
// <copyright file="PasswordEntry.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Global
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Class which wraps the call to create a new password entry.
    /// </summary>
    public static class PasswordEntry
    {
        /// <summary>
        /// Sends a message to create the password entry view, with the input check type.
        /// </summary>
        /// <param name="type">The password check type.</param>
        public static void Create(PasswordCheckType type)
        {
            Messenger.Default.Send(type);
        }
    }
}

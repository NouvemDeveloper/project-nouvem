﻿// -----------------------------------------------------------------------
// <copyright file="ProgressBar.cs" company="NOouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.Global
{
    using System.Windows;
    using System.Windows.Threading;
    using Nouvem.Properties;
    using Nouvem.ViewModel;

    /// <summary>
    /// Wrapper class for the application progress bar calls.
    /// </summary>
    public static class ProgressBar
    {
        /// <summary>
        /// Call to set up the progress bar.
        /// </summary>
        /// <param name="min">The minimum value.</param>
        /// <param name="max">The maximum value.</param>
        /// <param name="progressCompleteMessage">The complete bar message.</param>
        public static void SetUp(int min = 0, int max = 100, string progressCompleteMessage = "")
        {
            ViewModelLocator.SystemMessageStatic.ProgressBarMinValue = min;
            ViewModelLocator.SystemMessageStatic.ProgressBarMaxValue = max;
            ViewModelLocator.SystemMessageStatic.ProgressBarCompleteMessage = progressCompleteMessage;
        }

        /// <summary>
        /// Call to start the progress bar.
        /// </summary>
        /// <param name="increment">The incremental value.</param>
        public static void Run(int increment = 1)
        {
            if (ApplicationSettings.TouchScreenModeOnly)
            {
                return;
            }

            try
            {
                // make the call thread safe.
                Application.Current.Dispatcher.Invoke(() => ViewModelLocator.SystemMessageStatic.ProgressBarValue += increment, DispatcherPriority.Background);
            }
            catch 
            {
            }
        }

        /// <summary>
        /// Call to reset the progress bar.
        /// </summary>
        public static void Reset()
        {
            // make the call thread safe.
            Application.Current.Dispatcher.Invoke(() => ViewModelLocator.SystemMessageStatic.ProgressBarValue = 0, DispatcherPriority.Background);
            ViewModelLocator.SystemMessageStatic.ProgressComplete = true;
        }
        
        /// <summary>
        /// Sets the progress bar in and out of an error state.
        /// </summary>
        /// <param name="inErrorState"></param>
        public static void ErrorState(bool inErrorState)
        {
            // make the call thread safe.
            Application.Current.Dispatcher.Invoke(() => ViewModelLocator.SystemMessageStatic.ProgressBarError = inErrorState);
        }

        /// <summary>
        /// Gets the progress bar complete flag value.
        /// </summary>
        /// <returns>A flag, indicating whether the progress bar has finished.</returns>
        public static bool IsProgressComplete()
        {
            return ViewModelLocator.SystemMessageStatic.ProgressComplete;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Global
{
    public static class WindowSettings
    {
        public static double AllPricesTop { get; set; }
        public static double AllPricesLeft { get; set; }
        public static double AllPricesWidth { get; set; }
        public static double AllPricesHeight { get; set; }
        public static bool AllPricesMaximised { get; set; }

        public static double TransactionEditorTop { get; set; }
        public static double TransactionEditorLeft { get; set; }
        public static double TransactionEditorWidth { get; set; }
        public static double TransactionEditorHeight { get; set; }
        public static bool TransactionEditorMaximised { get; set; }

        public static double LabelAssociationTop { get; set; }
        public static double LabelAssociationLeft { get; set; }
        public static double LabelAssociationWidth { get; set; }
        public static double LabelAssociationHeight { get; set; }
        public static bool LabelAssociationMaximised { get; set; }

        public static double StockMoveTop { get; set; }
        public static double StockMoveLeft { get; set; }
        public static double StockMoveWidth { get; set; }
        public static double StockMoveHeight { get; set; }
        public static bool StockMoveMaximised { get; set; }

        public static double StockTakeTop { get; set; }
        public static double StockTakeLeft { get; set; }
        public static double StockTakeWidth { get; set; }
        public static double StockTakeHeight { get; set; }
        public static bool StockTakeMaximised { get; set; }

        public static double StockReconciliationTop { get; set; }
        public static double StockReconciliationLeft { get; set; }
        public static double StockReconciliationWidth { get; set; }
        public static double StockReconciliationHeight { get; set; }
        public static bool StockReconciliationMaximised { get; set; }

        public static double PriceListDetailTop { get; set; }
        public static double PriceListDetailLeft { get; set; }
        public static double PriceListDetailWidth { get; set; }
        public static double PriceListDetailHeight { get; set; }
        public static bool PriceListDetailMaximised{ get; set; }

        public static double PriceListMasterTop { get; set; }
        public static double PriceListMasterLeft { get; set; }
        public static double PriceListMasterWidth { get; set; }
        public static double PriceListMasterHeight { get; set; }
        public static bool PriceListMasterMaximised { get; set; }

        public static double BatchEditTop { get; set; }
        public static double BatchEditLeft { get; set; }
        public static double BatchEditWidth { get; set; }
        public static double BatchEditHeight { get; set; }
        public static bool BatchEditMaximised { get; set; }

        public static double BatchEditorTop { get; set; }
        public static double BatchEditorLeft { get; set; }
        public static double BatchEditorWidth { get; set; }
        public static double BatchEditorHeight { get; set; }
        public static bool BatchEditorMaximised { get; set; }


        public static double RecipeSearchTop { get; set; }
        public static double RecipeSearchLeft { get; set; }
        public static double RecipeSearchWidth { get; set; }
        public static double RecipeSearchHeight { get; set; }
        public static bool RecipeSearchMaximised { get; set; }

        public static double BatchSetUpTop { get; set; }
        public static double BatchSetUpLeft { get; set; }
        public static double BatchSetUpWidth { get; set; }
        public static double BatchSetUpHeight { get; set; }
        public static bool BatchSetUpMaximised { get; set; }

        public static double SpecificationsTop { get; set; }
        public static double SpecificationsLeft { get; set; }
        public static double SpecificationsWidth { get; set; }
        public static double SpecificationsHeight { get; set; }
        public static bool SpecificationsMaximised { get; set; }

        public static double TelesalesTop { get; set; }
        public static double TelesalesLeft { get; set; }
        public static double TelesalesWidth { get; set; }
        public static double TelesalesHeight { get; set; }
        public static bool TelesalesMaximised { get; set; }

        public static double TelesalesSetUpTop { get; set; }
        public static double TelesalesSetUpLeft { get; set; }
        public static double TelesalesSetUpWidth { get; set; }
        public static double TelesalesSetUpHeight { get; set; }
        public static bool TelesalesSetUpMaximised { get; set; }

        public static double TelesalesEndCallTop { get; set; }
        public static double TelesalesEndCallLeft { get; set; }
        public static double TelesalesEndCallWidth { get; set; }
        public static double TelesalesEndCallHeight { get; set; }

        public static double TelesalesSearchTop { get; set; }
        public static double TelesalesSearchLeft { get; set; }
        public static double TelesalesSearchWidth { get; set; }
        public static double TelesalesSearchHeight { get; set; }
        public static bool TelesalesSearchMaximised{ get; set; }

        public static double AttributeAllocationTop { get; set; }
        public static double AttributeAllocationLeft { get; set; }
        public static double AttributeAllocationWidth { get; set; }
        public static double AttributeAllocationHeight { get; set; }
        public static bool AttributeAllocationMaximised { get; set; }

        public static double ReportFoldersTop { get; set; }
        public static double ReportFoldersLeft { get; set; }
        public static double ReportFoldersWidth { get; set; }
        public static double ReportFoldersHeight { get; set; }
        public static bool ReportFoldersMaximised { get; set; }

        public static double ReportProcessTop { get; set; }
        public static double ReportProcessLeft { get; set; }
        public static double ReportProcessWidth { get; set; }
        public static double ReportProcessHeight { get; set; }
        public static bool ReportProcessMaximised { get; set; }

        public static double TemplateNameTop { get; set; }
        public static double TemplateNameLeft { get; set; }
        public static double TemplateNameWidth { get; set; }
        public static double TemplateNameHeight { get; set; }
        public static bool TemplateNameMaximised { get; set; }

        public static double TemplateGroupTop { get; set; }
        public static double TemplateGroupLeft { get; set; }
        public static double TemplateGroupWidth { get; set; }
        public static double TemplateGroupHeight { get; set; }
        public static bool TemplateGroupMaximised { get; set; }

        public static double WeightBandGroupTop { get; set; }
        public static double WeightBandGroupLeft { get; set; }
        public static double WeightBandGroupWidth { get; set; }
        public static double WeightBandGroupHeight { get; set; }
        public static bool WeightBandGroupMaximised { get; set; }

        public static double WeightBandPricingTop { get; set; }
        public static double WeightBandPricingLeft { get; set; }
        public static double WeightBandPricingWidth { get; set; }
        public static double WeightBandPricingHeight { get; set; }
        public static bool WeightBandPricingMaximised { get; set; }

        public static double WeightBandTop { get; set; }
        public static double WeightBandLeft { get; set; }
        public static double WeightBandWidth { get; set; }
        public static double WeightBandHeight { get; set; }
        public static bool WeightBandMaximised { get; set; }

        public static double AttributeMasterTop { get; set; }
        public static double AttributeMasterLeft { get; set; }
        public static double AttributeMasterWidth { get; set; }
        public static double AttributeMasterHeight { get; set; }
        public static bool AttributeMasterMaximised { get; set; }

        public static double TabsTop { get; set; }
        public static double TabsLeft { get; set; }
        public static double TabsWidth { get; set; }
        public static double TabsHeight { get; set; }
        public static bool TabsMaximised { get; set; }

        public static double NotesTop { get; set; }
        public static double NotesLeft { get; set; }
        public static double NotesWidth { get; set; }
        public static double NotesHeight { get; set; }
        public static bool NotesMaximised { get; set; }

        public static double DispatchContainerTop { get; set; }
        public static double DispatchContainerLeft { get; set; }
        public static double DispatchContainerWidth { get; set; }
        public static double DispatchContainerHeight { get; set; }
        public static bool DispatchContainerMaximised { get; set; }

        public static double DispatchTop { get; set; }
        public static double DispatchLeft { get; set; }
        public static double DispatchWidth { get; set; }
        public static double DispatchHeight { get; set; }
        public static bool DispatchMaximised { get; set; }

        public static double SyncPartnersTop { get; set; }
        public static double SyncPartnersLeft { get; set; }
        public static double SyncPartnersWidth { get; set; }
        public static double SyncPartnersHeight { get; set; }
        public static bool SyncPartnersMaximised{ get; set; }

        public static double SyncProductsTop { get; set; }
        public static double SyncProductsLeft { get; set; }
        public static double SyncProductsWidth { get; set; }
        public static double SyncProductsHeight { get; set; }
        public static bool SyncProductsMaximised { get; set; }

        public static double SyncPricesTop { get; set; }
        public static double SyncPricesLeft { get; set; }
        public static double SyncPricesWidth { get; set; }
        public static double SyncPricesHeight { get; set; }
        public static bool SyncPricesMaximised { get; set; }

        public static double ARDispatchDetailsTop { get; set; }
        public static double ARDispatchDetailsLeft { get; set; }
        public static double ARDispatchDetailsWidth { get; set; }
        public static double ARDispatchDetailsHeight { get; set; }
        public static bool ARDispatchDetailsMaximised { get; set; }

        public static double IntakeDetailsTop { get; set; }
        public static double IntakeDetailsLeft { get; set; }
        public static double IntakeDetailsWidth { get; set; }
        public static double IntakeDetailsHeight { get; set; }
        public static bool IntakeDetailsMaximised { get; set; }

        public static double Dispatch2Top { get; set; }
        public static double Dispatch2Left { get; set; }
        public static double Dispatch2Width { get; set; }
        public static double Dispatch2Height { get; set; }
        public static bool Dispatch2Maximised { get; set; }

        public static double QuoteTop { get; set; }
        public static double QuoteLeft { get; set; }
        public static double QuoteWidth { get; set; }
        public static double QuoteHeight { get; set; }
        public static bool QuoteMaximised { get; set; }

        public static double LairageOrderTop { get; set; }
        public static double LairageOrderLeft { get; set; }
        public static double LairageOrderWidth { get; set; }
        public static double LairageOrderHeight { get; set; }
        public static bool LairageOrderMaximised { get; set; }

        public static double LairageIntakeTop { get; set; }
        public static double LairageIntakeLeft { get; set; }
        public static double LairageIntakeWidth { get; set; }
        public static double LairageIntakeHeight { get; set; }
        public static bool LairageIntakeMaximised { get; set; }

        public static double LairageAnimalsSearchTop { get; set; }
        public static double LairageAnimalsSearchLeft { get; set; }
        public static double LairageAnimalsSearchWidth { get; set; }
        public static double LairageAnimalsSearchHeight { get; set; }
        public static bool LairageAnimalsSearchMaximised { get; set; }

        public static double CarcassDispatchTop { get; set; }
        public static double CarcassDispatchLeft { get; set; }
        public static double CarcassDispatchWidth { get; set; }
        public static double CarcassDispatchHeight { get; set; }
        public static bool CarcassDispatchMaximised { get; set; }

        public static double LairageSearchTop { get; set; }
        public static double LairageSearchLeft { get; set; }
        public static double LairageSearchWidth { get; set; }
        public static double LairageSearchHeight { get; set; }
        public static bool LairageSearchMaximised { get; set; }

        public static double PaymentTop { get; set; }
        public static double PaymentLeft { get; set; }
        public static double PaymentWidth { get; set; }
        public static double PaymentHeight { get; set; }
        public static bool PaymentMaximised { get; set; }

        public static double PaymentDeductionsTop { get; set; }
        public static double PaymentDeductionsLeft { get; set; }
        public static double PaymentDeductionsWidth { get; set; }
        public static double PaymentDeductionsHeight { get; set; }
        public static bool PaymentDeductionsMaximised { get; set; }

        public static double PricingMatrixTop { get; set; }
        public static double PricingMatrixLeft { get; set; }
        public static double PricingMatrixWidth { get; set; }
        public static double PricingMatrixHeight { get; set; }
        public static bool PricingMatrixMaximised { get; set; }

        public static double PaymentProposalTop { get; set; }
        public static double PaymentProposalLeft { get; set; }
        public static double PaymentProposalWidth { get; set; }
        public static double PaymentProposalHeight { get; set; }
        public static bool PaymentProposalMaximised { get; set; }

        public static double PaymentProposalSearchTop { get; set; }
        public static double PaymentProposalSearchLeft { get; set; }
        public static double PaymentProposalSearchWidth { get; set; }
        public static double PaymentProposalSearchHeight { get; set; }
        public static bool PaymentProposalSearchMaximised { get; set; }

        public static double RecipeTop { get; set; }
        public static double RecipeLeft { get; set; }
        public static double RecipeWidth { get; set; }
        public static double RecipeHeight { get; set; }
        public static bool RecipeMaximised { get; set; }

        public static double IntakeOrderTop { get; set; }
        public static double IntakeOrderLeft { get; set; }
        public static double IntakeOrderWidth { get; set; }
        public static double IntakeOrderHeight { get; set; }
        public static bool IntakeOrderMaximised { get; set; }

        public static double OrderTop { get; set; }
        public static double OrderLeft { get; set; }
        public static double OrderWidth { get; set; }
        public static double OrderHeight { get; set; }
        public static bool OrderMaximised { get; set; }

        public static double ReturnsTop { get; set; }
        public static double ReturnsLeft { get; set; }
        public static double ReturnsWidth { get; set; }
        public static double ReturnsHeight { get; set; }
        public static bool ReturnsMaximised { get; set; }

        public static double QuickOrderTop { get; set; }
        public static double QuickOrderLeft { get; set; }
        public static double QuickOrderWidth { get; set; }
        public static double QuickOrderHeight { get; set; }
        public static bool QuickOrderMaximised { get; set; }

        public static double Order2Top { get; set; }
        public static double Order2Left { get; set; }
        public static double Order2Width { get; set; }
        public static double Order2Height { get; set; }
        public static bool Order2Maximised { get; set; }

        public static double InvoiceTop { get; set; }
        public static double InvoiceLeft { get; set; }
        public static double InvoiceWidth { get; set; }
        public static double InvoiceHeight { get; set; }
        public static bool InvoiceMaximised { get; set; }

        public static double APInvoiceTop { get; set; }
        public static double APInvoiceLeft { get; set; }
        public static double APInvoiceWidth { get; set; }
        public static double APInvoiceHeight { get; set; }
        public static bool APInvoiceMaximised { get; set; }

        public static double BPMasterTop { get; set; }
        public static double BPMasterLeft { get; set; }
        public static double BPMasterWidth { get; set; }
        public static double BPMasterHeight { get; set; }
        public static bool BPMasterMaximised { get; set; }

        public static double INMasterTop { get; set; }
        public static double INMasterLeft { get; set; }
        public static double INMasterWidth { get; set; }
        public static double INMasterHeight { get; set; }
        public static bool INMasterMaximised { get; set; }

        public static double INGroupTop { get; set; }
        public static double INGroupLeft { get; set; }
        public static double INGroupWidth { get; set; }
        public static double INGroupHeight { get; set; }
        public static bool INGroupMaximised { get; set; }

        public static double INGroupSetUpTop { get; set; }
        public static double INGroupSetUpLeft { get; set; }
        public static double INGroupSetUpWidth { get; set; }
        public static double INGroupSetUpHeight { get; set; }
        public static bool INGroupSetUpMaximised { get; set; }

        public static double AccountsTop { get; set; }
        public static double AccountsLeft { get; set; }
        public static double AccountsWidth { get; set; }
        public static double AccountsHeight { get; set; }
        public static bool AccountsMaximised { get; set; }

        public static double DeviceSettingsTop { get; set; }
        public static double DeviceSettingsLeft { get; set; }
        public static double DeviceSettingsWidth { get; set; }
        public static double DeviceSettingsHeight { get; set; }
        public static bool DeviceSettingsMaximised { get; set; }

        public static double ImageViewTop { get; set; }
        public static double ImageViewLeft { get; set; }
        public static double ImageViewWidth { get; set; }
        public static double ImageViewHeight { get; set; }
        public static bool ImageViewMaximised { get; set; }


        public static double BatchDataTop { get; set; }
        public static double BatchDataLeft { get; set; }
        public static double BatchDataWidth { get; set; }
        public static double BatchDataHeight { get; set; }
        public static bool BatchDataMaximised { get; set; }

        public static double ApplyPriceTop { get; set; }
        public static double ApplyPriceLeft { get; set; }
        public static double ApplyPriceWidth { get; set; }
        public static double ApplyPriceHeight { get; set; }

        public static double MRPTop { get; set; }
        public static double MRPLeft { get; set; }
        public static double MRPWidth { get; set; }
        public static double MRPHeight { get; set; }
        public static bool MRPMaximised { get; set; }

        public static double SpecsTop { get; set; }
        public static double SpecsLeft { get; set; }
        public static double SpecsWidth { get; set; }
        public static double SpecsHeight { get; set; }
        public static bool SpecsMaximised { get; set; }

        public static double DepartmentTop { get; set; }
        public static double DepartmentLeft { get; set; }
        public static double DepartmentWidth { get; set; }
        public static double DepartmentHeight { get; set; }
        public static bool DepartmentMaximised { get; set; }

        public static double InvoiceCreationTop { get; set; }
        public static double InvoiceCreationLeft { get; set; }
        public static double InvoiceCreationWidth { get; set; }
        public static double InvoiceCreationHeight { get; set; }
        public static bool InvoiceCreationMaximised { get; set; }

        public static double APInvoiceCreationTop { get; set; }
        public static double APInvoiceCreationLeft { get; set; }
        public static double APInvoiceCreationWidth { get; set; }
        public static double APInvoiceCreationHeight { get; set; }
        public static bool APInvoiceCreationMaximised { get; set; }

        public static double CreditNoteCreationTop { get; set; }
        public static double CreditNoteCreationLeft { get; set; }
        public static double CreditNoteCreationWidth { get; set; }
        public static double CreditNoteCreationHeight { get; set; }
        public static bool CreditNoteCreationMaximised { get; set; }

        public static double ReportSetUpTop { get; set; }
        public static double ReportSetUpLeft { get; set; }
        public static double ReportSetUpWidth { get; set; }
        public static double ReportSetUpHeight { get; set; }
        public static bool ReportSetUpMaximised { get; set; }

        public static double ReportSearchTop { get; set; }
        public static double ReportSearchLeft { get; set; }
        public static double ReportSearchWidth { get; set; }
        public static double ReportSearchHeight { get; set; }
        public static bool ReportSearchMaximised { get; set; }

        public static double UserGroupTop { get; set; }
        public static double UserGroupLeft { get; set; }
        public static double UserGroupWidth { get; set; }
        public static double UserGroupHeight { get; set; }
        public static bool UserGroupMaximised { get; set; }

        public static double UserSearchTop { get; set; }
        public static double UserSearchLeft { get; set; }
        public static double UserSearchWidth { get; set; }
        public static double UserSearchHeight { get; set; }
        public static bool UserSearchMaximised { get; set; }

        public static double UserTop { get; set; }
        public static double UserLeft { get; set; }
        public static double UserWidth { get; set; }
        public static double UserHeight { get; set; }
        public static bool UserMaximised { get; set; }

        public static double AuthorisationsTop { get; set; }
        public static double AuthorisationsLeft { get; set; }
        public static double AuthorisationsWidth { get; set; }
        public static double AuthorisationsHeight { get; set; }
        public static bool AuthorisationsMaximised { get; set; }

        public static double StockAdjustTop { get; set; }
        public static double StockAdjustLeft { get; set; }
        public static double StockAdjustWidth { get; set; }
        public static double StockAdjustHeight { get; set; }
        public static bool StockAdjustMaximised{ get; set; }

        public static double AccountsPurchasesTop { get; set; }
        public static double AccountsPurchasesLeft { get; set; }
        public static double AccountsPurchasesWidth { get; set; }
        public static double AccountsPurchasesHeight { get; set; }
        public static bool AccountsPurchasesMaximised { get; set; }

        public static double AccountsReturnsTop { get; set; }
        public static double AccountsReturnsLeft { get; set; }
        public static double AccountsReturnsWidth { get; set; }
        public static double AccountsReturnsHeight { get; set; }
        public static bool AccountsReturnsMaximised { get; set; }

        public static double ReportViewerTop { get; set; }
        public static double ReportViewerLeft { get; set; }
        public static double ReportViewerWidth { get; set; }
        public static double ReportViewerHeight { get; set; }
        public static bool ReportViewerMaximised{ get; set; }

        public static double SearchDataTop { get; set; }
        public static double SearchDataLeft { get; set; }
        public static double SearchDataWidth { get; set; }
        public static double SearchDataHeight { get; set; }
        public static bool SearchDataMaximised{ get; set; }

        public static double INSearchDataTop { get; set; }
        public static double INSearchDataLeft { get; set; }
        public static double INSearchDataWidth { get; set; }
        public static double INSearchDataHeight { get; set; }
        public static bool INSearchDataMaximised { get; set; }

        public static double ProductionSearchDataTop { get; set; }
        public static double ProductionSearchDataLeft { get; set; }
        public static double ProductionSearchDataWidth { get; set; }
        public static double ProductionSearchDataHeight { get; set; }
        public static bool ProductionSearchDataMaximised { get; set; }

        public static double SaleSearchDataTop { get; set; }
        public static double SaleSearchDataLeft { get; set; }
        public static double SaleSearchDataWidth { get; set; }
        public static double SaleSearchDataHeight { get; set; }
        public static bool SaleSearchDataMaximised { get; set; }

        public static double SpecsSearchTop { get; set; }
        public static double SpecsSearchLeft { get; set; }
        public static double SpecsSearchWidth { get; set; }
        public static double SpecsSearchHeight { get; set; }
        public static bool SpecsSearchMaximised { get; set; }

        public static double AlertUsersSetUpTop { get; set; }
        public static double AlertUsersSetUpLeft { get; set; }
        public static double AlertUsersSetUpWidth { get; set; }
        public static double AlertUsersSetUpHeight { get; set; }
        public static bool AlertUsersSetUpMaximised { get; set; }

        public static double WorkflowAlertsSearchTop { get; set; }
        public static double WorkflowAlertsSearchLeft { get; set; }
        public static double WorkflowAlertsSearchWidth { get; set; }
        public static double WorkflowAlertsSearchHeight { get; set; }
        public static bool WorkflowAlertsSearchMaximised { get; set; }

        public static double WorkflowSearchTop { get; set; }
        public static double WorkflowSearchLeft { get; set; }
        public static double WorkflowSearchWidth { get; set; }
        public static double WorkflowSearchHeight { get; set; }
        public static bool WorkflowSearchMaximised { get; set; }

        public static double AttributeSearchTop { get; set; }
        public static double AttributeSearchLeft { get; set; }
        public static double AttributeSearchWidth { get; set; }
        public static double AttributeSearchHeight { get; set; }
        public static bool AttributeSearchMaximised { get; set; }

        public static double SpecialPricesTop { get; set; }
        public static double SpecialPricesLeft { get; set; }
        public static double SpecialPricesWidth { get; set; }
        public static double SpecialPricesHeight { get; set; }
        public static bool SpecialPricesMaximised { get; set; }

        public static double SearchSaleCustomersTop { get; set; }
        public static double SearchSaleCustomersLeft { get; set; }
        public static double SearchSaleCustomersWidth { get; set; }
        public static double SearchSaleCustomersHeight { get; set; }
        public static bool SearchSaleCustomersMaximised { get; set; }

        public static double TransactionSearchDataTop { get; set; }
        public static double TransactionSearchDataLeft { get; set; }
        public static double TransactionSearchDataWidth { get; set; }
        public static double TransactionSearchDataHeight { get; set; }
        public static bool TransactionSearchDataMaximised { get; set; }
    }
}

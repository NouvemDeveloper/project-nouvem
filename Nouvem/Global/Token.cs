﻿// -----------------------------------------------------------------------
// <copyright file="Token.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Global
{
    public static class Token
    {
        /// <summary> Token used to as the first parameter of the messenger construct, used when a standard generic message is only required.
        /// </summary>
        public const string Message = "";

        /// <summary>
        /// Token used to inform the app.xaml.cs to open a window.
        /// </summary>
        public const string OpenView = "OPEN_VIEW";

        /// <summary>
        /// Token used to inform the loading window to close.
        /// </summary>
        public const string CloseLoadingWindow = "CLOSE_LOADING_WINDOW";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string EmailingReport = "EMAILING_REPORT";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string DeleteTransaction = "DELETE_TRANSACTION";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string UndeleteTransaction = "UNDELETE_TRANSACTION";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string ClosingSaleOrderWindow = "CLOSING_SALE_ORDER_WINDOW";
        public const string ClosingSaleOrder2Window = "CLOSING_SALE_ORDER2_WINDOW";
        public const string ClosingDispatch2Window = "CLOSING_DISPATCH2_WINDOW";
        public const string ClosingDispatchWindow = "CLOSING_DISPATCH_WINDOW";
        public const string ClosingPurchaseOrderWindow = "CLOSING_PURCHASE_ORDER_WINDOW";
        public const string ApplyPrice = "APPLY_PRICE";
        public const string SetBatchOrderData = "SET_BATCH_ORDER_DATA";
        public const string DeleteLairageAnimal = "DELETE_LAIRAGE_ANIMAL";
        public const string LabelViewerClosed = "LABEL_VIEWER_CLOSED";
        public const string RefreshCarcassDispatch = "REFRESH_CARCASS_DISPATCH";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateTouchscreenPalletisation = "CREATE_TOUCHSCREEN_PALLETISATION";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowTransactionEditNotesWindow = "SHOW_TRANSACTION_EDIT_NOTES_WINDOW";

        public const string ScrollToRow = "SCROLL_TO_ROW";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateSequencerCategory = "CREATE_SEQUENCER_CATEGORY";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateScannerStockMoveOrders = "CREATE_SCANNER_STOCK_MOVE_ORDERS";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CreateTouchscreenWarehouse = "CREATE_TOUCHSCREEN_WAREHOUSE";

        ///<summary>
        ///Token used to inform the recipient to close the product selection view.
        ///</summary>
        public const string CloseScannerCategoryWindow = "CLOSE_SCANNER_CATEGORY_WINDOW";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string EnableProductSelectionOnIntoProduction = "ENABLE_PRODUCT_SELECTION_ON_INTOPRODUCTION";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string ShowAttributeQuestion = "SHOW_ATTRIBUTE_QUESTION";

        /// <summary>
        /// Token used to inform the recepient that a value has been selected on the Collection Display screen.
        /// </summary>
        public const string CalenderDisplayValue = "CALENDER_DISPLAY_VALUE";

        /// <summary>
        /// Token used to inform the recepient that a value has been selected on the Collection Display screen.
        /// </summary>
        public const string CalenderDisplayValueDispatch = "CALENDER_DISPLAY_VALUE_DISPATCH";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string CloseProcess = "CLOSE_PROCESS";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string HaccpComplete = "HACCP_COMPLETE";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string ShowAllRecentOrders = "SHOW_ALL_RECENT_ORDERS";

        public const string ShowSalesSearchScreen = "SHOW_SALES_SEARCH_SCREEN";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string ProductionTypeSelected = "PRODUCTION_TYPE_SELECTED";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string LocationSelected = "LOCATION_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateCalender = "CREATE_CALENDER";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string ShowStandardColumns = "SHOW_STANDARD_COLUMNS";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CountryItemSelected = "COUNTRY_ITEM_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string PlantItemSelected = "PLANT_ITEM_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string RecipeCellChange = "RECIPE_CELL_CHANGE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string ItemSelected = "ITEM_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateIndicator = "CREATE_INDICATOR";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string PrintBarcode = "PRINT_BARCODE";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected for a report.
        /// </summary>
        public const string DisplayTouchscreenBatchReport = "DISPLAY_TOUCHSCREEN_BATCH_REPORT";

        /// <summary>
        /// Token used to display the touchscreen label selection window.
        /// </summary>
        public const string DisplayProcessSelection = "DISPLAY_PROCESS_SELECTION";

        /// <summary>
        /// Token used to display the touchscreen label selection window.
        /// </summary>
        public const string SelectProcessSelection = "SELECT_PROCESS_SELECTION";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string ClearAttributeControls = "CLEAR_ATTRIBUTE_CONTROLS";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string AttributeTypeSelected = "ATTRIBUTE_TYPE_SELECTED";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string SetTraceability = "SET_TRACEABILITY";

        /// <summary>
        /// Token used to display a grid print preview.
        /// </summary>
        public const string ProcessSelected = "PROCESS_SELECTED";

        /// <summary>
        /// Token used to inform the loading window to close.
        /// </summary>
        public const string CloseSystemInformationWindow = "CLOSE_SYSTEM_INFORMATION_WINDOW";

        /// <summary>
        /// Token used to inform the factory window to close.
        /// </summary>
        public const string CloseFactoryWindow = "CLOSE_FACTORY_WINDOW";

        /// <summary>
        /// Token used to inform the factory window to close.
        /// </summary>
        public const string CloseTouchscreenStockTake = "CLOSE_TOUCHSCREEN_STOCKTAKE_WINDOW";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string ScalesInMotion = "SCALES_IN_MOTION";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string NoBaseGroups = "NO_BASE_GROUPS";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string CompleteMultiOrder = "COMPLETE_MULTI_ORDER";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string AllowSplitScanSelection = "ALLOW_SPLIT_SCAN_SELECTION";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string CarcassesSelected = "CARCASSES_SELECTED";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string SaveBatchAttributes = "SAVE_BATCH_ATTRIBUTES";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string WarehouseSelected = "WAREHOUSE_SELECTED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string SetTare = "SET_TARE";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string StockModeChange = "STOCK_MODE_CHANGE";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string EnterSelected = "ENTER_SELECTED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string UserOrGroupSelected = "USER_OR_GROUP__SELECTED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string ReportValues = "REPORT_VALUES";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string ReportSelected = "REPORT_SELECTED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string PaymentSelected = "PAYMENT_SELECTED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string GetPaymentSelected = "GET_PAYMENT_SELECTED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string NoResponseGiven = "NO_RESPONSE_GIVEN";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string ResponseGiven = "RESPONSE_GIVEN";

        /// <summary>
        /// Token used to inform the stock reconciliation window to close.
        /// </summary>
        public const string CloseStockReconciliationWindow = "CLOSE_STOCK_RECONCILIATION_WINDOW";

        /// <summary>
        /// Token used to inform the stock reconciliation window to close.
        /// </summary>
        public const string CloseStockStockTakeWindow = "CLOSE_STOCK_TAKE_WINDOW";

        /// <summary>
        /// Token used to inform the receipient to clear it's selected product.
        /// </summary>
        public const string ClearSelectedProduct = "CLEAR_SELECTED_PRODUCT";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the master view.
        /// </summary>
        public const string CreateMasterView = "CREATE_MASTER_VIEW";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the master view.
        /// </summary>
        public const string CreateTouchscreenView = "CREATE_TOUCHSCREEN_VIEW";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the master view.
        /// </summary>
        public const string PriceListsUpdated = "PRICE_LISTS_UPDATED";

        /// <summary>
        /// Token used to inform the recipient of a weight error.
        /// </summary>
        public const string WeightError = "WEIGHT_ERROR";

        /// <summary>
        /// Token used to inform the recipient of a weight error.
        /// </summary>
        public const string RefreshMasterDocument = "REFRESH_MASTER_DOCUMENT";

        /// <summary>
        /// Token used to inform the recipient of a weight error.
        /// </summary>
        public const string CreateWorkflowAlerts = "CREATE_WORKFLOW_ALERTS";

        /// <summary>
        /// Token used to inform the recipient of a weight error.
        /// </summary>
        public const string TemplateAllocationsSelected = "TEMPLATE_ALLOCTIONS_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the master view.
        /// </summary>
        public const string CreateTouchscreenOrdersView = "CREATE_TOUCHSCREEN_ORDERS_VIEW";

        /// <summary>
        /// Token used to close the recipient window.
        /// </summary>
        public const string CloseReportViewer = "CLOSE_REPORT_VIEWER";

        /// <summary>
        /// Token used to close the recipient window.
        /// </summary>
        public const string DisableMultipleReports = "DISDABLE_MULTIPLE_REPORTS";

        /// <summary>
        /// Token used to inform all observers that the grid filter box has closed.
        /// </summary>
        public const string FilterPanelClosed = "FILTER_PANEL_CLOSED";

        /// <summary>
        /// Token used to send the main menu item command parameter.
        /// </summary>
        public const string CommandParameter = "COMMAND_PARAMETER";

        /// <summary>
        /// Token used to set the report server path
        /// </summary>
        public const string SetReportPath = "SET_REPORT_PATH";

        /// <summary>
        /// Token used to set the report server path
        /// </summary>
        public const string SetTouchscreenReportPath = "SET_TOUCHSCREEN_REPORT_PATH";

        /// <summary>
        /// Token used to set the report server path
        /// </summary>
        public const string SetReportPathAndExport = "SET_REPORT_PATH_AND_EXPORT";

        /// <summary>
        /// Token used to create the invoice creation screen.
        /// </summary>
        public const string CreateInvoice = "CREATE_INVOICE";

        /// <summary>
        /// Token used to create the invoice creation screen.
        /// </summary>
        public const string CreateAPInvoice = "CREATE_APINVOICE";

        /// <summary>
        /// Token used to create the invoice creation screen.
        /// </summary>
        public const string CreateCreditNote = "CREATE_CREDIT_NOTE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateAttributeResponse = "CREATE_ATTRIBUTE_RESPONSE";

        /// <summary>
        /// Token used to send the scles in motion flag.
        /// </summary>
        public const string PostSelectionMacroRefresh = "POST_SELECTION_MACRO_REFRESH";

        /// <summary>
        /// Token used to inform the recipient to display the dispatches.
        /// </summary>
        public const string DisplayDispatches = "DISPLAY_DISPATCHES";

        /// <summary>
        /// Token used to set the ssrs report path.
        /// </summary>
        public const string ReportServerPath = "REPORT_SERVER_PATH";

        /// <summary>
        /// Token used to inform the receipients of a label association change.
        /// </summary>
        public const string LabelAssociationsUpdated = "LABEL_ASSOCIATIONS_UPDATED";

        /// <summary>
        /// Token used to inform the receipient of a labels number update.
        /// </summary>
        public const string LabelsToPrintUpdate = "LABELS_TO_PRINT_UPDATE";

        /// <summary>
        /// Token used to inform the receipient of a qty update.
        /// </summary>
        public const string QuantityUpdate = "QUANTITY_UPDATE";

        /// <summary>
        /// Token used to inform the receipient of a pieces update.
        /// </summary>
        public const string SetPieces = "SET_PIECES";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner module selectionview.
        /// </summary>
        public const string CreateScannerModuleSelection = "CREATE_SCANNER_MODULE_SELECTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the touchscreen module selection view.
        /// </summary>
        public const string CreateTouchscreenModuleOnlyMode = "CREATE_TOUCHSCREEN_MODULE_ONLY_MODULE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreatePrinterSelection = "CREATE_PRINTER_SELECTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateScannerFactory = "CREATE_SCANNER_FACTORY";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string SetScannerSize = "SET_SCANNER_SIZE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateWaitIndicator = "CREATE_WAIT_INDICATOR";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateScannerFactoryStockTake = "CREATE_SCANNER_FACTORY_STOCKTAKE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateScannerStockMovement = "CREATE_SCANNER_STOCK_MOVEMENT";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreatePalletisation = "CREATE_PALLETISATION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateScannerIntoProduction = "CREATE_SCANNER_INTO_PRODUCTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CreateTouchscreenModuleSelection = "CREATE_TOUCHSCREEN_MODULE_SELECTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CreateTouchscreenProductionOrder = "CREATE_TOUCHSCREEN_PRODUCTION_ORDER";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CreateTouchscreenIntakeBatches = "CREATE_TOUCHSCREEN_INTAKE_BATCHES";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CreateTouchscreenOrder = "CREATE_TOUCHSCREEN_ORDER";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CreateTouchscreenPartners = "CREATE_TOUCHSCREEN_PARTNERS";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string CreateServerSetUp = "CREATE_SERVER_SETUP";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string CreateServerSetUpScanner = "CREATE_SERVER_SETUP_SCANNER";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string CreateWorkflowTemplateSelection = "CREATE_WORKFLOW_TEMPLATE_SELECTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string CreateWorkflowSelection = "CREATE_WORKFLOW_SELECTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string CreateSystemInformation = "CREATE_SYSTEM_INFORMATION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string WorkflowSelected = "WORKFLOW_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string CreateTareCalculator = "CREATE_TARE_CALCULATOR";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the server set up view.
        /// </summary>
        public const string TareCalculatorRequested = "TARE_CALCULATOR_REQUESTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the login set up view.
        /// </summary>
        public const string CreateTouchscreenLogin = "CREATE_TOUCHSCREEN_LOGIN";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the user password change view.
        /// </summary>
        public const string CreateUserPasswordChange = "CREATE_USER_PASSWORD_CHANGE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the login selection view.
        /// </summary>
        public const string CreateLoginSelectionView = "CREATE_LOGIN_SELECTION";

        /// <summary>
        /// Token used to inform the epos system to update it's products.
        /// </summary>
        public const string UpdateEpos = "UPDATE_EPOS";
       
         ///<summary>
         ///Token used to inform the app.xaml.cs to close the device set up view.
         ///</summary>
        public const string CloseDeviceSetUpWindow = "CLOSE_DEVICE_SETUP";

        ///<summary>
        ///Token used to inform the app.xaml.cs to close the vat set up view.
        ///</summary>
        public const string CloseVatSetUpWindow = "CLOSE_VAT_SETUP";

        ///<summary>
        ///Token used to inform the recipient to close the device search view.
        ///</summary>
        public const string CloseDeviceSearchWindow = "CLOSE_DEVICE_SEARCH_SETUP";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseEposWindow = "CLOSE_EPOS_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseQualityTemplateNameWindow = "CLOSE_QUALITY_TEMPLATE_NAME_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseQualityTemplateAllocationWindow = "CLOSE_QUALITY_TEMPLATE_ALLOCATION_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseWarehouseWindow = "CLOSE_WAREHOUSE_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseWarehouseLocationWindow = "CLOSE_WAREHOUSE_LOCATION_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseInventoryGroupMasterWindow = "CLOSE_INVENTORY_GROUP__MASTER_WINDOW";
        
        ///<summary>
        ///Token used to inform the recipient to close the product selection view.
        ///</summary>
        public const string CloseProductSelectionWindow = "CLOSE_PRODUCTION_SELECTION_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close the product selection view.
        ///</summary>
        public const string CloseSpecificationsWindow = "CLOSE_SPECIFICATIONS_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close the product selection view.
        ///</summary>
        public const string CloseBatchSetUpWindow = "CLOSE_BATCH_SET_UP_WINDOW";
       
         ///<summary>
        ///Token used to inform the recepient to close the license admin view.
        ///</summary>
        public const string CloseLicenseAdminWindow = "CLOSE_LICENSE_ADMIN_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close the currency view.
        ///</summary>
        public const string CloseCurrencyWindow = "CLOSE_CURRENCY_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close the quality view.
        ///</summary>
        public const string CloseQualityWindow = "CLOSE_QUALITY_WINDOW";

          ///<summary>
        ///Token used to inform the recepient to close the user group set up view.
        ///</summary>
        public const string CloseInventoryGroupSetUpWindow = "CLOSE_INVENTORY_GROUP_SETUP_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close.
        ///</summary>
        public const string CloseDateTemplateAllocationWindow = "CLOSE_DATES_TEMPLATE_ALLOCATION_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close.
        ///</summary>
        public const string CloseTraceabilityTemplateAllocationWindow = "CLOSE_TRACEABILITY_TEMPLATE_ALLOCATION_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close.
        ///</summary>
        public const string CloseReportFoldersWindow = "CLOSE_REPORT_FOLDERS_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close.
        ///</summary>
        public const string CloseReportProcessWindow = "CLOSE_REPORT_PROCESS_WINDOW";

        ///<summary>
        ///Token used to inform the recepient to close the license import view.
        ///</summary>
        public const string CloseLicenseImportWindow = "CLOSE_LICENSE_IMPORT_SETUP";

         ///<summary>
         ///Token used to inform the app.xaml.cs to close the login selection view.
         ///</summary>
        public const string CloseLoginSelection = "CLOSE_LOGIN_SELECTION";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseMapWindow = "CLOSE_MAP_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseAPReceiptWindow = "CLOSE_APRECEIPT_WINDOW";

        ///<summary>
        ///Token used to inform the recipient to close.
        ///</summary>
        public const string CloseReturnWindow = "CLOSE_RETURN_WINDOW";

        public const string CloseQuickOrderWindow = "CLOSE_QUICK_ORDER_WINDOW";

        ///<summary>
        ///Token used to inform the app.xaml.cs to close the login view.
        ///</summary>
        public const string CloseLogin = "CLOSE_LOGIN";

        ///<summary>
        ///Token used to inform the receipient to close the module selection view.
        ///</summary>
        public const string CloseModuleSelection = "CLOSE_MODULE_SELECTION";

        /// <summary>
        /// Token used to inform the app.xaml.cs to close the emulator view.
        /// </summary>
        public const string CloseEmulator = "CLOSE_EMULATOR";

        ///<summary>
        ///Token used to inform the app.xaml.cs to close the scanner module selection view.
        ///</summary>
        public const string CloseScannerModuleSelection = "CLOSE_SCANNER_MODULE_SELECTION";

        ///<summary>
        ///Token used to inform the app.xaml.cs to close the scanner module selection view.
        ///</summary>
        public const string CloseScannerFactory = "CLOSE_SCANNER_FACTORY";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateSplashScreen = "CREATE_SPLASH_SCREEN";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateSpecs = "CREATE_SPECS";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateWorkflow = "CREATE_WORKFLOW";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateTelesale = "CREATE_TELESALE";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateWorkSearchflow = "CREATE_WORKFLOW_SEARCH";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the splash screen view.
        /// </summary>
        public const string CreateWorkflowSearch = "CREATE_WORKFLOW_SEARCH";

         /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSqlGeneratorWindow = "CLOSE_SQL_GENERATOR_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDataGeneratorWindow = "CLOSE_DATA_GENERATOR_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseLabelAssociationWindow = "CLOSE_LABEL_ASSOCIATION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseLabelSelectionWindow = "CLOSE_LABEL_SELECTION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWarehouseSelectionWindow = "CLOSE_WAREHOUSE_SELECTION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string NotesEntered = "NOTES_ENTERED";

        /// <summary>
        /// Token used to send the tare to the indicator.
        /// </summary>
        public const string TransactionEditingComplete = "TRANSACTION_EDITING_COMPLETE";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string DisplayNotes = "DISPLAY_NOTES";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAllWindows = "CLOSE_ALL_WINDOWS";

        public const string ShowQuickOrderView = "SHOW_QUICK_ORDER_VIEW";
        public const string CloseCarcassDispatchWindow = "CLOSE_CARCASS_DISPATCH_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseBatchEditorWindow = "CLOSE_BATCH_EDITOR_WINDOW";

        public const string CloseBatchEditWindow = "CLOSE_BATCH_EDIT_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseRecipeSearchWindow = "CLOSE_RECIPE_SEARCH_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDocumentTrailWindow = "CLOSE_DOCUMENT_TRAIL_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseEditTransactionWindow = "CLOSE_EDIT_TRANSACTION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTransactionDetailsWindow = "CLOSE_TRANSACTION_DETAILS__WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseEposSettingsWindow = "CLOSE_EPOS_SETTINGS_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseQuoteWindow = "CLOSE_QUOTE_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDispatchWindow = "CLOSE_DISPATCH_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDispatch2Window = "CLOSE_DISPATCH2_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDispatchContainerWindow = "CLOSE_DISPATCH_CONTAINER_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAPQuoteWindow = "CLOSE_AP_QUOTE_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAPOrderWindow = "CLOSE_AP_ORDER_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ApplyWeightBandPrices = "APPLY_WEIGHT_BAND_PRICES";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWorkflowSelection = "CLOSE_WORKFLOW_SELECTION";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWorkflow= "CLOSE_WORKFLOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWaitIndicatorWindow = "CLOSE_WAIT_INDICATOR_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseScannerIntoProduction = "CLOSE_SCANNER_INTO_PRODUCTION";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseScannerBatchSetUp = "CLOSE_SCANNER_BATCH_SET_UP";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSpecialPricesWindow = "CLOSE_SPECIAL_PRICES_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseOrderWindow = "CLOSE_ORDER_WIINDOW";

        public const string CanCloseOrderWindow = "CAN_CLOSE_ORDER_WINDOW";
        public const string CanCloseOrder2Window = "CAN_CLOSE_ORDER2_WINDOW";
        public const string CanCloseDispatchWindow = "CAN_CLOSE_DISPATCH_WINDOW";
        public const string CanCloseDispatch2Window = "CAN_CLOSE_DISPATCH2_WINDOW";
        public const string CanClosePurchaseOrderWindow = "CAN_CLOSE_PURCHASE_ORDER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseReceipeWithProductWindow = "CLOSE_RECEIPE_WITH_PRODUCT_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseReceipeWindow = "CLOSE_RECEIPE_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string SetActiveDocument = "SET_ACTIVE_DOCUMENT";

        public const string SetIndicatorManualMode = "SET_INDICATOR_MANUAL_MODE";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseLairageOrderWindow = "CLOSE_LAIRAGE_ORDER_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseLairageIntakeWindow = "CLOSE_LAIRAGE_INTAKE_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAttributeMasterSetUpWindow = "CLOSE_ATTRIBUTE_MASTER_SET_UP_WIINDOW";

        public const string CloseTabsSetUpWindow = "CLOSE_TABS_SET_UP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseLairageAnimalsSearchWindow = "CLOSE_LAIRAGE_ANIMALS_SEARCH_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePaymentDeductionsWindow = "CLOSE_PAYMENT_DEDUCTIONS_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePricingMatrixWindow = "CLOSE_PRICING_MATRIX_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseOrder2Window = "CLOSE_ORDER2_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseInvoiceWindow = "CLOSE_INVOICE_WIINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseUOMWindow = "CLOSE_UOM_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWindow = "CLOSE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePalletWindow = "CLOSE_PALLET_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseBPPropertySetUpWindow = "CLOSE_BPPROPERTY_SETUP_WINDOW";

         /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseINMasterWindow = "CLOSE_INMASTER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDocumentTypeWindow = "CLOSE_DOCUMENT_TYPE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseRegionWindow = "CLOSE_REGION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseRouteWindow = "CLOSE_ROUTE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDocumentNumberWindow = "CLOSE_DOCUMENT_NUMBER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseServerSetUpWindow = "CLOSE_SERVER_SETUP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseBPMasterWindow = "CLOSE_BPMASTER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseAuditWindow = "CLOSE_AUDIT_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseTareCalculatorWindow = "CLOSE_TARE_CALCULATOR_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseTouchscreenProductionOrders = "CLOSE_TOUCHSCREEN_PRODUCTION_ORDERS";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseTouchscreenIntakeBatches = "CLOSE_TOUCHSCREEN_INTAKE_BATCHES";

        /// <summary>
        /// Token used to inform the recepient to of a selected production order.
        /// </summary>
        public const string ProductionBatchSelected = "PRODUCTION_BATCH_SELECTED";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTraceabilityNameWindow = "CLOSE_TRACEABILITY_NAME_WINDOW";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayWarehouseKeyboard = "DISPLAY_WAREHOUSE_KEYBOARD";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the switch module ui.
        /// </summary>
        public const string CloseTouchscreenWarehouseWindow = "CREATE_TOUCHSCREEN_WAREHOUSE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTraceabilityGroupWindow = "CLOSE_TRACEABILITY_GROUP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWeightBandGroupWindow = "CLOSE_WEIGHT_BAND_GROUP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWeightBandWindow = "CLOSE_WEIGHT_BAND_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseWeightBandPricingWindow = "CLOSE_WEIGHT_BAND_PRICING_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseUserSetUpWindow = "CLOSE_USERSETUP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseMessageBoxWindow = "CLOSE_MESSAGEBOX_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CloseMenuWindow = "CLOSE_MENU_WINDOW";

        /// <summary>
        /// Token used to inform the recepient message box window to close.
        /// </summary>
        public const string CreateMenu = "CREATE_MENU";

        /// <summary>
        /// Token used to inform of an incorrect password entered.
        /// </summary>
        public const string IncorrectPasswordNotification = "INCORRECT_PASSWORD_NOTIFICATION";

         /// <summary>
        /// Token used to close the password box.
        /// </summary>
        public const string ClosePasswordEntryWindow = "CLOSE_PASSWORD_ENTRY";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseUserGroupWindow = "CLOSE_USER_GROUP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseContainerWindow = "CLOSE_CONTAINER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseNotesWindow = "CLOSE_NOTES_WINDOW";

         /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseBPGroupSetUpWindow = "CLOSE_BPGROUP_SET_UP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePlantWindow = "CLOSE_PLANT_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseItemMasterSetUpWindow = "CLOSE_ITEM_MASTER_SET_UP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseGS1AIWindow = "CLOSE_GS1AI_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseCountryMasterWindow = "CLOSE_COUNTRY_MASTER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseUserChangePasswordWindow = "CLOSE_CHANGE_PASSWORD_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAPReceiptDetailsWindow = "CLOSE_AP_RECEIPT_DETAILS_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDetainCondemnWindow = "CLOSE_DETAIN_CONDEMN_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTouchscreenOrdersWindow = "CLOSE_TOUCHSCREEN_ORDERS_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTouchscreenPartnersWindow = "CLOSE_TOUCHSCREEN_PARTNERS_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseHiddenWindows = "CLOSE_HIDDEN_WINDOWS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string GetSortProducts = "GET_SORT_PRODUCTS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string SendSortGroupProducts = "SEND_SORT_GROUP_PRODUCTS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClearSearch = "CLEAR_SEARCH";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string GetSortGroupProducts = "GET_SORT_GROUP_PRODUCTS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string SendSortProducts = "SEND_SORT_PRODUCTS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string GetOrders = "GET_ORDERS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string SendOrders = "SEND_ORDERS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseARDispatchDetailsWindow = "CLOSE_AR_DISPATCH_DETAILS_WINDOW";

        /// <summary>
        /// Token used to inform goods in that the serial data values are to be reset.
        /// </summary>
        public const string ResetLabel = "RESET_LABEL";


        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePriceListMasterWindow = "CLOSE_PRICE_LIST_MASTER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePriceListDetailWindow = "CLOSE_PRICE_LIST_DETAIL_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDateWindow = "CLOSE_DATE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePrinterSelectionWindow = "CLOSE_PRINTER_SELECTION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSearchSaleWindow = "CLOSE_SEARCH_SALE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSearchSpecWindow = "CLOSE_SEARCH_SPEC_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTelesalesSetUpWindow = "CLOSE_TELESALES_SET_UP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTelesalesWindow = "CLOSE_TELESALES_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTelesalesEndCallWindow = "CLOSE_TELESALES_END_CALL_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAlertUserSetUpWindow = "CLOSE_ALERT_USER_SET_UP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePaymentSearchWindow = "CLOSE_PAYMENT_SEARCH_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAccountsPurchases = "CLOSE_ACCOUNTS_PURCHASES";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAccountsReturns = "CLOSE_ACCOUNTS_RETURNS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSyncPartners= "CLOSE_SYNC_PARTNERS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSyncProducts = "CLOSE_SYNC_PRODUCTS";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSyncPrices = "CLOSE_SYNC_PRICES";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ClosePaymentWindow = "CLOSE_PAYMENT_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseInvoiceCreationWindow = "CLOSE_INVOICE_CREATION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAPInvoiceCreationWindow = "CLOSE_APINVOICE_CREATION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseCreditNoteCreationWindow = "CLOSE_CREDIT_NOTE_CREATION_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseAccountsWindow = "CLOSE_ACCOUNTS_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDeviceSettingsWindow = "CLOSE_DEVICESETTINGS_WINDOW";

        public const string CloseMRPWindow = "CLOSE_MRP_WINDOW";

        public const string CloseBatchDataWindow = "CLOSE_BATCH_DATA_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseSpecWindow = "CLOSE_SPEC_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDepartmentsWindow = "CLOSE_DEPARTMENTS_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseReportSetUpWindow = "CLOSE_REPORT_SET_UP_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseReportSearchWindow = "CLOSE_REPORT_SEARCH_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string Macro = "MACRO";
        public const string BatchAttributes = "BATCH_ATTRIBUTES";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ShowGridProduct = "SHOW_GRID_PRODUCT";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string SearchMultiSelect = "SEARCH_MULTI_SELECT";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseStockAdjustWindow = "CLOSE_STOCK_ADJUST_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseLairageWindow = "CLOSE_LAIRAGE_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseTraceabilityMasterWindow = "CLOSE_TRACEABILITY_MASTER_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseDateTemplateNameWindow = "CLOSE_DATE_TEMPLATE_NAME_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string CloseUserGroupAuthorisationWindow = "CLOSE_USER_GROUP_AUTHORISATION_WINDOW";

        /// <summary>
        /// Token used to inform a data update view to set focus to it's first editable control. 
        /// </summary>
        public const string FocusToDataUpdate = "FOCUS_TO_DATA_UPDATE";

        /// <summary>
        /// Token used to inform a data update view to set focus to it's password box control. 
        /// </summary>
        public const string FocusToPasswordBox = "FOCUS_TO_PASSWORD_BOX";

        /// <summary>
        /// Token used to inform the message box to open. 
        /// </summary>
        public const string OpenMessageBox = "OPEN_MESSAGEBOX";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string PalletiseItems = "PALLETISE_ITEMS";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string ProductionSelected = "PRODUCTION_SELECTED";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToSelectedControl = "FOCUS_TO_SELECTED_CONTROL";

        public const string ShowScannerMoveButton = "SHOW_SCANNER_MOVE_BUTTON";

        /// <summary>
        /// Token used to inform the app.xaml.cs to create the scanner factory.
        /// </summary>
        public const string CreateScannerFactorySequencer = "CREATE_SCANNER_FACTORY_SEQUENCER";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToEartag = "FOCUS_TO_EARTAG";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string CollapseGroups = "COLLAPSE_GROUPS";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToAttributeButton = "FOCUS_TO_ATTRIBUTE_BUTTON";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToButton = "FOCUS_TO_BUTTON";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToHerdNo = "FOCUS_TO_HERD_NO";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string DisableSequencerHerdNo = "DISABLE_SEQUENCER_HERD_NO";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string DisableLegacyAttributesView = "DISABLE_LEGACY_ATTRIBUTES_VIEW";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToSelectedPartnerControl = "FOCUS_TO_SELECTED_PARTNER_CONTROL";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToSelectedInventoryControl = "FOCUS_TO_SELECTED_INVENTORY_CONTROL";

        /// <summary>
        /// Token used to inform a ui to set focus to it's selected control. 
        /// </summary>
        public const string FocusToGrid = "FOCUS_TO_GRID";

        /// <summary>
        /// Token used to create the partner search scren.
        /// </summary>
        public const string SearchForBusinessPartner = "SEARCH_FOR_PARTNER";

        /// <summary>
        /// Token used to send the search term from the partner search view, to it's viewmodel.
        /// </summary>
        public const string SearchTerm = "SEARCH_TERM";

        /// <summary>
        /// Token used to send the user password to it's user master view model.
        /// </summary>
        public const string UserPassword = "USER_PASSWORD";

        /// <summary>
        /// Token used to send the user password to it's user master view model.
        /// </summary>
        public const string DBPassword = "DB_PASSWORD";

        /// <summary>
        /// Token used to send the user password confirmation to it's user master view model.
        /// </summary>
        public const string UserPasswordConfirm = "USER_PASSWORD_CONFIRM";

        /// <summary>
        /// Token used to send the user set up ui to reset the confirm password.
        /// </summary>
        public const string ResetConfirmationPassword = "RESET_CONFIRMATION_PASSWORD";

        /// <summary>
        /// Token used to send the user set up ui to reset the passwords.
        /// </summary>
        public const string ResetPasswords = "RESET_PASSWORDS";

        /// <summary>
        /// Token used to send the user set up ui to reset the passwords.
        /// </summary>
        public const string ResetProductionOrderBatchData = "RESET_PRODUCTION_ORDER_BATCH_DATA";
       
        /// <summary>
        /// Token used to send tell all subscribers to change their control button to that of the master ribbon selection.
        /// </summary>
        public const string SetControl = "SET_CONTROL";

        /// <summary>
        /// Token used to send tell all subscribers to handle a ribbon navigation command..
        /// </summary>
        public const string NavigationCommand = "NAVIGATION_COMMAND";

        /// <summary>
        /// Token used to send tell all subscribers to handle a ribbon navigation report command..
        /// </summary>
        public const string ReportCommand = "REPORT_COMMAND";

        public const string EnableSalesSearchScreen = "ENABLE_SALES_SEARCH_SCREEN";

        /// <summary>
        /// Token used to send tell all subscribers that the weight mode has changed (auto/manual).
        /// </summary>
        public const string WeightModeChanged = "WEIGHT_MODE_CHANGED";

        /// <summary>
        /// Token used to send tell all subscribers to handle a control mode switch.
        /// </summary>
        public const string SwitchControl = "SWITCH_CONTROL";

        /// <summary>
        /// Token used as part of the open windows tracking.
        /// </summary>
        public const string WindowStatus = "WINDOW_STATUS";

        /// <summary>
        /// Token used to inform the login viewmodel to set it's user logged in property.
        /// </summary>
        public const string SetUserLoggedIn = "SET_USER_LOGGED_IN";

        /// <summary>
        /// Token used to inform the licence admin viewmodel to refresh it's licencing totals.
        /// </summary>
        public const string RefreshLicencingTotals = "REFRESH_LICENCING_TOTALS";

        /// <summary>
        /// Token used to inform the date template allocation viewmoel that the date master names have been updated.
        /// </summary>
        public const string DateTemplateNamesUpdated = "DATE_TEMPLATE_NAMES_UPDATED";

        /// <summary>
        /// Token used to inform the quality template allocation viewmoel that the date master names have been updated.
        /// </summary>
        public const string QualityTemplateNamesUpdated = "QUALITY_TEMPLATE_NAMES_UPDATED";

        /// <summary>
        /// Token used to inform the traceability template allocation viewmoel that the traceability master names have been updated.
        /// </summary>
        public const string TraceabilityTemplateNamesUpdated = "TRACEABILITY_TEMPLATE_NAMES_UPDATED";

        /// <summary>
        /// Token used to inform the traceability template allocation viewmoel that the traceability master names have been updated.
        /// </summary>
        public const string WeightBandGroupUpdated = "WEIGHT_BAND_GROUP_UPDATED";

        /// <summary>
        /// Token used to inform the traceability template allocation viewmoel that the traceability master names have been updated.
        /// </summary>
        public const string TraceabilityTemplateGroupsUpdated = "TRACEABILITY_TEMPLATE_GROUPS_UPDATED";

        /// <summary>
        /// Token used to inform the product selection vm that a selection has changed.
        /// </summary>
        public const string TurnDefineNewGroupOff = "TURN_DEFINE_NEW_GROUP_OFF";

         /// <summary>
        /// Token used to inform the product selection vm that a selection has changed.
        /// </summary>
        public const string UpdateProductSelections = "UPDATE_PRODUCT_SELECTIONS";

        /// <summary>
        /// Token used to inform the product selection vm that a selection has changed.
        /// </summary>
        public const string UpdateModules = "UPDATE_MODULES";

        /// <summary>
        /// Token used to inform the product selection vm that a selection has changed.
        /// </summary>
        public const string UpdateWeights = "UPDATE_WEIGHTS";

        /// <summary>
        /// Token used to inform the price details vm that a new price master has been selected.
        /// </summary>
        public const string RefreshPriceDetails = "REFRESH_PRICE_DETAILS";

        /// <summary>
        /// Token used to inform the inventory master to refresh it's collections.
        /// </summary>
        public const string RefreshInventory = "REFRESH_INVENTORY";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayKeyboard = "DISPLAY_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayWorkflowKeyboard = "DISPLAY_WORKFLOW_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayOrdersKeyboard = "DISPLAY_ORDERS_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayLairageKeyboard = "DISPLAY_LAIRAGE_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayOrderKeyboard = "DISPLAY_ORDER_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayCollectionSearchKeyboard = "DISPLAY_COLLECTION_SEARCH_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayNotesKeyboard = "DISPLAY_NOTES_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string MultiReport = "MULTI_REPORT";

        public const string MultiReportTouchscreen = "MULTI_REPORT_TOUCHSCREEN";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayNotesContainerKeyboard = "DISPLAY_NOTES_CONTAINER_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to display it's keyboard.
        /// </summary>
        public const string DisplayMainUIKeyboard = "DISPLAY_MAIN_UI_KEYBOARD";

        /// <summary>
        /// Token used to inform the recipient to filter the suppliers.
        /// </summary>
        public const string FilterSuppliers = "FILTER_SUPPLIERS";

        /// <summary>
        /// Token used to inform the recipient to filter the transactions.
        /// </summary>
        public const string FilterTransactions = "FILTER_TRANSACTIONS";

        /// <summary>
        /// Token used to inform the recipient to filter the products.
        /// </summary>
        public const string FilterProducts = "FILTER_PRODUCTS";

        /// <summary>
        /// Token used to inform the recipient to display it's calender.
        /// </summary>
        public const string DisplayCalender = "DISPLAY_CALENDER";

        /// <summary>
        /// Token used to inform the recipient that the calender is closed.
        /// </summary>
        public const string CalenderClosed = "CALENDER_CLOSED";

         /// <summary>
        /// Token used to inform the recipient to display a confirmation message.
        /// </summary>
        public const string DisplayConfirmationBox = "DISPLAY_CONFIRMATION_BOX";

        /// <summary>
        /// Token used to inform the recipient to display an error message.
        /// </summary>
        public const string DisplayErrorBox = "DISPLAY_ERROR_BOX";

        /// <summary>
        /// Token used to inform the recipient to scroll.
        /// </summary>
        public const string EposScroll = "EPOS_SCROLL";

        /// <summary>
        /// Token used to inform the recipient to scroll.
        /// </summary>
        public const string EposPartnerScroll = "EPOS_PARTNER_SCROLL";

        /// <summary>
        /// Token used to inform the recipient that an epos touchpad selection has been made.
        /// </summary>
        public const string SetEposSelection = "SET_EPOS_SELECTION";

        /// <summary>
        /// Token used to inform the recipient to reset the touchpad value.
        /// </summary>
        public const string ResetTouchPad = "RESET_TOUCHPAD";

        /// <summary>
        /// Token used to inform the recipient to move focused control.
        /// </summary>
        public const string MovePasswordFocus = "MOVE_PASSWORD_FOCUS";

        /// <summary>
        /// Token used to inform the recipient that the password has changed.
        /// </summary>
        public const string PasswordChangeConfirmation = "PASSWORD_CHANGE_CONFIRMATION";

        /// <summary>
        /// Token used to inform the recipient that the password has changed.
        /// </summary>
        public const string PasswordRemembered = "PASSWORD_REMEMBERED";

        /// <summary>
        /// Token used to inform the recipient that the password has changed.
        /// </summary>
        public const string DBStoredPassword = "DB_STORED_REMEMBERED";

        /// <summary>
        /// Token used to inform the recipient that the audit data has changed.
        /// </summary>
        public const string AuditData = "AUDIT_DATA";

        /// <summary>
        /// Token used to inform the recipient that the audit data has been received..
        /// </summary>
        public const string AuditDataReceived = "AUDIT_DATA_RECEIVED";

        /// <summary>
        /// Token used to inform the recipient that the grid xml must be stored.
        /// </summary>
        public const string SaveGridXml = "SAVE_GRID_XML";

        /// <summary>
        /// Token used to inform the recipient search combo box to display it's drop down list.
        /// </summary>
        public const string ShowPopUp = "SHOW_POP_UP";

        /// <summary>
        /// Token used to inform the recipient search combo box to display it's drop down list.
        /// </summary>
        public const string GraderPanelLabel = "GRADER_PANEL_LABEL";

        /// <summary>
        /// Token used to inform the recipient search combo box to display it's drop down list.
        /// </summary>
        public const string ShowProducts = "SHOW_PRODUCTS";

        /// <summary>
        /// Token used to inform the recipient sale order to update it's totals.
        /// </summary>
        public const string UpdateSaleOrder = "UPDATE_SALE_ORDER";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string SearchSaleSelected = "SEARCH_SALE_SELECTED";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string AttributeSearchSaleSelected = "ATTRIBUTE_SEARCH_SALE_SELECTED";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string PaymentProposalSelected = "PAYMENT_PROPOSAL_SELECTED";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected for a report.
        /// </summary>
        public const string SearchSaleSelectedForReport = "SEARCH_SALE_SELECTED_FOR_REPORT";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected for a report.
        /// </summary>
        public const string SearchSpecSelected = "SEARCH_SPEC_SELECTED";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected for a report.
        /// </summary>
        public const string RecipeSelected = "RECIPE_SELECTED";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected for a report.
        /// </summary>
        public const string SearchSaleSelectedForMultiSelectReport = "SEARCH_SALE_SELECTED_FOR_MULTI_SELECT_REPORT";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected for a report.
        /// </summary>
        public const string SearchSaleSelectedForTouchscreenReport = "SEARCH_SALE_SELECTED_FOR_TOUCHSCREEN_REPORT";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string SearchForSale = "SEARCH_FOR_SALE";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string SearchDateSelected = "SEARCH_DATE_SELECTED";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string SearchForSaleMultiSelect = "SEARCH_FOR_SALE_MULTI_SELECT";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string SearchForReportSale = "SEARCH_FOR_REPORT_SALE";

        /// <summary>
        /// Token used to inform the recipient that a search sale has been selected.
        /// </summary>
        public const string SearchForTouchscreenReportSale = "SEARCH_FOR_TOUCHSCREEN_REPORT_SALE";

        /// <summary>
        /// Token used to inform the recipient that a search production sale has been selected.
        /// </summary>
        public const string SearchForProductionSale = "SEARCH_FOR_PRODUCTION__SALE";

        /// <summary>
        /// Token used to inform the recipient that a search production sale has been selected.
        /// </summary>
        public const string SearchForTouchscreenProductionSale = "SEARCH_FOR_TOUCHSCREEN_PRODUCTION__SALE";

        /// <summary>
        /// Token used to inform the recipient that a search transactionsale has been selected.
        /// </summary>
        public const string SearchForTransactionSale = "SEARCH_FOR_TRANSACTION_SALE";

        /// <summary>
        /// Token used to inform the recipient that a search transactionsale has been selected.
        /// </summary>
        public const string SearchForCarcassSale = "SEARCH_FOR_CARCASS_SALE";

        /// <summary>
        /// Token used to inform the recipient that a search transactionsale has been selected.
        /// </summary>
        public const string SearchForTouchscreenTransactionSale = "SEARCH_FOR_TOUCHSCREEN_TRANSACTION_SALE";

        /// <summary>
        /// Token used to inform the recipient search screen to display the selected customers sales.
        /// </summary>
        public const string DisplayCustomerSales = " DISPLAY_CUSTOMER_SALES";

        /// <summary>
        /// Token used to inform the recipient search screen to display the selected customers sales.
        /// </summary>
        public const string DisplayReportData = " DISPLAY_REPORT_DATA";

        /// <summary>
        /// Token used to inform the recipient screen to display the selected supplier sales.
        /// </summary>
        public const string DisplaySupplierSales = " DISPLAY_SUPPLIER_SALES";

        /// <summary>
        /// Token used to inform the recipient screen to display the selected supplier sales.
        /// </summary>
        public const string DisplayPartnerSales = " DISPLAY_PARTNER_SALES";

         /// <summary>
        /// Token used to inform the recipient search screen to display the selected business partner.
        /// </summary>
        public const string BusinessPartnerSelected = "BUSINESS_PARTNER_SELECTED";

        /// <summary>
        /// Token used to inform the recipient search screen to display the selected special price.
        /// </summary>
        public const string SpecialPriceSelected = "SPECIAL_PRICE_SELECTED";

        /// <summary>
        /// Token used to inform the recipient search screen to display the selected special price.
        /// </summary>
        public const string SetSpecialPrice = "SET_SPECIAL_PRICE_SELECTED";

        /// <summary>
        /// Token used to inform the recipient vm that a recent order item(s) has been selected.
        /// </summary>
        public const string RecentOrderItemSelected = "RECENT_ORDER_ITEM_SELECTED";

        /// <summary>
        /// Token used to inform the app.xaml.cs to open a window.
        /// </summary>
        public const string LabelDeleted = "LABEL_DELETED";

         /// <summary>
        /// Token used to inform the recipient vm that a manual recent order item has been selected.
        /// </summary>
        public const string AddManualRecentOrderItem = "ADD_MANUAL_RECENT_ORDER";

        /// <summary>
        /// Token used to inform the recipient vm that a manual recent order item has been selected.
        /// </summary>
        public const string CollectionScreenMoveBack = "COLLECTION_SCREEN_MOVE_BACK";

        /// <summary>
        /// Token used to inform the recipient vm that a manual recent order attempt has resulted in an error.
        /// </summary>
        public const string RecentOrderError = "RECENT_ORDER_ERROR";

        /// <summary>
        /// Token used to inform the recipient vm of what the current products order method is.
        /// </summary>
        public const string ParseOrderMethod = "PARSE_ORDER_METHOD";
        
        /// <summary>
        /// Token used to inform the recipient vm to set focus to it's recent orders grid.
        /// </summary>
        public const string SetRecentOrdersFocus = "SET_RECENT_ORDERS_FOCUS";

        /// <summary>
        /// Token used to inform the recipient vm to highlight the selected inventory item on the price list.
        /// </summary>
        public const string HightlightInventoryItem = "HIGHTLIGHT_INVENTORY_ITEM";

        /// <summary>
        /// Token used to inform the recipient vm to display the incoming price list.
        /// </summary>
        public const string DisplayPriceList = "DISPLAY_PRICE_LIST";

        /// <summary>
        /// Token used to inform the recipient vm to display the incoming inventory group.
        /// </summary>
        public const string DisplayInventoryGroup = "DISPLAY_INVENTORY_GROUP";

        /// <summary>
        /// Token used to inform the recipient to refresh it's groups.
        /// </summary>
        public const string RefreshGroups = "REFRESH_GROUPS";

        /// <summary>
        /// Token used to inform the recipient to refresh it's details.
        /// </summary>
        public const string RefreshGoodsinDetails = "REFRESH_GOODS_IN_DETAILS";

        /// <summary>
        /// Token used to inform the recipient to refresh it's orders.
        /// </summary>
        public const string RefreshOrders = "REFRESH_ORDERS";

        /// <summary>
        /// Token used to inform the recipient to refresh it's business partners.
        /// </summary>
        public const string RefreshPartners = "REFRESH_PARTNERS";

        /// <summary>
        /// Token used to inform the recipient to refresh it's products.
        /// </summary>
        public const string RefreshProducts = "REFRESH_PRODUCTS";

        /// <summary>
        /// Token used to inform the recipient to refresh it's products.
        /// </summary>
        public const string UpdateBatchNo = "UPDATE_BATCH_NO";

        /// <summary>
        /// Token used to inform the recipient to refresh it's products.
        /// </summary>
        public const string EmptyRequiredAttribute = "EMPTY_REQUIRED_ATTRIBUTE";

        /// <summary>
        /// Token used to inform the recipient to refresh it's products.
        /// </summary>
        public const string UpdateShowAttributes = "UPDATE_SHOW_ATTRIBUTES";

         /// <summary>
        /// Token used to inform the recipient to refresh it's groups.
        /// </summary>
        public const string UpdateUserGroups = "UPDATE_USER_GROUPS";

        /// <summary>
        /// Token used to inform the recipient to refresh it's currencies.
        /// </summary>
        public const string UpdateCurrencies = "UPDATE_CURRENCIES";

        /// <summary>
        /// Token used to inform the recipient to refresh it's types.
        /// </summary>
        public const string UpdateTypes = "UPDATE_TYPES";

        /// <summary>
        /// Token used to inform the recipient that a user has been selected.
        /// </summary>
        public const string UserSelected = "USER_SELECTED";

        /// <summary>
        /// Token used to inform the recipient to drill down.
        /// </summary>
        public const string DrillDown = "DRILL_DOWN";

        /// <summary>
        /// Token used to inform the recipient to drill down.
        /// </summary>
        public const string DrillDownToSpec = "DRILL_DOWN_TO_SPEC";

        /// <summary>
        /// Token used to send a sql script.
        /// </summary>
        public const string SqlScript = "SQL_SCRIPT";

        /// <summary>
        /// Token used to set a sql script.
        /// </summary>
        public const string SetSql = "SET_SQL";

        /// <summary>
        /// Token used to bring the main window to the front..
        /// </summary>
        public const string BringToFront = "BRING_TO_FRONT";

        /// <summary>
        /// Token used to set the data collection..
        /// </summary>
        public const string SetData = "SET_DATA";

        /// <summary>
        /// Token used to set the data collection..
        /// </summary>
        public const string SetDataContext = "SET_DATACONTEXT";

        /// <summary>
        /// Token used to send the data collection.
        /// </summary>
        public const string DataGenerated = "DATA_GENERATED";

        /// <summary>
        /// Token used to show the sql window.
        /// </summary>
        public const string ShowSqlWindow = "SHOW_SQL_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowColWindow = "SHOW_COL_WINDOW";

        /// <summary>
        /// Token used to inform the recepient window to close.
        /// </summary>
        public const string ShowDesktopNotesWindow = "SHOW_DESKTOP_NOTES_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowReportNotesWindow = "SHOW_REPORT_NOTES_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowBatchEditNotesWindow = "SHOW_BATCH_EDIT_NOTES_WINDOW";

        public const string ShowSalesNotesWindow = "SHOW_SALES_NOTES_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowPaymentNotesWindow = "SHOW_PAYMENT_NOTES_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowNotesWindow = "SHOW_NOTES_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowScannerNotesWindow = "SHOW_SCANNER_NOTES_WINDOW";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string ShowDetainCondemn = "SHOW_DETAIN_CONDEMN";

        /// <summary>
        /// Token used to show the col window.
        /// </summary>
        public const string DetainCondemnSelected = "DETAIN_CONDEMN_SELECTED";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic traceability controls.
        /// </summary>
        public const string CreateDynamicControls = "CREATE_DYNAMIC_CONTROLS";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic traceability controls for serial dispatch.
        /// </summary>
        public const string CreateDynamicControlsForSerial = "CREATE_DYNAMIC_CONTROLS_FOR_SERIAL";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic traceability controls.
        /// </summary>
        public const string CreateTouchscreenDynamicControls = "CREATE_TOUCHSCREEN_DYNAMIC_CONTROLS";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic serial traceability columns.
        /// </summary>
        public const string CreateDynamicSerialColumns = "CREATE_DYNAMIC_SERIAL_COLUMNS";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic traceability controls.
        /// </summary>
        public const string CreateEditDynamicControls = "CREATE_EDIT_DYNAMIC_CONTROLS";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic serial traceability columns for serial dispatch.
        /// </summary>
        public const string CreateDynamicSerialColumnsForSerial = "CREATE_DYNAMIC_SERIAL_COLUMNS_FOR_SERIAL";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic serial traceability columns.
        /// </summary>
        public const string CreateTouchscreenDynamicSerialColumns = "CREATE_TOUCHSCREEN_DYNAMIC_SERIAL_COLUMNS";

        /// <summary>
        /// Token used to inform the recipient  to create dynamic serial traceability controls.
        /// </summary>
        public const string CreateEditDynamicSerialControls = "CREATE_EDIT_DYNAMIC_SERIAL_CONTROLS";

        /// <summary>
        /// Token used to inform the recipient touchscreen traceability view to clear it's data..
        /// </summary>
        public const string ClearData = "CLEAR_DATA";

        /// <summary>
        /// Token used to inform the recipient touchscreen traceability view to clear it's data..
        /// </summary>
        public const string ClearDetainCondemnData = "CLEAR_DETAIN_CONDEMN_DATA";

        /// <summary>
        /// Token used to inform the recipient touchscreen traceability view to clear it's data..
        /// </summary>
        public const string GetCarcassSales = "GET_CARCASS_SALES";

        /// <summary>
        /// Token used to inform the recipient touchscreen traceability view to clear it's data..
        /// </summary>
        public const string SendCarcassSales = "SEND_CARCASS_SALES";

        /// <summary>
        /// Token used to inform the recipient touchscreen traceability view to clear it's data..
        /// </summary>
        public const string ClearAndResetData = "CLEAR_AND_RESET_DATA";

        /// <summary>
        /// Token used to inform the recipient touchscreen traceability view to clear it's data..
        /// </summary>
        public const string RefreshDispatchContainers = "REFRESH_DISDATCH_CONTINERS";

        /// <summary>
        /// Token used to set a sale detail.
        /// </summary>
        public const string SaleDetailSelected = "SALE_DETAIL_SELECTED";

        /// <summary>
        /// Token used to set a sale detail.
        /// </summary>
        public const string TransactionsEditingComplete = "TRANSACTIONS_EDITING_COMPLETE";

        /// <summary>
        /// Token used to open the sql generator window.
        /// </summary>
        public const string DisplaySql = "DISPLAY_SQL";

        /// <summary>
        /// Token used to open the sql generator window.
        /// </summary>
        public const string ShowReceipe = "SHOW_RECEIPE";

        /// <summary>
        /// Token used to open the sql generator window.
        /// </summary>
        public const string ShowStockTake = "SHOW_STOCK_TAKE";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string DisplayCol = "DISPLAY_COL";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string DisplaySaleSearchScreen = "DISPLAY_SALE_SEARCH_SCREEN";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string AddTare = "ADD_TARE";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string GraderEditMode = "GRADER_EDIT_MODE";

        /// <summary>
        /// Token used to open the col generator window.
        /// </summary>
        public const string TouchscreenKeyPadEntry = "TOUCHSCREEN_KEYPAD_ENTRY";

        /// <summary>
        /// Token used to inform the goods receipt vm to update it's item qty received.
        /// </summary>
        public const string UpdateGoodsInQtyReceived = "UPDATE_GOODS_IN_QTY_RECEIVED";

        /// <summary>
        /// Token used to send the batch traceability ui values.
        /// </summary>
        public const string BatchTraceabilityValues = "BATCH_TRACEABILITY_VALUES";

        /// <summary>
        /// Token used to send the batch traceability dispatch ui values.
        /// </summary>
        public const string BatchTraceabilityDispatchValues = "BATCH_TRACEABILITY_DISPATCH_VALUES";

        /// <summary>
        /// Token used to refresh the invoices at accounts.
        /// </summary>
        public const string RefreshInvoices = "REFRESH_INVOICES";

        /// <summary>
        /// Token used to refresh the invoices at accounts.
        /// </summary>
        public const string UpdatePartners = "UPDATE_PARTNERS";

        /// <summary>
        /// Token used to refresh the invoices at accounts.
        /// </summary>
        public const string UpdateUsers = "UPDATE_USERS";

        /// <summary>
        /// Token used to refresh the invoices at accounts.
        /// </summary>
        public const string CreateScannerBatchSetUp = "CREATE_SCANNER_BATCH_SET_UP";

        /// <summary>
        /// Token used to inform the ui to deselect its selected product..
        /// </summary>
        public const string DeselectProduct = "DESELECT_PRODUCT";

        /// <summary>
        /// Token used to send the traceability ui values.
        /// </summary>
        public const string TraceabilityValues = "TRACEABILITY_VALUES";

        /// <summary>
        /// Token used to send the traceability edit ui values.
        /// </summary>
        public const string TraceabilityEditValues = "TRACEABILITY_EDIT_VALUES";

        /// <summary>
        /// Token used to send the transaction edit ui values.
        /// </summary>
        public const string TransactionEditValues = "TRANSACTION_EDIT_VALUES";

         /// <summary>
        /// Token used to display the keypad.
        /// </summary>
        public const string DisplayKeypad = "DISPLAY_KEYPAD";

        /// <summary>
        /// Token used to inform the recepient of a label selection change.
        /// </summary>
        public const string LabelChange = "LABEL_CHANGE";

        /// <summary>
        /// Token used to inform the recepient of a label selection change.
        /// </summary>
        public const string LabelSearch = "LABEL_SEARCH";

        /// <summary>
        /// Token used to inform the recepient of a label selection change.
        /// </summary>
        public const string BatchAndProductMismatchReason = "BATCH_AND_PRODUCT_MISMATCH_REASON";

        /// <summary>
        /// Token used to inform the recepient of a label selection change.
        /// </summary>
        public const string AuthorisationResult = "AUTHORISATION_RESULT";

        /// <summary>
        /// Token used to inform the recepient of a label selection change, so update display name.
        /// </summary>
        public const string ChangeDisplayLabelName = "CHANGE_DISPLAY_LABEL_NAME";
        /// <summary>
        /// Token used to pass the touchscreen selected supplier.
        /// </summary>
        public const string SupplierSelected = "SUPPLIER_SELECTED";

        /// <summary>
        /// Token used to pass the touchscreen selected product.
        /// </summary>
        public const string ProductSelected = "PRODUCT_SELECTED";

        /// <summary>
        /// Token used to pass the touchscreen selected product pieces.
        /// </summary>
        public const string ProductPieces = "PRODUCT_PIECES";

        /// <summary>
        /// Token used to pass the touchscreen selected container.
        /// </summary>
        public const string ContainerSelected = "CONTAINER_SELECTED";

         /// <summary>
        /// Token used to navigate to the tare container view.
        /// </summary>
        public const string NavigateToTareContainer = "NAVIGATE_TO_TARE_CONTAINER";

        /// <summary>
        /// Token used to inform the goods receipt view to send it's traceability data.
        /// </summary>
        public const string GetTraceabilityData = "GET_TRACEABILITY_DATA";

        /// <summary>
        /// Token used to inform the dispatch details view to send it's traceability data.
        /// </summary>
        public const string GetDispatchTraceabilityData = "GET_DISPATCH_TRACEABILITY_DATA";

        /// <summary>
        /// Token used to inform the goods receipt vm to send it's edited traceability data.
        /// </summary>
        public const string GetTraceabilityDataForEdit = "GET_TRACEABILITY_DATA_FOR_EDIT";

        /// <summary>
        /// Token used to inform the goods receipt vm to send it's traceability data.
        /// </summary>
        public const string GetTouchscreenTraceabilityData = "GET_TOUCHSCREEN_TRACEABILITY_DATA";

        /// <summary>
        /// Token used to inform the goods receipt edit vm to send it's traceability data.
        /// </summary>
        public const string GetEditTraceabilityData = "GET_EDIT_TRACEABILITY_DATA";

        /// <summary>
        /// Token used to scroll the suppliers touchscreen grid.
        /// </summary>
        public const string ScrollSuppliers = "SCROLL_SUPPLIERS";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string AddNewRow = "ADD_NEW_ROW";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string NewAlerts = "NEW_ALERTS";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string ReprintLabel = "REPRINT_LABEL";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string CalculatePayment = "CALCULATE_PAYMENT";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string AddMenuToShortcut = "ADD_MENU_TO_SHORTCUT";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string RequestReportRows = "REQUEST_REPORT_ROWS";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string SendingReportRows = "SENDING_REPORT_ROWS";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string RequestLairages = "REQUEST_LAIRAGES";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string SendingLairages = "SENDING_LAIRAGES";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string ShortcutMenuSelected = "SHORTCUT_MENU_SELECTED";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string DisplaySearchKeyboard = "DISPLAY_SEARCH_KEYBOARD";

        /// <summary>
        /// Token used to inform any observers to add a new row to their grid.
        /// </summary>
        public const string DisplayTouchscreenReportViewer = "DISPLAY_TOUCHSCREEN_REPORT_VIEWER";

        /// <summary>
        /// Token used to attach a report to selected email account.
        /// </summary>
        public const string AttachReportToEmail = "ATTACH_REPORT_TO_EMAIL";

        /// <summary>
        /// Token used to attach a report to selected email account.
        /// </summary>
        public const string AttachReportsToEmail = "ATTACH_REPORTS_TO_EMAIL";

        /// <summary>
        /// Token used to set the selected container..
        /// </summary>
        public const string SetSelectedContainer = "SET_SELECTED_CONTAINER";

        /// <summary>
        /// Token used to scroll the orders touchscreen grid.
        /// </summary>
        public const string ScrollOrders = "SCROLL_ORDERS";

        /// <summary>
        /// Token used to scroll the orders touchscreen grid.
        /// </summary>
        public const string SortModeChange = "SORT_MODE_CHANGE";

        /// <summary>
        /// Token used to scroll the products touchscreen grid.
        /// </summary>
        public const string ScrollProducts = "SCROLL_PRODUCTS";

        /// <summary>
        /// Token used to inform the oop screen to remove the partner from label.
        /// </summary>
        public const string RemovePartner = "REMOVE_PARTNER";

        /// <summary>
        /// Token used to inform the ap receipt touchscreen to create a new order.
        /// </summary>
        public const string CreateOrder = "CREATE_ORDER";

        /// <summary>
        /// Token used to inform the any observers of the indicator weight.
        /// </summary>
        public const string IndicatorWeight = "INDICATOR_WEIGHT";

        /// <summary>
        /// Token used to inform the any observers of the tare weight.
        /// </summary>
        public const string TareSet = "TARE_SET";

        /// <summary>
        /// Token used to inform the any observers of the tare weight.
        /// </summary>
        public const string TaresSet = "TARES_SET";

         /// <summary>
        /// Token used to inform the any observers of the touchscreen complete order action.
        /// </summary>
        public const string TouchscreenCompleteOrder = "TOUCHSCREEN_COMPLETE_ORDER";

        /// <summary>
        /// Token used to inform the any observers of the touchscreen ui load completion.
        /// </summary>
        public const string TouchscreenUiLoaded = "TOUCHSCREEN_UI_LOADED";

        /// <summary>
        /// Token used to inform the any observers of the touchscreen ui load completion.
        /// </summary>
        public const string TouchscreenMode = "TOUCHSCREEN_MODE";

        /// <summary>
        /// Token used to inform the ardispatch touchscreen to highlight its selected product(if any) on ui load.
        /// </summary>
        public const string HighlightSelectedProduct = "HIGHLIGHT_SELECTED_PRODUCT";

        /// <summary>
        /// Token used to inform the any observers of the touchscreen new batch action.
        /// </summary>
        public const string TouchscreenNewBatch = "TOUCHSCREEN_NEW_BATCH";

        /// <summary>
        /// Token used to inform the any observers of the touchscreen weight recording action.
        /// </summary>
       // public const string TouchscreenRecordWeight = "TOUCHSCREEN_RECORD_WEIGHT";

        /// <summary>
        /// Token used to inform the any observers of the touchscreen qty recording action.
        /// </summary>
        public const string TouchscreenRecordQuantity = "TOUCHSCREEN_RECORD_QUANTITY";

        /// <summary>
        /// Token used to send a touchscreen selected sale to the main vm.
        /// </summary>
        public const string SaleSelected = "SALE_SELECTED";

        /// <summary>
        /// Token used to clear the dispatch details batch controls.
        /// </summary>
        public const string ClearBatchControls = "CLEAR_BATCH_CONTROLS";

        /// <summary>
        /// Token used to inform goods in that a new batch number has been generated.
        /// </summary>
        public const string NewBatchNumberGenerated = "NEW_BATCH_NUMBER_GENERATED";

        /// <summary>
        /// Token used to inform goods in that a new batch number has been generated.
        /// </summary>
        public const string NewTouchscreenBatchNumberGenerated = "NEW_TOUCHSCREEN_BATCH_NUMBER_GENERATED";

        /// <summary>
        /// Token used to inform goods in that the serial data values are to be reset.
        /// </summary>
        public const string ResetSerialData = "RESET_SERIAL_DATA";

        /// <summary>
        /// Token used to inform ap goods in details to switch batch controls.
        /// </summary>
        public const string SwitchBatchControls = "SWITCH_BATCH_CONTROLS";

        /// <summary>
        /// Token used to pass the batch number.
        /// </summary>
        public const string BatchNumber = "BATCH_NUMBER";

        /// <summary>
        /// Token used to pass the batch number to the touchscreen.
        /// </summary>
        public const string BatchNumberSelected = "BATCH_NUMBER_SELECTED";

        /// <summary>
        /// Token used to pass the batch number to the touchscreen.
        /// </summary>
        public const string BatchNumberChanged= "BATCH_NUMBER_CHANGED";

        /// <summary>
        /// Token used to pass the batch number to the touchscreen.
        /// </summary>
        public const string DispatchBatchNumberSelected = "DISPATCH_BATCH_NUMBER_SELECTED";

        /// <summary>
        /// Token used to pass the qty selected at the touchscreen.
        /// </summary>
        public const string QuantitySelected = "QUANTITY_SELECTED";

        /// <summary>
        /// Token used to pass the module the search request came from.
        /// </summary>
        public const string SetModuleType = "SET_MODULE_TYPE";

        /// <summary>
        /// Token used to pass the keyboard value out.
        /// </summary>
        public const string KeyboardValueEntered = "KEYBOARD_VALUE_ENTERED";

        /// <summary>
        /// Token used to pass the keyboard value out.
        /// </summary>
        public const string KeyboardValueEnteredForBatch = "KEYBOARD_VALUE_ENTERED_FOR_BATCH";

        /// <summary>
        /// Token used to pass the keyboard value out.
        /// </summary>
        public const string KeyboardValueEnteredForPalletNo = "KEYBOARD_VALUE_ENTERED_FOR_PALLET_NO";

        /// <summary>
        /// Token used to pass the keyboard value out.
        /// </summary>
        public const string KeyboardValueEnteredForReceiptBatch = "KEYBOARD_VALUE_ENTERED_FOR_RECEIPT_BATCH";

        /// <summary>
        /// Token used to reset the indicator weight and qty to 0.
        /// </summary>
        public const string ResetTouchscreenValues = "RESET_TOUCHSCREEN_VALUES";

        /// <summary>
        /// Token used to inform the recepient to clean up.
        /// </summary>
        public const string CleanUp = "CLEAN_UP";

        /// <summary>
        /// Token used to display the touchscreen label selection window.
        /// </summary>
        public const string DisplayLabelSelection = "DISPLAY_LABEL_SELECTION";

        /// <summary>
        /// Token used to display the touchscreen label selection window.
        /// </summary>
        public const string DisplayGenericSelection = "DISPLAY_GENERIC_SELECTION";

        /// <summary>
        /// Token used to display the touchscreen label selection window.
        /// </summary>
        public const string DisplayGenericScreenSelection = "DISPLAY_GENERIC_SCREEN_SELECTION";

        /// <summary>
        /// Token used to display the touchscreen label selection window.
        /// </summary>
        public const string CreateTouchscreenUsers = "CREATE_TOUCHSCREEN_USERS";

        /// <summary>
        /// Token used to inform the recepient to update it's weight.
        /// </summary>
        public const string UpdateWeight = "UPDATE_WEIGHT";

        /// <summary>
        /// Token used to inform the recepient that a device has been selected.
        /// </summary>
        public const string DeviceSelected = "DEVICE_SELECTED";

        /// <summary>
        /// Token used to inform the recepient that a device has been selected.
        /// </summary>
        public const string SpecSelected = "SPEC_SELECTED";

        /// <summary>
        /// Token used to inform the recepient of the current grid column.
        /// </summary>
        public const string GridColumn = "GRID_COLUMN";

        public const string DisplayBatchData = "DISPLAY_BATCH_DATA";

        /// <summary>
        /// Token used to inform the recepient to display the input warehouse.
        /// </summary>
        public const string DisplayWarehouse = "DISPLAY_WAREHOUSE";

        /// <summary>
        /// Token used to inform the recepient to refresh and send its sales..
        /// </summary>
        public const string RefreshSearchSales = "REFRESH_SEARCH_SALES";

        /// <summary>
        /// Token used to inform the recepient to of a selected production order.
        /// </summary>
        public const string ProductionOrderSelected = "PRODUCTION_ORDER_SELECTED";

        /// <summary>
        /// Token used to inform the recepient to display the input warehouse.
        /// </summary>
        public const string DisplaySelectedWarehouse = "DISPLAY_SELECTED_WAREHOUSE";

        /// <summary>
        /// Token used to inform the touchscreen vm to set it's core vm.
        /// </summary>
        public const string SetTouchscreenViewModel = "SET_TOUCHSCREENVIEWMODEL";

        /// <summary>
        /// Token used to inform the touchscreen vm to set it's core vm.
        /// </summary>
        public const string CheckDates = "CHECK_DATES";

        /// <summary>
        /// Token used to inform the touchscreen vm to set it's core vm.
        /// </summary>
        public const string CheckAttributeValue = "CHECK_ATTRIBUTE_VALUE";

         /// <summary>
        /// Token used to inform the recepient that a value has been selected on the Collection Display screen.
        /// </summary>
        public const string CollectionDisplayValue = "COLLECTION_DISPLAY_VALUE";

        /// <summary>
        /// Token used to inform the recepient that a value has been selected on the Collection Display screen.
        /// </summary>
        public const string EnableEartagEntryOnGrader = "ENABLE_EARTAG_ENTRY_ON_GRADER";

        /// <summary>
        /// Token used to inform the recepient that a into production batch has been created.
        /// </summary>
        public const string ProductionBatchCreated = "PRODUCTION_BATCH_CREATED";

        /// <summary>
        /// Token used to inform the recepient that a into production batch has been created.
        /// </summary>
        public const string ProductionOrderSearch = "PRODUCTION_ORDER_SEARCH";

        /// <summary>
        /// Token used to inform the recepient to display the incoming collection items.
        /// </summary>
        public const string DisplayTouchscreenCollection = "DISPLAY_TOUCHSCREEN_COLLECTION";

        /// <summary>
        /// Token used to inform the recepient to display the incoming collection items.
        /// </summary>
        public const string DisplayTouchscreenCollectionObject = "DISPLAY_TOUCHSCREEN_COLLECTION_OBJECT";

        /// <summary>
        /// Token used to inform the receipient to open its calender.
        /// </summary>
        public const string OpenCalender = "OPEN_CALENDER";

        /// <summary>
        /// Token used to inform the receipient to open its calender.
        /// </summary>
        public const string RecordCarcassSplitWeight = "RECORD_CARCASS_SPLIT_WEIGHT";

        /// <summary>
        /// Token used to inform the receipient to open its calender.
        /// </summary>
        public const string SerialStockSelected = "SERIAL_STOCK_SELECTED";

        /// <summary>
        /// Token used to inform the receipient to open its calender.
        /// </summary>
        public const string CloseCarcassSplitWindow = "CLOSE_CARCASS_SPLIT_WINDOW";

        /// <summary>
        /// Token used to display a grid print preview.
        /// </summary>
        public const string DisplayPrintPreview = "DISPLAY_PRINT_PREVIEW";

        /// <summary>
        /// Token used to display a grid print preview.
        /// </summary>
        public const string PrinterSelected = "PRINTER_SELECTED";

        /// <summary>
        /// Token used to inform the receipient to set it's top most to true..
        /// </summary>
        public const string SetTopMost = "SET_TOP_MOST";

          /// <summary>
        /// Token used to inform the recepient of a manual serial number entry.
        /// </summary>
        public const string ManualSerialNumberEntered = "MANUAL_SERIAL_NUMBER_ENTERED";

        /// <summary>
        /// Token used to send the edited transactions.
        /// </summary>
        public const string ManualTagEnteredAtSequencer = "MANUAL_TAG_ENTERED_AT_SEQUENCER";

        /// <summary>
        /// Token used to send the edited transactions.
        /// </summary>
        public const string TransactionsEdited = "TRANSACTIONS_EDITED";

        /// <summary>
        /// Token used to send the a successful copy flag.
        /// </summary>
        public const string CopySuccessful = "COPY_SUCCESSFUL";

        /// <summary>
        /// Token used to send the scanner data received.
        /// </summary>
        public const string Scanner = "SCANNER";

        /// <summary>
        /// Token used to send the scanner data received.
        /// </summary>
        public const string Wand = "WAND";

        /// <summary>
        /// Token used to send the indicator data received.
        /// </summary>
        public const string Indicator = "INDICATOR";
    }
}

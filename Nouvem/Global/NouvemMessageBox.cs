﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBox.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Global
{
    using System;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.ViewModel;

    /// <summary>
    /// Static handler for the message box.
    /// </summary>
    public static class NouvemMessageBox
    {
        #region private

        /// <summary>
        /// The user selection reference.
        /// </summary>
        private static UserDialogue userSelection;

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the user dialogue selection.
        /// </summary>
        public static UserDialogue UserSelection
        {
            get
            {
                return userSelection;
            }

            set
            {
                userSelection = value;

                // selection made, so close the message box.
                Messenger.Default.Send(Token.Message, Token.CloseMessageBoxWindow);
            }
        }

        /// <summary>
        /// Set, and display the nouvem message box.
        /// </summary>
        /// <param name="displayMessage">The message to display.</param>
        /// <param name="buttonSelection">The buttons configuration settings.</param>
        /// <param name="touchScreen">Is this message box running on a touchscreen flag.</param>
        /// <param name="flashMessage">Is this message box running as a flash message flag.</param>
        /// <param name="scanner">Is this message box running on a scanner flag.</param>
        public static void Show(
            string displayMessage,
            NouvemMessageBoxButtons buttonSelection = NouvemMessageBoxButtons.OK,
            bool touchScreen = false,
            bool flashMessage = false,
            bool scanner = false,
            bool isLeft = false,
            bool showCounter = false,
            bool isPopUp = false)
        {
            // Display the message, set the ui button configuration, and open the message box.
            Messenger.Default.Send(Tuple.Create(displayMessage, buttonSelection, touchScreen, flashMessage, scanner, isLeft, showCounter, isPopUp));
        }

        #endregion
    }
}


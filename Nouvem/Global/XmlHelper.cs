﻿// -----------------------------------------------------------------------
// <copyright file="XmlHelper.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Global
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Logging;

    public static class XmlHelper
    {
        /// <summary>
        /// The application logging reference.
        /// </summary>
        private static readonly Logger log = new Logger();

        /// <summary>
        /// Creates an xml file from the input data.
        /// </summary>
        /// <param name="data">The data to be used.</param>
        /// <returns>An xml document.</returns>
        public static XDocument CreateXml(IDictionary<string, string> data)
        {
            try
            {
                var doc = new XDocument(new XDeclaration("1.0", "utf-8", null),
                    new XElement("Root",
                        data.Select(
                            x =>
                                new XElement(x.Key, x.Value))));

                return doc;
            }
            catch (Exception ex)
            {
                log.LogError(typeof(XmlHelper), ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Creates an xml file from the input data.
        /// </summary>
        /// <param name="data">The data to be used.</param>
        /// <returns>An xml document.</returns>
        public static XDocument CreateXml(IList<Tuple<string, string, string>> data)
        {
            try
            {
                var doc = new XDocument(new XDeclaration("1.0", "utf-8", null),
                    new XElement("Root",
                        data.Select(
                            x =>
                                new XElement(x.Item1, 
                                    new XAttribute("NewValue", x.Item2),
                                    new XAttribute("OldValue", x.Item3)))));

                return doc;
            }
            catch (Exception ex)
            {
                log.LogError(typeof(XmlHelper), ex.Message);
            }
           
            return null;
        }

        /// <summary>
        /// Formats and correctly indents an xml string.
        /// </summary>
        /// <param name="xmlFile">The string to format.</param>
        /// <returns>A formatted xml string.</returns>
        public static string FormatXml(string xmlFile)
        {
            if (string.IsNullOrEmpty(xmlFile))
            {
                return string.Empty;
            }

            var doc = new XmlDocument();
            doc.LoadXml(xmlFile);

            var stringBuilder = new StringBuilder();
            var xmlWriterSettings = new XmlWriterSettings { Indent = true, OmitXmlDeclaration = true };

            doc.Save(XmlWriter.Create(stringBuilder, xmlWriterSettings));

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Determines if a documents attributes contain the value to check.
        /// </summary>
        /// <param name="xml">The xml document.</param>
        /// <param name="valueToCheckFor">The value to check.</param>
        /// <returns>A flag that indicates if a documents attributes contain the value to check.</returns>
        public static bool ParseXml(string xml, string valueToCheckFor)
        {
            var xDoc = XDocument.Parse(xml);

            // check if any attribute value equals the value to check for.
            var result =
                xDoc.Descendants("Root")
                .Elements()
                    .Any(
                        x =>
                            x.Attribute("OldValue").Value.ToLower().Equals(valueToCheckFor.ToLower()) ||
                            x.Attribute("NewValue").Value.ToLower().Equals(valueToCheckFor.ToLower()));

            return result;
        }
    }
}

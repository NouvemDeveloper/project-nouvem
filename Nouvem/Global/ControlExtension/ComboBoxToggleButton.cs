﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace Nouvem.Global.ControlExtension
{
    public class ComboBoxToggleButton : ToggleButton
    {
        // Dependency Property
        public static readonly DependencyProperty InnerBorderBrushProperty =
         DependencyProperty.Register("InnerBorderBrush", typeof(Brush),
             typeof(ComboBoxToggleButton), new FrameworkPropertyMetadata(Brushes.Transparent));

        // .NET Property wrapper
        public Brush InnerBorderBrush
        {
            get
            {
                return (Brush)GetValue(InnerBorderBrushProperty);
            }
            set
            {
                SetValue(InnerBorderBrushProperty, value);
            }
        }
    }
}

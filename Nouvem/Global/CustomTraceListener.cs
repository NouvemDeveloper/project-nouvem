﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Global
{
    public class CustomTraceListener : TraceListener
    {
        // Singleton 
        public static readonly CustomTraceListener Instance =
            new CustomTraceListener();

        public void InitializeTracing(bool ReadDebugOutput)
        {
            if (ReadDebugOutput == true)
                Debug.Listeners.Add(this);
            else
                Trace.Listeners.Add(this);
        }

        public StringBuilder Buffer = new StringBuilder();

        public override void Write(string message)
        {
            // Do something with your messages!
            Buffer.Append(message);
        }

        public override void WriteLine(string message)
        {
            // Do something with your messages!
            Buffer.Append(message);
        }
    }
}

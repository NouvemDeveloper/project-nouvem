﻿// -----------------------------------------------------------------------
// <copyright file="SystemMessageViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Logic
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Shared.Localisation;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;

    /// <summary>
    /// Class which models the system message view.
    /// </summary>
    public class SystemMessageViewModel : NouvemViewModelBase
    {
        #region Fields

        /// <summary>
        /// The information to be displayed in the information/status bar.
        /// </summary>
        private ObservableCollection<SystemMessageViewModel> informationBar =
            new ObservableCollection<SystemMessageViewModel>();

        /// <summary>
        /// The time of the message.
        /// </summary>
        private string messageTime;

        /// <summary>
        /// The app version number.
        /// </summary>
        private string versionNumber;

        /// <summary>
        /// The app version number.
        /// </summary>
        private string versionDeviceNumber;

        /// <summary>
        /// The new alerts flag
        /// </summary>
        private bool newAlerts;

        /// <summary>
        /// The category of the entry.
        /// </summary>
        private MessageType category;

        /// <summary>
        /// The display message.
        /// </summary>
        private string messageToDisplay;

        /// <summary>
        /// the last record field.
        /// </summary>
        private SystemMessageViewModel lastRecord;

        /// <summary>
        /// The progress bar value.
        /// </summary>
        private int progressBarValue;

        /// <summary>
        /// The progress bar minimum value.
        /// </summary>
        private int progressBarMinValue;

        /// <summary>
        /// The progress bar maximum value.
        /// </summary>
        private int progressBarMaxValue;

        /// <summary>
        /// The progress bar error state.
        /// </summary>
        private bool progressBarError;

        /// <summary>
        /// The current time reference.
        /// </summary>
        private string currentTime;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMessageViewModel"/> class.
        /// </summary>
        public SystemMessageViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message

            Messenger.Default.Register<bool>(this, Token.NewAlerts, b => this.NewAlerts = b);

            #endregion

            #region command

            this.SetDatabaseCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.CreateServerSetUp));

            // Handle the creation of the message history view.
            this.ShowMessageHistoryCommand = new RelayCommand(this.ShowMessageHistoryCommandExecute);

            this.OpenAlertsCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(ViewType.WorkflowAlerts);
            },() => this.VersionNumber == Message.NewAlerts);

            #endregion

            this.ProgressBarCompleteMessage = Message.ApplicationLoaded;
            this.VersionNumber = string.Format("{0}:{1}", Strings.Version, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
            this.VersionDeviceNumber = string.Format("{0} | {1}:{2}", this.VersionNumber, Strings.Device, NouvemGlobal.DeviceId);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMessageViewModel"/> class.
        /// </summary>
        /// <param name="category">The category for the entry</param>
        /// <param name="messageTime">The message time.</param>
        /// <param name="message">The message to display.</param>
        public SystemMessageViewModel(DateTime messageTime, MessageType category, string message)
        {
            this.MessageTime = messageTime.ToString("HH:mm:ss");
            this.Category = category;
            this.MessageToDisplay = message;
        }

        #endregion

        #region Public Interface

        #region property

        /// <summary>
        /// Gets or sets the progress bar comlete message.
        /// </summary>
        public string ProgressBarCompleteMessage { get; set; }

        /// <summary>
        /// Gets or sets the current time.
        /// </summary>
        public string CurrentTime
        {
            get
            {
                return this.currentTime;
            }

            set
            {
                this.currentTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether new alerts are available.
        /// </summary>
        public bool NewAlerts
        {
            get
            {
                return this.newAlerts;
            }
            set
            {
                this.newAlerts = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.VersionNumber = Message.NewAlerts;
                }
                else
                {
                    this.VersionNumber = string.Format("Version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
                }
            }
        }

        /// <summary>
        /// Gets the application version number.
        /// </summary>
        public string VersionNumber
        {
            get
            {
                return this.versionNumber;
            }

            set
            {
                this.versionNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the application version number.
        /// </summary>
        public string VersionDeviceNumber
        {
            get
            {
                return this.versionDeviceNumber;
            }

            set
            {
                this.versionDeviceNumber = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the progress bar has completed.
        /// </summary>
        public bool ProgressComplete { get; set; }

        /// <summary>
        /// Gets or sets the progress bar value.
        /// </summary>
        public int ProgressBarValue
        {
            get
            {
                return this.progressBarValue;
            }

            set
            {
                this.progressBarValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the progress bar min value.
        /// </summary>
        public int ProgressBarMinValue
        {
            get
            {
                return this.progressBarMinValue;
            }

            set
            {
                this.progressBarMinValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the progress bar max value.
        /// </summary>
        public int ProgressBarMaxValue
        {
            get
            {
                return this.progressBarMaxValue;
            }

            set
            {
                this.progressBarMaxValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether the the progress bar is in an error state.
        /// </summary>
        public bool ProgressBarError
        {
            get
            {
                return this.progressBarError;
            }

            set
            {
                this.progressBarError = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the last message.
        /// </summary>
        public SystemMessageViewModel LastRecord
        {
            get
            {
                return this.lastRecord;
            }

            set
            {
                this.lastRecord = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the records to be displayed on the information bar.
        /// </summary>
        public ObservableCollection<SystemMessageViewModel> StatusBarInformation
        {
            get
            {
                return this.informationBar;
            }

            set
            {
                if (value != null)
                {
                    this.informationBar = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Retrieve the system messages.
        /// </summary>
        /// <returns>Return the system messages.</returns>
        public ObservableCollection<SystemMessageViewModel> GetSystemMessages()
        {
            try
            {
                this.LastRecord = null;
                ObservableCollection<SystemMessageViewModel> records = null;

                var messages = from message in SystemMessage.GetSystemMessages()
                                select new SystemMessageViewModel(message.EntryDateTime, message.Category, message.Text);

                records = new ObservableCollection<SystemMessageViewModel>(messages);
                this.LastRecord = records.FirstOrDefault();
                return records;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Gets the message type to display.
        /// </summary>
        public string DisplayMessage
        {
            get
            {
                return this.Category == MessageType.Background
                       || this.Category == MessageType.Priority
                       || this.Category == MessageType.Warning
                           ? Constant.Message
                           : Constant.Error;
            }
        }

        /// <summary>
        /// Gets or sets the time of the message.
        /// </summary>
        public string MessageTime
        {
            get
            {
                return this.messageTime;
            }

            set
            {
                this.messageTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the category for the message. For example, error.
        /// </summary>
        public MessageType Category
        {
            get
            {
                return this.category;
            }

            set
            {
                this.category = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the message to be displayed.
        /// </summary>
        public string MessageToDisplay
        {
            get
            {
                return this.messageToDisplay;
            }

            set
            {
                this.messageToDisplay = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to show the message history.
        /// </summary>
        public ICommand ShowMessageHistoryCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the alerts screen.
        /// </summary>
        public ICommand OpenAlertsCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the message history.
        /// </summary>
        public ICommand SetDatabaseCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Not implemented.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Send a message to display the message history.
        /// </summary>
        private void ShowMessageHistoryCommandExecute()
        {
            Messenger.Default.Send(ViewType.MessageHistory);
        }

        #endregion
    }
}


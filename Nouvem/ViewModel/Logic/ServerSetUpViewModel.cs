﻿// -----------------------------------------------------------------------
// <copyright file="ServerSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;
using System.Windows;

namespace Nouvem.ViewModel.Logic
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Handles the engineer/installer db set up.
    /// </summary>
    public class ServerSetUpViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// Flags if the db connection has changed.
        /// </summary>
        private bool databaseChanged;

        /// <summary>
        /// The server name.
        /// </summary>
        private string serverName;

        /// <summary>
        /// The database name.
        /// </summary>
        private string databaseName;

        /// <summary>
        /// The company name.
        /// </summary>
        private string companyName;

        /// <summary>
        /// The user id.
        /// </summary>
        private string userId;

        /// <summary>
        /// The server password.
        /// </summary>
        private string password;

        /// <summary>
        /// The ui warning message
        /// </summary>
        private string warningMessage;

        /// <summary>
        /// The server integrated security authentication flag.
        /// </summary>
        private bool integratedSecurity;
       
        /// <summary>
        /// The stored server/dbs
        /// </summary>
        private ObservableCollection<Server> servers;

        /// <summary>
        /// The currently selected server.
        /// </summary>
        private Server selectedServer;

        /// <summary>
        /// the timer for displaying the connection message.
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerSetUpViewModel"/> class.
        /// </summary>
        public ServerSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message

            Messenger.Default.Register<string>(
                this,
                Token.DBPassword,
                x =>
                {
                    this.Password = x;
                });


            #endregion 

            #region command

            // Handle the connection to the current db.
            this.ConnectToDatabaseCommand = new RelayCommand(this.ConnectToDatabaseCommandExecute);

            // Handle the ui load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            #endregion

            this.timer.Interval = TimeSpan.FromSeconds(1);
            this.timer.Tick += (sender, args) =>
                {
                    this.WarningMessage = string.Empty;
                    this.timer.Stop();
                };

            this.HasWriteAuthorisation = true;
            this.GetServers();
            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        public ObservableCollection<Server> Servers
        {
            get
            {
                return this.servers;
            }

            set
            {
                this.servers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected server.
        /// </summary>
        public Server SelectedServer
        {
            get
            {
                return this.selectedServer;
            }

            set
            {
                this.selectedServer = value;
                this.RaisePropertyChanged();

                if (value != null && this.IsFormLoaded)
                {
                    this.SetControlMode(ControlMode.OK);
                    this.HandleSelectedServer();
                }
            }
        }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }

            set
            {
                this.SetMode(value, this.companyName);
                this.companyName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        public string ServerName
        {
            get
            {
                return this.serverName;
            }

            set
            {
                this.SetMode(value, this.serverName);
                this.serverName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the database name.
        /// </summary>
        public string DatabaseName
        {
            get
            {
                return this.databaseName;
            }

            set
            {
                this.SetMode(value, this.databaseName);
                this.databaseName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the server user id.
        /// </summary>
        public string UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.SetMode(value, this.userId);
                this.userId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the server password.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.SetMode(value, this.password);
                this.password= value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the warning message.
        /// </summary>
        public string WarningMessage
        {
            get
            {
                return this.warningMessage;
            }

            set
            {
                this.warningMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether integrated security is being used.
        /// </summary>
        public bool IntegratedSecurity
        {
            get
            {
                return this.integratedSecurity;
            }

            set
            {
                this.integratedSecurity = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to connect to the current db.
        /// </summary>
        public ICommand ConnectToDatabaseCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Handles the control ok selection. The input server/db credentials are stored locally.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddServer();
                        break;

                case ControlMode.Update:
                    this.UpdateServer();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Reset the form on add or update.
        /// </summary>
        /// <param name="mode">The control mode change.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Add || this.CurrentMode == ControlMode.Update)
            {
                this.Reset();
            }
        }
       
        /// <summary>
        /// Handles the control cancel selection.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Check that a server has been selected.
        /// </summary>
        /// <returns>A flag, indicating whether a server is currently selected.</returns>
        protected override bool CheckForEntity()
        {
            return this.SelectedServer != null;
        }

        #endregion

        #region private 

        #region command execution

        /// <summary>
        /// Attempt to connect to the selected database.
        /// </summary>
        private void ConnectToDatabaseCommandExecute()
        {
            if (this.SelectedServer == null)
            {
                this.SetMessage(Message.NoDBSelected);
                return;
            }

            ApplicationSettings.CompanyName = this.selectedServer.CompanyName;
            ApplicationSettings.ServerName = this.SelectedServer.ServerName;
            ApplicationSettings.DatabaseName = this.SelectedServer.DefaultDatabaseName;
            ApplicationSettings.UserId = this.SelectedServer.UserId;
            ApplicationSettings.UserPassword = this.SelectedServer.Password;
            Settings.Default.Save();
            EntityConnection.CreateConnectionString();

            if (EntityConnection.TestConnection())
            {
                this.SetMessage(Message.ConnectionSuccessful);
                this.databaseChanged = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.RestartNouvemOnDBChangePrompt, ApplicationSettings.DatabaseName), scanner:true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.RestartNouvemOnDBChangePrompt, ApplicationSettings.DatabaseName));
                    }
              
                }));
            }
            else
            {
                this.SetMessage(Message.ConnectionUnSuccessful);
            }
        }

        /// <summary>
        /// Hamndler for the ui load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SelectedServer = null;
        }

        #endregion

        #region helper

        /// <summary>
        /// Starts the global data procesing.
        /// </summary>
        private void LoadData()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (sender, args) => NouvemGlobal.ApplicationStart();
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Method that updates a selected server.
        /// </summary>
        private void UpdateServer()
        {
            if (this.SelectedServer == null)
            {
                return;
            }

            this.SelectedServer.CompanyName = this.CompanyName;
            this.SelectedServer.ServerName = this.ServerName;
            this.SelectedServer.DatabaseName = this.DatabaseName;
            this.SelectedServer.Password = this.Password;
            this.SelectedServer.UserId = this.UserId;

            EntityConnection.SaveConnectionString(this.SelectedServer);
            this.Reset();
            this.SelectedServer = null;
            this.SetMessage(Message.UpdateSuccessful);
            this.UpdateServers();
            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Method that adds the new server/db details.
        /// </summary>
        private void AddServer()
        {
            #region validation

            var error = string.Empty;

            if (string.IsNullOrWhiteSpace(this.CompanyName))
            {
                error = Message.CompanyNameEmpty;
            }
            else if (string.IsNullOrWhiteSpace(this.ServerName))
            {
                error = Message.ServerNameEmpty;
            }
            else if (string.IsNullOrWhiteSpace(this.DatabaseName))
            {
                error = Message.DatabaseNameEmpty;
            }
            else if (!string.IsNullOrWhiteSpace(this.UserId) && string.IsNullOrWhiteSpace(this.Password))
            {
                error = Message.PasswordEmpty;
            }
            else if (!string.IsNullOrWhiteSpace(this.Password) && string.IsNullOrWhiteSpace(this.UserId))
            {
                error = Message.UserIdEmpty;
            }

            if (error != string.Empty)
            {
                this.SetMessage(error);
                return;
            }

            #endregion

            var localServer = new Server
            {
                ConnectionNo = this.Servers.Count + 1,
                CompanyName = this.CompanyName,
                ServerName = this.ServerName,
                DefaultDatabaseName = this.DatabaseName,
                UserId = this.UserId,
                Password = this.Password,
                DatabaseName = this.DatabaseName
            };

            // Display the newly added details in the grid.
            this.Servers.Add(localServer);

            EntityConnection.SaveConnectionString(localServer);
            this.Reset();
            this.GetServers();
            this.SelectedServer = null;
            this.SetMessage(Message.NewDetailsAdded);
            this.SetControlMode(ControlMode.Add);
        }
        
        /// <summary>
        /// Handler for a server selection.
        /// </summary>
        private void HandleSelectedServer()
        {
            try
            {
                this.EntitySelectionChange = true;
                this.Reset();
                this.CompanyName = this.selectedServer.CompanyName;
                this.ServerName = this.selectedServer.ServerName;
                this.DatabaseName = this.selectedServer.DefaultDatabaseName;
                this.UserId = this.selectedServer.UserId;
                this.Password = this.selectedServer.Password;
                this.IntegratedSecurity = this.selectedServer.IntegratedSecurity;
                Messenger.Default.Send(this.Password, Token.DBStoredPassword);
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Reset the ui fields.
        /// </summary>
        private void Reset()
        {
            this.CompanyName = string.Empty;
            this.ServerName = string.Empty;
            this.DatabaseName = string.Empty;
            this.UserId = string.Empty;
            this.Password = string.Empty;
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        /// <summary>
        /// Get the application server/db data.
        /// </summary>
        private void GetServers()
        {
            if (this.servers != null)
            {
                this.servers.Clear();
            }

            this.Servers = new ObservableCollection<Server>(EntityConnection.GetServers());
        }

        /// <summary>
        /// Updates the application server/db data.
        /// </summary>
        private void UpdateServers()
        {
            if (this.servers != null)
            {
                this.servers.Clear();
            }

            this.GetServers();
        }

        /// <summary>
        /// Sets the ui message for display.
        /// </summary>
        /// <param name="message">The message to display.</param>
        private void SetMessage(string message)
        {
            this.WarningMessage = message;
            this.timer.Start();
        }

        /// <summary>
        /// close the ui.
        /// </summary>
        private void Close()
        {
            if (this.databaseChanged)
            {
                // force restart
                Environment.Exit(-1);
            }

            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            Messenger.Default.Send(Token.Message, Token.CloseServerSetUpWindow);
            ViewModelLocator.ClearServerSetUp();
        }

        #endregion

        #endregion
    }
}

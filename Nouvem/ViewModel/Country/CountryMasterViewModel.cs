﻿// -----------------------------------------------------------------------
// <copyright file="CountryMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Country
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class CountryMasterViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The countries collection.
        /// </summary>
        private ObservableCollection<Country> countryMasters;

        /// <summary>
        /// The selected country.
        /// </summary>
        private Country selectedCountry;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the CountryMasterViewModel class.
        /// </summary>
        public CountryMasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.CountryMasters = new ObservableCollection<Country>();

            #endregion

            this.GetCountryMasters();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected country.
        /// </summary>
        public Country SelectedCountry
        {
            get
            {
                return this.selectedCountry;
            }

            set
            {
                this.selectedCountry = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the country Masters.
        /// </summary>
        public ObservableCollection<Country> CountryMasters
        {
            get
            {
                return this.countryMasters;
            }

            set
            {
                this.countryMasters = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedCountry != null)
            {
                this.selectedCountry.Deleted = true;
                this.UpdateCountryMaster();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.GetCountryMasters();
                this.SelectedCountry = null;
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateCountryMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the country master group.
        /// </summary>
        private void UpdateCountryMaster()
        {
            try
            {
                if (this.DataManager.AddOrUpdateCountryMasters(this.CountryMasters))
                {
                    SystemMessage.Write(MessageType.Priority, Message.CountryUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.CountryNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.CountryNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearCountryMaster();
            Messenger.Default.Send(Token.Message, Token.CloseCountryMasterWindow);
        }

        /// <summary>
        /// Gets the application countries.
        /// </summary>
        private void GetCountryMasters()
        {
            this.CountryMasters.Clear();
            foreach (var country in this.DataManager.GetCountryMaster())
            {
                this.CountryMasters.Add(country);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}


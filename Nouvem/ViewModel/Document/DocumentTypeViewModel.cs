﻿// -----------------------------------------------------------------------
// <copyright file="DocumentTypeViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Document
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class DocumentTypeViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The document types collection.
        /// </summary>
        private ObservableCollection<DocumentNumberingType> documentTypes;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentTypeViewModel"/> class.
        /// </summary>
        public DocumentTypeViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration



            #endregion

            #region command handler

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion
       
            this.GetDocumentTypes();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the document types.
        /// </summary>
        public ObservableCollection<DocumentNumberingType> DocumentTypes
        {
            get
            {
                return this.documentTypes;
            }

            set
            {
                this.documentTypes = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the containers.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Overrides the mode selection, to set up a new or find partner, clearing the form.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateDocumentTypes();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

         /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
             this.Close();
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #region helper

        /// <summary>
        /// Updates the document types.
        /// </summary>
        private void UpdateDocumentTypes()
        {
            try
            {
                if (this.DataManager.AddOrUpdateDocumentTypes(this.DocumentTypes))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DocumentTypesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DocumentTypesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DocumentTypesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDocumentType();
            Messenger.Default.Send(Token.Message, Token.CloseDocumentTypeWindow);
        }

        /// <summary>
        /// Gets the Nou Document Names.
        /// </summary>
        private void GetDocumentTypes()
        {
            this.DocumentTypes = new ObservableCollection<DocumentNumberingType>(this.DataManager.GetDocumentNumberingTypes());
        }

        #endregion

        #endregion
    }
}

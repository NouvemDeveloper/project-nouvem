﻿// -----------------------------------------------------------------------
// <copyright file="ExitViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;

namespace Nouvem.ViewModel.Exit
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;

    public class ExitViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The unload application timer reference.
        /// </summary>
        private DispatcherTimer unLoadTimer;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ExitViewModel"/> class.
        /// </summary>
        public ExitViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            this.OnLoadingCommand = new RelayCommand(this.OnLoading);

            #endregion
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets the command to exit the application.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Method that handles the closing of the application.
        /// </summary>
        private void OnLoading()
        {
            var stopTime = DateTime.Now.AddSeconds(0.01);
            this.unLoadTimer = new DispatcherTimer{ Interval = TimeSpan.FromMilliseconds(100) };
            this.unLoadTimer.Start();
            this.unLoadTimer.Tick += (sender, args) =>
                {
                    if (DateTime.Now > stopTime)
                    {
                        Application.Current.Shutdown();
                        Process.GetCurrentProcess().Kill();
                    }
                };
         }

        #endregion
    }
}

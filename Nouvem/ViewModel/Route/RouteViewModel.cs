﻿// -----------------------------------------------------------------------
// <copyright file="RouteViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Route
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class RouteViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The routes collection.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Route> routes;

        /// <summary>
        /// The routes to update collection.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Route> routesToUpdate = new ObservableCollection<Model.BusinessObject.Route>();

        /// <summary>
        /// The selected routes.
        /// </summary>
        private Model.BusinessObject.Route selectedRoute;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteViewModel"/> class.
        /// </summary>
        public RouteViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration



            #endregion

            #region command handler

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                if (this.SelectedRoute != null && !this.routesToUpdate.Contains(this.selectedRoute))
                {
                    this.routesToUpdate.Add(this.SelectedRoute);
                }

                this.SetControlMode(ControlMode.Update);
            }, this.CanUpdate);

            #endregion

            this.GetRegions();
            this.GetRoutes();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the regions.
        /// </summary>
        public IList<Model.BusinessObject.Region> Regions { get; set; }

        /// <summary>
        /// Gets or sets the routes.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.Route> Routes
        {
            get
            {
                return this.routes;
            }

            set
            {
                this.routes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the routes.
        /// </summary>
        public Model.BusinessObject.Route SelectedRoute
        {
            get
            {
                return this.selectedRoute;
            }

            set
            {
                this.selectedRoute = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the regions.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedRoute != null)
            {
                this.selectedRoute.Deleted = DateTime.Now;
                this.UpdateRoutes();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.Routes.Remove(this.selectedRoute);
            }
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateRoutes();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #region helper

        /// <summary>
        /// Updates the routes.
        /// </summary>
        private void UpdateRoutes()
        {
            #region validation

            foreach (var route in this.routes)
            {
                if (string.IsNullOrWhiteSpace(route.Name))
                {
                    SystemMessage.Write(MessageType.Issue, Message.RoutesNameBlank);
                    return;
                }
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdateRoutes(this.routesToUpdate))
                {
                    SystemMessage.Write(MessageType.Priority, Message.RoutesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.RoutesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.RoutesUpdated);
            }
            finally
            {
                this.routesToUpdate.Clear();
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearRoute();
            Messenger.Default.Send(Token.Message, Token.CloseRouteWindow);
        }

        /// <summary>
        /// Gets the regions.
        /// </summary>
        private void GetRegions()
        {
            this.Regions = new List<Model.BusinessObject.Region>(this.DataManager.GetRegions());
        }

        /// <summary>
        /// Gets the routes.
        /// </summary>
        private void GetRoutes()
        {
            this.Routes = new ObservableCollection<Model.BusinessObject.Route>(this.DataManager.GetRoutes());
        }

        #endregion

        #endregion
    }
}
﻿// -----------------------------------------------------------------------
// <copyright file="LoginViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;

namespace Nouvem.ViewModel.StartUp
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class LoginViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The remember user password flag.
        /// </summary>
        private bool rememberPassword;

        /// <summary>
        /// The password not required flag.
        /// </summary>
        private bool passwordNotRequired;

        /// <summary>
        /// The user password.
        /// </summary>
        private string userPassword;

        /// <summary>
        /// The selected user.
        /// </summary>
        private UserMaster selectedUser;

        /// <summary>
        /// The user id
        /// </summary>
        private string userId;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The background processing completion flag.
        /// </summary>
        private bool backgroundProcessingComplete;

        /// <summary>
        /// Display the loading message flag.
        /// </summary>
        private bool displayLoadingMessage;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        public LoginViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.CreateTouchscreenView, s => this.ShowTouchscreenModule());

            // handle the touvhscreen password entry
            Messenger.Default.Register<string>(this, Token.KeyboardValueEntered, s => this.HandleTouchscreenPasswordEntry(s));
           
            // receives the incoming password.
            Messenger.Default.Register<string>(
                this,
                Token.UserPassword,
                x =>
                    {
                        this.userPassword = x;
                        this.ErrorMessage = string.Empty;
                    });

            #endregion

            #region command registration

            // Handle the choose company selection.
            this.ChooseCompanyCommand = new RelayCommand(this.ChooseCompanyCommandExecute);

            // Handle the user verification.
            this.VerifyUserCommand = new RelayCommand(this.VerifyLogin);

            // Handle the display of the set up database view.
            this.SetDatabaseCommand = new RelayCommand(this.SetDatabaseCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle a selected user.
            this.UserSelectedCommand = new RelayCommand(this.HandleTouchscreenSelectedUser);

            // Shut down.
            this.CloseCommand = new RelayCommand(() => Environment.Exit(0));

            this.OnClosingCommand = new RelayCommand(() =>
            {
                NouvemGlobal.LoggedInUser.UserMaster.RememberPassword = this.RememberPassword;
                this.DataManager.StoreRememberPassword(NouvemGlobal.LoggedInUser);
                ViewModelLocator.ClearLogin();
                Messenger.Default.Unregister(this);
            });

            #endregion

            if (ApplicationSettings.TouchScreenModeOnly)
            {
                this.GetUsers();
            }
      
            ApplicationSettings.UserLoggedIn = false;
            Settings.Default.Save();
       
            // NouvemGlobal.GetUsers();
            Messenger.Default.Send(Token.Message, Token.CloseLoadingWindow);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the password is to be remembered.
        /// </summary>
        public bool RememberPassword
        {
            get
            {
                return this.rememberPassword;
            }

            set
            {
                this.rememberPassword = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the busy indicator is to be displayed.
        /// </summary>
        public bool DisplayLoadingMessage
        {
            get
            {
                return this.displayLoadingMessage;
            }

            set
            {
                this.displayLoadingMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public ObservableCollection<UserMaster> Users { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public UserMaster SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                if (this.IsBusy)
                {
                    return;
                }

                this.selectedUser = value;
                //if (ApplicationSettings.TouchScreenModeOnly)
                //{
                //    this.HandleTouchscreenSelectedUser();
                //}
            }
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.RaisePropertyChanged();
                this.ErrorMessage = string.Empty;
            }
        }
       
        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
                this.RaisePropertyChanged();

                if (ApplicationSettings.TouchScreenModeOnly && !string.IsNullOrEmpty(value))
                {
                    NouvemMessageBox.Show(value, touchScreen:true);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a user selection.
        /// </summary>
        public ICommand UserSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to choose a database company.
        /// </summary>
        public ICommand ChooseCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the command to verify the user.
        /// </summary>
        public ICommand VerifyUserCommand { get; private set; }

        /// <summary>
        /// Gets the command to input database settings.
        /// </summary>
        public ICommand SetDatabaseCommand { get; private set; }

        /// <summary>
        /// Gets the loading event command.
        /// </summary>
        public ICommand OnLoadingCommand{ get; private set; }

        /// <summary>
        /// Gets the command command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command command.
        /// </summary>
        public ICommand CloseCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Verifies the user.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.SetControlMode(ControlMode.OK);
            this.VerifyLogin();
        }

        /// <summary>
        /// Close the form/application
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            if (!ApplicationSettings.UserLoggedIn)
            {
                Application.Current.Shutdown();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }

            Messenger.Default.Send(Token.Message, Token.CloseLogin);
        }

        #endregion

        #region private

        /// <summary>
        /// Handles the command to open the server/db set up window.
        /// </summary>
        private void SetDatabaseCommandExecute()
        {
            // password protected, in the unlikely event a client user right double mouse clicks on the logo.
            PasswordEntry.Create(PasswordCheckType.ServerSetUp);

            if (!this.Locator.PasswordEntry.PasswordMatch)
            {
                return;
            }

            // Open the server set up window
            Messenger.Default.Send(Token.Message, Token.CreateServerSetUp);
        }

        /// <summary>
        /// Method that verifies a user login.
        /// </summary>
        private void VerifyLogin()
        {
            this.Log.LogInfo(this.GetType(), "VerifyLogin(): logging on..");
            var isNouvemUser = this.UserId.CompareIgnoringCase("nouvem");
            #region validation

            var localUser = this.DataManager.GetUser(this.userId);
            if (!this.passwordNotRequired && !string.IsNullOrWhiteSpace(this.UserId))
            {
                //NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserName.CompareIgnoringCase(this.userId));
                if (localUser != null && localUser.UserMaster.PasswordNotRequired == true)
                {
                    this.passwordNotRequired = true;
                }
            }

            if (localUser == null && !isNouvemUser)
            {
                this.ErrorMessage = Message.UserNotFound; 
                this.IsBusy = false;
                return;
            }
            else if (string.IsNullOrWhiteSpace(this.UserId))
            {
                this.ErrorMessage = Message.UserIdEmpty;
                this.IsBusy = false;
                return;
            }
            else if (string.IsNullOrWhiteSpace(this.userPassword) && !this.passwordNotRequired)
            {
                this.ErrorMessage = Message.PasswordEmpty;
                this.IsBusy = false;
                return;
            }

            #endregion

            // verify the user, returning the user details if verified.
            Tuple<string, string, string, bool, bool, bool> userDetails;

            try
            {
                userDetails = this.DataManager.VerifyLoginUser(this.userId, this.userPassword, NouvemGlobal.Users, this.passwordNotRequired);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                return;
            }
          
            var userName = userDetails.Item1;
            var fullName = userDetails.Item2;
            var identifier = userDetails.Item3;
            var alreadyLoggedIn = userDetails.Item4;
            var userSuspended = localUser != null && localUser.UserMaster.SuspendUser.ToBool();
            var changePassword = localUser != null && localUser.UserMaster.ChangeAtNextLogin.ToBool();

            if (userSuspended)
            {
                this.Log.LogInfo(this.GetType(), "User suspended: Closing down.");
                NouvemMessageBox.Show(Message.UserSuspended);
                this.Close();
                return;
            }

            if (userName != string.Empty)
            {
                ApplicationSettings.LoggedInUser = userName;
                ApplicationSettings.LoggedInUserName = fullName;
                ApplicationSettings.LoggedInUserIdentifier = identifier;
                //Settings.Default.Save();

                // get the user/device licence credentials.
                this.CheckForLicence();
                this.SetAuthorisations();

                //if (alreadyLoggedIn)
                //{
                //    this.Log.LogDebug(this.GetType(), "User already logged on..checking log off permissions");

                //    // user logged in somewhere else, so check if they can force a log off.
                //    if (NouvemGlobal.CheckAuthorisation(Authorisation.AllowUserToForceLogOff) == Constant.FullAccess)
                //    {
                //        this.Log.LogDebug(this.GetType(), "Permission granted");
                //        // permission granted, so ask the user
                //        if (ApplicationSettings.ScannerMode)
                //        {
                //            NouvemMessageBox.Show(Message.ForceLogOut, NouvemMessageBoxButtons.YesNo, scanner:true);
                //        }
                //        else
                //        {
                //            NouvemMessageBox.Show(Message.ForceLogOut, NouvemMessageBoxButtons.YesNo);
                //        }
                     
                //        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                //        {
                //            // user doesn't want to force a log out so exit.
                //            this.Close();
                //            return;
                //        }

                //        this.DataManager.ForceLogOff();
                //    }
                //    else
                //    {
                //        this.Log.LogDebug(this.GetType(), "No permission to log other user off");

                //        // user does not have permission to force log out the other user, so exit.
                //        if (ApplicationSettings.ScannerMode)
                //        {
                //            NouvemMessageBox.Show(Message.UserAlreadyLoggedIn, scanner:true);
                //        }
                //        else
                //        {
                //            NouvemMessageBox.Show(Message.UserAlreadyLoggedIn);
                //        }
                   
                //        this.Close();
                //        return;
                //    }
                //}

                if (changePassword)
                {
                    // user needs to set their own password
                    Messenger.Default.Send(Token.Message, Token.CreateUserPasswordChange);
                    return;
                }
               
                if (!ApplicationSettings.UserLoggedIn)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        this.Log.LogInfo(this.GetType(), "Scanner mode detected..Creating module selection");
                        Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
                    }
                    else if (ApplicationSettings.TouchScreenModeOnly)
                    {
                        this.ShowTouchscreenModule();
                        return;
                    }
                    else
                    {
                        this.Log.LogInfo(this.GetType(), "Loading main screen");

                        // user verified at start up, open application.
                        Messenger.Default.Send(userName, Token.CreateMasterView);
                    }
                }
                else
                {
                    // User already logged in, and changing log in credentials.
                    SystemMessage.Write(MessageType.Priority, Message.UserLoginChange);
                }
              
                this.Close();
            }
            else
            {
                this.Log.LogInfo(this.GetType(), "Invalid log in credentials entered");
                this.IsBusy = false;
                Messenger.Default.Send(false, Token.SetTopMost);
                this.ErrorMessage = Message.InvalidLogin;
            }
        }

        private void ShowTouchscreenModule()
        {
            this.Log.LogInfo(this.GetType(), "Touchscreen mode detected..Moving directly to touchscreen");
            this.DisplayLoadingMessage = true;
            //Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            ViewModelLocator.CreateMaster();

            ApplicationSettings.TouchScreenMode = true;
            if (ApplicationSettings.TouchScreenModeOnlyIntake)
            {
                this.Log.LogInfo(this.GetType(), "Loading intake");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.APReceipt);
            }
            else if (ApplicationSettings.TouchScreenModeOnlyIntoProduction)
            {
                this.Log.LogInfo(this.GetType(), "Loading into production");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.IntoProduction);
            }
            else if (ApplicationSettings.TouchScreenModeOnlyOutOfProduction)
            {
                this.Log.LogInfo(this.GetType(), "Loading out of production");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.OutOfProduction);
            }
            else if (ApplicationSettings.TouchScreenModeOnlyDispatch)
            {
                this.Log.LogInfo(this.GetType(), "Loading dispatch");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.ARDispatch);
            }
            else if (ApplicationSettings.TouchScreenModeOnlySequencer)
            {
                this.Log.LogInfo(this.GetType(), "Loading sequencer");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.Sequencer);
            }
            else if (ApplicationSettings.TouchScreenModeOnlyGrader)
            {
                this.Log.LogInfo(this.GetType(), "Loading grader");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.Grader);
            }
            else if (ApplicationSettings.TouchScreenModeOnlyWorkflow)
            {
                this.Log.LogInfo(this.GetType(), "Loading workflow");
                ViewModelLocator.MasterStatic.CreateTouchscreenModule(ViewType.Workflow);
            }
        }

        /// <summary>
        /// Get the local licence.
        /// </summary>
        private void CheckForLicence()
        {
            var licenceManager = LicenceManager.Instance;
            licenceManager.CheckForLicence();
        }

        /// <summary>
        /// Get the local licence.
        /// </summary>
        private void SetAuthorisations()
        {
            var manager = AuthorisationsManager.Instance;
            manager.SetAuthorisations();
        }

        /// <summary>
        /// Call the login selection view.
        /// </summary>
        private void ChooseCompanyCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.CreateLoginSelectionView);
            this.Close();
        }

        /// <summary>
        /// Handles the ui loadiing event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            try
            {
                this.IsFormLoaded = true;
                if (!string.IsNullOrEmpty(NouvemGlobal.ApplicationStartUpError))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(NouvemGlobal.ApplicationStartUpError, touchScreen: true);
                    }));

                    return;
                }

                if (!ApplicationSettings.UserLoggedIn)
                {
                    // set control focus
                    if (!string.IsNullOrEmpty(ApplicationSettings.LoggedInUserIdentifier))
                    {
                        this.UserId = ApplicationSettings.LoggedInUserIdentifier;
                        var localPassword = this.DataManager.GetUserPassword(this.UserId);
                        if (localPassword != null)
                        {
                            this.RememberPassword = true;
                            Messenger.Default.Send(localPassword, Token.PasswordRemembered);
                        }
                        else
                        {
                            Messenger.Default.Send(Token.Message, Token.FocusToPasswordBox);
                        }
                    }
                    else
                    {
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }

                    return;
                }

                // logged in, so clear the user name, and set focus to it's control
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.CloseLoadingWindow);
            }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        private void GetUsers()
        {
            //this.Users = new ObservableCollection<UserMaster>(this.DataManager.GetDbUsers());
            this.Users = new ObservableCollection<UserMaster>(NouvemGlobal.Users.Select(x => x.UserMaster).OrderBy(x => x.FullName));
        }

        /// <summary>
        /// Handle the user selection, showing the password entry screen.
        /// </summary>
        private void HandleTouchscreenSelectedUser()
        {
            if (this.SelectedUser == null || this.IsBusy)
            {
                return;
            }

            this.userId = this.SelectedUser.UserName;
            this.Log.LogInfo(this.GetType(), $"User logging in:{this.userId}");
            var localUser =
                NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserName.CompareIgnoringCase(this.userId));
            if (localUser != null && localUser.UserMaster.PasswordNotRequired == true)
            {
                this.passwordNotRequired = true;
                this.HandleTouchscreenPasswordEntry(string.Empty);
            }
            else
            {
                Messenger.Default.Send(Tuple.Create(ViewType.KeyboardPassword, ViewType.Login));
            }
        }

        /// <summary>
        /// Handles the touchscreen password entry, verifying the login.
        /// </summary>
        /// <param name="password">The user entered password/.</param>
        private async void HandleTouchscreenPasswordEntry(string password)
        {
            //Messenger.Default.Send(true, Token.SetTopMost);
            this.userPassword = password;
            this.IsBusy = true;
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(300);
                this.Log.LogInfo(this.GetType(), $"Password entered - {this.userPassword}");
                Application.Current.Dispatcher.Invoke(this.VerifyLogin);
            });
        }

        /// <summary>
        /// Close the window, freeing up all resources.
        /// </summary>
        private void Close()
        {
            this.UserId = string.Empty;
           
            // Close the view
            Messenger.Default.Send(Token.Message, Token.CloseLogin);
        }

        #endregion
    }
}

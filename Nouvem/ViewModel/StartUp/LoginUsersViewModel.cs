﻿// -----------------------------------------------------------------------
// <copyright file="LoginUserViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;

namespace Nouvem.ViewModel.StartUp
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class LoginUserViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The user password.
        /// </summary>
        private string userPassword;

        /// <summary>
        /// The selected user.
        /// </summary>
        private UserMaster selectedUser;

        /// <summary>
        /// The user id
        /// </summary>
        private string userId;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The background processing completion flag.
        /// </summary>
        private bool backgroundProcessingComplete;

        /// <summary>
        /// Display the loading message flag.
        /// </summary>
        private bool displayLoadingMessage;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        public LoginUserViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // handle the touvhscreen password entry
            Messenger.Default.Register<string>(this, Token.KeyboardValueEntered, s => this.HandleTouchscreenPasswordEntry(s));

            // receives the incoming password.
            Messenger.Default.Register<string>(
                this,
                Token.UserPassword,
                x =>
                {
                    this.userPassword = x;
                    this.ErrorMessage = string.Empty;
                });

            #endregion

            #region command registration

            // Handle the choose company selection.
            this.ChooseCompanyCommand = new RelayCommand(this.ChooseCompanyCommandExecute);

            // Handle the user verification.
            this.VerifyUserCommand = new RelayCommand(this.VerifyAuthorisation);

            // Handle the display of the set up database view.
            this.SetDatabaseCommand = new RelayCommand(this.SetDatabaseCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle a selected user.
            this.UserSelectedCommand = new RelayCommand(this.HandleTouchscreenSelectedUser);

            // Shut down.
            this.CloseCommand = new RelayCommand(() =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(Constant.Cancel, Token.AuthorisationResult);
                }));
              
                this.Close();
            });

            #endregion

            this.GetUsers();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the authorisation to check.
        /// </summary>
        public bool VerifyUser { get; set; }

        /// <summary>
        /// Gets or sets the authorisation to check.
        /// </summary>
        public IList<int> AuthorisedUserGroups { get; set; }

        /// <summary>
        /// Gets or sets the authorisation to check.
        /// </summary>
        public string Authorisation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the busy indicator is to be displayed.
        /// </summary>
        public bool DisplayLoadingMessage
        {
            get
            {
                return this.displayLoadingMessage;
            }

            set
            {
                this.displayLoadingMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public ObservableCollection<UserMaster> Users { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public UserMaster SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                this.selectedUser = value;
                this.RaisePropertyChanged();
                this.HandleTouchscreenSelectedUser();
            }
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.RaisePropertyChanged();
                this.ErrorMessage = string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrEmpty(value))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.SelectedUser = null;
                        NouvemMessageBox.Show(value, touchScreen: true);
                    }));
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a user selection.
        /// </summary>
        public ICommand UserSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to choose a database company.
        /// </summary>
        public ICommand ChooseCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the command to verify the user.
        /// </summary>
        public ICommand VerifyUserCommand { get; private set; }

        /// <summary>
        /// Gets the command to input database settings.
        /// </summary>
        public ICommand SetDatabaseCommand { get; private set; }

        /// <summary>
        /// Gets the loading event command.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command command.
        /// </summary>
        public ICommand CloseCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Verifies the user.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.SetControlMode(ControlMode.OK);
            this.VerifyAuthorisation();
        }

        /// <summary>
        /// Close the form/application
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            if (!ApplicationSettings.UserLoggedIn)
            {
                Application.Current.Shutdown();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }

            Messenger.Default.Send(Token.Message, Token.CloseLogin);
        }

        #endregion

        #region private

        /// <summary>
        /// Handles the command to open the server/db set up window.
        /// </summary>
        private void SetDatabaseCommandExecute()
        {
            // password protected, in the unlikely event a client user right double mouse clicks on the logo.
            PasswordEntry.Create(PasswordCheckType.ServerSetUp);

            if (!this.Locator.PasswordEntry.PasswordMatch)
            {
                return;
            }

            // Open the server set up window
            Messenger.Default.Send(Token.Message, Token.CreateServerSetUp);
        }

        /// <summary>
        /// Method that verifies a user login.
        /// </summary>
        private void VerifyUserIsAuthorisated()
        {
            this.Log.LogDebug(this.GetType(), "VerifyUserIsAuthorisated(): verifying..");

            #region validation

            var error = string.Empty;
            if (string.IsNullOrWhiteSpace(this.UserId))
            {
                error = Message.UserIdEmpty;
            }
            else if (string.IsNullOrWhiteSpace(this.userPassword))
            {
                error = Message.PasswordEmpty;
            }

            if (error != string.Empty)
            {
                this.ErrorMessage = error;
                return;
            }

            #endregion

            try
            {
                var users = NouvemGlobal.GetUsers();
                var localUser = users.FirstOrDefault(x => x.UserMaster.UserName.CompareIgnoringCase(this.userId.Trim())
                    && Security.Security.DecryptString(x.UserMaster.Password).Trim().Equals(this.userPassword.Trim()));
                
                var message = Constant.NoAuthorisation;
                if (localUser != null)
                {
                    var groupId = localUser.UserMaster.UserGroupID;
                    if (this.AuthorisedUserGroups.Contains(groupId))
                    {
                        message = Constant.Authorised;
                    }

                    Messenger.Default.Send(message, Token.AuthorisationResult);
                    this.SelectedUser = null;
                    this.Close();
                }
                else
                {
                    this.ErrorMessage = Message.InvalidCredentials;
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }

        /// <summary>
        /// Method that verifies a user login.
        /// </summary>
        private void VerifyAuthorisation()
        {
            this.Log.LogDebug(this.GetType(), "VerifyAuthorisation(): verifying..");

            #region validation

            var error = string.Empty;
            if (string.IsNullOrWhiteSpace(this.UserId))
            {
                error = Message.UserIdEmpty;
            }
            else if (string.IsNullOrWhiteSpace(this.userPassword))
            {
                error = Message.PasswordEmpty;
            }

            if (error != string.Empty)
            {
                this.ErrorMessage = error;
                return;
            }

            #endregion

            // verify the user, returning the user details if verified.
            var loggedInUser = NouvemGlobal.LoggedInUser;
            var licencee = NouvemGlobal.Licensee;
            var currentIdentifier = ApplicationSettings.LoggedInUserIdentifier;
   
            try
            {
                var identifier = this.DataManager.VerifyLoginUserForAuthorisationCheck(this.userId, this.userPassword);
                if (identifier != string.Empty)
                {
                    ApplicationSettings.LoggedInUserIdentifier = identifier;

                    // get the user/device licence credentials.
                    this.CheckForLicence();
                    var isAuthorised = true;// this.AuthorisationCheck(this.Authorisation, Constant.FullAccess);
                    var message = isAuthorised ? Constant.Authorised : Constant.NoAuthorisation;
                    Messenger.Default.Send(message, Token.AuthorisationResult);
                    this.SelectedUser = null;
                    this.Close();
                }
                else
                {
                    this.ErrorMessage = Message.InvalidCredentials;
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            finally
            {
                NouvemGlobal.LoggedInUser = loggedInUser;
                NouvemGlobal.Licensee = licencee;
                ApplicationSettings.LoggedInUserIdentifier = currentIdentifier;
            }
        }

        /// <summary>
        /// Get the local licence.
        /// </summary>
        private void CheckForLicence()
        {
            var licenceManager = LicenceManager.Instance;
            licenceManager.CheckForUserLicence();
        }

        /// <summary>
        /// Call the login selection view.
        /// </summary>
        private void ChooseCompanyCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.CreateLoginSelectionView);
            this.Close();
        }

        /// <summary>
        /// Handles the ui loadiing event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        private void GetUsers()
        {
            this.Users = new ObservableCollection<UserMaster>(this.DataManager.GetDbUsers());
        }

        /// <summary>
        /// Handle the user selection, showing the password entry screen.
        /// </summary>
        private void HandleTouchscreenSelectedUser()
        {
            if (this.SelectedUser == null)
            {
                return;
            }

            this.userId = this.SelectedUser.UserName;
            Messenger.Default.Send(Tuple.Create(ViewType.KeyboardPassword, ViewType.Login));
        }

        /// <summary>
        /// Handles the touchscreen password entry, verifying the login.
        /// </summary>
        /// <param name="password">The user entered password/.</param>
        private void HandleTouchscreenPasswordEntry(string password)
        {
            try
            {
                this.userPassword = password;

                if (this.VerifyUser)
                {
                    this.VerifyUserIsAuthorisated();
                    return;
                }

                this.VerifyAuthorisation();
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            }
        }

        /// <summary>
        /// Close the window, freeing up all resources.
        /// </summary>
        private void Close()
        {
            this.UserId = string.Empty;

            // Close the view
            Messenger.Default.Send(Token.Message, Token.CloseLogin);
        }

        #endregion
    }
}

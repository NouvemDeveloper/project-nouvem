﻿// -----------------------------------------------------------------------
// <copyright file="PasswordEntry.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.UserInput
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Class which handles the password entry logic.
    /// </summary>
    public class PasswordEntryViewModel : NouvemViewModelBase
    {
        #region Field

        /// <summary>
        /// the password value
        /// </summary>
        private string password;

        /// <summary>
        /// The error message
        /// </summary>
        private string error;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordEntryViewModel"/> class.
        /// </summary>
        public PasswordEntryViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
     
            #region Command Registration

            // Ok command handler.
            this.OkCommand = new RelayCommand(this.OkCommandExecute);

            // New relay command for the selection of cancel.
            this.CancelCommand = new RelayCommand(this.CancelCommandExecute);

            // Handle the closing of the window.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecuted);

            #endregion
        }

        #endregion

        #region public interface

        #region properties

        /// <summary>
        /// Gets or sets a value indicating whether the password entered matches the stored password.
        /// </summary>
        public bool PasswordMatch { get; set; }

        /// <summary>
        /// Gets or sets the authorisation level.
        /// </summary>
        public PasswordCheckType CheckType { get; set; }
      
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.RaisePropertyChanged();
                this.Error = string.Empty;
            }
        }
       
       /// <summary>
       /// Gets or sets the error.
       /// </summary>
        public string Error
        {
            get
            {
                return this.error;
            }

            set
            {
                this.error = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets the ok selection command.
        /// </summary>
        public ICommand OkCommand { get; private set; }

        /// <summary>
        /// Gets the cancel selection command.
        /// </summary>
        public ICommand CancelCommand { get; private set; }

        /// <summary>
        /// Gets the closing event command.
        /// </summary>
        public ICommand OnClosingCommand { get; set; }

        #endregion

        #endregion

        #region protected override

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region Command execution

       /// <summary>
       /// Handle the password entry.
       /// </summary>
        private void OkCommandExecute()
        {
            if (this.CheckPassword(this.password))
            {
                this.PasswordMatch = true;
                Messenger.Default.Send(Token.Message, Token.ClosePasswordEntryWindow);
                return;
            }

            this.Error = Strings.IncorrectPassword;
            Messenger.Default.Send(Token.Message, Token.IncorrectPasswordNotification);
        }

        /// <summary>
        /// Handle the cancellation of the password box.
        /// </summary>
        private void CancelCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.ClosePasswordEntryWindow);
        }

       /// <summary>
       /// Clean up.
       /// </summary>
        private void OnClosingCommandExecuted()
        {
            ViewModelLocator.ClearPasswordEntry();
        }

        #endregion

        #region private helpers

        /// <summary>
        /// Method that checks the entered password against the stored passwords,
        /// and the authorisation levels.
        /// </summary>
        /// <param name="passwordToCheck">the password to check</param>
        /// <returns>a flag, indicating a correct password entry or not.</returns>
        private bool CheckPassword(string passwordToCheck)
        {
            switch (this.CheckType)
            {
                case PasswordCheckType.ServerSetUp:
                {
                    return passwordToCheck == ApplicationSettings.NouvemPassword;
                }
            }

            return false;
        }

        #endregion
    }
}

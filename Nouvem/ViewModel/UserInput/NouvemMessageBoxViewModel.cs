﻿// -----------------------------------------------------------------------
// <copyright file="MessageBoxViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Properties;

namespace Nouvem.ViewModel.UserInput
{
    using System;
    using System.Windows.Input;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class that handles the message box functionality.
    /// </summary>
    public class NouvemMessageBoxViewModel : ViewModelBase
    {
        #region fields

        /// <summary>
        /// The left button text.
        /// </summary>
        private string buttonLeft;

        /// <summary>
        /// The right button text.
        /// </summary>
        private string buttonRight;

        /// <summary>
        /// The right button text.
        /// </summary>
        private string buttonSpare;


        /// <summary>
        /// flag, as to whether the left button is visible or not.
        /// </summary>
        private bool buttonLeftVisibile;

        /// <summary>
        /// flag, as to whether the left button is visible or not.
        /// </summary>
        private bool buttonSpareVisibile;

        /// <summary>
        /// The message text to display.
        /// </summary>
        private string displayMessage;

        /// <summary>
        /// The dispatch report print flag.
        /// </summary>
        private bool printDispatch;

        /// <summary>
        /// The invoice report print flag.
        /// </summary>
        private bool printInvoice;

        /// <summary>
        /// The dispatchdetail report print flag.
        /// </summary>
        private bool printDispatchDetail;

        /// <summary>
        /// The message box buttons selection field
        /// </summary>
        private NouvemMessageBoxButtons buttonSelection;

        /// <summary>
        /// The counter value.
        /// </summary>
        private int counter;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NouvemMessageBoxViewModel"/> class.
        /// </summary>
        public NouvemMessageBoxViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the nouvem message box handler message, and buttons configuration.
            Messenger.Default.Register<Tuple<string, NouvemMessageBoxButtons, bool, bool, bool, bool, bool, Tuple<bool>>>(this, x =>
            {
                this.DisplayMessage = x.Item1;
                this.SetUpButtons(x.Item2);
                Messenger.Default.Send(Tuple.Create(x.Item3, x.Item4, x.Item5, x.Item6, x.Item7, x.Rest.Item1), Token.OpenMessageBox);
            });

            #endregion

            #region command registration

            this.CounterChangeCommand = new RelayCommand<string>(this.CounterChangeCommandExecute);
            this.ButtonLeftCommand = new RelayCommand(this.ButtonLeftCommandExecute);
            this.ButtonRightCommand = new RelayCommand(this.ButtonRightCommandExecute);
            this.ButtonSpareCommand = new RelayCommand(this.ButtonSpareCommandExecute);

            #endregion

            this.Counter = Settings.Default.DispatchReportCopiesToPrint == 0
                ? 1
                : Settings.Default.DispatchReportCopiesToPrint;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether a dispatch docket is to be printed.
        /// </summary>
        public bool PrintDispatch
        {
            get
            {
                return this.printDispatch;
            }

            set
            {
                this.printDispatch = value;
                this.RaisePropertyChanged();
                ApplicationSettings.AutoPrintDispatchReport = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a dispatch docket is to be printed.
        /// </summary>
        public bool PrintInvoice
        {
            get
            {
                return this.printInvoice;
            }

            set
            {
                this.printInvoice = value;
                this.RaisePropertyChanged();
                ApplicationSettings.AutoPrintInvoiceReport = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a dispatch docket is to be printed.
        /// </summary>
        public bool PrintDispatchDetail
        {
            get
            {
                return this.printDispatchDetail;
            }

            set
            {
                this.printDispatchDetail = value;
                this.RaisePropertyChanged();
                ApplicationSettings.AutoPrintDispatchDetailReport = value;
            }
        }

        /// <summary>
        /// Gets or sets the counter value.
        /// </summary>
        public int Counter
        {
            get
            {
                return this.counter;
            }

            set
            {
                this.counter = value;
                this.RaisePropertyChanged();
                Settings.Default.DispatchReportCopiesToPrint = value;
            }
        }

        /// <summary>
        /// Gets or sets the left buttons text.
        /// </summary>
        public string ButtonLeft
        {
            get
            {
                return this.buttonLeft;
            }

            set
            {
                this.buttonLeft = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the right buttons text.
        /// </summary>
        public string ButtonRight
        {
            get
            {
                return this.buttonRight;
            }

            set
            {
                this.buttonRight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the left buttons text.
        /// </summary>
        public string ButtonSpare
        {
            get
            {
                return this.buttonSpare;
            }

            set
            {
                this.buttonSpare = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the message box message.
        /// </summary>
        public string DisplayMessage
        {
            get
            {
                return this.displayMessage;
            }

            set
            {
                this.displayMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the left button is visible or not.
        /// </summary>
        public bool ButtonSpareVisible
        {
            get
            {
                return this.buttonSpareVisibile;
            }

            set
            {
                this.buttonSpareVisibile = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the left button is visible or not.
        /// </summary>
        public bool ButtonLeftVisible
        {
            get
            {
                return this.buttonLeftVisibile;
            }

            set
            {
                this.buttonLeftVisibile = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the left button command.
        /// </summary>
        public ICommand ButtonLeftCommand { get; private set; }

        /// <summary>
        /// Gets the right button command.
        /// </summary>
        public ICommand ButtonRightCommand { get; private set; }

        /// <summary>
        /// Gets the left button command.
        /// </summary>
        public ICommand ButtonSpareCommand { get; private set; }

        /// <summary>
        /// Gets the counter change command.
        /// </summary>
        public ICommand CounterChangeCommand { get; private set; }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Increments/decrements the counter.
        /// </summary>
        private void CounterChangeCommandExecute(string change)
        {
            if (change == "Up")
            {
                this.Counter++;
                return;
            }

            if (this.Counter == 1)
            {
                return;
            }

            this.Counter--;
        }

        /// <summary>
        /// Handle the left button selection.
        /// </summary>
        private void ButtonLeftCommandExecute()
        {
            switch (this.buttonSelection)
            {
                case NouvemMessageBoxButtons.OKCancel:
                case NouvemMessageBoxButtons.OKCancelBackOrder:
                    NouvemMessageBox.UserSelection = UserDialogue.OK;
                    break;

                case NouvemMessageBoxButtons.YesNo:
                    NouvemMessageBox.UserSelection = UserDialogue.Yes;
                    break;

                case NouvemMessageBoxButtons.StandardWorkflow:
                    NouvemMessageBox.UserSelection = UserDialogue.Workflow;
                    break;

                default:
                    NouvemMessageBox.UserSelection = UserDialogue.OK;
                    break;
            }
        }

        /// <summary>
        /// Handle the right button selection.
        /// </summary>
        private void ButtonRightCommandExecute()
        {
            switch (this.buttonSelection)
            {
                case NouvemMessageBoxButtons.OKCancel:
                case NouvemMessageBoxButtons.OKCancelBackOrder:
                    NouvemMessageBox.UserSelection = UserDialogue.Cancel;
                    break;

                case NouvemMessageBoxButtons.YesNo:
                    NouvemMessageBox.UserSelection = UserDialogue.No;
                    break;

                case NouvemMessageBoxButtons.StandardWorkflow:
                    NouvemMessageBox.UserSelection = UserDialogue.Standard;
                    break;

                default:
                    NouvemMessageBox.UserSelection = UserDialogue.OK;
                    break;
            }
        }

        /// <summary>
        /// Handle the right button selection.
        /// </summary>
        private void ButtonSpareCommandExecute()
        {
            NouvemMessageBox.UserSelection = UserDialogue.BackOrder;
        }

        #endregion

        #region helper

        /// <summary>
        /// Set the buttons to display on the ui.
        /// </summary>
        /// <param name="buttonsSelection">The button selection enumeration value.</param>
        private void SetUpButtons(NouvemMessageBoxButtons buttonsSelection)
        {
            switch (buttonsSelection)
            {
                case NouvemMessageBoxButtons.OKCancel:
                    this.ButtonLeft = Strings.OK;
                    this.ButtonRight = Strings.Cancel;
                    this.ButtonLeftVisible = true;
                    this.ButtonSpareVisible = false;
                    break;

                case NouvemMessageBoxButtons.YesNo:
                    this.ButtonLeft = Strings.Yes;
                    this.ButtonRight = Strings.No;
                    this.ButtonLeftVisible = true;
                    this.ButtonSpareVisible = false;
                    break;

                case NouvemMessageBoxButtons.StandardWorkflow:
                    this.ButtonLeft = Strings.Workflow;
                    this.ButtonRight = Strings.Standard;
                    this.ButtonLeftVisible = true;
                    this.ButtonSpareVisible = false;
                    break;

                case NouvemMessageBoxButtons.OK:
                    this.ButtonLeftVisible = false;
                    this.ButtonSpareVisible = false;
                    this.ButtonRight = Strings.OK;
                    break;

                case NouvemMessageBoxButtons.OKCancelBackOrder:
                    this.ButtonLeft = Strings.OK;
                    this.ButtonRight = Strings.Cancel;
                    this.ButtonSpare = "Back Order";
                    this.ButtonLeftVisible = true;
                    this.ButtonSpareVisible = true;
                    break;
            }

            this.buttonSelection = buttonsSelection;
        }

        #endregion

        #endregion
    }
}
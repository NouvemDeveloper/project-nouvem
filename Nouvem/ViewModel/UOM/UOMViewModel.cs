﻿// -----------------------------------------------------------------------
// <copyright file="UOMViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpf.Editors.Helpers;
using Nouvem.Shared;

namespace Nouvem.ViewModel.UOM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class UOMViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The uom masters.
        /// </summary>
        private ObservableCollection<UOMMaster> uomMasters;

        /// <summary>
        /// The selected uom master.
        /// </summary>
        private UOMMaster selectedUOMMaster;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the UOMViewModel class.
        /// </summary>
        public UOMViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            //this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);
            this.HasWriteAuthorisation = true; // TODO Investigate

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.UOMMasters = new ObservableCollection<UOMMaster>();

            #endregion

            this.GetUOMMasters();
            this.OrderMethods = this.DataManager.GetOrderMethods()
                .Where(x => x.Name.CompareIgnoringCase(Strings.Quantity) 
                            || x.Name.CompareIgnoringCase(Strings.Weight)).ToList();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected uom Master.
        /// </summary>
        public UOMMaster SelectedUOMMaster
        {
            get
            {
                return this.selectedUOMMaster;
            }

            set
            {
                this.selectedUOMMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the uom Masters.
        /// </summary>
        public ObservableCollection<UOMMaster> UOMMasters
        {
            get
            {
                return this.uomMasters;
            }

            set
            {
                this.uomMasters = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the uom masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedUOMMaster != null)
            {
                this.selectedUOMMaster.Deleted = true;
                this.UpdateUOMMaster();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.UOMMasters.Remove(this.selectedUOMMaster);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateUOMMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the uom master group.
        /// </summary>
        private void UpdateUOMMaster()
        {
            try
            {
                if (this.DataManager.AddOrUpdateUOMMasters(this.UOMMasters))
                {
                    SystemMessage.Write(MessageType.Priority, Message.UOMUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.UOMNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.UOMNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearUOM();
            Messenger.Default.Send(Token.Message, Token.CloseUOMWindow);
        }

        /// <summary>
        /// Gets the application uoms.
        /// </summary>
        private void GetUOMMasters()
        {
            this.UOMMasters.Clear();
            foreach (var uom in this.DataManager.GetUOMMaster())
            {
                this.UOMMasters.Add(uom);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

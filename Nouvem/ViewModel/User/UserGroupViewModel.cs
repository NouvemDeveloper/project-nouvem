﻿// -----------------------------------------------------------------------
// <copyright file="UserGroupViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.CodeDom;
using System.Collections.Generic;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.ViewModel.User
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using GalaSoft.MvvmLight.CommandWpf;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Interface;
    using Nouvem.Properties;

    /// <summary>
    /// Class which handles the user groups set up.
    /// </summary>
    public class UserGroupViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The stored user groups.
        /// </summary>
        private ObservableCollection<UserGroup> userGroups;

        /// <summary>
        /// The selected user group reference.
        /// </summary>
        private UserGroup selectedUserGroup;

        /// <summary>
        /// The selected user group reference.
        /// </summary>
        private UserGroup selectedCopyFrom;

        /// <summary>
        /// The selected user group reference.
        /// </summary>
        private UserGroup selectedCopyTo;

        /// <summary>
        /// The user group description.
        /// </summary>
        private string description;

        /// <summary>
        /// The user group description.
        /// </summary>
        private bool addGroup;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the UserGroupViewModel class.
        /// </summary>
        public UserGroupViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<bool,int>>(this,Token.UpdateModules, t =>
            {
                if (this.selectedUserGroup != null)
                {
                    foreach (var userGroup in this.UserGroups.Where(x => x.ParentID == t.Item2))
                    {
                        userGroup.IsSelected = t.Item1;
                    }
                }
            });

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    // Only change control if the user group container is the active window.
                    //if (this.IsFormActive)
                    //{
                    this.ControlChange(x);
                    // }
                });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            this.CreateBlankUserGroup();

            #endregion

            #region command registration

            this.OnClosingCommand = new RelayCommand(() =>
            {
                this.UserGroups?.Clear();
                this.UserGroups = null;
            });

            this.RemoveGroupCommand = new RelayCommand(this.RemoveGroupCommandExecute);

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                if (this.selectedUserGroup != null)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            });

            #endregion

            #region instantiation

            this.UserGroups = new ObservableCollection<UserGroup>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessUsers);
            this.GetUserGroups();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected user group.
        /// </summary>
        public bool AddGroup
        {
            get
            {
                return this.addGroup;
            }

            set
            {
                this.addGroup = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<UserGroup> UserGroups
        {
            get { return this.userGroups; }

            set
            {
                this.userGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected user group.
        /// </summary>
        public UserGroup SelectedUserGroup
        {
            get
            {
                return this.selectedUserGroup;
            }

            set
            {
                this.selectedUserGroup = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected user group.
        /// </summary>
        public UserGroup SelectedCopyFrom
        {
            get
            {
                return this.selectedCopyFrom;
            }

            set
            {
                this.selectedCopyFrom = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected user group.
        /// </summary>
        public UserGroup SelectedCopyTo
        {
            get
            {
                return this.selectedCopyTo;
            }

            set
            {
                this.selectedCopyTo = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Gets or sets the user group description
        /// </summary>
        public string Description
        {
            get { return this.description; }

            set
            {
                this.SetControlMode(ControlMode.Add);
                this.description = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the control.
        /// </summary>
        public ICommand OnClosingCommand { get; set; }

        /// <summary>
        /// Gets the command to update the control.
        /// </summary>
        public ICommand RemoveGroupCommand { get; set; }

        /// <summary>
        /// Gets the command to update the control.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected void RemoveGroupCommandExecute()
        {
            if (this.selectedUserGroup != null)
            {
                var msg = string.Format(Message.RemoveUserGroupPrompt,this.selectedUserGroup.Description);
                NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    this.selectedUserGroup.Deleted = true;
                    this.UpdateUserGroup();
                    SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                }
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddUserGroup();
                    break;

                case ControlMode.Update:
                    this.UpdateUserGroup();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

       /// <summary>
       /// Make a call to close.
       /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
           this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the user group.
        /// </summary>
        private void UpdateUserGroup()
        {
            if (this.SelectedCopyFrom != null && this.SelectedCopyTo != null)
            {
                NouvemMessageBox.Show(string.Format(Message.CopyUserGroupPrompt,this.SelectedCopyFrom.Description, this.SelectedCopyTo.Description), NouvemMessageBoxButtons.YesNo);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                if (this.DataManager.CopyUserGroupSettings(this.SelectedCopyFrom.UserGroupID,
                    this.SelectedCopyTo.UserGroupID))
                {
                    SystemMessage.Write(MessageType.Priority, Message.UserGroupUpdated);
                    this.ClearForm();
                    this.GetUserGroups();
                    Messenger.Default.Send(Token.Message, Token.UpdateUserGroups);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.UserGroupNotUpdated);
                }

                return;
            }

            try
            {
                var groups = this.UserGroups.ToList();

                #region validation

                var noAdminGroup = groups.SelectMany(x =>
                    x.Modules.Where(mod => mod.Description.CompareIgnoringCase(Strings.Administration)))
                    .All(admin => !admin.IsSelected);

                if (noAdminGroup)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoAdminGroup);
                    return;
                }

                #endregion

                ProgressBar.SetUp(0, groups.Count + 1);
                ProgressBar.Run();

                if (this.DataManager.AddOrUpdateUserGroups(groups))
                {
                    SystemMessage.Write(MessageType.Priority, Message.UserGroupUpdated);
                    this.ClearForm();
                    this.GetUserGroups();
                    Messenger.Default.Send(Token.Message, Token.UpdateUserGroups);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.UserGroupNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.UserGroupNotUpdated);
            }
            finally
            {
                ProgressBar.Reset();
            }

            this.SetControlMode(ControlMode.OK);
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        /// <summary>
        /// Updates the user group.
        /// </summary>
        private void AddUserGroup()
        {
            try
            {
                var groups = new List<UserGroup>();
                groups.Add(new UserGroup {Description = this.Description});

                ProgressBar.SetUp(0, groups.Count + 1);
                ProgressBar.Run();

                if (this.DataManager.AddOrUpdateUserGroups(groups))
                {
                    SystemMessage.Write(MessageType.Priority, Message.UserGroupUpdated);
                    this.ClearForm();
                    this.GetUserGroups();
                    Messenger.Default.Send(Token.Message, Token.UpdateUserGroups);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.UserGroupNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.UserGroupNotUpdated);
            }
            finally
            {
                ProgressBar.Reset();
            }

            this.SetControlMode(ControlMode.OK);
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }
        
        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearUserGroup();
            Messenger.Default.Send(Token.Message, Token.CloseUserGroupWindow);
        }

        /// <summary>
        /// Gets the application user groups.
        /// </summary>
        private void GetUserGroups()
        {
            this.UserGroups.Clear();
            foreach (var report in this.ReportManager.Reports.Where(x => x.ReportID == 0))
            {
                this.DataManager.AddOrUpdateReport(report);
            }

            foreach (var group in this.DataManager.GetUserGroupsAndModules(this.ReportManager.Reports))
            {
                this.UserGroups.Add(group);
            }

            if (!NouvemGlobal.IsNouvemUser)
            {
                var nouvemGroup = this.UserGroups.FirstOrDefault(x => x.Description.CompareIgnoringCase("Nouvem"));
                if (nouvemGroup != null)
                {
                    this.UserGroups.Remove(nouvemGroup);
                }
            }
        }

        /// <summary>
        /// Creates a default user group.
        /// </summary>
        private UserGroup CreateBlankUserGroup()
        {
            return new UserGroup { Description = string.Empty };
        }

        /// <summary>
        /// Clear the ui.
        /// </summary>
        private void ClearForm()
        {
            this.SetControlMode(ControlMode.OK);
            this.AddGroup = false;
            this.Description = string.Empty;
            this.SelectedUserGroup = this.CreateBlankUserGroup();
            this.SelectedCopyFrom = null;
            this.SelectedCopyTo = null;
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedUserGroup == null)
            {
                return;
            }

            var previousUser =
                this.UserGroups.TakeWhile(
                    userGroup => userGroup.UserGroupID != this.SelectedUserGroup.UserGroupID)
                    .LastOrDefault();

            if (previousUser != null)
            {
                this.SelectedUserGroup = previousUser;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedUserGroup == null)
            {
                return;
            }

            var nextUser =
                this.UserGroups.SkipWhile(
                    user => user.UserGroupID != this.SelectedUserGroup.UserGroupID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextUser != null)
            {
                this.SelectedUserGroup = nextUser;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedUserGroup == null)
            {
                return;
            }
            
            var firstUser = this.userGroups.FirstOrDefault();

            if (firstUser != null)
            {
                this.SelectedUserGroup = firstUser;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            var lastUser = this.UserGroups.LastOrDefault();

            if (lastUser != null)
            {
                this.SelectedUserGroup = lastUser;
            }
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;
            }
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        #endregion
    }
}

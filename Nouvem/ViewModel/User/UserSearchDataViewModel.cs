﻿// -----------------------------------------------------------------------
// <copyright file="UserSearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.User
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// User search data
    /// </summary>
    public class UserSearchDataViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected grid partner.
        /// </summary>
        private User selectedSearchUser;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<User> filteredUsers;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private IList<User> allUsers = new List<User>();

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        private bool keepVisible;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        private bool showInactiveUsers;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSearchDataViewModel"/> class.
        /// </summary>
        public UserSearchDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Handle the ui partner selection.
            this.UserSelectedCommand = new RelayCommand(this.UserSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            this.GetUsers();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected search user.
        /// </summary>
        public User SelectedSearchUser
        {
            get
            {
                return this.selectedSearchUser;
            }

            set
            {
                if (value != null)
                {
                    this.selectedSearchUser = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool ShowInactiveUsers
        {
            get
            {
                return this.showInactiveUsers;
            }

            set
            {
                this.showInactiveUsers = value;
                this.RaisePropertyChanged();
                this.Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the filtered users.
        /// </summary>
        public ObservableCollection<User> FilteredUsers
        {
            get
            {
                return this.filteredUsers;
            }

            set
            {
                this.filteredUsers = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the user selection.
        /// </summary>
        public ICommand UserSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #endregion

        #region protected override command execute

        /// <summary>
        /// Handler for the selection of a business partner.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.UserSelectedCommandExecute();
        }

        /// <summary>
        /// Handler for the cancelling of the business partner search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.CloseWindow();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the selection of a user.
        /// </summary>
        private void UserSelectedCommandExecute()
        {
            if (this.selectedSearchUser == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.SelectUser);
                return;
            }

            if (!this.Locator.UserSearchData.IsFormLoaded)
            {
                Messenger.Default.Send(ViewType.UserSetUp);
            }

            this.Locator.UserMaster.SelectedUser = this.selectedSearchUser;

            this.CloseWindow();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.KeepVisible = false;
            this.SetControlMode(ControlMode.OK);
            if (this.FilteredUsers.Any())
            {
                this.SelectedSearchUser = this.FilteredUsers.First();
            }
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.CloseWindow();
        }

        #endregion

        #region helper

        /// <summary>
        /// Method that sets the users to display.
        /// </summary>
        private void GetUsers()
        {
            this.allUsers = this.DataManager.GetUsers();
            this.Refresh();
        }

        /// <summary>
        /// Refresh the users.
        /// </summary>
        private void Refresh()
        {
            if (this.showInactiveUsers)
            {
                this.FilteredUsers = new ObservableCollection<User>(this.allUsers.Where(x => x.UserMaster.InActiveFrom <= DateTime.Today 
                                                                                             && x.UserMaster.InActiveTo >= DateTime.Today));
                return;
            }

            this.FilteredUsers = new ObservableCollection<User>(this.allUsers.Where(x => x.UserMaster.InActiveFrom == null 
                                                                                         || x.UserMaster.InActiveTo == null
                                                                                         || x.UserMaster.InActiveFrom > DateTime.Today
                                                                                         || x.UserMaster.InActiveTo < DateTime.Today));
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        private void CloseWindow()
        {
            if (!this.keepVisible)
            {
                this.FilteredUsers?.Clear();
                Messenger.Default.Send(Token.Message, Token.CloseWindow);
            }
        }

        #endregion

        #endregion
    }
}
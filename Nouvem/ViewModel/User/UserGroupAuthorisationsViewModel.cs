﻿// -----------------------------------------------------------------------
// <copyright file="UserGroupAuthorisationsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Nouvem.Shared;

namespace Nouvem.ViewModel.User
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class which handles the authorisation updates.
    /// </summary>
    public class UserGroupAuthorisationsViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The full access flag.
        /// </summary>
        private bool dontChangeFullAccess;

        /// <summary>
        /// The full access flag.
        /// </summary>
        private bool fullAccess; 

        /// <summary>
        /// The stored user groups.
        /// </summary>
        private ObservableCollection<UserGroup_> userGroups;

        /// <summary>
        /// The stored authorisation values.
        /// </summary>
        private ObservableCollection<NouAuthorisationValue> authorisationValues;
      
        /// <summary>
        /// The user authorisations.
        /// </summary>
        private ObservableCollection<UserGroupAuthorisations> userGroupAuthorisations;

        /// <summary>
        /// The selected user group reference.
        /// </summary>
        private UserGroupAuthorisations selectedUserGroup;

        #endregion

        #region construtor

         /// <summary>
        /// Initializes a new instance of the UserGroupAuthorisationsViewModel class.
        /// </summary>
        public UserGroupAuthorisationsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #region message registration

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    // Only change control if the user group container is the active window.
                    //if (this.IsFormActive)
                    //{
                        this.ControlChange(x);
                   // }
                });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);
  
            #endregion

            #endregion

            #region command registration

            // Set the control mode to update.
            this.ValueChangedCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.UserGroups = new ObservableCollection<UserGroup_>();
            this.AuthorisationValues = new ObservableCollection<NouAuthorisationValue>();
            this.UserGroupAuthorisationsData = new ObservableCollection<UserGroupAuthorisations>();

            #endregion
           
            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessAuthorisations);
            this.GetUserGroups();
            this.GetAuthorisationValues();
            this.GetUserGroupAuthorisations();
            this.IsFormLoaded = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the user groups authorisations.
        /// </summary>
        public bool FullAccess
        {
            get
            {
                return this.fullAccess;
            }

            set
            {
                this.fullAccess = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded && !this.EntitySelectionChange && this.SelectedUserGroup != null)
                {
                    foreach (var viewGroupRule in this.SelectedUserGroup.Rules)
                    {
                        viewGroupRule.NouAuthorisationValueID = value ? 1 : 3;
                    }

                    var groupId = this.selectedUserGroup.UserGroup.UserGroupID;
                    this.dontChangeFullAccess = true;
                    this.SelectedUserGroup = null;
                    this.SelectedUserGroup = this.UserGroupAuthorisationsData.FirstOrDefault(x => x.UserGroup.UserGroupID == groupId);
                    this.dontChangeFullAccess = false;
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Gets or sets the user groups authorisations.
        /// </summary>
        public ObservableCollection<NouAuthorisationValue> AuthorisationValues
        {
            get
            {
                return this.authorisationValues;
            }

            set
            {
                this.authorisationValues = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups authorisations.
        /// </summary>
        public ObservableCollection<UserGroupAuthorisations> UserGroupAuthorisationsData
        {
            get
            {
                return this.userGroupAuthorisations;
            }

            set
            {
                this.userGroupAuthorisations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<UserGroup_> UserGroups
        {
            get
            {
                return this.userGroups;
            }

            set
            {
                this.userGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected user group.
        /// </summary>
        public UserGroupAuthorisations SelectedUserGroup
        {
            get
            {
                return this.selectedUserGroup;
            }

            set
            {
                this.selectedUserGroup = value;
                this.RaisePropertyChanged();

                if (!this.dontChangeFullAccess)
                {
                    this.EntitySelectionChange = true;
                    this.FullAccess = false;
                    this.EntitySelectionChange = false;
                }
            }
        }

        #endregion

        #region command

        public ICommand ValueChangedCommand { get; set; }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            // not implemented
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            // not implemented
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            // not implemented
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            // not implemented
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
           // not implemented
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the control change, to ensure only ok is selected, when not updating.
        /// </summary>
        /// <param name="mode"></param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode != ControlMode.Update)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the control selection, to update an authorisation, or close the ui.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                this.AddOrUpdateAuthorisations();
                return;
            }

            if (this.CurrentMode == ControlMode.OK)
            {
                this.Close();
                return;
            }
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearUserGroupAuthorisations();
            Messenger.Default.Send(Token.Message, Token.CloseUserGroupAuthorisationWindow);
        }

        /// <summary>
        /// Update the selected user group rule.
        /// </summary>
        private void AddOrUpdateAuthorisations()
        {
            if (this.DataManager.AddOrUpdateAuthorisationRules(this.selectedUserGroup.Rules))
            {
                SystemMessage.Write(MessageType.Priority, Message.RuleUpdated);
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.RuleNotUpdated);
            }
        }

        /// <summary>
        /// Gets the application user groups.
        /// </summary>
        private void GetUserGroups()
        {
            this.UserGroups.Clear();
            foreach (var group in this.DataManager.GetUserGroups())
            {
                this.UserGroups.Add(group);
            }

            if (!NouvemGlobal.IsNouvemUser)
            {
                var nouvemGroup = this.UserGroups.FirstOrDefault(x => x.Description.CompareIgnoringCase("Nouvem"));
                if (nouvemGroup != null)
                {
                    this.UserGroups.Remove(nouvemGroup);
                }
            }
        }

        /// <summary>
        /// Gets the application authorisation values.
        /// </summary>
        private void GetAuthorisationValues()
        {
            this.AuthorisationValues.Clear();
            foreach (var value in this.DataManager.GetAuthorisationValues())
            {
                this.AuthorisationValues.Add(value);
            }
        }

        /// <summary>
        /// Gets the application authorisation list.
        /// </summary>
        private void GetUserGroupAuthorisations()
        {
            this.UserGroupAuthorisationsData.Clear();
            foreach (var value in this.DataManager.GetUserGroupAuthorisations())
            {
                this.UserGroupAuthorisationsData.Add(value);
            }

            if (!NouvemGlobal.IsNouvemUser)
            {
                var nouvemGroup = this.UserGroupAuthorisationsData.FirstOrDefault(x => x.UserGroup.Description.CompareIgnoringCase("Nouvem"));
                if (nouvemGroup != null)
                {
                    this.UserGroupAuthorisationsData.Remove(nouvemGroup);
                }
            }
        }

        #endregion
    }
}

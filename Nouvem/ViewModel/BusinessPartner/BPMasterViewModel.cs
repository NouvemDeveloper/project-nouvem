﻿// -----------------------------------------------------------------------
// <copyright file="BPMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Editors;

namespace Nouvem.ViewModel.BusinessPartner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Win32;
    using Nouvem.Shared;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.ViewModel.Interface;

    /// <summary>
    /// View model that controls the contact crud operations.
    /// </summary>
    public class BPMasterViewModel : BPViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// Dont refresh the partners flag.
        /// </summary>
        private bool dontRefreshPartners;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BPMasterViewModel"/> class.
        /// </summary>
        public BPMasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration
            
            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a new group addition.
            Messenger.Default.Register<string>(
                this, 
                Token.RefreshGroups,
                s =>
                {
                    this.GetPartnerGroups();
                });

            // Register for a new currency addition.
            Messenger.Default.Register<string>(
                this,
                Token.UpdateCurrencies,
                s =>
                {
                    this.BusinessPartnerCurrencies =
                        new ObservableCollection<BPCurrency>(this.DataManager.GetBusinessPartnerCurrencies());
                    this.BusinessPartnerCurrencies.Insert(0, new BPCurrency { Name = Strings.NoneSelected });
                    this.PartnerCurrency = this.BusinessPartnerCurrencies.ElementAt(0);
                });

            // Register for a new type addition.
            Messenger.Default.Register<string>(
                this,
                Token.UpdateTypes,
                s =>
                {
                    this.BusinessPartnerTypes =
                        new ObservableCollection<NouBPType>(this.DataManager.GetBusinessPartnerTypes());
                    this.BusinessPartnerTypes.Insert(0, new NouBPType { Type = Strings.NoneSelected });
                    this.PartnerType = this.BusinessPartnerTypes.ElementAt(0);
                });

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                    //// Only change control if the BPMasterWindowView is the active window.
                    //if (this.IsFormActive)
                    //{

                    //}
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(this, Token.BusinessPartnerSelected, partner => this.SelectedBusinessPartner = partner);

            #endregion

            #region command registration

            // Command to handle a partner cbo edit change.
            this.PartnerEditValueChangingCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);
            });

            // Command to handle a partner name edit.
            this.PartnerNameEditCommand = new RelayCommand<ProcessNewValueEventArgs>(this.HandleNameEdit);

            // Command handler to locate a business partner
            this.FindBusinessPartnerCommand = new RelayCommand(this.FindBusinessPartnerCommandExecute);

            // Command handler to add a new contact
            this.AddNewContactCommand = new RelayCommand(this.AddNewContactCommandExecute);

            // Command handler to add a new address
            this.AddNewAddressCommand = new RelayCommand(this.AddNewAddressCommandExecute);

            // Command handler to select/deselect the selected partner properties.
            this.SelectAllPropertiesCommand = new RelayCommand<string>(this.SelectAllPropertiesCommandExecute);

            // Command handler for a property selection.
            this.PropertySelectedCommand = new RelayCommand(this.PropertySelectedCommandExecute);

            // Command handler to attach a file.
            this.AttachFileCommand = new RelayCommand(this.AttachFileCommandExecute);

            // Command to delete an attachment.
            this.DeleteFileCommand = new RelayCommand(this.DeleteFileCommandExecute);

            // Command to delete an attachment.
            this.DisplayFileCommand = new RelayCommand(this.DisplayFileCommandExecute);

            // Handles the form loading event.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handles the form loading event.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            // Handles the group drill down.
            this.DrillDownToGroupCommand = new RelayCommand(() => Messenger.Default.Send(Tuple.Create(ViewType.BPGroupSetUp, 0), Token.DrillDown));

            // Handles the currency drill down.
            this.DrillDownToCurrencyCommand = new RelayCommand(() => Messenger.Default.Send(Tuple.Create(ViewType.Currency, 0), Token.DrillDown));
            
            // Handles the route drill down.
            this.DrillDownToRouteCommand = new RelayCommand(() => Messenger.Default.Send(Tuple.Create(ViewType.Route, 0), Token.DrillDown));

            #endregion

            this.CreateBlankBusinessPartner();
            this.GetPartnerGroups();
            this.GetAgents();
        }

        #endregion

        #region public interface

        #region property

        #endregion

        #region command

        /// <summary>
        /// Gets the command to drill down to the currencies.
        /// </summary>
        public ICommand DrillDownToCurrencyCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a parner cbo edit change.
        /// </summary>
        public ICommand PartnerEditValueChangingCommand { get; private set; }

         /// <summary>
        /// Gets the command to handle a name change.
        /// </summary>
        public ICommand PartnerNameEditCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to a type.
        /// </summary>
        public ICommand DrillDownToGroupCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to a group.
        /// </summary>
        public ICommand DrillDownToTypeCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to a route.
        /// </summary>
        public ICommand DrillDownToRouteCommand { get; private set; }

        /// <summary>
        /// Gets the command to locate a business partner.
        /// </summary>
        public ICommand FindBusinessPartnerCommand { get; private set; }

        /// <summary>
        /// Gets the command to add a new business partner contact.
        /// </summary>
        public ICommand AddNewContactCommand { get; private set; }

        /// <summary>
        /// Gets the command to add a new business partner address.
        /// </summary>
        public ICommand AddNewAddressCommand { get; private set; }

        /// <summary>
        /// Gets the command to select/deselect all the partner properties.
        /// </summary>
        public ICommand SelectAllPropertiesCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selection of a property.
        /// </summary>
        public ICommand PropertySelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to attach a file.
        /// </summary>
        public ICommand AttachFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to displaya file.
        /// </summary>
        public ICommand DisplayFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to delete a file.
        /// </summary>
        public ICommand DeleteFileCommand { get; private set; }

        /// <summary>
        /// Gets the form loading command.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the form closing command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region public method

        /// <summary>
        /// Handler for an accounts partner import.
        /// </summary>
        /// <param name="partner">The imported partner.</param>
        public void HandleAccountsPartnerImport(BusinessPartner partner)
        {
            if (this.BusinessPartnerTypes == null || this.BusinessPartnerTypes.Count == 0)
            {
                this.BusinessPartnerTypes =
                    new ObservableCollection<NouBPType>(this.DataManager.GetBusinessPartnerTypes());
            }

            if (this.BusinessPartnerCurrencies== null || this.BusinessPartnerCurrencies.Count == 0)
            {
                this.BusinessPartnerCurrencies =
                    new ObservableCollection<BPCurrency>(this.DataManager.GetBusinessPartnerCurrencies());
            }

            var exists =
                NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.Code.CompareIgnoringCase(partner.Details.Code));

            if (exists == null)
            {
                this.SetControlMode(ControlMode.Add);
                this.dontRefreshPartners = true;
                this.SelectedBusinessPartner = partner;
                ApplicationSettings.AutoCreateCustomerPriceList = true;
                this.AddNewBusinessPartner();
            }
            else
            {
                this.HasWriteAuthorisation = true;
                this.SetControlMode(ControlMode.Update);
                exists.Details.CreditLimit = partner.Details.CreditLimit;
                exists.Details.Balance = partner.Details.Balance;
                exists.Details.Name = partner.Details.Name;
                this.dontRefreshPartners = true;
                this.SelectedBusinessPartner = exists;
                this.SetControlMode(ControlMode.Update);
                this.UpdateBusinessPartner();
            }
        }

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedBusinessPartner == null)
            {
                return;
            }

            var previousPartner =
                NouvemGlobal.BusinessPartners.TakeWhile(
                    partner => partner.Details.BPMasterID != this.SelectedBusinessPartner.Details.BPMasterID)
                    .LastOrDefault();

            if (previousPartner != null)
            {
                this.SelectedBusinessPartner = previousPartner;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedBusinessPartner == null)
            {
                return;
            }

            var nextPartner =
                NouvemGlobal.BusinessPartners.SkipWhile(
                    partner => partner.Details.BPMasterID != this.SelectedBusinessPartner.Details.BPMasterID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextPartner != null)
            {
                this.SelectedBusinessPartner = nextPartner;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedBusinessPartner == null)
            {
                return;
            }

            var firstPartner = NouvemGlobal.BusinessPartners.FirstOrDefault();

            if (firstPartner != null)
            {
                this.SelectedBusinessPartner = firstPartner;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.SelectedBusinessPartner == null)
            {
                return;
            }

            var lastPartner = NouvemGlobal.BusinessPartners.LastOrDefault();

            if (lastPartner != null)
            {
                this.SelectedBusinessPartner = lastPartner;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            if (this.BusinessPartners == null || !this.BusinessPartners.Any())
            {
                return;
            }

            var orderedPartners = this.BusinessPartners.OrderByDescending(x => x.Details.EditDate).ToList();
            this.SelectedBusinessPartner = orderedPartners.First();
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        #endregion

        #endregion

        #region protected override

        protected override void HandleSelectedProcess(Model.DataLayer.Process value)
        {
            this.SetMode(value, this.SelectedProcess);
        }

        /// <summary>
        /// Drill down handler.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            if (this.SelectedPriceList == null || this.SelectedPriceList.CurrentPriceListName.Equals(Strings.NoneSelected))
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.PriceListDetail, this.SelectedPriceList.PriceListID), Token.DrillDown);
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new or find partner, clearing the form.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new partner mode, so clear the partner fields.
                this.ClearForm();
                this.SetControlMode(ControlMode.Add);
                this.SearchMode = false;

                Messenger.Default.Send(Token.Message, Token.FocusToDataUpdate);
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into find partner mode, so clear the partner fields.
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                this.SearchMode = true;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedPartnerControl);
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddNewBusinessPartner();
                    break;

                case ControlMode.Find:
                    this.DisplaySearchScreen();
                    break;

                case ControlMode.Update:
                    this.UpdateBusinessPartner();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Method that handles the cancel selection.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Overrides the command to display the audit screen.
        /// </summary>
        /// <param name="s">The command parameter.</param>
        protected override void AuditDisplayCommandExecute(string s)
        {
            base.AuditDisplayCommandExecute(s);

            if (s.Equals(Constant.BusinessPartner) && this.SelectedBusinessPartner.Details.BPMasterID > 0)
            {
                Messenger.Default.Send(Tuple.Create(Constant.BusinessPartner, this.SelectedBusinessPartner.Details.BPMasterID), Token.AuditData);
            }
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handler for the form load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.BusinessPartnerCurrencies.Clear();
            this.BusinessPartnerTypes.Clear();
            this.BPProperties.Clear();
            this.BusinessPartnerTypes.Add(new NouBPType { Type = Strings.NoneSelected });
            this.BusinessPartnerCurrencies.Add(new BPCurrency { Name = Strings.NoneSelected });

            NouvemGlobal.BusinessPartnerCurrencies.ToList().ForEach(x => this.BusinessPartnerCurrencies.Add(x));
            NouvemGlobal.BusinessPartnerTypes.ToList().ForEach(x => this.BusinessPartnerTypes.Add(x));
            NouvemGlobal.BusinessPartnerProperties.ToList().ForEach(x => this.BPProperties.Add(new BusinessPartnerProperty { Details = x }));

            this.SetControlMode(ControlMode.Find);
            this.IsFormLoaded = true;
            this.GetNewPartners();
            this.SearchMode = true;
            this.ClearForm();
            this.SearchMode = true;
        }

        private async void GetNewPartners()
        {
            await Task.Factory.StartNew(NouvemGlobal.UpdateBusinessPartners);
        }

        /// <summary>
        /// Method that handles the window close event, by freeing up it's resources.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.ClearForm();
            this.IsFormLoaded = false;
            ViewModelLocator.ClearBPMaster();
        }

        /// <summary>
        /// Method that handles the selection/deselection of a selected partners properties.
        /// </summary>
        /// <param name="mode">The selection mode(select/deselect).</param>
        private void SelectAllPropertiesCommandExecute(string mode)
        {
            if (this.SelectedBusinessPartner.Details.BPMasterID != 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }

            if (mode.Equals(Constant.Select))
            {
                // select all
                this.BPProperties.Clear();
                NouvemGlobal.BusinessPartnerProperties.ToList().ForEach(x => this.BPProperties.Add(new BusinessPartnerProperty { Details = x, IsSelected = true }));
            }
            else
            {
                // deselect all
                this.BPProperties.Clear();
                NouvemGlobal.BusinessPartnerProperties.ToList().ForEach(x => this.BPProperties.Add(new BusinessPartnerProperty { Details = x }));
            }
        }

        /// <summary>
        /// Method that locates a business partner(s), based on the user search term.
        /// </summary>
        private void FindBusinessPartnerCommandExecute()
        {
            this.DisplaySearchScreen();
        }

        /// <summary>
        /// Handler for the command to attach a new file to the db.
        /// </summary>
        private void AttachFileCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                var localFile = File.ReadAllBytes(openFileDialog.FileName);
                var localName = openFileDialog.SafeFileName;

                var localAttachment = new BPAttachment { AttachmentDate = DateTime.Now, FileName = localName, File = localFile };

                this.SelectedBusinessPartner.Attachments.Add(localAttachment);
                this.BusinessPartnerAttachments.Add(localAttachment);

                if (!this.CheckForEntity())
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Handler for the command to display a new file to the db.
        /// </summary>
        private void DisplayFileCommandExecute()
        {
            #region validation

            if (this.SelectedAttachment == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAttachmentSelected);
                return;
            }

            #endregion

            try
            {
                var file = this.SelectedAttachment.File;
                var fileName = this.SelectedAttachment.FileName;

                var path = Path.Combine(Settings.Default.AtachmentsPath, fileName);
                File.WriteAllBytes(path, file);
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handler for the command to delete a file from the db.
        /// </summary>
        private void DeleteFileCommandExecute()
        {
            if (this.SelectedAttachment != null)
            {
                if (this.SelectedBusinessPartner.Details.BPMasterID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }

                // mark it for removal.
                this.SelectedAttachment.Deleted = true;
                this.BusinessPartnerAttachments.Remove(this.SelectedAttachment);
            }
        }

        /// <summary>
        /// Handler for a property selection, setting the control button.
        /// </summary>
        private void PropertySelectedCommandExecute()
        {
            if (this.CheckForEntity())
            {
                this.SetControlMode(ControlMode.Update);
                return;
            }

            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #region helper

        /// <summary>
        /// Method that closes the window, or clears it if in embedded control mode.
        /// </summary>
        private void Close()
        {
            //this.ClearForm();
            ViewModelLocator.ClearBPMaster();
            Messenger.Default.Send(Token.Message, Token.CloseBPMasterWindow);
        }

        /// <summary>
        /// Method that determines if a search can be made.
        /// </summary>
        /// <returns>A flag, indicating a valid search.</returns>
        private bool IsSearchValid()
        {
            return !(string.IsNullOrWhiteSpace(this.PartnerCode) && string.IsNullOrWhiteSpace(this.PartnerName)
                   && (this.PartnerType == null || this.PartnerType.Type.Equals(Strings.NoneSelected))
                   && (this.PartnerGroup == null || this.PartnerGroup.BPGroupName.Equals(Strings.NoneSelected)));
        }

        /// <summary>
        /// Displays the search screen.
        /// </summary>
        private void DisplaySearchScreen()
        {
            this.Locator.BPSearchData.SetPartners();
            Messenger.Default.Send(ViewType.BPSearchData, Token.SearchForBusinessPartner);
            if (this.SelectedBusinessPartner != null && this.SelectedBusinessPartner.Details.BPMasterID > 0)
            {
                this.Locator.BPSearchData.SelectedSearchPartner = this.SelectedBusinessPartner;
            }
        }

        /// <summary>
        /// Method that clears the ui, by creating a blank partner.
        /// </summary>
        private void ClearForm()
        {
            this.CreateBlankBusinessPartner();
            this.ClearContact();
            this.ClearAddress();
            this.DisplayCurrency =
                this.BusinessPartnerCurrencies.FirstOrDefault(x => x.Name.Equals(Strings.NoneSelected)); // TODO Temporary. Will change when we have implemented the payments.
        }

        /// <summary>
        /// Method that adds a new business partner.
        /// </summary>
        private void AddNewBusinessPartner()
        {
            #region validation

            var error = this.ValidateBusinessPartner();
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            try
            {
                var newPartner = this.CreateOrUpdateBusinessPartner();

                this.Log.LogDebug(this.GetType(),
                    string.Format("Partner added. Checking for auto price list creation. New partner price id:{0}, Auto Creating:{1}", newPartner.Details.PriceListID, ApplicationSettings.AutoCreateCustomerPriceList));
                if (newPartner.Details.PriceListID.IsNullOrZero() && ApplicationSettings.AutoCreateCustomerPriceList)
                {
                    var currencyID = this.DataManager.GetBusinessPartnerCurrencies().First().BPCurrencyID;
                    if (currencyID > 0)
                    {
                        var list = new PriceList();
                        list.CurrentPriceListName = newPartner.Details.Name;
                        list.BasePriceListID = NouvemGlobal.BasePriceList != null
                            ? NouvemGlobal.BasePriceList.BasePriceListID
                            : null;
                        list.Factor = 0;
                        list.NouRoundingOptionsID = 1;
                        list.NouRoundingRulesID = 3;
                        list.BPCurrencyID = currencyID;
                        var localPrice = this.DataManager.AddPriceList(list);
                      
                        if (localPrice != null)
                        {
                            this.PriceLists.Add(localPrice);
                            newPartner.Details.PriceListID = localPrice.PriceListID;
                            var priceMaster = this.DataManager.GetPriceList(localPrice.PriceListID);
                            if (priceMaster != null)
                            {
                                NouvemGlobal.PriceListMasters.Add(priceMaster);
                            }
                        }
                    }
                }

                if (this.DataManager.AddOrUpdateBusinessPartner(newPartner, this.selectedBusinessPartnerSnapshot))
                {
                    SystemMessage.Write(MessageType.Priority, Message.BusinessPartnerAdded);
                    this.CreateBlankBusinessPartner();
                    this.CheckForNewEntities(EntityType.BusinessPartner);
                    //NouvemGlobal.AddNewPartner(newPartner);
                    this.SetControlMode(ControlMode.OK);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.GetBusinessPartners();
                    }));
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.BusinessPartnerNotUpdated);
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.ToString());
            }
        }

        /// <summary>
        /// Handles a partner combobox edit.
        /// </summary>
        /// <param name="e">The edit parameter.</param>
        private void HandleNameEdit(ProcessNewValueEventArgs e)
        {
            #region validation

            if (this.SelectedBusinessPartner == null || e == null || string.IsNullOrEmpty(e.DisplayText))
            {
                return;
            }

            #endregion

            var newPartnerName = e.DisplayText;
            this.SelectedBusinessPartner.Details.Name = newPartnerName;
            this.PartnerName = newPartnerName;
        }

        /// <summary>
        /// Gets the partyner groups.
        /// </summary>
        private void GetPartnerGroups()
        {
            this.BusinessPartnerGroups =
                new ObservableCollection<BPGroup>(this.DataManager.GetBusinessPartnerGroups());
            this.BusinessPartnerGroups.Insert(0, new BPGroup { BPGroupName = Strings.NoneSelected });
            this.PartnerGroup = this.BusinessPartnerGroups.ElementAt(0);
        }

        #endregion

        #endregion
    }
}


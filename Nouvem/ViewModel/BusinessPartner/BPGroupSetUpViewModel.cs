﻿// -----------------------------------------------------------------------
// <copyright file="BPGroupSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.BusinessPartner
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class BPGroupSetUpViewModel : NouvemViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the BPGroupSetUpViewModel class.
        /// </summary>
        public BPGroupSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.Groups = new ObservableCollection<PartnerGroup>();

            #endregion

            this.GetPartnerGroups();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the properties.
        /// </summary>
        public ObservableCollection<PartnerGroup> Groups { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the groups.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateGroups();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the partner groups.
        /// </summary>
        private void UpdateGroups()
        {
            var localGroups = this.Groups.ToList();
            try
            {
                if (this.DataManager.AddOrUpdatePartnerGroups(ref localGroups))
                {
                    SystemMessage.Write(MessageType.Priority, Message.GroupsUpdated);
                    Messenger.Default.Send(Token.Message, Token.RefreshGroups);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.GroupsNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.GroupsNotUpdated);
            }

            this.DataManager.UpdateGlobalGroups(localGroups);

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearBPGroupSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseBPGroupSetUpWindow);
        }

        /// <summary>
        /// Gets the application partner groups.
        /// </summary>
        private void GetPartnerGroups()
        {
            this.Groups.Clear();
            foreach (var group in this.DataManager.GetPartnerGroups())
            {
                this.Groups.Add(group);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}


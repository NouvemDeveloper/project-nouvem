﻿// -----------------------------------------------------------------------
// <copyright file="CountryMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;

namespace Nouvem.ViewModel.Department
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class DepartmentViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The countries collection.
        /// </summary>
        private ObservableCollection<DepartmentLookup> departments = new ObservableCollection<DepartmentLookup>();

        /// <summary>
        /// The countries collection.
        /// </summary>
        private DepartmentLookup selectedDepartment;
     
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the DepartmentViewModel class.
        /// </summary>
        public DepartmentViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            this.GetDepartments();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected department.
        /// </summary>
        public DepartmentLookup SelectedDepartment
        {
            get
            {
                return this.selectedDepartment;
            }

            set
            {
                this.selectedDepartment = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the country Masters.
        /// </summary>
        public ObservableCollection<DepartmentLookup> Departments
        {
            get
            {
                return this.departments;
            }

            set
            {
                this.departments = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedDepartment != null)
            {
                this.selectedDepartment.Deleted = true;
                this.UpdateDepartments();
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateDepartments();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the country master group.
        /// </summary>
        private void UpdateDepartments()
        {
            try
            {
                if (this.DataManager.AddOrUpdateDepartments(this.Departments))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                    this.GetDepartments();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.CountryNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseDepartmentsWindow);
        }

        /// <summary>
        /// Gets the application departments.
        /// </summary>
        private void GetDepartments()
        {
            this.Departments.Clear();
            foreach (var department in this.DataManager.GetDeviceDepartments(0))
            {
                this.Departments.Add(department);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}


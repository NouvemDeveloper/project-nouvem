﻿// -----------------------------------------------------------------------
// <copyright file="QuoteViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Sales.ARQuote
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using System.Threading.Tasks;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    

    public class QuoteViewModel : SalesViewModelBase
    {
        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteViewModel"/> class.
        /// </summary>
        public QuoteViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
               t =>
               {
                   var product = t.Item2;
                   if (t.Item1 == ViewType.Quote)
                   {
                       if (product != null)
                       {
                           var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID };
                           this.SaleDetails.Add(detailToAdd);
                           this.SelectedSaleDetail = detailToAdd;
                       }
                   }
               });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.Quote)
                    {
                        this.SelectedPartner =
                            this.Customers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            #endregion

            #region command handler

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.Quote)), 
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() => this.RemoveItem(ViewType.Quote));

            #endregion

            this.GetLocalDocNumberings();
        }

        #endregion

        #region public interface

        #region property

        #endregion

        #region command

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.Sale = this.DataManager.GetQuoteByID(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetQuoteByLastEdit();
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                this.ShowSearchGridCommandExecute(Constant.Quote);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            Task.Factory.StartNew(this.GetAllSales);
            base.OnLoadingCommandExecute();
            this.ClearForm();
            this.GetLocalDocNumberings();
        }

        /// <summary>
        /// Override, to set the focus to the order vm.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            Task.Factory.StartNew(this.GetAllSales);
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if ((e.Cell.Property.Equals("INMasterID") || e.Cell.Property.Equals("PriceListID")) &&
                this.SelectedSaleDetail != null && this.SelectedSaleDetail.SaleDetailID == 0)
            {
                this.SelectedSaleDetail.UnitPrice = null;
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                var localSales = this.DataManager.GetAllQuotes(orderStatuses);
                this.CustomerSales = localSales
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllQuotes(orderStatuses)
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.Quote), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.Quote);
            }
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            base.ParsePartner();

            // now get the associated customer quotes.
            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            this.CustomerSales = this.DataManager.GetRecentQuotes(this.SelectedPartner.BPMasterID);
            var recentOrders = this.CustomerSales.OrderByDescending(x => x.CreationDate).Take(6).ToList();

            // We're binding the recent order headers to their dates.
            this.ResetOrderDates();

            if (!recentOrders.Any())
            {
                return;
            }

            if (recentOrders.Count > 0)
            {
                var order = recentOrders.ElementAt(0);
                this.Order1Date = order.CreationDate.ToString();
                ProductRecentOrderData.RecentOrder1 = order;
            }

            if (recentOrders.Count > 1)
            {
                var order = recentOrders.ElementAt(1);
                this.Order2Date = order.CreationDate.ToString();ProductRecentOrderData.RecentOrder2 = order;
            }

            if (recentOrders.Count > 2)
            {
                var order = recentOrders.ElementAt(2);
                this.Order3Date = order.CreationDate.ToString();
                ProductRecentOrderData.RecentOrder3 = order;
            }

            if (recentOrders.Count > 3)
            {
                var order = recentOrders.ElementAt(3);
                this.Order4Date = order.CreationDate.ToString();
                ProductRecentOrderData.RecentOrder4 = order;
            }

            if (recentOrders.Count > 4)
            {
                var order = recentOrders.ElementAt(4);
                this.Order5Date = order.CreationDate.ToString();
                ProductRecentOrderData.RecentOrder5 = order;
            }

            if (recentOrders.Count > 5)
            {
                var order = recentOrders.ElementAt(5);
                this.Order6Date = order.CreationDate.ToString();
                ProductRecentOrderData.RecentOrder6 = order;
            }

            this.ProductRecentOrderDatas = new ObservableCollection<ProductRecentOrderData>(this.DataManager.GetRecentOrderData(recentOrders));

            if (this.ProductRecentOrderDatas.Any())
            {
                // set focus to the recent orders grid.
                Messenger.Default.Send(Token.Message, Token.SetRecentOrdersFocus);
            }
        }
        
        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.RefreshSales();
            }));
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            this.CreateSale();
            this.UpdateDocNumbering();
            var newSaleId = this.DataManager.AddQuote(this.Sale);
            if (newSaleId > 0)
            {
                this.Sale.SaleID = newSaleId;
                this.AllSales.Add(this.Sale);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.QuoteCreated, this.NextNumber));
                //this.RefreshCustomerSales();
                this.ClearForm();
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
                this.GetAllSales();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.QuoteNotCreated);
            }
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            string error = string.Empty;

            if (this.SelectedPartner== null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            this.CreateSale();

            if (this.DataManager.UpdateQuote(this.Sale))
            {
                if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.QuoteUpdated, this.NextNumber));
                }
                else if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.QuoteCompleted, this.NextNumber));
                    this.ClearForm();
                }
                else if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.QuoteCancelled, this.NextNumber));
                    this.ClearForm();
                }

                this.GetAllSales();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.QuoteNotUpdated, this.NextNumber));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.ARQuote)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearQuote();
            Messenger.Default.Send(Token.Message, Token.CloseQuoteWindow);
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            #region validation

            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            #endregion

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoQuoteSelected);
                return;
            }

            #endregion

            if (this.SelectedDocumentCopyTo.Equals(Strings.Order))
            {
                CopyingDocument = true;
                this.CopyQuoteToOrder();
            }
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var documents = this.DataManager.GetOrders(this.SelectedPartner.BPMasterID);

            if (!documents.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Orders));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(documents, ViewType.Quote), Token.SearchForSale);
            CopyingDocument = true;
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution



        #endregion

        #region helper

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.Quote)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                var localSales = this.DataManager.GetAllQuotes(orderStatuses);
                this.CustomerSales = localSales
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllQuotes(orderStatuses)
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.Quote);
            }
        }
       
        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.Quote);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        /// <summary>
        /// Gets all the quotes.
        /// </summary>
        private void GetAllSales()
        {
            this.AllSales = this.DataManager.GetAllQuotes(this.DocStatusesOpen);
        }

        /// <summary>
        /// Copies the current quote to a sale order.
        /// </summary>
        private void CopyQuoteToOrder()
        {
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
            Messenger.Default.Send(ViewType.Order);

            CopyingDocument = true;
            this.Locator.Order.CopySale(this.Sale);

            //if (newSaleId > 0)
            //{//    this.Sale.SaleID = newSaleId;
            //    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCreated, this.SelectedDocNumbering.NextNumber));
            //    this.RefreshCustomerSales();
            //}
            //else
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.OrderNotCreated);
            //}
        }

        #endregion

        #endregion
    }
}

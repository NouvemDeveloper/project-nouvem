﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Purchases.APInvoice;
using Nouvem.ViewModel.Sales.ARInvoice;

namespace Nouvem.ViewModel.Sales.ARReturn
{
    public class CreditNoteCreationViewModel : InvoiceCreationViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditNoteCreationViewModel"/> class.
        /// </summary>
        public CreditNoteCreationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.SelectedSales?.Clear();
            this.SelectedSales = null;
            base.OnLoadingCommandExecute();
            //this.SelectedSale = null;
            this.SelectedSales.Clear();
            this.Refresh();
        }

        /// <summary>
        /// Creates the invoices.
        /// </summary>
        protected override void CreateInvoices()
        {
            try
            {
                if (this.SelectedSales != null)
                {
                    if (this.SelectedSales.Any())
                    {
                        var invoiceNos = new List<int>();

                        ProgressBar.SetUp(0, this.SelectedSales.Count * 2);
                        foreach (var localInvoiceToCreate in this.SelectedSales)
                        {
                            this.DataManager.PriceReturnsDocket(localInvoiceToCreate.SaleID);
                            var invoiceToCreate = this.DataManager.GetARReturnById(localInvoiceToCreate.SaleID);
                            ProgressBar.Run();

                            invoiceToCreate.BaseDocumentReferenceID = invoiceToCreate.SaleID;
                            invoiceToCreate.CreationDate = DateTime.Now;
                            invoiceToCreate.DocumentDate = DateTime.Today;
                            invoiceToCreate.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                            var newSale = this.DataManager.AddARReturnInvoice(invoiceToCreate);
                            var newSaleId = newSale.Item1;
                            var docNumber = newSale.Item2;
                            if (newSaleId > 0)
                            {
                                invoiceNos.Add(invoiceToCreate.Number);
                            }
                            else
                            {
                                SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
                            }

                            ProgressBar.Run();
                        }

                        if (invoiceNos.Any())
                        {
                            var numbers = invoiceNos.Count == 1 ? invoiceNos.First().ToString() : string.Join(",", invoiceNos.Select(x => x.ToString()));
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCreated, numbers));
                            this.Refresh();
                        }
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Creates a consolidated invoice.
        /// </summary>
        protected override void CreateConsolidatedInvoice()
        {
            try
            {
                if (this.SelectedSales != null)
                {
                    if (this.SelectedSales.Any())
                    {
                        #region validation

                        var customers = this.SelectedSales.Select(x => x.BPCustomer.BPMasterID).ToList();
                        if (customers.Distinct().Count() != 1)
                        {
                            // attempting to consolidate invoices from different customers, so exit with error message
                            SystemMessage.Write(MessageType.Issue, Message.ConsolidateAPInvoiceError);
                            return;
                        }

                        #endregion

                        var intakeGrandTotalIncVat = 0M;
                        var intakeSubTotalexVat = 0M;
                        var intakeDetails = new List<SaleDetail>();
                        var intakeIds = new List<int>();
                        ProgressBar.SetUp(0, this.SelectedSales.Count);
                        foreach (var localDispatch in this.SelectedSales)
                        {
                            ProgressBar.Run();
                            this.DataManager.PriceReturnsDocket(localDispatch.SaleID);
                            var intake = this.DataManager.GetARReturnById(localDispatch.SaleID);

                            intakeIds.Add(intake.SaleID);
                            intakeSubTotalexVat += intake.SubTotalExVAT.ToDecimal();
                            intakeGrandTotalIncVat += intake.GrandTotalIncVAT.ToDecimal();

                            foreach (var intakeDetail in intake.SaleDetails)
                            {
                                var productOnIntake =
                                    intakeDetails.FirstOrDefault(x => x.INMasterID == intakeDetail.INMasterID);
                                if (productOnIntake == null)
                                {
                                    intakeDetails.Add(intakeDetail);
                                }
                                else
                                {
                                    if (productOnIntake.TotalExVAT == null)
                                    {
                                        productOnIntake.TotalExVAT = 0;
                                    }

                                    if (productOnIntake.TotalIncVAT == null)
                                    {
                                        productOnIntake.TotalIncVAT = 0;
                                    }

                                    if (productOnIntake.QuantityReceived == null)
                                    {
                                        productOnIntake.QuantityReceived = 0;
                                    }

                                    if (productOnIntake.WeightReceived == null)
                                    {
                                        productOnIntake.WeightReceived = 0;
                                    }

                                    productOnIntake.LoadingSale = true;
                                    productOnIntake.TotalExVAT += intakeDetail.TotalExVAT.ToDecimal();
                                    productOnIntake.TotalIncVAT += intakeDetail.TotalIncVAT.ToDecimal();
                                    productOnIntake.QuantityReceived += intakeDetail.QuantityReceived.ToDecimal();
                                    productOnIntake.WeightReceived += intakeDetail.WeightReceived.ToDecimal();
                                    productOnIntake.LoadingSale = false;
                                }
                            }
                        }

                        var invoiceToCreate = this.SelectedSales.First();
                        invoiceToCreate.SubTotalExVAT = intakeSubTotalexVat;
                        invoiceToCreate.GrandTotalIncVAT = intakeGrandTotalIncVat;
                        invoiceToCreate.SaleDetails = intakeDetails;
                        //invoiceToCreate.Number = documentNumber.CurrentNumber;
                        //invoiceToCreate.DocumentNumberingID = documentNumber.DocumentNumberingID;
                        invoiceToCreate.CreationDate = DateTime.Now;
                        invoiceToCreate.DocumentDate = DateTime.Today;
                        invoiceToCreate.BaseDocumentReferenceID = null;
                        invoiceToCreate.IdsToMarkAsInvoiced = intakeIds;
                        invoiceToCreate.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                        var newSale = this.DataManager.AddARReturnInvoice(invoiceToCreate);
                        var newSaleId = newSale.Item1;

                        if (newSaleId > 0)
                        {
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCreated, newSaleId));
                            this.Refresh();
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
                        }
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        protected override void Refresh()
        {
            this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetARReturnsNonInvoiced());
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseCreditNoteCreationWindow);
                this.FilteredSales?.Clear();
                this.ConsolidatedInvoice = false;
                ViewModelLocator.ClearCreditNoteCreation();
            }
        }

        #endregion

        #region private



        #endregion
    }
}


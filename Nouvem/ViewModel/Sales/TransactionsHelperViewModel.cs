﻿// -----------------------------------------------------------------------
// <copyright file="TransactionsHelperViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;
using Nouvem.ViewModel.Attribute;

namespace Nouvem.ViewModel.Sales
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.ViewModel;

    public class TransactionsHelperViewModel : AttributesViewModel
    {
       #region field

        /// <summary>
        /// The traceability data passed when waiting for scales stability.
        /// </summary>
        protected List<TraceabilityResult> waitForStabilityTraceabilityData;

        /// <summary>
        /// The scales in motion wait timer.
        /// </summary>
        protected DispatcherTimer scalesMotionTimer = new DispatcherTimer();

        /// <summary>
        /// The tick count for the scales to stabilise.
        /// </summary>
        protected int scalesStabilityWaitTicks;

        /// <summary>
        /// The temp.
        /// </summary>
        private decimal? temperature;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionsHelperViewModel"/> class.
        /// </summary>
        public TransactionsHelperViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.Temp, s =>
            {
                this.Temperature = s.ToDecimal();
            });

            #endregion

            #region command handler

            this.RecordTempCommand = new RelayCommand(() => Global.Keypad.Show(KeypadTarget.Temp));

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public decimal? Temperature
        {
            get
            {
                return this.temperature;
            }

            set
            {
                this.temperature = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to record a temp.
        /// </summary>
        public ICommand RecordTempCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        /// <param name="transactions">The transaction traceability data.</param>
        protected StockTransactionData ProcessTransactions(IList<TraceabilityResult> transactions, StockTransaction trans)
        {
            this.Log.LogDebug(this.GetType(), "ProcessTransactions(): Processing transactions");

            var stockTransaction = trans;
            var transTraceabilities = new List<TransactionTraceability>();

            // add the transaction weight to our stock transaction, and other relevant data.
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId;
            stockTransaction.WarehouseID = NouvemGlobal.DispatchId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;

            this.Log.LogDebug(this.GetType(), string.Format("Adding stock transaction: TransactionWeight:{0}, GrossWeight:{1}, Tare:{2}," +
                                                           "Trans Qty:{3}, INMasterID:{4}" +
                                                               "Transaction Date:{5}, NouTransactionType:{6}, Warehouse:{7}",
                                                               stockTransaction.TransactionWeight,
                                                               stockTransaction.GrossWeight,
                                                               stockTransaction.Tare,
                                                               stockTransaction.TransactionQTY,
                                                               stockTransaction.INMasterID,
                                                               stockTransaction.TransactionDate,
                                                               stockTransaction.NouTransactionTypeID,
                                                               stockTransaction.WarehouseID));

            foreach (var transaction in transactions)
            {
                // add to our transaction traceability result
                var traceability = this.ParseToTransactionTraceability(transaction);
                transTraceabilities.Add(traceability);
            }

            var transactionData = new StockTransactionData
            {
                Transaction = stockTransaction,
                TransactionTraceabilities = transTraceabilities
            };

            return transactionData;
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        /// <param name="transactions">The transaction traceability data.</param>
        protected StockTransactionData ProcessIntoProductionTransactions(IList<TraceabilityResult> transactions, StockTransaction trans)
        {
            this.Log.LogDebug(this.GetType(), "ProcessTransactions(): Processing transactions");

            var stockTransaction = trans;
            var transTraceabilities = new List<TransactionTraceability>();

            foreach (var transaction in transactions)
            {
                // add to our transaction traceability result
                var traceability = this.ParseToTransactionTraceability(transaction);
                transTraceabilities.Add(traceability);
            }

            var transactionData = new StockTransactionData
            {
                Transaction = stockTransaction,
                TransactionTraceabilities = transTraceabilities
            };

            return transactionData;
        }

        /// <summary>
        /// Process, and add the batch traceability data to the sale detail.
        /// </summary>
        /// <param name="results">The batch traceability data.</param>
        protected List<BatchTraceability> ProcessBatchTraceabilities(IList<TraceabilityResult> results)
        {
            return (from result in results
                    select this.ParseToBatchTraceability(result)).ToList();
        }

        /// <summary>
        /// Parses a traceability result to a transaction traceability.
        /// </summary>
        /// <param name="result">The traceability result data.</param>
        /// <returns>The parsed transaction traceability.</returns>
        protected TransactionTraceability ParseToTransactionTraceability(TraceabilityResult result)
        {
            return new TransactionTraceability
            {
                NouTraceabilityMethodID = result.NouTraceabilityMethodID,
                DateMasterID = result.DatemasterID,
                TraceabilityTemplateMasterID = result.TraceabilityMasterID,
                QualityMasterID = result.QualityMasterID,
                Value = result.TraceabilityValue,
                DynamicName = result.FieldName
            };
        }

        /// <summary>
        /// Parses a traceability result to a batch traceability.
        /// </summary>
        /// <param name="result">The traceability result data.</param>
        /// <returns>The parsed batch traceability.</returns>
        protected BatchTraceability ParseToBatchTraceability(TraceabilityResult result)
        {
            return new BatchTraceability
            {
                NouTraceabilityMethodID = result.NouTraceabilityMethodID,
                DateMasterID = result.DatemasterID,
                TraceabilityTemplateMasterID = result.TraceabilityMasterID,
                QualityMasterID = result.QualityMasterID,
                Value = result.TraceabilityValue
            };
        }

        #endregion

        #region private

       

        #endregion
            
        protected override void ControlSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }
    }
}

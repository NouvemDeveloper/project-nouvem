﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Sales.ARInvoice
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class InvoiceViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The selected invoice.
        /// </summary>
        private Sale selectedInvoice;

        /// <summary>
        /// The active invoices.
        /// </summary>
        private ObservableCollection<Sale> invoices = new ObservableCollection<Sale>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceViewModel"/> class.
        /// </summary>
        public InvoiceViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });


            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.Invoice)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID };
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.Invoice)
                    {
                        this.SelectedPartner =
                            this.Customers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            #endregion

            #region command handler

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.Invoice)),
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() => this.RemoveItem(ViewType.Order));

            // Handle the invoice selection.
            this.InvoiceSelectedCommand = new RelayCommand(() =>
            {
                this.Sale = this.SelectedInvoice;
                this.SelectedTab = 0;
            });

            #endregion
       
            this.GetLocalDocNumberings();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the current invoice id.
        /// </summary>
        public int CurrentInvoiceId { get; set; }

        /// <summary>
        /// Gets or sets the invoices.
        /// </summary>
        public ObservableCollection<Sale> Invoices
        {
            get
            {
                return this.invoices;
            }

            set
            {
                this.invoices = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected invoice.
        /// </summary>
        public Sale SelectedInvoice
        {
            get
            {
                return this.selectedInvoice;
            }

            set
            {
                this.selectedInvoice = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

         /// <summary>
        /// Gets the command to handle the selected invoice.
        /// </summary>
        public ICommand InvoiceSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Show the sales.
        /// </summary>
        public void ShowAllSales()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Copies a sale to another document.
        /// </summary>
        /// <param name="saleToCopy">The sale to copy to.</param>
        public override void CopySale(Sale saleToCopy)
        {
            this.Log.LogDebug(this.GetType(), "CopySale(): Copying sale");

            if (saleToCopy.SaleDetails != null)
            {
                // need to attach the inventory item, as it's needed for the in master snapshot.
                foreach (var detail in saleToCopy.SaleDetails)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }
            }

            if (ApplicationSettings.AccountsCustomer.CompareIgnoringCase(Customer.Woolleys))
            {
                this.Sale.DocumentDate = saleToCopy.CreationDate;
            }

            this.Sale = saleToCopy;

            //var dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);

            this.Sale.SaleID = 0;
            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Auto copies the dispatch docket to an invoice.
        /// </summary>
        public void AutoCopyInvoice(Sale invoice)
        {
            this.Log.LogInfo(this.GetType(), "Auto copying docket to invoice");
            this.CopySale(invoice);

            // remove empty lines.
            var localDetails = new List<SaleDetail>();
            foreach (var detail in invoice.SaleDetails)
            {
                if (detail.WeightDelivered.ToDouble() > 0 || detail.QuantityDelivered.ToDouble() > 0 ||
                    detail.TotalIncVAT > 0)
                {
                    localDetails.Add(detail);
                }
            }

            invoice.SaleDetails = localDetails;
            this.AddSale(invoice);
            this.Log.LogInfo(this.GetType(), "Auto copying docket to invoice complete");
        }

        /// <summary>
        /// Calls the search screen with the non invoiced dispatch dockets displayed.
        /// </summary>
        public void DisplayDispatches()
        {
            var dispatches = this.DataManager.GetNonInvoicedARDispatches(ApplicationSettings.PriceInvoiceDocket);
            this.SetActiveDocument(this);

            //if (ViewModelLocator.InvoiceCreationStatic.IsFormLoaded)
            //{
            //    Messenger.Default.Send(dispatches, Token.DisplayDispatches);
            //    return;
            //}

            //Messenger.Default.Send(new List<Sale>(), Token.CreateInvoice);
            Application.Current.Dispatcher.Invoke(() =>
            {
                ViewModelLocator.InvoiceCreationStatic.SetInvoices(dispatches);
                Messenger.Default.Send(new List<Sale>(), Token.CreateInvoice);
            });
        }

        #endregion

        #endregion

        #region protected

        #region override

        ///// <summary>
        ///// Handles a document status change.
        ///// </summary>
        //protected override void HandleDocStatusChange()
        //{
        //    if (!this.EntitySelectionChange)
        //    {
        //        if (this.Sale != null && this.Sale.SaleID > 0)
        //        {
        //            if (this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID 
        //                && this.SelectedDocStatus.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID)
        //            {
        //                SystemMessage.Write(MessageType.Issue, Message.ExportedInvoiceWarning);
        //                this.SelectedDocStatus =
        //                this.DocStatusItems.FirstOrDefault(
        //                    x => x.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID);
        //                this.RaisePropertyChanged("SelectedDocStatus");
        //                return;
        //            }

        //            if (this.SelectedDocStatus.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID)
        //            {
        //                  this.SetControlMode(ControlMode.Update);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.Sale = this.DataManager.GetInvoicesById(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetInvoiceByLastEdit();
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(66, this.Sale.SaleID.ToString(), !preview);
            return;


            //var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0 || this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidInvoiceDocket);
                return;
            }

            #endregion

            var invoiceId = this.Sale.SaleID;
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { invoiceId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.InvoiceDocket, ReportParameters = reportParam };

            try
            {
                if (preview)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                this.ShowSearchGridCommandExecute(Constant.Invoice);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllInvoicesByDate(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateInvoice).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllInvoicesByDate(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateInvoice).ToList();
            }
           
            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.Invoice), Token.SearchForSale);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateInvoice);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.Invoice);
            }
        }

        /// <summary>
        /// Override, to set the focus to the order vm.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if ((e.Cell.Property.Equals("INMasterID") || e.Cell.Property.Equals("PriceListID")) &&
                this.SelectedSaleDetail != null && this.SelectedSaleDetail.SaleDetailID == 0)
            {
                this.SelectedSaleDetail.UnitPrice = null;
            }

            if (this.Sale != null && this.Sale.SaleID > 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }
        }

        protected override bool AllowPriceChangeToPriceBook()
        {
            return !ApplicationSettings.PriceInvoiceDocket;
        }

        /// <summary>
        /// Updates the selected sale detail totals.
        /// </summary>
        protected override void UpdateSaleDetailTotals()
        {
            if (this.SelectedSaleDetail != null)
            {
                // goods in and dispatch will override.
                this.SelectedSaleDetail.UpdateTotal(false,true);
            }
        }

        /// <summary>
        /// Overide the selected sale, to reinstate the sale id.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;

            if (this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
            {
                this.SetControlMode(ControlMode.Add);
            }
            else if (this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                this.SetControlMode(ControlMode.Update);
            }
            else
            {
                this.SetControlMode(ControlMode.OK);
            }

            var localMode = this.CurrentMode;
            if (this.SelectedDocStatus == null && this.DocStatusItems != null)
            {
                this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
                this.SetControlMode(localMode);
            }

            if (this.Sale != null && this.Sale.SaleID > 0)
            {
                if (this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusExported.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Issue, Message.ExportedInvoiceWarning);
                    this.IsReadOnly = true;
                    this.DisableDocStatusChange = true;
                    this.SetControlMode(ControlMode.OK);
                }
                else if (ApplicationSettings.DisallowInvoiceReOpening)
                {
                    SystemMessage.Write(MessageType.Issue, Message.InvoiceEditStop);
                    this.IsReadOnly = true;
                    this.DisableDocStatusChange = true;
                    this.SetControlMode(ControlMode.OK);
                }
            }
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated invoices.
        /// </summary>
        protected override void ParsePartner()
        {
            base.ParsePartner();

            // now get the associated customer quotes.
            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            this.CustomerSales = new List<Sale>(); //this.DataManager.GetInvoices(this.SelectedPartner.BPMasterID);
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.RefreshSales();
            }));
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            this.CreateSale();
         
            if (this.Sale.DocumentDate == null && this.Sale.BaseDocumentReferenceID == 0)
            {
                this.Log.LogError(this.GetType(), "AddSale(): Doc date null , base doc ref id = 0");
                return;
            }

            this.Sale.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
            
            var newSale = this.DataManager.AddNewInvoice(this.Sale);
            var newSaleId = newSale.Item1;
            var docNumber = newSale.Item2;
            if (newSaleId > 0)
            {
                this.SelectedDocNumbering = docNumber;
                this.Sale.SaleID = newSaleId;
                this.AllSales.Add(this.Sale);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCreated, this.NextNumber));
                this.ClearForm();
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
            }
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            string error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            this.CreateSale();
            this.Sale.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;

            if (this.DataManager.UpdateInvoice(this.Sale))
            {
                if (ApplicationSettings.PriceInvoiceDocket)
                {
                    this.DataManager.PriceInvoice(0, this.Sale.SaleID,
                        NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
                }

                if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceUpdated, this.NextNumber));
                    this.ClearForm();
                }
                else if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCompleted, this.NextNumber));
                    this.ClearForm();
                }
                else if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCancelled, this.NextNumber));
                    Messenger.Default.Send(Token.Message, Token.RefreshInvoices);
                    this.ClearForm();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.InvoiceNotUpdated, this.NextNumber));
            }

            Messenger.Default.Send(Token.Message, Token.RefreshInvoices);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings =
                new ObservableCollection<DocNumber>(
                    this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.ARInvoice)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = new List<NouDocStatu>
            {
                NouvemGlobal.NouDocStatusActive,
                NouvemGlobal.NouDocStatusComplete,
                NouvemGlobal.NouDocStatusExported,
                NouvemGlobal.NouDocStatusCancelled
            };

            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            Task.Factory.StartNew(this.GetAllInvoices);
            this.GetDocStatusItems();
            base.OnLoadingCommandExecute();
            this.ClearForm();
            this.GetLocalDocNumberings();
            Messenger.Default.Register<string>(this, Token.CopySuccessful, s =>
            {
                if (this.AutoCopying)
                {
                    SystemMessage.Write(MessageType.Priority, Message.OrderAndDispatchCreated);
                }
            });
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            base.OnUnloadedCommandExecute();
            Messenger.Default.Unregister<string>(this, Token.CopySuccessful);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearInvoice();
            Messenger.Default.Send(Token.Message, Token.CloseInvoiceWindow);
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        [Obsolete]
        protected override void CopyDocumentTo()
        {
            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderSelected);
                return;

            }

            #endregion
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var dispatches = this.DataManager.GetNonInvoicedARDispatches(this.SelectedPartner.BPMasterID);

            if (!dispatches.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Dispatch));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(dispatches, ViewType.ARDispatch), Token.SearchForSale);
            CopyingDocument = true;
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution



        #endregion

        #region helper

        /// <summary>
        /// Adds an invoice, auto copied from dispatch.
        /// </summary>
        public void AddSale(Sale invoice)
        {
            if (invoice.DocumentDate == null && invoice.BaseDocumentReferenceID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
                this.Log.LogError(this.GetType(), "AddSale():- Doc date null, base doc ref id = 0");
                return;
            }

            invoice.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
            // this.UpdateDocNumbering();
            var newSale = this.DataManager.AddNewInvoice(invoice);
            var newSaleId = newSale.Item1;
            var docNumber = newSale.Item2;
            this.CurrentInvoiceId = newSaleId;
            if (docNumber != null && docNumber.DocumentNumberingTypeID == 1000)
            {
                this.ClearForm();
                this.RefreshDocStatusItems();
            }
            else if (newSaleId > 0)
            {
                this.SelectedDocNumbering = docNumber;
                this.Sale.SaleID = newSaleId;
                this.AllSales.Add(invoice);
                SystemMessage.Write(MessageType.Priority, Message.DispatchCompleteAndInvoiceCreated);
                this.ClearForm();
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
            }
        }

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.Invoice)
            {
                return;
            }

            var orderStatuses = this.DocStatusesClosed;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllInvoicesByDate(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateInvoice).ToList();
                
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllInvoicesByDate(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateInvoice).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateInvoice);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.Invoice);
            }
        }

        /// <summary>
        /// Gets all the invoices.
        /// </summary>
        private void GetAllInvoices()
        {
            var docStatuses = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
            };

            this.AllSales = this.DataManager.GetAllInvoices(docStatuses);
            this.Invoices = new ObservableCollection<Sale>(this.AllSales.Take(200));
        }

        /// <summary>
        /// Displays the base document.
        /// </summary>
        private void ShowBaseDocument()
        {
            try
            {
                if (this.Sale.BaseDocumentReferenceID == null)
                {
                    var docs = this.DataManager.GetARDispatchesByInvoice(this.Sale.SaleID);

                    if (!docs.Any())
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                        return;
                    }

                    var id = 1;
                    foreach (var doc in docs)
                    {
                        doc.NodeID = id++;
                        doc.ParentID = id++;
                        doc.DocumentTrailType = Constant.Dispatch;
                    }

                    SystemMessage.Write(MessageType.Priority, Message.MultipleInvoiceDispatchDockets);
                    this.Locator.DocumentTrail.SetDocuments(docs);
                    Messenger.Default.Send(ViewType.DocumentTrail);
                }

                var baseDoc = this.DataManager.GetARDispatchById(this.Sale.BaseDocumentReferenceID.ToInt());
                this.Locator.ARDispatch.DisplayModeOnly = true;
                Messenger.Default.Send(ViewType.ARDispatch);
                this.Locator.ARDispatch.Sale = baseDoc;
                SystemMessage.Write(MessageType.Priority, Message.BaseDocumentLoaded);
            }
            finally
            {
                this.Locator.ARDispatch.DisplayModeOnly = false;
            }
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.Invoice);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        #endregion

        #endregion
    }
}

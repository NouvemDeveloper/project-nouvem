﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceCreationViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Sales.ARInvoice
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class InvoiceCreationViewModel : InvoiceCreationBaseViewModel
    {
        #region field

        /// <summary>
        /// The selected grid sales.
        /// </summary>
        //private ObservableCollection<Sale> selectedSales = new ObservableCollection<Sale>();

        /// <summary>
        /// The consolidated invoice flag.
        /// </summary>
        private bool consolidatedInvoice;

        /// <summary>
        /// The running total.
        /// </summary>
        private decimal runningTotal;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceCreationViewModel"/> class.
        /// </summary>
        public InvoiceCreationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the incoming customers sales.
            Messenger.Default.Register<IList<Sale>>(this, Token.DisplayDispatches, list => this.FilteredSales = new ObservableCollection<Sale>(list.OrderByDescending(x => x.SaleID)));

            #endregion

            #region command handler

            // The handler for the grid selection changed event.
            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);

            // Display the selected dispatch docket.
            this.ShowDispatchDocketCommand = new RelayCommand(() =>
            {
                try
                {
                    this.Locator.ARDispatch.DisplayModeOnly = true;
                    Messenger.Default.Send(ViewType.ARDispatch);
                    this.Locator.ARDispatch.Sale = this.SelectedSales.FirstOrDefault();
                    SystemMessage.Write(MessageType.Priority, Message.DispatchScreenLoaded);
                }
                finally
                {
                    this.Locator.ARDispatch.DisplayModeOnly = false;
                }
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected invoice.
        /// </summary>
        //public Sale SelectedInvoice { get; set; }
  
        /// <summary>
        /// Gets or sets a value indicating whether the selected dispatches are to be consolidated into 1 invoice.
        /// </summary>
        public bool ConsolidatedInvoice
        {
            get
            {
                return this.consolidatedInvoice;
            }

            set
            {
                this.consolidatedInvoice = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the running total.
        /// </summary>
        public decimal RunningTotal
        {
            get
            {
                return this.runningTotal;
            }

            set
            {
                this.runningTotal = value;
                this.RaisePropertyChanged();
            }
        }

        ///// <summary>
        ///// Gets or sets the selected search sales.
        ///// </summary>
        //public ObservableCollection<Sale> SelectedSales
        //{
        //    get
        //    {
        //        return this.selectedSales;
        //    }

        //    set
        //    {
        //        if (value != null)
        //        {
        //            this.selectedSales = value;
        //            this.RaisePropertyChanged();

        //            if (value.Any())
        //            {
        //                this.SetControlMode(ControlMode.Add);
        //            }
        //            else
        //            {
        //                this.SetControlMode(ControlMode.OK);
        //            }
        //        }
        //    }
        //}

        #endregion

        #region method

        /// <summary>
        /// Sets the current invoices.
        /// </summary>
        /// <param name="sales">The invoices to set.</param>
        public void SetInvoices(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleID));
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to display the selected dispatch docket.
        /// </summary>
        public ICommand ShowDispatchDocketCommand { get; private set; }

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.SelectedSales?.Clear();
            this.SelectedSales = null;
            base.OnLoadingCommandExecute();
            //this.SelectedSale = null;
            this.SelectedSales.Clear();
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Override, to only allow update/ok.
        /// </summary>
        /// <param name="mode">The current mode.</param>
        protected override void ControlCommandExecute(string mode)
        {
            if (this.CurrentMode == ControlMode.Find || this.CurrentMode == ControlMode.Update)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    if (this.ConsolidatedInvoice)
                    {
                        this.CreateConsolidatedInvoice();
                    }
                    else
                    {
                        this.CreateInvoices();
                    }

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        protected virtual void Refresh()
        {
            this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetNonInvoicedARDispatches(ApplicationSettings.PriceInvoiceDocket));
        }

        /// <summary>
        /// Creates the invoices.
        /// </summary>
        protected virtual void CreateInvoices()
        {
            try
            {
                if (this.SelectedSales != null)
                {
                    if (this.SelectedSales.Any())
                    {
                        var invoiceNos = new List<int>();

                        ProgressBar.SetUp(0, this.SelectedSales.Count * 2);
                        foreach (var localInvoiceToCreate in this.SelectedSales)
                        {
                            int invoiceNo;
                            if (ApplicationSettings.PriceInvoiceDocket)
                            {
                                invoiceNo = this.DataManager.PriceInvoice(localInvoiceToCreate.SaleID,0,
                                    NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
                            }
                            else
                            {
                                this.DataManager.PriceDispatchDocket(localInvoiceToCreate.SaleID);
                                invoiceNo = this.DataManager.CopyDispatchToInvoice(localInvoiceToCreate.SaleID,
                                    NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
                            }
                            //var invoiceToCreate = this.DataManager.GetARDispatchById(localInvoiceToCreate.SaleID);
                            ProgressBar.Run();

                            //var currentDoc = this.DataManager.GetDocumentNumbers()
                            //.LastOrDefault(x => x.DocName.DocumentName.Equals(Constant.ARInvoice)
                            //               && x.DocType.Name.Equals(Constant.Standard));

                            //if (currentDoc == null)
                            //{
                            //    currentDoc = new DocNumber();
                            //}

                            //var documentNumber = this.DataManager.SetDocumentNumber(currentDoc.DocumentNumberingID);
                            //invoiceToCreate.Number = documentNumber.CurrentNumber;
                            //invoiceToCreate.DocumentNumberingID = documentNumber.DocumentNumberingID;
                            //invoiceToCreate.BaseDocumentReferenceID = invoiceToCreate.SaleID;
                            //invoiceToCreate.CreationDate = DateTime.Now;
                            //invoiceToCreate.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                            //var newSale = this.DataManager.AddNewInvoice(invoiceToCreate);
                            //var newSaleId = newSale.Item1;
                            //var docNumber = newSale.Item2;
                            if (invoiceNo > 0)
                            {
                                invoiceNos.Add(invoiceNo);
                            }
                            else
                            {
                                SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
                            }

                            ProgressBar.Run();
                        }

                        if (invoiceNos.Any())
                        {
                            var numbers = invoiceNos.Count == 1 ? invoiceNos.First().ToString() : string.Join(",", invoiceNos.Select(x => x.ToString()));
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCreated, numbers));
                            this.Refresh();
                        }
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Creates a consolidated invoice.
        /// </summary>
        protected virtual void CreateConsolidatedInvoice()
        {
            try
            {
                if (this.SelectedSales != null)
                {
                    if (this.SelectedSales.Any())
                    {
                        #region validation

                        var customers = this.SelectedSales.Select(x => x.BPCustomer.BPMasterID).ToList();
                        if (customers.Distinct().Count() != 1)
                        {
                            // attempting to consolidate invoices from different customers, so exit with error message
                            SystemMessage.Write(MessageType.Issue, Message.ConsolidatedInvoiceError);
                            return;
                        }

                        #endregion

                        var dispatchGrandTotalIncVat = 0M;
                        var dispatchSubTotalexVat = 0M;
                        var dispatchDetails = new List<SaleDetail>();
                        var dispatchIds = new List<int>();
                        ProgressBar.SetUp(0, this.SelectedSales.Count);
                        foreach (var localDispatch in this.SelectedSales)
                        {
                            ProgressBar.Run();
                            this.DataManager.PriceDispatchDocket(localDispatch.SaleID);
                            var dispatch = this.DataManager.GetARDispatchById(localDispatch.SaleID);

                            dispatchIds.Add(dispatch.SaleID);
                            dispatchSubTotalexVat += dispatch.SubTotalExVAT.ToDecimal();
                            dispatchGrandTotalIncVat += dispatch.GrandTotalIncVAT.ToDecimal();

                            foreach (var dispatchDetail in dispatch.SaleDetails)
                            {
                                var productOnDispatch =
                                    dispatchDetails.FirstOrDefault(x => x.INMasterID == dispatchDetail.INMasterID);
                                if (productOnDispatch == null)
                                {
                                    dispatchDetails.Add(dispatchDetail);
                                }
                                else
                                {
                                    if (productOnDispatch.TotalExVAT == null)
                                    {
                                        productOnDispatch.TotalExVAT = 0;
                                    }

                                    if (productOnDispatch.TotalIncVAT == null)
                                    {
                                        productOnDispatch.TotalIncVAT = 0;
                                    }

                                    if (productOnDispatch.QuantityDelivered == null)
                                    {
                                        productOnDispatch.QuantityDelivered = 0;
                                    }

                                    if (productOnDispatch.WeightDelivered == null)
                                    {
                                        productOnDispatch.WeightDelivered = 0;
                                    }

                                    productOnDispatch.LoadingSale = true;
                                    productOnDispatch.TotalExVAT += dispatchDetail.TotalExVAT.ToDecimal();
                                    productOnDispatch.TotalIncVAT += dispatchDetail.TotalIncVAT.ToDecimal();
                                    productOnDispatch.QuantityDelivered += dispatchDetail.QuantityDelivered.ToDecimal();
                                    productOnDispatch.WeightDelivered += dispatchDetail.WeightDelivered.ToDecimal();
                                    productOnDispatch.LoadingSale = false;
                                }
                            }
                        }

                        var currentDoc = this.DataManager.GetDocumentNumbers()
                        .LastOrDefault(x => x.DocName.DocumentName.Equals(Constant.ARInvoice)
                                       && x.DocType.Name.Equals(Constant.Standard));

                        if (currentDoc == null)
                        {
                            currentDoc = new DocNumber();
                        }

                        //var documentNumber = this.DataManager.SetDocumentNumber(currentDoc.DocumentNumberingID);

                        var invoiceToCreate = this.SelectedSales.First();
                        invoiceToCreate.SubTotalExVAT = dispatchSubTotalexVat;
                        invoiceToCreate.GrandTotalIncVAT = dispatchGrandTotalIncVat;
                        invoiceToCreate.SaleDetails = dispatchDetails;
                        //invoiceToCreate.Number = documentNumber.CurrentNumber;
                        //invoiceToCreate.DocumentNumberingID = documentNumber.DocumentNumberingID;
                        invoiceToCreate.CreationDate = DateTime.Now;
                        invoiceToCreate.BaseDocumentReferenceID = null;
                        invoiceToCreate.IdsToMarkAsInvoiced = dispatchIds;
                        invoiceToCreate.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                        var newSale = this.DataManager.AddNewInvoice(invoiceToCreate);
                        var newSaleId = newSale.Item1;

                        if (newSaleId > 0)
                        {
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceCreated, newSaleId));
                            this.Refresh();
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.InvoiceNotCreated);
                        }
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseInvoiceCreationWindow);
                this.FilteredSales.Clear();
                this.ConsolidatedInvoice = false;
                ViewModelLocator.ClearInvoiceCreation();
            }
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        protected virtual void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedSales == null || !this.SelectedSales.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.RunningTotal = this.SelectedSales.Sum(x => x.SubTotalExVAT).ToDecimal();

            this.SetControlMode(ControlMode.Add);
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region helper

       

        #endregion

        #endregion
    }
}

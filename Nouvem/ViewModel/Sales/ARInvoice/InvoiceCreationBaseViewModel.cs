﻿// -----------------------------------------------------------------------
// <copyright file="SalesSearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Sales.ARInvoice
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class that handles the search for the sale orders.
    /// </summary>
    public class InvoiceCreationBaseViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected sales.
        /// </summary>
        private ObservableCollection<Sale> selectedSales = new ObservableCollection<Sale>();

        /// <summary>
        /// The filtered search sales.
        /// </summary>
        private IList<Sale> searchSales;

        /// <summary>
        /// The selected grid sale.
        /// </summary>
        private Sale currentSale;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<Sale> filteredSales;

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// The searcht to date.
        /// </summary>
        private DateTime toDate;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        protected bool keepVisible;

        /// <summary>
        /// The show all orders flag..
        /// </summary>
        protected bool showAllOrders;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceCreationBaseViewModel "/> class.
        /// </summary>
        public InvoiceCreationBaseViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the incoming customers sales.
            Messenger.Default.Register<IList<Sale>>(this, Token.DisplayReportData, this.SetIncomingSales);


            #endregion

            #region command registration

            this.SaleSelectedCommand = new RelayCommand(this.SaleSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            #region instantiation

            this.FilteredSales = new ObservableCollection<Sale>();

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromDate
        {
            get
            {
                return this.fromDate;
            }

            set
            {
                this.fromDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    ApplicationSettings.SalesSearchFromDate = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToDate
        {
            get
            {
                return this.toDate;
            }

            set
            {
                this.toDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    ApplicationSettings.SalesSearchToDate = value;
                    //Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);

                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all the orders are to be dispalyed.
        /// </summary>
        public bool ShowAllOrders
        {
            get
            {
                return this.showAllOrders;
            }

            set
            {
                this.showAllOrders = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    ApplicationSettings.SalesSearchShowAllOrders = value;
                    this.HandleShowAllOrders();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected search sales.
        /// </summary>
        public ObservableCollection<Sale> SelectedSales
        {
            get { return this.selectedSales; }

            set
            {
                if (value != null)
                {
                    this.selectedSales = value;
                    this.RaisePropertyChanged();

                    if (value.Any())
                    {
                        this.SetControlMode(ControlMode.Add);
                    }
                    else
                    {
                        this.SetControlMode(ControlMode.OK);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the filtered sales.
        /// </summary>
        public ObservableCollection<Sale> FilteredSales
        {
            get
            {
                return this.filteredSales;
            }

            set
            {
                this.filteredSales = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the associated view.
        /// </summary>
        public ViewType AssociatedView { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the sale selection.
        /// </summary>
        public ICommand SaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Set the filtered sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        public void SetSales(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales);
        }

        /// <summary>
        /// Method that sets the sales to display.
        /// </summary>
        /// <param name="data">The sales to display.</param>
        public void SetFilteredSales(IList<Sale> data)
        {
            this.FilteredSales = new ObservableCollection<Sale>(data);
        }

        #endregion

        #endregion

        #region protected

        #region virtual

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected virtual void Close()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
                this.FilteredSales.Clear();
            }
        }

        /// <summary>
        /// Set the incoming sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        protected virtual void SetIncomingSales(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleID));
        }

        #endregion

        #region override

        protected virtual void HandleShowAllOrders() { }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected virtual void OnLoadingCommandExecute()
        {
            ApplicationSettings.SalesSearchFromDate = DateTime.Today;
            this.FromDate = ApplicationSettings.SalesSearchFromDate;
            this.ToDate = DateTime.Today;
            this.KeepVisible = false;
            this.ShowAllOrders = ApplicationSettings.SalesSearchShowAllOrders;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected virtual void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.Close();
        }

        /// <summary>
        /// Handler for the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            ApplicationSettings.SalesSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchToDate = this.ToDate;
            ApplicationSettings.SalesSearchKeepVisible = this.KeepVisible;
            ApplicationSettings.SalesSearchShowAllOrders = this.ShowAllOrders;
            Settings.Default.Save();

            if (this.SelectedSales.Count > 1)
            {
                Messenger.Default.Send(this.selectedSales.ToList(), Token.SearchSaleSelectedForReport);
            }
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            ApplicationSettings.SalesSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchToDate = this.ToDate;
            ApplicationSettings.SalesSearchKeepVisible = this.KeepVisible;
            ApplicationSettings.SalesSearchShowAllOrders = this.ShowAllOrders;
            Settings.Default.Save();

            this.KeepVisible = false;
            this.Close();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected virtual void SaleSelectedCommandExecute()
        {
            if (this.SelectedSales != null && this.SelectedSales.Any())
            {
                Messenger.Default.Send(this.selectedSales.First(), Token.SearchSaleSelectedForReport);
            }

            //Messenger.Default.Send(Token.Message, Token.SetTopMost);
            this.Close();
        }

        #endregion

        #region helper


        #endregion

        #endregion
    }
}



﻿// -----------------------------------------------------------------------
// <copyright file="OrderViewModelBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Sales
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Win32;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel;
    using Nouvem.ViewModel.Interface;

    public abstract class SalesViewModelBase : TransactionsHelperViewModel, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The cattle time in transit.
        /// </summary>
        private string timeInTransit;

        /// <summary>
        /// The sale reference.
        /// </summary>
        private Sale sale;

        private string productLineCount;

        /// <summary>
        /// The current sale partner balance.
        /// </summary>
        private decimal? accountBalance;

        /// <summary>
        /// The current sale partner credit limit.
        /// </summary>
        private int? creditLimit;

        /// <summary>
        /// The customers collection.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> customers;

        /// <summary>
        /// The selected customers contacts collection.
        /// </summary>
        private ObservableCollection<BusinessPartnerContact> contacts;

        /// <summary>
        /// The application price lists.
        /// </summary>
        private ObservableCollection<PriceMaster> priceLists;

        /// <summary>
        /// The customer sale details.
        /// </summary>
        private ObservableCollection<SaleDetail> customerSaleDetails;

        /// <summary>
        /// The application routes.
        /// </summary>
        private ObservableCollection<Route> routes;

        /// <summary>
        /// Is this a base document flag.
        /// </summary>
        private bool isBaseDocument;

        /// <summary>
        /// The typical batch value.
        /// </summary>
        private decimal? typicalBatchValue;

        /// <summary>
        /// The typical batch value.
        /// </summary>
        private decimal? actualBatchValue;

        /// <summary>
        /// Is this a base document flag.
        /// </summary>
        private int currentINMasterID;

        /// <summary>
        /// Are we disallowing a doc status change..
        /// </summary>
        private bool disableDocStatusChange;

        /// <summary>
        /// Is this a read only document flag.
        /// </summary>
        private bool isReadOnly;

        /// <summary>
        /// Is this a read only document flag.
        /// </summary>
        private bool isDiscountReadOnly;

        /// <summary>
        /// The selected route.
        /// </summary>
        private Route selectedRoute;

        /// <summary>
        /// The delivery date textbox width.
        /// </summary>
        private double deliveryDateWidth;

        /// <summary>
        /// The selected tab index.
        /// </summary>
        private int selectedTab;

        /// <summary>
        /// The partner name.
        /// </summary>
        private string partnerName;

        /// <summary>
        /// The selected partner reference.
        ///  </summary>
        private ViewBusinessPartner selectedPartner;

        /// <summary>
        /// The selected haulier reference.
        ///  </summary>
        private ViewBusinessPartner selectedHaulier;

        /// <summary>
        /// The selected document status.
        /// </summary>
        private NouDocStatu selectedDocStatus;

        /// <summary>
        /// The partners collection.
        /// </summary>
        protected IList<ViewBusinessPartner> allPartners;

        /// <summary>
        /// The suppliers collection.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> suppliers;

        /// <summary>
        /// The selected customers contact.
        /// </summary>
        private BusinessPartnerContact selectedContact;
     
        /// <summary>
        /// The selected customers delivery contact.
        /// </summary>
        private BusinessPartnerContact selectedDeliveryContact;

        /// <summary>
        /// The selected sale detail.
        /// </summary>
        private SaleDetail selectedSaleDetail;

        /// <summary>
        /// The valid until date.
        /// </summary>
        private DateTime? validUntilDate;

        /// <summary>
        /// The delivery date.
        /// </summary>
        private DateTime? deliveryDate;

        /// <summary>
        /// The kill date.
        /// </summary>
        private DateTime? proposedKillDate;

        /// <summary>
        /// The delivery time.
        /// </summary>
        private string deliveryTime;

        /// <summary>
        /// The shipping date.
        /// </summary>
        private DateTime? shippingDate;

        /// <summary>
        /// The delivery date touchscreen.
        /// </summary>
        private DateTime? deliveryDateTouchscreen;

        /// <summary>
        /// The document date.
        /// </summary>
        private DateTime? documentDate;

        /// <summary>
        /// The partner selected attachment
        /// </summary>
        private Attachment selectedAttachment;

        /// <summary>
        /// The customer po reference.
        /// </summary>
        private string customerPOReference;

        /// <summary>
        /// The selected document to copy to.
        /// </summary>
        private string selectedDocumentCopyTo;

        /// <summary>
        /// The selected document to copy from.
        /// </summary>
        private string selectedDocumentCopyFrom;

        /// <summary>
        /// The localised document numbering collection.
        /// </summary>
        private ObservableCollection<DocNumber> docNumberings;

        /// <summary>
        /// The sales employees collection.
        /// </summary>
        private ObservableCollection<User> salesEmployees;

        /// <summary>
        /// The products recent order data collection.
        /// </summary>
        private ObservableCollection<ProductRecentOrderData> productRecentOrderData;

        /// <summary>
        /// The selected products recent order data.
        /// </summary>
        private ProductRecentOrderData selectedProductRecentOrderData;

        /// <summary>
        /// The selected sales employee.
        /// </summary>
        private User selectedSalesEmployee;

        /// <summary>
        /// The selected document numbering.
        /// </summary>
        private DocNumber selectedDocNumbering;

        /// <summary>
        /// The selected customer billing address.
        /// </summary>
        private BusinessPartnerAddress selectedInvoiceAddress;

        /// <summary>
        /// The selected customer delivery address.
        /// </summary>
        private BusinessPartnerAddress selectedDeliveryAddress;

        /// <summary>
        /// The selected customer delivery address.
        /// </summary>
        private string selectedDeliveryAddressFull;

        /// <summary>
        /// The inventory sale items collection.
        /// </summary>
        private ObservableCollection<InventoryItem> saleItems;

        /// <summary>
        /// The inventory sale items collection.
        /// </summary>
        private ObservableCollection<InventoryItem> receipeItems;

        /// <summary>
        /// The inventory purchase items collection.
        /// </summary>
        private ObservableCollection<InventoryItem> purchaseItems;

        /// <summary>
        /// The sale details collection.
        /// </summary>
        private ObservableCollection<SaleDetail> saleDetails;

        /// <summary>
        /// The customer addresses collection.
        /// </summary>
        private ObservableCollection<BusinessPartnerAddress> addresses;
        
        /// <summary>
        /// The selected inventory item.
        /// </summary>
        private InventoryItem selectedInventoryItem;

        /// <summary>
        /// The selected customer sale item.
        /// </summary>
        private SaleDetail selectedCustomerSaleDetail;

        /// <summary>
        /// The docket note name.
        /// </summary>
        private string docketNoteName;

        /// <summary>
        /// The remarks field.
        /// </summary>
        private string remarks;

        /// <summary>
        /// Determines is a p.o. is arequired.
        /// </summary>
        private bool poRequired;

        /// <summary>
        /// The discount percentage.
        /// </summary>
        private decimal discountPercentage;

        /// <summary>
        /// The sub total value.
        /// </summary>
        private decimal subTotal;

        /// <summary>
        /// The total value.
        /// </summary>
        private decimal total;

        /// <summary>
        /// The total value.
        /// </summary>
        private decimal? orderMargin;

        /// <summary>
        /// The total value.
        /// </summary>
        private decimal? orderValue;

        /// <summary>
        /// The total value.
        /// </summary>
        private decimal orderCost;

        /// <summary>
        /// The tax value.
        /// </summary>
        private decimal tax;

        /// <summary>
        /// The show route flag.
        /// </summary>
        private bool showRoute;

        /// <summary>
        /// The discount value.
        /// </summary>
        private decimal discount;

        /// <summary>
        /// The dispatch docket note text.
        /// </summary>
        private string docketNote;

        /// <summary>
        /// The dispatch pop up note text.
        /// </summary>
        private string popUpNote;

        /// <summary>
        /// The clear traceability flag.
        /// </summary>
        private string customerPopUpNote;

        /// <summary>
        /// The invoice note text.
        /// </summary>
        private string partnerInvoiceNote;

        /// <summary>
        /// The invoice note text.
        /// </summary>
        private string invoiceNote;

        /// <summary>
        /// The recent order 1 date.
        /// </summary>
        private string order1Date;

        /// <summary>
        /// The recent order 2 date.
        /// </summary>
        private string order2Date;

        /// <summary>
        /// The recent order 3 date.
        /// </summary>
        private string order3Date;

        /// <summary>
        /// The recent order 4 date.
        /// </summary>
        private string order4Date;

        /// <summary>
        /// The recent order 15 date.
        /// </summary>
        private string order5Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order6Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order7Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order8Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order9Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order10Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order11Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order12Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order13Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order14Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order15Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order16Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order17Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order18Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order19Date;

        /// <summary>
        /// The recent order 6 date.
        /// </summary>
        private string order20Date;

        #region overflow


        private string order21Date;
        private string order22Date;
        private string order23Date;
        private string order24Date;
        private string order25Date;
        private string order26Date;
        private string order27Date;
        private string order28Date;
        private string order29Date;
        private string order30Date;
        private string order31Date;
        private string order32Date;
        private string order33Date;
        private string order34Date;
        private string order35Date;
        private string order36Date;
        private string order37Date;
        private string order38Date;
        private string order39Date;
        private string order40Date;
        private string order41Date;
        private string order42Date;
        private string order43Date;
        private string order44Date;
        private string order45Date;
        private string order46Date;
        private string order47Date;
        private string order48Date;
        private string order49Date;
        private string order50Date;
        private string order51Date;
        private string order52Date;
        private string order53Date;
        private string order54Date;
        private string order55Date;
        private string order56Date;
        private string order57Date;
        private string order58Date;
        private string order59Date;
        private string order60Date;
        private string order61Date;
        private string order62Date;
        private string order63Date;
        private string order64Date;
        private string order65Date;
        private string order66Date;
        private string order67Date;
        private string order68Date;
        private string order69Date;
        private string order70Date;
        private string order71Date;
        private string order72Date;
        private string order73Date;
        private string order74Date;
        private string order75Date;
        private string order76Date;
        private string order77Date;
        private string order78Date;
        private string order79Date;
        private string order80Date;
        private string order81Date;
        private string order82Date;
        private string order83Date;
        private string order84Date;
        private string order85Date;
        private string order86Date;
        private string order87Date;
        private string order88Date;
        private string order89Date;
        private string order90Date;
        private string order91Date;
        private string order92Date;
        private string order93Date;
        private string order94Date;
        private string order95Date;
        private string order96Date;
        private string order97Date;
        private string order98Date;
        private string order99Date;
        private string order100Date;


        #endregion

        /// <summary>
        /// The sale order code search term
        /// </summary>
        private string searchTermCode;

        /// <summary>
        /// The sale order name search term 
        /// </summary>
        private string searchTermName;

        /// <summary>
        /// Flag as to whether a document can be copied from.
        /// </summary>
        private bool canCopyFrom;

        /// <summary>
        /// Flag as to whether a document can be copied to.
        /// </summary>
        private bool canCopyTo;

        /// <summary>
        /// The document number.
        /// </summary>
        private int nextNumber;

        /// <summary>
        /// The document number.
        /// </summary>
        private int? nextNumberDisplay;

        /// <summary>
        /// Holds the ui updated pricelist changes details.
        /// </summary>
        private Tuple<int, int> priceListUpdateDetails = new Tuple<int, int>(0,0);

        /// <summary>
        /// Flag, as to whether a unit price has been manually changed.
        /// </summary>
        protected bool unitPriceChanged;

        /// <summary>
        /// Flag, as to whether sales totals are to be calculated.
        /// </summary>
        protected bool CalculateSalesTotals = true;

        /// <summary>
        /// Flag, used to lock the discount percentage when copying from an order.
        /// </summary>
        protected bool LockDiscountPercentage;

        /// <summary>
        /// Determines if user can override an order over the allowed partner credit limit.
        /// </summary>
        protected bool CanOverrideCreditLimit;

        /// <summary>
        /// Flag, used to indicate an auto copy in progress.
        /// </summary>
        protected bool AutoCopying;

        private int recentOrdersCount;

        /// <summary>
        /// Disables the pop up notes.
        /// </summary>
        protected bool DisablePartnerPopUp;

        /// <summary>
        /// Disable any more processing flag.
        /// </summary>
        protected bool DisableProcessing;

        protected bool CanCloseForm;

        /// <summary>
        /// Flag, used to indicate an order add/update in progress.
        /// </summary>
        protected bool OrderUpdateInProgress;

        /// <summary>
        /// The document numbering collection.
        /// </summary>
        protected IList<DocNumber> allDocNumberings;

        /// <summary>
        /// Determines if user can override an order over the allowed partner limit.
        /// </summary>
        protected bool CanOverrideOrderLimit;

        /// <summary>
        /// Determines if user can override an order over the allowed partner limit.
        /// </summary>
        protected Telesale CurrentTelesale;

        /// <summary>
        /// Show all the recent orders falg.
        /// </summary>
        protected bool ShowAllRecentOrders;
        
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesViewModelBase"/> class.
        /// </summary>
        protected SalesViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToCreateDispatches);

            #region instantiation

            this.Contacts = new ObservableCollection<BusinessPartnerContact>();
            this.Addresses = new ObservableCollection<BusinessPartnerAddress>();
            this.SaleDetails = new ObservableCollection<SaleDetail>();
            this.SaleAttachments = new ObservableCollection<Attachment>();
            this.ProductRecentOrderDatas = new ObservableCollection<ProductRecentOrderData>();
            this.Sale = new Sale();
            this.CustomerSales = new List<Sale>();
            this.routes = new ObservableCollection<Route>();
            this.DeletedSaleDetails = new List<SaleDetail>();

            #endregion

            #region message registration

            Messenger.Default.Register<Tuple<List<App_GetOrderBatchData_Result>,int?>>(this, Token.SetBatchOrderData, o =>
            {
                if (this.SaleDetails == null)
                {
                    return;
                }

                var localSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == o.Item2);
                if (localSaleDetail != null)
                {
                    localSaleDetail.OrderBatchData = o.Item1;
                    localSaleDetail.QuantityOrdered = o.Item1.Sum(x => x.QuantityOrdered.ToDecimal());
                    localSaleDetail.WeightOrdered = o.Item1.Sum(x => x.WeightOrdered.ToDecimal());
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowSalesSearchScreen, o => { FindSaleCommandExecute(); });

            Messenger.Default.Register<string>(this, Token.SetActiveDocument, o =>
            {
                if (this.IsFormLoaded)
                {
                    this.SetActiveDocument(this);
                }
            });

            Messenger.Default.Register<List<PriceDetail>>(this, Token.PriceListsUpdated, o =>
            {
                this.HandlePriceListUpdates(o);
            });

            Messenger.Default.Register<string>(this, Token.PriceListsUpdated, s =>
            {
                try
                {
                    this.GetPriceLists();
                    if (this.priceListUpdateDetails == null)
                    {
                        return;
                    }

                    var localPriceListId = this.priceListUpdateDetails.Item1;
                    var localINMasterId = this.priceListUpdateDetails.Item2;

                    if (localPriceListId == 0)
                    {
                        return;
                    }

                    if (localPriceListId == NouvemGlobal.CostPriceList.PriceListID)
                    {
                        var detail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localINMasterId);
                        if (detail == null)
                        {
                            return;
                        }

                        detail.CalculateCosts(true);
                        return;
                    }

                    this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localINMasterId);

                    if (this.SelectedSaleDetail != null)
                    {
                        var localPriceDetail = this.DataManager.GetPriceListDetail(localPriceListId,
                            localINMasterId);

                        if (localPriceDetail != null)
                        {
                            this.SelectedSaleDetail.UnitPrice = localPriceDetail.Price;
                        }

                        this.SelectedSaleDetail.PriceListID = localPriceListId;
                        this.SelectedSaleDetail.UnitPriceAfterDiscount = this.SelectedSaleDetail.UnitPrice;
                        this.UpdateSaleDetailTotals();
                    }

                    this.priceListUpdateDetails = new Tuple<int, int>(0, 0);
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                }
            });

            // Register for the incoming orders grid column.
            Messenger.Default.Register<string>(this, Token.GridColumn, s => this.CurrentColumn = s);

            // Register for a previous order item selection.
            Messenger.Default.Register<string>(this, Token.RecentOrderItemSelected, this.PreviousOrderItemSelectionHandler);

            // Register to update the totals (Updates when an order item total changes)
            Messenger.Default.Register<string>(this, Token.UpdateSaleOrder, s =>
            {
                this.UpdateSaleOrderTotals();
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for an incoming search sale selection.
            Messenger.Default.Register<Sale>(this, Token.SearchSaleSelected, sale =>
            {
                if (this.IsFocusedSaleModule)
                {
                    this.IgnoreOutstandingOrderCheck = true;
                    this.HandleSearchSale(sale);
                }
            });

            // Register for a master window change control command.
            Messenger.Default.Register<string>(this, Token.SwitchControl, this.ControlChange);

            // Register for a manually created recent order item i.e. user entered a qty/wgt value.
            Messenger.Default.Register<SaleDetail>(this, Token.AddManualRecentOrderItem, this.AddManualRecentOrderItem);

            // Register for a manually created recent order error.
            Messenger.Default.Register<string>(this, Token.RecentOrderError, this.HandleRecentOrderError);

            // Register for a new partner addition (update the local partner collections).
            Messenger.Default.Register<string>(this, Token.RefreshPartners, s => this.UpdatePartners());

            // Register for a new product creation.
            Messenger.Default.Register<Tuple<InventoryItem, bool>>(this, Token.RefreshProducts, i => this.GetInventoryItems());

            #endregion

            #region command handler

            this.ScrollRecentOrdersCommand = new RelayCommand<string>(this.ScrollRecentOrdersCommandExecute);

            // Command handler for the base document recollection.
            this.ShowAllRecentOrdersCommand = new RelayCommand(this.ShowAllRecentOrdersCommandExecute);

            // Command handler for the base document recollection.
            this.BaseDocumentsCommand = new RelayCommand<string>(this.BaseDocumentsCommandExecute);

            // Command handler for the F1 search direction to the appropriate handler.
            this.DirectSearchCommand = new RelayCommand(this.DirectSearchCommandExecute);

            // Command handler to add a sale order item to the current order.
            this.AddToOrderCommand = new RelayCommand(this.AddToOrderCommandExecute, () => this.SelectedCustomerSaleDetail != null);

            // Command handler for the form load event.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Command handler for the form unload event.
            this.OnUnloadedCommand = new RelayCommand(this.OnUnloadedCommandExecute);

            // Handler for the search grid display.
            this.ShowSearchGridCommand = new RelayCommand<string>(this.ShowSearchGridCommandExecute);
         
            // Command handler to add an attachment to the db.
            this.AttachFileCommand = new RelayCommand(this.AttachFileCommandExecute);

            // Command handler to display an attachment.
            this.DisplayFileCommand = new RelayCommand(this.DisplayFileCommandExecute);

            // Command handler to delete an attachment from the db.
            this.DeleteFileCommand = new RelayCommand(this.DeleteFileCommandExecute);

            // Command handler for the grid cell value changing event.
            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>(this.UpdateCommandExecute);

            // Command handler for the user drill down.
            this.DrillDownToUserCommand = new RelayCommand(this.DrillDownToUserCommandExecute);

            // Command handler for the item drill down.
            this.DrillDownToSpecCommand = new RelayCommand(this.DrillDownToSpecCommandExecute);

            // Command handler for the item drill down.
            this.DrillDownToBatchesCommand = new RelayCommand(this.DrillDownToBatchesCommandExecute);

            // Command handler for the item drill down.
            this.DrillDownToItemCommand = new RelayCommand(this.DrillDownToItemCommandExecute);

            // Command handler for the item drill down.
            this.DrillDownToMasterItemCommand = new RelayCommand(this.DrillDownToMasterItemCommandExecute);

            // Command handler for the selected price list drill down.
            this.DrillDownToPriceListCommand = new RelayCommand(this.DrillDownToPriceListCommandExecute);

            // Command handler for the selected price list drill down.
            this.DrillDownToCostPriceListCommand = new RelayCommand(this.DrillDownToCostPriceListCommandExecute);

            // Command handler to flag the embedded grid focus.
            this.OnFocusedCommand = new RelayCommand<string>(s => this.IsGridFocused = s.ToBool());

            // Command to handle a column focus change.
            this.OnFocusedColumnChangedCommand = new RelayCommand<EditorEventArgs>(this.OnFocusedColumnChangedCommandExecute);

            #endregion
          
            Messenger.Default.Send(Token.Message, Token.EnableSalesSearchScreen);
            this.CheckForNewEntities();
            this.AllSales = new List<Sale>();
            this.GetDocNumbering();
            this.GetAllPartners();
            this.GetCustomers();
            this.GetSuppliers();
            this.GetAgents();
            this.GetDocStatusItems();
            this.GetInventoryItems();
            this.GetSalesEmployees();
            this.SetDefaultDates();
            this.GetRoutes();
            this.GetPriceLists();
            this.GetDocStatusIds();
            this.DocketNoteName = Strings.DispatchDocketNote;
            this.ShowRoute = false;
            this.IsDiscountReadOnly = ApplicationSettings.DisableSaleOrderDiscounts;
            Messenger.Default.Send(false, Token.DisableLegacyAttributesView);
            this.ProductLineCount = $"{Strings.ProductLineCount}0";
        }

        #endregion

        #region public interface

        #region property

        public string ProductLineCount
        {
            get { return this.productLineCount; }
            set
            {
                this.productLineCount = value;
                this.RaisePropertyChanged();
            }
        }

        public int RecentOrdersCount
        {
            get { return this.recentOrdersCount; }
            set
            {
                this.recentOrdersCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the delivery dtime.
        /// </summary>
        public string TimeInTransit
        {
            get
            {
                return this.timeInTransit;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.Replace(".", ":");
                    var localValue = value.Split(':');
                    if (localValue.Length == 2)
                    {
                        var hrs = localValue[0];
                        if (hrs.Length == 1)
                        {
                            hrs = hrs.PadLeft(2, '0');
                        }

                        var mins = localValue[1];
                        if (mins.Length == 1)
                        {
                            mins = mins.PadLeft(2, '0');
                        }

                        value = string.Format("{0}:{1}", hrs, mins);
                    }

                    if (!this.ValidateTime(value))
                    {
                        return;
                    }
                }

                this.SetMode(value, this.timeInTransit);
                this.timeInTransit = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public decimal? ActualBatchValue
        {
            get
            {
                return this.actualBatchValue;
            }

            set
            {
                this.actualBatchValue = value;
                this.RaisePropertyChanged();
                this.HandleActualBatchValue();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public decimal? TypicalBatchValue
        {
            get
            {
                return this.typicalBatchValue;
            }

            set
            {
                this.typicalBatchValue = value;
                this.RaisePropertyChanged();
                this.HandleBatchValue();
            }
        }

        /// <summary>
        /// Gets or sets the partner balance.
        /// </summary>
        public decimal? AccountBalance
        {
            get
            {
                return this.accountBalance;
            }

            set
            {
                this.accountBalance = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner credit limit.
        /// </summary>
        public int? CreditLimit
        {
            get
            {
                return this.creditLimit;
            }

            set
            {
                this.creditLimit = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the last edit time.
        /// </summary>
        public DateTime LastEdit { get; set; }

        /// <summary>
        /// Ignores the outstanding order check.
        /// </summary>
        public bool IgnoreOutstandingOrderCheck { get; set; }
        
        /// <summary>
        /// Gets or sets the partner name.
        /// </summary>
        public string PartnerName
        {
            get
            {
                return this.partnerName;
            }

            set
            {
                this.partnerName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected tab index.
        /// </summary>
        public int SelectedTab
        {
            get
            {
                return this.selectedTab;
            }

            set
            {
                this.selectedTab = value;
                this.RaisePropertyChanged();
            }
        }
      
        /// <summary>
        /// Gets or sets a value indicating whether the route ui controls are to be displayed.
        /// </summary>
        public bool ShowRoute
        {
            get
            {
                return this.showRoute;
            }

            set
            {
                this.showRoute = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets a value indicating whether a selected details handling logic is to be applied.
        /// </summary>
        public bool DoNotHandleSelectedSaleDetail { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a selected details handling logic is to be applied.
        /// </summary>
        public bool DoNotResetQty { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are closing the current sale.
        /// </summary>
        public bool CancellingCurrentSale
        {
            get
            {
                return this.SelectedDocStatus != null &&
                       this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are closing the current sale.
        /// </summary>
        public bool ClosingCurrentSale
        {
            get
            {
                return this.SelectedDocStatus != null &&
                       this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
            }
        }

        /// <summary>
        /// Gets or sets the application routes.
        /// </summary>
        public ObservableCollection<Route> Routes
        {
            get
            {
                return this.routes;
            }

            set
            {
                this.routes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected route.
        /// </summary>
        public Route SelectedRoute
        {
            get
            {
                return this.selectedRoute;
            }

            set
            {
                this.SetMode(value, this.selectedRoute);
                this.selectedRoute = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customer po reference
        /// </summary>
        public string CustomerPOReference
        {
            get
            {
                return this.customerPOReference;
            }

            set
            {
                this.SetMode(value, this.customerPOReference);
                this.customerPOReference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Flag, as to whether the embedded grid is focused.
        /// </summary>
        public bool IsGridFocused { get; set; }
        
        /// <summary>
        /// Gets or sets the dispatch/intake note name.
        /// </summary>
        public string DocketNoteName
        {
            get
            {
                return this.docketNoteName;
            }

            set
            {
                this.docketNoteName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner suppliers.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Suppliers
        {
            get
            {
                return this.suppliers;
            }

            set
            {
                this.suppliers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customer sale detail.
        /// </summary>
        public SaleDetail SelectedCustomerSaleDetail
        {
            get
            {
                return this.selectedCustomerSaleDetail;
            }

            set
            {
                this.selectedCustomerSaleDetail = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customer sales.
        /// </summary>
        public IList<Sale> CustomerSales { get; set; }

        /// <summary>
        /// Gets or sets all the sales/orders.
        /// </summary>
        public IList<Sale> AllSales { get; set; }

        /// <summary>
        /// Gets or sets the document number.
        /// </summary>
        public int NextNumber
        {
            get
            {
                return this.nextNumber;
            }

            set
            {
                this.nextNumber = value;
                this.RaisePropertyChanged();
                if (this.Sale == null || this.Sale.SaleID == 0)
                {
                    this.NextNumberDisplay = null;
                }
                else
                {
                    this.NextNumberDisplay = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the document number.
        /// </summary>
        public int? NextNumberDisplay
        {
            get
            {
                return this.nextNumberDisplay;
            }

            set
            {
                this.nextNumberDisplay = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the document to copy to.
        /// </summary>
        public string SelectedDocumentCopyTo
        {
            get
            {
                return this.selectedDocumentCopyTo;
            }

            set
            {
                this.selectedDocumentCopyTo = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.CopyDocumentTo();
                }
            }
        }

        /// <summary>
        /// Gets or sets the document to copy from.
        /// </summary>
        public string SelectedDocumentCopyFrom
        {
            get
            {
                return this.selectedDocumentCopyFrom;
            }

            set
            {
                this.selectedDocumentCopyFrom = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.CopyDocumentFrom();
                }
            }
        }

        /// <summary>
        /// Gets or sets the sale order search term by code.
        /// </summary>
        public string SearchTermCode
        {
            get
            {
                return this.searchTermCode;
            }

            set
            {
                this.searchTermCode = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrEmpty(value))
                {
                    this.SearchTermName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets the sale order search term by name.
        /// </summary>
        public string SearchTermName
        {
            get
            {
                return this.searchTermName;
            }

            set
            {
                this.searchTermName = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrEmpty(value))
                {
                    this.SearchTermCode = string.Empty;
                }
            }
        }
       
        /// <summary>
        /// Gets or sets the recent order 1 date.
        /// </summary>
        public string Order1Date
        {
            get
            {
                return this.order1Date;
            }

            set
            {
                this.order1Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 2 date.
        /// </summary>
        public string Order2Date
        {
            get
            {
                return this.order2Date;
            }

            set
            {
                this.order2Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 3 date.
        /// </summary>
        public string Order3Date
        {
            get
            {
                return this.order3Date;
            }

            set
            {
                this.order3Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 4 date.
        /// </summary>
        public string Order4Date
        {
            get
            {
                return this.order4Date;
            }

            set
            {
                this.order4Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 5 date.
        /// </summary>
        public string Order5Date
        {
            get
            {
                return this.order5Date;
            }

            set
            {
                this.order5Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order6Date
        {
            get
            {
                return this.order6Date;
            }

            set
            {
                this.order6Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order7Date
        {
            get
            {
                return this.order7Date;
            }

            set
            {
                this.order7Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order8Date
        {
            get
            {
                return this.order8Date;
            }

            set
            {
                this.order8Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order9Date
        {
            get
            {
                return this.order9Date;
            }

            set
            {
                this.order9Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order10Date
        {
            get
            {
                return this.order10Date;
            }

            set
            {
                this.order10Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order11Date
        {
            get
            {
                return this.order11Date;
            }

            set
            {
                this.order11Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order12Date
        {
            get
            {
                return this.order12Date;
            }

            set
            {
                this.order12Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order13Date
        {
            get
            {
                return this.order13Date;
            }

            set
            {
                this.order13Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order14Date
        {
            get
            {
                return this.order14Date;
            }

            set
            {
                this.order14Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order15Date
        {
            get
            {
                return this.order15Date;
            }

            set
            {
                this.order15Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order16Date
        {
            get
            {
                return this.order16Date;
            }

            set
            {
                this.order16Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order17Date
        {
            get
            {
                return this.order17Date;
            }

            set
            {
                this.order17Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order18Date
        {
            get
            {
                return this.order18Date;
            }

            set
            {
                this.order18Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order19Date
        {
            get
            {
                return this.order19Date;
            }

            set
            {
                this.order19Date = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recent order 6 date.
        /// </summary>
        public string Order20Date
        {
            get
            {
                return this.order20Date;
            }

            set
            {
                this.order20Date = value;
                this.RaisePropertyChanged();
            }
        }

        #region overflow


        public string Order21Date { get { return this.order21Date; } set { this.order21Date = value; this.RaisePropertyChanged(); } }
        public string Order22Date { get { return this.order22Date; } set { this.order22Date = value; this.RaisePropertyChanged(); } }
        public string Order23Date { get { return this.order23Date; } set { this.order23Date = value; this.RaisePropertyChanged(); } }
        public string Order24Date { get { return this.order24Date; } set { this.order24Date = value; this.RaisePropertyChanged(); } }
        public string Order25Date { get { return this.order25Date; } set { this.order25Date = value; this.RaisePropertyChanged(); } }
        public string Order26Date { get { return this.order26Date; } set { this.order26Date = value; this.RaisePropertyChanged(); } }
        public string Order27Date { get { return this.order27Date; } set { this.order27Date = value; this.RaisePropertyChanged(); } }
        public string Order28Date { get { return this.order28Date; } set { this.order28Date = value; this.RaisePropertyChanged(); } }
        public string Order29Date { get { return this.order29Date; } set { this.order29Date = value; this.RaisePropertyChanged(); } }
        public string Order30Date { get { return this.order30Date; } set { this.order30Date = value; this.RaisePropertyChanged(); } }
        public string Order31Date { get { return this.order31Date; } set { this.order31Date = value; this.RaisePropertyChanged(); } }
        public string Order32Date { get { return this.order32Date; } set { this.order32Date = value; this.RaisePropertyChanged(); } }
        public string Order33Date { get { return this.order33Date; } set { this.order33Date = value; this.RaisePropertyChanged(); } }
        public string Order34Date { get { return this.order34Date; } set { this.order34Date = value; this.RaisePropertyChanged(); } }
        public string Order35Date { get { return this.order35Date; } set { this.order35Date = value; this.RaisePropertyChanged(); } }
        public string Order36Date { get { return this.order36Date; } set { this.order36Date = value; this.RaisePropertyChanged(); } }
        public string Order37Date { get { return this.order37Date; } set { this.order37Date = value; this.RaisePropertyChanged(); } }
        public string Order38Date { get { return this.order38Date; } set { this.order38Date = value; this.RaisePropertyChanged(); } }
        public string Order39Date { get { return this.order39Date; } set { this.order39Date = value; this.RaisePropertyChanged(); } }
        public string Order40Date { get { return this.order40Date; } set { this.order40Date = value; this.RaisePropertyChanged(); } }
        public string Order41Date { get { return this.order41Date; } set { this.order41Date = value; this.RaisePropertyChanged(); } }
        public string Order42Date { get { return this.order42Date; } set { this.order42Date = value; this.RaisePropertyChanged(); } }
        public string Order43Date { get { return this.order43Date; } set { this.order43Date = value; this.RaisePropertyChanged(); } }
        public string Order44Date { get { return this.order44Date; } set { this.order44Date = value; this.RaisePropertyChanged(); } }
        public string Order45Date { get { return this.order45Date; } set { this.order45Date = value; this.RaisePropertyChanged(); } }
        public string Order46Date { get { return this.order46Date; } set { this.order46Date = value; this.RaisePropertyChanged(); } }
        public string Order47Date { get { return this.order47Date; } set { this.order47Date = value; this.RaisePropertyChanged(); } }
        public string Order48Date { get { return this.order48Date; } set { this.order48Date = value; this.RaisePropertyChanged(); } }
        public string Order49Date { get { return this.order49Date; } set { this.order49Date = value; this.RaisePropertyChanged(); } }
        public string Order50Date { get { return this.order50Date; } set { this.order50Date = value; this.RaisePropertyChanged(); } }
        public string Order51Date { get { return this.order51Date; } set { this.order51Date = value; this.RaisePropertyChanged(); } }
        public string Order52Date { get { return this.order52Date; } set { this.order52Date = value; this.RaisePropertyChanged(); } }
        public string Order53Date { get { return this.order53Date; } set { this.order53Date = value; this.RaisePropertyChanged(); } }
        public string Order54Date { get { return this.order54Date; } set { this.order54Date = value; this.RaisePropertyChanged(); } }
        public string Order55Date { get { return this.order55Date; } set { this.order55Date = value; this.RaisePropertyChanged(); } }
        public string Order56Date { get { return this.order56Date; } set { this.order56Date = value; this.RaisePropertyChanged(); } }
        public string Order57Date { get { return this.order57Date; } set { this.order57Date = value; this.RaisePropertyChanged(); } }
        public string Order58Date { get { return this.order58Date; } set { this.order58Date = value; this.RaisePropertyChanged(); } }
        public string Order59Date { get { return this.order59Date; } set { this.order59Date = value; this.RaisePropertyChanged(); } }
        public string Order60Date { get { return this.order60Date; } set { this.order60Date = value; this.RaisePropertyChanged(); } }
        public string Order61Date { get { return this.order61Date; } set { this.order61Date = value; this.RaisePropertyChanged(); } }
        public string Order62Date { get { return this.order62Date; } set { this.order62Date = value; this.RaisePropertyChanged(); } }
        public string Order63Date { get { return this.order63Date; } set { this.order63Date = value; this.RaisePropertyChanged(); } }
        public string Order64Date { get { return this.order64Date; } set { this.order64Date = value; this.RaisePropertyChanged(); } }
        public string Order65Date { get { return this.order65Date; } set { this.order65Date = value; this.RaisePropertyChanged(); } }
        public string Order66Date { get { return this.order66Date; } set { this.order66Date = value; this.RaisePropertyChanged(); } }
        public string Order67Date { get { return this.order67Date; } set { this.order67Date = value; this.RaisePropertyChanged(); } }
        public string Order68Date { get { return this.order68Date; } set { this.order68Date = value; this.RaisePropertyChanged(); } }
        public string Order69Date { get { return this.order69Date; } set { this.order69Date = value; this.RaisePropertyChanged(); } }
        public string Order70Date { get { return this.order70Date; } set { this.order70Date = value; this.RaisePropertyChanged(); } }
        public string Order71Date { get { return this.order71Date; } set { this.order71Date = value; this.RaisePropertyChanged(); } }
        public string Order72Date { get { return this.order72Date; } set { this.order72Date = value; this.RaisePropertyChanged(); } }
        public string Order73Date { get { return this.order73Date; } set { this.order73Date = value; this.RaisePropertyChanged(); } }
        public string Order74Date { get { return this.order74Date; } set { this.order74Date = value; this.RaisePropertyChanged(); } }
        public string Order75Date { get { return this.order75Date; } set { this.order75Date = value; this.RaisePropertyChanged(); } }
        public string Order76Date { get { return this.order76Date; } set { this.order76Date = value; this.RaisePropertyChanged(); } }
        public string Order77Date { get { return this.order77Date; } set { this.order77Date = value; this.RaisePropertyChanged(); } }
        public string Order78Date { get { return this.order78Date; } set { this.order78Date = value; this.RaisePropertyChanged(); } }
        public string Order79Date { get { return this.order79Date; } set { this.order79Date = value; this.RaisePropertyChanged(); } }
        public string Order80Date { get { return this.order80Date; } set { this.order80Date = value; this.RaisePropertyChanged(); } }
        public string Order81Date { get { return this.order81Date; } set { this.order81Date = value; this.RaisePropertyChanged(); } }
        public string Order82Date { get { return this.order82Date; } set { this.order82Date = value; this.RaisePropertyChanged(); } }
        public string Order83Date { get { return this.order83Date; } set { this.order83Date = value; this.RaisePropertyChanged(); } }
        public string Order84Date { get { return this.order84Date; } set { this.order84Date = value; this.RaisePropertyChanged(); } }
        public string Order85Date { get { return this.order85Date; } set { this.order85Date = value; this.RaisePropertyChanged(); } }
        public string Order86Date { get { return this.order86Date; } set { this.order86Date = value; this.RaisePropertyChanged(); } }
        public string Order87Date { get { return this.order87Date; } set { this.order87Date = value; this.RaisePropertyChanged(); } }
        public string Order88Date { get { return this.order88Date; } set { this.order88Date = value; this.RaisePropertyChanged(); } }
        public string Order89Date { get { return this.order89Date; } set { this.order89Date = value; this.RaisePropertyChanged(); } }
        public string Order90Date { get { return this.order90Date; } set { this.order90Date = value; this.RaisePropertyChanged(); } }
        public string Order91Date { get { return this.order91Date; } set { this.order91Date = value; this.RaisePropertyChanged(); } }
        public string Order92Date { get { return this.order92Date; } set { this.order92Date = value; this.RaisePropertyChanged(); } }
        public string Order93Date { get { return this.order93Date; } set { this.order93Date = value; this.RaisePropertyChanged(); } }
        public string Order94Date { get { return this.order94Date; } set { this.order94Date = value; this.RaisePropertyChanged(); } }
        public string Order95Date { get { return this.order95Date; } set { this.order95Date = value; this.RaisePropertyChanged(); } }
        public string Order96Date { get { return this.order96Date; } set { this.order96Date = value; this.RaisePropertyChanged(); } }
        public string Order97Date { get { return this.order97Date; } set { this.order97Date = value; this.RaisePropertyChanged(); } }
        public string Order98Date { get { return this.order98Date; } set { this.order98Date = value; this.RaisePropertyChanged(); } }
        public string Order99Date { get { return this.order99Date; } set { this.order99Date = value; this.RaisePropertyChanged(); } }
        public string Order100Date { get { return this.order100Date; } set { this.order100Date = value; this.RaisePropertyChanged(); } }


        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether the current sale vm is the one focused.
        /// </summary>
        public bool IsFocusedSaleModule { get; set; }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public bool OrderAlreadyCompleted { get; set; }

        /// <summary>
        /// Gets or sets the customer sales.
        /// </summary>
        /// <remarks>Note- this property is used in the sub classes e.g. Quote VM will contain only the customers quote sales etc.</remarks>
        public ObservableCollection<SaleDetail> CustomerSaleDetails
        {
            get
            {
                return this.customerSaleDetails;
            }

            set
            {
                this.customerSaleDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the sale is on a parent document.
        /// </summary>
        public bool IsBaseDocument
        {
            get
            {
                return this.isBaseDocument;
            }

            set
            {
                this.isBaseDocument = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the sale is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.isReadOnly;
            }

            set
            {
                this.isReadOnly = value;
                this.RaisePropertyChanged();

                if (!ApplicationSettings.DisableSaleOrderDiscounts)
                {
                    this.IsDiscountReadOnly = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the issue types.
        /// </summary>
        public IList<NouIssueMethod> NouIssueTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the sale is read only.
        /// </summary>
        public bool IsDiscountReadOnly
        {
            get
            {
                return this.isReadOnly;
            }

            set
            {
                this.isReadOnly = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the  document status can be changed.
        /// </summary>
        public bool DisableDocStatusChange
        {
            get
            {
                return this.disableDocStatusChange;
            }

            set
            {
                this.disableDocStatusChange = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// The sale header.
        /// </summary>
        public Sale Sale
        {
            get
            {
                return this.sale;
            }

            set
            {
                this.sale = value;

                if (value != null)
                {
                    //this.selectedUserSnapshot = value.Copy; //TODO
                    this.HandleSelectedSale();
                }
            }
        }

        /// <summary>
        /// Gets or sets the open order status ids.
        /// </summary>
        public IList<int?> DocStatusesOpen { get; set; }

        /// <summary>
        /// Gets or sets the closed order status id.
        /// </summary>
        public IList<int?> DocStatusesClosed { get; set; }

        /// <summary>
        /// Gets or sets the cancelled order status id.
        /// </summary>
        public IList<int?> DocStatusesCancelled { get; set; }

        /// <summary>
        /// Gets or sets the in progress order status id.
        /// </summary>
        public IList<int?> DocStatusesInProgress { get; set; }

        /// <summary>
        /// Gets or sets the complete order status id.
        /// </summary>
        public IList<int?> DocStatusesComplete { get; set; }

        /// <summary>
        /// Gets or sets al order status id.
        /// </summary>
        public IList<int?> DocStatusesAll { get; set; }

        /// <summary>
        /// Gets or sets the exprted order status id.
        /// </summary>
        public IList<int?> DocStatusesExported { get; set; }

        /// <summary>
        /// Gets the sale attachments.
        /// </summary>
        public ObservableCollection<Attachment> SaleAttachments { get; private set; }

        /// <summary>
        /// Gets or sets the selected partner attachment.
        /// </summary>
        public Attachment SelectedAttachment
        {
            get
            {
                return this.selectedAttachment;
            }

            set
            {
                this.selectedAttachment = value;
                this.RaisePropertyChanged();
            }
        }
     
        /// <summary>
        /// Gets or sets a value indicating whether a po is required.
        /// </summary>
        public bool PORequired
        {
            get
            {
                return this.poRequired;
            }

            set
            {
                this.SetMode(value, this.poRequired);
                this.poRequired = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        public ObservableCollection<PriceMaster> PriceLists
        {
            get
            {
                return this.priceLists;
            }

            set
            {
                this.priceLists = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets a products typical weight.
        /// </summary>
        public decimal? TypicalWeight
        {
            get
            {
                if (this.SelectedSaleDetail?.InventoryItem != null &&
                    !this.SelectedSaleDetail.InventoryItem.Master.NominalWeight.IsNullOrZero())
                {
                    return this.SelectedSaleDetail.InventoryItem.Master.NominalWeight.ToDecimal();
                }

                return null;
            }
        }
    
        /// <summary>
        /// Gets or sets the selected sale detail.
        /// </summary>
        public SaleDetail SelectedSaleDetail
        {
            get
            {
                return this.selectedSaleDetail;
            }

            set
            {
                this.selectedSaleDetail = value;
                this.RaisePropertyChanged();

                if (this.SaleDetails != null)
                {
                    this.ProductLineCount = $"{Strings.ProductLineCount}{this.SaleDetails?.Count}";
                }

                if (value != null)
                {
                    //if (SaleDetail.PriceChangingOnDesktop)
                    //{
                    //    value.SetPriceMethodOnly();
                    //}
                    //else
                    //{
                    //    value.SetPriceMethod();
                    //}

                    value.SetPriceMethod();

                    if (this.DoNotResetQty)
                    {
                        if (value.INMasterID > 0 && value.INMasterID != this.currentINMasterID)
                        {
                            this.DoNotResetQty = false;
                        }
                    }

                    if (ApplicationSettings.ResetLabelOnProductSelection && value.INMasterID > 0 && value.INMasterID != this.currentINMasterID)
                    {
                        // product switched, so always allow tare change on indicator
                        Messenger.Default.Send(true, Token.ResetLabel);
                        Messenger.Default.Send(Token.Message, Token.ResetLabel);
                    }

                    if (ApplicationSettings.TouchScreenMode && value.INMasterID > 0 && value.INMasterID != this.currentINMasterID)
                    {
                        // product switched, so always allow tare change on indicator
                        this.Locator.Indicator.DoNotResetTare = false;
                    }

                    if (!this.DoNotResetQty && !value.ScannedStock && !this.ResetQtyToZero())
                    {
                        Messenger.Default.Send(Token.Message, Token.SerialStockSelected);
                    }

                    if (value.INMasterID > 0)
                    {
                        this.currentINMasterID = value.INMasterID;
                    }

                    if (!this.DoNotHandleSelectedSaleDetail)
                    {
                        this.HandleSelectedSaleDetail();
                    }
                }
                else
                {
                    double zeroTare = 0;
                    Messenger.Default.Send(zeroTare, Token.SetTare);
                }
            }
        }

        /// <summary>
        /// Gets or sets the discount pop up note text.
        /// </summary>
        public string CustomerPopUpNote
        {
            get
            {
                return this.customerPopUpNote;
            }

            set
            {
                this.SetMode(value, this.customerPopUpNote);
                this.customerPopUpNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the discount pop up note text.
        /// </summary>
        public string PopUpNote
        {
            get
            {
                return this.popUpNote;
            }
            
            set
            {
                this.SetMode(value, this.popUpNote);
                this.popUpNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the discount docket note text.
        /// </summary>
        public string DocketNote
        {
            get
            {
                return this.docketNote;
            }

            set
            {
                this.SetMode(value, this.docketNote);
                this.docketNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner invoice note text.
        /// </summary>
        public string PartnerInvoiceNote
        {
            get
            {
                return this.partnerInvoiceNote;
            }

            set
            {
                this.SetMode(value, this.partnerInvoiceNote);
                this.partnerInvoiceNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the invoice note text.
        /// </summary>
        public string InvoiceNote
        {
            get
            {
                return this.invoiceNote;
            }

            set
            {
                this.SetMode(value, this.invoiceNote);
                this.invoiceNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the discount percentage.
        /// </summary>
        public decimal DiscountPercentage
        {
            get
            {
                return this.discountPercentage;
            }

            set
            {
                this.discountPercentage = value;
                this.RaisePropertyChanged();

                if (this.CalculateSalesTotals)
                {
                    this.UpdateSaleOrderTotals(false, true);
                }
               
                // reset the flag
                this.CalculateSalesTotals = true;
            }
        }

        /// <summary>
        /// Gets or sets the sub total.
        /// </summary>
        public decimal SubTotal
        {
            get
            {
                return this.subTotal;
            }

            set
            {
                this.subTotal = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public decimal Total
        {
            get
            {
                return this.total;
            }

            set
            {
                this.total = value;
                this.RaisePropertyChanged();
                this.CalculateMargin();
            }
        }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public decimal? OrderMargin
        {
            get
            {
                return this.orderMargin;
            }

            set
            {
                this.orderMargin = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public decimal? OrderValue
        {
            get
            {
                return this.orderValue;
            }

            set
            {
                this.orderValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public decimal OrderCost
        {
            get
            {
                return this.orderCost;
            }

            set
            {
                this.orderCost = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the discount value.
        /// </summary>
        public decimal Discount
        {
            get
            {
                return this.discount;
            }

            set
            {
                this.discount = value;
                this.RaisePropertyChanged();

                if (this.CalculateSalesTotals)
                {
                    this.UpdateSaleOrderTotals();
                }
           
                // reset the flag
                this.CalculateSalesTotals = true;
            }
        }

        /// <summary>
        /// Gets or sets the tax value.
        /// </summary>
        public decimal Tax
        {
            get
            {
                return this.tax;
            }

            set
            {
                this.tax = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the remarks text.
        /// </summary>
        public string Remarks
        {
            get
            {
                return this.remarks;
            }

            set
            {
                this.SetMode(value, this.remarks);
                this.remarks = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets any deleted items.
        /// </summary>
        public IList<SaleDetail> DeletedSaleDetails { get; set; }

        /// <summary>
        /// Gets or sets the sale details.
        /// </summary>
        public ObservableCollection<SaleDetail> SaleDetails
        {
            get
            {
                return this.saleDetails;
            }

            set
            {
                this.saleDetails = value;
                this.RaisePropertyChanged();

                if (value == null)
                {
                    this.ProductLineCount = $"{Strings.ProductLineCount}0";
                    return;
                }

                foreach (var saleDetail in value)
                {
                    saleDetail.UnitPriceOld = saleDetail.UnitPrice;

                    // shouldn't happen..but just in case
                    if (saleDetail.InventoryItem == null)
                    {
                        saleDetail.InventoryItem =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == saleDetail.INMasterID);
                    }
                }

                this.ProductLineCount = $"{Strings.ProductLineCount}{value.Count}";
            }
        }

        /// <summary>
        /// Gets or sets the order items recent order data.
        /// </summary>
        public ObservableCollection<ProductRecentOrderData> ProductRecentOrderDatas
        {
            get
            {
                return this.productRecentOrderData;
            }

            set
            {
                this.productRecentOrderData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected order items recent order data.
        /// </summary>
        public ProductRecentOrderData SelectedProductRecentOrderData
        {
            get
            {
                return this.selectedProductRecentOrderData;
            }

            set
            {
                this.selectedProductRecentOrderData= value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sales employees.
        /// </summary>
        public ObservableCollection<User> SalesEmployees
        {
            get
            {
                return this.salesEmployees;
            }

            set
            {
                this.salesEmployees = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected sales employee.
        /// </summary>
        public User SelectedSalesEmployee
        {
            get
            {
                return this.selectedSalesEmployee;
            }

            set
            {
                this.SetMode(value, this.selectedSalesEmployee);
                this.selectedSalesEmployee = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customers billing address.
        /// </summary>
        public BusinessPartnerAddress SelectedInvoiceAddress
        {
            get
            {
                return this.selectedInvoiceAddress;
            }

            set
            {
                this.SetMode(value, this.selectedInvoiceAddress);
                this.selectedInvoiceAddress = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customers delivery address.
        /// </summary>
        public string SelectedDeliveryAddressFull
        {
            get
            {
                return this.selectedDeliveryAddressFull;
            }

            set
            {
                this.selectedDeliveryAddressFull = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customers delivery address.
        /// </summary>
        public BusinessPartnerAddress SelectedDeliveryAddress
        {
            get
            {
                return this.selectedDeliveryAddress;
            }

            set
            {
                this.SetMode(value, this.selectedDeliveryAddress);
                this.selectedDeliveryAddress = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether a sale has a base document.
        /// </summary>
        public bool DoesSaleHaveBaseDocument
        {
            get
            {
                return this.Sale != null && !this.Sale.BaseDocumentReferenceID.IsNullOrZero();
            }
        }

        /// <summary>
        /// Gets or sets the nou doc status items.
        /// </summary>
        public IList<NouDocStatu> DocStatusItems { get; set; }
        
        /// <summary>
        /// Gets or sets the selected document status.
        /// </summary>
        public NouDocStatu SelectedDocStatus
        {
            get
            {
                return this.selectedDocStatus;
            }

            set
            {
                this.selectedDocStatus = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleDocStatusChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the valid until date.
        /// </summary>
        public DateTime? ValidUntilDate
        {
            get
            {
                return this.validUntilDate;
            }

            set
            {
                this.SetMode(value, this.validUntilDate);
                this.validUntilDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the delivery dtime.
        /// </summary>
        public string DeliveryTime
        {
            get
            {
                return this.deliveryTime;
            }

            set
            {
                this.SetMode(value, this.deliveryTime);
                this.deliveryTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        public DateTime? DeliveryDate
        {
            get
            {
                return this.deliveryDate;
            }

            set
            {
                this.SetMode(value, this.deliveryDate);
                this.deliveryDate = value;
                this.RaisePropertyChanged();

                if (!ApplicationSettings.UseShippingDate)
                {
                    if (this.Sale == null || this.Sale.SaleDetails == null || !this.Sale.SaleDetails.Any())
                    {
                        this.DeliveryDateTouchscreen = null;
                    }
                    else
                    {
                        this.DeliveryDateTouchscreen = value;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the delivery date.
        /// </summary>
        public DateTime? ProposedKillDate
        {
            get
            {
                return this.proposedKillDate;
            }

            set
            {
                this.SetMode(value, this.proposedKillDate);
                this.proposedKillDate= value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the shipping date.
        /// </summary>
        public DateTime? ShippingDate
        {
            get
            {
                return this.shippingDate;
            }

            set
            {
                this.SetMode(value, this.shippingDate);
                this.shippingDate = value;
                this.RaisePropertyChanged();

                if (ApplicationSettings.UseShippingDate)
                {
                    if (this.Sale == null || this.Sale.SaleDetails == null || !this.Sale.SaleDetails.Any())
                    {
                        this.DeliveryDateTouchscreen = null;
                    }
                    else
                    {
                        this.DeliveryDateTouchscreen = value;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the delivery date displayed on the touchscreen.
        /// </summary>
        public DateTime? DeliveryDateTouchscreen
        {
            get
            {
                return this.deliveryDateTouchscreen;
            }

            set
            {
                this.deliveryDateTouchscreen = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the document date.
        /// </summary>
        public DateTime? DocumentDate
        {
            get
            {
                return this.documentDate;
            }

            set
            {
                this.SetMode(value, this.documentDate);
                this.documentDate = value;
                this.RaisePropertyChanged();
            }
        }
      
        /// <summary>
        /// Gets or sets the selected partner.
        /// </summary>
        public ViewBusinessPartner SelectedPartner
        {
            get
            {
                return this.selectedPartner;
            }

            set
            {
                if (value != null && value.BPMasterID > 0)
                {
                    value = this.DataManager.GetBusinessPartnerShort(value.BPMasterID);
                }
        
                this.selectedPartner = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandlePartnerChange();
                }
                else
                {
                    // no customer selected, so disable the copy document combos.
                    this.CanCopyFrom = false;
                }

                this.HandlePartner();
            }
        }

        /// <summary>
        /// Gets or sets the selected partner.
        /// </summary>
        public ViewBusinessPartner SelectedHaulier
        {
            get
            {
                return this.selectedHaulier;
            }

            set
            {
                this.SetMode(value, this.selectedHaulier);
                this.selectedHaulier = value;
                this.RaisePropertyChanged();
            }
        }
      
        /// <summary>
        /// Gets or sets the del date width.
        /// </summary>
        public double DeliveryDateWidth
        {
            get
            {
                return this.deliveryDateWidth;
            }

            set
            {
                this.deliveryDateWidth = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customer contact.
        /// </summary>
        public BusinessPartnerContact SelectedContact
        {
            get
            {
                return this.selectedContact;
            }

            set
            {
                this.selectedContact = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The current orders grid column.
        /// </summary>
        public string CurrentColumn { get; set; }

        /// <summary>
        /// Gets or sets the selected customer delivery contact.
        /// </summary>
        public BusinessPartnerContact SelectedDeliveryContact
        {
            get
            {
                return this.selectedDeliveryContact;
            }

            set
            {
                this.selectedDeliveryContact = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the partner customers.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.customers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public ObservableCollection<InventoryItem> ReceipeItems
        {
            get
            {
                return this.receipeItems;
            }

            set
            {
                this.receipeItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public ObservableCollection<InventoryItem> SaleItems
        {
            get
            {
                return this.saleItems;
            }

            set
            {
                this.saleItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public ObservableCollection<InventoryItem> PurchaseItems
        {
            get
            {
                return this.purchaseItems;
            }

            set
            {
                this.purchaseItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application document numbers.
        /// </summary>
        public ObservableCollection<DocNumber> DocNumberings
        {
            get
            {
                return this.docNumberings;
            }

            set
            {
                this.docNumberings = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected document numbering.
        /// </summary>
        public DocNumber SelectedDocNumbering
        {
            get
            {
                return this.selectedDocNumbering;
            }

            set
            {
                this.SetMode(value, this.selectedDocNumbering);
                if ((value != null && this.selectedDocNumbering != null) && (value.DocumentNumberingTypeID != this.selectedDocNumbering.DocumentNumberingTypeID))
                {
                    this.NextNumber = value.NextNumber;
                }

                this.selectedDocNumbering = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customers contacts.
        /// </summary>
        public ObservableCollection<BusinessPartnerContact> Contacts
        {
            get
            {
                return this.contacts;
            }

            set
            {
                this.contacts = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customers addresses.
        /// </summary>
        public ObservableCollection<BusinessPartnerAddress> Addresses
        {
            get
            {
                return this.addresses;
            }

            set
            {
                this.addresses = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in copy document mode.
        /// </summary>
        public static bool CopyingDocument { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a document can be copied from.
        /// </summary>
        public bool CanCopyFrom
        {
            get
            {
                return this.canCopyFrom;
            }

            set
            {
                this.canCopyFrom = value;
                this.RaisePropertyChanged();

                // A stored document must be on display, and active, to enable the copy to combo.
                this.CanCopyTo = value && this.IsActiveDocument();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a document can be copied to.
        /// </summary>
        public bool CanCopyTo
        {
            get
            {
                return this.canCopyTo;
            }

            set
            {
                this.canCopyTo = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets or sets the command to view the base document(s).
        /// </summary>
        public ICommand ScrollRecentOrdersCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to view the base document(s).
        /// </summary>
        public ICommand ShowAllRecentOrdersCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to view the base document(s).
        /// </summary>
        public ICommand BaseDocumentsCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to direct the (non grid) F1 search.
        /// </summary>
        public ICommand DirectSearchCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to direct the (non grid) F1 search.
        /// </summary>
        public ICommand DirectSearchCommandF2 { get; set; }

        /// <summary>
        /// Gets or sets the command to handle the column focus change.
        /// </summary>
        public ICommand OnFocusedColumnChangedCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to handle the items grid focus/unfocus.
        /// </summary>
        public ICommand OnFocusedCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to display the search screen.
        /// </summary>
        public ICommand DisplaySearchScreenCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to display the search screen.
        /// </summary>
        public ICommand DisplaySearchScreenF2Command { get; set; }

        /// <summary>
        /// Gets the command to add a sale item to the current order.
        /// </summary>
        public ICommand AddToOrderCommand { get; private set; }  

        /// <summary>
        /// Gets the command to attach a file.
        /// </summary>
        public ICommand AttachFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to displaya file.
        /// </summary>
        public ICommand DisplayFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to delete a file.
        /// </summary>
        public ICommand DeleteFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the search grid.
        /// </summary>
        public ICommand ShowSearchGridCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a combo box edit value change.
        /// </summary>
        public ICommand EditValueChangingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unload.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the grid cell value changing event.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down to user.
        /// </summary>
        public ICommand DrillDownToUserCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down to the inventory item.
        /// </summary>
        public ICommand DrillDownToItemCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down to the inventory item.
        /// </summary>
        public ICommand DrillDownToSpecCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down to the inventory item.
        /// </summary>
        public ICommand DrillDownToBatchesCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down to the master inventory item.
        /// </summary>
        public ICommand DrillDownToMasterItemCommand { get; private set; }

         /// <summary>
        /// Gets the command to handle the drill down to the price list
        /// </summary>
        public ICommand DrillDownToPriceListCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down to the price list
        /// </summary>
        public ICommand DrillDownToCostPriceListCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Copies a sale to another document.
        /// </summary>
        /// <param name="saleToCopy">The sale to copy to.</param>
        public virtual void CopySale(Sale saleToCopy)
        {
            #region validation

            this.Log.LogWarning(this.GetType(), "4. CopySale(): Is document active?");
            if (!this.IsActiveDocument())
            {
                this.Log.LogWarning(this.GetType(), "5. CopySale(): No");
                return;
            }

            this.Log.LogWarning(this.GetType(), "5. CopySale(): Yes");

            #endregion

            this.DisablePartnerPopUp = true;
            this.Log.LogDebug(this.GetType(), "CopySale(): Copying sale");

            if (saleToCopy.SaleDetails != null)
            {
                // need to attach the inventory item, as it's needed for the in master snapshot.
                foreach (var detail in saleToCopy.SaleDetails)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }
            }

            if (ApplicationSettings.AccountsCustomer.CompareIgnoringCase(Customer.Woolleys))
            {
                this.Sale.DocumentDate = saleToCopy.CreationDate;
            }

            this.Sale = saleToCopy;

            //var dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);
            
            this.Sale.SaleID = 0;
            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Warns user that the product selected is not on the customers price list.
        /// </summary>
        public void HandleProductNotOnPriceList()
        {
            if (!ApplicationSettings.TouchScreenMode)
            {
                SystemMessage.Write(MessageType.Issue, Message.ProductNotOnPriceList);
            }
        }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public virtual void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                if (!this.AllSales.Any())
                {
                    return;
                }

                this.Sale = this.AllSales.First();
                return;
            }

            var previousSale =
                this.AllSales.TakeWhile(
                    item => item.SaleID != this.Sale.SaleID)
                    .LastOrDefault();

            if (previousSale != null)
            {
                this.Sale = previousSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public virtual void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                if (!this.AllSales.Any())
                {
                    return;
                }

                this.Sale = this.AllSales.Last();
                return;
            }

            var nextItem =
                this.AllSales.SkipWhile(
                    item => item.SaleID != this.Sale.SaleID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.Sale = nextItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public virtual void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (!this.AllSales.Any())
            {
                return;
            }

            this.Sale = this.AllSales.First();
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public virtual void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (!this.AllSales.Any())
            {
                return;
            }

            this.Sale = this.AllSales.Last();
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public virtual void MoveLastEdit()
        {
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        #endregion

        #endregion

        #region protected

        #region override

        protected void CheckForUnsavedData()
        {
            if (this.CurrentMode == ControlMode.Update || (this.CurrentMode == ControlMode.Add && this.SaleDetails.Any()))
            {
                NouvemMessageBox.Show(Message.UnsavedDataPrompt, NouvemMessageBoxButtons.OKCancel);
                if (NouvemMessageBox.UserSelection == UserDialogue.OK)
                {
                    this.AllowWindowToClose();                   
                }
            }
            else
            {
                this.AllowWindowToClose();
            }
        }

        /// <summary>
        /// Checks an order total against the partner limit.
        /// </summary>
        /// <returns>Flag, as to whther order can proceed or not.</returns>
        protected bool CheckOrderLimit()
        {
            if (!ApplicationSettings.TouchScreenMode && this.SelectedPartner != null
                && this.SelectedDocStatus.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
            {
                var orderTotal = Math.Round(this.GetOrderValue(this.Sale), 2);
                var currency = this.SelectedPartner.Symbol ?? "€";

                // check the order limits first..
                if (this.SelectedPartner.OrderLimit > 0)
                {
                    var partnerLimit = this.SelectedPartner.OrderLimit.ToDouble();
                    if (orderTotal > partnerLimit)
                    {
                        if (this.CanOverrideOrderLimit)
                        {
                            var msg = string.Format(Message.OverOrderLimitWarning, currency.Trim(), orderTotal, partnerLimit);
                            SystemMessage.Write(MessageType.Issue, msg);
                            NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo);
                            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            var msg = string.Format(Message.OverOrderLimitStop, currency.Trim(), orderTotal, partnerLimit);
                            SystemMessage.Write(MessageType.Issue, msg);
                            NouvemMessageBox.Show(msg);
                            return false;
                        }
                    }
                }

                // check the credit limits..
                if (this.SelectedPartner.CreditLimit > 0)
                {
                    var partnerLimit = this.SelectedPartner.CreditLimit.ToInt();
                    var orderAndBalance = this.SelectedPartner.Balance.ToDecimal() + orderTotal.ToDecimal();
                    if (orderAndBalance > partnerLimit)
                    {
                        if (this.CanOverrideCreditLimit || this.Sale.SaleID > 0)
                        {
                            var msg = string.Format(Message.OverCreditLimitWarning, currency.Trim(), orderTotal, this.accountBalance, orderAndBalance, partnerLimit);
                            SystemMessage.Write(MessageType.Issue, msg);
                            NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo);
                            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            var msg = string.Format(Message.OverCreditLimitStop, currency.Trim(), orderTotal, this.accountBalance, orderAndBalance, partnerLimit);
                            SystemMessage.Write(MessageType.Issue, msg);
                            NouvemMessageBox.Show(msg);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Gets an order total value.
        /// </summary>
        protected double GetOrderValue(Sale order)
        {
            var details = order.SaleDetails;
            if (details == null || !details.Any())
            {
                return 0;
            }

            decimal price;
            double total = 0;
            foreach (var saleDetail in details)
            {
                price = saleDetail.UnitPrice.ToDecimal();
                if (saleDetail.PriceMethod == PriceMethod.Weight)
                {
                    total += saleDetail.WeightOrdered.ToDouble() * price.ToDouble();
                }
                else if (saleDetail.PriceMethod == PriceMethod.Quantity)
                {
                    total += saleDetail.QuantityOrdered.ToDouble() * price.ToDouble();
                }
                else
                {
                    if (saleDetail.INMaster != null)
                    {
                        total += ((saleDetail.QuantityOrdered.ToDouble() * saleDetail.INMaster.NominalWeight.ToDouble()) * price.ToDouble());
                    }
                }
            }

            return total;
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new or find partner, clearing the form.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new user mode, so clear the fields.
                this.ClearForm(true);
                this.SetControlMode(ControlMode.Add);
                this.ResetDocumentNumber();
                this.RefreshDocStatusItems();
                CopyingDocument = false;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                return;
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into add new user mode, so clear the partner fields.
                //this.SelectedDocNumbering = null;
                //this.NextNumber = 0;
                this.ClearForm();
                this.RefreshDocStatusItems();
                this.SetControlMode(ControlMode.Find);
                CopyingDocument = false;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Override, to disable any writes when docket is part of a parent document.
        /// </summary>
        /// <param name="mode">The control mode.</param>
        protected override void SetControlMode(ControlMode mode)
        {
            if (this.IsBaseDocument)
            {
                if (mode == ControlMode.Add || mode == ControlMode.Update)
                {
                    mode = ControlMode.OK;
                }
            }

            if (this.DisableDocStatusChange && mode == ControlMode.Update)
            {
                 return;
            }

            base.SetControlMode(mode);
        }

        /// <summary>
        /// Handle the drilldown command.
        /// </summary>
        protected void DrillDownToSpecCommandExecute()
        {
            var productId = this.SelectedSaleDetail?.INMasterID;
            var partnerId = this.SelectedPartner?.BPMasterID;
            if (this.SelectedSaleDetail == null)
            {
                return;
            }
           
            Messenger.Default.Send(ViewType.Specs);
            Messenger.Default.Send(Tuple.Create(partnerId, productId), Token.DrillDownToSpec);
        }

        /// <summary>
        /// Handle the drilldown command.
        /// </summary>
        protected void DrillDownToBatchesCommandExecute()
        {             
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            IList<App_GetOrderBatchData_Result> lineData;
            if (this.SelectedSaleDetail.SaleDetailID > 0)
            {
                lineData = this.DataManager.GetOrderBatchData(this.SelectedSaleDetail.SaleDetailID);
            }
            else
            {
                lineData = this.SelectedSaleDetail.OrderBatchData;
            }

            if (lineData == null)
            {
                lineData = new List<App_GetOrderBatchData_Result>();
            }

            var productId = this.SelectedSaleDetail?.INMasterID;        
            Messenger.Default.Send(Tuple.Create(productId, lineData), ViewType.BatchData);
        }


        /// <summary>
        /// Handle the drilldown command.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.BPMaster, this.selectedPartner.BPMasterID), Token.DrillDown);
        }

        /// <summary>
        /// Determines if the current sale already exists in the db.
        /// </summary>
        /// <returns>A flag, as to whether the current sale already exists in the db.</returns>
        protected override bool CheckForEntity()
        {
            return this.Sale != null && this.Sale.SaleID > 0;
        }

        #endregion

        #region virtual
        protected virtual bool ResetQtyToZero() { return false; }
        public virtual void RefreshSales(){}
        protected virtual void HandleBatchValue() { }
        protected virtual void HandleActualBatchValue() { }
        protected virtual void AllowWindowToClose() { }

        /// <summary>
        /// Checks the sale order product lines stock availability.
        /// </summary>
        /// <returns>Flag, true if sufficient stock, false if not.</returns>
        protected virtual bool CheckStockAvailability()
        {
            if (!ApplicationSettings.CheckAvailableStockOnSaleOrders)
            {
                return true;
            }

            var notEnoughStockProducts = new List<string>();
            foreach (var saleDetail in this.SaleDetails)
            {
                if (!saleDetail.IsSufficientStockForOrder)
                {
                    notEnoughStockProducts.Add(saleDetail.InventoryItem.Name);
                }
            }

            if (!notEnoughStockProducts.Any())
            {
                return true;
            }

            var message = $"The following products have insufficient stock to fill the order -{Environment.NewLine}";
            foreach (var notEnoughStockProduct in notEnoughStockProducts)
            {
                message += $"{notEnoughStockProduct}{Environment.NewLine}";
            }

            message += "Do you wish to proceed?";
            NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo);
            return NouvemMessageBox.UserSelection == UserDialogue.Yes;
        }

        /// <summary>
        /// Validates a time (must be HH:MM)
        /// </summary>
        /// <param name="time">The time to validate.</param>
        /// <returns>Flag, as to valid time or not.</returns>
        protected virtual bool ValidateTime(string time)
        {
            return true;
        }       

        /// <summary>
        /// Handle a partner change.
        /// </summary>
        protected virtual void HandlePartnerChange()
        {
            if (!this.EntitySelectionChange)
            {
                if (ApplicationSettings.AllowCustomerChangeOnOrders)
                {
                    if (this.SaleDetails == null || !this.SaleDetails.Any())
                    {
                        this.ClearForm(false);
                    }
                }
                else
                {
                    this.ClearForm(false);
                }
            }

            this.PartnerName = this.selectedPartner.Name;
            this.ParsePartner();
            this.CanCopyFrom = true;
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected virtual void HandleSearchSale(Sale searchSale)
        {
        }

        /// <summary>
        /// Handle the column focus change.
        /// </summary>
        /// <param name="e">The event arg.</param>
        protected virtual void OnFocusedColumnChangedCommandExecute(EditorEventArgs e)
        {
            if (this.IsReadOnly)
            {
                return;
            }

            if (e != null && e.Column != null && e.Column.FieldName.Equals("CostTotal"))
            {
                this.DrillDownToCostPriceListCommandExecute();
                return;
            }

            if (e != null && e.Column != null && e.Column.FieldName.Equals("UnitPrice"))
            {
                if (e.Editor.EditValue != null && e.Row is SaleDetail
                    && (decimal?)e.Editor.EditValue != (e.Row as SaleDetail).UnitPriceOld)
                {
                    (e.Row as SaleDetail).UnitPriceOld = (decimal?)e.Editor.EditValue;
                    
                    try
                    {
                        var addingToCustomerBook = false;
                        var message = Message.AddUnitPriceChangeToPriceBookConfirmation;
                        if (this.SelectedSaleDetail.PriceListID != this.SelectedPartner.PriceListID)
                        {
                            addingToCustomerBook = true;
                            message = Message.AddProductAndUnitPriceChangeToPriceBookConfirmation;
                        }

                        var currency = this.SelectedPartner != null && this.SelectedPartner.Symbol != null ? this.SelectedPartner.Symbol.Trim() : ApplicationSettings.CustomerCurrency;

                        if (this.AllowPriceChangeToPriceBook())
                        {
                            NouvemMessageBox.Show(
                                string.Format(message, currency, this.SelectedSaleDetail.UnitPrice), NouvemMessageBoxButtons.YesNo);
                            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                            {
                                if (this.SelectedSaleDetail.PriceListID == 0)
                                {
                                    SystemMessage.Write(MessageType.Issue, Message.NoPriceBookExists);
                                    return;
                                }

                                this.UpdatePriceToPriceBook();
                                //System.Threading.Tasks.Task.Factory.StartNew(this.RefreshPriceLists);
                                //this.RefreshPriceDetails();
                                if (addingToCustomerBook)
                                {
                                    this.SelectedSaleDetail.LoadingSale = true;
                                    this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                                }
                            }
                        }

                        this.UpdateSaleDetailTotals();
                        this.SelectedSaleDetail.PriceHasBeenManuallyChanged = true;


                        if (this.Sale != null && this.Sale.SaleID > 0)
                        {
                            this.SetControlMode(ControlMode.Update);
                        }
                        else
                        {
                            this.SetControlMode(ControlMode.Add);
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), ex.Message);
                        SystemMessage.Write(MessageType.Issue, ex.Message);
                    }
                }
            }
            //if (e.OldColumn != null && e.OldColumn.ActualColumnChooserHeaderCaption.ToString().Equals("Unit Price") && this.unitPriceChanged && this.selectedSaleDetail.UnitPrice != null)
            //{
            //    try
            //    {
            //        NouvemMessageBox.Show(
            //            string.Format(Message.AddUnitPriceChangeToPriceBookConfirmation,
            //                this.selectedSaleDetail.UnitPrice), NouvemMessageBoxButtons.YesNo);
            //        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
            //        {
            //            if (this.selectedSaleDetail.PriceListID == 0)
            //            {
            //                SystemMessage.Write(MessageType.Issue, Message.NoPriceBookExists);
            //                return;
            //            }

            //            this.UpdatePriceToPriceBook();
            //            System.Threading.Tasks.Task.Factory.StartNew(this.RefreshPriceLists);
            //        }

            //        this.UpdateSaleDetailTotals();
            //    }
            //    catch (Exception ex)
            //    {
            //        this.Log.LogError(this.GetType(), ex.Message);
            //        SystemMessage.Write(MessageType.Issue, ex.Message);
            //    }
            //    finally
            //    {
            //        this.unitPriceChanged = false;
            //    }
            //}
        }

        protected virtual bool AllowPriceChangeToPriceBook()
        {
            return true;
        }

        /// <summary>
        /// Updates the selected sale detail totals.
        /// </summary>
        protected virtual void UpdateSaleDetailTotals()
        {
            if (this.selectedSaleDetail != null)
            {
                // goods in and dispatch will override.
                this.selectedSaleDetail.UpdateTotal();
            }
        }

        /// <summary>
        /// Checks the route and products match.
        /// </summary>
        /// <returns>A string, containing an error if no match and not proceeding.</returns>
        protected string CheckRoutesAtOrder()
        {
            #region validation

            if (this.selectedRoute == null)
            {
                return "No route selected?";
            }

            #endregion

            var route = this.SelectedRoute;
            var error = string.Empty;
            switch (route.Name)
            {
                case "Slicing":
                    foreach (var saleDetail in this.SaleDetails)
                    {
                        if (saleDetail.InventoryItem == null)
                        {
                            saleDetail.SetInventoryItem();
                        }

                        if (saleDetail.InventoryItem != null && !saleDetail.InventoryItem.Code.ContainsIgnoringCase("SL"))
                        {
                            error = "Warning. There are non slicing products on this order. Do you wish to complete it?";
                            break;
                        }
                    }

                    break;

                case "Raw":
                    foreach (var saleDetail in this.SaleDetails)
                    {
                        if (saleDetail.InventoryItem == null)
                        {
                            saleDetail.SetInventoryItem();
                        }

                        if (saleDetail.InventoryItem != null && !saleDetail.InventoryItem.Code.ContainsIgnoringCase("Raw"))
                        {
                            error = "Warning. There are non raw products on this order. Do you wish to complete it?";
                            break;
                        }
                    }

                    break;

                case "Joints":
                    foreach (var saleDetail in this.SaleDetails)
                    {
                        if (saleDetail.InventoryItem == null)
                        {
                            saleDetail.SetInventoryItem();
                        }
                        if (saleDetail.InventoryItem != null && (saleDetail.InventoryItem.Code.ContainsIgnoringCase("SL") || saleDetail.InventoryItem.Code.ContainsIgnoringCase("Raw")))
                        {
                            error = "Warning. There are non joints products on this order. Do you wish to complete it?";
                            break;
                        }
                    }

                    break;
            }

            if (error != string.Empty)
            {
                NouvemMessageBox.Show(error, NouvemMessageBoxButtons.YesNo);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    error = string.Empty;
                }
                else
                {
                    error = "Order completion cancelled";
                }
            }

            return error;
        }

        /// <summary>
        /// Overriden, to display the searh screen.
        /// </summary>
        protected virtual void DisplaySearchScreenCommandExecute(Tuple<bool, ViewType> data)
        {
            var items = data.Item1 ? this.PurchaseItems : this.SaleItems;
            this.Locator.InventorySearchData.SetFilteredItems(items);
            this.Locator.InventorySearchData.SetModule(data.Item2);

            // Multiple matches, so show them in the search grid.
            Messenger.Default.Send(ViewType.InventorySearchData);
        }

        /// <summary>
        /// Creates a sale order object.
        /// </summary>
        protected virtual void CreateSale()
        {
            // header
            this.Sale.QuoteValidDate = this.ValidUntilDate;
            this.Sale.DeliveryDate = this.DeliveryDate;
            this.Sale.ShippingDate = this.ShippingDate;
            this.Sale.ProposedKillDate = this.ProposedKillDate;
            this.Sale.DocumentDate = this.DocumentDate;
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.DeliveryTime = this.deliveryTime;
            this.Sale.Remarks = this.Remarks;
            this.Sale.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;
            this.Sale.BPContactID = this.selectedContact != null ? (int?)this.selectedContact.Details.BPContactID : null;
            this.Sale.DocketNote = this.docketNote;
            this.Sale.InvoiceNote = this.invoiceNote;
            this.Sale.PopUpNote = this.PopUpNote;
            this.Sale.SalesEmployeeID = this.selectedSalesEmployee != null ? (int?)this.selectedSalesEmployee.UserMaster.UserMasterID : null;
            this.Sale.VAT = this.Tax;
            this.Sale.DeviceId = NouvemGlobal.DeviceId;
            this.Sale.EditDate = DateTime.Now;
            this.Sale.SubTotalExVAT = this.subTotal;
            this.Sale.DiscountIncVAT = this.Discount;
            this.Sale.GrandTotalIncVAT = this.Total;
            this.Sale.OrderCost = this.OrderCost;
            this.Sale.OrderMargin = this.OrderMargin;
            this.Sale.DiscountPercentage = this.DiscountPercentage;
            this.Sale.Customer = this.selectedPartner;
            this.Sale.Haulier = this.selectedHaulier;
            this.Sale.AgentID = this.SelectedAgent?.BPMasterID;
            this.Sale.DeliveryAddress = this.selectedDeliveryAddress;
            this.Sale.InvoiceAddress = this.selectedInvoiceAddress;
            this.Sale.DeliveryContact = this.selectedDeliveryContact;
            this.Sale.MainContact = this.selectedContact;
            this.Sale.PORequired = this.PORequired;

            if (!string.IsNullOrEmpty(this.CustomerPOReference))
            {
                this.Sale.CustomerPOReference = this.CustomerPOReference;
            }

            if (this.SelectedRoute != null && this.SelectedRoute.RouteID > 0)
            {
                this.Sale.RouteID = this.SelectedRoute.RouteID;
            }

            // add the sale details
            this.Sale.SaleDetails = this.SaleDetails.Where(x => x.INMasterID > 0).ToList();
            if (this.DeletedSaleDetails != null && this.DeletedSaleDetails.Any())
            {
                foreach (var deletedSaleDetail in this.DeletedSaleDetails)
                {
                    this.Sale.SaleDetails.Add(deletedSaleDetail);
                }

                this.DeletedSaleDetails.Clear();
            }
        }

        /// <summary>
        /// Get the localised document numberings.
        /// </summary>
        protected virtual void GetLocalDocNumberings()
        {
            this.GetDocNumbering();
        }

        protected virtual void HandlePartner() { }

        /// <summary>
        /// Handles a sale selection.
        /// </summary>
        protected virtual void HandleSelectedSale()
        {
            #region validation

            if (this.sale == null || this.sale.SaleID == 0)
            {
                return;
            }

            if (CopyingDocument && !this.IsActiveDocument())
            {
                SystemMessage.Write(MessageType.Issue, Message.ClosedDocumentCopyRestriction);
                this.ClearForm(false);
                this.SetControlMode(ControlMode.Add);
                return;
            }

            #endregion

            try
            {
                this.EntitySelectionChange = true;

                this.IsReadOnly = false;
                this.DisableDocStatusChange = false;
                this.IsBaseDocument = false;

                // header
                var partnerId = this.sale.BPCustomer != null
                    ? this.sale.BPCustomer.BPMasterID
                    : this.sale.Customer.BPMasterID;

                this.AddInactiveSelectedPartner(partnerId);

                this.SelectedAgent = null;
                this.SelectedPartner = this.allPartners.FirstOrDefault(x => x.BPMasterID == partnerId);

                if (CopyingDocument || this.sale.CopyingFromOrder)
                {
                    // Copying document, so we want to use the next document number (Not the document number of the copied sale)
                    this.ResetDocumentNumber();
                }
                else
                {
                    // Not copying, so use the selected sales document number.
                    this.SelectedDocNumbering =
                        this.DocNumberings.FirstOrDefault(x => x.DocumentNumberingID == this.sale.DocumentNumberingID);
                    this.NextNumber = CopyingDocument && this.SelectedDocNumbering != null
                        ? this.SelectedDocNumbering.NextNumber
                        : this.Sale.Number;
                }

                this.SelectedDocStatus =
                    this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.sale.NouDocStatusID);
                this.ValidUntilDate = this.Sale.QuoteValidDate;
                this.DeliveryDate = this.Sale.DeliveryDate;
                this.ProposedKillDate = this.Sale.ProposedKillDate;
                this.ShippingDate = this.Sale.ShippingDate;
                this.DocumentDate = this.Sale.DocumentDate;
                this.DiscountPercentage = this.Sale.DiscountPercentage.ToDecimal();
                this.Remarks = this.Sale.Remarks;
                this.CustomerPOReference = this.Sale.CustomerPOReference;
                this.SelectedSalesEmployee = this.sale.SalesEmployeeID != null
                    ? this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == this.sale.SalesEmployeeID)
                    : null;
                this.SubTotal = this.sale.SubTotalExVAT.ToDecimal();
                this.Tax = this.sale.VAT.ToDecimal();
                this.Discount = this.sale.DiscountIncVAT.ToDecimal();
                this.Total = this.sale.GrandTotalIncVAT.ToDecimal();
                this.DocketNote = this.Sale.DocketNote;
                this.PopUpNote = this.Sale.PopUpNote;
                this.DeliveryTime = this.Sale.DeliveryTime;
                this.InvoiceNote = this.Sale.InvoiceNote;
                this.SelectedDeliveryAddress = this.sale.DeliveryAddress != null &&
                                               this.sale.DeliveryAddress.Details != null
                    ? this.Addresses.FirstOrDefault(
                        x => x.Details.BPAddressID == this.sale.DeliveryAddress.Details.BPAddressID)
                    : null;
                this.SelectedInvoiceAddress = this.sale.InvoiceAddress != null &&
                                              this.sale.InvoiceAddress.Details != null
                    ? this.Addresses.FirstOrDefault(
                        x => x.Details.BPAddressID == this.sale.InvoiceAddress.Details.BPAddressID)
                    : null;

                if (this.Sale.AgentID.HasValue)
                {
                    this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == this.Sale.AgentID);
                }

                this.SelectedHaulier = null;
                this.SelectedContact = null;
                this.SelectedDeliveryContact = null;
                this.SaleAttachments = null;
                this.SelectedRoute = null;
                this.SelectedAgent = null;

                if (this.Sale.AgentID.HasValue)
                {
                    this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == this.Sale.AgentID);
                }

                if (this.sale.BPHaulier != null)
                {
                    this.SelectedHaulier =
                        this.Hauliers.FirstOrDefault(x => x.BPMasterID == this.sale.BPHaulier.BPMasterID);
                }
                else if (this.sale.Haulier != null)
                {
                    this.SelectedHaulier =
                        this.Hauliers.FirstOrDefault(x => x.BPMasterID == this.sale.Haulier.BPMasterID);
                }

                if (this.sale.MainContact != null && this.sale.MainContact.Details != null)
                {
                    this.SelectedContact =
                        this.Contacts.FirstOrDefault(
                            x => x.Details.BPContactID == this.sale.MainContact.Details.BPContactID);
                }

                if (this.sale.DeliveryContact != null && this.sale.DeliveryContact.Details != null)
                {
                    this.SelectedDeliveryContact =
                        this.Contacts.FirstOrDefault(
                            x => x.Details.BPContactID == this.sale.DeliveryContact.Details.BPContactID);
                }

                if (this.sale.Attachments != null)
                {
                    this.SaleAttachments = new ObservableCollection<Attachment>(this.sale.Attachments);
                }

                if (!this.sale.RouteID.IsNullOrZero() && this.Routes != null)
                {
                    this.SelectedRoute = this.Routes.FirstOrDefault(x => x.RouteID == this.sale.RouteID);
                }

                this.PORequired = this.sale.PORequired.ToBool();

                // Add the sale details.
                if (this.sale.SaleDetails != null)
                {
                    this.SaleDetails = new ObservableCollection<SaleDetail>(this.sale.SaleDetails);
                    this.CalculateMargin();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.OK);
                this.SearchTermCode = string.Empty;
                this.SearchTermName = string.Empty;

                // if we are copying a document then we need to put in into 'Add' mode.
                if (CopyingDocument)
                {
                    this.SetControlMode(ControlMode.Add);
                    this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
                    this.Sale.SaleID = 0;
                    CopyingDocument = false;
                }
            }
        }

        /// <summary>
        /// Handles a document status change.
        /// </summary>
        protected virtual void HandleDocStatusChange()
        {
            if (!this.EntitySelectionChange)
            {
                if (this.Sale != null && this.Sale.SaleID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Handles a sale detail selection.
        /// </summary>
        protected virtual void HandleSelectedSaleDetail(){}

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected virtual void BaseDocumentsCommandExecute(string command){}

        /// <summary>
        /// Shows all the partner recent orders.
        /// </summary>
        protected virtual void ShowAllRecentOrdersCommandExecute(){}

        /// <summary>
        /// Handles a grid field change.
        /// </summary>
        protected virtual void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (e == null || e.Cell == null || e.Value == null || string.IsNullOrEmpty(e.Cell.Property))
            {
                return;
            }

            #endregion

            try
            {
                if (e.Cell.Property.Equals("PriceListID") && this.SelectedPartner != null && this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null && this.SelectedSaleDetail.INMasterID > 0)
                {
                    var inMaster = this.selectedSaleDetail.InventoryItem.Master;
                    var pricelistId = (int)e.Value;
                    var inMasterId = this.SelectedSaleDetail.INMasterID;
                    var localPriceList = this.DataManager.GetPriceListDetail(pricelistId,inMasterId);

                    if (localPriceList == null)
                    {
                        // not on a price book. Ask the user if they want to add the product to the price list
                        NouvemMessageBox.Show(Message.AddProductToPriceLIstConfirmation, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.priceListUpdateDetails = new Tuple<int, int>(pricelistId, inMasterId);

                            Messenger.Default.Send(ViewType.PriceListDetail);
                            this.Locator.PriceListDetail.AddProductToPriceListDetail(pricelistId,
                                inMaster);
                            this.GetPriceLists();
                        }
                    }
                }

                if (!this.ControlModeWrite)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogDebug(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Shows all the partner recent orders.
        /// </summary>
        protected virtual void ScrollRecentOrdersCommandExecute(string direction)
        {
            this.SetRecentOrders(direction.Equals("Back"));
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected virtual void UpdateSaleOrderTotals(bool calcDisAmountOnly = true, bool calcDisPercentOnly = false)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            // reset the totals/taxes
            this.SubTotal = 0;
            this.Total = 0;
            this.Tax = 0;
            this.OrderCost = 0;
            this.OrderMargin = 0;
            this.OrderValue = 0;

            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }

            // get the accumulated totals and taxes.
            this.SaleDetails.ToList().ForEach(item =>
            {
                this.OrderCost += item.CostTotal.ToDecimal();
                this.SubTotal += item.TotalExVAT.ToDecimal();
                this.Total += item.TotalIncVAT.ToDecimal();
            });
            
            // Set the tax value.
            if (this.Total > this.SubTotal)
            {
                this.Tax = this.Total - this.SubTotal;
            }
        
            if (calcDisPercentOnly)
            {
                this.CalculateSalesTotals = false;

                // apply the discount (if any)
                if (this.DiscountPercentage > 0)
                {
                    var convertedDiscount = 100M - this.DiscountPercentage.ToDecimal();
                    this.Tax = Math.Round(this.tax / 100 * convertedDiscount, 2);
                    var discountedTotal = Math.Round(this.Total / 100 * convertedDiscount, 2);

                    // convert the discount amount.
                    this.Discount = this.Total - discountedTotal;

                    // assign the discounted total.
                    this.Total = discountedTotal;
                }
                else
                {
                    this.Discount = 0;
                }
            }

            if (calcDisAmountOnly)
            {
                this.CalculateSalesTotals = false;

                // apply the discount amount (if any)
                if (this.Discount > 0)
                {
                    if (this.SubTotal > 0)
                    {
                        this.DiscountPercentage = Math.Round((this.Discount / this.SubTotal) * 100, 2).ToDecimal();
                    }
                   
                    this.Total = this.Total - this.Discount;
                }
                else
                {
                    this.DiscountPercentage = 0;
                }
            }
        }

        /// <summary>
        /// Parses the selected customer data.
        /// </summary>
        /// <remarks>Virtual, so as to allow the child vms to implement their own additional logic.</remarks>
        protected virtual void ParsePartner()
        {
            if (this.selectedPartner == null)
            {
                return;
            }

            this.CustomerPopUpNote = string.Empty;
            var customer = this.DataManager.GetBusinessPartner(this.selectedPartner.BPMasterID);
            if (customer != null)
            {
                if (!ApplicationSettings.IgnoreCustomerWithNoPriceListCheck && customer.Details.PriceListID.IsNullOrZero())
                {
                    this.SelectedPartner = null;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        if (ApplicationSettings.ScannerMode)
                        {
                            NouvemMessageBox.Show(string.Format(Message.CustomerDoesNotHavePriceList, customer.Details.Name), scanner:true);
                        }
                        else
                        {
                            NouvemMessageBox.Show(string.Format(Message.CustomerDoesNotHavePriceList, customer.Details.Name), touchScreen: ApplicationSettings.TouchScreenMode);
                        }
                    }));

                    return;
                }

                if (customer.Details.RouteMasterID.HasValue)
                {
                    this.SelectedRoute = this.Routes.FirstOrDefault(x => x.RouteID == customer.Details.RouteMasterID);
                }

                this.PartnerInvoiceNote = customer.Details.InvoiceNote;
                this.selectedPartner.PopUpNotes = customer.Details.PopUpNotes;
                this.CustomerPopUpNote = customer.Details.PopUpNotes;

                this.SelectedContact = null;
                //this.SelectedSalesEmployee = null;
                this.SelectedDeliveryContact = null;
                this.SelectedInvoiceAddress = null;
                this.SelectedDeliveryAddress = null;
                this.AccountBalance = customer.Details.Balance;
                this.CreditLimit = customer.Details.CreditLimit;
                //this.PartnerCurrency = customer.PartnerCurrency?.Symbol ?? ApplicationSettings.DefaultCurrency;
                this.PartnerCurrency =
                    customer.PartnerCurrency != null && !string.IsNullOrEmpty(customer.PartnerCurrency.Symbol)
                        ? customer.PartnerCurrency.Symbol
                        : ApplicationSettings.DefaultCurrency;

                this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == customer.Details.BPMasterID_Agent);

                if (!ApplicationSettings.TouchScreenMode)
                {
                    customer.Contacts = this.DataManager.GetBusinessPartnerContactsById(customer.Details.BPMasterID);
                }
      
                if (customer.Contacts != null)
                {
                    this.Contacts = new ObservableCollection<BusinessPartnerContact>(customer.Contacts);
                    var primaryContact = customer.Contacts.FirstOrDefault(x => x.Details.PrimaryContact == true);

                    if (primaryContact != null)
                    {
                        this.SelectedContact = primaryContact;
                    }
                }

                if (customer.Addresses != null)
                {
                    this.Addresses = new ObservableCollection<BusinessPartnerAddress>(customer.Addresses.OrderBy(x => x.FullAddress));
                    var billingAddress = customer.Addresses.FirstOrDefault(x => x.Details.Billing == true);
                    var deliveryAddress = customer.Addresses.FirstOrDefault(x => x.Details.Shipping == true);

                    if (billingAddress != null)
                    {
                        this.SelectedInvoiceAddress = billingAddress;
                    }

                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                    }
                }

                if (!this.EntitySelectionChange)
                {
                    if (this.Sale != null && this.Sale.SaleID > 0)
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Handler for the ui load event.
        /// </summary>
        protected virtual void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetActiveDocument(this);
            this.SetWriteAuthorisation(Authorisation.AllowUserToCreateDispatches);

            if (this.HasWriteAuthorisation)
            {
                this.SetControlMode(ControlMode.Add);
            }
            else
            {
                this.SetControlMode(ControlMode.OK);
            }

            this.GetDocNumbering();
            //this.GetPriceLists();
            //this.RefreshPriceDetails();
            ApplicationSettings.SalesSearchToDate = DateTime.Today;
            ApplicationSettings.SalesSearchFromDate = DateTime.Today;
        }

        /// <summary>
        /// Handler for the ui unload event.
        /// </summary>
        protected virtual void OnUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
            //SaleDetail.PriceChangingOnDesktop = false;
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected virtual void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems
                               .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                || x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ||
                                x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Updates the sale details with any price change.
        /// </summary>
        /// <param name="details">The updated details.</param>
        protected virtual void HandlePriceListUpdates(List<PriceDetail> details)
        {
            foreach (var saleDetail in this.SaleDetails)
            {
                foreach (var priceDetail in details)
                {
                    if (saleDetail.INMasterID == priceDetail.INMasterID &&
                        saleDetail.PriceListID == priceDetail.PriceListID)
                    {
                        saleDetail.UnitPrice = priceDetail.Price;
                        saleDetail.UpdateTotal();
                    }
                }
            }
        }

        protected virtual void SetRecentOrders(bool back)
        {
            var recentOrders = this.CustomerSales.OrderByDescending(x => x.CreationDate).ToList();
            var localOrders = new List<Sale>();

            // We're binding the recent order headers to their dates.
            if (!recentOrders.Any())
            {
                this.ResetOrderDates();
                return;
            }

            if (back)
            {
                this.RecentOrdersCount++;
                if (this.RecentOrdersCount == 1)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 0)
                    {
                        var order = recentOrders.ElementAt(0);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder1 = order;
                    }

                    if (recentOrders.Count > 1)
                    {
                        var order = recentOrders.ElementAt(1);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder2 = order;
                    }

                    if (recentOrders.Count > 2)
                    {
                        var order = recentOrders.ElementAt(2);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder3 = order;
                    }

                    if (recentOrders.Count > 3)
                    {
                        var order = recentOrders.ElementAt(3);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder4 = order;
                    }

                    if (recentOrders.Count > 4)
                    {
                        var order = recentOrders.ElementAt(4);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder5 = order;
                    }
         
                    localOrders = recentOrders.Take(5).ToList();
                }
                else if (this.RecentOrdersCount == 2 && recentOrders.Count > 5)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 5)
                    {
                        var order = recentOrders.ElementAt(5);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder6 = order;
                    }

                    if (recentOrders.Count > 6)
                    {
                        var order = recentOrders.ElementAt(6);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder7 = order;
                    }

                    if (recentOrders.Count > 7)
                    {
                        var order = recentOrders.ElementAt(7);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder8 = order;
                    }

                    if (recentOrders.Count > 8)
                    {
                        var order = recentOrders.ElementAt(8);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder9 = order;
                    }

                    if (recentOrders.Count > 9)
                    {
                        var order = recentOrders.ElementAt(9);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder10 = order;
                    }
  
                    localOrders = recentOrders.Skip(5).Take(5).ToList();
                }
                else if (this.RecentOrdersCount == 3 && recentOrders.Count > 10)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 10)
                    {
                        var order = recentOrders.ElementAt(10);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder11 = order;
                    }

                    if (recentOrders.Count > 11)
                    {
                        var order = recentOrders.ElementAt(11);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder12 = order;
                    }

                    if (recentOrders.Count > 12)
                    {
                        var order = recentOrders.ElementAt(12);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder13 = order;
                    }

                    if (recentOrders.Count > 13)
                    {
                        var order = recentOrders.ElementAt(13);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder14 = order;
                    }

                    if (recentOrders.Count > 14)
                    {
                        var order = recentOrders.ElementAt(14);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder15 = order;
                    }
          
                    localOrders = recentOrders.Skip(10).Take(5).ToList();
                }
                else if (this.RecentOrdersCount == 4 && recentOrders.Count > 15)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 15)
                    {
                        var order = recentOrders.ElementAt(15);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder16 = order;
                    }

                    if (recentOrders.Count > 16)
                    {
                        var order = recentOrders.ElementAt(16);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder17 = order;
                    }

                    if (recentOrders.Count > 17)
                    {
                        var order = recentOrders.ElementAt(17);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder18 = order;
                    }

                    if (recentOrders.Count > 18)
                    {
                        var order = recentOrders.ElementAt(18);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder19 = order;
                    }

                    if (recentOrders.Count > 19)
                    {
                        var order = recentOrders.ElementAt(19);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder20 = order;
                    }
           
                    localOrders = recentOrders.Skip(15).Take(5).ToList();
                }
            }
            else
            {
                this.RecentOrdersCount--;
                if (this.RecentOrdersCount == 4)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 15)
                    {
                        var order = recentOrders.ElementAt(15);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder16 = order;
                    }

                    if (recentOrders.Count > 16)
                    {
                        var order = recentOrders.ElementAt(16);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder17 = order;
                    }

                    if (recentOrders.Count > 17)
                    {
                        var order = recentOrders.ElementAt(17);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder18 = order;
                    }

                    if (recentOrders.Count > 18)
                    {
                        var order = recentOrders.ElementAt(18);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder19 = order;
                    }

                    if (recentOrders.Count > 19)
                    {
                        var order = recentOrders.ElementAt(19);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder20 = order;
                    }

                    localOrders = recentOrders.Skip(15).Take(5).ToList();
                }
                else if (this.RecentOrdersCount == 3)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 10)
                    {
                        var order = recentOrders.ElementAt(10);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder11 = order;
                    }

                    if (recentOrders.Count > 11)
                    {
                        var order = recentOrders.ElementAt(11);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder12 = order;
                    }

                    if (recentOrders.Count > 12)
                    {
                        var order = recentOrders.ElementAt(12);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder13 = order;
                    }

                    if (recentOrders.Count > 13)
                    {
                        var order = recentOrders.ElementAt(13);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder14 = order;
                    }

                    if (recentOrders.Count > 14)
                    {
                        var order = recentOrders.ElementAt(14);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder15 = order;
                    }
                   
                    localOrders = recentOrders.Skip(10).Take(5).ToList();
                }
                else if (this.RecentOrdersCount == 2)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 5)
                    {
                        var order = recentOrders.ElementAt(5);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder6 = order;
                    }

                    if (recentOrders.Count > 6)
                    {
                        var order = recentOrders.ElementAt(6);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder7 = order;
                    }

                    if (recentOrders.Count > 7)
                    {
                        var order = recentOrders.ElementAt(7);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder8 = order;
                    }

                    if (recentOrders.Count > 8)
                    {
                        var order = recentOrders.ElementAt(8);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder9 = order;
                    }

                    if (recentOrders.Count > 9)
                    {
                        var order = recentOrders.ElementAt(9);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder10 = order;
                    }
                 
                    localOrders = recentOrders.Skip(5).Take(5).ToList();
                }
                else if (this.RecentOrdersCount == 1)
                {
                    this.ResetOrderDates();
                    if (recentOrders.Count > 0)
                    {
                        var order = recentOrders.ElementAt(0);
                        this.Order1Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder1 = order;
                    }

                    if (recentOrders.Count > 1)
                    {
                        var order = recentOrders.ElementAt(1);
                        this.Order2Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder2 = order;
                    }

                    if (recentOrders.Count > 2)
                    {
                        var order = recentOrders.ElementAt(2);
                        this.Order3Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder3 = order;
                    }

                    if (recentOrders.Count > 3)
                    {
                        var order = recentOrders.ElementAt(3);
                        this.Order4Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder4 = order;
                    }

                    if (recentOrders.Count > 4)
                    {
                        var order = recentOrders.ElementAt(4);
                        this.Order5Date = order.CreationDate.ToString();
                        ProductRecentOrderData.RecentOrder5 = order;
                    }
             
                    localOrders = recentOrders.Take(5).ToList();
                }
            }

            if (!localOrders.Any())
            {
                if (back)
                {
                    this.RecentOrdersCount--;
                }
                else
                {
                    this.RecentOrdersCount++;
                }

                return;
            }

            this.ProductRecentOrderDatas =
                new ObservableCollection<ProductRecentOrderData>(this.DataManager.GetRecentOrderData(localOrders, this.CustomerSales));

            if (this.ProductRecentOrderDatas.Any())
            {
                // set focus to the recent orders grid.
                Messenger.Default.Send(Token.Message, Token.SetRecentOrdersFocus);
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        protected void GetIssueMethods()
        {
            this.NouIssueTypes = this.DataManager.GetNouIssueMethods();
        }

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        protected void GetUOMs()
        {
            this.NouUOMs = this.DataManager.GetNouUOMs();
        }

        /// <summary>
        /// Sets the selected sale detail.
        /// </summary>
        /// <param name="detail">The detail to set.</param>
        protected void SetSelectedSaleDetail(SaleDetail detail)
        {
            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            detail.IsSelectedSaleDetail = true;
        }

        protected void SetOrderLinesSummary()
        {
            if (this.SaleDetails == null)
            {
                return;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                if (string.IsNullOrEmpty(saleDetail.Summary))
                {
                    var data = this.DataManager.GetProductStockAndOrderData(saleDetail.INMasterID);
                    if (data != null)
                    {
                        saleDetail.Summary = data.Item1;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the selected product price book with the changed price.
        /// </summary>
        protected void UpdatePriceToPriceBook()
        {
            this.Log.LogDebug(this.GetType(), "UpdatePriceToPriceBook() Updating Price Book");

            if (this.SelectedPartner.PriceListID.ToInt() == 0)
            {
                this.SelectedPartner.PriceListID =
                    this.DataManager.GetPartnerPriceListId(this.SelectedPartner.BPMasterID);
            }

            var update = this.DataManager.UpdatePrice(this.selectedSaleDetail.PriceListID,
                this.SelectedPartner.PriceListID.ToInt(),
                this.selectedSaleDetail.UnitPrice.ToDecimal(), this.SelectedSaleDetail.INMasterID);
            var success = update.Item1;
            var detail = update.Item2;

            if (success)
            {
                SystemMessage.Write(MessageType.Priority, Message.PriceBookUpdated);

                if (this.selectedSaleDetail.PriceListID != this.selectedPartner.PriceListID)
                {
                    this.selectedSaleDetail.LoadingSale = true;
                    this.selectedSaleDetail.PriceListID = this.selectedPartner.PriceListID.ToInt();
                }
                
                this.PriceLists = new ObservableCollection<PriceMaster>(NouvemGlobal.PriceListMasters);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.PriceBookNotUpdated);
            }
        }

        /// <summary>
        /// Removes a sale detail.
        /// </summary>
        protected bool RemoveItem(ViewType module)
        {
            try
            {
                if (this.SelectedSaleDetail != null)
                {
                    if (this.SelectedSaleDetail.SaleDetailID > 0)
                    {
                        // Trying to remove an order item with no transactions recorded against it. Check if user is authorised.
                        if (this.AuthorisationsManager.AllowUserToRemoveProductFromSaleOrder)
                        {
                            this.Log.LogWarning(this.GetType(), string.Format("Removing sale item:{0}. Module:{1}", this.SelectedSaleDetail.SaleDetailID, module));
                            this.SelectedSaleDetail.Deleted = DateTime.Now;
                            if (this.DataManager.RemoveSaleDetail(module, this.SelectedSaleDetail.SaleDetailID))
                            {
                                ProgressBar.Run();
                                this.Log.LogWarning(this.GetType(), "Item removed");
                                this.SaleDetails.Remove(this.SelectedSaleDetail);
                                this.UpdateSaleOrderTotals();
                                if (ApplicationSettings.TouchScreenMode)
                                {
                                    this.SelectedSaleDetail = null;
                                }

                                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                                return true;
                            }
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.NoAuthorisationToRemoveProduct);
                            return false;
                        }
                    }
                    else
                    {
                        this.SaleDetails.Remove(this.SelectedSaleDetail);
                        this.UpdateSaleOrderTotals();
                        ProgressBar.Run();
                        if (ApplicationSettings.TouchScreenMode)
                        {
                            this.SelectedSaleDetail = null;
                        }

                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Resets the selected document number.
        /// </summary>
        protected void ResetDocumentNumber()
        {
            if (this.DocNumberings == null)
            {
                return;
            }

            this.GetDocNumbering();
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering == null)
            {
                this.Log.LogError(this.GetType(), "ResetDocumentNumber: Doc number not found");
                return;
            }

            var retrievedDocNo =
                this.allDocNumberings.FirstOrDefault(
                    x => x.DocumentNumberingID == this.SelectedDocNumbering.DocumentNumberingID);

            if (retrievedDocNo != null)
            {
                this.SelectedDocNumbering.NextNumber = retrievedDocNo.NextNumber;
            }

            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Refreshes the selected doc status, to display the active status.
        /// </summary>
        protected void RefreshDocStatusItems()
        {
            if (this.DocStatusItems == null)
            {
                return;
            }

            this.SelectedDocStatus =
               this.DocStatusItems.FirstOrDefault(x => x.Value.CompareIgnoringCase(Constant.Active));
        }

        /// <summary>
        /// Resets the ui.
        /// </summary>
        /// <param name="clearSelectedCustomer">Flag, indicating whether the selected customer data is cleared.</param>
        protected virtual void ClearForm(bool clearSelectedCustomer = true)
        {
            this.Log.LogDebug(this.GetType(), "ClearForm()  Clearing form");
            this.IsBaseDocument = false;
            this.IsReadOnly = false;
            this.DisableDocStatusChange = false;
            if (clearSelectedCustomer)
            {
                this.SelectedPartner = null;
                this.SelectedContact = null;
                this.SelectedSalesEmployee = null;
                this.SelectedDeliveryContact = null;
                this.SelectedInvoiceAddress = null;
                this.SelectedDeliveryAddress = null;
                this.ProductRecentOrderDatas?.Clear();
                this.ResetOrderDates();
            }

            this.Sale = new Sale();
            this.SelectedHaulier = null;
            this.SelectedAgent = null;
            this.SaleDetails?.Clear();
            this.SaleAttachments?.Clear();
            this.PORequired = false;
            this.DocketNote = string.Empty;
            this.PopUpNote = string.Empty;
            this.InvoiceNote = string.Empty;
            this.Remarks = string.Empty;
            this.CreditLimit = null;
            this.AccountBalance = null;
            this.SubTotal = 0;
            this.DeliveryTime = string.Empty;
            this.SelectedDeliveryAddressFull = string.Empty;
            this.Total = 0;
            this.Discount = 0;
            this.Tax = 0;
            this.DiscountPercentage = 0;
            this.SelectedRoute = null;
            this.PartnerInvoiceNote = null;
            this.CustomerPopUpNote = null;
            this.SelectedDocumentCopyFrom = null;
            this.SelectedDocumentCopyTo = null;
            this.CustomerPOReference = string.Empty;
            this.LockDiscountPercentage = false;
            CopyingDocument = false;
            this.SetDefaultDates();
            this.ClearAttributes();
            this.SelectedSalesEmployee = NouvemGlobal.LoggedInUser;
            this.SelectedDocStatus = this.DocStatusItems?.FirstOrDefault();
        }

        /// <summary>
        /// Resets the recent order grid header dates.
        /// </summary>
        protected void ResetOrderDates()
        {
            this.Order1Date = string.Empty;
            this.Order2Date = string.Empty;
            this.Order3Date = string.Empty;
            this.Order4Date = string.Empty;
            this.Order5Date = string.Empty;
            this.Order6Date = string.Empty;
            this.Order7Date = string.Empty;
            this.Order8Date = string.Empty;
            this.Order9Date = string.Empty;
            this.Order10Date = string.Empty;
            this.Order11Date = string.Empty;
            this.Order12Date = string.Empty;
            this.Order13Date = string.Empty;
            this.Order14Date = string.Empty;
            this.Order15Date = string.Empty;
            this.Order16Date = string.Empty;
            this.Order17Date = string.Empty;
            this.Order18Date = string.Empty;
            this.Order19Date = string.Empty;
            this.Order20Date = string.Empty;

            this.Order21Date = string.Empty;
            this.Order22Date = string.Empty;
            this.Order23Date = string.Empty;
            this.Order24Date = string.Empty;
            this.Order25Date = string.Empty;
            this.Order26Date = string.Empty;
            this.Order27Date = string.Empty;
            this.Order28Date = string.Empty;
            this.Order29Date = string.Empty;
            this.Order30Date = string.Empty;
            this.Order31Date = string.Empty;
            this.Order32Date = string.Empty;
            this.Order33Date = string.Empty;
            this.Order34Date = string.Empty;
            this.Order35Date = string.Empty;
            this.Order36Date = string.Empty;
            this.Order37Date = string.Empty;
            this.Order38Date = string.Empty;
            this.Order39Date = string.Empty;
            this.Order40Date = string.Empty;
            this.Order41Date = string.Empty;
            this.Order42Date = string.Empty;
            this.Order43Date = string.Empty;
            this.Order44Date = string.Empty;
            this.Order45Date = string.Empty;
            this.Order46Date = string.Empty;
            this.Order47Date = string.Empty;
            this.Order48Date = string.Empty;
            this.Order49Date = string.Empty;
            this.Order50Date = string.Empty;
            this.Order51Date = string.Empty;
            this.Order52Date = string.Empty;
            this.Order53Date = string.Empty;
            this.Order54Date = string.Empty;
            this.Order55Date = string.Empty;
            this.Order56Date = string.Empty;
            this.Order57Date = string.Empty;
            this.Order58Date = string.Empty;
            this.Order59Date = string.Empty;
            this.Order60Date = string.Empty;
            this.Order61Date = string.Empty;
            this.Order62Date = string.Empty;
            this.Order63Date = string.Empty;
            this.Order64Date = string.Empty;
            this.Order65Date = string.Empty;
            this.Order66Date = string.Empty;
            this.Order67Date = string.Empty;
            this.Order68Date = string.Empty;
            this.Order69Date = string.Empty;
            this.Order70Date = string.Empty;
            this.Order71Date = string.Empty;
            this.Order72Date = string.Empty;
            this.Order73Date = string.Empty;
            this.Order74Date = string.Empty;
            this.Order75Date = string.Empty;
            this.Order76Date = string.Empty;
            this.Order77Date = string.Empty;
            this.Order78Date = string.Empty;
            this.Order79Date = string.Empty;
            this.Order80Date = string.Empty;
            this.Order81Date = string.Empty;
            this.Order82Date = string.Empty;
            this.Order83Date = string.Empty;
            this.Order84Date = string.Empty;
            this.Order85Date = string.Empty;
            this.Order86Date = string.Empty;
            this.Order87Date = string.Empty;
            this.Order88Date = string.Empty;
            this.Order89Date = string.Empty;
            this.Order90Date = string.Empty;
            this.Order91Date = string.Empty;
            this.Order92Date = string.Empty;
            this.Order93Date = string.Empty;
            this.Order94Date = string.Empty;
            this.Order95Date = string.Empty;
            this.Order96Date = string.Empty;
            this.Order97Date = string.Empty;
            this.Order98Date = string.Empty;
            this.Order99Date = string.Empty;
            this.Order100Date = string.Empty;

        }

        /// <summary>
        /// Gets the document numbering data.
        /// </summary>
        protected virtual void GetDocNumbering()
        {
            this.allDocNumberings = new List<DocNumber>(this.DataManager.GetDocumentNumbers());
        }

        /// <summary>
        /// Determines if a document is an active document or not.
        /// </summary>
        /// <returns>A flag, indicating whether the current document is an active document.</returns>
        protected bool IsActiveDocument()
        {
            // If the current document is a non active document, then we only allow read only access.
            if (this.sale != null && this.sale.SaleID > 0 && !ApplicationSettings.TouchScreenMode &&
                (this.sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID 
                || this.sale.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID))
            {
                this.SetControlMode(ControlMode.OK);
                SystemMessage.Write(MessageType.Issue, Message.NonActiveDocument);
                this.IsReadOnly = true;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Updates the doc numbering when a sale/order is created.
        /// </summary>
        protected void UpdateDocNumbering()
        {
            var documentNumber = this.DataManager.SetDocumentNumber(this.selectedDocNumbering.DocumentNumberingID);
            this.Sale.Number = documentNumber.CurrentNumber;
            this.SelectedDocNumbering = documentNumber;
            this.Sale.DocumentNumberingID = this.SelectedDocNumbering.DocumentNumberingID;
        }

        /// <summary>
        /// Gets the price lists.
        /// </summary>
        protected void GetPriceLists()
        {
            this.PriceLists = new ObservableCollection<PriceMaster>(NouvemGlobal.PriceListMasters);
        }

        /// <summary>
        /// Sets the active sale vm (deactivating the others).
        /// </summary>
        /// <param name="salevm">The vm to set as active.</param>
        protected void SetActiveDocument(SalesViewModelBase salevm)
        {
            if (!ViewModelLocator.IsOrderNull())
            {
                this.Locator.Order.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsOrder2Null())
            {
                this.Locator.Order2.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsQuoteNull())
            {
                this.Locator.Quote.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsARDispatchNull())
            {
                this.Locator.ARDispatch.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsARDispatch2Null())
            {
                this.Locator.ARDispatch2.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsInvoiceNull())
            {
                this.Locator.Invoice.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsAPInvoiceNull())
            {
                this.Locator.APInvoice.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsARDispatchTouchscreenNull())
            {
                this.Locator.ARDispatchTouchscreen.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsAPOrderNull())
            {
                this.Locator.APOrder.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsAPQuoteNull())
            {
                this.Locator.APQuote.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsAPReceiptNull())
            {
                this.Locator.APReceipt.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsAPReceiptTouchscreenNull())
            {
                this.Locator.APReceiptTouchscreen.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsLairageNull())
            {
                this.Locator.Lairage.IsFocusedSaleModule = false;
            }

            if (!ViewModelLocator.IsARReturnNull())
            {
                this.Locator.ARReturn.IsFocusedSaleModule = false;
            }

            salevm.IsFocusedSaleModule = true;
        }

        /// <summary>
        /// Refreshes the price lists.
        /// </summary>
        protected void RefreshPriceLists()
        {
            NouvemGlobal.GetPriceLists();
            this.GetPriceLists();
        }

        /// <summary>
        /// Sets the order lines status.
        /// </summary>
        protected void SetOrderLinesStatus()
        {
            if (this.SaleDetails == null)
            {
                return;
            }

            var localDetails = new List<SaleDetail>(this.SaleDetails);
            foreach (var localDetail in localDetails)
            {
                this.SetLineStatus(localDetail);
            }
        }

        /// <summary>
        /// Sets the line status.
        /// </summary>
        protected void SetLineStatus(SaleDetail detail)
        {
            if (detail == null || !this.SaleDetails.Any())
            {
                return;
            }

            detail.CheckLineStatus();
            if (detail.IsFilled)
            {
                var index = 0;
                for (int i = 0; i < this.SaleDetails.Count; i++)
                {
                    if (this.SaleDetails.ElementAt(i).SaleDetailID == detail.SaleDetailID)
                    {
                        index = i;
                        break;
                    }
                }

                // move filled line to the bottom, greyed out.
                this.SaleDetails.Move(index, this.SaleDetails.Count - 1);
            }
        }

        #endregion

        #region abstract

        /// <summary>
        /// Copy to the selected document type.
        /// </summary>
        protected abstract void CopyDocumentTo();

        /// <summary>
        /// Copy from the selected document type.
        /// </summary>
        protected abstract void CopyDocumentFrom();

        /// <summary>
        /// Locate a sale.
        /// </summary>
        protected abstract void FindSale();

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected abstract void FindSaleCommandExecute();

        /// <summary>
        /// Handle the main F1 search by directing to the correct handler.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected abstract void DirectSearchCommandExecute();

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected abstract void AddSale();

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected abstract void UpdateSale();

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected abstract void Close();

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Adds the selected sale item to the current order.
        /// </summary>
        private void AddToOrderCommandExecute()
        {
            this.SaleDetails.Add(this.selectedCustomerSaleDetail);
            this.UpdateSaleOrderTotals();
            SystemMessage.Write(MessageType.Priority, Message.OrderUpdateComplete);
        }

        /// <summary>
        /// Drills down to the selected user.
        /// </summary>
        private void DrillDownToUserCommandExecute()
        {
            if (this.SelectedSalesEmployee == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.UserSetUp, this.SelectedSalesEmployee.UserMaster.UserMasterID), Token.DrillDown);
        }

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        private void DrillDownToItemCommandExecute()
        {
            if (this.SelectedCustomerSaleDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedCustomerSaleDetail.INMasterID), Token.DrillDown);
        }

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        protected virtual void DrillDownToMasterItemCommandExecute()
        {
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
        }

         /// <summary>
        /// Drills down to the selected price list.
        /// </summary>
        private void DrillDownToPriceListCommandExecute()
        {
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            this.priceListUpdateDetails = new Tuple<int, int>(this.SelectedSaleDetail.PriceListID, this.SelectedSaleDetail.INMasterID);

             SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
             if (this.SelectedSaleDetail.PriceListID == NouvemGlobal.SpecialPriceList.PriceListID)
             {
                 Messenger.Default.Send(Tuple.Create(ViewType.SpecialPrices, this.SelectedSaleDetail.INMasterID, this.SelectedPartner.BPMasterID, this.SelectedPartner.BPGroupID.ToInt()), Token.DrillDown);
             }
             else
             {
                 Messenger.Default.Send(Tuple.Create(ViewType.PriceListDetail, this.SelectedSaleDetail.PriceListID, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
             }
        }

        /// <summary>
        /// Drills down to the selected price list.
        /// </summary>
        protected void DrillDownToCostPriceListCommandExecute()
        {
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            this.priceListUpdateDetails = new Tuple<int, int>(NouvemGlobal.CostPriceList.PriceListID, this.SelectedSaleDetail.INMasterID);

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.PriceListDetail, NouvemGlobal.CostPriceList.PriceListID, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
        }

        /// <summary>
        /// Handler for the command to attach a new file to the db.
        /// </summary>
        private void AttachFileCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                var localFile = File.ReadAllBytes(openFileDialog.FileName);

                var localName = openFileDialog.SafeFileName;

                var localAttachment = new Attachment { AttachmentDate = DateTime.Now, FileName = localName, File = localFile };

                this.Sale.Attachments.Add(localAttachment);
                this.SaleAttachments.Add(localAttachment);

                if (!this.CheckForEntity())
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Handler for the command to display a new file to the db.
        /// </summary>
        private void DisplayFileCommandExecute()
        {
            #region validation

            if (this.SelectedAttachment == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAttachmentSelected);
                return;
            }

            #endregion

            try
            {
                var file = this.SelectedAttachment.File;
                var fileName = this.SelectedAttachment.FileName;

                var path = Path.Combine(Settings.Default.AtachmentsPath, fileName);
                File.WriteAllBytes(path, file);
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handler for the command to delete a file from the db.
        /// </summary>
        private void DeleteFileCommandExecute()
        {
            if (this.SelectedAttachment != null)
            {
                if (this.Sale.SaleID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }

                // mark it for removal.
                this.SelectedAttachment.Deleted = DateTime.Now;
                this.SaleAttachments.Remove(this.SelectedAttachment);
            }
        }

        /// <summary>
        /// Displays the BP search grid, containing the customer partners.
        /// </summary>
        /// <param name="type">The sale type.</param>
        protected void ShowSearchGridCommandExecute(string type)
        {
            if (type == Constant.Quote || type == Constant.Order || type == Constant.Dispatch || type == Constant.Invoice)
            {
                this.Locator.BPSearchData.SetFilteredPartners(NouvemGlobal.CustomerPartners);
            }
            else
            {
                this.Locator.BPSearchData.SetFilteredPartners(NouvemGlobal.SupplierPartners);
            }

            // Multiple matches, so show them in the search grid.
            Messenger.Default.Send(ViewType.SalesCustomerSearch, Token.SearchForBusinessPartner);
            this.Locator.BPSearchData.CurrentSearchType = type == Constant.Quote ? ViewType.Quote :
                 type == Constant.Order ? ViewType.Order:
                 type == Constant.Dispatch ? ViewType.ARDispatch :
                 type == Constant.APQuote ? ViewType.APQuote:
                 type == Constant.APOrder ? ViewType.APOrder:
                 ViewType.APReceipt;
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the live price data when using recent orders.
        /// </summary>
        /// <param name="saleDetail">The recent order data.</param>
        protected virtual void GetLiveRecentOrderPrice(SaleDetail saleDetail)
        {
            if (this.SelectedPartner == null || saleDetail == null)
            {
                return;
            }

            var data = this.DataManager.GetPriceListDetailByPartner(this.SelectedPartner.BPMasterID, saleDetail.INMasterID, NouvemGlobal.BasePriceList.PriceListID);
            if (data != null)
            {
                saleDetail.LoadingSale = true;
                saleDetail.UnitPrice = data.Price;
                saleDetail.PriceListID = data.PriceListID;
            }
        }

        protected virtual void CalculateMargin()
        {
            try
            {
                if (this.Total > 0)
                {
                    if (this.OrderCost == 0 && this.SaleDetails != null && this.SaleDetails.Any())
                    {
                        this.SaleDetails.ToList().ForEach(item => this.OrderCost += item.CostTotal.ToDecimal());
                    }

                    this.OrderMargin = Math.Round(((this.Total - this.OrderCost) / this.Total * 100).ToDecimal(), 2);
                }
                else
                {
                    this.OrderMargin = 0;
                }
            }
            catch 
            {
            }
        }

        /// <summary>
        /// Gets the nou doc status ids.
        /// </summary>
        private void GetDocStatusIds()
        {
            this.DocStatusesAll = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusActiveEdit.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID,
            };

            this.DocStatusesOpen = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusActiveEdit.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
            };

            this.DocStatusesClosed = new List<int?> { NouvemGlobal.NouDocStatusComplete.NouDocStatusID };
            this.DocStatusesCancelled = new List<int?> { NouvemGlobal.NouDocStatusCancelled.NouDocStatusID };
            this.DocStatusesInProgress = new List<int?> { NouvemGlobal.NouDocStatusInProgress.NouDocStatusID };
            this.DocStatusesComplete = new List<int?> { NouvemGlobal.NouDocStatusFilled.NouDocStatusID };
            this.DocStatusesExported = new List<int?> { NouvemGlobal.NouDocStatusExported.NouDocStatusID };
        }

        /// <summary>
        /// Gets the application routes.
        /// </summary>
        private void GetRoutes()
        {
            this.Routes = new ObservableCollection<Route>(this.DataManager.GetRoutes());
        }

        /// <summary>
        /// Sets the default form dates.
        /// </summary>
        protected virtual void SetDefaultDates()
        {
            this.DocumentDate = DateTime.Today;
            this.DeliveryDate = DateTime.Today.Add(TimeSpan.FromDays(ApplicationSettings.SaleOrderDeliveryDaysForward));
            if (ApplicationSettings.ProposedKillDateDaysForward < 0)
            {
                this.ProposedKillDate = null;
            }
            else
            {
                this.ProposedKillDate = DateTime.Today.Add(TimeSpan.FromDays(ApplicationSettings.ProposedKillDateDaysForward));
            }

            if (ApplicationSettings.DefaultShippingDateToPreviousDay)
            {
                this.ShippingDate = DateTime.Today.Add(TimeSpan.FromDays(-1));
            }
            else
            {
                this.ShippingDate = DateTime.Today.Add(TimeSpan.FromDays(ApplicationSettings.SaleOrderDeliveryDaysForward));
            }

            this.ValidUntilDate = DateTime.Today.Add(TimeSpan.FromDays(ApplicationSettings.SaleOrderValidUntilDaysForward));
        }

        /// <summary>
        /// Gets all the business partners.
        /// </summary>
        private void GetAllPartners()
        {
            if (NouvemGlobal.BusinessPartners == null)
            {
                NouvemGlobal.RefreshPartners();
            }

            if (NouvemGlobal.BusinessPartners == null)
            {
                return;
            }

            this.DataManager.GetInactivePartners();
            this.allPartners = new List<ViewBusinessPartner>(NouvemGlobal.BusinessPartners
                .Select(x => x.Details));

            this.GetHauliers();
            this.GetAgents();
        }

        /// <summary>
        /// Adds an iactive customer if retrieved on historic docket.
        /// </summary>
        /// <param name="partnerId"></param>
        protected virtual void AddInactiveSelectedPartner(int partnerId)
        {
            if (!this.Customers.Any(x => x.BPMasterID == partnerId))
            {
                var localPartner = NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == partnerId);
                if (localPartner != null)
                {
                    this.Customers.Add(localPartner.Details);
                }
            }
        }

        /// <summary>
        /// Gets the suppliers.
        /// </summary>
        protected virtual void GetSuppliers()
        {
            this.Suppliers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.SupplierPartners
                 .Where(x => ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                       || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)) &&
                                         !x.Details.Type.StartsWithIgnoringCase(Strings.Haulier))
                .OrderBy(x => x.Details.Name)
                .Select(x => x.Details));
        }

        /// <summary>
        /// Gets the application supplier and customer partners.
        /// </summary>
        protected override void GetCustomers()
        {
            this.Customers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.CustomerPartners
                .Where(x => ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                         || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)) &&
                                         !x.Details.Type.StartsWithIgnoringCase(Strings.Haulier))
                .OrderBy(x => x.Details.Name)
                .Select(x => x.Details));
        }

        /// <summary>
        /// Gets the inventory items.
        /// </summary>
        protected virtual void GetInventoryItems()
        {
            this.SaleItems = new ObservableCollection<InventoryItem>(NouvemGlobal.SaleItems
                .Where(x => x.Master.Deleted == null && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                          || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                .OrderBy(x => x.Master.Name));

            this.PurchaseItems = new ObservableCollection<InventoryItem>(NouvemGlobal.PurchaseItems
                .Where(x => x.Master.Deleted == null && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                          || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                .OrderBy(x => x.Master.Name));
        }

        /// <summary>
        /// Gets the sales employees.
        /// </summary>
        private void GetSalesEmployees()
        {
            this.SalesEmployees = new ObservableCollection<User>(NouvemGlobal.Users);
        }

        /// <summary>
        /// Adds a selected recent sale order item that has had it's qty/wgt manually entered.
        /// </summary>
        /// <param name="saleDetail">The sale item detail.</param>
        private void AddManualRecentOrderItem(SaleDetail saleDetail)
        {
            if (ApplicationSettings.RetrieveLivePriceOnRecentOrders)
            {
                this.GetLiveRecentOrderPrice(saleDetail);
            }

            if (saleDetail.PriceListID == 0)
            {
                this.Log.LogDebug(this.GetType(), string.Format("AddManualRecentOrderItem(): Price list 0. Sale detail Id:{0}", saleDetail.SaleDetailID));
                saleDetail.LoadingSale = true;
                if (this.SelectedPartner != null && !this.SelectedPartner.PriceListID.IsNullOrZero())
                {
                    saleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                    this.Log.LogDebug(this.GetType(), string.Format("AddManualRecentOrderItem(): Price list 0. Changing to customer price list:{0}", this.SelectedPartner.PriceListID));
                }
                else
                {
                    saleDetail.PriceListID = NouvemGlobal.BasePriceList.PriceListID;
                    this.Log.LogDebug(this.GetType(), string.Format("AddManualRecentOrderItem(): Price list 0. Changing to base price list:{0}", NouvemGlobal.BasePriceList.PriceListID));
                }

                saleDetail.LoadingSale = false;
            }

            var localProduct = this.DataManager.GetInventoryItemById(saleDetail.INMasterID);
            if (localProduct != null)
            {
                if (localProduct.Master.Deleted != null || (localProduct.Master.InActiveFrom <= DateTime.Today &&
                                                            localProduct.Master.InActiveTo > DateTime.Today))
                {
                    var msg = string.Format(Message.InactiveProductWarning, localProduct.Name);
                    SystemMessage.Write(MessageType.Issue, msg);
                    NouvemMessageBox.Show(msg);
                    return;
                }
            }

            var localDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == saleDetail.INMasterID);
            if (localDetail == null)
            {
                this.SaleDetails.Add(saleDetail);
            }
            else
            {
                localDetail.QuantityOrdered = saleDetail.QuantityOrdered.ToDecimal();
                localDetail.WeightOrdered = saleDetail.WeightOrdered.ToDecimal();
                if (localDetail.PriceListID == 0)
                {
                    localDetail.LoadingSale = true;
                    localDetail.PriceListID = saleDetail.PriceListID;
                    localDetail.LoadingSale = false;
                    this.Log.LogDebug(this.GetType(), string.Format("AddManualRecentOrderItem(): Editing existing line price list. Changing to customer price list:{0}", saleDetail.PriceListID));
                }
            }

            var amount = string.Empty;
            var quantity = saleDetail.QuantityOrdered;
            var weight = saleDetail.WeightOrdered;

            if (quantity > 0)
            {
                amount = quantity.ToString();
            }

            if (weight > 0)
            {
                amount = string.Format("{0}kg", weight);
            }

            SystemMessage.Write(MessageType.Priority, string.Format(Message.ItemAddedToSaleOrder, amount, saleDetail.InventoryItem.Master.Name));
            this.UpdateSaleOrderTotals();

            if (this.sale.SaleID > 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }
        }

        /// <summary>
        /// Updates the businbess partner collections.
        /// </summary>
        private void UpdatePartners()
        {
            this.GetCustomers();
            this.GetSuppliers();
            this.GetAllPartners();
            this.ParsePartner();
        }

        /// <summary>
        /// Adds a selected recent order item, or entire recent order, to the current order.
        /// </summary>
        /// <param name="itemValue">The item value (either a selected item, or an or order header date).</param>
        private void PreviousOrderItemSelectionHandler(string itemValue)
        {
            try
            {
                var orderDate = itemValue.ToDateNullable();
                if (orderDate.HasValue)
                {
                    // itemValue is a column header selection
                    var order = ProductRecentOrderData.GetRecentOrder(orderDate);

                    if (order != null)
                    {
                        // Add all the sale items from this order to the current order.
                        foreach (var saleItem in order.SaleDetails)
                        {
                            var localItem = this.DataManager.GetInventoryItemById(saleItem.INMasterID);
                            if (localItem != null)
                            {
                                if (localItem.Master.Deleted != null || (localItem.Master.InActiveFrom <= DateTime.Today &&
                                                                            localItem.Master.InActiveTo > DateTime.Today))
                                {
                                    var msg = string.Format(Message.InactiveProductWarning, localItem.Name);
                                    SystemMessage.Write(MessageType.Issue, msg);
                                    NouvemMessageBox.Show(msg);
                                    continue;
                                }
                            }

                            var localDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == saleItem.INMasterID);
                            if (localDetail == null)
                            {
                                this.SaleDetails.Add(new SaleDetail
                                {
                                    INMasterID = saleItem.INMasterID,
                                    QuantityOrdered = saleItem.QuantityOrdered,
                                    WeightOrdered = saleItem.WeightOrdered
                                });
                            }
                            else
                            {
                                localDetail.QuantityOrdered = saleItem.QuantityOrdered.ToDecimal();
                                localDetail.WeightOrdered = saleItem.WeightOrdered.ToDecimal();
                            }
                        }

                        SystemMessage.Write(MessageType.Priority, string.Format(Message.AllSaleOrderItemsAddedToCurrentOrder, itemValue));
                        this.UpdateSaleOrderTotals();

                        if (this.sale.SaleID > 0)
                        {
                            this.SetControlMode(ControlMode.Update);
                        }
                        else
                        {
                            this.SetControlMode(ControlMode.Add);
                        }
                    }

                    return;
                }

                // itemValue is a cell value
                if (this.SelectedProductRecentOrderData == null)
                {
                    return;
                }

                var localProduct = this.DataManager.GetInventoryItemById(this.SelectedProductRecentOrderData.InventoryItem.Master.INMasterID);
                if (localProduct != null)
                {
                    if (localProduct.Master.Deleted != null || (localProduct.Master.InActiveFrom <= DateTime.Today &&
                                                                localProduct.Master.InActiveTo > DateTime.Today))
                    {
                        var msg = string.Format(Message.InactiveProductWarning, localProduct.Name);
                        SystemMessage.Write(MessageType.Issue, msg);
                        NouvemMessageBox.Show(msg);
                        return;
                    }
                }

                var backSlashPos = itemValue.IndexOf('/');
                var space = itemValue.IndexOf(' ');

                var quantity = itemValue.Substring(0, backSlashPos).ToDecimal();
                var weight = itemValue.Substring(backSlashPos + 1, space - (backSlashPos + 1)).ToDecimal();

                var localSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID
                    == this.SelectedProductRecentOrderData.InventoryItem.Master.INMasterID);
                if (localSaleDetail == null)
                {
                    this.SaleDetails.Add(new SaleDetail
                    {
                        INMasterID = this.SelectedProductRecentOrderData.InventoryItem.Master.INMasterID,
                        QuantityOrdered = quantity,
                        WeightOrdered = weight
                    });
                }
                else
                {
                    localSaleDetail.QuantityOrdered = quantity;
                    localSaleDetail.WeightOrdered = weight;
                }

                var amount = string.Empty;
                if (quantity > 0)
                {
                    amount = quantity.ToString();
                }

                if (weight > 0)
                {
                    amount = string.Format("{0}kg", weight);
                }

                SystemMessage.Write(MessageType.Priority, string.Format(Message.ItemAddedToSaleOrder, amount, this.SelectedProductRecentOrderData.InventoryItem.Master.Name));
                this.UpdateSaleOrderTotals();

                if (this.sale.SaleID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }
                else
                {
                    this.SetControlMode(ControlMode.Add);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"PreviousOrderItemSelectionHandler: Value:{itemValue}, Error:{e.Message}");
            }
        }

        /// <summary>
        /// Handler for a manual recent order item amount error.
        /// </summary>
        /// <param name="error">The error message.</param>
        private void HandleRecentOrderError(string error)
        {
            this.Log.LogError(this.GetType(), string.Format("HandleRecentOrderError():{0}", error));
            SystemMessage.Write(MessageType.Issue, Message.RecentOrderError);
        }

        #endregion

        #endregion
    }
}

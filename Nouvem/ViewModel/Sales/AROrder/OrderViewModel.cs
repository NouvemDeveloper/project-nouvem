﻿// -----------------------------------------------------------------------
// <copyright file="OrderViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Sales.AROrder
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class OrderViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// Flag, as to whether the order is to be auto copied to a dispatch docket.
        /// </summary>
        private bool autoCopyToDispatch;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderViewModel"/> class.
        /// </summary>
        public OrderViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            Messenger.Default.Register<string>(this, Token.ClosingSaleOrderWindow, s =>
             {
                 this.Close();
             });

            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.Order)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail {INMasterID = product.Master.INMasterID};
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.Order)
                    {
                        this.SelectedPartner =
                            this.Customers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            #endregion

            #region command handler

            this.DisplayDispatchesCommand = new RelayCommand(this.DisplayDispatchesCommandExecute);

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e =>
            {
                this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.Order));
            }); // ,e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID")

            // Handler to display the search screen
            this.DirectSearchCommandF2 = new RelayCommand(() =>
            {
                this.DirectSearchCommandF2Execute();
            }); // ,e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID")

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() =>
            {
                if (this.Sale == null || (this.Sale.NouDocStatusID != null && this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusActive.NouDocStatusID))
                {
                    return;
                }

                var localDetail = this.SelectedSaleDetail;
                if (this.SelectedSaleDetail != null && this.RemoveItem(ViewType.Order))
                {
                    this.Sale.SaleDetails = this.SaleDetails;
                    var localSale = this.Sale;
                    if (this.Sale.SaleID == 0)
                    {
                        this.SetControlMode(ControlMode.Add);
                    }
                    else
                    {
                        localDetail.Deleted = DateTime.Now;
                        this.DeletedSaleDetails.Add(localDetail);
                        this.SetControlMode(ControlMode.Update);
                    }

                    //if (localSale.SaleID > 0)
                    //{
                    //    this.Log.LogWarning(this.GetType(), string.Format("Sale order item removed. Updating dispatch for sale order id:{0}, Number:{1}", localSale.SaleID, localSale.Number));
                    //    var dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);
                    //    if (dispatchId > 0)
                    //    {
                    //        this.Log.LogDebug(this.GetType(), string.Format("Updating dispatch docket:{0}", dispatchId));
                    //        this.Sale.ParentID = dispatchId;
                    //        this.DataManager.UpdateARDispatchFromSaleOrder(this.Sale);
                    //    }
                    //}
                }
            });

            #endregion

            NouvemGlobal.RetrieveProductStockSummary = true;
            this.GetLocalDocNumberings();
            this.AutoCopyToDispatch = ApplicationSettings.AutoCopyToDispatch;
            this.CanOverrideOrderLimit = this.AuthorisationsManager.AllowUserToOverrideOrderLimit;
            this.CanOverrideCreditLimit = this.AuthorisationsManager.AllowUserToCreateSaleOrderExceedingCreditLimit;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether an order is to be auto copied to a dispatch docket.
        /// </summary>
        public bool AutoCopyToDispatch
        {
            get
            {
                return this.autoCopyToDispatch;
            }

            set
            {
                this.autoCopyToDispatch = ApplicationSettings.AlwaysCopyToDispatch ? true : value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand DisplayDispatchesCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Show the sales.
        /// </summary>
        public void ShowAllSales()
        {
            this.FindSaleCommandExecute();
        }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(64, this.Sale.SaleID.ToString(), !preview);
        }

        /// <summary>
        /// Gets the inventory items.
        /// </summary>
        protected override void GetInventoryItems()
        {
            this.DataManager.GetItemsStock(NouvemGlobal.SaleItems);
            this.SaleItems = new ObservableCollection<InventoryItem>(NouvemGlobal.SaleItems
                .Where(x => x.Master.Deleted == null && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                          || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                .OrderBy(x => x.Master.Name));
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.Sale = this.DataManager.GetOrderByID(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (!ViewModelLocator.IsOrder2Null())
            {
                if (this.Locator.Order2.LastEdit > this.LastEdit)
                {
                    return;
                }
            }

            this.Sale = this.DataManager.GetOrderByLastEdit();
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetOrderByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetOrderByID(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetOrderByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetARDispatchById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetOrderByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetOrderByFirstLast(false);
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                if (this.Sale.BaseDocumentReferenceID.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                    return;
                }

                this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected void DirectSearchCommandF2Execute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"))
            {
                this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.Order));
            }
          
            //if (this.CurrentMode == ControlMode.Add)
            //{
            //    this.ShowSearchGridCommandExecute(Constant.Order);
            //}
            //else if (this.CurrentMode == ControlMode.Find)
            //{
            //    this.FindSaleCommandExecute();
            //}
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            //if (this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"))
            //{
            //    this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.Order));
            //}

            //return;

            if (this.CurrentMode == ControlMode.Add)
            {
                this.ShowSearchGridCommandExecute(Constant.Order);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateSaleOrder);
                //this.CustomerSales = localSales
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date 
                //        && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateSaleOrder);
                //this.CustomerSales = this.DataManager.GetAllOrders(orderStatuses)
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.Order), Token.SearchForSale);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateSaleOrder);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.Order);
            }
        }

        /// <summary>
        /// Override, to set the selected detail flag.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            base.HandleSelectedSaleDetail();

            //if (this.SelectedSaleDetail != null)
            //{
            //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //    {
            //        if (this.SelectedSaleDetail != null)
            //        {
            //            if (string.IsNullOrEmpty(this.SelectedSaleDetail.Summary))
            //            {
            //                this.SelectedSaleDetail.Summary =
            //                    this.DataManager.GetProductStockAndOrderData(this.SelectedSaleDetail.INMasterID);
            //            }
            //        }
            //    }));
            //}

            if (ApplicationSettings.ShowProductRemarksAtSaleOrder)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (this.SelectedSaleDetail?.InventoryItem != null &&
                        !string.IsNullOrWhiteSpace(this.SelectedSaleDetail.InventoryItem.Remarks) 
                        && !this.SelectedSaleDetail.RemarksDisplayed
                        && (this.SelectedDocStatus.NouDocStatusID == 1 || this.SelectedDocStatus.NouDocStatusID == 4))
                    {
                        this.SelectedSaleDetail.RemarksDisplayed = true;
                        var localRemarks = this.SelectedSaleDetail.InventoryItem.Remarks;
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(localRemarks, isPopUp: true);
                        }));
                    }
                }));
            }
        }

        /// <summary>
        /// Override, to set the focus to the order vm.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if ((e.Cell.Property.Equals("INMasterID") || e.Cell.Property.Equals("PriceListID")) &&
               this.SelectedSaleDetail != null
               && (this.SelectedSaleDetail.WeightDelivered == null || this.SelectedSaleDetail.WeightDelivered == 0)
               && (this.SelectedSaleDetail.QuantityDelivered == null || this.SelectedSaleDetail.QuantityDelivered == 0))
            {
                this.SelectedSaleDetail.UnitPrice = null;
            }
        }

        /// <summary>
        /// Overide the selected sale, to reinstate the sale id.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            if (ApplicationSettings.AlwaysCopyToDispatch)
            {
                this.AutoCopyToDispatch = true;
            }

            this.Log.LogDebug(this.GetType(), string.Format("HandleSelectedSale(): Sale Id:{0}", this.Sale.SaleID));
            CopyingDocument = false;
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;
            this.Log.LogDebug(this.GetType(), string.Format("HandleSelectedSale(): After base: Sale Id:{0}", this.Sale.SaleID));

            if (this.Sale != null && this.Sale.SaleID > 0)
            {
                if (this.DataManager.DoesMasterDocumentHaveTransactions(this.Sale.SaleID))
                {
                    SystemMessage.Write(MessageType.Issue, Message.OrderPartOfInprogressDispatch);
                    this.IsReadOnly = true;
                    this.DisableDocStatusChange = true;
                }
            }
            
            //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //{
            //    this.SetOrderLinesSummary();
            //}));
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            base.ParsePartner();

            // now get the associated customer quotes.
            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.ShowPartnerPopUpNoteAtDesktopModules)
            {
                if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                        //this.CustomerPopUpNote = string.Empty;
                    }));
                }
            }

            if (this.DataManager.GetPartnerOnHoldStatus(this.SelectedPartner.BPMasterID))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.PartnerOnHold, this.SelectedPartner.Name));
                    NouvemMessageBox.Show(string.Format(Message.PartnerOnHold, this.SelectedPartner.Name), touchScreen: ApplicationSettings.TouchScreenMode);
                    var localBusinessPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                    if (localBusinessPartner != null)
                    {
                        localBusinessPartner.Details.OnHold = true;
                    }

                    this.SelectedPartner = null;
                }));

                return;
            }
            else
            {
                var localBusinessPartner =
                   NouvemGlobal.BusinessPartners.FirstOrDefault(
                       x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                if (localBusinessPartner != null)
                {
                    localBusinessPartner.Details.OnHold = false;
                }
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetRecentOrders(localPartner.BPMasterID);
                //var recentOrders = this.CustomerSales.OrderByDescending(x => x.CreationDate).ToList();
                this.RecentOrdersCount = 0;
                this.SetRecentOrders(true);
            }));
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            this.OrderUpdateInProgress = true;

            try
            {
                #region validation

                if (!this.AuthorisationsManager.AllowUserToAddOrUpdateSaleOrders)
                {
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    this.SetControlMode(ControlMode.OK);
                    return;
                }

                var error = string.Empty;

                if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
                {
                    error = Message.NoCustomerSelected;
                }
                else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
                {
                    error = Message.NoDocNumberingSelected;
                }
                else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
                {
                    error = Message.NoDocStatusSelected;
                }
                else if (ApplicationSettings.RouteMustBeSetOnOrder && this.SelectedRoute == null)
                {
                    error = Message.SelectRoute;
                }
                else if (ApplicationSettings.CheckRoutesAtOrder && this.SelectedRoute != null)
                {
                    error = this.CheckRoutesAtOrder();
                }
                else if (ApplicationSettings.DeliveryDateCannotBeInThePast && this.DeliveryDate.HasValue && this.DeliveryDate < DateTime.Today)
                {
                    error = Message.DeliveryDateInPastError;
                }
                else if (ApplicationSettings.ShippingDateMustBeAfterDeliveryDate && this.DeliveryDate.HasValue && this.ShippingDate.HasValue)
                {
                    if (this.DeliveryDate < this.ShippingDate)
                    {
                        error = Message.ShippingDateBeforeDeliveryDate;
                    }
                }

                if (error != string.Empty)
                {
                    this.OrderUpdateInProgress = false;
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: ApplicationSettings.TouchScreenMode);
                    return;
                }

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate)
                {
                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice.IsNullOrZero()))
                    {
                        NouvemMessageBox.Show(Message.NoPriceWarningOnOrderLines, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            this.OrderUpdateInProgress = false;
                            return;
                        }
                    }

                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.PriceListID == 0))
                    {
                        NouvemMessageBox.Show(Message.NoPriceBookFoundOnOrderLine, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }

                    if (!ApplicationSettings.DisablePriceChecksOnSaleOrders && this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice != x.UnitPriceOnPriceBook))
                    {
                        var msg = $"{Message.SaleOrderPricesChange}{Environment.NewLine}";
                        foreach (var saleDetail in this.SaleDetails.Where(x => x.UnitPrice != x.UnitPriceOnPriceBook))
                        {
                            msg +=
                                $"{saleDetail.InventoryItem.Master.Name}: From {saleDetail.UnitPriceOnPriceBook.ToDecimal().ToDisplayPrecision(2)} to {saleDetail.UnitPrice.ToDecimal().ToDisplayPrecision(2)}{Environment.NewLine}";
                        }

                        msg += Message.ProceedPrompt;
                        NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.OKCancel);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Cancel)
                        {
                            return;
                        }
                    }
                }

                #endregion

                if (!this.CheckStockAvailability())
                {
                    return;
                }

                this.CreateSale();

                if (!this.CheckOrderLimit())
                {
                    return;
                }

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.CheckForOutstandingOrders)
                {
                    var localSales = this.DataManager.GetOpenARDispatches(this.SelectedPartner.BPMasterID);
                    Sale matchingSale = null;
                    var match = true;
                    foreach (var localSale in localSales)
                    {
                        if (localSale.SaleDetails.Count != this.Sale.SaleDetails.Count)
                        {
                            continue;
                        }

                        foreach (var localSaleDetail in localSale.SaleDetails)
                        {
                            var existingDetail =
                                this.Sale.SaleDetails.FirstOrDefault(x => x.INMasterID == localSaleDetail.INMasterID);
                            if (existingDetail == null)
                            {
                                match = false;
                                break;
                            }

                            if (existingDetail.QuantityOrdered != localSaleDetail.QuantityOrdered
                                || existingDetail.WeightOrdered != localSaleDetail.WeightOrdered)
                            {
                                match = false;
                                break;
                            }
                        }

                        if (match)
                        {
                            matchingSale = localSale;
                            break;
                        }
                    }

                    if (matchingSale != null)
                    {
                        NouvemMessageBox.Show(Message.DuplicateOrderDetected, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.SetActiveDocument(this);
                            IList<Sale> localSaleToSend = new List<Sale> { matchingSale };
                            Messenger.Default.Send(Tuple.Create(localSaleToSend, ViewType.Order), Token.SearchForSale);

                            if (this.Sale != null)
                            {
                                this.Locator.SalesSearchData.SetView(ViewType.Order);
                            }

                            return;
                        }
                    }
                }

                this.UpdateDocNumbering();
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
                this.Sale.Copy = this.AutoCopyToDispatch || ApplicationSettings.AlwaysCopyToDispatch;
                var newSaleId = this.DataManager.AddNewOrder(this.Sale);
                if (newSaleId > 0)
                {
                    this.LastEdit = DateTime.Now;
                    this.Log.LogWarning(this.GetType(),
                        string.Format("1. AddSale(): Order created: Id:{0}, Number:{1}, Sale items count:{2}, AutoCopyToDispatch:{3}, AlwaysCoptToDispatch:{4}",
                        newSaleId, this.Sale.Number, this.Sale.SaleDetails.Count, this.AutoCopyToDispatch, ApplicationSettings.AlwaysCopyToDispatch));

                    this.Sale.SaleID = newSaleId;

                    if (this.AutoCopyToDispatch || ApplicationSettings.AlwaysCopyToDispatch)
                    {
                        ProgressBar.Run();
                        //this.AutoCopyOrderToDispatch();
                        if (ApplicationSettings.ShowMessageBoxOnSaleOrderAdd)
                        {
                            var dispatchNo = this.Sale.TopDocumentID;
                            var localNumber = this.NextNumber;
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                SystemMessage.Write(MessageType.Priority, string.Format(Message.SaleOrderAndDispatchCreated, localNumber, dispatchNo));
                                NouvemMessageBox.Show(string.Format(Message.SaleOrderAndDispatchCreated, localNumber, dispatchNo));
                            }));
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCreated, this.NextNumber));
                    }

                    this.ClearForm();
                    this.NextNumber = this.SelectedDocNumbering.NextNumber;
                    this.RefreshDocStatusItems();

                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.OrderNotCreated);
                }
            }
            finally
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    //this.GetInventoryItems();
                }));

                ProgressBar.Reset();
                this.SetControlMode(ControlMode.Add);
                this.OrderUpdateInProgress = false;
            }
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            this.OrderUpdateInProgress = true;

            try
            {
                #region validation

                if (!this.AuthorisationsManager.AllowUserToAddOrUpdateSaleOrders)
                {
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    this.SetControlMode(ControlMode.OK);
                    return;
                }

                string error = string.Empty;

                if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
                {
                    error = Message.NoCustomerSelected;
                }
                else if (this.Sale != null && this.Sale.SaleID > 0 && this.DataManager.DoesMasterDocumentHaveTransactions(this.Sale.SaleID))
                {
                    error = Message.OrderPartOfInprogressDispatch;
                    this.IsReadOnly = true;
                    this.DisableDocStatusChange = true;
                }
                else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
                {
                    error = Message.NoDocNumberingSelected;
                }
                else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
                {
                    error = Message.NoDocStatusSelected;
                }
                else if (ApplicationSettings.DeliveryDateCannotBeInThePast && this.DeliveryDate.HasValue && this.DeliveryDate < DateTime.Today)
                {
                    error = Message.DeliveryDateInPastError;
                }
                else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    if (this.SelectedDocStatus.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                    {
                        error = Message.NoSaleDetails;
                    }
                }
                else if (ApplicationSettings.ShippingDateMustBeAfterDeliveryDate && this.DeliveryDate.HasValue && this.ShippingDate.HasValue)
                {
                    if (this.DeliveryDate < this.ShippingDate)
                    {
                        error = Message.ShippingDateBeforeDeliveryDate;
                    }
                }

                if (error == string.Empty && ApplicationSettings.CheckRoutesAtOrder && this.SelectedRoute != null
                    && this.SelectedDocStatus != null && this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    error = this.CheckRoutesAtOrder();
                }

                if (error != string.Empty)
                {
                    this.OrderUpdateInProgress = false;
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: ApplicationSettings.TouchScreenMode);
                    return;
                }

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate)
                {
                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice.IsNullOrZero()))
                    {
                        NouvemMessageBox.Show(Message.NoPriceWarningOnOrderLines, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            this.OrderUpdateInProgress = false;
                            return;
                        }
                    }

                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.PriceListID == 0))
                    {
                        NouvemMessageBox.Show(Message.NoPriceBookFoundOnOrderLine, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }

                    if (!ApplicationSettings.DisablePriceChecksOnSaleOrders && this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice != x.UnitPriceOnPriceBook))
                    {
                        var msg = $"{Message.SaleOrderPricesChange}{Environment.NewLine}";
                        foreach (var saleDetail in this.SaleDetails.Where(x => x.UnitPrice != x.UnitPriceOnPriceBook))
                        {
                            msg +=
                                $"{saleDetail.InventoryItem.Master.Name}: From {saleDetail.UnitPriceOnPriceBook.ToDecimal().ToDisplayPrecision(2)} to {saleDetail.UnitPrice.ToDecimal().ToDisplayPrecision(2)}{Environment.NewLine}";
                        }

                        msg += Message.ProceedPrompt;
                        NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.OKCancel);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Cancel)
                        {
                            return;
                        }
                    }
                }

                #endregion

                if (!this.CheckStockAvailability())
                {
                    return;
                }

                // if we are re-opening an order, we need to cancel the associated dispatch docket, and recreate it.
                var updateDispatch = this.Sale.NouDocStatusID ==
                                                NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                var cancelDispatch = this.SelectedDocStatus != null && this.SelectedDocStatus.NouDocStatusID ==
                                                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;

                this.CreateSale();

                if (!this.CheckOrderLimit())
                {
                    return;
                }

                int dispatchId = 0;
                if (!cancelDispatch && updateDispatch)
                {
                    dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);
                    if (dispatchId == 0)
                    {
                        this.Sale.Copy = this.AutoCopyToDispatch || ApplicationSettings.AlwaysCopyToDispatch;
                    }
                }

                if (this.DataManager.UpdateOrder(this.Sale))
                {
                    this.LastEdit = DateTime.Now;
                    this.Log.LogWarning(this.GetType(),
                        string.Format("U 1. UpdateSale(): Order updated: Id:{0}, Number:{1}, Sale items count:{2}, cancel:{3}, update dispatch:{4}",
                        this.Sale.SaleID, this.Sale.Number, this.Sale.SaleDetails.Count, cancelDispatch, updateDispatch));
                    if (cancelDispatch)
                    {
                        if (this.DataManager.CancelARDispatchFromSaleOrder(this.Sale.SaleID))
                        {

                        }
                    }
                    else if (updateDispatch)
                    {
                        //var dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);
                        if (dispatchId > 0)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Updating dispatch docket:{0}", dispatchId));
                            this.Sale.ParentID = dispatchId;
                            this.DataManager.UpdateARDispatchFromSaleOrder(this.Sale);
                        }
                        else
                        {
                            if (this.AutoCopyToDispatch || ApplicationSettings.AlwaysCopyToDispatch)
                            {
                                ProgressBar.Run();
                                //this.AutoCopyOrderToDispatch();
                                ProgressBar.Reset();
                                if (ApplicationSettings.ShowMessageBoxOnSaleOrderAdd)
                                {
                                    var dispatchNo = this.Sale.TopDocumentID;
                                    var localNumber = this.NextNumber;
                                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                    {
                                        SystemMessage.Write(MessageType.Priority, string.Format(Message.DispatchDocketCreatedFromExistingSaleOrder, localNumber, dispatchNo));
                                        NouvemMessageBox.Show(string.Format(Message.DispatchDocketCreatedFromExistingSaleOrder, localNumber, dispatchNo));
                                    }));
                                }
                            }
                        }
                    }

                    if (this.SelectedDocStatus != null &&
                        this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.NextNumber));
                        this.ClearForm();
                    }
                    else if (this.SelectedDocStatus != null &&
                        this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.NextNumber));
                        this.ClearForm();
                    }
                    else if (this.SelectedDocStatus != null &&
                        this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCancelled, this.NextNumber));
                        this.ClearForm();
                    }

                    //this.GetAllSales();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderNotUpdated, this.NextNumber));
                }
            }
            finally
            {

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    //this.GetInventoryItems();
                }));

                this.SetControlMode(ControlMode.Add);
                this.OrderUpdateInProgress = false;
            }
        }
        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings =
                new ObservableCollection<DocNumber>(
                    this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.AROrder)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            //SaleDetail.PriceChangingOnDesktop = false;
            //Task.Factory.StartNew(this.GetAllSales);
            base.OnLoadingCommandExecute();
            this.ShowRoute = true;
            this.ClearForm();
            this.GetLocalDocNumberings();
            if (ApplicationSettings.AlwaysCopyToDispatch)
            {
                this.AutoCopyToDispatch = true;
            }

            //Messenger.Default.Register<string>(this, Token.CopySuccessful, s =>
            //{
            //    if (this.AutoCopying)
            //    {
            //        SystemMessage.Write(MessageType.Priority, Message.OrderAndDispatchCreated);
            //    }
            //});
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            NouvemGlobal.RetrieveProductStockSummary = false;
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            base.OnUnloadedCommandExecute();
            Messenger.Default.Unregister<string>(this, Token.CopySuccessful);
            this.Close();
        }

        protected override void AllowWindowToClose()
        {
            this.CanCloseForm = true;
            Messenger.Default.Send(true, Token.CanCloseOrderWindow);
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (!this.CanCloseForm)
            {
                this.CheckForUnsavedData();
                return;
            }

            ViewModelLocator.ClearOrder();
           
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseOrderWindow);
                Messenger.Default.Send(Token.Message, Token.SetActiveDocument);
                ViewModelLocator.ClearARDispatch();
            }));
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderSelected);
                return;

            }

            #endregion

            if (this.SelectedDocumentCopyTo.Equals(Strings.DispatchDocket))
            {
                CopyingDocument = true;
                this.CopyOrderToDispatch();
            }
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var quotes = this.DataManager.GetQuotes(this.SelectedPartner.BPMasterID);

            if (!quotes.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Quotes));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(quotes, ViewType.Quote), Token.SearchForSale);
            CopyingDocument = true;
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        private void DisplayDispatchesCommandExecute()
        {
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            this.Locator.ARDispatch.FindDispatchSales(this.SelectedSaleDetail.INMasterID);
        }

        #endregion

        #region helper
       
        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.Order)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllOrdersByDate(orderStatuses, 
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateSaleOrder);
                //this.CustomerSales = localSales
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date 
                //        && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateSaleOrder);
                //this.CustomerSales = this.DataManager.GetAllOrders(orderStatuses)
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateSaleOrder);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.Order);
            }
        }

        /// <summary>
        /// Copies the current order to a dispatch docket.
        /// </summary>
        private void CopyOrderToDispatch()
        {
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
            Messenger.Default.Send(ViewType.ARDispatch);
            CopyingDocument = true;
            this.Sale.CopyingFromOrder = true;
            this.Locator.ARDispatch.CopySale(this.Sale);
            this.Locator.ARDispatch.AddTransaction();
        }

        /// <summary>
        /// Copies the current order to a dispatch docket.(We don't show the dispatch screen).
        /// </summary>
        private void AutoCopyOrderToDispatch(Sale saleToCopy = null)
        {
            var localSale = this.Sale;
            if (saleToCopy != null)
            {
                localSale = saleToCopy;
            }

            try
            {
                ProgressBar.Run();
                this.AutoCopying = true;
                localSale.CreationDate = DateTime.Now;
                localSale.CopyingFromOrder = true;
               
                localSale.BaseDocumentReferenceID = this.Sale.SaleID;
                this.Log.LogWarning(this.GetType(), string.Format("3. AutoCopyOrderToDispatch(): Base document ref id:{0}", localSale.BaseDocumentReferenceID));
                this.Locator.ARDispatch.CopySale(localSale);
                this.Locator.ARDispatch.AddTransaction();                

            }
            finally
            {
                ProgressBar.Reset();
                this.AutoCopying = false;
            }
        }

        /// <summary>
        /// Gets all the quotes.
        /// </summary>
        private void GetAllSales()
        {
            //this.AllSales = this.DataManager.GetAllOrders(this.DocStatusesOpen);
        }

        /// <summary>
        /// Displays the base document.
        /// </summary>
        private void ShowBaseDocument()
        {
            try
            {
                var baseDoc = this.DataManager.GetQuoteByID(this.Sale.BaseDocumentReferenceID.ToInt());
                Messenger.Default.Send(ViewType.Quote);
                this.Locator.Quote.Sale = baseDoc;
                SystemMessage.Write(MessageType.Priority, Message.BaseDocumentLoaded);
            }
            finally
            {
                //this.Locator.ARDispatch.DisplayModeOnly = false;
            }
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.Order);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        #endregion

        #endregion
    }
}



   
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Sales
{
    public class BatchDataViewModel : NouvemViewModelBase
    {

        #region field

        /// <summary>
        /// The associated product batches.
        /// </summary>
        private ObservableCollection<SaleDetail> batches = new ObservableCollection<SaleDetail>();

        private int? productId;

        private IList<App_GetOrderBatchData_Result> lineData;

        /// <summary>
        /// The batch product.
        /// </summary>
        private string productName;

        /// <summary>
        /// The batch product.
        /// </summary>
        private bool enforce;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchDataViewModel"/> class.
        /// </summary>
        public BatchDataViewModel(Tuple<int?, IList<App_GetOrderBatchData_Result>> productData)
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.UpdateCommand = new RelayCommand(this.UpdateCommandExecute);

            #endregion

            this.productId = productData.Item1;
            this.lineData = productData.Item2;
            this.GetBatches();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the product batches.
        /// </summary>
        public bool Enforce
        {
            get { return this.enforce; }
            set
            {
                this.enforce = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the product batches.
        /// </summary>
        public string ProductName
        {
            get { return this.productName; }
            set
            {
                this.productName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the product batches.
        /// </summary>
        public ObservableCollection<SaleDetail> Batches
        {
            get { return this.batches; }
            set
            {
                this.batches = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        public ICommand UpdateCommand{ get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Override, to set the focus to the order vm.
        /// </summary>
        private void UpdateCommandExecute()
        {
            this.SetControlMode(ControlMode.Update);
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.SendBatchData();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        #endregion

        #region private

        private void GetBatches()
        {
            var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.productId);
            if (localProduct != null)
            {
                this.ProductName = localProduct.Master.Name;
            }

            this.Batches.Clear();
            var data = this.DataManager.GetProductStockDataByBatch(this.productId.ToInt());
            foreach (var productionData in data)
            {
                decimal? qtyOrdered = null;
                decimal? wgtOrdered = null;
             
                var localData = this.lineData.FirstOrDefault(x => x.BatchID == productionData.BatchNumberID);
                if (localData != null)
                {
                    qtyOrdered = localData.QuantityOrdered;
                    wgtOrdered = localData.WeightOrdered;
                    this.Enforce = localData.Enforce.ToBool();
                }

                this.Batches.Add(new SaleDetail
                {
                    IgnoreStockRetrieval = true,
                    INMasterID = productId.ToInt(),
                    Wgt = productionData.RemainingWgt,
                    Qty = productionData.RemainingQty,
                    Batch = new BatchNumber {BatchNumberID = productionData.BatchNumberID, Number = productionData.Reference},
                    PROrderID = productionData.PROrderID,
                    QuantityOrdered = qtyOrdered,
                    WeightOrdered = wgtOrdered,
                    Enforce = this.Enforce
                });
            }
        }

        /// <summary>
        /// Send the batch data to the main order screen.
        /// </summary>
        private void SendBatchData()
        {
            foreach (var saleDetail in this.Batches.Where(x => x.CompleteBatch))
            {
                this.DataManager.ChangeOrderStatus(saleDetail.PROrderID.ToInt(),3);
            }

            var orderData = new List<App_GetOrderBatchData_Result>();
            foreach (var saleDetail in Batches.Where(x => x.QuantityOrdered.ToDecimal() > 0 || x.WeightOrdered.ToDecimal() > 0))
            {
                orderData.Add(new App_GetOrderBatchData_Result
                {
                    BatchID = saleDetail.BatchNumberId,
                    QuantityOrdered = saleDetail.QuantityOrdered,
                    WeightOrdered = saleDetail.WeightOrdered,
                    INMasterID = this.productId,
                    Enforce = this.Enforce
                });
            }

            Messenger.Default.Send(Tuple.Create(orderData,this.productId), Token.SetBatchOrderData);
            this.Close();
        }

        /// <summary>
        /// Close, and clean up.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseBatchDataWindow);
        }

        #endregion
    }
}

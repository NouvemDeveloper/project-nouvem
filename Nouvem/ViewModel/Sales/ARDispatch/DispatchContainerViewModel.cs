﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Reporting.WinForms;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using DispatchContainer = Nouvem.Model.BusinessObject.DispatchContainer;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    public class DispatchContainerViewModel : NouvemViewModelBase
    {
       #region field

        /// <summary>
        /// Show all flag.
        /// </summary>
        private bool showAllContainers;

        /// <summary>
        /// The containers.
        /// </summary>
        private ObservableCollection<DispatchContainer> dispatchContainers;

        /// <summary>
        /// The containers.
        /// </summary>
        private DispatchContainer selectedDispatchContainer;

        /// <summary>
        /// The report values.
        /// </summary>
        private IList<MultiReportValue> multivalues = new List<MultiReportValue>();

        /// <summary>
        /// The selected sale.
        /// </summary>
        private Sale selectedSale;

        /// <summary>
        /// The selected sale.
        /// </summary>
        private Sale previousSelectedSale;

        private int reportId;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatchContainerViewModel"/> class.
        /// </summary>
        public DispatchContainerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.RefreshDispatchContainers, s =>
            {
                this.Refresh();
            });

            // Register for the incoming customers sales.
            Messenger.Default.Register<List<string>>(this, Token.ReportValues, s =>
            {
                var reportsPrinted = new List<int>();
                foreach (var value in s)
                {
                    reportsPrinted.Add(value.ToInt());
                }

                if (this.DataManager.MarkInvoicesPrinted(reportsPrinted))
                {
                    this.GetContainers(this.showAllContainers);
                }
            });

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            #endregion

            #region command handler

            this.PrintReportCommand = new RelayCommand(this.PrintReportCommandExecute);
            this.OnLoadingCommand = new RelayCommand(() => this.IsFormLoaded = true);
            this.OnClosingCommand = new RelayCommand(() => this.IsFormLoaded = false);
            this.MarkAsPrintedCommand = new RelayCommand<string>(s => this.MarkAsPrintedCommandExecute(s));
            this.PrintNextCommand = new RelayCommand<string>(s => this.PrintNextCommandExecute(s));
            this.ShowDispatchesCommand = new RelayCommand(this.ShowDispatchesCommandExecute);
            this.OnMasterRowExpandedCommand = new RelayCommand(() => this.SelectedSale = null);
            this.SaleItemSelectedCommand = new RelayCommand<DevExpress.Xpf.Grid.SelectedItemChangedEventArgs>(e =>
            {
                if (e.NewItem is Sale)
                {
                    this.SelectedDispatchContainer = null;
                    this.SelectedSale = (Sale) e.NewItem;
                }
            });

            this.RefreshCommand = new RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.RefreshComplete);
            });

            #endregion

            this.GetContainers();
        }

        #endregion

        #region public interface

        #region property
       
        /// <summary>
        /// Gets or sets a value indicating whether all the containers are to be displayed
        /// </summary>
        public bool ShowAllContainers
        {
            get
            {
                return this.showAllContainers;
            }

            set
            {
                this.showAllContainers = value;
                this.RaisePropertyChanged();
                this.GetContainers(value);
            }
        }
    
        /// <summary>
        /// Gets or sets the containers.
        /// </summary>
        public ObservableCollection<DispatchContainer> DispatchContainers
        {
            get
            {
                return this.dispatchContainers;
            }

            set
            {
                this.dispatchContainers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the containers.
        /// </summary>
        public DispatchContainer SelectedDispatchContainer
        {
            get
            {
                return this.selectedDispatchContainer;
            }

            set
            {
                this.selectedDispatchContainer = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.SelectedSale = null;
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand OnMasterRowExpandedCommand { get; private set; } 

         /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand SaleItemSelectedCommand { get; private set; } 

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand PrintReportCommand { get; private set; } 

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand ShowDispatchesCommand { get; private set; }

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand MarkAsPrintedCommand { get; private set; }

        /// <summary>
        /// Gets the command to print a report.
        /// </summary>
        public ICommand PrintNextCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        public Sale SelectedSale
        {
            get
            {
                return this.selectedSale;
            }

            set
            {
                this.selectedSale = value;
                if (value != null)
                {
                    this.previousSelectedSale = this.SelectedSale;
                }
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            #endregion

            var reportsPrinted = new List<int>();
            this.multivalues = new List<MultiReportValue>();

            if (this.SelectedDispatchContainer == null && this.SelectedSale == null)
            {
                this.SelectedSale = this.previousSelectedSale;
            }

            IList<Sale> sales = new List<Sale>();
            if (this.SelectedSale == null)
            {
                if (this.SelectedDispatchContainer == null)
                {
                    this.SelectedSale = null;
                    SystemMessage.Write(MessageType.Issue, Message.NoContainerSelected);
                    return;
                }

                sales = this.SelectedDispatchContainer.Sales.Where(x => x.Invoice != null).ToList();
            }
            else
            {
                if (this.SelectedSale.Invoice == null)
                {
                    this.SelectedDispatchContainer = null;
                    SystemMessage.Write(MessageType.Issue, Message.DocketNotInvoiced);

                    // open the dispatch docket
                    var baseDoc = this.DataManager.GetARDispatchById(this.SelectedSale.SaleID);
                    this.Locator.ARDispatch.DisplayModeOnly = true;
                    Messenger.Default.Send(ViewType.ARDispatch);
                    this.Locator.ARDispatch.Sale = baseDoc;

                    this.SelectedSale = null;
                    return;
                }

                sales.Add(this.SelectedSale);
            }

            if (!sales.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.NoContainerInvoicesToPrint);
                return;
            }

            this.SelectedDispatchContainer = null;
            this.SelectedSale = null;

            foreach (var selectedInvoice in sales)
            {
                this.multivalues.Add(new MultiReportValue { ID = selectedInvoice.Invoice.ARInvoiceID, Number = selectedInvoice.Invoice.Number });
                reportsPrinted.Add(selectedInvoice.Invoice.ARInvoiceID);
            }

            this.DataManager.UpdateMultiValueReports(this.multivalues);

            this.reportId = sales.First().Invoice.ARInvoiceID;
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { this.reportId.ToString() } } };
          
            var reportData = new ReportData { Name = ReportName.BallonInvoiceFiltered, ReportParameters = reportParam };

            try
            {
                if (preview)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {
                    foreach (var selectedInvoice in sales)
                    {
                        var localReportId = selectedInvoice.Invoice.ARInvoiceID;
                        reportParam = new List<ReportParameter>
                        {
                            new ReportParameter {Name = "InvoiceID", Values = {localReportId.ToString()}}
                        };

                        reportData = new ReportData
                        {
                            Name = ReportName.BallonInvoiceFiltered,
                            ReportParameters = reportParam
                        };

                        this.ReportManager.PrintReport(reportData);
                    }

                    if (this.DataManager.MarkInvoicesPrinted(reportsPrinted))
                    {
                        this.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
            finally
            {
                //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.CheckForContainerCompletion()));
            }
        }

        #endregion

        #region private

        private void GetContainers(bool showAll = false)
        {
            if (showAll)
            {
                this.DispatchContainers = new ObservableCollection<DispatchContainer>(this.DataManager.GetDispatchContainers(0));
            }
            else
            {
                this.DispatchContainers = new ObservableCollection<DispatchContainer>(this.DataManager.GetDispatchContainers(NouvemGlobal.NouDocStatusActive.NouDocStatusID));
            }
        }

        private void PrintReportCommandExecute()
        {
            this.PrintReport(Constant.Preview);
        }

        /// <summary>
        /// Display the dispatch search screen.
        /// </summary>
        private void ShowDispatchesCommandExecute()
        {
            this.Locator.ARDispatch.ShowAllSales();
        }

        private void PrintNextCommandExecute(string mode)
        {
            var newReportId = string.Empty;
            if (mode == "Next")
            {
                var localId = this.multivalues.FirstOrDefault(x => x.ID == this.reportId);
                for (int i = 0; i < this.multivalues.Count; i++)
                {
                    if (this.multivalues.ElementAt(i).ID == this.reportId && i + 1 < this.multivalues.Count)
                    {
                        newReportId = this.multivalues.ElementAt(i + 1).ID.ToString();
                        break;
                    }
                }
            }
            else
            {
                var localId = this.multivalues.FirstOrDefault(x => x.ID == this.reportId);
                for (int i = 0; i < this.multivalues.Count; i++)
                {
                    if (this.multivalues.ElementAt(i).ID == this.reportId && i > 0)
                    {
                        newReportId = this.multivalues.ElementAt(i - 1).ID.ToString();
                        break;
                    }
                }
            }

            if (newReportId != string.Empty)
            {
                this.reportId = newReportId.ToInt();
                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter {Name = "InvoiceID", Values = {this.reportId.ToString()}}
                };
                var reportData = new ReportData
                {
                    Name = ReportName.BallonInvoiceFiltered,
                    ReportParameters = reportParam
                };
                this.ReportManager.PreviewReport(reportData);
            }
        }

        /// <summary>
        /// marks the invoices as printed.
        /// </summary>
        /// <param name="printed">The printed flag.</param>
        private void MarkAsPrintedCommandExecute(string printed)
        {
            if (this.SelectedDispatchContainer == null)
            {
                return;
            }

            var ids =
                this.SelectedDispatchContainer.Sales.Where(x => x.Invoice != null)
                    .Select(x => x.Invoice.ARInvoiceID)
                    .ToList();
           var complete = printed == "Printed";
            if (this.DataManager.CompleteDispatchContainer(ids, this.SelectedDispatchContainer.DispatchContainerID,
                complete))
            {
                if (complete)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DispatchContainerClosed, this.SelectedDispatchContainer.Reference));
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DispatchContainerOpened, this.SelectedDispatchContainer.Reference));
                }

                this.ShowAllContainers = false;
            }
        }

        /// <summary>
        /// Refreshes the containers.
        /// </summary>
        private void Refresh()
        {
            this.GetContainers();
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseDispatchContainerWindow);
            ViewModelLocator.ClearDispatchContainer();
        }

        #endregion


        protected override void ControlSelectionCommandExecute()
        {
           this.Close();
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="FTraceViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.FTrace;

    public class FTraceViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// The ftrace connection object.
        /// </summary>
        private FTraceConnect ftrace;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FTraceViewModel"/> class.
        /// </summary>
        public FTraceViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.SetUpFTrace();
            this.GetFTraceSales();
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.FromDate = ApplicationSettings.SalesSearchFromDate;
            this.ToDate = DateTime.Today;
            this.ShowAllOrders = ApplicationSettings.SalesSearchShowAllOrders;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            ApplicationSettings.SalesSearchFromDate = this.FromDate;
            ApplicationSettings.SalesSearchShowAllOrders = this.ShowAllOrders;
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Export:
                    this.ExportToFTrace();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        protected override void HandleSelectedSale(Sale sale)
        {
            this.selectedSale = sale;
            this.RaisePropertyChanged("SelectedSale");
            this.SetControlMode(ControlMode.Export);
        }

        /// <summary>
        /// Refresh the calling orders.
        /// </summary>
        protected override void Refresh()
        {
            this.GetFTraceSales();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            this.ReportMode = false;
            Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
        }

        #endregion

        #region private

        private void SetUpFTrace()
        {
            this.ftrace = new FTraceConnect(
                ApplicationSettings.FTracePath,
                ApplicationSettings.FTraceURL,
                ApplicationSettings.FTraceUserName,
                ApplicationSettings.FTracePassword);

            this.ftrace.DataSent += (sender, args) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    ProgressBar.Run();
                });
            };
        }

        /// <summary>
        /// Gets the dispatches.
        /// </summary>
        private void GetFTraceSales()
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetFTraceDispatches(this.ShowAllOrders, this.FromDate, this.ToDate));
            }));
        }

        /// <summary>
        /// Exports the selected dispatch ftrace data.
        /// </summary>
        private void ExportToFTrace()
        {
            if (this.SelectedSale == null)
            {
                return;
            }

            int dataCount;
            try
            {
                DataTable ftraceData = null;
                DataTable ftraceDataProcessing = null;
                DataTable ftraceDataSlaughter = null;
                if (this.SelectedSale.SlaughterOnly)
                {
                    ftraceDataSlaughter = this.DataManager.GetFTraceSlaughterOnly(this.SelectedSale.SaleID);
                    if (ftraceDataSlaughter.Rows.Count == 0)
                    {
                        SystemMessage.Write(MessageType.Issue, "The batches associated with this dispatch have no into batch data");
                        return;
                    }

                    dataCount = ftraceDataSlaughter.Rows.Count + 3;
                }
                else if (this.SelectedSale.DispatchAndProcessingOnly)
                {
                    ftraceData = this.DataManager.GetFTraceDispatch(this.SelectedSale.SaleID);
                    ftraceDataProcessing = this.DataManager.GetFTraceProcessing(this.SelectedSale.SaleID);

                    if (ftraceData.Rows.Count == 0)
                    {
                        SystemMessage.Write(MessageType.Issue, "There are no GTIN products on this dispatch");
                        return;
                    }

                    dataCount = ftraceData.Rows.Count + 3;
                }
                else
                {
                    ftraceData = this.DataManager.GetFTraceDispatch(this.SelectedSale.SaleID);
                    ftraceDataProcessing = this.DataManager.GetFTraceProcessing(this.SelectedSale.SaleID);
                    ftraceDataSlaughter = this.DataManager.GetFTraceSlaughter(this.SelectedSale.SaleID);

                    if (ftraceData.Rows.Count == 0)
                    {
                        SystemMessage.Write(MessageType.Issue, "There are no GTIN products on this dispatch");
                        return;
                    }

                    if (ftraceDataSlaughter.Rows.Count == 0)
                    {
                        SystemMessage.Write(MessageType.Issue, "The batches associated with this dispatch have no into batch data");
                        return;
                    }

                    dataCount = ftraceDataSlaughter.Rows.Count + 3;
                }

                ProgressBar.SetUp(0, dataCount);
                ProgressBar.Run();

                this.ftrace.GenerateFTraceData(ftraceData, ftraceDataProcessing, ftraceDataSlaughter);
                this.DataManager.MarkFTraceAsExported(this.SelectedSale.SaleID);
                SystemMessage.Write(MessageType.Priority, "FTrace data successfully sent");
                this.Refresh();
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        #endregion

    }
}






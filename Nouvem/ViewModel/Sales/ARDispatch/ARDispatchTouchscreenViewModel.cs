﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchTouchscreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Nouvem.View.Production.IntoProduction;
using Nouvem.ViewModel.Purchases.APReceipt;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class ARDispatchTouchscreenViewModel : ARDispatchViewModel
    {
        #region field

        /// <summary>
        /// The pallet stock running count.
        /// </summary>
        private string palletCount;

        /// <summary>
        /// The touch screen selected sale details.
        /// </summary>
        private ObservableCollection<SaleDetail> selectedSaleDetails;

        /// <summary>
        /// The grid/count vm.
        /// </summary>
        private ViewModelBase centerDisplayViewModel;

        /// <summary>
        /// The product groups.
        /// </summary>
        private IList<INGroup> groups = new List<INGroup>();

        /// <summary>
        /// The incoming manually set weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The incoming clead read flag.
        /// </summary>
        private bool cleanRead;

        /// <summary>
        /// The tares to apply from the calculator.
        /// </summary>
        private IList<TareCalculator> tares = new List<TareCalculator>();

        /// <summary>
        /// The container reference.
        /// </summary>
        private string dispatchContainerReference;

        /// <summary>
        /// The incoming tare weight.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The batch number used when in scan and weigh mode.
        /// </summary>
        private BatchNumber scanAndWeighModeBatchNumber;

        /// <summary>
        /// The root group children.
        /// </summary>
        private HashSet<int> childGroupIds = new HashSet<int>();

        /// <summary>
        /// Can add a production batch flag.
        /// </summary>
        private bool canAddBatch;

        /// <summary>
        /// Flag, as to whether a batch number is to be generated.
        /// </summary>
        private bool generateBatchNo;

        /// <summary>
        /// The current batch number.
        /// </summary>
        private BatchNumber batchNumber;

        /// <summary>
        /// The batch traceability data.
        /// </summary>
        private readonly List<BatchTraceability> traceabilityData = new List<BatchTraceability>();

        /// <summary>
        /// The dispatch containers.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.DispatchContainer> dispatchContainers = new ObservableCollection<Model.BusinessObject.DispatchContainer>();
        
        /// <summary>
        /// All the order details transactions.
        /// </summary>
        private IList<SaleDetail> allOrderDetails;

        /// <summary>
        /// The batch/spec data to display.
        /// </summary>
        private string batchDataToDisplay;

        /// <summary>
        /// The batch/spec label text.
        /// </summary>
        private string batchLabelText;

        /// <summary>
        /// The booked against data.
        /// </summary>
        private GoodsReceiptData BookedAgainstProductData;

        /// <summary>
        /// The trim product group ids.
        /// </summary>
        private IList<int> trimProductGroups = new List<int>();

        /// <summary>
        /// Temporarily disable weighing flag.
        /// </summary>
        private bool disableWeighing;

        /// <summary>
        /// The items to palletise.
        /// </summary>
        protected HashSet<int> itemsToPalletise = new HashSet<int>();

        #endregion
     
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARDispatchTouchscreenViewModel"/> class.
        /// </summary>
        public ARDispatchTouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.DisplayBatchData, s =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.OnPreviewMouseUpBatchCommandExecute();
                }));
            });

            Messenger.Default.Register<string>(this, Token.CalenderDisplayValueDispatch, s =>
            {
                if (!string.IsNullOrWhiteSpace(s))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(string.Format(Message.DeliveryDateChangePrompt, s),NouvemMessageBoxButtons.YesNo, touchScreen:true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.DeliveryDateTouchscreen = s.ToDate();
                            this.DataManager.UpdateDeliveryDate(this.Sale.SaleID, s.ToDate());
                            SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
                        }
                    }));
                }
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals(Constant.Route))
                {
                    this.ShowOrders(c.ID);
                }
            });

            // register for the incoming plus manual weight.
            Messenger.Default.Register<string>(this, KeypadTarget.PlusManualWeight, this.HandlePlusManualWeight);

            // Register for the label change name.
            Messenger.Default.Register<Model.DataLayer.Process>(this, Token.ProcessSelected, i =>
            {
                var checkNotMade = this.DataManager.GetProcessStartUpChecks(i.ProcessID);
                if (checkNotMade.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.MissingProcessStartUpCheckMessage = string.Format(Message.HACCPNotCompleted, i.Name.Trim(), checkNotMade.First());
                        SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                        NouvemMessageBox.Show(this.MissingProcessStartUpCheckMessage, touchScreen: true);
                    }));

                    this.DisableModule = true;
                    return;
                }

                this.SetDispatchMode(i);
                if (this.Locator.ProcessSelection.SaveAsDefaultProcess)
                {
                    this.DataManager.SetDefaultModuleProcess(ViewType.ARDispatch, ApplicationSettings.DispatchMode.ToString(), NouvemGlobal.DeviceId.ToInt());
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<List<Sale>>(this, Token.CompleteMultiOrder, orders =>
            {
                var printReports = false;
                if (ApplicationSettings.PrintReportOnCompletion)
                {
                    NouvemMessageBox.Show(Message.PrintMultiReportPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true, showCounter: true);
                    printReports = NouvemMessageBox.UserSelection == UserDialogue.Yes;
                }

                foreach (var order in orders)
                {
                    this.SelectedPartner = this.Customers.FirstOrDefault(x => x.BPMasterID == order.BPCustomer.BPMasterID);
                    this.Sale = this.DataManager.GetARDispatchById(order.SaleID);
                    this.CompleteMultiOrder(printReports);
                    NouvemMessageBox.Show(string.Format(Message.OrdersCompleted, order.Number), touchScreen: true, flashMessage: true);
                }
            });

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowDispatchAttributes = b);

            Messenger.Default.Register<string>(this, Token.AddTare, s => this.HandleTareCalculators());

            // apply the tares from the calculator.
            Messenger.Default.Register<List<TareCalculator>>(this, Token.TaresSet, o =>
            {
                this.tares = o;
                this.applyTares = true;
                Application.Current.Dispatcher.Invoke(() => this.Locator.Indicator.Tare = TareCalculator.TotalTare);
            });

            Messenger.Default.Register<bool>(this, Token.WeightModeChanged, b => this.manualWeight = b);

            if (ApplicationSettings.AutoWeightAtDispatch)
            {
                Messenger.Default.Register<Tuple<decimal, bool>>(this, Token.ScalesInMotion, data =>
                {
                    if (this.disableWeighing)
                    {
                        return;
                    }

                    if (this.Locator.Indicator.WeighMode == WeighMode.Manual)
                    {
                        return;
                    }

                    var localWeight = data.Item1;
                    var inMotion = data.Item2;

                    if (inMotion)
                    {
                        this.CheckDisturbance();
                    }

                    if (localWeight < ApplicationSettings.MinWeightAllowedDispatch || this.TolerantWeightReached)
                    {
                        this.allowAutoWeigh = true;
                    }

                    if (this.allowAutoWeigh && this.Locator.Indicator.Weight >= ApplicationSettings.MinWeightAllowedDispatch
                        && !inMotion && this.IndicatorManager.Indicator.IsWeightStable)
                    {
                        this.allowAutoWeigh = false;
                        this.TolerantWeightReached = false;
                        this.RecordWeight();
                    }
                });
            }

            Messenger.Default.Register<string>(this, Token.BatchAndProductMismatchReason, this.AddBatchProductMismatchReason);

            Messenger.Default.Register<bool>(this, Token.HighlightSelectedProduct, b =>
            {
                this.highlightSelectedProduct = b;
            });

            // Register for the touchscreen ui load completion notification.
            Messenger.Default.Register<string>(this, Token.TouchscreenUiLoaded, s => this.HandleTouchscreenLoad());

            // Register for the incoming supplier.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.SupplierSelected, s => this.SelectedPartner = s);

            // Register for the incoming supplier, creating a new order.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.CreateOrder, this.CreateNewOrder);

            // Register for the incoming supplier order.
            Messenger.Default.Register<Tuple<Sale, ViewBusinessPartner>>(this, Token.SaleSelected, this.HandleSelectedOrder);

            // Register for the incoming supplier order.
            Messenger.Default.Register<string>(this, Token.TareCalculatorRequested, s =>
            {
                #region validation

                if (this.Sale == null)
                {
                    return;
                }

                if (ApplicationSettings.MustSelectContainerForDispatch && (this.SelectedDispatchContainer == null || this.SelectedDispatchContainer.DispatchContainerID == 0))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoDispatchContainerSelected);
                    NouvemMessageBox.Show(Message.NoDispatchContainerSelected, touchScreen: true);
                    return;
                }

                if (this.SelectedPartner == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                    NouvemMessageBox.Show(Message.NoPartnerSelected, touchScreen: true);
                    return;
                }

                if (this.SelectedSaleDetail == null)
                {
                    NouvemMessageBox.Show(Message.NoProductForTareSelected, touchScreen:true);
                    return;
                }

                #endregion

                Messenger.Default.Send(Token.Message, Token.CreateTareCalculator);
                if (this.SelectedSaleDetail.InventoryItem != null)
                {
                    this.Locator.TareCalculator.ProductName = this.SelectedSaleDetail.InventoryItem.Name;
                }
            });

            // Register for the incoming product selection.
            Messenger.Default.Register<InventoryItem>(this, Token.ProductSelected, o =>
            {
                this.HandleProductSelection(o);
            });

            // Register for an incoming keypad price change.
            Messenger.Default.Register<string>(this, KeypadTarget.ChangePriceAtDispatch, s =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.ChangePriceAtTouchscreenDispatch(s);
                }));
            });

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                if (ApplicationSettings.DispatchMode == DispatchMode.Dispatch)
                {
                    return;
                }

                if (this.Locator.Indicator.WeighMode == WeighMode.Auto)
                {
                    return;
                }

                this.manualWeight = false;
                this.RecordWeight();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                if (ApplicationSettings.DispatchMode == DispatchMode.Dispatch)
                {
                    return;
                }

                if (this.Locator.Indicator.WeighMode == WeighMode.Auto)
                {
                    return;
                }

                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;
                this.RecordWeight();
            });

            // Register for the indicator weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            // Register for the touchscreen complete order action.
            Messenger.Default.Register<string>(this, Token.TouchscreenCompleteOrder, action => this.CompleteOrder());

            // Register for the new generated batch.
            Messenger.Default.Register<string>(this, Token.SpecSelected, spec =>
            {
                if (this.RecordWeightModes.Contains(ApplicationSettings.DispatchMode))
                {
                    this.BatchDataToDisplay = spec;
                    if (this.BatchNumber != null)
                    {
                        this.BatchDataToDisplay = string.Format("{0}/{1}", spec, this.BatchNumber.Number);
                    }
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleScannerData(s);
                    }));
                }
            });

            // Register for the incoming pieces.
            Messenger.Default.Register<decimal>(this, Token.QuantityUpdate, i =>
            {
                this.ProductQuantity = i;
         
                if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
                {
                    var localQty = this.ProductQuantity < 1 ? 1 : this.ProductQuantity;
                    var localReset = this.Locator.Indicator.DoNotResetTare;
                    this.Locator.Indicator.DoNotResetTare = false;
                    this.Locator.Indicator.Tare =
                        (this.SelectedSaleDetail.InventoryItem.PiecesTareWeight * localQty.ToDouble())
                        + this.SelectedSaleDetail.InventoryItem.BoxTareWeight;
                    this.Locator.Indicator.DoNotResetTare = localReset;
                }
            });

            Messenger.Default.Register<ProductionData>(this, Token.BatchNumberSelected, b =>
            {
                this.BatchNumber = b.BatchNumber;
                this.scanAndWeighModeBatchNumber = b.BatchNumber;

                if (ApplicationSettings.DispatchMode != DispatchMode.Dispatch)
                {
                    if (this.Sale != null)
                    {
                        this.CheckAttributeBatchChangeReset();
                    }

                    // regenerate the batch attribute values.
                    this.clearTraceability = false;
                    this.HandleSelectedSaleDetail();
                    this.clearTraceability = true;
                }
            });

            // Register for the incoming batch number.
            Messenger.Default.Register<BatchNumber>(this, Token.BatchNumber, batchNo => this.BatchNumber = batchNo);

            Messenger.Default.Register<BatchNumber>(this, Token.BatchNumberSelected, b =>
            {
                if (ApplicationSettings.DispatchMode != DispatchMode.Dispatch)
                {
                    this.BatchNumber = b;
                    this.scanAndWeighModeBatchNumber = b;

                    if (this.Sale != null)
                    {
                        this.CheckAttributeBatchChangeReset();
                    }

                    this.clearTraceability = false;
                    this.HandleSelectedSaleDetail();
                    this.clearTraceability = true;
                }
            });

            Messenger.Default.Register<GoodsReceiptData>(this, Token.BatchNumberSelected, b =>
            {
                this.prOrderId = b.PROrderId;
                if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                {
                    if (ApplicationSettings.CheckDispatchBatchAgainstProduct)
                    {
                        this.CheckBatchMatch(b);
                    }
                }
            });

            #endregion

            #region command

            this.OnPreviewMouseUpBatchCommand = new RelayCommand(this.OnPreviewMouseUpBatchCommandExecute);
            this.ShowNoteCommand = new RelayCommand(this.ShowNoteCommandExecute);

            this.ChangeDeliveryDateCommand = new RelayCommand(this.ChangeDeliveryDateCommandExecute);

            this.ChangePriceCommand = new RelayCommand(this.ChangePrice);

            this.OnPreviewMouseUpCommand = new RelayCommand(this.OnPreviewMouseUpCommandExecute);
            this.AddToPalletCommand = new RelayCommand(() => { this.AddToPalletCommandExecute(); });

            this.OnDispatchTransactionsLoadingCommand = new RelayCommand(() =>
            {
                this.EntitySelectionChange = true;
                this.GetTransactions();
                this.EntitySelectionChange = false;
            });

            this.OnDispatchTransactionsUnloadingCommand = new RelayCommand(() =>
            {
                this.StockDetails.Clear();
            });

            this.SwitchGridViewCommand = new RelayCommand(() =>
            {
                if (this.CenterDisplayViewModel == this.Locator.DispatchGrid)
                {
                    this.CenterDisplayViewModel = this.Locator.DispatchTransactions;
                }
                else if (this.CenterDisplayViewModel == this.Locator.DispatchTransactions)
                {
                    this.CenterDisplayViewModel = this.Locator.DispatchCount;
                }
                else
                {
                    this.CenterDisplayViewModel = this.Locator.DispatchGrid;
                }
            });

            // Handle the notes ui load.
            this.OnNotesLoadedCommand = new RelayCommand(this.GetDispatchContainers);

            // Handle the ui load event.
            this.OnARDispatchLoadingCommand = new RelayCommand(this.OnARDispatchLoadingCommandExecute);

            // Handle the ui load event.
            this.OnARDispatchUnloadingCommand = new RelayCommand(this.OnARDispatchUnloadingCommandExecute);

            // Handle the view switch.
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);

            // Handle the manual box labelling.
            this.BoxLabelCommand = new RelayCommand(this.BoxLabelCommandExecute);

            this.CreateBoxLabelCommand = new RelayCommand(this.CreateBoxLabelCommandExecute);

            // Handle the manual box labelling.
            this.ShippingLabelCommand = new RelayCommand(this.ShippingLabelCommandExecute);

            // Handle the manual box labelling.
            this.CreateContainerCommand = new RelayCommand(this.CreateContainerCommandExecute);

            // Handle the weight recording.
            //this.RecordWeightCommand = new RelayCommand(this.RecordWeight, () => ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight);

            #endregion

            this.AutoBoxLabels = new HashSet<int>();
            this.Locator.TouchscreenProductionOrders.AllOrders.Clear();
            this.CloseHiddenWindows();
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowDispatchAttributes;
            this.StockDetails = new ObservableCollection<StockDetail>();
            this.CenterDisplayViewModel = this.Locator.DispatchGrid;
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowDispatchAttributes;
            ViewModelLocator.ClearTouchscreenSuppliers();
            ViewModelLocator.ClearAPReceiptDetails();
            this.Locator.Touchscreen.OutOfProductionExtensionWidth = 0;
            this.SelectedSaleDetails = new ObservableCollection<SaleDetail>();
            this.allOrderDetails = new List<SaleDetail>();
            this.GetProductGroups();
            NouvemGlobal.CheckBatchValuesSet = false;
            this.CanAddBatch = this.RecordWeightModes.Contains(ApplicationSettings.DispatchMode);
            this.PartnerName = Strings.SelectCustomer;
            this.StockLabelsToPrint = 1;
            this.CanCreateOrder = this.AuthorisationsManager.AllowUserToCreateDispatches;
            this.CanAddProductToOrder = this.AuthorisationsManager.AllowUserToAddProductsToDispatches;

            if (ApplicationSettings.IgnoreTrimGroupsForConcessions)
            {
                this.GetTrimGroups();
            }

            this.Locator.Touchscreen.UIText = string.Empty;
            this.GetDispatchContainers();
            this.GetDateDays();
            this.ClearAttributes();
            this.ClearAttributeControls();
            this.CloseHiddenWindows();
            this.GetAttributeAllocationData();
            this.GetAttributeLookUps();
            //this.IndicatorManager.SetQtyBasedProductValues(true);
            var mode = ApplicationSettings.DispatchMode.ToString();
           
            this.Locator.ProcessSelection.SetDefaultProcesses(ApplicationSettings.DefaultDispatchModes, mode);
            ViewModelLocator.ClearARDispatchDetails();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the running pallet items count.
        /// </summary>
        public string PalletCount
        {
            get
            {
                return this.palletCount;
            }

            set
            {
                this.palletCount = value;
                this.RaisePropertyChanged();
                if (value != null && value == this.GetDefaultPalletCount())
                {
                    this.itemsToPalletise.Clear();
                }
            }
        }

        /// <summary>
        /// The selected production order data.
        /// </summary>
        private StockDetail plusManualGoodsData;

        /// <summary>
        /// Gets or sets the split product id.
        /// </summary>
        public int SplitProductId { get; set; }

        /// <summary>
        /// The grid/count center ui vm.
        /// </summary>
        public ViewModelBase CenterDisplayViewModel
        {
            get
            {
                return this.centerDisplayViewModel;
            }

            set
            {
                this.centerDisplayViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inmaster id of the selected product.
        /// </summary>
        public int SelectedINMasterID { get; set; }
     
        /// <summary>
        /// Gets or sets the container reference.
        /// </summary>
        public string DispatchContainerReference
        {
            get
            {
                return this.dispatchContainerReference;
            }

            set
            {
                this.dispatchContainerReference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the containers.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.DispatchContainer> DispatchContainers
        {
            get
            {
                return this.dispatchContainers;
            }

            set
            {
                this.dispatchContainers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a user can create an order.
        /// </summary>
        public bool CanCreateOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a user can add a product to an order.
        /// </summary>
        public bool CanAddProductToOrder { get; set; }
       
        /// <summary>
        /// Gets or sets the batch number/production spec data to display on the ui.
        /// </summary>
        public string BatchDataToDisplay
        {
            get
            {
                return this.batchDataToDisplay;
            }

            set
            {
                this.batchDataToDisplay = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch number/production text to display on the batch label.
        /// </summary>
        public string BatchLabelText
        {
            get
            {
                return this.batchLabelText;
            }

            set
            {
                this.batchLabelText = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets a value indicating whether we can add a production batch or not.
        /// </summary>
        public bool CanAddBatch
        {
            get
            {
                return this.canAddBatch;
            }

            set
            {
                this.canAddBatch = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the touchscreen sale details.
        /// </summary>
        public ObservableCollection<SaleDetail> SelectedSaleDetails
        {
            get
            {
                return this.selectedSaleDetails;
            }

            set
            {
                this.selectedSaleDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber
        {
            get
            {
                return this.batchNumber;
            }

            set
            {
                this.batchNumber = value;

                if (ApplicationSettings.DispatchMode == DispatchMode.Dispatch
                    || ApplicationSettings.DispatchMode == DispatchMode.DispatchBatch)
                {
                    if (value != null)
                    {
                        this.BatchDataToDisplay = value.Number;
                    }

                    Messenger.Default.Send(value, Token.DispatchBatchNumberSelected);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand OnPreviewMouseUpBatchCommand { get; set; }

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand ShowNoteCommand { get; set; }

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand ChangeDeliveryDateCommand { get; set; }

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand ChangePriceCommand { get; set; }

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand OnPreviewMouseUpCommand { get; set; }

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand AddToPalletCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the center grid view switch
        /// </summary>
        public ICommand SwitchGridViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event
        /// </summary>
        public ICommand OnNotesLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event
        /// </summary>
        public ICommand CreateContainerCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event
        /// </summary>
        public ICommand OnARDispatchUnloadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event
        /// </summary>
        public ICommand OnARDispatchLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the center grid view switch
        /// </summary>
        public ICommand OnDispatchTransactionsLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the center grid view switch
        /// </summary>
        public ICommand OnDispatchTransactionsUnloadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the manual shipping labelling.
        /// </summary>
        public ICommand ShippingLabelCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the manual box labelling.
        /// </summary>
        public ICommand BoxLabelCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the manual box labelling.
        /// </summary>
        public ICommand CreateBoxLabelCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a supplier group selection.
        /// </summary>
        public ICommand SupplierGroupSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to check for a grid product removal.
        /// </summary>
        public ICommand CheckForProductRemovalCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a sale detail selection.
        /// </summary>
        public ICommand SaleDetailSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the recording of a dispatch item weight.
        /// </summary>
        public ICommand RecordWeightCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Handles a pallet scan.
        /// </summary>
        /// <param name="palletId">The pallet id.</param>
        public void HandlePallet(int palletId)
        {
            var scanOn = this.ScannerStockMode != ScannerMode.MinusScan;
            if (scanOn && this.Sale.SaleID == 0)
            {
                this.AddDesktopSale();
            }

            if (this.DataManager.AddOrRemovePalletStock(palletId, this.Sale.SaleID, scanOn))
            {
                var localTemp = this.Temperature;
                this.GetLiveStatus();
                this.Temperature = localTemp;
            }
        }
       
        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        public void HandleSelectedOrder(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            this.ClearAttributes();
            this.ClearAttributeControls();
            StockDetail lastBatchAttribute = null;
            
            this.displayPopUpNote = true;
            this.highlightSelectedProduct = false;
            this.generateBatchNo = false;
            this.SelectedPartner = orderDetails.Item2;
            this.Sale = orderDetails.Item1;
            int? localBatchId = null;
            //if (this.BatchNumber != null)
            //{
            //    localBatchId = this.BatchNumber.BatchNumberID;
            //}
            //else if (this.Sale != null)
            //{
            //    localBatchId = this.Sale.BatchNumberID;
            //}

            //if (!localBatchId.IsNullOrZero())
            //{
            //    lastBatchAttribute = this.DataManager.GetLastBatchAttributeData(localBatchId.ToInt(),
            //        NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.TransactionTypeGoodsDeliveryId);
            //}

            //this.Sale.LastBatchAttribute = lastBatchAttribute;
            this.SelectedSaleDetail = null;
            this.OrderNo = this.Sale.Number;
            this.GetLiveStatus();
        }

        #endregion

        #endregion

        #region protected

        #region virtual
        
        /// <summary>
        /// Gets the default pallet value.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetDefaultPalletCount()
        {
            return Strings.NoPallet;
        }

        /// <summary>
        /// Adds the dispatch stock to a pallet.
        /// </summary>
        protected virtual void AddToPalletCommandExecute()
        {
            try
            {
                if (this.PalletCount.Equals(this.GetDefaultPalletCount()))
                {
                    this.PalletCount = "0";
                    return;
                }

                NouvemMessageBox.Show(string.Format("You are about to palletise these {0} items. Do you wish to proceed?", this.PalletCount), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPalletisation);
                this.Locator.ScannerPalletisation.PrintLabel = true;
                Messenger.Default.Send(this.itemsToPalletise, Token.PalletiseItems);
                this.PalletCount = this.GetDefaultPalletCount();
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        #endregion

        #region override

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleStockLabels(string data)
        {
            this.StockLabelsToPrint = data.ToInt();
        }

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        protected override void RemoveProductCommandExecute()
        {
            if (ApplicationSettings.DisallowProductRemovalAtDispatch)
            {
                return;
            }

            base.RemoveProductCommandExecute();
        }

        /// <summary>
        /// Updates the pallet item count.
        /// </summary>
        protected override void UpdatePalletCount(int id)
        {
            if (!this.PalletCount.IsNumeric())
            {
                return;
            }

            var count = this.PalletCount.ToInt() + 1;
            this.PalletCount = count.ToString();
            this.itemsToPalletise.Add(id);
        }

        /// <summary>
        /// Opens the notes screen.
        /// </summary>
        protected virtual void OpenNotesWindow()
        {
            if (!this.NotesScreenCreated)
            {
                this.NotesScreenCreated = true;
                Messenger.Default.Send(Token.Message, Token.ShowNotesWindow);
                Messenger.Default.Send(new CollectionData { DataContext = this }, Token.SetDataContext);
                return;
            }

            this.GetDispatchContainers();
            Messenger.Default.Send(false, Token.CloseNotesWindow);

            if (ApplicationSettings.Customer != Customer.Ballon)
            {
                Messenger.Default.Send(true, Token.DisplayNotesKeyboard);
            }
        }

        /// <summary>
        /// Sets the process flag (visibility/edit)
        /// </summary>
        /// <param name="data">The attribute allocation data.</param>
        protected override Tuple<bool, bool> SetProcessData(AttributeAllocationData data)
        {
            var visible = false;
            var editable = false;

            if (ApplicationSettings.DispatchMode == DispatchMode.Dispatch)
            {
                visible = data.AttributeVisibleProcesses != null &&
                              data.AttributeVisibleProcesses.Any(
                                  x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Dispatch));

                editable = data.AttributeEditProcesses != null &&
                               data.AttributeEditProcesses.Any(
                                   x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Dispatch));
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchBatch)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchBatch));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchBatch));
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchRecordWeight));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchRecordWeight));
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeightWithNotes)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchRecordWeightWithNotes));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchRecordWeightWithNotes));
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeightWithShipping)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchRecordWeightWithShipping));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.DispatchRecordWeightWithShipping));
            }

            return Tuple.Create(visible, editable);
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int GetWorkflowDocketId()
        {
            return this.Sale != null ? this.Sale.SaleID : 0;
        }

        /// <summary>
        /// Gets the current attribute product id.
        /// </summary>
        /// <returns>THe corresponding product id.</returns>
        protected override int GetWorkflowProductId()
        {
            return this.SelectedSaleDetail != null ? this.SelectedSaleDetail.INMasterID : 0;
        }

        /// <summary>
        /// Handle the selected stock detail parse/edit.
        /// </summary>
        /// <param name="detail">The detail to parse for edit.</param>
        protected override void HandleSelectedStockDetail(StockDetail detail)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            this.Locator.TransactionManager.GetDeviceOnly = true;
            this.Locator.TransactionManager.SetDeleteMode(TransactionManagerViewModel.DeleteMode.Dispatch);
            this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.Locator.TransactionManager.SetTransaction(detail.StockTransactionID.ToInt()); 
            }));
        }

        /// <summary>
        /// Clears the production batch data (In batch scanning mode for tsbloors)
        /// </summary>
        protected override void ClearBatchData()
        {
            // clear the batch data
            this.BatchNumber = null;
            this.BatchDataToDisplay = string.Empty;
            Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void DeleteTransaction(int stockId, string barcode = "")
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to delete dispatch label:{stockId}");
            var error = this.DataManager.DeleteDispatchStock(stockId, this.Sale.SaleID, NouvemGlobal.UserId.ToInt(),
                NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.OutOfProductionId, this.AutoSplitProductId.ToInt());
            if (string.IsNullOrEmpty(error))
            {
                this.Log.LogInfo(this.GetType(), "Label deleted");
                this.GetLiveStatus();
                this.SetOrderLinesStatus();
                SystemMessage.Write(MessageType.Priority, Message.StockRemoved);
                this.TransactionCount--;
            }
            else
            {
                this.Log.LogError(this.GetType(), $"Label deletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void UndeleteTransaction(int stockId)
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to undelete into prod label:{stockId}");
            var error = this.DataManager.UndeleteStock(stockId, NouvemGlobal.TransactionTypeGoodsDeliveryId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(), this.Sale.SaleID);

            if (string.IsNullOrEmpty(error))
            {
                this.Log.LogInfo(this.GetType(), "Label undeleted");
                this.GetLiveStatus();
                SystemMessage.Write(MessageType.Priority, Message.StockUndeleted);
                this.Log.LogInfo(this.GetType(), "Label deleted");
            }
            else
            {
                this.Log.LogError(this.GetType(), $"Label undeletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            if (ViewModelLocator.IsProductionOrdersLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.ProductionOrderSearch);
                return;
            }

            if (ViewModelLocator.IsTransactionManagerLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                return;
            }

            //data = "060617500006AL";
            data = data.Replace("\r", "");
            if (ApplicationSettings.ScanningForExternalBarcodeReadOnlyAtDispatch)
            {
                this.HandleScannerDataForBatchSelection(data);
                return;
            }

            data = this.CheckForSplitStock(data);
            data = this.RetrieveSerialNoFromBarcode(data);

            if (data.ToInt() > 500000000)
            {
                this.HandleScannerDataForTrayBatchSelection(data);
                return;
            }

            if (ApplicationSettings.RecallProductAtScan)
            {
                var localProduct = this.RetrieveProductFromEan13Barcode(data, ApplicationSettings.RecallProductAtScanStartPos, ApplicationSettings.RecallProductAtScanLength);
                if (localProduct == null)
                {
                    var productCode = data.Substring(ApplicationSettings.RecallProductAtScanStartPos, ApplicationSettings.RecallProductAtScanLength);
                    var msg = string.Format(Message.ProductMatchingCodeNotFound, productCode);
                    this.Log.LogError(this.GetType(), msg);
                    SystemMessage.Write(MessageType.Issue, msg);
                    NouvemMessageBox.Show(msg, touchScreen: true);
                    return;
                }

                if (this.SaleDetails == null)
                {
                    this.SaleDetails = new ObservableCollection<SaleDetail>();
                }

                var orderProduct = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localProduct.Master.INMasterID);
                if (orderProduct != null)
                {
                    this.SelectedSaleDetail = orderProduct;
                }
                else
                {
                    this.HandleProductSelection(localProduct);
                    if (this.SelectedSaleDetail != null)
                    {
                        this.SelectedSaleDetail.DontPrintLabel = true;
                    }
                }

                return;
            }

            if (ApplicationSettings.ScanningForScotBeefBarcodeReadAtDispatch)
            {
                //0100012241060741418052131020036
                // woolleys - check for a scotbeef barcode
                if (this.IsScotBeefBarcode(data))
                {
                    this.Log.LogDebug(this.GetType(), "YES");
                    // get the associated serial number.
                    data = this.DataManager.GetCarcassTransaction(data);
                }
                else
                {
                    data = this.HandleGs1Barcode(data);
                }
            }
            else
            {
                data = this.HandleGs1Barcode(data);
            }

            data = data.RemoveNonIntegers();

            this.Log.LogDebug(this.GetType(), string.Format("Touchscreen: HandleScannerData(): Data:{0}, Data length:{1}", data, data.Length));

            #region reprint

            if (this.ScannerStockMode == ScannerMode.Reprint)
            {
                var transaction = this.DataManager.GetStockTransaction(data.ToInt());

                if (transaction != null)
                {
                    try
                    {
                        this.PrintManager.ReprintLabel(transaction.StockTransactionID);
                    }
                    catch (Exception ex)
                    {
                        SystemMessage.Write(MessageType.Issue, ex.Message);
                        this.Log.LogError(this.GetType(), ex.Message);
                    }
                }

                return;
            }

            #endregion

            #region minus scan

            if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                #region validation

                if (this.Sale == null)
                {
                    return;
                }

                #endregion

                this.DeleteTransaction(data.ToInt());
                return;
            }

            #endregion

            try
            {
                #region validation

                if (ApplicationSettings.MustSelectContainerForDispatch && (this.SelectedDispatchContainer == null || this.SelectedDispatchContainer.DispatchContainerID == 0))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoDispatchContainerSelected);
                    NouvemMessageBox.Show(Message.NoDispatchContainerSelected, touchScreen: true);
                    return;
                }

                if (string.IsNullOrEmpty(data))
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.InvalidScannerRead, scanner: true);
                    }

                    SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);
                    return;
                }

                if (this.SelectedPartner == null)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.NoOrderHasBeenSelected, scanner: true);
                    }

                    SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                    return;
                }

                if (this.ScannerStockMode == ScannerMode.ReWeigh 
                    && ApplicationSettings.SelectingProductInReweighMode
                    && this.SelectedSaleDetail == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                    NouvemMessageBox.Show(Message.NoProductSelected, touchScreen: true);
                    return;
                }

                NouvemGlobal.SelectingAndWeighingProductAtDispatch = false;
                
                if (this.ScannerStockMode == ScannerMode.ReWeigh)
                {
                    this.DoNotResetQty = true;
                }

                if (this.AutoBoxing && this.ScannerStockMode == ScannerMode.ReWeigh)
                {
                    if (this.AutoBoxLabels.Contains(data.ToInt()))
                    {
                        NouvemMessageBox.Show($"{data} has already been scanned", touchScreen: true);
                        return;
                    }
                }

                this.DoNotHandleSelectedSaleDetail = true;
                this.SelectedSaleDetail = new SaleDetail();
                this.DoNotHandleSelectedSaleDetail = false;
                this.DoNotResetQty = false;

                #endregion

                if (ApplicationSettings.AutoCarcassSplitReweighingAtDispatch)
                {
                    if (this.DataManager.IsCarcassSplitAnimal(data.Trim()))
                    {
                        this.ScannerStockMode = ScannerMode.Split;
                    }
                }
      
                if (this.SelectedSaleDetail != null)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        this.HandleSerialEntry(data.ToInt());
                    });
                }
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Handles a serial dispatch entry.
        /// </summary>
        /// <param name="serial">The entered serial.</param>
        protected override void HandleSerialEntry(int serial)
        {
            this.Locator.Indicator.DoNotResetTare = false;
            StockDetail stockData = null;
            #region reweigh 
            if (this.ScannerStockMode == ScannerMode.ReWeigh && !ApplicationSettings.ScanningExternalCarcass)
            {
                this.Log.LogDebug(this.GetType(), "ReweighMode");
                stockData = this.DataManager.GetTransactionDataForLinkedCarcassSerial(serial, !ApplicationSettings.SelectingProductInReweighMode);

                if (!string.IsNullOrEmpty(stockData.Error))
                {
                    NouvemMessageBox.Show(stockData.Error, touchScreen: true);
                    return;
                }

                if (ApplicationSettings.SelectingProductInReweighMode)
                {
                    this.DoNotResetQty = true;
                    var localDetailID = this.SelectedINMasterID;

                    if (localDetailID > 0)
                    {
                        var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(
                              x => x.Master.INMasterID == localDetailID);
                        if (localProduct != null)
                        {
                            if (this.Locator.Touchscreen.Quantity <= 0)
                            {
                                this.Locator.Touchscreen.Quantity = 1;
                            }

                            if (ApplicationSettings.EnforceAutoBoxing && !this.AutoBoxing)
                            {
                                var lambGroup = stockData.StockAttribute.AgeInMonths.ToInt();
                                if (lambGroup == ApplicationSettings.AutoBoxProductGroups)
                                {
                                    NouvemMessageBox.Show($"You must scan traceability labels for lamb products. Please enter a value in 'Labels to Scan'", touchScreen: true);
                                    return;
                                }
                            }
                       
                            stockData.ProductTare = (localProduct.PiecesTareWeight * this.Locator.Touchscreen.Quantity.ToDouble()) + localProduct.BoxTareWeight;
                            stockData.Transaction.INMasterID = localProduct.Master.INMasterID;

                            if (ApplicationSettings.CheckWeightBoundaryAtCarcassDispatch)
                            {
                                var localQty = this.ProductQuantity < 1 ? 1 : this.ProductQuantity.ToDecimal();
                                var minWgt = localProduct.MinWeight.ToDecimal();
                                var maxWgt = localProduct.MaxWeight.ToDecimal();

                                if ((minWgt != 0 && this.Locator.Indicator.Weight < (minWgt * localQty))
                                    || (maxWgt != 0 && this.Locator.Indicator.Weight > (maxWgt * localQty)))
                                {
                                    var localMessage = string.Format(Message.OutOfBoundsWeight, (minWgt * localQty), (maxWgt * localQty));
                                    SystemMessage.Write(MessageType.Issue, localMessage);
                                    NouvemMessageBox.Show(localMessage, touchScreen: true);
                                    return;
                                }
                            }
                        }
                    }
                }

                double tare = 0;
                if (!this.Locator.Indicator.ManualWeight)
                {
                    this.Locator.Indicator.Tare = stockData.ProductTare;
                    this.Locator.Indicator.DoNotResetTare = true;
                    tare = stockData.ProductTare;
                    System.Threading.Thread.Sleep(200);
                }

                try
                {
                    if (ApplicationSettings.PromptForReweighAtDispatch)
                    {
                        NouvemMessageBox.Show(Message.ReweighItemPrompt, NouvemMessageBoxButtons.YesNo,
                            touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (!this.Locator.Indicator.ManualWeight)
                            {
                                var saveWeightResult =
                                    this.IndicatorManager.SaveWeight(ApplicationSettings.AutoWeightAtDispatch);
                                if (saveWeightResult == string.Empty)
                                {
                                    var wgt = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                                    stockData.Transaction.TransactionWeight = wgt;
                                }
                                else
                                {
                                    Messenger.Default.Send(Token.Message, Token.WeightError);
                                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                                    NouvemMessageBox.Show(
                                        string.Format("{0} {1}", saveWeightResult, Message.RescanItem),
                                        touchScreen: true);
                                    return;
                                }
                            }
                            else
                            {
                                stockData.Transaction.TransactionWeight =
                                    this.Locator.Indicator.Weight;
                                this.Locator.Indicator.DoNotResetTare = false;
                            }
                        }
                    }
                    else
                    {
                        decimal wgt = 0m;
                        decimal qty = 1;
                        if (!this.Locator.Indicator.ManualWeight && (!this.AutoBoxing || this.SelectedSaleDetail.CompleteAutoBoxing))
                        {
                            var saveWeightResult =
                               this.IndicatorManager.SaveWeight(ApplicationSettings.AutoWeightAtDispatch);

                            if (saveWeightResult == string.Empty)
                            {
                                wgt = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                                qty = this.Locator.Touchscreen.Quantity.ToDecimal();
                                stockData.Transaction.TransactionWeight = wgt;
                                stockData.Transaction.TransactionQTY = qty;
                                stockData.Transaction.Tare = tare.ToDecimal();
                                stockData.Transaction.GrossWeight = tare.ToDecimal() + wgt;
                            }
                            else
                            {
                                Messenger.Default.Send(Token.Message, Token.WeightError);
                                SystemMessage.Write(MessageType.Issue, saveWeightResult);
                                NouvemMessageBox.Show(string.Format("{0} {1}", saveWeightResult, Message.RescanItem),
                                    touchScreen: true);
                                return;
                            }
                        }
                        else
                        {
                            wgt = this.Locator.Indicator.Weight;
                            qty = this.Locator.Touchscreen.Quantity.ToDecimal();
                            stockData.Transaction.TransactionWeight = wgt;
                            stockData.Transaction.TransactionQTY = qty;
                            this.Locator.Indicator.DoNotResetTare = false;
                        }

                        var batch = string.Empty;
                        var killdate = string.Empty;
                        var killNo = string.Empty;
                        var attributes = stockData.StockAttribute;
                        if (attributes != null)
                        {
                            batch = attributes.BatchNumber;
                            if (attributes.KillDate.HasValue)
                            {
                                killdate = attributes.KillDate.ToString();
                            }

                            if (attributes.KillNumber.HasValue)
                            {
                                killNo = attributes.KillNumber.ToString();
                                stockData.Transaction.Comments = killNo;
                            }
                        }

                        var message = string.Format("Label:{0}{1}Weight recorded:{2} kg{3}Tare:{4} kg{5}Kill number:{6}{7}Batch number:{8}{9}Kill date:{10}"
                                                    , serial,
                                                    Environment.NewLine,
                                                    wgt,
                                                    Environment.NewLine,
                                                    tare,
                                                    Environment.NewLine,
                                                    killNo,
                                                    Environment.NewLine,
                                                    batch,
                                                    Environment.NewLine, killdate);

                        this.Log.LogDebug(this.GetType(), string.Format("3. {0}", message));

                        if (!this.AutoBoxing || this.SelectedSaleDetail.CompleteAutoBoxing)
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                                //SystemMessage.Write(MessageType.Priority, message);
                            }));
                        }
                    }
                }
                finally
                {
                    this.Locator.Indicator.ManualWeight = false;
                    this.IndicatorManager.OpenIndicatorPort();
                }
            }
            #endregion
            #region reweigh external
            else if (this.ScannerStockMode == ScannerMode.ReWeigh && ApplicationSettings.ScanningExternalCarcass)
            {
                this.Log.LogDebug(this.GetType(), "+CarcassMode");
                // TODO This is a temporary measure for dunleavys. Take out when kill line in
                stockData = this.DataManager.GetTransactionDataForLinkedCarcassSerial(serial);

                if (!string.IsNullOrEmpty(stockData.Error))
                {
                    if (!ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(stockData.Error, touchScreen: ApplicationSettings.TouchScreenMode, scanner: ApplicationSettings.ScannerMode);
                    }

                    return;
                }

                var wgt = stockData.Transaction.TransactionWeight;
                var batch = string.Empty;
                var killdate = string.Empty;
                var killNo = string.Empty;

                var attributes = stockData.StockAttribute;
                if (attributes != null)
                {
                    batch = attributes.BatchNumber;
                    if (attributes.KillDate.HasValue)
                    {
                        killdate = attributes.KillDate.ToString();
                    }

                    if (attributes.KillNumber.HasValue)
                    {
                        killNo = attributes.KillNumber.ToString();
                    }
                }

                var message = string.Format("Label:{0}{1}Weight recorded:{2} kg{3}Tare:{4} kg{5}Kill number:{6}{7}Batch number:{8}{9}Kill date:{10}"
                                            , serial,
                                            Environment.NewLine,
                                            wgt,
                                            Environment.NewLine,
                                            0,
                                            Environment.NewLine,
                                            killNo,
                                            Environment.NewLine,
                                            batch,
                                            Environment.NewLine, killdate);

                this.Log.LogDebug(this.GetType(), string.Format("3. {0}", message));

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (!ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }
                }));
            }
            #endregion
            #region split
            else if (this.ScannerStockMode == ScannerMode.Split)
            {
                stockData = this.DataManager.GetTransactionDataForLinkedCarcassSerialSplit(serial);
                try
                {
                    if (!string.IsNullOrEmpty(stockData.Error))
                    {
                        NouvemMessageBox.Show(stockData.Error, touchScreen: true);
                        return;
                    }

                    double tare = 0;
                    if (!this.Locator.Indicator.ManualWeight)
                    {
                        this.Locator.Indicator.Tare = stockData.ProductTare;
                        this.Locator.Indicator.DoNotResetTare = true;
                        tare = stockData.ProductTare;
                        System.Threading.Thread.Sleep(200);
                    }

                    var scannedData = this.DataManager.GetScannedCarcassSplitStock(serial.ToString(), false, 7);
                    var split = new CarcassSplitView(scannedData, ViewType.ARDispatch);
                    split.ShowDialog();
                    split.Focus();

                    if (this.SplitProductId == 0)
                    {
                        NouvemMessageBox.Show(Message.NoProductSelectedCancellation, touchScreen: true);
                        return;
                    }

                    decimal wgt = 0m;
                    if (!this.Locator.Indicator.ManualWeight)
                    {
                        var saveWeightResult =
                           this.IndicatorManager.SaveWeight(ApplicationSettings.AutoWeightAtDispatch);

                        if (saveWeightResult == string.Empty)
                        {
                            wgt = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                            stockData.Transaction.TransactionWeight = wgt;
                            stockData.Transaction.Tare = tare.ToDecimal();
                            stockData.Transaction.GrossWeight = tare.ToDecimal() + wgt;
                            stockData.Transaction.INMasterID = this.SplitProductId;
                        }
                        else
                        {
                            Messenger.Default.Send(Token.Message, Token.WeightError);
                            SystemMessage.Write(MessageType.Issue, saveWeightResult);
                            NouvemMessageBox.Show(string.Format("{0} {1}", saveWeightResult, Message.RescanItem),
                                touchScreen: true);
                            return;
                        }
                    }
                    else
                    {
                        wgt = this.Locator.Indicator.Weight;
                        stockData.Transaction.TransactionWeight = wgt;
                        stockData.Transaction.GrossWeight = wgt;
                        stockData.Transaction.INMasterID = this.SplitProductId;
                        this.Locator.Indicator.DoNotResetTare = false;
                    }

                    var batch = string.Empty;
                    var killdate = string.Empty;
                    var killNo = string.Empty;
                    var attributes = stockData.StockAttribute;
                    if (attributes != null)
                    {
                        batch = attributes.BatchNumber;
                        if (attributes.KillDate.HasValue)
                        {
                            killdate = attributes.KillDate.ToString();
                        }

                        if (attributes.KillNumber.HasValue)
                        {
                            killNo = attributes.KillNumber.ToString();
                        }
                    }

                    var message = string.Format("Label:{0}{1}Weight recorded:{2} kg{3}Tare:{4} kg{5}Kill number:{6}{7}Batch number:{8}{9}Kill date:{10}"
                                                , serial,
                                                Environment.NewLine,
                                                wgt,
                                                Environment.NewLine,
                                                tare,
                                                Environment.NewLine,
                                                killNo,
                                                Environment.NewLine,
                                                batch,
                                                Environment.NewLine, killdate);

                    this.Log.LogDebug(this.GetType(), string.Format("3. {0}", message));

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }));
                }
                finally
                {
                    this.Locator.Indicator.ManualWeight = false;
                    this.IndicatorManager.OpenIndicatorPort();
                    if (ApplicationSettings.AutoCarcassSplitReweighingAtDispatch)
                    {
                        this.ScannerStockMode = ScannerMode.Scan;
                    }
                }

            }
            #endregion
            #region scan plus weigh
            else if (this.ScannerStockMode == ScannerMode.ScanPlusManualWeigh)
            //(serial < ApplicationSettings.LinkedDatabaseScannedDispatchSerialIdentity && serial > ApplicationSettings.LinkedDatabaseScannedDispatchSerialStart))
            {
                this.Log.LogDebug(this.GetType(), "+ManualMode");
                // TODO This is a temporary measure for Wooleys. Take out when kill line in
                stockData = this.DataManager.GetTransactionDataForLinkedCarcassSerial(serial);

                if (!string.IsNullOrEmpty(stockData.Error))
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(stockData.Error, scanner: true);
                        return;
                    }
                    else
                    {
                        NouvemMessageBox.Show(stockData.Error, touchScreen: true);
                        return;
                    }
                }

                if (this.ScannerStockMode == ScannerMode.ScanPlusManualWeigh)
                {
                    stockData.Transaction.Serial = serial;
                    stockData.Transaction.ManualWeight = true;
                    stockData.Transaction.Tare = 0;
                    this.plusManualGoodsData = stockData;
                    Keypad.Show(KeypadTarget.PlusManualWeight, ApplicationSettings.ScannerMode, setTopmost: true);
                    return;
                }
            }
            #endregion
            #region +mode
            else
            {
                this.Log.LogDebug(this.GetType(), "+Mode");
                if (ApplicationSettings.UseNewScanFunctionalityAtDispatch && this.Sale != null)
                {
                    this.AddStockToDispatch(serial, false);
                    return;
                }

                stockData = this.DataManager.GetTransactionDataForSerial(serial, useAttribute:ApplicationSettings.RetrieveAttributeOnDispatchScan, splitStockId:this.AutoSplitProductId);

                if (ApplicationSettings.ScanningForIntakeBatchesAtDispatch && stockData.Transaction != null)
                {
                    if (this.GetStockMode(stockData.Transaction.INMasterID) > 1)
                    {
                        this.HandleScannerDataForIntakeBatchSelection(stockData.Transaction.INMasterID, stockData.Transaction.BatchNumberID.ToInt());
                        return;
                    }
                }
            }
            #endregion

            if (stockData.PalletId > 0)
            {
                this.HandlePallet(stockData.PalletId);
                return;
            }

            #region error
            if (!string.IsNullOrEmpty(stockData.Error))
            {
                var message = string.Empty;
                var alreadydispatched = false;
                if (stockData.Error.Equals(Constant.NotFound))
                {
                    message = string.Format(Message.SerialNumberNotFound, serial);
                }
                else
                {
                    message = string.Format(Message.SerialNumberAlreadyDispatched, serial, stockData.Error);
                    alreadydispatched = true;

                    if (this.Sale.Number.ToString() != stockData.Error)
                    {
                        var localMessage = string.Format("{0}.{1}", message, Message.AddToOrder);
                        if (ApplicationSettings.ScannerMode)
                        {
                            NouvemMessageBox.Show(localMessage, NouvemMessageBoxButtons.YesNo, scanner: true);
                        }
                        else
                        {
                            NouvemMessageBox.Show(localMessage, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        }

                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.DataManager.UnConsumeTransaction(stockData.StockTransactionIdToUnconsume))
                            {
                                this.HandleSerialEntry(serial);
                                return;
                            }
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Priority, Message.TransactionCancelled);
                            return;
                        }
                    }
                }

                //if (ApplicationSettings.ScannerMode)
                //{
                //    if (alreadydispatched)
                //    {
                //        if (this.Sale.SaleID.ToString() != stockData.Error)
                //        {
                //            // not on the current order, so display message.
                //            NouvemMessageBox.Show(message, scanner: true);
                //        }
                //    }
                //    else
                //    {
                //        NouvemMessageBox.Show(message, scanner: true);
                //    }
                //}

                SystemMessage.Write(MessageType.Issue, message);
                return;
            }

            #endregion

            #region validation
            if (!ApplicationSettings.AllowDispatchProductNotOnOrder)
            {
                if (!this.IsDispatchItemOnOrder(stockData.Transaction.INMasterID))
                {
                    var productName = string.Empty;
                    if (this.SelectedSaleDetail.InventoryItem != null)
                    {
                        productName = this.SelectedSaleDetail.InventoryItem.Master.Name;
                    }
                    else
                    {
                        var trans = stockData.Transaction;
                        if (trans != null)
                        {
                            var productId = trans.INMasterID;
                            var product =
                                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == productId);

                            if (product != null)
                            {
                                productName = product.Name;
                            }
                        }
                    }

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.ProductNotOnOrder, productName), scanner: true);
                    }

                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductNotOnOrder, productName));
                    NouvemMessageBox.Show(string.Format(Message.ProductNotOnOrder, productName), touchScreen: true);
                    return;
                }
            }

            #endregion

            this.indicatorWeight = stockData.Transaction.TransactionWeight.ToDecimal();
            this.ProductQuantity = stockData.Transaction.TransactionQTY.ToDecimal();
            this.SelectedSaleDetail.INMasterID = this.AutoSplitProductId.HasValue ? this.AutoSplitProductId.ToInt() : stockData.Transaction.INMasterID;
            this.SelectedSaleDetail.ScannedStock = true;
            this.SelectedSaleDetail.IsBox = stockData.Transaction.IsBox;
            //this.SelectedSaleDetail.OrderBatchID = stockData.Transaction.BatchNumberID;

            //var enforedLines = this.SaleDetails.Where(x => x.Enforce.ToBool() && x.INMasterID == this.SelectedSaleDetail.INMasterID).ToList();
            //if (enforedLines.Any())
            //{
            //    var localEnforcedDetail = enforedLines.FirstOrDefault(x => x.INMasterID == this.SelectedSaleDetail.INMasterID && x.OrderBatchID == this.SelectedSaleDetail.OrderBatchID);
            //    if (localEnforcedDetail == null)
            //    {
            //        var msg = string.Format(Message.NonAllowableStockBatch,
            //            this.GetProductName(this.SelectedSaleDetail.INMasterID), serial,
            //            this.DataManager.GetPROrderReference(stockData.Transaction.MasterTableID.ToInt()));
            //        SystemMessage.Write(MessageType.Issue,msg);
            //        NouvemMessageBox.Show(msg, touchScreen:true);
            //        return;
            //    }
            //}

            this.SetSaleDetail(this.SelectedSaleDetail);

            this.SelectedSaleDetail.ManualEntryComplete = true;
            if ((this.ScannerStockMode == ScannerMode.Scan
                 || this.ScannerStockMode == ScannerMode.ReWeigh
                 || this.ScannerStockMode == ScannerMode.ScanPlusManualWeigh
                 || this.ScannerStockMode == ScannerMode.Split)
             )
            {
                this.SetUseBy(stockData.UseBy);
                this.RecordSerialDispatch(stockData);
                if (this.AutoBoxing)
                {
                    this.AutoBoxLabels.Add(serial);
                }
            }
        }

        /// <summary>
        /// Handles a serial dispatch entry.
        /// </summary>
        /// <param name="incomingWeight">The entered weight.</param>
        private void HandlePlusManualWeight(string incomingWeight)
        {
            if (string.IsNullOrEmpty(incomingWeight))
            {
                return;
            }

            var weight = incomingWeight.ToDecimal();

            var localModule = ApplicationSettings.ScannerMode
                    ? this.Locator.ScannerMainDispatch
                    : this.Locator.ARDispatchTouchscreen;

            var stockData = this.plusManualGoodsData;
            stockData.Transaction.TransactionWeight = weight;
            stockData.Transaction.GrossWeight = weight;

            var batch = string.Empty;
            var killdate = string.Empty;
            var killNo = string.Empty;
            var attributes = stockData.StockAttribute;
            if (attributes != null)
            {
                batch = attributes.BatchNumber;
                if (attributes.KillDate.HasValue)
                {
                    killdate = attributes.KillDate.ToString();
                }

                if (attributes.KillNumber.HasValue)
                {
                    killNo = attributes.KillNumber.ToString();
                }
            }

            var serial = stockData.Transaction.Serial;
            var message = string.Format("Label:{0}{1}Weight recorded:{2} kg{3}Tare:{4} kg{5}Kill number:{6}{7}Batch number:{8}{9}Kill date:{10}"
                                        , serial,
                                        Environment.NewLine,
                                        weight,
                                        Environment.NewLine,
                                        0,
                                        Environment.NewLine,
                                        killNo,
                                        Environment.NewLine,
                                        batch,
                                        Environment.NewLine, killdate);

            this.Log.LogDebug(this.GetType(), string.Format("3. {0}", message));

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
            }));

            if (ApplicationSettings.TouchScreenMode)
            {
                if (localModule.DoesSaleHaveBaseDocument)
                {
                    if (!localModule.IsDispatchItemOnOrder(stockData.Transaction.INMasterID))
                    {
                        if (!ApplicationSettings.AllowDispatchProductNotOnOrder)
                        {
                            var productName = string.Empty;
                            if (this.SelectedSaleDetail.InventoryItem != null)
                            {
                                productName = this.SelectedSaleDetail.InventoryItem.Master.Name;
                            }
                            else
                            {
                                var trans = stockData;
                                if (trans != null)
                                {
                                    var productId = trans.Transaction.INMasterID;
                                    var product =
                                       NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == productId);

                                    if (product != null)
                                    {
                                        productName = product.Name;
                                    }
                                }
                            }

                            if (ApplicationSettings.ScannerMode)
                            {
                                NouvemMessageBox.Show(string.Format(Message.ProductNotOnOrder, productName), scanner: true);
                            }

                            SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductNotOnOrder, productName));
                            return;
                        }
                    }
                }
            }
            else
            {
                if (this.Locator.ARDispatch.DoesSaleHaveBaseDocument)
                {
                    if (!this.Locator.ARDispatch.IsDispatchItemOnOrder(stockData.Transaction.INMasterID))
                    {
                        if (!ApplicationSettings.AllowDispatchProductNotOnOrder)
                        {
                            var productName = string.Empty;
                            if (this.SelectedSaleDetail.InventoryItem != null)
                            {
                                productName = this.SelectedSaleDetail.InventoryItem.Master.Name;
                            }
                            else
                            {
                                var trans = stockData;
                                if (trans != null)
                                {
                                    var productId = trans.Transaction.INMasterID;
                                    var product =
                                       NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == productId);

                                    if (product != null)
                                    {
                                        productName = product.Name;
                                    }
                                }
                            }

                            SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductNotOnOrder, productName));
                            return;
                        }
                    }
                }
            }
            
            this.SelectedSaleDetail.INMasterID = stockData.Transaction.INMasterID;
            if (ApplicationSettings.TouchScreenMode)
            {
                localModule.SetSaleDetail(this.SelectedSaleDetail);
            }
            else
            {
                this.Locator.ARDispatch.SetSaleDetail(this.SelectedSaleDetail);
            }

            this.SelectedSaleDetail.ManualEntryComplete = true;
            this.RecordSerialDispatch(null);
        }


        /// <summary>
        /// Override the sale selection.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.SelectedDispatchContainer = null;
                return;
            }

            #endregion

            #region parse sale

            this.AutoBoxCount = 0;
            this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.Sale.NouDocStatusID);
            this.ValidUntilDate = this.Sale.QuoteValidDate;
            this.DeliveryDate = this.Sale.DeliveryDate;
            this.ShippingDate = this.Sale.ShippingDate;
            this.DocumentDate = this.Sale.DocumentDate;
            this.Temperature = !string.IsNullOrEmpty(this.Sale.OtherReference)
                ? this.Sale.OtherReference.ToDecimal()
                : (decimal?)null;
            this.DiscountPercentage = this.Sale.DiscountPercentage.ToDecimal();
            this.Remarks = this.Sale.Remarks;
            this.SelectedSalesEmployee = this.Sale.SalesEmployeeID != null ?
                this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == this.Sale.SalesEmployeeID) : null;
            this.SubTotal = this.Sale.SubTotalExVAT.ToDecimal();
            this.Tax = this.Sale.VAT.ToDecimal();
            this.Discount = this.Sale.DiscountIncVAT.ToDecimal();
            this.Total = this.Sale.GrandTotalIncVAT.ToDecimal();
            this.DocketNote = this.Sale.DocketNote;
            this.PopUpNote = this.Sale.PopUpNote;
            this.InvoiceNote = this.Sale.InvoiceNote;
            this.SelectedDeliveryAddress = this.Sale.DeliveryAddress != null && this.Sale.DeliveryAddress.Details != null ?
                this.Addresses.FirstOrDefault(
                x => x.Details.BPAddressID == this.Sale.DeliveryAddress.Details.BPAddressID) : null;
            this.SelectedInvoiceAddress = this.Sale.InvoiceAddress != null && this.Sale.InvoiceAddress.Details != null ?
                this.Addresses.FirstOrDefault(
                x => x.Details.BPAddressID == this.Sale.InvoiceAddress.Details.BPAddressID) : null;
            this.CustomerPOReference = this.Sale.CustomerPOReference;
            this.Locator.Touchscreen.UIText = this.Sale.Number.ToString();
            if (this.Sale.DispatchContainerID.HasValue)
            {
                this.EntitySelectionChange = true;
                this.SelectedDispatchContainer =
                    this.DispatchContainers.FirstOrDefault(x => x.DispatchContainerID == this.Sale.DispatchContainerID);
                this.EntitySelectionChange = false;
            }
            else
            {
                this.SelectedDispatchContainer = null;
            }

            #endregion

            #region parse sale details

            foreach (var saleDetail in this.Sale.SaleDetails)
            {
                if (saleDetail.InventoryItem == null)
                {
                    saleDetail.SetInventoryItem();
                }

                if (saleDetail.WeightOrdered == null)
                {
                    saleDetail.WeightOrdered = 0;
                }

                if (saleDetail.QuantityOrdered == null)
                {
                    saleDetail.QuantityOrdered = 0;
                }

                if (saleDetail.WeightReceived == null)
                {
                    saleDetail.WeightReceived = 0;
                }

                if (saleDetail.QuantityReceived == null)
                {
                    saleDetail.QuantityReceived = 0;
                }
            }

            // Add the Sale details.
            this.SaleDetails = new ObservableCollection<SaleDetail>(this.Sale.SaleDetails);
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SetOrderLinesStatus();
            }));

            #endregion

            if (!string.IsNullOrWhiteSpace(this.PopUpNote) && this.displayPopUpNote)
            {
                this.OrderPopUpNote = this.PopUpNote;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(this.PopUpNote, scanner: true);
                    }
                }));
            }

            if (!ApplicationSettings.ScannerMode)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
                    {
                        NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp:true);
                        //this.CustomerPopUpNote = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(this.OrderPopUpNote))
                    {
                        NouvemMessageBox.Show(this.OrderPopUpNote, isPopUp: true);
                        this.OrderPopUpNote = string.Empty;
                    }
                }));
            }

            if (this.centerDisplayViewModel == this.Locator.DispatchTransactions)
            {
                this.GetTransactions();
            }

            //this.SetWeightData();

            //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //{
            //    this.GetLiveStatus();
            //}));
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int? GetReferenceId()
        {
            return this.BatchNumber != null ? this.BatchNumber.BatchNumberID : 0; 
        }

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                this.ScannerStockMode = ScannerMode.Reprint;
            }
            else if (this.ScannerStockMode == ScannerMode.Reprint)
            {
                this.ScannerStockMode = ScannerMode.ReWeigh;
            }
            else if (this.ScannerStockMode == ScannerMode.ReWeigh)
            {
                this.ScannerStockMode = ScannerMode.Split;
            }
            else if (this.ScannerStockMode == ScannerMode.Split)
            {
                this.ScannerStockMode = ScannerMode.ScanPlusManualWeigh;
            }
            else if (this.ScannerStockMode == ScannerMode.ScanPlusManualWeigh)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }

        /// <summary>
        /// Override, to send the order item to the traceability module, and the container to the indicator vm.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            if (this.SelectedSaleDetail == null)
            {
                this.Locator.Indicator.Tare = 0;
                return;
            }

            this.SelectedINMasterID = this.SelectedSaleDetail.INMasterID;
            if (this.SelectedSaleDetail.InventoryItem != null &&
                this.SelectedSaleDetail.InventoryItem.PiecesTareContainer != null)
            {
                this.Locator.Indicator.Tare = this.SelectedSaleDetail.InventoryItem.PiecesTareContainer.Tare;
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
            {
                if (!this.highlightSelectedProduct)
                {
                    // clear the batch data
                    this.BatchNumber = null;
                    this.BatchDataToDisplay = string.Empty;
                    Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
                }

                this.highlightSelectedProduct = false;

                if (this.SelectedSaleDetail.InventoryItem != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        try
                        {
                            if (this.SelectedSaleDetail != null)
                            {
                                int? productId = this.SelectedSaleDetail.InventoryItem.Master.INMasterID;
                                int? productGroupId = this.SelectedSaleDetail.InventoryItem.Master.INGroupID;
                                int? partnerId = null;
                                int? partnerGroupId = null;

                                if (this.SelectedPartner != null)
                                {
                                    partnerId = this.SelectedPartner.BPMasterID;
                                    partnerGroupId = this.SelectedPartner.BPGroupID;
                                }

                                this.LabelManager.GetLabelAndDisplay(partnerId, partnerGroupId, productId, productGroupId, LabelType.Item);
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                        }

                    }), DispatcherPriority.Background);
                }
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;

            Messenger.Default.Send(this.SelectedSaleDetail, Token.SaleDetailSelected);

            if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null && this.SelectedSaleDetail.InventoryItem.BoxTareContainer != null)
            {
                Messenger.Default.Send(this.SelectedSaleDetail.InventoryItem.BoxTareContainer, Token.ContainerSelected);
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight 
                || ApplicationSettings.DispatchMode == DispatchMode.DispatchBatch)
            {
                if (this.clearTraceability)
                {
                    this.BatchNumber = null;
                    this.BatchDataToDisplay = string.Empty;
                    Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
                }

                //if (this.SelectedSaleDetail.OrderBatchID.HasValue)
                //{
                //    this.HandleScannerDataForIntakeBatchSelection(this.SelectedSaleDetail.INMasterID, this.SelectedSaleDetail.OrderBatchID.ToInt());
                //}
            }

            if (ApplicationSettings.ClearBatchOnDispatchProductChange)
            {
                this.BatchNumber = null;
                this.BatchDataToDisplay = string.Empty;
            }

            if (this.SelectedSaleDetail.Batch == null)
            {
                this.SelectedSaleDetail.Batch = this.BatchNumber;
            }

            this.CheckAttributeProductChangeReset(this.SelectedSaleDetail);
            this.HandleSaleDetail(this.SelectedSaleDetail, this);
            if (this.SelectedSaleDetail != null && !string.IsNullOrWhiteSpace(this.SelectedSaleDetail.Notes))
            {
                var notes = this.SelectedSaleDetail.Notes;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(notes, isPopUp: true);
                }));
            }
        }

        #endregion

        #endregion

        #region private 

        #region command execution

        private void OnPreviewMouseUpBatchCommandExecute()
        {
            if (this.SelectedSaleDetail == null || string.IsNullOrEmpty(this.SelectedSaleDetail.OrderBatchNumber))
            {
                return;
            }

            var lineData = this.DataManager.GetOrderBatchData(this.SelectedSaleDetail.AROrderDetailID.ToInt());
            var msg = $"Batches To Use:{Environment.NewLine}";
            foreach (var data in lineData)
            {
                msg += $"{data.Reference}{Environment.NewLine}";
            }

            NouvemMessageBox.Show(msg,isPopUp:true);
        }

        /// <summary>
        /// Displays the partner note.
        /// </summary>
        private void ShowNoteCommandExecute()
        {
            if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                });
            }

            if (this.Sale != null && !string.IsNullOrWhiteSpace(this.Sale.PopUpNote))
            {
                System.Threading.Thread.Sleep(200);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    NouvemMessageBox.Show(this.Sale.PopUpNote, isPopUp: true);
                });
            }
        }

        /// <summary>
        /// Brings up the delivery date selector, allowing user to change date.
        /// </summary>
        private void ChangeDeliveryDateCommandExecute()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            if (!this.AuthorisationsManager.AllowUserToChangeDeliveryDateOnDispatchOrderAtTouchscreen)
            {
                SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToChangeDeliveryDate);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.NotAuthorisedToChangeDeliveryDate, flashMessage: true);
                }));

                return;
            }

            #endregion

            Messenger.Default.Send("Dispatch", Token.CreateCalender);
        }

        private void OnPreviewMouseUpCommandExecute()
        {
            var productId = this.SelectedSaleDetail?.INMasterID;
            var partnerId = this.SelectedPartner?.BPMasterID;
            if (!ApplicationSettings.ShowSpecsAtDispatch 
                || this.SelectedSaleDetail == null 
                || this.SelectedPartner == null 
                || this.SelectedSaleDetail.SpecDisplayed)
            {
                return;
            }

            this.SelectedSaleDetail.SpecDisplayed = true;
            var localSpec = this.DataManager.GetsProductSpecificationByPartner(productId.ToInt(), partnerId.ToInt());
            if (localSpec != null && localSpec.INMasterSpecificationID > 0)
            {
                Messenger.Default.Send(Token.Message, Token.CreateSpecs);
                Messenger.Default.Send(localSpec, Token.DrillDownToSpec);
            }
        }

        /// <summary>
        /// Handle the ui load event.
        /// </summary>
        private void OnARDispatchLoadingCommandExecute()
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.IsBusy = false;
            }));

            this.PalletCount = this.GetDefaultPalletCount();
            this.SetActiveDocument(this);
            this.Locator.FactoryScreen.ModuleName = Strings.Dispatch;
            ViewModelLocator.ClearAPReceiptTouchscreen();
            this.generateBatchNo = true;
            if (this.ScannerStockMode != ScannerMode.ReWeigh)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }

            if (ApplicationSettings.StartDispatchInReweighMode)
            {
                this.ScannerStockMode = ScannerMode.ReWeigh;
            }
      
            //this.clearTraceability = true;
            this.OpenScanner();
            Messenger.Default.Send(false, Token.DisableLegacyAttributesView);
            //if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
            //{
            //    NouvemMessageBox.Show(this.CustomerPopUpNote, touchScreen:true);
            //    this.CustomerPopUpNote = string.Empty;
            //}

            //if (!string.IsNullOrWhiteSpace(this.OrderPopUpNote))
            //{
            //    NouvemMessageBox.Show(this.OrderPopUpNote, touchScreen: true);
            //    this.OrderPopUpNote = string.Empty;
            //}
        }

        /// <summary>
        /// Handle the parent touchscreen load.
        /// </summary>
        public void TouchscreenLoaded()
        {
            //this.RefreshLocalDocNumberings();
            this.SetActiveDocument(this);
            this.GetPriceLists();
        }

        /// <summary>
        /// Handle the ui load event.
        /// </summary>
        private void OnARDispatchUnloadingCommandExecute()
        {
        }

        /// <summary>
        /// Create a dispatch container.
        /// </summary>
        private void CreateContainerCommandExecute()
        {
            if (string.IsNullOrWhiteSpace(this.DispatchContainerReference))
            {
                //SystemMessage.Write(MessageType.Issue, Message.NoDispatchContainerReferenceEntered);
                //NouvemMessageBox.Show(Message.NoDispatchContainerReferenceEntered, touchScreen:true);
                Messenger.Default.Send(true, Token.DisplayNotesContainerKeyboard);
                return;
            }

            var dispatchContainer = new Model.DataLayer.DispatchContainer
            {
                Reference = this.dispatchContainerReference,
                CreationDate = DateTime.Now,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID
            };

            if (this.DataManager.AddDispatchContainer(dispatchContainer))
            {
                SystemMessage.Write(MessageType.Priority, Message.DispatchContainerCreated);
                this.RefreshDispatchContainers();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DispatchContainerNotCreated);
            }

            Messenger.Default.Send(false, Token.DisplayNotesKeyboard);
            this.DispatchContainerReference = string.Empty;
        }

        /// <summary>
        /// Prints a shipping label.
        /// </summary>
        private void ShippingLabelCommandExecute()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                NouvemMessageBox.Show(Message.NoPartnerSelected, touchScreen: true);
                return;
            }
            
            #endregion

            // use the first sale line as the master table id in order to pull back the dispatch data for the label.
            int? orderId = null;
            if (this.Sale != null && this.SaleDetails != null && this.SaleDetails.Any())
            {
                var firstId = this.SaleDetails.First().SaleDetailID;
                if (firstId > 0)
                {
                    orderId = firstId;
                }
            }

            // fetch the traceability values
            var stockTransaction = new StockTransaction();
            stockTransaction.TransactionWeight = 0;
            stockTransaction.GrossWeight = 0;
            stockTransaction.Tare = 0;
            stockTransaction.MasterTableID = orderId;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = 0;
            stockTransaction.INMasterID = NouvemGlobal.InventoryItems.First().Master.INMasterID;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId;
            stockTransaction.WarehouseID = NouvemGlobal.WarehouseIntakeId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;
            stockTransaction.BPMasterID = this.SelectedPartner.BPMasterID;
            stockTransaction.Deleted = DateTime.Now;
            stockTransaction.Consumed = DateTime.Now;

            var id = this.DataManager.AddTransaction(stockTransaction);
            if (id > 0)
            {
                this.Print(id, LabelType.Shipping);
            }
        }

        /// <summary>
        /// Handle the manual box label print.
        /// </summary>
        private void BoxLabelCommandExecute()
        {
            if (this.Locator.TouchscreenDispatchPanel.BoxOrPriceText == Strings.BoxLabel)
            {
                this.PrintBoxLabelAnyQty();
                return;
            }

            this.ChangePrice();
        }


        /// <summary>
        /// Handle the manual box label print.
        /// </summary>
        private void CreateBoxLabelCommandExecute()
        {
            this.PrintBoxLabelAnyQty();
        }

        /// <summary>
        /// Calls the keypad to change the line price.
        /// </summary>
        private void ChangePrice()
        {
            if (this.Sale == null)
            {
                return;
            }

            var status = this.DataManager.GetARDispatchOrderStatus(this.Sale.SaleID);
            if (status.HasValue && status == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                SystemMessage.Write(MessageType.Issue, Message.DispatchDocketCompletedAtAnotherTerminal);
                NouvemMessageBox.Show(Message.DispatchDocketCompletedAtAnotherTerminal, touchScreen: true);
                return;
            }

            if (this.SelectedSaleDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderLineSelected);
                NouvemMessageBox.Show(Message.NoOrderLineSelected, touchScreen: true);
                return;
            }

            Keypad.Show(KeypadTarget.ChangePriceAtDispatch);
        }

        /// <summary>
        /// Changes the line price.
        /// </summary>
        private void ChangePriceAtTouchscreenDispatch(string changedPrice)
        {
            if (string.IsNullOrEmpty(changedPrice))
            {
                return;
            }

            var price = changedPrice.ToDecimal();
            this.SelectedSaleDetail.UnitPrice = price;

            try
            {
                var addingToCustomerBook = false;
                var message = Message.AddUnitPriceChangeToPriceBookConfirmationTouchscreen;
                if (this.SelectedSaleDetail.PriceListID != this.SelectedPartner.PriceListID)
                {
                    addingToCustomerBook = true;
                    message = Message.AddProductAndUnitPriceChangeToPriceBookConfirmationTouchscreen;
                }

                NouvemMessageBox.Show(
                    string.Format(message, ApplicationSettings.CustomerCurrency, this.SelectedSaleDetail.UnitPrice), NouvemMessageBoxButtons.YesNo, touchScreen:true);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    if (this.SelectedPartner.PriceListID == 0)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoPriceBookExists);
                        NouvemMessageBox.Show(Message.NoPriceBookExists, touchScreen:true);
                        return;
                    }

                    //if (this.SelectedSaleDetail.PriceListID == 0)
                    //{
                    //    NouvemMessageBox.Show(Message.ProductNotOnAnyPriceList, NouvemMessageBoxButtons.YesNo, touchScreen:true);
                    //    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    //    {
                    //        Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
                    //        this.Close();
                    //    }

                    //    return;
                    //}

                    this.UpdatePriceToPriceBook();
                    System.Threading.Tasks.Task.Factory.StartNew(this.RefreshPriceLists);
                    if (addingToCustomerBook)
                    {
                        this.SelectedSaleDetail.LoadingSale = true;
                        this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                    }
                }

                this.SelectedSaleDetail.UnitPriceAfterDiscount = this.SelectedSaleDetail.UnitPrice;
                this.UpdateSaleDetailTotals();
                if (this.SelectedSaleDetail.SaleDetailID > 0)
                {
                    this.UpdateOrderPrice();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Updates the price on the order line.
        /// </summary>
        protected void UpdateOrderPrice()
        {
            this.Sale.VAT = this.Tax;
            this.Sale.SubTotalExVAT = this.SubTotal;
            this.Sale.DiscountIncVAT = this.Discount;
            this.Sale.GrandTotalIncVAT = this.Total;
            this.Sale.DiscountPercentage = this.DiscountPercentage;
            this.Sale.Remarks = this.Remarks;
            this.Sale.CustomerPOReference = this.CustomerPOReference;
            this.Sale.DeliveryDate = this.DeliveryDate;
            this.Sale.ShippingDate = this.ShippingDate;
            this.Sale.DocumentDate = this.DocumentDate;

            if (this.SelectedDeliveryAddress != null)
            {
                this.Sale.DeliveryContact = this.SelectedDeliveryContact;
            }

            if (this.SelectedContact != null)
            {
                this.Sale.MainContact = this.SelectedContact;
            }

            if (this.SelectedDeliveryAddress != null)
            {
                this.Sale.DeliveryAddress = this.SelectedDeliveryAddress;
            }

            if (this.SelectedInvoiceAddress != null)
            {
                this.Sale.InvoiceAddress = this.SelectedInvoiceAddress;
            }

            if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusFilled.NouDocStatusID
                || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID)
            {
                if (this.DataManager.ChangeARDispatchStatus(this.Sale, this.SelectedDocStatus.NouDocStatusID))
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.Sale.Number));
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.OrderNotUpdated);
                }
            }
        }

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private async void SwitchViewCommandExecute(string view)
        {
            Messenger.Default.Send(Token.Message, Token.CloseMessageBoxWindow);
            this.IsBusy = true;
            await this.SwitchView(view);
            this.IsBusy = false;
        }

        #endregion

        #region helper

        private void AddStockToDispatch(int serial, bool ignoreIfOnOtherOrder)
        {
            var processID = this.Locator.ProcessSelection.SelectedProcess != null
                ? this.Locator.ProcessSelection.SelectedProcess.ProcessID
                : 0;
            var result = this.DataManager.AddStockToDispatch(serial, this.Sale.SaleID,
                this.AuthorisationsManager.AllowUserToAddProductsToDispatches, this.AuthorisationsManager.AllowUserToDispatchOverOrderAmounts,
                ignoreIfOnOtherOrder, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.DispatchId, processID, this.SelectedPartner.BPMasterID);

            var stockId = result.Item1;
            var msg = result.Item2;
            var data = result.Item3;
            var newOrderId = result.Item4;
            var newOrderNo = result.Item5;
            if (this.Sale.SaleID == 0)
            {
                this.Sale.SaleID = newOrderId.ToInt();
                this.Sale.Number = newOrderNo.ToInt();
                this.Locator.Touchscreen.UIText = this.Sale.Number.ToString();
            }

            if (msg.CompareIgnoringCaseAndWhitespace("Pallet"))
            {
                this.HandlePallet(serial);
                return;
            }

            if (msg.StartsWithIgnoringCase("On Order"))
            {
                var orderNo = msg.RemoveNonIntegers().ToInt();
                var localMsg = string.Format(Message.ItemOnOrderPrompt, orderNo);
                NouvemMessageBox.Show(localMsg, NouvemMessageBoxButtons.YesNo, scanner: ApplicationSettings.ScannerMode,
                    touchScreen: ApplicationSettings.TouchScreenMode);

                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    this.AddStockToDispatch(serial, true);
                    return;
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, Message.TransactionCancelled);
                    return;
                }
            }

            if (!string.IsNullOrEmpty(msg))
            {
                NouvemMessageBox.Show(msg, scanner: ApplicationSettings.ScannerMode, touchScreen: ApplicationSettings.TouchScreenMode);
                return;
            }

            this.TransactionCount++;
            this.UpdatePalletCount(stockId.ToInt());

            if (data != null && data.Count > 0)
            {
                this.GetLiveStatus(data);
                this.SetOrderLinesStatus();
            }
        }

        private Task SwitchView(string view)
        {
            return
                Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(150);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (view.Equals(Constant.Suppliers))
                        {
                            if (!ApplicationSettings.DisallowCustomerSelectionAtDispatch)
                            {
                                //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenSuppliers;
                                this.Locator.TouchscreenSuppliers.MasterModule = ViewType.ARDispatch;
                                if (!this.PartnersScreenCreated)
                                {
                                    this.PartnersScreenCreated = true;
                                    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                                    //this.Locator.TouchscreenOrders.OnEntry();
                                    return;
                                }

                                //Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplayPartnerSales);
                                Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                                this.Locator.TouchscreenSuppliers.OnEntry();
                            }

                            return;
                        }

                        if (view.Equals(Constant.Products))
                        {
                            if (ApplicationSettings.MustSelectContainerForDispatch &&
                                (this.SelectedDispatchContainer == null ||
                                 this.SelectedDispatchContainer.DispatchContainerID == 0))
                            {
                                SystemMessage.Write(MessageType.Issue, Message.NoDispatchContainerSelected);
                                NouvemMessageBox.Show(Message.NoDispatchContainerSelected, touchScreen: true);
                                return;
                            }

                            if (!this.CanAddProductToOrder)
                            {
                                NouvemMessageBox.Show(Message.AddProductToOrderPermissionDenied, touchScreen: true);
                                return;
                            }

                            this.Locator.TouchscreenProducts.Module = ViewType.ARDispatch;
                            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenProducts;
                            return;
                        }

                        if (view.Equals(Constant.ScannerOrders))
                        {
                            Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen),
                                Token.DisplaySupplierSales);
                            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerOrders;
                            return;
                        }

                        if (view.Equals(Constant.ScannerCustomers))
                        {
                            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerCustomers;
                            return;
                        }

                        if (view.Equals(Constant.Production))
                        {
                            if (this.Sale == null)
                            {
                                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                                return;
                            }

                            this.ShowBatches();
                            return;
                        }

                        if (view.Equals(Constant.Notes))
                        {
                            this.OpenNotesWindow();
                            return;
                        }

                        if (ApplicationSettings.ShowRoutesAtTouchscreenDispatch)
                        {
                            // show the routes first
                            var collectionData = (from route in NouvemGlobal.Routes
                                select new CollectionData
                                {
                                    ID = route.RouteID,
                                    Identifier = Constant.Route,
                                    Item1 = route.Name,
                                    Item2 = route.Description,
                                    Item3 = route.Remark,
                                    Item4 = route.RunNumber.ToInt().ToString()
                                }).ToList();

                            foreach (var data in collectionData)
                            {
                                data.SetDisplayData();
                            }

                            Messenger.Default.Send(collectionData, Token.DisplayTouchscreenCollection);
                            Messenger.Default.Send(ViewType.RoutesDisplay);
                            return;
                        }

                        this.ShowOrders();
                    });
                });
        }

        #region record weight

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordWeight()
        {
            this.Log.LogInfo(this.GetType(), "RecordWeight");
            //if (this.DisableProcessing)
            //{
            //    this.Log.LogInfo(this.GetType(), "Disabled");
            //    return;
            //}

            try
            {
                //this.DisableProcessing = true;
                this.AutoBoxCount = 0;
                this.printLabel = true;
                this.recordingWeightFromScales = true;
                this.IndicatorManager.OpenIndicatorPort();

                if (ApplicationSettings.DispatchMode != DispatchMode.DispatchRecordWeight 
                    && ApplicationSettings.DispatchMode != DispatchMode.DispatchBatch)
                {
                    this.BatchNumber = this.scanAndWeighModeBatchNumber;
                }

                #region validation

                if (this.DisableModule)
                {
                    SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                    return;
                }

                if (ApplicationSettings.MustSelectContainerForDispatch
                    && (this.SelectedDispatchContainer == null || this.SelectedDispatchContainer.DispatchContainerID == 0))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoDispatchContainerSelected);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(Message.NoDispatchContainerSelected, flashMessage: true);
                    });
                
                    return;
                }

                if (this.SelectedPartner == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(Message.NoPartnerSelected, flashMessage: true);
                    });
             
                    return;
                }

                if (this.SelectedSaleDetail == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(Message.NoProductSelected, flashMessage: true);
                    });
               
                    return;
                }

                if (this.BatchNumber == null || this.BatchNumber.BatchNumberID == 0)
                {
                    if (!this.SelectedSaleDetail.IsProductStockMode)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.NoProductionBatchSelected, flashMessage: true);
                        });

                        return;
                    }
                    else
                    {
                        this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Dispatch);
                    }
                }

                if (this.RecordWeightModes.Contains(ApplicationSettings.DispatchMode))
                {
                    if (string.IsNullOrEmpty(this.BatchDataToDisplay))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.NoProductionBatchSelected, flashMessage: true);
                        });
                      
                        return;
                    }
                }

                if (ApplicationSettings.CheckWeightBoundaryAtDispatch
                    && this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
                {
                    var localQty = this.ProductQuantity < 1 ? 1 : this.ProductQuantity.ToDecimal();
                    var minWgt = this.SelectedSaleDetail.InventoryItem.MinWeight.ToDecimal();
                    var maxWgt = this.SelectedSaleDetail.InventoryItem.MaxWeight.ToDecimal();

                    if ((minWgt != 0 && this.Locator.Indicator.Weight < (minWgt * localQty))
                        || (maxWgt != 0 && this.Locator.Indicator.Weight > (maxWgt * localQty)))
                    {
                        var message = string.Format(Message.OutOfBoundsWeight, (minWgt * localQty), (maxWgt * localQty));
                        SystemMessage.Write(MessageType.Issue, message);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(message, flashMessage: true);
                        });
                 
                        return;
                    }
                }

                #endregion

                #region attributes validation

                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(message, flashMessage: true);
                    });
                
                    return;
                }

                this.CheckForUnsavedAttributes();

                #endregion

                #region weight validation

                var weightRequired = true;
                if (this.SelectedSaleDetail != null)
                {
                    if (this.SelectedSaleDetail.PriceMethod == PriceMethod.Quantity)
                    {
                        weightRequired = false;
                    }
                }

                this.IndicatorManager.SetQtyBasedProductValues(!weightRequired);

                if (!this.manualWeight)
                {
                    this.Log.LogInfo(this.GetType(), "...");
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.cleanRead = true;
                        this.manualWeight = false;
                    }
                    else 
                    {
                        if (this.SelectedSaleDetail != null &&
                            this.SelectedSaleDetail.StockMode != StockMode.BatchQty &&
                            this.SelectedSaleDetail.StockMode != StockMode.ProductQty)
                        {
                            if (saveWeightResult.Equals(Message.ScalesInMotion))
                            {
                                this.scalesMotionTimer.Start();
                                return;
                            }
                            else
                            {
                                Messenger.Default.Send(Token.Message, Token.WeightError);
                                SystemMessage.Write(MessageType.Issue, saveWeightResult);
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    NouvemMessageBox.Show(saveWeightResult, flashMessage: true);
                                });

                                return;
                            }
                        }
                        else
                        {
                            this.cleanRead = true;
                            this.manualWeight = false;
                        }
                    }
                }

                #endregion

                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.Batch = this.BatchNumber;
                }

                var stockDetail = this.CreateTransaction();
                stockDetail.BatchNumberID = this.SelectedSaleDetail?.Batch?.BatchNumberID;
                this.CreateAttribute(stockDetail);
                this.SelectedSaleDetail.StockDetails.Add(stockDetail);
                this.SelectedSaleDetail.StockDetailToProcess = stockDetail;
                this.SelectedSaleDetail.Batch = this.BatchNumber;

                var wgt = this.indicatorWeight;
                var qty = this.ProductQuantity < 1 ? 1 : this.ProductQuantity;
                
                this.HandleDispatchData(this.SelectedSaleDetail, wgt.ToDecimal(), qty);
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }
            finally
            {
                this.printLabel = false;
                this.DisableProcessing = false;
                this.recordingWeightFromScales = false;
            }
        }

        #endregion

        #region scan stock

        ///// <summary>
        ///// Records a scanned serial dispatch.
        ///// </summary>
        //public void RecordSerialDispatch()
        //{
        //    this.recordingWeightFromScales = false;
        //    if (this.SelectedSaleDetail == null)
        //    {
        //        return;
        //    }

        //    if (this.SelectedSaleDetail.IsBoxStock)
        //    {
        //        // box stock, so no traceability data to retrieve.
        //        this.RecordSerialDispatchComplete(Tuple.Create(Constant.BoxStock, new List<TraceabilityResult>()));
        //        return;
        //    }

        //}

        /// <summary>
        /// Handles the scanned stock item.
        /// </summary>
        /// <param name="trans">The item scanned.</param>
        private void RecordSerialDispatch(StockDetail detail)
        {
            try
            {
                int splitId = 0;
                if (this.AutoSplitProductId.HasValue)
                {
                    if (!detail.SplitID.HasValue)
                    {
                        this.indicatorWeight = Math.Round(this.indicatorWeight * .98m / 2, 2);
                    }
               
                    this.SelectedSaleDetail.INMasterID = this.AutoSplitProductId.ToInt();
                    splitId = ApplicationSettings.HindProduct == this.AutoSplitProductId
                        ? ApplicationSettings.ForeProduct
                        : ApplicationSettings.HindProduct;
                }

                var trans = detail.Transaction;
                var wgt = this.indicatorWeight;
                var qty = this.ProductQuantity < 1 ? 1 : this.ProductQuantity;

                if (this.SelectedSaleDetail.PriceByTypicalWeight)
                {
                    qty = 1;

                    //if (this.SelectedSaleDetail.IsBoxStock)
                    //{
                    //    qty = detail.BoxItemsCount;
                    //}
                }

                this.SelectedSaleDetail.Batch = this.BatchNumber;

                var stockDetail = this.CreateTransaction();
                stockDetail.AttributeID = trans.AttributeID;
                stockDetail.AutoSplittingStock = this.AutoSplitProductId.HasValue && !detail.SplitID.HasValue;
                stockDetail.AutoSplittingStockId = splitId;
                stockDetail.BaseWarehouseID = detail.BaseWarehouseID;
                stockDetail.BaseMasterTableID = detail.BaseMasterTableID;

                if (detail.ReprintLabelAtDispatch)
                {
                    this.SetAttributeValues(detail);
                    this.CheckAttributeScanChangeReset();
                    this.HandleSaleDetail(this.SelectedSaleDetail, this);
                    this.CreateAttribute(stockDetail);
                    stockDetail.AttributeID = null;
                    this.printLabel = true;
                }

                stockDetail.BatchNumberID = trans.BatchNumberID;
                stockDetail.Serial = trans.Serial;
                stockDetail.StoredLabelId = trans.StoredLabelID;
                stockDetail.StockTransactionIdToConsume = trans.StockTransactionID;
                if (this.scanAndWeighModeBatchNumber != null && !detail.ScanningProductionStock)
                {
                    this.SelectedSaleDetail.Batch = this.scanAndWeighModeBatchNumber;
                    stockDetail.BatchNumberID = this.scanAndWeighModeBatchNumber.BatchNumberID;
                }

                this.SelectedSaleDetail.StockDetails.Add(stockDetail);
                this.SelectedSaleDetail.StockDetailToProcess = stockDetail;

                this.HandleDispatchData(this.SelectedSaleDetail, wgt.ToDecimal(), qty);
            }
            finally
            {
                this.printLabel = false;
                this.AutoSplitProductId = null;
            }
        }

        #endregion

        /// <summary>
        /// Shows the dispatch order selection screen.
        /// </summary>
        private void ShowOrders(int? routeId = null)
        {
            this.Locator.TouchscreenOrders.RouteId = routeId;
            Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen),
                Token.DisplaySupplierSales);

            if (!this.OrdersScreenCreated)
            {
                this.OrdersScreenCreated = true;
                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                this.Locator.TouchscreenOrders.OnEntry();
                return;
            }

            Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
            this.Locator.TouchscreenOrders.OnEntry();
        }

        /// <summary>
        /// Displays the batches.
        /// </summary>
        private void ShowBatches()
        {
            // tsbloors manual batch selection
            this.scanningLabelForBatchNo = false;

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchBatch)
            {
                if (this.SelectedSaleDetail == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoProductLineSelected);
                    });

                    return;
                }

                // serial products must be scanned
                if (this.SelectedSaleDetail.NouStockMode != null && this.SelectedSaleDetail.NouStockMode.Name.CompareIgnoringCase("Serial"))
                {
                    string msg;
                    if (!ApplicationSettings.AllowBatchSelectionForSerialStockAtDispatch)
                    {
                        msg = string.Format(Message.MustScanSerialStock,
                            this.GetProductName(this.SelectedSaleDetail.INMasterID));
                        SystemMessage.Write(MessageType.Issue, msg);
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(msg, touchScreen: true);
                        }));

                        return;
                    }

                    msg = string.Format(Message.SerialBatchSelectionPrompt,
                        this.GetProductName(this.SelectedSaleDetail.INMasterID));

                    NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, touchScreen:true);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }

                    this.Locator.TouchscreenProductionOrders.SetModule(ViewType.ARDispatch);
                    this.Locator.TouchscreenProductionOrders.GetAllOrders();
                    this.Locator.TouchscreenProductionOrders.OnEntry();
                    if (!this.ProductionOrdersScreenCreated)
                    {
                        this.ProductionOrdersScreenCreated = true;
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                        return;
                    }

                    Messenger.Default.Send(false, Token.CloseTouchscreenProductionOrders);
                    return;
                }

                this.Locator.TouchscreenIntakeBatches.NouOrderMethod =
                    this.SelectedSaleDetail.NouOrderMethod;
                this.Locator.TouchscreenIntakeBatches.GetAllOrders(this.SelectedSaleDetail.INMasterID, orderBatchDataID:this.SelectedSaleDetail.OrderBatchID);
                this.Locator.TouchscreenIntakeBatches.OnEntry();
                //if (!this.IntakeBatchesScreenCreated)
                //{
                //    this.IntakeBatchesScreenCreated = true;
                   Messenger.Default.Send(Token.Message, Token.CreateTouchscreenIntakeBatches);
                //    return;
                //}

                //Messenger.Default.Send(false, Token.CloseTouchscreenIntakeBatches);
                return;
            }

            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.ARDispatch);
            this.Locator.TouchscreenProductionOrders.GetAllOrders();
            this.Locator.TouchscreenProductionOrders.OnEntry();
            if (!this.ProductionOrdersScreenCreated)
            {
                this.ProductionOrdersScreenCreated = true;
                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                return;
            }

            Messenger.Default.Send(false, Token.CloseTouchscreenProductionOrders);
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        private StockDetail CreateTransaction()
        {
            this.Log.LogDebug(this.GetType(), "CreatingTransaction(): Creating transaction");
            return new StockDetail
            {
                TransactionWeight = this.indicatorWeight,
                Tare = this.tare,
                ManualWeight = this.manualWeight,
                TransactionQty = this.ProductQuantity.ToDecimal(),
                Pieces = this.ProductQuantity.ToInt(),
                INMasterID = this.SelectedSaleDetail.INMasterID,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId,
                WarehouseID = NouvemGlobal.DispatchId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = this.SelectedSaleDetail?.IsBox,
                ProcessID = this.Locator.ProcessSelection.SelectedProcess != null ? this.Locator.ProcessSelection.SelectedProcess.ProcessID : (int?)null,
                BatchNumberID = this.SelectedSaleDetail != null ? this.SelectedSaleDetail.BatchNumberId : null
            };
        }

        /// <summary>
        /// Gets the order transactions.
        /// </summary>
        private void GetTransactions()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion
          
            this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetStockTransactions(this.Sale.SaleID).OrderByDescending(x => x.TransactionDate));
            this.SelectedStockDetail = null;
        }

        /// <summary>
        /// Gets the containers.
        /// </summary>
        private void GetDispatchContainers()
        {
            this.DispatchContainers = 
                new ObservableCollection<Model.BusinessObject.DispatchContainer>(
                    this.DataManager.GetDispatchContainers(NouvemGlobal.NouDocStatusActive.NouDocStatusID));
        }

        /// <summary>
        /// Determines if the barcode is a scotbeef barcode.
        /// </summary>
        /// <param name="data">The scanned data.</param>
        /// <returns>Flag, as to whether it's a ascotbeef barcode or not.</returns>
        private string IsScotBeefProductionBarcode(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("IsScotBeefProductionBarcode:{0}", data));
            if (data.Length >= 30 && data.StartsWith("01"))
            {
                return data.Substring(data.Length - 8, 8);
            }

            return string.Empty;
        }

        /// <summary>
        /// Refreshes the containers.
        /// </summary>
        private void RefreshDispatchContainers()
        {
            this.GetDispatchContainers();
            if (this.Sale != null && this.Sale.DispatchContainerID.HasValue)
            {
                this.SelectedDispatchContainer =
                    this.DispatchContainers.FirstOrDefault(x => x.DispatchContainerID == this.Sale.DocumentNumberingID);
            }
            else
            {
                this.SelectedDispatchContainer = null;
            }
        }

        /// <summary>
        /// Handles the batch scan.
        /// </summary>
        /// <param name="data">The label scanned data.</param>
        private void HandleScannerDataForBatchSelection(string data)
        {
            //data = "J03130417";
            this.Log.LogDebug(this.GetType(), string.Format("Touchscreen: HandleScannerDataForBatchSelection(): Data:{0}, Data length:{1}", data, data.Length));
            data = data.Replace("-", "").Replace("#", "").Replace(" ","");
            if (data.StartsWithIgnoringCase("dm"))
            {
                data = data.Substring(2, data.Length - 2);
            }

            #region validation

            if (string.IsNullOrEmpty(data))
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);
                return;
            }

            if (this.SelectedSaleDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderLineSelected);
                return;
            }

            #endregion

            this.scanningLabelForBatchNo = true;
            var orders = this.DataManager.GetProductionOrders(this.Locator.TouchscreenProductionOrders.AllOrders);

            IList<ProductionData> localBatches;
            if (data.IsNumericSequence())
            {
                var prOrder = this.DataManager.GetStockTransactionIncDeleted(data.ToInt());
                localBatches = orders.Where(
                   x => x.Order.PROrderID == prOrder.MasterTableID).ToList();
            }
            else
            {
                // changeover multivac barcodes (going forward multivac barcodes will replicate jointing/slicing Nouvem barcode)
                localBatches = orders.Where(
                   x => !string.IsNullOrWhiteSpace(x.Reference) && x.Reference.ContainsIgnoringCase(data)).ToList();
            }

            if (!localBatches.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.BatchNotFound, data));
                NouvemMessageBox.Show(Message.BatchNotFound, touchScreen: true);
                return;
            }

            if (localBatches.Count > 1)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(string.Format(Message.SimiliarBatchesFound, localBatches.Count, data), touchScreen: true);
                }));

                return;
            }

            this.Locator.TouchscreenProductionOrders.SelectedOrder = localBatches.First();
            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.OutOfProduction);
            this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();

            //var localBatch = localBatches.First();
            //var goodsData = this.DataManager.GetProductionOrderBatchData(localBatch.BatchNumber);

            //this.BatchDataToDisplay = localBatch.Spec;
            //if (this.BatchNumber != null)
            //{
            //    this.BatchDataToDisplay = string.Format("{0}/{1}", localBatch.Spec, this.BatchNumber.Number);
            //}

            //this.Locator.ARDispatchDetails.SetProductionBatch(goodsData);

            //this.CheckBatchMatch(goodsData);
            //Messenger.Default.Send(localBatch.Spec, Token.SpecSelected);
        }

        /// <summary>
        /// Handles the batch scan.
        /// </summary>
        /// <param name="data">The label scanned data.</param>
        private void HandleScannerDataForTrayBatchSelection(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Touchscreen: HandleScannerDataForTrayBatchSelection(): Data:{0}, Data length:{1}", data, data.Length));

            var prOrderID = this.DataManager.GetPROrderNumber(data.ToInt());
            if (prOrderID == null)
            {
                return;
            }

            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.ARDispatch);
            this.Locator.TouchscreenProductionOrders.GetAllOrders();
            this.Locator.TouchscreenProductionOrders.SelectedOrder
                =
                this.Locator.TouchscreenProductionOrders.AllOrders.FirstOrDefault(
                    x => x.Order.PROrderID == prOrderID);

            if (this.Locator.TouchscreenProductionOrders.SelectedOrder != null)
            {
                this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
            }
        }

        /// <summary>
        /// Handles the batch scan.
        /// </summary>
        /// <param name="data">The label scanned data.</param>
        private void HandleScannerDataForIntakeBatchSelection(int productId, int batchId)
        {
            this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == productId);
            if (this.SelectedSaleDetail == null)
            {
                var msg = string.Format(Message.WrongBatchProductScanned,this.GetProductName(productId));
                SystemMessage.Write(MessageType.Issue,msg);
                NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, touchScreen:true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                var localProduct =
                    NouvemGlobal.InventoryItems.FirstOrDefault(x => x.INMasterID == productId);
                if (localProduct == null)
                {
                    return;
                }

                this.HandleProductSelection(localProduct);
                this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == productId);
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.Locator.TouchscreenIntakeBatches.GetAllOrdersNoThread(productId);
                this.Locator.TouchscreenIntakeBatches.SelectedOrder
                    =
                    this.Locator.TouchscreenIntakeBatches.Orders.FirstOrDefault(
                        x => x.BatchID == batchId);

                if (this.Locator.TouchscreenIntakeBatches.SelectedOrder != null)
                {
                    this.Locator.TouchscreenIntakeBatches.HandleSelectedOrder();
                }
                else
                {
                    ViewModelLocator.ClearTouchscreenIntakeBatches();
                }
            }));
        }

        /// <summary>
        /// Creates a new order.
        /// </summary>
        /// <param name="partner">The supplier we are creating an order for.</param>
        private void CreateNewOrder(ViewBusinessPartner partner)
        {
            if (!this.CanCreateOrder)
            {
                NouvemMessageBox.Show(Message.CreateOrderPermissionsDenied, touchScreen:true);
                return;
            }

            this.SelectedPartner = partner;
            this.SaleDetails.Clear();
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void HandleProductSelection(InventoryItem product)
        {
            if (!this.CanAddProductToOrder)
            {
                if (!ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatch || (ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatch && product != null &&
                    !product.Name.ContainsIgnoringCase(Strings.Box)))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.AddProductToOrderPermissionDenied, touchScreen: true);
                    }));

                    return;
                }
            }

            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }

            if (!this.CanCreateOrder && (this.SaleDetails != null && !this.SaleDetails.Any()))
            {
                NouvemMessageBox.Show(Message.CreateOrderPermissionsDenied, touchScreen: true);
                return;
            }

            var localSaleDetail = new SaleDetail
            {
                InventoryItem = product,
                INMasterID = product.Master.INMasterID,
                NouStockMode = product.StockMode,
                WeightOrdered = 0.00m,
                WeightDelivered = 0.00m,
                QuantityOrdered = 0.00m,
                QuantityDelivered = 0.00m,
                WeightOutstanding = 0.00m,
                QuantityOutstanding = 0.00m
            };

            if (localSaleDetail.NouStockMode == null)
            {
                localSaleDetail.SetNouStockMode();
            }

            NouvemGlobal.SelectingAndWeighingProductAtDispatch = true;
            this.DoNotResetQty = false;

            try
            {
                this.Locator.Indicator.Tare = product.PiecesTareWeight;
                //this.DoNotHandleSelectedSaleDetail = true;
                this.SaleDetails.Add(localSaleDetail);
                //this.DoNotHandleSelectedSaleDetail = false;
                this.SelectedSaleDetail = localSaleDetail;
                this.highlightSelectedProduct = true;
            }
            finally
            {
                //NouvemGlobal.SelectingAndWeighingProductAtDispatch = false;
            }

            this.OnPreviewMouseUpCommandExecute();
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void HandleTareProductSelection(InventoryItem product)
        {
            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }

            if (!this.CanCreateOrder && (this.SaleDetails != null && !this.SaleDetails.Any()))
            {
                NouvemMessageBox.Show(Message.CreateOrderPermissionsDenied, touchScreen: true);
                return;
            }

            var localSaleDetail = new SaleDetail
            {
                InventoryItem = product,
                INMasterID = product.Master.INMasterID,
                NouStockMode = product.StockMode,
                WeightOrdered = 0,
                WeightReceived = 0,
                QuantityOrdered = 0,
                QuantityReceived = 0,
                WeightOutstanding = 0,
                QuantityOutstanding = 0
            };

            NouvemGlobal.SelectingAndWeighingProductAtDispatch = true;

            var alreadyExists = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localSaleDetail.INMasterID);

            try
            {
                this.Locator.Indicator.Tare = 0;
                //this.DoNotHandleSelectedSaleDetail = true;
                if (alreadyExists == null)
                {
                    this.SaleDetails.Add(localSaleDetail);
                }
                else
                {
                    localSaleDetail = alreadyExists;
                }
           
                //this.DoNotHandleSelectedSaleDetail = false;
                this.SelectedSaleDetail = localSaleDetail;
                this.SelectedSaleDetail.DontPrintLabel = true;
                //this.highlightSelectedProduct = true;
            }
            finally
            {
                //NouvemGlobal.SelectingAndWeighingProductAtDispatch = false;
            }
        }
        
        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        /// <param name="transactions">The transaction trceability data.</param>
        private StockTransactionData ProcessTransactions(IList<TraceabilityResult> transactions)
        {
            this.Log.LogDebug(this.GetType(), "ProcessTransactions(): Processing transactions");

            var stockTransaction = new StockTransaction();
            var transTraceabilities = new List<TransactionTraceability>();

            // add the transaction weight to our stock transaction, and other relevant data.
            stockTransaction.TransactionWeight = this.indicatorWeight;
            stockTransaction.GrossWeight = this.indicatorWeight + this.tare;
            stockTransaction.Tare = this.tare;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = this.ProductQuantity.ToDecimal();
            stockTransaction.INMasterID = this.SelectedSaleDetail.INMasterID;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId;
            stockTransaction.WarehouseID = NouvemGlobal.WarehouseIntakeId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;

            this.Log.LogDebug(this.GetType(), string.Format("Adding stock transaction: TransactionWeight:{0}, GrossWeight:{1}, Tare:{2}," +
                                                           "Trans Qty:{3}, INMasterID:{4}" +
                                                               "Transaction Date:{5}, NouTransactionType:{6}, Warehouse:{7}",
                                                               stockTransaction.TransactionWeight,
                                                               stockTransaction.GrossWeight,
                                                               stockTransaction.Tare,
                                                               stockTransaction.TransactionQTY,
                                                               stockTransaction.INMasterID,
                                                               stockTransaction.TransactionDate,
                                                               stockTransaction.NouTransactionTypeID,
                                                               stockTransaction.WarehouseID));

            foreach (var transaction in transactions)
            {
                // add to our transaction traceability result
                var traceability = this.ParseToTransactionTraceability(transaction);
                transTraceabilities.Add(traceability);
            }

            var transactionData = new StockTransactionData
            {
                Transaction = stockTransaction,
                TransactionTraceabilities = transTraceabilities
            };

            return transactionData;
        }
        
        /// <summary>
        /// Handle the touchscreen load completion notification.
        /// </summary>
        private void HandleTouchscreenLoad()
        {
            if (!this.highlightSelectedProduct)
            {
                this.SelectedSaleDetail = null;
            }
            else
            {
                // Need to regenerate the ui traceability data
                this.SelectedSaleDetail = this.SelectedSaleDetail;
            }

            this.highlightSelectedProduct = false;
        }

        /// <summary>
        /// Check for a batch match with the selected order line product.
        /// </summary>
        /// <param name="b">The batch data.</param>
        private void CheckBatchMatch(GoodsReceiptData b)
        {
            this.BookedAgainstProductData = b;
            this.clearTraceability = true;

            // check that the selected line group matches the batch group.
            if (b.ProductGroup != null && this.SelectedSaleDetail != null)
            {
                if (this.SelectedSaleDetail.Concessions != null && this.SelectedSaleDetail.Concessions.Any(x => x.Reference == b.Reference && x.INMasterID == b.Product.INMasterID))
                {
                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                    {
                        this.highlightSelectedProduct = true;
                        this.clearTraceability = false;
                        var localDetail = this.SelectedSaleDetail;
                        this.SelectedSaleDetail = localDetail;
                        this.clearTraceability = true;
                    }

                    return;
                }

                var productid = b.Product != null ? b.Product.INMasterID : 0;
                var productGroupid = b.ProductGroup != null ? b.ProductGroup.INGroupID : 0;
                this.Log.LogInfo(this.GetType(), string.Format("4. CheckBatchMatch(): " +
                                                                  "Batch Product id:{0}, " +
                                                                  "Batch group id:{1} Product Id:{2}, Group Id:{3}",
                                                                  productid, productGroupid, this.SelectedSaleDetail.INMasterID, this.SelectedSaleDetail.INGroupID));

                // get all the parent group id's 
                var hirearchyGroupIds = this.GetParentGroups(this.SelectedSaleDetail.INGroupID);
                var isInTrimGroup = this.trimProductGroups.Contains(this.SelectedSaleDetail.INGroupID);

                // if there is no group, or hierarchical parent group match, warn the user.
                if (!hirearchyGroupIds.Contains(b.ProductGroup.INGroupID) && !isInTrimGroup)
                {
                    this.Log.LogInfo(this.GetType(), "5. No match. Asking for concession reason");
                    var groupName = b.ProductGroup.Name;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        if (!this.AuthorisationsManager.AllowUserToMakeConcessions)
                        {
                            SystemMessage.Write(MessageType.Issue, Message.NoAuthorisationToMakeConcessions);
                            return;
                        }

                        try
                        {
                            this.disableWeighing = true;
                            NouvemMessageBox.Show(string.Format(Message.DispatchLineBatchGroupMismatch, groupName), NouvemMessageBoxButtons.YesNo, true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                            {
                                Messenger.Default.Send(Tuple.Create(ViewType.Keyboard, ViewType.ARDispatch));
                            }
                            else
                            {
                                this.BatchNumber = null;
                                this.BatchDataToDisplay = string.Empty;
                                this.clearTraceability = true;
                                Messenger.Default.Send(Token.Message, Token.ClearAndResetData);

                            }
                        }
                        finally
                        {
                            this.disableWeighing = false;
                        }
                    }));
                }
                else
                {
                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                    {
                        this.highlightSelectedProduct = true;
                        this.clearTraceability = false;
                        var localDetail = this.SelectedSaleDetail;
                        this.SelectedSaleDetail = localDetail;
                        this.clearTraceability = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets all the parent ids in the group hierarchy.
        /// </summary>
        /// <param name="groupId">The group id to check.</param>
        /// <returns>Returns all the parent ids in the group hierarchy.</returns>
        private HashSet<int> GetParentGroups(int groupId)
        {
            int? childsParentId = null;
            var childsGroup = this.groups.FirstOrDefault(x => x.INGroupID == groupId);
            if (childsGroup != null)
            {
                childsParentId = childsGroup.ParentInGroupID;
            }
          
            var hierarchyIds = new HashSet<int> { groupId };

            while (childsParentId != null)
            {
                var parent = this.groups.FirstOrDefault(x => x.INGroupID == childsParentId);
                if (parent != null)
                {
                    hierarchyIds.Add(parent.INGroupID);
                    childsParentId = parent.ParentInGroupID;

                    if (childsParentId == null)
                    {
                        // we at the root level, so we also need to add all the child groups.
                        var rootChildGroups = this.groups.Where(x => x.ParentInGroupID == parent.INGroupID).ToList();
                        hierarchyIds.UnionWith(this.GetChildGroups(rootChildGroups));
                        this.childGroupIds.Clear();
                    }
                }
                else
                {
                    // we at the root level, so we also need to add all the child groups.
                    var rootChildGroups = this.groups.Where(x => x.ParentInGroupID == parent.INGroupID).ToList();
                    hierarchyIds.UnionWith(this.GetChildGroups(rootChildGroups));
                    this.childGroupIds.Clear();

                    childsParentId = null;
                }
            }

            return hierarchyIds;
        }

        /// <summary>
        /// Gets all the root child group ids.
        /// </summary>
        /// <param name="childGroups">The child groups wholse own child groups we recursively call.</param>
        /// <returns>All the child groups.</returns>
        private HashSet<int> GetChildGroups(List<INGroup> childGroups)
        {
            if (childGroups == null || !childGroups.Any())
            {
                return this.childGroupIds;
            }

            foreach (var childGroup in childGroups)
            {
                this.childGroupIds.Add(childGroup.INGroupID);
                this.GetChildGroups(this.groups.Where(x => x.ParentInGroupID == childGroup.INGroupID).ToList());
            }

            return this.childGroupIds;
        }

        /// <summary>
        /// Gets the application product groups.
        /// </summary>
        private void GetProductGroups()
        {
            this.groups = this.DataManager.GetInventoryGroups();
        }

        /// <summary>
        /// Adds the user entered batch/product mismatch reason.
        /// </summary>
        /// <param name="reason">The user reason.</param>
        private void AddBatchProductMismatchReason(string reason)
        {
            if (string.IsNullOrWhiteSpace(reason))
            {
                this.BatchDataToDisplay = string.Empty;
                this.batchNumber = null;
                this.clearTraceability = true;
                SystemMessage.Write(MessageType.Issue, Message.NoBatchProductMismatchReasonRecorded);
                return;
            }

            if (this.SelectedSaleDetail.Concessions == null)
            {
                this.SelectedSaleDetail.Concessions = new List<Concession>();
            }

            var con = new Concession
            {
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                INMasterID = this.BookedAgainstProductData.Product.INMasterID,
                Reference = this.BookedAgainstProductData.Reference ?? string.Empty,
                Reason = reason
            };

            this.SelectedSaleDetail.Concessions.Add(con);
            this.Log.LogInfo(this.GetType(), string.Format("6. Recording concession. InmasterId:{0}, Reference:{1}, Reason:{2}", con.INMasterID, con.Reference, con.Reason));

            SystemMessage.Write(MessageType.Priority, string.Format("{0}: {1}", Strings.ReasonRecorded, reason));
            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
            {
                this.highlightSelectedProduct = true;
                this.clearTraceability = false;
                var localDetail = this.SelectedSaleDetail;
                this.SelectedSaleDetail = localDetail;
                this.clearTraceability = true;
            }
        }

        /// <summary>
        /// Gets the trim product groups.
        /// </summary>
        private void GetTrimGroups()
        {
            this.trimProductGroups = this.groups.Where(x => x.Name.EndsWithIgnoringCase(" Trim")).Select(group => group.INGroupID).ToList();
        }

        /// <summary>
        /// Handles the incoming selected tare calcuators.
        /// </summary>
        /// <param name="tares">The incoming tares.</param>
        private void HandleTareCalculators()
        {
            var localSelectedSaleDetail = this.SelectedSaleDetail;

            try
            {
                foreach (var tareCalculator in this.tares)
                {
                    var localItem =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == tareCalculator.INMasterID);

                    if (localItem != null)
                    {
                        var price = this.DataManager.GetPriceListDetail(this.SelectedSaleDetail.PriceListID,
                            localItem.Master.INMasterID);

                        if (price == null)
                        {
                            price = this.DataManager.GetPriceListDetail(NouvemGlobal.BasePriceList.PriceListID,
                            localItem.Master.INMasterID);
                        }

                        if (price != null && price.Price > 0)
                        {
                            var localbatchToDisplay = this.BatchDataToDisplay;
                            var localBatch = this.BatchNumber;
                            this.HandleTareProductSelection(localItem);
                            this.BatchNumber = localBatch;
                            this.BatchDataToDisplay = localbatchToDisplay;
                            this.manualWeight = true;
                            this.indicatorWeight = 0;
                            this.ProductQuantity = tareCalculator.Quantity;
                            this.DisableProcessing = false;
                            this.RecordWeight();
                        }
                    }
                }
            }
            finally
            {
                this.manualWeight = false;
                this.DoNotHandleSelectedSaleDetail = true;
                this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localSelectedSaleDetail.INMasterID);
                this.DoNotHandleSelectedSaleDetail = false;
            }
        }

        /// <summary>
        /// Determines if the barcode is a scotbeef barcode.
        /// </summary>
        /// <param name="data">The scanned data.</param>
        /// <returns>Flag, as to whether it's a ascotbeef barcode or not.</returns>
        private bool IsScotBeefBarcode(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("IsScotBeefBarcode:{0}", data));
            return data.Length >= 12 &&
                (data.ContainsIgnoringCase("H")
                || data.ContainsIgnoringCase("F")
                || data.ContainsIgnoringCase("L"));
        }

        /// <summary>
        /// Sets the mode.
        /// </summary>
        /// <param name="process">The incoming process selection.</param>
        private void SetDispatchMode(Nouvem.Model.DataLayer.Process process)
        {
            try
            {
                this.IndicatorManager.IgnoreValidation(false);
                ApplicationSettings.DispatchMode =
                    (DispatchMode)Enum.Parse(typeof(DispatchMode), process.Name.Replace(" ", ""));
                this.SetPanel(process);
                this.GetAttributeAllocationApplicationData(process.ProcessID);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("SetDispatchMode: {0}", ex.Message));
            }
        }

        private void SetUseBy(string useBy)
        {
            if (ApplicationSettings.UseLegacyUseByAttribute)
            {
                this.Attribute6 = useBy;
            }
            else
            {
                this.Attribute1 = useBy;
            }
        }

        /// <summary>
        /// Sets the panel depending on the process.
        /// </summary>
        /// <param name="process">The process.</param>
        private void SetPanel(Process process)
        {
            if (process.Name.CompareIgnoringCase(ProcessType.Dispatch))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenDispatchPanelStatic;
                this.Locator.Indicator.WeighMode = WeighMode.Manual;
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.DispatchBatch))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenDispatchBatchPanelStatic;
                this.Locator.Indicator.WeighMode = WeighMode.Manual;
                this.Locator.TouchscreenProductionOrders.GetOrders(ApplicationSettings.SelectIntakeBatchesAtDispatch);
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.DispatchRecordWeight))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                this.Locator.Indicator.WeighMode = WeighMode.Manual;
                this.Locator.TouchscreenProductionOrders.GetOrders(ApplicationSettings.SelectIntakeBatchesAtDispatch);
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.DispatchRecordWeightWithNotes))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
                this.Locator.Indicator.WeighMode = WeighMode.Manual;
                this.Locator.TouchscreenProductionOrders.GetOrders(ApplicationSettings.SelectIntakeBatchesAtDispatch);
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.DispatchRecordWeightWithShipping))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
                this.Locator.Indicator.WeighMode = WeighMode.Manual;
                this.Locator.TouchscreenProductionOrders.GetOrders(ApplicationSettings.SelectIntakeBatchesAtDispatch);
                return;
            }
        }

        
        
        #endregion

        #endregion
    }
}


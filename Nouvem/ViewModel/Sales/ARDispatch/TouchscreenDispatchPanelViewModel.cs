﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenDispatchPanelViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Sales.ARDispatch
{
    using GalaSoft.MvvmLight;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class TouchscreenDispatchPanelViewModel : ViewModelBase
    {
        #region field

        /// <summary>
        /// The qty or pieces text.
        /// </summary>
        private string qtyPiecesText;

        /// <summary>
        /// The box/price text.
        /// </summary>
        private string boxOrPriceText;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenDispatchPanelViewModel"/> class.
        /// </summary>
        public TouchscreenDispatchPanelViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.QtyPiecesText = Strings.Quantity;
            this.BoxOrPriceText = ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight ? Strings.BoxLabel : Strings.ChangePrice;
        }

        #region public interface

        /// <summary>
        /// Gets or sets the box/price text.
        /// </summary>
        public string BoxOrPriceText
        {
            get
            {
                return this.boxOrPriceText;
            }

            set
            {
                this.boxOrPriceText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the qty/pieces text.
        /// </summary>
        public string QtyPiecesText
        {
            get
            {
                return this.qtyPiecesText;
            }

            set
            {
                this.qtyPiecesText = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion
    }
}

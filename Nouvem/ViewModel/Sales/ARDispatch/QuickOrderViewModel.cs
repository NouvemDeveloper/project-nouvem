﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    public class QuickOrderViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// Batch mode.
        /// </summary>
        private bool batchSelected;

        /// <summary>
        /// Production mode.
        /// </summary>
        private bool productSelected;

        /// <summary>
        /// Order mode.
        /// </summary>
        private bool orderSelected;

        /// <summary>
        /// All stock mode.
        /// </summary>
        private bool allStock;

        /// <summary>
        /// All stock mode.
        /// </summary>
        private bool unsoldStock;

        /// <summary>
        /// All stock mode.
        /// </summary>
        private bool soldStock;

        /// <summary>
        /// The running total.
        /// </summary>
        private decimal runningTotal;

        /// <summary>
        /// The running stock total.
        /// </summary>
        private decimal stockValue;

        /// <summary>
        /// The stock wgt total.
        /// </summary>
        private decimal stockWgt;

        /// <summary>
        /// The stock qty total.
        /// </summary>
        private int stockQty;

        /// <summary>
        /// The batch view data.
        /// </summary>
        private ObservableCollection<App_GetQuickOrderViewData_Result> batchViewData = new ObservableCollection<App_GetQuickOrderViewData_Result>();

        /// <summary>
        /// The batch view data.
        /// </summary>
        private ObservableCollection<App_GetQuickOrderViewData_Result> productViewData = new ObservableCollection<App_GetQuickOrderViewData_Result>();

        /// <summary>
        /// The batch view data.
        /// </summary>
        private ObservableCollection<App_GetQuickOrderViewData_Result> orderViewData = new ObservableCollection<App_GetQuickOrderViewData_Result>();

        /// <summary>
        /// The batch view data.
        /// </summary>
        private ObservableCollection<App_GetQuickOrderStockViewData_Result> batchViewStockData = new ObservableCollection<App_GetQuickOrderStockViewData_Result>();

        /// <summary>
        /// The batch view data.
        /// </summary>
        private ObservableCollection<App_GetQuickOrderStockViewData_Result> productViewStockData = new ObservableCollection<App_GetQuickOrderStockViewData_Result>();

        /// <summary>
        /// The batch view data.
        /// </summary>
        private ObservableCollection<App_GetQuickOrderStockViewData_Result> orderViewStockData = new ObservableCollection<App_GetQuickOrderStockViewData_Result>();

        /// <summary>
        /// The selected batch view data.
        /// </summary>
        private App_GetQuickOrderViewData_Result selectedBatchViewData;

        /// <summary>
        /// The selected product view data.
        /// </summary>
        private App_GetQuickOrderViewData_Result selectedProductViewData;

        /// <summary>
        /// The selected order view data.
        /// </summary>
        private App_GetQuickOrderViewData_Result selectedOrderViewData;

        /// <summary>
        /// The selected order view data.
        /// </summary>
        private App_GetQuickOrderStockViewData_Result selectedOrderViewStockData;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickOrderViewModel"/> class.
        /// </summary>
        public QuickOrderViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<decimal>(this, Token.ApplyPrice, this.ApplyPrice);

            #endregion

            #region command handler

            this.RefreshCommand = new RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.RefreshComplete);
            });

            this.SaleSelectedCommand = new RelayCommand(this.SaleSelectedCommandExecute);
            this.DeleteStockCommand = new RelayCommand(this.DeleteStockCommandExecute);
            this.PasteStockCommand = new RelayCommand(this.PasteStockCommandExecute);
            this.ApplyPriceCommand = new RelayCommand(() => Messenger.Default.Send(ViewType.ApplyPrice));

            // The handler for the grid selection changed event.
            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);
            this.OnSelectionChangedOrderCommand = new RelayCommand(this.OnSelectionChangedOrderCommandExecute);

            #endregion

            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected stock items.
        /// </summary>
        public IList<App_GetQuickOrderStockViewData_Result> SelectedStock { get; set; } = new List<App_GetQuickOrderStockViewData_Result>();

        /// <summary>
        /// Gets or sets the selected order stock items.
        /// </summary>
        public IList<App_GetQuickOrderStockViewData_Result> SelectedOrderStock { get; set; } = new List<App_GetQuickOrderStockViewData_Result>();

        /// <summary>
        /// Gets or sets the batch view data.
        /// </summary>
        public ObservableCollection<App_GetQuickOrderViewData_Result> BatchViewData
        {
            get { return this.batchViewData; }
            set
            {
                this.batchViewData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the product view data.
        /// </summary>
        public ObservableCollection<App_GetQuickOrderViewData_Result> ProductViewData
        {
            get { return this.productViewData; }
            set
            {
                this.productViewData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the order view data.
        /// </summary>
        public ObservableCollection<App_GetQuickOrderViewData_Result> OrderViewData
        {
            get { return this.orderViewData; }
            set
            {
                this.orderViewData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch view stock data.
        /// </summary>
        public ObservableCollection<App_GetQuickOrderStockViewData_Result> BatchViewStockData
        {
            get { return this.batchViewStockData; }
            set
            {
                this.batchViewStockData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the product view stock data.
        /// </summary>
        public ObservableCollection<App_GetQuickOrderStockViewData_Result> ProductViewStockData
        {
            get { return this.productViewStockData; }
            set
            {
                this.productViewStockData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the order view stock data.
        /// </summary>
        public ObservableCollection<App_GetQuickOrderStockViewData_Result> OrderViewStockData
        {
            get { return this.orderViewStockData; }
            set
            {
                this.orderViewStockData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected batch view data.
        /// </summary>
        public App_GetQuickOrderViewData_Result SelectedBatchViewData
        {
            get { return this.selectedBatchViewData; }
            set
            {
                this.selectedBatchViewData = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleSelectedView();
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected product view data.
        /// </summary>
        public App_GetQuickOrderViewData_Result SelectedProductViewData
        {
            get { return this.selectedProductViewData; }
            set
            {
                this.selectedProductViewData = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleSelectedView();
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected order view data.
        /// </summary>
        public App_GetQuickOrderViewData_Result SelectedOrderViewData
        {
            get { return this.selectedOrderViewData; }
            set
            {
                this.selectedOrderViewData = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleSelectedView();
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected order view data.
        /// </summary>
        public App_GetQuickOrderStockViewData_Result SelectedOrderViewStockData
        {
            get { return this.selectedOrderViewStockData; }
            set
            {
                this.selectedOrderViewStockData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch mode.
        /// </summary>
        public bool BatchSelected
        {
            get { return this.batchSelected; }
            set
            {
                this.batchSelected = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.SetSelectionViewData("Batch");
                }
            }
        }

        /// <summary>
        /// Gets or sets the product mode.
        /// </summary>
        public bool ProductSelected
        {
            get { return this.productSelected; }
            set
            {
                this.productSelected = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.SetSelectionViewData("Product");
                }
            }
        }

        /// <summary>
        /// Gets or sets the order mode.
        /// </summary>
        public bool OrderSelected
        {
            get { return this.orderSelected; }
            set
            {
                this.orderSelected = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.SetSelectionViewData("Order");
                }
            }
        }

        /// <summary>
        /// Gets or sets the sold stock view.
        /// </summary>
        public bool SoldStock
        {
            get { return this.soldStock; }
            set
            {
                this.soldStock = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleSelectedView();
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the unsold stock view.
        /// </summary>
        public bool UnsoldStock
        {
            get { return this.unsoldStock; }
            set
            {
                this.unsoldStock = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleSelectedView();
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the all stock view.
        /// </summary>
        public bool AllStock
        {
            get { return this.allStock; }
            set
            {
                this.allStock = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.HandleSelectedView();
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the running total.
        /// </summary>
        public decimal RunningTotal
        {
            get
            {
                return this.runningTotal;
            }

            set
            {
                this.runningTotal = Math.Round(value, 2);
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the running total.
        /// </summary>
        public decimal StockValue
        {
            get
            {
                return this.stockValue;
            }

            set
            {
                this.stockValue = Math.Round(value, 2);
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock wgt total.
        /// </summary>
        public decimal StockWgt
        {
            get
            {
                return this.stockWgt;
            }

            set
            {
                this.stockWgt = Math.Round(value, 2);
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock qty total.
        /// </summary>
        public int StockQty
        {
            get
            {
                return this.stockQty;
            }

            set
            {
                this.stockQty = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the sale selection.
        /// </summary>
        public ICommand SaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to copy/paste stock to order.
        /// </summary>
        public ICommand DeleteStockCommand { get; private set; }

        /// <summary>
        /// Gets the command to copy/paste stock to order.
        /// </summary>
        public ICommand PasteStockCommand { get; private set; }

        /// <summary>
        /// Gets the command to apply price to selected stock.
        /// </summary>
        public ICommand ApplyPriceCommand { get; private set; }

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand OnSelectionChangedOrderCommand { get; private set; }

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Override, to handle a control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddOrUpdateOrder();
                    break;

                case ControlMode.Find:

                    break;

                case ControlMode.Update:
                    this.AddOrUpdateOrder();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void DeleteTransaction(int stockId, string barcode = "")
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to delete dispatch label:{stockId}");
            var error = this.DataManager.DeleteDispatchStock(stockId, this.SelectedOrderViewData.Field1.ToInt(), NouvemGlobal.UserId.ToInt(),
                NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.OutOfProductionId, this.AutoSplitProductId.ToInt());
        }

        /// <summary>
        /// Handles a grid field change.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            #region validation
           

            if (e == null || e.Cell == null || e.Value == null || string.IsNullOrEmpty(e.Cell.Property))
            {
                return;
            }

            #endregion

            try
            {
                if (e.Cell.Property.Equals("Field10") && this.SelectedOrderViewData != null)
                {
                    if (this.SelectedOrderViewData.Field1.ToInt() == 0)
                    {
                        this.SetControlMode(ControlMode.Add);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogDebug(this.GetType(), ex.Message);
            }
        }

        protected override void OnLoadingCommandExecute()
        {
            this.SetControlMode(ControlMode.OK);
            this.IsFormLoaded = true;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.BatchSelected = true;
            }));
        }

        protected override void OnUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
            this.Close();
        }

        protected override void AddSale()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        protected override void CopyDocumentFrom()
        {
            
        }

        protected override void CopyDocumentTo()
        {
     
        }

        protected override void DirectSearchCommandExecute()
        {
          
        }

        protected override void FindSale()
        {
          
        }

        protected override void FindSaleCommandExecute()
        {
        
        }

        protected override void UpdateSale()
        {
      
        }

        protected override void Close()
        {
            this.BatchViewStockData?.Clear();
            this.ProductViewStockData?.Clear();
            this.OrderViewStockData?.Clear();
            this.BatchViewData?.Clear();
            this.BatchViewData?.Clear();
            this.BatchViewData?.Clear();
            this.BatchViewStockData = null;
            this.ProductViewStockData = null;
            this.OrderViewStockData = null;
            this.BatchViewData = null;
            this.BatchViewData = null;
            this.BatchViewData = null;
            Messenger.Default.Send(Token.Message, Token.CloseQuickOrderWindow);
            Messenger.Default.Unregister(this);
        }

        #endregion

        #region private

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        private void SaleSelectedCommandExecute()
        {
            if (this.SelectedOrderViewData != null)
            {
                Messenger.Default.Send(ViewType.ARDispatch);
                Messenger.Default.Send(new Sale { SaleID = this.SelectedOrderViewData.Field1.ToInt() }, Token.SearchSaleSelected);
            }
        }

        /// <summary>
        /// Applies a price to the selected stock items.
        /// </summary>
        /// <param name="price">The price to apply.</param>
        private void ApplyPrice(decimal price)
        {
            var localStock = this.SelectedOrderStock.Where(x => x.Field1.ToInt() > 0).ToList();
            if (!localStock.Any())
            {
                return;
            }

            if (this.DataManager.PriceStocktransactions(string.Join(",", localStock.Select(x => x.Field1)), price))
            {
                this.HandleSelectedView();
                SystemMessage.Write(MessageType.Priority, string.Format(Message.PriceAppliedToSelectedStockItems,price));
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.PriceNotAppliedToSelectedStockItems);
            }
        }

        /// <summary>
        /// Pastes the selected stock items into the order grid.
        /// </summary>
        private void PasteStockCommandExecute()
        {
            if (!this.SelectedStock.Any())
            {
                return;
            }

            if (this.SelectedOrderViewData == null)
            {
                return;
            }

            // pasting
            var localStock = this.SelectedStock.ToList();
            foreach (var stockItem in localStock)
            {
                stockItem.Field6 = Strings.NotOnDocket;
                this.OrderViewStockData.Add(stockItem);
            }

            if (this.SelectedOrderViewData != null)
            {
                if (this.SelectedOrderViewData.Field1.ToInt() == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }

            this.SelectedStock.Clear();
            localStock = null;
        }

        /// <summary>
        /// Deletes the selected stock item.
        /// </summary>
        private void DeleteStockCommandExecute()
        {
            if (this.SelectedOrderStock == null || !this.SelectedOrderStock.Any())
            {
                return;
            }

            NouvemMessageBox.Show(Message.DeleteStockItemMsg, NouvemMessageBoxButtons.OKCancel);
            if (NouvemMessageBox.UserSelection != UserDialogue.OK)
            {
                return;
            }

            var localOrder = this.SelectedOrderViewData;
            try
            {
                foreach (var item in this.SelectedOrderStock)
                {
                    this.DeleteTransaction(item.Field2.ToInt());
                }


                this.Refresh();
                this.SelectedOrderViewData = this.OrderViewData.FirstOrDefault(x => x.Field4 == localOrder.Field4);
                SystemMessage.Write(MessageType.Priority, Message.StockRemoved);
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(),e.Message);
            }
        }

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedStock == null)
            {
                return;
            }

            if (this.SelectedOrderViewData != null)
            {
                if (this.SelectedOrderViewData.Field1.ToInt() == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }

            this.RunningTotal = this.SelectedStock.Sum(x => x.Field5.ToDecimal());
        }

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedOrderCommandExecute()
        {
            if (this.SelectedOrderStock == null)
            {
                return;
            }

            this.RunningTotal = this.SelectedOrderStock.Sum(x => x.Field5.ToDecimal());
            this.StockValue = this.SelectedOrderStock.Sum(x => x.Field5.ToDecimal() * x.Field8.ToDecimal());
        }

        /// <summary>
        /// Sets the stock to be displayed.
        /// </summary>
        private void HandleSelectedView()
        {
            var mode = this.BatchSelected ? "Batch" : this.ProductSelected ? "Product" : "Order";
            var stockType = this.AllStock ? "All" : this.SoldStock ? "Sold" : "Unsold";
            switch (mode)
            {
                case "Batch":
                    if (this.SelectedBatchViewData != null)
                    {
                        this.BatchViewStockData = new ObservableCollection<App_GetQuickOrderStockViewData_Result>(
                            this.DataManager.GetQuickOrderStockViewData(mode, stockType, this.SelectedBatchViewData.Field1.ToInt()));
                        this.StockWgt = this.BatchViewStockData.Sum(x => x.Field5.ToDecimal());
                        this.StockQty = this.BatchViewStockData.Count;
                    }
                    
                    break;

                case "Product":
                    if (this.SelectedProductViewData != null)
                    {
                        this.ProductViewStockData = new ObservableCollection<App_GetQuickOrderStockViewData_Result>(
                            this.DataManager.GetQuickOrderStockViewData(mode, stockType, this.SelectedProductViewData.Field1.ToInt()));
                        this.StockWgt = this.ProductViewStockData.Sum(x => x.Field5.ToDecimal());
                        this.StockQty = this.ProductViewStockData.Count;
                    }
                  
                    break;

                case "Order":
                    if (this.SelectedOrderViewData != null)
                    {
                        this.OrderViewStockData = new ObservableCollection<App_GetQuickOrderStockViewData_Result>(
                            this.DataManager.GetQuickOrderStockViewData(mode, stockType, this.SelectedOrderViewData.Field1.ToInt()));
                        this.StockWgt = this.OrderViewStockData.Sum(x => x.Field5.ToDecimal());
                        this.StockQty = this.OrderViewStockData.Count;
                    }
                   
                    break;
            }
        }

        /// <summary>
        /// Refrshes th eui.
        /// </summary>
        private void Refresh()
        {
            if (this.BatchSelected)
            {
                this.BatchSelected = true;
            }
            else if (this.ProductSelected)
            {
                this.ProductSelected = true;
            }
            else if (this.OrderSelected)
            {
                this.OrderSelected = true;
            }
        }

        private void SetSelectionViewData(string mode)
        {
            //this.BatchViewStockData.Clear();
            //this.ProductViewStockData.Clear();
            //this.OrderViewStockData.Clear();
            this.SetControlMode(ControlMode.OK);
            if (mode == "Batch")
            {
                this.StockWgt = 0;
                this.StockQty = 0;
                this.RunningTotal = 0;
                this.StockValue = 0;
                this.BatchViewStockData.Clear();
                this.BatchViewData = new ObservableCollection<App_GetQuickOrderViewData_Result>(this.DataManager.GetQuickOrderViewData("Batch"));
            }
            else if (mode == "Product")
            {
                this.StockWgt = 0;
                this.StockQty = 0;
                this.RunningTotal = 0;
                this.StockValue = 0;
                this.ProductViewStockData.Clear();
                this.ProductViewData = new ObservableCollection<App_GetQuickOrderViewData_Result>(this.DataManager.GetQuickOrderViewData("Product"));
            }
            else
            {
                this.StockWgt = 0;
                this.StockQty = 0;
                //this.RunningTotal = 0;
                this.OrderViewStockData.Clear();
                this.OrderViewData = new ObservableCollection<App_GetQuickOrderViewData_Result>(this.DataManager.GetQuickOrderViewData("Order"));
            }

            this.AllStock = true;
            Messenger.Default.Send(mode, Token.ShowQuickOrderView);
        }

        private void AddOrUpdateOrder()
        {
            if (this.SelectedOrderViewData == null)
            {
                return;
            }

            try
            {
                ProgressBar.SetUp(0, this.SelectedStock.Count == 0 ? 1 : this.SelectedStock.Count);
                ProgressBar.Run();
                var dispatchId = this.SelectedOrderViewData.Field1.ToInt();
                if (dispatchId == 0)
                {
                    var result = this.DataManager.AddOrUpdateQuickOrder(
                        dispatchId,
                        this.SelectedOrderViewData.Field10, 0,
                        NouvemGlobal.UserId.ToInt(),
                        NouvemGlobal.DeviceId.ToInt());

                    var error = result.Item1;
                    if (!string.IsNullOrEmpty(error))
                    {
                        SystemMessage.Write(MessageType.Issue, error);
                        NouvemMessageBox.Show(error);
                        this.SetControlMode(ControlMode.OK);
                        return;
                    }

                    dispatchId = result.Item2;
                }

                foreach (var stock in this.OrderViewStockData.Where(x => x.Field6.Equals(Strings.NotOnDocket)))
                {
                    ProgressBar.Run();
                    this.DataManager.AddOrUpdateQuickOrder(
                        dispatchId,
                        this.SelectedOrderViewData.Field10,
                        stock.Field1.ToInt(),
                        NouvemGlobal.UserId.ToInt(),
                        NouvemGlobal.DeviceId.ToInt());
                }

                this.SetSelectionViewData("Order");
                this.SelectedOrderViewData = this.OrderViewData.FirstOrDefault(x => x.Field1 == dispatchId.ToString());
                this.SetControlMode(ControlMode.OK);
                if (this.SelectedOrderViewData != null)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DispatchDocketUpdated, this.SelectedOrderViewData.Field4));
                    this.SelectedStock.Clear();
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        #endregion
    }
}

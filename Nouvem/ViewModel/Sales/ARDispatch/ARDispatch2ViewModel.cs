﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatch2ViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Windows;
using DevExpress.Xpf.Editors.Helpers;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using System.Windows.Threading;
    using DevExpress.Utils;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class ARDispatch2ViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The order no.
        /// </summary>
        private int orderNo;

        /// <summary>
        /// The transaction count.
        /// </summary>
        private int transactionCount;

        /// <summary>
        /// The suppliers collection.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> suppliers;

        /// <summary>
        /// The incoming product quantity.
        /// </summary>
        private double productQuantity;

        /// <summary>
        /// The container reference.
        /// </summary>
        private string containerRef;

        /// <summary>
        /// Is a price change uncommitted flag.
        /// </summary>
        private bool uncommittedPriceChange;

        /// <summary>
        /// The selected dispatch containers.
        /// </summary>
        private Model.BusinessObject.DispatchContainer selectedDispatchContainer;

        /// <summary>
        /// The recent dispatches.
        /// </summary>
        private ObservableCollection<Sale> dispatches = new ObservableCollection<Sale>();

        /// <summary>
        /// The selected dispatch.
        /// </summary>
        private Sale selectedDispatch;

        /// <summary>
        /// The price methods.
        /// </summary>
        private IList<NouPriceMethod> priceMethods = new List<NouPriceMethod>();

        /// <summary>
        /// Flag, as to whether the dispatch is to be auto copied to an invoice.
        /// </summary>
        private bool autoCopyToInvoice;

        /// <summary>
        /// The allow auto weigh flag.
        /// </summary>
        protected bool allowAutoWeigh = true;

        /// <summary>
        /// Flag, as to whether the current product is highlighted on ui load.
        /// </summary>
        protected bool highlightSelectedProduct;

        /// <summary>
        /// The clear traceability flag.
        /// </summary>
        protected bool clearTraceability = true;

        /// <summary>
        /// The display popup flag.
        /// </summary>
        protected bool displayPopUpNote;

        /// <summary>
        /// Flag, as to whether tares from the calculator are to be applied.
        /// </summary>
        protected bool applyTares;

        /// <summary>
        /// Flag, as to whether we are scanning a multivac label for the batch data.
        /// </summary>
        protected bool scanningLabelForBatchNo;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARDispatch2ViewModel"/> class.
        /// </summary>
        public ARDispatch2ViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.ClosingDispatch2Window, s =>
            {
                this.Close();
            });

            //// Refresh the current sale when a price list is updated.
            //Messenger.Default.Register<string>(this, Token.PriceListsUpdated, s =>
            //{
            //    this.GetPriceLists();
            //});

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            // Register to handle the authorisation result.
            Messenger.Default.Register<string>(this, Token.AuthorisationResult, this.HandleUnfilledOrderAuthorisation);
           
            // Register for a product selection.
            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.ARDispatch)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID, InventoryItem = product, NouStockMode = product.StockMode };
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.ARDispatch)
                    {
                        this.SelectedPartner =
                            this.Customers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            // Register to handle the incoming goods in details (traceability data, and total weight).
            //Messenger.Default.Register<SaleDetail>(this, Token.UpdateGoodsInQtyReceived, this.HandleDispatchData);

            Messenger.Default.Register<decimal>(this, Token.UpdateWeight, d =>
            {
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.WeightReceived += d;
                }
            });

            #endregion

            #region command handler

            this.ResetCountCommand = new RelayCommand(() => this.TransactionCount = 0);

            this.MoveBackCommand = new RelayCommand(this.MoveBackCommandExecute);

            // Complete order command.
            this.CompleteOrderCommand = new RelayCommand(() => this.CompleteOrder());

            // Drul down to the items window.
            this.DrillDownToDispatchItemsCommand = new RelayCommand(this.DrillDownToDispatchItemsCommandExecute);

            // Handle the attempt to remove the selected product.
            this.RemoveProductCommand = new RelayCommand(this.RemoveProductCommandExecute);

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.ARDispatch)),
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() => this.RemoveItem(ViewType.ARDispatch));

            // Command to handle a drill down to manual serial entry.
            this.DrillDownManualSerialCommand = new RelayCommand(this.DrillDownManualSerialCommandExecute);

            // Handle the invoice selection.
            this.DispatchSelectedCommand = new RelayCommand(() =>
            {
                this.Sale = this.SelectedDispatch;
                this.SelectedTab = 0;
            });

            #endregion

            this.GetSearchTypes();
            this.AutoCopyToInvoice = Settings.Default.AutoCopyToInvoice;
            this.GetLocalDocNumberings();
            this.ShowRoute = true;
            this.SetUpTimer();
            this.CanOverrideOrderLimit = this.AuthorisationsManager.AllowUserToOverrideOrderLimit;
            this.CanOverrideCreditLimit = this.AuthorisationsManager.AllowUserToCreateSaleOrderExceedingCreditLimit;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the transaction count.
        /// </summary>
        public int TransactionCount
        {
            get
            {
                return this.transactionCount;
            }

            set
            {
                this.transactionCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the border number.
        /// </summary>
        public int OrderNo
        {
            get
            {
                return this.orderNo;
            }

            set
            {
                this.orderNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the container reference.
        /// </summary>
        public string ContainerRef
        {
            get
            {

                return this.containerRef;
            }

            set
            {
                this.containerRef = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected container.
        /// </summary>
        public Model.BusinessObject.DispatchContainer SelectedDispatchContainer
        {
            get
            {
                return this.selectedDispatchContainer;
            }

            set
            {
                this.selectedDispatchContainer = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.ContainerRef = value.Reference;
                    this.MoveBackCommandExecute();
                }
                else
                {
                    this.ContainerRef = string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in display sale mode only.
        /// </summary>
        public bool DisplayModeOnly { get; set; }

        /// <summary>
        /// Gets or sets the dispatches.
        /// </summary>
        public ObservableCollection<Sale> Dispatches
        {
            get
            {
                return this.dispatches;
            }

            set
            {
                this.dispatches = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected dispatch.
        /// </summary>
        public Sale SelectedDispatch
        {
            get
            {
                return this.selectedDispatch;
            }

            set
            {
                this.selectedDispatch = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether a dispatch docket is to be auto copied to an invoice.
        /// </summary>
        public bool AutoCopyToInvoice
        {
            get
            {
                return this.autoCopyToInvoice;
            }

            set
            {
                this.autoCopyToInvoice = value;
                this.RaisePropertyChanged();

                Settings.Default.AutoCopyToInvoice = value;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Gets or sets the product qty selected.
        /// </summary>
        public double ProductQuantity
        {
            get
            {
                return this.productQuantity;
            }

            set
            {
                this.productQuantity = value;
                //Messenger.Default.Send(value, Token.QuantitySelected);
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to reset the scanned/weighed count to 0.
        /// </summary>
        public ICommand ResetCountCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the dispatch completion.
        /// </summary>
        public ICommand CompleteOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selected dispatch.
        /// </summary>
        public ICommand DispatchSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove the selected product.
        /// </summary>
        public ICommand RemoveProductCommand { get; private set; }

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        /// <summary>
        /// Gets the command to edit the transactions.
        /// </summary>
        public ICommand EditTransactionsCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down for manual serial entry.
        /// </summary>
        public ICommand DrillDownManualSerialCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to the items.
        /// </summary>
        public ICommand DrillDownToDispatchItemsCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Show the sales.
        /// </summary>
        public void ShowAllSales()
        {
            this.ShowSales();
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        public void FindDispatchSales(int productID)
        {
            var dockets = this.DataManager.GetAllProductDockets(productID);

            var orderStatuses = this.DocStatusesOpen;
            this.CustomerSales = this.DataManager.GetAllARDispatches(orderStatuses)
                     .Where(x => dockets.Contains(x.SaleID)).ToList();

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.ARDispatch), Token.SearchForSale);
            Messenger.Default.Send(true, Token.SetTopMost);
            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch2);
            }
        }


        /// <summary>
        /// Handle the box label print for any qty.
        /// </summary>
        public void PrintBoxLabelAnyQty()
        {
            #region validation


            if (this.SelectedSaleDetail == null || this.SelectedSaleDetail.GoodsReceiptDatas == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductSelected);
                return;
            }

            if (!this.SelectedSaleDetail.GoodsReceiptDatas.Any())
            {
                return;
            }

            #endregion

            // add the transaction weight to our stock transaction, and other relevant data.
            var localGoodsReceiptData = this.SelectedSaleDetail.GoodsReceiptDatas.First();
            var qtyPerBox = -1;

            var trans = new StockTransaction
            {
                BatchNumberID = localGoodsReceiptData.BatchNumber.BatchNumberID,
                MasterTableID = this.SelectedSaleDetail.SaleDetailID,
                TransactionDate = DateTime.Now,
                INMasterID = this.SelectedSaleDetail.InventoryItem.Master.INMasterID,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId,
                WarehouseID = NouvemGlobal.DispatchId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = true
            };

            try
            {
                var transactionId = this.DataManager.AddBoxTransaction(trans, qtyPerBox);
                if (transactionId > 0)
                {
                    this.Print(transactionId, LabelType.Box);
                    SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
                }
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
            }
        }

        /// <summary>
        /// Handles the incoming dispatch details, by setting the control mode.
        /// </summary>
        public void HandleDispatchData(SaleDetail detail, decimal wgt = 0, decimal qty = 0)
        {
            try
            {
                this.Log.LogDebug(this.GetType(), "HandleDispatchData(): SaleDetail returned");

                this.DoNotHandleSelectedSaleDetail = true;
                this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                this.DoNotHandleSelectedSaleDetail = false;

                if (this.SelectedSaleDetail != null)
                {
                    if (this.SelectedSaleDetail.GoodsReceiptDatas == null ||
                        !this.SelectedSaleDetail.GoodsReceiptDatas.Any())
                    {
                        this.SelectedSaleDetail.GoodsReceiptDatas = detail.GoodsReceiptDatas;
                    }

                    // ensure the price list data is set (this relates to the touchscreen only)
                    if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.PriceListID == 0)
                    {
                        this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                    }

                    foreach (var data in this.SelectedSaleDetail.GoodsReceiptDatas)
                    {
                        if (data.BatchTraceabilities == null)
                        {
                            data.BatchTraceabilities = new List<BatchTraceability>();
                        }

                        this.Log.LogDebug(this.GetType(), string.Format("Batch Number:{0}", data.BatchNumber));
                        this.Log.LogDebug(this.GetType(), string.Format("Batch Traceabilities Count:{0}, Transaction Data Count:{1}", data.BatchTraceabilities.Count, data.TransactionData.Count));
                    }

                    // new sale detail is referencing the selected sale detail, so no need to update totals.
                    if (this.SelectedSaleDetail != detail)
                    {
                        // non referenced existing sale detail.
                        this.SelectedSaleDetail.GoodsReceiptDatas = detail.GoodsReceiptDatas;
                    }

                    this.CompleteTransaction(wgt, qty);
                }
            }
            finally
            {
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Determines if a product is on a sale order.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Flag, as to whether the product is on the order.</returns>
        public bool IsDispatchItemOnOrder(int productId)
        {
            if (this.Sale == null || this.SaleDetails == null || !this.SaleDetails.Any())
            {
                // no order details, so allow product to be scanned onto docket.
                return true;
            }

            return this.SaleDetails.Any(x => x.INMasterID == productId);
        }

        /// <summary>
        /// Removes a stock item.
        /// </summary>
        /// <param name="minusStockSaleDetail">The stock data.</param>
        /// <param name="serial">The stock serial number.</param>
        public void MinusScanStock(List<GoodsReceiptData> minusStockSaleDetail, int serial)
        {
            
        }

        /// <summary>
        /// Adds the incoming dispatch product.
        /// </summary>
        /// <param name="detail">The incoming sale detail.</param>
        public void SetSaleDetail(SaleDetail detail)
        {
            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }

            if (this.SaleDetails.Any())
            {
                var localDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                if (localDetail != null)
                {
                    return;
                }
            }

            this.SaleDetails.Add(detail);
            this.DoNotHandleSelectedSaleDetail = true;
            this.SelectedSaleDetail = detail;
            this.DoNotHandleSelectedSaleDetail = false;
        }

        /// <summary>
        /// Copies a sale to another document.
        /// </summary>
        /// <param name="saleToCopy">The sale to copy to.</param>
        public override void CopySale(Sale saleToCopy)
        {
            this.Log.LogDebug(this.GetType(), "CopySale(): Copying sale");

            if (saleToCopy.SaleDetails != null)
            {
                // need to attach the inventory item, as it's needed for the in master snapshot.
                foreach (var detail in saleToCopy.SaleDetails)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }
            }

            if (ApplicationSettings.AccountsCustomer.CompareIgnoringCase(Customer.Woolleys))
            {
                this.Sale.DocumentDate = saleToCopy.CreationDate;
            }

            this.Sale = saleToCopy;

            //var dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);

            this.Sale.SaleID = 0;
            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void GetSearchTypes()
        {
            base.GetSearchTypes();
            this.SelectedSearchType = this.SearchTypes.First();
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (searchSale != null)
            {
                this.Sale = this.DataManager.GetARDispatchById(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (!ViewModelLocator.IsARDispatchNull())
            {
                if (this.Locator.ARDispatch.LastEdit > this.LastEdit)
                {
                    return;
                }
            }

            this.Sale = this.DataManager.GetARDispatchByLastEdit();
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetARDispatchByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetARDispatchById(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetARDispatchByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetARDispatchById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetARDispatchByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetARDispatchByFirstLast(false);
        }

        /// <summary>
        /// Updates the selected sale detail totals.
        /// </summary>
        protected override void UpdateSaleDetailTotals()
        {
            if (this.SelectedSaleDetail != null)
            {
                this.SelectedSaleDetail.UpdateTotal(dispatch: true);
            }
        }

        /// <summary>
        /// Updates the sale details with any price change.
        /// </summary>
        /// <param name="details">The updated details.</param>
        protected override void HandlePriceListUpdates(List<PriceDetail> details)
        {
            // do nothing
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            var dispatchId = this.Sale.SaleID;
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.DispatchDocket, ReportParameters = reportParam };

            try
            {
                if (preview)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Get the local document status types.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems.Where(x => x != NouvemGlobal.NouDocStatusExported && x != NouvemGlobal.NouDocStatusMoved).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            if (!this.DisplayModeOnly)
            {
                Task.Factory.StartNew(this.GetAllSales);
            }

            base.OnLoadingCommandExecute();
            this.ClearForm();

            if (ApplicationSettings.DefaultToFindAtDesktopDispatch)
            {
                this.SetControlMode(ControlMode.Find);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }

            this.ScannerStockMode = ScannerMode.Scan;

            if (!this.DisplayModeOnly)
            {
                this.OpenScanner();
            }
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            this.Close();
        }

        /// <summary>
        /// Creates a sale order object.
        /// </summary>
        protected override void CreateSale()
        {
            this.Log.LogDebug(this.GetType(), "Creating Sale");

            // header
            this.Sale.QuoteValidDate = this.ValidUntilDate;
            this.Sale.DeliveryDate = this.DeliveryDate;
            this.Sale.ShippingDate = this.ShippingDate;
            this.Sale.DocumentDate = this.DocumentDate;
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.Remarks = this.Remarks;
            this.Sale.DeviceId = NouvemGlobal.DeviceId;
            this.Sale.EditDate = DateTime.Now;
            this.Sale.BPContactID = this.SelectedContact != null ? (int?)this.SelectedContact.Details.BPContactID : null;
            this.Sale.DocketNote = this.DocketNote;
            this.Sale.PopUpNote = this.PopUpNote;
            this.Sale.InvoiceNote = this.InvoiceNote;
            this.Sale.SalesEmployeeID = this.SelectedSalesEmployee != null ? (int?)this.SelectedSalesEmployee.UserMaster.UserMasterID : null;
            this.Sale.VAT = this.Tax;
            this.Sale.SubTotalExVAT = this.SubTotal;
            this.Sale.DiscountIncVAT = this.Discount;
            this.Sale.GrandTotalIncVAT = this.Total;
            this.Sale.DiscountPercentage = this.DiscountPercentage;
            this.Sale.Customer = this.SelectedPartner;
            this.Sale.Haulier = this.SelectedHaulier;
            this.Sale.DeliveryAddress = this.SelectedDeliveryAddress;
            this.Sale.InvoiceAddress = this.SelectedInvoiceAddress;
            this.Sale.DeliveryContact = this.SelectedDeliveryContact;
            this.Sale.MainContact = this.SelectedContact;
            this.Sale.PORequired = this.PORequired;
            this.Sale.CustomerPOReference = this.CustomerPOReference;
            this.Sale.PartnerInvoice = this.PartnerInvoiceNote;
            this.Sale.PartnerPopUp = this.CustomerPopUpNote;
            if (this.SelectedDispatchContainer != null)
            {
                this.Sale.DispatchContainerID = this.SelectedDispatchContainer.DispatchContainerID;
            }
            else
            {
                this.Sale.DispatchContainerID = null;
            }

            if (this.SelectedRoute != null)
            {
                this.Sale.RouteID = this.SelectedRoute.RouteID;
            }

            // add the sale details
            this.Sale.SaleDetails = this.SaleDetails;
        }

        /// <summary>
        /// Override the selected purchase order, resetting it's totals.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localCopyValue = CopyingDocument;
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;
            this.IsBaseDocument = this.Sale.IsBaseDocument.ToBool();

            //if (this.Sale.SaleDetails != null)
            //{
            //    this.DataManager.GetDispatchTransactionData(this.Sale.SaleDetails);
            //}

            if (this.IsBaseDocument)
            {
                SystemMessage.Write(MessageType.Issue, Message.DocketInvoicedWarning);
                this.IsReadOnly = true;
                this.DisableDocStatusChange = true;
            }

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.CheckForExtraDispatchLines)
            {
                this.CheckForExtraDispatchLines();
            }

            if (!localCopyValue && !this.Sale.CopyingFromOrder)
            {
                // We only reset the totals when we are copying from an order.
                return;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.TotalExVAT = 0;
            }

            this.Tax = 0;
            this.Total = 0;
            this.SubTotal = 0;

            // we need to keep the discount % (Setting discount to 0 will reset it to 0)
            this.CalculateSalesTotals = false;
            this.Discount = 0;
        }

        /// <summary>
        /// Clears the production batch data (In batch scanning mode for tsbloors)
        /// </summary>
        protected virtual void ClearBatchData()
        {
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected override void UpdateSaleOrderTotals(bool calcDisAmountOnly = true, bool calcDisPercentOnly = false)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            // reset the totals/taxes
            this.SubTotal = 0;
            this.Total = 0;
            this.Tax = 0;

            // get the accumulated totals and taxes.
            this.SaleDetails.ToList().ForEach(item =>
            {
                this.SubTotal += item.TotalExVAT.ToDecimal();
                this.Total += item.TotalIncVAT.ToDecimal();
            });

            // Set the tax value.
            if (this.Total > this.SubTotal)
            {
                this.Tax = this.Total - this.SubTotal;
            }

            if (calcDisPercentOnly)
            {
                this.CalculateSalesTotals = false;

                // apply the discount (if any)
                if (this.DiscountPercentage > 0)
                {
                    var convertedDiscount = 100M - this.DiscountPercentage.ToDecimal();
                    this.Tax = Math.Round(this.Tax / 100 * convertedDiscount, 2);
                    var discountedTotal = Math.Round(this.Total / 100 * convertedDiscount, 2);

                    // convert the discount amount.
                    this.Discount = this.Total - discountedTotal;

                    // assign the discounted total.
                    this.Total = discountedTotal;
                }
                else
                {
                    this.Discount = 0;
                }
            }

            if (calcDisAmountOnly)
            {
                this.CalculateSalesTotals = false;

                // apply the discount amount (if any)
                if (this.Discount > 0)
                {
                    if (this.SubTotal > 0)
                    {
                        this.DiscountPercentage = Math.Round((this.Discount / this.SubTotal) * 100, 2).ToDecimal();
                        this.Total = this.Total - this.Discount;
                    }
                }
                else
                {
                    if (!this.LockDiscountPercentage)
                    {
                        this.DiscountPercentage = 0;
                    }
                    else
                    {
                        this.UpdateSaleOrderTotals(false, true);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected void UpdateSaleOrderTotals(Sale sale)
        {
            // reset the totals/taxes
            var subTotal = 0M;
            var total = 0M;
            var tax = 0;

            // get the accumulated totals and taxes.
            sale.SaleDetails.ToList().ForEach(item =>
            {
                subTotal += item.TotalExVAT.ToDecimal();
                total += item.TotalIncVAT.ToDecimal();
            });

            sale.SubTotalExVAT = subTotal;
            sale.GrandTotalIncVAT = total;
        }

        /// <summary>
        /// Handle the column focus change.
        /// </summary>
        /// <param name="e">The event arg.</param>
        protected override void OnFocusedColumnChangedCommandExecute(EditorEventArgs e)
        {
            if (this.IsReadOnly)
            {
                return;
            }

            // SaleDetail.PriceChangingOnDesktop = true;

            if (e != null && e.Column != null && e.Column.FieldName.Equals("UnitPrice"))
            {
                if (e.Editor.EditValue != null && e.Row is SaleDetail
                    && (decimal?)e.Editor.EditValue != (e.Row as SaleDetail).UnitPriceOld)
                {
                    (e.Row as SaleDetail).UnitPriceOld = (decimal?)e.Editor.EditValue;

                    try
                    {
                        var addingToCustomerBook = false;
                        var message = Message.AddUnitPriceChangeToPriceBookConfirmation;
                        if (this.SelectedSaleDetail.PriceListID != this.SelectedPartner.PriceListID)
                        {
                            addingToCustomerBook = true;
                            message = Message.AddProductAndUnitPriceChangeToPriceBookConfirmation;
                        }

                        var currency = this.SelectedPartner != null && this.SelectedPartner.Symbol != null ? this.SelectedPartner.Symbol.Trim() : ApplicationSettings.CustomerCurrency;
                        NouvemMessageBox.Show(
                            string.Format(message, currency, this.SelectedSaleDetail.UnitPrice), NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.SelectedPartner.PriceListID == 0)
                            {
                                SystemMessage.Write(MessageType.Issue, Message.NoPriceBookExists);
                                return;
                            }

                            if (this.SelectedSaleDetail.PriceListID == 0)
                            {
                                NouvemMessageBox.Show(Message.ProductNotOnAnyPriceList, NouvemMessageBoxButtons.YesNo);
                                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                                {
                                    Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
                                    this.Close();
                                }

                                return;
                            }

                            this.UpdatePriceToPriceBook();
                            //System.Threading.Tasks.Task.Factory.StartNew(this.RefreshPriceLists);
                            //this.RefreshPriceDetails();
                            if (addingToCustomerBook)
                            {
                                this.SelectedSaleDetail.LoadingSale = true;
                                this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                            }
                        }

                        //this.SelectedSaleDetail.SetPriceMethod();
                        //this.SelectedSaleDetail.UnitPrice = (decimal?) e.Editor.EditValue;
                        this.SelectedSaleDetail.UnitPriceAfterDiscount = this.SelectedSaleDetail.UnitPrice;
                        this.UpdateSaleDetailTotals();
                        if (this.Sale != null && this.Sale.SaleID > 0)
                        {
                            this.SetControlMode(ControlMode.Update);
                        }
                        else
                        {
                            this.SetControlMode(ControlMode.Add);
                        }

                        this.uncommittedPriceChange = true;
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), ex.Message);
                        SystemMessage.Write(MessageType.Issue, ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Drill down to the selected item details.
        /// </summary>
        private void DrillDownToDispatchItemsCommandExecute()
        {
            #region validation

            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            #endregion

            ApplicationSettings.TouchScreenMode = false;
      
            //this.SelectedSaleDetail.StockMode = StockMode.Serial;
            this.SelectedSaleDetail.DisplayOnly = true;
            var localSaleDetail = this.SelectedSaleDetail;

            Messenger.Default.Send(ViewType.ARDispatchDetails);
            Messenger.Default.Send(localSaleDetail, Token.SaleDetailSelected);
            this.Locator.ARDispatchDetails.IsReadOnly = this.IsReadOnly;
        }

        /// <summary>
        /// Drill down for manual serial entry.
        /// </summary>
        private void DrillDownManualSerialCommandExecute()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            #endregion

            this.DoNotHandleSelectedSaleDetail = true;
            this.SelectedSaleDetail = new SaleDetail();
            //this.SelectedSaleDetail.StockMode = StockMode.Serial;
            this.SelectedSaleDetail.DisplayOnly = false;
            this.DoNotHandleSelectedSaleDetail = false;

            Messenger.Default.Send(ViewType.ARDispatchDetails);
            Messenger.Default.Send(this.SelectedSaleDetail, Token.SaleDetailSelected);
        }

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if ((e.Cell.Property.Equals("INMasterID") || e.Cell.Property.Equals("PriceListID")) &&
               this.SelectedSaleDetail != null
               && (this.SelectedSaleDetail.WeightDelivered == null || this.SelectedSaleDetail.WeightDelivered == 0)
               && (this.SelectedSaleDetail.QuantityDelivered == null || this.SelectedSaleDetail.QuantityDelivered == 0))
            {
                this.SelectedSaleDetail.UnitPrice = null;
            }
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.OK)
            {
                this.ShowSearchGridCommandExecute(Constant.Dispatch);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            this.ShowSales();
        }

        /// <summary>
        /// Override, to set the selected detail flag.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            base.HandleSelectedSaleDetail();

            // TODO when search for the sales, the price method is not set (no selected supplier) this works, but need to fix properly.
            this.SelectedSaleDetail.UpdateTotals = false;
            this.SelectedSaleDetail.INMasterID = this.SelectedSaleDetail.INMasterID;

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;
            if (ApplicationSettings.ShowProductRemarksAtSaleOrder)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (this.SelectedSaleDetail?.InventoryItem != null &&
                        !string.IsNullOrWhiteSpace(this.SelectedSaleDetail.InventoryItem.Remarks)
                        && !this.SelectedSaleDetail.RemarksDisplayed
                        && (this.SelectedDocStatus.NouDocStatusID == 1 || this.SelectedDocStatus.NouDocStatusID == 4))
                    {
                        this.SelectedSaleDetail.RemarksDisplayed = true;
                        var localRemarks = this.SelectedSaleDetail.InventoryItem.Remarks;
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(localRemarks, isPopUp: true);
                        }));
                    }
                }));
            }
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            this.allowAutoWeigh = true;
            base.ParsePartner();

            if (this.SelectedPartner == null)
            {
                return;
            }

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.ShowPartnerPopUpNoteAtDesktopModules && !this.DisablePartnerPopUp)
            {
                if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                        //this.CustomerPopUpNote = string.Empty;
                    }));
                }
            }

            if (this.DataManager.GetPartnerOnHoldStatus(this.SelectedPartner.BPMasterID))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.PartnerOnHold, this.SelectedPartner.Name));
                    NouvemMessageBox.Show(string.Format(Message.PartnerOnHold, this.SelectedPartner.Name), touchScreen: ApplicationSettings.TouchScreenMode);
                    var localBusinessPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                    if (localBusinessPartner != null)
                    {
                        localBusinessPartner.Details.OnHold = true;
                    }

                    this.SelectedPartner = null;
                }));

                return;
            }
            else
            {
                var localBusinessPartner =
                   NouvemGlobal.BusinessPartners.FirstOrDefault(
                       x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                if (localBusinessPartner != null)
                {
                    localBusinessPartner.Details.OnHold = false;
                }
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    try
                    {
                        int? productId = null;
                        int? productGroupId = null;

                        if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
                        {
                            productId = this.SelectedSaleDetail.InventoryItem.Master.INMasterID;
                            productGroupId = this.SelectedSaleDetail.InventoryItem.Master.INGroupID;
                        }

                        this.LabelManager.GetLabelAndDisplay(this.SelectedPartner.BPMasterID, this.SelectedPartner.BPGroupID, productId, productGroupId, LabelType.Item);
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), ex.Message);
                    }
                }), DispatcherPriority.Background);
            }

            if (ApplicationSettings.TouchScreenMode)
            {
                if (!string.IsNullOrWhiteSpace(this.SelectedPartner.PopUpNotes))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(this.SelectedPartner.PopUpNotes, touchScreen: true);
                    }));
                }

                return;
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetRecentDispatches(localPartner.BPMasterID);
                this.RecentOrdersCount = 0;
                this.SetRecentOrders(true);
            }));
        }

        /// <summary>
        /// Overrides the mode selection, to disable user add.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            //if (this.CurrentMode == ControlMode.Add)
            //{
            //    this.SetControlMode(ControlMode.OK);
            //}
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddDesktopSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateOrder();
                    Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        public void AddTransaction()
        {
            // ensure the price list data is set (this relates to the touchscreen only)
            if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.PriceListID == 0)
            {
                this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
            }

            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                //error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(error, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(error, touchScreen: true);
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, error);
                }

                return;
            }

            #endregion

            this.CreateSale();
            if (this.Sale.SaleID == 0)
            {
                this.AddSale();
            }
            else
            {
                this.UpdateSale();
            }
        }

        /// <summary>
        /// Completes a transaction, check the orders live status.
        /// </summary>
        /// <param name="wgt">The weight to apply.</param>
        /// <param name="qty">The qty to apply.</param>
        protected void CompleteTransaction(decimal wgt, decimal qty)
        {
            if (qty <= 0)
            {
                qty = 1;
            }

            if (this.SelectedSaleDetail.WeightDelivered == null)
            {
                this.SelectedSaleDetail.WeightDelivered = this.SelectedSaleDetail.WeightDelivered.ToDecimal();
            }

            if (this.SelectedSaleDetail.QuantityDelivered == null)
            {
                this.SelectedSaleDetail.QuantityDelivered = this.SelectedSaleDetail.QuantityDelivered.ToDecimal();
            }

            Sale liveUpdate = null;

            //if (ApplicationSettings.GetLiveOrderStatusDispatch)
            // {
            liveUpdate = this.GetLiveOrderStatus();
            // }

            if (liveUpdate != null)
            {
                var localSaleDetails = this.SaleDetails;
                var localSelectedSaleDetail = this.SelectedSaleDetail;

                this.displayPopUpNote = false;
                this.Sale = liveUpdate;
                if (this.SaleDetails.All(x => x.INMasterID != localSelectedSaleDetail.INMasterID))
                {
                    this.SaleDetails.Add(localSelectedSaleDetail);
                }

                this.DoNotHandleSelectedSaleDetail = true;
                this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localSelectedSaleDetail.INMasterID);
                this.DoNotHandleSelectedSaleDetail = false;
                this.SelectedSaleDetail.Concessions = localSelectedSaleDetail.Concessions;
                this.SelectedSaleDetail.DispatchOverAmountWarningGiven = localSelectedSaleDetail.DispatchOverAmountWarningGiven;
                if (this.SelectedSaleDetail.GoodsReceiptDatas == null ||
                    !this.SelectedSaleDetail.GoodsReceiptDatas.Any())
                {
                    this.SelectedSaleDetail.GoodsReceiptDatas = localSelectedSaleDetail.GoodsReceiptDatas;
                }

                foreach (var localDetail in this.SaleDetails.Where(x => x.GoodsReceiptDatas == null || !x.GoodsReceiptDatas.Any()))
                {
                    var matchingDetail = localSaleDetails.FirstOrDefault(x => x.INMasterID == localDetail.INMasterID);
                    if (matchingDetail != null)
                    {
                        localDetail.GoodsReceiptDatas = matchingDetail.GoodsReceiptDatas;
                    }
                }

                this.Sale.NouDocStatusID = liveUpdate.NouDocStatusID;
                if (liveUpdate.OrderStatus == OrderStatus.Complete)
                {
                    var deviceName = liveUpdate.Device != null ? liveUpdate.Device.DeviceName : string.Empty;
                    this.Log.LogDebug(this.GetType(), string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), touchScreen: true);
                    }

                    return;
                }

                if (liveUpdate.OrderStatus == OrderStatus.InProgress || liveUpdate.OrderStatus == OrderStatus.Filled || liveUpdate.OrderStatus == OrderStatus.Active)
                {
                    this.Log.LogDebug(this.GetType(), "Order in progress");

                    if (liveUpdate.SaleDetails.Any())
                    {
                        var matchSaleDetail =
                            liveUpdate.SaleDetails.FirstOrDefault(
                                x => x.SaleDetailID == this.SelectedSaleDetail.SaleDetailID);

                        if (matchSaleDetail != null)
                        {
                            this.Log.LogDebug(this.GetType(), "Checking if received amounts have been updated at another terminal");

                            if ((matchSaleDetail.WeightDelivered.ToDouble() > this.SelectedSaleDetail.WeightDelivered.ToDouble()
                                || matchSaleDetail.QuantityDelivered.ToDouble() > this.SelectedSaleDetail.QuantityDelivered.ToDouble())
                                || (matchSaleDetail.WeightDelivered.ToDouble() < this.SelectedSaleDetail.WeightDelivered.ToDouble()
                                || matchSaleDetail.QuantityDelivered.ToDouble() < this.SelectedSaleDetail.QuantityDelivered.ToDouble()))
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("Updating Qty:{0}, Wgt:{1}", matchSaleDetail.QuantityDelivered, matchSaleDetail.WeightDelivered));

                                this.SelectedSaleDetail.WeightDelivered = matchSaleDetail.WeightDelivered;
                                this.SelectedSaleDetail.QuantityDelivered = matchSaleDetail.QuantityDelivered;

                                SystemMessage.Write(MessageType.Priority, Message.OrderItemUpdatedElsewhere);
                                if (ApplicationSettings.DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox)
                                {
                                    if (ApplicationSettings.ScannerMode)
                                    {
                                        NouvemMessageBox.Show(Message.OrderItemUpdatedElsewhere, scanner: true);
                                    }
                                    else
                                    {
                                        NouvemMessageBox.Show(Message.OrderItemUpdatedElsewhere, touchScreen: true);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!ApplicationSettings.AllowDispatchOverAmount)
            {
                var wgtOrdered = this.SelectedSaleDetail.WeightOrdered.ToDecimal();
                var qtyOrdered = this.SelectedSaleDetail.QuantityOrdered.ToDecimal();
                var wgtDel = this.SelectedSaleDetail.WeightDelivered.ToDecimal() + wgt;
                var qtyDel = this.SelectedSaleDetail.QuantityDelivered.ToDecimal() + qty;
                var overAmount = false;
                if (this.SelectedSaleDetail.NouOrderMethod == OrderMethod.Weight)
                {
                    if (wgtOrdered > 0 && wgtDel > wgtOrdered)
                    {
                        overAmount = true;
                    }
                }
                else if (this.SelectedSaleDetail.NouOrderMethod == OrderMethod.Quantity)
                {
                    if (qtyOrdered > 0 && qtyDel > qtyOrdered)
                    {
                        overAmount = true;
                    }
                }
                else
                {
                    if ((wgtOrdered > 0 && wgtDel > wgtOrdered)
                        || (qtyOrdered > 0 && qtyDel > qtyOrdered))
                    {
                        overAmount = true;
                    }
                }

                if (overAmount)
                {
                    if (ApplicationSettings.DispatchOverAmountHardStop)
                    {
                        // hard stop. user cannot proceed.
                        SystemMessage.Write(MessageType.Issue, Message.DispatchOverAmountHardStopWarning);
                        NouvemMessageBox.Show(Message.DispatchOverAmountHardStopWarning, touchScreen: true);
                        return;
                    }

                    // soft stop. user has choice.
                    if (!this.SelectedSaleDetail.DispatchOverAmountWarningGiven)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.DispatchOverOrderAmtWarning);
                        NouvemMessageBox.Show(Message.DispatchOverOrderAmtWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }

                        this.SelectedSaleDetail.DispatchOverAmountWarningGiven = true;
                    }
                }
            }

            if (wgt > 0)
            {
                this.SelectedSaleDetail.WeightDelivered = Math.Round(this.SelectedSaleDetail.WeightDelivered.ToDecimal() + wgt, 5);
            }

            if (qty > 0)
            {
                this.SelectedSaleDetail.QuantityDelivered += qty;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;

            this.ProductQuantity = 0;
            this.AddTransaction();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            try
            {
                if (!this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
                {
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    this.SetControlMode(ControlMode.OK);
                    if (ApplicationSettings.TouchScreenMode)
                    {
                        if (ApplicationSettings.ScannerMode)
                        {
                            NouvemMessageBox.Show(Message.AccessDenied, scanner: true);
                        }
                        else
                        {
                            NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                        }
                    }

                    return;
                }

                this.OrderUpdateInProgress = true;

                #region validation

                var error = string.Empty;

                if (ApplicationSettings.RouteMustBeSetOnOrder && this.SelectedRoute == null)
                {
                    error = Message.SelectRoute;
                }
                else if (ApplicationSettings.CheckRoutesAtOrder && this.SelectedRoute != null)
                {
                    error = this.CheckRoutesAtOrder();
                }
                else if (ApplicationSettings.DeliveryDateCannotBeInThePast && this.DeliveryDate.HasValue && this.DeliveryDate < DateTime.Today)
                {
                    error = Message.DeliveryDateInPastError;
                }

                if (ApplicationSettings.ShippingDateMustBeAfterDeliveryDate && this.DeliveryDate.HasValue && this.ShippingDate.HasValue)
                {
                    if (this.DeliveryDate < this.ShippingDate)
                    {
                        error = Message.ShippingDateBeforeDeliveryDate;
                    }
                }

                if (ApplicationSettings.DefaultShippingDateToPreviousDay)
                {
                    if (this.ShippingDate.ToDate() < DateTime.Today)
                    {
                        error = Message.InvalidShippingDate;
                    }
                }

                if (error != string.Empty)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: ApplicationSettings.TouchScreenMode);
                    return;
                }

                if (this.ScannerStockMode == ScannerMode.ReWeigh)
                {
                    var detail = this.Sale.SaleDetails.FirstOrDefault(x => x.Process);
                    if (detail != null)
                    {
                        var data = detail.GoodsReceiptDatas.FirstOrDefault();
                        if (data != null)
                        {
                            var transData = data.TransactionData.FirstOrDefault();
                            if (transData != null && transData.StockAttribute == null)
                            {
                                this.Log.LogDebug(this.GetType(), "Stock attribute null...retrieving");
                                transData.StockAttribute =
                                     this.DataManager.GetTransactionAttributesCarcassSerial(transData.Serial);
                            }
                        }
                    }
                }

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate)
                {
                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice.IsNullOrZero()))
                    {
                        NouvemMessageBox.Show(Message.NoPriceWarningOnOrderLines, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }

                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.PriceListID == 0))
                    {
                        NouvemMessageBox.Show(Message.NoPriceBookFoundOnOrderLine, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }
                }

                if (!this.CheckOrderLimit())
                {
                    return;
                }

                #endregion

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.CheckForOutstandingOrders)
                {
                    var localSales = this.DataManager.GetOpenARDispatches(this.SelectedPartner.BPMasterID);
                    Sale matchingSale = null;
                    var match = true;
                    foreach (var localSale in localSales)
                    {
                        if (localSale.SaleDetails.Count != this.Sale.SaleDetails.Count)
                        {
                            continue;
                        }

                        foreach (var localSaleDetail in localSale.SaleDetails)
                        {
                            var existingDetail =
                                this.Sale.SaleDetails.FirstOrDefault(x => x.INMasterID == localSaleDetail.INMasterID);
                            if (existingDetail == null)
                            {
                                match = false;
                                break;
                            }

                            if (existingDetail.QuantityOrdered != localSaleDetail.QuantityOrdered
                                || existingDetail.WeightOrdered != localSaleDetail.WeightOrdered)
                            {
                                match = false;
                                break;
                            }
                        }

                        if (match)
                        {
                            matchingSale = localSale;
                            break;
                        }
                    }

                    if (matchingSale != null)
                    {
                        NouvemMessageBox.Show(Message.DuplicateOrderDetected, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.SetActiveDocument(this);
                            IList<Sale> localSaleToSend = new List<Sale> { matchingSale };
                            Messenger.Default.Send(Tuple.Create(localSaleToSend, ViewType.ARDispatch), Token.SearchForSale);

                            if (this.Sale != null)
                            {
                                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch2);
                            }

                            return;
                        }
                    }
                }

                this.Log.LogWarning(this.GetType(), "6. AddSale(): adding a new dispatch docket");
                this.UpdateDocNumbering();

                var newSaleId = this.DataManager.AddAPDispatch(this.Sale);

                if (newSaleId > 0)
                {
                    this.LastEdit = DateTime.Now;
                    this.TransactionCount++;
                    this.Log.LogDebug(this.GetType(), "Docket created");
                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight && this.Sale.ReturnedStockTransactionId > 0)
                    {
                        if (this.SelectedSaleDetail != null && !this.SelectedSaleDetail.DontPrintLabel)
                        {
                            this.Print(this.Sale.ReturnedStockTransactionId);
                            this.PrintBoxLabel();
                        }
                    }

                    this.OrderNo = this.Sale.Number;
                    this.Sale.SaleID = newSaleId;
                    //this.AllSales.Add(this.Sale);
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DispatchDocketCreated, this.NextNumber));
                    this.RefreshGoodsIn();
                    //SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                    Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);

                    // send message the the order screen (if copying order to dispatch) of successful goods receipt addition
                    Messenger.Default.Send(Token.Message, Token.CopySuccessful);

                    if (ApplicationSettings.TouchScreenMode)
                    {
                        // this.SelectedSaleDetail = null;
                        var localDetail = this.SelectedSaleDetail;
                        this.SelectedSaleDetail = null;
                        this.clearTraceability = false;
                        this.Locator.Touchscreen.UIText = this.Sale.Number.ToString();

                        if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                        {
                            if (this.scanningLabelForBatchNo)
                            {
                                // clear the batch data (we're in scan multivac label mode)
                                this.ClearBatchData();
                            }

                            this.highlightSelectedProduct = true;
                            this.SelectedSaleDetail = localDetail;
                            this.SetLineStatus(this.SelectedSaleDetail);
                            this.clearTraceability = true;
                        }
                        else
                        {
                            if (this.ScannerStockMode == ScannerMode.ReWeigh &&
                                ApplicationSettings.SelectingProductInReweighMode)
                            {
                                this.DoNotHandleSelectedSaleDetail = true;
                                this.SelectedSaleDetail = localDetail;
                                this.DoNotHandleSelectedSaleDetail = false;
                            }
                        }
                    }
                    else
                    {
                        this.ClearForm();
                    }

                    if (this.applyTares)
                    {
                        // applying tares from the calculator
                        this.applyTares = false;
                        Messenger.Default.Send(Token.Message, Token.AddTare);
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DispatchDocketNotCreated);
                }
            }
            finally
            {
                this.OrderUpdateInProgress = false;
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <param name="labelType">The label type.</param>
        protected void Print(int stockTransactionId, LabelType labelType = LabelType.Item)
        {
            int? productId = null;
            int? productGroupId = null;
            int? customerId = this.SelectedPartner.BPMasterID;
            int? customerGroupId = this.SelectedPartner.BPGroupID;

            if (labelType != LabelType.Shipping && this.SelectedSaleDetail.InventoryItem != null &&
                this.SelectedSaleDetail.InventoryItem.Master != null)
            {
                productId = this.SelectedSaleDetail.InventoryItem.Master.INMasterID;
                productGroupId = this.SelectedSaleDetail.InventoryItem.Master.INGroupID;
            }

            try
            {
                var labels = this.PrintManager.ProcessPrinting(customerId, customerGroupId, productId, productGroupId, ViewType.ARDispatch, labelType, stockTransactionId, ApplicationSettings.OutOfProductionLabelsToPrint);

                if (labels.Item1.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.DataManager.AddLabelToTransaction(stockTransactionId,
                            string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                    }), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Cancel the current order.
        /// </summary>
        protected void CancelOrder()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusCancelled.NouDocStatusID))
            {
                this.LastEdit = DateTime.Now;
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCancelled, this.Sale.Number));
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.OrderNotCancelled);
            }
        }


        /// <summary>
        /// Completes the current order.
        /// </summary>
        protected void CompleteOrder(bool updatingFromDesktop = false)
        {
            this.Log.LogDebug(this.GetType(), "CompleteOrder()...Attempting to complete the order");
            this.displayPopUpNote = false;

            if (!updatingFromDesktop)
            {
                var localSale = this.GetLiveOrderStatus();
                if (localSale == null)
                {
                    return;
                }

                var localSaleDetails = this.SaleDetails;
                this.Sale = localSale;
                this.displayPopUpNote = true;

                foreach (var localDetail in this.SaleDetails.Where(x => x.GoodsReceiptDatas == null || !x.GoodsReceiptDatas.Any()))
                {
                    var matchingDetail = localSaleDetails.FirstOrDefault(x => x.INMasterID == localDetail.INMasterID);
                    if (matchingDetail != null)
                    {
                        localDetail.GoodsReceiptDatas = matchingDetail.GoodsReceiptDatas;
                    }
                }

                if (this.SaleDetails.Any(x => x.GoodsReceiptDatas == null || !x.GoodsReceiptDatas.Any()))
                {
                    this.DataManager.GetDispatchTransactionData(this.Sale.SaleDetails);
                }
            }

            try
            {
                #region validation

                SaleDetail missingTransactionSaleDetail = null;
                var error = string.Empty;

                if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        error = Message.NoOrderHasBeenSelected;
                    }
                    else
                    {
                        error = Message.NoPartnerSelected;
                    }
                }
                else if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    error = Message.EmptyOrder;
                }
                //else
                //{
                //    foreach (var saleDetail in this.SaleDetails)
                //    {
                //        if (!saleDetail.GoodsReceiptDatas.Any() || saleDetail.GoodsReceiptDatas.Any(x => !x.TransactionData.Any()))
                //        {
                //            var code = saleDetail.InventoryItem != null ? saleDetail.InventoryItem.Code : string.Empty;
                //            var product = saleDetail.InventoryItem != null ? saleDetail.InventoryItem.Name : string.Empty;
                //            error =
                //                Settings.Default.TouchScreenMode
                //                    ? string.Format("{0}{1}", Message.SaleDetailNotRecordedTouchscreen, product)
                //                    : string.Format("{0}{1}", Message.SaleDetailNotRecorded, code);
                //            missingTransactionSaleDetail = saleDetail;
                //            break;
                //        }

                //        if (saleDetail.GoodsReceiptDatas == null || !saleDetail.GoodsReceiptDatas.Any())
                //        {
                //            var code = saleDetail.InventoryItem != null ? saleDetail.InventoryItem.Code : String.Empty;
                //            var product = saleDetail.InventoryItem != null ? saleDetail.InventoryItem.Name : String.Empty;
                //            error =
                //                Settings.Default.TouchScreenMode
                //                    ? String.Format("{0}{1}", Message.MissingTraceabilityValuesTouchscreen, product)
                //                    : String.Format("{0}{1}", Message.MissingTraceabilityValues, code);
                //            missingTransactionSaleDetail = saleDetail;
                //            break;
                //        }

                //        foreach (var goodsReceiptData in saleDetail.GoodsReceiptDatas)
                //        {
                //            if (goodsReceiptData.TransactionData == null)
                //            {
                //                var code = saleDetail.InventoryItem != null ? saleDetail.InventoryItem.Code : string.Empty;
                //                var product = saleDetail.InventoryItem != null
                //                    ? saleDetail.InventoryItem.Name
                //                    : string.Empty;
                //                error =
                //                    Settings.Default.TouchScreenMode
                //                        ? string.Format("{0}{1}", Message.MissingTraceabilityValuesTouchscreen, product)
                //                        : string.Format("{0}{1}", Message.MissingTraceabilityValues, code);
                //                missingTransactionSaleDetail = saleDetail;
                //                break;
                //            }
                //        }
                //    }
                //}

                if (error != string.Empty)
                {
                    if (ApplicationSettings.TouchScreenMode)
                    {
                        SystemMessage.Write(MessageType.Issue, error);

                        if (ApplicationSettings.ScannerMode)
                        {
                            NouvemMessageBox.Show(error, scanner: true);
                        }
                        else
                        {
                            NouvemMessageBox.Show(error, touchScreen: true);
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, error);
                        if (missingTransactionSaleDetail != null)
                        {
                            this.SelectedSaleDetail = missingTransactionSaleDetail;
                            Messenger.Default.Send(ViewType.ARDispatchDetails);
                            Messenger.Default.Send(this.SelectedSaleDetail, Token.SaleDetailSelected);
                        }
                    }

                    return;
                }

                #endregion

                #region empty order

                if (ApplicationSettings.TouchScreenMode && this.SaleDetails != null)
                {
                    if (this.SaleDetails.All(x => x.IsDispatchLineEmpty))
                    {
                        NouvemMessageBox.Show(Message.DispatchOrderEmptyWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusCancelled.NouDocStatusID))
                            {
                                this.ClearForm();
                                this.PartnerName = Strings.SelectCustomer;
                                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                                this.RefreshDocStatusItems();
                                Messenger.Default.Send(Token.Message, Token.ClearData);
                            }
                            else
                            {
                                SystemMessage.Write(MessageType.Priority, Message.OrderNotCancelled);
                            }
                        }

                        return;
                    }
                }

                #endregion

                var displayStandardCompletionMessage = true;

                if (ApplicationSettings.CheckForPartBoxesAtDispatch)
                {
                    var halfBoxes = this.DataManager.CheckBoxTransactions(this.SaleDetails);
                    if (halfBoxes.Any())
                    {
                        var message = halfBoxes.Count == 1
                            ? Message.PartBoxOrderCompletionVerification
                            : Message.PartBoxesOrderCompletionVerification;

                        NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            foreach (var item in halfBoxes)
                            {
                                int? batchId = null;
                                if (item.GoodsReceiptDatas.Any() && item.GoodsReceiptDatas.First().BatchNumber != null)
                                {
                                    batchId = item.GoodsReceiptDatas.First().BatchNumber.BatchNumberID;
                                }
                                else
                                {
                                    this.Log.LogError(this.GetType(), string.Format("No batch found:INMasterID:{0}, Dispatch Detail id:{1}", item.INMasterID, item.SaleDetailID));
                                }

                                var trans = new StockTransaction
                                {
                                    INMasterID = item.INMasterID,
                                    MasterTableID = item.SaleDetailID,
                                    WarehouseID = NouvemGlobal.DispatchId,
                                    UserMasterID = NouvemGlobal.UserId,
                                    DeviceMasterID = NouvemGlobal.DeviceId,
                                    TransactionDate = DateTime.Now,
                                    IsBox = true,
                                    BatchNumberID = batchId,
                                    NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId
                                };

                                try
                                {
                                    this.DoNotHandleSelectedSaleDetail = true;
                                    this.SelectedSaleDetail = item;
                                    this.DoNotHandleSelectedSaleDetail = false;
                                    var transactionIds = this.DataManager.AddBoxTransactionOnCompleteOrder(trans, -1);
                                    if (transactionIds.Any())
                                    {
                                        foreach (var transactionId in transactionIds)
                                        {
                                            this.Print(transactionId, LabelType.Box);
                                            SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.Log.LogError(this.GetType(), ex.Message);
                                    SystemMessage.Write(MessageType.Issue, ex.Message);
                                }
                            }
                        }
                    }
                }

                var touchscreen = ApplicationSettings.TouchScreenMode && !ApplicationSettings.ScannerMode;
                if (ApplicationSettings.DispatchOrderNotFilledWarning)
                {
                    if (!this.IsOrderFilled())
                    {
                        if (ApplicationSettings.UnfilledDispatchCompletionAuthorisationCheck)
                        {
                            // need to check if user is authorised to complete unfilled orders.
                            var isAuthorised = this.AuthorisationsManager.AllowUserToCompleteAnUnfilledOrder;

                            if (!isAuthorised)
                            {
                                NouvemMessageBox.Show(Message.UnfilledOrderCompletionAuthorisationRequired, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                                //Messenger.Default.Send(Tuple.Create(ViewType.KeyboardPassword, ViewType.ARDispatchAuthorisation));
                                this.Locator.LoginUser.Authorisation = Authorisation.AllowUserToCompleteAnUnfilledOrder;
                                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenUsers);
                                return;
                            }
                        }

                        displayStandardCompletionMessage = false;
                        NouvemMessageBox.Show(Message.UnfilledOrderWarning, NouvemMessageBoxButtons.OKCancel, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                        if (NouvemMessageBox.UserSelection != UserDialogue.OK)
                        {
                            return;
                        }
                    }
                }

                if (displayStandardCompletionMessage)
                {
                    NouvemMessageBox.Show(Message.VerifyOrderCompletion, NouvemMessageBoxButtons.OKCancel, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                    if (NouvemMessageBox.UserSelection != UserDialogue.OK)
                    {
                        return;
                    }
                }

                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();

                //this.CompleteOrderCheck();
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                this.Sale.DocketNote = this.DocketNote;
                this.Log.LogDebug(this.GetType(), string.Format("Attempting to change the nou doc status id to:{0}", NouvemGlobal.NouDocStatusComplete.NouDocStatusID));
                if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusComplete.NouDocStatusID))
                {
                    this.LastEdit = DateTime.Now;
                    var docketId = this.Sale.SaleID;
                    this.Log.LogDebug(this.GetType(), string.Format("Order {0} has been successfully completed. AutoCopyToInvoive:{1}, AutoCreateInvoiceOnOrderCompletion:{2} ", docketId, this.AutoCopyToInvoice, ApplicationSettings.AutoCreateInvoiceOnOrderCompletion));
                    if (this.AutoCopyToInvoice || ApplicationSettings.AutoCreateInvoiceOnOrderCompletion)
                    {
                        ProgressBar.Run();
                        this.Log.LogDebug(this.GetType(), "CompleteOrder(): Auto copying to an invoice");
                        this.AutoCopyDispatchToInvoice();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                        this.DataManager.PriceDispatchDocket(docketId);
                    }

                    this.ClearForm();
                    this.SelectedDispatchContainer = null;
                    this.PartnerName = Strings.SelectCustomer;
                    this.NextNumber = this.SelectedDocNumbering.NextNumber;
                    this.RefreshDocStatusItems();
                    Messenger.Default.Send(Token.Message, Token.ClearAndResetData);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        // refresh the dispatch continer ecreen if open
                        Messenger.Default.Send(Token.Message, Token.RefreshDispatchContainers);
                    }));

                    if (ApplicationSettings.PrintReportOnCompletion && !ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.PrintDocketPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true, showCounter: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (ApplicationSettings.AutoPrintDispatchReport)
                            {
                                this.PrintReport(docketId);
                            }

                            if (ApplicationSettings.AutoPrintDispatchDetailReport)
                            {
                                this.PrintDispatchDetailReport(docketId);
                            }

                            if (ApplicationSettings.AutoPrintInvoiceReport)
                            {
                                if (this.Locator.Invoice.CurrentInvoiceId > 0)
                                {
                                    var id = this.Locator.Invoice.CurrentInvoiceId;
                                    this.PrintInvoiceReport(id);
                                }
                            }
                        }
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
                }
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                if (this.Sale.BaseDocumentReferenceID.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                    return;
                }

                this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                if (ApplicationSettings.TouchScreenMode)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.AccessDenied, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                    }
                }

                return;
            }

            string error = String.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }
            else if (ApplicationSettings.DeliveryDateCannotBeInThePast && this.DeliveryDate.HasValue && this.DeliveryDate < DateTime.Today)
            {
                error = Message.DeliveryDateInPastError;
            }
            else if (ApplicationSettings.ShippingDateMustBeAfterDeliveryDate && this.DeliveryDate.HasValue && this.ShippingDate.HasValue)
            {
                if (this.DeliveryDate < this.ShippingDate)
                {
                    error = Message.ShippingDateBeforeDeliveryDate;
                }
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error, touchScreen: ApplicationSettings.TouchScreenMode);
                return;
            }

            if (this.ScannerStockMode == ScannerMode.ReWeigh)
            {
                var detail = this.Sale.SaleDetails.FirstOrDefault(x => x.Process);
                if (detail != null)
                {
                    var data = detail.GoodsReceiptDatas.FirstOrDefault();
                    if (data != null)
                    {
                        var transData = data.TransactionData.FirstOrDefault();
                        if (transData != null && transData.StockAttribute == null)
                        {
                            this.Log.LogDebug(this.GetType(), "Stock attribute null...retrieving");
                            transData.StockAttribute =
                                this.DataManager.GetTransactionAttributesCarcassSerial(transData.Serial);
                        }
                    }
                }
            }

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate)
            {
                if (this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice.IsNullOrZero()))
                {
                    NouvemMessageBox.Show(Message.NoPriceWarningOnOrderLines, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }
                }

                if (this.SaleDetails != null && this.SaleDetails.Any(x => x.PriceListID == 0))
                {
                    NouvemMessageBox.Show(Message.NoPriceBookFoundOnOrderLine, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }
                }
            }

            #endregion

            if (this.DataManager.UpdateARDispatch(this.Sale))
            {
                this.LastEdit = DateTime.Now;
                this.TransactionCount++;

                if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight && this.Sale.ReturnedStockTransactionId > 0)
                {
                    if (this.SelectedSaleDetail != null && !this.SelectedSaleDetail.DontPrintLabel)
                    {
                        this.Print(this.Sale.ReturnedStockTransactionId);
                        this.PrintBoxLabel();
                    }
                }

                SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
                Messenger.Default.Send(Token.Message, Token.ResetSerialData);

                if (ApplicationSettings.TouchScreenMode)
                {
                    // this.SelectedSaleDetail = null;
                    var localDetail = this.SelectedSaleDetail;
                    this.SelectedSaleDetail = null;

                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                    {
                        if (this.scanningLabelForBatchNo)
                        {
                            // clear the batch data (we're in scan multivac label mode)
                            this.ClearBatchData();
                        }

                        this.highlightSelectedProduct = true;
                        this.clearTraceability = false;
                        this.SelectedSaleDetail = localDetail;
                        this.SetLineStatus(this.SelectedSaleDetail);
                        this.clearTraceability = true;
                    }
                    else
                    {
                        if (this.ScannerStockMode == ScannerMode.ReWeigh &&
                            ApplicationSettings.SelectingProductInReweighMode)
                        {
                            this.DoNotHandleSelectedSaleDetail = true;
                            this.SelectedSaleDetail = localDetail;
                            this.DoNotHandleSelectedSaleDetail = false;
                        }
                    }
                }

                if (this.applyTares)
                {
                    // applying tares from the calculator
                    this.applyTares = false;
                    Messenger.Default.Send(Token.Message, Token.AddTare);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, String.Format(Message.GoodsInNotCreated, this.NextNumber));
            }

            this.uncommittedPriceChange = false;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.ARDelivery)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(ApplicationSettings.DispatchDocketNumberType));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        protected override void AllowWindowToClose()
        {
            this.CanCloseForm = true;
            Messenger.Default.Send(true, Token.CanCloseDispatch2Window);
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (!this.CanCloseForm)
            {
                this.CheckForUnsavedData();
                return;
            }

            ViewModelLocator.ClearARDispatch2();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseDispatch2Window);
                Messenger.Default.Send(Token.Message, Token.SetActiveDocument);
                Messenger.Default.Unregister(this);
            }));           
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoQuoteSelected);
                return;
            }

            #endregion

            if (this.SelectedDocumentCopyTo.Equals(Strings.Invoice))
            {
                CopyingDocument = true;
                this.CopyDispatchToInvoice();
            }
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var documents = this.DataManager.GetOrders(this.SelectedPartner.BPMasterID);

            if (!documents.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Orders));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(documents, ViewType.ARDispatch), Token.SearchForSale);
            CopyingDocument = true;
            this.LockDiscountPercentage = true;
        }

        #endregion

        #region virtual

        #endregion

        #region method

        /// <summary>
        /// Handles the update request.
        /// </summary>
        protected void UpdateOrder()
        {
            if (this.IsBaseDocument)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.OrderUpdateInProgress)
            {
                return;
            }

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.OrderUpdateInProgress = true;

            try
            {
                this.Sale.VAT = this.Tax;
                this.Sale.SubTotalExVAT = this.SubTotal;
                this.Sale.DiscountIncVAT = this.Discount;
                this.Sale.GrandTotalIncVAT = this.Total;
                this.Sale.DiscountPercentage = this.DiscountPercentage;
                this.Sale.Remarks = this.Remarks;
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                this.Sale.DeliveryDate = this.DeliveryDate;
                this.Sale.ShippingDate = this.ShippingDate;
                this.Sale.DocumentDate = this.DocumentDate;
                if (this.SelectedRoute != null)
                {
                    this.Sale.RouteID = this.SelectedRoute.RouteID;
                }

                if (this.SelectedDeliveryAddress != null)
                {
                    this.Sale.DeliveryContact = this.SelectedDeliveryContact;
                }

                if (this.SelectedContact != null)
                {
                    this.Sale.MainContact = this.SelectedContact;
                }

                if (this.SelectedDeliveryAddress != null)
                {
                    this.Sale.DeliveryAddress = this.SelectedDeliveryAddress;
                }

                if (this.SelectedInvoiceAddress != null)
                {
                    this.Sale.InvoiceAddress = this.SelectedInvoiceAddress;
                }

                if (this.SelectedHaulier != null)
                {
                    this.Sale.Haulier = this.SelectedHaulier;
                }

                this.Sale.SaleDetails = this.SaleDetails;

                if (!this.CheckOrderLimit())
                {
                    return;
                }

                if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                    || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusFilled.NouDocStatusID
                    || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                    || this.uncommittedPriceChange)
                {
                    var localDocStatus = this.SelectedDocStatus.NouDocStatusID;
                    if (this.uncommittedPriceChange)
                    {
                        localDocStatus = NouvemGlobal.NouDocStatusFilled.NouDocStatusID;
                    }

                    if (this.DataManager.ChangeARDispatchStatus(this.Sale, localDocStatus))
                    {
                        if (this.uncommittedPriceChange)
                        {
                            this.uncommittedPriceChange = false;
                            //this.UpdateOrder();
                            //return;
                        }

                        this.LastEdit = DateTime.Now;
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.Sale.Number));
                        Task.Factory.StartNew(this.GetAllSales);
                        this.ClearForm();
                        this.RefreshDocStatusItems();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.OrderNotUpdated);
                    }
                }
                else if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    var localsaleId = this.Sale.SaleID;
                    this.CompleteOrder(true);
                    this.DataManager.PriceDispatchDocket(localsaleId);
                }
                else if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                {
                    this.CancelOrder();
                    return;
                }

                if (ApplicationSettings.DefaultToFindAtDesktopDispatch)
                {
                    this.SetControlMode(ControlMode.Find);
                }
                else
                {
                    this.SetControlMode(ControlMode.Add);
                }
            }
            finally
            {
                this.OrderUpdateInProgress = false;

                if (this.Locator.SalesSearchData.IsFormLoaded)
                {
                    this.RefreshSales();
                }
            }
        }

        /// <summary>
        /// Gets the current orders live status.
        /// </summary>
        /// <returns>The current orders live status.</returns>
        protected Sale GetLiveOrderStatus()
        {
            if (this.Sale == null)
            {
                return null;
            }

            var localSale = this.DataManager.GetARDispatchesById(this.Sale.SaleID);
            return localSale;
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Moves back to the main dispatch screen.
        /// </summary>
        private void MoveBackCommandExecute()
        {
            if ((!string.IsNullOrWhiteSpace(this.CustomerPOReference) ||
                    !string.IsNullOrWhiteSpace(this.DocketNote))
                    || this.SelectedDispatchContainer != null && this.Sale.SaleID > 0)
            {
                this.Sale.DocketNote = this.DocketNote;
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                if (this.SelectedDispatchContainer != null)
                {
                    this.Sale.DispatchContainerID = this.SelectedDispatchContainer.DispatchContainerID;
                }

                if (this.DataManager.UpdateNotes(this.Sale))
                {
                    SystemMessage.Write(MessageType.Background, Message.NotesUpdated);
                }
            }

            Messenger.Default.Send(Token.Message, Token.CloseNotesWindow);
        }

        /// <summary>
        /// Handle the box label command.
        /// </summary>
        private void PrintBoxLabel()
        {
            #region validation

            if (this.SelectedSaleDetail == null || this.SelectedSaleDetail.GoodsReceiptDatas == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductSelected);
                return;
            }

            if (!this.SelectedSaleDetail.GoodsReceiptDatas.Any())
            {
                return;
            }

            #endregion

            // add the transaction weight to our stock transaction, and other relevant data.
            var localGoodsReceiptData = this.SelectedSaleDetail.GoodsReceiptDatas.First();
            var qtyPerBox = this.SelectedSaleDetail.InventoryItem.Master.QtyPerBox.ToInt();
            if (qtyPerBox == 0)
            {
                return;
            }

            var trans = new StockTransaction
            {
                BatchNumberID = localGoodsReceiptData.BatchNumber.BatchNumberID,
                MasterTableID = this.SelectedSaleDetail.SaleDetailID,
                TransactionDate = DateTime.Now,
                INMasterID = this.SelectedSaleDetail.InventoryItem.Master.INMasterID,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId,
                WarehouseID = NouvemGlobal.DispatchId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = true
            };

            try
            {
                var transactionId = this.DataManager.AddBoxTransaction(trans, qtyPerBox);
                if (transactionId > 0)
                {
                    this.Print(transactionId, LabelType.Box);
                    SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.BoxLabelPrinted, flashMessage: true);
                    }));
                }
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
            }
        }

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        protected virtual void RemoveProductCommandExecute()
        {
            if (!this.AuthorisationsManager.AllowUserToRemoveProductFromSaleOrder || !this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                return;
            }

            ProgressBar.SetUp(0, 5);
            ProgressBar.Run();

            try
            {
                if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
                {
                    // If there are no transactions recorded against this product, remove.
                    if (!this.DataManager.DoesProductLineContainTransactions(this.SelectedSaleDetail.SaleDetailID))
                    {
                        ProgressBar.Run();
                        // Deleting product
                        if (this.RemoveItem(ViewType.ARDispatch))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                            ProgressBar.Run();
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SelectedSaleDetail = null;
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Show the sales.
        /// </summary>
        private void ShowSales()
        {
            this.Log.LogDebug(this.GetType(), "FindSaleCommandExecute()");
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales =
                    this.DataManager.GetAllARDispatchesByDate(true, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, this.Locator.SalesSearchData.SelectedSearchType).ToList();
                //this.CustomerSales = localSales
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllARDispatchesByDate(false, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, this.Locator.SalesSearchData.SelectedSearchType).ToList();
                //this.CustomerSales = this.DataManager.GetAllARDispatches(orderStatuses)
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.ARDispatch), Token.SearchForSale);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateDispatch);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch2);
            }
        }

        /// <summary>
        /// Adds a new sale from the desktop.
        /// </summary>
        private void AddDesktopSale()
        {
            if (this.Sale == null)
            {
                return;
            }

            // we use the copying from order flag to ensure all sale details get prcessed.
            this.Sale.CopyingFromOrder = true;
            this.AddTransaction();
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        private void PrintReport(int dispatchId)
        {
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.WoolleysDispatchDocket, ReportParameters = reportParam };

            try
            {
                for (int i = 0; i < Settings.Default.DispatchReportCopiesToPrint; i++)
                {
                    this.ReportManager.PrintReport(reportData);
                }

                SystemMessage.Write(MessageType.Priority, Message.DispatchDocketPrinted);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        private void PrintDispatchDetailReport(int dispatchId)
        {
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.WoolleysDispatchDocketDetail, ReportParameters = reportParam };

            try
            {
                for (int i = 0; i < Settings.Default.DispatchReportCopiesToPrint; i++)
                {
                    this.ReportManager.PrintReport(reportData);
                }

                SystemMessage.Write(MessageType.Priority, Message.DispatchDocketPrinted);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        private void PrintInvoiceReport(int dispatchId)
        {
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.WoolleysInvoice, ReportParameters = reportParam };

            try
            {
                for (int i = 0; i < Settings.Default.DispatchReportCopiesToPrint; i++)
                {
                    this.ReportManager.PrintReport(reportData);
                }

                SystemMessage.Write(MessageType.Priority, Message.DispatchDocketPrinted);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handles an unfilled order authorisation entered password.
        /// </summary>
        /// <param name="result">The authorisation result.</param>
        private void HandleUnfilledOrderAuthorisation(string result)
        {
            if (result.Equals(Constant.Cancel))
            {
                NouvemMessageBox.Show(Message.OrderCompletionCancelled, touchScreen: true);
                return;
            }

            if (result.Equals(Constant.NoAuthorisation))
            {
                NouvemMessageBox.Show(Message.CredentialsNotAuthorised, touchScreen: true);
                return;
            }

            //this.CompleteOrderCheck();
            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusComplete.NouDocStatusID))
            {
                if (this.AutoCopyToInvoice || ApplicationSettings.AutoCreateInvoiceOnOrderCompletion)
                {
                    ProgressBar.Run();
                    this.Log.LogDebug(this.GetType(), "CompleteOrder(): Auto copying to an invoice");
                    this.AutoCopyDispatchToInvoice();
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                }

                this.ClearForm();
                this.PartnerName = Strings.SelectCustomer;
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
                Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
            }
        }

        /// <summary>
        /// Determines if an order is filled.
        /// </summary>
        /// <returns>Flag, as to whether an order has been filled.</returns>
        private bool IsOrderFilled()
        {
            foreach (var detail in this.SaleDetails)
            {
                if (detail.NouOrderMethod == OrderMethod.Weight)
                {
                    if (detail.WeightDelivered.ToDouble() < detail.WeightOrdered.ToDouble())
                    {
                        return false;
                    }
                }
                else if (detail.NouOrderMethod == OrderMethod.Quantity)
                {
                    if (detail.QuantityDelivered.ToDouble() < detail.QuantityOrdered.ToDouble())
                    {
                        return false;
                    }
                }
                else
                {
                    if (detail.WeightDelivered.ToDouble() < detail.WeightOrdered.ToDouble()
                        || detail.QuantityDelivered.ToDouble() < detail.QuantityOrdered.ToDouble())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Copies the current dispatch docket to an invoice.(We don't show the invoice screen).
        /// </summary>
        private void AutoCopyDispatchToInvoice()
        {
            try
            {
                this.DataManager.PriceDispatchDocket(this.Sale.SaleID);
                this.DataManager.AddDeliveryCharges(this.Sale.SaleID);

                ProgressBar.Run();
                this.AutoCopying = true;
                this.DataManager.CopyDispatchToInvoice(this.Sale.SaleID, NouvemGlobal.UserId.ToInt(),
                    NouvemGlobal.DeviceId.ToInt());
            }
            finally
            {
                ProgressBar.Reset();
                this.AutoCopying = false;
            }
        }

        /// <summary>
        /// Copies the current dispatch to an invoice
        /// </summary>
        private void CopyDispatchToInvoice()
        {
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
            this.Sale.BaseDocumentCreationDate = this.Sale.CreationDate;
            Messenger.Default.Send(ViewType.Invoice);
            CopyingDocument = true;
            this.Sale.CopyingFromOrder = true;
            this.Locator.Invoice.CopySale(this.Sale);
        }

        /// <summary>
        /// Gets the price methods.
        /// </summary>
        private void GetPriceMethods()
        {
            this.priceMethods = this.DataManager.GetPriceMethods();
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                if (ApplicationSettings.TouchScreenMode)
                {
                    Keypad.Show(KeypadTarget.ManualSerial, ApplicationSettings.ScannerMode);
                }
                else
                {
                    this.DrillDownManualSerialCommandExecute();
                }
            };
        }

        /// <summary>
        /// Gets all the quotes.
        /// </summary>
        private void GetAllSales()
        {
            this.Log.LogDebug(this.GetType(), "GetAllSales()");
            //this.AllSales = this.DataManager.GetAllARDispatches(this.DocStatusesOpen);
            //this.Dispatches = new ObservableCollection<Sale>(this.AllSales.Take(200));
        }

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            this.Log.LogDebug(this.GetType(), "RefeshSales()");
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.ARDispatch)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;

            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales =
                    this.DataManager.GetAllARDispatchesByDate(true, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, this.Locator.SalesSearchData.SelectedSearchType).ToList();
                //this.CustomerSales = localSales
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllARDispatchesByDate(false, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, this.Locator.SalesSearchData.SelectedSearchType).ToList();
                //this.CustomerSales = this.DataManager.GetAllARDispatches(orderStatuses)
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateDispatch);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch2);
            }
        }

        /// <summary>
        /// Refreshes the goods in details.
        /// </summary>
        private void RefreshGoodsIn()
        {
            Messenger.Default.Send(Token.Message, Token.RefreshGoodsinDetails);
        }

        /// <summary>
        /// Checks for any dispatch lines not on the associated sale order.
        /// </summary>
        private void CheckForExtraDispatchLines()
        {
            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            var data = this.DataManager.CheckForExtraDispatchLines(this.Sale.SaleID);
            if (data.Count > 0)
            {
                var products = string.Join(",", data);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show($"{products} were not on the sale order. Please check the prices");
                }));
            }
        }

        /// <summary>
        /// Displays the base document.
        /// </summary>
        private void ShowBaseDocument()
        {
            try
            {
                var baseDoc = this.DataManager.GetOrderByID(this.Sale.BaseDocumentReferenceID.ToInt());
                Messenger.Default.Send(ViewType.Order);
                this.Locator.Order.Sale = baseDoc;
                SystemMessage.Write(MessageType.Priority, Message.BaseDocumentLoaded);
            }
            finally
            {
                //this.Locator.ARDispatch.DisplayModeOnly = false;
            }
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.ARDispatch);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        /// <summary>
        /// Refreshes the purchase orders.
        /// </summary>
        [Obsolete]
        private void RefreshOrders()
        {
            Messenger.Default.Send(this.Sale.SaleID, Token.RefreshOrders);
        }

        /// <summary>
        /// Sets the line status.
        /// </summary>
        protected void SetLineStatus(SaleDetail detail)
        {
            if (detail == null || !this.SaleDetails.Any())
            {
                return;
            }

            detail.CheckLineStatus();
            if (detail.IsFilled)
            {
                var index = 0;
                for (int i = 0; i < this.SaleDetails.Count; i++)
                {
                    if (this.SaleDetails.ElementAt(i).SaleDetailID == detail.SaleDetailID)
                    {
                        index = i;
                        break;
                    }
                }

                // move filled line to the bottom, greyed out.
                this.SaleDetails.Move(index, this.SaleDetails.Count - 1);
            }
        }

        #endregion

        #endregion
    }
}
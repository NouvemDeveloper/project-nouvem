﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    public class CarcassDispatchViewModel : ARDispatchViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CarcassDispatchViewModel"/> class.
        /// </summary>
        public CarcassDispatchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<List<Sale>>(this, Token.SearchSaleSelectedForReport, s =>
            {
                var dispatches = string.Join(",", s.Select(x => x.SaleID));
                this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetCarcassDispatchData(dispatches));
            });

            Messenger.Default.Register<string>(this, Token.RefreshCarcassDispatch, s => this.RefreshSales());

            #endregion

            #region command handler

            #endregion

            this.SetControlMode(ControlMode.Find);
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            if (this.SelectedPartner == null)
            {
                return;
            }

            var customer = this.DataManager.GetBusinessPartner(this.SelectedPartner.BPMasterID);
            if (customer != null)
            {
                this.SelectedContact = null;
                //this.SelectedSalesEmployee = null;
                this.SelectedDeliveryContact = null;
                this.SelectedInvoiceAddress = null;
                this.SelectedDeliveryAddress = null;
                this.AccountBalance = customer.Details.Balance;
                this.CreditLimit = customer.Details.CreditLimit;
                //this.PartnerCurrency = customer.PartnerCurrency?.Symbol ?? ApplicationSettings.DefaultCurrency;
                this.PartnerCurrency =
                    customer.PartnerCurrency != null && !string.IsNullOrEmpty(customer.PartnerCurrency.Symbol)
                        ? customer.PartnerCurrency.Symbol
                        : ApplicationSettings.DefaultCurrency;

                this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == customer.Details.BPMasterID_Agent);

                if (customer.Contacts != null)
                {
                    this.Contacts = new ObservableCollection<BusinessPartnerContact>(customer.Contacts);
                    var primaryContact = customer.Contacts.FirstOrDefault(x => x.Details.PrimaryContact == true);

                    if (primaryContact != null)
                    {
                        this.SelectedContact = primaryContact;
                    }
                }

                if (customer.Addresses != null)
                {
                    this.Addresses = new ObservableCollection<BusinessPartnerAddress>(customer.Addresses.OrderBy(x => x.FullAddress));
                    var billingAddress = customer.Addresses.FirstOrDefault(x => x.Details.Billing == true);
                    var deliveryAddress = customer.Addresses.FirstOrDefault(x => x.Details.Shipping == true);

                    if (billingAddress != null)
                    {
                        this.SelectedInvoiceAddress = billingAddress;
                    }

                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                    }
                }
            }
        }


        /// <summary>
        /// Handles a sale selection.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            try
            {
                this.EntitySelectionChange = true;

                this.IsReadOnly = false;
                this.DisableDocStatusChange = false;
                this.IsBaseDocument = false;

                // header
                var partnerId = this.Sale.BPCustomer != null
                    ? this.Sale.BPCustomer.BPMasterID
                    : this.Sale.Customer.BPMasterID;

                this.AddInactiveSelectedPartner(partnerId);

                this.SelectedAgent = null;
                this.SelectedPartner = this.allPartners.FirstOrDefault(x => x.BPMasterID == partnerId);
                this.SelectedDocNumbering =
                    this.DocNumberings.FirstOrDefault(x => x.DocumentNumberingID == this.Sale.DocumentNumberingID);
                this.NextNumber = CopyingDocument && this.SelectedDocNumbering != null
                    ? this.SelectedDocNumbering.NextNumber
                    : this.Sale.Number;

                this.SelectedDocStatus =
                    this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.Sale.NouDocStatusID);
                this.ValidUntilDate = this.Sale.QuoteValidDate;
                this.DeliveryDate = this.Sale.DeliveryDate;
                this.ProposedKillDate = this.Sale.ProposedKillDate;
                this.ShippingDate = this.Sale.ShippingDate;
                this.DocumentDate = this.Sale.DocumentDate;
                this.DiscountPercentage = this.Sale.DiscountPercentage.ToDecimal();
                this.Remarks = this.Sale.Remarks;
                this.CustomerPOReference = this.Sale.CustomerPOReference;
                this.SelectedSalesEmployee = this.Sale.SalesEmployeeID != null
                    ? this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == this.Sale.SalesEmployeeID)
                    : null;
                this.SubTotal = this.Sale.SubTotalExVAT.ToDecimal();
                this.Tax = this.Sale.VAT.ToDecimal();
                this.Discount = this.Sale.DiscountIncVAT.ToDecimal();
                this.Total = this.Sale.GrandTotalIncVAT.ToDecimal();
                this.DocketNote = this.Sale.DocketNote;
                this.PopUpNote = this.Sale.PopUpNote;
                this.DeliveryTime = this.Sale.DeliveryTime;
                this.InvoiceNote = this.Sale.InvoiceNote;
                this.SelectedDeliveryAddress = this.Sale.DeliveryAddress != null &&
                                               this.Sale.DeliveryAddress.Details != null
                    ? this.Addresses.FirstOrDefault(
                        x => x.Details.BPAddressID == this.Sale.DeliveryAddress.Details.BPAddressID)
                    : null;
                this.SelectedInvoiceAddress = this.Sale.InvoiceAddress != null &&
                                              this.Sale.InvoiceAddress.Details != null
                    ? this.Addresses.FirstOrDefault(
                        x => x.Details.BPAddressID == this.Sale.InvoiceAddress.Details.BPAddressID)
                    : null;

                if (this.Sale.AgentID.HasValue)
                {
                    this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == this.Sale.AgentID);
                }

                this.SelectedHaulier = null;
                this.SelectedContact = null;
                this.SelectedDeliveryContact = null;
                this.SelectedRoute = null;
                this.SelectedAgent = null;

                if (this.Sale.MainContact != null && this.Sale.MainContact.Details != null)
                {
                    this.SelectedContact =
                        this.Contacts.FirstOrDefault(
                            x => x.Details.BPContactID == this.Sale.MainContact.Details.BPContactID);
                }

                if (!this.Sale.RouteID.IsNullOrZero() && this.Routes != null)
                {
                    this.SelectedRoute = this.Routes.FirstOrDefault(x => x.RouteID == this.Sale.RouteID);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.Find);
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            base.FindSaleCommandExecute();
            this.Locator.SalesSearchData.SetView(ViewType.CarcassDispatch);
        }


        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            base.RefreshSales();
            this.Locator.SalesSearchData.SetView(ViewType.CarcassDispatch);
        }

        protected override ViewType GetView()
        {
            return ViewType.CarcassDispatch;
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseCarcassDispatchWindow);
            Messenger.Default.Unregister(this);
        }

        #endregion

        #region private



        #endregion

    }
}

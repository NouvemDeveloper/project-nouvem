﻿// -----------------------------------------------------------------------
// <copyright file="TareCalculatorViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Utility
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TareCalculatorViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The tares.
        /// </summary>
        private ObservableCollection<TareCalculator> tares;

        /// <summary>
        /// The selected tares.
        /// </summary>
        private TareCalculator selectedTare;

        /// <summary>
        /// The total tare.
        /// </summary>
        private double totalTare;

        /// <summary>
        /// The product name
        /// </summary>
        private string productName;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TareCalculatorViewModel"/> class.
        /// </summary>
        public TareCalculatorViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for an incoming keypad value for the tare.
            Messenger.Default.Register<string>(this, KeypadTarget.TareContainer, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.TotalTare = s.ToInt();
                }
            });

            Messenger.Default.Register<string>(this, KeypadTarget.TareCalculator, (s) =>
            {
                var amt = s.ToInt();
                if (this.SelectedTare != null && amt > 0)
                {
                    this.SelectedTare.Quantity = amt;
                    this.SelectedTare.TotalLineTare = this.SelectedTare.Quantity*this.SelectedTare.Tare;
                    this.TotalTare = this.tares.Sum(x => x.TotalLineTare);
                }
            });

            #endregion

            #region command handler

            this.OnLoadedCommand = new RelayCommand(() =>
            {
                this.Refresh();
                this.SelectedTare = null;
                this.IsFormLoaded = true;
            });

            this.OnUnloadedCommand = new RelayCommand(() => this.IsFormLoaded = false);
            this.MoveBackCommand = new RelayCommand(this.Close);
            this.ResetCommand = new RelayCommand(this.ResetCommandExecute);
            this.ApplyCommand = new RelayCommand(this.ApplyCommandExecute);
            this.TareSelectedCommand = new RelayCommand(this.TareSelectedCommandExecute);
            this.ShowManualTareCommand = new RelayCommand(() =>
            {
                this.Locator.Indicator.SetManualTare();
            });

            #endregion

            this.GetTares();
        }

        #endregion

        #region public interface

        #region property
   
        /// <summary>
        /// Gets or sets the product the tare will be applied to.
        /// </summary>
        public string ProductName
        {
            get
            {
                return this.productName;
            }

            set
            {
                this.productName = value;
                this.RaisePropertyChanged();
            }
        }
     
        /// <summary>
        /// Gets or sets the tares.
        /// </summary>
        public ObservableCollection<TareCalculator> Tares
        {
            get
            {
                return this.tares;
            }

            set
            {
                this.tares = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected tare.
        /// </summary>
        public TareCalculator SelectedTare
        {
            get
            {
                return this.selectedTare;
            }

            set
            {
                this.selectedTare = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected tare.
        /// </summary>
        public double TotalTare
        {
            get
            {
                return this.totalTare;
            }

            set
            {
                this.totalTare = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the unload command.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the apply tare command.
        /// </summary>
        public ICommand TareSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand ShowManualTareCommand { get; private set; }

        /// <summary>
        /// Gets the apply tare command.
        /// </summary>
        public ICommand ApplyCommand { get; private set; }

        /// <summary>
        /// Gets the reset tares command.
        /// </summary>
        public ICommand ResetCommand { get; private set; }

        /// <summary>
        /// Gets the move back command.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the tare selection by calling the keypad.
        /// </summary>
        private void TareSelectedCommandExecute()
        {
            if (this.SelectedTare != null && this.IsFormLoaded)
            {
                Global.Keypad.Show(KeypadTarget.TareCalculator);
            }
        }

        /// <summary>
        /// Applys the selected tares.
        /// </summary>
        private void ApplyCommandExecute()
        {
            if (this.TotalTare <= 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoTaresCalculated);
                NouvemMessageBox.Show(Message.NoTaresCalculated, touchScreen: true);
                return;
            }

            TareCalculator.TotalTare = this.TotalTare;

            // send our selected tares.
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(this.tares.Where(x => x.Quantity > 0).ToList(), Token.TaresSet);
            }));
          
            this.Close();
        }

        /// <summary>
        /// Resets the tares 
        /// </summary>
        private void ResetCommandExecute()
        {
            this.Refresh();
            SystemMessage.Write(MessageType.Priority, Message.TareCalculationsReset);
        }

        private void Refresh()
        {
            foreach (var tareCalculator in this.Tares)
            {
                tareCalculator.Quantity = 0;
                tareCalculator.TotalLineTare = 0;
            }

            this.TotalTare = 0;
            TareCalculator.TotalTare = 0;
        }

        #endregion

        #region helper

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseTareCalculatorWindow);
        }

        /// <summary>
        /// Gets the tares.
        /// </summary>
        private void GetTares()
        {
            this.tares = new ObservableCollection<TareCalculator>(this.DataManager.GetProductContainers());
        }

        #endregion

        #endregion
    }
}

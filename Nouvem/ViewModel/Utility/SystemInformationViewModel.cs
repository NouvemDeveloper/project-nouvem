﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.ViewModel.Utility
{
    public class SystemInformationViewModel : NouvemViewModelBase
    {
       #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemInformationViewModel"/> class.
        /// </summary>
        public SystemInformationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.MoveBackCommand = new RelayCommand(this.Close);
         
            #endregion

            this.AlibiChecksum = ApplicationSettings.AlibiChecksum;
            this.AlibiCertNo = ApplicationSettings.AlibiCertNo;
            this.NAWISerialNo = ApplicationSettings.NAWISerialNo;
            this.AlibiVersionNo = ApplicationSettings.AlibiVersionNo;
            this.SystemVersionNo = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the alibi checksum.
        /// </summary>
        public string AlibiChecksum { get; set; }

        /// <summary>
        /// Gets or sets the indicator serial no.
        /// </summary>
        public string NAWISerialNo { get; set; }

        /// <summary>
        /// Gets or sets the alibi checksum.
        /// </summary>
        public string AlibiVersionNo { get; set; }

        /// <summary>
        /// Gets or sets the system version no.
        /// </summary>
        public string SystemVersionNo { get; set; }

        /// <summary>
        /// Gets or sets the alibi checksum.
        /// </summary>
        public string AlibiCertNo { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to move back.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected



        #endregion

        #region private

        /// <summary>
        /// Close, and cleanup.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearSystemInformation();
            Messenger.Default.Send(Token.Message, Token.CloseSystemInformationWindow);
        }


        #endregion
            
        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="AlibiViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Utility
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using Nouvem.Global;
    using Nouvem.Indicator;
    using Nouvem.Model.Enum;

    public class AlibiViewModel : NouvemViewModelBase
    {
       #region field

        /// <summary>
        /// The alibi no.
        /// </summary>
        private int alibiNumber;

        /// <summary>
        /// The alibi data.
        /// </summary>
        private ObservableCollection<AlibiWeightInfo> alibiData = new ObservableCollection<AlibiWeightInfo>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AlibiViewModel"/> class.
        /// </summary>
        public AlibiViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.SearchAlibiCommand = new RelayCommand(this.SearchAlibiCommandExecute);

            // Handle the move back to the main screen.
            this.MoveBackCommand = new RelayCommand(() =>
            {
                ViewModelLocator.ClearAlibi();
                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Touchscreen);
            });

            #endregion

            this.GetChecksums();
        }

        #endregion

        #region public interface

        #region property
      
        /// <summary>
        /// Gets or sets the alibi data.
        /// </summary>
        public ObservableCollection<AlibiWeightInfo> AlibiData
        {
            get
            {
                return this.alibiData;
            }

            set
            {
                this.alibiData = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the alibi no.
        /// </summary>
        public int AlibiNumber
        {
            get
            {
                return this.alibiNumber;
            }

            set
            {
                this.alibiNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application checksum.
        /// </summary>
        public string ApplicationChecksum { get; set; }

        /// <summary>
        /// Gets or sets the astart up checksum.
        /// </summary>
        public string StartUpChecksum { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Command to search for an alibi number data.
        /// </summary>
        public ICommand SearchAlibiCommand { get; set; }

        /// <summary>
        /// Gets the command to move back to the orders screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Search the alibi data.
        /// </summary>
        private void SearchAlibiCommandExecute()
        {
            try
            {
                var data = this.IndicatorManager.GetAlibiData(this.alibiNumber);
                if (data != null && data.Alibi > 0)
                {
                    this.AlibiData.Add(data);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.AlibiNotFound);
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the checksums.
        /// </summary>
        private void GetChecksums()
        {
            this.ApplicationChecksum = this.IndicatorManager.GetCertifiedCheckSum();
            this.StartUpChecksum = this.IndicatorManager.GetCurrentCheckSum();
        }

        #endregion

        #endregion
    }
}

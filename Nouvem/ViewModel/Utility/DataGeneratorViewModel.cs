﻿// -----------------------------------------------------------------------
// <copyright file="DataGeneratorViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Global;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Utility
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;

    public class DataGeneratorViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The data string collection.
        /// </summary>
        private ObservableCollection<CollectionData> dataItems = new ObservableCollection<CollectionData>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the DataGeneratorViewModel class.
        /// </summary>
        public DataGeneratorViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for setting the incoming data.
            Messenger.Default.Register<string>(this, Token.SetData, s =>
            {
                this.DataItems = new ObservableCollection<CollectionData>(from data in s.Split(',')
                                                                    select new CollectionData { Data = data});
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the  data.
        /// </summary>
        public ObservableCollection<CollectionData> DataItems
        {
            get
            {
                return this.dataItems;
            }

            set
            {
                this.dataItems = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion
       
        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.SendData();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region helper

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDataGenerator();
            Messenger.Default.Send(Token.Message, Token.CloseDataGeneratorWindow);
        }

        /// <summary>
        /// Sends the generated data to interested observers.
        /// </summary>
        private void SendData()
        {
            if (!this.dataItems.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.DataCollectionEmpty);
                return;
            }

            var dataToSend = string.Join(",", this.dataItems.Select(x => x.Data));
            Messenger.Default.Send(dataToSend, Token.DataGenerated);
            this.Close();
        }
       
        #endregion

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IndicatorViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Utility
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class IndicatorViewModel : NouvemViewModelBase
    {
        #region field

        private decimal receipeQty;

        private string uom;

        /// <summary>
        /// The receipe value.
        /// </summary>
        private string receipeValue;

        /// <summary>
        /// The progress bars within tolerance values.
        /// </summary>
        private decimal withinToleranceRange;

        /// <summary>
        /// The progress bars within tolerance values.
        /// </summary>
        private decimal withinToleranceValue;

        /// <summary>
        /// The needle upper value.
        /// </summary>
        private decimal arcScaleValue;

        /// <summary>
        /// The within tolerance start value.
        /// </summary>
        private decimal arcInsideToleranceStart;

        /// <summary>
        /// The within tolerance end value.
        /// </summary>
        private decimal arcInsideToleranceEnd;

        /// <summary>
        /// The outside tolerance start value.
        /// </summary>
        private decimal arcOutsideToleranceStart;

        /// <summary>
        /// The outside tolerance end value.
        /// </summary>
        private decimal arcOutsideToleranceEnd;

        /// <summary>
        /// The anchor value.
        /// </summary>
        private decimal arcAnchorValue;

        /// <summary>
        /// The anchor target value.
        /// </summary>
        private string targetValue;

        /// <summary>
        /// The anchor target value.
        /// </summary>
        private string issuedValue;

        /// <summary>
        /// The anchor target value.
        /// </summary>
        private string orderedValue;

        /// <summary>
        /// The anchor target value.
        /// </summary>
        private string targetProduct;

        /// <summary>
        /// The test mode indicator motion emulator timer.
        /// </summary>
        private DispatcherTimer testTimer;

        /// <summary>
        /// The test mode indicator motion emulator timer.
        /// </summary>
        private DispatcherTimer testWeightTimer;

        /// <summary>
        /// The weigh mode.
        /// </summary>
        private WeighMode weighMode;

        /// <summary>
        /// The net gross value.
        /// </summary>
        private string netGrossValue;

        /// <summary>
        /// The wgt precision.
        /// </summary>
        private int weightPrecision;

        /// <summary>
        /// The indicator weight.
        /// </summary>
        private decimal weight;

        /// <summary>
        /// The tare to not change flag.
        /// </summary>
        private bool tareToHold;

        /// <summary>
        /// The container being set flag.
        /// </summary>
        private bool containerBeingSet;

        /// <summary>
        /// The indicator display weight.
        /// </summary>
        private string displayWeight;

        /// <summary>
        /// The indicator display tare.
        /// </summary>
        private string displayTare;

        /// <summary>
        /// The tare weight.
        /// </summary>
        private double tare;

        /// <summary>
        /// The tare container type code.
        /// </summary>
        private string tareContainer;

        /// <summary>
        /// The scales state.
        /// </summary>
        private string scalesState;

        /// <summary>
        /// The clean read value
        /// </summary>
        private bool cleanRead;

        /// <summary>
        /// The scales movement value.
        /// </summary>
        private bool scalesInMotion;

        /// <summary>
        /// The indicator state (net or gross).
        /// </summary>
        private IndicatorState state;

        /// <summary>
        /// The current connected scales.
        /// </summary>
        private string scales;

        /// <summary>
        /// The weight increment for the test weight arrows..
        /// </summary>
        private decimal testWeightIncrement;

        /// <summary>
        /// The current container.
        /// </summary>
        private Model.DataLayer.Container selectedContainer;

        /// <summary>
        /// Flag that indicates if the weight has been manually set.
        /// </summary>
        private bool manualWeight;

        private IList<UOMMaster> uoms = new List<UOMMaster>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="IndicatorViewModel"/> class.
        /// </summary>
        public IndicatorViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<bool>(this, Token.SetIndicatorManualMode, b =>
            {
                this.ManualWeight = b;
            });

            // Register for the incoming indicator weight.
            Messenger.Default.Register<Tuple<string, bool, bool>>(this, Token.Indicator, this.HandleIndicatorWeight);

            // Register for an incoming keypad value for the tare.
            Messenger.Default.Register<string>(this, KeypadTarget.TareContainer, s => this.SelectedContainer = new Model.DataLayer.Container { Name = Constant.ManualEntry, Tare = s.ToDouble() });

            // Register for an incoming keypad value for the weight.
            Messenger.Default.Register<string>(this, KeypadTarget.Weight, s =>
            {
                if (string.IsNullOrWhiteSpace(s))
                {
                    this.IndicatorManager.OpenIndicatorPort();
                    return;
                }

                this.ManualWeight = true;
                this.Weight = s.ToDecimal();
            });

            // Register for the incoming container.
            Messenger.Default.Register<Model.DataLayer.Container>(this, Token.ContainerSelected, c => this.SelectedContainer = c);

            // Register for a reset.
            Messenger.Default.Register<string>(this, Token.ResetTouchscreenValues, s =>
            {
                if (this.manualWeight)
                {
                    this.Weight = 0;
                    this.ManualWeight = false;
                }
            });

            Messenger.Default.Register<double>(this, Token.SetTare, d =>
            {
                if (this.selectedContainer != null && !this.selectedContainer.Name.Equals(Strings.NoContainer))
                {
                    return;
                }

                this.Tare = d;
            });

            #endregion

            #region command

            this.TareScalesCommand = new RelayCommand(() => this.TareScalesCommandExecute());

            if (ApplicationSettings.TestMode)
            {
                this.KeyUpCommand = new RelayCommand<KeyEventArgs>(e =>
                {

                    this.KeyPressCommandExecute(true);
                });
                this.KeyDownCommand = new RelayCommand<KeyEventArgs>(e => this.KeyPressCommandExecute(false));
                this.TestWeightCommand = new RelayCommand<string>(this.TestWeightCommandExecute, s => ApplicationSettings.TestMode);
            }

            // Switch the scales.
            this.SwitchScalesCommand = new RelayCommand(this.SwitchScalesCommandExecute);

            // Display the keypad.
            this.SetManualWeightCommand = new RelayCommand(() =>
            {
                if (!this.AuthorisationsManager.AllowUserToEnterManualWeightOnTouchscreen)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToEnterManualWeight);
                    this.SetControlMode(ControlMode.OK);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.NotAuthorisedToEnterManualWeight, flashMessage:true);
                    }));

                    return;
                }

                //if (this.IndicatorManager.IsIndicatorPortOpen())
                //{
                    this.IndicatorManager.CloseIndicatorPort();
               // }
               
                Keypad.Show(KeypadTarget.Weight);
            }
            );

            // Display the keypad.
            this.SetManualTareCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.TareContainer, showHold: true));

            // Show the tare container view.
            this.SetContainerTareCommand = new RelayCommand(() =>
            {
                if (ApplicationSettings.UsingTareCalculator)
                {
                    Messenger.Default.Send(Token.Message, Token.TareCalculatorRequested);
                    return;
                }

                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TouchscreenContainer);
            });

            #endregion

            this.weightPrecision = ApplicationSettings.ScalesDivision.DecimalPrecision();
            this.SetTestWeightIncrement(this.weightPrecision);
            this.Weight = 0;
            this.Scales = "Scales 1 ";
            this.SelectedContainer = new Model.DataLayer.Container { Name = Strings.NoContainer };
            if (ApplicationSettings.ConnectToIndicator)
            {
                this.SetUpIndicator();
            }

            if (ApplicationSettings.TestMode)
            {
                this.testTimer = new DispatcherTimer();
                this.testTimer.Interval = TimeSpan.FromSeconds(1);
                this.testTimer.Tick += this.TestTimerOnTick;

                this.testWeightTimer = new DispatcherTimer();
                this.testWeightTimer.Interval = TimeSpan.FromSeconds(ApplicationSettings.TestModeIncrementTime);
                this.testWeightTimer.Tick += this.TestWeightTimerOnTick;
            }

            this.SetScales();
            this.uoms = this.DataManager.GetNouUOMs();
        }

        /// <summary>
        /// The test mode motion eulator timer.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="eventArgs">The sender event.</param>
        private void TestTimerOnTick(object sender, EventArgs eventArgs)
        {
            this.ScalesInMotion = false;
            this.testTimer.Stop();
        }

        /// <summary>
        /// The test mode motion eulator timer.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="eventArgs">The sender event.</param>
        private void TestWeightTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (this.testWeightIncrement == ApplicationSettings.TestModeMaxIncrement)
            {
                return;
            }

            if (this.testWeightIncrement == 1)
            {
                this.testWeightIncrement = ApplicationSettings.TestModeMaxIncrement;
                return;
            }

            var precision = this.testWeightIncrement.DecimalPrecision() - 1;
            this.SetTestWeightIncrement(precision);
        }

        #endregion

        #region public interface

        #region enum

        /// <summary>
        /// Enumewration to hold the indicator state.
        /// </summary>
        public enum IndicatorState
        {
            /// <summary>
            /// The net state.
            /// </summary>
            NET,

            /// <summary>
            /// The gross state.
            /// </summary>
            GROSS
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether we are using the needle.
        /// </summary>
        public bool UsingNeedle { get; set; }

        /// <summary>
        /// Gets or sets the needle arc upper value.
        /// </summary>
        public string ReceipeValue
        {
            get
            {
                return this.receipeValue;
            }

            set
            {
                this.receipeValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc upper value.
        /// </summary>
        public decimal ArcScaleValue
        {
            get
            {
                return this.arcScaleValue;
            }

            set
            {
                this.arcScaleValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc anchor value.
        /// </summary>
        public decimal ArcAnchorValue
        {
            get
            {
                return this.arcAnchorValue;
            }

            set
            {
                this.arcAnchorValue = value;
                this.RaisePropertyChanged();
                this.TargetValue = $"{Strings.Target}: {value.ToDisplayPrecision(ApplicationSettings.TouchscreenWgtMaskNo)} {this.uom}"; 
            }
        }

        /// <summary>
        /// Gets or sets the needle arc anchor value + uom.
        /// </summary>
        public string OrderedValue
        {
            get
            {
                return this.orderedValue;
            }

            set
            {
                this.orderedValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc anchor value + uom.
        /// </summary>
        public string IssuedValue
        {
            get
            {
                return this.issuedValue;
            }

            set
            {
                this.issuedValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc anchor value + uom.
        /// </summary>
        public string TargetValue
        {
            get
            {
                return this.targetValue;
            }

            set
            {
                this.targetValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current product.
        /// </summary>
        public string TargetProduct
        {
            get
            {
                return this.targetProduct;
            }

            set
            {
                this.targetProduct = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc within tolerance start value.
        /// </summary>
        public decimal ArcInsideToleranceStart
        {
            get
            {
                return this.arcInsideToleranceStart;
            }

            set
            {
                this.arcInsideToleranceStart = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc within tolerance end value.
        /// </summary>
        public decimal ArcInsideToleranceEnd
        {
            get
            {
                return this.arcInsideToleranceEnd;
            }

            set
            {
                this.arcInsideToleranceEnd = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc outside tolerance start value.
        /// </summary>
        public decimal ArcOutsideToleranceStart
        {
            get
            {
                return this.arcOutsideToleranceStart;
            }

            set
            {
                this.arcOutsideToleranceStart = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the needle arc outside tolerance end value.
        /// </summary>
        public decimal ArcOutsideToleranceEnd
        {
            get
            {
                return this.arcOutsideToleranceEnd;
            }

            set
            {
                this.arcOutsideToleranceEnd = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the progress bar within tolerance range.
        /// </summary>
        public decimal WithinToleranceRange
        {
            get
            {
                return this.withinToleranceRange;
            }

            set
            {
                this.withinToleranceRange = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the progress bar within tolerance value.
        /// </summary>
        public decimal WithinToleranceValue
        {
            get
            {
                return this.withinToleranceValue;
            }

            set
            {
                this.withinToleranceValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the tare is to be reset.
        /// </summary>
        public bool DoNotResetTare { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the tare is to be reset.
        /// </summary>
        public bool TareToHold
        {
            get
            {
                return this.tareToHold;
            }

            set
            {
                this.tareToHold = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the net or gross value.
        /// </summary>
        public string NetGrossValue
        {
            get
            {
                return this.netGrossValue;
            }

            set
            {
                this.netGrossValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the weighing mode.
        /// </summary>
        public WeighMode WeighMode
        {
            get
            {
                return this.weighMode;
            }

            set
            {
                this.weighMode = value;
                if (value == WeighMode.Auto)
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Auto;
                    Messenger.Default.Send(false, Token.WeightModeChanged);
                }
                else
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Record;
                    Messenger.Default.Send(true, Token.WeightModeChanged);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current scales.
        /// </summary>
        public decimal ReceipeQty
        {
            get
            {
                return this.receipeQty;
            }

            set
            {
                this.receipeQty = value;
                this.RaisePropertyChanged();
            }
        }

        public decimal Produced { get; set; }

        /// <summary>
        /// Gets or sets the current scales.
        /// </summary>
        public string Scales
        {
            get
            {
                return this.scales;
            }

            set
            {
                this.scales = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current contianer.
        /// </summary>
        public Model.DataLayer.Container SelectedContainer
        {
            get
            {
                return this.selectedContainer;
            }

            set
            {
                this.selectedContainer = value;
                this.RaisePropertyChanged();

                if (value != null && !value.Name.Equals(Strings.Container))
                {
                    this.TareToHold = Keypad.HoldKepadValue;
                    //this.DontResetTare = false;
                    //this.containerBeingSet = true;
                    this.Tare = value.Tare;
                    //this.containerBeingSet = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the indicator weight.
        /// </summary>
        public IndicatorState State
        {
            get
            {
                return this.state;
            }

            set
            {
                this.state = value;
                this.RaisePropertyChanged();
                this.ScalesState = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the indicator weight.
        /// </summary>
        public decimal Weight
        {
            get
            {
                return this.weight;
            }

            set
            {
                this.weight = Math.Round(value, this.weightPrecision);
                this.RaisePropertyChanged();

                this.DisplayWeight = this.weight.ToDisplayPrecision(this.weightPrecision);
                if (this.UsingNeedle)
                {
                    this.SetWithinToleranceValue();
                }
            }
        }

        /// <summary>
        /// Gets or sets the indicator display weight.
        /// </summary>
        public string DisplayWeight
        {
            get
            {
                return this.displayWeight;
            }

            set
            {
                this.displayWeight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the indicator tare.
        /// </summary>
        public double Tare
        {
            get
            {
                return this.tare;
            }

            set
            {
                if (this.TareToHold || this.DoNotResetTare)
                {
                    return;
                }

                //if (this.SelectedContainer != null && this.SelectedContainer.Name != Strings.NoContainer && !this.containerBeingSet)
                //{
                //    return;
                //}

                this.IndicatorManager.SetTare(value.ToDecimal());

                // remove the original tare/reset the weight.
                this.tare = 0;
                this.Weight = 0;

                this.tare = Math.Round(value, this.weightPrecision);
                this.RaisePropertyChanged();

                // refresh the weight (in case we are in manual weigh mode)
                this.Weight = this.weight;

                this.DisplayTare = this.tare.ToDisplayPrecision(this.weightPrecision);

                this.NetGrossValue = this.IndicatorManager.WeightMode;

                // broadcast to any interested observers.
                Messenger.Default.Send(value, Token.TareSet);
            }
        }

        /// <summary>
        /// Gets or sets the tare display.
        /// </summary>
        public string DisplayTare
        {
            get
            {
                return this.displayTare;
            }

            set
            {
                this.displayTare = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the tare container code.
        /// </summary>
        public string TareContainer
        {
            get
            {
                return this.tareContainer;
            }

            set
            {
                this.tareContainer = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scales state.
        /// </summary>
        public string ScalesState
        {
            get
            {
                return this.scalesState;
            }

            set
            {
                this.scalesState = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a clean read has been attained.
        /// </summary>
        public bool CleanRead
        {
            get
            {
                return this.cleanRead;
            }

            set
            {
                this.cleanRead = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scales movement value.
        /// </summary>
        public bool ScalesInMotion
        {
            get
            {
                return this.scalesInMotion;
            }

            set
            {
                this.scalesInMotion = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// The test weight key press event capture.
        /// </summary>
        public ICommand TareScalesCommand { get; set; }

        /// <summary>
        /// The test weight key press event capture.
        /// </summary>
        public ICommand KeyUpCommand { get; set; }

        /// <summary>
        /// The test weight key press event capture.
        /// </summary>
        public ICommand KeyDownCommand { get; set; }

        /// <summary>
        /// Command to use the test weight keys.
        /// </summary>
        public ICommand TestWeightCommand { get; private set; }

        /// <summary>
        /// Command to switch the scales.
        /// </summary>
        public ICommand SwitchScalesCommand { get; private set; }

        /// <summary>
        /// Command to select the container tare.
        /// </summary>
        public ICommand SetContainerTareCommand { get; private set; }

        /// <summary>
        /// Command to set the tare manually.
        /// </summary>
        public ICommand SetManualTareCommand { get; private set; }

        /// <summary>
        /// Command to set the weight manually.
        /// </summary>
        public ICommand SetManualWeightCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Auto sets the manual weight.
        /// </summary>
        /// <param name="wgt">The incoming manual wgt to set.</param>
        public void SetManualWeight(decimal wgt)
        {
            if (wgt == 0)
            {
                this.Weight = 0;
                this.ManualWeight = false;
                this.IndicatorManager.OpenIndicatorPort();
                return;
            }

            //if (this.IndicatorManager.IsIndicatorPortOpen())
            //{
                this.IndicatorManager.CloseIndicatorPort();
           // }

            this.ManualWeight = true;
            this.Weight = wgt;
        }

        /// <summary>
        /// Sets the issue value.
        /// </summary>
        /// <param name="value">The produced value to update.</param>
        public void SetIssuedValue(decimal value)
        {
            this.Produced = value;
            this.IssuedValue = $"{Strings.Issued}: {this.Produced} {this.uom}";
        }

        /// <summary>
        /// Sets the recipe needle values.
        /// </summary>
        /// <param name="ordered">The line order amt.</param>
        /// <param name="produced">What ahs been produced.</param>
        /// <param name="minusTolerance">Mius tolerance value.</param>
        /// <param name="plusTolerance">Plus tolerance value.</param>
        /// <param name="uom">The line unit of measure.</param>
        /// <param name="product">The line product name.</param>
        public void SetNeedleValues(decimal ordered, decimal produced, decimal minusTolerance, decimal plusTolerance, int uom, string product)
        {
            this.Produced = produced;
            this.receipeQty = produced;
            this.TargetProduct = product;

            var remaining = ordered - produced;
            var localUom = this.uoms.FirstOrDefault(x => x.UOMMasterID == uom);
            if (localUom != null)
            {
                this.uom = localUom.Code;
            }

            if (minusTolerance == 0)
            {
                minusTolerance = 0.1m;
            }

            if (plusTolerance == 0)
            {
                plusTolerance = 0.1m;
            }

            var start = remaining - (remaining / 100 * minusTolerance);
            var end = remaining + (remaining / 100 * plusTolerance);
            this.ArcAnchorValue = remaining;
            this.OrderedValue = $"{Strings.Ordered}: {ordered.ToDisplayPrecision(ApplicationSettings.TouchscreenWgtMaskNo)} {this.uom}";
            this.IssuedValue = $"{Strings.Issued}: {produced.ToDisplayPrecision(ApplicationSettings.TouchscreenWgtMaskNo)} {this.uom}";
            this.UsingNeedle = true;
            this.ArcInsideToleranceStart = start;
            this.ArcInsideToleranceEnd = end;
            var range = end - start;
            if (range > 0)
            {
                var upper = (end - remaining);
                var lower = (remaining - start);
                this.WithinToleranceRange = upper >= lower ? upper : lower;
            }

            this.ArcOutsideToleranceStart = end;
            this.ArcOutsideToleranceEnd = end * 1.05M;
            this.ArcScaleValue = ordered * 1.1M;
            this.SetWithinToleranceValue();
        }

        /// <summary>
        /// Moves the weight up/down (test mode).
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        public void TestWeight(string direction)
        {
            //var increment = Math.Round(this.testWeightIncrement / 10,5);

            decimal localWgt;
            this.ScalesInMotion = true;
            this.testTimer.Start();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                if (direction.Equals(Constant.Left))
                {
                    //localWgt = this.weight - actualIncrement;
                    //while (this.weight > localWgt)
                    //{
                    //    this.Weight -= this.testWeightIncrement;
                    //    this.ReceipeQty -= this.testWeightIncrement;
                    //}
                    this.Weight -= this.testWeightIncrement;
                    this.ReceipeQty -= this.testWeightIncrement;

                    return;
                }

                this.Weight += this.testWeightIncrement;
                this.ReceipeQty += this.testWeightIncrement;

                //localWgt = this.weight + actualIncrement;
                //while (this.weight < localWgt)
                //{
                //    this.Weight += increment;
                //    this.ReceipeQty += increment;
                //}
            }));
        }

        /// <summary>
        /// Sets the manual tare.
        /// </summary>
        public void SetManualTare()
        {
            Keypad.Show(KeypadTarget.TareContainer, showHold: true);
        }

        /// <summary>
        /// Reset the container.
        /// </summary>
        public void ResetContainer()
        {
            this.SelectedContainer = new Model.DataLayer.Container { Name = Strings.NoContainer };
            this.TareToHold = false;
            this.DoNotResetTare = false;
            this.Tare = 0;
        }

        /// <summary>
        /// Switch the scales.
        /// </summary>
        public void SetScales(int localScales)
        {
            this.Weight = 0;
            this.IndicatorManager.CloseIndicatorPort();

            if (localScales == 1)
            {
                this.IndicatorManager.CurrentIndicator = 1;
                this.Scales = "Scales 1 ";
            }
            else if (localScales == 2)
            {
                this.IndicatorManager.CurrentIndicator = 2;
                this.Scales = "Scales 2 ";
            }
            else if (localScales == 3)
            {
                this.IndicatorManager.CurrentIndicator = 3;
                this.Scales = "Scales 3 ";
            }

            var scalesConnected = this.IndicatorManager.OpenIndicatorPort();

            if (!scalesConnected)
            {
                this.NetGrossValue = Strings.NotConnected;
                this.Weight = 0;
            }
            else
            {
                this.NetGrossValue = this.IndicatorManager.WeightMode ?? "GROSS";
            }
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Tares the scales to 0.
        /// </summary>
        private void TareScalesCommandExecute()
        {
            var localHold = this.TareToHold;
            this.TareToHold = false;
            this.DoNotResetTare = false;
            this.Tare = this.Tare + this.Weight.ToDouble();
            this.TareToHold = localHold;
            SystemMessage.Write(MessageType.Priority, Message.ScalesTared);
        }

        /// <summary>
        /// Moves the weight up/down (test mode).
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        private void TestWeightCommandExecute(string direction)
        {
            //var increment = Math.Round(this.testWeightIncrement / 10,5);
            
            decimal localWgt;
            this.ScalesInMotion = true;
            this.testTimer.Start();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                if (direction.Equals(Constant.Left))
                {
                    //localWgt = this.weight - actualIncrement;
                    //while (this.weight > localWgt)
                    //{
                    //    this.Weight -= this.testWeightIncrement;
                    //    this.ReceipeQty -= this.testWeightIncrement;
                    //}
                    this.Weight -= this.testWeightIncrement;
                    this.ReceipeQty -= this.testWeightIncrement;

                    return;
                }

                this.Weight += this.testWeightIncrement;
                this.ReceipeQty += this.testWeightIncrement;

                //localWgt = this.weight + actualIncrement;
                //while (this.weight < localWgt)
                //{
                //    this.Weight += increment;
                //    this.ReceipeQty += increment;
                //}
            }));
        }

        /// <summary>
        /// Switch the scales.
        /// </summary>
        private void SwitchScalesCommandExecute()
        {
            this.Weight = 0;
            this.IndicatorManager.CloseIndicatorPort();

            if (this.Scales.Equals("Scales 1 "))
            {
                this.IndicatorManager.CurrentIndicator = 2;
                this.Scales = "Scales 2 ";
            }
            else if (this.Scales.Equals("Scales 2 "))
            {
                this.IndicatorManager.CurrentIndicator = 3;
                this.Scales = "Scales 3 ";
            }
            else if (this.Scales.Equals("Scales 3 "))
            {
                this.IndicatorManager.CurrentIndicator = 1;
                this.Scales = "Scales 1 ";
            }

            this.IndicatorManager.HandleScalesPolling();
            var scalesConnected = this.IndicatorManager.OpenIndicatorPort();

            if (!scalesConnected)
            {
                this.NetGrossValue = Strings.NotConnected;
                this.Weight = 0;
            }
            else
            {
                this.NetGrossValue = this.IndicatorManager.WeightMode ?? "GROSS";
            }
        }

        /// <summary>
        /// Handles a key press in test mode.
        /// </summary>
        /// <param name="up">Key up/down flag.</param>
        public void KeyPressCommandExecute(bool up)
        {
            if (up)
            {
                this.testWeightTimer.Stop();
                this.SetTestWeightIncrement(this.weightPrecision);
            }
            else
            {
                this.testWeightTimer.Start();
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets or sets a flag indicating whether this is a manual weight.
        /// </summary>
        public bool ManualWeight
        {
            get
            {
                return this.manualWeight;
            }

            set
            {
                this.manualWeight = value;
            }
        }

        /// <summary>
        /// Handle the weight recording.
        /// </summary>
        public void RecordWeight()
        {
            if (this.manualWeight || ApplicationSettings.TestMode)
            {
                Messenger.Default.Send(Tuple.Create(this.Weight, true, true), Token.IndicatorWeight);
                return;
            }

            Messenger.Default.Send(Token.Message, Token.IndicatorWeight);
        }

        /// <summary>
        /// Connect to the attached indicator.
        /// </summary>
        private void SetUpIndicator()
        {
            try
            {
                this.IndicatorManager.ConnectToIndicator();

                this.IndicatorManager.Indicator.WeightDataReceived += (sender, args)
                => Application.Current.Dispatcher.BeginInvoke(
                   DispatcherPriority.Background,
                      new Action(() =>
                      {
                          try
                          {
                              if (this.ManualWeight)
                              {
                                  return;
                              }

                              this.Weight = args.SerialData.ToDecimal();
                              this.CleanRead = args.IsClean;
                              this.ScalesInMotion = args.InMotion;
                              Messenger.Default.Send(Tuple.Create(this.Weight, this.ScalesInMotion), Token.ScalesInMotion);
                          }
                          catch (Exception)
                          {
                              // no need to do anything.
                          }
                      }));

                this.IndicatorManager.Indicator2.WeightDataReceived += (sender, args)
                => Application.Current.Dispatcher.BeginInvoke(
                   DispatcherPriority.Background,
                      new Action(() =>
                      {
                          try
                          {
                              if (this.ManualWeight)
                              {
                                  return;
                              }

                              this.Weight = args.SerialData.ToDecimal();
                              this.CleanRead = args.IsClean;
                              this.ScalesInMotion = args.InMotion;
                              Messenger.Default.Send(Tuple.Create(this.Weight, this.ScalesInMotion), Token.ScalesInMotion);
                          }
                          catch (Exception)
                          {
                              // no need to do anything.
                          }
                      }));

                this.IndicatorManager.Indicator3.WeightDataReceived += (sender, args)
                => Application.Current.Dispatcher.BeginInvoke(
                   DispatcherPriority.Background,
                      new Action(() =>
                      {
                          try
                          {
                              if (this.ManualWeight)
                              {
                                  return;
                              }

                              this.Weight = args.SerialData.ToDecimal();
                              this.CleanRead = args.IsClean;
                              this.ScalesInMotion = args.InMotion;
                              Messenger.Default.Send(Tuple.Create(this.Weight, this.ScalesInMotion), Token.ScalesInMotion);
                          }
                          catch (Exception)
                          {
                              // no need to do anything.
                          }
                      }));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Sets the scales.
        /// </summary>
        private void SetScales()
        {
            if (ApplicationSettings.CurrentIndicator == 0 || ApplicationSettings.CurrentIndicator == 1)
            {
                this.Scales = "Scales 3 ";
            }
            else if (ApplicationSettings.CurrentIndicator == 2)
            {
                this.Scales = "Scales 1 ";
            }
            else
            {
                this.Scales = "Scales 2 ";
            }

            this.SwitchScalesCommandExecute();
        }

        /// <summary>
        /// Handles the incoming weight data.
        /// </summary>
        /// <param name="indicatorData">The weight data.</param>
        private void HandleIndicatorWeight(Tuple<string, bool, bool> indicatorData)
        {
            try
            {
                this.Weight = indicatorData.Item1.ToDecimal();
                this.CleanRead = indicatorData.Item2;
                this.ScalesInMotion = indicatorData.Item3;
            }
            catch (Exception)
            {
                // no need to do anything.
            }
        }

        /// <summary>
        /// Sets the test weight increment.
        /// </summary>
        private void SetTestWeightIncrement(int precision)
        {
            switch (precision)
            {
                case 0:
                    this.testWeightIncrement = 1m;
                    break;
                case 1:
                    this.testWeightIncrement = 0.1m;
                    break;
                case 2:
                    this.testWeightIncrement = 0.01m;
                    break;
                case 3:
                    this.testWeightIncrement = 0.001m;
                    break;
                case 4:
                    this.testWeightIncrement = 0.0001m;
                    break;
                case 5:
                    this.testWeightIncrement = 0.00001m;
                    break;
            }
        }

        /// <summary>
        /// Sets the ui receipe weight\value;
        /// </summary>
        private void SetReceipeValue()
        {
            this.ReceipeQty = this.weight;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.ReceipeValue = string.Format("{0} {1}", this.receipeQty, this.uom);
            }));
        }

        /// <summary>
        /// Sets the within tolerance bar values.
        /// </summary>
        private void SetWithinToleranceValue()
        {
            this.SetReceipeValue();

            #region validation

            if (this.WithinToleranceRange <= 0)
            {
                return;
            }

            #endregion

            if (this.receipeQty <= 0 || this.receipeQty < this.ArcInsideToleranceStart || this.receipeQty > this.ArcInsideToleranceEnd)
            {
                this.WithinToleranceValue = 0;
                return;
            }

            // we are within tolerance levels
           this.WithinToleranceValue = this.WithinToleranceRange - Math.Abs(this.ArcAnchorValue - this.receipeQty);
        }

        /// <summary>
        /// Format the incoming weight.
        /// </summary>
        /// <param name="weight">The incoming weight.</param>
        /// <returns>A formatted weight.</returns>
        private string FormatWeight(string weight)
        {
            var localWeight = weight.ToDecimal();

            if (localWeight >= 1000)
            {

            }

            return string.Format("{0} kg", Math.Round(localWeight, Settings.Default.WeightsPrecision));
        }

        #endregion

        #endregion
    }
}


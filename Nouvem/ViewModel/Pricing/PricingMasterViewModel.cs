﻿// -----------------------------------------------------------------------
// <copyright file="PricingMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Pricing
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class PricingMasterViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The price lists collection reference.
        /// </summary>
        private ObservableCollection<PriceList> priceLists;

        /// <summary>
        /// The display price lists.
        /// </summary>
        public ObservableCollection<PriceMaster> priceMasters;

        /// <summary>
        /// The display price lists.
        /// </summary>
        public ObservableCollection<PriceMaster> priceMastersCombo;

        /// <summary>
        /// The price lists to update.
        /// </summary>
        private IList<PriceMaster> priceListsToUpdate = new List<PriceMaster>();

        /// <summary>
        /// Display inactive books flag.
        /// </summary>
        private bool displayInactivePriceBooks;

        /// <summary>
        /// The active ids.
        /// </summary>
        private HashSet<int?> activePriceBookIds = new HashSet<int?>();

        /// <summary>
        /// The partners.
        /// </summary>
        private ObservableCollection<BusinessPartner> partners = new ObservableCollection<BusinessPartner>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the PricingMasterViewModel class.
        /// </summary>
        public PricingMasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessPricing);
            this.IsFormLoaded = false;

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);
                if (this.SelectedPriceMaster != null)
                {
                    if (!this.priceListsToUpdate.Contains(this.SelectedPriceMaster))
                    {
                        this.priceListsToUpdate.Add(this.SelectedPriceMaster);
                    }

                    if (ApplicationSettings.PriceListDefaultCurrency > 0)
                    {
                        var localCurrency =
                            this.BPCurrencies.FirstOrDefault(
                                x => x.BPCurrencyID == ApplicationSettings.PriceListDefaultCurrency);

                        if (localCurrency != null)
                        {
                            this.SelectedPriceMaster.BPCurrencyID = localCurrency.BPCurrencyID;
                        }
                    }

                    if (ApplicationSettings.PriceListDefaultRoundingOption > 0)
                    {
                        var localRounding =
                            this.RoundingOptions.FirstOrDefault(
                                x => x.NouRoundingOptionsID == ApplicationSettings.PriceListDefaultRoundingOption);

                        if (localRounding != null)
                        {
                            this.SelectedPriceMaster.NouRoundingOptionsID = localRounding.NouRoundingOptionsID;
                        }
                    }

                    if (ApplicationSettings.PriceListDefaultRounding > 0)
                    {
                        var localRounding =
                            this.RoundingRules.FirstOrDefault(
                                x => x.NouRoundingRulesID == ApplicationSettings.PriceListDefaultRounding);

                        if (localRounding != null)
                        {
                            this.SelectedPriceMaster.NouRoundingRulesID = localRounding.NouRoundingRulesID;
                        }
                    }
                }
            }, this.CanUpdate);

            this.OnCellValueChangedCommand = new RelayCommand<DevExpress.Xpf.Grid.CellValueChangedEventArgs>((e) =>
            {
                #region validation

                if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
                {
                    return;
                }

                #endregion

                if (this.SelectedPriceMaster != null)
                {
                    this.SelectedPriceMaster.CreationDate = DateTime.Today;

                    if (!e.Cell.Property.Equals("BPMasterID"))
                    {
                        return;
                    }

                    if (this.SelectedPriceMaster.BPMasterID > 0)
                    {
                        var partner = this.Partners.FirstOrDefault(x => x.Details.BPMasterID == this.SelectedPriceMaster.BPMasterID);
                        if (partner != null)
                        {
                            this.SelectedPriceMaster.CurrentPriceListName = partner.Details.Name;
                        }
                    }
                }
            });

            // Display the price list details
            this.ShowPriceListDetailCommand = new RelayCommand(this.DrillDownCommandExecute);

            this.RemovePriceListCommand = new RelayCommand(() =>
            {
                this.RemovePriceListCommandExecute();
            }, () => this.IsFormLoaded);

            this.OnLoadedCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = true;
            });

            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                this.PriceLists?.Clear();
                this.PriceMasters?.Clear();
                this.IsFormLoaded = false;
                this.Close();
            });

            #endregion

            #region instantiation

            this.PriceMasters = new ObservableCollection<PriceMaster>();
            this.PriceLists = new ObservableCollection<PriceList>();
            this.RoundingOptions = new ObservableCollection<NouRoundingOption>();
            this.RoundingRules = new ObservableCollection<NouRoundingRule>();
            this.BPCurrencies = new ObservableCollection<BPCurrency>();
            this.LocalPriceMasters = new List<PriceMaster>();

            #endregion

            this.GetActivePriceBookIds();
            this.GetPriceMasters();
            //this.GetPriceLists();
            this.GetRoundingOptions();
            this.GetRoundingRules();
            this.GetCurrencies();
            this.GetPartners();
            this.IsFormLoaded = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// The price lists collection reference.
        /// </summary>
        public IList<PriceMaster> LocalPriceMasters { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the inactive books are to ber displayed.
        /// </summary>
        public bool DisplayInactivePriceBooks
        {
            get
            {
                return this.displayInactivePriceBooks;
            }

            set
            {
                this.displayInactivePriceBooks = value;
                this.RaisePropertyChanged();

                if (this.LocalPriceMasters.Any())
                {
                    if (value)
                    {
                        var localPrices =
                            this.LocalPriceMasters.Where(x => !this.activePriceBookIds.Contains(x.PriceListID)
                                && x.PriceListID != NouvemGlobal.BasePriceList.PriceListID && x.PriceListID != NouvemGlobal.CostPriceList.PriceListID).ToList();
                        this.PriceMasters = new ObservableCollection<PriceMaster>(localPrices);
                        this.PriceMastersCombo = new ObservableCollection<PriceMaster>(localPrices.OrderBy(x => x.CurrentPriceListName));
                    }
                    else
                    {
                        var localPrices =
                            this.LocalPriceMasters.Where(x => this.activePriceBookIds.Contains(x.PriceListID)).ToList();
                        this.PriceMasters = new ObservableCollection<PriceMaster>(localPrices);
                        this.PriceMastersCombo = new ObservableCollection<PriceMaster>(localPrices.OrderBy(x => x.CurrentPriceListName));

                        var costList = this.PriceMasters.FirstOrDefault(x => x.PriceListID == NouvemGlobal.CostPriceList.PriceListID);
                        if (costList != null)
                        {
                            this.PriceMasters.Remove(costList);
                            this.PriceMasters.Insert(0, costList);
                        }

                        var baseList = this.PriceMasters.FirstOrDefault(x => x.PriceListID == NouvemGlobal.BasePriceList.PriceListID);
                        if (baseList != null)
                        {
                            this.PriceMasters.Remove(baseList);
                            this.PriceMasters.Insert(0, baseList);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected price master.
        /// </summary>
        public PriceMaster SelectedPriceMaster { get; set; }

        /// <summary>
        /// Get or sets the price masters.
        /// </summary>
        public ObservableCollection<PriceMaster> PriceMasters
        {
            get
            {
                return this.priceMasters;
            }

            set
            {
                this.priceMasters = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the price masters.
        /// </summary>
        public ObservableCollection<PriceMaster> PriceMastersCombo
        {
            get
            {
                return this.priceMastersCombo;
            }

            set
            {
                this.priceMastersCombo = value;
                this.RaisePropertyChanged();
            }
        }
      
        /// <summary>
        /// Get or sets the partners.
        /// </summary>
        public ObservableCollection<BusinessPartner> Partners
        {
            get
            {
                return this.partners;
            }

            set
            {
                this.partners = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the price masters.
        /// </summary>
        public ObservableCollection<PriceList> PriceLists
        {
            get
            {
                return this.priceLists;
            }

            set
            {
                this.priceLists = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the rounding options.
        /// </summary>
        public ObservableCollection<NouRoundingOption> RoundingOptions { get; set; }

        /// <summary>
        /// Get or sets the rounding rules.
        /// </summary>
        public ObservableCollection<NouRoundingRule> RoundingRules { get; set; }

        /// <summary>
        /// Get or sets the bp currencies
        /// </summary>
        public ObservableCollection<BPCurrency> BPCurrencies { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the uom masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to uhandle a cell value change.
        /// </summary>
        public ICommand OnCellValueChangedCommand { get; set; }

        /// <summary>
        /// Gets the command to display the selected price lists details.
        /// </summary>
        public ICommand ShowPriceListDetailCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the selected price list removal.
        /// </summary>
        public ICommand RemovePriceListCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Refresh the grid.
        /// </summary>
        public void Refresh()
        {
            this.GetActivePriceBookIds();
            this.GetPriceMasters();
            //NouvemGlobal.UpdatePriceLists();
            this.priceListsToUpdate.Clear();
            Messenger.Default.Send(Token.Message, Token.RefreshInventory);
        }

        #endregion

        #endregion

        #region protected override

        /// <summary> Remove the vat line.
        /// </summary>
        private void RemovePriceListCommandExecute()
        {
            if (this.SelectedPriceMaster != null)
            {
                NouvemMessageBox.Show(string.Format(Message.RemovePriceListPrompt, this.SelectedPriceMaster.CurrentPriceListName), NouvemMessageBoxButtons.OKCancel);
                if (NouvemMessageBox.UserSelection == UserDialogue.OK)
                {
                    this.SelectedPriceMaster.Deleted = true;
                    this.priceListsToUpdate.Clear();
                    this.priceListsToUpdate.Add(this.SelectedPriceMaster);
                    this.UpdatePriceMaster();
                    SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                }
            }
        }

        /// <summary>
        /// Handle the drill down functionality.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            if (this.CanDisplayDetail())
            {
                this.IsFormLoaded = false;
                SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
                Messenger.Default.Send(ViewType.PriceListDetail);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdatePriceMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Validates new price list entrys.
        /// </summary>
        /// <returns>A flag, indicating a validation or not.</returns>
        private bool Validate()
        {
            // Get the new price lists(updated price lists will always validate)
            var newPriceLists = this.priceListsToUpdate.Where(x => x.PriceListID == 0).ToList();

            if (newPriceLists.Any())
            {
                var error = string.Empty;
                newPriceLists.ForEach(x =>
                {
                    if (string.IsNullOrWhiteSpace(x.CurrentPriceListName))
                    {
                        error = Message.PriceListNameEmpty;
                    }
                    else if (x.BPCurrencyID == 0)
                    {
                        error = Message.CurrencyEmpty;
                    }
                    else if (x.NouRoundingOptionsID == 0)
                    {
                        error = Message.RoundingEmpty;
                    }
                    else if (x.NouRoundingRulesID == 0)
                    {
                        error = Message.RoundingRuleEmpty;
                    }
                });

                if (error != string.Empty)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Updates the price master group.
        /// </summary>
        private void UpdatePriceMaster()
        {
            #region validation

            if (!this.priceListsToUpdate.Any())
            {
                return;
            }

            if (!this.Validate())
            {
                return;
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdatePriceMasters(this.priceListsToUpdate))
                {
                    SystemMessage.Write(MessageType.Priority, Message.PriceListUpdated);
                    var localList = this.priceListsToUpdate.LastOrDefault();
                    this.Refresh();
                    this.SelectedPriceMaster = localList;
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PriceListNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.PriceListNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearPricingMaster();
            Messenger.Default.Send(Token.Message, Token.ClosePriceListMasterWindow);
        }

        /// <summary>
        /// Gets the application price masters.
        /// </summary>
        private void GetPriceMasters()
        {
            this.DataManager.GetNewPriceLists();

            var localSelectedPriceMasterId = 0;
            if (this.SelectedPriceMaster != null)
            {
                localSelectedPriceMasterId = this.SelectedPriceMaster.PriceListID;
            }

            var updatedPriceLists = this.DataManager.GetUpdatedPriceLists().OrderBy(x => x.PriceListID).ToList();
            var storedIds = NouvemGlobal.PriceListMasters.Select(x => x.PriceListID).ToList();
            foreach (var updatedPriceList in updatedPriceLists)
            {
                if (storedIds.Contains(updatedPriceList.PriceListID))
                {
                    var localPriceMaster =
                        NouvemGlobal.PriceListMasters.FirstOrDefault(x => x.PriceListID == updatedPriceList.PriceListID);
                    if (localPriceMaster != null)
                    {
                        NouvemGlobal.PriceListMasters.Remove(localPriceMaster);
                    }
                }

                NouvemGlobal.PriceListMasters.Add(updatedPriceList);
            }

            this.LocalPriceMasters = NouvemGlobal.PriceListMasters.ToList();
            this.PriceMasters.Clear();
            foreach (var priceList in this.LocalPriceMasters.Where(x => this.activePriceBookIds.Contains(x.PriceListID)))
            {
                this.PriceMasters.Add(priceList);
            }

            var costList = this.PriceMasters.FirstOrDefault(x => x.PriceListID == NouvemGlobal.CostPriceList.PriceListID);
            if (costList != null)
            {
                this.PriceMasters.Remove(costList);
                this.PriceMasters.Insert(0, costList);
            }

            var baseList = this.PriceMasters.FirstOrDefault(x => x.PriceListID == NouvemGlobal.BasePriceList.PriceListID);
            if (baseList != null)
            {
                this.PriceMasters.Remove(baseList);
                this.PriceMasters.Insert(0, baseList);
            }

            if (localSelectedPriceMasterId > 0)
            {
                this.SelectedPriceMaster =
                    this.PriceMasters.FirstOrDefault(x => x.PriceListID == localSelectedPriceMasterId);
            }

            this.PriceMastersCombo = new ObservableCollection<PriceMaster>(this.PriceMasters.ToList().OrderBy(x => x.CurrentPriceListName));
        }

        /// <summary>
        /// Gets the application price lists.
        /// </summary>
        private void GetPriceLists()
        {
            this.PriceLists.Clear();
            foreach (var priceList in this.DataManager.GetAllPriceLists())
            {
                this.PriceLists.Add(priceList);
            }
        }

        /// <summary>
        /// Gets the application partners.
        /// </summary>
        private void GetPartners()
        {
            this.Partners = new ObservableCollection<BusinessPartner>(NouvemGlobal.BusinessPartners.OrderBy(x => x.Details.Name));
            //this.PartnersCombo = new ObservableCollection<BusinessPartner>(NouvemGlobal.BusinessPartners.OrderBy(x => x.Details.Name));
        }

        /// <summary>
        /// Gets the application currencies.
        /// </summary>
        private void GetCurrencies()
        {
            this.BPCurrencies.Clear();
            foreach (var currency in this.DataManager.GetBusinessPartnerCurrencies())
            {
                this.BPCurrencies.Add(currency);
            }
        }

        /// <summary>
        /// Gets the rounding rules.
        /// </summary>
        private void GetRoundingRules()
        {
            this.RoundingRules.Clear();
            foreach (var rule in this.DataManager.GetRoundingRules())
            {
                this.RoundingRules.Add(rule);
            }
        }

        /// <summary>
        /// Gets the rounding options
        /// </summary>
        private void GetRoundingOptions()
        {
            this.RoundingOptions.Clear();
            foreach (var option in this.DataManager.GetRoundingOptions())
            {
                this.RoundingOptions.Add(option);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        /// <summary>
        /// Determine if the user can display the selected price list details.
        /// </summary>
        /// <returns>A flag, indicating whether the user can display the selected price list details.</returns>
        private bool CanDisplayDetail()
        {
            return !(this.SelectedPriceMaster == null || this.SelectedPriceMaster.PriceListID == 0);
        }

        /// <summary>
        /// Gets the active price book ids.
        /// </summary>
        private void GetActivePriceBookIds()
        {
            this.activePriceBookIds.Clear();
            foreach (var partner in NouvemGlobal.BusinessPartners)
            {
                this.activePriceBookIds.Add(partner.Details.PriceListID);
            }

            if (NouvemGlobal.BasePriceList != null)
            {
                this.activePriceBookIds.Add(NouvemGlobal.BasePriceList.PriceListID);
            }

            if (NouvemGlobal.CostPriceList != null)
            {
                this.activePriceBookIds.Add(NouvemGlobal.CostPriceList.PriceListID);
            }

            this.GetPriceLists();
            foreach (var futureBook in this.PriceLists.Where(x => x.EndDate == null || x.EndDate.ToDate().Date > DateTime.Today))
            {
                this.activePriceBookIds.Add(futureBook.PriceListID);
            }
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="ProductSelectionViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Pricing
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.Shared;

    public class ProductSelectionViewModel : NouvemViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the ProductSelectionViewModel class.
        /// </summary>
        public ProductSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessPricing);

            #region message registration

            Messenger.Default.Register<Tuple<bool, int?>>(this, Token.UpdateProductSelections, tuple => this.UpdateSelections(tuple.Item1, tuple.Item2));

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.Products = new ObservableCollection<ProductSelection>();

            #endregion

            this.GetProducts();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Get or sets the products.
        /// </summary>
        public ObservableCollection<ProductSelection> Products { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the containers.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Updates all the selected nodes, sub nodes 'add to list' property'.
        /// </summary>
        /// <param name="selected">The add to list selection value.</param>
        /// <param name="nodeID">The node selected.</param>
        public void UpdateSelections(bool selected, int? nodeID)
        {
            // Set the nodes 'sub nodes' add to list property to that of the selected value.
            foreach (var product in this.Products.Where(x => x.ParentID == nodeID))
            {
                product.AddToList = selected;
            }
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.AddToPriceListDetails();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the containers group.
        /// </summary>
        private void AddToPriceListDetails()
        {
            // get the id's of the products to be added
            var productsToAdd = new HashSet<int>();
            this.Products.ToList().ForEach(x =>
            {
                // If it's selected, and it's a leaf inventory master product, add it.
                if (x.AddToList && x.IsLeafNode)
                {
                    productsToAdd.Add(-x.NodeID.ToInt());
                }
            });

            #region validation

            if (!productsToAdd.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductsSelected);
                return;
            }

            #endregion

            try
            {
                if (this.Locator.PriceListDetail.AddSpecifiedProdcts(productsToAdd))
                {
                    SystemMessage.Write(MessageType.Priority, Message.ProductsAdded);
                    this.Close();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.ProductsNotAdded);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.ProductsNotAdded);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearProductSelection();
            Messenger.Default.Send(Token.Message, Token.CloseProductSelectionWindow);
        }

        /// <summary>
        /// Gets the application products.
        /// </summary>
        private void GetProducts()
        {
            this.Products.Clear();
            foreach (var product in this.DataManager.GetGroupedProducts().OrderBy(x => x.ProductName))
            {
                this.Products.Add(product);
            }
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPricesViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Pricing
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class SpecialPricesViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The special price lists.
        /// </summary>
        private ObservableCollection<SpecialPrice> specialPriceLists;

        /// <summary>
        /// The historic special price lists.
        /// </summary>
        private IList<SpecialPrice> historicSpecialPriceLists = new List<SpecialPrice>();

        /// <summary>
        /// The current special price lists.
        /// </summary>
        private IList<SpecialPrice> currentSpecialPriceLists = new List<SpecialPrice>();

        /// <summary>
        /// The prices to update.
        /// </summary>
        private HashSet<SpecialPrice> specialPricesToUpdate = new HashSet<SpecialPrice>();

        /// <summary>
        /// The selected special price.
        /// </summary>
        private SpecialPrice selectedSpecialPrice;

        /// <summary>
        /// The customer groups.
        /// </summary>
        private ObservableCollection<BPGroup> customerGroups;

        /// <summary>
        /// The customers.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.BusinessPartner> customers;

        /// <summary>
        /// The inventory groups.
        /// </summary>
        private ObservableCollection<INGroup> inventoryGroups;

        /// <summary>
        /// The products.
        /// </summary>
        private ObservableCollection<InventoryItem> saleItems;

        /// <summary>
        /// The pshow historic prices flag.
        /// </summary>
        private bool showHistoricPrices;
     
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialPricesViewModel"/> class.
        /// </summary>
        public SpecialPricesViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<int,int,int>>(this, Token.SetSpecialPrice, this.SetSelectedSpecialPrice);

            #endregion

            #region command handler

            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                #region validation

                if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property))
                {
                    return;
                }

                #endregion

                if (this.selectedSpecialPrice != null)
                {
                    this.SetControlMode(ControlMode.Update);
                    this.specialPricesToUpdate.Add(this.selectedSpecialPrice);

                    if (e.Cell.Property.Equals("BPGroupID"))
                    {
                        if ((int) e.Value > 0)
                        {
                            this.SelectedSpecialPrice.BPMasterID = -1;
                        }
                    }
                    else if (e.Cell.Property.Equals("BPMasterID"))
                    {
                        if ((int)e.Value > 0)
                        {
                            this.SelectedSpecialPrice.BPGroupID = -1;
                        }
                    }
                }
            });

            this.DrillDownToMasterItemCommand = new RelayCommand(this.DrillDownToMasterItemCommandExecute);
            this.DrillDownToPartnerCommand = new RelayCommand(this.DrillDownToPartnerCommandExecute);

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetCustomers();
            this.GetCustomerGroups();
            this.GetInventoryGroups();
            this.GetProducts();
            this.GetSpecialPriceLists();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether we are displaying historic prices.
        /// </summary>
        public bool ShowHistoricPrices
        {
            get
            {
                return this.showHistoricPrices;
            }

            set
            {
                this.showHistoricPrices = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SpecialPriceLists = new ObservableCollection<SpecialPrice>(this.historicSpecialPriceLists);
                }
                else
                {
                    this.SpecialPriceLists = new ObservableCollection<SpecialPrice>(this.currentSpecialPriceLists);
                }
            }
        }

        /// <summary>
        /// Gets or sets the special price lists.
        /// </summary>
        public ObservableCollection<SpecialPrice> SpecialPriceLists
        {
            get
            {
                return this.specialPriceLists;
            }

            set
            {
                this.specialPriceLists = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected special price
        /// </summary>
        public SpecialPrice SelectedSpecialPrice
        {
            get
            {
                return this.selectedSpecialPrice;
            }

            set
            {
                this.selectedSpecialPrice = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customer groups.
        /// </summary>
        public ObservableCollection<BPGroup> CustomerGroups
        {
            get
            {
                return this.customerGroups;
            }

            set
            {
                this.customerGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.BusinessPartner> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.customers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryGroups
        {
            get
            {
                return this.inventoryGroups;
            }

            set
            {
                this.inventoryGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        public ObservableCollection<InventoryItem> SaleItems
        {
            get
            {
                return this.saleItems;
            }

            set
            {
                this.saleItems = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Drill down to item command.
        /// </summary>
        public ICommand DrillDownToPartnerCommand  { get; set; }

        /// <summary>
        /// Drill down to item command.
        /// </summary>
        public ICommand DrillDownToMasterItemCommand  { get; set; }

        /// <summary>
        /// Gets the update command.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Remove the label association.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedSpecialPrice!= null)
            {
                this.selectedSpecialPrice.Deleted = DateTime.Now;
                //this.UpdateLabelAssociations(new List<LabelsAssociation> { this.selectedLabelAssociation });
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.GetSpecialPriceLists();
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateSpecialPrices(this.specialPricesToUpdate.ToList());
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        private void DrillDownToMasterItemCommandExecute()
        {
            if (this.selectedSpecialPrice == null)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedSpecialPrice.INMasterID), Token.DrillDown);
        }

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        private void DrillDownToPartnerCommandExecute()
        {
            if (this.selectedSpecialPrice == null)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.BPMaster, this.SelectedSpecialPrice.BPMasterID.ToInt()), Token.DrillDown);
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the selected special price to that of the incoming price data.
        /// </summary>
        /// <param name="data">The incoming price data.</param>
        private void SetSelectedSpecialPrice(Tuple<int,int,int> data)
        {
            var localPrices = this.SpecialPriceLists.Where(x => x.StartDate  <= DateTime.Today && x.EndDate >= DateTime.Today).ToList();
            var productId = data.Item1;
            var partnerId = data.Item2;
            var groupId = data.Item3;

            // check product/partner combination first
            var localPrice = localPrices.FirstOrDefault(x => x.INMasterID == productId && x.BPMasterID == partnerId);
            if (localPrice == null)
            {
                // ..check product/partner group combination next
                localPrice = localPrices.FirstOrDefault(x => x.INMasterID == productId && x.BPGroupID == groupId);

                if (localPrice == null)
                {
                    // ..finally, check product/all partners combination
                    localPrice = localPrices
                        .FirstOrDefault(x => x.INMasterID == productId && (x.BPGroupID == null || x.BPGroupID == -1) && (x.BPMasterID == null || x.BPMasterID == -1));
                }
            }

            this.SelectedSpecialPrice = localPrice;
        }

        /// <summary>
        /// Adds/updates the special prices.
        /// </summary>
        /// <param name="prices">The prices to update.</param>
        private void UpdateSpecialPrices(IList<SpecialPrice> prices)
        {
            #region validation

            var error = string.Empty;
            foreach (var price in prices)
            {
                if (price.BPGroupID <= 0)
                {
                    price.BPGroupID = null;
                }

                if (price.BPMasterID <= 0)
                {
                    price.BPMasterID = null;
                }

                if (price.INMasterID <= 0)
                {
                    error = Message.NoProductSelectedForSpecialPrice;
                }

                if (price.EndDate < price.StartDate)
                {
                    error = Message.EndDateAfterStartDateWarning;
                }

                if (!string.IsNullOrEmpty(error))
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    return;
                }
            }

            #endregion 

            if (this.DataManager.AddOrUpdateSpecialPrices(prices))
            {
                SystemMessage.Write(MessageType.Priority, Message.SpecialPricesUpdated);
                this.specialPricesToUpdate.Clear();
                this.SetControlMode(ControlMode.OK);
                this.Refresh();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.SpecialPricesNotUpdated);
            }
        }

        /// <summary>
        /// Refresh the associations.
        /// </summary>
        private void Refresh()
        {
            this.GetSpecialPriceLists();
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(NouvemGlobal.GetSpecialPrices));
        }

        /// <summary>
        /// Gets the label associations.
        /// </summary>
        private void GetSpecialPriceLists()
        {
            var localPrices = this.DataManager.GetSpecialPrices();
            foreach (var localPrice in localPrices)
            {
                if (localPrice.BPGroupID == null && localPrice.BPMasterID == null)
                {
                    localPrice.BPGroupID = -1;
                    localPrice.BPMasterID = -1;
                }
                else if (localPrice.BPGroupID > 0)
                {
                    localPrice.BPMasterID = -1;
                }
                else if (localPrice.BPMasterID > 0)
                {
                    localPrice.BPGroupID = -1;
                }
            }

            this.currentSpecialPriceLists = localPrices.Where(x => x.EndDate >= DateTime.Today).ToList();
            this.historicSpecialPriceLists = localPrices.Where(x => x.EndDate < DateTime.Today).ToList();

            this.SpecialPriceLists
                = new ObservableCollection<SpecialPrice>(this.currentSpecialPriceLists);
        }

        /// <summary>
        /// Gets the customer groups.
        /// </summary>
        private void GetCustomerGroups()
        {
            this.CustomerGroups = new ObservableCollection<BPGroup>(this.DataManager.GetBusinessPartnerGroups());
            this.CustomerGroups.Insert(0, new BPGroup { BPGroupName = Strings.AllGroups, BPGroupID = -1 });
            //this.CustomerGroups.Insert(0, new BPGroup { BPGroupName = Strings.NoneSelected, BPGroupID = -2 });
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        private void GetCustomers()
        {
            this.Customers = new ObservableCollection<Model.BusinessObject.BusinessPartner>(NouvemGlobal.CustomerPartners);
            this.Customers.Insert(0, new Model.BusinessObject.BusinessPartner { Details = new ViewBusinessPartner { Name = Strings.AllCustomers, BPMasterID = -1 } });
            //this.Customers.Insert(0, new Model.BusinessObject.BusinessPartner { Details = new ViewBusinessPartner { Name = Strings.NoneSelected, BPMasterID = -2 } });
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        private void GetProducts()
        {
            this.SaleItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems);
        }

        /// <summary>
        /// Gets the inventory groups.
        /// </summary>
        private void GetInventoryGroups()
        {
            this.InventoryGroups = new ObservableCollection<INGroup>(this.DataManager.GetInventoryGroups());
            this.InventoryGroups.Insert(0, new INGroup { Name = Strings.AllGroups, INGroupID = -1 });
            this.InventoryGroups.Insert(0, new INGroup { Name = Strings.NoneSelected, INGroupID = -2 });
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearSpecialPrices();
            Messenger.Default.Send(Token.Message, Token.CloseSpecialPricesWindow);
        }

        #endregion

        #endregion
    }
}

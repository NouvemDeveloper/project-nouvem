﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.StartUp;

namespace Nouvem.ViewModel.EPOS
{
    public class EposLoggedOutViewModel : EntryViewModel
    {
        #region field

        /// <summary>
        /// The display message.
        /// </summary>
        private string displayMessage;

        #endregion

            #region constructor

        /// <summary>
        /// Initializes a new instance of the EposLoggedOutViewModel class.
        /// </summary>
        public EposLoggedOutViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.DisplayMessage = string.Format("{0}...", Strings.LoggedOut);
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the ui display message.
        /// </summary>
        public string DisplayMessage
        {
            get
            {
                return this.displayMessage;
            }

            set
            {
                this.displayMessage = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion
    }
}

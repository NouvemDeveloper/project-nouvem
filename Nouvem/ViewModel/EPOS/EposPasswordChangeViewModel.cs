﻿// -----------------------------------------------------------------------
// <copyright file="EposPasswordChangeViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Nouvem.ViewModel.EPOS
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Global;
    using Nouvem.ViewModel.User;

    public class EposPasswordChangeViewModel : UserChangePasswordViewModel
    {
        /// <summary>
        /// Initializes a new instance of the EposPasswordChangeViewModel class.
        /// </summary>
        public EposPasswordChangeViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration
         
            Messenger.Default.Register<string>(this, Token.PasswordChangeConfirmation, s => NouvemMessageBox.Show(s, touchScreen: true));

            #endregion
        }
    }
}

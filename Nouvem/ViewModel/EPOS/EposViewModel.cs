﻿// -----------------------------------------------------------------------
// <copyright file="EposViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.EPOS
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Media;
    using System.Windows.Input;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Printing;
    using Nouvem.Printing.BusinessObject;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class EposViewModel : NouvemViewModelBase
    {
        #region private

        /// <summary>
        /// The print reference.
        /// </summary>
        private EposPrint print;

        /// <summary>
        /// The total ex vat.
        /// </summary>
        private decimal totalExVat;

        /// <summary>
        /// The payments screen top left navigation message.
        /// </summary>
        private string paymentScreenNavigationMessage;

        /// <summary>
        /// The invoice details reference i.e. header, and item details.
        /// </summary>
        private Tuple<InvoiceHeader, List<InvoiceDetail>> invoiceDetails;

        /// <summary>
        /// The products purchased data reference.
        /// </summary>
        private ObservableCollection<ProductData> purchaseData;

        /// <summary>
        /// Gets or sets the application selected group products.
        /// </summary>
        private ObservableCollection<ProductData> products;

        /// <summary>
        /// Gets or sets the application products.
        /// </summary>
        private IList<ProductData> allProducts;

        /// <summary>
        /// THe selected product.
        /// </summary>
        private ProductData selectedProductData;

        /// <summary>
        /// The application price lists.
        /// </summary>
        private IList<PriceMaster> priceLists;

        /// <summary>
        /// The currently selected group.
        /// </summary>
        private INGroup currentGroup;

        /// <summary>
        /// The selected purchased product.
        /// </summary>
        private ProductData selectedPurchaseData;

        /// <summary>
        /// The selected product group.
        /// </summary>
        private INGroup selectedProductGroup;

        /// <summary>
        /// The user input touch pad string total.
        /// </summary>
        private string touchPadValueString;

        /// <summary>
        /// The user input touch pad total.
        /// </summary>
        private decimal touchPadValue;

        /// <summary>
        /// The change to give total.
        /// </summary>
        private string changeToGive;

        /// <summary>
        /// The running purchase total.
        /// </summary>
        private decimal total;

        /// <summary>
        /// The running goods receipt total.
        /// </summary>
        private int totalGoods;

        /// <summary>
        /// The current transaction mode.
        /// </summary>
        private TransactionMode transactionMode;

        /// <summary>
        /// The media player reference.
        /// </summary>
        private MediaPlayer mediaPlayer;

        /// <summary>
        /// Flag, as to whether a tranaction is fully completed or not.
        /// </summary>
        private bool transactionCompleted;

        /// <summary>
        /// The currently selected epos view model reference.
        /// </summary>
        private NouvemViewModelBase currentEposViewModel;

        /// <summary>
        /// The logged on/off status flag.
        /// </summary>
        private bool logStatus;

        /// <summary>
        /// The logged on/off display string.
        /// </summary>
        private string logMessage;

        /// <summary>
        /// The logged in user string.
        /// </summary>
        private string loggedInUser;

        /// <summary>
        /// The selected epos account partner.
        /// </summary>
        private BusinessPartner selectedPartner;

        /// <summary>
        /// The epos partners reference.
        /// </summary>
        private ObservableCollection<BusinessPartner> partners;

        /// <summary>
        /// Flag, as to whether the virtual keyboard is displayed or not.
        /// </summary>
        private bool showKeyboard;

        /// <summary>
        /// Flag, as to whether the calender is displayed or not.
        /// </summary>
        private bool showCalender;

        /// <summary>
        /// The epos transaction types.
        /// </summary>
        private IList<NouTransactionType> transactionTypes;
        
        /// <summary>
        /// The epos transaction type.
        /// </summary>
        private NouTransactionType eposTransactionType;

        /// <summary>
        /// The stock adjust transaction type.
        /// </summary>
        private NouTransactionType stockAdjustTransactionType;

        /// <summary>
        /// The goods receipt transaction type.
        /// </summary>
        private NouTransactionType goodsReceiptTransactionType;

        /// <summary>
        /// The goods return transaction type.
        /// </summary>
        private NouTransactionType goodsReturnTransactionType;

        /// <summary>
        /// The goods return to head office transaction type.
        /// </summary>
        private NouTransactionType goodsReturnToHeadOfficeType;

        /// <summary>
        /// The selected sale date.
        /// </summary>
        private DateTime? selectedDate;

        /// <summary>
        /// The retrieved epos sales.
        /// </summary>
        private ObservableCollection<EposSale> eposSales;

        /// <summary>
        /// The retrieved epos sales.
        /// </summary>
        private ObservableCollection<EposSale> eposUnpaidSales;

        /// <summary>
        /// The selected epos sale.
        /// </summary>
        private EposSale selectedEposSale;

        /// <summary>
        /// The selected unpaid epos sale.
        /// </summary>
        private EposSale selectedUnpaidEposSale;

        /// <summary>
        /// The invoice no.
        /// </summary>
        private int? invoiceNumber;

        /// <summary>
        /// Gets the factory location warehouses.
        /// </summary>
        private IList<Warehouse> warehouses;

        /// <summary>
        /// The epos stock items.
        /// </summary>
        private ObservableCollection<ProductData> stock;

        /// <summary>
        /// The selected stock item quantity.
        /// </summary>
        private int stockQuantity;

        /// <summary>
        /// The selected stock record.
        /// </summary>
        private ProductData selectedStock;

        /// <summary>
        /// Flag, as to whether the passwrd change screen is visible.
        /// </summary>
        private bool passwordChangeMode;

        /// <summary>
        /// The report/cose button content.
        /// </summary>
        private string reportCloseButtonContent;

        /// <summary>
        /// The print receipt flag.
        /// </summary>
        private bool printReceiptOn;

        /// <summary>
        /// The vat receipt flag.
        /// </summary>
        private bool vatReceiptOn;

        /// <summary>
        /// The vat receipt string value.
        /// </summary>
        private string vatReceiptString;

        /// <summary>
        /// The vat receipt string value.
        /// </summary>
        private string printReceiptString;

        /// <summary>
        /// Flag that indicates if we are in goods return mode.
        /// </summary>
        private bool goodsReturnMode;

        /// <summary>
        /// The media player.
        /// </summary>
        private SoundPlayer soundPlayer;

        /// <summary>
        /// The sale delivery method.
        /// </summary>
        private NouDeliveryMethod deliveryMethod;

        /// <summary>
        /// The sale payment type.
        /// </summary>
        private NouPaymentType paymentType;

        /// <summary>
        /// The delivery methods collection.
        /// </summary>
        private IList<NouDeliveryMethod> deliveryMethods;

        /// <summary>
        /// The payment types collection.
        /// </summary>
        private IList<NouPaymentType> paymentTypes;

        private IList<ProductData> openingStock;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the EposViewModel class.
        /// </summary>
        public EposViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.CalenderClosed, x => this.ShowCalender = false);

            // The password change window has closed, so recind the change mode.
            Messenger.Default.Register<string>(this, Token.CloseUserChangePasswordWindow, s => this.passwordChangeMode = false);

            // reset the touchpad
            Messenger.Default.Register<string>(this, Token.ResetTouchPad, s => this.SetTouchPadValue("0"));

            // update the product data.
            Messenger.Default.Register<string>(this, Token.UpdateEpos, s =>
            {
                this.GetProductData();
                this.HandleGroupChange();
            });

            #endregion

            #region command

            // The command to handle a touch pad selection.
            this.TouchPadCommand = new RelayCommand<string>(this.TouchPadCommandExecute);

            // The command to handle a product selection.
            this.ProductSelectionCommand = new RelayCommand(this.ProductSelectionExecute);

            // The command to handle the arrows movement.
            this.MoveToItemCommand = new RelayCommand<string>(this.MoveToItemCommandExecute, this.CanNavigatePurchaseItems);

            // The command to open the server/bd set up screen.
            this.ConnectToDBCommand = new RelayCommand(this.ConnectToDBCommandExecute);

            // The command to handle the partner selection load event
            this.OnPartnerSelectionLoadingCommand = new RelayCommand(this.OnPartnerSelectionLoadingCommandExecute);

            // The command to handle the goods receipt load event.
            this.OnGoodsReceiptLoadingCommand = new RelayCommand(this.OnGoodsReceiptLoadingCommandExecute);

            // The command to handle the goods return selection from the goods in screen.
            this.GoodsReturnSelectionCommand = new RelayCommand(() => this.goodsReturnMode = true);

            // The command to handle the goods receipt unload event.
            this.OnGoodsReceiptUnloadedCommand = new RelayCommand(() =>
            {
                if (!this.goodsReturnMode)
                {
                    this.TotalGoods = 0;
                }
            });

            // The command to handle the goods return load event.
            this.OnGoodsReturnLoadingCommand = new RelayCommand(this.OnGoodsReturnLoadingCommandExecute);

            // The command to handle the goods return unload event.
            this.OnGoodsReturnUnloadedCommand = new RelayCommand(() =>
            {
                this.TotalGoods = 0;
                this.goodsReturnMode = false;
            });

            // The command to handle the payemt summary load event (it just calculates the change to give)
            this.OnPaymentLoadingCommand = new RelayCommand(this.OnPaymentLoadingCommandExecute);

            // The command to handle the payemt summary load event (it just calculates the change to give)
            this.OnPaymentUnloadedCommand = new RelayCommand(this.OnPaymentUnloadedCommandExecute);

            // The command to handle the cancel sale load event
            this.OnCancelSaleLoadingCommand = new RelayCommand(this.OnCancelSaleLoadingCommandExecute);

            // The command to handle the cancel sale load event
            this.OnCancelSaleUnloadedCommand = new RelayCommand(this.OnCancelSaleUnloadedCommandExecute);

            // The command to cancel the selected sale.
            this.CancelSaleCommand = new RelayCommand(this.CancelSaleCommandExecute);

            // The command to handle the window closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            // The command to handle the selection of an unpaid account sale, as paid(or unmark to unpaid).
            this.AccountSalePaidCommand = new RelayCommand(() =>
            {
                if (this.SelectedUnpaidEposSale != null)
                {
                    this.SelectedUnpaidEposSale.Paid = !this.SelectedUnpaidEposSale.Paid;
                }
            });

            // The command to handle the window closing.
            this.CloseEposCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.CloseEposWindow));

            // Handle the partner selection, by instructing the view to navigate to the delivery method view.
            this.PartnerSelectedCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.NavigationCommand), () => this.selectedPartner != null);
       
            // Handle the current transaction completion
            this.CompleteTransactionCommand = new RelayCommand(this.CompleteTransactionCommandExecute);

            // Handles the goods receipt processing.
            this.CompleteGoodsReceiptCommand = new RelayCommand(this.CompleteGoodsReceiptCommandExecute);

            // Handles the goods receipt processing.
            this.CompleteReturnStockCommand = new RelayCommand(this.CompleteReturnStockCommandExecute);
           
            // Handles the calender date change event.
            this.OnSelectedDateChangeCommand = new RelayCommand(this.OnSelectedDateChangeCommandExecute);

            // Handles the calender opened event.
            this.OnCalendarOpenedCommand= new RelayCommand(() => this.InvoiceNo = null);

            // Handles the stock adjust load event.
            this.OnStockAdjustLoadingCommand = new RelayCommand(this.OnStockAdjustLoadingCommandExecute);

            // Handles the stock adjust unload event.
            this.OnStockAdjustUnloadCommand= new RelayCommand(this.OnStockAdjustUnloadCommandExecute);

            // Handles the stock adjustment.
            this.AdjustStockCommand = new RelayCommand(this.AdjustStockCommandExecute);

            // Handles the main epos screen load event.
            this.OnEposMainLoadingCommand = new RelayCommand(() => this.PaymentScreenNavigationMessage = Strings.MainEposScreen);

            // Handles the delivery method selection.
            this.DeliveryMethodSelectedCommand = new RelayCommand<string>(this.DeliveryMethodSelectedCommandExecute);

            // Handles the payment type selection.
            this.SelectedPaymentTypeCommand = new RelayCommand<string>(s =>
            {
                this.paymentType = s.Equals(Constant.Cash)
                    ? this.paymentTypes.FirstOrDefault(x => x.PaymentType.CompareIgnoringCase(Constant.Cash))
                    : paymentTypes.FirstOrDefault(x => x.PaymentType.CompareIgnoringCase(Constant.CreditCard));
            });

            // Handles the command to scroll the main products grid.
            this.ScrollCommand = new RelayCommand<string>(s =>
            {
                Messenger.Default.Send(s, Token.EposScroll);
                Messenger.Default.Send(s, Token.MovePasswordFocus);
            });

            // Handles the report/close button selection.
            this.ReportClosePressedCommand = new RelayCommand(() =>
            {
                if (!this.LogStatus)
                {
                    // user logged out, so close.
                    this.OnClosingCommandExecute();
                }
            });

            // Handles the command to scroll the partners grid.
            this.ScrollPartnersCommand = new RelayCommand<string>(s => Messenger.Default.Send(s, Token.EposPartnerScroll));

            // Handles the unpaid sales load event.
            this.OnUnpaidSalesLoadingCommand = new RelayCommand(this.OnUnpaidSalesLoadingCommandExecute);

            // Handles the unpaid sales unload event.
            this.OnUnpaidSalesUnloadedCommand = new RelayCommand(this.OnUnpaidSalesUnloadedCommandExecute);

            // Handles the marking of account sales as paid.
            this.PaidAccountSalesCommand = new RelayCommand(this.PaidAccountSalesCommandExecute);
            
            #endregion

            #region instantiation

            this.Products = new ObservableCollection<ProductData>();
            this.ProductGroups = new ObservableCollection<INGroup>();
            this.PurchaseData = new ObservableCollection<ProductData>();
            this.allProducts = new List<ProductData>();
            this.priceLists = new List<PriceMaster>();
            this.Partners = new ObservableCollection<BusinessPartner>();
            this.transactionTypes = new List<NouTransactionType>();
            this.mediaPlayer = new MediaPlayer();
            this.soundPlayer = new SoundPlayer(Resources.beep);

            #endregion
        
            this.GetProductData();
            this.GetPriceLists();
            this.CurrentEposViewModel = this.Locator.EposLoggedOut;
            this.LogStatus = false;
            this.GetTransactionTypes();
            this.SetTransactionTypes();
            this.GetWarehouses();
            this.GetDeliveryMethods();
            this.GetPaymentTypes();
            this.ReportCloseButtonContent = Strings.CloseUpper;
            this.print = EposPrint.Instance(Settings.Default.EposAddressLine1, Settings.Default.EposAddressLine2, Settings.Default.EposAddressLine3);
            this.PrintReceiptOn = Settings.Default.PrintReceiptOn;
            this.VatReceiptOn = Settings.Default.VatReceiptOn;
            this.PaymentScreenNavigationMessage = Strings.MainEposScreen;
        }

        #endregion

        #region enum

        /// <summary>
        /// Enum that models the current transaction mode.
        /// </summary>
        private enum TransactionMode
        {
            /// <summary>
            /// Product selection mode.
            /// </summary>
            ProductSelection,

            /// <summary>
            /// Goods receipt mode.
            /// </summary>
            GoodsReceipt
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the payments screen navigation message.
        /// </summary>
        public string PaymentScreenNavigationMessage
        {
            get
            {
                return this.paymentScreenNavigationMessage;
            }

            set
            {
                this.paymentScreenNavigationMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user has readonly epos authorisations.
        /// </summary>
        public bool ReadOnlyAuthorisation { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has full epos authorisations.
        /// </summary>
        public bool FullAuthorisation { get; private set; }
      
        /// <summary>
        /// Gets or sets a value indicating whether a receipt is to be printed.
        /// </summary>
        public bool PrintReceiptOn
        {
            get
            {
                return this.printReceiptOn;
            }

            set
            {
                this.printReceiptOn = value;
                this.RaisePropertyChanged();

                this.PrintReceiptString = value ? Strings.PrintReceiptOn : Strings.PrintReceiptOff;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the receipt contains the vat.
        /// </summary>
        public bool VatReceiptOn
        {
            get
            {
                return this.vatReceiptOn;
            }

            set
            {
                this.vatReceiptOn = value;
                this.RaisePropertyChanged();

                this.VatReceiptString = value ? Strings.VatReceiptOn : Strings.VatReceiptOff;
            }
        }

        /// <summary>
        /// Gets or sets the vat receipt string.
        /// </summary>
        public string VatReceiptString
        {
            get
            {
                return this.vatReceiptString;
            }

            set
            {
                this.vatReceiptString = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the print receipt string.
        /// </summary>
        public string PrintReceiptString
        {
            get
            {
                return this.printReceiptString;
            }

            set
            {
                this.printReceiptString = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected stock quantity.
        /// </summary>
        public int StockQuantity
        {
            get
            {
                return this.stockQuantity;
            }

            set
            {
                this.stockQuantity = value;
                this.RaisePropertyChanged();

                if (this.selectedStock != null && value != this.SelectedStock.StockQuantity)
                {
                    // update the selected stocks quantity.
                    this.SelectedStock.StockQuantity = value;
                    this.SelectedStock.StockAdjusted = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected stock.
        /// </summary>
        public ProductData SelectedStock
        {
            get
            {
                return this.selectedStock;
            }

            set
            {
                this.selectedStock = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.StockQuantity = value.StockQuantity;
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected invoice no.
        /// </summary>
        public int? InvoiceNo
        {
            get
            {
                return this.invoiceNumber;
            }

            set
            {
                this.invoiceNumber = value;
                this.RaisePropertyChanged();

                if (!value.IsNullOrZero())
                {
                    this.GetSystemSales();
                }
            }
        }

        /// <summary>
        /// Gets or sets the retrieved epos stock.
        /// </summary>
        public ObservableCollection<ProductData> Stock
        {
            get
            {
                return this.stock;
            }

            set
            {
                this.stock = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the retrieved epos sales.
        /// </summary>
        public ObservableCollection<EposSale> EposSales
        {
            get
            {
                return this.eposSales;
            }

            set
            {
                this.eposSales= value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the retrieved unpai epos sales.
        /// </summary>
        public ObservableCollection<EposSale> UnpaidEposSales
        {
            get
            {
                return this.eposUnpaidSales;
            }

            set
            {
                this.eposUnpaidSales = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the selected epos sale.
        /// </summary>
        public EposSale SelectedEposSale
        {
            get
            {
                return this.selectedEposSale;
            }

            set
            {
                this.selectedEposSale= value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected unpaid epos sale.
        /// </summary>
        public EposSale SelectedUnpaidEposSale
        {
            get
            {
                return this.selectedUnpaidEposSale;
            }

            set
            {
                this.selectedUnpaidEposSale = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected sale date.
        /// </summary>
        public DateTime? SelectedDate
        {
            get
            {
                return this.selectedDate;
            }

            set
            {
                this.selectedDate = value;
                this.RaisePropertyChanged();

                // Gets the system sales for the new date.
                this.GetSystemSales();
            }
        }

        /// <summary>
        /// Gets or sets the epos partners.
        /// </summary>
        public ObservableCollection<BusinessPartner> Partners
        {
            get
            {
                return this.partners;
            }

            set
            {
                this.partners = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected account partner.
        /// </summary>
        public BusinessPartner SelectedPartner
        {
            get
            {
                return this.selectedPartner;
            }

            set
            {
                this.selectedPartner = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the logged in user name.
        /// </summary>
        public string LoggedInUser
        {
            get
            {
                return this.loggedInUser;
            }

            set
            {
                this.loggedInUser = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the logged on/off message to display.
        /// </summary>
        public string LogMessage
        {
            get
            {
                return this.logMessage;
            }

            set
            {
                this.logMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a user is logged on/off.
        /// </summary>
        public bool LogStatus
        {
            get
            {
                return this.logStatus;
            }

            set
            {
                if (value)
                {
                    // logging in
                    this.LogIn();
                    //this.ReadOnlyAuthorisation = this.AuthorisationCheck(Authorisation.AllowUserToAccessEpos, Constant.ReadOnly);
                    this.FullAuthorisation = true;//this.AuthorisationCheck(Authorisation.AllowUserToAccessEpos, Constant.FullAccess);
                    this.ReportCloseButtonContent = Strings.ReportsUpper;
                    return;
                }

                this.logStatus = false;
                this.RaisePropertyChanged();

                // logging out
                this.LogOut();
                this.ReportCloseButtonContent = Strings.CloseUpper;
            }
        }

        /// <summary>
        /// Gets or sets the report/close button content.
        /// </summary>
        public string ReportCloseButtonContent
        {
            get
            {
                return this.reportCloseButtonContent;
            }

            set
            {
                this.reportCloseButtonContent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current view model to be displayed in the content area.
        /// </summary>
        public NouvemViewModelBase CurrentEposViewModel
        {
            get
            {
                return this.currentEposViewModel;
            }

            set
            {
                if (this.currentEposViewModel == value)
                {
                    return;
                }

                this.currentEposViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the touch pad selection value.
        /// </summary>
        public string TouchPadValueString
        {
            get
            {
                return this.touchPadValueString;
            }

            set
            {
                if (!this.passwordChangeMode && (string.IsNullOrWhiteSpace(value) || value.StartsWith(".")))
                {
                    this.TouchPadValueString = "0";
                    return;
                }

                this.touchPadValueString = value;
                this.RaisePropertyChanged();

                if (!value.EndsWith("."))
                {
                    this.TouchPadValue = value.ToDecimal();
                }

                if (this.passwordChangeMode && !value.Contains("."))
                {
                    if (value.StartsWith("0"))
                    {
                        value = value.Remove(0, 1);
                    }
                    
                    // The password change ui is being displayed, so we send the touch pad value to it.
                    Messenger.Default.Send(value, Token.SetEposSelection);
                    return;
                }
            }
        }

        /// <summary>
        /// Gets or sets the touch pad selection value.
        /// </summary>
        public decimal TouchPadValue
        {
            get
            {
                return this.touchPadValue;
            }

            set
            {
                this.touchPadValue = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the change to give value.
        /// </summary>
        public string ChangeToGive
        {
            get
            {
                return this.changeToGive;
            }

            set
            {
                this.changeToGive = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application products.
        /// </summary>
        public ObservableCollection<ProductData> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.products = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application product groups.
        /// </summary>
        public ObservableCollection<INGroup> ProductGroups { get; set; }

        /// <summary>
        /// Gets or sets the running total.
        /// </summary>
        public decimal Total
        {
            get
            {
                return this.total;
            }

            set
            {
                this.total = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the running goods receipt total.
        /// </summary>
        public int TotalGoods
        {
            get
            {
                return this.totalGoods;
            }

            set
            {
                this.totalGoods = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected product.
        /// </summary>
        public ProductData SelectedProductData
        {
            get
            {
                return this.selectedProductData;
            }

            set
            {
                if (value != null)
                {
                    this.selectedProductData = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected product group.
        /// </summary>
        public INGroup SelectedProductGroup
        {
            get
            {
                return this.selectedProductGroup;
            }

            set
            {
                this.selectedProductGroup = value;
                this.RaisePropertyChanged();
                this.HandleGroupChange();

                if (value != null)
                {
                    /* the view sets this to null once finished processing, so we need to record this so we can 
                     * immediately update product prices when coming from the main application */
                    this.currentGroup = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected purchased product.
        /// </summary>
        public ProductData SelectedPurchaseData
        {
            get
            {
                return this.selectedPurchaseData;
            }

            set
            {
                this.selectedPurchaseData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the products purchased collection.
        /// </summary>
        public ObservableCollection<ProductData> PurchaseData
        {
            get
            {
                return this.purchaseData;
            }

            set
            {
                this.purchaseData = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether the virtual keyboard is displayed or not.
        /// </summary>
        public bool ShowKeyboard
        {
            get
            {
                return this.showKeyboard;
            }

            set
            {
                this.showKeyboard = value;
                this.RaisePropertyChanged();

                // display/hide the keyboard.
                Messenger.Default.Send(value, Token.DisplayKeyboard);

                if (value)
                {
                    this.SelectedDate = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is displayed or not.
        /// </summary>
        public bool ShowCalender
        {
            get
            {
                return this.showCalender;
            }

            set
            {
                this.showCalender = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // open the calender.
                    Messenger.Default.Send(true, Token.DisplayCalender);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the touch pad button selection.
        /// </summary>
        public ICommand TouchPadCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the goods return selection from the goods in screen.
        /// </summary>
        public ICommand GoodsReturnSelectionCommand { get; private set; }

         /// <summary>
        /// Gets the command to handle the payment type selection.
        /// </summary>
        public ICommand SelectedPaymentTypeCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the product selection.
        /// </summary>
        public ICommand ProductSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the product group selection.
        /// </summary>
        public ICommand ProductGroupSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the arrow grid movements.
        /// </summary>
        public ICommand MoveToItemCommand { get; private set; }

         /// <summary>
        /// Gets the command to handle the payment summary load event.
        /// </summary>
        public ICommand OnPaymentLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the payment unload event.
        /// </summary>
        public ICommand OnPaymentUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the goods receipt load event.
        /// </summary>
        public ICommand OnGoodsReceiptLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the goods receipt unload event.
        /// </summary>
        public ICommand OnGoodsReceiptUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the goods return load event.
        /// </summary>
        public ICommand OnGoodsReturnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the goods return unload event.
        /// </summary>
        public ICommand OnGoodsReturnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the partner selection load event.
        /// </summary>
        public ICommand OnPartnerSelectionLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the partner selection unload event.
        /// </summary>
        public ICommand OnPartnerSelectionUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the cancel sale load event.
        /// </summary>
        public ICommand OnCancelSaleLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the cancel sale unload event.
        /// </summary>
        public ICommand OnCancelSaleUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the payment summary unload event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to instruct the epos window to close.
        /// </summary>
        public ICommand CloseEposCommand { get; private set; }

          /// <summary>
        /// Gets the command to mark an account sale as paid.
        /// </summary>
        public ICommand AccountSalePaidCommand { get; private set; }

        /// <summary>
        /// Gets the command to open the server set up window.
        /// </summary>
        public ICommand ConnectToDBCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the tranaction completion.
        /// </summary>
        public ICommand CompleteTransactionCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the partner selection.
        /// </summary>
        public ICommand PartnerSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the goods receipt processing.
        /// </summary>
        public ICommand CompleteGoodsReceiptCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the stock returns.
        /// </summary>
        public ICommand CompleteReturnStockCommand { get; private set; }

        /// <summary>
        /// Gets the command to cancel the selected sale.
        /// </summary>
        public ICommand CancelSaleCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the calendar date change event.
        /// </summary>
        public ICommand OnSelectedDateChangeCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the calendar opened event.
        /// </summary>
        public ICommand OnCalendarOpenedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the calendar opened event.
        /// </summary>
        public ICommand OnCalendarClosedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the stock adjust load event.
        /// </summary>
        public ICommand OnStockAdjustLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the stock adjust unload event.
        /// </summary>
        public ICommand OnStockAdjustUnloadCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the stock adjustment.
        /// </summary>
        public ICommand AdjustStockCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the delivery method selection.
        /// </summary>
        public ICommand DeliveryMethodSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll through the products.
        /// </summary>
        public ICommand ScrollCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll through the partners.
        /// </summary>
        public ICommand ScrollPartnersCommand { get; private set; }

         /// <summary>
        /// Gets the command to handle the main screen load event.
        /// </summary>
        public ICommand OnEposMainLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unpaid sales load event.
        /// </summary>
        public ICommand OnUnpaidSalesLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unpaid sales unload event.
        /// </summary>
        public ICommand OnUnpaidSalesUnloadedCommand { get; private set; }

          /// <summary>
        /// Gets the command to handle the marking of uppaid account sales as paid.
        /// </summary>
        public ICommand PaidAccountSalesCommand { get; private set; }

         /// <summary>
        /// Gets the command to handle the reports/close button selection.
        /// </summary>
        public ICommand ReportClosePressedCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Updates the products when entering from the main nouvem application.
        /// </summary>
        public void UpdateProductData()
        {
            this.GetProductData();

            if (this.currentGroup != null)
            {
                this.Products.Clear();
                foreach (var product in allProducts.Where(x => x.GroupId == this.currentGroup.INGroupID))
                {
                    this.Products.Add(product);
                }
            }
        }

        /// <summary>
        /// Completes the login process.
        /// </summary>
        public void CompleteLogIn()
        {
            this.CurrentEposViewModel = this;
            this.LoggedInUser = string.Format("{0}: {1}", Strings.LoggedIn, ApplicationSettings.LoggedInUserName);
            this.logStatus = true;
            this.LogMessage = Strings.LogOut;
            this.RaisePropertyChanged("LogStatus");
        }

        #endregion

        #endregion

        #region protected override

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the unpaid sales load event.
        /// </summary>
        private void OnUnpaidSalesLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                return;
            }

            // check user authorisations
            if (!this.FullAuthorisation)
            {
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                return;
            }

            this.GetUnpaidSales();
        }

        /// <summary>
        /// Handles the payment of account sales.
        /// </summary>
        private void PaidAccountSalesCommandExecute()
        {
            var accountPaymentType =
                this.paymentTypes.FirstOrDefault(x => x.PaymentType.CompareIgnoringCase(Constant.Account));

            if (accountPaymentType == null)
            {
                this.Log.LogError(this.GetType(), "PaidAccountSalesCommandExecute(): no account payment type found");
                return;
            }

            var salesToBePaid = this.UnpaidEposSales.Where(x => x.Paid).ToList();

            if (salesToBePaid.Any())
            {
                this.DataManager.HandleAccountSalesPaid(salesToBePaid, accountPaymentType.NouPaymentTypeID);
            }
            else
            {
                NouvemMessageBox.Show(Message.NoAccountSalesMarked, touchScreen:true);
            }
        }

        /// <summary>
        /// Handles the unpaid sales unload event.
        /// </summary>
        private void OnUnpaidSalesUnloadedCommandExecute()
        {
            this.UnpaidEposSales = null;
        }

        /// <summary>
        /// Handles the stock adjust load event.
        ///  </summary>
        private void OnStockAdjustLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                return;
            }

            // check user authorisations
            if (!this.FullAuthorisation)
            {
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen:true);
                return;
            }

            this.GetStock();
        }

        /// <summary>
        /// Handles the stock adjust unload event.
        /// </summary>
        private void OnStockAdjustUnloadCommandExecute()
        {
            this.Stock = null;
            this.StockQuantity = 0;
        }

        /// <summary>
        /// Handles the delivery method.
        /// </summary>
        /// <param name="s">The method parameter (delivery or collection).</param>
        private void DeliveryMethodSelectedCommandExecute(string s)
        {
            this.deliveryMethod = s.Equals(Constant.Delivery)
                    ? this.deliveryMethods.FirstOrDefault(x => x.Method.CompareIgnoringCase(Constant.Delivery))
                    : deliveryMethods.FirstOrDefault(x => x.Method.CompareIgnoringCase(Constant.Collection));
            this.PaymentScreenNavigationMessage = Strings.DeliveryMethodSelectionScreen;

            // On account, so use the select partners price list.
            this.SetCustomerPricing();
        }

        /// <summary>
        /// Adjusts the selected stock quantity. </summary>
        private void AdjustStockCommandExecute()
        {
            if (this.stock == null)
            {
                NouvemMessageBox.Show(Message.AdjustStockInvalidated, touchScreen: true);
                return;
            }

            if (!this.DataManager.AdjustStock(this.Stock, this.stockAdjustTransactionType, this.warehouses.First()))
            {
                NouvemMessageBox.Show(Message.DatabaseError, touchScreen:true);
            }
        }

        /// <summary>
        /// Handles the calender date change event.
        /// </summary>
        private void OnSelectedDateChangeCommandExecute()
        {
            this.ShowCalender = false;
        }

        /// <summary>
        /// Handles the cancel sale load event logic.
        /// </summary>
        private void OnCancelSaleLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                return;
            }

            // check user authorisations
            //if (!this.AuthorisationCheck(Authorisation.AllowUserToAccessEpos, Constant.FullAccess))
            //{
            //    NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
            //    return;
            //}

            this.SelectedDate = DateTime.Today;
            this.InvoiceNo = null;
        }

        /// <summary>
        /// Handles the cancel sale unload event.
        ///  </summary>
        private void OnCancelSaleUnloadedCommandExecute()
        {
            if (this.EposSales != null)
            {
                this.EposSales.Clear();
            }

            this.SelectedDate = null;
            this.InvoiceNo = null;
        }

        /// <summary>
        /// Handles the selected sale cancellation.
        /// </summary>
        private void CancelSaleCommandExecute()
        {
            if (this.SelectedEposSale == null)
            {
                NouvemMessageBox.Show(Message.CancelSaleInvalidated, touchScreen:true);
                return;
            }

            if (!this.DataManager.CancelEposSale(this.selectedEposSale, this.goodsReturnTransactionType))
            {
                // issue, so inform the user
                NouvemMessageBox.Show(Message.DatabaseError, touchScreen:true);
            }
        }

        /// <summary>
        /// Handles the return stock process.
        /// </summary>
        private void CompleteReturnStockCommandExecute()
        {
            if (this.purchaseData == null || !this.purchaseData.Any())
            {
                NouvemMessageBox.Show(Message.ReturnStockInvalidated, touchScreen: true);
                return;
            }

            this.ProcessReturnedStock();
        }

        /// <summary>
        /// Handles the goods receipt processing.
        /// </summary>
        private void CompleteGoodsReceiptCommandExecute()
        {
            if (this.purchaseData == null || !this.purchaseData.Any())
            {
                NouvemMessageBox.Show(Message.GoodsReceiptInvalidated, touchScreen: true);
                return;
            }

            this.ProcessGoodsReceived();
        }
        
        /// <summary>
        /// Handles the transaction processing logic.
        /// </summary>
        private void CompleteTransactionCommandExecute()
        {
            this.transactionCompleted = true;

            if (this.ChangeToGive != Strings.Invalid && this.Total > 0 && this.ReadOnlyAuthorisation)
            {
                this.changeToGive = this.changeToGive.Remove(0, 1);
                this.CreateNewInvoice();
            }
            else
            {
                NouvemMessageBox.Show(Message.SaleInvalidated, touchScreen: true);
            }
        }

        /// <summary>
        /// Handles the epos closing event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            // save the print/vat receipt values.
            Settings.Default.PrintReceiptOn = this.printReceiptOn;
            Settings.Default.VatReceiptOn = this.vatReceiptOn;
            Settings.Default.Save();

            // If the user is logged in, and has full epos authorisations, open the main nouvenm application.
            if (this.LogStatus && this.FullAuthorisation)
            {
                Messenger.Default.Send(Token.Message, Token.CreateMasterView);
            }
            else
            {
                Messenger.Default.Send(Token.Message, Token.CloseEposWindow);
            }
        }

        /// <summary>
        /// Handles the payment load event.
        /// </summary>
        private void OnPaymentLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                this.ChangeToGive = "€0";
                return;
            }

            if (!this.ReadOnlyAuthorisation)
            {
                this.PurchaseData.Clear();
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen:true);
                return;
            }

            if (this.TouchPadValue.DecimalPrecision() > 2)
            {
                NouvemMessageBox.Show(string.Format(Message.InvalidPaymentAmount, string.Format("€{0}", this.touchPadValue)), touchScreen: true);
                this.SetTouchPadValue("0");
                this.ChangeToGive = Strings.Invalid;
                this.transactionCompleted = false;
                return;
            }

            if (this.TouchPadValue == 0)
            {
                // A touch pad value of 0 means the exact amount has been tendered
                this.SetTouchPadValue(this.Total.ToString());
                this.ChangeToGive = "€0";
            }
            else
            {
                var change = this.TouchPadValue - this.Total;

                if (change < 0)
                {
                    ChangeToGive = Strings.Invalid;
                    NouvemMessageBox.Show(string.Format(Message.InsufficientPaymentAmount, string.Format("€{0}", this.touchPadValue)), touchScreen: true);
                }
                else
                {
                    this.ChangeToGive = string.Format("€{0}", change); //.PadDecimalRight('0', 2);
                }
            }

            this.SelectedPurchaseData = null;
            this.transactionCompleted = false;
        }

        /// <summary>
        /// Handles the payments unload event.
        /// </summary>
        private void OnPaymentUnloadedCommandExecute()
        {
            if (this.transactionCompleted)
            {
                this.ChangeToGive = "€0";
                this.Total = 0;
                this.totalExVat = 0;
                this.PurchaseData.Clear();
            }

            this.SetTouchPadValue("0");
        }

          /// <summary>
        /// Handles the partner selection load event.
        /// </summary>
        private void OnGoodsReceiptLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                this.PurchaseData.Clear();
                return;
            }

            // check user authorisations
            if (!this.FullAuthorisation)
            {
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                this.PurchaseData.Clear();
                return;
            }

            this.SelectedPurchaseData = null;
            this.purchaseData.ToList().ForEach(x => this.TotalGoods += x.Quantity);
        }

        /// <summary>
        /// Handles the goods return load event.
        /// </summary>
        private void OnGoodsReturnLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                this.PurchaseData.Clear();
                return;
            }

            // check user authorisations
            if (!this.FullAuthorisation)
            {
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                this.PurchaseData.Clear();
                return;
            }

            this.SelectedPurchaseData = null;
        }
               
        /// <summary>
        /// Handles the partner selection load event.
        /// </summary>
        private void OnPartnerSelectionLoadingCommandExecute()
        {
            if (!this.logStatus)
            {
                // user logged out
                NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                this.SetTouchPadValue("0");
                this.PurchaseData.Clear();
                return;
            }

            if (!this.ReadOnlyAuthorisation)
            {
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen:true);
                return;
            }

            this.ShowKeyboard = false;
            this.GetAccountPartners();
            this.SelectedPartner = null;
        }

        /// <summary>
        /// Handles the touch pad selection.
        /// </summary>
        /// <param name="selection">The pad button selected.</param>
        private void TouchPadCommandExecute(string selection)
        {
            this.Beep();
            this.TouchPadProductSelection(selection);
        }

        /// <summary>
        /// Handles the purchases arrow navigation.
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        private void MoveToItemCommandExecute(string direction)
        {
            if (this.EposSales != null && this.EposSales.Any())
            {
                // We are looking at the cancellation screen.
                if (direction.Equals(Constant.Up))
                {
                    this.MoveBackSales();
                    return;
                }

                this.MoveNextSales();
                return;
            }

            if (this.Stock != null && this.Stock.Any())
            {
                // We are looking at the stock adjust screen.
                if (direction.Equals(Constant.Up))
                {
                    this.MoveBackStockAdjust();
                    return;
                }

                this.MoveNextStockAdjust();
                return;
            }

            if (this.UnpaidEposSales != null && this.UnpaidEposSales.Any())
            {
                // We are looking at the uppaid sales screen.
                if (direction.Equals(Constant.Up))
                {
                    this.MoveBackUnpaidSales();
                    return;
                }

                this.MoveNextUnpaidSales();
                return;
            }

            // main epos screen
            if (direction.Equals(Constant.Up))
            {
                this.MoveBack();
                return;
            }

            this.MoveNext();
        }

        /// <summary>
        /// Determines if the purchase data grid can be navigated.
        /// </summary>
        /// <returns></returns>
        private bool CanNavigatePurchaseItems(string direction)
        {
            return this.purchaseData != null;
        }

        /// <summary>
        /// Opens the server/db set up screen.
        /// </summary>
        private void ConnectToDBCommandExecute()
        {
            // password protected, in the unlikely event a client user right double mouse clicks on the logo.
            PasswordEntry.Create(PasswordCheckType.ServerSetUp);

            if (!this.Locator.PasswordEntry.PasswordMatch)
            {
                return;
            }

            // Open the server set up window
            Messenger.Default.Send(Token.Message, Token.CreateServerSetUp);
        }
        
        /// <summary>
        /// Handle the selection of a product.
        /// </summary>
        private void ProductSelectionExecute()
        {
            this.Beep();
            var productSelected = this.purchaseData.FirstOrDefault(x => x.Id == this.selectedProductData.Id);
           
            var value = this.touchPadValue == 0 ? "1" : this.touchPadValueString;

            if (value.Contains("."))
            {
                // decimal value, so invalidate
                NouvemMessageBox.Show(string.Format(Message.InvalidProductAmount, this.touchPadValue), touchScreen:true);
                this.SetTouchPadValue("0");
                return;
            }

            this.SetTouchPadValue(value);

            if (productSelected != null)
            {
                var localTotal = this.touchPadValue.ToInt();

                // product exists in purchased items, so update quantity
                this.SelectedPurchaseData = productSelected;
                this.SelectedPurchaseData.Quantity += localTotal;
            }
            else
            {
                // product doesn't exist in purchased items, so create new.
                var price = this.DataManager.GetPartnerPrice(Settings.Default.BasePriceListId,this.SelectedProductData.Id).ToDecimal();
                var purchaseItem = new ProductData
                {
                    Id = this.selectedProductData.Id,
                    Description = this.selectedProductData.Description,
                    Price = price,
                    Code = this.selectedProductData.Code,
                    Name = this.selectedProductData.Name,
                    MinWeight = this.selectedProductData.MinWeight,
                    MaxWeight = this.selectedProductData.MaxWeight,
                    NominalWeight = this.selectedProductData.NominalWeight,
                    TypicalPieces = this.selectedProductData.TypicalPieces,
                    Quantity = this.touchPadValue.ToInt(),
                    VatRate = this.selectedProductData.VatRate
                };

                this.PurchaseData.Add(purchaseItem);
                this.SelectedPurchaseData = purchaseItem;
            }

            // update the running total.
            this.Total += (this.selectedProductData.PriceIncVat * this.touchPadValue);
            this.totalExVat += (this.selectedProductData.Price * this.touchPadValue);
            this.SetTouchPadValue("0");
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the account partners.
        /// </summary>
        private void GetAccountPartners()
        {
            if (!this.Partners.Any() && NouvemGlobal.BusinessPartners != null)
            {
                this.Partners = new ObservableCollection<BusinessPartner>(
                    NouvemGlobal.BusinessPartners
                    .Where(x => x.PartnerType.Type.CompareIgnoringCase(Constant.Customer))
                    .OrderBy(x => x.Details.Name));
            }
        }

        /// <summary>
        /// Processes the goods received.
        /// </summary>
        private void ProcessGoodsReceived()
        {
            try
            {
                if (!this.DataManager.ProcessGoodsReceived(this.purchaseData, this.goodsReceiptTransactionType, this.warehouses.First()))
                {
                    // issue, so inform the user.
                    NouvemMessageBox.Show(Message.DatabaseError, touchScreen: true);
                }
            }
            finally
            {
                this.PurchaseData.Clear();
                this.SetTouchPadValue("0");
                this.Total = 0;
                this.totalExVat = 0;
            }
        }

        /// <summary>
        /// Processes the stock returns.
        /// </summary>
        private void ProcessReturnedStock()
        {
            try
            {
                if (!this.DataManager.ProcessGoodsReturned(this.purchaseData, this.goodsReturnToHeadOfficeType, this.warehouses.First()))
                {
                    // issue, so inform the user.
                    NouvemMessageBox.Show(Message.DatabaseError, touchScreen: true);
                }
            }
            finally
            {
                this.PurchaseData.Clear();
                this.SetTouchPadValue("0");
                this.Total = 0;
                this.totalExVat = 0;
            }
        }

        /// <summary>
        /// Creates the invoice (header and items) 
        /// </summary>
        private void CreateNewInvoice()
        {
            var receiptData = new List<Printing.BusinessObject.SaleItemData>();
            int? invoiceNo = 0;
            decimal tax = 0;

            try
            {
                // create the invoice header
                var header = new InvoiceHeader
                {
                    CreationDate = DateTime.Now,
                    CreationUserID = Settings.Default.LoggedInUserID,
                    GrandTotalIncVAT = this.total,
                    TotalExVAT = this.totalExVat,
                    Tendered = this.TouchPadValue,
                    Change = this.changeToGive.ToDecimal(),
                    NouDeliveryMethodID = this.deliveryMethod != null ? (int?)this.deliveryMethod.NouDeliveryMethodID : null,
                    NouPaymentTypeID = this.paymentType != null ? (int?)this.paymentType.NouPaymentTypeID : null
                };

                var invoiceData = this.DataManager.AddInvoiceHeader(header, this.selectedPartner);
                
                if (invoiceData != null)
                {
                    var headerId = invoiceData.Item1;
                    invoiceNo = invoiceData.Item2;

                    // header created, so we add the items details.
                    var data = new Dictionary<InvoiceDetail, ProductData>();

                    foreach (var item in this.purchaseData)
                    {
                        var invoice = new InvoiceDetail
                        {
                            INMasterID = item.Id,
                            InvoiceHeaderID = headerId,
                            QuantityOrdered = item.Quantity,
                            QuantityDelivered = item.Quantity,
                            UnitPrice = item.Price,
                            TotalIncVAT = item.PriceIncVat*item.Quantity,
                            TotalExcVAT = item.Price*item.Quantity,
                            VAT = (item.PriceIncVat - item.Price)*item.Quantity,
                            CreationDate = DateTime.Now,
                            Deleted = false
                        };

                        // accumatate the tax total.
                        tax += item.VatAmount;

                        data.Add(invoice, item);
                        receiptData.Add(new SaleItemData { Amount = invoice.QuantityDelivered.ToInt(), Description = item.Description, Total = invoice.TotalIncVAT.ToString()});
                    }

                    var error = this.DataManager.AddInvoiceItems(data, this.eposTransactionType, this.warehouses.First());
                    if (error != string.Empty)
                    {
                        // issue, so inform the user.
                        NouvemMessageBox.Show(error, touchScreen: true);
                    }
                    else if (this.printReceiptOn)
                    {
                        // print the invoice.
                        this.PrintReceipt(receiptData, invoiceNo.ToString(), tax.ToString(), this.total.ToString(), this.TouchPadValue.ToString(), this.ChangeToGive );
                    }
                }
                else
                {
                    // issue, so inform the user.
                    NouvemMessageBox.Show(Message.DatabaseError, touchScreen: true);
                }
            }
            finally
            {
                this.SelectedPartner = null;
                this.deliveryMethod = null;
                this.paymentType = null;
            }
        }

        /// <summary>
        /// Print the invoice receipt.
        /// </summary>
        /// <param name="receiptData">The invoice items.</param>
        /// <param name="invoiceNo">The receipt no.</param>
        /// <param name="tax">The tax.</param>
        /// <param name="saleTotal">The sale total.</param>
        /// <param name="tendered">The amount tendered.</param>
        /// <param name="change">The change given.</param>
        private void PrintReceipt(List<SaleItemData> receiptData, string invoiceNo, string tax, string saleTotal, string tendered, string change)
        {
            try
            {
                this.print.PrintReceipt(receiptData, invoiceNo, tax, saleTotal, tendered, change, this.vatReceiptOn);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Method that verifies a user login.
        /// </summary>
        private void LogIn()
        {
            try
            {
                #region validation

                if (this.touchPadValue == 0)
                {
                    NouvemMessageBox.Show(Message.PasswordEmpty, touchScreen: true);
                    return;
                }

                #endregion

                // verify the user, returning the user details if verified.
                var userDetails = this.DataManager.VerifyLoginUser(this.touchPadValue.ToString());
                var userName = userDetails.Item1;
                var fullName = userDetails.Item2;
                var identifier = userDetails.Item3;
                var alreadyLoggedIn = userDetails.Item4;
                var userSuspended = userDetails.Item5;
                var changePassword = userDetails.Item6;

                if (userSuspended)
                {
                    NouvemMessageBox.Show(Message.UserSuspended, touchScreen: true);
                    return;
                }

                if (userName != string.Empty)
                {
                    ApplicationSettings.LoggedInUser = userName;
                    ApplicationSettings.LoggedInUserName = fullName;
                    ApplicationSettings.LoggedInUserIdentifier = identifier;
                    //Settings.Default.Save();

                    // get the user/device licence credentials.
                    this.CheckForLicence();

                    //if (alreadyLoggedIn)
                    //{
                    //    // user logged in somewhere else, so check if they can force a log off.
                    //    if (NouvemGlobal.CheckAuthorisation(Authorisation.AllowUserToForceLogOff) == Constant.FullAccess)
                    //    {
                    //        // permission granted, so ask the user
                    //        NouvemMessageBox.Show(Message.ForceLogOut, NouvemMessageBoxButtons.YesNo, true);
                    //        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    //        {
                    //            // user doesn't want to force a log out so exit.
                    //            return;
                    //        }

                    //        this.DataManager.ForceLogOff();
                    //    }
                    //    else
                    //    {
                    //        // user does not have permission to force log out the other user, so exit.
                    //        NouvemMessageBox.Show(Message.UserAlreadyLoggedIn, touchScreen: true);
                    //        return;
                    //    }
                    //}

                    if (changePassword)
                    {
                        // user needs to set their own password
                        this.CurrentEposViewModel = this.Locator.EposPasswordChange;
                        this.passwordChangeMode = true;
                        return;
                    }

                    // log in validation complete.
                    this.CurrentEposViewModel = this;
                    this.LoggedInUser = string.Format("{0}: {1}", Strings.LoggedIn, ApplicationSettings.LoggedInUserName);
                    this.logStatus = true;
                    this.LogMessage = Strings.LogOut;
                    this.RaisePropertyChanged("LogStatus");
                }
                else
                {
                    NouvemMessageBox.Show(Message.InvalidLogin, touchScreen: true);
                }
            }
            finally
            {
                this.SetTouchPadValue("0");
            }
        }

        /// <summary>
        /// Logs the user out.
        /// </summary>
        private void LogOut()
        {
            this.LogMessage = Strings.LogIn;
            this.LoggedInUser = Strings.LoggedOut;
            this.PurchaseData.Clear();
            this.Total = 0;
            this.totalExVat = 0;
            this.SetTouchPadValue("0");
            this.CurrentEposViewModel = this.Locator.EposLoggedOut;
        }

        /// <summary>
        /// Get the local licence.
        /// </summary>
        private void CheckForLicence()
        {
            var licenceManager = LicenceManager.Instance;
            licenceManager.CheckForLicence();
        }

        /// <summary>
        /// Moves to the previous item in the purchases grid.
        /// </summary>
        private void MoveBack()
        {
            var previousItem =
                this.PurchaseData.TakeWhile(
                    item => item.Id != this.SelectedPurchaseData.Id)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedPurchaseData = previousItem;
            }
        }

        /// <summary>
        /// Moves to the next item in the purchases grid.
        /// </summary>
        private void MoveNext()
        {
            var nextItem =
                this.PurchaseData.SkipWhile(
                    item => item.Id != this.SelectedPurchaseData.Id)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedPurchaseData = nextItem;
            }
        }

        /// <summary>
        /// Moves to the previous item in the cancellation sales grid.
        /// </summary>
        private void MoveBackSales()
        {
            var previousItem =
                this.EposSales.TakeWhile(
                    item => item.InvoiceHeader.InvoiceHeaderID != this.SelectedEposSale.InvoiceHeader.InvoiceHeaderID)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedEposSale = previousItem;
            }
        }

        /// <summary>
        /// Moves to the next item in the cancellation sales grid.
        /// </summary>
        private void MoveNextSales()
        {
            var nextItem =
                this.EposSales.SkipWhile(
                    item => item.InvoiceHeader.InvoiceHeaderID != this.SelectedEposSale.InvoiceHeader.InvoiceHeaderID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedEposSale = nextItem;
            }
        }

        /// <summary>
        /// Moves to the previous item in the stock adjust grid.
        /// </summary>
        private void MoveBackStockAdjust()
        {
            var previousItem =
                this.Stock.TakeWhile(
                    item => item.Id != this.SelectedStock.Id)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedStock = previousItem;
            }
        }

        /// <summary>
        /// Moves to the next item in the stock adjust grid.
        /// </summary>
        private void MoveNextStockAdjust()
        {
            var nextItem =
                this.Stock.SkipWhile(
                    item => item.Id != this.SelectedStock.Id)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedStock = nextItem;
            }
        }

        /// <summary>
        /// Moves to the previous item in the unpaid sales grid.
        /// </summary>
        private void MoveBackUnpaidSales()
        {
            var previousItem =
                this.UnpaidEposSales.TakeWhile(
                    item => item.InvoiceHeader.InvoiceHeaderID != this.SelectedUnpaidEposSale.InvoiceHeader.InvoiceHeaderID)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedUnpaidEposSale = previousItem;
            }
        }

        /// <summary>
        /// Moves to the next item in the unpaid sales grid.
        /// </summary>
        private void MoveNextUnpaidSales()
        {
            var nextItem =
                this.UnpaidEposSales.SkipWhile(
                    item => item.InvoiceHeader.InvoiceHeaderID != this.SelectedUnpaidEposSale.InvoiceHeader.InvoiceHeaderID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedUnpaidEposSale = nextItem;
            }
        }

        /// <summary>
        /// Removes a quantity of stock from the selected purchased product.
        /// </summary>
        private void VoidItem()
        {
            if (this.selectedPurchaseData == null)
            {
                return;
            }
            
            // if an amount hasn't been selected from the touch pad, then remove it all, else remove the amount entered.
            var quantityToRemove = this.touchPadValue == 0
                ? this.selectedPurchaseData.Quantity
                : this.touchPadValue;

            #region validation

            if (quantityToRemove > this.selectedPurchaseData.Quantity)
            {
                NouvemMessageBox.Show(Message.RemoveTooManyItemsError, touchScreen:true);
                this.SetTouchPadValue("0");
                return;
            }

            #endregion

            // subtract the total of the item to remove from the running total.
            var amountToSubtract = this.selectedPurchaseData.PriceIncVat * quantityToRemove;
            this.Total -= amountToSubtract;

            var amountToSubtractExVat = this.selectedPurchaseData.Price * quantityToRemove;
            this.totalExVat -= amountToSubtractExVat;

            // decrement the quantity removed
            this.SelectedPurchaseData.Quantity -= quantityToRemove.ToInt();

            if (this.selectedPurchaseData.Quantity == 0)
            {
                // quantity is 0, so remove item.
                this.PurchaseData.Remove(this.selectedPurchaseData);
            }

            this.SetTouchPadValue("0");
        }

        /// <summary>
        /// Handles the touch pad selection when in product selection mode.
        /// </summary>
        /// <param name="selection">The touch pad selection.</param>
        private void TouchPadProductSelection(string selection)
        {
            switch (selection)
            {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case ".":
                    this.SetTouchPadValue(this.touchPadValueString += selection); 
                    break;

                case "10":
                    this.SetTouchPadValue("10");
                    break;

                case "20":
                    this.SetTouchPadValue("20");
                    break;

                case "50":
                    this.SetTouchPadValue("50");
                    break;

                case "CLR":
                    if (!this.passwordChangeMode)
                    {
                        this.SetTouchPadValue("0");
                    }
                    else
                    {
                        this.SetTouchPadValue(string.Empty);
                    }
                  
                    break;

                case "VoidItem":
                    this.VoidItem();
                    break;
            }
        }

        /// <summary>
        /// Gets the application price lists.
        /// </summary>
        private void GetPriceLists()
        {
            this.priceLists = new List<PriceMaster>(this.DataManager.GetPriceListsAndDetails());
        }

        /// <summary>
        /// Gets the products and groups.
        /// </summary>
        private void GetProductData()
        {
            this.allProducts = new ObservableCollection<ProductData>(this.DataManager.GetEposItems());
            var productGroups = this.DataManager.GetInventoryGroups().OrderBy(x => x.Name);
            var nonEmptyProductGroups = new List<INGroup>();

            // remove the product groups that don't contain any products.
            foreach (var productGroup in productGroups)
            {
                if (this.allProducts.Any(x => x.GroupId == productGroup.INGroupID))
                {
                    nonEmptyProductGroups.Add(productGroup);
                }
            }

            // If there's only 1 non empty product group, then it's pointless to display it..use it's ui space for the products instead.
            if (nonEmptyProductGroups.Count() > 1)
            {
                this.ProductGroups = new ObservableCollection<INGroup>(nonEmptyProductGroups);
            }
            else
            {
                this.Products = new ObservableCollection<ProductData>(this.allProducts);
            }
        }

        /// <summary>
        /// Gets the application transaction types. </summary>
        private void GetTransactionTypes()
        {
            this.transactionTypes = this.DataManager.GetTransactionTypes();
        }

        /// <summary>
        /// Sets the module transaction types.
        /// </summary>
        private void SetTransactionTypes()
        {
            this.eposTransactionType =
               this.transactionTypes.FirstOrDefault(
                   x => x.Description.CompareIgnoringCase(TransactionType.SaleEpos.ToString()));

            this.goodsReceiptTransactionType =
              this.transactionTypes.FirstOrDefault(
                  x => x.Description.CompareIgnoringCase(TransactionType.GoodsReceipt.ToString()));

            this.goodsReturnTransactionType =
             this.transactionTypes.FirstOrDefault(
                 x => x.Description.CompareIgnoringCase(TransactionType.GoodsReturn.ToString()));

            this.stockAdjustTransactionType =
            this.transactionTypes.FirstOrDefault(
                x => x.Description.CompareIgnoringCase(TransactionType.StockAdjust.ToString()));

            this.goodsReturnToHeadOfficeType =
          this.transactionTypes.FirstOrDefault(
              x => x.Description.CompareIgnoringCase(TransactionType.GoodsReturnToHeadOffice.ToString()));
        }

        /// <summary>
        /// Gets the factory warehouse locations.
        /// </summary>
        private void GetWarehouses()
        {
            this.warehouses = new List<Warehouse>(this.DataManager.GetWarehouses());
        }

        /// <summary>
        /// Gets the application delivery methods.
        /// </summary>
        private void GetDeliveryMethods()
        {
            this.deliveryMethods = this.DataManager.GetDeliveryMethods();
        }

        /// <summary>
        /// Gets the application delivery methods.
        /// </summary>
        private void GetPaymentTypes()
        {
            this.paymentTypes = this.DataManager.GetPaymentTypes();
        }

        /// <summary>
        /// Handles the group selection change, by populating the ui with the corresponding group products.
        /// </summary>
        private void HandleGroupChange()
        {
            if (this.selectedProductGroup == null)
            {
                return;
            }

            this.Products.Clear();
            foreach (var product in allProducts.Where(x => x.GroupId == this.selectedProductGroup.INGroupID))
            {
                this.Products.Add(product);
            }
        }

        /// <summary>
        /// Sets the touch pad value.
        /// </summary>
        private void SetTouchPadValue(string value)
        {
            this.TouchPadValueString = value;
        }

        /// <summary>
        /// Gets the system sales corresponding to the selected date.
        /// </summary>
        private void GetSystemSales()
        {
            this.EposSales = new ObservableCollection<EposSale>(this.DataManager.GetEposSales(this.selectedDate, this.invoiceNumber));
            this.SelectedEposSale = this.EposSales.FirstOrDefault();
        }

        /// <summary>
        /// Gets the unpaid account sales.
        /// </summary>
        private void GetUnpaidSales()
        {
            this.UnpaidEposSales = new ObservableCollection<EposSale>(this.DataManager.GetUnpaidEposSales());
            this.SelectedUnpaidEposSale = this.UnpaidEposSales.FirstOrDefault();
        }

        /// <summary>
        /// Plays the audio beep.
        /// </summary>
        private void Beep()
        {
            this.soundPlayer.Play();
        }

        /// <summary>
        /// Gets the current stock.
        /// </summary>
        private void GetStock()
        {
            this.Stock = new ObservableCollection<ProductData>(this.DataManager.GetEposStock());
            this.openingStock = this.Stock;
            if (this.Stock.Any())
            {
                this.SelectedStock = this.Stock.First();
            }
        }


        /// <summary>
        /// Sets the pricing to that of the selected partners price list.
        /// </summary>
        private void SetCustomerPricing()
        {
            this.Log.LogDebug(this.GetType(), string.Format("SetCustomerPricing - Getting price list for customer:{0}", this.selectedPartner.Details.BPMasterID));
            var priceListId = this.selectedPartner.Details.PriceListID;

            //var priceList = this.priceLists.FirstOrDefault(x => x.PriceListID == priceListId);
            if (priceListId.HasValue)
            {
                this.Total = 0;
                this.totalExVat = 0;
                this.ChangeToGive = "0";
                //var details = priceList.PriceListDetails;
                

                foreach (var sale in this.PurchaseData)
                {
                    //var partnerPriceData = details.FirstOrDefault(x => x.INMasterID == sale.Id);
                    var price = this.DataManager.GetPartnerPrice(priceListId.ToInt(), sale.Id);

                    if (price != null)
                    {
                        sale.Price = price.ToDecimal();
                        this.Total += (sale.PriceIncVat * sale.Quantity);
                        this.totalExVat += (sale.Price * sale.Quantity);
                        continue;
                    }

                    this.Log.LogDebug(this.GetType(), "No price list found. Using base price list");
                }

                this.TouchPadValue = this.Total;
            }
        }

        #endregion

        #endregion
    }
}

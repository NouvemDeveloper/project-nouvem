﻿// -----------------------------------------------------------------------
// <copyright file="IRibbonBarCommand.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Interface
{
    using Nouvem.Model.Enum;

    /// <summary>
    /// Interface for the ribbon bar control implementation classes.
    /// </summary>
    public interface IRibbonBarCommand
    {
        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        void MoveBack();

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        void MoveNext();

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        void MoveFirst();

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        void MoveLast();

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon navigation command sent.</param>
        void HandleRibbonNavigation(RibbonCommand command);

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        void ControlChange(string mode);
    }
}

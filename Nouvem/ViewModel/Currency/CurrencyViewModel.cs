﻿// -----------------------------------------------------------------------
// <copyright file="CurrencyViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Currency
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class CurrencyViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected currency.
        /// </summary>
        private BPCurrency selectedCurrency;

        /// <summary>
        /// The currencies collection.
        /// </summary>
        private ObservableCollection<BPCurrency> currencies;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the CurrencyViewModel class.
        /// </summary>
        public CurrencyViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.Currencies = new ObservableCollection<BPCurrency>();

            #endregion

            this.GetCountryMasters();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the currencies.
        /// </summary>
        public ObservableCollection<BPCurrency> Currencies
        {
            get
            {
                return this.currencies;
            }

            set
            {
                this.currencies = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the currencies.
        /// </summary>
        public BPCurrency SelectedCurrency
        {
            get
            {
                return this.selectedCurrency;
            }

            set
            {
                this.selectedCurrency = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the currencies.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedCurrency != null)
            {
                this.selectedCurrency.Deleted = true;
                this.UpdateCurrencies();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.Currencies.Remove(this.selectedCurrency);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateCurrencies();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the currencies.
        /// </summary>
        private void UpdateCurrencies()
        {
            try
            {
                if (this.DataManager.AddOrUpdateCurrencies(this.Currencies))
                {
                    SystemMessage.Write(MessageType.Priority, Message.CurrenciesUpdated);
                    Messenger.Default.Send(Token.Message, Token.UpdateCurrencies);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.CurrenciesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.CurrenciesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearCurrency();
            Messenger.Default.Send(Token.Message, Token.CloseCurrencyWindow);
        }

        /// <summary>
        /// Gets the application currencies.
        /// </summary>
        private void GetCountryMasters()
        {
            this.Currencies.Clear();
            foreach (var currency in this.DataManager.GetBusinessPartnerCurrencies())
            {
                this.Currencies.Add(currency);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}



﻿// -----------------------------------------------------------------------
// <copyright file="InventorySearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Grid;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Inventory
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Inventory search data
    /// </summary>
    public class InventorySearchDataViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected grid partner.
        /// </summary>
        private InventoryItem selectedSearchItem;

        /// <summary>
        /// The all data collection.
        /// </summary>
        private IList<InventoryItem> allItems = new List<InventoryItem>();

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<InventoryItem> filteredItems;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        private bool keepVisible;

        private IList<InventoryItem> productsToUpdate = new List<InventoryItem>();

        /// <summary>
        /// Show the inactve products flag.
        /// </summary>
        private bool showInactiveProducts;

        /// <summary>
        /// The sale/purchase module.
        /// </summary>
        private ViewType orderModule;

        /// <summary>
        /// Flag, as to whether we were called by an order module.
        /// </summary>
        private bool sendToOrderModule;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InventorySearchDataViewModel"/> class.
        /// </summary>
        public InventorySearchDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region

            #endregion

            #region command registration

            // Handle the ui item selection.
            this.ItemSelectedCommand = new RelayCommand(this.ItemSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            this.UpdateCommmand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                if (this.SelectedSearchItem != null)
                {
                    if (e.Cell.Property.Equals("Master.NouPriceMethodID"))
                    {
                        var localProduct = this.productsToUpdate.FirstOrDefault(x => x.Master.INMasterID == this.SelectedSearchItem.Master.INMasterID);
                        if (localProduct != null)
                        {
                            this.productsToUpdate.Remove(localProduct);
                        }

                        this.productsToUpdate.Add(this.SelectedSearchItem);
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            });

            #endregion

            #region instantiation

            this.FilteredItems = new ObservableCollection<InventoryItem>();

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetPriceMethods();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the price methods.
        /// </summary>
        public IList<NouPriceMethod> PriceMethods { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the inactive products are to be displayed
        /// </summary>
        public bool ShowInactiveProducts
        {
            get
            {
                return this.showInactiveProducts;
            }

            set
            {
                this.showInactiveProducts = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    var acticeItems =
                     NouvemGlobal.InventoryItems.Where(x => (x.Master.InActiveFrom != null && x.Master.InActiveTo != null)
                                         && (DateTime.Today >= x.Master.InActiveFrom && DateTime.Today <= x.Master.InActiveTo));
                    this.FilteredItems = new ObservableCollection<InventoryItem>(acticeItems);
                }
                else
                {
                    this.FilteredItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems.Where(x => (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                         || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)));
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected search item.
        /// </summary>
        public InventoryItem SelectedSearchItem
        {
            get
            {
                return this.selectedSearchItem;
            }

            set
            {
                if (value != null)
                {
                    this.selectedSearchItem = value;
                }

                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the filtered users.
        /// </summary>
        public ObservableCollection<InventoryItem> FilteredItems
        {
            get
            {
                return this.filteredItems;
            }

            set
            {
                this.filteredItems = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the item selection.
        /// </summary>
        public ICommand ItemSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the sale/purchase order module which called this search module.
        /// </summary>
        /// <param name="module">The order module.</param>
        public void SetModule(ViewType module)
        {
            this.orderModule = module;
            this.sendToOrderModule = true;
        }

        /// <summary>
        /// Method that sets the users to display.
        /// </summary>
        /// <param name="data">The users to display.</param>
        public void SetFilteredItems(IList<InventoryItem> data)
        {
            this.allItems = data;
            var filterData = data.Where(x => (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                            || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo));
            this.FilteredItems = new ObservableCollection<InventoryItem>(filterData);
        }

        #endregion

        #endregion

        #region protected override command execute

        /// <summary>
        /// Handler for the selection of a business partner.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateInventoryItem();
                    break;

                case ControlMode.OK:
                    this.ItemSelectedCommandExecute();
                    break;
            }
        }

        /// <summary>
        /// Handler for the cancelling of the item search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.CloseWindow();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommmand { get; private set; }

        /// <summary>
        /// Handles the selection of an item.
        /// </summary>
        private void ItemSelectedCommandExecute()
        {
            if (this.selectedSearchItem == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.SelectItem);
                return;
            }

            if (this.sendToOrderModule)
            {
                this.sendToOrderModule = false;
                Messenger.Default.Send(Tuple.Create(this.orderModule, this.selectedSearchItem), Token.ProductSelected);
            }
            else
            {
                if (!this.Locator.UserSearchData.IsFormLoaded)
                {
                    Messenger.Default.Send(ViewType.InventoryMaster);
                }

                if (this.Locator.InventoryMaster != null)
                {
                    this.Locator.InventoryMaster.SelectedInventoryItem = this.selectedSearchItem;
                }
            }

            Messenger.Default.Send(Token.Message, Token.SetTopMost);

            this.CloseWindow();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.KeepVisible = false;
            this.SetControlMode(ControlMode.OK);
            this.ShowInactiveProducts = false;
            if (this.FilteredItems.Any())
            {
                this.SelectedSearchItem = this.FilteredItems.First();
            }
        }

        private void UpdateInventoryItem()
        {
            if (!this.productsToUpdate.Any())
            {
                return;
            }

            if (this.DataManager.UpdateSearchItems(this.productsToUpdate))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                this.productsToUpdate.Clear();
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.CloseWindow();
        }

        #endregion

        #region helper

        /// <summary>
        /// Method which gets the price methods.
        /// </summary>
        private void GetPriceMethods()
        {
            this.PriceMethods = this.DataManager.GetPriceMethods();
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        private void CloseWindow()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseWindow);
                this.FilteredItems.Clear();
            }
        }

        #endregion

        #endregion
    }
}
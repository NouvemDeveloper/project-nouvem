﻿// -----------------------------------------------------------------------
// <copyright file="ItemMasterPropertySetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Inventory
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class ItemMasterPropertySetUpViewModel : NouvemViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the ItemMasterPropertySetUpViewModel  class.
        /// </summary>
        public ItemMasterPropertySetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.Properties = new ObservableCollection<INProperty>();

            #endregion

            this.GetProperties();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the properties.
        /// </summary>
        public ObservableCollection<INProperty> Properties { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the properties.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateProperties();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the properties group.
        /// </summary>
        private void UpdateProperties()
        {
            try
            {
                if (this.DataManager.AddOrUpdateProperties(this.Properties))
                {
                    SystemMessage.Write(MessageType.Priority, Message.PropertiesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PropertiesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.PropertiesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearItemMasterPropertySetUp();
            Messenger.Default.Send(Token.Message, Token.CloseItemMasterSetUpWindow);
        }

        /// <summary>
        /// Gets the application item master properties.
        /// </summary>
        private void GetProperties()
        {
            this.Properties.Clear();
            foreach (var property in this.DataManager.GetItemMasterProperties())
            {
                this.Properties.Add(property);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

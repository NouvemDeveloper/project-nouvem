﻿// -----------------------------------------------------------------------
// <copyright file="InventoryGroupViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;
using Nouvem.ViewModel.Interface;

namespace Nouvem.ViewModel.Inventory
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class InventoryGroupViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The traceability allocated reference collection.
        /// </summary>
        private ObservableCollection<AttributeAllocationData> traceabilityTemplateAllocations;

        /// <summary>
        /// All the traceability allocated reference collection.
        /// </summary>
        private IList<AttributeAllocationData> allTraceabilityTemplateAllocations = new List<AttributeAllocationData>();

        /// <summary>
        /// The selected inventory group reference.
        /// </summary>
        private INGroup inventoryGroup;

        /// <summary>
        /// The selected inventory parent group reference.
        /// </summary>
        private INGroup inventoryParentGroup;

        /// <summary>
        /// The inventory groups reference.
        /// </summary>
        private ObservableCollection<INGroup> inventoryGroups;

        /// <summary>
        /// The inventory parent groups reference.
        /// </summary>
        private ObservableCollection<INGroup> inventoryParentGroups;
 

        /// <summary>
        /// The selected traceability template name.
        /// </summary>
        private AttributeTemplate selectedTraceabilityTemplateName;
       
        /// <summary>
        /// The use parent date group flag.
        /// </summary>
        private bool useParentGroup;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the InventoryGroupViewModel class.
        /// </summary>
        public InventoryGroupViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for incoming group.
            Messenger.Default.Register<int>(this, Token.DisplayInventoryGroup, groupId => this.InventoryGroup = this.InventoryGroups.FirstOrDefault(x => x.INGroupID == groupId));

            #endregion

            #region command registration

            // Specify the products for the list.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Specify the products for the list.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);
            
            #endregion

            #region instantiation
      
            this.InventoryGroups = new ObservableCollection<INGroup>();
            this.InventoryParentGroups = new ObservableCollection<INGroup>();
            this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplate>();
            this.TraceabilityTemplateAllocations = new ObservableCollection<AttributeAllocationData>();

            #endregion
         
            this.GetInventoryGroups();
            this.GetTraceabilityTemplateNames();
            this.GetTraceabilityTemplateAllocations();

        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Get or sets a value indicating whether the parent group dates template is to be applied.
        /// </summary>
        public bool UseParentGroup
        {
            get
            {
                return this.useParentGroup;
            }

            set
            {
                this.useParentGroup = value;
                this.RaisePropertyChanged();

                if (this.EntitySelectionChange)
                {
                    return;
                }

                if (this.IsFormLoaded)
                {
                    this.SetControlMode(ControlMode.Update);
                }

                if (value)
                {
                    if (this.InventoryGroup == null || this.InventoryGroup.Name.Equals(Strings.DefineNew))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoInventoryGroupSelected);
                        this.UseParentGroup = false;
                        return;
                    }

                    if (this.InventoryGroup.ParentInGroupID.IsNullOrZero())
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoParentGroup);
                        this.UseParentGroup = false;
                        return;
                    }

                    this.DisplayParentTemplates();
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.ParentGroupTemplateDisplayed, this.InventoryParentGroup.Name, this.InventoryGroup.Name));
                    this.SetControlMode(ControlMode.Update);
                }
                else
                {
                    // revert the ui templates display to the selected group templates
                    this.DisplayGroupTemplates();
                }
            }
        }

        /// <summary>
        /// Get or sets the inventory groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryGroups
        {
            get
            {
                return this.inventoryGroups;
            }

            set
            {
                this.inventoryGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the inventory parent groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryParentGroups
        {
            get
            {
                return this.inventoryParentGroups;
            }

            set
            {
                this.inventoryParentGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the traceability template allocations.
        /// </summary>
        public ObservableCollection<AttributeAllocationData> TraceabilityTemplateAllocations
        {
            get
            {
                return this.traceabilityTemplateAllocations;
            }

            set
            {
                this.traceabilityTemplateAllocations = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the traceability template names.
        /// </summary>
        public ObservableCollection<AttributeTemplate> TraceabilityTemplateNames { get; set; }


        /// <summary>
        /// Get or sets the selected traceability template name.
        /// </summary>
        public AttributeTemplate SelectedTraceabilityTemplateName
        {
            get
            {
                return this.selectedTraceabilityTemplateName;
            }

            set
            {
                this.selectedTraceabilityTemplateName = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.SetTraceabilityAllocations();

                    if (this.IsFormLoaded)
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }
       
        /// <summary>
        /// Get or sets the selected inventory group.
        /// </summary>
        public INGroup InventoryGroup
        {
            get
            {
                return this.inventoryGroup;
            }

            set
            {
                this.inventoryGroup = value;
                this.RaisePropertyChanged();

                if (value == null)
                {
                    return;
                }

                if (value.Name.Equals(Strings.DefineNew))
                {
                    this.AddNewGroup();
                    return;
                }

                this.DisplayGroupTemplates();

                if (!value.ParentInGroupID.IsNullOrZero())
                {
                    this.InventoryParentGroup =
                        this.InventoryGroups.FirstOrDefault(x => x.INGroupID == value.ParentInGroupID);
                }

                if (this.InventoryGroupAndParentMatch())
                {
                    SystemMessage.Write(MessageType.Issue, Message.InParentAndGroupMatchError);
                }
            }
        }

        /// <summary>
        /// Get or sets the selected inventory parent group.
        /// </summary>
        public INGroup InventoryParentGroup
        {
            get
            {
                return this.inventoryParentGroup;
            }

            set
            {
                this.inventoryParentGroup = value;
                this.RaisePropertyChanged();

                if (value == null)
                {
                    return;
                }

                if (this.InventoryGroupAndParentMatch())
                {
                    SystemMessage.Write(MessageType.Issue, Message.InParentAndGroupMatchError);
                    return;
                }

                if (this.IsFormLoaded)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the ui load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unload.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.InventoryGroup == null)
            {
                this.InventoryGroup = this.InventoryGroups.LastOrDefault();
                return;
            }

            var previousItem =
                this.InventoryGroups.TakeWhile(
                    item => item.INGroupID != this.InventoryGroup.INGroupID)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.InventoryGroup = previousItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.InventoryGroup == null)
            {
                this.InventoryGroup = this.InventoryGroups.FirstOrDefault();
                return;
            }

            var nextItem =
                this.InventoryGroups.SkipWhile(
                    item => item.INGroupID != this.InventoryGroup.INGroupID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.InventoryGroup = nextItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.InventoryGroup == null)
            {
                this.InventoryGroup = this.InventoryGroups.FirstOrDefault();
                return;
            }

            var firstItem = this.InventoryGroups.FirstOrDefault();

            if (firstItem != null)
            {
                this.InventoryGroup = firstItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.InventoryGroup == null)
            {
                this.InventoryGroup = this.InventoryGroups.LastOrDefault();
                return;
            }

            var lastItem = this.InventoryGroups.LastOrDefault();

            if (lastItem != null)
            {
                this.InventoryGroup = lastItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            this.InventoryGroup = this.InventoryGroups.OrderByDescending(x => x.EditDate).FirstOrDefault();
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        #endregion

        #region internal

        /// <summary>
        /// Updates the inventory groups. (Called when a new group(s) is dynamically added)
        /// </summary>
        internal void UpdateInventoryGroups()
        {
            this.GetInventoryGroups();

            if (this.InventoryGroups.Count >= 2)
            {
                // Select the most receently created group (the group before define new)
                this.InventoryGroup = this.inventoryGroups.ElementAt(this.inventoryGroups.Count - 2);
            }
        }

        #endregion

        #region protected override

        /// <summary>
        /// Drill down to the group set up.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryGroupSetUp,0), Token.DrillDown);
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find || this.CurrentMode == ControlMode.Add)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateInventoryGroup();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution
     
        /// <summary>
        /// handles the ui load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// handles the ui unload event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.IsFormLoaded = false;
            this.Close();
        }

        #endregion

        #region helper

        /// <summary>
        /// Clear the ui.
        /// </summary>
        private void ClearForm()
        {
            this.InventoryGroup = null;
            this.InventoryParentGroup = null;
            this.TraceabilityTemplateAllocations.Clear();
            this.SelectedTraceabilityTemplateName = null;
            this.EntitySelectionChange = true;
            this.UseParentGroup = false;
            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Makes a call to open the set up screen.
        /// </summary>
        private void AddNewGroup()
        {
            Messenger.Default.Send(ViewType.InventoryGroupSetUp);
        }

        /// <summary>
        /// Gets the traceability template names.
        /// </summary>
        private void GetTraceabilityTemplateNames()
        {
            this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplate>(this.DataManager.GetAttributeTemplateNames());
            this.TraceabilityTemplateNames.Insert(0, new AttributeTemplate { Name = Strings.NoneSelected });
        }

        /// <summary>
        /// Gets the traceability template allocations.
        /// </summary>
        private void GetTraceabilityTemplateAllocations()
        {
            this.allTraceabilityTemplateAllocations = NouvemGlobal.AttributeAllocationData;
        }

        /// <summary>
        /// Gets the application inventory groups.
        /// </summary>
        private void GetInventoryGroups()
        {
            this.InventoryGroups.Clear();
            this.InventoryParentGroups.Clear();
            this.InventoryParentGroups.Add(new INGroup { Name = Strings.NoParent });
            foreach (var group in this.DataManager.GetInventoryGroups())
            {
                if (group.ParentInGroupID.IsNullOrZero())
                {
                    this.InventoryParentGroups.Add(group);
                }

                this.InventoryGroups.Add(group);
            }

            this.InventoryGroups.Add(new INGroup { Name = Strings.DefineNew });
        }

        /// <summary>
        /// Update the current traceability template allocation to corrrespond with the selected traceability template name.
        /// </summary>
        private void SetTraceabilityAllocations()
        {
            this.TraceabilityTemplateAllocations.Clear();
            foreach (var template in this.allTraceabilityTemplateAllocations.Where(x => x.AttributeTemplate != null && x.AttributeTemplate.AttributeTemplateID == this.selectedTraceabilityTemplateName.AttributeTemplateID))
            {
                this.TraceabilityTemplateAllocations.Add(template);
            }
        }

        /// <summary>
        /// Handles the inventory group change, by selected it's associated templates.
        /// </summary>
        private void DisplayGroupTemplates()
        {
            // traceability
            if (this.InventoryGroup.AttributeTemplateID.IsNullOrZero())
            {
                // no associated traceability template, so clear.
                this.SelectedTraceabilityTemplateName = null;
                this.TraceabilityTemplateAllocations.Clear();
            }
            else
            {
                // apply the traceability template data
                this.SelectedTraceabilityTemplateName
                    = this.TraceabilityTemplateNames.FirstOrDefault(x => x.AttributeTemplateID == this.InventoryGroup.AttributeTemplateID);
            }
          
            // Set the parent group (if applicable)
            var parent =
                this.InventoryParentGroups.FirstOrDefault(x => x.INGroupID == this.InventoryGroup.ParentInGroupID);

            if (parent != null)
            {
                this.InventoryParentGroup = parent;
            }
            else
            {
                this.InventoryParentGroup = this.InventoryParentGroups.FirstOrDefault(x => x.Name.Equals(Strings.NoParent));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Updates the selected inventory group.
        /// </summary>
        private void UpdateInventoryGroup()
        {
            #region validation

            if (this.InventoryGroupAndParentMatch())
            {
                SystemMessage.Write(MessageType.Issue, Message.InParentAndGroupMatchError);
                return;
            }

            #endregion

            var result = this.DataManager.UpdateInventoryGroup(
                this.InventoryGroup,
                this.InventoryParentGroup,
                this.SelectedTraceabilityTemplateName);
            
            if (result != null)
            {
                SystemMessage.Write(MessageType.Priority, Message.INGroupUpdated);
                this.Refresh();
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.INGroupNotUpdated);
            }
          
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Changes the display templates to that of the inventory groups parent group templates.
        /// </summary>
        private void DisplayParentTemplates()
        {
            // traceability
            if (this.InventoryParentGroup.TraceabilityTemplateNameID.IsNullOrZero())
            {
                // no associated traceability template, so clear.
                this.SelectedTraceabilityTemplateName = null;
                this.TraceabilityTemplateAllocations.Clear();
            }
            else
            {
                // apply the traceability template data
                this.SelectedTraceabilityTemplateName
                    = this.TraceabilityTemplateNames.FirstOrDefault(x => x.AttributeTemplateID == this.InventoryParentGroup.TraceabilityTemplateNameID);
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearInventoryGroup();
            Messenger.Default.Send(Token.Message, Token.CloseInventoryGroupMasterWindow);
        }

        /// <summary>
        /// Refreshes the updated inventory template..
        /// </summary>
        private void Refresh()
        {
            this.GetInventoryGroups();
        }

        /// <summary>
        /// Determines if the selected inventory group is also the selected inventory parent group.
        /// </summary>
        /// <returns>Flag which determines if the selected inventory group is also the selected inventory parent group.</returns>
        private bool InventoryGroupAndParentMatch()
        {
            if (this.inventoryGroup == null || this.inventoryParentGroup == null)
            {
                return false;
            }

            if (this.inventoryGroup.INGroupID == this.inventoryParentGroup.INGroupID)
            {
                this.SetControlMode(ControlMode.OK);
                return true;
            }

            return false;
        }

        #endregion

        #endregion
    }
}

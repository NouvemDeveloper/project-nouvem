﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Inventory
{
    public class SpecsSearchViewModel : SalesSearchDataViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecsSearchViewModel"/> class.
        /// </summary>
        public SpecsSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.GetSpecs();
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.selectedSale != null)
            {
                Messenger.Default.Send(this.SelectedSale, Token.SearchSpecSelected);
            }

            this.Close();
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            this.ReportMode = false;
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseSearchSpecWindow);
                this.FilteredSales.Clear();
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the specs.
        /// </summary>
        private void GetSpecs()
        {
            this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetsSearchProductSpecifications().OrderBy(x => x.Product));
        }

        #endregion
    }
}

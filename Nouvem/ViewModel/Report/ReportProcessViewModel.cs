﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Module = Nouvem.Model.BusinessObject.Module;

namespace Nouvem.ViewModel.Report
{
    public class ReportProcessViewModel : ReportViewModelBase
    {
        #region field

        /// <summary>
        /// The modules.
        /// </summary>
        private ObservableCollection<Module> modules = new ObservableCollection<Module>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportProcessViewModel"/> class.
        /// </summary>
        public ReportProcessViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.UpdateModules, s => this.SetControlMode(ControlMode.Update));

            #endregion

            #region command handler

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetModules();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the modules.
        /// </summary>
        public ObservableCollection<Module> Modules
        {
            get
            {
                return this.modules;
            }

            set
            {
                this.modules = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the module reports.
        /// </summary>
        public IList<Module> ModuleReports { get; set; } = new List<Module>();

        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Sets the reports.
        /// </summary>
        protected override void GetReports()
        {
            base.GetReports();
            foreach (var reportData in this.Reports.Where(x => !x.Name.StartsWithIgnoringCase(Constant.SubReport)))
            {
                this.ModuleReports.Add(new Module { NouModuleID = reportData.ReportID, ReportID = reportData.ReportID, Name = reportData.Name });
            }
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Override the cancellation to close locally.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.Close();
                    break;

                case ControlMode.Update:
                    this.UpdateReportProcesses();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseReportProcessWindow);
            Messenger.Default.Unregister(this);
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the processes.
        /// </summary>
        private void UpdateReportProcesses()
        {
            var modulesToUpdate = new List<Module>();
            foreach (var module in this.modules)
            {
                foreach (var reportModule in module.Modules)
                {
                    if (reportModule.UseAtModule)
                    {
                        modulesToUpdate.Add(new Module
                        {
                            NouModuleID = module.NouModuleID,
                            ReportID = reportModule.ReportID,
                            UseAtModule = true
                        });
                    }

                    foreach (var partnerModule in reportModule.Modules)
                    {
                        foreach (var contactModule in partnerModule.Modules)
                        {
                            if (contactModule.ShowEmail)
                            {
                                modulesToUpdate.Add(new Module
                                {
                                    NouModuleID = module.NouModuleID,
                                    ReportID = reportModule.ReportID,
                                    ContactID = contactModule.ContactID,
                                    ShowEmail = contactModule.ShowEmail
                                });
                            }
                        }
                    }
                }
            }

            if (this.DataManager.UpdateReportProcesses(modulesToUpdate))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                this.Refresh();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Refresh the ui data.
        /// </summary>
        private void Refresh()
        {
            this.GetModules();
        }

        /// <summary>
        /// Gets the application modules.
        /// </summary>
        private void GetModules()
        {
            this.Modules.Clear();
            var lookUps = this.DataManager.GetReportEmailLookups();
            var contacts = this.DataManager.GetBusinessPartnerContacts();
            var partners = new List<Model.BusinessObject.BusinessPartner>();  //NouvemGlobal.CustomerPartners.OrderBy(x => x.Details.Name).ToList();
            var localModules = this.DataManager.GetModules().Where(x => x.ReportProcess.IsTrue()).ToList();
            //var progressCount = localModules.Count * partners.Count;
            //if (progressCount > 0)
            //{
            //    ProgressBar.SetUp(0, progressCount - 1);
            //    ProgressBar.Run();
            //}

            try
            {
                foreach (var localModule in localModules)
                {
                    var moduleSelected = false;
                    var reportModules = new List<Module>();
                    foreach (var moduleReport in this.ModuleReports)
                    {
                        var reportSelected = lookUps.Any(x => x.NouModuleID == localModule.NouModuleID && x.ReportID == moduleReport.ReportID);
                        var useForModule = lookUps.Any(x => x.NouModuleID == localModule.NouModuleID && x.ReportID == moduleReport.ReportID && x.UseAtModule.IsTrue());
                        if (reportSelected)
                        {
                            moduleSelected = true;
                        }

                        var partnerModules = new List<Module>();
                        foreach (var businessPartner in partners)
                        {
                            //ProgressBar.Run();
                            var contactModules = new List<Module>();
                            var partnerContacts = contacts.Where(x =>
                                x.Details.BPMasterID == businessPartner.Details.BPMasterID);
                            var partnerSelected = false;
                            foreach (var businessPartnerContact in partnerContacts)
                            {
                                var isSelected = false;
                                var showEmail = false;
                                var localLookUp = lookUps.FirstOrDefault(x =>
                                    x.BPContactID == businessPartnerContact.Details.BPContactID &&
                                    x.ReportID == moduleReport.ReportID && x.NouModuleID == localModule.NouModuleID);

                                if (localLookUp != null)
                                {
                                    isSelected = true;
                                    showEmail = localLookUp.ShowEmail.ToBool();
                                    partnerSelected = true;
                                }

                                contactModules.Add(new Module
                                {
                                    NouModuleID = businessPartnerContact.Details.BPContactID,
                                    ContactID = businessPartnerContact.Details.BPContactID,
                                    Name = businessPartnerContact.FullName,
                                    Email = businessPartnerContact.Details.Email,
                                    IsSelected = isSelected,
                                    ShowEmail = showEmail
                                });
                            }

                            partnerModules.Add(new Module { NouModuleID = businessPartner.Details.BPMasterID, Name = businessPartner.Details.Name, IsSelected = partnerSelected, Modules = contactModules });
                        }

                        reportModules.Add(new Module { NouModuleID = moduleReport.NouModuleID, ReportID = moduleReport.NouModuleID, IsSelected = reportSelected, Name = moduleReport.Name, Modules = partnerModules, UseAtModule = useForModule });
                    }

                    this.Modules.Add(new Module { NouModuleID = localModule.NouModuleID, Name = localModule.Name, IsSelected = moduleSelected, Modules = reportModules });
                }
            }
            finally
            {
                //ProgressBar.Reset();
            }
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ReportProductDataViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Report
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Shared;
    using Nouvem.ViewModel.Sales;

    public class ReportProductDataViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// All the report sales.
        /// </summary>
        private ObservableCollection<Sale> allSales = new ObservableCollection<Sale>();

        /// <summary>
        /// All the product data.
        /// </summary>
        private ObservableCollection<App_ReportProductData_Result> productData = new ObservableCollection<App_ReportProductData_Result>();

        /// <summary>
        /// The selected customer.
        /// </summary>
        private BusinessPartner selectedCustomer;

        /// <summary>
        /// The customers.
        /// </summary>
        private ObservableCollection<BusinessPartner> customers;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportProductDataViewModel"/> class.
        /// </summary>
        public ReportProductDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            #endregion

            #region command handler

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public ObservableCollection<BusinessPartner> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.customers = value;
                this.RaisePropertyChanged();
            }
        }

        public BusinessPartner SelectedCustomer 
        {
            get
            {
                return this.selectedCustomer;
            }

            set
            {
                if (value != null)
                {
                    this.selectedCustomer = value;
                    this.RaisePropertyChanged();
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ObservableCollection<App_ReportProductData_Result> ProductData
        {
            get
            {
                return this.productData;
            }

            set
            {
                this.productData = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Override, to show the selected document.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            ApplicationSettings.ReportsSearchFromDate = this.FromDate;
            ApplicationSettings.ReportsSearchToDate = this.ToDate;
            ApplicationSettings.ReportSalesSearchDataKeepVisible = this.KeepVisible;
            ApplicationSettings.ReportSalesSearchShowAllOrders = this.ShowAllOrders;

            if (this.SelectedSales.Count > 1)
            {
                if (this.ReportMode)
                {
                    Messenger.Default.Send(this.SelectedSales.ToList(), Token.SearchSaleSelectedForReport);
                }
            }
            else
            {
                this.SaleSelectedCommandExecute();
            }
        }

        /// <summary>
        /// print the report.
        /// </summary>
        /// <param name="mode">The report mode selected.</param>
        protected override void PrintReport(string mode)
        {
            // we use some random sale fields to hold our report parameters.
            var sale = new Sale
            {
                SaleID = this.selectedCustomer == null || this.selectedCustomer.Details.BPMasterID == 0 
                         ? 0 : this.selectedCustomer.Details.BPMasterID,
                DeliveryDate = this.FromDate,
                CreationDate = this.ToDate
            };

            // send to the master vm for processing.
            Messenger.Default.Send(sale, Token.SearchSaleSelectedForReport);
        }

        protected override void Close()
        {
            base.Close();
            this.ReportMode = true;
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            base.OnLoadingCommandExecute();
            this.ReportMode = true;
            //this.FromDate = DateTime.Today;
            //this.ToDate = DateTime.Today;
            this.KeepVisible = ApplicationSettings.ReportSalesSearchDataKeepVisible;
            this.Customers = new ObservableCollection<BusinessPartner>(NouvemGlobal.CustomerPartners);
            this.Customers.Insert(0, new BusinessPartner { Details = new ViewBusinessPartner{ Name = Strings.AllCustomers }});
            this.SelectedCustomer = this.Customers.First();
            this.GetData();
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            ApplicationSettings.ReportsSearchFromDate = this.FromDate;
            ApplicationSettings.ReportsSearchToDate = this.ToDate;
            ApplicationSettings.ReportSalesSearchDataKeepVisible = this.KeepVisible;
            ApplicationSettings.ReportSalesSearchShowAllOrders = this.ShowAllOrders;

            this.keepVisible = false;
            this.Close();
        }

        /// <summary>
        /// Handle the incoming sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        protected override void SetIncomingSales(IList<Sale> sales)
        {
        }

        /// <summary>
        /// Refresh the sales.
        /// </summary>
        protected override void Refresh()
        {
            this.GetData();
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the product data.
        /// </summary>
        private void GetData()
        {
            var customerId = this.selectedCustomer == null ||
                             this.selectedCustomer.Details.Name.Equals(Strings.AllCustomers)
                ? 0
                : this.selectedCustomer.Details.BPMasterID;

            IList<App_ReportProductData_Result> data;
            //if (this.ShowAllOrders)
            //{
            //    data = this.DataManager.GetProductData(customerId, "01/01/2000".ToDate(), DateTime.Today.AddDays(1000));
            //}
            //else
            //{
            //    data = this.DataManager.GetProductData(customerId, this.FromDate, this.ToDate);
            //}

            data = this.DataManager.GetProductData(customerId, this.FromDate, this.ToDate);

            this.ProductData = new ObservableCollection<App_ReportProductData_Result>(data);
        }

        #endregion

    }
}

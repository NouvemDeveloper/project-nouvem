﻿// -----------------------------------------------------------------------
// <copyright file="ReportViewModelBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Report
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public abstract class ReportViewModelBase : NouvemViewModelBase
    {
        #region field

        private List<DataColumn> stringDataColumns = new List<DataColumn>();
        private List<DataColumn> intDataColumns = new List<DataColumn>();
        private List<DataColumn> decimalDataColumns = new List<DataColumn>();
        private List<DataColumn> dateDataColumns = new List<DataColumn>();

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime? fromDate;

        /// <summary>
        /// The search to date.
        /// </summary>
        private DateTime? toDate;

        /// <summary>
        /// The current report mode.
        /// </summary>
        private string holdingMode;

        /// <summary>
        /// The selected search data row.
        /// </summary>
        private CollectionData selectedSearchGridRow;

        /// <summary>
        /// The macro columns.
        /// </summary>
        private ObservableCollection<string> macroColumns = new ObservableCollection<string>();

        /// <summary>
        /// The application report data.
        /// </summary>
        private ObservableCollection<ReportData> reports = new ObservableCollection<ReportData>();

        /// <summary>
        /// The application report data.
        /// </summary>
        private ObservableCollection<CollectionData> searchGridData = new ObservableCollection<CollectionData>();

        /// <summary>
        /// The show all data flag.
        /// </summary>
        private bool displayShowAll;

        /// <summary>
        /// The show all data flag.
        /// </summary>
        private bool showCreateFile;

        /// <summary>
        /// The show keep screen open flag.
        /// </summary>
        private bool keepVisible;

        /// <summary>
        /// The show all data flag.
        /// </summary>
        private bool showAll;

        /// <summary>
        /// The show all data flag.
        /// </summary>
        private string searchText;

        /// <summary>
        /// The keep visible flag.
        /// </summary>
        private bool displayKeepVisible;

        /// <summary>
        /// The multi select data flag.
        /// </summary>
        private bool multiSelect;

        /// <summary>
        /// The multi select data flag.
        /// </summary>
        private bool viewMultiple;

        /// <summary>
        /// The show dates flag.
        /// </summary>
        private bool displayDates;

        /// <summary>
        /// The show dates flag.
        /// </summary>
        private bool displayDateTypes;

        /// <summary>
        /// The search date types.
        /// </summary>
        private ObservableCollection<SearchDateType> dateTypes = new ObservableCollection<SearchDateType>();

        /// <summary>
        /// The search date types.
        /// </summary>
        private ObservableCollection<SearchDateType> reportDateTypes = new ObservableCollection<SearchDateType>();

        /// <summary>
        /// The search date types.
        /// </summary>
        private SearchDateType selectedDateType;

        /// <summary>
        /// The show dates flag.
        /// </summary>
        private bool displaySearchId;

        /// <summary>
        /// The show refresh flag.
        /// </summary>
        private bool displayRefresh;

        /// <summary>
        /// The selected report macro.
        /// </summary>
        private string selectedReportMacro;

        /// <summary>
        /// The selected search screen macro.
        /// </summary>
        private string selectedSearchScreenMacro;

        /// <summary>
        /// The selected report data.
        /// </summary>
        private ReportData selectedReport;

        /// <summary>
        /// In process flag.
        /// </summary>
        private bool processing;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportViewModelBase"/> class.
        /// </summary>
        public ReportViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<List<CollectionData>>(this, Token.SendingReportRows, o =>
            {
                if (o.Count > 0)
                {
                    this.SelectedSearchGridRows = o;
                    this.PrintReport(this.holdingMode);
                }
            });

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            #endregion

            #region command handler

            this.CreateFileCommand = new RelayCommand(this.CreateFileCommandExecute);
            this.UpdateCommand = new RelayCommand(this.UpdateCommandExecute);
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);
            this.RefreshCommand = new RelayCommand(this.RefreshCommandExecute);
            this.RowSelectedCommand = new RelayCommand(this.RowSelectedCommandExecute);

            #endregion

            this.GetReports();
            this.HasWriteAuthorisation = true;
            this.GetDateTypes();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public SearchDateType SelectedDateType
        {
            get
            {
                return this.selectedDateType;
            }

            set
            {
                this.selectedDateType = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded && this.DisplayDateTypes)
                {
                    this.RefreshData();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public ObservableCollection<SearchDateType> DateTypes
        {
            get
            {
                return this.dateTypes;
            }

            set
            {
                this.dateTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public ObservableCollection<SearchDateType> ReportDateTypes
        {
            get
            {
                return this.reportDateTypes;
            }

            set
            {
                this.reportDateTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime? FromDate
        {
            get
            {
                return this.fromDate;
            }

            set
            {
                this.fromDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.RefreshData();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search to date.
        /// </summary>
        public DateTime? ToDate
        {
            get
            {
                return this.toDate;
            }

            set
            {
                this.toDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.RefreshData();
                }
            }
        }

        /// <summary>
        /// Gets or sets the report data.
        /// </summary>
        public ReportData SelectedReport
        {
            get
            {
                return this.selectedReport;
            }

            set
            {
                this.selectedReport = value;
                this.RaisePropertyChanged();
                this.HandleSelectedReport();
            }
        }

        /// <summary>
        /// Gets or sets the report data.
        /// </summary>
        public CollectionData SelectedSearchGridRow
        {
            get
            {
                return this.selectedSearchGridRow;
            }

            set
            {
                this.selectedSearchGridRow = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the report data.
        /// </summary>
        public IList<CollectionData> SelectedSearchGridRows { get; set; } = new List<CollectionData>();
        
        /// <summary>
        /// Gets or sets the macro column data.
        /// </summary>
        public ObservableCollection<string> MacroColumns
        {
            get
            {
                return this.macroColumns;
            }

            set
            {
                this.macroColumns = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application report data.
        /// </summary>
        public ObservableCollection<CollectionData> SearchGridData
        {
            get
            {
                return this.searchGridData;
            }

            set
            {
                this.searchGridData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application report data.
        /// </summary>
        public ObservableCollection<ReportData> Reports
        {
            get
            {
                return this.reports;
            }

            set
            {
                this.reports = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute code.
        /// </summary>
        public string SelectedReportMacro
        {
            get
            {
                return this.selectedReportMacro;
            }

            set
            {
                this.SetMode(value, this.selectedReportMacro);
                this.selectedReportMacro = value;
                this.RaisePropertyChanged();
                this.HandleSelectedReportMacro();
            }
        }

        /// <summary>
        /// Gets or sets the search screen macro.
        /// </summary>
        public string SelectedSearchScreenMacro
        {
            get
            {
                return this.selectedSearchScreenMacro;
            }

            set
            {
                this.SetMode(value, this.selectedSearchScreenMacro);
                this.selectedSearchScreenMacro = value;
                this.RaisePropertyChanged();
                this.HandleSelectedSearchScreenMacro();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether show all is selected/displayed.
        /// </summary>
        public bool DisplayShowAll
        {
            get
            {
                return this.displayShowAll;
            }

            set
            {
                this.SetMode(value, this.displayShowAll);
                this.displayShowAll = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether show all is selected/displayed.
        /// </summary>
        public bool ShowCreateFile
        {
            get
            {
                return this.showCreateFile;
            }

            set
            {
                this.SetMode(value, this.showCreateFile);
                this.showCreateFile = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether show all is selected/displayed.
        /// </summary>
        public bool ShowAll
        {
            get
            {
                return this.showAll;
            }

            set
            {
                this.showAll = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.RefreshData();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether show all is selected/displayed.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool DisplayDates
        {
            get
            {
                return this.displayDates;
            }

            set
            {
                this.SetMode(value, this.displayDates);
                this.displayDates = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool DisplayDateTypes
        {
            get
            {
                return this.displayDateTypes;
            }

            set
            {
                this.SetMode(value, this.displayDateTypes);
                this.displayDateTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                this.searchText = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.RefreshData();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool DisplaySearchID
        {
            get
            {
                return this.displaySearchId;
            }

            set
            {
                this.SetMode(value, this.displaySearchId);
                this.displaySearchId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether multi select is used.
        /// </summary>
        public bool MultiSelect
        {
            get
            {
                return this.multiSelect;
            }

            set
            {
                this.SetMode(value, this.multiSelect);
                this.multiSelect = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.ViewMultiple = false;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(value || this.ViewMultiple, Token.SearchMultiSelect);
                }));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether multi select is used.
        /// </summary>
        public bool ViewMultiple
        {
            get
            {
                return this.viewMultiple;
            }

            set
            {
                this.SetMode(value, this.viewMultiple);
                this.viewMultiple = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.MultiSelect = false;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(value || this.MultiSelect, Token.SearchMultiSelect);
                }));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether keep visible is selected/displayed.
        /// </summary>
        public bool DisplayKeepVisible
        {
            get
            {
                return this.displayKeepVisible;
            }

            set
            {
                this.SetMode(value, this.displayKeepVisible);
                this.displayKeepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether refresh is selected/displayed.
        /// </summary>
        public bool DisplayRefresh
        {
            get
            {
                return this.displayRefresh;
            }

            set
            {
                this.SetMode(value, this.displayRefresh);
                this.displayRefresh = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the update command.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to refresh the ui.
        /// </summary>
        public ICommand RowSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to refresh the ui.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        /// <summary>
        /// Gets the command to refresh the ui.
        /// </summary>
        public ICommand CreateFileCommand { get; private set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand OnLoadingCommand{ get; private set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        #region virtual

        protected virtual void UpdateCommandExecute() {}
        protected virtual void OnLoadingCommandExecute() { }
        protected virtual void OnClosingCommandExecute() { }
        protected virtual void HandleSelectedReport() { }
        protected virtual void HandleSelectedSearchScreenMacro() { }
        protected virtual void HandleSelectedReportMacro() { }
        protected virtual void Close() { }

        #endregion

        #region override

        /// <summary>
        /// Prints/preview used for multi selection row data.
        /// </summary>
        /// <param name="mode">The print\[preview mode selected.</param>
        protected override void PrintReport(string mode)
        {
            if (this.processing)
            {
                return;
            }

            if (this.SelectedSearchGridRows == null || this.SelectedSearchGridRows.Count == 0)
            {
                this.holdingMode = mode;
                Messenger.Default.Send(Token.Message, Token.RequestReportRows);
                return;
            }

            this.processing = true;
            try
            {
                var print = mode.Equals(Constant.Print);
                var email = mode.Equals(Strings.Email);
                if (this.selectedReport.MultiSelect)
                {
                    this.ReportManager.ShowReport(this.SelectedSearchGridRows, this.selectedReport, print, email);
                }
                else if (this.selectedReport.ViewMultiple)
                {
                    this.ReportManager.ShowReports(this.SelectedSearchGridRows, this.selectedReport, print, email);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                this.processing = false;
            }
        }

        protected override void CancelSelectionCommandExecute(){}
        protected override void ControlSelectionCommandExecute(){}

        #endregion

        #region method

        /// <summary>
        /// Save the user date type.
        /// </summary>
        protected void SaveReportDateType()
        {
            if (this.DisplayDateTypes && this.SelectedReport != null && this.SelectedReport.ReportID > 0 && this.SelectedDateType != null)
            {
                if (this.DataManager.SaveReportDateType(this.SelectedReport.ReportID,
                    this.SelectedDateType.NouSearchDateTypeID, NouvemGlobal.UserId.ToInt()))
                {
                    var localUserDateType = new ReportUserDateType { ReportID = this.SelectedReport.ReportID, ReportDateTypeID = this.SelectedDateType.NouSearchDateTypeID, UserID = NouvemGlobal.UserId.ToInt() };
                    this.SelectedReport.ReportUserDateType = localUserDateType;
                    var localReport =
                        this.ReportManager.Reports.FirstOrDefault(x => x.ReportID == this.SelectedReport.ReportID);
                    if (localReport != null)
                    {
                        localReport.ReportUserDateType = localUserDateType;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the search data for the selected report and populates the grid.
        /// </summary>
        protected void PopulateSearchGrid()
        {
            this.SearchGridData.Clear();
            this.MacroColumns.Clear();
            if (string.IsNullOrEmpty(this.SelectedSearchScreenMacro))
            {
                Messenger.Default.Send(new CollectionData(), Token.Macro);
                return;
            }

            var dt = this.RefreshData();

            //if (this.SearchGridData.Any())
            //{
            //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //    {
            //        Messenger.Default.Send(this.SearchGridData.First(), Token.Macro);
            //    }));

            //    return;
            //}

            var schemaData = new CollectionData();
            this.stringDataColumns.Clear();
            this.intDataColumns.Clear();
            this.decimalDataColumns.Clear();
            this.dateDataColumns.Clear();

            foreach (DataColumn column in dt.Columns)
            {
                if (column.DataType == typeof(string))
                {
                    stringDataColumns.Add(column);
                    continue;
                }

                if (column.DataType == typeof(int))
                {
                    intDataColumns.Add(column);
                    continue;
                }

                if (column.DataType == typeof(decimal))
                {
                    decimalDataColumns.Add(column);
                    continue;
                }

                if (column.DataType == typeof(DateTime))
                {
                    dateDataColumns.Add(column);
                    continue;
                }
            }

            var count = 1;
            foreach (DataColumn column in stringDataColumns)
            {
                this.MacroColumns.Add(column.ColumnName);
                schemaData.Data = column.ColumnName;
                #region Columns

                if (count == 1)
                {
                    schemaData.RowData.Add(Tuple.Create("Item1", schemaData.Data, string.Empty));
                }
                else if (count == 2)
                {
                    schemaData.RowData.Add(Tuple.Create("Item2", schemaData.Data, string.Empty));
                }
                else if (count == 3)
                {
                    schemaData.RowData.Add(Tuple.Create("Item3", schemaData.Data, string.Empty));
                }
                else if (count == 4)
                {
                    schemaData.RowData.Add(Tuple.Create("Item4", schemaData.Data, string.Empty));
                }
                else if (count == 5)
                {
                    schemaData.RowData.Add(Tuple.Create("Item5", schemaData.Data, string.Empty));
                }
                else if (count == 6)
                {
                    schemaData.RowData.Add(Tuple.Create("Item6", schemaData.Data, string.Empty));
                }
                else if (count == 7)
                {
                    schemaData.RowData.Add(Tuple.Create("Item7", schemaData.Data, string.Empty));
                }

                if (count == 8)
                {
                    schemaData.RowData.Add(Tuple.Create("Item8", schemaData.Data, string.Empty));
                    continue;
                }
                else if (count == 9)
                {
                    schemaData.RowData.Add(Tuple.Create("Item9", schemaData.Data, string.Empty));
                }
                else if (count == 10)
                {
                    schemaData.RowData.Add(Tuple.Create("Item10", schemaData.Data, string.Empty));
                }
                else if (count == 11)
                {
                    schemaData.RowData.Add(Tuple.Create("Item11", schemaData.Data, string.Empty));
                }
                else if (count == 12)
                {
                    schemaData.RowData.Add(Tuple.Create("Item12", schemaData.Data, string.Empty));
                }
                else if (count == 13)
                {
                    schemaData.RowData.Add(Tuple.Create("Item13", schemaData.Data, string.Empty));
                }
                else if (count == 14)
                {
                    schemaData.RowData.Add(Tuple.Create("Item14", schemaData.Data, string.Empty));
                }
                else if (count == 15)
                {
                    schemaData.RowData.Add(Tuple.Create("Item15", schemaData.Data, string.Empty));
                }

                #endregion

                count++;
            }

            count = 16;
            foreach (DataColumn column in intDataColumns)
            {
                this.MacroColumns.Add(column.ColumnName);
                schemaData.Data = column.ColumnName;
                #region Columns

                if (count == 16)
                {
                    schemaData.RowData.Add(Tuple.Create("Item16", schemaData.Data, string.Empty));
                }
                else if (count == 17)
                {
                    schemaData.RowData.Add(Tuple.Create("Item17", schemaData.Data, string.Empty));
                }
                else if (count == 18)
                {
                    schemaData.RowData.Add(Tuple.Create("Item18", schemaData.Data, string.Empty));
                }
                else if (count == 19)
                {
                    schemaData.RowData.Add(Tuple.Create("Item19", schemaData.Data, string.Empty));
                }
                else if (count == 20)
                {
                    schemaData.RowData.Add(Tuple.Create("Item20", schemaData.Data, string.Empty));
                }

                #endregion

                count++;
            }

            count = 21;
            foreach (DataColumn column in decimalDataColumns)
            {
                this.MacroColumns.Add(column.ColumnName);
                schemaData.Data = column.ColumnName;
                #region Columns

                if (count == 21)
                {
                    schemaData.RowData.Add(Tuple.Create("Item21", schemaData.Data, string.Empty));
                }
                else if (count == 22)
                {
                    schemaData.RowData.Add(Tuple.Create("Item22", schemaData.Data, string.Empty));
                }
                else if (count == 23)
                {
                    schemaData.RowData.Add(Tuple.Create("Item23", schemaData.Data, string.Empty));
                }
                else if (count == 24)
                {
                    schemaData.RowData.Add(Tuple.Create("Item24", schemaData.Data, string.Empty));
                }
                else if (count == 25)
                {
                    schemaData.RowData.Add(Tuple.Create("Item25", schemaData.Data, string.Empty));
                }
                
                #endregion

                count++;
            }

            count = 26;
            foreach (DataColumn column in dateDataColumns)
            {
                this.MacroColumns.Add(column.ColumnName);
                schemaData.Data = column.ColumnName;
                #region Columns

                if (count == 26)
                {
                    schemaData.RowData.Add(Tuple.Create("Item26", schemaData.Data, string.Empty));
                }
                else if (count == 27)
                {
                    schemaData.RowData.Add(Tuple.Create("Item27", schemaData.Data, string.Empty));
                }
                else if (count == 28)
                {
                    schemaData.RowData.Add(Tuple.Create("Item28", schemaData.Data, string.Empty));
                }
                else if (count == 29)
                {
                    schemaData.RowData.Add(Tuple.Create("Item29", schemaData.Data, string.Empty));
                }
                else if (count == 30)
                {
                    schemaData.RowData.Add(Tuple.Create("Item30", schemaData.Data, string.Empty));
                }

                #endregion

                count++;
            }

           
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(schemaData, Token.Macro);
            }));
        }

        /// <summary>
        /// Refreshes the search data.
        /// </summary>
        /// <returns>The query datatable.</returns>
        protected DataTable RefreshData()
        {
            var data = this.DataManager.GetSearchScreenMacroData(this.SelectedSearchScreenMacro, this.FromDate.ToDate(), this.ToDate.ToDate(), this.ShowAll, this.SearchText, this.SelectedDateType?.DateType);
            var dt = data.Item1;
            var msg = data.Item2;
            this.SearchGridData.Clear();
            this.SelectedSearchGridRows?.Clear();
            var columnCount = dt.Columns.Count;

            foreach (DataRow row in dt.Rows)
            {
                this.stringDataColumns.Clear();
                this.intDataColumns.Clear();
                this.decimalDataColumns.Clear();
                this.dateDataColumns.Clear();

                var x = new CollectionData();

                foreach (DataColumn dataColumn in dt.Columns)
                {
                    if (dataColumn.DataType == typeof(string))
                    {
                        this.stringDataColumns.Add(dataColumn);
                        continue;
                    }

                    if (dataColumn.DataType == typeof(int))
                    {
                        this.intDataColumns.Add(dataColumn);
                        continue;
                    }

                    if (dataColumn.DataType == typeof(decimal))
                    {
                        this.decimalDataColumns.Add(dataColumn);
                        continue;
                    }

                    if (dataColumn.DataType == typeof(DateTime))
                    {
                        this.dateDataColumns.Add(dataColumn);
                        continue;
                    }
                }

                var count = 1;
                foreach (DataColumn column in stringDataColumns)
                {
                    #region columns
              
                    x.Data = column.ColumnName;
                    if (count == 1)
                    {
                        x.Item1 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item1", x.Data, x.Item1.ToString()));
                    }
                    else if (count == 2)
                    {
                        x.Item2 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item2", x.Data, x.Item2.ToString()));
                    }
                    else if (count == 3)
                    {
                        x.Item3 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item3", x.Data, x.Item3.ToString()));
                    }
                    else if (count == 4)
                    {
                        x.Item4 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item4", x.Data, x.Item4.ToString()));
                    }
                    else if (count == 5)
                    {
                        x.Item5 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item5", x.Data, x.Item5.ToString()));
                    }
                    else if (count == 6)
                    {
                        x.Item6 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item6", x.Data, x.Item6.ToString()));
                    }
                    else if (count == 7)
                    {
                        x.Item7 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item7", x.Data, x.Item7.ToString()));
                    }
                    else if (count == 8)
                    {
                        x.Item8 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item8", x.Data, x.Item8.ToString()));
                    }
                    else if (count == 9)
                    {
                        x.Item9 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item9", x.Data, x.Item9.ToString()));
                    }
                    else if (count == 10)
                    {
                        x.Item10 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item10", x.Data, x.Item10.ToString()));
                    }
                    else if (count == 11)
                    {
                        x.Item11 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item11", x.Data, x.Item11.ToString()));
                    }
                    else if (count == 12)
                    {
                        x.Item12 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item12", x.Data, x.Item12.ToString()));
                    }
                    else if (count == 13)
                    {
                        x.Item13 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item13", x.Data, x.Item13.ToString()));
                    }
                    else if (count == 14)
                    {
                        x.Item14 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item14", x.Data, x.Item14.ToString()));
                    }
                    else if (count == 15)
                    {
                        x.Item15 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item15", x.Data, x.Item15.ToString()));
                    }

                    #endregion

                    x.ID = columnCount;
                    count++;
                }

                count = 16;
                foreach (DataColumn column in this.intDataColumns)
                {
                    #region columns
           
                    x.Data = column.ColumnName;
                    if (count == 16)
                    {
                        x.Item16 = row[column].ToString().ToInt();
                        x.RowData.Add(Tuple.Create("Item16", x.Data, x.Item16.ToString()));
                    }
                    else if (count == 17)
                    {
                        x.Item17 = row[column].ToString().ToInt();
                        x.RowData.Add(Tuple.Create("Item17", x.Data, x.Item17.ToString()));
                    }
                    else if (count == 18)
                    {
                        x.Item18 = row[column].ToString().ToInt();
                        x.RowData.Add(Tuple.Create("Item18", x.Data, x.Item18.ToString()));
                    }
                    else if (count == 19)
                    {
                        x.Item19 = row[column].ToString().ToInt();
                        x.RowData.Add(Tuple.Create("Item19", x.Data, x.Item19.ToString()));
                    }
                    else if (count == 20)
                    {
                        x.Item20 = row[column].ToString().ToInt();
                        x.RowData.Add(Tuple.Create("Item20", x.Data, x.Item20.ToString()));
                    }
                    else if (count == 6)
                    {
                        x.Item6 = row[column].ToString();
                        x.RowData.Add(Tuple.Create("Item6", x.Data, x.Item6.ToString()));
                    }

                    #endregion

                    x.ID = columnCount;
                    count++;}

                count = 21;
                foreach (DataColumn column in this.decimalDataColumns)
                {
                    #region columns
                
                    x.Data = column.ColumnName;
                    if (count == 21)
                    {
                        x.Item21 = row[column].ToString().ToDecimal();
                        x.RowData.Add(Tuple.Create("Item21", x.Data, x.Item21.ToString()));
                    }
                    else if (count == 22)
                    {
                        x.Item22 = row[column].ToString().ToDecimal();
                        x.RowData.Add(Tuple.Create("Item22", x.Data, x.Item22.ToString()));
                    }
                    else if (count == 23)
                    {
                        x.Item23 = row[column].ToString().ToDecimal();
                        x.RowData.Add(Tuple.Create("Item23", x.Data, x.Item23.ToString()));
                    }
                    else if (count == 24)
                    {
                        x.Item24 = row[column].ToString().ToDecimal();
                        x.RowData.Add(Tuple.Create("Item24", x.Data, x.Item24.ToString()));
                    }
                    else if (count == 25)
                    {
                        x.Item25 = row[column].ToString().ToDecimal();
                        x.RowData.Add(Tuple.Create("Item25", x.Data, x.Item25.ToString()));
                    }

                    #endregion

                    x.ID = columnCount;
                    count++;
                }

                count = 26;
                foreach (DataColumn column in dateDataColumns)
                {
                    #region columns
                
                    x.Data = column.ColumnName;
                    if (count == 26)
                    {
                        x.Item26 = row[column].ToString().ToDate();
                        x.RowData.Add(Tuple.Create("Item26", x.Data, x.Item26.ToString()));
                    }
                    else if (count == 27)
                    {
                        x.Item27 = row[column].ToString().ToDate();
                        x.RowData.Add(Tuple.Create("Item27", x.Data, x.Item27.ToString()));
                    }
                    else if (count == 28)
                    {
                        x.Item28 = row[column].ToString().ToDate();
                        x.RowData.Add(Tuple.Create("Item28", x.Data, x.Item28.ToString()));
                    }
                    else if (count == 29)
                    {
                        x.Item29 = row[column].ToString().ToDate();
                        x.RowData.Add(Tuple.Create("Item29", x.Data, x.Item29.ToString()));
                    }
                    else if (count == 30)
                    {
                        x.Item30 = row[column].ToString().ToDate();
                        x.RowData.Add(Tuple.Create("Item30", x.Data, x.Item30.ToString()));
                    }

                    #endregion

                    x.ID = columnCount;
                    count++;
                }

                this.SearchGridData.Add(x);
            }

            if (!string.IsNullOrEmpty(msg))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(msg);
                }));
            }

            return dt;
        }

        /// <summary>
        /// Handle a row selection.
        /// </summary>
        protected virtual void RowSelectedCommandExecute()
        {
            #region validation

            if (this.SelectedSearchGridRow.IsNull())
            {
                return;
            }

            #endregion

            if (!this.KeepVisible)
            {
                this.Close();
            }

            this.ReportManager.ShowReport(this.SelectedSearchGridRow, this.SelectedReport);
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Refresh the search.
        /// </summary>
        private void CreateFileCommandExecute()
        {
            #region validation

            if (this.SelectedReport == null)
            {
                return;
            }

            if (this.SelectedReport.CreateFileProcess == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoReportFileProcess);
                return;
            }

            #endregion

            switch (this.SelectedReport.CreateFileProcess)
            {
                case "Dispatch XML":
                    this.GenerateDispatchFiles();
                    break;
            }
        }

        /// <summary>
        /// Refresh the search.
        /// </summary>
        private void RefreshCommandExecute()
        {
            this.RefreshData();
            SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
        }

        #endregion

        #region helper

        /// <summary>
        /// Exports the dispatch stock data.
        /// </summary>
        private void GenerateDispatchFiles()
        {
            var selectedDispatches = new List<Sale>();
            foreach (var selectedDispatch in this.SelectedSearchGridRows)
            {
                var row = selectedDispatch.RowData.FirstOrDefault(x => x.Item2.CompareIgnoringCase("ID"));
                if (row != null)
                {
                    selectedDispatches.Add(new Sale { SaleID =  row.Item3.ToInt()});
                }
            }

            var dispatches = this.DataManager.GetDispatchStock(selectedDispatches);
            try
            {
                this.DepartmentBodiesManager.ExportDispatchData(dispatches);
                SystemMessage.Write(MessageType.Priority, Message.DataExported);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Gets the date types.
        /// </summary>
        private void GetDateTypes()
        {
            this.ReportDateTypes = new ObservableCollection<SearchDateType>(this.DataManager.GetSearchDateTypes());
            this.DateTypes = new ObservableCollection<SearchDateType>(this.ReportDateTypes);
        }

        /// <summary>
        /// Sets the reports.
        /// </summary>
        protected virtual void GetReports()
        {
            this.ReportManager.GetReports();
            var ssrsReports = this.ReportManager.Reports;
            if (ssrsReports != null)
            {
                this.Reports = new ObservableCollection<ReportData>(ssrsReports.OrderBy(x => x.Name));
            }
        }

        #endregion

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="ReportViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Report
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class EposReportViewModel : NouvemViewModelBase
    {
        #region private

        /// <summary>
        /// The selected report start date.
        /// </summary>
        private DateTime? selectedStartDate;

        /// <summary>
        /// The selected report end date.
        /// </summary>
        private DateTime? selectedEndDate;

        /// <summary>
        /// Flag, as to whether the start date calender is displayed or not.
        /// </summary>
        private bool showStartDateCalender;

        /// <summary>
        /// Flag, as to whether the end date calender is displayed or not.
        /// </summary>
        private bool showEndDateCalender;

        /// <summary>
        /// The sales data.
        /// </summary>
        private ObservableCollection<ProductData> salesData;

        /// <summary>
        /// The selected salews data item.
        /// </summary>
        private ProductData selectedSalesData;

        /// <summary>
        /// The between dates sales data flag.
        /// </summary>
        private bool betweenDates;

        /// <summary>
        /// The daily sales data flag.
        /// </summary>
        private bool daily;

        /// <summary>
        /// The weekly sales data flag.
        /// </summary>
        private bool weekly;

        /// <summary>
        /// The monthly sales data flag.
        /// </summary>
        private bool monthly;

        /// <summary>
        /// The quarterly sales data flag.
        /// </summary>
        private bool quarterly;

        /// <summary>
        /// The yearly sales data flag.
        /// </summary>
        private bool yearly;

        /// <summary>
        /// The user authorisation flag.
        /// </summary>
        private bool canAccess;

        /// <summary>
        /// Flag, that indicates whether we are in chart display mode.
        /// </summary>
        private bool chartDisplayMode;

        /// <summary>
        /// The stock takes collection.
        /// </summary>
        private ObservableCollection<StockTake> stockTakes;

        /// <summary>
        /// The selected stock take.
        /// </summary>
        private StockTake selectedStockTake;

        #endregion

        #region constructor

         /// <summary>
        /// Initializes a new instance of the ReportViewModel class.
        /// </summary>
        public EposReportViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

           // Messenger.Default.Register<string>(this, Token.CalenderClosed, x => this.ShowCalender = false);
            Messenger.Default.Register<string>(this, Token.DisplayConfirmationBox, x => NouvemMessageBox.Show(Message.EposReportExportSuccessful, touchScreen:true));

            Messenger.Default.Register<string>(this, Token.DisplayErrorBox, x =>
            {
                this.Log.LogError(this.GetType(), x);
                NouvemMessageBox.Show(Message.EposReportExportError, touchScreen: true);
            });

            #endregion

            #region command

            // Resets the dates.
            this.OnLoadedCommand = new RelayCommand(() =>
            {
                this.canAccess = false;

                if (!this.Locator.Epos.LogStatus)
                {
                    // user logged out
                    NouvemMessageBox.Show(Message.LoggedOut, touchScreen: true);
                    return;
                }

                // check user authorisations
                if (!this.Locator.Epos.FullAuthorisation)
                {
                    NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                    return;
                }

                this.canAccess = true;
                this.GetStockTakes();
            });

            // Resets the dates.
            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                if (!this.chartDisplayMode)
                {
                    this.ResetDates();
                }

                this.chartDisplayMode = false;
            });

            // The command to handle the move to display the product revenue chart.
             this.SetChartDisplayModeCommand = new RelayCommand(() => this.chartDisplayMode = true);

            // The command to handle the arrows movement.
            this.MoveToItemCommand = new RelayCommand<string>(this.MoveToItemCommandExecute);

            // Handles the calender date change event.
            this.OnSelectedStartDateChangedCommand = new RelayCommand(() => this.ShowStartDateCalender = false);

            // Handles the calender date change event.
            this.OnSelectedDateChangedCommand = new RelayCommand(() =>
            {
                this.ShowEndDateCalender = false;
                this.ShowStartDateCalender = false;
            });

            // Handle the variance report after stocktake.
            this.VarianceReportCommand = new RelayCommand(this.VarianceReportCommandExecute);

            #endregion

            #region instantiation

            this.SalesData = new ObservableCollection<ProductData>();

            #endregion

            this.BetweenDates = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a the application stock takes.
        /// </summary>
        public ObservableCollection<StockTake> StockTakes
        {
            get
            {
                return this.stockTakes;
            }

            set
            {
                this.stockTakes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected stock take.
        /// </summary>
        public StockTake SelectedStockTake
        {
            get
            {
                return this.selectedStockTake;
            }

            set
            {
                this.selectedStockTake = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the sales are to be displayed is to be between the selected dates.
        /// </summary>
        public bool BetweenDates
        {
            get
            {
                return this.betweenDates;
            }

            set
            {
                this.betweenDates = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SelectedStartDate = null;
                    this.SelectedEndDate = null;
                    this.SalesData.Clear();
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the current daily sales are to be displayed.
        /// </summary>
        public bool Daily
        {
            get
            {
                return this.daily;
            }

            set
            {
                this.daily = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SelectDailyRange();
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the current weekly sales are to be displayed.
        /// </summary>
        public bool Weekly
        {
            get
            {
                return this.weekly;
            }

            set
            {
                this.weekly = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SelectWeeklyRange();
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the current months sales are to be displayed.
        /// </summary>
        public bool Monthly
        {
            get
            {
                return this.monthly;
            }

            set
            {
                this.monthly = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SelectMonthlyRange();
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the current quarters sales are to be displayed.
        /// </summary>
        public bool Quarterly
        {
            get
            {
                return this.quarterly;
            }

            set
            {
                this.quarterly = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SelectQuarterlyRange();
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the current years sales are to be displayed.
        /// </summary>
        public bool Yearly
        {
            get
            {
                return this.yearly;
            }

            set
            {
                this.yearly = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.SelectYearlyRange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected sales data.
        /// </summary>
        public ProductData SelectedSalesData
        {
            get
            {
                return this.selectedSalesData;
            }

            set
            {
                this.selectedSalesData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sales data.
        /// </summary>
        public ObservableCollection<ProductData> SalesData
        {
            get
            {
                return this.salesData;
            }

            set
            {
                this.salesData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected start date.
        /// </summary>
        public DateTime? SelectedStartDate
        {
            get
            {
                return this.selectedStartDate;
            }

            set
            {
                this.selectedStartDate = value;
                this.RaisePropertyChanged();

                if (this.betweenDates)
                {
                    this.GetSalesData();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected end date.
        /// </summary>
        public DateTime? SelectedEndDate
        {
            get
            {
                return this.selectedEndDate;
            }

            set
            {
                this.selectedEndDate = value;
                this.RaisePropertyChanged();

                if (this.betweenDates)
                {
                    this.GetSalesData();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the start date calender is displayed or not.
        /// </summary>
        public bool ShowStartDateCalender
        {
            get
            {
                return this.showStartDateCalender;
            }

            set
            {
                this.showStartDateCalender = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // open the calender.
                    Messenger.Default.Send(Constant.Start, Token.DisplayCalender);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the end date calender is displayed or not.
        /// </summary>
        public bool ShowEndDateCalender
        {
            get
            {
                return this.showEndDateCalender;
            }

            set
            {
                this.showEndDateCalender = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // open the calender.
                    Messenger.Default.Send(Constant.End, Token.DisplayCalender);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the arrow grid movements.
        /// </summary>
        public ICommand MoveToItemCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }
        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the start date change.
        /// </summary>
        public ICommand OnSelectedStartDateChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the end date change.
        /// </summary>
        public ICommand OnSelectedDateChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the move to chart display.
        /// </summary>
        public ICommand SetChartDisplayModeCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the variance report.
        /// </summary>
        public ICommand VarianceReportCommand { get; private set; }

        #endregion

        #endregion

        #region protected override



        #endregion

        #region private

        /// <summary>
        /// Gets the application stock takes.
        /// </summary>
        private void GetStockTakes()
        {
            this.StockTakes = new ObservableCollection<StockTake>(this.DataManager.GetStockTakes());

            if (this.StockTakes.Any())
            {
                this.SelectedStockTake = this.StockTakes.First();
            }
        }

        /// <summary>
        /// Handles the variance report.
        /// </summary>
        private void VarianceReportCommandExecute()
        {
            this.Log.LogDebug(this.GetType(), "VarianceReportCommandExecute(): Debug");
            // use this flag to keep the main report data in place.
            this.chartDisplayMode = true;
        }

        /// <summary>
        /// Handles the purchases arrow navigation.
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        private void MoveToItemCommandExecute(string direction)
        {
            if (this.salesData == null)
            {
                return;
            }

            if (direction.Equals(Constant.Up))
            {
                this.MoveBack();
                return;
            }

            this.MoveNext();
        }

        /// <summary>
        /// Moves to the previous item in the sales grid.
        /// </summary>
        private void MoveBack()
        {
            var previousItem =
                this.SalesData.TakeWhile(
                    item => item.Id != this.SelectedSalesData.Id)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedSalesData = previousItem;
            }
        }

        /// <summary>
        /// Moves to the next item in the sales grid.
        /// </summary>
        private void MoveNext()
        {
            var nextItem =
                this.SalesData.SkipWhile(
                    item => item.Id != this.SelectedSalesData.Id)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedSalesData = nextItem;
            }
        }

        /// <summary>
        /// Resets the dates. 
        /// </summary>
        private void ResetDates()
        {
            this.SelectedStartDate = null;
            this.SelectedEndDate = null;
            this.SalesData.Clear();
            this.BetweenDates = true;
        }

        /// <summary>
        /// Gets the sales data.
        /// </summary>
        private void GetSalesData()
        {
            if (!this.SelectedStartDate.HasValue || !this.SelectedEndDate.HasValue || !this.canAccess)
            {
                return;
            }

            if (this.SelectedEndDate < this.SelectedStartDate)
            {
                NouvemMessageBox.Show(Message.SelectionDatesError, touchScreen:true);
                return;
            }

            this.SalesData = new ObservableCollection<ProductData>(this.DataManager.GetEposSalesData(this.selectedStartDate.ToDate(), this.selectedEndDate.ToDate()));

            if (this.salesData.Any())
            {
                this.SelectedSalesData = this.salesData.ElementAt(0);
            }
        }

        /// <summary>
        /// Gets the date range of the current day.
        /// </summary>
        private void SelectDailyRange()
        {
            this.SelectedStartDate = DateTime.Today;
            this.SelectedEndDate = DateTime.Today;
            this.GetSalesData();
        }

        /// <summary>
        /// Gets the date range for the current week.
        /// </summary>
        private void SelectWeeklyRange()
        {
            // get the current weeks mondays date.
            var currentWeek = DateTime.Today.Year.GetMondaysDate();
            this.SelectedStartDate = currentWeek;
            this.SelectedEndDate = DateTime.Today;
            this.GetSalesData();
        }

        /// <summary>
        /// Gets the date range for the current month.
        /// </summary>
        private void SelectMonthlyRange()
        {
            var today = DateTime.Today;
            var month = today.Month;
            var year = today.Year;

            int[] thirtyDayMonths = { 4, 6, 9, 11 };
            int[] thirtyOneDayMonths = { 1, 3, 5, 7, 8, 10, 12 };

            this.SelectedStartDate = new DateTime(year, month, 1);

            if (thirtyDayMonths.Contains(month))
            {
                this.SelectedEndDate = new DateTime(year, month, 30);
            }
            else if (thirtyOneDayMonths.Contains(month))
            {
                this.SelectedEndDate = new DateTime(year, month, 31);
            }
            else
            {
                // february
                if (DateTime.IsLeapYear(year))
                {
                    this.SelectedEndDate = new DateTime(year, month, 29);
                }
                else
                {
                    this.SelectedEndDate = new DateTime(year, month, 28);
                }
            }

            this.GetSalesData();
        }

        /// <summary>
        /// Selects the quarterly date range.
        /// </summary>
        private void SelectQuarterlyRange()
        {
            var today = DateTime.Today;
            var month = today.Month;
            var year = today.Year;

            if (month <= 3)
            {
                this.SelectedStartDate = new DateTime(year, 1, 1);
                this.SelectedEndDate = new DateTime(year, 3, 31);
            }
            else if (month <= 6)
            {
                this.SelectedStartDate = new DateTime(year, 4, 1);
                this.SelectedEndDate = new DateTime(year, 6, 30);
            }
            else if (month <= 9)
            {
                this.SelectedStartDate = new DateTime(year, 7, 1);
                this.SelectedEndDate = new DateTime(year, 9, 30);
            }
            else
            {
                this.SelectedStartDate = new DateTime(year, 10, 1);
                this.SelectedEndDate = new DateTime(year, 12, 31);
            }

            this.GetSalesData();
        }

        /// <summary>
        /// Selects the yearly date range.
        /// </summary>
        private void SelectYearlyRange()
        {
            this.SelectedStartDate = new DateTime(DateTime.Now.Year, 1, 1);
            this.SelectedEndDate = new DateTime(DateTime.Now.Year, 12, 31);
            this.GetSalesData();
        }

        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SalesSearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Report
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class that handles the search for the sale orders.
    /// </summary>
    public class ReportBaseViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected sales.
        /// </summary>
        private ObservableCollection<Sale> selectedSales = new ObservableCollection<Sale>();

        /// <summary>
        /// The selected grid sale.
        /// </summary>
        private Sale currentSale;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<Sale> filteredSales;

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// The searcht to date.
        /// </summary>
        private DateTime toDate;

        /// <summary>
        /// The search from id.
        /// </summary>
        private string fromId;

        /// <summary>
        /// The searcht to date.
        /// </summary>
        private string toId;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        protected bool keepVisible;

        /// <summary>
        /// The show all orders flag..
        /// </summary>
        protected bool showAllOrders;

        /// <summary>
        /// Are we in touchscreen mode flag.
        /// </summary>
        public bool isTouchscreenMode;

        /// <summary>
        /// All the report sales.
        /// </summary>
        protected IList<Sale> allSales = new List<Sale>();

        protected bool runReport;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportBaseViewModel"/> class.
        /// </summary>
        public ReportBaseViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the incoming customers sales.
            Messenger.Default.Register<IList<Sale>>(this, Token.DisplayReportData, this.SetIncomingSales);

            Messenger.Default.Register<string>(this, Token.TouchscreenMode, s =>
            {
                this.runReport = false;
                this.isTouchscreenMode = true;
            });

            #endregion

            #region command registration

            this.SearchRangeCommand = new RelayCommand(this.HandleReportIdSearch);

            this.SaleSelectedCommand = new RelayCommand(this.SaleSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            #region instantiation

            this.FilteredSales = new ObservableCollection<Sale>();

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether active sales are included when filtering.
        /// </summary>
        public bool IncludeActiceSalesInFilter { get; set; }

        /// <summary>
        /// Gets or sets the selected sales.
        /// </summary>
        public ObservableCollection<Sale> SelectedSales
        {
            get
            {
                return this.selectedSales;
            }

            set
            {
                this.selectedSales = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromDate
        {
            get
            {
                return this.fromDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.fromDate = DateTime.Today;
                }
                else
                {
                    this.fromDate = value;
                }

                this.RaisePropertyChanged();
                ApplicationSettings.ReportsSearchFromDate = value;

                if (this.IsFormLoaded)
                {
                   this.HandleReportDateSearch();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToDate
        {
            get
            {
                return this.toDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.toDate = DateTime.Today;
                }
                else
                {
                    this.toDate = value;
                }

                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    //ApplicationSettings.SalesSearchToDate = value;
                    this.HandleReportDateSearch();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from id.
        /// </summary>
        public string FromId
        {
            get
            {
                return this.fromId;
            }

            set
            {
                this.fromId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public string ToId
        {
            get
            {
                return this.toId;
            }

            set
            {
                this.toId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all the orders are to be dispalyed.
        /// </summary>
        public bool ShowAllOrders
        {
            get
            {
                return this.showAllOrders;
            }

            set
            {
                this.showAllOrders = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    this.HandleReportDateSearch();
                }
            }
        }

        /// <summary>
        /// Gets or sets the filtered sales.
        /// </summary>
        public ObservableCollection<Sale> FilteredSales
        {
            get
            {
                return this.filteredSales;
            }

            set
            {
                this.filteredSales = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search is emanating from the master vm i.e. report display search.
        /// </summary>
        public bool ReportMode { get; set; }

        /// <summary>
        /// Gets or sets the associated view.
        /// </summary>
        public ViewType AssociatedView { get; set; }

        /// <summary>
        /// Gets or sets the associated view.
        /// </summary>
        public int? BatchProductID { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the sale selection.
        /// </summary>
        public ICommand SearchRangeCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the sale selection.
        /// </summary>
        public ICommand SaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Set the mode.
        /// </summary>
        /// <param name="inTouchscreenMode">Is touchscreen mode flag.</param>
        public void SetData(bool inTouchscreenMode)
        {
            if (inTouchscreenMode)
            {
                this.runReport = true;
                this.isTouchscreenMode = true;
            }
            else
            {
                this.runReport = false;
                this.isTouchscreenMode = false;
            }
        }

        /// <summary>
        /// Set the filtered sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        public void SetSales(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales);
        }

        /// <summary>
        /// Method that sets the sales to display.
        /// </summary>
        /// <param name="data">The sales to display.</param>
        public void SetFilteredSales(IList<Sale> data)
        {
            this.FilteredSales = new ObservableCollection<Sale>(data);
        }

        /// <summary>
        /// Method that sets the sales to display.
        /// </summary>
        public void SetIncomingReportSales(IList<Sale> sales )
        {
            this.SetIncomingSales(sales);
        }

        #endregion

        #endregion

        #region protected

        #region virtual

        /// <summary>
        /// Handle a date change.
        /// </summary>
        protected virtual void HandleReportDateSearch()
        {
            this.EntitySelectionChange = true;

            var orderStatuses = new List<int?>
            {
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID
            };

            if (this.AssociatedView == ViewType.BatchEdit)
            {
                var localSales = this.DataManager.GetProductionOrdersForReport(this.FromDate, this.ToDate);
                if (this.showAllOrders)
                {
                    this.FilteredSales = new ObservableCollection<Sale>(localSales);
                }
                else
                {
                    this.FilteredSales = new ObservableCollection<Sale>(localSales.Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID));
                }

                return;
            }

            if (this.AssociatedView == ViewType.BatchEditOrdered)
            {
                var localSales = this.DataManager.GetProductionOrdersForReportByOrderId(this.FromDate, this.ToDate);
                if (this.showAllOrders)
                {
                    this.FilteredSales = new ObservableCollection<Sale>(localSales);
                }
                else
                {
                    this.FilteredSales = new ObservableCollection<Sale>(localSales.Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID));
                }

                return;
            }

            if (this.AssociatedView == ViewType.Invoice)
            {
                if (this.ShowAllOrders)
                {
                    var statuses = new List<int?> { NouvemGlobal.NouDocStatusExported.NouDocStatusID, NouvemGlobal.NouDocStatusActive.NouDocStatusID, NouvemGlobal.NouDocStatusComplete.NouDocStatusID, NouvemGlobal.NouDocStatusCancelled.NouDocStatusID };
                    var data = this.DataManager.GetAllInvoicesByDate(statuses, this.FromDate, this.ToDate);
                    this.FilteredSales =
                   new ObservableCollection<Sale>(data.OrderByDescending(x => x.SaleID));
                }
                else
                {
                    if (!this.IncludeActiceSalesInFilter)
                    {
                        var statuses = new List<int?> { NouvemGlobal.NouDocStatusExported.NouDocStatusID, NouvemGlobal.NouDocStatusComplete.NouDocStatusID };
                        var data = this.DataManager.GetAllInvoicesByDate(statuses, this.FromDate, this.ToDate);
                        this.FilteredSales =
                       new ObservableCollection<Sale>(data.OrderByDescending(x => x.SaleID));
                    }
                    else
                    {
                        var statuses = new List<int?> { NouvemGlobal.NouDocStatusExported.NouDocStatusID, NouvemGlobal.NouDocStatusActive.NouDocStatusID, NouvemGlobal.NouDocStatusComplete.NouDocStatusID };
                        var data = this.DataManager.GetAllInvoicesByDate(statuses, this.FromDate, this.ToDate);
                        this.FilteredSales =
                       new ObservableCollection<Sale>(data.OrderByDescending(x => x.SaleID));
                    }
                }

                this.EntitySelectionChange = false;
                return;
            }

            if (this.AssociatedView == ViewType.Pallets)
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetAllPallets(this.FromDate, this.ToDate)
                .Where(x => !x.Quantity.IsNullOrZero()));
                return;
            }

            if (this.AssociatedView == ViewType.APReceipt)
            {
                if (this.ShowAllOrders)
                {
                    this.FilteredSales =
                        new ObservableCollection<Sale>(this.DataManager.GetAllAPReceiptsForReportByDates(orderStatuses, this.FromDate, this.ToDate).OrderByDescending(x => x.SaleID));
                }
                else
                {
                    orderStatuses = new List<int?>
                    {
                        NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                        NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                        NouvemGlobal.NouDocStatusExported.NouDocStatusID
                    };

                    this.FilteredSales =
                        new ObservableCollection<Sale>(this.DataManager.GetAllAPReceiptsForReportByDates(orderStatuses, this.FromDate, this.ToDate).OrderByDescending(x => x.SaleID));
                }

                return;
            }

            if (this.ShowAllOrders)
            {
                this.FilteredSales =
               new ObservableCollection<Sale>(this.DataManager.GetAllARDispatchesForReportByDates(orderStatuses, this.FromDate, this.ToDate).OrderByDescending(x => x.SaleID));
            }
            else
            {
                if (!this.IncludeActiceSalesInFilter)
                {
                    orderStatuses = new List<int?>
                    {
                        NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                        NouvemGlobal.NouDocStatusExported.NouDocStatusID
                    };

                    this.FilteredSales =
                        new ObservableCollection<Sale>(this.DataManager.GetAllARDispatchesForReportByDates(orderStatuses, this.FromDate, this.ToDate).OrderByDescending(x => x.SaleID));
                }
                else
                {
                    orderStatuses = new List<int?>
                    {
                        NouvemGlobal.NouDocStatusCancelled.NouDocStatusID
                    };

                    this.FilteredSales =
                        new ObservableCollection<Sale>(this.DataManager.GetAllARDispatchesForReportByDates(orderStatuses, this.FromDate, this.ToDate).OrderByDescending(x => x.SaleID));
                }
            }

            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Handle a date change.
        /// </summary>
        protected virtual void HandleReportIdSearch()
        {
            if (string.IsNullOrEmpty(this.fromId))
            {
                return;
            }

            if (string.IsNullOrEmpty(this.toId))
            {
                this.toId = this.fromId;
            }

            if (this.fromId.ToInt() > 0 && this.toId.ToInt() > 0)
            {
                var range = this.toId.ToInt() - this.fromId.ToInt();
                if (range > ApplicationSettings.TransactionsToTakeForReportSearch)
                {
                    var message =
                        string.Format(Message.OutsideAllowableSearchRange, ApplicationSettings.TransactionsToTakeForReportSearch);
                    SystemMessage.Write(MessageType.Issue, message);
                    return;
                }

                var localTransactions = this.DataManager.GetTransactionsForReports(this.fromId.ToInt(), this.toId.ToInt());
                this.FilteredSales =
                    new ObservableCollection<Sale>(localTransactions.OrderByDescending(x => x.SaleID));
            }
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected virtual void Close()
        {
            this.ReportMode = false;
            this.runReport = false;
            this.IncludeActiceSalesInFilter = false;
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
                this.FilteredSales.Clear();
            }
        }

        /// <summary>
        /// Set the incoming sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        protected virtual void SetIncomingSales(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleID));
            this.allSales = this.filteredSales;
            this.HandleReportDateSearch();
        }

        #endregion

        #region override

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected virtual void OnLoadingCommandExecute()
        {
            this.FromDate = ApplicationSettings.ReportsSearchFromDate;
            this.ToDate = DateTime.Today;
            this.KeepVisible = ApplicationSettings.ReportSalesSearchDataKeepVisible;
            this.ShowAllOrders = ApplicationSettings.ReportSalesSearchShowAllOrders;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected virtual void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            this.isTouchscreenMode = false;
            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.runReport = false;
            this.AssociatedView = ViewType.APOrder;
            this.Close();
        }

        /// <summary>
        /// Handler for the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            ApplicationSettings.ReportsSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchToDate = this.ToDate;
            ApplicationSettings.ReportSalesSearchDataKeepVisible = this.KeepVisible;
            ApplicationSettings.ReportSalesSearchShowAllOrders = this.ShowAllOrders;

            if (this.SelectedSales.Count > 1)
            {
                Messenger.Default.Send(this.selectedSales.ToList(), Token.SearchSaleSelectedForReport);
            }

            this.ShowReport(false);

            this.Close();
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            ApplicationSettings.ReportsSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchToDate = this.ToDate;
            ApplicationSettings.ReportSalesSearchDataKeepVisible = this.KeepVisible;
            ApplicationSettings.ReportSalesSearchShowAllOrders = this.ShowAllOrders;

            this.KeepVisible = false;
            this.Close();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Displays the report.
        /// </summary>
        protected void ShowReport(bool isTouchscreen = true)
        {
            if (isTouchscreen)
            {
                if (this.FromId != null && this.ToId != null && this.FromId.ToInt() > 0 && this.ToId.ToInt() > 0)
                {
                    Messenger.Default.Send(new Sale { DocumentNumberingID = this.FromId.ToInt(), Number = this.ToId.ToInt() }, Token.SearchSaleSelectedForTouchscreenReport);
                }
            }
            else
            {
                if (this.FromId != null && this.ToId != null && this.FromId.ToInt() > 0 && this.ToId.ToInt() > 0)
                {
                    Messenger.Default.Send(new Sale { DocumentNumberingID = this.FromId.ToInt(), Number = this.ToId.ToInt() }, Token.SearchSaleSelectedForReport);
                }
            }
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected virtual void SaleSelectedCommandExecute()
        {
            if (this.SelectedSales != null && this.SelectedSales.Any())
            {
                Messenger.Default.Send(this.selectedSales.First(), Token.SearchSaleSelectedForReport);
            }

            //Messenger.Default.Send(Token.Message, Token.SetTopMost);
            this.Close();
        }

        #endregion

        #region helper


        #endregion

        #endregion
    }
}



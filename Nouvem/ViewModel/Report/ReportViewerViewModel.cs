﻿// -----------------------------------------------------------------------
// <copyright file="ReportViewerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Reporting.WinForms;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Report
{
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;

    public class ReportViewerViewModel : NouvemViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportViewerViewModel"/> class.
        /// </summary>
        public ReportViewerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<ReportData>(this, Token.EmailingReport, this.HandleReportEmail);

            #endregion

            #region command

            // Handle the ui load event.
            this.OnLoadingCommand = new RelayCommand(() => this.IsFormLoaded = true);

            // Handle the un load event.
            this.OnUnloadedCommand = new RelayCommand(() => this.IsFormLoaded = false);

            // Inform the report viewer to attach the current report to the selected email account and open.
            this.EmailReportCommand = new RelayCommand(() => Messenger.Default.Send(this.AttachmentType, Token.AttachReportToEmail));

            #endregion

            this.AttachmentType = "PDF";
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the attachment type.
        /// </summary>
        public string AttachmentType { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to attach the report.
        /// </summary>
        public RelayCommand EmailReportCommand { get; private set; }

        /// <summary>
        /// Gets the main form load event.
        /// </summary>
        public RelayCommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the main form load event.
        /// </summary>
        public RelayCommand OnUnloadedCommand { get; private set; }

        #endregion

        #region protected

        /// <summary>
        /// Override, to display ok only.
        /// </summary>
        /// <param name="mode">The incoming mode.</param>
        protected override void ControlCommandExecute(string mode)
        {
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and cleanup.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Close and cleanup.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execute


        #endregion

        #region helper

        private bool processing;


        public void HandlePalletsCardMultiBatch(int id, bool print, bool generateFile)
        {
            if (this.processing)
            {
                return;
            }

            this.processing = true;

            try
            {
                var stock = this.DataManager.GetDispatchPalletData(id);

                var palletStock = stock.StockDetails;

                ProgressBar.SetUp(0, palletStock.Count + 1);
                ProgressBar.Run();

                this.Log.LogInfo(this.GetType(),
                    string.Format("Exporting Labels: {0}", ApplicationSettings.LabelImagePath));
                if (print)
                {
                    foreach (var item in palletStock)
                    {
                        ProgressBar.Run();
                        var label = this.LabelManager.GetGs1PalletLabel("Pallet Card");
                        if (label != null)
                        {
                            var files = Directory.EnumerateFiles(ApplicationSettings.LabelImagePath).Where(x => !x.EndsWithIgnoringCase("_ignored"));
                            foreach (var file in files)
                            {
                                var newFilename = $"{file}{DateTime.Now.ToString("ddMMyyyyHHmmssFFF")}_ignored";

                                if (!file.Equals(newFilename))
                                {
                                    // mark the file as having been processed.
                                    File.Move(file, newFilename);
                                }
                            }

                            var palletId = item.PalletID.ToInt();

                            var count = 1;
                            foreach (var batchPallet in item.StockDetails.GroupBy(x => x.BatchNumberID))
                            {
                                var path = ApplicationSettings.LabelImagePath;
                                var wgt = batchPallet.Sum(x => x.TransactionWeight).ToDecimal();
                                var qty = batchPallet.Sum(x => x.TransactionQty).ToDecimal();
                                var batchId = batchPallet.Key.ToInt();
                                var imageName = $"LabelImage{count}.Png";
                                this.PrintManager.ExportLabelImageMultiBatch(label, palletId, path, wgt, qty, batchId, imageName);
                                System.Threading.Thread.Sleep(100);
                                count++;
                            }

                            var reportParam = new List<ReportParameter>
                            {
                                new ReportParameter {Name = "DispatchID", Values = {id.ToString()}},
                                new ReportParameter {Name = "PalletID", Values = {palletId.ToString()}},
                                new ReportParameter {Name = "PalletNo", Values = {item.PalletNo.ToString()}}
                            };

                            var reportData = new ReportData
                            {
                                Name = "Pallets Card Multi Batch",
                                ReportParameters = reportParam
                            };

                            if (!ApplicationSettings.DontCreateCSVForPalletCard)
                            {
                                this.ReportManager.PrintReport(reportData);
                            }
                            else
                            {
                                this.ReportManager.PrintReport(reportData);
                                this.ReportManager.CreateReportForAttachment(reportData,
                                    ApplicationSettings.ExportReportPath, Constant.Pallet);
                            }
                        }
                    }
                }

                if (generateFile)
                {
                    this.DepartmentBodiesManager.ExportDispatchPalletData(stock);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                this.processing = false;
                ProgressBar.Reset();
            }
        }

        public void HandlePalletsCard(int id, bool print, bool generateFile)
        {
            if (this.processing)
            {
                return;
            }

            this.processing = true;

            try
            {
                var stock = this.DataManager.GetDispatchPalletData(id);

                var palletStock = stock.StockDetails;

                ProgressBar.SetUp(0, palletStock.Count + 1);
                ProgressBar.Run();

                this.Log.LogInfo(this.GetType(),
                    string.Format("Exporting Labels: {0}", ApplicationSettings.LabelImagePath));
                if (print)
                {
                    foreach (var item in palletStock)
                    {
                        ProgressBar.Run();
                        var label = this.LabelManager.GetGs1PalletLabel("Pallet Card");
                        if (label != null)
                        {
                            var palletId = item.PalletID.ToInt();
                            this.PrintManager.ExportLabelImage(label, palletId, ApplicationSettings.LabelImagePath);

                            var reportParam = new List<ReportParameter>
                            {
                                new ReportParameter {Name = "DispatchID", Values = {id.ToString()}},
                                new ReportParameter {Name = "PalletID", Values = {palletId.ToString()}},
                                new ReportParameter {Name = "PalletNo", Values = {item.PalletNo.ToString()}}
                            };

                            var reportData = new ReportData
                            {
                                Name = ReportName.SubReportPalletsCard,
                                ReportParameters = reportParam
                            };

                            if (!ApplicationSettings.DontCreateCSVForPalletCard)
                            {
                                this.ReportManager.PrintReport(reportData);
                            }
                            else
                            {
                                this.ReportManager.PrintReport(reportData);
                                this.ReportManager.CreateReportForAttachment(reportData,
                                    ApplicationSettings.ExportReportPath, Constant.Pallet);
                            }
                        }
                    }
                }

                if (generateFile)
                {
                    this.DepartmentBodiesManager.ExportDispatchPalletData(stock);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                this.processing = false;
                ProgressBar.Reset();
            }
        }


        private void HandleReportEmail(ReportData data)
        {
            if (!data.Name.CompareIgnoringCase(ReportName.DunleavysDispatchDocket))
            {
                return;
            }

            if (this.processing)
            {
                return;
            }

            this.processing = true;

            try
            {
                var id = data.ReportParameters.First().Values.Cast<string>().First().ToInt();
                var stock = this.DataManager.GetDispatchPalletData(id);

                var palletStock = stock.StockDetails;

                ProgressBar.SetUp(0, palletStock.Count + 1);
                ProgressBar.Run();

                this.Log.LogInfo(this.GetType(),
                    string.Format("Exporting Labels: {0}", ApplicationSettings.LabelImagePath));
                if (true)
                {
                    foreach (var item in palletStock)
                    {
                        ProgressBar.Run();
                        var label = this.LabelManager.GetGs1PalletLabel("Pallet Card");
                        if (label != null)
                        {
                            var palletId = item.PalletID.ToInt();
                            this.PrintManager.ExportLabelImage(label, palletId, ApplicationSettings.LabelImagePath);

                            var reportParam = new List<ReportParameter>
                            {
                                new ReportParameter {Name = "DispatchID", Values = {id.ToString()}},
                                new ReportParameter {Name = "PalletID", Values = {palletId.ToString()}},
                                new ReportParameter {Name = "PalletNo", Values = {item.PalletNo.ToString()}}
                            };

                            var reportData = new ReportData
                            {
                                Name = ReportName.SubReportPalletsCard,
                                ReportParameters = reportParam
                            };

                            if (!ApplicationSettings.DontCreateCSVForPalletCard)
                            {
                                this.ReportManager.PrintReport(reportData);
                            }
                            else
                            {
                                this.ReportManager.PrintReport(reportData);
                                this.ReportManager.CreateReportForAttachment(reportData,
                                    ApplicationSettings.ExportReportPath, Constant.Pallet);
                            }
                        }
                    }
                }

                if (!ApplicationSettings.DontCreateCSVForPalletCard)
                {
                    this.DepartmentBodiesManager.ExportDispatchPalletData(stock);
                }
                else
                {
                    this.ReportManager.AttachReportsToEmail(ApplicationSettings.ExportReportPath);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                this.processing = false;
                ProgressBar.Reset();
            }
        }


        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearReportViewer();
            Messenger.Default.Send(Token.Message, Token.CloseReportViewer);
        }

        #endregion

        #endregion
    }
}





﻿// -----------------------------------------------------------------------
// <copyright file="ReportSearchViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Report
{
    using System;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;

    public class ReportSearchViewModel : ReportViewModelBase
    {
        #region field

        

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSearchViewModel"/> class.
        /// </summary>
        public ReportSearchViewModel(ReportData report)
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration


            #endregion

            #region command handler
            

            #endregion

            this.SetDefaultData(report);
            this.GetReports();
            this.SelectedReport = report;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSearchViewModel"/> class.
        /// </summary>
        public ReportSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration


            #endregion

            #region command handler


            #endregion
        }

        #endregion

        #region public interface

        #region property

        #endregion

        #region command


        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles a report selection.
        /// </summary>
        protected override void HandleSelectedReport()
        {
            this.ClearForm(false);
            this.SelectedReport.UserReportSettings = this.DataManager.GetUserReportSettings(this.SelectedReport, NouvemGlobal.UserId.ToInt());
            if (this.SelectedReport.UserReportSettings != null)
            {
                this.ShowAll = this.SelectedReport.UserReportSettings.ShowAll.ToBool();
                this.KeepVisible = this.SelectedReport.UserReportSettings.KeepVisible.ToBool();
                this.FromDate = this.SelectedReport.UserReportSettings.StartDate.HasValue ? this.SelectedReport.UserReportSettings.StartDate : DateTime.Today;
            }

            this.DisplayKeepVisible = this.SelectedReport.KeepVisible;
            this.MultiSelect = this.SelectedReport.MultiSelect;
            this.ViewMultiple = this.SelectedReport.ViewMultiple;
            this.DisplayRefresh = this.SelectedReport.Refresh;
            this.DisplayDates = this.SelectedReport.ShowDates;
            this.DisplayDateTypes = this.SelectedReport.ShowDateTypes;
            this.DisplaySearchID = this.SelectedReport.ShowSearchID;
            this.ShowCreateFile = this.SelectedReport.ShowCreateFile;
            this.DisplayShowAll = this.SelectedReport.ShowAll;
            this.SelectedSearchScreenMacro = this.SelectedReport.SearchGridMacro;
            this.SelectedReportMacro = this.SelectedReport.ReportMacro;

            if (this.DisplayDateTypes)
            {
                this.DateTypes.Clear();
                foreach (var reportDateType in this.ReportDateTypes)
                {
                    if (this.SelectedReport.ReportDateLookUps != null &&
                        this.SelectedReport.ReportDateLookUps.Any(x =>
                            x.NouSearchDateTypeID == reportDateType.NouSearchDateTypeID))
                    {
                        this.DateTypes.Add(reportDateType);
                    }
                }

                if (this.SelectedReport.ReportUserDateType != null)
                {
                    this.SelectedDateType = this.DateTypes.FirstOrDefault(x =>
                        x.NouSearchDateTypeID == this.SelectedReport.ReportUserDateType.ReportDateTypeID);
                }
                else
                {
                    this.SelectedDateType = this.DateTypes.FirstOrDefault();
                }
            }

            this.PopulateSearchGrid();
        }

        /// <summary>
        /// Override the cancellation to close locally.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.SaveReportDateType();
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            this.IsFormLoaded = false;
            Messenger.Default.Send(Token.Message, Token.CloseReportSearchWindow);
            Messenger.Default.Unregister(this);

            if (this.SelectedReport != null && this.SelectedReport.UserReportSettings != null)
            {
                this.SelectedReport.UserReportSettings.KeepVisible = this.KeepVisible;
                this.SelectedReport.UserReportSettings.ShowAll = this.ShowAll;
                this.SelectedReport.UserReportSettings.StartDate = this.FromDate;
                this.DataManager.SaveUserReportSettings(this.SelectedReport.UserReportSettings);
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.Close();
                    break;

                case ControlMode.Update:
                    this.UpdateReport();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Sets the default stored data.
        /// </summary>
        /// <param name="report">The report data.</param>
        protected void SetDefaultData(ReportData report)
        {
            if (!this.FromDate.HasValue)
            {
                this.FromDate = DateTime.Today;
            }

            if (!this.ToDate.HasValue)
            {
                this.ToDate = DateTime.Today;
            }
        }

        #endregion

        #region private

        #region command


        #endregion

        #region helper

        /// <summary>
        /// Clears the ui.
        /// </summary>
        private void ClearForm(bool clearReport = true)
        {
            
        }

        private void UpdateReport()
        {
            
        }

        #endregion

        #endregion
    }
}

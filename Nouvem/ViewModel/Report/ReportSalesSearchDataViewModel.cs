﻿// -----------------------------------------------------------------------
// <copyright file="ReportSalesSearchDataViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Report
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Shared;
    using Nouvem.ViewModel.Sales;

    public class ReportSalesSearchDataViewModel : ReportBaseViewModel
    {
        #region field

        private bool printReports;
        private bool generateFiles;

        /// <summary>
        /// The search from carcass date.
        /// </summary>
        private DateTime fromCarcassDate;

        /// <summary>
        /// The search to carcass date.
        /// </summary>
        private DateTime toCarcassDate;

        /// <summary>
        /// The groups.
        /// </summary>
        private ObservableCollection<ProductGroup> groups;

        /// <summary>
        /// The selected group.
        /// </summary>
        private ProductGroup selectedGroup;

        /// <summary>
        /// The batches to edit.
        /// </summary>
        private HashSet<Sale> salesToUpdate = new HashSet<Sale>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSalesSearchDataViewModel"/> class.
        /// </summary>
        public ReportSalesSearchDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.ShowReportNotesWindow, s => this.ShowNotesWindow());

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                if (this.SelectedSales != null && this.SelectedSales.Any())
                {
                    this.SelectedSales.First().TechnicalNotes = s;
                    this.SetControlMode(ControlMode.Update);
                    Messenger.Default.Send(Token.Message, Token.FocusToButton);

                    var localBatch = this.salesToUpdate.FirstOrDefault(x => x.SaleID == this.SelectedSales.First().SaleID);
                    if (localBatch != null)
                    {
                        this.salesToUpdate.Remove(localBatch);
                    }

                    this.salesToUpdate.Add(this.SelectedSales.First());
                }
            });

            #endregion

            #region command handler

            this.MoveBackCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow));

            this.FindCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.DisplaySearchKeyboard));

            this.ShowReportCommand = new RelayCommand(() => this.ShowReport());

            this.CarcassSaleSelectedCommand = new RelayCommand(this.CarcassSaleSelectedCommandExecute);

            #endregion

            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the groups.
        /// </summary>
        public bool GenerateFile
        {
            get
            {
                return this.generateFiles;
            }

            set
            {
                this.generateFiles = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the groups.
        /// </summary>
        public bool PrintReports
        {
            get
            {
                return this.printReports;
            }

            set
            {
                this.printReports = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the groups.
        /// </summary>
        public ProductGroup SelectedGroup
        {
            get
            {
                return this.selectedGroup;
            }

            set
            {
                this.selectedGroup = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    var localSales = this.allSales.Where(x => x.GroupId == value.ID).ToList();
                    this.FilteredSales = new ObservableCollection<Sale>(localSales);
                }
            }
        }

        /// <summary>
        /// Gets or sets the groups.
        /// </summary>
        public ObservableCollection<ProductGroup> Groups
        {
            get
            {
                return this.groups;
            }

            set
            {
                this.groups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromCarcassDate
        {
            get
            {
                return this.fromCarcassDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.fromCarcassDate = DateTime.Today;
                }
                else
                {
                    this.fromCarcassDate = value;
                }

                this.RaisePropertyChanged();
                ApplicationSettings.ReportsSearchFromDate = value;

                if (this.IsFormLoaded)
                {
                    this.HandleCarcassReportDateSearch();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToCarcassDate
        {
            get
            {
                return this.toCarcassDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.toCarcassDate = DateTime.Today;
                }
                else
                {
                    this.toCarcassDate = value;
                }

                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.HandleCarcassReportDateSearch();
                }
            }
        }

        #region property

        private Sale selectedReport;

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public Sale SelectedReport
        {
            get
            {
                return this.selectedReport;
            }

            set
            {
                this.selectedReport = value;
                this.RaisePropertyChanged();

                if (value == null)
                {
                    return;
                }

                if (this.IsFormLoaded && !this.EntitySelectionChange && this.runReport)
                {
                    this.SaleSelectedCommandExecute();
                }

                this.runReport = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are filtering by date.
        /// </summary>
        public bool DontFilterByDates { get; set; }

        #region method

        /// <summary>
        /// Sets the groups.
        /// </summary>
        /// <param name="sales"></param>
        public void SetGroups(IList<Sale> sales)
        {
            var localGroup = this.selectedGroup;
            this.Groups = new ObservableCollection<ProductGroup>();
            foreach (var sale in sales)
            {
                if (this.groups.Any(x => x.ID == sale.GroupId))
                {
                    continue;
                }

                this.Groups.Add(new ProductGroup { ID = sale.GroupId, GroupDescription = sale.GroupDescription });
            }

            if (localGroup != null)
            {
                var group = this.Groups.FirstOrDefault(x => x.ID == localGroup.ID);
                if (group != null)
                {
                    this.SelectedGroup = group;
                }
            }
        }

        #endregion

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommmand { get; private set; }

        /// <summary>
        /// Gets the command to search.
        /// </summary>
        public ICommand CarcassSaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to search.
        /// </summary>
        public ICommand FindCommand { get; private set; }

        /// <summary>
        /// Gets the command to close ui.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to close ui.
        /// </summary>
        public ICommand ShowReportCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Override, to display the touchscreen report viewer.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.isTouchscreenMode)
            {
                if (this.SelectedReport != null)
                {
                    Messenger.Default.Send(this.SelectedReport, Token.SearchSaleSelectedForTouchscreenReport);
                }
            }
            else
            {
                if (this.SelectedSales != null && this.SelectedSales.Any())
                {
                    if (this.SelectedSales.Count > 1)
                    {
                        this.SelectedSales.First().SaleIDs = string.Join(",", this.SelectedSales.Select(x => x.SaleID));
                    }

                    if (this.AssociatedView == ViewType.PalletsCard)
                    {
                        this.Locator.ReportViewer.HandlePalletsCard(this.SelectedSales.First().SaleID, this.PrintReports, this.GenerateFile);
                    }
                    if (this.AssociatedView == ViewType.PalletsCardMultiBatch)
                    {
                        this.Locator.ReportViewer.HandlePalletsCardMultiBatch(this.SelectedSales.First().SaleID, this.PrintReports, this.GenerateFile);
                    }
                    else
                    {
                        Messenger.Default.Send(this.SelectedSales.First(), Token.SearchSaleSelectedForReport);
                    }

                    this.Close();
                }
            }
        }

        /// <summary>
        /// Override, to show the selected document.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            if (this.SelectedSales == null || !this.SelectedSales.Any())
            {
                return;
            }

            if (this.AssociatedView == ViewType.Invoice)
            {
                if (this.SelectedSales != null && this.SelectedSales.Any())
                {
                    Messenger.Default.Send(ViewType.Invoice);
                    this.Locator.Invoice.Sale = this.DataManager.GetAPReceiptById(this.SelectedSales.First().SaleID);
                    SystemMessage.Write(MessageType.Priority, Message.InvoiceScreenLoaded);
                }
            }
            else if (this.AssociatedView == ViewType.ARDispatch)
            {
                try
                {
                    if (this.SelectedSales != null && this.SelectedSales.Any())
                    {
                        this.Locator.ARDispatch.DisplayModeOnly = true;
                        Messenger.Default.Send(ViewType.ARDispatch);
                        this.Locator.ARDispatch.Sale = this.DataManager.GetARDispatchById(this.SelectedSales.First().SaleID);
                        SystemMessage.Write(MessageType.Priority, Message.DispatchScreenLoaded);
                    }
                }
                finally
                {
                    this.Locator.ARDispatch.DisplayModeOnly = false;
                }
            }
        }

        protected override void Close()
        {
            Messenger.Default.Unregister(this);
            ViewModelLocator.ClearReportSalesSearchData();
            base.Close();
            this.ReportMode = true;
            this.runReport = false;
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            #region message deregistration

            //Messenger.Default.Unregister<string>(this);

            #endregion

            if (this.isTouchscreenMode)
            {
                ViewModelLocator.ClearReportSalesSearchData();
            }

            this.isTouchscreenMode = false;
            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.runReport = false;
            this.Close();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            //Messenger.Default.Register<List<Sale>>(this, Token.SendCarcassSales, this.HandleCarcassSales);

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    if (ApplicationSettings.CarcassSearchScreenLoaded)
                    {
                        this.HandleCarcassSales(null);
                    }
                    else
                    {
                        var print = s.CompareIgnoringCase(Constant.Print);
                        if (this.SelectedSales != null && this.SelectedSales.Any())
                        {
                            this.SelectedSales.First().Print = print;
                            this.SaleSelectedCommandExecute();
                        }
                    }
                }
            });

            this.FromDate = ApplicationSettings.ReportsSearchFromDate;
            this.ToDate = DateTime.Today;
            this.KeepVisible = ApplicationSettings.ReportSalesSearchDataKeepVisible;
            this.ShowAllOrders = ApplicationSettings.ReportSalesSearchShowAllOrders;

            this.SetControlMode(ControlMode.OK);
            this.DontFilterByDates = false;
            this.ReportMode = true;
            this.FromId = string.Empty;
            this.ToId = string.Empty;
            this.IsFormLoaded = true;
            this.runReport = false;
        }

        /// <summary>
        /// Handle the incoming sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        protected override void SetIncomingSales(IList<Sale> sales)
        {
            if (sales == null)
            {
                return;
            }

            //if (this.DontFilterByDates)
            // {
            this.FilteredSales =
              new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleID));
            // }
            //else
            //{
            //    this.FilteredSales =
            //      new ObservableCollection<Sale>(sales
            //      .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date)
            //      .OrderByDescending(x => x.SaleID));
            //}

            this.allSales = this.FilteredSales;
            this.HandleReportDateSearch();
            this.SelectedReport = null;
        }

        /// <summary>
        /// Refresh the sales.
        /// </summary>
        protected void Refresh()
        {
            var orderStatuses = new List<int?>
            {
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
            };

            this.allSales = this.DataManager.GetAllAPReceiptsForReport(orderStatuses);
            this.HandleReportDateSearch();
            //if (this.ShowAllOrders)
            //{
            //    this.FilteredSales = new ObservableCollection<Sale>(this.allSales);
            //    return;
            //}

            //this.FilteredSales =
            //   new ObservableCollection<Sale>(this.allSales
            //       .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.ReportsSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.ReportsSearchToDate.Date)
            //       .OrderByDescending(x => x.SaleID));
        }


        /// <summary>
        /// Handler for the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    if (this.FromId.ToInt() > 0 && this.ToId.ToInt() > 0)
                    {
                        this.ShowReport(false);
                    }
                    else if (ApplicationSettings.GenerateDispatchFile)
                    {
                        this.GenerateDispatchFiles();
                    }

                    break;

                case ControlMode.Update:
                    if (this.salesToUpdate.Any())
                    {
                        if (this.DataManager.UpdateIntakes(this.salesToUpdate, ViewType.APReceipt))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                            this.salesToUpdate.Clear();
                            this.Refresh();
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                        }
                    }

                    break;
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Exports the dispatch stock data.
        /// </summary>
        private void GenerateDispatchFiles()
        {
            var dispatches = this.DataManager.GetDispatchStock(this.SelectedSales);
            try
            {
                this.DepartmentBodiesManager.ExportDispatchData(dispatches);
                SystemMessage.Write(MessageType.Priority, Message.DataExported);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowNotesWindow()
        {
            if (this.SelectedSales == null || !this.SelectedSales.Any() || this.SelectedSales.First().SaleID == 0)
            {
                return;
            }

            //var notes = this.SelectedSales.First().TechnicalNotes ?? string.Empty;
            var notes = string.Empty;
            Messenger.Default.Send(notes, Token.DisplayNotes);
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        private void CarcassSaleSelectedCommandExecute()
        {
            if (this.SelectedSales != null && this.SelectedSales.Any())
            {
                this.HandleCarcassSales(this.SelectedSales.First());
            }
        }

        /// <summary>
        /// Handle the carcass selections.
        /// </summary>
        /// <param name="carcassSales">The filtered carcass selections.</param>
        private void HandleCarcassSales(Sale carcassSale)
        {
            var sale = new Sale();
            sale.Breed = carcassSale == null ? "0" : carcassSale.KillNumber.ToString();
            sale.CreationDate = this.ToCarcassDate;
            sale.GroupId = this.selectedGroup == null ? 0 : this.selectedGroup.ID;

            Messenger.Default.Send(sale, Token.SearchSaleSelectedForReport);
        }

        /// <summary>
        /// Handle a date change.
        /// </summary>
        private void HandleCarcassReportDateSearch()
        {
            this.EntitySelectionChange = true;
            if (this.SelectedGroup == null)
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetCarcasses(this.fromCarcassDate, this.toCarcassDate));
                this.allSales = this.FilteredSales;
            }
            else
            {
                this.allSales = this.DataManager.GetCarcasses(this.fromCarcassDate, this.toCarcassDate);
                var localSales = this.allSales.Where(x => x.GroupId == this.selectedGroup.ID).ToList();
                this.FilteredSales = new ObservableCollection<Sale>(localSales);
            }

            this.SetGroups(this.allSales);
            this.EntitySelectionChange = false;
        }

        #endregion
    }

    public class ProductGroup
    {
        public long ID { get; set; }
        public string GroupDescription { get; set; }
    }
}



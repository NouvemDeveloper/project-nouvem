﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using Microsoft.Reporting.WinForms;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using System.Windows.Threading;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class APReceiptViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The pallet no.
        /// </summary>
        private string palletNumber;

        /// <summary>
        /// The current batch number.
        /// </summary>
        private BatchNumber batchNumber;

        /// <summary>
        /// The suppliers collection.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> suppliers;

        /// <summary>
        /// The incoming product quantity.
        /// </summary>
        private decimal productQuantity;

        /// <summary>
        /// Flag, as to whether the mode is to be changed.
        /// </summary>
        private bool dontChangeMode;

        /// <summary>
        /// The batch reference.
        /// </summary>
        private string reference;

        /// <summary>
        /// Timer used to determine a manual serial entry operation.
        /// </summary>
        private readonly DispatcherTimer timer = new DispatcherTimer();

        protected bool scanProcessing;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APReceiptViewModel"/> class.
        /// </summary>
        public APReceiptViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Refresh the current sale when a price list is updated.
            Messenger.Default.Register<string>(this, Token.RefreshMasterDocument, s =>
            {
                this.DataManager.PriceIntakeDocket(this.Sale.SaleID);
                this.Sale = this.DataManager.GetAPReceiptFullById(this.Sale.SaleID);
            });


            Messenger.Default.Register<string>(this, Token.ClearSelectedProduct, s =>
            {
                this.SelectedSaleDetail = null;
            });

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            // Register for the serial stock selection flag.
            Messenger.Default.Register<string>(this, Token.SerialStockSelected, s => this.ProductQuantity = 1);

            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.APReceipt)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID, InventoryItem = product, NouStockMode = product.StockMode };
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.APReceipt)
                    {
                        this.SelectedPartner =
                            this.Suppliers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            // Register to handle the incoming goods in details (traceability data, and total weight).
            // Messenger.Default.Register<SaleDetail>(this, Token.UpdateGoodsInQtyReceived, this.HandleGoodsInData);

            Messenger.Default.Register<decimal>(this, Token.UpdateWeight, d =>
            {
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.WeightReceived += d;
                }
            });

            #endregion

            #region command handler

            this.DrillDownToDetailsCommand = new RelayCommand(this.DrillDownToDetailsCommandExecute);

            // Handle the attempt to remove the selected product.
            this.RemoveProductCommand = new RelayCommand(this.RemoveProductCommandExecute);

            // Handle the mode button touch down, starting the timer.
            this.ModeButtonTouchDownCommand = new RelayCommand(() =>
            {
                this.dontChangeMode = false;
                this.timer.Start();
            });

            // Handle the mode button touch up, starting the timer.
            this.ModeButtonTouchUpCommand = new RelayCommand(() =>
            {
                this.timer.Stop();
                if (!this.dontChangeMode)
                {
                    // this.DispatchModeCommandExecute();
                }
            });

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(true, ViewType.APReceipt)),
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() => this.RemoveItem(ViewType.APReceipt));

            #endregion

            this.GetLocalDocNumberings();
            this.SetControlMode(ControlMode.OK);
            this.DocketNoteName = Strings.IntakeDocketNote;
            this.SetUpTimer();
            this.GetAttributeAllocationData();
            this.GetAttributeLookUps();
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or set the pallet no.
        /// </summary>
        public string PalletNumber
        {
            get
            {
                return this.palletNumber;
            }

            set
            {
                this.palletNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber
        {
            get
            {
                return this.batchNumber;
            }

            set
            {
                this.batchNumber = value;
                this.RaisePropertyChanged();
                this.SetUIBatches(value);

                Messenger.Default.Send(value, Token.BatchNumberSelected);
            }
        }

        /// <summary>
        /// Gets a value determining if a product label can be printed.
        /// </summary>
        public bool PrintProductLabel
        {
            get { return !ApplicationSettings.DisablePrintingAtIntake && (this.SelectedSaleDetail == null || !this.SelectedSaleDetail.DontPrintLabel); }
        }

        /// <summary>
        /// Gets or sets the stock import 
        /// </summary>
        public int? StocktransactionId { get; set; }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public string Reference
        {
            get
            {
                return this.reference;
            }

            set
            {
                this.reference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the product qty selected.
        /// </summary>
        public decimal ProductQuantity
        {
            get
            {
                return this.productQuantity;
            }

            set
            {
                this.productQuantity = value;
                Messenger.Default.Send(value, Token.QuantitySelected);
            }
        }

        #endregion

        #region command


        /// <summary>
        /// Gets the command to remove the selected product.
        /// </summary>
        public ICommand DrillDownToDetailsCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove the selected product.
        /// </summary>
        public ICommand RemoveProductCommand { get; private set; }

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        /// <summary>
        /// Gets the command to edit the transactions.
        /// </summary>
        public ICommand EditTransactionsCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Show the sales.
        /// </summary>
        public void ShowAllSales()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Refreshes the sale.
        /// </summary>
        public void UpdateSaleData()
        {
            this.UpdateSale();
        }

        #endregion

        #endregion

        #region protected

        #region virtual

        /// <summary>
        /// Sets the ui batch(s) id to display.
        /// </summary>
        /// <param name="value">The current batch.</param>
        protected virtual void SetUIBatches(BatchNumber value) { }

        #endregion

        #region override

        /// <summary>
        /// Adds an iactive customer if retrieved on historic docket.
        /// </summary>
        /// <param name="partnerId"></param>
        protected override void AddInactiveSelectedPartner(int partnerId)
        {
            if (!this.Suppliers.Any(x => x.BPMasterID == partnerId))
            {
                var localPartner = NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == partnerId);
                if (localPartner != null)
                {
                    this.Suppliers.Add(localPartner.Details);
                }
            }
        }

        /// <summary>
        /// Gets the live price data when using recent orders.
        /// </summary>
        /// <param name="saleDetail">The recent order data.</param>
        protected override void GetLiveRecentOrderPrice(SaleDetail saleDetail)
        {
            if (this.SelectedPartner == null || saleDetail == null)
            {
                return;
            }

            var data = this.DataManager.GetPriceListDetailByPartner(this.SelectedPartner.BPMasterID, saleDetail.INMasterID, NouvemGlobal.CostPriceList.PriceListID);
            if (data != null)
            {
                saleDetail.LoadingSale = true;
                saleDetail.UnitPrice = data.Price;
                saleDetail.PriceListID = data.PriceListID;
                saleDetail.LoadingSale = false;
            }
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            this.Close();
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.DataManager.PriceIntakeDocket(searchSale.SaleID);
                this.Sale = this.DataManager.GetAPReceiptFullById(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetAPReceiptFullByLastEdit();
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                if (this.Sale.BaseDocumentReferenceID.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                    return;
                }

                this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Updates the selected sale detail totals.
        /// </summary>
        protected override void UpdateSaleDetailTotals()
        {
            if (this.SelectedSaleDetail != null)
            {
                this.SelectedSaleDetail.UpdateTotal(true);
            }
        }

        /// <summary>
        /// Get the local document status types.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems.ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            Task.Factory.StartNew(this.GetAllSales);
            base.OnLoadingCommandExecute();
            this.ScannerStockMode = ScannerMode.Scan;
            this.ClearForm();
            this.SetControlMode(ControlMode.Find);
        }

        /// <summary>
        /// Creates a sale order object.
        /// </summary>
        protected override void CreateSale()
        {
            // header
            this.Sale.QuoteValidDate = this.ValidUntilDate;
            this.Sale.DeliveryDate = this.DeliveryDate;
            this.Sale.DocumentDate = this.DocumentDate;
            this.Sale.DeliveryTime = this.DeliveryTime;
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.Remarks = this.Remarks;
            this.Sale.DeviceId = NouvemGlobal.DeviceId;
            this.Sale.EditDate = DateTime.Now;
            this.Sale.BPContactID = this.SelectedContact != null ? (int?)this.SelectedContact.Details.BPContactID : null;
            this.Sale.DocketNote = this.DocketNote;
            this.Sale.InvoiceNote = this.InvoiceNote;
            this.Sale.PopUpNote = this.PopUpNote;
            this.Sale.SalesEmployeeID = this.SelectedSalesEmployee != null ? (int?)this.SelectedSalesEmployee.UserMaster.UserMasterID : null;
            this.Sale.VAT = this.Tax;
            this.Sale.SubTotalExVAT = this.SubTotal;
            this.Sale.DiscountIncVAT = this.Discount;
            this.Sale.GrandTotalIncVAT = this.Total;
            this.Sale.DiscountPercentage = this.DiscountPercentage;
            this.Sale.Customer = this.SelectedPartner;
            this.Sale.DeliveryAddress = this.SelectedDeliveryAddress;
            this.Sale.InvoiceAddress = this.SelectedInvoiceAddress;
            this.Sale.DeliveryContact = this.SelectedDeliveryContact;
            this.Sale.MainContact = this.SelectedContact;
            this.Sale.PORequired = this.PORequired;
            this.Sale.Reference = this.Attribute30;

            if (this.NonStandardResponses != null && this.NonStandardResponses.Any())
            {
                this.Sale.NonStandardResponses = new List<Model.DataLayer.Attribute>();
                foreach (var attributeAllocationData in this.NonStandardResponses)
                {
                    this.Sale.NonStandardResponses.Add(new Model.DataLayer.Attribute
                    {
                        Attribute1 = attributeAllocationData.TraceabilityValueNonStandard,
                        AttributeMasterID = attributeAllocationData.AttributeMaster.AttributeMasterID
                    });
                }
            }

            // add the sale details
            this.Sale.SaleDetails = this.SaleDetails;
        }

        /// <summary>
        /// Override the selected purchase order, resetting it's totals.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localCopyValue = CopyingDocument;
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;

            if (this.Sale.SaleDetails != null)
            {
                this.DataManager.GetTransactionData(this.Sale.SaleDetails);
            }

            //this.SetControlMode(ControlMode.OK);

            if (!localCopyValue && !this.Sale.CopyingFromOrder)
            {
                // We only reset the totals when we are copying from an order.
                return;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.TotalExVAT = 0;
            }

            this.Tax = 0;
            this.Total = 0;
            this.SubTotal = 0;

            // we need to keep the discount % (Setting discount to 0 will reset it to 0)
            this.CalculateSalesTotals = false;
            this.Discount = 0;
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected override void UpdateSaleOrderTotals(bool calcDisAmountOnly = true, bool calcDisPercentOnly = false)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            if (this.SaleDetails == null)
            {
                return;
            }

            try
            {
                // reset the totals/taxes
                this.SubTotal = 0;
                this.Total = 0;
                this.Tax = 0;

                // get the accumulated totals and taxes.
                this.SaleDetails.ToList().ForEach(item =>
                {
                    this.SubTotal += item.TotalExVAT.ToDecimal();
                    this.Total += item.TotalIncVAT.ToDecimal();
                });

                // Set the tax value.
                if (this.Total > this.SubTotal)
                {
                    this.Tax = this.Total - this.SubTotal;
                }

                if (calcDisPercentOnly)
                {
                    this.CalculateSalesTotals = false;

                    // apply the discount (if any)
                    if (this.DiscountPercentage > 0)
                    {
                        var convertedDiscount = 100M - this.DiscountPercentage.ToDecimal();
                        this.Tax = Math.Round(this.Tax / 100 * convertedDiscount, 2);
                        var discountedTotal = Math.Round(this.Total / 100 * convertedDiscount, 2);

                        // convert the discount amount.
                        this.Discount = this.Total - discountedTotal;

                        // assign the discounted total.
                        this.Total = discountedTotal;
                    }
                    else
                    {
                        this.Discount = 0;
                    }
                }

                if (calcDisAmountOnly)
                {
                    this.CalculateSalesTotals = false;

                    // apply the discount amount (if any)
                    if (this.Discount > 0)
                    {
                        if (this.SubTotal > 0)
                        {
                            this.DiscountPercentage = Math.Round((this.Discount / this.SubTotal) * 100, 2).ToDecimal();
                            this.Total = this.Total - this.Discount;
                        }
                    }
                    else
                    {
                        if (!this.LockDiscountPercentage)
                        {
                            this.DiscountPercentage = 0;
                        }
                        else
                        {
                            this.UpdateSaleOrderTotals(false, true);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected void UpdateSaleOrderTotals(Sale sale)
        {
            // reset the totals/taxes
            this.SubTotal = 0;
            this.Total = 0;
            this.Tax = 0;

            if (sale.SaleDetails == null)
            {
                return;
            }

            // get the accumulated totals and taxes.
            sale.SaleDetails.ToList().ForEach(item =>
            {
                this.SubTotal += item.TotalExVAT.ToDecimal();
                this.Total += item.TotalIncVAT.ToDecimal();
            });
        }

        public void NewBatchGenerationHandler()
        {
            this.DrillDownCommandExecute();
        }

        /// <summary>
        /// Drill down to the selected item details.
        /// </summary>
        protected virtual void DrillDownToDetailsCommandExecute()
        {
            var localSaleDetail = this.SelectedSaleDetail;
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            Messenger.Default.Send(ViewType.APReceiptDetails);
            localSaleDetail.DocketNo = this.Sale.Number;
            this.Locator.APReceiptDetails.SelectedSaleDetail = localSaleDetail;
            this.Locator.APReceiptDetails.IsReadOnly = this.IsReadOnly;
        }

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                this.ShowSearchGridCommandExecute(Constant.GoodsIn);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                var localSales = this.DataManager.GetAllAPReceipts(orderStatuses);
                this.CustomerSales = localSales
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllAPReceipts(orderStatuses)
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.APReceipt), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.APReceipt);
            }
        }

        /// <summary>
        /// Override, to set the selected detail flag.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            base.HandleSelectedSaleDetail();

            // TODO when search for the sales, the price method is not set (no selected supplier) this works, but need to fix properly.
            this.SelectedSaleDetail.UpdateTotals = false;
            this.SelectedSaleDetail.INMasterID = this.SelectedSaleDetail.INMasterID;

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            if (this.SelectedPartner == null)
            {
                return;
            }

            //var supplier =
            //    NouvemGlobal.SupplierPartners.FirstOrDefault(
            //        x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);

            var supplier = this.DataManager.GetBusinessPartner(this.SelectedPartner.BPMasterID);

            if (supplier != null)
            {
                if (!ApplicationSettings.TouchScreenMode)
                {
                    supplier.Contacts = this.DataManager.GetBusinessPartnerContactsById(supplier.Details.BPMasterID);
                }
                
                this.PartnerCurrency =
                    supplier.PartnerCurrency != null && !string.IsNullOrEmpty(supplier.PartnerCurrency.Symbol)
                        ? supplier.PartnerCurrency.Symbol
                        : ApplicationSettings.DefaultCurrency;

                if (supplier.Contacts != null)
                {
                    this.Contacts = new ObservableCollection<BusinessPartnerContact>(supplier.Contacts);
                    var primaryContact = supplier.Contacts.FirstOrDefault(x => x.Details.PrimaryContact == true);

                    if (primaryContact != null)
                    {
                        this.SelectedContact = primaryContact;
                    }
                }

                if (supplier.Addresses != null)
                {
                    this.Addresses = new ObservableCollection<BusinessPartnerAddress>(supplier.Addresses);
                    var billingAddress = supplier.Addresses.FirstOrDefault(x => x.Details.Billing == true);
                    var deliveryAddress = supplier.Addresses.FirstOrDefault(x => x.Details.Shipping == true);

                    if (billingAddress != null)
                    {
                        this.SelectedInvoiceAddress = billingAddress;
                    }

                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                    }
                }

                this.SelectedPartner.PopUpNotes = supplier.Details.PopUpNotes;
                this.CustomerPopUpNote = supplier.Details.PopUpNotes;

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.ShowPartnerPopUpNoteAtDesktopModules && !this.DisablePartnerPopUp)
                {
                    if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                            //this.CustomerPopUpNote = string.Empty;
                        }));
                    }
                }
            }

            if (ApplicationSettings.TouchScreenMode)
            {
                return;
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetRecentPurchaseReceiptOrders(localPartner.BPMasterID);
                this.RecentOrdersCount = 0;
                this.SetRecentOrders(true);
            }));
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    if (this.Sale != null)
                    {
                        this.Sale.CopyingFromOrder = true;
                    }

                    this.AddTransaction();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateOrder();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.RefreshSales();
            }));
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Print the intake docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
                                  //|| this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(79, this.Sale.SaleID.ToString(), !preview);
            return;

            var intakeId = this.Sale.SaleID;
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { intakeId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.IntakeDocket, ReportParameters = reportParam };

            try
            {
                if (preview)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        public void AddTransaction()
        {
            this.Log.LogDebug(this.GetType(), "AddTransaction(): Adding a transaction");

            // ensure the price list data is set (this relates to the touchscreen only)
            if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.PriceListID == 0)
            {
                this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
            }

            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }
            else if (!string.IsNullOrEmpty(this.DeliveryTime) && !this.DeliveryTime.IsValidTime())
            {
                error = Message.InvalidTime;
            }

            if (error != string.Empty)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: true);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, error);
                }

                return;
            }

            #endregion

            this.CreateSale();

            try
            {
                if (this.Sale.SaleID == 0)
                {
                    this.AddSale();
                }
                else
                {
                    this.UpdateSale();
                }
            }
            finally
            {
                this.StocktransactionId = null;
                //if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
                //{
                //    this.AddProductionBatch();
                //}

                this.GetLiveStatus();
                if (ApplicationSettings.ResetQtyToZeroAtIntake && ApplicationSettings.TouchScreenMode)
                {
                    this.Locator.Touchscreen.Quantity = 0;
                }
            }
        }

        /// <summary>
        /// Gets the current orders live status.
        /// </summary>
        /// <returns>The current orders live status.</returns>
        protected virtual void GetLiveStatus()
        {
            if (this.Sale == null)
            {
                return;
            }

            var orderDetails = this.DataManager.GetIntakeStatusDetails(this.Sale.SaleID);
            if (orderDetails == null)
            {
                return;
            }

            foreach (var detail in this.SaleDetails)
            {
                detail.QuantityDelivered = 0;
                detail.WeightDelivered = 0;
            }

            var order = orderDetails.FirstOrDefault();
            if (order == null)
            {
                return;
            }

            foreach (var detail in orderDetails)
            {
                var localSaleDetail = this.SaleDetails.FirstOrDefault(x => x.SaleDetailID == detail.APGoodsReceiptDetailID);
                if (localSaleDetail != null)
                {
                    localSaleDetail.LoadingSale = true;
                    localSaleDetail.QuantityOrdered = detail.QuantityOrdered.ToDecimal();
                    localSaleDetail.WeightOrdered = detail.WeightOrdered.ToDecimal();
                    localSaleDetail.UnitPrice = detail.UnitPrice;
                    localSaleDetail.UnitPriceAfterDiscount = detail.UnitPrice;
                    localSaleDetail.QuantityReceived = detail.Qty.ToDecimal();
                    localSaleDetail.WeightReceived = detail.Wgt.ToDecimal();
                    if (detail.PriceListID.HasValue)
                    {
                        localSaleDetail.PriceListID = detail.PriceListID.ToInt();
                    }
                }
                else
                {
                    var newLine = new SaleDetail
                    {
                        LoadingSale = true,
                        SaleDetailID = detail.APGoodsReceiptDetailID,
                        SaleID = detail.APGoodsReceiptID,
                        INMasterID = detail.INMasterID,
                        QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                        WeightOrdered = detail.WeightOrdered.ToDecimal(),
                        QuantityReceived = detail.Qty.ToDecimal(),
                        WeightReceived = detail.Wgt.ToDecimal(),
                        UnitPrice = detail.UnitPrice,
                        UnitPriceAfterDiscount = detail.UnitPrice,
                        InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID)
                    };

                    if (detail.PriceListID.HasValue)
                    {
                        newLine.PriceListID = detail.PriceListID.ToInt();
                    }

                    this.SaleDetails.Add(newLine);
                }
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The newly created stock transaction id.</param>
        protected void Print(int stockTransactionId, LabelType labelType = LabelType.Item, bool isBatch = false, 
            Process process = null, bool batchLabel = false)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Print(): Attempting to print Stocktransactionid:{0}", stockTransactionId));

            #region validation

            if (this.scanProcessing)
            {
                return;
            }

            if (stockTransactionId == 0)
            {
                return;
            }

            #endregion

            var customerId = this.Sale.BPCustomer != null ? this.Sale.BPCustomer.BPMasterID : (int?)null;
            var customerGroupId = this.Sale.BPCustomer != null ? this.Sale.BPCustomer.BPGroupID : (int?)null;
            int? productId = null;
            int? productGroupId = null;

            var localSaleDetail = this.Sale.SaleDetails.FirstOrDefault(x => x.IsSelectedSaleDetail);
            if (localSaleDetail != null && localSaleDetail.InventoryItem != null)
            {
                productId = localSaleDetail.INMasterID;
                productGroupId = localSaleDetail.INGroupID;
            }

            if (isBatch)
            {
                customerId = null;
                customerGroupId = null;
                productId = null;
                productGroupId = null;
            }

            try
            {
                var labels = this.PrintManager.ProcessPrinting(customerId, customerGroupId, productId, productGroupId, ViewType.APReceipt, labelType, stockTransactionId, ApplicationSettings.GoodsInLabelsToPrint, process,batchLabel:batchLabel);
                if (labels.Item1.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.DataManager.AddLabelToTransaction(stockTransactionId,
                            string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                    }), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Gets the local module.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetModule()
        {
            return Constant.Intake;
        }

        /// <summary>
        /// Gets the local module.
        /// </summary>
        /// <returns></returns>
        protected virtual int GetTransactionId()
        {
            return NouvemGlobal.TransactionTypeGoodsReceiptId;
        }

        /// <summary>
        /// Completes a transaction, check the orders live status.
        /// </summary>
        /// <param name="wgt">The weight to apply.</param>
        protected void CompleteTransaction(decimal wgt)
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            try
            {
                if (!this.ValidateData(
                    this.traceabilityDataTransactions,
                    this.Sale.SaleID,
                    this.indicatorWeight, this.SelectedSaleDetail.StockDetailToProcess.TransactionQty.ToDecimal(),
                    this.SelectedSaleDetail.StockDetailToProcess.INMasterID,
                    this.SelectedSaleDetail.StockDetailToProcess.WarehouseID,
                    this.SelectedSaleDetail.StockDetailToProcess.ProcessID.ToInt(),
                    this.GetModule()))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            this.SelectedSaleDetail.WeightReceived = this.SelectedSaleDetail.WeightReceived.ToDecimal();
            this.SelectedSaleDetail.QuantityReceived = this.SelectedSaleDetail.QuantityReceived.ToDecimal();

            #region live update

            Sale liveUpdate = null;

            if (ApplicationSettings.GetLiveOrderStatusIntake)
            {
                liveUpdate = this.GetLiveOrderStatus();
            }

            if (liveUpdate != null)
            {
                this.Sale.NouDocStatusID = liveUpdate.NouDocStatusID;
                if (liveUpdate.OrderStatus == OrderStatus.Complete)
                {
                    var deviceName = liveUpdate.Device != null ? liveUpdate.Device.DeviceName : string.Empty;
                    this.Log.LogDebug(this.GetType(), string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));
                    NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), touchScreen: true);
                    return;
                }

                if (liveUpdate.OrderStatus == OrderStatus.InProgress || liveUpdate.OrderStatus == OrderStatus.Filled)
                {
                    this.Log.LogDebug(this.GetType(), "Order in progress");

                    if (liveUpdate.SaleDetails.Any())
                    {
                        var matchSaleDetail =
                            liveUpdate.SaleDetails.FirstOrDefault(
                                x => x.SaleDetailID == this.SelectedSaleDetail.SaleDetailID);

                        if (matchSaleDetail != null)
                        {
                            this.Log.LogDebug(this.GetType(), "Checking if received amounts have been updated at another terminal");

                            if (matchSaleDetail.WeightReceived.ToDouble() > this.SelectedSaleDetail.WeightReceived.ToDouble()
                                || matchSaleDetail.QuantityReceived.ToDouble() > this.SelectedSaleDetail.QuantityReceived.ToDouble())
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("Updating Qty:{0}, Wgt:{1}", matchSaleDetail.QuantityReceived, matchSaleDetail.WeightReceived));

                                this.SelectedSaleDetail.WeightReceived = matchSaleDetail.WeightReceived;
                                this.SelectedSaleDetail.QuantityReceived = matchSaleDetail.QuantityReceived;

                                SystemMessage.Write(MessageType.Priority, Message.OrderItemUpdatedElsewhere);

                                if (ApplicationSettings.DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox)
                                {
                                    NouvemMessageBox.Show(Message.OrderItemUpdatedElsewhere, touchScreen: true);
                                }
                            }
                        }
                    }
                }
            }

            #endregion

            var localWgt = wgt;
            var localQty = this.ProductQuantity <= 0 ? 1 : this.ProductQuantity;
            this.SelectedSaleDetail.WeightReceived += localWgt;
            this.SelectedSaleDetail.QuantityReceived += localQty;
            this.ProductQuantity = 0;
            this.AddTransaction();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateIntakes)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen:true);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.Log.LogDebug(this.GetType(), "AddSale(): adding a new order");
            this.UpdateDocNumbering();

            var details = this.Sale.SaleDetails;
            if (details != null && details.Any())
            {
                if (!details.First().BatchNumberId.HasValue && ApplicationSettings.IntakeMode != IntakeMode.IntakeMultiBatch)
                {
                    var batch = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                    foreach (var saleDetail in details)
                    {
                        saleDetail.Batch = batch;
                    }
                }
            }

            var receipt = this.DataManager.AddAPReceipt(this.Sale);
            var newSaleId = receipt.Item1;
            var transactionId = receipt.Item2;
            var localSaleDetail = this.SelectedSaleDetail;

            if (newSaleId > 0)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    var message = string.Format(Message.IntakeWeightRecorded, this.SelectedSaleDetail.StockDetailToProcess.TransactionWeight, this.SelectedSaleDetail.InventoryItem.Name);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }));
                }

                if (this.NonStandardResponses.Any() || this.NonStandardBatchResponses.Any())
                {
                    foreach (var response in this.NonStandardResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: newSaleId);
                    }

                    foreach (var response in this.NonStandardBatchResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: newSaleId);
                    }
                }

                this.NonStandardResponses.Clear();
                this.Sale.NonStandardResponses = null;
                SystemMessage.Write(MessageType.Priority, string.Format(Message.GoodsInCreated, this.NextNumber));

                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataPostTransactions,
                        transactionId, 0, 0, 0, 0, 0, Constant.Intake))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                if (this.PrintProductLabel)
                {
                    this.Print(transactionId);
                }

                this.Sale.SaleID = newSaleId;
                this.AllSales.Add(this.Sale);
                this.RefreshGoodsIn();
                this.PalletNumber = string.Empty;
                this.SetOrderLinesStatus();
                if (!ApplicationSettings.TouchScreenMode)
                {
                    this.ClearForm();
                    this.SetControlMode(ControlMode.Find);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.GoodsInNotCreated);
            }
        }

        /// <summary>
        /// Cancel the current order.
        /// </summary>
        protected void CancelOrder()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            if (this.DataManager.CompleteOrCancelAPReceipt(this.Sale, false))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCancelled, this.Sale.Number));
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.OrderNotCancelled);
            }
        }

        /// <summary>
        /// Completes the current order.
        /// </summary>
        protected virtual void CompleteOrder()
        {
            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoPartnerSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.EmptyOrder;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            if (this.DataManager.CompleteOrCancelAPReceipt(this.Sale))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                this.ClearForm();
                this.PartnerName = Strings.SelectSupplier;
                this.Reference = string.Empty;
                this.Locator.APReceiptDetails.BatchNumber = null;
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
                Messenger.Default.Send(Token.Message, Token.ClearData);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
            }
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateIntakes)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            string error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            //else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            //{
            //    error = Message.NoDocStatusSelected;
            //}
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var localSaleDetail = this.SelectedSaleDetail;
            var result = this.DataManager.UpdateAPReceipt(this.Sale);
            if (result)
            {
                if (this.NonStandardResponses.Any() || this.NonStandardBatchResponses.Any())
                {
                    foreach (var response in this.NonStandardResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: this.Sale.SaleID);
                    }

                    foreach (var response in this.NonStandardBatchResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: this.Sale.SaleID);
                    }
                }

                this.NonStandardResponses.Clear();
                this.Sale.NonStandardResponses = null;
                this.PalletNumber = string.Empty;
                SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                if (ApplicationSettings.TouchScreenMode)
                {
                    var message = string.Format(Message.IntakeWeightRecorded, this.SelectedSaleDetail.StockDetailToProcess.TransactionWeight, this.SelectedSaleDetail.InventoryItem.Name);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }));
                }

                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataPostTransactions,
                        this.Sale.ReturnedStockTransactionId, 0, 0, 0, 0, 0, Constant.Intake))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                if (this.PrintProductLabel)
                {
                    this.Print(this.Sale.ReturnedStockTransactionId);
                }

                this.SetOrderLinesStatus();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.GoodsInNotCreated, this.NextNumber));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.APDelivery)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearAPReceipt();
            Messenger.Default.Send(Token.Message, Token.CloseAPReceiptWindow);
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoQuoteSelected);
                return;
            }

            #endregion

            if (this.SelectedDocumentCopyTo.Equals(Strings.POOrder))
            {
                CopyingDocument = true;
                //this.CopyQuoteToOrder();
            }
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var documents = this.DataManager.GetAPOrders(this.SelectedPartner.BPMasterID);

            if (!documents.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Orders));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(documents, ViewType.APReceipt), Token.SearchForSale);
            CopyingDocument = true;
            this.LockDiscountPercentage = true;
        }

        #endregion

        #region method
       
        /// <summary>
        /// Gets the current orders live status.
        /// </summary>
        /// <returns>The current orders live status.</returns>
        protected Sale GetLiveOrderStatus()
        {
            return this.DataManager.GetAPReceiptById(this.Sale.SaleID);
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        protected virtual void RemoveProductCommandExecute()
        {
            if (this.SelectedSaleDetail != null)
            {
                // If there are no transactions recorded against this product, remove.
                if (this.SelectedSaleDetail.QuantityReceived.ToDouble() > 0 || this.SelectedSaleDetail.WeightDelivered.ToDouble() > 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                }
                else
                {
                    // Deleting product
                    if (this.RemoveItem(ViewType.APReceipt))
                    {
                        SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                    }
                }
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Adds a prorder batch.
        /// </summary>
        private void AddProductionBatch()
        {
            if (this.BatchNumber == null)
            {
                this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                this.CheckAttributeBatchChangeReset();
            }

            if (this.Sale.ProductionBatchNumber != null &&
                this.Sale.ProductionBatchNumber.BatchNumberID == this.BatchNumber.BatchNumberID)
            {
                return;
            }

            var data = new ProductionData {BatchNumber = this.BatchNumber};
            data.Order = new PROrder
            {
                DocumentNumberingID = this.SelectedDocNumbering.DocumentNumberingID,
                NouDocStatusID = this.SelectedDocStatus.NouDocStatusID,
                ScheduledDate = DateTime.Now,
                Reference = data.BatchNumber.Number,
                CreationDate = DateTime.Now,
                EditDate = DateTime.Now,
                PRSpecID = 1,
                BPMasterID = this.SelectedCustomer != null ? this.SelectedCustomer.BPMasterID : (int?)null,
                BPContactID = this.SelectedContact != null ? this.SelectedContact.Details.BPContactID : (int?)null,
                UserID = NouvemGlobal.UserId.ToInt(),
                DeviceID = NouvemGlobal.DeviceId.ToInt(),
                BatchNumberID = data.BatchNumber.BatchNumberID
            };

            this.DataManager.AddUniqueProductionOrder(data);
            this.Sale.ProductionBatchNumber = data.BatchNumber;
        }

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.APReceipt)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                var localSales = this.DataManager.GetAllAPReceipts(orderStatuses);
                this.CustomerSales = localSales
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllAPReceipts(orderStatuses)
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.APReceipt);
            }
        }

        /// <summary>
        /// Displays the base document.
        /// </summary>
        private void ShowBaseDocument()
        {
            try
            {
                var baseDoc = this.DataManager.GetAPOrderByID(this.Sale.BaseDocumentReferenceID.ToInt());
                Messenger.Default.Send(ViewType.APOrder);
                this.Locator.APOrder.Sale = baseDoc;
                SystemMessage.Write(MessageType.Priority, Message.BaseDocumentLoaded);
            }
            finally
            {
                //this.Locator.ARDispatch.DisplayModeOnly = false;
            }
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.APReceipt);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.timer.Interval = TimeSpan.FromSeconds(1.5);
            this.timer.Tick += (sender, args) =>
            {
                this.dontChangeMode = true;
                this.timer.Stop();
                if (ApplicationSettings.TouchScreenMode)
                {
                    Keypad.Show(KeypadTarget.IntakeScan, useKeypadSides: ApplicationSettings.UseKeypadSidesAtIntake);
                }
            };
        }

        /// <summary>
        /// Gets all the quotes.
        /// </summary>
        private void GetAllSales()
        {
            this.AllSales = this.DataManager.GetAllAPReceipts(this.DocStatusesOpen);
        }

        /// <summary>
        /// Handles the update request.
        /// </summary>
        protected virtual void UpdateOrder()
        {
            this.CreateSale();
            var docketComplete = this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID;

            this.Sale.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;

            if (docketComplete)
            {
                this.DataManager.UpdateAPReceiptStatus(this.Sale);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.GoodsInUpdated, this.Sale.Number));
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                return;
            }

            if (this.DataManager.UpdateDesktopAPReceipt(this.Sale))
            {
                if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    this.DataManager.PriceIntakeDocket(this.Sale.SaleID);
                }

                if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    this.DataManager.PriceIntakeDocket(this.Sale.SaleID);
                }

                SystemMessage.Write(MessageType.Priority, string.Format(Message.GoodsInUpdated, this.Sale.Number));
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.GoodsInNotUpdated);
            }

            this.SetControlMode(ControlMode.Find);
        }

        /// <summary>
        /// Refreshes the goods in details.
        /// </summary>
        private void RefreshGoodsIn()
        {
            Messenger.Default.Send(Token.Message, Token.RefreshGoodsinDetails);
        }

        /// <summary>
        /// Remove the current transaction.
        /// </summary>
        private void RemoveTransaction()
        {
            this.Log.LogDebug(this.GetType(), "RemoveTransaction: Removing transaction");
            if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.GoodsReceiptDatas != null &&
                this.SelectedSaleDetail.GoodsReceiptDatas.Any())
            {
                if (this.SelectedSaleDetail.GoodsReceiptDatas.Last().TransactionData.Any())
                {
                    var transctionData = this.SelectedSaleDetail.GoodsReceiptDatas.Last().TransactionData;
                    var transaction = transctionData.Last().Transaction;
                    if (transaction.StockTransactionID == 0)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Transaction removed. Wgt:{0}, Qty:{1}", transaction.TransactionWeight, transaction.TransactionQTY));
                        transctionData.RemoveAt(transctionData.Count - 1);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the incoming goods in details, by setting the control mode.
        /// </summary>
        /// <param name="detail">The incoming sale detail.</param>
        /// <param name="wgt">The incoming wgt recorded.</param>
        public void HandleGoodsInData(SaleDetail detail, decimal wgt)
        {
            try
            {
                this.Log.LogDebug(this.GetType(), "HandleGoodsInData(): APSaleDetail returned");
                this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x == detail);

                if (this.SelectedSaleDetail != null)
                {
                    foreach (var data in this.SelectedSaleDetail.GoodsReceiptDatas)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Batch Number:{0}", data.BatchNumber));
                        this.Log.LogDebug(this.GetType(), string.Format("Batch Traceabilities Count:{0}, Transaction Data Count:{1}", data.BatchTraceabilities.Count, data.TransactionData.Count));
                    }

                    this.CompleteTransaction(wgt);
                }
            }
            finally
            {
                this.SetControlMode(ControlMode.Update);
            }
        }

        #endregion

        #endregion
    }
}



﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenSuppliersViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
 
    public class TouchscreenSuppliersViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The partner groups.
        /// </summary>
        private ObservableCollection<BPGroup> partnerGroups;

        /// <summary>
        /// The partner groups.
        /// </summary>
        private IList<BPGroup> allPartnerGroups = new List<BPGroup>();

        /// <summary>
        /// The selected partner group.
        /// </summary>
        private BPGroup selectedPartnerGroup;

        /// <summary>
        /// The partners search text.
        /// </summary>
        private string partnersSearchText;

        /// <summary>
        /// Enable the removal of a partner.
        /// </summary>
        private bool enableRemovePartner;

        /// <summary>
        /// All the partners.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> allPartners = new ObservableCollection<ViewBusinessPartner>();

        /// <summary>
        /// The group suppliers.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> partners = new ObservableCollection<ViewBusinessPartner>();

        /// <summary>
        /// The partners (pre filtered).
        /// </summary>
        private List<ViewBusinessPartner> preFilterPartners = new List<ViewBusinessPartner>();

        /// <summary>
        /// The selected partner.
        /// </summary>
        private ViewBusinessPartner selectedPartner;

        /// <summary>
        /// Show the grid group panel flag.
        /// </summary>
        private bool showGroupPanel;

        /// <summary>
        /// The grid search filter text.
        /// </summary>
        private string searchString;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenSuppliersViewModel"/> class.
        /// </summary>
        public TouchscreenSuppliersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            //Messenger.Default.Register<string>(this,Token.UpdatePartners, s =>
            //{
            //    NouvemGlobal.RefreshData();
            //    this.SetPartners(ViewType.ARDispatch);
            //    this.SetPartnerGroups();
            //});

            #endregion

            #region command handler

            // Handler to turn the goup panel display on/off.
            this.ShowGroupPanelCommand = new RelayCommand(() => this.ShowGroupPanel = !this.ShowGroupPanel);

            // Scroll the suppliers grid.
            this.ScrollCommand = new RelayCommand<string>(s => Messenger.Default.Send(s, Token.ScrollSuppliers));

            // Move back to main
            this.MoveBackCommand = new RelayCommand(this.MoveBackCommandExecute);

            // Search for a supplier.
            this.FindPartnerCommand = new RelayCommand(this.FindPartnerCommandExecute);

            // Order the partners.
            this.OrderPartnersCommand = new RelayCommand<string>(this.OrderPartnersCommandExecute);

            // Handle the supplier selection.
            this.PartnerSelectedCommand = new RelayCommand(this.PartnerSelectedCommandExecute);

            // Handle the partner removal.
            this.RemovePartnerCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.RemovePartner);
                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
            });

            // Handle the partners load event.
            this.OnPartnersLoadingCommand = new RelayCommand(() =>
            {
                this.OnPartnersLoadingCommandExecute();
            });

            // Handle the partners unload event.
            this.OnPartnersUnloadedCommand = new RelayCommand(() => this.IsFormLoaded = false);

            #endregion

            this.GetPartnerGroups();
            this.SearchString = string.Empty;
        }

        #endregion

        #region public interface

        #region property
       
        /// <summary>
        /// Gets or sets a value indicating whether the remove partner from label button is visible/enabled.
        /// </summary>
        public bool EnableRemovePartner
        {
            get
            {
                return this.enableRemovePartner;
            }

            set
            {
                this.enableRemovePartner = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the calling module.
        /// </summary>
        public ViewType MasterModule { get; set; }

        /// <summary>
        /// Gets or sets the search string.
        /// </summary>
        public string SearchString
        {
            get
            {
                return this.searchString;
            }

            set
            {
                this.searchString = value;
                if (value != null)
                {
                    Messenger.Default.Send(value, Token.FilterSuppliers);
                }

                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid couup panel is displayed.
        /// </summary>
        public bool ShowGroupPanel
        {
            get
            {
                return this.showGroupPanel;
            }

            set
            {
                this.showGroupPanel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partners/filter text.
        /// </summary>
        public string PartnersSearchText
        {
            get
            {
                return this.partnersSearchText;
            }

            set
            {
                this.partnersSearchText = value;
                this.RaisePropertyChanged();

                if (ApplicationSettings.ScannerMode)
                {
                    return;
                }

                if (string.IsNullOrEmpty(value))
                {
                    if (!this.clearingSalesSearch)
                    {
                        if (ApplicationSettings.ClearPartnersOnEntry)
                        {
                            this.Partners.Clear();
                        }
                        else
                        {
                            if (this.allPartners.Count != this.Partners.Count)
                            {
                                this.Partners = new ObservableCollection<ViewBusinessPartner>(this.allPartners);
                            }
                        }
                    }
                    else
                    {
                        if (ApplicationSettings.ClearPartnersOnEntry)
                        {
                            this.Partners.Clear();
                        }
                        else
                        {
                            if (this.allPartners.Count != this.Partners.Count)
                            {
                                this.Partners = new ObservableCollection<ViewBusinessPartner>(this.allPartners);
                            }
                        }
                    }
                }
                else
                {
                    this.Partners.Clear();

                    IList<ViewBusinessPartner> localPartners;
                    if (ApplicationSettings.TouchscreenPartnerSearchByIncludes)
                    {
                        localPartners = this.allPartners.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.ContainsIgnoringCase(value)).ToList();
                    }
                    else
                    {
                        localPartners = this.allPartners.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.StartsWithIgnoringCase(value)).ToList();
                    }

                    if (localPartners.Count > ApplicationSettings.FilterPartnerCountOnSearchScreen)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            foreach (var viewBusinessPartner in localPartners.Take(ApplicationSettings.FilterPartnerCountOnSearchScreen))
                            {
                                this.Partners.Add(viewBusinessPartner);
                            }
                        }));
                    }
                    else
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            foreach (var viewBusinessPartner in localPartners)
                            {
                                this.Partners.Add(viewBusinessPartner);
                            }
                        }));
                    }

                    //this.Partners = new ObservableCollection<ViewBusinessPartner>(localPartners);
                    //if (value.Length > this.previousSearchTextLength)
                    //{
                    //    var newPartners = localPartners.Where(x => !this.Partners.Contains(x));
                    //    var count = 0;
                    //    var start = 0;
                    //    var amountToSkip = 0;
                    //    while (amountToSkip <= newPartners.Count())
                    //    {
                    //        var currentPartners = newPartners.Skip(amountToSkip).Take(20).ToList();
                    //        amountToSkip += 20;
                    //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    //        {
                    //            foreach (var partner in currentPartners)
                    //            {
                    //                this.Partners.Add(partner);
                    //            }
                    //        }));
                    //    }
                    //}
                    //else
                    //{
                    //    var partnersToRemove = localPartners.Where(x => this.Partners.Contains(x));
                    //    var count = 0;
                    //    var start = 0;
                    //    var amountToSkip = 0;
                    //    while (amountToSkip <= partnersToRemove.Count())
                    //    {
                    //        var currentPartners = partnersToRemove.Skip(amountToSkip).Take(20).ToList();
                    //        amountToSkip += 20;
                    //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    //        {
                    //            foreach (var partner in currentPartners)
                    //            {
                    //                this.Partners.Remove(partner);
                    //            }
                    //        }));
                    //    }
                    //}

                    //this.Partners = new ObservableCollection<ViewBusinessPartner>(localPartners);
                }
            }
        }
        
        /// <summary>
        /// Gets or set the selected partner group.
        /// </summary>
        public ViewBusinessPartner SelectedPartner
        {
            get
            {
                return this.selectedPartner;
            }

            set
            {
                this.selectedPartner = value;
                this.RaisePropertyChanged();

                if (ApplicationSettings.ScannerMode)
                {
                    if (value != null && this.IsFormLoaded)
                    {
                        this.Locator.TouchscreenOrders.GetAllOrders();
                        var partnerOrders = this.Locator.TouchscreenOrders.AllOrders.Where(x => x.BPCustomer.BPMasterID == this.selectedPartner.BPMasterID).ToList();
                        if (!partnerOrders.Any()) 
                        {
                            // no partner orders found, so move directly back to the main screen.
                            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerTouchscreen;
                            this.Locator.ScannerMainDispatch.SelectedPartner = value;
                            this.Locator.ScannerMainDispatch.SelectedPartnerName = value.Name;
                        }
                        else
                        {
                            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerOrders;
                            Messenger.Default.Send(Tuple.Create(this.selectedPartner, ViewType.TouchscreenSuppliers, partnerOrders), Token.DisplaySupplierSales);
                        }
                    }
                }
                //else
                //{
                //    this.HandlePartnerSelection();
                //}
            }
        }

        /// <summary>
        /// Gets or set the suppliers.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Partners
        {
            get
            {
                return this.partners;
            }

            set
            {
                this.partners = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the partner groups.
        /// </summary>
        public ObservableCollection<BPGroup> PartnerGroups
        {
            get
            {
                return this.partnerGroups;
            }

            set
            {
                this.partnerGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected partner group.
        /// </summary>
        public BPGroup SelectedPartnerGroup
        {
            get
            {
                return this.selectedPartnerGroup;
            }

            set
            {
                this.selectedPartnerGroup = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.HandleSupplierGroupSelection();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the partners load event.
        /// </summary>
        public ICommand OnPartnersLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the partners unload event.
        /// </summary>
        public ICommand OnPartnersUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the group panel.
        /// </summary>
        public ICommand ShowGroupPanelCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the partners removal.
        /// </summary>
        public ICommand RemovePartnerCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand ScrollCommand { get; set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to find a partner.
        /// </summary>
        public ICommand FindPartnerCommand { get; private set; }

        /// <summary>
        /// Gets the command to select the partner.
        /// </summary>
        public ICommand PartnerSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to order the partners.
        /// </summary>
        public ICommand OrderPartnersCommand { get; private set; }

        #endregion

        #region method

        public void OnEntry()
        {
            this.OnPartnersLoadingCommandExecute();
        }

        /// <summary>
        /// Sets the partners.
        /// </summary>
        /// <param name="view">The partner type (supplier or customer).</param>
        public void SetPartners(ViewType view)
        {
            switch (view)
            {
                case ViewType.APReceipt:
                    this.allPartners = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.SupplierPartners
                        .Where(bp => (bp.Details.BPGroupName != Constant.Haulier) && ((bp.Details.InActiveFrom == null && bp.Details.InActiveTo == null) || !(DateTime.Today >= bp.Details.InActiveFrom && DateTime.Today < bp.Details.InActiveTo)))
                        .OrderBy(part => part.Details.DisplayData)
                        .Select(x => x.Details));
                break;

                case ViewType.ARDispatch:
                case ViewType.ARReturns:
                    this.allPartners = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.CustomerPartners
                     .Where(bp => (bp.Details.BPGroupName != Constant.Haulier) && ((bp.Details.InActiveFrom == null && bp.Details.InActiveTo == null) || !(DateTime.Today >= bp.Details.InActiveFrom && DateTime.Today < bp.Details.InActiveTo)))
                    .OrderBy(part => part.Details.DisplayData)
                    .Select(x => x.Details));
                break;

                case ViewType.LairageIntakeTouchscreen:
                    this.allPartners = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.BusinessPartners
                        .Where(x => (x.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier)
                                     || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock))
                                    && ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                        || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)))
                        .OrderBy(x => x.Details.DisplayData)
                        .Select(x => x.Details));
                    break;
            }

            this.SetPartnerGroups();
        }

        #endregion

        #endregion

        #region protected

        protected override void CheckForNewEntities(string entityType = "")
        {
            try
            {
                var newEntities = this.DataManager.CheckForNewEntities(NouvemGlobal.DeviceId.ToInt(), entityType);
                var partnerEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.BusinessPartner));
                foreach (var partnerEntity in partnerEntities)
                {
                    if (NouvemGlobal.BusinessPartners.Any(x => x.Details.BPMasterID == partnerEntity.EntityID))
                    {
                        continue;
                    }

                    var localPartner = this.DataManager.GetBusinessPartner(partnerEntity.EntityID);
                    if (localPartner != null)
                    {
                        NouvemGlobal.AddNewPartner(localPartner);
                        this.allPartners.Add(localPartner.Details);
                    }
                }
            }
            catch (Exception e)
            {
               
            }
        }

        /// <summary>
        /// Moves back to the main screen.
        /// </summary>
        protected virtual void MoveBackCommandExecute()
        {
            //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
            Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
        }

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the load event.
        /// </summary>
        private void OnPartnersLoadingCommandExecute()
        {
            this.CheckForNewEntities(EntityType.BusinessPartner);

            if (ApplicationSettings.ClearPartnersOnEntry)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.FindPartnerCommandExecute();
                }));
            }

            this.EnableRemovePartner = this.MasterModule == ViewType.OutOfProduction &&
                                       ApplicationSettings.CanSelectCustomersAtOutOfProduction;
            this.PartnersSearchText = string.Empty;
            this.SelectedPartner = null;
            this.IsFormLoaded = true;
            this.Locator.FactoryScreen.ModuleName = Strings.PartnerSelectionScreen;

            if (this.PartnerGroups == null)
            {
                if (this.MasterModule == ViewType.LairageIntakeTouchscreen)
                {
                    this.SetPartners(ViewType.LairageIntakeTouchscreen);
                }
                else if (!ViewModelLocator.IsAPReceiptTouchscreenNull())
                {
                    this.SetPartners(ViewType.APReceipt);
                }
                else
                {
                    this.SetPartners(ViewType.ARDispatch);
                }
            }

            if (this.PartnerGroups != null && this.PartnerGroups.Count == 1)
            {
                this.SelectedPartnerGroup = this.PartnerGroups.First();
            }
        }

        /// <summary>
        /// Shows/hides the keyboard.
        /// </summary>
        private void FindPartnerCommandExecute()
        {
            this.clearingSalesSearch = true;
            this.PartnersSearchText = string.Empty;
            this.clearingSalesSearch = false;
            Messenger.Default.Send(Token.Message, Token.DisplayKeyboard);
        }

        /// <summary>
        /// Order the suppliers.
        /// </summary>
        /// <param name="direction">The direction in which to order the suppliers.</param>
        private void OrderPartnersCommandExecute(string direction)
        {
            this.OrderAndFilterPartners(direction);
        }

        /// <summary>
        /// Handle the supplier selection.
        /// </summary>
        private void PartnerSelectedCommandExecute()
        {
            this.HandlePartnerSelection();
        }

        #endregion

        #region helper

        /// <summary>
        /// Handle the partner selection.
        /// </summary>
        private void HandlePartnerSelection()
        {
            if (this.selectedPartner != null)
            {
                if (this.MasterModule == ViewType.OutOfProduction)
                {
                    Messenger.Default.Send(this.selectedPartner, Token.SupplierSelected);
                    //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
                    Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
                    return;
                }

                if (this.MasterModule == ViewType.LairageIntakeTouchscreen)
                {
                    Messenger.Default.Send(this.selectedPartner, Token.SupplierSelected);
                    //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
                    Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
                    return;
                }

                if (this.MasterModule == ViewType.TouchscreenProductionOrders)
                {
                    // Production order screen always displayed.
                    Messenger.Default.Send(Tuple.Create(this.selectedPartner, ViewType.TouchscreenSuppliers), Token.DisplaySupplierSales);
                    Messenger.Default.Send(false, Token.CloseTouchscreenProductionOrders);
                    return;
                }

                this.Locator.TouchscreenOrders.GetAllOrders();

                var partnerOrders = this.Locator.TouchscreenOrders.AllOrders.Where(x => x.BPCustomer != null && x.BPCustomer.BPMasterID == this.selectedPartner.BPMasterID).ToList();
                if (!partnerOrders.Any()) 
                {
                    // no partner orders found, so move directly back to the main screen.
                    Messenger.Default.Send(this.selectedPartner, Token.SupplierSelected);
                    Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        // instruct the apgoodsreceipttouchscreen to update it's batch no.
                        Messenger.Default.Send(Token.Message, Token.UpdateBatchNo);
                    }));
                }
                else
                {
                    try
                    {
                        // move to the orders screen 
                        if (this.MasterModule == ViewType.APReceipt)
                        {
                            if (!this.Locator.APReceiptTouchscreen.OrdersScreenCreated)
                            {
                                Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
                                this.Locator.APReceiptTouchscreen.OrdersScreenCreated = true;
                                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                                this.Locator.TouchscreenOrders.OnEntry();
                                return;
                            }

                            Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
                            Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
                        }
                        else
                        {
                            if (!this.Locator.ARDispatchTouchscreen.OrdersScreenCreated)
                            {
                                Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
                                this.Locator.ARDispatchTouchscreen.OrdersScreenCreated = true;
                                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                                this.Locator.TouchscreenOrders.OnEntry();
                                return;
                            }

                            Messenger.Default.Send(true, Token.CloseTouchscreenPartnersWindow);
                            Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
                        }
                    }
                    finally
                    {
                        Messenger.Default.Send(Tuple.Create(this.selectedPartner, ViewType.TouchscreenSuppliers, partnerOrders), Token.DisplaySupplierSales);
                    }
                }
            }
        }

        /// <summary>
        /// Orders and/or filters the partners.
        /// </summary>
        /// <param name="mode">The order/filter mode.</param>
        private void OrderAndFilterPartners(string mode)
        {
            this.SelectedPartner = null;

            // ordering
            if (mode.Equals(Constant.Ascending))
            {
                var orderedSuppliers = this.Partners.OrderBy(x => x.DisplayData);
                this.Partners = new ObservableCollection<ViewBusinessPartner>(orderedSuppliers);
                return;
            }

            if (mode.Equals(Constant.Descending))
            {
                var orderedSuppliersDesc = this.Partners.OrderByDescending(x => x.DisplayData);
                this.Partners = new ObservableCollection<ViewBusinessPartner>(orderedSuppliersDesc);
                return;
            }

            // filtering
            var filteredSuppliers 
                = this.searchString != string.Empty ? this.preFilterPartners.Where(x => x.Name.StartsWithIgnoringCase(this.searchString)) : this.preFilterPartners;

            this.Partners.Clear();
            foreach (var localSupplier in filteredSuppliers)
            {
                this.Partners.Add(localSupplier);
            }
        }

        /// <summary>
        /// Update the suppliers to that of the selected group.
        /// </summary>
        private void HandleSupplierGroupSelection()
        {
            if (ApplicationSettings.ClearPartnersOnEntry)
            {
                return;
            }

            var localPartners = this.allPartners
                .Where(x => x.BPGroupID == this.selectedPartnerGroup.BPGroupID && x.BPGroupName != Constant.Haulier
                       && ((x.InActiveFrom == null && x.InActiveTo == null) || !(DateTime.Today >= x.InActiveFrom && DateTime.Today < x.InActiveTo))
                          ).OrderBy(x => x.DisplayData);
            this.Partners = new ObservableCollection<ViewBusinessPartner>(localPartners.OrderBy(x => x.DisplayData));
            this.preFilterPartners = this.Partners.ToList();
        }

        /// <summary>
        /// Gets the partner groups.
        /// </summary>
        private void GetPartnerGroups()
        {
            this.allPartnerGroups = new ObservableCollection<BPGroup>(this.DataManager.GetBusinessPartnerGroups());
        }

        /// <summary>
        /// Sets the partner groups.
        /// </summary>
        private void SetPartnerGroups()
        {
            this.PartnerGroups = new ObservableCollection<BPGroup>();
            foreach (var group in this.allPartnerGroups)
            {
                if (this.allPartners.Any(x => x.BPGroupID == group.BPGroupID))
                {
                    this.PartnerGroups.Add(group);
                }
            }

            if (this.PartnerGroups.Count == 1)
            {
                // only 1 group, so hide it.
                this.SelectedPartnerGroup = this.partnerGroups.First();
                this.PartnerGroups.Clear();
            }
        }

        #endregion

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenContainerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Editors.Helpers;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class TouchscreenContainerViewModel : NouvemViewModelBase
    {
        #region field
       
        /// <summary>
        /// The contianer types.
        /// </summary>
        private ObservableCollection<ContainerType> containerTypes;

        /// <summary>
        /// The selected container type.
        /// </summary>
        private ContainerType selectedContainerType;
       
        /// <summary>
        /// The containers.
        /// </summary>
        private ObservableCollection<Container> containers;

        /// <summary>
        /// All the containers.
        /// </summary>
        private IList<Container> allContainers;

        /// <summary>
        /// The selected container.
        /// </summary>
        private Container selectedContainer;

        /// <summary>
        /// Show the grid group panel flag.
        /// </summary>
        private bool showGroupPanel;

        /// <summary>
        /// The product search string.
        /// </summary>
        private string searchString;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenContainerViewModel"/> class.
        /// </summary>
        public TouchscreenContainerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration



            #endregion

            #region command handler

            // Handle the ordering.
            this.OrderContainersCommand = new RelayCommand<string>(this.OrderContainersCommandExecute);

            // Handle the container selection.
            this.ContainerSelectedCommand = new RelayCommand(this.ContainerSelectedCommandExecute);
            
            // Search for a container.
            this.FindContainerCommand = new RelayCommand(this.FindContainerCommandExecute);

            // Handler to turn the goup panel display on/off.
            this.ShowGroupPanelCommand = new RelayCommand(() => this.ShowGroupPanel = !this.ShowGroupPanel);

            // Scroll the suppliers grid.
            this.ScrollCommand = new RelayCommand<string>(s => Messenger.Default.Send(s, Token.ScrollProducts));

            // Move back to main
            this.MoveBackCommand = new RelayCommand(() => this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen);

            this.ResetTareCommand = new RelayCommand(() =>
            {
                this.Locator.Indicator.ResetContainer();
                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
            });

            #endregion

            #region instantiation

            this.ContainerTypes = new ObservableCollection<ContainerType>();
            this.Containers = new ObservableCollection<Container>();
        
            #endregion

            this.GetContainerTypes();
            this.SelectedContainerType = this.ContainerTypes.FirstOrDefault();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the search string.
        /// </summary>
        public string SearchString
        {
            get
            {
                return this.searchString;
            }

            set
            {
                this.searchString = value;
                if (value != null)
                {
                    Messenger.Default.Send(value, Token.FilterProducts);
                }

                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid couup panel is displayed.
        /// </summary>
        public bool ShowGroupPanel
        {
            get
            {
                return this.showGroupPanel;
            }

            set
            {
                this.showGroupPanel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected container.
        /// </summary>
        public Container SelectedContainer
        {
            get
            {
                return this.selectedContainer;
            }

            set
            {
                this.selectedContainer = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the containers.
        /// </summary>
        public ObservableCollection<Container> Containers
        {
            get
            {
                return this.containers;
            }

            set
            {
                this.containers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the container types.
        /// </summary>
        public ObservableCollection<ContainerType> ContainerTypes
        {
            get
            {
                return this.containerTypes;
            }

            set
            {
                this.containerTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected container type.
        /// </summary>
        public ContainerType SelectedContainerType
        {
            get
            {
                return this.selectedContainerType;
            }

            set
            {
                this.selectedContainerType = value;

                if (value != null)
                {
                    this.HandleContainerTypeSelection();
                }

                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command
        
        /// <summary>
        /// Gets the command to handle the container selection.
        /// </summary>
        public ICommand ContainerSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the tare reset.
        /// </summary>
        public ICommand ResetTareCommand { get; private set; }

        /// <summary>
        /// Gets the command to find a container.
        /// </summary>
        public ICommand FindContainerCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the group panel.
        /// </summary>
        public ICommand ShowGroupPanelCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand ScrollCommand { get; set; }

        /// <summary>
        /// Gets the command to order the containers.
        /// </summary>
        public ICommand OrderContainersCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command ecexution

        /// <summary>
        /// Shows/hides the keyboard.
        /// </summary>
        private void FindContainerCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.DisplayKeyboard);
        }

        /// <summary>
        /// Order the products.
        /// </summary>
        /// <param name="direction">The direction in which to order the containers.</param>
        private void OrderContainersCommandExecute(string direction)
        {
            this.SelectedContainer = null;

            // ordering
            if (direction.Equals(Constant.Ascending))
            {
                var orderedContainers = this.Containers.OrderBy(x => x.Name);
                this.Containers = new ObservableCollection<Container>(orderedContainers);
                return;
            }

            if (direction.Equals(Constant.Descending))
            {
                var orderedContainersDesc = this.Containers.OrderByDescending(x => x.Name);
                this.Containers = new ObservableCollection<Container>(orderedContainersDesc);
            }
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void ContainerSelectedCommandExecute()
        {
            #region validation

            if (this.SelectedContainer == null)
            {
                return;
            }

            #endregion

            Messenger.Default.Send(this.selectedContainer, Token.ContainerSelected);
            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
        }
       
        #endregion

        #region helper

        /// <summary>
        /// Update the containers to that of the selected type.
        /// </summary>
        private void HandleContainerTypeSelection()
        {
            this.Containers.Clear();

            var localContainers =
                NouvemGlobal.Containers.Where(x => x.ContainerTypeID == this.selectedContainerType.ContainerTypeID);

            foreach (var item in localContainers)
            {
                if (item.Photo == null)
                {
                    item.Photo = new Uri("pack://application:,,,/Design/Image/NoImageAvailable.jpg").UriToBytes();
                }
            }

            this.Containers 
                = new ObservableCollection<Container>(NouvemGlobal.Containers.Where(x => x.ContainerTypeID == this.selectedContainerType.ContainerTypeID).OrderBy(x => x.Name));
        }

        /// <summary>
        /// Gets the container types.
        /// </summary>
        private void GetContainerTypes()
        {
            this.ContainerTypes = new ObservableCollection<ContainerType>(NouvemGlobal.ContainerTypes);
        }

        #endregion

        #endregion
    }
}


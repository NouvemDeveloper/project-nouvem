﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenSuppliersViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Nouvem.Model.Enum;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Editors.Helpers;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class TouchscreenProductsViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The inventory purchase items collection.
        /// </summary>
        private ObservableCollection<InventoryItem> purchaseItems;

        /// <summary>
        /// The selected view.
        /// </summary>
        private ViewModelBase productsDisplayViewModel;
      
        /// <summary>
        /// The selected product group.
        /// </summary>
        private INGroup selectedProductGroup;

        /// <summary>
        /// The previously selected product group.
        /// </summary>
        private INGroup previousSelectedProductGroup;

        /// <summary>
        /// Are we searching on the ui flag.
        /// </summary>
        private bool filterPanelOpen;

        /// <summary>
        /// Flag as to whether we are at intake.
        /// </summary>
        private bool atIntakeMode;

        /// <summary>
        /// The search text.
        /// </summary>
        private string productSearchText;

        /// <summary>
        /// The group products.
        /// </summary>
        private ObservableCollection<InventoryItem> products = new ObservableCollection<InventoryItem>();

        /// <summary>
        /// The group products.
        /// </summary>
        private IList<InventoryItem> carcassSplitProducts = new List<InventoryItem>();

        /// <summary>
        /// The selected supplier.
        /// </summary>
        private InventoryItem selectedProduct;

        /// <summary>
        /// Show the grid group panel flag.
        /// </summary>
        private bool showGroupPanel;

        /// <summary>
        /// The product search string.
        /// </summary>
        private string searchString;

        /// <summary>
        /// The product groups.
        /// </summary>
        private ObservableCollection<INGroup> productGroups;

        /// <summary>
        /// All the product groups.
        /// </summary>
        protected ObservableCollection<INGroup> allProductGroups;

        /// <summary>
        /// The entry base product groups.
        /// </summary>
        protected IList<INGroup> homeProductGroups;

        /// <summary>
        /// All the suppliers.
        /// </summary>
        protected IDictionary<INGroup, List<InventoryItem>> allProducts;

        /// <summary>
        /// All the suppliers.
        /// </summary>
        //protected List<InventoryItem> AllSearchProducts = new List<InventoryItem>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenProductsViewModel"/> class.
        /// </summary>
        public TouchscreenProductsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
         
            #region message registration

            // Register for a new product addition.
            Messenger.Default.Register<Tuple<InventoryItem, bool>>(this, Token.RefreshProducts, x =>
            {
                var item = x.Item1;
                var isNewProduct = x.Item2;
                if (isNewProduct)
                {
                    this.RefreshProducts(x.Item1);
                }
                else
                {
                    this.GetAllProducts();
                }
            });

            // Register for a filter panel close flag.
            Messenger.Default.Register<string>(this, Token.FilterPanelClosed, s => this.filterPanelOpen = false);

            #endregion

            #region command handler

            this.HandeProductionProductGroupSelectionCommand = new RelayCommand(this.HandleProductionProductGroupSelection);

            // Handle the products load event.
            this.OnProductsLoadingCommand = new RelayCommand(() =>
            {
                if (this.Module == ViewType.IntoProduction &&
                    (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit))
                {
                    return;
                }

                if (this.Module == ViewType.ARDispatch && !string.IsNullOrEmpty(ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatchGroups))
                {
                    var allowableGroups = ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatchGroups.Split(',');
                    if (allowableGroups.Length != this.ProductGroups.Count)
                    {
                        this.GetFilteredProductGroups(allowableGroups);
                    }
                    else if (this.ProductGroups.Count == 1)
                    {
                        this.SelectedProductGroup = null;
                        this.SelectedProductGroup = this.ProductGroups.FirstOrDefault();
                    }
                }

                this.GetAllProducts();
                this.ProductsSearchText = string.Empty;
                this.SelectedProduct = null;
                this.IsFormLoaded = true;
                this.atIntakeMode = true;
                this.Locator.FactoryScreen.ModuleName = Strings.ProductSelection;
                HandleProductionProductGroupSelection();
            });

            // Handle the products unload event.
            this.OnProductsUnloadedCommand = new RelayCommand(() => this.IsFormLoaded = false);

            // Handle the group level up action.
            this.GroupLevelUpCommand = new RelayCommand<string>(this.GroupLevelUpCommandExecute);

            // Handle the product selection.
            this.ProductSelectedCommand = new RelayCommand(this.ProductSelectedCommandExecute);

            // Order the suppliers.
            this.OrderProductsCommand = new RelayCommand<string>(this.OrderProductsCommandExecute);

            // Search for a product.
            this.FindProductCommand = new RelayCommand(this.FindProductCommandExecute);

            // Handler to turn the goup panel display on/off.
            this.ShowGroupPanelCommand = new RelayCommand(() => this.ShowGroupPanel = !this.ShowGroupPanel);

            // Scroll the suppliers grid.
            this.ScrollCommand = new RelayCommand<string>(s => Messenger.Default.Send(s, Token.ScrollProducts));

            // Move back to main
            this.MoveBackCommand = new RelayCommand(() => this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen);

            #endregion

            #region instantiation

            this.allProductGroups = new ObservableCollection<INGroup>();
            this.ProductGroups = new ObservableCollection<INGroup>();
            this.Products = new ObservableCollection<InventoryItem>();
            this.homeProductGroups = new List<INGroup>();

            #endregion

            this.GetProductGroups();
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
            {
                this.SetCarcassSplitProducts();
            }

            if (ApplicationSettings.ShowExpandedProductGroups)
            {
                this.ProductsDisplayViewModel = this.Locator.TouchscreenProductsExpandGroups;
            }
            else
            {
                this.ProductsDisplayViewModel = this.Locator.TouchscreenProductsMaster;
            }
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the partner id.
        /// </summary>
        public int? PartnerId { get; set; }

        /// <summary>
        /// Gets or sets the products display vm.
        /// </summary>
        public ViewModelBase ProductsDisplayViewModel
        {
            get
            {
                return this.productsDisplayViewModel;
            }

            set
            {
                this.productsDisplayViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the module.
        /// </summary>
        public ViewType Module { get; set; }

        /// <summary>
        /// Gets or sets the orders serch/filter text.
        /// </summary>
        public string ProductsSearchText
        {
            get
            {
                return this.productSearchText;
            }

            set
            {
                this.productSearchText = value;
                this.RaisePropertyChanged();
                if (!this.EntitySelectionChange)
                {
                    this.HandleProductSearch();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search string.
        /// </summary>
        public string SearchString
        {
            get
            {
                return this.searchString;
            }

            set
            {
                this.searchString = value;
                if (value != null)
                {
                    Messenger.Default.Send(value, Token.FilterProducts);
                }

                this.RaisePropertyChanged();
            }
       }
     
        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public ObservableCollection<InventoryItem> PurchaseItems
        {
            get
            {
                return this.purchaseItems;
            }

            set
            {
                this.purchaseItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid couup panel is displayed.
        /// </summary>
        public bool ShowGroupPanel
        {
            get
            {
                return this.showGroupPanel;
            }

            set
            {
                this.showGroupPanel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected product.
        /// </summary>
        public InventoryItem SelectedProduct
        {
            get
            {
                return this.selectedProduct;
            }

            set
            {
                this.selectedProduct = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (ApplicationSettings.ResetLabelOnProductSelection)
                    {
                        Messenger.Default.Send(true, Token.ResetLabel);
                    }

                    if (!this.atIntakeMode)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.HandleProductSelection();
                        }));
                    }
                }
                else
                {
                    Messenger.Default.Send(Token.Message, Token.DeselectProduct);
                }
            }
        }

        /// <summary>
        /// Gets or set the products.
        /// </summary>
        public ObservableCollection<InventoryItem> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.products = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the product groups.
        /// </summary>
        public ObservableCollection<INGroup> ProductGroups
        {
            get
            {
                return this.productGroups;
            }

            set
            {
                this.productGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected product group.
        /// </summary>
        public INGroup SelectedProductGroup
        {
            get
            {
                return this.selectedProductGroup;
            }

            set
            {
                this.selectedProductGroup = value;
                
                if (value != null && !value.Name.CompareIgnoringCase("DO NOT USE"))
                {
                    this.previousSelectedProductGroup = value;
                    this.HandleProductGroupSelection();
                }

                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand HandeProductionProductGroupSelectionCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the the load event.
        /// </summary>
        public ICommand OnProductsLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the the unload event.
        /// </summary>
        public ICommand OnProductsUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the group level up action.
        /// </summary>
        public ICommand GroupLevelUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the product selection.
        /// </summary>
        public ICommand ProductSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to order the products.
        /// </summary>
        public ICommand OrderProductsCommand { get; private set; }

        /// <summary>
        /// Gets the command to find a product.
        /// </summary>
        public ICommand FindProductCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the group panel.
        /// </summary>
        public ICommand ShowGroupPanelCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand ScrollCommand { get; set; }

        /// <summary>
        /// Gets the command tomove back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #region method

        public void FilterProducts(IList<int> data)
        {
            this.Log.LogDebug(this.GetType(), "FilterProducts()....filtering");
            this.ProductsSearchText = string.Empty;
            this.IsFormLoaded = true;
            this.Locator.FactoryScreen.ModuleName = Strings.ProductSelection;
            this.ProductGroups.Clear();
            this.Products = new ObservableCollection<InventoryItem>(this.carcassSplitProducts);

            foreach (var productToRemove in data)
            {
                var product = this.Products.FirstOrDefault(x => x.Master.INMasterID == productToRemove);
                if (product != null)
                {
                    this.Products.Remove(product);
                }
            }

            foreach (var localProduct in this.Products)
            {
                this.Log.LogDebug(this.GetType(), string.Format("Displaying product id:{0}", localProduct.Master.Name));
            }

            this.SelectedProduct = null;

            if (this.Products.Count == 0)
            {
                NouvemMessageBox.Show(Message.CarcassSplitLabelWeighingsComplete, touchScreen:true, flashMessage:true);
            }
        }

        /// <summary>
        /// Sets the into production groups.
        /// </summary>
        /// <param name="options"></param>
        public void SetIntoProductionProductGroups(IList<CarcassSplitOption> options)
        {
            if (ApplicationSettings.IntoProductionMode != IntoProductionMode.IntoProductionSelectProduct)
            {
                return;
            }

            if (!string.IsNullOrEmpty(ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatchGroups))
            {
                var allowableGroups = ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatchGroups.Split(',');
                if (allowableGroups.Length == this.ProductGroups.Count)
                {
                    this.GetProductGroups();
                }
            }

            var groupIds = options.Where(x => x.INGroupID != null).Select(x => x.INGroupID);
            this.ProductGroups = new ObservableCollection<INGroup>(this.allProductGroups.Where(x => groupIds.Contains(x.INGroupID)));
            this.homeProductGroups = this.productGroups.ToList();
        }

        #endregion

        #endregion

        #region protected

        #region virtual

        /// <summary>
        /// Handles a product selection.
        /// </summary>
        protected virtual void HandleProductSelection()
        {
            #region validation

            if (this.SelectedProduct == null || !this.IsFormLoaded)
            {
                return;
            }
           
            #endregion

            Messenger.Default.Send(this.selectedProduct, Token.ProductSelected);
            //int pieces = 1;
            //if (this.selectedProduct.Master.TypicalPieces.HasValue)
            //{
            //    pieces = this.selectedProduct.Master.TypicalPieces.ToInt();
            //}

            //this.Locator.Touchscreen.Quantity = pieces;
            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
            //Messenger.Default.Send(Token.Message, Token.RecordCarcassSplitWeight);
        }

        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command execution
      
        /// <summary>
        /// Shows/hides the keyboard.
        /// </summary>
        protected virtual void FindProductCommandExecute()
        {
            this.filterPanelOpen = true;
            Messenger.Default.Send(true, Token.DisplayKeyboard);
        }

        /// <summary>
        /// Order the products.
        /// </summary>
        /// <param name="direction">The direction in which to order the products.</param>
        private void OrderProductsCommandExecute(string direction)
        {
            this.SelectedProduct = null;

            // ordering
            if (direction.Equals(Constant.Ascending))
            {
                var orderedProducts = this.Products.OrderBy(x => x.Master.Name);
                this.Products = new ObservableCollection<InventoryItem>(orderedProducts);
                return;
            }

            if (direction.Equals(Constant.Descending))
            {
                var orderedProductsDesc = this.Products.OrderByDescending(x => x.Master.Name);
                this.Products = new ObservableCollection<InventoryItem>(orderedProductsDesc);
            }
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void ProductSelectedCommandExecute()
        {
            //if (!this.filterPanelOpen)
            //{
                this.HandleProductSelection();
            //}
        }

        /// <summary>
        /// Handle the group level up action.
        /// </summary>
        /// <param name="action">The action type.</param>
        private void GroupLevelUpCommandExecute(string action)
        {
            #region validation

            if (this.previousSelectedProductGroup == null)
            {
                return;
            }

            #endregion

            if (action.Equals(Constant.Up))
            {
                // move up 1 level
                var groupId = this.previousSelectedProductGroup.INGroupID;
                if (groupId > 0)
                {
                    var parentId = this.previousSelectedProductGroup.ParentInGroupID;
                    if (parentId.IsNullOrZero())
                    {
                        // Moving to home level.
                        this.ProductGroups.Clear();
                        this.homeProductGroups.ForEach(x => this.ProductGroups.Add(x));
                        return;
                    }

                    // Up 1 level, but not home level.
                    //this.allProductGroups.Where(x => x.ParentInGroupID == this.previousSelectedProductGroup.ParentInGroupID)
                    this.ProductGroups.Clear();

                    foreach (var localGroup in this.allProductGroups)
                    {
                        if (localGroup.ParentInGroupID == this.previousSelectedProductGroup.ParentInGroupID)
                        {
                            this.ProductGroups.Add(localGroup);
                        }
                    }

                    var previousId = this.previousSelectedProductGroup.ParentInGroupID;
                    this.previousSelectedProductGroup =
                        this.allProductGroups.FirstOrDefault(x => x.INGroupID == previousId);
                    this.Products.Clear();
                }

                Messenger.Default.Send(false, Token.ShowProducts);
                return;
            }

            // Home
            this.ProductGroups.Clear();
            this.homeProductGroups.ForEach(x => this.ProductGroups.Add(x));
            this.Products.Clear();
            Messenger.Default.Send(false, Token.ShowProducts);
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the carcass split products for selection.
        /// </summary>
        private void SetCarcassSplitProducts()
        {
            var splitProductIds = new List<int>();
            var splitproducts = this.DataManager.GetCarcassSplitProducts();
            if (splitproducts != null)
            {
                foreach (var carcassSplitOption in splitproducts)
                {
                    splitProductIds.Add(carcassSplitOption.INMasterID.ToInt());
                }
            }

            this.carcassSplitProducts = NouvemGlobal.InventoryItems.Where(x => splitProductIds.Contains(x.Master.INMasterID)).ToList();
        }
        
        /// <summary>
        /// Refreshes the products.
        /// </summary>
        /// <param name="item">The newly created product.</param>
        private void RefreshProducts(InventoryItem item)
        {
            if (this.allProducts == null)
            {
                return;
            }

            this.GetProductGroups();

            // Newly added products key object will be unique, so we need to append to correct key.
            var existingGroup = this.allProducts.FirstOrDefault(x => x.Key.INGroupID == item.Group.INGroupID);
            if (existingGroup.Key != null)
            {
                existingGroup.Value.Add(item);
            }
            else
            {
                this.allProducts.Add(item.Group, new List<InventoryItem> { item });
            }
        }

        /// <summary>
        /// Gets the purchase items.
        /// </summary>
        private void GetPurchaseItems()
        {
            this.PurchaseItems = new ObservableCollection<InventoryItem>(NouvemGlobal.PurchaseItems);
        }

        /// <summary>
        /// Update the products to that of the selected group.
        /// </summary>
        protected virtual void HandleProductGroupSelection()
        {
            //this.DeselectProduct();
            //this.Products.Clear();
            //var localProductGroup =
            //    this.allProducts.FirstOrDefault(x => x.Key.INGroupID == this.selectedProductGroup.INGroupID);
            
            //if (localProductGroup.Key != null)
            //{
            //    this.Products = new ObservableCollection<InventoryItem>(localProductGroup.Value.OrderBy(x => x.Name));
            //    this.DeselectProduct();
            //}

            //this.DisplayProductGroups();
        }

        /// <summary>
        /// Update the products to that of the selected group.
        /// </summary>
        protected virtual void HandleProductionProductGroupSelection()
        {
            if (this.selectedProductGroup == null)
            {
                return;
            }

            this.DeselectProduct();
            this.Products.Clear();
            var localProductGroup =
                this.allProducts.FirstOrDefault(x => x.Key.INGroupID == this.selectedProductGroup.INGroupID);

            if (localProductGroup.Key != null)
            {
                //this.AllSearchProducts = localProductGroup.Value.OrderBy(x => x.Name).ToList();
                this.Products = new ObservableCollection<InventoryItem>(localProductGroup.Value.OrderBy(x => x.Name));
                this.DeselectProduct();
            }

            this.DisplayProductGroups();
        }

        //protected void HandleProductSearch()
        //{
        //    string value = this.ProductsSearchText;
        //    if (string.IsNullOrEmpty(value))
        //    {
        //        var localProducts = this.AllSearchProducts;
        //        this.Products = new ObservableCollection<InventoryItem>(localProducts);
        //        //if (this.SelectedProduct != null)
        //        //{
        //        //    this.HandleProductSelection();
        //        //}
        //        //else
        //        //{
        //        //    this.Products = new ObservableCollection<InventoryItem>();
        //        //}
        //    }
        //    else
        //    {
        //        var localProducts = this.AllSearchProducts;

        //        IEnumerable<InventoryItem> filteredProducts;
        //        if (ApplicationSettings.SearchProductsBycode)
        //        {
        //            filteredProducts = localProducts.Where(x => !string.IsNullOrEmpty(x.Code) && x.Code.ContainsIgnoringCase(value));
        //        }
        //        else
        //        {
        //            filteredProducts = localProducts.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.ContainsIgnoringCase(value));
        //        }

        //        this.Products = new ObservableCollection<InventoryItem>(filteredProducts);
        //    }
        //}
        protected void HandleProductSearch()
        {
            string value = this.ProductsSearchText;
            if (string.IsNullOrEmpty(value))
            {
                //var localProducts = this.allProducts.Values.SelectMany(x => x.ToList()); // this.AllSearchProducts;
                //this.Products = new ObservableCollection<InventoryItem>(localProducts);
                if (this.SelectedProduct != null)
                {
                    this.HandleProductSelection();
                }
                else
                {
                    if (this.SelectedProductGroup != null)
                    {
                        var localProducts = this.allProducts.Values.SelectMany(x => x.Where(xx => xx.Master != null &&
                                                                                              xx.Master.INGroupID == this.SelectedProductGroup.INGroupID).ToList());
                        this.Products = new ObservableCollection<InventoryItem>(localProducts);
                    }
                    else
                    {
                        this.Products = new ObservableCollection<InventoryItem>();
                    }
                }
            }
            else
            {
                IEnumerable<InventoryItem> localProducts;
                if (this.SelectedProductGroup == null)
                {
                    localProducts = this.allProducts.Values.SelectMany(x => x.ToList()); // this.AllSearchProducts;
                }
                else
                {
                    localProducts = this.allProducts.Values.SelectMany(x => x.Where(xx => xx.Master != null &&
                                                                                         xx.Master.INGroupID == this.SelectedProductGroup.INGroupID).ToList());
                }

                IEnumerable<InventoryItem> filteredProducts;
                if (ApplicationSettings.SearchProductsBycode)
                {
                    filteredProducts = localProducts.Where(x => !string.IsNullOrEmpty(x.Code) && x.Code.ContainsIgnoringCase(value));
                }
                else
                {
                    filteredProducts = localProducts.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.ContainsIgnoringCase(value));
                }

                this.Products = new ObservableCollection<InventoryItem>(filteredProducts);
            }
        }

        /// <summary>
        /// Gets the product groups.
        /// </summary>
        protected virtual void GetProductGroups()
        {
            var localGroups = this.DataManager.GetInventoryGroups().OrderBy(x => x.Name).ToList();
            this.allProductGroups = new ObservableCollection<INGroup>(localGroups);
            if (!string.IsNullOrEmpty(ApplicationSettings.ProductGroupName))
            {
                this.allProductGroups.Clear();
                var singleGroup =
                    localGroups.FirstOrDefault(x => x.Name.CompareIgnoringCase(ApplicationSettings.ProductGroupName));
                if (singleGroup != null)
                {
                    //this.allProductGroups.Add(singleGroup);
                    var subGroups = localGroups.Where(x => x.ParentInGroupID == singleGroup.INGroupID);
                    foreach (var subGroup in subGroups)
                    {
                        this.allProductGroups.Add(subGroup);
                    }

                    this.ProductGroups = new ObservableCollection<INGroup>(this.allProductGroups);
                    this.homeProductGroups = this.productGroups.ToList();
                    //this.SelectedProductGroup = singleGroup;
                    //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    //{
                    //    this.HandleProductionProductGroupSelection();
                    //}));
                }

                return;
            }

            // Set the base groduct groups for initial display.
            this.ProductGroups = new ObservableCollection<INGroup>(this.allProductGroups.Where(x => x.ParentInGroupID.IsNullOrZero()));
            this.homeProductGroups = this.productGroups.ToList();

            if (this.ProductGroups.Count == 1)
            {
                this.SelectedProductGroup = this.ProductGroups.First();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.HandleProductionProductGroupSelection();
                }));
            }
        }

        /// <summary>
        /// Gets the product groups.
        /// </summary>
        protected void GetFilteredProductGroups(string[] filteredGroupIds)
        {
            var localGroups = this.DataManager.GetInventoryGroups().Where(x => filteredGroupIds.Contains(x.INGroupID.ToString())).OrderBy(x => x.Name).ToList();
            this.allProductGroups = new ObservableCollection<INGroup>(localGroups);
            if (!string.IsNullOrEmpty(ApplicationSettings.ProductGroupName))
            {
                this.allProductGroups.Clear();
                var singleGroup =
                    localGroups.FirstOrDefault(x => x.Name.CompareIgnoringCase(ApplicationSettings.ProductGroupName));
                if (singleGroup != null)
                {
                    //this.allProductGroups.Add(singleGroup);
                    var subGroups = localGroups.Where(x => x.ParentInGroupID == singleGroup.INGroupID);
                    foreach (var subGroup in subGroups)
                    {
                        this.allProductGroups.Add(subGroup);
                    }

                    this.ProductGroups = new ObservableCollection<INGroup>(this.allProductGroups);
                    this.homeProductGroups = this.productGroups.ToList();
                    //this.SelectedProductGroup = singleGroup;
                    //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    //{
                    //    this.HandleProductionProductGroupSelection();
                    //}));
                }

                return;
            }

            // Set the base groduct groups for initial display.
            this.ProductGroups = new ObservableCollection<INGroup>(this.allProductGroups.Where(x => x.ParentInGroupID.IsNullOrZero()));
            this.homeProductGroups = this.productGroups.ToList();

            if (this.ProductGroups.Count == 1)
            {
                this.SelectedProductGroup = this.ProductGroups.First();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.HandleProductionProductGroupSelection();
                }));
            }
        }

        /// <summary>
        /// Gets all the products.
        /// </summary>
        protected virtual void GetAllProducts()
        {
            this.CheckForNewEntities(EntityType.INMaster);

            if (this.Module == ViewType.APReceipt)
            {
                if (!this.PartnerId.HasValue)
                {
                    this.allProducts = NouvemGlobal.PurchaseItems
                        .Where(x => x.Master.Deleted == null && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                                 || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                        .GroupBy(x => x.Group).ToDictionary(key => key.Key, value => value.ToList());
                }
                else
                {
                    this.allProducts = NouvemGlobal.PurchaseItems
                        .Where(x => x.Master.Deleted == null && x.Master.BPMasterID == this.PartnerId && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                                 || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                        .GroupBy(x => x.Group).ToDictionary(key => key.Key, value => value.ToList());
                }
            }
            else
            {
                this.allProducts = NouvemGlobal.SaleItems
                    .Where(x => x.Master.Deleted == null && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                             || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                    .GroupBy(x => x.Group).ToDictionary(key => key.Key, value => value.ToList());
            }

            if (this.allProducts.Count == 1 && this.ProductGroups != null)
            {
                // 1 group, so open it up.
                this.SelectedProductGroup = this.ProductGroups.FirstOrDefault();
                if (this.SelectedProductGroup != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                    {
                        this.HandleProductionProductGroupSelection();
                    }));
                }
            }
        }

        /// <summary>
        /// Sets the sub groups to display of the selected base group.
        /// </summary>
        protected void DisplayProductGroups()
        {
            #region validation

            if (this.previousSelectedProductGroup == null)
            {
                return;
            }

            #endregion

            var baseId = this.previousSelectedProductGroup.INGroupID;
            var baseGroups = this.allProductGroups.Where(x => x.ParentInGroupID == baseId).ToList();

            if (baseGroups.Any())
            {
                this.ProductGroups = new ObservableCollection<INGroup>(baseGroups);
            }
            else
            {
                Messenger.Default.Send(true, Token.ShowProducts);
            }
        }

        #endregion

        /// <summary>
        /// Deselects the selected product.
        /// </summary>
        protected void DeselectProduct()
        {
            if (this.Products != null)
            {
                foreach (var localProduct in this.Products)
                {
                    localProduct.IsSelected = false;
                }

                this.SelectedProduct = null;
            }
        }

        #endregion
    }
}

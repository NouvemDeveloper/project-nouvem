﻿// -----------------------------------------------------------------------
// <copyright file="TransactionManagerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;
   
    public class TransactionManagerViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The current delete mode
        /// </summary>
        private DeleteMode deleteMode = DeleteMode.Standard;

        /// <summary>
        /// The terminal device (default = current pc, or another in search mode).
        /// </summary>
        private DeviceMaster device;

        /// <summary>
        /// The recent transactions collection.
        /// </summary>
        private ObservableCollection<StockTransactionData> transactions = new ObservableCollection<StockTransactionData>();

        /// <summary>
        /// The selected transaction.
        /// </summary>
        private StockTransactionData selectedTransaction;

        /// <summary>
        /// Flag, as to whether the group panel is displayed or not.
        /// </summary>
        private bool showGroupPanel;

        /// <summary>
        /// The ui search string.
        /// </summary>
        private string searchString;

        /// <summary>
        /// The calling ui module.
        /// </summary>
        private ViewType callingModule;

        /// <summary>
        /// flag, as to whether we are showing line transactions only.
        /// </summary>
        private bool showingLineTransactionsOnly;

        private int transactionTypeId;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionManagerViewModel"/> class.
        /// </summary>
        public TransactionManagerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the printer selection creation.
           

            // Register for the printer selection.
            Messenger.Default.Register<Printer>(this, Token.PrinterSelected, p =>
            {
                try
                {
                    this.PrintManager.ReprintLabel(this.selectedTransaction.Transaction.StockTransactionID, p.PrinterNumber);
                    this.Locator.Touchscreen.UIText = string.Format("{0} {1}", Strings.Printer, p.PrinterNumber);
                    SystemMessage.Write(MessageType.Priority, Message.LabelReprinted);
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }
            });

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.GraderWeightChange, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleGraderWeightChange(s.ToDecimal());
                }
            });

            // Register for incoming device.
            Messenger.Default.Register<DeviceMaster>(this, Token.DeviceSelected, d =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Device = d;
                }));
            });

            Messenger.Default.Register<string>(this, Token.LabelSearch, this.HandleLabelSearch);

            #endregion

            #region command handler

            // Handle the menu item selection.
            this.MenuCommand = new RelayCommand<string>(this.MenuCommandExecute);

            // Handle the loaded event.
            this.OnLoadedCommand = new RelayCommand(this.OnLoadedCommandExecute);

            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = false;
                this.Transactions = null;
                this.GetDeviceOnly = false;
            });

            // Handle the group panel selection.
            this.ShowGroupPanelCommand = new RelayCommand(() => this.ShowGroupPanel = !this.ShowGroupPanel);

            // Handle the command to move back to the orders screen.
            this.MoveBackCommand = new RelayCommand(() =>
            {
                if (this.GetDeviceOnly)
                {
                    this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Touchscreen);
                }
                else
                {
                    this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Menu);
                }
            });

            #endregion
        }

        #endregion

        #region enum

        /// <summary>
        /// The current deletion mode.
        /// </summary>
        public enum DeleteMode
        {
            /// <summary>
            /// Standard deletion.
            /// </summary>
            Standard,

            /// <summary>
            /// Used for into slicing module.
            /// </summary>
            IntoSlicing,

            /// <summary>
            /// Used for dispatch module.
            /// </summary>
            Dispatch,

            /// <summary>
            /// Used for intake module.
            /// </summary>
            Intake,

            /// <summary>
            /// Used for the grader module.
            /// </summary>
            Grader
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether only the device is selected, and not the x amounbt of associated transactions.
        /// </summary>
        public bool GetDeviceOnly { get; set; }

        /// <summary>
        /// Gets or sets the recent transactions.
        /// </summary>
        public ObservableCollection<StockTransactionData> Transactions
        {
            get
            {
                return this.transactions;
            }

            set
            {
                this.transactions = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected transaction.
        /// </summary>
        public StockTransactionData SelectedTransaction
        {
            get
            {
                return this.selectedTransaction;
            }

            set
            {
                this.selectedTransaction = value;

                if (this.Transactions == null)
                {
                    return;
                }

                foreach (var transaction in this.Transactions)
                {
                    transaction.LabelStatus = transaction.Deleted ? "Deleted" : transaction.IsBox ? "Box" : "Item";
                }

                if (value != null)
                {
                    //value.IsSelected = true;
                    value.LabelStatus = "IsSelected";
                }

                this.RaisePropertyChanged();

                if (this.callingModule == ViewType.Grader)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        if (value != null && value.Transaction != null && value.Transaction.StoredLabelID.HasValue)
                        {
                            var printerNo = this.DataManager.GetTransactionPrinter(value.Transaction.StoredLabelID.ToInt());
                            this.Locator.Touchscreen.UIText = string.Format("{0} {1}", Strings.Printer, printerNo);
                        }
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the group panel is displayed or not.
        /// </summary>
        public bool ShowGroupPanel
        {
            get
            {
                return this.showGroupPanel;
            }

            set
            {
                this.showGroupPanel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search string.
        /// </summary>
        public string SearchString
        {
            get
            {
                return this.searchString;
            }

            set
            {
                this.searchString = value;
                if (value != null)
                {
                    Messenger.Default.Send(value, Token.FilterTransactions);
                }

                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the terminal.
        /// </summary>
        public DeviceMaster Device
        {
            get
            {
                return this.device;
            }

            set
            {
                this.device = value;
                this.RaisePropertyChanged();
                this.HandleSelectedDevice();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle an unloaded event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a menu item selection.
        /// </summary>
        public ICommand MenuCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the group panel selection.
        /// </summary>
        public ICommand ShowGroupPanelCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the orders screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #region method

        public void SetTransaction(int id)
        {
            if (this.Transactions == null)
            {
                this.Transactions = new ObservableCollection<StockTransactionData>();
            }

            this.Transactions.Clear();
            var trans = this.DataManager.GetTransactionDataById(id);
            if (trans != null)
            {
                trans.LabelStatus = trans.Deleted ? "Deleted" : trans.IsBox ? "Box" : "Item";
                trans.LabelWeight = trans.Weight;
                trans.LabelQuantity = trans.Quantity;
                this.Transactions.Add(trans);
                this.SelectedTransaction = trans;
            }
        }

        /// <summary>
        /// Sets the delete mode.
        /// </summary>
        /// <param name="mode">The mode to set to.</param>
        public void SetDeleteMode(DeleteMode mode)
        {
            this.deleteMode = mode;
        }

        /// <summary>
        /// Sets the incoming transactions.
        /// </summary>
        /// <param name="transactions">The incoming transactions.</param>
        public void SetTransactions(IList<StockTransactionData> transactions)
        {
            this.showingLineTransactionsOnly = true;
            this.Transactions
                = new ObservableCollection<StockTransactionData>(transactions);

            foreach (var item in this.Transactions)
            {
                item.LabelStatus = item.Deleted ? "Deleted" : item.IsBox ? "Box" : "Item";
                item.LabelWeight = item.Weight;
                item.LabelQuantity = item.Quantity;
            }
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the ui loaded event.
        /// </summary>
        private void OnLoadedCommandExecute()
        {
            this.IsFormLoaded = true;
            this.ShowGroupPanel = false;
            this.SelectedTransaction = null;
            this.GetCallingModule();
            this.SetDevice();
        }

        /// <summary>
        /// Handles the menu item selection.
        /// </summary>
        /// <param name="menuItem">The menu item selected.</param>
        private void MenuCommandExecute(string menuItem)
        {
            if (menuItem.Equals(MenuItem.Delete))
            {
                this.DeleteTransaction();
                return;
            }

            if (menuItem.Equals(MenuItem.Undelete))
            {
                this.UndeleteTransaction();
                return;
            }

            if (menuItem.Equals(MenuItem.Reprint))
            {
                this.ReprintLabel();
                return;
            }

            if (menuItem.Equals(MenuItem.View))
            {
                this.ViewLabel();
                return;
            }

            if (menuItem.Equals(MenuItem.Find))
            {
                Messenger.Default.Send(Tuple.Create(ViewType.Keyboard, ViewType.TransactionDetails));
                return;
            }

            if (menuItem.Equals(MenuItem.TerminalName))
            {
                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.DeviceSearch);
            }

            if (menuItem.Equals(MenuItem.Edit))
            {
                var authorised = this.AuthorisationsManager.AllowUserToEditCarcass;
                if (!authorised)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToEditCarcass);
                    NouvemMessageBox.Show(Message.NotAuthorisedToEditCarcass, touchScreen: true);
                    return;
                }

                var carcassNo = 0;
                var attributeId = 0;
                if (this.selectedTransaction != null)
                {
                    attributeId = this.selectedTransaction.Transaction.AttributeID.ToInt();
                    carcassNo = this.DataManager.GetCarcassNo(attributeId);
                }

                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Touchscreen);
                Messenger.Default.Send(Tuple.Create(carcassNo, attributeId), Token.GraderEditMode);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Delete the selected transaction.
        /// </summary>
        private void DeleteTransaction()
        {
            this.Log.LogInfo(this.GetType(), "Attempting to delete a label");

            #region validation

            if (!this.AuthorisationsManager.AllowUserToDeleteLabel)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAuthorisationToDeleteLabel);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.NoAuthorisationToDeleteLabel, touchScreen: true);
                }));

                return;
            }

            if (this.SelectedTransaction == null)
            {
                this.Log.LogInfo(this.GetType(), "No label selected");
                SystemMessage.Write(MessageType.Issue, Message.NoTransactionSelected);
                return;
            }

            if (!this.AuthorisationsManager.AllowUserToDeleteLabelBySelectingTileInLabelHistory && !this.SelectedTransaction.RetrievedByScanning)
            {
                // label wasn't scanned, and user has no authorisation to delete non scanned labels.
                SystemMessage.Write(MessageType.Issue, Message.NoAuthorisationToDeleteNonScannedLabel);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.NoAuthorisationToDeleteNonScannedLabel, touchScreen: true);
                }));

                return;
            }

            #endregion

            Application.Current.Dispatcher.Invoke(() =>
            {
                NouvemMessageBox.Show(
                    string.Format(Message.DeleteLabelConfirmation, this.SelectedTransaction.Serial), NouvemMessageBoxButtons.YesNo, touchScreen: true);
            });
 
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            Messenger.Default.Send(Tuple.Create(this.SelectedTransaction.Serial, this.SelectedTransaction.Comments), Token.DeleteTransaction);
            this.HandleSelectedDevice();
        }

        /// <summary>
        /// Delete the selected transaction.
        /// </summary>
        private void UndeleteTransaction()
        {
            this.Log.LogInfo(this.GetType(), "Attempting to undelete a label");

            #region validation

            if (!this.AuthorisationsManager.AllowUserToDeleteLabel)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAuthorisationToUndeleteLabels);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.NoAuthorisationToUndeleteLabels, touchScreen: true);
                }));
            }

            if (this.SelectedTransaction == null)
            {
                this.Log.LogInfo(this.GetType(), "No label selected");
                SystemMessage.Write(MessageType.Issue, Message.NoTransactionSelected);
                return;
            }

            if (!this.SelectedTransaction.Deleted)
            {
                SystemMessage.Write(MessageType.Issue, Message.LabelCannotBeDeleted);
                return;
            }

            #endregion

            Application.Current.Dispatcher.Invoke(() =>
            {
                NouvemMessageBox.Show(
                    string.Format(Message.UndeleteLabelConfirmation, this.SelectedTransaction.Serial), NouvemMessageBoxButtons.YesNo, touchScreen: true);
            });

            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            Messenger.Default.Send(this.SelectedTransaction.StocktransactionID, Token.UndeleteTransaction);
            this.HandleSelectedDevice();
        }

        /// <summary>
        /// Handler for the selected device.
        /// </summary>
        private void HandleSelectedDevice()
        {
            transactionTypeId = 0;
            if (this.Device != null && !this.GetDeviceOnly)
            {
                if (this.callingModule == ViewType.Grader)
                {
                    transactionTypeId = NouvemGlobal.TransactionTypeGoodsReceiptId;
                }
                else if (this.callingModule == ViewType.Sequencer)
                {
                    transactionTypeId = 1;
                }
                else if (this.callingModule == ViewType.IntoProduction)
                {
                    transactionTypeId = NouvemGlobal.TransactionTypeProductionIssueId;
                }
                else if (this.callingModule == ViewType.OutOfProduction)
                {
                    transactionTypeId = NouvemGlobal.TransactionTypeProductionReceiptId;
                }
                else if (this.callingModule == ViewType.ARDispatch)
                {
                    transactionTypeId = NouvemGlobal.TransactionTypeGoodsDeliveryId;
                }
                else if (this.callingModule == ViewType.APReceipt)
                {
                    transactionTypeId = NouvemGlobal.TransactionTypeGoodsReceiptId;
                }

                if (this.showingLineTransactionsOnly)
                {
                    this.showingLineTransactionsOnly = false;
                    return;
                }

                this.Transactions
                    = new ObservableCollection<StockTransactionData>(
                        this.DataManager.GetTransactions(this.device.DeviceID, transactionTypeId));

                foreach (var item in this.Transactions)
                {
                    item.LabelStatus = item.Deleted ? "Deleted" : item.IsBox ? "Box" : "Item";
                    item.LabelWeight = item.Weight;
                    item.LabelQuantity = item.Quantity;
                }
            }
        }

        /// <summary>
        /// Reprints the selected transaction.
        /// </summary>
        private void ReprintLabel()
        {
            if (this.SelectedTransaction != null)
            {
                if (this.callingModule == ViewType.Grader)
                {
                    NouvemMessageBox.Show(Message.ReEnterWeightPrompt, NouvemMessageBoxButtons.YesNo, touchScreen:true);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        Keypad.Show(KeypadTarget.GraderWeightChange);
                        return;
                    }
                }

                try
                {
                    this.PrintManager.ReprintLabel(this.selectedTransaction.Transaction.StockTransactionID);
                    SystemMessage.Write(MessageType.Priority, Message.LabelReprinted);
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }
            }
        }

        /// <summary>
        /// Reprints the selected transaction.
        /// </summary>
        private void ViewLabel()
        {
            if (this.SelectedTransaction != null)
            {
                try
                {
                    this.PrintManager.ReprintLabel(this.selectedTransaction.Transaction.StockTransactionID);
                    SystemMessage.Write(MessageType.Priority, Message.LabelReprinted);
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }
            }
        }

        /// <summary>
        /// Sets the local device data.
        /// </summary>
        private void SetDevice()
        {
            if (NouvemGlobal.LocalDevice != null)
            {
                this.Device = NouvemGlobal.LocalDevice;
            }
        }

        /// <summary>
        /// Handles a scanned label for selection.
        /// </summary>
        /// <param name="serial">The scanned barcode.</param>
        private void HandleLabelSearch(string serial)
        {
            if (!string.IsNullOrEmpty(serial))
            {
                this.Transactions?.Clear();
                var localLabel = serial.ToInt();
                if (localLabel == 0)
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.InvalidLabel, serial));
                    return;
                }

                var labels = this.DataManager.GetTransaction(serial.ToInt(), this.transactionTypeId);
                if (labels.Count == 0)
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.LabelNotFound, serial));
                    return;
                }

                foreach (var label in labels)
                {
                    label.LabelStatus = label.Deleted ? "Deleted" : label.IsBox ? "Box" : "Item";
                    label.LabelWeight = label.Weight;
                    label.LabelQuantity = label.Quantity;
                    label.RetrievedByScanning = true;
                }

                this.Transactions = null;
                this.Transactions = new ObservableCollection<StockTransactionData>(labels);
            }
        }

        /// <summary>
        /// Gets the module that called the menu.
        /// </summary>
        /// <returns>The calling module.</returns>
        private void GetCallingModule()
        {
            if (!ViewModelLocator.IsAPReceiptTouchscreenNull())
            {
                this.callingModule = ViewType.APReceipt;
                return;
            }

            if (!ViewModelLocator.IsIntoProductionNull())
            {
                this.callingModule = ViewType.IntoProduction;
                return;
            }

            if (!ViewModelLocator.IsOutOfProductionNull())
            {
                this.callingModule = ViewType.OutOfProduction;
                return;
            }

            if (!ViewModelLocator.IsGraderNull())
            {
                this.callingModule = ViewType.Grader;
                return;
            }

            if (!ViewModelLocator.IsSequencerNull())
            {
                this.callingModule = ViewType.Sequencer;
                return;
            }

            this.callingModule = ViewType.ARDispatch;
        }

        /// <summary>
        /// Handles a change of a grader label weight.
        /// </summary>
        /// <param name="weight">The changed weight.</param>
        private void HandleGraderWeightChange(decimal weight)
        {
            if (this.SelectedTransaction == null)
            {
                return;
            }

            this.SelectedTransaction.Transaction.TransactionWeight = weight;
            this.SelectedTransaction.Transaction.GrossWeight = weight +
                                                               this.SelectedTransaction.Transaction.Tare.ToDecimal();
            var result = this.DataManager.ChangeTransactionWeight(this.SelectedTransaction.Transaction);
            if (result.Item1)
            {
                this.SelectedTransaction.LabelWeight = weight;
                Messenger.Default.Send(Tuple.Create(this.SelectedTransaction.Transaction.StockTransactionID, 
                    this.SelectedTransaction.Transaction.BPMasterID, this.SelectedTransaction.Transaction.Reference.ToInt(),result.Item2,result.Item3,result.Item4),Token.ReprintLabel);
                this.HandleSelectedDevice();
            }
        }

        #endregion

        #endregion
    }
}

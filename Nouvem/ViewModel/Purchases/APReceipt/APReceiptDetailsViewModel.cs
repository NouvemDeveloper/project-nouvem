﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptDetailsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.ViewModel.Attribute;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class APReceiptDetailsViewModel : AttributesViewModel
    {
        #region field

        /// <summary>
        /// The current batch traceability vm in focus.
        /// </summary>
        private NouvemViewModelBase currentTraceabilityViewModel;

        /// <summary>
        /// Can we edit flag.
        /// </summary>
        private bool isReadOnly;

        /// <summary>
        /// Select all docket transactions flag.
        /// </summary>
        private bool selectAll;

        /// <summary>
        /// The incoming sale detail
        /// </summary>
        private SaleDetail selectedSaleDetail;

        /// <summary>
        /// The application traceability data.
        /// </summary>
        protected Dictionary<Tuple<int, string>, List<TraceabilityData>> traceabilityData;

        /// <summary>
        /// The goods in details.
        /// </summary>
        private ObservableCollection<GoodsIn> goodsInDetails;

        /// <summary>
        /// Apply intake transactions group price flag.
        /// </summary>
        private bool applyGroupPrice;

        /// <summary>
        /// The selected goods in detail.
        /// </summary>
        private GoodsIn selectedGoodsInDetail;

        /// <summary>
        /// The batch traceability grid height.
        /// </summary>
        private double batchTraceabilityGridHeight;

        /// <summary>
        /// The incoming item number.
        /// </summary>
        private string itemNo;

        /// <summary>
        /// The incoming item description.
        /// </summary>
        private string itemDescription;

        /// <summary>
        /// The incoming item description.
        /// </summary>
        private string docketNo;

        /// <summary>
        /// The current batch.
        /// </summary>
        private BatchNumber batchNumber;

        /// <summary>
        /// The next batch number to be applied.
        /// </summary>
        private BatchNumber nextBatchNumber;

        /// <summary>
        /// The stored transaction wgt.
        /// </summary>
        protected decimal? storedTransactionWeight = 0;

        /// <summary>
        /// The wft difference between the stored transaction wgt and the edited transaction wgt.
        /// </summary>
        protected decimal? weightDifference = 0;

        protected IList<GoodsIn> transactionsToUpdate = new List<GoodsIn>();

        #endregion

        #region constructor

         /// <summary>
        /// Initializes a new instance of the <see cref="APReceiptDetailsViewModel"/> class.
        /// </summary>
        public APReceiptDetailsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command

            this.PrintCommand = new RelayCommand(this.PrintCommandExecute);

            // On new row generation, add the batch number.
            this.NewRowCommand = new RelayCommand(() => this.SelectedGoodsInDetail.BatchNumber = this.BatchNumber);

            // Handle a cell value change i.e. allow edit of recorded transaction only if user is authorised.
            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>(this.UpdateCommandExecute, e => this.HasWriteAuthorisation && this.SelectedGoodsInDetail != null && this.SelectedGoodsInDetail.SerialNumber != null);

            #endregion
       
            this.GoodsInDetails = new ObservableCollection<GoodsIn>();
            this.GetAttributeLookUps();
            this.GetProcesses();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether this is a returns document.
        /// </summary>
        public bool IsReturn { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we can edit.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.isReadOnly;
            }

            set
            {
                this.isReadOnly = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are selecting all a dockets transactions, or just the lines.
        /// </summary>
        public bool SelectAll
        {
            get
            {
                return this.selectAll;
            }

            set
            {
                this.selectAll = value;
                this.RaisePropertyChanged();
                this.HandleSaleDetail();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a price change is to be applied to all intake transactions.
        /// </summary>
        public bool ApplyGroupPrice
        {
            get
            {
                return this.applyGroupPrice;
            }

            set
            {
                this.applyGroupPrice = value;
                this.RaisePropertyChanged();

                if (!this.EntitySelectionChange)
                {
                    this.ApplyGroupPriceToIntake();
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public GoodsIn SelectedGoodsInDetail
        {
            get
            {
                return this.selectedGoodsInDetail;
            }

            set
            {
                this.selectedGoodsInDetail = value;
                this.RaisePropertyChanged();

                if (value != null && value.BatchNumber != null)
                {
                    if (value.BatchNumber.Number != this.BatchNumber.Number)
                    {
                        // row selected belongs to different batch than current batch, so batch controls need to be switched.
                        Messenger.Default.Send(value.BatchNumber, Token.SwitchBatchControls);
                        this.BatchNumber = value.BatchNumber;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the teed up next batch number.
        /// </summary>
        public BatchNumber NextBatchNumber
        {
            get
            {
                return this.nextBatchNumber;
            }

            set
            {
                this.nextBatchNumber = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    Messenger.Default.Send(value, Token.BatchNumber);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber
        {
            get
            {
                return this.batchNumber;
            }

            set
            {
                this.batchNumber = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    Messenger.Default.Send(value, Token.BatchNumber);
                }
            }
        }

        /// <summary>
        /// Gets or sets the incoming item number.
        /// </summary>
        public string ItemNo
        {
            get
            {
                return this.itemNo;
            }

            set
            {
                this.itemNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the incoming item number.
        /// </summary>
        public string DocketNo
        {
            get
            {
                return this.docketNo;
            }

            set
            {
                this.docketNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the incoming item description.
        /// </summary>
        public string ItemDescription
        {
            get
            {
                return this.itemDescription;
            }

            set
            {
                this.itemDescription = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the dispatch items details.
        /// </summary>
        public ObservableCollection<GoodsIn> GoodsInDetails
        {
            get
            {
                return this.goodsInDetails;
            }

            set
            {
                this.goodsInDetails = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the batch traceability grid height.
        /// </summary>
        public double BatchTraceabilityGridHeight
        {
            get
            {
                return this.batchTraceabilityGridHeight;
            }

            set
            {
                this.batchTraceabilityGridHeight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sale detail used for editing transactions.
        /// </summary>
        public SaleDetail SaleDetailEdited { get; set; }

        /// <summary>
        /// Gets or sets the sale detail.
        /// </summary>
        public SaleDetail SelectedSaleDetail
        {
            get
            {
                return this.selectedSaleDetail;
            }

            set
            {
                this.selectedSaleDetail = value;
                this.RaisePropertyChanged();
                this.HandleSaleDetail(); 
            }
        }

        private ColumnBase currentColumn;

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ColumnBase CurrentColumn
        {
            get
            {
                return this.currentColumn;
            }

            set
            {
                this.currentColumn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current batch traceabilty vm in focus.
        /// </summary>
        public NouvemViewModelBase CurrentTraceabilityViewModel
        {
            get
            {
                return this.currentTraceabilityViewModel;
            }

            set
            {
                this.currentTraceabilityViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        public ICommand PrintCommand { get; set; }

        /// <summary>
        /// Gets the command to update a transaction edit.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a new row event.
        /// </summary>
        public ICommand NewRowCommand { get; private set; }

        /// <summary>
        /// Gets the command to generate a new batch number.
        /// </summary>
        public ICommand GenerateNewBatchCommand { get; private set; }

        /// <summary>
        /// Gets the command to record a transaction.
        /// </summary>
        public ICommand RecordTransactionCommand { get; private set; }

        #endregion

        #region method

      
        #endregion

        #endregion

        #region protected

        #region virtual

        /// <summary>
        /// Handles a cell value change.
        /// </summary>
        /// <param name="e">The event arg.</param>
        protected virtual void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            #region validation

            if (e == null || e.Row == null)
            {
                return;
            }

            #endregion

            if (e.Row is GoodsIn)
            {
                if (this.storedTransactionWeight == 0)
                {
                    this.storedTransactionWeight = (e.Row as GoodsIn).NettWeight.ToDecimal();
                }

                this.SetControlMode(ControlMode.Update);
            }

            if (this.IsReadOnly)
            {
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                if (this.SelectedGoodsInDetail != null)
                {
                    var exists = this.transactionsToUpdate.FirstOrDefault(x =>
                        x.StockTransactionID == this.SelectedGoodsInDetail.StockTransactionID);
                    if (exists != null)
                    {
                        this.transactionsToUpdate.Remove(exists);
                    }

                    this.transactionsToUpdate.Add(this.SelectedGoodsInDetail);
                }
            }
        }

        /// <summary>
        /// Update the transaction weights.
        /// </summary>
        protected virtual void UpdateWeights()
        {
            if (!this.AuthorisationsManager.AllowUserToEditTransactions)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.Locator.APReceipt.IsBaseDocument)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.DataManager.UpdateTransactionWeight(this.transactionsToUpdate, ViewType.ARDispatch))
            {
                if (this.transactionsToUpdate.Any(x => x.Delete))
                {
                    var localData = this.GoodsInDetails.Where(x => !x.Delete);
                    this.GoodsInDetails = new ObservableCollection<GoodsIn>(localData);
                }
               
                SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
                this.transactionsToUpdate.Clear();
                Messenger.Default.Send(Token.Message, Token.RefreshMasterDocument);
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
            }

            //this.SelectedSaleDetail.SetPriceMethod();
            //foreach (var detail in this.GoodsInDetails)
            //{
            //    var stockId = detail.StockTransactionID;
            //    foreach (var transData in this.SelectedSaleDetail.GoodsReceiptDatas.Where(x => x != null).Select(x => x.TransactionData))
            //    {
            //        foreach (var transaction in transData.Select(x => x.Transaction))
            //        {
            //            if (transaction.StockTransactionID == stockId)
            //            {
            //                var weightdifference = detail.NettWeight - transaction.TransactionWeight.ToDouble();

            //                if (weightdifference < 0 || weightdifference > 0)
            //                {
            //                    transaction.GrossWeight = detail.GrossWeight.ToDecimal();
            //                    transaction.TransactionWeight = detail.NettWeight.ToDecimal();
            //                    this.SelectedSaleDetail.WeightReceived += weightdifference;
            //                }

            //                transaction.Price = detail.Price;
            //            }
            //        }
            //    }
            //}

            //if (this.DataManager.UpdateTransactionWeight(this.SelectedSaleDetail, ViewType.APReceipt))
            //{
            //    //this.Locator.APReceipt.UpdateSaleData();
            //    SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
            //    this.Close();
            //}
            //else
            //{
            //    SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
            //}
        }

        /// <summary>
        /// Handle the incoming sale detail.
        /// </summary>
        protected virtual void HandleSaleDetail()
        {
            this.HandleDisplaySaleDetail();
        }

        /// <summary>
        /// Direct the incoming sale detail.
        /// </summary>
        protected virtual void DirectSaleDetail()
        {
            this.HandleSaleDetail();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected virtual void Close()
        {
            this.EntitySelectionChange = true;
            this.ApplyGroupPrice = false;
            this.EntitySelectionChange = false;
            Messenger.Default.Send(true, Token.CloseAPReceiptDetailsWindow);
        }
       
        #endregion

        #region override

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    //this.AddSale();
                    break;

                case ControlMode.Find:
                   // this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateWeights();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region method
       
        /// <summary>
        /// Handle the incoming sale detail.
        /// </summary>
        protected virtual void HandleDisplaySaleDetail(int transactionTypeid = 2)
        {
            if (this.IsReturn)
            {
                transactionTypeid = 9;
            }

            var item = this.SelectedSaleDetail.InventoryItem.Master;
            this.DocketNo = this.SelectedSaleDetail.DocketNo.ToString();
            var docketid = 0;
            if (this.selectAll)
            {
                docketid = this.selectedSaleDetail.SaleID;
                this.ItemNo = Strings.AllOrderLines;
                this.ItemDescription = Strings.AllOrderLines;
            }
            else
            {
                this.ItemNo = item.Code;
                this.ItemDescription = item.Name;
            }

            Messenger.Default.Send(this.selectAll, Token.ShowGridProduct);
            var stockData = this.DataManager.GetOrderLineTransactionData(this.SelectedSaleDetail.SaleDetailID, transactionTypeid, docketid);
            var boxData = stockData.Where(x => !x.StockTransactionID_Container.HasValue);
            this.GoodsInDetails.Clear();
            var uiData = new CollectionData();

            foreach (var localData in stockData)
            {
                if (!string.IsNullOrEmpty(localData.Attribute1)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute1")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute1", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute2)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute2")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute2", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute3)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute3")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute3", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute4)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute4")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute4", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute5)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute5")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute5", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute6)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute6")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute6", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute7)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute7")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute7", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute8)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute8")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute8", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute9)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute9")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute9", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute10)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute10")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute10", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute11)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute11")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute11", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute12)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute12")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute12", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute13)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute13")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute13", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute14)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute14")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute14", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute15)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute15")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute15", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute16)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute16")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute16", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute17)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute17")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute17", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute18)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute18")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute18", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute19)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute19")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute19", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute20)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute20")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute20", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute21)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute21")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute21", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute22)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute22")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute22", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute23)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute23")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute23", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute24)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute24")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute24", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute25)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute25")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute25", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute26)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute26")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute26", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute27)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute27")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute27", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute28)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute28")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute28", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute29)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute29")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute29", name.AttributeDescription, string.Empty)); } }
                if (!string.IsNullOrEmpty(localData.Attribute30)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute30")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute30", name.AttributeDescription, string.Empty)); } }

            }

            Messenger.Default.Send(uiData, Token.Macro);

            foreach (var data in boxData)
            {
                
                uiData.RowData.Add(Tuple.Create(string.Empty,"", string.Empty));
                this.GoodsInDetails.Add(new GoodsIn
                {
                    StockTransactionID = data.StockTransactionID,
                    SerialNumber = data.Serial,
                    BatchNumberID = data.BatchNumberID.ToInt(),
                    NettWeight = data.TransactionWeight.ToDouble(),
                    INMasterID = data.INMasterID,
                    TareWeight = data.Tare.ToDouble(),
                    Qty = data.TransactionQTY.ToInt(),
                    CreationDate = data.TransactionDate,
                    User = data.FullName,
                    Device = data.DeviceMAC,
                    Price = data.Price,
                    Product = data.Product,
                    Item1 = data.Attribute1,
                    Item2 = data.Attribute2,
                    Item3 = data.Attribute3,
                    Item4 = data.Attribute4,
                    Item5 = data.Attribute5,
                    Item6 = data.Attribute6,
                    Item7 = data.Attribute7,
                    Item8 = data.Attribute8,
                    Item9 = data.Attribute9,
                    Item10 = data.Attribute10,
                    Item11 = data.Attribute11,
                    Item12 = data.Attribute12,
                    Item13 = data.Attribute13,
                    Item14 = data.Attribute14,
                    Item15 = data.Attribute15,
                    Item16 = data.Attribute16,
                    Item17 = data.Attribute17,
                    Item18 = data.Attribute18,
                    Item19 = data.Attribute19,
                    Item20 = data.Attribute20,
                    Item21 = data.Attribute21,
                    Item22 = data.Attribute22,
                    Item23 = data.Attribute23,
                    Item24 = data.Attribute24,
                    Item25 = data.Attribute25,
                    Item26 = data.Attribute26,
                    Item27 = data.Attribute27,
                    Item28 = data.Attribute28,
                    Item29 = data.Attribute29,
                    Item30 = data.Attribute30,
                    PieceLabels = (from boxItem in stockData.Where(x => x.StockTransactionID_Container == data.Serial)
                                   select new GoodsIn
                                   {
                                       StockTransactionID = boxItem.StockTransactionID,
                                       SerialNumber = boxItem.Serial,
                                       NettWeight = boxItem.TransactionWeight.ToDouble(),
                                       TareWeight = boxItem.Tare.ToDouble(),
                                       INMasterID = data.INMasterID,
                                       Qty = data.TransactionQTY.ToInt(),
                                       CreationDate = data.TransactionDate,
                                       User = data.FullName,
                                       Device = data.DeviceMAC,
                                       Price = boxItem.Price,
                                       Item1 = data.Attribute1,
                                       Item2 = data.Attribute2,
                                       Item3 = data.Attribute3,
                                       Item4 = data.Attribute4,
                                       Item5 = data.Attribute5,
                                       Item6 = data.Attribute6,
                                       Item7 = data.Attribute7,
                                       Item8 = data.Attribute8,
                                       Item9 = data.Attribute9,
                                       Item10 = data.Attribute10,
                                       Item11 = data.Attribute11,
                                       Item12 = data.Attribute12,
                                       Item13 = data.Attribute13,
                                       Item14 = data.Attribute14,
                                       Item15 = data.Attribute15,
                                       Item16 = data.Attribute16,
                                       Item17 = data.Attribute17,
                                       Item18 = data.Attribute18,
                                       Item19 = data.Attribute19,
                                       Item20 = data.Attribute20,
                                       Item21 = data.Attribute21,
                                       Item22 = data.Attribute22,
                                       Item23 = data.Attribute23,
                                       Item24 = data.Attribute24,
                                       Item25 = data.Attribute25,
                                       Item26 = data.Attribute26,
                                       Item27 = data.Attribute27,
                                       Item28 = data.Attribute28,
                                       Item29 = data.Attribute29,
                                       Item30 = data.Attribute30,
                                   }).ToList()
                });
            }
        }

        #endregion

        #endregion

        #region private

        #region command execution

        protected virtual void PrintCommandExecute()
        {
            try
            {
                if (this.SelectedGoodsInDetail != null)
                {
                    int? partnerGroupId = null;
                    int? productGroupId = null;
                    var transaction = this.DataManager.GetStockTransactionBySerial(this.SelectedGoodsInDetail.SerialNumber.ToInt(), 2);
                    var localPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == transaction.Transaction.BPMasterID);
                    if (localPartner != null)
                    {
                        partnerGroupId = localPartner.Details.BPGroupID;
                    }

                    var localProduct =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == transaction.INMasterID);
                    if (localProduct != null)
                    {
                        productGroupId = localProduct.Master.INGroupID;
                    }

                    var process = this.Processes.FirstOrDefault(x => x.ProcessID == transaction.Transaction.ProcessID);
                    var labels = this.PrintManager.ProcessPrinting(transaction.BPMasterID,
                        partnerGroupId, transaction.INMasterID, productGroupId,
                        ViewType.APReceipt, LabelType.Item, transaction.StockTransactionID.ToInt(),
                        1, warehouseId: transaction.Transaction.WarehouseID, process: process);
                    if (labels.Item1.Any())
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(transaction.StockTransactionID.ToInt(), string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                    }

                    SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelPrinted, transaction.Serial.ToInt()));
                    return;
                }
            }
            catch (Exception e)
            {
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
           
        }

        #endregion

        #region helper

        /// <summary>
        /// Applys a group price to the selected intake transactions.
        /// </summary>
        private void ApplyGroupPriceToIntake()
        {
            if (this.SelectedGoodsInDetail != null && this.SelectedGoodsInDetail.Price.HasValue)
            {
                var localPrice = this.SelectedGoodsInDetail.Price;
                foreach (var goodsInDetail in this.GoodsInDetails)
                {
                    goodsInDetail.Price = this.applyGroupPrice ? localPrice : null;
                    var exists = this.transactionsToUpdate.FirstOrDefault(x =>
                        x.StockTransactionID == goodsInDetail.StockTransactionID);
                    if (exists != null)
                    {
                        this.transactionsToUpdate.Remove(exists);
                    }

                    this.transactionsToUpdate.Add(goodsInDetail);
                }
            }
        }
  
        #endregion

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="APOrderViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Purchases.APOrder
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class APOrderViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// Flag, as to whether the order is to be auto copied to a goods receipt.
        /// </summary>
        private bool autoCopyToGoodsIn;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APOrderViewModel"/> class.
        /// </summary>
        public APOrderViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            Messenger.Default.Register<string>(this, Token.ClosingPurchaseOrderWindow, s =>
            {
                this.Close();
            });

            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.APOrder)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID };
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.APOrder)
                    {
                        this.SelectedPartner =
                            this.Suppliers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            #endregion

            #region command handler

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(true, ViewType.APOrder)),
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() =>
            {
                var att = this.SelectedSaleDetail;
                this.RemoveItem(ViewType.APOrder);
            });

            #endregion

            this.GetLocalDocNumberings();
            this.DocketNoteName = Strings.IntakeDocketNote;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether an order is to be auto copied to a receipt.
        /// </summary>
        public bool AutoCopyToGoodsIn
        {
            get
            {
                return this.autoCopyToGoodsIn;
            }

            set
            {
                this.autoCopyToGoodsIn = value;

                if (value)
                {
                    if (!this.AuthorisationsManager.AllowUserToCompletePurchaseOrders)
                    {
                        if (this.IsFormLoaded)
                        {
                            SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToAutoCopyToIntake);
                        }
                
                        this.autoCopyToGoodsIn = false;
                    }
                }

                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Show the sales.
        /// </summary>
        public void ShowAllSales()
        {
            this.FindSaleCommandExecute();
        }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Adds an iactive customer if retrieved on historic docket.
        /// </summary>
        /// <param name="partnerId"></param>
        protected override void AddInactiveSelectedPartner(int partnerId)
        {
            if (!this.Suppliers.Any(x => x.BPMasterID == partnerId))
            {
                var localPartner = NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == partnerId);
                if (localPartner != null)
                {
                    this.Suppliers.Add(localPartner.Details);
                }
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(78, this.Sale.SaleID.ToString(), !preview);
        }

        /// <summary>
        /// Gets the live price data when using recent orders.
        /// </summary>
        /// <param name="saleDetail">The recent order data.</param>
        protected override void GetLiveRecentOrderPrice(SaleDetail saleDetail)
        {
            if (this.SelectedPartner == null || saleDetail == null)
            {
                return;
            }

            var data = this.DataManager.GetPriceListDetailByPartner(this.SelectedPartner.BPMasterID, saleDetail.INMasterID, NouvemGlobal.CostPriceList.PriceListID);
            if (data != null)
            {
                saleDetail.LoadingSale = true;
                saleDetail.UnitPrice = data.Price;
                saleDetail.PriceListID = data.PriceListID;
            }
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.Sale = this.DataManager.GetAPOrderByID(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetAPOrderByLastEdit();
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                if (this.Sale.BaseDocumentReferenceID.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                    return;
                }

                this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.AutoCopyToGoodsIn = ApplicationSettings.AutoCopyToGoodsIn || ApplicationSettings.AlwaysCopyToGoodsIn;
            base.OnLoadingCommandExecute();
            this.ClearForm();

            Messenger.Default.Register<string>(this, Token.CopySuccessful, s =>
            {
                if (this.AutoCopying)
                {
                    SystemMessage.Write(MessageType.Priority, Message.OrderAndIntakeCreated);
                }
            });
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            base.OnUnloadedCommandExecute();
            ApplicationSettings.AutoCopyToGoodsIn = this.AutoCopyToGoodsIn;
            Messenger.Default.Unregister<string>(this, Token.CopySuccessful);
        }

        /// <summary>
        /// Override, to set the focus to the order vm.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if (e.Cell.Property.Equals("INMasterID") &&
                this.SelectedSaleDetail != null && this.SelectedSaleDetail.SaleDetailID == 0)
            {
                this.SelectedSaleDetail.UnitPrice = null;
            }
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                this.ShowSearchGridCommandExecute(Constant.APOrder);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllAPOrders(orderStatuses, ApplicationSettings.SalesSearchFromDate, DateTime.Today);

            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllAPOrders(orderStatuses, ApplicationSettings.SalesSearchFromDate, DateTime.Today);
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.APOrder), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.APOrder);
            }
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            //base.ParsePartner();

            // now get the associated customer quotes.
            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            var supplier = this.DataManager.GetBusinessPartner(this.SelectedPartner.BPMasterID);

            if (supplier != null)
            {
                this.PartnerCurrency =
                    supplier.PartnerCurrency != null && !string.IsNullOrEmpty(supplier.PartnerCurrency.Symbol)
                        ? supplier.PartnerCurrency.Symbol
                        : ApplicationSettings.DefaultCurrency;

                supplier.Contacts = this.DataManager.GetBusinessPartnerContactsById(supplier.Details.BPMasterID);
                if (supplier.Contacts != null)
                {
                    this.Contacts = new ObservableCollection<BusinessPartnerContact>(supplier.Contacts);
                    var primaryContact = supplier.Contacts.FirstOrDefault(x => x.Details.PrimaryContact == true);

                    if (primaryContact != null)
                    {
                        this.SelectedContact = primaryContact;
                    }
                }

                if (supplier.Addresses != null)
                {
                    this.Addresses = new ObservableCollection<BusinessPartnerAddress>(supplier.Addresses);
                    var billingAddress = supplier.Addresses.FirstOrDefault(x => x.Details.Billing == true);
                    var deliveryAddress = supplier.Addresses.FirstOrDefault(x => x.Details.Shipping == true);

                    if (billingAddress != null)
                    {
                        this.SelectedInvoiceAddress = billingAddress;
                    }

                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                    }
                }

                this.SelectedPartner.PopUpNotes = supplier.Details.PopUpNotes;
                this.CustomerPopUpNote = supplier.Details.PopUpNotes;

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.ShowPartnerPopUpNoteAtDesktopModules)
                {
                    if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                            //this.CustomerPopUpNote = string.Empty;
                        }));
                    }
                }
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetRecentPurchaseOrders(localPartner.BPMasterID);
                this.RecentOrdersCount = 0;
                this.SetRecentOrders(true);
            }));
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.RefreshSales();
            }));
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Overide the selected sale, to reinstate the sale id.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            #region validation

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdatePurchaseOrders)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }
            else if (!string.IsNullOrEmpty(this.DeliveryTime) && !this.DeliveryTime.IsValidTime())
            {
                error = Message.InvalidTime;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            try
            {
                this.CreateSale();
                this.UpdateDocNumbering();
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
                var newSaleId = this.DataManager.AddAPOrder(this.Sale);
                if (newSaleId > 0)
                {
                    this.Sale.SaleID = newSaleId;
                    this.AllSales.Add(this.Sale);
                    this.Log.LogDebug(this.GetType(), string.Format("AddSale(): Auto Copy:{0}, Always Copy:{1}", this.AutoCopyToGoodsIn, ApplicationSettings.AlwaysCopyToGoodsIn));
                    if (this.AutoCopyToGoodsIn || ApplicationSettings.AlwaysCopyToGoodsIn)
                    {
                        ProgressBar.Run();
                        this.Log.LogDebug(this.GetType(), "AddSale(): Auto copying to a goods in receipt");
                        this.AutoCopyOrderToGoodsIn();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCreated, this.NextNumber));
                    }

                    //this.RefreshCustomerSales();
                    this.ClearForm();
                    this.NextNumber = this.SelectedDocNumbering.NextNumber;
                    this.RefreshDocStatusItems();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.OrderNotCreated);
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdatePurchaseOrders)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            string error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }
            else if (!string.IsNullOrEmpty(this.DeliveryTime) && !this.DeliveryTime.IsValidTime())
            {
                error = Message.InvalidTime;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var cancelIntake = this.SelectedDocStatus != null && this.SelectedDocStatus.NouDocStatusID ==
                                        NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
            var updateIntake = this.Sale.NouDocStatusID ==
                                          NouvemGlobal.NouDocStatusActive.NouDocStatusID || (this.Sale.NouDocStatusID ==
                               NouvemGlobal.NouDocStatusCancelled.NouDocStatusID && this.SelectedDocStatus != null
                               && this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);

            if (this.SelectedDocStatus != null && this.SelectedDocStatus.NouDocStatusID ==
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                if (!this.AuthorisationsManager.AllowUserToCompletePurchaseOrders)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToCompletePurchaseOrders);
                    this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x =>
                        x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
                    //Application.Current.Dispatcher.Invoke(() =>
                    //{
                    //    NouvemMessageBox.Show(Message.NotAuthorisedToCompletePurchaseOrders);
                    //});

                    return;
                }
            }

            this.CreateSale();
            if (this.DataManager.UpdateAPOrder(this.Sale))
            {
                if (cancelIntake)
                {
                    this.DataManager.CancelIntakeFromOrder(this.Sale.SaleID);
                }
                else if (updateIntake)
                {
                    var intakeId = this.DataManager.GetIntakeIdForOrder(this.Sale.SaleID);
                    if (intakeId > 0)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Updating intake docket:{0}", intakeId));
                        this.Sale.ParentID = intakeId;
                        this.DataManager.UpdateIntakeFromOrder(this.Sale);
                    }
                    else
                    {
                        if (this.AutoCopyToGoodsIn || ApplicationSettings.AlwaysCopyToGoodsIn)
                        {
                            try
                            {
                                ProgressBar.SetUp(0, 3);
                                ProgressBar.Run();
                                this.AutoCopyOrderToGoodsIn();
                            }
                            finally
                            {
                                ProgressBar.Reset();
                            }
                        }
                    }
                }

                if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.NextNumber));
                    this.ClearForm();
                }
                else if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.NextNumber));
                    this.ClearForm();
                }
                else if (this.SelectedDocStatus != null &&
                    this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCancelled, this.NextNumber));
                    this.ClearForm();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderNotUpdated, this.NextNumber));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.APOrder)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        protected override void AllowWindowToClose()
        {
            this.CanCloseForm = true;
            Messenger.Default.Send(true, Token.CanClosePurchaseOrderWindow);
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (!this.CanCloseForm)
            {
                this.CheckForUnsavedData();
                return;
            }

            ViewModelLocator.ClearAPOrder();          
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseAPOrderWindow);
                ViewModelLocator.ClearAPReceipt();
            }));
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            #region validation

            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (!this.AuthorisationsManager.AllowUserToCompletePurchaseOrders)
            {
                SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToAutoCopyToIntake);
                return;
            }

            #endregion

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderSelected);
                return;
            }

            #endregion

            if (this.SelectedDocumentCopyTo.Equals(Strings.APGoodsIn))
            {
                CopyingDocument = true;
                this.CopyOrderToGoodsIn();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(RibbonCommand.LastEdit, Token.NavigationCommand);
                }));
            }
        }

        /// <summary>
        /// Removes an item from the grid.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            this.RemoveItem(ViewType.APOrder);
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var documents = this.DataManager.GetAPQuotes(this.SelectedPartner.BPMasterID);

            if (!documents.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Orders));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(documents, ViewType.APOrder), Token.SearchForSale);
            CopyingDocument = true;
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        #endregion

        #region helper

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.APOrder)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllAPOrders(orderStatuses, ApplicationSettings.SalesSearchFromDate, ApplicationSettings.SalesSearchToDate);
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllAPOrders(orderStatuses, ApplicationSettings.SalesSearchFromDate, ApplicationSettings.SalesSearchToDate);
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.APOrder);
            }
        }

        /// <summary>
        /// Displays the base document.
        /// </summary>
        private void ShowBaseDocument()
        {
            try
            {
                var baseDoc = this.DataManager.GetAPQuoteByID(this.Sale.BaseDocumentReferenceID.ToInt());
                Messenger.Default.Send(ViewType.APQuote);
                this.Locator.APQuote.Sale = baseDoc;
                SystemMessage.Write(MessageType.Priority, Message.BaseDocumentLoaded);
            }
            finally
            {
                //this.Locator.ARDispatch.DisplayModeOnly = false;
            }
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.APOrder);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        /// <summary>
        /// Copies the current order to a goods in receipt.
        /// </summary>
        private void CopyOrderToGoodsIn()
        {
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
            Messenger.Default.Send(ViewType.APReceipt);
            CopyingDocument = true;
            this.Sale.CopyingFromOrder = true;
            this.Locator.APReceipt.CopySale(this.Sale);
            this.Locator.APReceipt.AddTransaction();
        }

        /// <summary>
        /// Copies the current order to a goods in receipt.(We don't show the apreceipt screen).
        /// </summary>
        private void AutoCopyOrderToGoodsIn()
        {
            try
            {
                ProgressBar.Run();
                this.AutoCopying = true;
                this.Sale.CreationDate = DateTime.Now;
                this.Sale.CopyingFromOrder = true;
                this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
                this.Locator.APReceipt.CopySale(this.Sale);
                this.Locator.APReceipt.AddTransaction();
            }
            finally
            {
                ProgressBar.Reset();
                this.AutoCopying = false;
            }
        }

        #endregion

        #endregion
    }
}




﻿// -----------------------------------------------------------------------
// <copyright file="AttributeMasterSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Attribute
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Win32;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Interface;

    public class AttributeMasterSetUpViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// Dates enabled flag.
        /// </summary>
        private bool datesEnabled;

        /// <summary>
        /// Sql enabled flag.
        /// </summary>
        private bool sqlEnabled;

        /// <summary>
        /// Collection enabled flag.
        /// </summary>
        private bool collectionEnabled;

        /// <summary>
        /// Sql enabled flag.
        /// </summary>
        private bool sqlEnabledNonStandard;

        /// <summary>
        /// Collection enabled flag.
        /// </summary>
        private bool collectionEnabledNonStandard;

        /// <summary>
        /// The attribute description.
        /// </summary>
        private string description;

        /// <summary>
        /// The attribute description.
        /// </summary>
        private string notes;

        /// <summary>
        /// The attribute description.
        /// </summary>
        private string alertMessage;

        /// <summary>
        /// The attribute code.
        /// </summary>
        private string code;

        /// <summary>
        /// The attribute code.
        /// </summary>
        private bool isWorkflow;

        /// <summary>
        /// The attribute code.
        /// </summary>
        private bool isStandard;

        /// <summary>
        /// The selected macro.
        /// </summary>
        private string selectedLoadMacro;

        /// <summary>
        /// The selected post selection macro.
        /// </summary>
        private string selectedPostSelectionMacro;

        /// <summary>
        /// The non standrad text.
        /// </summary>
        private string standardMessageText;

        /// <summary>
        /// The non standard text.
        /// </summary>
        private string nonStandardMessageText;

        /// <summary>
        /// The standard sql.
        /// </summary>
        private string standardSQL;

        /// <summary>
        /// The standard sql.
        /// </summary>
        private string nonStandardSQL;

        /// <summary>
        /// The standard colection.
        /// </summary>
        private string standardCollection;

        /// <summary>
        /// The standard colection.
        /// </summary>
        private string nonStandardCollection;

        /// <summary>
        /// The standard colection.
        /// </summary>
        private string selectedResponseChecker;

        /// <summary>
        /// The response type.
        /// </summary>
        private NouTraceabilityType selectedStandardResponseType;

        /// <summary>
        /// The non standard response type.
        /// </summary>
        private NouTraceabilityType selectedNonStandardResponseType;
        /// <summary>
        /// Gets or sets the templates the attribute is on.
        /// </summary>
        private ObservableCollection<string> attributeTemplates = new ObservableCollection<string>();

        /// <summary>
        /// The users.
        /// </summary>
        private ObservableCollection<AllUser> users = new ObservableCollection<AllUser>();

        /// <summary>
        /// The users.
        /// </summary>
        private ObservableCollection<AllUser> userPermissions = new ObservableCollection<AllUser>();

        /// <summary>
        /// The user groups.
        /// </summary>
        private ObservableCollection<AllUser> userGroups = new ObservableCollection<AllUser>();

        /// <summary>
        /// The user group permissions.
        /// </summary>
        private ObservableCollection<AllUser> userGroupPermissions = new ObservableCollection<AllUser>();

        /// <summary>
        /// The user groups.
        /// </summary>
        private ObservableCollection<AllUser> userGroupsNonStandard = new ObservableCollection<AllUser>();

        /// <summary>
        /// Non standard log alert flag.
        /// </summary>
        private bool logAlertForNonStandard;

        private AttributeMaster currentAttribute;

        private AttributeSetUp attributeData;

        /// <summary>
        /// Gets or sets the look ups.
        /// </summary>
        private IList<AttributeLookUpData> attributeLookUps;

        /// <summary>
        /// The selected attachment
        /// </summary>
        private AttributeAttachment selectedAttachment;

        /// <summary>
        /// The attribute attachment
        /// </summary>
        private ObservableCollection<AttributeAttachment> attachments = new ObservableCollection<AttributeAttachment>();

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        private bool standardMode;

        /// <summary>
        /// The application user groups.
        /// </summary>
        private IList<UserGroup_> dbUserGroups;

        /// <summary>
        /// The selected attribute.
        /// </summary>
        private AttributeMasterData selectedAttributeMaster;

        /// <summary>
        /// The selected attribute.
        /// </summary>
        private NouDateType selectedDateType;

        /// <summary>
        /// Show dates flag.
        /// </summary>
        private bool showDatesTab;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeMasterSetUpViewModel"/> class.
        /// </summary>
        public AttributeMasterSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a master window change control command.
            Messenger.Default.Register<string>(this, Token.SwitchControl, this.ControlChange);

            Messenger.Default.Register<Sale>(this, Token.AttributeSearchSaleSelected, s =>
            {
                this.AttributeData = this.DataManager.GetAttributeById(s.SaleID);
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<string>(this, Token.UserOrGroupSelected, s =>
            {
                if (this.AttributeMasterId == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            Messenger.Default.Register<string>(this, Token.SqlScript, s =>
            {
                if (this.standardMode)
                {
                    this.StandardSQL = s;
                }
                else
                {
                    this.NonStandardSQL = s;
                }
            });

            Messenger.Default.Register<string>(this, Token.DataGenerated, s =>
            {
                if (this.standardMode)
                {
                    this.StandardCollection = s;
                }
                else
                {
                    this.NonStandardCollection = s;
                }
            });

            #endregion

            #region command handler

            this.ShowSqlWindowCommand = new RelayCommand<string>(s =>
            {
                if (s == "Standard")
                {
                    this.standardMode = true;
                    Messenger.Default.Send(this.StandardSQL ?? string.Empty, Token.DisplaySql);
                }
                else
                {
                    this.standardMode = false;
                    Messenger.Default.Send(this.NonStandardSQL ?? string.Empty, Token.DisplaySql);
                }
            });

            this.ShowCollectionWindowCommand = new RelayCommand<string>(s =>
            {
                if (s == "Standard")
                {
                    this.standardMode = true;
                    Messenger.Default.Send(this.StandardCollection ?? string.Empty, Token.DisplayCol);
                }
                else
                {
                    this.standardMode = false;
                    Messenger.Default.Send(this.NonStandardCollection ?? string.Empty, Token.DisplayCol);
                }
            });

            // Command handler to attach a file.
            this.AttachFileCommand = new RelayCommand(this.AttachFileCommandExecute);

            // Command to delete an attachment.
            this.DeleteFileCommand = new RelayCommand(this.DeleteFileCommandExecute);

            // Command to delete an attachment.
            this.DisplayFileCommand = new RelayCommand(this.DisplayFileCommandExecute);

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = true;
                this.SetControlMode(ControlMode.OK);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.SelectAttributeType, NouvemMessageBoxButtons.StandardWorkflow);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Standard)
                    {
                        this.IsStandard = true;
                    }
                    else
                    {
                        this.IsWorkflow = true;
                    }
                }));
            });

            #endregion

            this.ShowDatesTab = false;
            this.GetUsersAndGroups();
            this.GetSPNames();
            this.GetDateTypes();
            this.GetAttributeLookUps();
            this.GetResponseTypes();
            this.GetDateAttributeMasters();
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the templates the attribute is on.
        /// </summary>
        public ObservableCollection<string> AttributeTemplates
        {
            get
            {
                return this.attributeTemplates;
            }

            set
            {
                this.attributeTemplates = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// </summary>
        public bool DatesEnabled
        {
            get
            {
                return this.datesEnabled;
            }

            set
            {
                this.datesEnabled = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// </summary>
        public bool SqlEnabled
        {
            get
            {
                return this.sqlEnabled;
            }

            set
            {
                this.sqlEnabled = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// </summary>
        public bool CollectionEnabled
        {
            get
            {
                return this.collectionEnabled;
            }

            set
            {
                this.collectionEnabled = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// </summary>
        public bool SqlEnabledNonStandard
        {
            get
            {
                return this.sqlEnabledNonStandard;
            }

            set
            {
                this.sqlEnabledNonStandard = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether
        /// </summary>
        public bool CollectionEnabledNonStandard
        {
            get
            {
                return this.collectionEnabledNonStandard;
            }

            set
            {
                this.collectionEnabledNonStandard = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is a workflow.
        /// </summary>
        public bool IsWorkflow
        {
            get
            {
                return this.isWorkflow;
            }

            set
            {
                this.SetMode(value, this.isWorkflow);
                this.isWorkflow = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is standard.
        /// </summary>
        public bool IsStandard
        {
            get
            {
                return this.isStandard;
            }

            set
            {
                this.SetMode(value, this.isStandard);
                this.isStandard = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes
        {
            get
            {
                return this.notes;
            }

            set
            {
                this.SetMode(value, this.notes);
                this.notes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the system date types.
        /// </summary>
        public ObservableCollection<AttributeMasterData> AttributeMasters { get; private set; }

        /// <summary>
        /// Get the system date types.
        /// </summary>
        public ObservableCollection<NouDateType> DateTypes { get; private set; }

        /// <summary>
        /// Gets or sets the attribute data.
        /// </summary>
        public AttributeMasterData SelectedAttributeMaster
        {
            get
            {
                return this.selectedAttributeMaster;
            }

            set
            {
                this.SetMode(value, this.selectedAttributeMaster);
                this.selectedAttributeMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute data.
        /// </summary>
        public NouDateType SelectedDateType
        {
            get
            {
                return this.selectedDateType;
            }

            set
            {
                this.SetMode(value, this.selectedDateType);
                this.selectedDateType = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute data.
        /// </summary>
        public AttributeSetUp AttributeData
        {
            get
            {
                return this.attributeData;
            }

            set
            {
                this.attributeData = value;
                if (value != null && value.AttributeMaster != null)
                {
                    this.HandleAttributeData();
                }
            }
        }

        /// <summary>
        /// Gets or sets the response types.
        /// </summary>
        public IList<NouTraceabilityType> ResponseTypes { get; set; }

        /// <summary>
        /// Gets or sets the attachments.
        /// </summary>
        public ObservableCollection<AttributeAttachment> Attachments
        {
            get
            {
                return this.attachments;
            }

            set
            {
                this.attachments = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected standard response type.
        /// </summary>
        public NouTraceabilityType SelectedStandardResponseType
        {
            get
            {
                return this.selectedStandardResponseType;
            }

            set
            {
                this.SetMode(value, this.selectedStandardResponseType);
                this.selectedStandardResponseType = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.SetEnabledValues(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected standard response type.
        /// </summary>
        public NouTraceabilityType SelectedNonStandardResponseType
        {
            get
            {
                return this.selectedNonStandardResponseType;
            }

            set
            {
                this.SetMode(value, this.selectedNonStandardResponseType);
                this.selectedNonStandardResponseType = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.SetNonStandardEnabledValues(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the dates tab is visible.
        /// </summary>
        public bool ShowDatesTab
        {
            get
            {
                return this.showDatesTab;
            }

            set
            {
                this.showDatesTab = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected partner attachment.
        /// </summary>
        public AttributeAttachment SelectedAttachment
        {
            get
            {
                return this.selectedAttachment;
            }

            set
            {
                this.selectedAttachment = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application sp names.
        /// </summary>
        public IList<string> SPNames { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public ObservableCollection<AllUser> UserPermissions
        {
            get
            {
                return this.userPermissions;
            }

            set
            {
                this.userPermissions = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public ObservableCollection<AllUser> Users
        {
            get
            {
                return this.users;
            }

            set
            {
                this.users = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<AllUser> UserGroupsNonStandard
        {
            get
            {
                return this.userGroupsNonStandard;
            }

            set
            {
                this.userGroupsNonStandard = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<AllUser> UserGroups
        {
            get
            {
                return this.userGroups;
            }

            set
            {
                this.userGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<AllUser> UserGroupPermissions
        {
            get
            {
                return this.userGroupPermissions;
            }

            set
            {
                this.userGroupPermissions = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute id.
        /// </summary>
        public int AttributeMasterId { get; set; }

        /// <summary>
        /// Gets or sets the attribute code.
        /// </summary>
        public string Code
        {
            get
            {
                return this.code;
            }

            set
            {
                this.SetMode(value, this.code);
                this.code = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute description.
        /// </summary>
        public string Description
        {
            get
            {
                return this.description;
            }

            set
            {
                this.SetMode(value, this.description);
                this.description = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute code.
        /// </summary>
        public string AlertMessage
        {
            get
            {
                return this.alertMessage;
            }

            set
            {
                this.SetMode(value, this.alertMessage);
                this.alertMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute code.
        /// </summary>
        public string SelectedLoadMacro
        {
            get
            {
                return this.selectedLoadMacro;
            }

            set
            {
                this.SetMode(value, this.selectedLoadMacro);
                this.selectedLoadMacro = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute code.
        /// </summary>
        public string SelectedPostSelectionMacro
        {
            get
            {
                return this.selectedPostSelectionMacro;
            }

            set
            {
                this.SetMode(value, this.selectedPostSelectionMacro);
                this.selectedPostSelectionMacro = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the attribute code.
        /// </summary>
        public string SelectedResponseChecker
        {
            get
            {
                return this.selectedResponseChecker;
            }

            set
            {
                this.SetMode(value, this.selectedResponseChecker);
                this.selectedResponseChecker = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether an alert is logged for non standard responses.
        /// </summary>
        public bool LogAlertForNonStandard
        {
            get
            {
                return this.logAlertForNonStandard;
            }

            set
            {
                this.SetMode(value, this.logAlertForNonStandard);
                this.logAlertForNonStandard = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the standard message text.
        /// </summary>
        public string StandardMessageText
        {
            get
            {
                return this.standardMessageText;
            }

            set
            {
                this.SetMode(value, this.standardMessageText);
                this.standardMessageText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the non standard message text.
        /// </summary>
        public string NonStandardMessageText
        {
            get
            {
                return this.nonStandardMessageText;
            }

            set
            {
                this.SetMode(value, this.nonStandardMessageText);
                this.nonStandardMessageText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the non standard message text.
        /// </summary>
        public string StandardSQL
        {
            get
            {
                return this.standardSQL;
            }

            set
            {
                this.SetMode(value, this.standardSQL);
                this.standardSQL = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the non standard message text.
        /// </summary>
        public string NonStandardSQL
        {
            get
            {
                return this.nonStandardSQL;
            }

            set
            {
                this.SetMode(value, this.nonStandardSQL);
                this.nonStandardSQL = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the standard collection data string.
        /// </summary>
        public string StandardCollection
        {
            get
            {
                return this.standardCollection;
            }

            set
            {
                this.SetMode(value, this.standardCollection);
                this.standardCollection = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the standard collection data string.
        /// </summary>
        public string NonStandardCollection
        {
            get
            {
                return this.nonStandardCollection;
            }

            set
            {
                this.SetMode(value, this.nonStandardCollection);
                this.nonStandardCollection = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to show the sql window.
        /// </summary>
        public ICommand ShowSqlWindowCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the collection window.
        /// </summary>
        public ICommand ShowCollectionWindowCommand { get; private set; }

        /// <summary>
        /// Gets the command to attach a file.
        /// </summary>
        public ICommand AttachFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to displaya file.
        /// </summary>
        public ICommand DisplayFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to delete a file.
        /// </summary>
        public ICommand DeleteFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.AttributeData = this.DataManager.GetAttributeByLastEdit();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.AttributeData == null || this.AttributeData.AttributeMaster.AttributeMasterID == 0)
            {
                this.AttributeData = this.DataManager.GetAttributeByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetAttributeById(this.AttributeData.AttributeMaster.AttributeMasterID - 1);
            if (localSale != null)
            {
                this.AttributeData = localSale;
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.currentAttribute == null || this.currentAttribute.AttributeMasterID == 0)
            {
                this.AttributeData = this.DataManager.GetAttributeByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetAttributeById(this.AttributeData.AttributeMaster.AttributeMasterID + 1);
            if (localSale != null)
            {
                this.AttributeData = localSale;
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.AttributeData = this.DataManager.GetAttributeByFirstLast(true);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.AttributeData = this.DataManager.GetAttributeByFirstLast(false);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
            if (this.CurrentMode == ControlMode.Add)
            {
                this.ClearForm();
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Method that sets the application mode.
        /// </summary>
        /// <param name="mode">The mode to set.</param>
        /// <remarks>Note - Add authorisations checks are handled in the overriding ControlCommandExecute methods, 
        ///          but update authorisation checks need to handled here, as there is no cntr U.</remarks> 
        protected override void SetControlMode(ControlMode mode)
        {
            switch (mode)
            {
                case ControlMode.OK:
                    this.CurrentMode = ControlMode.OK;
                    this.ControlButtonContent = Strings.OK;
                    break;

                case ControlMode.Add:
                    this.CurrentMode = ControlMode.Add;
                    this.ControlButtonContent = Strings.Add;
                    break;

                case ControlMode.Find:
                    this.CurrentMode = ControlMode.Find;
                    this.ControlButtonContent = Strings.Find;
                    break;

                case ControlMode.Update:
                    this.CurrentMode = ControlMode.Update;
                    this.ControlButtonContent = Strings.Update;
                    break;
            }
        }

        /// <summary>
        /// Override, to handle a control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddAttribute();
                    break;

                case ControlMode.Find:
                    this.Locator.AttributeSearch.SetSales(this.DataManager.GetAttributesForSearch());
                    Messenger.Default.Send(ViewType.AttributeSearch);
                    break;

                case ControlMode.Update:
                    this.UpdateAttribute();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Set the control mode on user data entry.
        /// </summary>
        /// <typeparam name="T">The generic type parameter.</typeparam>
        /// <param name="newValue">The generic new value to compare.</param>
        /// <param name="oldValue">The generic old value to compare.</param>
        protected override void SetMode<T>(T newValue, T oldValue)
        {
            // ignore if we in add or update mode.
            if (this.CurrentMode == ControlMode.Add || this.CurrentMode == ControlMode.Update)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.OK || this.CurrentMode == ControlMode.Find)
            {
                if (newValue is ValueType && !newValue.Equals(oldValue))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                    return;
                }

                // check for a change of value
                if ((oldValue != null && newValue != null) && !newValue.Equals(oldValue))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                    return;
                }

                if ((oldValue == null && newValue != null))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Add);
                }
            }
        }

        /// <summary>
        /// Determine if an entity has been selected.
        /// </summary>
        /// <returns>A flag as to whether an entity is selected.</returns>
        /// <remarks>This must be overridden in the calling class, to check for it's local selected entity.</remarks>
        protected override bool CheckForEntity()
        {
            return this.AttributeMasterId > 0;
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handler for the command to attach a new file to the db.
        /// </summary>
        private void AttachFileCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                var localFile = File.ReadAllBytes(openFileDialog.FileName);
                var localName = openFileDialog.SafeFileName;

                var localAttachment = new AttributeAttachment { AttachmentDate = DateTime.Now, FileName = localName, File = localFile };
                this.Attachments.Add(localAttachment);

                if (!this.CheckForEntity())
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Handler for the command to display a new file to the db.
        /// </summary>
        private void DisplayFileCommandExecute()
        {
            #region validation

            if (this.SelectedAttachment == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAttachmentSelected);
                return;
            }

            #endregion

            try
            {
                var file = this.SelectedAttachment.File;
                var fileName = this.SelectedAttachment.FileName;

                var path = Path.Combine(Settings.Default.AtachmentsPath, fileName);
                File.WriteAllBytes(path, file);
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handler for the command to delete a file from the db.
        /// </summary>
        private void DeleteFileCommandExecute()
        {
            if (this.SelectedAttachment != null)
            {
                //if (this.SelectedBusinessPartner.Details.BPMasterID > 0)
                //{
                //    this.SetControlMode(ControlMode.Update);
                //}

                //// mark it for removal.
                //this.SelectedAttachment.Deleted = true;
                //this.BusinessPartnerAttachments.Remove(this.SelectedAttachment);
            }
        }

        #endregion

        /// <summary>
        /// Set the enabled values.
        /// </summary>
        /// <param name="type">The selected attribute type.</param>
        private void SetEnabledValues(NouTraceabilityType type)
        {
            if (type.TraceabilityType.CompareIgnoringCase(Constant.Calender))
            {
                this.DatesEnabled = true;
                this.SqlEnabled = false;
                this.CollectionEnabled = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.Collection))
            {
                this.SqlEnabled = false;
                this.DatesEnabled = false;
                this.CollectionEnabled = true;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.SQL))
            {
                this.SqlEnabled = true;
                this.DatesEnabled = false;
                this.CollectionEnabled = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.ManualEntry))
            {
                this.SqlEnabled = false;
                this.CollectionEnabled = false;
                this.DatesEnabled = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.ManualNumericEntry))
            {
                this.SqlEnabled = false;
                this.CollectionEnabled = false;
                this.DatesEnabled = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.Weight))
            {
                this.SqlEnabled = false;
                this.CollectionEnabled = false;
                this.DatesEnabled = false;
                return;
            }
        }

        /// <summary>
        /// Set the non standard enabled values.
        /// </summary>
        /// <param name="type">The selected attribute type.</param>
        private void SetNonStandardEnabledValues(NouTraceabilityType type)
        {
            if (type.TraceabilityType.CompareIgnoringCase(Constant.Calender))
            {
                this.SqlEnabledNonStandard = false;
                this.CollectionEnabledNonStandard = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.Collection))
            {
                this.SqlEnabledNonStandard = false;
                this.CollectionEnabledNonStandard = true;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.SQL))
            {
                this.SqlEnabledNonStandard = true;
                this.CollectionEnabledNonStandard = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.ManualEntry))
            {
                this.SqlEnabledNonStandard = false;
                this.CollectionEnabledNonStandard = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.ManualNumericEntry))
            {
                this.SqlEnabledNonStandard = false;
                this.CollectionEnabledNonStandard = false;
                return;
            }

            if (type.TraceabilityType.CompareIgnoringCase(Constant.Weight))
            {
                this.SqlEnabledNonStandard = false;
                this.CollectionEnabledNonStandard = false;
                return;
            }
        }

        /// <summary>
        /// Gets the application date types information.
        /// </summary>
        private void GetDateTypes()
        {
            this.DateTypes = new ObservableCollection<NouDateType>(this.DataManager.GetDateTypes());
            this.DateTypes.Insert(0, new NouDateType());
        }

        /// <summary>
        /// Creates/updates an attribute.
        /// </summary>
        private void CreateAttribute()
        {
            this.attributeData = new AttributeSetUp();
            var userAlerts = new List<UserAlert>();
            var userGroupAlerts = new List<UserGroupAlert>();
            var userGroupsNonStandard = new List<UserGroupNonStandard>();
            var userPermissions = new List<UserAttributePermission>();
            var userGroupPermissions = new List<UserGroupAttributePermission>();

            var usersToAlert = this.Users.Where(x => x.IsSelected).ToList();
            var userGroupsToAlert = this.UserGroups.Where(x => x.IsSelected).ToList();
            var usersPermissions = this.UserPermissions.Where(x => x.CanAdd || x.CanEdit).ToList();
            var userGroupsPermissions = this.UserGroupPermissions.Where(x => x.CanAdd || x.CanEdit).ToList();

            var userGroupsNonStandardToAllow = this.UserGroupsNonStandard.Where(x => x.IsSelected).ToList();

            foreach (var allUser in usersPermissions)
            {
                userPermissions.Add(new UserAttributePermission
                {
                    UserMasterID = allUser.Id,
                    CanAdd = allUser.CanAdd,
                    CanEdit = allUser.CanEdit,
                    CreationDate = DateTime.Now,
                    DeviceID = NouvemGlobal.DeviceId.ToInt(),
                    UserMasterID1 = NouvemGlobal.UserId.ToInt()
                });
            }

            foreach (var allUser in userGroupsPermissions)
            {
                userGroupPermissions.Add(new UserGroupAttributePermission
                {
                    UserGroupID = allUser.Id,
                    CanAdd = allUser.CanAdd,
                    CanEdit = allUser.CanEdit,
                    CreationDate = DateTime.Now,
                    DeviceID = NouvemGlobal.DeviceId.ToInt(),
                    UserMasterID1 = NouvemGlobal.UserId.ToInt()
                });
            }

            foreach (var userAlert in usersToAlert)
            {
                userAlerts.Add(new UserAlert
                {
                    UserMasterID = userAlert.Id,
                    CreationDate = DateTime.Now,
                    DeviceID = NouvemGlobal.DeviceId.ToInt(),
                    UserMasterID1 = NouvemGlobal.UserId.ToInt()
                });
            }

            foreach (var userGroupAlert in userGroupsToAlert)
            {
                userGroupAlerts.Add(new UserGroupAlert
                {
                    UserGroupID = userGroupAlert.Id,
                    CreationDate = DateTime.Now,
                    DeviceID = NouvemGlobal.DeviceId.ToInt(),
                    UserMasterID1 = NouvemGlobal.UserId.ToInt()
                });
            }

            foreach (var userGroupNS in userGroupsNonStandardToAllow)
            {
                userGroupsNonStandard.Add(new UserGroupNonStandard
                {
                    UserGroupID = userGroupNS.Id,
                    CreationDate = DateTime.Now,
                    DeviceID = NouvemGlobal.DeviceId.ToInt(),
                    UserMasterID = NouvemGlobal.UserId.ToInt()
                });
            }

            var nonStandardResponseId = (int?)null;
            if (this.selectedNonStandardResponseType != null)
            {
                nonStandardResponseId = this.selectedNonStandardResponseType.NouTraceabilityTypeID;
            }

            this.currentAttribute = new AttributeMaster
            {
                AttributeMasterID = this.AttributeMasterId,
                Code = this.Code,
                IsWorkflow = this.isWorkflow,
                Description = this.Description ?? string.Empty,
                LoadMACRO = this.SelectedLoadMacro,
                PostSelectionMACRO = this.SelectedPostSelectionMacro,
                AlertMessage = this.AlertMessage,
                EditDate = DateTime.Now,
                CreationDate = DateTime.Now,
                UserMasterID = NouvemGlobal.UserId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                LogAlertNonStd = this.LogAlertForNonStandard,
                MessageText = this.StandardMessageText,
                MessageText_NonStd = this.NonStandardMessageText,
                SQL = this.StandardSQL,
                SQL_NonStd = this.NonStandardSQL,
                Collection = this.StandardCollection,
                Collection_NonStd = this.NonStandardCollection,
                StandardResponseChecker = this.selectedResponseChecker,
                NouTraceabilityTypeID = this.selectedStandardResponseType.NouTraceabilityTypeID,
                NouTraceabilityTypeID_NonStd = nonStandardResponseId,
                Notes = this.notes,
                NouDateTypeID = this.selectedDateType != null && this.selectedDateType.NouDateTypeID > 0 ? this.selectedDateType.NouDateTypeID : (int?)null,
                AttributeMasterID_BasedOn = this.SelectedAttributeMaster != null && this.SelectedAttributeMaster.AttributeMasterId > 0 ? this.SelectedAttributeMaster.AttributeMasterId : (int?)null
            };

            this.attributeData.AttributeMaster = this.currentAttribute;
            this.attributeData.UserAlerts = userAlerts;
            this.attributeData.UserGroupAlerts = userGroupAlerts;
            this.attributeData.UserGroupsNonStandard = userGroupsNonStandard;
            this.attributeData.UserPermissions = userPermissions;
            this.attributeData.UserGroupPermissions = userGroupPermissions;

            if (this.IsStandard)
            {
                this.attributeData.AttributeLookUpName = this.GetNextAttributeName();
            }

            this.attributeData.AttributeAttachments = this.Attachments;
        }

        /// <summary>
        /// Validates the input data.
        /// </summary>
        /// <returns>A validation error message, or else empty string.</returns>
        private string Validate()
        {
            if (string.IsNullOrWhiteSpace(this.Code))
            {
                return Message.NoCodeEntered;
            }

            if (this.SelectedStandardResponseType == null)
            {
                return Message.NoResponseTypeEntered;
            }

            return string.Empty;
        }

        /// <summary>
        /// Adds a new attribute.
        /// </summary>
        private void AddAttribute()
        {
            var validation = this.Validate();
            if (validation != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, validation);
                return;
            }

            this.CreateAttribute();
            if (this.DataManager.AddAttribute(this.attributeData))
            {
                SystemMessage.Write(MessageType.Priority, Message.AttributeAdded);
                this.ClearForm();
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.AttributeNotAdded);
            }
        }

        /// <summary>
        /// Updates an existing attribute
        /// </summary>
        private void UpdateAttribute()
        {
            var validation = this.Validate();
            if (validation != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, validation);
                return;
            }

            this.CreateAttribute();
            if (this.DataManager.UpdateAttribute(this.attributeData))
            {
                SystemMessage.Write(MessageType.Priority, Message.AttributeUpdated);
                this.ClearForm();
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.AttributeNotUpdated);
            }
        }

        /// <summary>
        /// Gets the application sp names. 
        /// </summary>
        private void GetSPNames()
        {
            this.SPNames = this.DataManager.GetSPNames().Where(x => x.StartsWithIgnoringCase(Constant.Macro)).ToList();
            this.SPNames.Insert(0, string.Empty);
        }

        /// <summary>
        /// Gets the users/groups.
        /// </summary>
        private void GetUsersAndGroups()
        {
            this.Users.Clear();
            this.UserGroups.Clear();
            this.UserPermissions.Clear();
            this.UserGroupPermissions.Clear();
            this.UserGroupsNonStandard.Clear();
            this.dbUserGroups = this.DataManager.GetUserGroups().OrderBy(x => x.Description).ToList();
            foreach (var group in this.dbUserGroups)
            {
                var userGroup = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    IsSelected = false
                };

                this.UserGroups.Add(userGroup);

                var userGroupPermission = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    CanAdd = false,
                    CanEdit = false,
                    IsSelected = false
                };

                this.UserGroupPermissions.Add(userGroupPermission);

                var userGroupNonStandard = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    IsSelected = false
                };

                this.UserGroupsNonStandard.Add(userGroupNonStandard);
            }

            var localUsers = NouvemGlobal.Users.OrderBy(x => x.UserMaster.FullName);
            foreach (var user in localUsers)
            {
                var localUser = new AllUser
                {
                    Id = user.UserMaster.UserMasterID,
                    Name = user.UserMaster.FullName,
                    IsSelected = false
                };

                this.Users.Add(localUser);

                var localUserPermission = new AllUser
                {
                    Id = user.UserMaster.UserMasterID,
                    Name = user.UserMaster.FullName,
                    CanAdd = false,
                    CanEdit = false,
                    IsSelected = false
                };

                this.UserPermissions.Add(localUserPermission);
            }
        }

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        private void GetResponseTypes()
        {
            this.ResponseTypes = this.DataManager.GetTraceabilityTypes();
        }

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        private void GetAttributeLookUps()
        {
            this.attributeLookUps = this.DataManager.GetAttributeLookUps();
        }

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        private void GetDateAttributeMasters()
        {
            this.AttributeMasters = new ObservableCollection<AttributeMasterData>(
                this.DataManager.GetAttributes().Where(x => x.TraceabilityType.CompareIgnoringCase(Constant.Calender)));
            this.AttributeMasters.Insert(0, new AttributeMasterData());
        }

        /// <summary>
        /// Gets the next attribute name to use.
        /// </summary>
        /// <returns>The next attribute name.</returns>
        private string GetNextAttributeName()
        {
            if (!this.attributeLookUps.Any())
            {
                return string.Format("{0}1", Constant.Attribute);
            }

            // get the last 3 characters, extract the attribute number (could be 1,2 or 3 digits), and return the largest.
            var lookup = this.attributeLookUps.Max(x => x.Attribute_Name.Substring(x.Attribute_Name.Length - 3, 3).RemoveNonDecimals().ToInt());
            return string.Format("{0}{1}", Constant.Attribute, lookup + 1);
        }

        /// <summary>
        /// Handle the selected attribute data.
        /// </summary>
        private void HandleAttributeData()
        {
            var attribute = this.AttributeData.AttributeMaster;
            var userAlerts = this.AttributeData.UserAlerts;
            var userGroupAlerts = this.AttributeData.UserGroupAlerts;
            var userPermissions = this.AttributeData.UserPermissions;
            var userGroupPermissions = this.AttributeData.UserGroupPermissions;
            var groupsNonStandard = this.AttributeData.UserGroupsNonStandard;
            var attributeAttachments = this.AttributeData.AttributeAttachments;
            var templates = this.AttributeData.AttributeTemplates;

            this.AttributeMasterId = attribute.AttributeMasterID;
            this.Code = attribute.Code;
            this.Description = attribute.Description;
            this.SelectedStandardResponseType =
                this.ResponseTypes.FirstOrDefault(x => x.NouTraceabilityTypeID == attribute.NouTraceabilityTypeID);
            if (attribute.NouTraceabilityTypeID_NonStd.HasValue)
            {
                this.SelectedNonStandardResponseType =
                    this.ResponseTypes.FirstOrDefault(x => x.NouTraceabilityTypeID == attribute.NouTraceabilityTypeID_NonStd);
            }

            if (attribute.NouDateTypeID.HasValue)
            {
                this.SelectedDateType = this.DateTypes.FirstOrDefault(x => x.NouDateTypeID == attribute.NouDateTypeID);
            }
            else
            {
                this.SelectedDateType = null;
            }

            if (attribute.AttributeMasterID_BasedOn.HasValue)
            {
                this.SelectedAttributeMaster =
                    this.AttributeMasters.FirstOrDefault(x => x.AttributeMasterId == attribute.AttributeMasterID_BasedOn);
            }
            else
            {
                this.SelectedAttributeMaster = null;
            }

            this.StandardSQL = attribute.SQL;
            this.IsWorkflow = attribute.IsWorkflow.ToBool();
            this.NonStandardSQL = attribute.SQL_NonStd;
            this.StandardCollection = attribute.Collection;
            this.NonStandardCollection = attribute.Collection_NonStd;
            this.StandardMessageText = attribute.MessageText;
            this.NonStandardMessageText = attribute.MessageText_NonStd;
            this.SelectedResponseChecker = attribute.StandardResponseChecker;
            this.LogAlertForNonStandard = attribute.LogAlertNonStd.ToBool();
            this.SelectedLoadMacro = attribute.LoadMACRO;
            this.SelectedPostSelectionMacro = attribute.PostSelectionMACRO;
            this.AlertMessage = attribute.AlertMessage;
            this.Notes = attribute.Notes ?? string.Empty;

            this.Users.Clear();
            this.UserGroups.Clear();
            this.UserPermissions.Clear();
            this.UserGroupPermissions.Clear();
            this.UserGroupsNonStandard.Clear();
            var localUsers = NouvemGlobal.Users.OrderBy(x => x.UserMaster.FullName);
            foreach (var user in localUsers)
            {
                var localUser = new AllUser
                {
                    Id = user.UserMaster.UserMasterID,
                    Name = user.UserMaster.FullName,
                    IsSelected = userAlerts.Any(x => x.UserMasterID == user.UserMaster.UserMasterID)
                };

                this.Users.Add(localUser);

                var localUserPermission = new AllUser
                {
                    Id = user.UserMaster.UserMasterID,
                    Name = user.UserMaster.FullName,
                    CanAdd = userPermissions.Any(x => x.UserMasterID == user.UserMaster.UserMasterID && x.CanAdd),
                    CanEdit = userPermissions.Any(x => x.UserMasterID == user.UserMaster.UserMasterID && x.CanEdit),
                };

                this.UserPermissions.Add(localUserPermission);
            }

            foreach (var group in this.dbUserGroups)
            {
                var userGroup = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    IsSelected = userGroupAlerts.Any(x => x.UserGroupID == group.UserGroupID)
                };

                this.UserGroups.Add(userGroup);

                var userGroupPermission = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    CanAdd = userGroupPermissions.Any(x => x.UserGroupID == group.UserGroupID && x.CanAdd),
                    CanEdit = userGroupPermissions.Any(x => x.UserGroupID == group.UserGroupID && x.CanEdit)
                };

                this.UserGroupPermissions.Add(userGroupPermission);

                var userGroupNonStandard = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    IsSelected = groupsNonStandard.Any(x => x.UserGroupID == group.UserGroupID)
                };

                this.UserGroupsNonStandard.Add(userGroupNonStandard);
            }

            if (attributeAttachments != null)
            {
                this.Attachments = new ObservableCollection<AttributeAttachment>(attributeAttachments);
            }

            this.AttributeTemplates.Clear();
            foreach (var template in templates)
            {
                this.AttributeTemplates.Add(template);
            }
        }

        /// <summary>
        /// Clear the ui.
        /// </summary>
        private void ClearForm()
        {
            this.AttributeMasterId = 0;
            this.Code = string.Empty;
            this.Description = string.Empty;
            this.GetUsersAndGroups();
            this.SelectedLoadMacro = null;
            this.SelectedPostSelectionMacro = null;
            this.AlertMessage = string.Empty;
            this.LogAlertForNonStandard = false;
            this.SelectedStandardResponseType = null;
            this.SelectedNonStandardResponseType = null;
            this.StandardMessageText = string.Empty;
            this.NonStandardMessageText = string.Empty;
            this.Notes = string.Empty;
            this.StandardSQL = string.Empty;
            this.NonStandardSQL = string.Empty;
            this.StandardCollection = string.Empty;
            this.NonStandardCollection = string.Empty;
            this.SelectedResponseChecker = string.Empty;
            this.Attachments = null;
            this.AttributeData = null;
            this.Attachments = null;
            this.SelectedAttributeMaster = null;
            this.SelectedDateType = null;
            this.GetAttributeLookUps();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearAttributeMasterSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseAttributeMasterSetUpWindow);
        }

        #endregion
    }
}



﻿// -----------------------------------------------------------------------
// <copyright file="CountryMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Attribute
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class TabsViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The countries collection.
        /// </summary>
        private ObservableCollection<AttributeTabName> tabs = new ObservableCollection<AttributeTabName>();

        /// <summary>
        /// The selected tab.
        /// </summary>
        private AttributeTabName selectedTab;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TabsViewModel class.
        /// </summary>
        public TabsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            this.GetTabs();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected country.
        /// </summary>
        public Nouvem.Model.DataLayer.AttributeTabName SelectedTab
        {
            get
            {
                return this.selectedTab;
            }

            set
            {
                this.selectedTab = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the country Masters.
        /// </summary>
        public ObservableCollection<AttributeTabName> Tabs
        {
            get
            {
                return this.tabs;
            }

            set
            {
                this.tabs = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedTab != null)
            {
                this.selectedTab.Deleted = DateTime.Now;
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateTabs();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the tabs group.
        /// </summary>
        private void UpdateTabs()
        {
            if (this.DataManager.UpdateTabs(this.tabs))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                this.GetTabs();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
           Messenger.Default.Send(Token.Message, Token.CloseTabsSetUpWindow);
        }

        /// <summary>
        /// Gets the application countries.
        /// </summary>
        private void GetTabs()
        {
            this.Tabs.Clear();
            foreach (var tab in this.DataManager.GetAttributeTabNames())
            {
                this.Tabs.Add(tab);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

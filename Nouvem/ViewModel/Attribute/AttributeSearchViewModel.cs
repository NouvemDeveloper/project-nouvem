﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;

namespace Nouvem.ViewModel.Attribute
{
    public class AttributeSearchViewModel : Sales.SalesSearchDataViewModel
    {
        #region private

        /// <summary>
        /// The attributes to edit.
        /// </summary>
        private IList<Sale> attributesToEdit = new List<Sale>();

        #endregion

        public AttributeSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            Messenger.Default.Register<string>(this, Token.ShowDesktopNotesWindow, s => this.ShowNotesWindow());

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                if (this.SelectedSale != null)
                {
                    this.SelectedSale.PopUpNote = s;
                    this.SetControlMode(ControlMode.Update);

                    var localBatch = this.attributesToEdit.FirstOrDefault(x => x.SaleID == this.SelectedSale.SaleID);
                    if (localBatch != null)
                    {
                        this.attributesToEdit.Remove(localBatch);
                    }

                    this.attributesToEdit.Add(this.SelectedSale);
                }
            });
        }

        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale != null)
            {
                Messenger.Default.Send(this.SelectedSale, Token.AttributeSearchSaleSelected);
            }

            if (!this.KeepVisible)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowNotesWindow()
        {
            if (this.SelectedSale == null || this.SelectedSale.SaleID == 0)
            {
                return;
            }

            var notes = this.SelectedSale.PopUpNote ?? string.Empty;
            Messenger.Default.Send(notes, Token.DisplayNotes);
        }
    }
}


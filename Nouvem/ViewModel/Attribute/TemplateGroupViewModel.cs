﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityNameViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Attribute
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class TemplateGroupViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The template names collection.
        /// </summary>
        private ObservableCollection<AttributeTemplateGroup> traceabilityTemplateGroups;

        /// <summary>
        /// The selected group.
        /// </summary>
        private AttributeTemplateGroup selectedTraceabilityTemplateGroup;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TemplateNameViewModel class.
        /// </summary>
        public TemplateGroupViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.TraceabilityTemplateGroups = new ObservableCollection<AttributeTemplateGroup>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);
            this.GetTraceabilityTemplateGroups();
        }

        #endregion

        #region property

        /// <summary>
        /// Get the traceability Masters.
        /// </summary>
        public AttributeTemplateGroup SelectedTraceabilityTemplateGroup
        {
            get
            {
                return this.selectedTraceabilityTemplateGroup;
            }

            set
            {
                this.selectedTraceabilityTemplateGroup = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the traceability Masters.
        /// </summary>
        public ObservableCollection<AttributeTemplateGroup> TraceabilityTemplateGroups
        {
            get
            {
                return this.traceabilityTemplateGroups;
            }

            set
            {
                this.traceabilityTemplateGroups = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedTraceabilityTemplateGroup != null)
            {
                this.selectedTraceabilityTemplateGroup.Deleted = DateTime.Now;
                this.UpdateTraceabilityTemplateGroups();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.TraceabilityTemplateGroups.Remove(this.selectedTraceabilityTemplateGroup);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateTraceabilityTemplateGroups();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the traceability template names group.
        /// </summary>
        private void UpdateTraceabilityTemplateGroups()
        {
            try
            {
                if (this.DataManager.AddOrUpdateAttributeTemplateGroups(this.TraceabilityTemplateGroups))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                    Messenger.Default.Send(Token.Message, Token.TraceabilityTemplateGroupsUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTemplateGroup();
            Messenger.Default.Send(Token.Message, Token.CloseTraceabilityGroupWindow);
        }

        /// <summary>
        /// Gets the application traceability template names information.
        /// </summary>
        private void GetTraceabilityTemplateGroups()
        {
            this.TraceabilityTemplateGroups.Clear();
            foreach (var traceability in this.DataManager.GetAttributeTemplateGroups())
            {
                this.TraceabilityTemplateGroups.Add(traceability);
            }
        }

        #endregion
    }
}




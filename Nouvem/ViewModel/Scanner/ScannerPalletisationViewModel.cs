﻿// -----------------------------------------------------------------------
// <copyright file="StockTakeViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Scanner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.View.Scanner;
    using StockTake = Nouvem.Model.BusinessObject.StockTake;

    public class ScannerPalletisationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The pallet count.
        /// </summary>
        private int palletCount;

        /// <summary>
        /// The application stock details.
        /// </summary>
        private ObservableCollection<StockDetail> stockDetails = new ObservableCollection<StockDetail>();

        /// <summary>
        /// Timer started flag.
        /// </summary>
        private bool timerStarted;

        /// <summary>
        /// Palletising dispatch stock flag.
        /// </summary>
        private bool palletisingDispatchItems;

        /// <summary>
        /// The pallet batcode.
        /// </summary>
        private string palletID;

        /// <summary>
        /// The scanner timer.
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// The parent pallet transaction.
        /// </summary>
        private StockDetail pallet = new StockDetail();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScannerPalletisationViewModel"/> class.
        /// </summary>
        public ScannerPalletisationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                this.HandleScannerData(s);
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.Palletisation, s =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.HandleScannerData(s);
                }));
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<HashSet<int>>(this, Token.PalletiseItems, s =>
            {
                this.palletisingDispatchItems = true;
                foreach (var i in s)
                {
                    var trans = this.DataManager.GetStockTransactionById(i);
                    if (trans != null)
                    {
                        this.HandleScannerData(trans.Serial.ToString(), true);
                    }
                }

                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

                var id = this.PalletID;
                this.DataManager.UpdateDispatchPalletTransactions(s, id.ToInt());
            });


            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.StockTake, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }

                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            });

            #endregion

            #region command handler

            this.CreateLabelCommand = new RelayCommand(() =>
            {
                try
                {
                    if (this.pallet.StockTransactionID.IsNullOrZero())
                    {
                        return;
                    }

                    if (this.PrintLabel)
                    {
                        try
                        {
                            this.PrintManager.ProcessPrinting(null, null, null, null, ViewType.Palletisation, LabelType.Pallet,
                               this.PalletID.ToInt());


                            this.ClearForm();
                            this.Close();

                        }
                        catch (Exception e)
                        {
                            this.Log.LogError(this.GetType(), e.Message);
                            NouvemMessageBox.Show(e.Message, touchScreen: true);
                        }
                    }

                    if (ApplicationSettings.PrintReportAtPalletisation)
                    {
                        this.ReportManager.CreatePalletisationReport(this.pallet.StockTransactionID.ToInt());
                    }

                    // Mark the pallet as unconsumed, in order for a label to print.
                    if (this.DataManager.UnConsumePalletTransaction(this.pallet.StockTransactionID.ToInt()))
                    {
                        this.ClearForm();
                    }
                }
                finally
                {
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            });

            this.ManualScanCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.StockTake, true, ApplicationSettings.ScannerEmulatorMode));

            this.MoveBackCommand = new RelayCommand<string>(s =>
            {
                this.Close();
            });

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = true;
            });

            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                //ViewModelLocator.ClearStockTake();
                this.IsFormLoaded = false;
            });

            this.OnClosingCommand = new RelayCommand(() =>
            {
                //ViewModelLocator.ClearStockTake();
                this.IsFormLoaded = false;
            });

            #endregion

            this.SetUpTimer();
            this.SetUpMainTimer();
            this.ScannerStockMode = ScannerMode.Scan;
            this.HasWriteAuthorisation = true;
            this.OpenScanner();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value determining if a touchscreen pallet label is to be printed.
        /// </summary>
        public bool PrintLabel { get; set; }

        /// <summary>
        /// Gets or sets the pallet count.
        /// </summary>
        public int PalletCount
        {
            get
            {
                return this.palletCount;
            }

            set
            {
                this.palletCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the parent pallet transaction.
        /// </summary>
        public string PalletID
        {
            get
            {
                return this.palletID;
            }

            set
            {
                this.palletID = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the parent pallet transaction.
        /// </summary>
        public StockDetail Pallet
        {
            get
            {
                return this.pallet;
            }

            set
            {
                this.pallet = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock details.
        /// </summary>
        public ObservableCollection<StockDetail> StockDetails
        {
            get
            {
                return this.stockDetails;
            }

            set
            {
                this.stockDetails = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.PalletCount = value.Count;
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; set; }

        /// <summary>
        /// Gets the command to create a label.
        /// </summary>
        public ICommand CreateLabelCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Handles the scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            this.HandleScannerData(data, false);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:

                    break;

                case ControlMode.Update:

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Clean up and close.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Unregister(this);
            ViewModelLocator.ClearScannerPalletisation();

            if (!this.palletisingDispatchItems && !this.PrintLabel)
            {
                Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
            }

            Messenger.Default.Send(Token.Message, Token.ClosePalletWindow);
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                Keypad.Show(KeypadTarget.Palletisation, ApplicationSettings.ScannerMode);
            };
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpMainTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                Global.Keypad.Show(KeypadTarget.StockTake, true, ApplicationSettings.ScannerEmulatorMode);
            };
        }

        /// <summary>
        /// Handles the scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        private void HandleScannerData(string data, bool ignoreConsume = false)
        {
            #region validation

            if (string.IsNullOrEmpty(data))
            {
                return;
            }

            #endregion

            data = this.HandleGs1Barcode(data);

            try
            {
                var serial = data.RemoveNonIntegers().ToInt();
                if (this.Pallet.StockTransactionID.IsNullOrZero())
                {
                    var trans = new StockTransaction
                    {
                        TransactionDate = DateTime.Now,
                        UserMasterID = NouvemGlobal.UserId.ToInt(),
                        DeviceMasterID = NouvemGlobal.DeviceId,
                        INMasterID = NouvemGlobal.InventoryItems.First().Master.INMasterID,
                        WarehouseID = NouvemGlobal.PalletLocationId,
                        Consumed = DateTime.Now,
                        Comments = Constant.Pallet,
                        ManualWeight = false
                    };

                    this.Pallet = new StockDetail();
                    this.Pallet.StockTransactionID = this.DataManager.AddPalletTransaction(trans);
                    this.Pallet.WarehouseId = trans.WarehouseID;
                    this.PalletID = this.Pallet.StockTransactionID.ToString();
                }

                var scanOn = this.ScannerStockMode == ScannerMode.Scan;
                var detail = this.DataManager.UpdatePalletTransaction(serial, this.pallet, scanOn, ignoreConsume);
                if (detail.LogInfo.CompareIgnoringCase(Constant.Pallet))
                {
                    this.ClearForm();
                    this.Pallet.WarehouseId = detail.WarehouseId;
                    this.GetPalletDetails(serial);
                    return;
                }

                if (!string.IsNullOrEmpty(detail.Error))
                {
                    this.Log.LogError(this.GetType(), string.Format("Scanner palletisation error:{0}", detail.Error));
                    this.LogToDatabase(LogType.Error, this.pallet.StockTransactionID.ToString(), serial.ToString(), detail.Error);
                    return;
                }

                if (!string.IsNullOrEmpty(detail.LogInfo))
                {
                    this.LogToDatabase(LogType.Warning, this.pallet.StockTransactionID.ToString(), serial.ToString(), detail.LogInfo);
                }

                if (scanOn)
                {
                    var scanItemOn = this.StockDetails.FirstOrDefault(x => x.Serial == serial);
                    if (scanItemOn == null)
                    {
                        this.StockDetails.Add(detail);
                    }
                }
                else
                {
                    var scanOff = this.StockDetails.FirstOrDefault(x => x.Serial == serial);
                    if (scanOff != null)
                    {
                        this.StockDetails.Remove(scanOff);
                    }
                }
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                this.PalletCount = this.stockDetails.Count;
            }
        }

        /// <summary>
        /// Gets the stock on a pallet.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        private void GetPalletDetails(int serial)
        {
            try
            {
                this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetPalletDetails(serial, false));
                this.PalletID = serial.ToString();
                this.pallet.StockTransactionID = serial;
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Clears the UI.
        /// </summary>
        private void ClearForm()
        {
            try
            {
                this.PalletID = string.Empty;
                this.StockDetails.Clear();
                this.Pallet = new StockDetail();
                this.PalletCount = 0;
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        #endregion
    }
}



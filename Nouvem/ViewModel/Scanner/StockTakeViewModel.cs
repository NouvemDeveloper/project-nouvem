﻿// -----------------------------------------------------------------------
// <copyright file="StockTakeViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Scanner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.View.Scanner;
    using StockTake = Nouvem.Model.BusinessObject.StockTake;

    public class StockTakeViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The scan counter.
        /// </summary>
        private int scanCount;

        /// <summary>
        /// The current counter scanned barcodes.
        /// </summary>
        private HashSet<string> counterScans = new HashSet<string>();

        /// <summary>
        /// The application stocktakes.
        /// </summary>
        private ObservableCollection<StockTake> stockTakes;

        /// <summary>
        /// The selected stock take details.
        /// </summary>
        private ObservableCollection<StockTakeData> stockTakeDetails;

        /// <summary>
        /// The selected stock take detail.
        /// </summary>
        private StockTakeData selectedStockTakeDetail;

        /// <summary>
        /// The factory locations.
        /// </summary>
        private ObservableCollection<Warehouse> warehouses;

        /// <summary>
        /// The factory stock locations.
        /// </summary>
        private List<object> stockWarehouses = new List<object>();

        /// <summary>
        /// The selected warehouse.
        /// </summary>
        private Warehouse selectedWarehouse;

        /// <summary>
        /// The selected stock take.
        /// </summary>
        private StockTake selectedStockTake;

        private bool listsLoading;

        /// <summary>
        /// The selected set up stock take.
        /// </summary>
        private StockTake selectedSetUpStockTake;

        /// <summary>
        /// The stock take name.
        /// </summary>
        private string stockTakeName;

        /// <summary>
        /// The stock take description.
        /// </summary>
        private string stockTakeDescription;

        /// <summary>
        /// The stock take description.
        /// </summary>
        private string description;

        /// <summary>
        /// Timer started flag.
        /// </summary>
        private bool timerStarted;

        private List<Warehouse> allowableWarehouses;

        /// <summary>
        /// The scanner timer.
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// The selected status.
        /// </summary>
        private NouDocStatu selectedDocStatus;

        /// <summary>
        /// The doc status items.
        /// </summary>
        public IList<NouDocStatu> docStatusItems;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StockTakeViewModel"/> class.
        /// </summary>
        public StockTakeViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                this.Log.LogInfo(this.GetType(), string.Format("Stock take-Scan:{0}", s));
                this.HandleScannerData(s);
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.StockTake, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });


            // Register for a master window change control command.
            Messenger.Default.Register<string>(this, Token.SwitchControl, this.ControlCommandExecute);

            #endregion

            #region command handler

            this.HandleSelectedStockTake = new RelayCommand(() =>
            {
                if (this.SelectedStockTake == null)
                {
                    return;
                }

                if (this.IsFormLoaded && this.SelectedStockTake != null && this.SelectedStockTake.StockTakeID > 0)
                {
                    this.DataManager.GetStockTakeDetails(this.SelectedStockTake);
                    this.StockTakeDescription = this.SelectedStockTake.Description;
                    this.StockTakeDetails = new ObservableCollection<StockTakeData>(this.SelectedStockTake.StockTakeDetails);
                    this.ScanCount = 0;
                    var localLocations = this.SelectedStockTake.Locations.Split(',');
                    this.Warehouses = new ObservableCollection<Warehouse>(this.AllowableWarehouses.Where(x => localLocations.Contains(x.WarehouseID.ToString())));
                    this.Warehouses.Insert(0, new Warehouse { Name = Strings.SelectLocation });
                    this.SelectedWarehouse = this.Warehouses.First();
                    Messenger.Default.Send(Token.Message, Token.CloseWindow);

                    var localDescription = this.SelectedStockTake.Description;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Description = localDescription;
                    }));
                }
            });

            this.OnListClosingCommand = new RelayCommand(() =>
            {
                this.listsLoading = false;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            });

            this.ResetCounterCommand = new RelayCommand(() =>
            {
                NouvemMessageBox.Show(Message.ResetCounterPrompt, NouvemMessageBoxButtons.YesNo, scanner:true);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    this.ScanCount = 0;
                    this.counterScans.Clear();
                }
            });

            this.CreateStockTakeCommand = new RelayCommand(this.CreateStockTakeCommandExecute);

            this.ManualScanCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.StockTake, true, ApplicationSettings.ScannerEmulatorMode));

            this.MoveBackCommand = new RelayCommand<string>(this.MoveBackCommandExecute);

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = true;
                //this.SelectedStockTake = null;
                
            });

            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                //ViewModelLocator.ClearStockTake();
                this.IsFormLoaded = false;
            });

            //this.OnClosingCommand = new RelayCommand(() =>
            //{
            //    //ViewModelLocator.ClearStockTake();
            //    this.IsFormLoaded = false;
            //});

            this.SwitchViewCommand = new RelayCommand(() =>
            {
                this.listsLoading = true;
                this.GetStockTakes();
                var search = new ScannerStockTakeListView();
                search.ShowDialog();
                search.Focus();
            });

            #endregion

            this.GetWarehouses();
            this.GetStockTakes();
            this.SetUpTimer();
            this.SetUpMainTimer();
            this.ScannerStockMode = ScannerMode.Scan;
            this.StockTakeDescription = Strings.SelectStockTake;
            this.GetDocStatusItems();
            this.HasWriteAuthorisation = true;
            this.OpenScanner();
            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected doc status.
        /// </summary>
        public NouDocStatu SelectedDocStatus
        {
            get
            {
                return this.selectedDocStatus;
            }

            set
            {
                this.selectedDocStatus = value;
                this.RaisePropertyChanged();
                if (!this.EntitySelectionChange)
                {
                    this.SetControlMode(ControlMode.Update);
                }
                else
                {
                    this.SetControlMode(ControlMode.OK);
                }
            }
        }

        /// <summary>
        /// Gets or sets the factory locations.
        /// </summary>
        public int ScanCount
        {
            get
            {
                return this.scanCount;
            }

            set
            {
                this.scanCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the factory locations.
        /// </summary>
        public List<Warehouse> AllowableWarehouses
        {
            get
            {
                return this.allowableWarehouses;
            }

            set
            {
                this.allowableWarehouses = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock take name.
        /// </summary>
        public string StockTakeName
        {
            get
            {
                return this.stockTakeName;
            }

            set
            {
                this.stockTakeName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock take name.
        /// </summary>
        public string Description
        {
            get
            {
                return this.stockTakeDescription;
            }

            set
            {
                this.stockTakeDescription = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock take name.
        /// </summary>
        public string StockTakeDescription
        {
            get
            {
                return this.stockTakeDescription;
            }

            set
            {
                this.stockTakeDescription = value;
                this.RaisePropertyChanged();
                this.Description = value;
            }
        }

        /// <summary>
        /// Gets or sets the warehouses.
        /// </summary>
        public ObservableCollection<Warehouse> Warehouses
        {
            get
            {
                return this.warehouses;
            }

            set
            {
                this.warehouses = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the warehouses.
        /// </summary>
        public List<object> StockWarehouses
        {
            get
            {
                return this.stockWarehouses;
            }

            set
            {
                this.stockWarehouses = value;
                //this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected warehouse.
        /// </summary>
        public Warehouse SelectedWarehouse
        {
            get
            {
                return this.selectedWarehouse;
            }

            set
            {
                this.selectedWarehouse = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

                if (value != null && value.WarehouseID != 0)
                {
                    this.StockTakeDetails = new ObservableCollection<StockTakeData>(this.selectedStockTake.StockTakeDetails.Where(x => x.WarehouseID == value.WarehouseID));
                }
            }
        }

        /// <summary>
        /// Gets or sets the stock take details.
        /// </summary>
        public ObservableCollection<StockTakeData> StockTakeDetails
        {
            get
            {
                return this.stockTakeDetails;
            }

            set
            {
                this.stockTakeDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected stock take details.
        /// </summary>
        public StockTakeData SelectedStockTakeDetail
        {
            get
            {
                return this.selectedStockTakeDetail;
            }

            set
            {
                this.selectedStockTakeDetail = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock takes.
        /// </summary>
        public ObservableCollection<StockTake> StockTakes
        {
            get
            {
                return this.stockTakes;
            }

            set
            {
                this.stockTakes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected stock stake.
        /// </summary>
        public StockTake SelectedStockTake
        {
            get
            {
                return this.selectedStockTake;
            }

            set
            {
                this.selectedStockTake = value;
                this.RaisePropertyChanged();

                if (this.listsLoading)
                {
                    return;
                }

                if (this.IsFormLoaded && value != null && value.StockTakeID > 0)
                {
                    this.DataManager.GetStockTakeDetails(value);
                    this.StockTakeDescription = value.Description;
                    this.StockTakeDetails = new ObservableCollection<StockTakeData>(value.StockTakeDetails);
                    var localLocations = value.Locations.Split(',');
                    this.Warehouses = new ObservableCollection<Warehouse>(this.AllowableWarehouses.Where(x => localLocations.Contains(x.WarehouseID.ToString())));
                    this.Warehouses.Insert(0, new Warehouse { Name = Strings.SelectLocation });
                    this.SelectedWarehouse = this.Warehouses.First();
                    Messenger.Default.Send(Token.Message, Token.CloseWindow);

                    var localDescription = value.Description;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Description = localDescription;
                    }));
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected stock stake.
        /// </summary>
        public StockTake SelectedSetUpStockTake
        {
            get
            {
                return this.selectedSetUpStockTake;
            }

            set
            {
                this.selectedSetUpStockTake = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded && value != null)
                {
                    this.EntitySelectionChange = true;
                    this.StockTakeName = value.Description;
                    this.SelectedDocStatus =
                        NouvemGlobal.NouDocStatusItems.FirstOrDefault(x => x.NouDocStatusID == value.NouDocStatusID);
                    this.EntitySelectionChange = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the nou doc status items.
        /// </summary>
        public IList<NouDocStatu> DocStatusItems
        {
            get
            {
                return this.docStatusItems;
            }

            set
            {
                this.docStatusItems = value;
                if (!this.EntitySelectionChange)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand ResetCounterCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnListLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand HandleSelectedStockTake { get; private set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnListClosingCommand { get; private set; }


        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand CreateStockTakeCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddStockTake();
                    break;

                case ControlMode.Update:
                    this.UpdateStockTake();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        protected virtual void MoveBackCommandExecute(string s)
        {
            if (s.Equals(Strings.Menu))
            {
                //ViewModelLocator.ClearStockTake();
                Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
             
                return;
            }

            // back to main
            Messenger.Default.Send(Token.Message, Token.CloseWindow);
        }

        private void CreateStockTakeCommandExecute()
        {
            var localStockTake = this.StockTakes.FirstOrDefault(x => x.StockTakeID == 0);
            if (localStockTake == null)
            {
                return;
            }

            this.StockTakeName = localStockTake.Description;
            this.StockWarehouses = this.AllowableWarehouses.Select(x => (object)x.WarehouseID).ToList();
            this.AddStockTake();
            this.SelectedStockTake = this.StockTakes.OrderBy(x => x.StockTakeID).LastOrDefault();
        }

        /// <summary>
        /// Adds a stock take.
        /// </summary>
        private void AddStockTake()
        {
            #region validation

            if (string.IsNullOrEmpty(this.StockTakeName))
            {
                SystemMessage.Write(MessageType.Issue, Message.EnterStockTakeName);
                return;
            }

            if (this.StockWarehouses == null || !this.StockWarehouses.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.EnterLocation);
                return;
            }

            #endregion

            try
            {
                var stockLocations = string.Join(",", this.StockWarehouses.Select(x => (int)x));

                var stockTake = new Model.DataLayer.StockTake
                {
                    Description = this.StockTakeName,
                    Locations = stockLocations,
                    CreationDate = DateTime.Now,
                    NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID
                };

                if (this.DataManager.AddStockTake(stockTake))
                {
                    SystemMessage.Write(MessageType.Priority, Message.StockTakeAdded);
                    this.SetControlMode(ControlMode.OK);
                    this.Refresh();
                    return;
                }
            }
            finally
            {
                this.SetControlMode(ControlMode.OK);
            }

            SystemMessage.Write(MessageType.Issue, Message.StockTakeNotAdded);
            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Adds a stock take.
        /// </summary>
        private void UpdateStockTake()
        {
            #region validation

            if (this.selectedSetUpStockTake == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(this.StockTakeName))
            {
                SystemMessage.Write(MessageType.Issue, Message.EnterStockTakeName);
                return;
            }

            //if (this.StockWarehouses == null || !this.StockWarehouses.Any())
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.EnterLocation);
            //    return;
            //}

            #endregion

            //var stockLocations = string.Join(",", this.StockWarehouses.Select(x => (int)x));

            var stockTake = new Model.DataLayer.StockTake
            {
                StockTakeID = this.selectedSetUpStockTake.StockTakeID,
                Description = this.StockTakeName,
                // Locations = stockLocations,
                //CreationDate = DateTime.Now,
                NouDocStatusID = this.SelectedDocStatus.NouDocStatusID
            };

            if (this.DataManager.UpdateStockTake(stockTake))
            {
                SystemMessage.Write(MessageType.Priority, Message.StockTakeUpdated);
                this.SetControlMode(ControlMode.OK);
                this.Refresh();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.StockTakeNotUpdated);
            }
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetWarehouses();
            this.GetStockTakes();
            this.StockTakeName = string.Empty;
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Clean up and close.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearStockTake();
            Messenger.Default.Send(Token.Message, Token.CloseStockStockTakeWindow);
        }

        /// <summary>
        /// Gets the warehouses.
        /// </summary>
        private void GetWarehouses()
        {
            this.AllowableWarehouses = this.DataManager.GetWarehouses().Where(x => x.StockLocation).ToList();

            this.Warehouses = new ObservableCollection<Warehouse>();
            this.Warehouses.Add(new Warehouse { Name = Strings.SelectLocation });
            this.SelectedWarehouse = this.Warehouses.First();
        }

        /// <summary>
        /// Gets the stock takes.
        /// </summary>
        protected virtual void GetStockTakes()
        {
            this.StockTakes = new ObservableCollection<StockTake>(this.DataManager.GetStockTakes()
                .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID));

            this.SelectedStockTake = null;
        }

        /// <summary>
        /// Set up the timers.
        /// </summary>
        private void SetUpTimer()
        {
            this.timer.Interval = TimeSpan.FromMilliseconds(ApplicationSettings.HandheldScannerDataWaitTime);
            this.timer.Tick += (sender, args) =>
            {
                try
                {
                    this.timer.Stop();
                    var data = this.ScannerText.RemoveNonIntegers();
                    this.ScannerText = string.Empty;

                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                       new Action(() =>
                       {
                           this.HandleScannerData(data);
                       }));

                    //this.HandleScannerData(data);
                }
                finally
                {
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            };
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpMainTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                Global.Keypad.Show(KeypadTarget.StockTake, true, ApplicationSettings.ScannerEmulatorMode);
            };
        }

        /// <summary>
        /// Handles the scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            #region validation

            if (string.IsNullOrEmpty(data))
            {
                return;
            }


            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.selectedStockTake == null || this.selectedStockTake.StockTakeID == 0)
            {
                NouvemMessageBox.Show(Message.SelectStocktake, scanner: true);
                return;
            }

            if (this.selectedWarehouse == null || this.selectedWarehouse.WarehouseID == 0)
            {
                NouvemMessageBox.Show(Message.SelectLocation, scanner: true);
                return;
            }

            #endregion

            if (ApplicationSettings.CheckForImportedLabelAtStockTake && data.Length > 17)
            {
                var stockData = this.DataManager.GetTransactionByComments
                    (data.RemoveEndCharacterIfMatch(ApplicationSettings.KeyboardWedgeTerminator), 0);
                data = stockData.Serial.ToInt().ToString();
            }

            data = this.CheckForSplitStock(data);
            data = this.HandleGs1Barcode(data);
            data = data.RemoveNonIntegers();

            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                if (this.StockTakeDetails.Any(x => x.Barcode != null && x.Barcode.Equals(data)))
                {
                    this.SelectedStockTakeDetail = this.StockTakeDetails.FirstOrDefault(x => x.Barcode.Equals(data));
                    //NouvemMessageBox.Show(string.Format(Message.BarcodeOnStockTake, data), scanner:true);
                    return;
                }

                var returnedDetail = this.DataManager.AddToStockTake(data, this.selectedStockTake.StockTakeID,
                this.selectedWarehouse.WarehouseID);

                if (returnedDetail.PalletStock != null)
                {
                    foreach (var stockTakeData in returnedDetail.PalletStock)
                    {
                        this.StockTakeDetails.Insert(0, stockTakeData);
                        this.SetCounterScan(stockTakeData.Barcode);
                    }

                    return;
                }

                this.StockTakeDetails.Insert(0, returnedDetail);
                this.SelectedStockTakeDetail = returnedDetail;
                this.SetCounterScan(returnedDetail.Barcode);
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                if (this.DataManager.RemoveFromStockTake(data))
                {
                    var stockToRemove = this.StockTakeDetails.FirstOrDefault(x => x.Barcode == data);
                    if (stockToRemove != null)
                    {
                        this.StockTakeDetails.Remove(stockToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the data for the counter scan.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        private void SetCounterScan(string barcode)
        {
            this.counterScans.Add(barcode);
            this.ScanCount = this.counterScans.Count;
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        private void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems
                               .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID ||
                                x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Refreshes the selected doc status, to display the active status.
        /// </summary>
        private void RefreshDocStatusItems()
        {
            this.SelectedDocStatus =
               this.DocStatusItems.FirstOrDefault(x => x.Value.CompareIgnoringCase(Constant.Active));
        }

        #endregion
    }
}


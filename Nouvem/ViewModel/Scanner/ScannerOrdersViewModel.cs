﻿// -----------------------------------------------------------------------
// <copyright file="ScannerOrdersViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using GalaSoft.MvvmLight.Messaging;

namespace Nouvem.ViewModel.Scanner
{
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Purchases.APReceipt;

    /// <summary>
    /// Initializes a new instance of the <see cref="ScannerOrdersViewModel"/> class.
    /// </summary>
    public class ScannerOrdersViewModel : TouchscreenOrdersViewModel
    {
        public ScannerOrdersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            this.CloseCommand = new RelayCommand(() =>
            {
                NouvemMessageBox.Show(Message.ConfirmApplicationClose, NouvemMessageBoxButtons.OKCancel, scanner:true);
                if (NouvemMessageBox.UserSelection == UserDialogue.OK)
                {
                    Application.Current.Shutdown();
                    Process.GetCurrentProcess().Kill();
                }
            });

            #endregion

            this.SetModule(ViewType.ARDispatch);
        }

        /// <summary>
        /// Gets the command to close the application.
        /// </summary>
        public ICommand CloseCommand { get; private set; }

        /// <summary>
        /// Creates an order for the selected supplier.
        /// </summary>
        protected override void CreateOrderCommandExecute()
        {
            if (this.Partner == null || this.Partner.Name.Equals(Strings.AllPartners))
            {
                NouvemMessageBox.Show(Message.NoPartnerSelected, scanner: true);
                return;
            }

            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerTouchscreen;
            this.Locator.ScannerMainDispatch.SelectedPartner = this.Partner;
            this.Locator.ScannerMainDispatch.SelectedPartnerName = this.Partner.Name;
        
            Messenger.Default.Send(Token.Message, Token.UpdateBatchNo);
            Messenger.Default.Send(this.Partner, Token.CreateOrder);
        }
    }
}

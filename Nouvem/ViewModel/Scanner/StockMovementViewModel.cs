﻿// -----------------------------------------------------------------------
// <copyright file="StockTakeViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Charts;
using DevExpress.Xpf.Ribbon.Customization;
using GalaSoft.MvvmLight;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Scanner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.View.Scanner;
    using StockTake = Nouvem.Model.BusinessObject.StockTake;

    public class StockMovementViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The application stocktakes.
        /// </summary>
        private ObservableCollection<StockTake> stockTakes;

        /// <summary>
        /// The stock move orders.
        /// </summary>
        private ObservableCollection<Sale> stockMoveOrders = new ObservableCollection<Sale>();

        /// <summary>
        /// The selected stock take details.
        /// </summary>
        private ObservableCollection<StockTakeData> stockMoveOrderDetails = new ObservableCollection<StockTakeData>();

        /// <summary>
        /// The selected stock take details.
        /// </summary>
        private ObservableCollection<StockTakeData> stockMoveDetails = new ObservableCollection<StockTakeData>();

        /// <summary>
        /// The selected stock take detail.
        /// </summary>
        private StockTakeData selectedMoveDetail;

        /// <summary>
        /// The execute batch stock flag.
        /// </summary>
        private bool executeBatchStockMove;

        /// <summary>
        /// The factory locations.
        /// </summary>
        private ObservableCollection<Warehouse> warehouses;

        /// <summary>
        /// The factory sub locations.
        /// </summary>
        private ObservableCollection<WarehouseLocation> subWarehouses;

        /// <summary>
        /// The selected warehouse.
        /// </summary>
        private Warehouse selectedWarehouse;

        /// <summary>
        /// The selected sub warehouse.
        /// </summary>
        private WarehouseLocation selectedSubWarehouse;

        /// <summary>
        /// Timer started flag.
        /// </summary>
        private bool timerStarted;

        /// <summary>
        /// The scanner timer.
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// The sub locations.
        /// </summary>
        private IList<WarehouseLocation> warehouseLocations = new List<WarehouseLocation>();

        /// <summary>
        /// The move to location.
        /// </summary>
        //private Warehouse selectedMoveTo;

        /// <summary>
        /// The move from location.
        /// </summary>
        private Warehouse selectedMoveFrom;

        /// <summary>
        /// The move to location.
        /// </summary>
        private DateTime? moveDate;

        /// <summary>
        /// The ref.
        /// </summary>
        private string reference;

        /// <summary>
        /// The move.
        /// </summary>
        private Sale stockMove = new Sale();

        /// <summary>
        /// The grid view to show.
        /// </summary>
        private ViewModelBase centerDisplayModel;

        private IList<ProductionData> allBatches;
        private ObservableCollection<ProductionData> batches;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StockMovementViewModel"/> class.
        /// </summary>
        public StockMovementViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<int?>(this, Token.BatchNumberChanged, batch =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (batch.HasValue && this.SelectedStockMoveDetail != null)
                    {
                        var localBatch = this.allBatches.FirstOrDefault(x => x.BatchNumber.BatchNumberID == batch);
                        if (localBatch != null)
                        {
                            this.SelectedStockMoveDetail.INMasterID = localBatch.INMasterID;
                        }
                    }
                }));
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.StockMovement, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            #endregion

            #region command handler

            this.MoveBatchStockCommand = new RelayCommand(() =>
            {
                NouvemMessageBox.Show(Message.BatchMoveStockPrompt, NouvemMessageBoxButtons.YesNo, scanner:true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                this.ExecuteBatchStockMove = true;
                this.ControlSelectionCommandExecute();
            });

            this.CompleteMoveCommand = new RelayCommand(() =>
            {
                if (this.StockMove == null)
                {
                    return;
                }

                NouvemMessageBox.Show(Message.CompleteStockMovePrompt, NouvemMessageBoxButtons.YesNo, scanner: true);
                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    return;
                }

                this.StockMove.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                this.CompleteOrder();
            });

            this.SwapViewCommand = new RelayCommand(() =>
            {
                if (this.CenterDisplayViewModel == this.Locator.StockMoveGrid)
                {
                    this.CenterDisplayViewModel = this.Locator.StockMoveGroupedGrid;
                    ApplicationSettings.StockMoveGrid = Constant.StockMoveGroupedGrid;
                    Messenger.Default.Send(false, Token.ShowScannerMoveButton);
                }
                else if(this.CenterDisplayViewModel == this.Locator.StockMoveGroupedGrid)
                {
                    this.CenterDisplayViewModel = this.Locator.StockMoveBatchGrid;
                    ApplicationSettings.StockMoveGrid = Constant.StockMoveBatchGrid;
                    Messenger.Default.Send(true, Token.ShowScannerMoveButton);
                }
                else
                {
                    this.CenterDisplayViewModel = this.Locator.StockMoveGrid;
                    ApplicationSettings.StockMoveGrid = Constant.StockMoveGrid;
                    Messenger.Default.Send(false, Token.ShowScannerMoveButton);
                }
            });

            this.ShowOrdersCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CreateScannerStockMoveOrders);
            });

            this.ManualScanCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.StockMovement, true, ApplicationSettings.ScannerEmulatorMode));

            this.MoveBackCommand = new RelayCommand<string>(s =>
            {
                if (s.Equals(Strings.Menu))
                {
                    this.ClearForm();
                    ViewModelLocator.ClearStockMovement();
                    Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                    Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
                    return;
                }

                // back to main
                Messenger.Default.Send(Token.Message, Token.CloseWindow);
            });

            this.MoveBackOrdersCommand = new RelayCommand<string>(s =>
            {
                // back to main
                Messenger.Default.Send(Token.Message, Token.CloseScannerCategoryWindow);
            });

            this.OnOrdersLoadingCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = false;
                this.StockMoveOrders = new ObservableCollection<Sale>(
                    this.DataManager.GetStockMoveOrders
                    (new List<int?> { NouvemGlobal.NouDocStatusActive.NouDocStatusID, NouvemGlobal.NouDocStatusInProgress.NouDocStatusID },
                        DateTime.Today.AddYears(-10), DateTime.Today).OrderByDescending(x => x.SaleID));

                this.StockMove = null;
                this.IsFormLoaded = true;

            });

            this.OnOrdersUnloadedCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = true;
            });

            this.OnClosingCommand = new RelayCommand(() =>
            {
                //ViewModelLocator.ClearStockTake();
                this.IsFormLoaded = false;
                Messenger.Default.Send(false, Token.ShowScannerMoveButton);
            });

            this.SwitchViewCommand = new RelayCommand(() =>
            {
                var search = new ScannerStockTakeListView();
                search.ShowDialog();
                search.Focus();
            });

            #endregion

            if (ApplicationSettings.StockMoveGrid == Constant.StockMoveGrid)
            {
                this.CenterDisplayViewModel = this.Locator.StockMoveGrid;
                Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        Messenger.Default.Send(false, Token.ShowScannerMoveButton);
                    }));
            }
            else if (ApplicationSettings.StockMoveGrid == Constant.StockMoveGroupedGrid)
            {
                this.CenterDisplayViewModel = this.Locator.StockMoveGroupedGrid;
                Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        Messenger.Default.Send(false, Token.ShowScannerMoveButton);
                    }));
            }
            else
            {
                this.CenterDisplayViewModel = this.Locator.StockMoveBatchGrid;
                Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        Messenger.Default.Send(true, Token.ShowScannerMoveButton);
                    }));
            }

            this.HasWriteAuthorisation = true;
            this.StockMove = new Sale();
            this.GetWarehouses();
            this.SetUpTimer();
            this.SetUpMainTimer();
            this.ScannerStockMode = ScannerMode.Scan;
            this.SetControlMode(ControlMode.Add);
            this.HasWriteAuthorisation = true;
            this.GetLocalDocNumberings();
            this.RefreshMoves();
            this.IsFormLoaded = true;
            this.SetActiveDocument(this);
            this.GetAllBatches();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets orc sets the grid view to display.
        /// </summary>
        public ViewModelBase CenterDisplayViewModel
        {
            get
            {
                return this.centerDisplayModel;
            }

            set
            {
                this.centerDisplayModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scanned barcode.
        /// </summary>
        public Sale StockMove
        {
            get
            {
                return this.stockMove;
            }

            set
            {
                this.stockMove = value;
                this.RaisePropertyChanged();
                if (value != null && value.SaleID > 0 && this.IsFormLoaded)
                {
                    value = this.DataManager.GetStockMoveOrderById(value.SaleID);
                    Messenger.Default.Send(Token.Message, Token.CloseScannerCategoryWindow);
                    this.SelectedMoveFrom = this.Warehouses.FirstOrDefault(x => x.WarehouseID == value.WarehouseIDFrom);
                    this.SelectedWarehouse = this.Warehouses.FirstOrDefault(x => x.WarehouseID == value.WarehouseID);
                    this.HandleSearchSale(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batches.
        /// </summary>
        public ObservableCollection<ProductionData> Batches
        {
            get
            {
                return this.batches;
            }

            set
            {
                this.batches = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the orders.
        /// </summary>
        public ObservableCollection<Sale> StockMoveOrders
        {
            get
            {
                return this.stockMoveOrders;
            }

            set
            {
                this.stockMoveOrders = value;
                this.RaisePropertyChanged();
            }
        }

        public IList<Warehouse> MoveToWarehouses { get; set; }

        /// <summary>
        /// Gets or sets the warehouses.
        /// </summary>
        public ObservableCollection<Warehouse> Warehouses
        {
            get
            {
                return this.warehouses;
            }

            set
            {
                this.warehouses = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sub warehouses.
        /// </summary>
        public ObservableCollection<WarehouseLocation> SubWarehouses
        {
            get
            {
                return this.subWarehouses;
            }

            set
            {
                this.subWarehouses = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected warehouse.
        /// </summary>
        public WarehouseLocation SelectedSubWarehouse
        {
            get
            {
                return this.selectedSubWarehouse;
            }

            set
            {
                this.selectedSubWarehouse = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected warehouse.
        /// </summary>
        public Warehouse SelectedWarehouse
        {
            get
            {
                return this.selectedWarehouse;
            }

            set
            {
                this.SetMode(value, this.selectedWarehouse);
                this.selectedWarehouse = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

                if (value != null && value.WarehouseID != 0)
                {
                    var localSubs = this.warehouseLocations.Where(x => x.WarehouseID == value.WarehouseID).ToList();

                    if (localSubs.Any())
                    {
                        this.SubWarehouses = new ObservableCollection<WarehouseLocation>(localSubs);
                        this.SubWarehouses.Insert(0, new WarehouseLocation { Name = Strings.SelectSubLocation });
                        this.SelectedSubWarehouse = this.SubWarehouses.First();
                    }
                    else
                    {
                        this.SubWarehouses = new ObservableCollection<WarehouseLocation>();
                        this.SubWarehouses.Insert(0, new WarehouseLocation { Name = Strings.NoSubLocations });
                        this.SelectedSubWarehouse = this.SubWarehouses.First();
                    }
                }
                else
                {
                    this.SubWarehouses = new ObservableCollection<WarehouseLocation>();
                    this.SubWarehouses.Insert(0, new WarehouseLocation { Name = Strings.NoSubLocations });
                    this.SelectedSubWarehouse = this.SubWarehouses.First();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected warehouse.
        /// </summary>
        public Warehouse SelectedMoveFrom
        {
            get
            {
                return this.selectedMoveFrom;
            }

            set
            {
                this.SetMode(value, this.selectedMoveFrom);
                this.selectedMoveFrom = value;
                this.RaisePropertyChanged();

                if (this.allBatches != null && this.allBatches.Any())
                {
                    if (value.WarehouseID == 0)
                    {
                        this.Batches = new ObservableCollection<ProductionData>(this.allBatches);
                    }
                    else
                    {
                        this.Batches = new ObservableCollection<ProductionData>(this.allBatches.Where(x => x.WarehouseID == value.WarehouseID));
                    }
               
                    this.StockMoveDetails = new ObservableCollection<StockTakeData>();
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch stock move value.
        /// </summary>
        public bool ExecuteBatchStockMove
        {
            get
            {
                return this.executeBatchStockMove;
            }

            set
            {
                this.SetMode(value, this.executeBatchStockMove);
                this.executeBatchStockMove = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public string Reference
        {
            get
            {
                return this.reference;
            }

            set
            {
                this.SetMode(value, this.reference);
                this.reference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the move date.
        /// </summary>
        public DateTime? MoveDate
        {
            get
            {
                return this.moveDate;
            }

            set
            {
                this.SetMode(value, this.moveDate);
                this.moveDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock take details.
        /// </summary>
        public ObservableCollection<StockTakeData> StockMoveOrderDetails
        {
            get
            {
                return this.stockMoveOrderDetails;
            }

            set
            {
                this.stockMoveOrderDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock take details.
        /// </summary>
        public ObservableCollection<StockTakeData> StockMoveDetails
        {
            get
            {
                return this.stockMoveDetails;
            }

            set
            {
                this.stockMoveDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected move details.
        /// </summary>
        public StockTakeData SelectedStockMoveDetail
        {
            get
            {
                return this.selectedMoveDetail;
            }

            set
            {
                this.selectedMoveDetail = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.StockMove = this.DataManager.GetStockMoveOrderByFirstLast(false);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.StockMove == null || this.StockMove.SaleID == 0)
            {
                this.StockMove = this.DataManager.GetStockMoveOrderByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetStockMoveOrderById(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.StockMove = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.StockMove == null || this.StockMove.SaleID == 0)
            {
                this.StockMove = this.DataManager.GetStockMoveOrderByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetStockMoveOrderById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.StockMove = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.StockMove = this.DataManager.GetStockMoveOrderByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.StockMove = this.DataManager.GetStockMoveOrderByFirstLast(false);
        }

        public void RefreshSales()
        {
            this.FindSaleCommandExecute();
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to move batch stock.
        /// </summary>
        public ICommand MoveBatchStockCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand CompleteMoveCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand SwapViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand ShowOrdersCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackOrdersCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnOrdersLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event..
        /// </summary>
        public ICommand OnOrdersUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            if (this.DisableProcessing)
            {
                return;
            }

            this.DisableProcessing = true;
            this.Log.LogInfo(this.GetType(), string.Format("HandleScannerData: {0}", data));

            try
            {
                #region validation

                if (string.IsNullOrEmpty(data))
                {
                    return;
                }

                #endregion

                if (this.CenterDisplayViewModel == this.Locator.StockMoveBatchGrid)
                {
                    data = data.RemoveEndCharacterIfMatch(ApplicationSettings.KeyboardWedgeTerminator);
                    var subLocation = this.warehouseLocations.FirstOrDefault(x => x.Barcode.CompareIgnoringCase(data));
                    if (subLocation != null)
                    {
                        this.Log.LogInfo(this.GetType(), string.Format("Sub location found: {0}", subLocation.Name));
                        this.SelectedWarehouse =
                            this.Warehouses.FirstOrDefault(x => x.WarehouseID == subLocation.WarehouseID);

                        this.SelectedSubWarehouse = this.SubWarehouses.FirstOrDefault(x => x.WarehouseLocationID == subLocation.WarehouseLocationID);
                        return;
                    }

                    var trans = this.DataManager.GetStockTransactionById(data.ToInt());
                    if (trans != null)
                    {
                        this.SelectedStockMoveDetail =
                            this.StockMoveDetails.FirstOrDefault(x => x.BatchID == trans.BatchNumberID);
                        if (this.SelectedStockMoveDetail == null)
                        {
                            this.StockMoveDetails.Add(new StockTakeData { BatchID = trans.BatchNumberID, Qty = trans.TransactionQTY, Wgt = trans.TransactionWeight});
                        }
                    }
                    
                    return;
                }

                data = this.HandleGs1Barcode(data);
          
                if (this.ScannerStockMode == ScannerMode.Scan)
                {
                    data = data.RemoveEndCharacterIfMatch(ApplicationSettings.KeyboardWedgeTerminator);
                    var alreadyOnMove = this.StockMoveOrderDetails.Any(x => x.Barcode == data);
                    if (alreadyOnMove)
                    {
                        return;
                    }

                    var subLocation = this.warehouseLocations.FirstOrDefault(x => x.Barcode.CompareIgnoringCase(data));
                    if (subLocation != null)
                    {
                        this.Log.LogInfo(this.GetType(), string.Format("Sub location found: {0}", subLocation.Name));
                        this.SelectedWarehouse =
                            this.Warehouses.FirstOrDefault(x => x.WarehouseID == subLocation.WarehouseID);

                        this.SelectedSubWarehouse = this.SubWarehouses.FirstOrDefault(x => x.WarehouseLocationID == subLocation.WarehouseLocationID);
                        return;
                    }

                    data = data.RemoveNonIntegers();
                    if (this.SelectedWarehouse == null || this.SelectedWarehouse.WarehouseID == 0)
                    {
                        NouvemMessageBox.Show(Message.SelectLocation, scanner: true);
                        return;
                    }

                    if (this.SelectedWarehouse == null || this.SelectedWarehouse.WarehouseID == 0)
                    {
                        NouvemMessageBox.Show(Message.SelectLocation, scanner: true);
                        return;
                    }

                    int? localSubLocation = this.SelectedSubWarehouse == null ||
                                       this.SelectedSubWarehouse.WarehouseLocationID == 0
                        ? (int?)null
                        : this.SelectedSubWarehouse.WarehouseLocationID;

                    if (this.StockMove.SaleID == 0)
                    {
                        this.AddOrder(false);
                    }

                    var returnedDetails = this.DataManager.MoveStock(
                        this.SelectedWarehouse.WarehouseID,
                        localSubLocation,
                        data, this.StockMove.SaleID, ApplicationSettings.ReprintStockMoveItem);

                    foreach (var returnedDetail in returnedDetails)
                    {
                        returnedDetail.WarehouseName = this.selectedWarehouse.Name;
                        returnedDetail.SubWarehouseName =
                            this.selectedSubWarehouse != null ? this.selectedSubWarehouse.Name : string.Empty;
                        this.StockMoveOrderDetails.Insert(0, returnedDetail);
                    }

                    this.SetStockMoveOrderDetails();
                    this.SelectedStockMoveDetail = this.StockMoveDetails.FirstOrDefault();
                    var lastReturned = returnedDetails.LastOrDefault();
                    if (lastReturned != null)
                    {
                        this.SelectedStockMoveDetail = this.StockMoveDetails.FirstOrDefault(x => x.INMasterID == lastReturned.INMasterID);
                    }
                }
                else if (this.ScannerStockMode == ScannerMode.MinusScan)
                {
                    data = data.RemoveNonIntegers();
                    var localTrans = this.StockMoveOrderDetails.FirstOrDefault(x => x.Barcode.Trim() == data.Trim());
                    if (localTrans != null)
                    {
                        if (this.DataManager.UndoMoveStock(localTrans))
                        {
                            this.StockMoveOrderDetails.Remove(localTrans);
                        }
                    }
                }

                if (ApplicationSettings.ResetLocationsAtStockMovement)
                {
                    this.SelectedWarehouse = this.Warehouses.FirstOrDefault();
                }
            }
            finally
            {
                this.DisableProcessing = false;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Resets the ui.
        /// </summary>
        /// <param name="clearSelectedCustomer">Flag, indicating whether the selected customer data is cleared.</param>
        protected override void ClearForm(bool clearSelectedCustomer = true)
        {
            this.RefreshMoves();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handles the incoming search selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            searchSale = this.DataManager.GetStockMoveOrderById(searchSale.SaleID);
            this.SelectedDocStatus =
                this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == searchSale.NouDocStatusID);
            this.SelectedSalesEmployee =
                this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == searchSale.UserIDCreation);
            this.SelectedWarehouse = this.Warehouses.FirstOrDefault(x => x.WarehouseID == searchSale.WarehouseID);
            this.SelectedMoveFrom = this.Warehouses.FirstOrDefault(x => x.WarehouseID == searchSale.WarehouseIDFrom);
            this.DocumentDate = searchSale.DocumentDate;
            this.DeliveryDate = searchSale.DeliveryDate;
            this.Reference = searchSale.Reference;
            this.NextNumber = searchSale.Number;
            this.Remarks = searchSale.Remarks;
            this.StockMoveOrderDetails = new ObservableCollection<StockTakeData>(searchSale.StockMoveOrderDetails);
            this.StockMoveDetails = new ObservableCollection<StockTakeData>(searchSale.StockMoveDetails);
            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings =
                new ObservableCollection<DocNumber>(
                    this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.StockMove)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }


        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems
                .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                            || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                            || x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ||
                            x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Clean up and close.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearStockMovement();
            Messenger.Default.Send(Token.Message, Token.CloseStockStockTakeWindow);
        }

        #endregion

        #region private

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void RefreshMoves()
        {
            this.StockMove = new Sale();
            this.StockMoveDetails.Clear();
            this.StockMoveOrderDetails.Clear();
            this.Reference = string.Empty;
            this.Remarks = string.Empty;
            this.ExecuteBatchStockMove = false;
            this.SelectedDocStatus =
                this.DocStatusItems.FirstOrDefault(
                    x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
            this.SelectedSalesEmployee = null;
            this.SelectedMoveFrom = this.Warehouses.First();
            this.SelectedWarehouse = this.Warehouses.First();
            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetWarehouses();
        }

        /// <summary>
        /// Gets the warehouses.
        /// </summary>
        private void GetWarehouses()
        {
            this.Warehouses = new ObservableCollection<Warehouse>(this.DataManager.GetWarehouses().Where(x => x.StockLocation));
            this.MoveToWarehouses = new List<Warehouse>(this.DataManager.GetWarehouses().Where(x => x.StockLocation));
            this.warehouseLocations = new List<WarehouseLocation>(this.DataManager.GetWarehouseLocations());
            this.Warehouses.Insert(0, new Warehouse { Name = Strings.SelectLocation });
            this.MoveToWarehouses.Insert(0, new Warehouse { Name = Strings.SelectLocation });
            this.SelectedWarehouse = this.Warehouses.First();
            this.SelectedMoveFrom = this.Warehouses.First();
        }

        /// <summary>
        /// Set up the timers.
        /// </summary>
        private void SetUpTimer()
        {
            this.timer.Interval = TimeSpan.FromMilliseconds(ApplicationSettings.HandheldScannerDataWaitTime);
            this.timer.Tick += (sender, args) =>
            {
                try
                {
                    this.timer.Stop();
                    var data = this.ScannerText.RemoveNonIntegers();
                    this.ScannerText = string.Empty;

                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                       new Action(() =>
                       {
                           this.HandleScannerData(data);
                       }));

                    //this.HandleScannerData(data);
                }
                finally
                {
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            };
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpMainTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                Global.Keypad.Show(KeypadTarget.StockMovement, true, ApplicationSettings.ScannerEmulatorMode);
            };
        }

        /// <summary>
        /// Sets the order line data after a scan.
        /// </summary>
        private void SetStockMoveOrderDetails()
        {
            var details = this.StockMoveOrderDetails.GroupBy(x => x.INMasterID);
            foreach (var detail in details)
            {
                var wgt = detail.Sum(x => x.ClosingWgt);
                var qty = detail.Sum(x => x.ClosingQty);

                var localDetail = this.StockMoveDetails.FirstOrDefault(x => x.INMasterID == detail.Key);
                if (localDetail != null)
                {
                    localDetail.ClosingWgt = Math.Round(wgt.ToDecimal(), 2);
                    localDetail.ClosingQty = Math.Round(qty.ToDecimal(), 0);
                }
                else
                {
                    var newLine = new StockTakeData
                    {
                        INMasterID = detail.Key,
                        Wgt = 0,
                        Qty = 0,
                        ClosingWgt = Math.Round(wgt.ToDecimal(), 2),
                        ClosingQty = Math.Round(qty.ToDecimal(), 0)
                    };

                    this.StockMoveDetails.Add(newLine);
                    this.StockMove.StockMoveDetails = new List<StockTakeData> { newLine };
                    this.UpdateOrder(false);
                }
            }
        }

        /// <summary>
        /// Validates and creates stock move data.
        /// </summary>
        private void CreateStockMove(bool refresh = true)
        {
            #region validation

            var error = string.Empty;

            if (this.SelectedWarehouse == null || this.SelectedWarehouse.WarehouseID == 0)
            {
                error = Message.NoMoveToLocationSelected;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error);
                return;
            }

            #endregion

            this.StockMove.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;
            if (this.SelectedSalesEmployee != null)
            {
                this.StockMove.UserIDCreation = this.SelectedSalesEmployee.UserMaster.UserMasterID;
            }

            this.StockMove.WarehouseID = this.SelectedWarehouse.WarehouseID;
            this.StockMove.WarehouseIDFrom = this.SelectedMoveFrom.WarehouseID;
            this.StockMove.DocumentDate = this.DocumentDate;
            this.StockMove.DeliveryDate = this.DeliveryDate;
            this.StockMove.Reference = this.Reference;
            this.StockMove.Remarks = this.Remarks;
            if (refresh)
            {
                this.StockMove.StockMoveDetails = this.StockMoveDetails;
            }
        }

        /// <summary>
        /// Updates an order.
        /// </summary>
        /// <param name="refresh">Flag, as to a refresh or not.</param>
        private void UpdateOrder(bool refresh = true)
        {
            this.CreateStockMove(refresh);
            if (this.DataManager.UpdateStockMoveOrder(this.StockMove))
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderUpdated);
                this.ExecuteBatchStockMovement();
                this.GetLocalDocNumberings();
                if (refresh)
                {
                    this.RefreshMoves();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotUpdated);
            }
        }

        /// <summary>
        /// Updates an order.
        /// </summary>
        private void CompleteOrder()
        {
            if (this.DataManager.ChangeStockMoveOrderStatus(this.StockMove))
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderUpdated);
                this.GetLocalDocNumberings();
                this.RefreshMoves();
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotUpdated);
            }
        }

        #endregion

        protected override void CopyDocumentTo()
        {
            // not implemented
        }

        protected override void CopyDocumentFrom()
        {
            // not implemented
        }

        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetStockMoveOrders(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            }
            else
            {
                this.CustomerSales = this.DataManager.GetStockMoveOrders(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            }

            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.StockMove), Token.SearchForSale);
            this.Locator.SalesSearchData.SetView(ViewType.StockMove);
        }

        protected override void DirectSearchCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void AddSale()
        {
            this.AddOrder();
        }

        private void GetAllBatches()
        {
            this.allBatches = this.DataManager.GetProductionMultiBatchOrders(inMasterId: 0, productionLocationOnly:false);
            this.Batches = new ObservableCollection<ProductionData>(this.allBatches);
        }

        private void AddOrder(bool refresh = true)
        {
            this.CreateStockMove();
            this.StockMove.SaleID = this.DataManager.AddStockMoveOrder(this.StockMove);
            if (this.StockMove.SaleID > 0)
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderCreated);
                this.ExecuteBatchStockMovement();

                if (refresh)
                {
                    this.RefreshMoves();
                }
                else
                {
                    this.StockMove.Number = this.NextNumber;
                }

                this.GetLocalDocNumberings();
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotCreated);
            }
        }

        protected override void UpdateSale()
        {
            this.UpdateOrder();
        }

        /// <summary>
        /// Handler for the ui load event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
        }

        private void ExecuteBatchStockMovement()
        {
            if (!this.ExecuteBatchStockMove)
            {
                return;
            }

            var batchMoves = this.StockMoveDetails.Where(x => x.BatchID.HasValue).ToList();
            if (!batchMoves.Any())
            {
                return;
            }

            foreach (var stockTakeData in batchMoves)
            {
                stockTakeData.WarehouseID = this.SelectedWarehouse.WarehouseID;
            }

            this.DataManager.MoveBatchStock(batchMoves);
            this.GetAllBatches();
            this.StockMoveDetails = new ObservableCollection<StockTakeData>();
        }
    }
}




















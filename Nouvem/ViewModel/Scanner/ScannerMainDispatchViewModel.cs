﻿// -----------------------------------------------------------------------
// <copyright file="ScannerMainDispatchViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;

namespace Nouvem.ViewModel.Scanner
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class ScannerMainDispatchViewModel : Sales.ARDispatch.ARDispatchTouchscreenViewModel
    {
        #region field
        
        /// <summary>
        /// Carriage return
        /// </summary>
        private char carriageReturn = Microsoft.VisualBasic.Strings.Chr(13);

        /// <summary>
        /// The scanner timer.
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// Timer started flag.
        /// </summary>
        private bool timerStarted;

        /// <summary>
        /// The selected customer name.
        /// </summary>
        private string partnerName;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScannerMainDispatchViewModel"/> class.
        /// </summary>
        public ScannerMainDispatchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command
              

            this.OnLoadedCommand = new RelayCommand(() =>
            {
                this.ScannerStockMode = ScannerMode.Scan;
                this.IsFocusedSaleModule = true;
                this.PalletCount = this.GetDefaultPalletCount();
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            });

            #endregion
       
            this.SetUpTimer();
            this.SelectedPartnerName = Strings.SelectCustomer;
            this.PartnerName = Strings.SelectCustomer;
            this.SaleDetails.Clear();
            this.Sale = null;
        }

        #endregion

        #region public interface

        #region property
        
        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string SelectedPartnerName
        {
            get
            {
                return this.partnerName;
            }

            set
            {
                this.partnerName = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; set; }

        /// <summary>
        /// Gets the loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Adds the dispatch stock to a pallet.
        /// </summary>
        protected override void AddToPalletCommandExecute()
        {
            try
            {
                if (this.PalletCount.Equals(this.GetDefaultPalletCount()))
                {
                    this.PalletCount = "0";
                    return;
                }

                Messenger.Default.Send(Token.Message, Token.CreatePalletisation);
                Messenger.Default.Send(this.itemsToPalletise, Token.PalletiseItems);
                this.PalletCount = this.GetDefaultPalletCount();
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Gets the default pallet value.
        /// </summary>
        /// <returns></returns>
        protected override string GetDefaultPalletCount()
        {
            return Strings.P;
        }

        ///// <summary>
        ///// Gets the current orders live status.
        ///// </summary>
        ///// <returns>The current orders live status.</returns>
        //protected override Sale GetLiveOrderStatus()
        //{
        //    if (this.Sale == null)
        //    {
        //        return null;
        //    }

        //    var localSale = ApplicationSettings.ShowBoxCountAtScannerDispatch
        //        ? this.DataManager.GetARDispatchesByIdWithBoxCount(this.Sale.SaleID)
        //        : this.DataManager.GetARDispatchesById(this.Sale.SaleID);
        //    return localSale;
        //}

        /// <summary>
        /// Opens the notes screen.
        /// </summary>
        protected override void OpenNotesWindow()
        {
            if (!this.NotesScreenCreated)
            {
                this.NotesScreenCreated = true;
                Messenger.Default.Send(Token.Message, Token.ShowScannerNotesWindow);
                Messenger.Default.Send(new CollectionData { DataContext = this }, Token.SetDataContext);
                return;
            }

            Messenger.Default.Send(false, Token.CloseNotesWindow);

            if (ApplicationSettings.Customer != Customer.Ballon)
            {
                Messenger.Default.Send(true, Token.DisplayNotesKeyboard);
            }
        }
       
        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                this.ScannerStockMode = ScannerMode.ScanPlusManualWeigh;
            }
            else if (this.ScannerStockMode == ScannerMode.ScanPlusManualWeigh)
            {
                this.ScannerStockMode = ScannerMode.ReWeigh;
            }
            else if (this.ScannerStockMode == ScannerMode.ReWeigh)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Set up the timers.
        /// </summary>
        private void SetUpTimer()
        {
            this.timer.Interval = TimeSpan.FromMilliseconds(ApplicationSettings.HandheldScannerDataWaitTime);
            this.timer.Tick += (sender, args) =>
            {
                try
                {
                    this.timer.Stop();
                    var data = this.ScannerText.RemoveNonIntegers();
                    this.ScannerText = string.Empty;
                    
                  Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Background,
                     new Action(() =>
                     {
                         this.HandleScannerData(data);
                     }));
                }
                finally
                {
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            };
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ScannerCustomersViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Scanner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.ViewModel.Purchases.APReceipt;

    public class ScannerCustomersViewModel : TouchscreenSuppliersViewModel
    {
        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScannerCustomersViewModel"/> class.
        /// </summary>
        public ScannerCustomersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.Partners = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.CustomerPartners.Select(x => x.Details).OrderBy(x => x.Name));
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Moves back to the main screen.
        /// </summary>
        protected override void MoveBackCommandExecute()
        {
            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerTouchscreen;
        }

        #endregion

        #region private



        #endregion
    }
}

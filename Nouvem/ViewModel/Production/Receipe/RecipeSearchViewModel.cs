﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Production.Receipe
{
    public class RecipeSearchViewModel : SalesSearchDataViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RecipeSearchViewModel"/> class.
        /// </summary>
        public RecipeSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.GetRecipes();
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            Messenger.Default.Send(this.selectedSale, Token.RecipeSelected);

            if (!this.KeepVisible)
            {
                this.Close();
            }
        }

        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseRecipeSearchWindow);
            this.FilteredSales.Clear();
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the recipes.
        /// </summary>
        private void GetRecipes()
        {
            this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetRecipes().OrderBy(x => x.INMaster));
        }

        #endregion

    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SpecificationsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.ViewModel.Interface;

namespace Nouvem.ViewModel.Production
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.Shared;

    public class SpecificationsViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The spec name.
        /// </summary>
        private string specName;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool searchMode;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool qualityAssured;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool nonQualityAssured;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool allowOverAge;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool allowUnderAge;

        /// <summary>
        /// The born in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.NouCountry> countriesBornIn;

        /// <summary>
        /// The reared in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.NouCountry> countriesRearedIn;

        /// <summary>
        /// The reared in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Plant> plants;

        /// <summary>
        /// The categories.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Category> categories;

        /// <summary>
        /// The specs.
        /// </summary>
        private ObservableCollection<ProductionData> specifications = new ObservableCollection<ProductionData>();

        /// <summary>
        /// The selected spec.
        /// </summary>
        private ProductionData selectedSpecification = new ProductionData();

        /// <summary>
        /// The selected copy from spec.
        /// </summary>
        private ProductionData selectedCopyFromSpecification = new ProductionData();

        /// <summary>
        /// The selected spec products.
        /// </summary>
        private ObservableCollection<ProductSelection> specificationProducts = new ObservableCollection<ProductSelection>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the SpecificationsViewModel class.
        /// </summary>
        public SpecificationsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessPricing);

            #region message registration

            Messenger.Default.Register<Tuple<int, bool>>(
                this,
                Token.PlantItemSelected,
                x =>
                {
                    this.UpdateCommandExecute();
                    if (x.Item1 == 0)
                    {
                        this.SelectAllPlants(x.Item2);
                    }
                });


            Messenger.Default.Register<Tuple<int, bool>>(
                this,
                Token.ItemSelected,
                x =>
                {
                    this.UpdateCommandExecute();
                    if (x.Item1 == 0)
                    {
                        this.SelectAllCategories(x.Item2);
                    }
                });

            Messenger.Default.Register<Tuple<int, bool, bool>>(
                this,
                Token.CountryItemSelected,
                x =>
                {
                    this.UpdateCommandExecute();
                    if (x.Item1 == 0)
                    {
                        this.SelectAllCountries(x.Item2, x.Item3);
                    }
                });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                });

            Messenger.Default.Register<Tuple<bool, int?>>(this, Token.UpdateProductSelections, tuple => this.UpdateSelections(tuple.Item1, tuple.Item2));

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.UpdateCommandExecute();
            });

            #endregion

            #region instantiation

            this.Products = new ObservableCollection<ProductSelection>();
            this.ProductsIn = new ObservableCollection<ProductSelection>();

            #endregion

            this.GetPlants();
            this.GetCountries();
            this.GetCategories();
            this.GetProducts();
            this.GetSpecifications();
            this.Refresh();
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the spec name.
        /// </summary>
        public string SpecName
        {
            get
            {
                return this.specName;
            }

            set
            {
                this.specName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool NonQualityAssured
        {
            get
            {
                return this.nonQualityAssured;
            }

            set
            {
                this.nonQualityAssured = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool QualityAssured
        {
            get
            {
                return this.qualityAssured;
            }

            set
            {
                this.qualityAssured = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool AllowOverAge
        {
            get
            {
                return this.allowOverAge;
            }

            set
            {
                this.allowOverAge = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool AllowUnderAge
        {
            get
            {
                return this.allowUnderAge;
            }

            set
            {
                this.allowUnderAge = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in find mode.
        /// </summary>
        public bool SearchMode
        {
            get
            {
                return this.searchMode;
            }

            set
            {
                this.searchMode = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // Find sale order mode, so set focus to inventory search combo box.
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
                }
            }
        }

        /// <summary>
        /// Gets or sets the born in countries.
        /// </summary>
        public ObservableCollection<NouCountry> CountriesBornIn
        {
            get
            {
                return this.countriesBornIn;
            }

            set
            {
                this.countriesBornIn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the reared in countries.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.Plant> Plants
        {
            get
            {
                return this.plants;
            }

            set
            {
                this.plants = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the reared in countries.
        /// </summary>
        public ObservableCollection<NouCountry> CountriesRearedIn
        {
            get
            {
                return this.countriesRearedIn;
            }

            set
            {
                this.countriesRearedIn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the specifications.
        /// </summary>
        public ObservableCollection<ProductionData> Specifications
        {
            get
            {
                return this.specifications;
            }

            set
            {
                this.specifications = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.Category> Categories
        {
            get
            {
                return this.categories;
            }

            set
            {
                this.categories = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected specification.
        /// </summary>
        public ProductionData SelectedSpecification
        {
            get
            {
                return this.selectedSpecification;
            }

            set
            {
                this.selectedSpecification = value;
                this.RaisePropertyChanged();

                if (value != null && value.Specification.PRSpecID > 0 && value.Specification.Name != Strings.NoneSelected && !this.EntitySelectionChange)
                {
                    this.HandleSelectedSpecification();
                }
                else
                {
                    this.SpecName = string.Empty;
                    this.SpecificationProducts.Clear();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected copy from specification.
        /// </summary>
        public ProductionData SelectedCopyFromSpecification
        {
            get
            {
                return this.selectedCopyFromSpecification;
            }

            set
            {
                this.selectedCopyFromSpecification = value;
                this.RaisePropertyChanged();

                if (value != null && value.Specification.Name != Strings.NoneSelected)
                {
                    this.HandleSelectedCopyFromSpecification();
                }
            }
        }

        /// <summary>
        /// Get or sets the products.
        /// </summary>
        public ObservableCollection<ProductSelection> ProductsIn { get; set; }

        /// <summary>
        /// Get or sets the products.
        /// </summary>
        public ObservableCollection<ProductSelection> Products { get; set; }

        /// <summary>
        /// Get or sets the specification products.
        /// </summary>
        public ObservableCollection<ProductSelection> SpecificationProducts
        {
            get
            {
                return this.specificationProducts;
            }

            set
            {
                this.specificationProducts = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the containers.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Updates all the selected nodes, sub nodes 'add to list' property'.
        /// </summary>
        /// <param name="selected">The add to list selection value.</param>
        /// <param name="nodeID">The node selected.</param>
        public void UpdateSelections(bool selected, int? nodeID)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            this.UpdateCommandExecute();

            // Set the nodes 'sub nodes' add to list property to that of the selected value.
            foreach (var product in this.Products.Where(x => x.ParentID == nodeID))
            {
                product.AddToList = selected;
            }

            this.SetSpecificationProducts();
        }

        #endregion

        #endregion

        #region protected override

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedSpecification == null || this.SelectedSpecification.SpecificationName == Strings.NoneSelected)
            {
                this.SelectedSpecification = this.Specifications.LastOrDefault();
                return;
            }

            var previousItem =
                this.Specifications.TakeWhile(
                        item => item.Specification.PRSpecID != this.SelectedSpecification.Specification.PRSpecID && this.SelectedSpecification.Specification.Name != Strings.NoneSelected)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedSpecification = previousItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedSpecification == null || this.SelectedSpecification.SpecificationName == Strings.NoneSelected)
            {
                this.SelectedSpecification = this.Specifications.FirstOrDefault(x => x.Specification.Name != Strings.NoneSelected);
                return;
            }

            var nextItem =
                this.Specifications.SkipWhile(
                    item => item.Specification.PRSpecID != this.SelectedSpecification.Specification.PRSpecID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedSpecification = nextItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            this.SelectedSpecification = this.Specifications.FirstOrDefault(x => x.SpecificationName != Strings.NoneSelected);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            this.SelectedSpecification = this.Specifications.LastOrDefault();
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            this.SelectedSpecification = this.Specifications.LastOrDefault();
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.SetControlMode(ControlMode.Find);
            this.SearchMode = true;

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new partner mode, so clear the partner fields.
                this.Refresh();
                this.SetControlMode(ControlMode.Add);
                this.SearchMode = false;
                Messenger.Default.Send(Token.Message, Token.FocusToDataUpdate);
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into find partner mode, so clear the partner fields.
                this.Refresh();
                this.SetControlMode(ControlMode.Find);
                this.SearchMode = true;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSpecification();
                    break;

                case ControlMode.Find:
                    //this.DisplaySearchScreen();
                    break;

                case ControlMode.Update:
                    this.UpdateSpecification();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Handles an update.
        /// </summary>
        private void UpdateCommandExecute()
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            if (this.SelectedSpecification == null || this.SelectedSpecification.Specification.PRSpecID == 0)
            {
                this.SetControlMode(ControlMode.Add);
                return;
            }

            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Adds a new spec.
        /// </summary>
        private void AddSpecification()
        {
            #region validation

            if (string.IsNullOrWhiteSpace(this.SpecName))
            {
                SystemMessage.Write(MessageType.Issue, Message.NoSpecNameEntered);
                return;
            }

            if (this.selectedSpecification == null)
            {
                this.SelectedSpecification = new ProductionData();
            }

            #endregion

            this.CreateSpecification();
            if (this.DataManager.AddSpecification(this.SelectedSpecification))
            {
                SystemMessage.Write(MessageType.Priority, Message.SpecificationAdded);
                this.Refresh();
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.SpecificationNotAdded);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Adds a new spec.
        /// </summary>
        private void UpdateSpecification()
        {
            #region validation

            if (this.selectedSpecification == null || this.selectedSpecification.Specification.Name == Strings.NoneSelected)
            {
                return;
            }

            #endregion

            this.CreateSpecification();
            if (this.DataManager.UpdateSpecification(this.SelectedSpecification))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                this.Refresh();
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Generates a specification.
        /// </summary>
        private void CreateSpecification()
        {
            this.SelectedSpecification.Specification.Name = this.SpecName;

            // get the id's of the products to be added
            var productsToAdd = new HashSet<int>();
            foreach (var specificationProduct in this.specificationProducts)
            {
                productsToAdd.Add(-specificationProduct.NodeID.ToInt());
            }

            this.SelectedSpecification.OutputsSpec.Clear();
            foreach (var i in productsToAdd)
            {
                this.SelectedSpecification.OutputsSpec.Add(new PRSpecOutput { INMasterID = i });
            }

            this.SelectedSpecification.CarcassTypesSpec.Clear();
            foreach (var category in this.Categories.Where(x => x.IsSelected && x.CategoryID > 0))
            {
                this.SelectedSpecification.CarcassTypesSpec.Add(new PRInputCarcassType { CarcassTypeID = category.CategoryID });
            }

            this.SelectedSpecification.CountriesSpec.Clear();
            foreach (var country in this.CountriesBornIn.Where(x => x.IsSelected && x.CountryID > 0))
            {
                this.SelectedSpecification.CountriesSpec.Add(new PRInputCountry { CountryID = country.CountryID, BornIn = true });
            }

            foreach (var country in this.CountriesRearedIn.Where(x => x.IsSelected && x.CountryID > 0))
            {
                this.SelectedSpecification.CountriesSpec.Add(new PRInputCountry { CountryID = country.CountryID, BornIn = false });
            }

            this.SelectedSpecification.PlantsSpec.Clear();
            foreach (var plant in this.Plants.Where(x => x.IsSelected && x.PlantID > 0))
            {
                this.SelectedSpecification.PlantsSpec.Add(new PRInputPlant { PlantID = plant.PlantID });
            }

            if ((this.QualityAssured && this.NonQualityAssured) || (!this.QualityAssured && !this.NonQualityAssured))
            {
                this.SelectedSpecification.Specification.QA = null;
            }
            else
            {
                this.SelectedSpecification.Specification.QA = this.QualityAssured;
            }

            if ((this.AllowOverAge && this.AllowUnderAge) || (!this.AllowOverAge && !this.AllowUnderAge))
            {
                this.SelectedSpecification.Specification.OverAge = null;
            }
            else
            {
                this.SelectedSpecification.Specification.OverAge = this.AllowOverAge;
            }
        }

        /// <summary>
        /// Handles a selected spec.
        /// </summary>
        private void HandleSelectedSpecification()
        {
            this.EntitySelectionChange = true;
            this.SpecificationProducts.Clear();
            var specProducts = this.SelectedSpecification.OutputsSpec.Select(x => (int?)x.INMasterID).ToList();

            foreach (var productSelection in this.Products)
            {
                //productSelection.AddToList = specProducts.Contains(productSelection.NodeID);
                if (specProducts.Contains(Math.Abs(productSelection.NodeID.ToInt())))
                {
                    productSelection.AddToList = true;
                }
                else
                {
                    productSelection.AddToList = false;
                }
            }

            var specCategories = this.SelectedSpecification.CarcassTypesSpec.Select(x => (int?)x.CarcassTypeID).ToList();
            foreach (var categorySelection in this.Categories)
            {
                categorySelection.IsSelected = specCategories.Contains(categorySelection.CategoryID);
            }

            var specCountriesBornIn = this.SelectedSpecification.CountriesSpec.Where(c => c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = specCountriesBornIn.Contains(country.CountryID);
            }

            var specCountriesRearedIn = this.SelectedSpecification.CountriesSpec.Where(c => !c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = specCountriesRearedIn.Contains(country.CountryID);
            }

            var specPlants = this.SelectedSpecification.PlantsSpec.Select(x => (int?)x.PlantID).ToList();
            foreach (var plant in this.Plants)
            {
                plant.IsSelected = specPlants.Contains(plant.PlantID);
            }

            if (!this.SelectedSpecification.Specification.QA.HasValue)
            {
                this.QualityAssured = true;
                this.NonQualityAssured = true;
            }
            else
            {
                this.QualityAssured = this.SelectedSpecification.Specification.QA.ToBool();
                this.NonQualityAssured = !this.QualityAssured;
            }

            if (!this.SelectedSpecification.Specification.OverAge.HasValue)
            {
                this.AllowOverAge = true;
                this.AllowUnderAge = true;
            }
            else
            {
                this.AllowOverAge = this.SelectedSpecification.Specification.OverAge.ToBool();
                this.AllowUnderAge = !this.AllowOverAge;
            }

            this.EntitySelectionChange = false;
            this.SetSpecificationProducts();
        }

        /// <summary>
        /// Handles a selected spec.
        /// </summary>
        private void HandleSelectedCopyFromSpecification()
        {
            this.EntitySelectionChange = true;
            var specProducts = this.SelectedCopyFromSpecification.OutputsSpec.Select(x => (int?)x.INMasterID).ToList();
            foreach (var productSelection in this.Products)
            {
                //productSelection.AddToList = specProducts.Contains(productSelection.NodeID);
                if (specProducts.Contains(Math.Abs(productSelection.NodeID.ToInt())))
                {
                    productSelection.AddToList = true;
                }
                else
                {
                    productSelection.AddToList = false;
                }
            }

            var specCategories = this.SelectedCopyFromSpecification.CarcassTypesSpec.Select(x => (int?)x.CarcassTypeID).ToList();
            foreach (var categorySelection in this.Categories)
            {
                categorySelection.IsSelected = specCategories.Contains(categorySelection.CategoryID);
            }

            var specCountriesBornIn = this.SelectedCopyFromSpecification.CountriesSpec.Where(c => c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = specCountriesBornIn.Contains(country.CountryID);
            }

            var specCountriesRearedIn = this.SelectedCopyFromSpecification.CountriesSpec.Where(c => !c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = specCountriesRearedIn.Contains(country.CountryID);
            }

            var specPlants = this.SelectedCopyFromSpecification.PlantsSpec.Select(x => (int?)x.PlantID).ToList();
            foreach (var plant in this.Plants)
            {
                plant.IsSelected = specPlants.Contains(plant.PlantID);
            }

            this.QualityAssured = this.SelectedCopyFromSpecification.Specification.QA.ToBool();
            this.AllowOverAge = this.SelectedCopyFromSpecification.Specification.OverAge.ToBool();

            this.EntitySelectionChange = false;
            this.SetSpecificationProducts();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearSpecifications();
            Messenger.Default.Send(Token.Message, Token.CloseSpecificationsWindow);
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        private void GetCategories()
        {
            var localCategories = (from cat in this.DataManager.GetCategories()
                                   select new Model.BusinessObject.Category
                                   {
                                       CategoryID = cat.CategoryID,
                                       Name = cat.Name,
                                       Description = cat.Description,
                                       CatType = cat.CatType
                                   }).ToList();

            localCategories.Insert(0, new Category());
            this.Categories = new ObservableCollection<Model.BusinessObject.Category>(localCategories);
        }

        /// <summary>
        /// Gets the application products.
        /// </summary>
        private void GetProducts()
        {
            this.Products.Clear();
            this.ProductsIn.Clear();
            var localProducts = this.DataManager.GetGroupedProducts().OrderBy(x => x.ProductName);
            foreach (var product in localProducts)
            {
                this.Products.Add(product);
                this.ProductsIn.Add(product);
            }
        }

        /// <summary>
        /// Gets the application specs.
        /// </summary>
        private void GetSpecifications()
        {
            this.Specifications.Clear();
            var specs = this.DataManager.GetSpecifications();
            foreach (var productionData in specs)
            {
                this.Specifications.Add(productionData);
            }

            var defaultSpec = new ProductionData { Specification = new PRSpec { Name = Strings.NoneSelected } };
            this.Specifications.Insert(0, defaultSpec);
            this.SelectedSpecification = this.Specifications.First();
            this.SelectedCopyFromSpecification = this.Specifications.First();
        }

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void Refresh()
        {
            this.ClearForm();
            this.RefreshProducts();
            this.GetSpecifications();
        }

        /// <summary>
        /// Sets the spec products.
        /// </summary>
        private void SetSpecificationProducts()
        {
            this.SpecificationProducts.Clear();
            var localProducts = this.Products
                .Where(x => x.AddToList && x.IsLeafNode)
                .OrderBy(prod => prod.GroupName).ThenBy(prod => prod.ProductName).ToList();

            foreach (var productSelection in localProducts)
            {
                this.SpecificationProducts.Add(productSelection);
            }

            this.EntitySelectionChange = true;
            var specGroups = this.SpecificationProducts.Select(x => x.ParentID).Distinct().ToList();
            var groups = this.Products.Where(x => !x.IsLeafNode && specGroups.Contains(x.NodeID)).ToList();
            foreach (var group in groups)
            {
                var count = this.Products.Count(x => x.IsLeafNode && x.ParentID == group.NodeID);
                var specProductsCount = this.SpecificationProducts.Count(x => x.ParentID == group.NodeID);
                if (count == specProductsCount)
                {
                    group.AddToList = true;
                }
            }

            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Refreshes the product selections.
        /// </summary>
        private void RefreshProducts()
        {
            this.EntitySelectionChange = true;
            foreach (var productSelection in this.Products)
            {
                productSelection.AddToList = false;
            }

            Messenger.Default.Send(Token.Message, Token.CollapseGroups);
            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Gets the application plants.
        /// </summary>
        private void GetPlants()
        {
            var localPlants = this.DataManager.GetAllPlants();
            localPlants.Insert(0, new Model.DataLayer.Plant());

            this.Plants = new ObservableCollection<Nouvem.Model.BusinessObject.Plant>(
                (from x in localPlants
                 select new Nouvem.Model.BusinessObject.Plant
                 {
                     PlantID = x.PlantID,
                     CountryID = x.CountryID,
                     Code = x.Code,
                     SiteName = x.SiteName
                 }).ToList());
        }

        /// <summary>
        /// Gets the application countries.
        /// </summary>
        private void GetCountries()
        {
            var localCountries = this.DataManager.GetCountryMaster();
            localCountries.Insert(0, new Country());
            this.CountriesBornIn = new ObservableCollection<NouCountry>(
                (from x in localCountries
                 select new NouCountry
                 {
                     CountryID = x.CountryID,
                     Name = x.Name,
                     Code = x.Code,
                     BornIn = true
                 }).ToList());

            this.CountriesRearedIn = new ObservableCollection<NouCountry>(
                (from x in localCountries
                 select new NouCountry
                 {
                     CountryID = x.CountryID,
                     Name = x.Name,
                     Code = x.Code
                 }).ToList());
        }

        /// <summary>
        /// Selects/deselects all plants.
        /// </summary>
        /// <param name="select">The select flag.</param>
        private void SelectAllPlants(bool select)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            foreach (var plant in this.Plants.Where(x => x.PlantID > 0))
            {
                plant.IsSelected = select;
            }
        }

        /// <summary>
        /// Selects/deselects all categories.
        /// </summary>
        /// <param name="select">The select flag.</param>
        private void SelectAllCategories(bool select)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            foreach (var category in this.Categories.Where(x => x.CategoryID > 0))
            {
                category.IsSelected = select;
            }
        }

        /// <summary>
        /// Selects/deselects all countries.
        /// </summary>
        /// <param name="select">The select flag.</param>
        private void SelectAllCountries(bool select, bool bornIn)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            if (bornIn)
            {
                foreach (var country in this.CountriesBornIn.Where(x => x.CountryID > 0))
                {
                    country.IsSelected = select;
                }
            }
            else
            {
                foreach (var country in this.CountriesRearedIn.Where(x => x.CountryID > 0))
                {
                    country.IsSelected = select;
                }
            }
        }

        /// <summary>
        /// Clears the form.
        /// </summary>
        private void ClearForm()
        {
            this.SpecName = string.Empty;
            this.EntitySelectionChange = true;
            this.SelectedSpecification = this.Specifications.FirstOrDefault(x => x.Specification.Name == Strings.NoneSelected);
            this.SelectedCopyFromSpecification = this.Specifications.FirstOrDefault(x => x.Specification.Name == Strings.NoneSelected);
            foreach (var category in this.Categories)
            {
                category.IsSelected = false;
            }

            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = false;
            }

            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = false;
            }

            foreach (var plant in this.Plants)
            {
                plant.IsSelected = false;
            }

            this.QualityAssured = false;
            this.NonQualityAssured = false;

            this.AllowOverAge = false;
            this.AllowUnderAge = false;

            this.EntitySelectionChange = false;
            this.SpecificationProducts.Clear();
        }

        #endregion
    }
}



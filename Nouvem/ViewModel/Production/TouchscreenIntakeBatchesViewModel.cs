﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Production
{
    public class TouchscreenIntakeBatchesViewModel : TouchscreenProductionOrdersViewModel
    {
        #region field

        private int? productId;

     

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenIntakeBatchesViewModel"/> class.
        /// </summary>
        public TouchscreenIntakeBatchesViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the current recipe product order method.
        /// </summary>
        public string NouOrderMethod { get; set; }

        #endregion

        #region command


        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Gets the orders.
        /// </summary>
        public override void GetAllOrders(int? inmasterId = null, bool useIntakeBatches = false, int? orderBatchDataID = null)
        {
            this.productId = inmasterId;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                var data = this.DataManager.GetProductionOrders(this.allOrders, true, inmasterId)
                    .OrderBy(x => x.OrderDate).ToList();
                if (orderBatchDataID.HasValue)
                {
                    data = data.Where(x => x.BatchID == orderBatchDataID).ToList();
                }

                this.allOrders = new ObservableCollection<ProductionData>(data);
                this.Orders.Clear();

                foreach (var productionData in this.AllOrders)
                {
                    productionData.OrderMethod = this.NouOrderMethod;
                }

                foreach (var productionData in AllOrders)
                {
                    this.Orders.Add(productionData);
                }
            }));
        }

        /// <summary>
        /// Gets the orders.
        /// </summary>
        public void GetAllOrdersNoThread(int? inmasterId = null, bool useIntakeBatches = false)
        {
            this.productId = inmasterId;
            this.allOrders = new ObservableCollection<ProductionData>(this.DataManager.GetProductionOrders(this.allOrders, true, inmasterId).OrderBy(x => x.OrderDate));
            this.Orders.Clear();

            foreach (var productionData in this.AllOrders)
            {
                productionData.OrderMethod = this.NouOrderMethod;
            }

            foreach (var productionData in AllOrders)
            {
                this.Orders.Add(productionData);
            }
        }

        /// <summary>
        /// Handles an orders search.
        /// </summary>
        protected override void HandleOrdersSearch()
        {
            var value = this.OrdersSearchText;
            if (string.IsNullOrEmpty(value))
            {
                this.Orders.Clear();
            }
            else
            {
                var localOrders = this.AllOrders.Where(x => !string.IsNullOrWhiteSpace(x.Reference) && x.Reference.StartsWithIgnoringCase(value));
                this.Orders = new ObservableCollection<ProductionData>(localOrders);
            }
        }
        
        /// <summary>
        /// Handles the selected intake batch.
        /// </summary>
        public override void HandleSelectedOrder()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }

            if (this.CompleteOrder)
            {
                this.CompleteIntakeOrder();
                return;
            }

            //if ((this.NouOrderMethod.CompareIgnoringCase(Constant.Quantity) && this.SelectedOrder.RemainingQty <= 0)
            //    || (this.NouOrderMethod.CompareIgnoringCase(Constant.Weight) && this.SelectedOrder.RemainingWgt <= 0))
            //{
            //    Application.Current.Dispatcher.Invoke(() =>
            //    {
            //        NouvemMessageBox.Show(Message.BatchEmptyPrompt, touchScreen: true);
            //    });
                
            //    return;
            //}

            Messenger.Default.Send(this.SelectedOrder, Token.BatchNumberSelected);
            this.MoveBackCommandExecute();
        }

        /// <summary>
        /// Move back to the previous module.
        /// </summary>
        protected override void MoveBackCommandExecute()
        {
            ViewModelLocator.ClearTouchscreenIntakeBatches();
            Messenger.Default.Send(true, Token.CloseTouchscreenIntakeBatches);
        }

        #endregion

        #region private

        private void CompleteIntakeOrder()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }

            try
            {
                NouvemMessageBox.Show(Message.CompleteBatchVerification, NouvemMessageBoxButtons.YesNo, touchScreen: true, scanner: ApplicationSettings.ScannerMode);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                if (this.SelectedOrder.Generic3.Equals("Intake"))
                {
                    if (this.DataManager.ChangeAPReceiptDetailOrderStatus(this.SelectedOrder.BatchID.ToInt(), 3));
                    {
                        this.GetAllOrders(this.productId);
                    }
                }
                else
                {
                    if (this.DataManager.ChangeRecipeBatchStatus(this.SelectedOrder.Order.PROrderID, 1))
                    {
                        this.GetAllOrders(this.productId);
                    }
                }
            }
            finally
            {
                this.CompleteOrder = false;
            }
        }

        #endregion
    }
}

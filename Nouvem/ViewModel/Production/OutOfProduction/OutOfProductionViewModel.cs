﻿// -----------------------------------------------------------------------
// <copyright file="OutOfProductionViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Production.OutOfProduction
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class OutOfProductionViewModel : TransactionsHelperViewModel
    {
        #region field

        /// <summary>
        /// Holds the default process name.
        /// </summary>
        private string defaultProcess;

        /// <summary>
        /// The production spec name.
        /// </summary>
        private string spec;

        /// <summary>
        /// The pcustomer name.
        /// </summary>
        private string customerName;

        /// <summary>
        /// The resetting of the customer flag. (Stored product customer is temporarily ignored).
        /// </summary>
        private bool resettingCustomer;

        /// <summary>
        /// The box/pieces button content.
        /// </summary>
        private string boxPieceButtonText;

        /// <summary>
        /// The piece labels to print.
        /// </summary>
        private int pieceLabelsToPrint;

        /// <summary>
        /// The default product selected.
        /// </summary>
        private InventoryItem defaultProduct;

        /// <summary>
        /// The incoming indicator weight clead read flag.
        /// </summary>
        private bool cleanRead;

        /// <summary>
        /// The incoming manually set weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The incoming tare weight.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The incoming pieces count.
        /// </summary>
        private int pieces;

        /// <summary>
        /// The incoming product quantity.
        /// </summary>
        private decimal productQuantity;

        /// <summary>
        /// The selected order.
        /// </summary>
        private ProductionData selectedOrder;

        /// <summary>
        /// The product warehouse location.
        /// </summary>
        private Warehouse selectedWarehouse;

        /// <summary>
        /// The production details collection.
        /// </summary>
        private ObservableCollection<SaleDetail> productionDetails = new ObservableCollection<SaleDetail>();

        /// <summary>
        /// The selected product detail.
        /// </summary>
        private SaleDetail selectedProductionDetail;

        /// <summary>
        /// The selected customer.
        /// </summary>
        private ViewBusinessPartner selectedCustomer;

        /// <summary>
        /// All the products.
        /// </summary>
        private IList<SaleDetail> allProducts;

        /// <summary>
        /// The products view.
        /// </summary>
        private ViewModelBase productsViewModel;

        /// <summary>
        /// The header view.
        /// </summary>
        private ViewModelBase headerViewModel;

        /// <summary>
        /// Piece label creation process flag.
        /// </summary>
        private bool creatingPieceAndBoxLabels;

        /// <summary>
        /// Piece label creation process flag.
        /// </summary>
        private bool creatingPieceLabel;

        /// <summary>
        /// The current batch trace data.
        /// </summary>
        private IList<BatchTraceability> currentBatchTraceabilities = new List<BatchTraceability>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OutOfProductionViewModel"/> class.
        /// </summary>
        public OutOfProductionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.PrintBarcode, s =>
            {
                if (this.IsFormLoaded)
                {
                    //this.PrintBarcode();
                }
            });

            // Register for the label change name.
            Messenger.Default.Register<Model.DataLayer.Process>(this, Token.ProcessSelected, i =>
            {
                var checkNotMade = this.DataManager.GetProcessStartUpChecks(i.ProcessID);
                if (checkNotMade.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.MissingProcessStartUpCheckMessage = string.Format(Message.HACCPNotCompleted, i.Name.Trim(), checkNotMade.First());
                        SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                        NouvemMessageBox.Show(this.MissingProcessStartUpCheckMessage, touchScreen: true);
                    }));

                    this.DisableModule = true;
                    return;
                }

                this.DisableModule = false;

                this.SetOutOfProductionMode(i);
                if (this.Locator.ProcessSelection.SaveAsDefaultProcess)
                {
                    this.DataManager.SetDefaultModuleProcess(ViewType.OutOfProduction, ApplicationSettings.OutOfProductionMode.ToString(), NouvemGlobal.DeviceId.ToInt());
                    this.defaultProcess = ApplicationSettings.OutOfProductionMode.ToString();
                }
            });

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowOutOfProductionAttributes = b);

            Messenger.Default.Register<string>(this, KeypadTarget.ProductionPieces, s =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.indicatorWeight = this.Locator.Indicator.Weight;
                    this.RecordPieceLabels(s);
                }));
            });

            // Register for the incoming warehouse selection.
            Messenger.Default.Register<Warehouse>(this, Token.WarehouseSelected, o =>
            {
                this.selectedWarehouse = o;
            });

            // Register for the system message stock location change.
            Messenger.Default.Register<string>(this, Token.DisplayGenericScreenSelection, s => Messenger.Default.Send(Token.Message, Token.DisplayGenericSelection));

            // Register for the incoming message to remove the selected customer.
            Messenger.Default.Register<string>(this, Token.RemovePartner, s =>
            {
                this.resettingCustomer = true;
                this.SelectedCustomer = null;
            });

            // Register for the incoming supplier.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.SupplierSelected, s =>
            {
                this.resettingCustomer = true;
                this.SelectedCustomer = s;
            });

            // Deselect the selected detail.
            Messenger.Default.Register<string>(this, Token.DeselectProduct, s =>
            {
                this.SelectedProductionDetail = null;
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                if (this.ScannerStockMode == ScannerMode.Reprint)
                {
                    var transaction = this.DataManager.GetStockTransaction(s.ToInt());

                    if (transaction != null)
                    {
                        try
                        {
                            this.PrintManager.ReprintLabel(transaction.StockTransactionID);
                        }
                        catch (Exception ex)
                        {
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }

                    return;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.HandleScannerData(s);
                }));
            });

            Messenger.Default.Register<ProductionData>(this, Token.ProductionOrderSelected, p =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                    {
                        NouvemMessageBox.Show(Message.OutOfInjectionPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.No)
                        {
                            return;
                        }
                    }

                    //this.ClearAttributes();
                    this.ClearAttributeControls();
                    this.SelectedOrder = p;
                    this.SelectedProductionDetail = null;
                    this.SelectedCustomer = null;

                    if (ApplicationSettings.UseDefaultProductAtOutOfProduction)
                    {
                        this.defaultProduct =
                            NouvemGlobal.InventoryItems.FirstOrDefault(
                                x => x.Master.INMasterID == ApplicationSettings.DefaultProductAtOutOfProductionID);
                        if (this.defaultProduct != null)
                        {
                            this.Locator.ProductionProductsCards.SelectedProduct = this.defaultProduct;
                        }
                    }

                    if (this.selectedOrder != null)
                    {
                        //this.selectedOrder.LastBatchAttribute = this.DataManager.GetLastBatchAttributeData(this.selectedOrder.Order.BatchNumberID,
                        //    NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.TransactionTypeProductionReceiptId);
                        //this.selectedOrder.ProductionAttributesSet = false;

                        if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
                        {
                            this.Locator.Touchscreen.Quantity = this.selectedOrder.Order.BatchQty.ToDecimal();
                        }
                    }

                    this.CheckAttributeBatchChangeReset();
                }));
            });

            // Register for an incoming keypad value for the pieces.
            Messenger.Default.Register<decimal>(this, Token.QuantityUpdate, i =>
            {
                this.productQuantity = i;
                this.SetTypicalWeight();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (this.creatingPieceAndBoxLabels)
                    {
                        if (i <= 0)
                        {
                            this.Locator.Touchscreen.Quantity = 1;
                            return;
                        }

                        if (this.SelectedProductionDetail != null
                            && this.SelectedProductionDetail.InventoryItem != null
                            && this.SelectedProductionDetail.InventoryItem.Master.BoxTareContainer != null)
                        {
                            double piecesTare = 0;
                            if (this.SelectedProductionDetail.InventoryItem.Master.PiecesTareContainer != null)
                            {
                                piecesTare = this.SelectedProductionDetail.InventoryItem.Master.PiecesTareContainer.Tare;
                            }

                            var boxTare = this.SelectedProductionDetail.InventoryItem.Master.BoxTareContainer.Tare.ToDecimal();
                            var accumulateTare = this.SelectedProductionDetail.InventoryItem.Master.AccumulateTares.ToBool();

                            var localTare = boxTare;
                            if (accumulateTare)
                            {
                                localTare = boxTare + (piecesTare.ToDecimal() * this.ProductQuantity.ToInt());
                            }

                            this.Locator.Indicator.Tare = Math.Round(localTare.ToDouble(), 2);
                        }
                    }
                }));
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                if (this.creatingPieceAndBoxLabels)
                {
                    if (this.SelectedProductionDetail != null && this.SelectedProductionDetail.InventoryItem != null)
                    {
                        var message = string.Format(Message.PieceItemLabelWarning,
                            this.SelectedProductionDetail.InventoryItem.Master.Name);
                        SystemMessage.Write(MessageType.Issue, message);
                    }

                    return;
                }

                this.manualWeight = false;
                this.RecordWeight();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                if (this.creatingPieceAndBoxLabels)
                {
                    if (this.SelectedProductionDetail != null && this.SelectedProductionDetail.InventoryItem != null)
                    {
                        var message = string.Format(Message.PieceItemLabelWarning,
                            this.SelectedProductionDetail.InventoryItem.Master.Name);
                        SystemMessage.Write(MessageType.Issue, message);
                    }

                    return;
                }

                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;
                this.RecordWeight();
            });

            // Register for the indicator tare weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            // register for the incoming traceability results.
            //Messenger.Default.Register<Tuple<string, List<TraceabilityResult>>>(this, Token.TraceabilityValues, o =>
            //{
            //    if (this.creatingPieceLabel)
            //    {
            //        this.RecordPieceWeightComplete(o);
            //        return;
            //    }

            //    this.RecordWeightComplete(o);
            //});

            #endregion

            #region command handler

            this.ViewProductSpecCommand = new RelayCommand(() =>
            {
                var rowData = new List<Tuple<string, string, string>>();
                rowData.Add(Tuple.Create(string.Empty, "INMasterID", this.SelectedProductionDetail.InventoryItem.INMasterID.ToString()));
                var reportParams = new List<ReportParam>();
                reportParams.Add(new ReportParam { Property = "INMasterID", Name = "INMasterID" });
                this.ReportManager.ShowReport(new CollectionData { RowData = rowData }, new ReportData { Name = "SubReport Product Spec", ReportParams = reportParams });
            }, () => this.SelectedProductionDetail != null);

            // Handle the box label print functionality.
            this.BoxLabelCommand = new RelayCommand(() =>
            {
                if (this.creatingPieceAndBoxLabels)
                {
                    this.RecordBoxWeight();
                    return;
                }

                this.BoxLabelCommandExecute();
            });

            this.PieceLabelCommand = new RelayCommand(this.PieceLabelCommandExecute);

            // Handle the view switch.
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);

            // Handle the ui loaded event.
            this.OnLoadedCommand = new RelayCommand(this.OnLoadedCommandExecute);

            // Handle the ui unloaded event.
            this.OnUnloadedCommand = new RelayCommand(this.OnUnloadedCommandExecute);

            // Handle a products products ui swap.
            this.SwapViewCommand = new RelayCommand(() =>
            {
                if (this.ProductsViewModel == ViewModelLocator.ProductionProductsCardsStatic)
                {
                    ViewModelLocator.ProductionProductsCardsStatic.PROrderID = this.SelectedOrder?.Order?.PROrderID;
                    this.ProductsViewModel = ViewModelLocator.ProductionProductsGridStatic;
                    ViewModelLocator.ProductionProductsCardsStatic.GridViewLoaded = true;
                    return;
                }

                ViewModelLocator.ProductionProductsCardsStatic.GridViewLoaded = false;
                this.ProductsViewModel = ViewModelLocator.ProductionProductsCardsStatic;
            });

            #endregion

            this.GetDateDays();
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowOutOfProductionAttributes;
            this.Locator.TouchscreenProductionOrders.GetOrders();
            this.GetAttributeLookUps();
            this.GetAttributeAllocationData();
            this.ClearAttributes();
            this.ClearAttributeControls();
            ViewModelLocator.CreateWarehouseSelection();

            if (ApplicationSettings.CanSelectCustomersAtOutOfProduction)
            {
                this.HeaderViewModel = this.Locator.OutOfProductionSplitHeader;
            }
            else
            {
                this.HeaderViewModel = this.Locator.OutOfProductionHeader;
            }

            if (ApplicationSettings.Customer == Customer.Ballon)
            {
                this.BoxPieceButtonText = Strings.Piece;
            }
            else
            {
                this.BoxPieceButtonText = Strings.Box;
            }

            this.SelectedCustomer = null;
            this.SetUpTimers();
            this.GetAllProducts();
            this.GetProductsView();
            this.Locator.Touchscreen.OutOfProductionExtensionWidth = ApplicationSettings.OutOfProductionProductsHomeMenuWidth;
            ViewModelLocator.ClearStockMovementTouchscreen();
            this.ScannerModeText = "+";
            this.StockLabelsToPrint = 1;
            NouvemGlobal.CheckBatchValuesSet = true;
            this.Spec = Message.SelectProductionOrder;
            this.Locator.Indicator.WeighMode = WeighMode.Manual;
            this.Locator.Touchscreen.UIText = Strings.TerminalLocation;
            this.OpenScanner();
            this.CloseHiddenWindows();
            this.defaultProcess = ApplicationSettings.OutOfProductionMode.ToString();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets a products typical weight.
        /// </summary>
        public decimal? TypicalWeight
        {
            get
            {
                if (this.SelectedProductionDetail?.InventoryItem != null &&
                    !this.SelectedProductionDetail.InventoryItem.Master.NominalWeight.IsNullOrZero())
                {
                    return this.SelectedProductionDetail.InventoryItem.Master.NominalWeight.ToDecimal();
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the warehouse.
        /// </summary>
        public Warehouse SelectedWarehouse
        {
            get
            {
                return this.selectedWarehouse;
            }
        }

        /// <summary>
        /// Gets or sets the customer selected.
        /// </summary>
        public ViewBusinessPartner SelectedCustomer
        {
            get
            {
                return this.selectedCustomer;
            }

            set
            {
                this.selectedCustomer = value;
                if (value == null)
                {
                    this.CustomerName = Strings.SelectCustomerForLabel;
                }
                else
                {
                    this.CustomerName = value.Name;
                }

                this.HandleSelectedCustomer();
            }
        }

        /// <summary>
        /// Gets or sets the box/pieces button content.
        /// </summary>
        public string BoxPieceButtonText
        {
            get
            {
                return this.boxPieceButtonText;
            }

            set
            {
                this.boxPieceButtonText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string CustomerName
        {
            get
            {
                return this.customerName;
            }

            set
            {
                this.customerName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the production order name.
        /// </summary>
        public string Spec
        {
            get
            {
                return this.spec;
            }

            set
            {
                this.spec = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the product qty selected.
        /// </summary>
        public decimal ProductQuantity
        {
            get
            {
                return this.productQuantity;
            }

            set
            {
                this.productQuantity = value;
                Messenger.Default.Send(value, Token.QuantitySelected);
            }
        }

        /// <summary>
        /// Gets or sets the products view to display..
        /// </summary>
        public ViewModelBase ProductsViewModel
        {
            get
            {
                return this.productsViewModel;
            }

            set
            {
                this.productsViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the header view to display.
        /// </summary>
        public ViewModelBase HeaderViewModel
        {
            get
            {
                return this.headerViewModel;
            }

            set
            {
                this.headerViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the production details.
        /// </summary>
        public ObservableCollection<SaleDetail> ProductionDetails
        {
            get
            {
                return this.productionDetails;
            }

            set
            {
                this.productionDetails = value;
                this.RaisePropertyChanged();

                foreach (var saleDetail in value)
                {
                    // shouldn't happen..but just in case
                    if (saleDetail.InventoryItem == null)
                    {
                        saleDetail.InventoryItem =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == saleDetail.INMasterID);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the sale detail.
        /// </summary>
        public SaleDetail SelectedProductionDetail
        {
            get
            {
                return this.selectedProductionDetail;
            }

            set
            {
                this.selectedProductionDetail = value;
                this.RaisePropertyChanged();

                if (value != null && this.SelectedOrder != null)
                {
                    if (value.InventoryItem != null)
                    {
                        this.creatingPieceAndBoxLabels = value.InventoryItem.Master.PrintPieceLabels.ToBool();
                        if (this.creatingPieceAndBoxLabels)
                        {
                            if (value.InventoryItem != null
                                && value.InventoryItem.Master.BoxTareContainer != null)
                            {
                                var boxTare = value.InventoryItem.Master.BoxTareContainer.Tare.ToDecimal();
                                var accumulateTare = value.InventoryItem.Master.AccumulateTares.ToBool();

                                var localTare = boxTare;
                                if (accumulateTare)
                                {
                                    if (this.pieces == 0)
                                    {
                                        this.pieces = 1;
                                    }

                                    localTare = boxTare + (this.tare * this.pieces);
                                }

                                this.Locator.Indicator.Tare = Math.Round(localTare.ToDouble(), 2);
                            }
                        }
                    }

                    //value.StockMode = StockMode.NoMode;
                    foreach (var detail in this.ProductionDetails)
                    {
                        detail.IsSelectedSaleDetail = false;
                    }

                    value.IsSelectedSaleDetail = true;

                    this.CheckAttributeProductChangeReset(this.SelectedProductionDetail);
                    this.HandleSaleDetail(this.SelectedProductionDetail, this);

                    //if (!value.GoodsReceiptDatas.Any())
                    //{

                    //    var transactionData = new List<StockTransactionData>();
                    //    var localDetail = this.DisplayProductionDetails.FirstOrDefault(x => x.InventoryItem != null && value.InventoryItem != null && x.InventoryItem.Master.INMasterID == value.InventoryItem.Master.INMasterID);
                    //    if (localDetail != null)
                    //    {
                    //        this.Log.LogDebug(this.GetType(), string.Format("{0} found in DispalyproductionDetails", value.InventoryItem.Master.INMasterID));
                    //        var localReceiptData = localDetail.GoodsReceiptDatas.FirstOrDefault();
                    //        if (localReceiptData != null)
                    //        {
                    //            transactionData = localReceiptData.TransactionData;
                    //            this.Log.LogDebug(this.GetType(), string.Format("Using GoodsReceiptData - transaction Count:{0}", transactionData.Count));
                    //        }
                    //    }

                    //    value.GoodsReceiptDatas.Add(new GoodsReceiptData
                    //    {
                    //        BatchNumber = this.SelectedOrder.BatchNumber, 
                    //        TransactionData = transactionData,
                    //        BatchTraceabilities = this.currentBatchTraceabilities != null ? this.currentBatchTraceabilities.ToList() : new List<BatchTraceability>(),
                    //        ProductionBatchKillDate = this.SelectedOrder.KillDate,
                    //        ProductionBatchBornIn = this.SelectedOrder.BornIn,
                    //        ProductionBatchRearedIn = this.SelectedOrder.RearedIn,
                    //        ProductionBatchSlaughteredIn = this.SelectedOrder.SlaughteredIn,
                    //        ProductionBatchCutIn = this.SelectedOrder.CutIn
                    //    });
                    //}

                    //this.Locator.ProductionDetails.SelectedSaleDetail = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected order.
        /// </summary>
        public ProductionData SelectedOrder
        {
            get
            {
                return this.selectedOrder;
            }

            set
            {
                this.selectedOrder = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedOrder();
                }
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        public void HandleProductSelection(InventoryItem product)
        {
            int pieces = 0;
            //if (product.Master.TypicalPieces.HasValue)
            //{
            pieces = product.Master.TypicalPieces.ToInt();
            // }

            if (ApplicationSettings.OutOfProductionMode != OutOfProductionMode.OutOfButchery)
            {
                this.Locator.Touchscreen.Quantity = pieces;
            }

            var localSaleDetail = new SaleDetail
            {
                InventoryItem = product,
                WeightOutOfProduction = 0,
                QuantityOutOfProduction = 0
            };

            localSaleDetail.SetFirstPriceMethod();

            if (ApplicationSettings.ZeroTareOutsideOfDispatch)
            {
                this.Locator.Indicator.Tare = 0;
            }
            else
            {
                this.Locator.Indicator.Tare = product.PiecesTareWeight;
            }

            this.IndicatorManager.SetScales(product.Master.Scales);
            this.ProductionDetails.Add(localSaleDetail);
            this.SelectedProductionDetail = localSaleDetail;
            if (this.selectedWarehouse != null && this.selectedWarehouse.LocationSetOnProduct.IsTrue())
            {
                this.selectedWarehouse = null;
            }

            if (product.Master.WarehouseID == null)
            {
                this.Locator.WarehouseSelection.SetWarehouse(null);
            }
            else
            {
                this.selectedWarehouse =
                    NouvemGlobal.WarehouseLocations.FirstOrDefault(x => x.WarehouseID == product.Master.WarehouseID);
                if (this.selectedWarehouse != null)
                {
                    this.selectedWarehouse.LocationSetOnProduct = true;
                    this.Locator.WarehouseSelection.SetWarehouse(this.selectedWarehouse);
                }
                else
                {
                    this.Locator.WarehouseSelection.SetWarehouse(null);
                }
            }

            if (ApplicationSettings.RecallCustomerFromProductAtOutOfProduction)
            {
                if (!this.resettingCustomer)
                {
                    if (product.Master.BPMasterID_LabelPartner == null)
                    {
                        this.SelectedCustomer = null;
                        return;
                    }

                    var localCustomer =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == product.Master.BPMasterID_LabelPartner);
                    if (localCustomer != null)
                    {
                        this.SelectedCustomer = localCustomer.Details;
                    }
                }
            }

            //this.SetTypicalWeight();
        }

        /// <summary>
        /// Adds the incoming dispatch product.
        /// </summary>
        /// <param name="detail">The incoming sale detail.</param>
        public void SetProductDetail(SaleDetail detail)
        {
            if (this.ProductionDetails == null)
            {
                this.ProductionDetails = new ObservableCollection<SaleDetail>();
            }

            if (this.ProductionDetails.Any())
            {
                if (this.ProductionDetails.Any(x => x.INMasterID == detail.INMasterID))
                {
                    return;
                }
            }

            this.ProductionDetails.Add(detail);
            this.SelectedProductionDetail = detail;
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to view a product spec report.
        /// </summary>
        public ICommand ViewProductSpecCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a box label print.
        /// </summary>
        public ICommand PieceLabelCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a box label print.
        /// </summary>
        public ICommand BoxLabelCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a products view change.
        /// </summary>
        public ICommand SwapViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unloaded event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void DeleteTransaction(int stockId, string barcode = "")
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to delete out of prod label:{stockId}");
            var error = this.DataManager.DeleteOutOfProductionStock(stockId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
            if (!string.IsNullOrEmpty(error))
            {
                this.Log.LogError(this.GetType(), $"Label deletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
            else
            {
                this.Log.LogInfo(this.GetType(), "Label deleted");
                SystemMessage.Write(MessageType.Priority, Message.StockRemoved);
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void UndeleteTransaction(int stockId)
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to undelete into prod label:{stockId}");
            var error = this.DataManager.UndeleteStock(stockId, NouvemGlobal.TransactionTypeProductionReceiptId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
            if (!string.IsNullOrEmpty(error))
            {
                this.Log.LogError(this.GetType(), $"Label undeletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
            else
            {
                this.Log.LogInfo(this.GetType(), "Label undeleted");
                SystemMessage.Write(MessageType.Priority, Message.StockUndeleted);
            }
        }

        /// <summary>
        /// Show the batch report.
        /// </summary>
        protected override void ShowReportCommandExecute()
        {
            if (!this.IsFormLoaded || this.SelectedOrder == null)
            {
                return;
            }

            var reportData = new Sale
            {
                Reference = this.SelectedOrder.Order.PROrderID.ToString()
            };

            Messenger.Default.Send(reportData, Token.DisplayTouchscreenBatchReport);
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int? GetReferenceId()
        {
            return this.SelectedOrder != null && this.SelectedOrder.Order != null ? this.SelectedOrder.Order.BatchNumberID : 0; ;
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int GetWorkflowDocketId()
        {
            return this.SelectedOrder != null && this.SelectedOrder.Order != null ? this.SelectedOrder.Order.PROrderID : 0;
        }

        /// <summary>
        /// Gets the current attribute product id.
        /// </summary>
        /// <returns>THe corresponding product id.</returns>
        protected override int GetWorkflowProductId()
        {
            return this.SelectedProductionDetail != null ? this.SelectedProductionDetail.INMasterID : 0;
        }

        /// <summary>
        /// Sets the process flag (visibility/edit)
        /// </summary>
        /// <param name="data">The attribute allocation data.</param>
        protected override Tuple<bool, bool> SetProcessData(AttributeAllocationData data)
        {
            var visible = false;
            var editable = false;
            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfButchery));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfButchery));
            }

            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfInjection));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfInjection));
            }

            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfProductionStandard)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfProductionStandard));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfProductionStandard));
            }

            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfProductionWithPieceLabels)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfProductionWithPieceLabels));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.OutOfProductionWithPieceLabels));
            }

            return Tuple.Create(visible, editable);
        }

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        ///// <summary>
        ///// Handle the recording of the weight.
        ///// </summary>
        //private void RecordWeight()
        //{
        //    #region validation

        //    if (this.SelectedOrder == null)
        //    {
        //        return;
        //    }

        //    if (this.ProductsViewModel == ViewModelLocator.ProductionProductsStatic)
        //    {
        //        return;
        //    }

        //    if (this.SelectedOrder == null)
        //    {
        //        SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
        //        return;
        //    }

        //    if (this.SelectedProductionDetail == null)
        //    {
        //        SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
        //        return;
        //    }

        //    #endregion

        //    if (!this.Locator.ProductionProductsCards.ProductProcessingComplete)
        //    {
        //        System.Threading.Thread.Sleep(500);
        //        this.RecordWeight();
        //    }

        //    this.IndicatorManager.OpenIndicatorPort();

        //    // fetch the traceability values
        //    Messenger.Default.Send(Token.Message, Token.GetTouchscreenTraceabilityData);
        //}


        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        private void RecordWeight()
        {
            #region validation

            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            if (ApplicationSettings.SortMode)
            {
                SystemMessage.Write(MessageType.Issue, Message.SortProductsOnIssue);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.SortProductsOnIssue, flashMessage: true);
                }));

                return;
            }

            if (ApplicationSettings.SortGroupMode)
            {
                SystemMessage.Write(MessageType.Issue, Message.SortGroupsOnIssue);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.SortGroupsOnIssue, flashMessage: true);
                }));

                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                return;
            }

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                });
              
                return;
            }

            this.CheckForUnsavedAttributes();

            #endregion

            if (!this.Locator.ProductionProductsCards.ProductProcessingComplete)
            {
                System.Threading.Thread.Sleep(500);
                this.RecordWeight();
            }

            this.IndicatorManager.OpenIndicatorPort();

            var weightRequired = true;
            if (this.SelectedProductionDetail != null)
            {
                if (this.SelectedProductionDetail.PriceMethod == PriceMethod.Quantity)
                {
                    weightRequired = false;
                }
            }

            this.IndicatorManager.SetQtyBasedProductValues(!weightRequired);

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.cleanRead = true;
                    this.manualWeight = false;
                }
                else 
                {
                    if (this.SelectedProductionDetail != null &&
                        this.SelectedProductionDetail.StockMode != StockMode.BatchQty &&
                        this.SelectedProductionDetail.StockMode != StockMode.ProductQty)
                    {
                        if (saveWeightResult.Equals(Message.ScalesInMotion))
                        {
                            // motion on scales, so move processing to the tick event and wait for scales stability.
                            this.scalesMotionTimer.Start();
                            return;
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, saveWeightResult);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true);
                            });

                            return;
                        }
                    }
                    else
                    {
                        this.cleanRead = true;
                        this.manualWeight = false;
                    }
                }
            }

            if (this.SelectedProductionDetail != null && this.SelectedProductionDetail.InventoryItem != null)
            {
                var minWgt = this.SelectedProductionDetail.InventoryItem.MinWeight.ToDecimal();
                var maxWgt = this.SelectedProductionDetail.InventoryItem.MaxWeight.ToDecimal();
                if ((minWgt != 0 && this.Locator.Indicator.Weight < minWgt)
                    || (maxWgt != 0 && this.Locator.Indicator.Weight > maxWgt))
                {
                    var message = string.Format(Message.OutOfBoundsWeight, minWgt, maxWgt);
                    SystemMessage.Write(MessageType.Issue, message);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true);
                    });
                    
                    return;
                }
            }

            this.ParseTraceabilityData();
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        private void RecordPieceWeight()
        {
            try
            {
                #region validation

                if (ApplicationSettings.SortMode)
                {
                    SystemMessage.Write(MessageType.Issue, Message.SortProductsOnIssue);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.SortProductsOnIssue, flashMessage: true);
                    }));

                    return;
                }

                if (ApplicationSettings.SortGroupMode)
                {
                    SystemMessage.Write(MessageType.Issue, Message.SortGroupsOnIssue);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.SortGroupsOnIssue, flashMessage: true);
                    }));

                    return;
                }

                if (this.SelectedOrder == null)
                {
                    return;
                }

                if (this.ProductsViewModel == ViewModelLocator.ProductionProductsStatic)
                {
                    return;
                }

                if (this.SelectedOrder == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                    return;
                }

                if (this.SelectedProductionDetail == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                    return;
                }

                #endregion

                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true);
                    });

                    return;
                }

                //this.indicatorWeight = 0;
                this.ParseTraceabilityData();
            }
            finally
            {
                this.creatingPieceLabel = false;
            }
        }

        /// <summary>
        /// Parse the incoming traceability data.
        /// </summary>
        private void ParseTraceabilityData()
        {
            var stockDetail = this.CreateTransaction();
            this.CreateAttribute(stockDetail);
            this.SelectedProductionDetail.StockDetailToProcess = stockDetail;

            if (this.creatingPieceLabel)
            {
                stockDetail.Consumed = DateTime.Now;
                stockDetail.Deleted = DateTime.Now;
            }

            int? customerId = this.SelectedCustomer == null ? (int?)null : this.SelectedCustomer.BPMasterID;
            if (customerId.HasValue)
            {
                stockDetail.BPMasterID = customerId;
            }

            this.Log.LogDebug(this.GetType(), string.Format("Adding weight:{0}", this.indicatorWeight));
            this.CompleteTransaction();
        }

        /// <summary>
        /// Handle the load event.
        /// </summary>
        private void OnLoadedCommandExecute()
        {
            Messenger.Default.Send(false, Token.DisableLegacyAttributesView);
            this.IsBusy = false;
            ViewModelLocator.ClearIntoProduction();
            ApplicationSettings.UsingWorkflowForStandardAttributes = true;
            this.Locator.FactoryScreen.ModuleName = Strings.OutOfProduction;
            this.Locator.Touchscreen.OutOfProductionExtensionWidth = ApplicationSettings.OutOfProductionProductsHomeMenuWidth;

            this.IndicatorManager.SetMinimumWeight(ApplicationSettings.MinWeightAllowedOutOfProduction);

            this.ScannerStockMode = ScannerMode.Scan;
            this.OpenScanner();
            this.IsFormLoaded = true;
            this.Locator.ProcessSelection.SetDefaultProcesses(ApplicationSettings.DefaultOutOfProductionModes, ApplicationSettings.OutOfProductionMode.ToString());
        }

        /// <summary>
        /// Handle the unload event.
        /// </summary>
        private void OnUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
        }

        /// <summary>
        /// handle the command to print a piece label.
        /// </summary>
        private void PieceLabelCommandExecute()
        {
            Keypad.Show(KeypadTarget.ProductionPieces);
        }

        /// <summary>
        /// Handle the box label command.
        /// </summary>
        private void BoxLabelCommandExecute()
        {
            #region validation

            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery ||
                ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
            {
                return;
            }

            if (this.ProductsViewModel == ViewModelLocator.ProductionProductsStatic)
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductSelected);
                return;
            }

            #endregion

            var inmasterid = this.SelectedProductionDetail.InventoryItem.Master.INMasterID;
            if (inmasterid != this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID)
            {
                this.Log.LogError(this.GetType(), string.Format("BoxLabelCommandExecute() Switching from {0} to {1}", inmasterid, this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID));
                inmasterid = this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID;
            }

            int? customerId = this.SelectedCustomer == null ? (int?)null : this.SelectedCustomer.BPMasterID;

            // add the transaction weight to our stock transaction, and other relevant data.
            var trans = new StockTransaction
            {
                BatchNumberID = this.selectedOrder.Order.BatchNumberID,
                MasterTableID = this.selectedOrder.Order.PROrderID,
                TransactionDate = DateTime.Now,
                INMasterID = inmasterid,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionReceiptId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = true,
                BPMasterID = customerId,
                ProcessID = this.Locator.ProcessSelection.SelectedProcess != null ? this.Locator.ProcessSelection.SelectedProcess.ProcessID : (int?)null
            };

            if (this.selectedWarehouse == null || this.selectedWarehouse.Name == Strings.TerminalLocation)
            {
                trans.WarehouseID = NouvemGlobal.OutOfProductionId;
            }
            else
            {
                trans.WarehouseID = this.selectedWarehouse.WarehouseID;
            }

            try
            {
                var qtyPerBox = 1;
                var boxTare = 0M;
                if (this.SelectedProductionDetail != null
                    && this.SelectedProductionDetail.InventoryItem != null)
                {
                    if (this.SelectedProductionDetail.InventoryItem.Master.BoxTareContainer != null)
                    {
                        boxTare = this.SelectedProductionDetail.InventoryItem.Master.BoxTareContainer.Tare.ToDecimal();
                    }

                    qtyPerBox = this.SelectedProductionDetail.InventoryItem.Master.QtyPerBox.ToInt() == 0
                        ? 1
                        : this.SelectedProductionDetail.InventoryItem.Master.QtyPerBox.ToInt();
                }

                var result = this.ProductionManager.AddBoxTransaction(trans, boxTare, qtyPerBox, ApplicationSettings.CanOnlyBoxOffProductQtyAtProduction);
                var transactionId = result.Item1;

                if (transactionId > 0)
                {
                    this.Print(transactionId, LabelType.Box);
                    SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);
                }
                else
                {
                    var items = result.Item2;
                    var msg = string.Empty;
                    if (transactionId == -1)
                    {
                        msg =
                            $"This product is set to have a qty of {qtyPerBox} per box, but you are attempting to box only {items.Count} items. Please fill the box first";
                    }
                    else
                    {
                        var deleteAmt = items.Count - qtyPerBox;
                        var labels = string.Join(",", items.Select(x => x));
                        msg = $"This product is set to have a qty of {qtyPerBox} per box, but you are attempting to box {items.Count} items. Please delete {deleteAmt} of the following items - {labels}";
                    }

                    NouvemMessageBox.Show(msg, touchScreen: true);

                    //SystemMessage.Write(MessageType.Issue, Message.BoxLabelNotPrinted);
                }
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
            }
        }

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private void SwitchViewCommandExecute(string view)
        {
            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view.Equals(Constant.Order))
                    {
                        if (ApplicationSettings.PrintQALabelAfterFirstLabelInBatch)
                        {
                            if (this.SelectedOrder != null && this.SelectedOrder.Order != null && this.SelectedOrder.Order.QALabelPrinted != null)
                            {
                                var localTransId =
                                    this.DataManager.GetLastTransactionInBatch(this.SelectedOrder.Order.PROrderID);
                                if (localTransId > 0)
                                {
                                    NouvemMessageBox.Show(Message.ReprintLastQALabel, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                                    {
                                        this.PrintManager.ReprintLabel(localTransId);
                                    }
                                }
                            }
                        }

                        //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenProductionOrders;
                        this.Locator.TouchscreenProductionOrders.SetModule(ViewType.OutOfProduction);
                        this.Locator.TouchscreenProductionOrders.GetAllOrders();
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                        //Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplaySupplierSales);
                        return;
                    }

                    if (view.Equals(Constant.BatchSetUp))
                    {
                        this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ProductionBatchSetUp;
                        return;
                    }

                    if (view.Equals(Constant.Customer))
                    {
                        this.Locator.TouchscreenSuppliers.MasterModule = ViewType.OutOfProduction;
                        if (!this.PartnersScreenCreated)
                        {
                            this.PartnersScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                            return;
                        }
 
                        Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                        this.Locator.TouchscreenSuppliers.OnEntry();
                        return;
                    }
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        #endregion

        #region helper

        /// <summary>
        /// Handles a selected customer, setting the associated process if applicable.
        /// </summary>
        private void HandleSelectedCustomer()
        {
            if (this.SelectedCustomer == null || this.SelectedCustomer.Name == Strings.SelectCustomerForLabel ||
                !this.SelectedCustomer.ProcessID.HasValue)
            {
                this.Locator.ProcessSelection.SetProcess(this.defaultProcess);
            }
            else
            {
                this.Locator.ProcessSelection.SetProcess(this.SelectedCustomer.ProcessID.ToInt());
            }
        }

        /// <summary>
        /// Sets the mode.
        /// </summary>
        /// <param name="process">The incoming process selection.</param>
        private void SetOutOfProductionMode(Nouvem.Model.DataLayer.Process process)
        {
            try
            {
                this.IndicatorManager.IgnoreValidation(false);
                ApplicationSettings.OutOfProductionMode =
                    (OutOfProductionMode)Enum.Parse(typeof(OutOfProductionMode), process.Name.Replace(" ", ""));

                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery
                    || ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                {
                    this.IndicatorManager.IgnoreValidation(true);

                    if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
                    {
                        if (ApplicationSettings.DefaultQty > 0)
                        {
                            this.Locator.Touchscreen.Quantity = ApplicationSettings.DefaultQty;
                        }
                    }
                }

                this.SetPanel(process);
                this.GetAttributeAllocationApplicationData(process.ProcessID);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("SetOutOfProductionMode: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Sets the panel depending on the process.
        /// </summary>
        /// <param name="process">The process.</param>
        private void SetPanel(Process process)
        {
            if (process.Name.CompareIgnoringCase(ProcessType.OutOfProductionStandard))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenOutOfProductionPanelStatic;
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.OutOfButchery))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenOutOfProductionPanelStatic;
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.OutOfInjection))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenOutOfProductionPanelStatic;
                return;
            }

            if (process.Name.CompareIgnoringCase(ProcessType.OutOfProductionWithPieceLabels))
            {
                this.Locator.Touchscreen.PanelViewModel =
                    ViewModelLocator.TouchscreenOutOfProductionWithPiecesPanelStatic;
                return;
            }
        }

        /// <summary>
        /// Records (amt) of piece labels.
        /// </summary>
        /// <param name="amt">The amt to print.</param>
        private void RecordPieceLabels(string amt)
        {
            this.pieceLabelsToPrint = amt.ToInt();
            if (this.pieceLabelsToPrint == 0)
            {
                return;
            }

            this.creatingPieceLabel = true;
            this.RecordPieceWeight();
        }

        /// <summary>
        /// Refresh the order data.
        /// </summary>
        private void RefreshOrderData()
        {
            Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
        }

        /// <summary>
        /// Completes a transaction, check the orders live status.
        /// </summary>
        protected void CompleteTransaction()
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            #endregion

            var localTrans = this.selectedProductionDetail.StockDetailToProcess;
            int prOrderId = 0;

            if (localTrans != null)
            {
                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataTransactions,
                        this.SelectedOrder.Order.PROrderID,
                        localTrans.TransactionWeight.ToDecimal(), localTrans.TransactionQty.ToDecimal(),
                        localTrans.INMasterID,
                        localTrans.WarehouseID,
                        localTrans.ProcessID.ToInt(),
                        Constant.Batch,
                        this.ReferenceValue))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                prOrderId = localTrans.MasterTableID.ToInt();

                if (localTrans.INMasterID !=
                    this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID)
                {
                    this.Log.LogDebug(this.GetType(),
                        string.Format("Switching from:{0} to {1}", localTrans.INMasterID,
                            this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID));
                    localTrans.INMasterID =
                        this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID;
                }
            }
            else
            {
                this.Log.LogDebug(this.GetType(), "Processed trans not found");
            }

            // will usually be 1 transaction, but user may automate the process when there are x amount of identical products to process.
            var stockTransactions = 1;
            if (this.StockLabelsToPrint > 1)
            {
                stockTransactions = this.StockLabelsToPrint;
            }

            try
            {
                for (int i = 0; i < stockTransactions; i++)
                {
                    var transactionId = this.DataManager.AddOutOfProductionTransaction(localTrans);
                    if (transactionId > 0)
                    {
                        try
                        {
                            if (!this.ValidateData(
                                this.traceabilityDataPostTransactions,
                                transactionId, 0, 0, 0, 0, 0, Constant.Batch))
                            {
                                return;
                            };
                        }
                        catch (Exception e)
                        {
                            this.Log.LogError(this.GetType(), e.Message);
                        }

                        if (this.NonStandardResponses.Any() || this.NonStandardBatchResponses.Any())
                        {
                            foreach (var response in this.NonStandardResponses)
                            {
                                this.LogAlerts(response, prOrderID: this.SelectedOrder.Order.PROrderID, processId: this.Locator.ProcessSelection.SelectedProcess.ProcessID);
                            }

                            foreach (var response in this.NonStandardBatchResponses)
                            {
                                this.LogAlerts(response, prOrderID: this.SelectedOrder.Order.PROrderID, processId: this.Locator.ProcessSelection.SelectedProcess.ProcessID);
                            }
                        }

                        this.NonStandardResponses.Clear();

                        if (this.creatingPieceAndBoxLabels)
                        {
                            var localQty = this.ProductQuantity.ToInt();
                            if (localQty > 0)
                            {
                                if (!ApplicationSettings.DisablePrintingAtOutOfProduction)
                                {
                                    this.Print(transactionId, LabelType.Item, localQty);
                                }
                            }

                            if (!ApplicationSettings.DisablePrintingAtOutOfProduction)
                            {
                                this.Print(transactionId, LabelType.Box);
                            }
                        }
                        else if (this.creatingPieceLabel)
                        {
                            this.DataManager.UpdatePieceTransaction(transactionId);
                            if (!ApplicationSettings.DisablePrintingAtOutOfProduction)
                            {
                                //TODO: Should always be piece labels but Ballon are using item labels here. Need to fix up on Ballon.
                                var labelType = ApplicationSettings.UsingItemLabelsForPieceLabelsAtOutOfProduction ? LabelType.Item : LabelType.Piece;
                                this.Print(transactionId, labelType, this.pieceLabelsToPrint);
                            }

                            return;
                        }
                        else
                        {
                            if (ApplicationSettings.Customer == Customer.Ballon)
                            {
                                if (!ApplicationSettings.DisablePrintingAtOutOfProduction)
                                {
                                    this.Print(transactionId, LabelType.Box);
                                }
                            }
                            else
                            {
                                if (ApplicationSettings.OutOfProductionMode != OutOfProductionMode.OutOfButchery
                                    && ApplicationSettings.OutOfProductionMode != OutOfProductionMode.OutOfInjection)
                                {
                                    if (!ApplicationSettings.DisablePrintingAtOutOfProduction)
                                    {
                                        this.Print(transactionId);
                                    }
                                }
                            }
                        }

                        // Refresh the order, to get updated data.
                        this.RefreshOrderData();

                        var message = Message.StockCreated;

                        if (this.SelectedProductionDetail.StockDetailToProcess != null
                            && this.SelectedProductionDetail.InventoryItem != null)
                        {
                            var processName = Strings.StockLwr;
                            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
                            {
                                processName = Strings.OutOfButchery;
                            }
                            else if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                            {
                                processName = Strings.OutOfInjection;
                            }

                            message = string.Format(Message.StockItemCreated,
                                this.SelectedProductionDetail.StockDetailToProcess.TransactionWeight,
                                this.SelectedProductionDetail.InventoryItem.Master.Name, processName);
                        }

                        SystemMessage.Write(MessageType.Priority, message);
                        if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery
                            || ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                            });
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.StockNotCreated);
                    }
                }

                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery
                                    || ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataManager.ConsumeProductionBatch(prOrderId, NouvemGlobal.TransactionTypeProductionIssueId);
                        this.DataManager.UpdateBatchWeight(prOrderId);
                    }));
                }

                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
                {
                    ApplicationSettings.IntoProductionMode = IntoProductionMode.Injection;
                    this.Locator.Touchscreen.SelectedModuleCommandExecute(Constant.IntoProduction);
                    this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
                    this.Locator.TouchscreenProductionOrders.GetAllOrders();
                    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                    this.Locator.TouchscreenProductionOrders.CreateOrder();

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Locator.ProcessSelection.SetProcess(ProcessType.Injection);
                    }));
                }
            }
            finally
            {
                this.StockLabelsToPrint = 1;
                this.pieceLabelsToPrint = 1;
                this.resettingCustomer = false;
                this.CheckAttributeTransactionReset(this.SelectedProductionDetail);
                this.Locator.Indicator.SetManualWeight(0);
                if (localTrans != null)
                {
                    Messenger.Default.Send(Tuple.Create(localTrans.TransactionWeight.ToDecimal(), localTrans.TransactionQty.ToDecimal()), Token.UpdateWeights);
                }

                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery
                    || ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.DataManager.UpdateBatchWeight(this.SelectedOrder.Order.PROrderID);
                        }));
                    }));
                }
            }
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            #region validation

            if (string.IsNullOrEmpty(data))
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);
                return;
            }

            if (ViewModelLocator.IsProductionOrdersLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.ProductionOrderSearch);
                return;
            }

            if (ViewModelLocator.IsTransactionManagerLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                return;
            }

            #endregion

            this.ReferenceValue = data;
            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery
                || ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
            {
                var intoButcheryTransaction = this.DataManager.GetStockTransactionIncProduct(data.RemoveNonDecimals().ToInt());

                if (intoButcheryTransaction != null)
                {
                    #region validation

                    if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
                    {
                        if (intoButcheryTransaction.Comments.CompareIgnoringCase(Constant.Injection))
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                SystemMessage.Write(MessageType.Issue, Message.InjectionBatchScanAtButcheryStop);
                                NouvemMessageBox.Show(Message.InjectionBatchScanAtButcheryStop, touchScreen: true);
                            }));

                            return;
                        }
                    }

                    if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                    {
                        if (intoButcheryTransaction.Comments.CompareIgnoringCase(Constant.Butchery))
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                SystemMessage.Write(MessageType.Issue, Message.ButcheryBatchScanAtInjectionStop);
                                NouvemMessageBox.Show(Message.ButcheryBatchScanAtInjectionStop, touchScreen: true);
                            }));

                            return;
                        }
                    }

                    #endregion

                    this.BatchIdBase = intoButcheryTransaction.BatchID_Base;
                    this.Locator.TouchscreenProductionOrders.SetModule(ViewType.OutOfProduction);
                    this.Locator.TouchscreenProductionOrders.SetOrder(intoButcheryTransaction.MasterTableID.ToInt());
                    this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                    if (this.SelectedOrder != null && intoButcheryTransaction.INMasterID > 0)
                    {
                        this.Locator.ProductionProductsCards.SetProduct(intoButcheryTransaction.INMasterID, intoButcheryTransaction.StoredLabelID.ToInt());
                    }
                }
            }
        }

        /// <summary>
        /// Saves the batch attributes.
        /// </summary>
        protected override void SaveBatchAttributes(int? parentId = null, bool ignoreChecks = false)
        {
            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (!ignoreChecks)
            {
                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);
                    return;
                }
            }

            #endregion

            this.UnsavedAttributes = false;
            var attributeId = this.SaveBatchData(this.SelectedOrder.Order.BatchNumberID);
            if (attributeId > 0)
            {
                base.SaveBatchAttributes(attributeId);
                SystemMessage.Write(MessageType.Priority, Message.BatchAttributesSaved);

                if (!ignoreChecks)
                {
                    NouvemMessageBox.Show(Message.BatchAttributesSaved, touchScreen: true, flashMessage: true);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchAttributesNotSaved);
            }
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleStockLabels(string data)
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);
                return;
            }

            #endregion

            this.StockLabelsToPrint = data.ToInt();
        }

        /// <summary>
        /// Completes a transaction, check the orders live status.
        /// </summary>
        protected void CompletePieceTransaction(List<TraceabilityResult> data)
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            #endregion

            this.Log.LogDebug(this.GetType(), string.Format("CompleteTransaction(): GoodsReceipt Count:{0}", this.selectedProductionDetail.GoodsReceiptDatas.Count));

            var localTrans = this.selectedProductionDetail.StockDetailToProcess;

            if (localTrans != null)
            {
                if (localTrans.INMasterID !=
                    this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID)
                {
                    this.Log.LogDebug(this.GetType(),
                        string.Format("Switching from:{0} to {1}", localTrans.INMasterID,
                            this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID));
                    localTrans.INMasterID =
                        this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID;
                }
            }
            else
            {
                this.Log.LogDebug(this.GetType(), "Processed trans not found");
            }

            // will usually be 1 transaction, but user may automate the process when there are x amount of identical products to process.
            var stockTransactions = 1;
            var localPieces = this.ProductQuantity.ToInt();
            if (localPieces > 1)
            {
                stockTransactions = localPieces;
            }

            try
            {
                var transactionId = this.DataManager.AddOutOfProductionTransaction(localTrans);

                if (transactionId > 0)
                {
                    this.Print(transactionId, LabelType.Box);
                    this.Print(transactionId, LabelType.Item, stockTransactions);
                    var message = Message.PieceLabelPrinted;
                    SystemMessage.Write(MessageType.Priority, message);

                    //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    //{
                    this.RecordWeight();
                    //}));
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PieceLabelNotPrinted);
                }
            }
            finally
            {
                this.StockLabelsToPrint = 1;
                //this.pieces = 1;
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <param name="labelType">The label type.</param>
        private void Print(int stockTransactionId, LabelType labelType = LabelType.Item, int labelsToPrint = 1)
        {
            int? customerId = this.SelectedCustomer == null ? (int?)null : this.SelectedCustomer.BPMasterID;
            int? customerGroupId = this.SelectedCustomer == null ? (int?)null : this.SelectedCustomer.BPGroupID;
            int? productId = null;
            int? productGroupId = null;

            if (this.SelectedProductionDetail.InventoryItem != null &&
                this.SelectedProductionDetail.InventoryItem.Master != null)
            {
                productId = this.SelectedProductionDetail.InventoryItem.Master.INMasterID;
                productGroupId = this.SelectedProductionDetail.InventoryItem.Master.INGroupID;
            }

            try
            {
                var warehouseId =
                    this.selectedWarehouse == null || this.selectedWarehouse.Name == Strings.TerminalLocation
                        ? NouvemGlobal.OutOfProductionId
                        : this.selectedWarehouse.WarehouseID;

                var labels = this.PrintManager.ProcessPrinting(customerId, customerGroupId, productId, productGroupId, 
                    ViewType.OutOfProduction, labelType, stockTransactionId, labelsToPrint, warehouseId:warehouseId);
                if (labels.Item1.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(stockTransactionId, string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                    if (ApplicationSettings.PrintQALabelAfterFirstLabelInBatch &&
                        !this.SelectedOrder.Order.QALabelPrinted.HasValue)
                    {
                        NouvemMessageBox.Show(Message.ReprintQALabel, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.PrintManager.ReprintLabel(stockTransactionId);
                        }

                        this.SelectedOrder.Order.QALabelPrinted = true;
                        this.DataManager.UpdateProductionOrder(this.SelectedOrder.Order);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(ex.Message, touchScreen: true);
                }));

                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        private StockDetail CreateTransaction()
        {
            this.Log.LogDebug(this.GetType(), "ProcessTransactions(): Processing transactions");

            var inmasterid = this.SelectedProductionDetail.InventoryItem.Master.INMasterID;
            if (inmasterid != this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID)
            {
                inmasterid = this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID;
                this.Log.LogError(this.GetType(), string.Format("Product Id Mismatch: Switching from -{0} to {1}", this.SelectedProductionDetail.InventoryItem.Master.INMasterID, inmasterid));
            }

            var stockTransaction = new StockDetail
            {
                TransactionWeight = this.indicatorWeight,
                Tare = this.tare,
                BatchNumberID = this.SelectedOrder.Order.BatchNumberID,
                MasterTableID = this.SelectedOrder.Order.PROrderID,
                Pieces = this.pieces < 1 ? 1 : this.pieces,
                ManualWeight = this.manualWeight,
                TransactionQty = this.productQuantity < 1 ? 1 : this.productQuantity,
                INMasterID = inmasterid,
                TransactionDate = DateTime.Now,
                BatchNumberBaseID = this.BatchIdBase,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionReceiptId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = this.creatingPieceAndBoxLabels,
                ProcessID = this.Locator.ProcessSelection.SelectedProcess != null ? this.Locator.ProcessSelection.SelectedProcess.ProcessID : (int?)null,
                InLocation = true
            };

            if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery)
            {
                stockTransaction.WarehouseID = NouvemGlobal.WarehouseOutOfButcheryId;
            }
            else if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
            {
                stockTransaction.WarehouseID = NouvemGlobal.WarehouseOutOfInjectionId;
            }
            else if (this.selectedWarehouse == null || this.selectedWarehouse.Name == Strings.TerminalLocation)
            {
                stockTransaction.WarehouseID = NouvemGlobal.OutOfProductionId;
            }
            else
            {
                stockTransaction.WarehouseID = this.selectedWarehouse.WarehouseID;
            }

            if (!ApplicationSettings.PricingItemByThePiece)
            {
                stockTransaction.TransactionQty = this.pieces < 1 ? 1 : this.pieces;
                stockTransaction.Pieces = this.productQuantity < 1 ? 1 : (int)this.productQuantity;
            }

            return stockTransaction;
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        /// <param name="transactions">The transaction trceability data.</param>
        private StockTransactionData ProcessPieceTransactions(IList<TraceabilityResult> transactions)
        {
            this.Log.LogDebug(this.GetType(), "ProcessTransactions(): Processing transactions");

            var inmasterid = this.SelectedProductionDetail.InventoryItem.Master.INMasterID;
            if (inmasterid != this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID)
            {
                inmasterid = this.Locator.ProductionProductsCards.SelectedProduct.Master.INMasterID;
                this.Log.LogError(this.GetType(), string.Format("Product Id Mismatch: Switching from -{0} to {1}", this.SelectedProductionDetail.InventoryItem.Master.INMasterID, inmasterid));
            }

            var stockTransaction = new StockTransaction();
            var transTraceabilities = new List<TransactionTraceability>();

            // add the transaction weight to our stock transaction, and other relevant data.
            stockTransaction.Pieces = this.pieces < 1 ? 1 : this.pieces;
            stockTransaction.ManualWeight = false;
            stockTransaction.TransactionWeight = 0;
            stockTransaction.TransactionQTY = 1;
            stockTransaction.INMasterID = inmasterid;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.Consumed = DateTime.Now;
            stockTransaction.InLocation = false;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionReceiptId;
            stockTransaction.WarehouseID = NouvemGlobal.OutOfProductionId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;

            foreach (var transaction in transactions)
            {
                // add to our transaction traceability result
                var traceability = this.ParseToTransactionTraceability(transaction);
                transTraceabilities.Add(traceability);
            }

            var transactionData = new StockTransactionData
            {
                Transaction = stockTransaction,
                TransactionTraceabilities = transTraceabilities
            };

            return transactionData;
        }

        /// <summary>
        /// Gets the products view to display.
        /// </summary>
        private void GetProductsView()
        {
            this.ProductsViewModel = ViewModelLocator.ProductionProductsCardsStatic;
        }

        /// <summary>
        /// Gets all the application products.
        /// </summary>
        private void GetAllProducts()
        {
            this.CheckForNewEntities(EntityType.INMaster);
            this.allProducts = (from item in NouvemGlobal.ProductionItems
                                select new SaleDetail { InventoryItem = item, INMasterID = item.Master.INMasterID }).ToList();
        }

        /// <summary>
        /// Handles the selected order.
        /// </summary>
        private void HandleSelectedOrder()
        {
            if (this.ProductsViewModel == ViewModelLocator.ProductionProductsGridStatic)
            {
                ViewModelLocator.ProductionProductsCardsStatic.UpdateGridData(this.SelectedOrder?.Order?.PROrderID);
            }

            this.CheckForNewEntities(EntityType.INMaster);
            this.Spec = this.SelectedOrder.Spec;

            if (this.SelectedOrder.Outputs == null)
            {
                this.ProductionDetails = new ObservableCollection<SaleDetail>(this.allProducts);
                return;
            }

            IList<InventoryItem> allowableProducts = null;

            if (ApplicationSettings.AllowAllProductsOutOfProduction)
            {
                allowableProducts = NouvemGlobal.ProductionItems;
            }
            else
            {
                allowableProducts = this.ProductionManager.GetAllProductsAllowedOutOfProduction(this.SelectedOrder.Order.PROrderID);
                if (!allowableProducts.Any())
                {
                    allowableProducts = NouvemGlobal.ProductionItems;
                }
            }

            this.Locator.ProductionProductsCards.SetAllowableProducts(allowableProducts);
            this.RefreshOrderData();
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimers()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(1.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                if (ApplicationSettings.TouchScreenMode)
                {
                    Keypad.Show(KeypadTarget.ManualSerial);
                }
            };

            this.scalesMotionTimer.Interval = TimeSpan.FromMilliseconds(5);
            this.scalesMotionTimer.Tick += (sender, args) =>
            {
                if (this.scalesStabilityWaitTicks == ApplicationSettings.WaitForScalesToStable)
                {
                    this.scalesStabilityWaitTicks = 0;
                    this.scalesMotionTimer.Stop();

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.ScalesInMotion, touchScreen: true, isLeft: true);
                    }));

                    return;
                }

                if (!this.Locator.Indicator.ScalesInMotion)
                {
                    this.scalesMotionTimer.Stop();
                    this.scalesStabilityWaitTicks = 0;

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.RecordWeight();
                    }));

                    return;
                }
                else
                {
                    this.scalesStabilityWaitTicks++;
                }
            };
        }

        /// <summary>
        /// Sets the typical wgt.
        /// </summary>
        private void SetTypicalWeight()
        {
            if (!this.TypicalWeight.IsNullOrZero())
            {
                var localQty = this.ProductQuantity == 0 ? 1 : this.ProductQuantity;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Indicator.SetManualWeight(this.TypicalWeight.ToDecimal() * localQty);
                }));

                return;
            }

            this.Locator.Indicator.SetManualWeight(0);
        }

        /// <summary>
        /// Handle the keypad quantity selection.
        /// </summary>
        /// <param name="qty">The quantity amount.</param>
        private void HandleQtySelection(string qty)
        {
            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderItemSelected);
                NouvemMessageBox.Show(Message.NoOrderItemSelected, touchScreen: true, flashMessage: true);
                return;
            }

            this.ProductQuantity = qty.ToDecimal();
        }

        /// <summary>
        /// Validates, then broadcasts a successfull saved weight.
        /// </summary>
        private void RecordBoxWeight()
        {
            if (this.Locator.Indicator.ManualWeight)
            {
                this.indicatorWeight = this.Locator.Indicator.Weight;
                this.manualWeight = true;
                this.RecordWeight();
                return;
            }

            this.manualWeight = false;
            this.RecordWeight();
        }

        #endregion

        #endregion
    }
}




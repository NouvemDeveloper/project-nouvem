﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Attribute;
using Nouvem.ViewModel.Interface;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Production
{
    public class BatchEditorViewModel : AttributesViewModel, IRibbonBarCommand
    {

        #region field

        /// <summary>
        /// The selected batch.
        /// </summary>
        private Sale selectedBatch = new Sale();

        private string batchReference;
        private string scheduledDate;

        private IList<BatchData> allBatches = new List<BatchData>();
        private ObservableCollection<BatchData> batches;

        /// <summary>
        /// The batches.
        /// </summary>
        private ObservableCollection<BatchData> batchDataIn = new ObservableCollection<BatchData>();

        /// <summary>
        /// The batches.
        /// </summary>
        private ObservableCollection<BatchData> batchDataOut = new ObservableCollection<BatchData>();

        /// <summary>
        /// The batches.
        /// </summary>
        private ObservableCollection<BatchData> batchDataAttributes = new ObservableCollection<BatchData>();

        private IList<BatchData> filteredBatchDataOut = new List<BatchData>();
        private IList<BatchData> filteredBatchDataIn = new List<BatchData>();
        private IList<BatchData> filteredBatchDataAttributes = new List<BatchData>();
        private BatchData selectedBatchDataIn;
        private BatchData selectedBatchDataOut;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchEditorViewModel"/> class.
        /// </summary>
        public BatchEditorViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<Sale>(this, Token.ProductionSelected, s =>
            {
                this.SelectedBatch = s;
            });

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlCommandExecute(x);
                });

            #endregion

            #region command handler

            this.PrintCommand = new RelayCommand(this.PrintCommandExecute);
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);
            this.OnCellValueChangingCommmand = new RelayCommand<string>(this.UpdateCommandExecute);

            #endregion

            this.IsFormLoaded = true;
            this.GetBatches();
            this.GetAttributeLookUps();
            this.GetProcesses();
        }

        #endregion

        #region public interface

        #region property
        
        /// <summary>
        /// Gets or sets the selected copy from specification.
        /// </summary>
        public Sale SelectedBatch
        {
            get
            {
                return this.selectedBatch;
            }

            set
            {
                this.selectedBatch = value;
                this.RaisePropertyChanged();
                this.HandleSelectedBatch();
            }
        }

        /// <summary>
        /// Gets or sets the selected batch in data.
        /// </summary>
        public BatchData SelectedBatchDataIn
        {
            get
            {
                return this.selectedBatchDataIn;
            }

            set
            {
                this.selectedBatchDataIn = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.SelectedBatchDataOut = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected batch out data.
        /// </summary>
        public BatchData SelectedBatchDataOut
        {
            get
            {
                return this.selectedBatchDataOut;
            }

            set
            {
                this.selectedBatchDataOut = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.SelectedBatchDataIn = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch reference.
        /// </summary>
        public string BatchReference
        {
            get
            {
                return this.batchReference;
            }

            set
            {
                this.batchReference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scheduled date of batch.
        /// </summary>
        public string ScheduledDate
        {
            get
            {
                return this.scheduledDate;
            }

            set
            {
                this.scheduledDate = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets the batch data into prod.
        /// </summary>
        public ObservableCollection<BatchData> BatchDataIn
        {
            get
            {
                return this.batchDataIn;
            }

            set
            {
                this.batchDataIn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch data into prod.
        /// </summary>
        public ObservableCollection<BatchData> BatchDataOut
        {
            get
            {
                return this.batchDataOut;
            }

            set
            {
                this.batchDataOut = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch data into prod.
        /// </summary>
        public ObservableCollection<BatchData> BatchDataAttributes
        {
            get
            {
                return this.batchDataAttributes;
            }

            set
            {
                this.batchDataAttributes = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets the batch data into prod.
        /// </summary>
        public ObservableCollection<BatchData> Batches
        {
            get
            {
                return this.batches;
            }

            set
            {
                this.batches = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        public ICommand PrintCommand { get; set; }
        public ICommand OnClosingCommand { get; set; }
        public ICommand OnCellValueChangingCommmand { get; set; }

        #endregion

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
           
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            var lastEdit = this.DataManager.GetPROrderLastEdit();
            if (lastEdit != null)
            {
                this.SelectedBatch = new Sale {SaleID = lastEdit.PROrderID, Reference = lastEdit.Reference};
            }
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        #region protected


        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedBatch == null || this.SelectedBatch.SaleID == 0)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(100, this.SelectedBatch.SaleID.ToString(), !preview);
            return;
        }


        protected void OnClosingCommandExecute()
        {
            this.Close();
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    
                    break;

                case ControlMode.Find:
                    this.ClearForm();
                    this.SetControlMode(ControlMode.Find);
                    Messenger.Default.Send(ViewType.BatchEdit);
                    this.Locator.BatchEdit.SearchMode = true;
                    break;

                case ControlMode.Update:
                    this.UpdateBatchData();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }


        #endregion

        #region private

        /// <summary>
        /// Updates a batch.
        /// </summary>
        private void UpdateBatchData()
        {
            NouvemMessageBox.Show(Message.EditBatchPrompt, NouvemMessageBoxButtons.OKCancel);
            if (NouvemMessageBox.UserSelection == UserDialogue.Cancel)
            {
                return;
            }
            
            var count = this.filteredBatchDataIn.Count + this.filteredBatchDataOut.Count;
            ProgressBar.SetUp(0,count == 0 ? 1 : count);

            try
            {
                if (this.filteredBatchDataAttributes.Any())
                {
                    this.filteredBatchDataAttributes.First().AttributesOnly = true;
                    this.DataManager.EditBatchData(this.filteredBatchDataAttributes.First());
                }

                foreach (var batchData in this.filteredBatchDataIn)
                {
                    ProgressBar.Run();
                    this.DataManager.EditBatchData(batchData);
                }

                foreach (var batchData in this.filteredBatchDataOut)
                {
                    ProgressBar.Run();
                    this.DataManager.EditBatchData(batchData);
                }
            }
            finally
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                ProgressBar.Reset();
                this.ClearForm();
            }
        }

        /// <summary>
        /// Handles a batch selection.
        /// </summary>
        private void HandleSelectedBatch()
        {
            this.BatchDataIn?.Clear();
            this.BatchDataOut?.Clear();
            if (this.SelectedBatch == null)
            {
                this.BatchReference = string.Empty;
                this.ScheduledDate = string.Empty;
                return;
            }

            this.BatchReference = this.SelectedBatch.Reference;
            this.ScheduledDate = this.SelectedBatch.ScheduledDate.ToDate().ToShortDateString();
            var batchData = this.DataManager.GetBatchData(this.SelectedBatch.SaleID, 3, true);
            var uiData = new CollectionData();

            if (batchData != null)
            {
                foreach (var batch in batchData)
                {
                    batch.Batches = this.allBatches.Where(x => x.INMasterID == batch.INMasterID).ToList();
                }

                this.BatchDataIn = new ObservableCollection<BatchData>(batchData);
                var localData = batchData.FirstOrDefault();
                if (localData != null)
                {
                    if (!string.IsNullOrEmpty(localData.Attribute1)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute1")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute1", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute2)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute2")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute2", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute3)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute3")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute3", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute4)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute4")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute4", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute5)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute5")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute5", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute6)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute6")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute6", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute7)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute7")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute7", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute8)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute8")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute8", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute9)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute9")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute9", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute10)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute10")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute10", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute11)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute11")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute11", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute12)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute12")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute12", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute13)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute13")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute13", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute14)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute14")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute14", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute15)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute15")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute15", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute16)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute16")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute16", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute17)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute17")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute17", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute18)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute18")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute18", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute19)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute19")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute19", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute20)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute20")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute20", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute21)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute21")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute21", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute22)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute22")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute22", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute23)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute23")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute23", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute24)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute24")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute24", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute25)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute25")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute25", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute26)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute26")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute26", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute27)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute27")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute27", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute28)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute28")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute28", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute29)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute29")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute29", name.AttributeDescription, string.Empty)); } }
                    if (!string.IsNullOrEmpty(localData.Attribute30)) { var name = this.AttributeLookUps.FirstOrDefault(x => x.Attribute_Name.CompareIgnoringCase("Attribute30")); if (name != null) { uiData.RowData.Add(Tuple.Create("Attribute30", name.AttributeDescription, string.Empty)); } }
                    this.BatchDataAttributes = new ObservableCollection<BatchData> {localData};
                }
            }

            Messenger.Default.Send(uiData, Token.BatchAttributes);

            var batchOut = this.DataManager.GetBatchData(this.SelectedBatch.SaleID, 4, true);
            if (batchOut != null)
            {
                foreach (var batch in batchOut)
                {
                    batch.Batches = this.allBatches.Where(x => x.INMasterID == batch.INMasterID).ToList();
                }

                this.BatchDataOut = new ObservableCollection<BatchData>(batchOut);
            }
        }
        private void UpdateCommandExecute(string param)
        {
            if (param == "Attributes")
            {
                if (this.BatchDataAttributes == null || !this.BatchDataAttributes.Any())
                {
                    return;
                }

                this.filteredBatchDataAttributes.Clear();
                this.filteredBatchDataAttributes.Add(this.BatchDataAttributes.FirstOrDefault());
                this.SetControlMode(ControlMode.Update);
                return;
            }

            if (param == "In")
            {
                if (this.SelectedBatchDataIn == null)
                {
                    return;
                }

                var inData = this.filteredBatchDataIn.FirstOrDefault(x => x.StockTransactionID == this.SelectedBatchDataIn.StockTransactionID);
                if (inData != null)
                {
                    this.filteredBatchDataIn.Remove(inData);
                }

                this.filteredBatchDataIn.Add(this.selectedBatchDataIn);
                this.SetControlMode(ControlMode.Update);
                return;
            }

            if (this.SelectedBatchDataOut == null)
            {
                return;
            }

            var outData = this.filteredBatchDataOut.FirstOrDefault(x => x.StockTransactionID == this.SelectedBatchDataIn.StockTransactionID);
            if (outData != null)
            {
                this.filteredBatchDataOut.Remove(outData);
            }

            this.filteredBatchDataOut.Add(this.selectedBatchDataOut);
            this.SetControlMode(ControlMode.Update);
        }

        private void PrintCommandExecute()
        {
            try
            {
                if (this.SelectedBatchDataIn != null)
                {
                    var transaction = this.DataManager.GetStockTransactionBySerial(this.SelectedBatchDataIn.Serial.ToInt(), 3);
                    var process = this.Processes.FirstOrDefault(x => x.ProcessID == this.SelectedBatchDataIn.ProcessID);
                    var labels = this.PrintManager.ProcessPrinting(this.SelectedBatchDataIn.BPMasterID,
                        this.SelectedBatchDataIn.BPGroupID, this.SelectedBatchDataIn.INMasterID, this.SelectedBatchDataIn.INGroupID,
                        ViewType.OutOfProduction, LabelType.Item, transaction.StockTransactionID.ToInt(),
                        1, warehouseId: this.SelectedBatchDataIn.WarehouseID, process: process);
                    if (labels.Item1.Any())
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(this.SelectedBatchDataIn.StockTransactionID, string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                    }

                    SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelPrinted, this.SelectedBatchDataIn.Serial));
                    return;
                }

                if (this.SelectedBatchDataOut != null)
                {
                    var transaction = this.DataManager.GetStockTransactionBySerial(this.SelectedBatchDataOut.Serial.ToInt(), 4);
                    var process = this.Processes.FirstOrDefault(x => x.ProcessID == this.SelectedBatchDataOut.ProcessID);
                    var labels = this.PrintManager.ProcessPrinting(this.SelectedBatchDataOut.BPMasterID,
                        this.SelectedBatchDataOut.BPGroupID, this.SelectedBatchDataOut.INMasterID, this.SelectedBatchDataOut.INGroupID,
                        ViewType.OutOfProduction, LabelType.Item, transaction.StockTransactionID.ToInt(),
                        1, warehouseId: this.SelectedBatchDataOut.WarehouseID, process: process);
                    if (labels.Item1.Any())
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(this.SelectedBatchDataOut.StockTransactionID, string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                    }

                    SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelPrinted, this.SelectedBatchDataOut.Serial));
                    return;
                }
            }
            catch (Exception e)
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelPrinted, this.SelectedBatchDataOut.Serial));
            }
        }

        private void ClearForm()
        {
            this.filteredBatchDataIn?.Clear();
            this.filteredBatchDataOut?.Clear();
            this.filteredBatchDataAttributes?.Clear();
            this.BatchDataIn?.Clear();
            this.BatchDataOut?.Clear();
            this.BatchDataAttributes?.Clear();
            this.SelectedBatch = null;
            this.SetControlMode(ControlMode.OK);
        }

        private void GetBatches()
        {
            this.allBatches = this.DataManager.GetBatches(0);
            //this.Batches = new ObservableCollection<BatchData>(this.allBatches);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            this.BatchDataIn = null;
            this.BatchDataOut = null;
            this.BatchDataAttributes = null;
            this.filteredBatchDataIn = null;
            this.filteredBatchDataOut = null;
            Messenger.Default.Send(Token.Message,Token.CloseBatchEditorWindow);
        }

        #endregion
    }
}

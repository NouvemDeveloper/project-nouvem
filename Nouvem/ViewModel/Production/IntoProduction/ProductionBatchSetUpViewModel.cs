﻿// -----------------------------------------------------------------------
// <copyright file="ProductionBatchSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Production.IntoProduction
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.DataLayer;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class ProductionBatchSetUpViewModel : SalesViewModelBase
    {
        #region field

        private decimal? recipeBatchQty;

        private HashSet<int> departmentsIds = new HashSet<int>();

        /// <summary>
        /// The selected touchscreen recipe item.
        /// </summary>
        private InventoryItem selectedRecipeItem;

        /// <summary>
        /// The selected production type.
        /// </summary>
        private NouPROrderType selectedProductionType;

        /// <summary>
        /// Show all the orders flag.
        /// </summary>
        private ObservableCollection<InventoryItem> items = new ObservableCollection<InventoryItem>();

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool nonQualityAssured;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool qualityAssured;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool allowOverAge;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool allowUnderAge;

        /// <summary>
        /// The born in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.NouCountry> countriesBornIn;

        /// <summary>
        /// The reared in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.NouCountry> countriesRearedIn;

        /// <summary>
        /// The reared in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Plant> plants;

        /// <summary>
        /// The categories.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Category> categories;

        /// <summary>
        /// The selected batch.
        /// </summary>
        private ProductionData selectedBatch = new ProductionData();

        /// <summary>
        /// The selected spec products.
        /// </summary>
        private ObservableCollection<ProductSelection> specificationProducts = new ObservableCollection<ProductSelection>();

        /// <summary>
        /// The selected document status.
        /// </summary>
        private NouDocStatu selectedDocStatus;

        /// <summary>
        /// The selected batch mode.
        /// </summary>
        private string selecteBatchMode;

        /// <summary>
        /// The batch options.
        /// </summary>
        private ObservableCollection<BatchOption> batchOptions;

        /// <summary>
        /// The customers.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.BusinessPartner> customers;

        /// <summary>
        /// The selected customer.
        /// </summary>
        private Model.BusinessObject.BusinessPartner selectedCustomer;

        /// <summary>
        /// The contacts.
        /// </summary>
        private ObservableCollection<BusinessPartnerContact> contacts;

        /// <summary>
        /// The selected spec.
        /// </summary>
        private ProductionData selectedSpecification = new ProductionData();

        /// <summary>
        /// The selected contact.
        /// </summary>
        private BusinessPartnerContact selectedContact;

        /// <summary>
        /// The spec name.
        /// </summary>
        private string specName;

        /// <summary>
        /// The current ui control.
        /// </summary>
        private string controlName;

        /// <summary>
        /// The localised document numbering collection.
        /// </summary>
        private ObservableCollection<DocNumber> docNumberings;

        /// <summary>
        /// The selected document numbering.
        /// </summary>
        private DocNumber selectedDocNumbering;

        /// <summary>
        /// The selected batch option.
        /// </summary>
        private BatchOption selectedBatchOption;

        /// <summary>
        /// The doc next number.
        /// </summary>
        private int nextNumber;

        /// <summary>
        /// The scheduled date.
        /// </summary>
        private DateTime scheduledDate;

        /// <summary>
        /// The release date.
        /// </summary>
        private DateTime releaseDate;

        /// <summary>
        /// The batch reference.
        /// </summary>
        private string batchReference;

        /// <summary>
        /// The specs.
        /// </summary>
        private ObservableCollection<ProductionData> specifications = new ObservableCollection<ProductionData>();

        /// <summary>
        /// The recalled editable order.
        /// </summary>
        private ProductionData orderToEdit;

        /// <summary>
        /// Flag, as to whether the controls are editable.
        /// </summary>
        private bool controlEnabled = true;
        /// <summary>
        /// The control name text (Add or Update).
        /// </summary>
        private string orderControlName;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductionBatchSetUpViewModel"/> class.
        /// </summary>
        public ProductionBatchSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.RecipeQty, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.RecipeBatchQty = s.ToDecimal();
                }
            });

            // Register to handle a collection data selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, this.HandleDataSelection);

            // Register for the incoming keyboard selection.
            Messenger.Default.Register<string>(this, Token.KeyboardValueEntered, s => this.BatchReference = s);

            #endregion

            #region command handler

            // Move back to main
            this.MoveBackCommand = new RelayCommand(() =>
            {
                if (ApplicationSettings.ScannerMode)
                {
                    ViewModelLocator.ClearProductionBatchSetUp();
                    Messenger.Default.Send(Token.Message, Token.CloseScannerBatchSetUp);
                    return;
                }

                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
            });

            // Handle the ui control selection.
            this.ControlSelectedCommand = new RelayCommand<string>(this.ControlSelectedCommandExecute);

            // Handle the ui load event.
            this.OnLoadedCommand = new RelayCommand(this.OnLoadedCommandExecute);

            // Handle a production batch creation.
            this.ProductionBatchCommand = new RelayCommand(this.ProductionBatchCommandExecute);

            // Handle a production batch cancellation.
            this.CancelProductionBatchCommand = new RelayCommand(this.CancelProductionBatchCommandExecute);

            // Edit an order.
            this.EditOrderCommand = new RelayCommand(this.EditOrderCommandExecute);

            #endregion

            this.Products = new ObservableCollection<ProductSelection>();
            this.departmentsIds = this.DataManager.GetDeviceDepartmentIds(NouvemGlobal.DeviceId.ToInt());
            this.GetReceipeItems();
            this.GetPlants();
            this.GetCountries();
            this.GetCategories();
            this.GetProducts();

            this.OrderControlName = Strings.Add;
            this.customers = new ObservableCollection<Model.BusinessObject.BusinessPartner>();
            this.contacts = new ObservableCollection<BusinessPartnerContact>();
            this.GetBatchModes();
            this.GetDocStatusItems();
            this.RefreshDates();
            this.GetBatchOptions();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public InventoryItem SelectedRecipeItem
        {
            get
            {
                return this.selectedRecipeItem;
            }

            set
            {
                this.selectedRecipeItem = value;
                this.RaisePropertyChanged();
                this.HandleSelectedRecipeItem();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public decimal? RecipeBatchQty
        {
            get
            {
                return this.recipeBatchQty;
            }

            set
            {
                if (this.SelectedRecipeItem == null && value.HasValue)
                {
                    SystemMessage.Write(MessageType.Issue, "Please select a recipe first");
                    return;
                }

                this.recipeBatchQty = value;
                this.RaisePropertyChanged();
                if (value.HasValue)
                {
                    this.HandleSelectedRecipeQty();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public ObservableCollection<InventoryItem> Items
        {
            get
            {
                return this.items;
            }

            set
            {
                this.items = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public NouPROrderType SelectedProductionType
        {
            get
            {
                return this.selectedProductionType;
            }

            set
            {
                this.selectedProductionType = value;
                this.RaisePropertyChanged();
                var isStandard = value == null || value.NouPROrderTypeID == 1;
                if (isStandard)
                {
                    this.Items = new ObservableCollection<InventoryItem>(this.SaleItems);
                }
                else
                {
                    this.Items = new ObservableCollection<InventoryItem>(this.ReceipeItems);
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(isStandard, Token.ProductionTypeSelected);
                }));
            }
        }

        /// <summary>
        /// Gets or sets the production types.
        /// </summary>
        public IList<NouPROrderType> ProductionTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool NonQualityAssured
        {
            get
            {
                return this.nonQualityAssured;
            }

            set
            {
                this.nonQualityAssured = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool QualityAssured
        {
            get
            {
                return this.qualityAssured;
            }

            set
            {
                this.qualityAssured = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool AllowOverAge
        {
            get
            {
                return this.allowOverAge;
            }

            set
            {
                this.allowOverAge = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public bool AllowUnderAge
        {
            get
            {
                return this.allowUnderAge;
            }

            set
            {
                this.allowUnderAge = value;
                this.RaisePropertyChanged();
                this.UpdateCommandExecute();
            }
        }

        /// <summary>
        /// Gets or sets the born in countries.
        /// </summary>
        public ObservableCollection<NouCountry> CountriesBornIn
        {
            get
            {
                return this.countriesBornIn;
            }

            set
            {
                this.countriesBornIn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the reared in countries.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.Plant> Plants
        {
            get
            {
                return this.plants;
            }

            set
            {
                this.plants = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the reared in countries.
        /// </summary>
        public ObservableCollection<NouCountry> CountriesRearedIn
        {
            get
            {
                return this.countriesRearedIn;
            }

            set
            {
                this.countriesRearedIn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.Category> Categories
        {
            get
            {
                return this.categories;
            }

            set
            {
                this.categories = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the products.
        /// </summary>
        public ObservableCollection<ProductSelection> Products { get; set; }

        /// <summary>
        /// Gets or sets the spec name.
        /// </summary>
        public string SpecName
        {
            get
            {
                return this.specName;
            }

            set
            {
                this.specName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected copy from specification.
        /// </summary>
        public ProductionData SelectedBatch
        {
            get
            {
                return this.selectedBatch;
            }

            set
            {
                this.selectedBatch = value;
                this.RaisePropertyChanged();

                if (value != null && value.Order.Reference != Strings.NoneSelected && !this.EntitySelectionChange)
                {
                    this.HandleSelectedBatch();
                }

                if (value == null || value.Order.Reference == Strings.NoneSelected)
                {
                    this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x =>
                        x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected specification.
        /// </summary>
        public ProductionData SelectedSpecification
        {
            get
            {
                return this.selectedSpecification;
            }

            set
            {
                this.selectedSpecification = value;
                this.RaisePropertyChanged();

                if (value != null && value.Specification.PRSpecID > 0 && value.Specification.Name != Strings.NoneSelected && !this.EntitySelectionChange)
                {
                    this.HandleSelectedSpecification();
                }
                else
                {
                    this.SpecName = string.Empty;
                    this.SpecificationProducts.Clear();
                }
            }
        }

        /// <summary>
        /// Get or sets the specification products.
        /// </summary>
        public ObservableCollection<ProductSelection> SpecificationProducts
        {
            get
            {
                return this.specificationProducts;
            }

            set
            {
                this.specificationProducts = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the add/update control name.
        /// </summary>
        public string OrderControlName
        {
            get
            {
                return this.orderControlName;
            }

            set
            {
                this.orderControlName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch option.
        /// </summary>
        public BatchOption SelectedBatchOption
        {
            get
            {
                return this.selectedBatchOption;
            }

            set
            {
                this.selectedBatchOption = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.GenerateReference();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui controls are editable.
        /// </summary>
        public bool ControlEnabled
        {
            get
            {
                return this.controlEnabled;
            }

            set
            {
                this.controlEnabled = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch reference.
        /// </summary>
        public ProductionData OrderToEdit
        {
            get
            {
                return this.orderToEdit;
            }

            set
            {
                this.orderToEdit = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleOrder();
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch reference.
        /// </summary>
        public string BatchReference
        {
            get
            {
                return this.batchReference;
            }

            set
            {
                this.batchReference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the production scheduled date.
        /// </summary>
        public DateTime ScheduledDate
        {
            get
            {
                return this.scheduledDate;
            }

            set
            {
                this.scheduledDate = value;
                this.RaisePropertyChanged();
                if (ApplicationSettings.TouchScreenMode || (this.SelectedBatch?.Order != null && this.SelectedBatch.Order.PROrderID == 0))
                {
                    this.BatchReference =
                        this.DataManager.GetProductionBatchReference(NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.UserId.ToInt(), 0, this.ScheduledDate);
                }
            }
        }

        /// <summary>
        /// Gets or sets the release date.
        /// </summary>
        public DateTime ReleaseDate
        {
            get
            {
                return this.releaseDate;
            }

            set
            {
                this.releaseDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected document status.
        /// </summary>
        public NouDocStatu SelectedDocStatus
        {
            get
            {
                return this.selectedDocStatus;
            }

            set
            {
                this.selectedDocStatus = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleDocStatusChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the nou doc status items.
        /// </summary>
        public IList<NouDocStatu> DocStatusItems { get; set; }

        /// <summary>
        /// Gets or sets the application batch modes.
        /// </summary>
        public string[] BatchModes { get; set; }

        /// <summary>
        /// Gets or sets the selected application batch mode.
        /// </summary>
        public string SelectedBatchMode
        {
            get
            {
                return this.selecteBatchMode;
            }

            set
            {
                this.selecteBatchMode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch options.
        /// </summary>
        public ObservableCollection<BatchOption> BatchOptions
        {
            get
            {
                return this.batchOptions;
            }

            set
            {
                this.batchOptions = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application document numbers.
        /// </summary>
        public ObservableCollection<DocNumber> DocNumberings
        {
            get
            {
                return this.docNumberings;
            }

            set
            {
                this.docNumberings = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected document numbering.
        /// </summary>
        public DocNumber SelectedDocNumbering
        {
            get
            {
                return this.selectedDocNumbering;
            }

            set
            {
                this.selectedDocNumbering = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.NextNumber = value.NextNumber;
                }
            }
        }

        /// <summary>
        /// Gets or sets the doc next number.
        /// </summary>
        public int NextNumber
        {
            get
            {
                return this.nextNumber;
            }

            set
            {
                this.nextNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the specifications.
        /// </summary>
        public ObservableCollection<ProductionData> Specifications
        {
            get
            {
                return this.specifications;
            }

            set
            {
                this.specifications = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        public ObservableCollection<BusinessPartnerContact> Contacts
        {
            get
            {
                return this.contacts;
            }

            set
            {
                this.contacts = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact.
        /// </summary>
        public BusinessPartnerContact SelectedContact
        {
            get
            {
                return this.selectedContact;
            }

            set
            {
                this.selectedContact = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to edit an order.
        /// </summary>
        public ICommand EditOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the move back command.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the control selection.
        /// </summary>
        public ICommand ControlSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to add/update a production batch.
        /// </summary>
        public ICommand ProductionBatchCommand { get; private set; }

        /// <summary>
        /// Gets the command to cancel a production batch.
        /// </summary>
        public ICommand CancelProductionBatchCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Opens up an order for editing.
        /// </summary>
        /// <param name="order">The order to edit.</param>
        public void EditOrder(ProductionData order)
        {
            this.SelectedBatch = order;
            this.ControlEnabled = false;
            this.OrderControlName = Strings.Update;
        }

        #endregion

        #endregion

        #region protected

        protected virtual void HandleSelectedRecipeItem() { }
        protected virtual void HandleSelectedRecipeQty() { }

        /// <summary>
        /// Gets the production types.
        /// </summary>
        protected virtual void GetProductionTypes()
        {
            var types = this.DataManager.GetPrOrderTypes();
            if (!types.Any())
            {
                this.ProductionTypes = new List<NouPROrderType>{new NouPROrderType{NouPROrderTypeID = 1, Description = Strings.Standard}};
            }
            else
            {
                this.ProductionTypes = types;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SelectedProductionType = this.ProductionTypes.First();
            }));
        }

        /// <summary>
        /// Clears the ui.
        /// </summary>
        protected void Clear()
        {
            this.SelectedCustomer = null;
            this.SelectedContact = null;
            this.SelectedBatchOption = null;
            this.SelectedRecipeItem = null;
            this.RecipeBatchQty = null;
            this.GetDocNumbering();
            this.BatchReference = string.Empty;
            this.RefreshDocStatusItems();
            this.RefreshDates();
            this.ControlEnabled = true;
            if (this.SelectedDocStatus == null)
            {
                this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x =>
                    x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
            }

            this.OrderControlName = Strings.Add;
        }

        /// <summary>
        /// Gets the document numbering data.
        /// </summary>
        protected override void GetDocNumbering()
        {
            this.DocNumberings = new ObservableCollection<DocNumber>(this.DataManager.GetDocumentNumbers()
                .Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.PRProductionBatch)));

            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
        }

        /// <summary>
        /// Updates a production batch.
        /// </summary>
        protected virtual void UpdateProduction()
        {
            // get the id's of the products to be added
            var productsToAdd = new HashSet<int>();
            foreach (var specificationProduct in this.specificationProducts)
            {
                productsToAdd.Add(-specificationProduct.NodeID.ToInt());
            }

            foreach (var productSelection in this.Products.Where(x => x.AddToList))
            {
                productsToAdd.Add(-productSelection.NodeID.ToInt());
            }

            #region validation

            if (this.selectedBatch == null || this.selectedBatch.Order.Reference == Strings.NoneSelected)
            {
                return;
            }

            #endregion

            if (this.SelectedSpecification == null)
            {
                this.SelectedSpecification = new ProductionData();
            }

            this.SelectedSpecification.Order = this.SelectedBatch.Order;
            foreach (var i in productsToAdd)
            {
                this.SelectedSpecification.Outputs.Add(new PROrderOutput { INMasterID = i, PROrderID = this.SelectedBatch.Order.PROrderID });
            }

            this.SelectedSpecification.CarcassTypes.Clear();
            foreach (var category in this.Categories.Where(x => x.IsSelected && x.CategoryID > 0))
            {
                this.SelectedSpecification.CarcassTypes.Add(new PRInputCarcassTypeOrder { CarcassTypeID = category.CategoryID, PROrderID = this.SelectedBatch.Order.PROrderID });
            }

            this.SelectedSpecification.Countries.Clear();
            foreach (var country in this.CountriesBornIn.Where(x => x.IsSelected && x.CountryID > 0))
            {
                this.SelectedSpecification.Countries.Add(new PRInputCountryOrder { CountryID = country.CountryID, BornIn = true, PROrderID = this.SelectedBatch.Order.PROrderID });
            }

            foreach (var country in this.CountriesRearedIn.Where(x => x.IsSelected && x.CountryID > 0))
            {
                this.SelectedSpecification.Countries.Add(new PRInputCountryOrder { CountryID = country.CountryID, BornIn = false, PROrderID = this.SelectedBatch.Order.PROrderID });
            }

            this.SelectedSpecification.Plants.Clear();
            foreach (var plant in this.Plants.Where(x => x.IsSelected && x.PlantID > 0))
            {
                this.SelectedSpecification.Plants.Add(new PRInputPlantOrder { PlantID = plant.PlantID, PROrderID = this.SelectedBatch.Order.PROrderID });
            }

            if ((this.QualityAssured && this.NonQualityAssured) || (!this.QualityAssured && !this.NonQualityAssured))
            {
                this.SelectedSpecification.Order.QA = null;
            }
            else
            {
                this.SelectedSpecification.Order.QA = this.QualityAssured;
            }

            if ((this.AllowOverAge && this.AllowUnderAge) || (!this.AllowOverAge && !this.AllowUnderAge))
            {
                this.SelectedSpecification.Order.OverAge = null;
            }
            else
            {
                this.SelectedSpecification.Order.OverAge = this.AllowOverAge;
            }

            this.SelectedSpecification.Order.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;
            if (this.DataManager.UpdateOrderSpecification(this.SelectedSpecification))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                //this.Refresh();
                //this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }

            this.SetControlMode(ControlMode.OK);
            //this.orderToEdit.Order.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;
            //this.orderToEdit.Order.BPContactID = this.SelectedContact != null
            //    ? this.SelectedContact.Details.BPContactID
            //    : (int?)null;
            //this.orderToEdit.Order.BPMasterID = this.SelectedCustomer != null
            //    ? this.SelectedCustomer.Details.BPMasterID
            //    : (int?)null;

            //if (this.DataManager.UpdateProductionOrder(this.OrderToEdit.Order))
            //{
            //    SystemMessage.Write(MessageType.Priority, Message.ProductionOrderUpdated);
            //    this.Clear();
            //}
            //else
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.ProductionOrderNotUpdated);
            //}
        }

        protected override void ControlSelectionCommandExecute()
        {
            // throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            // throw new NotImplementedException();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Adds or updates a production.
        /// </summary>
        private void ProductionBatchCommandExecute()
        {
            if (this.OrderControlName.Equals(Strings.Add))
            {
                this.AddProduction();
            }
            else
            {
                this.UpdateProduction();
            }
        }

        /// <summary>
        /// Creates a production batch.
        /// </summary>
        protected virtual void AddProduction()
        {
            if (this.Locator.ProcessSelection.SelectedProcess == null
                || this.Locator.ProcessSelection.SelectedProcess.Name != ProcessType.Butchery)
            {
                var localReference = this.BatchReference;
                if (ApplicationSettings.BatchAppendDDMM &&
                    (this.Locator.ProcessSelection.SelectedProcess == null || this.Locator.ProcessSelection.SelectedProcess.Name != ProcessType.Butchery)
                    && localReference.Length >= 4)
                {
                    localReference = localReference.Substring(0, localReference.Length - 4);
                }

                if (string.IsNullOrWhiteSpace(localReference))
                {
                    SystemMessage.Write(MessageType.Issue, Message.BatchReferenceCannotBeBlank);
                    NouvemMessageBox.Show(Message.BatchReferenceCannotBeBlank, touchScreen: true);
                    return;
                }
            }

            if (ApplicationSettings.WarnIfDuplicateProductionReference && !string.IsNullOrWhiteSpace(this.BatchReference))
            {
                var localOrder = this.DataManager.DoesPROrderByReferenceExist(this.BatchReference);
                if (localOrder.HasValue)
                {
                    NouvemMessageBox.Show(string.Format(Message.BatchReferenceAlreadyExists,
                            this.BatchReference, localOrder.ToDate().ToShortDateString()),
                        NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    if (NouvemMessageBox.UserSelection == UserDialogue.No)
                    {
                        return;
                    }
                }
            }

            if (ApplicationSettings.DisallowSlashesInProductionName && (this.BatchReference.Contains("/") || this.BatchReference.Contains(@"\")))
            {
                var msg = "Batch references cannot contain forward or back slashes";
                SystemMessage.Write(MessageType.Issue, msg);
                NouvemMessageBox.Show(msg, touchScreen: true);
                return;
            }

            // TODO need to implement production order set ups.
            this.GetDocNumbering();
            this.SelectedBatch = new ProductionData();

            this.Log.LogDebug(this.GetType(), "Creating new batch: Add production");
            this.SelectedBatch.BatchNumber =
           this.BatchManager.GenerateBatchNumber(false, this.BatchReference, referenceOnly:true);

            this.SelectedBatch.Order = new PROrder
            {
                DocumentNumberingID = this.SelectedDocNumbering.DocumentNumberingID,
                Number = this.NextNumber,
                NouDocStatusID = this.SelectedDocStatus.NouDocStatusID,
                ScheduledDate = this.ScheduledDate,
                Reference = this.BatchReference,
                CreationDate = DateTime.Now,
                PRSpecID = this.SelectedSpecification.Specification.PRSpecID,
                BPMasterID = this.SelectedCustomer != null ? this.SelectedCustomer.BPMasterID : (int?)null,
                BPContactID = this.SelectedContact != null ? this.SelectedContact.Details.BPContactID : (int?)null,
                UserID = NouvemGlobal.UserId.ToInt(),
                DeviceID = NouvemGlobal.DeviceId.ToInt(),
                BatchNumberID = this.SelectedBatch.BatchNumber.BatchNumberID
            };

            this.SelectedBatch.Order.PROrderID = this.DataManager.AddProductionOrder(this.SelectedBatch);
            if (this.SelectedBatch.Order.PROrderID > 0)
            {
                SystemMessage.Write(MessageType.Priority, Message.ProductionOrderCreated);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.ProductionOrderCreated, scanner: true);
                }

                Messenger.Default.Send(Token.Message, Token.ProductionBatchCreated);
                this.DataManager.SetDocumentNumber(this.selectedDocNumbering.DocumentNumberingID);
                this.Clear();
                this.UpdateProduction();

                if (!ApplicationSettings.ScannerMode)
                {
                    this.MoveBackCommandExecute();
                    this.Locator.TouchscreenProductionOrders.SelectedOrder = this.SelectedBatch;
                    this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.ProductionOrderNotCreated);
            }
        }

        /// <summary>
        /// Moves back to main screen.
        /// </summary>
        protected virtual void MoveBackCommandExecute()
        {
            if (ApplicationSettings.ScannerMode)
            {
                ViewModelLocator.ClearProductionBatchSetUp();
                Messenger.Default.Send(Token.Message, Token.CloseScannerBatchSetUp);
                return;
            }

            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
        }

        /// <summary>
        /// Cancels a production batch.
        /// </summary>
        private void CancelProductionBatchCommandExecute()
        {
            this.Clear();
        }

        /// <summary>
        /// Gets the command to handle the control selection.
        /// </summary>
        private void OnLoadedCommandExecute()
        {
            this.Locator.FactoryScreen.ModuleName = Strings.ProductionBatchSetUp;

            if (this.OrderControlName.Equals(Strings.Update))
            {
                return;
            }

            this.GetSpecifications();
            this.GetCustomers();
            this.GetDocNumbering();
            this.Clear();
            this.BatchReference =
                this.DataManager.GetProductionBatchReference(NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.UserId.ToInt(),0, this.ScheduledDate);
        }

        /// <summary>
        /// Handles the ui unload event.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.ControlEnabled = true;
            this.OrderControlName = Strings.Add;
        }

        /// <summary>
        /// Handle the selection of a control.
        /// </summary>
        /// <param name="control">The control name.</param>
        private void ControlSelectedCommandExecute(string control)
        {
            this.controlName = control;
            if (control.Equals(Constant.Specification) && this.Specifications.Any())
            {
                var data = (from localSpec in this.specifications
                            select new CollectionData { Data = localSpec.Specification.Name, ID = localSpec.Specification.PRSpecID, Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.Customer) && this.Customers.Any())
            {
                var data = (from localData in this.customers.OrderBy(x => x.Details.Name)
                            select new CollectionData { Data = localData.Details.Name, ID = localData.Details.BPMasterID, Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.Contact) && this.Contacts.Any())
            {
                var data = (from localData in this.contacts
                            select new CollectionData { Data = localData.FullName, ID = localData.Details.BPContactID, Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.Receipe) && this.ReceipeItems.Any())
            {
                var data = (from localData in this.ReceipeItems
                        select new CollectionData { Data = localData.Name, ID = localData.INMasterID.ToInt(), Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.Quantity))
            {
                Keypad.Show(KeypadTarget.RecipeQty);
                return;
            }

            if (control.Equals(Constant.Document) && this.DocNumberings.Any())
            {
                var data = (from localData in this.docNumberings
                            select new CollectionData { Data = localData.DocType.Name, ID = localData.DocumentNumberingTypeID, Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.BatchMode) && this.BatchModes.Any())
            {
                var data = (from localData in this.BatchModes
                            select new CollectionData { Data = localData, Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.Status) && this.DocStatusItems.Any())
            {
                var data = (from localData in this.DocStatusItems
                            select new CollectionData { Data = localData.Value, ID = localData.NouDocStatusID, Identifier = ViewType.ProductionBatchSetUp.ToString() })
                    .ToList();

                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            if (control.Equals(Constant.DocumentDate))
            {
                Messenger.Default.Send(true, Token.OpenCalender);
                return;
            }

            if (control.Equals(Constant.ReleaseDate))
            {
                Messenger.Default.Send(false, Token.OpenCalender);
                return;
            }

            if (control.Equals(Constant.BatchReference))
            {
                if (ApplicationSettings.ScannerMode)
                {
                    Messenger.Default.Send(ViewType.KeyboardSmall);
                    return;
                }

                Messenger.Default.Send(ViewType.Keyboard);
            }
        }

        private void EditOrderCommandExecute()
        {
            //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenProductionOrders;
            this.Locator.TouchscreenProductionOrders.EditOrder = true;
            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.OutOfProduction);
            //this.Locator.TouchscreenProductionOrders.GetAllOrders();
            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the application products.
        /// </summary>
        private void GetProducts()
        {
            this.Products.Clear();
            foreach (var product in this.DataManager.GetGroupedProducts().OrderBy(x => x.ProductName))
            {
                this.Products.Add(product);
            }
        }

        /// <summary>
        /// Handles a selected spec.
        /// </summary>
        protected virtual void HandleSelectedSpecification()
        {
            this.EntitySelectionChange = true;
            this.SpecificationProducts.Clear();
            var specProducts = this.SelectedSpecification.OutputsSpec.Select(x => (int?)x.INMasterID).ToList();

            foreach (var productSelection in this.Products)
            {
                //productSelection.AddToList = specProducts.Contains(productSelection.NodeID);
                if (specProducts.Contains(Math.Abs(productSelection.NodeID.ToInt())))
                {
                    productSelection.AddToList = true;
                }
                else
                {
                    productSelection.AddToList = false;
                }
            }

            var specCategories = this.SelectedSpecification.CarcassTypesSpec.Select(x => (int?)x.CarcassTypeID).ToList();
            foreach (var categorySelection in this.Categories)
            {
                categorySelection.IsSelected = specCategories.Contains(categorySelection.CategoryID);
            }

            var specCountriesBornIn = this.SelectedSpecification.CountriesSpec.Where(c => c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = specCountriesBornIn.Contains(country.CountryID);
            }

            var specCountriesRearedIn = this.SelectedSpecification.CountriesSpec.Where(c => !c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = specCountriesRearedIn.Contains(country.CountryID);
            }

            var specPlants = this.SelectedSpecification.PlantsSpec.Select(x => (int?)x.PlantID).ToList();
            foreach (var plant in this.Plants)
            {
                plant.IsSelected = specPlants.Contains(plant.PlantID);
            }

            if (!this.SelectedSpecification.Specification.QA.HasValue)
            {
                this.QualityAssured = true;
                this.NonQualityAssured = true;
            }
            else
            {
                this.QualityAssured = this.SelectedSpecification.Specification.QA.ToBool();
                this.NonQualityAssured = !this.QualityAssured;
            }

            if (!this.SelectedSpecification.Specification.OverAge.HasValue)
            {
                this.AllowOverAge = true;
                this.AllowUnderAge = true;
            }
            else
            {
                this.AllowOverAge = this.SelectedSpecification.Specification.OverAge.ToBool();
                this.AllowUnderAge = !this.AllowOverAge;
            }

            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Handle an order for editing.
        /// </summary>
        private void HandleOrder()
        {
            this.SelectedSpecification = this.orderToEdit;
            this.SelectedCustomer = this.orderToEdit.BPCustomer != null ? this.Customers.FirstOrDefault(x => x.BPMasterID == this.orderToEdit.BPCustomer.BPMasterID) : null;
            this.SelectedContact = this.orderToEdit.BPContact != null ? this.Contacts.FirstOrDefault(x => x.Details.BPContactID == this.orderToEdit.BPContact.BPContactID) : null;
            this.SelectedDocNumbering = this.orderToEdit.DocumentNumberingType != null
                ? this.DocNumberings.FirstOrDefault(
                    x => x.DocumentNumberingTypeID == this.orderToEdit.DocumentNumberingType.DocumentNumberingTypeID)
                : null;
            this.NextNumber = this.orderToEdit.DocumentNo;
            this.BatchReference = this.orderToEdit.Reference;
            this.SelectedBatchMode = this.BatchModes.FirstOrDefault();
            this.ScheduledDate = this.orderToEdit.ScheduledDate.ToDate();
            this.SelectedDocStatus = this.orderToEdit.DocStatus;
        }

        /// <summary>
        /// Refresh the dates.
        /// </summary>
        private void RefreshDates()
        {
            this.ScheduledDate = DateTime.Today;
            this.ReleaseDate = DateTime.Today;
        }

        /// <summary>
        /// Gets the batch modes.
        /// </summary>
        private void GetBatchModes()
        {
            this.BatchModes = new[] { Strings.Sequential, Strings.Manual };
            this.SelectedBatchMode = this.BatchModes.FirstOrDefault();
        }

        /// <summary>
        /// Handles the incoming data selection.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        private void HandleDataSelection(CollectionData data)
        {
            #region validation

            if (string.IsNullOrEmpty(this.controlName))
            {
                return;
            }

            if (!data.Identifier.Equals(ViewType.ProductionBatchSetUp.ToString()))
            {
                return;
            }

            #endregion

            try
            {
                if (this.controlName.Equals(Constant.Specification))
                {
                    this.SelectedSpecification = this.Specifications.FirstOrDefault(x => x.Specification.PRSpecID == data.ID);
                    return;
                }

                if (this.controlName.Equals(Constant.Customer))
                {
                    this.SelectedCustomer = this.Customers.FirstOrDefault(x => x.BPMasterID == data.ID);
                    return;
                }

                if (this.controlName.Equals(Constant.Contact))
                {
                    this.SelectedContact = this.Contacts.FirstOrDefault(x => x.Details.BPContactID == data.ID);
                    return;
                }

                if (this.controlName.Equals(Constant.Document))
                {
                    this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocumentNumberingTypeID == data.ID);
                    return;
                }

                if (this.controlName.Equals(Constant.BatchMode))
                {
                    this.SelectedBatchMode = this.BatchModes.FirstOrDefault(x => x == data.Data);
                    return;
                }

                if (this.controlName.Equals(Constant.Status))
                {
                    this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == data.ID);
                    return;
                }

                if (this.controlName.Equals(Constant.Receipe))
                {
                    this.SelectedRecipeItem = this.ReceipeItems.FirstOrDefault(x => x.Master.INMasterID == data.ID);
                    return;
                }
            }
            finally
            {
                this.controlName = null;
            }
        }

        /// <summary>
        /// Handles a selected spec.
        /// </summary>
        protected virtual void HandleSelectedBatch() { }

        /// <summary>
        /// Handles an update.
        /// </summary>
        protected virtual void UpdateCommandExecute() { }

        /// <summary>
        /// Gets the specifications.
        /// </summary>
        protected virtual void GetSpecifications()
        {
            var localSpecs = this.DataManager.GetSpecifications();
            this.Specifications = new ObservableCollection<ProductionData>(localSpecs);
            this.SelectedSpecification = this.Specifications.FirstOrDefault();
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems =
                this.DataManager.GetNouDocStatusItems()
                    .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                || x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ||
                                x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Gets the receipe products.
        /// </summary>
        private void GetReceipeItems()
        {
            if (ApplicationSettings.FilterProductionOrdersByDepartment && this.departmentsIds.Any())
            {
                this.ReceipeItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems
                    .Where(x => this.departmentsIds.Contains(x.Master.DepartmentID.ToInt()) && x.Master.Deleted == null && (x.Master.ReceipeProduct != null && x.Master.ReceipeProduct == true && (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                             || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                    .OrderBy(x => x.Master.Name));
                return;
            }

            this.ReceipeItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems
                .Where(x => x.Master.Deleted == null && (x.Master.ReceipeProduct != null && x.Master.ReceipeProduct == true && (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                         || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                .OrderBy(x => x.Master.Name));
        }

        /// <summary>
        /// Gets the batch options.
        /// </summary>
        private void GetBatchOptions()
        {
            this.BatchOptions = new ObservableCollection<BatchOption>(this.DataManager.GetBatchOptions());
        }

        /// <summary>
        /// Auto generates the batch reference.
        /// </summary>
        private void GenerateReference()
        {
            if (string.IsNullOrEmpty(this.SelectedBatchOption.Name))
            {
                return;
            }

            this.BatchReference = this.BatchManager.GenerateBatchReference(this.SelectedBatchOption.Name);
        }

        /// <summary>
        /// Gets the application plants.
        /// </summary>
        protected override void GetPlants()
        {
            var localPlants = this.DataManager.GetAllPlants();
            localPlants.Insert(0, new Model.DataLayer.Plant());

            this.Plants = new ObservableCollection<Nouvem.Model.BusinessObject.Plant>(
                (from x in localPlants
                 select new Nouvem.Model.BusinessObject.Plant
                 {
                     PlantID = x.PlantID,
                     CountryID = x.CountryID,
                     Code = x.Code,
                     SiteName = x.SiteName
                 }).ToList());
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        protected override void GetCategories()
        {
            var localCategories = (from cat in this.DataManager.GetCategories()
                                   select new Model.BusinessObject.Category
                                   {
                                       CategoryID = cat.CategoryID,
                                       Name = cat.Name,
                                       Description = cat.Description,
                                       CatType = cat.CatType
                                   }).ToList();

            localCategories.Insert(0, new Category());
            this.Categories = new ObservableCollection<Model.BusinessObject.Category>(localCategories);
        }

        /// <summary>
        /// Gets the application countries.
        /// </summary>
        protected override void GetCountries()
        {
            var localCountries = this.DataManager.GetCountryMaster();
            localCountries.Insert(0, new Country());
            this.CountriesBornIn = new ObservableCollection<NouCountry>(
                (from x in localCountries
                 select new NouCountry
                 {
                     CountryID = x.CountryID,
                     Name = x.Name,
                     Code = x.Code,
                     BornIn = true
                 }).ToList());

            this.CountriesRearedIn = new ObservableCollection<NouCountry>(
                (from x in localCountries
                 select new NouCountry
                 {
                     CountryID = x.CountryID,
                     Name = x.Name,
                     Code = x.Code
                 }).ToList());
        }

        #endregion

        #endregion

        protected override void CopyDocumentTo()
        {
            //throw new NotImplementedException();
        }

        protected override void CopyDocumentFrom()
        {
            //throw new NotImplementedException();
        }

        protected override void FindSale()
        {
            //throw new NotImplementedException();
        }

        protected override void FindSaleCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void DirectSearchCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void AddSale()
        {
            //throw new NotImplementedException();
        }

        protected override void UpdateSale()
        {
            //throw new NotImplementedException();
        }

        protected override void Close()
        {
            //throw new NotImplementedException();
        }
    }
}


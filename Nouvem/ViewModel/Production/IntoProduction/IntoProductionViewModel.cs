﻿// -----------------------------------------------------------------------
// <copyright file="IntoProductionViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Nouvem.Model.DataLayer;
using Nouvem.View.Production.IntoProduction;

namespace Nouvem.ViewModel.Production.IntoProduction
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class IntoProductionViewModel : TransactionsHelperViewModel
    {
        #region field

        private bool isRecipeBusy;

        private decimal? typicalBatchSize;

        private ProductionData recipeBatch;

        private ViewModelBase mainViewModel;

        private bool allowManualWeightEntry;

        /// <summary>
        /// The transaction of the product scanned in for a record last weight line.
        /// </summary>
        private StockTransaction recipeLostWeightTransaction;

        /// <summary>
        /// The manually selected batch.
        /// </summary>
        private BatchNumber batchNumber;

        /// <summary>
        /// The scanned barcode text.
        /// </summary>
        private string scannerText;

        private string entryProcessType;

        /// <summary>
        /// The recipe order methods.
        /// </summary>
        private IList<App_GetRecipeOrderMethods_Result> recipeOrderMethods;

        /// <summary>
        /// The incoming manually set weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The receipe mix indicator.
        /// </summary>
        private string mix;

        /// <summary>
        /// The conformace details.
        /// </summary>
        private ObservableCollection<SaleDetail> conformanceDetails = new ObservableCollection<SaleDetail>();

        /// <summary>
        /// The receipe product.
        /// </summary>
        private SaleDetail receipeProduct;

        /// <summary>
        /// The mix count.
        /// </summary>
        private int mixCount;

        /// <summary>
        /// The mix count.
        /// </summary>
        private int mixesRequired;

        /// <summary>
        /// The incoming pieces selection.
        /// </summary>
        private int pieces;

        /// <summary>
        /// The incoming pieces selection.
        /// </summary>
        private string attributeReference;

        /// <summary>
        /// The incoming qty selection.
        /// </summary>
        private decimal qty;

        /// <summary>
        /// The rework stock flag.
        /// </summary>
        private bool reworkStock;

        /// <summary>
        /// The order spec name.
        /// </summary>
        private string spec;

        /// <summary>
        /// The incoming tare weight.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The carcass split transaction id.
        /// </summary>
        private int carcassSplitTransactionId;

        /// <summary>
        /// The carcass split options.
        /// </summary>
        private IList<CarcassSplitOption> carcassSplitOptions;

        /// <summary>
        /// The selected order.
        /// </summary>
        private ProductionData selectedOrder;

        /// <summary>
        /// The production details collection.
        /// </summary>
        private ObservableCollection<SaleDetail> productionDetails;

        /// <summary>
        /// The production details collection.
        /// </summary>
        private List<SaleDetail> recipeDetails = new List<SaleDetail>();

        /// <summary>
        /// The selected product detail.
        /// </summary>
        private SaleDetail selectedProductionDetail;

        private string serial;

        /// <summary>
        /// Carcass split hind.
        /// </summary>
        private InventoryItem HindProduct;

        /// <summary>
        /// Carcass split fore.
        /// </summary>
        private InventoryItem ForeProduct;

        /// <summary>
        /// Carcass split fore.
        /// </summary>
        private InventoryItem FullCarcassProduct;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="IntoProductionViewModel"/> class.
        /// </summary>
        public IntoProductionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.RecipeQuantity, s =>
            {
                var currentBatchSize = this.TypicalBatchSize.ToDecimal();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (s.ToDecimal() <= 0)
                    {
                        NouvemMessageBox.Show(string.Format(Message.ZeroQuantityRecipeStop, s.ToDecimal()), touchScreen:true);
                        return;
                    }

                    NouvemMessageBox.Show(string.Format(Message.RecipeOrderWeightChange, currentBatchSize, s.ToDecimal()), NouvemMessageBoxButtons.OKCancel, true);
                    if (NouvemMessageBox.UserSelection != UserDialogue.OK)
                    {
                        return;
                    }

                    this.TypicalBatchSize = s.ToDecimal();
                    this.ResetRecipeAmounts(currentBatchSize);
                }));               
            });

            Messenger.Default.Register<int>(this, Token.HaccpComplete, i =>
            {
                this.Locator.Touchscreen.Module = Strings.IntoProduction;
                this.DisableModule = false;
                if (this.SelectedOrder != null)
                {
                    this.SelectedOrder.Order.AttributeTemplateRecordID = i;
                }
            });

            Messenger.Default.Register<BatchNumber>(this, Token.BatchNumberSelected, b => this.HandleNewBatch(b));

            Messenger.Default.Register<ProductionData>(this, Token.BatchNumberSelected, b =>
            {
                this.recipeBatch = b;
                this.BatchIdBase = b.RecipeBatchId;
                this.HandleNewBatch(b.BatchNumber);
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.PostSelectionMacroRefresh, s =>
            {
                if (this.IsFormLoaded && ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing && this.SelectedOrder != null)
                {
                    this.Locator.TouchscreenProductionOrders.SetOrder(this.SelectedOrder.Order.PROrderID);
                    this.Locator.TouchscreenProductionOrders.HandleBatchOrder();
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.StockLabels, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleStockLabels(s);
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.PrintBarcode, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintBarcode();
                }
            });

            // Register for the incoming process.
            Messenger.Default.Register<Model.DataLayer.Process>(this, Token.ProcessSelected, i =>
            {
                var checkNotMade = this.DataManager.GetProcessStartUpChecks(i.ProcessID);
                if (checkNotMade.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.MissingProcessStartUpCheckMessage = string.Format(Message.HACCPNotCompleted, i.Name.Trim(), checkNotMade.First());
                        SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                        NouvemMessageBox.Show(this.MissingProcessStartUpCheckMessage, touchScreen: true);
                    }));

                    this.DisableModule = true;
                    return;
                }

                this.DisableModule = false;

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.SetIntoProductionMode(i);
                    if (this.Locator.ProcessSelection.SaveAsDefaultProcess)
                    {
                        this.DataManager.SetDefaultModuleProcess(ViewType.IntoProduction, ApplicationSettings.IntoProductionMode.ToString(), NouvemGlobal.DeviceId.ToInt());
                    }
                }));
            });

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowIntoProductionAttributes = b);

            // Register for the incoming product selection.
            Messenger.Default.Register<InventoryItem>(this, Token.ProductSelected, o =>
            {
                this.HandleProductSelection(o);
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.IntoProduction, s =>
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // Register for the indicator tare weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                this.manualWeight = false;
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.SelectAllProduct)
                {
                    this.RecordSelectedProductWeight();
                }
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionConformance)
                {
                    this.RecordConformanceProductWeight();
                }
                else
                {
                    this.RecordWeight();
                }
            });

            // Register for an incoming keypad value for the weight.
            Messenger.Default.Register<string>(this, KeypadTarget.Weight, s =>
            {
                if (string.IsNullOrWhiteSpace(s))
                {
                    this.IndicatorManager.OpenIndicatorPort();
                }

                this.manualWeight = true;
                this.indicatorWeight = s.ToDecimal();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.SelectAllProduct)
                {
                    this.RecordSelectedProductWeight();
                }
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionConformance)
                {
                    this.RecordConformanceProductWeight();
                }
                else
                {
                    this.RecordWeight();
                }
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                if (this.SelectedProductionDetail != null)
                {
                    this.SelectedProductionDetail.ManualEntryComplete = false;
                }

                this.HandleScannerData(s);
            });

            // Register for the incoming pieces.
            Messenger.Default.Register<decimal>(this, Token.QuantityUpdate, i =>
            {
                this.qty = i;
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery ||
                    ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
                {
                    return;
                }

                if (this.SelectedProductionDetail != null && this.SelectedProductionDetail.InventoryItem != null)
                {
                    if (ApplicationSettings.IntoProductionMode != IntoProductionMode.Recipe)
                    {
                        var localQty = this.qty < 1 ? 1 : this.qty;
                        var localWgt = this.Locator.Indicator.Weight;
                        this.Locator.Indicator.Tare =
                            (this.SelectedProductionDetail.InventoryItem.PiecesTareWeight * localQty.ToDouble())
                            + this.SelectedProductionDetail.InventoryItem.BoxTareWeight;
                        this.Locator.Indicator.Weight = localWgt;
                    }
                }

                this.SetTypicalWeight();

                //if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                //{
                //    if (i == 0)
                //    {
                //        this.Locator.Indicator.SetManualWeight(0);
                //        return;
                //    }

                //    if (this.SelectedProductionDetail?.NouOrderMethod == Constant.Weight && this.SelectedProductionDetail?.InventoryItem != null)
                //    {
                //        var typicalWgt = this.SelectedProductionDetail.InventoryItem.Master.NominalWeight.ToDecimal();
                //        if (typicalWgt > 0)
                //        {
                //            this.Locator.Indicator.SetManualWeight(i * typicalWgt);
                //        }
                //    }
                //}
            });

            Messenger.Default.Register<ProductionData>(this, Token.ProductionOrderSelected, p =>
            {
                this.receipeProduct = null;
                this.HandleProductionOrder(p, false);
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // register for the incoming traceability results.
            Messenger.Default.Register<Tuple<string, List<TraceabilityResult>>>(this, Token.TraceabilityValues, this.RecordSerialProductionComplete);

            #endregion

            #region command handler

            this.RecordRecipeQuantityCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.RecipeQuantity),
                () => this.SelectedOrder != null && this.receipeProduct != null && this.productionDetails != null);

            // Remove a product line.
            this.CompleteMixCommand = new RelayCommand(this.CompleteMixCommandExecute);

            // Remove a product line.
            this.PrintPorkLabelsCommand = new RelayCommand(this.PrintPorkLabelsCommandExecute);

            // Remove a product line.
            this.RecordBatchWeightCommand = new RelayCommand(this.RecordBatchWeightCommandExecute);

            // Remove a product line.
            this.RemoveProductCommand = new RelayCommand(this.RemoveProductCommandExecute);

            // Handle the scanner move back to main ui request.
            this.MoveBackCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearTouchscreenProductionOrders();
                Messenger.Default.Send(Token.Message, Token.CloseScannerIntoProduction);
            });

            // Handle the scanner manual scan.
            this.ManualScanCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.IntoProduction, true));

            // Handle the view switch.
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);

            // Handle the ui loaded event.
            this.OnLoadedCommand = new RelayCommand(this.OnLoadedCommandExecute);

            // Handle the ui unloaded event.
            this.OnUnloadedCommand = new RelayCommand(this.OnUnloadedCommandExecute);

            #endregion

            this.AutoBoxLabels = new HashSet<int>();
            this.allowManualWeightEntry = this.AuthorisationsManager.AllowUserToEnterManualWeightOnTouchscreen;
            this.MainViewModel = this.Locator.IntoProductionMainGrid;
            this.GetCategories();
            this.GetDateDays();
            this.GetCountries();
            this.GetPlants();
            this.OrderMethods = this.DataManager.GetOrderMethods()
                .Where(x => x.Name.CompareIgnoringCase(Strings.Quantity)
                            || x.Name.CompareIgnoringCase(Strings.Weight)).ToList();
            this.NouUOMs = this.DataManager.GetNouUOMs();
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowIntoProductionAttributes;
            this.Locator.TouchscreenProductionOrders.GetOrders();
            this.Locator.Touchscreen.OutOfProductionExtensionWidth = 0;
            this.SetUpTimer();
            NouvemGlobal.CheckBatchValuesSet = false;
            this.Spec = Message.SelectProductionOrder;
            this.Locator.Indicator.WeighMode = WeighMode.Manual;
            ViewModelLocator.ClearStockMovementTouchscreen();
            this.GetAttributeLookUps();
            this.GetAttributeAllocationData();
            this.ClearAttributes();
            this.ClearAttributeControls();
            this.GetRecipeOrderMethods();
            this.StockLabelsToPrint = 1;
            this.StockDetails = new ObservableCollection<StockDetail>();
            this.Locator.ProcessSelection.SetDefaultProcesses(ApplicationSettings.DefaultIntoProductionModes, ApplicationSettings.IntoProductionMode.ToString());

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
            {
                this.ForeProduct = NouvemGlobal.InventoryItems.First(x => x.Master.INMasterID == ApplicationSettings.ForeProduct);
                this.HindProduct = NouvemGlobal.InventoryItems.First(x => x.Master.INMasterID == ApplicationSettings.HindProduct);
                if (ApplicationSettings.UseFullCarcassProduct)
                {
                    this.FullCarcassProduct = NouvemGlobal.InventoryItems.First(x => x.Master.INMasterID == ApplicationSettings.FullCarcassProduct);
                }
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct)
            {
                this.GetCarcassSplitOptions();
            }

            this.Locator.Touchscreen.UIText = string.Empty;
            this.ProductionOrdersScreenCreated = false;
            this.CloseHiddenWindows();
            this.SetPanel();
            this.IndicatorManager.IgnoreValidation(false);
            this.entryProcessType = ApplicationSettings.IntoProductionMode.ToString();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets a products typical weight.
        /// </summary>
        public bool IsRecipeBusy
        {
            get { return this.isRecipeBusy; }
            set
            {
                this.isRecipeBusy = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets a products typical weight.
        /// </summary>
        public decimal? TypicalWeight
        {
            get
            {
                if (this.SelectedProductionDetail?.InventoryItem != null &&
                    !this.SelectedProductionDetail.InventoryItem.Master.NominalWeight.IsNullOrZero())
                    
                {
                    return this.SelectedProductionDetail.InventoryItem.Master.NominalWeight.ToDecimal();
                }

                return null;
            }
        }

        public bool ReworkingStock
        {
            get { return this.reworkStock && this.recipeLostWeightTransaction != null; }
        }

        /// <summary>
        /// Gets or sets the conformance product.
        /// </summary>
        public ObservableCollection<SaleDetail> ConformanceDetails
        {
            get
            {
                return this.conformanceDetails;
            }

            set
            {
                this.conformanceDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public decimal? TypicalBatchSize
        {
            get
            {
                return this.typicalBatchSize;
            }

            set
            {
                this.typicalBatchSize = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber
        {
            get
            {
                return this.batchNumber;
            }

            set
            {
                this.batchNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock labels to print.
        /// </summary>
        public ViewModelBase MainViewModel
        {
            get
            {
                return this.mainViewModel;
            }

            set
            {
                this.mainViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock labels to print.
        /// </summary>
        public int MixCount
        {
            get
            {
                return this.mixCount;
            }

            set
            {
                this.mixCount = value;
                if (this.mixesRequired == 0)
                {
                    this.mixesRequired++;
                }

                if (value > this.mixesRequired)
                {
                    value = this.mixesRequired;
                }

                this.Mix = string.Format("{0} {1} {2}", value, Strings.Of, this.mixesRequired);
            }
        }

        /// <summary>
        /// Gets or sets the stock labels to print.
        /// </summary>
        public string Mix
        {
            get
            {
                return this.mix;
            }

            set
            {
                this.mix = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the intake lorry code.
        /// </summary>
        public string AttributeReference
        {
            get
            {
                return this.attributeReference;
            }

            set
            {
                this.attributeReference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the spec name.
        /// </summary>
        public string Spec
        {
            get
            {
                return this.spec;
            }

            set
            {
                this.spec = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the production details.
        /// </summary>
        public ObservableCollection<SaleDetail> ProductionDetails
        {
            get
            {
                return this.productionDetails;
            }

            set
            {
                this.productionDetails = value;
                this.RaisePropertyChanged();

                foreach (var saleDetail in value)
                {
                    // shouldn't happen..but just in case
                    if (saleDetail.InventoryItem == null)
                    {
                        saleDetail.InventoryItem =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == saleDetail.INMasterID);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the sale detail.
        /// </summary>
        public SaleDetail SelectedProductionDetail
        {
            get
            {
                return this.selectedProductionDetail;
            }

            set
            {
                this.selectedProductionDetail = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (value.InventoryItem != null && !ApplicationSettings.ZeroTareOutsideOfDispatch)
                    {
                        var localQty = this.qty < 1 ? 1 : this.qty;
                        this.Locator.Indicator.Tare =
                            (value.InventoryItem.PiecesTareWeight * localQty.ToDouble())
                            + value.InventoryItem.BoxTareWeight;
                    }

                    this.CheckAttributeProductChangeReset(this.SelectedProductionDetail);
                    this.HandleSaleDetail(this.SelectedProductionDetail, this);

                    //if (this.SelectedOrder != null && !this.SelectedOrder.ProductionAttributesSet && this.SelectedOrder.LastBatchAttribute != null)
                    //{
                    //    this.SetAttributeValues(this.SelectedOrder.LastBatchAttribute);
                    //    this.HandleSaleDetailForTransaction(value, this);
                    //    this.SelectedOrder.ProductionAttributesSet = true;
                    //}

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                        || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                        || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                        || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
                    {
                        this.Locator.Touchscreen.Quantity = 0;
                    }

                    if (value.StockMode != StockMode.Serial && ApplicationSettings.IntoProductionMode != IntoProductionMode.Recipe)
                    {
                        this.BatchNumber = this.DataManager.GetLastProductBatch(value.INMasterID);
                    }

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch)
                    {
                        this.BatchNumber = null;
                    }

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                    {
                        this.Locator.Touchscreen.Quantity = 0;
                        this.qty = 0;
                        if (!this.Scanning && this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse())
                        {
                            this.HandleRecipeProduct();
                        }

                        if (this.SelectedProductionDetail.IsHeaderProduct.IsTrue())
                        {
                            this.BatchNumber = this.SelectedOrder?.BatchNumber;
                        }
                    }

                    if (this.SelectedProductionDetail != null && this.SelectedProductionDetail.UseAllBatch.IsNullOrFalse())
                    {
                        this.SetTypicalWeight();
                    }

                    if (!string.IsNullOrWhiteSpace(value.Notes))
                    {
                        var localNotes = value.Notes;
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                        {
                            NouvemMessageBox.Show(localNotes,isPopUp:true);
                        }));
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected order.
        /// </summary>
        public ProductionData SelectedOrder
        {
            get
            {
                return this.selectedOrder;
            }

            set
            {
                this.selectedOrder = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard)
                    {
                        this.Spec = value.Spec;
                    }
                    else
                    {
                        this.Spec = value.Reference;
                    }

                    if (value.ProductionDetails != null)
                    {
                        this.ProductionDetails = new ObservableCollection<SaleDetail>(value.ProductionDetails.OrderBy(x => x.OrderIndexAll));
                    }
                }
                else
                {
                    this.Spec = string.Empty;
                    this.ProductionDetails.Clear();
                    this.Mix = string.Empty;
                    this.AttributeReference = string.Empty;
                    this.ClearAttributeControls();
                    this.ClearAttributes();
                }
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Handle the carcass split selection.
        /// </summary>
        /// <param name="isHind">Is it a hind selection flag.</param>
        public void CarcassSplitProductSelected(bool isHind, bool fullCarcass)
        {
            if (fullCarcass)
            {
                this.HandleCarcassSplitSelection(this.FullCarcassProduct);
                return;
            }

            if (isHind)
            {
                this.Log.LogDebug(this.GetType(), "3. Hind selected");
                this.HandleCarcassSplitSelection(this.HindProduct);
                return;
            }

            this.Log.LogDebug(this.GetType(), "3. Fore selected");
            this.HandleCarcassSplitSelection(this.ForeProduct);
        }

        /// <summary>
        /// Adds the incoming dispatch product.
        /// </summary>
        /// <param name="detail">The incoming sale detail.</param>
        public void SetProductDetail(SaleDetail detail)
        {
            if (this.ProductionDetails == null)
            {
                this.ProductionDetails = new ObservableCollection<SaleDetail>();
            }

            if (this.ProductionDetails.Any())
            {
                var currentProduct = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                if (currentProduct != null)
                {
                    //try
                    //{
                    //    var localWgt = detail.StockDetailToProcess.TransactionWeight;
                    //    var localQty = detail.StockDetailToProcess.TransactionQty;
                    //    currentProduct.WeightIntoProduction = currentProduct.WeightIntoProduction.ToDouble() + localWgt.ToDouble();
                    //    currentProduct.QuantityIntoProduction = currentProduct.QuantityIntoProduction.ToDouble() + localQty.ToDouble();
                    //}
                    //catch (Exception ex)
                    //{
                    //    this.Log.LogDebug(this.GetType(), ex.Message);
                    //}

                    return;
                }
            }

            this.ProductionDetails.Add(detail);
            this.SelectedProductionDetail = detail;
        }
      
        /// <summary>
        /// Refresh the order on transaction deletion.
        /// </summary>
        /// <param name="stock">The deleted transaction.</param>
        public void RefreshOrder(StockTransactionData stock)
        {
            if (this.ProductionDetails == null || !this.ProductionDetails.Any())
            {
                return;
            }

            var inmasterid = stock.INMasterId;
            var localProduct = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == inmasterid);
            if (localProduct != null)
            {
                localProduct.WeightIntoProduction -= stock.Transaction.TransactionWeight.ToDecimal();
                localProduct.QuantityIntoProduction -= stock.Transaction.TransactionQTY.ToDecimal();
            }
        }

        /// <summary>
        /// Adds serial stock to a production.
        /// </summary>
        public void RecordSerialProduction()
        {
            #region validation

            if (this.SelectedProductionDetail == null)
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);
                return;
            }

            #endregion

            var localOrder = this.SelectedOrder.Order;
            if (this.Locator.TouchscreenProductionOrders.SelectedOrder != null && this.SelectedOrder.Order.PROrderID !=
                this.Locator.TouchscreenProductionOrders.SelectedOrder.Order.PROrderID)
            {
                this.Log.LogError(this.GetType(), string.Format("RecordSerialProduction():Switching from {0} to {1}", localOrder.PROrderID, this.Locator.TouchscreenProductionOrders.SelectedOrder.Order.PROrderID));
                localOrder = this.Locator.TouchscreenProductionOrders.SelectedOrder.Order;
            }

            var weight =
                this.SelectedProductionDetail.StockDetailToProcess.TransactionWeight;

            var qty =
                this.SelectedProductionDetail.StockDetailToProcess.TransactionQty;

            var inMasterId = this.SelectedProductionDetail.StockDetailToProcess.INMasterID;
            var warehouseId = this.SelectedProductionDetail.StockDetailToProcess.WarehouseID;
            var processId = this.SelectedProductionDetail.StockDetailToProcess.ProcessID;

            #region macro validation

            try
            {
                if (!this.ValidateData(this.traceabilityDataTransactions, this.SelectedOrder.Order.PROrderID,
                    weight.ToDecimal(), qty.ToDecimal(),
                    inMasterId, warehouseId, processId.ToInt(), Constant.Batch, this.ReferenceValue))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion
            
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Carcass ||
                ApplicationSettings.IntoProductionMode == IntoProductionMode.CarcassNoReweigh)
            {
                // final check before commit (2 very quick scans could bypass the original check)
                var localSerial = this.SelectedProductionDetail.GoodsReceiptDatas.First()
                    .TransactionData.First()
                    .Transaction.Serial;
                var localTrans = this.DataManager.GetIntoProductionTransaction(localSerial.ToInt());
                if (localTrans != null)
                {
                    return;
                }
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
            {
                this.SelectedProductionDetail.StockDetailToProcess.MixCount = this.MixCount;
            }

            if (this.SelectedProductionDetail.CompleteAutoBoxing)
            {
                qty = 0;
            }
            else if (this.AutoBoxing)
            {
                weight = 0;
                qty = 0;
            }

            this.SelectedProductionDetail.AutoBoxing = !this.SelectedProductionDetail.CompleteAutoBoxing && this.AutoBoxing;
            this.SelectedProductionDetail.SaleID = localOrder.PROrderID;
            this.SelectedProductionDetail.WeightIntoProduction = this.SelectedProductionDetail.WeightIntoProduction.ToDecimal() + weight.ToDecimal();
            this.SelectedProductionDetail.QuantityIntoProduction = this.SelectedProductionDetail.QuantityIntoProduction.ToDecimal() + qty.ToDecimal();

            if (this.DataManager.AddStockToProduction(this.SelectedProductionDetail))
            {
                // Refresh the order, to get updated data.
                Application.Current.Dispatcher.BeginInvoke(
                           DispatcherPriority.Background,
                              new Action(() =>
                              {
                                  if (ApplicationSettings.IntoProductionMode != IntoProductionMode.IntoProductionSelectProduct)
                                  {
                                      Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
                                  }
                                  else
                                  {
                                      this.serial = null;
                                      this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                                  }

                                  if (ApplicationSettings.IntoProductionMode != IntoProductionMode.Butchery
                                      && ApplicationSettings.IntoProductionMode != IntoProductionMode.Injection
                                      && ApplicationSettings.IntoProductionMode != IntoProductionMode.Recipe)
                                  {
                                      this.pieces = 1;
                                      this.qty = 1;
                                  }

                                  if (ApplicationSettings.IntoProductionMode == IntoProductionMode.RecallOrder)
                                  {
                                      this.SelectedOrder = null;
                                      if (this.ProductionDetails != null)
                                      {
                                          this.ProductionDetails.Clear();
                                      }

                                      this.Spec = Message.SelectProductionOrder;
                                  }

                                  if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit
                                      || ApplicationSettings.IntoProductionMode == IntoProductionMode.Carcass
                                      || ApplicationSettings.IntoProductionMode == IntoProductionMode.CarcassNoReweigh)
                                  {
                                      this.SelectedProductionDetail = null;
                                  }
                              }
                          ));

                try
                {
                    if (this.NonStandardResponses.Any() || this.NonStandardBatchResponses.Any())
                    {
                        foreach (var response in this.NonStandardResponses)
                        {
                            this.LogAlerts(response, prOrderID: this.SelectedOrder.Order.PROrderID, processId: this.Locator.ProcessSelection.SelectedProcess.ProcessID);
                        }

                        foreach (var response in this.NonStandardBatchResponses)
                        {
                            this.LogAlerts(response, prOrderID: this.SelectedOrder.Order.PROrderID, processId: this.Locator.ProcessSelection.SelectedProcess.ProcessID);
                        }
                    }

                    this.NonStandardResponses.Clear();

                    var localWgt = weight.ToDecimal();
                    var localProduct = string.Empty;

                    var isReceipe = this.SelectedProductionDetail.IsHeaderProduct == true;
                    var orderComplete = this.mixCount >= this.mixesRequired;
                    try
                    {
                        if (!this.ValidateData(
                            this.traceabilityDataPostTransactions,
                            this.SelectedProductionDetail.StockDetailToProcess.StockTransactionID.ToInt(), 0, 0, 0, 0, 0, Constant.Batch))
                        {
                            return;
                        };
                    }
                    catch (Exception e)
                    {
                        this.Log.LogError(this.GetType(), e.Message);
                    }

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe && isReceipe)
                    {
                        this.Print(this.SelectedProductionDetail.StockDetailToProcess.StockTransactionID.ToInt(),
                            LabelType.Item, null,
                            this.SelectedProductionDetail.StockDetailToProcess.INMasterID,
                            this.SelectedProductionDetail.StockDetailToProcess.INGroupID);
                        this.SelectedOrder.MixReadyForCompletion = true;

                        if (!this.SelectedOrder.MultiReceipts && this.SelectedOrder.AutoCompleteRecipeMix)
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                            {
                                this.CompleteMixCommandExecute();
                            }));
                        }
                    }

                    if (this.SelectedProductionDetail != null)
                    {
                        if (this.SelectedProductionDetail.InventoryItem != null)
                        {
                            localProduct = this.SelectedProductionDetail.InventoryItem.Name;
                        }
                        else if (this.SelectedProductionDetail.StockDetailToProcess != null)
                        {
                            var prod = NouvemGlobal.InventoryItems.FirstOrDefault(x =>
                                x.Master.INMasterID == this.SelectedProductionDetail.StockDetailToProcess.INMasterID);
                            if (prod != null)
                            {
                                localProduct = prod.Name;
                            }
                        }
                    }

                    var processName = Strings.ProductionLwr;
                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
                    {
                        processName = Strings.Butchery;
                        this.PrintBarcode();
                    }
                    else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
                    {
                        processName = Strings.Injection;
                        this.PrintBarcode();
                    }

                    var message = string.Format(Message.ItemAddedToProduction, localWgt, localProduct, processName);

                    if (!ApplicationSettings.ScannerMode && !orderComplete)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                        }));
                    }

                    SystemMessage.Write(MessageType.Priority, message);

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery ||
                        ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
                    {
                        this.Locator.Touchscreen.Quantity = 0;
                        this.AttributeReference = string.Empty;
                        this.IntakeId = null;
                        this.BPMasterId = null;
                        this.BatchIdBase = null;
                    }
                    else
                    {
                        this.Locator.Touchscreen.Quantity = 1;
                    }

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery
                        || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                        || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                        || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.DataManager.UpdateBatchWeight(localOrder.PROrderID);
                        }));
                    }
                }
                finally
                {
                    this.CheckAttributeTransactionReset(this.SelectedProductionDetail);

                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                    {
                        this.SetNeedleValue();
                        if (this.SelectedProductionDetail != null)
                        {
                            if (this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse()
                            && !this.SelectedProductionDetail.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush))
                            {
                                var localAmt = this.SelectedProductionDetail.StockMode == StockMode.BatchQty
                                    ? this.qty.ToDecimal()
                                    : this.indicatorWeight;
                                this.SelectedProductionDetail.BatchStockRemaining =
                                    this.SelectedProductionDetail.BatchStockRemaining.ToDecimal() - localAmt;
                            }

                            this.Locator.Indicator.SetNeedleValues(
                                this.selectedProductionDetail.WeightOrdered.ToDecimal(),
                                this.selectedProductionDetail.WeightIntoProduction.ToDecimal(),
                                this.selectedProductionDetail.PlusTolerance.ToDecimal(),
                                this.selectedProductionDetail.MinusTolerance.ToDecimal(),
                                this.selectedProductionDetail.NouUOMID.ToInt(),
                                this.SelectedProductionDetail.InventoryItem.Master.Name);
                        }
                    }

                    this.CheckAutoBoxing();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.ItemNotAddedToProduction);
            }
        }

        private void SetNeedleValue()
        {
            if (ApplicationSettings.IntoProductionMode != IntoProductionMode.Recipe)
            {
                return;
            }

            this.Locator.Touchscreen.Quantity = 0;
            this.SetOrderLinesStatus();

            if (this.SelectedProductionDetail != null
                && !this.Scanning
                && this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse())
            {
                var produced = this.SelectedProductionDetail.NouOrderMethod == Strings.Quantity
                    ? this.SelectedProductionDetail.QuantityIntoProduction
                    : this.SelectedProductionDetail.WeightIntoProduction;
                this.Locator.Indicator.SetIssuedValue(produced.ToDecimal());

                if (this.SelectedProductionDetail.IsFilled && this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse())
                {
                    //this.SelectedProductionDetail = null;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.RecipeLineFilled, flashMessage: true);

                    }));

                    this.MainViewModel = this.Locator.IntoProductionMainGrid;
                }
                else
                {
                    this.SetRecipeBatch(true);
                }
            }
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        /// <param name="dataResults">The incoming traceability data.</param>
        private void RecordSerialProductionComplete(Tuple<string, List<TraceabilityResult>> dataResults)
        {
            this.Log.LogDebug(this.GetType(), "RecordSerialProductionComplete(): Complete processing started");
            var error = dataResults.Item1;
            var data = dataResults.Item2;

            #region validation

            if (error != string.Empty)
            {
                // value hasn't been selected/entered by the user, so exit here.
                this.Log.LogDebug(this.GetType(), "Missing value. Not selected by user");
                SystemMessage.Write(MessageType.Issue, Message.TraceabilityValuesNotSet);
                NouvemMessageBox.Show(Message.TraceabilityValuesNotSet, touchScreen: true);
                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }

            var transData = new List<StockTransactionData>
            {
                new StockTransactionData
                {
                    Transaction = new StockTransaction
                    {
                        MasterTableID = this.SelectedOrder.Order.PROrderID,
                        TransactionQTY = this.qty < 1 ? 1 : this.qty.ToDecimal(),
                        TransactionWeight = this.indicatorWeight,
                        Tare = this.tare,
                        GrossWeight = this.indicatorWeight + this.tare,
                        Pieces = this.pieces < 1 ? 1 : this.pieces,
                        BatchNumberID = this.SelectedProductionDetail.GoodsReceiptDatas.First().BatchNumber.BatchNumberID,
                        TransactionDate = DateTime.Now,
                        NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId,
                        WarehouseID = NouvemGlobal.ProductionId,
                        INMasterID = this.SelectedProductionDetail.INMasterID,
                        ManualWeight = this.manualWeight,
                        DeviceMasterID = NouvemGlobal.DeviceId,
                        UserMasterID = NouvemGlobal.UserId
                    }
                }
            };

            this.SelectedProductionDetail.GoodsReceiptDatas.First().TransactionData = transData;

            if (this.SelectedProductionDetail.GoodsReceiptDatas == null)
            {
                this.Log.LogDebug(this.GetType(), string.Format("Creating goods receipt data list for product:{0}", this.SelectedProductionDetail.INMasterID));
                this.SelectedProductionDetail.GoodsReceiptDatas = new List<GoodsReceiptData>();
            }

            var localGoodsReceiptData = this.SelectedProductionDetail.GoodsReceiptDatas.FirstOrDefault();

            if (localGoodsReceiptData == null)
            {
                this.Log.LogError(this.GetType(), string.Format("Product detail id:{0} has no goodsreceipt data", this.SelectedProductionDetail.SaleDetailID));
                return;
            }

            var transactions = data.Where(x => !x.BatchAttribute).ToList();
            if (transactions.Any())
            {
                var transction = localGoodsReceiptData.TransactionData.First().Transaction;
                localGoodsReceiptData.TransactionData = new List<StockTransactionData> { this.ProcessIntoProductionTransactions(transactions, transction) };
            }

            var batchTraceabilityData = data.Where(x => x.BatchAttribute).ToList();
            if (batchTraceabilityData.Any())
            {
                localGoodsReceiptData.BatchTraceabilities = this.ProcessBatchTraceabilities(batchTraceabilityData);
            }

            var wgt = this.SelectedProductionDetail.GoodsReceiptDatas.First().TransactionData.First().Weight;
            var qty = this.SelectedProductionDetail.GoodsReceiptDatas.First().TransactionData.First().Quantity;

            this.Log.LogDebug(this.GetType(), string.Format("Current Wgt:{0}. Adding weight:{1}, Current Qty:{2}. Adding qty:{3}",
                this.SelectedProductionDetail.WeightDelivered, wgt, this.SelectedProductionDetail.QuantityDelivered, qty));

            this.SelectedProductionDetail.WeightDelivered = this.SelectedProductionDetail.WeightDelivered.ToDecimal() + wgt.ToDecimal();
            this.SelectedProductionDetail.QuantityDelivered = this.SelectedProductionDetail.QuantityDelivered.ToDecimal() + qty.ToDecimal();

            this.RecordSerialProduction();
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to record a quantity.
        /// </summary>
        public ICommand RecordRecipeQuantityCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove a product.
        /// </summary>
        public ICommand CompleteMixCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove a product.
        /// </summary>
        public ICommand PrintPorkLabelsCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove a product.
        /// </summary>
        public ICommand RecordBatchWeightCommand { get; private set; }

        ///// <summary>
        ///// Gets the command to set the multiple stock labels/transaction.
        ///// </summary>
        //public ICommand SetStockLabelsToPrintCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove a product.
        /// </summary>
        public ICommand RemoveProductCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a scanner move back to main ui request.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a scanner manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unloaded event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles a batch selection.
        /// </summary>
        /// <param name="batchNo"></param>
        protected void HandleNewBatch(BatchNumber batchNo)
        {
            this.BatchNumber = batchNo;

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
            {
                if (this.SelectedOrder != null && this.SelectedProductionDetail != null 
                                               && this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse()
                                               && !this.SelectedProductionDetail.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush))
                {
                    this.SelectedProductionDetail.Batch = this.BatchNumber;
                    this.SelectedProductionDetail.BatchStockRemaining = this.recipeBatch?.BatchStockRemaining;
                }
            }
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {

#if DEBUG

            // data = "(20)FQ(21)18149";
#endif

            try
            {
                #region validation

                if (this.Scanning)
                {
                    return;
                }

                this.Scanning = true;

                if (string.IsNullOrEmpty(data))
                {
                    SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.InvalidScannerRead, scanner: true);
                    }

                    return;
                }

                if (ViewModelLocator.IsProductionOrdersLoaded())
                {
                    Messenger.Default.Send(data.Trim(), Token.ProductionOrderSearch);
                    return;
                }

                if (ViewModelLocator.IsTransactionManagerLoaded())
                {
                    Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                    return;
                }

                if (this.SelectedOrder == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.NoProductionBatchSelected, scanner: true);
                    }

                    return;
                }

                if (ApplicationSettings.ScanningForScotBeefBarcodeReadAtDispatch)
                {
                    // woolleys - check for a scotbeef barcode
                    if (this.IsScotBeefBarcode(data))
                    {
                        this.Log.LogDebug(this.GetType(), "Yes");

                        // get the associated serial number.
                        data = this.DataManager.GetCarcassTransaction(data);
                    }
                }

                if (this.AutoBoxing && this.ScannerStockMode == ScannerMode.Scan)
                {
                    if (this.AutoBoxLabels.Contains(data.ToInt()))
                    {
                        NouvemMessageBox.Show($"{data} has already been scanned", touchScreen: true);
                        return;
                    }
                }

                #endregion

                this.ReferenceValue = data;
                this.Log.LogInfo(this.GetType(), string.Format("HandleScannerData:{0}", data));
                data = data.RemoveEndCharacterIfMatch(ApplicationSettings.KeyboardWedgeTerminator);

                #region minus scan

                if (this.ScannerStockMode == ScannerMode.MinusScan)
                {
                    this.DeleteTransaction(data.RemoveNonIntegers().ToInt(), data);
                    return;
                }

                #endregion

                #region reprint

                if (this.ScannerStockMode == ScannerMode.Reprint)
                {
                    var transaction = this.DataManager.GetStockTransaction(data.ToInt());
                    
                    if (transaction != null)
                    {
                        try
                        {
                            this.PrintManager.ReprintLabel(transaction.StockTransactionID);
                        }
                        catch (Exception ex)
                        {
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }

                    return;
                }

                #endregion

                //#region rework

                //if (this.ScannerStockMode == ScannerMode.ReWork)
                //{
                //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                //    {
                //        this.ReworkRecipeStock(data);
                //    }));
        
                //    return;
                //}

                //#endregion

                #region toppings

                if (this.ScannerStockMode == ScannerMode.ReWeigh)
                {
                    this.HandleRecipeReweigh(data);
                    return;
                }

                #endregion

                #region eligibility

                var notAllowedMessage = this.CheckBatchEligibility(data);
                if (notAllowedMessage != string.Empty)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        SystemMessage.Write(MessageType.Issue, notAllowedMessage);
                        NouvemMessageBox.Show(notAllowedMessage, touchScreen: true);
                    }));

                    return;
                }

                #endregion

                #region process

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch)
                {
                    this.HandleScannerDataStandard(data);
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing)
                {
                    this.HandleScannerDataSlicingJointing(data);
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
                {
                    this.HandleScannerDataButchery(data);
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
                {
                    this.HandleScannerDataInjection(data);
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
                {
                    this.HandleScannerDataCarcassSplit(data);
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Carcass
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.CarcassNoReweigh)
                {
                    this.HandleScannerDataCarcass(data);
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.SelectAllProduct)
                {
                    this.HandleScannerDataCarcassProductSelect(data.Trim());
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionConformance)
                {
                    this.HandleScannerDataConformance(data);
                }

                #endregion
            }
            finally
            {
                this.Scanning = false;
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void DeleteTransaction(int stockId, string barcode = "")
        {
            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.DeleteLabelNoProductionBatch);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.DeleteLabelNoProductionBatch, flashMessage:true);
                }));

                return;
            }

            var localId = stockId > 0 ? stockId.ToString() : barcode;

            this.Log.LogInfo(this.GetType(), $"Attempting to delete into prod label:{stockId}");
            var error = this.DataManager.DeleteProductionStock(localId, this.SelectedOrder.Order.PROrderID, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
            if (!string.IsNullOrEmpty(error))
            {
                this.Log.LogError(this.GetType(), $"Label deletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
            else
            {
                this.Log.LogInfo(this.GetType(), "Label deleted");
                SystemMessage.Write(MessageType.Priority, Message.StockRemoved);
                this.RefreshOrderDetails();
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void UndeleteTransaction(int stockId)
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to undelete into prod label:{stockId}");
            var error = this.DataManager.UndeleteStock(stockId, NouvemGlobal.TransactionTypeProductionIssueId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
            if (!string.IsNullOrEmpty(error))
            {
                this.Log.LogError(this.GetType(), $"Label undeletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
            else
            {
                this.Log.LogInfo(this.GetType(), "Label undeleted");
                SystemMessage.Write(MessageType.Priority, Message.StockUndeleted);
                this.RefreshOrderDetails();
            }
        }

        /// <summary>
        /// Show the batch report.
        /// </summary>
        protected override void ShowReportCommandExecute()
        {
            if (!this.IsFormLoaded || this.SelectedOrder == null)
            {
                return;
            }

            var reportData = new Sale
            {
                Reference = this.SelectedOrder.Order.PROrderID.ToString()
            };

            Messenger.Default.Send(reportData, Token.DisplayTouchscreenBatchReport);
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int GetWorkflowDocketId()
        {
            return this.SelectedOrder != null && this.SelectedOrder.Order != null ? this.SelectedOrder.Order.PROrderID : 0;
        }

        /// <summary>
        /// Gets the current attribute product id.
        /// </summary>
        /// <returns>THe corresponding product id.</returns>
        protected override int GetWorkflowProductId()
        {
            return this.SelectedProductionDetail != null ? this.SelectedProductionDetail.INMasterID : 0;
        }

        /// <summary>
        /// Saves the batch attributes.
        /// </summary>
        protected override void SaveBatchAttributes(int? parentId = null, bool ignoreChecks = false)
        {
            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (!ignoreChecks)
            {
                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);
                    return;
                }
            }

            #endregion

            this.UnsavedAttributes = false;
            var attributeId = this.SaveBatchData(this.SelectedOrder.Order.BatchNumberID);
            if (attributeId > 0)
            {
                base.SaveBatchAttributes(attributeId);
                SystemMessage.Write(MessageType.Priority, Message.BatchAttributesSaved);
                if (!ignoreChecks)
                {
                    NouvemMessageBox.Show(Message.BatchAttributesSaved, touchScreen: true, flashMessage: true);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchAttributesNotSaved);
            }
        }

        /// <summary>
        /// Sets the process flag (visibility/edit)
        /// </summary>
        /// <param name="data">The attribute allocation data.</param>
        protected override Tuple<bool, bool> SetProcessData(AttributeAllocationData data)
        {
            var visible = false;
            var editable = false;
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Butchery));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Butchery));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionBatch));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionBatch));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Injection));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Injection));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.IntoSlicing));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.IntoSlicing));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.IntoJointing));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.IntoJointing));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionStandard));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionStandard));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionCarcassSplit));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionCarcassSplit));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionSelectProduct));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionSelectProduct));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Recipe));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Recipe));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionConformance)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionConformance));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntoProductionConformance));
            }

            return Tuple.Create(visible, editable);
        }

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (ApplicationSettings.ScannerMode)
            {
                if (this.ScannerStockMode == ScannerMode.Scan)
                {
                    this.ScannerStockMode = ScannerMode.MinusScan;
                }
                else
                {
                    this.ScannerStockMode = ScannerMode.Scan;
                }

                return;
            }

            if (ApplicationSettings.CreatingTouchscreenRecipes)
            {
                if (this.ScannerStockMode == ScannerMode.Scan)
                {
                    this.ScannerStockMode = ScannerMode.MinusScan;
                }
                else if (this.ScannerStockMode == ScannerMode.MinusScan)
                {
                    this.ScannerStockMode = ScannerMode.Reprint;
                }
                else if (this.ScannerStockMode == ScannerMode.Reprint)
                {
                    this.ScannerStockMode = ScannerMode.ReWeigh;
                }
                else if (this.ScannerStockMode == ScannerMode.ReWeigh)
                {
                    this.ScannerStockMode = ScannerMode.Scan;
                }

                return;
            }

            base.ScannerModeHandler();
        }

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleStockLabels(string data)
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);
                return;
            }

            #endregion

            this.StockLabelsToPrint = data.ToInt();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Completes a mix.
        /// </summary>
        private void CompleteMixCommandExecute()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }

            var mixNotes = this.SelectedOrder.Order.MixNote;

            try
            {
                if (this.SelectedOrder.MixReadyForCompletion)
                {
                    if (!this.SelectedOrder.AutoCompleteRecipeMix)
                    {
                        NouvemMessageBox.Show(string.Format(Message.CompleteMixPrompt, this.Mix), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }

                    if (this.SelectedOrder.MultiReceipts)
                    {
                        NouvemMessageBox.Show(Message.CompleteBatchVerification, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }

                    this.FinaliseMix();
                    return;
                }

                try
                {
                    if (!this.ValidateData(this.traceabilityDataCompletion, this.SelectedOrder.Order.PROrderID,
                        0, 0, 0, 0, 0, Constant.Batch, this.ReferenceValue))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                #region filled batch validation

                var notFilledProducts = this.ProductionDetails.Where(x => !x.IsFilled && !x.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush)).ToList();
                if (notFilledProducts.Any())
                {
                    if (notFilledProducts.Count == 1 && notFilledProducts.First().IgnoreTolerances.IsTrue())
                    {
                        // line fill checks not required on this product, but ask the user.
                        NouvemMessageBox.Show(string.Format(Message.RecipeBatchNotFilledByIgnoreToleranceProduct,notFilledProducts.First().InventoryItem.Name),
                            NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }
                    else
                    {
                        notFilledProducts = notFilledProducts.Where(x => x.IgnoreTolerances.IsNullOrFalse()).ToList();
                        if (notFilledProducts.Any())
                        {
                            var msg = string.Empty;
                            foreach (var notFilledProduct in notFilledProducts)
                            {
                                msg += notFilledProduct.InventoryItem.Name + Environment.NewLine;
                            }

                            var localMsg = $"{Message.RecipeBatchNotFilled}{Environment.NewLine}{msg}";
                            SystemMessage.Write(MessageType.Issue, localMsg);
                            NouvemMessageBox.Show(localMsg, touchScreen: true);
                            return;
                        }
                    }
                }

                #endregion

                NouvemMessageBox.Show(string.Format(Message.FinaliseMixPrompt, this.Mix), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    this.HandleProductionOrder(this.SelectedOrder, true);
                    return;
                }

                var backflushItems = this.SelectedOrder.ProductionDetails
                    .Where(x => x.NouIssueMethodName.CompareIgnoringCaseAndWhitespace(Constant.BackFlush) && !x.IsFilled).ToList();

                if (backflushItems.Any())
                {
                    this.IsRecipeBusy = true;
                }

                Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(200);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            if (!this.IssueBackFlush())
                            {
                                return;
                            }

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                System.Threading.Thread.Sleep(100);
                                this.manualWeight = true;
                                this.ProductionDetails.Clear();
                                this.ProductionDetails.Add(this.receipeProduct);
                                this.SelectedProductionDetail = this.ProductionDetails.First();
                                this.SelectedProductionDetail.IsHeaderProduct = true;
                                var totalMixWgt = this.SelectedProductionDetail.WeightOrdered.ToDecimal();
                                var totalMixQty = this.SelectedProductionDetail.QuantityOrdered.ToDecimal();

                                if (this.SelectedOrder.MultiReceipts)
                                {
                                    NouvemMessageBox.Show(Message.CreateRecipePalletLabelsPrompt, touchScreen: true);
                                    this.SelectedOrder.MixReadyForCompletion = true;
                                    return;
                                }

                                if (this.SelectedOrder.AutoCompleteRecipeMix)
                                {
                                    this.qty = totalMixQty;
                                    if(!this.TypicalWeight.IsNullOrZero())
                                    {
                                        this.SetTypicalWeight();
                                        totalMixWgt = this.Locator.Indicator.Weight;
                                        this.Log.LogInfo(this.GetType(),$"Auto Complete...Typical wgt set:{totalMixWgt}");
                                    }
                      
                                    this.indicatorWeight = totalMixWgt;
                                    this.RecordWeight();
                                }
                                else
                                {
                                    NouvemMessageBox.Show(Message.WeighFinishedProduct, touchScreen: true);
                                    if (this.SelectedProductionDetail == null)
                                    {
                                        this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault();
                                    }
                                }
                            }));
                        }
                        finally
                        {
                            if (!string.IsNullOrWhiteSpace(mixNotes))
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                                {
                                    NouvemMessageBox.Show(mixNotes, isPopUp: true);
                                }));
                            }
                        }
                       
                    });
                });
            }
            finally
            {
                
            }
        }

        /// <summary>
        /// Issues the backflush items to the receipe.
        /// </summary>
        private bool IssueBackFlush()
        {
            var backflushItems = this.SelectedOrder.ProductionDetails
                .Where(x => x.NouIssueMethodName.CompareIgnoringCaseAndWhitespace(Constant.BackFlush) && !x.IsFilled).ToList();

            try
            {
                string noBatchProduct = string.Empty;
                foreach (var detail in backflushItems)
                {
                    this.SelectedProductionDetail =
                        this.productionDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                    if (this.SelectedProductionDetail != null)
                    {
                        if (this.SelectedProductionDetail.BatchNumberId.IsNullOrZero())
                        {
                            noBatchProduct = this.selectedProductionDetail.InventoryItem.Name;
                            break;
                        }
                    }
                }

                if (noBatchProduct != string.Empty)
                {
                    NouvemMessageBox.Show($"There is no open batch in the system for {noBatchProduct}. You cannot proceed with this production.", touchScreen: true);
                    return false;
                }

                foreach (var detail in backflushItems)
                {
                    if (detail.BatchStockRemaining.ToDecimal() -
                        detail.WeightOrdered.ToDecimal() < 0)
                    {
                        noBatchProduct = detail.InventoryItem.Name;
                        this.BatchNumber = detail.Batch;
                        this.SelectedProductionDetail = detail;
                        detail.BackflushBatchDataRetrieved = false;
                        NouvemMessageBox.Show($"There is not enough produce in batch {detail.BatchNo} for {noBatchProduct}.You must complete this batch off before you can continue.", touchScreen: true);
                        return false;
                    }
                }

                if (noBatchProduct != string.Empty)
                {
                    NouvemMessageBox.Show($"There is no open batch in the system for {noBatchProduct}. You cannot proceed with this production.", touchScreen: true);
                    return false;
                }

                foreach (var detail in backflushItems)
                {
                    this.manualWeight = true;
                    this.SelectedProductionDetail =
                        this.productionDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                    if (this.SelectedProductionDetail != null)
                    {
                        detail.BackflushBatchDataRetrieved = false;
                        this.BatchNumber = detail.Batch;
                        this.qty = this.SelectedProductionDetail.QuantityOrdered.ToDecimal();
                        this.indicatorWeight = this.SelectedProductionDetail.WeightOrdered.ToDecimal();

                        var processId = this.Locator.ProcessSelection.SelectedProcess != null
                            ? this.Locator.ProcessSelection.SelectedProcess.ProcessID
                            : (int?)null;
                        if (this.DataManager.IssueBackflush(this.SelectedOrder.Order.PROrderID,
                            this.BatchNumber.BatchNumberID, this.qty, this.indicatorWeight,
                            NouvemGlobal.ProductionId, this.SelectedProductionDetail.INMasterID,
                            processId,
                            NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(), this.MixCount))
                        {
                            this.SelectedProductionDetail.WeightIntoProduction = this.SelectedProductionDetail.WeightIntoProduction.ToDecimal() + this.indicatorWeight.ToDecimal();
                            this.SelectedProductionDetail.QuantityIntoProduction = this.SelectedProductionDetail.QuantityIntoProduction.ToDecimal() + this.qty.ToDecimal();
                        }
                    }
                }
            }
            finally
            {
                this.SetOrderLinesStatus();
                this.manualWeight = false;
                this.IsRecipeBusy = false;
            }

            return true;
        }

        private void PrintPorkLabelsCommandExecute()
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                return;
            }

            #endregion

            try
            {
                if (!this.ValidateData(this.traceabilityDataLabel, this.SelectedOrder.Order.PROrderID,
                    0, 0, 0,
                    NouvemGlobal.ProductionId, this.Locator.ProcessSelection.SelectedProcess.ProcessID, Constant.Batch, this.ReferenceValue))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            var productId = NouvemGlobal.InventoryItems.First().Master.INMasterID;
            if (this.ProductionDetails != null && this.ProductionDetails.Any())
            {
                productId = this.ProductionDetails.Last().INMasterID;
            }

            // fetch the traceability values
            var stockTransaction = new StockTransaction();
            stockTransaction.TransactionWeight = 0;
            stockTransaction.GrossWeight = 0;
            stockTransaction.Tare = 0;
            stockTransaction.MasterTableID = this.SelectedOrder.Order.PROrderID;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = 0;
            stockTransaction.BatchNumberID = this.SelectedOrder.Order.BatchNumberID;
            stockTransaction.BatchID_Base = this.BatchIdBase;
            stockTransaction.INMasterID = productId;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
            stockTransaction.WarehouseID = NouvemGlobal.ProductionId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;
            stockTransaction.Deleted = DateTime.Now;
            stockTransaction.Consumed = DateTime.Now;
            stockTransaction.Reference = -1;
            stockTransaction.Comments = ApplicationSettings.IntoProductionMode.ToString();

            var id = this.DataManager.AddTransaction(stockTransaction);
            if (id > 0)
            {
                if (this.StockLabelsToPrint == 0)
                {
                    this.StockLabelsToPrint = 1;
                }

                for (int i = 0; i < this.StockLabelsToPrint; i++)
                {
                    this.Print(id, LabelType.Shipping, process: this.Locator.ProcessSelection.SelectedProcess, batchLabel:true);
                }

                SystemMessage.Write(MessageType.Priority, string.Format(Message.BarcodesPrinted, this.StockLabelsToPrint));
                this.StockLabelsToPrint = 1;
            }
        }

        /// <summary>
        /// Records a batch qty/wgt
        /// </summary>
        private void RecordBatchWeightCommandExecute()
        {
            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            #region attributes validation

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            this.CheckForUnsavedAttributes();

            #endregion

            var processName = ProcessType.IntoSlicing;
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing)
            {
                processName = ProcessType.IntoJointing;
            }

            this.IndicatorManager.OpenIndicatorPort();

            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.ProductionDetails == null || !this.ProductionDetails.Any())
            {
                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                if (this.ProductionDetails.Count == 1)
                {
                    this.SelectedProductionDetail = this.ProductionDetails.First();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBatchLineSelected);
                    NouvemMessageBox.Show(Message.NoBatchLineSelected, touchScreen: true);
                    return;
                }
            }


            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                if (this.Locator.Touchscreen.Quantity <= 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.QtyZeroStop);
                    NouvemMessageBox.Show(Message.QtyZeroStop, touchScreen: true);
                    return;
                }
            }

            #endregion

            if (this.Locator.Indicator.Weight > 0 && !this.Locator.Indicator.ManualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }
            else
            {
                this.indicatorWeight = this.Locator.Indicator.Weight;
            }

            int locationId;
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing)
            {
                locationId = NouvemGlobal.WarehouseIntoSlicingId;
            }
            else
            {
                locationId = NouvemGlobal.WarehouseIntoJointingId;
            }

            var processId = this.Locator.ProcessSelection.SelectedProcess != null
                ? this.Locator.ProcessSelection.SelectedProcess.ProcessID
                : (int?)null;

            #region macro validation

            try
            {
                if (!this.ValidateData(this.traceabilityDataTransactions, this.SelectedOrder.Order.PROrderID,
                    this.indicatorWeight, this.qty.ToDecimal(),
                    this.SelectedProductionDetail.INMasterID,
                    locationId, processId.ToInt(), Constant.Batch, this.ReferenceValue))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            var msg = string.Format(Message.MoveStockToProductionPrompt, this.qty, this.indicatorWeight,
               this.selectedProductionDetail.InventoryItem.Name, processName);
            NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, touchScreen: true);
            if (NouvemMessageBox.UserSelection == UserDialogue.No)
            {
                return;
            }

            var result = this.DataManager.MoveBatchStockIntoProduction(this.SelectedOrder.Order,
                this.SelectedProductionDetail.INMasterID, locationId, this.indicatorWeight, this.qty.ToInt(), processId);

            if (result)
            {
                var localQty = this.qty;
                this.SelectedProductionDetail.QuantityIntoProduction -= this.qty;
                this.selectedProductionDetail.WeightIntoProduction -= this.indicatorWeight.ToDecimal();

                var localProduct = string.Empty;
                if (this.SelectedProductionDetail != null)
                {
                    if (this.SelectedProductionDetail.InventoryItem != null)
                    {
                        localProduct = this.SelectedProductionDetail.InventoryItem.Name;
                    }

                    this.Locator.Touchscreen.Quantity = this.SelectedProductionDetail.QuantityIntoProduction.ToDecimal();
                }

                var message = string.Format(Message.ItemAddedToPorkProduction, localQty, localProduct, processName);

                if (!ApplicationSettings.ScannerMode)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }));
                }

                SystemMessage.Write(MessageType.Priority, message);

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
                {
                    this.Locator.Touchscreen.Quantity = 0;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataManager.UpdateBatchWeight(this.SelectedOrder.Order.PROrderID);
                    }));
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }
        }

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        private void RemoveProductCommandExecute()
        {
            if (this.SelectedOrder != null && this.SelectedOrder.Order != null)
            {
                if (this.SelectedProductionDetail != null)
                {
                    // If there are no transactions recorded against this product, remove.
                    if (this.SelectedProductionDetail.QuantityIntoProduction.ToDouble() > 0 || this.SelectedProductionDetail.WeightIntoProduction.ToDouble() > 0)
                    {
                        var authorised = this.AuthorisationsManager.AllowUserToDeleteLabel;
                        if (authorised)
                        {
                            var message = string.Format(Message.ViewBatchTransactions, this.SelectedOrder.Order.Reference);
                            NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                            {
                                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);
                                this.Locator.TransactionManager.SetTransactions(this.DataManager.GetTransactionsForIntoProductionLine(this.SelectedOrder.Order.PROrderID));
                            }
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                        }
                    }
                    else
                    {
                        if (ApplicationSettings.IntoProductionMode != IntoProductionMode.RecallOrder)
                        {
                            // Deleting product
                            if (this.ProductionDetails.Remove(this.SelectedProductionDetail))
                            {
                                SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                            }
                        }
                    }
                }
                else
                {
                    // no product selected. open the batch transactions
                    var authorised = this.AuthorisationsManager.AllowUserToDeleteLabel;
                    if (authorised)
                    {
                        var message = string.Format(Message.ViewBatchTransactions, this.SelectedOrder.Order.Reference);
                        NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);
                            this.Locator.TransactionManager.SetTransactions(this.DataManager.GetTransactionsForIntoProductionLine(this.SelectedOrder.Order.PROrderID));
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                    }
                }
            }
        }

        private void OnLoadedCommandExecute()
        {
            Messenger.Default.Send(false, Token.DisableLegacyAttributesView);
            this.IsBusy = false;
            this.IsFormLoaded = true;
            ApplicationSettings.UsingWorkflowForStandardAttributes = true;
            ViewModelLocator.ClearOutOfProduction();
            this.Locator.FactoryScreen.ModuleName = Strings.IntoProduction;
            this.ScannerStockMode = ScannerMode.Scan;
            this.OpenScanner();

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.RecallOrder)
            {
                this.IndicatorManager.SetMinimumWeight(ApplicationSettings.MinWeightAllowedIntoProduction);
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct)
            {
                var buttonContent = this.serial == null ? Strings.Wait : Strings.Record;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = buttonContent;
                }));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.SelectAllProduct)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Record;
                }));
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                Messenger.Default.Send(true, Token.EnableProductSelectionOnIntoProduction);
            }

            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
            this.SetUpModule();
            this.SelectedProductionDetail = this.SelectedProductionDetail;
        }

        private void OnUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
            this.AuthorisationsManager.AllowUserToEnterManualWeightOnTouchscreen = this.allowManualWeightEntry;
        }

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private void SwitchViewCommandExecute(string view)
        {
            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            if (view.Equals("Main"))
            {
                if (this.MainViewModel == this.Locator.IntoProductionMainGrid)
                {
                    this.MainViewModel = this.Locator.Needle;
                }
                else
                {
                    this.MainViewModel = this.Locator.IntoProductionMainGrid;
                }

                return;
            }

            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view.Equals(Constant.Order))
                    {
                        this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
                        this.Locator.TouchscreenProductionOrders.GetAllOrders();
                        this.Locator.TouchscreenProductionOrders.OnEntry();
                        if (!this.Locator.IntoProduction.ProductionOrdersScreenCreated)
                        {
                            this.Locator.IntoProduction.ProductionOrdersScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                            return;
                        }

                        Messenger.Default.Send(false, Token.CloseTouchscreenProductionOrders);
                    }

                    if (view.Equals(Constant.IntakeBatches))
                    {
                        if (this.SelectedProductionDetail == null)
                        {
                            SystemMessage.Write(MessageType.Issue, Message.NoProductLineSelected);
                            return;
                        }

                        if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch)
                        {
                            // serial products must be scanned
                            if (this.SelectedProductionDetail.NouStockMode != null && this.SelectedProductionDetail.NouStockMode.Name.CompareIgnoringCase("Serial"))
                            {
                                var msg = string.Format(Message.MustScanSerialStock,
                                    this.GetProductName(this.SelectedProductionDetail.INMasterID));
                                SystemMessage.Write(MessageType.Issue, msg);
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    NouvemMessageBox.Show(msg, touchScreen: true);
                                }));

                                return;
                            }
                        }

                        this.Locator.TouchscreenIntakeBatches.SetModule(ViewType.IntoProduction);
                        this.Locator.TouchscreenIntakeBatches.NouOrderMethod =
                            this.SelectedProductionDetail.NouOrderMethod;
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenIntakeBatches);
                        this.Locator.TouchscreenIntakeBatches.GetAllOrders(this.SelectedProductionDetail.INMasterID);

                        //if (!this.IntakeBatchesScreenCreated)
                        //{
                        //    this.IntakeBatchesScreenCreated = true;
                        //    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenIntakeBatches);
                        //    this.Locator.TouchscreenIntakeBatches.GetAllOrders(this.SelectedProductionDetail.INMasterID);
                        //    return;
                        //}

                        //this.Locator.TouchscreenIntakeBatches.OnEntry();
                        //this.Locator.TouchscreenIntakeBatches.GetAllOrders(this.SelectedProductionDetail.INMasterID);
                        //Messenger.Default.Send(false, Token.CloseTouchscreenIntakeBatches);
                    }

                    if (view.Equals(Constant.Products))
                    {
                        this.Locator.TouchscreenProducts.Module = ViewType.APReceipt;
                        if (ApplicationSettings.IntoProductionMode != IntoProductionMode.Butchery
                            && ApplicationSettings.IntoProductionMode != IntoProductionMode.Injection)
                        {
                            this.Locator.TouchscreenProducts.SetIntoProductionProductGroups(this.carcassSplitOptions);
                        }
                        else
                        {
                            this.Locator.TouchscreenProducts.Module = ViewType.IntoProduction;
                        }

                        this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenProducts;
                        return;
                    }

                    if (view.Equals(Constant.BatchSetUp))
                    {
                        this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ProductionBatchSetUp;
                        return;
                    }

                    if (view.Equals(Constant.ScannerBatchSetUp))
                    {
                        Messenger.Default.Send(Token.Message, Token.CreateScannerBatchSetUp);
                        return;
                    }
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the typical wgt.
        /// </summary>
        private void SetTypicalWeight()
        {
            this.Log.LogInfo(this.GetType(), $"SetTypicalWeight - MOde:{ApplicationSettings.IntoProductionMode}, Qty:{this.qty}, Typical Wgt:{this.TypicalWeight}");
            if (!this.TypicalWeight.IsNullOrZero())
            {
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                {
                    if (this.qty > 0)
                    {
                        this.IndicatorManager.CloseIndicatorPort();
                        var localWgt = this.TypicalWeight.ToDecimal();
                        var localQty = this.qty;

                        //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                       // {
                            ///System.Threading.Thread.Sleep(ApplicationSettings.AutoTypicalWeightSetWait);
                            this.Locator.Indicator.SetManualWeight(localWgt * localQty);
                            this.Log.LogInfo(this.GetType(), $"Setting Typical Weight - Mode:{ApplicationSettings.IntoProductionMode}, Qty:{this.qty}, Typical Wgt:{this.TypicalWeight}, Actual Wgt:{this.Locator.Indicator.Weight}");
                       // }));
                       
                        return;
                    }
                }

                //var localQty = this.qty == 0 ? 1 : this.qty;
                //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                //{
                //    this.Locator.Indicator.SetManualWeight(this.TypicalWeight.ToDecimal() * localQty);
                //}));

                //return;
            }

            this.Locator.Indicator.SetManualWeight(0);
        }

        /// <summary>
        /// Handles the slicing/jointing processes.
        /// </summary>
        /// <param name="data">The scanner data.</param>
        private void HandleScannerDataSlicingJointing(string data)
        {
            var localSerial = data.RemoveNonDecimals().ToInt();
            var trans = this.DataManager.GetStockTransactionIncDeleted(localSerial);
            this.Locator.TouchscreenProductionOrders.SetOrder(trans.MasterTableID.ToInt());
            this.Locator.TouchscreenProductionOrders.HandleBatchOrderPreDispatch();
        }

        /// <summary>
        /// Handles the butchery process.
        /// </summary>
        /// <param name="data">The scanner data.</param>
        private void HandleScannerDataButchery(string data)
        {
            var localSerial = data.RemoveNonDecimals().ToInt();
            var trans = this.DataManager.GetStockTransactionIncDeleted(localSerial);

            if (trans == null)
            {
                this.Log.LogDebug(this.GetType(), "Not found");
                return;
            }

            if (trans.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
            {
                // intake label
                var attribute = this.DataManager.GetAttributeByBatchId(localSerial);

                if (attribute != null)
                {
                    this.AttributeReference = this.GetAttributeValue("Lorry Code", attribute);
                    this.BatchIdBase = attribute.Generic1.ToInt();
                    this.IntakeId = attribute.Generic2.ToInt();
                    SystemMessage.Write(MessageType.Priority, Message.IntakeBatchFound);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.IntakeBatchNotFound);
                    NouvemMessageBox.Show(Message.IntakeBatchNotFound);
                }
            }
            else
            {
                var intoButcheryTransaction = this.DataManager.GetStockTransactionIncProduct(localSerial);
                if (intoButcheryTransaction != null)
                {
                    this.BatchIdBase = intoButcheryTransaction.BatchID_Base;
                    this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
                    this.Locator.TouchscreenProductionOrders.SetOrder(intoButcheryTransaction.MasterTableID.ToInt());
                    this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                }
            }
        }

        /// <summary>
        /// Handles the injection process.
        /// </summary>
        /// <param name="data">The scanner data.</param>
        private void HandleScannerDataInjection(string data)
        {
            int? basePROrderId = null;
            var localSerial = data.RemoveNonDecimals().ToInt();

            var trans = this.DataManager.GetStockTransactionIncDeleted(localSerial);

            if (trans != null && trans.Comments.CompareIgnoringCase(ProcessType.Butchery))
            {
                if (this.SelectedOrder == null)
                {
                    // scanning a butchery batch at injection..move to the batch creation screen.
                    this.Locator.TouchscreenProductionOrders.GetAllOrders();
                    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                    this.Locator.TouchscreenProductionOrders.CreateOrder();
                    return;
                }

                basePROrderId = trans.MasterTableID;
            }

            if (trans != null && trans.Comments.CompareIgnoringCase(ProcessType.Injection))
            {
                // scanning an injection batch at injection..move to the batch creation screen.
                this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
                this.Locator.TouchscreenProductionOrders.SetOrder(trans.MasterTableID.ToInt());
                this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                return;
            }

            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            #endregion

            var attribute = this.DataManager.GetAttributeByBaseBatchId(localSerial);

            if (attribute != null)
            {
                this.AttributeReference = this.GetAttributeValue("Lorry Code", attribute);
                this.BatchIdBase = attribute.Generic2.ToInt();
                this.serial = attribute.HoldingNumber;
                this.IntakeId = attribute.AgeInMonths;
                this.BPMasterId = attribute.SupplierID;

                if (basePROrderId.HasValue)
                {
                    var baseOrder = this.DataManager.GetPROrder(basePROrderId.ToInt());
                    if (baseOrder != null)
                    {
                        this.IntakeId = baseOrder.PROrderID_Base;
                        this.BPMasterId = baseOrder.BPMasterID;
                    }
                }

                SystemMessage.Write(MessageType.Priority, Message.IntakeBatchFound);

                var butcheryData = this.DataManager.GetButcheryTransactionData(localSerial);
                if (butcheryData != null)
                {
                    var localProduct = new SaleDetail
                    {
                        INMasterID = butcheryData.INMasterID,
                        StockDetailToProcess = butcheryData
                    };

                    this.SetProductDetail(localProduct);
                    var stockDetail = this.CreateTransaction();
                    stockDetail.TransactionWeight = butcheryData.TransactionWeight;
                    stockDetail.TransactionQty = butcheryData.TransactionQty;
                    this.CreateAttribute(stockDetail);
                    this.SelectedProductionDetail.StockDetails.Add(stockDetail);
                    this.SelectedProductionDetail.StockDetailToProcess = stockDetail;
                    this.SelectedProductionDetail.BasePROrderId = basePROrderId;
                    this.RecordSerialProduction();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.IntakeBatchNotFound);
                NouvemMessageBox.Show(Message.IntakeBatchNotFound, touchScreen: true);
            }
        }

        /// <summary>
        /// Handles the carcass split process.
        /// </summary>
        /// <param name="data">The scanner data.</param>
        private void HandleScannerDataCarcassSplit(string data)
        {
            if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                return;
            }

            if (this.DataManager.IsCarcassSplitAnimal(data.Trim()))
            {
                this.SetCarcassSplitData(data.Trim());
            }
            else
            {
                this.HandleScannerDataStandard(data);
            }
        }

        /// <summary>
        /// Handles the carcass process.
        /// </summary>
        /// <param name="data">The scanner data.</param>
        private void HandleScannerDataCarcass(string data)
        {
            if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                return;
            }

            this.HandleCarcass(data.Trim());
        }

        /// <summary>
        /// Completes the mix.
        /// </summary>
        private void FinaliseMix()
        {
            var localMixes = this.Mix;
            if (this.DataManager.CompleteMix(this.SelectedOrder))
            {
                this.Locator.TouchscreenProductionOrders.SetOrder(this.SelectedOrder.Order.PROrderID);
                this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                var orderComplete = this.mixCount > this.mixesRequired;
                if (orderComplete)
                {
                    var orderId = this.SelectedOrder.Order.PROrderID;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        var msg = Message.ReceipeComplete;
                        SystemMessage.Write(MessageType.Priority, msg);
                        NouvemMessageBox.Show(msg, touchScreen: true);
                        this.DataManager.ChangeOrderStatus(orderId, NouvemGlobal.NouDocStatusComplete.NouDocStatusID);
                        this.SelectedOrder = null;
                        this.Spec = Message.SelectProductionOrder;
                    }));
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        var msg = string.Format(Message.MixComplete, localMixes);
                        SystemMessage.Write(MessageType.Priority, msg);
                        NouvemMessageBox.Show(msg, flashMessage: true);
                    }));
                }
            }
        }

        /// <summary>
        /// Handles a recipe product selection.
        /// </summary>
        private void HandleRecipeProduct()
        {
            try
            {
                this.SetRecipeBatch(false);
                var orderMethod = this.SelectedProductionDetail.NouOrderMethod;

                if (this.allowManualWeightEntry)
                {
                    this.AuthorisationsManager.AllowUserToEnterManualWeightOnTouchscreen =
                        this.SelectedProductionDetail.AllowManualWeightEntry.ToBool();
                }
               
                var useFullBatch = false;
                if (this.SelectedProductionDetail != null && this.SelectedProductionDetail.UseAllBatch.IsTrue())
                {
                    useFullBatch = this.UseFullRecipeBatch();
                }

                if (orderMethod == Strings.Quantity || useFullBatch || this.SelectedProductionDetail.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush))
                {
                    this.MainViewModel = this.Locator.IntoProductionMainGrid;
                    return;
                }

                var produced = this.selectedProductionDetail.WeightIntoProduction;

                this.MainViewModel = this.Locator.Needle;
                this.Locator.Indicator.SetNeedleValues(
                    this.selectedProductionDetail.WeightOrdered.ToDecimal(),
                    produced.ToDecimal(),
                    this.selectedProductionDetail.PlusTolerance.ToDecimal(),
                    this.selectedProductionDetail.MinusTolerance.ToDecimal(),
                    this.selectedProductionDetail.NouUOMID.ToInt(),
                    this.SelectedProductionDetail.InventoryItem.Master.Name);
            }
            finally
            {
                if (this.SelectedProductionDetail.RecordLostWeight.IsTrue())
                {
                    if (this.ScannerStockMode != ScannerMode.ReWeigh)
                    {
                        this.ScannerStockMode = ScannerMode.ReWeigh;
                    }
                }
                else
                {
                    if (this.ScannerStockMode != ScannerMode.Scan)
                    {
                        this.ScannerStockMode = ScannerMode.Scan;
                    }
                }
            }
        }

        /// <summary>
        /// Pull the full batch amount into play.
        /// </summary>
        private bool UseFullRecipeBatch()
        {
            if (this.SelectedProductionDetail.NouOrderMethod.CompareIgnoringCase(Constant.Quantity) 
                && this.SelectedProductionDetail.QuantityIntoProduction.ToDouble() <= 0)
            {
                this.Locator.Touchscreen.Quantity = this.SelectedProductionDetail.QuantityOrdered.ToDecimal();
                return true;
            }
            else if (this.SelectedProductionDetail.WeightIntoProduction.ToDouble() <= 0)
            {
                this.IndicatorManager.CloseIndicatorPort();
                var localWgt = this.SelectedProductionDetail.WeightOrdered.ToDecimal();
                //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
               // {
               //     System.Threading.Thread.Sleep(ApplicationSettings.AutoTypicalWeightSetWait);
                    this.Locator.Indicator.SetManualWeight(localWgt);
             //   }));

                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets a receipr products batch.
        /// </summary>
        /// <returns></returns>
        private bool SetRecipeBatch(bool displayMessage)
        {
            if (this.SelectedOrder == null || this.SelectedProductionDetail == null || this.SelectedProductionDetail.BackflushBatchDataRetrieved)
            {
                return true;
            }

            this.SelectedProductionDetail.BatchStockRemaining = null;
            this.SelectedProductionDetail.Batch = null;
            this.BatchNumber = null;

            ProductionBatchData batchData;
            if (this.MixCount == 1 && ApplicationSettings.AlwaysManuallySelectBatchOnMix1 
                && !this.SelectedProductionDetail.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush))
            {
                // make the operator select a batch
                batchData = null;
            }
            else
            {
                batchData = this.DataManager.GetProductionBatchData(this.SelectedProductionDetail.INMasterID);
            }
          
            if (batchData != null && !this.SelectedProductionDetail.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush))
            {
                if (!batchData.CurrentBatchId.IsNullOrZero())
                {
                    this.BatchNumber = new BatchNumber
                    {
                        BatchNumberID = batchData.CurrentBatchId.ToInt(),
                        Number = batchData.CurrentBatchNo
                    };

                    this.SelectedProductionDetail.Batch = this.BatchNumber;
                    this.SelectedProductionDetail.BatchStockRemaining = batchData.CurrentRemaining;
                }
                else if (this.recipeBatch != null)
                {
                    // newly selected batch, so use it's remaining stock
                    this.BatchNumber = this.recipeBatch.BatchNumber;
                    this.SelectedProductionDetail.Batch = this.BatchNumber;
                    this.SelectedProductionDetail.BatchStockRemaining = this.recipeBatch.BatchStockRemaining;
                }
                
                //this.BatchIdBase = batchData.BaseId;
            }
            else
            {
                if (this.SelectedProductionDetail.NouIssueMethodName.CompareIgnoringCase(Constant.BackFlush)
                    && !this.SelectedProductionDetail.BackflushBatchDataRetrieved)
                {
                    if (batchData != null && !batchData.BatchId.IsNullOrZero())
                    {
                        this.BatchNumber = new BatchNumber
                        {
                            BatchNumberID = batchData.BatchId.ToInt(),
                            Number = batchData.BatchNo
                        };

                        this.SelectedProductionDetail.Batch = this.BatchNumber;
                        this.SelectedProductionDetail.BatchStockRemaining = batchData.Remaining;
                        this.SelectedProductionDetail.BackflushBatchDataRetrieved = true;
                    }
                }
            }

            this.recipeBatch = null;
            return true;
        }

        /// <summary>
        /// Sets a receipr products batch.
        /// </summary>
        /// <returns></returns>
        private decimal GetRecipeBatchRemainingAmount()
        {
            var batchData = this.DataManager.GetLastRecipeUsedBatchWeight(this.BatchNumber.BatchNumberID, this.SelectedProductionDetail.INMasterID);
            if (batchData != null)
            {
                if (this.SelectedProductionDetail.NouOrderMethod.CompareIgnoringCase(Constant.Quantity))
                {
                    return batchData.RemainingQty.ToDecimal();
                }

                return batchData.RemainingWgt.ToDecimal();
            }

            return 0;
        }

        /// <summary>
        /// Sets the mode.
        /// </summary>
        /// <param name="process">The incoming process selection.</param>
        private void SetIntoProductionMode(Nouvem.Model.DataLayer.Process process)
        {
            try
            {
                ApplicationSettings.IntoProductionMode =
                    (IntoProductionMode)Enum.Parse(typeof(IntoProductionMode), process.Name.Replace(" ", ""));

                this.SetPanel();
                this.GetAttributeAllocationApplicationData(process.ProcessID);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("SetIntoProductionMode: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Sets the touchscreen panel.
        /// </summary>
        private void SetPanel()
        {
            this.IndicatorManager.IgnoreValidation(true);
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard)
            {
                this.IndicatorManager.IgnoreValidation(false);
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionPanel;
                Messenger.Default.Send(false, Token.EnableProductSelectionOnIntoProduction);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch)
            {
                this.IndicatorManager.IgnoreValidation(false);
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionPanel;
                Messenger.Default.Send(true, Token.EnableProductSelectionOnIntoProduction);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
            {
                this.IndicatorManager.IgnoreValidation(false);
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionReceipePanel;
                Messenger.Default.Send(false, Token.EnableProductSelectionOnIntoProduction);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.SetOrderLinesStatus();
                }));

                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionConformance)
            {
                this.IndicatorManager.IgnoreValidation(false);
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionPanel;
                this.MainViewModel = this.Locator.IntoProductionConformance;
                this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                Messenger.Default.Send(false, Token.EnableProductSelectionOnIntoProduction);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
            {
                this.IndicatorManager.IgnoreValidation(false);
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionPanel;
                Messenger.Default.Send(false, Token.EnableProductSelectionOnIntoProduction);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct)
            {
                this.IndicatorManager.IgnoreValidation(false);
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionPanel;
                Messenger.Default.Send(true, Token.EnableProductSelectionOnIntoProduction);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
            {
                this.Locator.TouchscreenProducts.Module = ViewType.IntoProduction;
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionButcheryPanel;
                Messenger.Default.Send(true, Token.EnableProductSelectionOnIntoProduction);
                this.Locator.Touchscreen.Quantity = 0;
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing)
            {
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionPorkPanel;
                this.Locator.Touchscreen.Quantity = 0;
                Messenger.Default.Send(false, Token.EnableProductSelectionOnIntoProduction);
                return;
            }
        }

        /// <summary>
        /// Sets up the module.
        /// </summary>
        private void SetUpModule()
        {
            try
            {
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
                {
                    this.Locator.TouchscreenProducts.Module = ViewType.IntoProduction;
                    this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenProductionButcheryPanel;
                    Messenger.Default.Send(true, Token.EnableProductSelectionOnIntoProduction);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("SetIntoProductionMode: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Handles a carcass selection.
        /// </summary>
        /// <param name="product">The selected product.</param>
        /// <param name="weight">The scanned weight.</param>
        private void HandleCarcassSelection(InventoryItem product, decimal weight)
        {
            this.HandleProductSelection(product);

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Carcass)
            {
                var localSerial = this.serial;
                this.Log.LogDebug(this.GetType(), string.Format("5. About to record weight serial:{0}", localSerial));
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.RecordCarcassWeight(localSerial);
                }));

                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.CarcassNoReweigh)
            {
                var localSerial = this.serial;
                this.Log.LogDebug(this.GetType(), string.Format("5. About to record weight serial:{0}", localSerial));
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.RecordCarcassNoReweighWeight(localSerial, weight);
                }));
            }
        }

        /// <summary>
        /// Handles a carcass split selection.
        /// </summary>
        /// <param name="product">The selected product.</param>
        private void HandleCarcassSplitSelection(InventoryItem product)
        {
            if (this.ProcessingData)
            {
                return;
            }
        
            this.ProcessingData = true;
            Messenger.Default.Send(Token.Message, Token.CloseCarcassSplitWindow);
            try
            {
                this.HandleProductSelection(product);
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
                {
                    if (ApplicationSettings.CarcassSplitReweighing)
                    {
                        this.RecordWeight();
                    }
                    else
                    {
                        this.HandleScannerDataStandard(this.serial.Trim(), product.Master.INMasterID, true);
                    }
                }
            }
            finally
            {
                this.ProcessingData = false;
            }
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        private StockDetail CreateTransaction()
        {
            this.Log.LogDebug(this.GetType(), "CreatingTransaction(): Creating transaction");

            var localWarehouseId = NouvemGlobal.ProductionId;
            int? reference = null;
            int? splitStockId = null;
            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                localWarehouseId = NouvemGlobal.WarehouseButcheryId;
                reference = 1;
            }
            else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
            {
                localWarehouseId = NouvemGlobal.WarehouseInjectionId;
                reference = 1;
            }
            else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
            {
                splitStockId = this.MixCount;
            }

            var batchid = this.batchNumber?.BatchNumberID;
            if ((this.SelectedProductionDetail.StockMode == StockMode.Serial && ApplicationSettings.IntoProductionMode != IntoProductionMode.Recipe)
                || this.batchNumber == null)
            {
                batchid = this.SelectedOrder.Order.BatchNumberID;
            }

            IList<Model.DataLayer.Attribute> nonStandardResponses = null;
            if (this.NonStandardResponses != null && this.NonStandardResponses.Any())
            {
                nonStandardResponses = new List<Model.DataLayer.Attribute>();
                foreach (var attributeAllocationData in this.NonStandardResponses)
                {
                    nonStandardResponses.Add(new Model.DataLayer.Attribute
                    {
                        Attribute1 = attributeAllocationData.TraceabilityValueNonStandard,
                        AttributeMasterID = attributeAllocationData.AttributeMaster.AttributeMasterID
                    });
                }
            }

            return new StockDetail
            {
                MasterTableID = this.SelectedOrder.Order.PROrderID,
                TransactionQty = this.qty < 1 ? 1 : this.qty.ToDecimal(),
                TransactionWeight = this.indicatorWeight,
                Tare = this.tare,
                Pieces = this.pieces < 1 ? 1 : this.pieces,
                BatchNumberID = batchid,
                ProductionBatchID = this.SelectedOrder.Order.BatchNumberID,
                BatchNumberBaseID = this.BatchIdBase,
                IntakeID = this.IntakeId > 0 ? this.IntakeId : (int?)null,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId,
                WarehouseID = localWarehouseId,
                INMasterID = this.SelectedProductionDetail.INMasterID,
                ManualWeight = this.manualWeight,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                Reference = reference,
                ProcessID = this.Locator.ProcessSelection.SelectedProcess != null ? this.Locator.ProcessSelection.SelectedProcess.ProcessID : (int?)null,
                LabelID = this.serial.ToInt(),
                Serial = this.serial.ToNullableInt(),
                SplitID = splitStockId,
                NonStandardResponses = nonStandardResponses
            };
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordWeight()
        {
            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            #region attributes validation

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            this.CheckForUnsavedAttributes();

            #endregion

            decimal? decrementReweighStock = null;
            this.IndicatorManager.OpenIndicatorPort();

            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.SelectedProductionDetail == null && !this.ReworkingStock)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                if (this.BatchIdBase.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoAssociatedIntake);
                    NouvemMessageBox.Show(Message.NoAssociatedIntake, touchScreen: true);
                    return;
                }
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
            {
                if (this.BatchIdBase.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoAssociatedButchery);
                    NouvemMessageBox.Show(Message.NoAssociatedButchery, touchScreen: true);
                    return;
                }
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                if (this.Locator.Touchscreen.Quantity <= 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.QtyZeroStop);
                    NouvemMessageBox.Show(Message.QtyZeroStop, touchScreen: true);
                    return;
                }
            }

            if (this.SelectedProductionDetail != null && 
                this.SelectedProductionDetail.StockMode != StockMode.Serial 
                && this.batchNumber == null 
                && this.SelectedProductionDetail.RecordLostWeight.IsNullOrFalse())
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBatchSelected);
                NouvemMessageBox.Show(Message.NoBatchSelected, touchScreen: true);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe && this.SelectedProductionDetail != null
                && this.batchNumber == null && this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse()
                && this.SelectedProductionDetail.RecordLostWeight.IsNullOrFalse() && !this.ReworkingStock)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBatchSelected);
                NouvemMessageBox.Show(Message.NoBatchSelected, touchScreen: true);
                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    if (this.SelectedProductionDetail != null &&
                        this.SelectedProductionDetail.StockMode != StockMode.BatchQty &&
                        this.SelectedProductionDetail.StockMode != StockMode.ProductQty)
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                        return;
                    }
                    else
                    {
                        this.manualWeight = false;
                    }
                }
            }

            #region recipe weight validation

            if (this.ReworkingStock)
            {
                this.ReworkRecipeStock();
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe && !this.SelectedOrder.AutoCompleteRecipeMix
                                                                                    && this.SelectedProductionDetail != null
                                                                                    && this.SelectedProductionDetail.IsHeaderProduct.IsTrue() &&
                                                                                    this.qty < ApplicationSettings.MinQtyForRecipeFinishedProductManualComplete)
            {
                // below min qty required for recipe product when not auto completing.
                NouvemMessageBox.Show(string.Format(Message.QtyTooLow, this.qty, ApplicationSettings.MinQtyForRecipeFinishedProductManualComplete), touchScreen: true);
                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe && this.SelectedProductionDetail != null
                && this.SelectedProductionDetail.IsHeaderProduct.IsNullOrFalse())
            {
                if (!string.IsNullOrEmpty(ApplicationSettings.RecipeNegativeBatchStockMessage))
                {
                    var localAmt = this.SelectedProductionDetail.StockMode == StockMode.BatchQty
                        ? this.qty.ToDecimal()
                        : this.indicatorWeight;
                    if (this.SelectedProductionDetail.BatchStockRemaining.ToDecimal() -
                        localAmt < 0)
                    {
                        if (ApplicationSettings.RecipeNegativeBatchStockMessage.CompareIgnoringCaseAndWhitespace(Constant.SoftStop))
                        {
                            var msg = this.SelectedProductionDetail.BatchStockRemaining >= 0 ? Message.UnderBatchWarning : Message.AlreadyUnderBatchWarning;
                            NouvemMessageBox.Show(msg, touchScreen: true, buttonSelection: NouvemMessageBoxButtons.YesNo);
                            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                            {
                                return;
                            }
                        }
                        else
                        {
                            var msg = this.SelectedProductionDetail.BatchStockRemaining >= 0 ? Message.UnderBatchStop : Message.AlreadyUnderBatchStop;
                            NouvemMessageBox.Show(msg, touchScreen: true);
                            return;
                        }
                    }
                }

                if (this.SelectedProductionDetail.RecordLostWeight.IsTrue())
                {
                    if (this.recipeLostWeightTransaction == null)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoRecipeToppingsBarcodeEntered);
                        NouvemMessageBox.Show(Message.NoRecipeToppingsBarcodeEntered, touchScreen:true);
                        return;
                    }

                    var localWgt = this.recipeLostWeightTransaction.TransactionWeight - this.indicatorWeight;
                    NouvemMessageBox.Show(string.Format(Message.RecipeLostWgtPrompt, this.indicatorWeight, localWgt),
                        NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }

                    if (this.ProductionDetails == null || !this.ProductionDetails.Any(x => x.INMasterID == this.recipeLostWeightTransaction.INMasterID))
                    {
                        var localName = this.GetProductName(this.recipeLostWeightTransaction.INMasterID);
                        var msg = string.Format(Message.IncorrectToppingsProduct, localName);
                        SystemMessage.Write(MessageType.Issue, msg);
                        NouvemMessageBox.Show(msg, touchScreen: true);
                        return;
                    }

                    if (this.SelectedProductionDetail == null || this.SelectedProductionDetail.INMasterID !=
                        this.recipeLostWeightTransaction.INMasterID)
                    {
                        this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault(x =>
                            x.INMasterID == this.recipeLostWeightTransaction.INMasterID);
                    }

                    if (this.BatchNumber == null)
                    {
                        this.BatchNumber = new BatchNumber
                        {
                            BatchNumberID = this.recipeLostWeightTransaction.BatchNumberID.ToInt(),
                            Number = this.recipeLostWeightTransaction.BatchNumberID.ToInt().ToString()
                        };
                    }

                    decrementReweighStock = this.indicatorWeight;
                    this.indicatorWeight = localWgt.ToDecimal();
                }

                if (this.SelectedProductionDetail.NouOrderMethod.CompareIgnoringCase(Constant.Quantity))
                {
                    if (!this.IsAmountWithinToleranceLevels(this.qty.ToDecimal(),
                        this.SelectedProductionDetail.QuantityOrdered.ToDecimal(),
                        this.SelectedProductionDetail.QuantityIntoProduction.ToDecimal(),
                        this.SelectedProductionDetail.PlusTolerance.ToDecimal()))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.WeightOutsideOfToleranceRange);
                        NouvemMessageBox.Show(Message.QtyOutsideOfToleranceRange, touchScreen: true);
                        return;
                    }
                }
                else
                {
                    if (!this.IsAmountWithinToleranceLevels(this.indicatorWeight,
                        this.SelectedProductionDetail.WeightOrdered.ToDecimal(),
                        this.SelectedProductionDetail.WeightIntoProduction.ToDecimal(),
                        this.SelectedProductionDetail.PlusTolerance.ToDecimal()))
                    {
                        if (this.SelectedProductionDetail.IgnoreTolerances.IsNullOrFalse())
                        {
                            SystemMessage.Write(MessageType.Issue, Message.WeightOutsideOfToleranceRange);
                            NouvemMessageBox.Show(Message.WeightOutsideOfToleranceRange, touchScreen: true);
                            return;
                        }
                        else
                        {
                            // tolerances can be ignored, but ask the operator.
                            NouvemMessageBox.Show(Message.WeightOutsideOfToleranceRangePrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                            {
                                return;
                            }
                        }
                    }
                }

                // ignore batch check if reweigh box on scales.
                //if (!decrementReweighStock.HasValue)
                //{
                //    var remainingInBatch = this.GetRecipeBatchRemainingAmount();
                //    if (this.SelectedProductionDetail.NouOrderMethod.CompareIgnoringCase(Constant.Quantity))
                //    {
                //        if (remainingInBatch < this.qty.ToDecimal())
                //        {
                //            this.SelectedProductionDetail.IgnoreRecipeTolerance = true;
                //            var msg = string.Format(Message.RemainingInBatchLessThanScalesQty, remainingInBatch);
                //            SystemMessage.Write(MessageType.Issue, msg);
                //            NouvemMessageBox.Show(msg, touchScreen: true);
                //            return;
                //        }
                //    }
                //    else
                //    {
                //        if (remainingInBatch < this.indicatorWeight)
                //        {
                //            this.SelectedProductionDetail.IgnoreRecipeTolerance = true;
                //            var msg = string.Format(Message.RemainingBatchWeightLessThanScalesWeight, remainingInBatch);
                //            SystemMessage.Write(MessageType.Issue, msg);
                //            NouvemMessageBox.Show(msg, touchScreen: true);
                //            return;
                //        }
                //    }
                //}
            }

            #endregion

            var stockDetail = this.CreateTransaction();
            this.CreateAttribute(stockDetail);
            this.SelectedProductionDetail.StockDetails.Add(stockDetail);
            stockDetail.RecordingWeight = true;
            this.SelectedProductionDetail.StockDetailToProcess = stockDetail;
            var transactions = this.StockLabelsToPrint < 1 ? 1 : this.StockLabelsToPrint;
            try
            {
                for (int i = 0; i < transactions; i++)
                {
                    this.RecordSerialProduction();
                }
            }
            finally
            {
                this.StockLabelsToPrint = 1;
                this.Locator.Indicator.SetManualWeight(0);
            }

            if (decrementReweighStock.HasValue)
            {
                this.HandleScannerDataReweigh(decrementReweighStock, true);
                if (this.ScannerStockMode != ScannerMode.Scan)
                {
                    this.ScannerStockMode = ScannerMode.Scan;
                }
            }
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordCarcassSplitWeight(string localSerial)
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                    Messenger.Default.Send(Token.Message, Token.CloseCarcassSplitWindow);
                }
                else
                {
                    Messenger.Default.Send(Token.Message, Token.AllowSplitScanSelection);
                    this.Log.LogDebug(this.GetType(), string.Format("Error: Cannot Save weight: {0}", saveWeightResult));
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }
            else
            {
                this.IndicatorManager.OpenIndicatorPort();
                Messenger.Default.Send(Token.Message, Token.CloseCarcassSplitWindow);
            }

            this.Locator.Indicator.ManualWeight = false;
            this.manualWeight = false;
            var stockDetail = this.CreateTransaction();
            this.CreateAttribute(stockDetail);
            this.SelectedProductionDetail.StockDetails.Add(stockDetail);
            this.SelectedProductionDetail.StockDetailToProcess = stockDetail;

            this.RecordSerialProduction();
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordCarcassWeight(string localSerial)
        {
            this.Log.LogDebug(this.GetType(), string.Format("6. Recording weight for Serial:{0}", localSerial));
            this.IndicatorManager.OpenIndicatorPort();

            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    //this.serial = localSerial;
                    this.Log.LogDebug(this.GetType(), string.Format("Error: Cannot Save weight: {0}", saveWeightResult));
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }

            var transData = new List<StockTransactionData>
            {
                new StockTransactionData
                {
                    Transaction = new StockTransaction
                    {
                        MasterTableID = this.SelectedOrder.Order.PROrderID,
                        TransactionQTY = this.qty < 1 ? 1 : this.qty.ToDecimal(),
                        TransactionWeight = this.indicatorWeight,
                        Tare = this.tare,
                        GrossWeight = this.indicatorWeight + this.tare,
                        Pieces = this.pieces < 1 ? 1 : this.pieces,
                        //BatchNumberID = this.SelectedProductionDetail.GoodsReceiptDatas.First().BatchNumber.BatchNumberID,
                        BatchNumberID = this.SelectedOrder.Order.BatchNumberID,
                        TransactionDate = DateTime.Now,
                        NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId,
                        WarehouseID = NouvemGlobal.ProductionId,
                        INMasterID = this.SelectedProductionDetail.INMasterID,
                        ManualWeight = this.manualWeight,
                        DeviceMasterID = NouvemGlobal.DeviceId,
                        UserMasterID = NouvemGlobal.UserId,
                        LabelID = localSerial,
                        Serial = this.serial.ToInt()
                    }
                }
            };

            this.Log.LogDebug(this.GetType(), string.Format("7. Saving weight:Serial:{0}, wgt:{1}, productId:{2}", this.serial, this.indicatorWeight, this.SelectedProductionDetail.INMasterID));

            if (!this.SelectedProductionDetail.GoodsReceiptDatas.Any())
            {
                this.SelectedProductionDetail.GoodsReceiptDatas.Add(new GoodsReceiptData());
            }

            this.SelectedProductionDetail.GoodsReceiptDatas.First().TransactionData = transData;
            this.RecordSerialProduction();
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordConformanceProductWeight()
        {
            #region validation

            if (!this.ConformanceDetails.Any())
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                NouvemMessageBox.Show(Message.NoOrderHasBeenSelected, touchScreen: true);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                }));

                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                NouvemMessageBox.Show(Message.NoProductSelected, touchScreen: true);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                }));

                return;
            }

            #endregion

            var localDetail = this.SelectedProductionDetail.StockDetailToProcess;
            this.qty = localDetail.TransactionQty.ToDecimal();
            this.indicatorWeight = localDetail.ColdWeight;
            var stockDetail = this.CreateTransaction();
            this.CreateAttribute(stockDetail);
            stockDetail.Eartag = localDetail.Eartag;
            stockDetail.CarcassNumber = localDetail.CarcassNumber;
            stockDetail.HerdNo = localDetail.HerdNo;
            stockDetail.Category = localDetail.Category;
            stockDetail.SequencedDate = localDetail.SequencedDate;
            stockDetail.GradingDate = localDetail.GradingDate;
            stockDetail.SupplierID = localDetail.SupplierID;
            stockDetail.Reference = localDetail.AttributeID;
            stockDetail.Serial = localDetail.Serial;
            this.SelectedProductionDetail.StockDetails.Add(stockDetail);
            this.SelectedProductionDetail.StockDetailToProcess = stockDetail;
            var localProductionDetail = this.SelectedProductionDetail;
            this.RecordSerialProduction();
            this.ConformanceDetails.Clear();
            this.AttributeReference = string.Empty;
            if (!this.ProductionDetails.Any(x => x.INMasterID == localDetail.INMasterID))
            {
                this.ProductionDetails.Add(localProductionDetail);
            }

            this.RefreshOrderDetails();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
            }));
        }

        /// <summary>
        /// Refreshes the order.
        /// </summary>
        private void RefreshOrderDetails()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }

            var orderDetails = this.DataManager.GetProductionDetails(this.SelectedOrder.Order.PROrderID, NouvemGlobal.TransactionTypeProductionIssueId);
            if (orderDetails == null)
            {
                return;
            }

            foreach (var localDetail in this.ProductionDetails)
            {
                localDetail.QuantityIntoProduction = 0;
                localDetail.WeightIntoProduction = 0;
            }

            foreach (var detail in orderDetails)
            {
                var localDetail = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                if (localDetail != null)
                {
                    localDetail.QuantityIntoProduction = detail.Qty.ToDecimal();
                    localDetail.WeightIntoProduction = detail.Wgt.ToDecimal();
                }
            }
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordSelectedProductWeight()
        {
            this.Log.LogDebug(this.GetType(), string.Format("6. Recording weight for Serial:{0}", this.serial));
            this.IndicatorManager.OpenIndicatorPort();
            var localSerial = this.serial;

            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                NouvemMessageBox.Show(Message.NoOrderHasBeenSelected, touchScreen: true);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                }));

                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                NouvemMessageBox.Show(Message.NoProductSelected, touchScreen: true);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                }));

                return;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct && this.serial == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoLabelScanned);
                NouvemMessageBox.Show(Message.NoLabelScanned, touchScreen: true);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.RecordButtonContent = Strings.Wait;
                }));

                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    //this.serial = localSerial;
                    this.Log.LogDebug(this.GetType(), string.Format("Error: Cannot Save weight: {0}", saveWeightResult));
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }
            else
            {
                this.indicatorWeight = this.Locator.Indicator.Weight;
            }

            var stockDetail = this.CreateTransaction();
            this.CreateAttribute(stockDetail);
            this.SelectedProductionDetail.StockDetails.Add(stockDetail);
            this.SelectedProductionDetail.StockDetailToProcess = stockDetail;
            this.RecordSerialProduction();
            if (this.AutoBoxing)
            {
                this.AutoBoxLabels.Add(localSerial.ToInt());
            }
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordCarcassNoReweighWeight(string localSerial, decimal weight)
        {
            this.Log.LogDebug(this.GetType(), string.Format("6. Recording weight for Serial:{0}", localSerial));

            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (this.SelectedProductionDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                return;
            }

            #endregion

            var transData = new List<StockTransactionData>
            {
                new StockTransactionData
                {
                    Transaction = new StockTransaction
                    {
                        MasterTableID = this.SelectedOrder.Order.PROrderID,
                        TransactionQTY = this.qty < 1 ? 1 : this.qty.ToDecimal(),
                        TransactionWeight = weight,
                        Tare = 0,
                        GrossWeight = weight,
                        Pieces = this.pieces < 1 ? 1 : this.pieces,
                        BatchNumberID = this.SelectedOrder.Order.BatchNumberID,
                        TransactionDate = DateTime.Now,
                        NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId,
                        WarehouseID = NouvemGlobal.ProductionId,
                        INMasterID = this.SelectedProductionDetail.INMasterID,
                        ManualWeight = this.manualWeight,
                        DeviceMasterID = NouvemGlobal.DeviceId,
                        UserMasterID = NouvemGlobal.UserId,
                        LabelID = localSerial,
                        Serial = this.serial.ToInt()
                    }
                }
            };

            this.Log.LogDebug(this.GetType(), string.Format("7. Saving weight:Serial:{0}, wgt:{1}, productId:{2}", this.serial, this.indicatorWeight, this.SelectedProductionDetail.INMasterID));

            if (!this.SelectedProductionDetail.GoodsReceiptDatas.Any())
            {
                this.SelectedProductionDetail.GoodsReceiptDatas.Add(new GoodsReceiptData());
            }

            this.SelectedProductionDetail.GoodsReceiptDatas.First().TransactionData = transData;
            this.RecordSerialProduction();
        }

        /// <summary>
        /// Handles the batch scan.
        /// </summary>
        /// <param name="data">The label scanned data.</param>
        private void HandleScannerDataForBatchSelection(string data)
        {
            //data = "J03130417";
            this.Log.LogDebug(this.GetType(), string.Format("Touchscreen: HandleScannerDataForBatchSelection(): Data:{0}, Data length:{1}", data, data.Length));
            data = data.Replace("-", "").Replace("#", "");
            if (data.StartsWithIgnoringCase("dm"))
            {
                data = data.Substring(2, data.Length - 2);
            }

            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
            this.Locator.TouchscreenProductionOrders.GetAllOrdersOnly();
            var localBatches = this.Locator.TouchscreenProductionOrders.AllOrders.Where(
                 x => !string.IsNullOrWhiteSpace(x.Reference) && x.Reference.ContainsIgnoringCase(data)).ToList();

            if (!localBatches.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.BatchNotFound, data));
                NouvemMessageBox.Show(string.Format(Message.BatchNotFound, data), touchScreen: true);
                return;
            }

            if (localBatches.Count > 1)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(string.Format(Message.SimiliarBatchesFound, localBatches.Count, data), touchScreen: true);
                }));

                return;
            }

            var localBatch = localBatches.First();
            this.Locator.TouchscreenProductionOrders.SelectedOrder
                =
                this.Locator.TouchscreenProductionOrders.AllOrders.FirstOrDefault(
                    x => x.Order.PROrderID == localBatch.Order.PROrderID);

            if (this.Locator.TouchscreenProductionOrders.SelectedOrder != null)
            {
                this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
            }
        }

        /// <summary>
        /// Handles scanned stock.
        /// </summary>
        /// <param name="serial">The scanned stock id.</param>
        private void HandleScannerDataConformance(string serial)
        {
            #region validation

            var notProcessed = this.ConformanceDetails.FirstOrDefault();
            if (notProcessed != null)
            {
                var msg = string.Format(Message.PreviousScanNotProcessed, notProcessed.Serial);
                SystemMessage.Write(MessageType.Issue, msg);
                NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, true);
                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    return;
                }
            }

            #endregion

            this.ConformanceDetails.Clear();
            this.AttributeReference = string.Empty;
            var stockData = this.DataManager.GetTransactionDataForIntoProduction(serial.ToInt());

            #region validation

            if (!string.IsNullOrEmpty(stockData.Error))
            {
                var message = string.Empty;
                if (stockData.Error.StartsWith(Message.StockConsumedOnBox))
                {
                    message = stockData.Error;
                }
                else if (stockData.Error.Equals(Constant.NotFound))
                {
                    message = string.Format(Message.SerialNumberNotFound, serial);
                }
                else if (stockData.Error.Equals(Strings.Unknown))
                {
                    message = Message.StockAlreadyConsumed;
                }
                else
                {
                    message = string.Format(Message.StockAddedToPROrder, serial, stockData.Error);
                }

                SystemMessage.Write(MessageType.Issue, message);
                return;
            }

            #endregion

            var product = this.ProductionDetails.FirstOrDefault(x => x != null && x.INMasterID == stockData.INMasterID);
            if (product != null)
            {
                product.WeightDelivered = stockData.ColdWeight.ToDecimal();
                product.QuantityDelivered = stockData.TransactionQty.ToDecimal();
            }
            else
            {
                product = new SaleDetail
                {
                    INMasterID = stockData.INMasterID,
                    WeightOrdered = 0,
                    QuantityOrdered = 0,
                    WeightIntoProduction = 0,
                    QuantityIntoProduction = 0,
                    Serial = stockData.Serial,
                    WeightDelivered = stockData.ColdWeight.ToDecimal(),
                    QuantityDelivered = stockData.TransactionQty.ToDecimal(),
                    InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == stockData.INMasterID)
                };
            }

            this.AttributeReference = stockData.HerdNo;
            product.StockDetailToProcess = stockData;
            this.ConformanceDetails.Add(product);
            this.SelectedProductionDetail = product;
            this.Locator.Touchscreen.RecordButtonContent = Strings.AddToBatch;
        }

        /// <summary>
        /// Handles scanned stock.
        /// </summary>
        /// <param name="serial">The scanned stock id.</param>
        private void HandleScannerDataStandard(string serial, int inmasterid = 0, bool imported = false)
        {
            var nonSerialBarcode = false;
            if (!serial.IsNumeric() || serial.Length > 17)
            {
                nonSerialBarcode = true;
            }

            StockDetail stockData;
            if (imported)
            {
                stockData = this.DataManager.GetTransactionDataForIntoProductionByComments(serial, inmasterid);
            }
            else if (nonSerialBarcode)
            {
                stockData = this.DataManager.GetTransactionDataForIntoProductionByComments(serial, 0);
            }
            else
            {
                serial = serial.RemoveNonIntegers();
                stockData = this.DataManager.GetTransactionDataForIntoProduction(serial.ToInt());
            }

            #region validation

            if (ApplicationSettings.ScanningForIntakeBatchesAtDispatch && stockData != null)
            {
                if (this.GetStockMode(stockData.INMasterID) > 1)
                {
                    this.HandleScannerDataForIntakeBatchSelection(stockData.INMasterID, stockData.BatchNumberID.ToInt());
                    return;
                }
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
            {
                if (stockData != null && stockData.INMasterID == this.receipeProduct?.INMasterID)
                {
                    // rework stock
                    this.ScannerStockMode = ScannerMode.ReWeigh;
                    this.HandleScannerData(serial);
                    return;
                }
            }

            if (!string.IsNullOrEmpty(stockData.Error))
            {
                var message = string.Empty;
                if (stockData.Error.StartsWith(Message.StockConsumedOnBox))
                {
                    message = stockData.Error;
                }
                else if (stockData.Error.Equals(Constant.NotFound))
                {
                    message = string.Format(Message.SerialNumberNotFound, serial);
                }
                else if (stockData.Error.Equals(Strings.Unknown))
                {
                    message = Message.StockAlreadyConsumed;
                }
                else
                {
                    message = string.Format(Message.StockAddedToPROrder, serial, stockData.Error);
                }

                SystemMessage.Write(MessageType.Issue, message);

                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }

                return;
            }

            #endregion
           
            var localProduct = new SaleDetail
            {
                INMasterID = stockData.INMasterID,
                StockDetailToProcess = stockData
            };

            var localDetail = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == stockData.INMasterID);
            if (localDetail != null)
            {
                this.SelectedProductionDetail = localDetail;
            }

            this.SetProductDetail(localProduct);
            this.SetAttributeValues(stockData);
            var stockDetail = this.CreateTransaction();
            stockDetail.TransactionWeight = stockData.TransactionWeight;
            stockDetail.TransactionQty = stockData.TransactionQty;
            stockDetail.Serial = stockData.Serial;
            this.SelectedProductionDetail.StockDetails.Add(stockDetail);
            this.SelectedProductionDetail.StockDetailToProcess = stockDetail;
            this.RecordSerialProduction();
            Messenger.Default.Send(Token.Message, Token.CloseCarcassSplitWindow);
        }


        /// <summary>
        /// Handles the recipe topping product, recreating it with remaining wgt.
        /// </summary>
        /// <param name="remainingWgt">The remaining wgt.</param>
        private StockTransaction HandleScannerDataReweigh(decimal? remainingWgt, bool resetLostWgt, int? prOrderID = null)
        {
            StockTransaction trans;
            try
            {
                var localWarehouseId = NouvemGlobal.ProductionId;
                var productId = this.recipeLostWeightTransaction.INMasterID;
                trans = this.DataManager.RecreateTransactionWithWgt(this.recipeLostWeightTransaction, remainingWgt.ToDecimal(), localWarehouseId, prOrderID);
                var mode = this.reworkStock ? ViewType.IntoProduction : ViewType.APReceipt;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Print(trans.StockTransactionID, productId: productId, mode:mode);
                }));
            }
            finally
            {
                if (resetLostWgt)
                {
                    this.recipeLostWeightTransaction = null;
                }
            }

            return trans;
        }

        /// <summary>
        /// Handle the carcass split scan.
        /// </summary>
        /// <param name="data">The scanned barcode.</param>
        private void HandleScannerDataCarcassProductSelect(string data)
        {
            if (this.selectedProductionDetail == null || this.selectedProductionDetail.INMasterID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                NouvemMessageBox.Show(Message.NoProductSelected, touchScreen: true);
                return;
            }
   
            this.serial = data;
           
            var linkedProduct = this.DataManager.GetLinkedProduct(data);
            if (linkedProduct == null)
            {
                this.Log.LogDebug(this.GetType(), "Linked product not found.");
                SystemMessage.Write(MessageType.Issue, Message.ProductNotFound);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, touchScreen: true);
                }

                return;
            }

            if (ApplicationSettings.EnforceAutoBoxing && !this.AutoBoxing)
            {
                var lambGroup = linkedProduct.MonthsOld.ToInt();
                if (lambGroup == ApplicationSettings.AutoBoxProductGroups)
                {
                    NouvemMessageBox.Show($"You must scan traceability labels for lamb products. Please enter a value in 'Labels to Scan'", touchScreen: true);
                    return;
                }
            }

            Application.Current.Dispatcher.Invoke(() => this.Locator.Touchscreen.RecordButtonContent = Strings.Record);
            if (!ApplicationSettings.IntoProductionSelectProductScanAndWeighSeparately)
            {
                this.RecordSelectedProductWeight();
            }
        }

        /// <summary>
        /// Handle the carcass split scan.
        /// </summary>
        /// <param name="data">The scanned barcode.</param>
        private void HandleCarcass(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("1. Scanned barcode:{0}", data));
            this.serial = data;
            var localTrans = this.DataManager.GetIntoProductionTransaction(data.ToInt());
            if (localTrans != null)
            {
                var message = string.Format(Message.CarcassAlreadyInProduction, data);
                SystemMessage.Write(MessageType.Issue, message);

                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                }

                return;
            }

            var linkedProduct = this.DataManager.GetLinkedProduct(data);

            //Test data
            //var linkedProduct = new GetLinkedProduct_Result2();
            //linkedProduct.ProductCode = "1053";
            //linkedProduct.ScaleNettWeight = 150;

            if (linkedProduct == null)
            {
                this.Log.LogDebug(this.GetType(), "Linked product not found.");
                SystemMessage.Write(MessageType.Issue, Message.ProductNotFound);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, touchScreen: true);
                }

                return;
            }

            var localProduct =
                NouvemGlobal.InventoryItems.FirstOrDefault(
                    x => x.Master.Code.CompareIgnoringCase(linkedProduct.ProductCode));

            if (localProduct == null)
            {
                this.Log.LogDebug(this.GetType(), string.Format("Linked product code:{0} not found in the nouvem database", linkedProduct.ProductCode));
                SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductNotFoundInDb, linkedProduct.ProductCode));
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, touchScreen: true);
                }

                return;
            }

            this.HandleCarcassSelection(localProduct, linkedProduct.ScaleNettWeight.ToDecimal());
        }

        /// <summary>
        /// Handle the carcass split scan.
        /// </summary>
        /// <param name="data">The scanned barcode.</param>
        private void HandleCarcassSplit(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("1. Scanned barcode:{0}", data));
            this.serial = data;
            var localTrans = this.DataManager.GetIntoProductionTransaction(data.ToInt());
            if (localTrans != null)
            {
                //this.Log.LogDebug(this.GetType(), string.Format("1.1 Scanned barcode:{0}", data));
                this.Log.LogDebug(this.GetType(), "Already scanned");

                this.SetCarcassSplitData(data.Trim());
                return;
            }

            var linkedProduct = this.DataManager.GetLinkedProduct(data);
            //Test data
            //var linkedProduct = new GetLinkedProduct_Result2();
            //linkedProduct.ProductCode = "1023";
            //linkedProduct.ScaleNettWeight = 50;

            if (linkedProduct == null)
            {
                this.Log.LogDebug(this.GetType(), "Linked product not found.");
                SystemMessage.Write(MessageType.Issue, Message.ProductNotFound);
                NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, touchScreen: true);
                return;
            }

            var localWgt = linkedProduct.ScaleNettWeight.ToDecimal();

            var localProduct =
                NouvemGlobal.InventoryItems.FirstOrDefault(
                    x => x.Master.Code.CompareIgnoringCase(linkedProduct.ProductCode));

            if (localProduct == null)
            {
                this.Log.LogDebug(this.GetType(), string.Format("Linked product code:{0} not found in the nouvem database", linkedProduct.ProductCode));
                //SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductNotFoundInDb, linkedProduct.ProductCode));
                //NouvemMessageBox.Show(Message.ProductNotFound, NouvemMessageBoxButtons.OK, touchScreen: true);
                //return;

                localProduct = NouvemGlobal.InventoryItems.First();
            }

            this.Log.LogDebug(this.GetType(), string.Format("Linked Product found:{0} with weight:{1}", linkedProduct.ProductCode, linkedProduct.ScaleNettWeight));

            var localProductId = localProduct.Master.INMasterID;
            var localBatch = this.BatchManager.GenerateBatchNumber(false, this.SelectedOrder.Order.Reference, referenceOnly:true);

            var stockTransaction = new StockTransaction();
            stockTransaction.TransactionWeight = localWgt;
            stockTransaction.GrossWeight = localWgt;
            stockTransaction.Tare = 0;
            stockTransaction.ManualWeight = false;
            stockTransaction.TransactionQTY = 1;
            stockTransaction.Pieces = 1;
            //stockTransaction.MasterTableID = this.SelectedOrder.Order.PROrderID;
            stockTransaction.INMasterID = localProductId;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.BatchNumberID = localBatch.BatchNumberID;
            //stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
            stockTransaction.WarehouseID = NouvemGlobal.ProductionId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;
            stockTransaction.Serial = data.ToInt();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.DataManager.AddStockTransaction(stockTransaction);
                //this.carcassSplitTransactionId = stockTransaction.StockTransactionID;
            }));

            var localScannedData = this.DataManager.GetScannedCarcassSplitStock(data.Trim());
            var localScannedProducts = string.Empty;

            foreach (var i in localScannedData)
            {
                localScannedProducts = localScannedProducts + "," + i.ToString();
            }

            this.Log.LogDebug(this.GetType(), string.Format("2.1 Sending products:{0} for UI de-selection", localScannedProducts));

            var carcassSplit = new CarcassSplitView(localScannedData);
            carcassSplit.Show();
            carcassSplit.Focus();
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void HandleProductSelection(InventoryItem product)
        {
            this.Log.LogDebug(this.GetType(), string.Format("4. Product selected:{0}", product.Master.Name));
            if (this.ProductionDetails == null)
            {
                this.ProductionDetails = new ObservableCollection<SaleDetail>();
            }

            var localSaleDetail = new SaleDetail
            {
                InventoryItem = product,
                INMasterID = product.Master.INMasterID,
                NouStockMode = product.StockMode,
                WeightOrdered = 0,
                WeightReceived = 0,
                QuantityOrdered = 0,
                QuantityReceived = 0,
                WeightOutstanding = 0,
                QuantityOutstanding = 0
            };

            if (localSaleDetail.NouStockMode == null)
            {
                localSaleDetail.SetNouStockMode();
            }

            var localWgt = 0M;
            if (this.Locator.Indicator.ManualWeight)
            {
                localWgt = this.Locator.Indicator.Weight;
            }

            if (ApplicationSettings.IntoProductionMode != IntoProductionMode.Butchery &&
                ApplicationSettings.IntoProductionMode != IntoProductionMode.Injection)
            {
                this.Locator.Touchscreen.Quantity = 1;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
            {
                this.Locator.Touchscreen.Quantity = 0;
            }

            var localQty = this.qty < 1 ? 1 : this.qty;

            if (ApplicationSettings.ZeroTareOutsideOfDispatch)
            {
                this.Locator.Indicator.Tare = 0;
            }
            else
            {
                this.Locator.Indicator.Tare = (product.PiecesTareWeight * localQty.ToDouble()) + product.BoxTareWeight;
            }

            if (localWgt > 0)
            {
                this.Locator.Indicator.Weight = localWgt;
            }

            var localDetail = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == product.Master.INMasterID);
            if (localDetail == null)
            {
                this.ProductionDetails.Add(localSaleDetail);
                this.SelectedProductionDetail = localSaleDetail;
            }
            else
            {
                this.SelectedProductionDetail = localDetail;
            }

            if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.Carcass
                || ApplicationSettings.IntoProductionMode == IntoProductionMode.CarcassNoReweigh)
            {
                System.Threading.Thread.Sleep(200);
            }
        }

        /// <summary>
        /// Prints a batch barcode.
        /// </summary>
        private void PrintBarcode()
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                return;
            }

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            try
            {
                if (!this.ValidateData(this.traceabilityDataLabel, this.SelectedOrder.Order.PROrderID,
                    0, 0, 0,
                    NouvemGlobal.ProductionId, this.Locator.ProcessSelection.SelectedProcess.ProcessID, Constant.Batch))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            var productId = NouvemGlobal.InventoryItems.First().Master.INMasterID;
            if (this.ProductionDetails != null && this.ProductionDetails.Any())
            {
                productId = this.ProductionDetails.Last().INMasterID;
            }

            // fetch the traceability values
            var stockTransaction = new StockTransaction();
            stockTransaction.TransactionWeight = 0;
            stockTransaction.GrossWeight = 0;
            stockTransaction.Tare = 0;
            stockTransaction.MasterTableID = this.SelectedOrder.Order.PROrderID;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = 0;
            stockTransaction.BatchNumberID = this.SelectedOrder.Order.BatchNumberID;
            stockTransaction.BatchID_Base = this.BatchIdBase;
            stockTransaction.INMasterID = productId;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
            stockTransaction.WarehouseID = NouvemGlobal.ProductionId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;
            stockTransaction.Deleted = DateTime.Now;
            stockTransaction.Consumed = DateTime.Now;
            stockTransaction.Reference = -1;
            stockTransaction.Comments = ApplicationSettings.IntoProductionMode.ToString();

            var id = this.DataManager.AddTransaction(stockTransaction);
            if (id > 0)
            {
                this.Print(id, LabelType.Shipping, this.Locator.ProcessSelection.SelectedProcess, batchLabel:true);
                SystemMessage.Write(MessageType.Priority, Message.BatchBarcodePrinted);
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The newly created stock transaction id.</param>
        protected void Print(int stockTransactionId, LabelType labelType = LabelType.Item, Process process = null, int? productId = null, int? productGroupId = null, ViewType mode = ViewType.IntoProduction, bool batchLabel = false)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Print(): Attempting to print Stocktransactionid:{0}", stockTransactionId));

            #region validation

            if (stockTransactionId == 0)
            {
                return;
            }

            #endregion

            try
            {
                var labelCount = this.Locator.Touchscreen.LabelsToPrint < 1
                    ? 1
                    : this.Locator.Touchscreen.LabelsToPrint;
                var labels = this.PrintManager.ProcessPrinting(null, null, productId, productGroupId, mode, labelType, stockTransactionId, labelCount, process, batchLabel:batchLabel);
                if (labels.Item1.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.DataManager.AddLabelToTransaction(stockTransactionId,
                            string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                    }), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                if (ApplicationSettings.TouchScreenMode)
                {
                    Keypad.Show(KeypadTarget.ManualSerial, useKeypadSides: ApplicationSettings.UseKeypadSidesAtIntoProduction);
                }
            };
        }

        /// <summary>
        /// Get the carcass product selections.
        /// </summary>
        private void GetCarcassSplitOptions()
        {
            this.carcassSplitOptions = this.DataManager.GetCarcassSplitProducts();
        }

        /// <summary>
        /// Determines if the barcode is a scotbeef barcode.
        /// </summary>
        /// <param name="data">The scanned data.</param>
        /// <returns>Flag, as to whether it's a ascotbeef barcode or not.</returns>
        private bool IsScotBeefBarcode(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("IsScotBeefBarcode:{0}", data));
            return data.Length >= 12 &&
                (data.ContainsIgnoringCase("H")
                || data.ContainsIgnoringCase("F")
                || data.ContainsIgnoringCase("L"));
        }

        /// <summary>
        /// Checks whether the carcass or product is allowed into the current batch.
        /// </summary>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A string error message if not allowed in.</returns>
        private string CheckBatchEligibility(string serial)
        {
            if (this.SelectedOrder == null)
            {
                return string.Empty;
            }

            var bornIns = this.SelectedOrder.Countries.Where(x => x.BornIn).ToList();
            var rearedIns = this.SelectedOrder.Countries.Where(x => !x.BornIn).ToList();
            var qa = this.SelectedOrder.Order.QA;
            var categories = this.SelectedOrder.CarcassTypes;
            var plants = this.SelectedOrder.Plants;
            var age = this.SelectedOrder.Order.OverAge;

            if (!bornIns.Any() && !rearedIns.Any() && !qa.HasValue && !categories.Any() && !plants.Any() && !age.HasValue)
            {
                // empty spec
                return string.Empty;
            }

            var data = this.DataManager.GetProductionOrderStockData(serial);

            if (data == null)
            {
                return string.Empty;
            }

            if (categories.Any())
            {
                var category =
                    this.Categories.FirstOrDefault(x => x.CategoryID == data.Category);
                if (category != null)
                {
                    var id = data.Category.ToInt();
                    if (!categories.Select(x => x.CarcassTypeID).Contains(id))
                    {
                        return string.Format(Message.BatchCategoryNotAllowed, category.Name);
                    }
                }
            }

            if (bornIns.Any())
            {
                var stockBornIn =
                    this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(data.CountryOfOrigin));
                if (stockBornIn != null)
                {
                    var id = stockBornIn.CountryID;
                    if (!bornIns.Select(x => x.CountryID).Contains(id))
                    {
                        return string.Format(Message.BatchBornInNotAllowed, data.CountryOfOrigin);
                    }
                }
                else
                {
                    return string.Format(Message.BatchBornInNotAllowed, data.CountryOfOrigin);
                }
            }

            if (rearedIns.Any())
            {
                var stockRearedIn =
                    this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(data.RearedIn));
                if (stockRearedIn != null)
                {
                    var id = stockRearedIn.CountryID;
                    if (!rearedIns.Select(x => x.CountryID).Contains(id))
                    {
                        return string.Format(Message.BatchRearedInNotAllowed, data.CountryOfOrigin);
                    }
                }
                else
                {
                    return string.Format(Message.BatchBornInNotAllowed, data.CountryOfOrigin);
                }
            }

            if (qa.HasValue)
            {
                var dataQa = data.FarmAssured.ToBool();
                if (qa != dataQa)
                {
                    return string.Format(Message.BatchQANotAllowed, dataQa ? string.Empty : Strings.not);
                }
            }

            if (age.HasValue)
            {
                var dataAge = data.Age_In_Months.ToDate().DateDifferenceInMonths(DateTime.Today);
                if (age == true)
                {
                    if (dataAge <= ApplicationSettings.YoungBullMaxAge)
                    {
                        return Message.UnderAgeInOverAgeBatch;
                    }
                }
                else
                {
                    if (dataAge > ApplicationSettings.YoungBullMaxAge)
                    {
                        return Message.OverrAgeInUnderAgeBatch;
                    }
                }
            }

            if (plants.Any())
            {
                var plant =
                    this.Plants.FirstOrDefault(x => x.Code.CompareIgnoringCaseAndWhitespace(data.Slaughtered_In));
                if (plant != null)
                {
                    var id = plant.PlantID;
                    if (!plants.Select(x => x.PlantID).Contains(id))
                    {
                        return string.Format(Message.BatchSlaughterPlantNotAllowed, data.Slaughtered_In);
                    }
                }
                else
                {
                    return string.Format(Message.BatchSlaughterPlantNotAllowed, data.Slaughtered_In);
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Sets the hind/fores to be displayed, and calls the s[plit screen.
        /// </summary>
        /// <param name="data"></param>
        private void SetCarcassSplitData(string data)
        {
            var scannedData = this.DataManager.GetScannedCarcassSplitStock(data.Trim(), ApplicationSettings.IntakeMode == IntakeMode.IntakeImport);
            var scannedProducts = string.Empty;

            foreach (var i in scannedData)
            {
                scannedProducts = scannedProducts + "," + i;
            }

            this.serial = data.Trim();
            var split = new CarcassSplitView(scannedData);
            split.Show();
            split.Focus();
        }

        /// <summary>
        /// Sets the order lines status.
        /// </summary>
        protected void SetOrderLinesStatus()
        {
            if (this.ProductionDetails == null)
            {
                return;
            }

            var localDetails = new List<SaleDetail>(this.ProductionDetails);
            foreach (var localDetail in localDetails)
            {
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                {
                    this.SetRecipeLineStatus(localDetail);
                }
                else
                {
                    this.SetLineStatus(localDetail);
                }
            }
        }

        /// <summary>
        /// Sets the line status.
        /// </summary>
        protected void SetLineStatus(SaleDetail detail)
        {
            if (detail == null || !this.ProductionDetails.Any())
            {
                return;
            }

            detail.CheckLineStatus();
            if (detail.IsFilled)
            {
                var index = 0;
                for (int i = 0; i < this.ProductionDetails.Count; i++)
                {
                    if (this.ProductionDetails.ElementAt(i).SaleDetailID == detail.SaleDetailID)
                    {
                        index = i;
                        break;
                    }
                }

                // move filled line to the bottom, greyed out.
                this.ProductionDetails.Move(index, this.ProductionDetails.Count - 1);
            }
        }

        /// <summary>
        /// Sets the line status.
        /// </summary>
        protected void SetRecipeLineStatus(SaleDetail detail)
        {
            if (detail == null || !this.ProductionDetails.Any())
            {
                return;
            }

            detail.NouOrderMethod = Constant.Weight;
            var uom = this.recipeOrderMethods.FirstOrDefault(x => x.UOMMasterID == detail.NouUOMID);
            if (uom != null && uom.OrderMethod.CompareIgnoringCaseAndWhitespace(Constant.Quantity))
            {
                detail.NouOrderMethod = Constant.Quantity;
            }

            try
            {
                detail.CheckRecipeLineStatus();
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }
            
            if (detail.IsFilled)
            {
                var index = 0;
                for (int i = 0; i < this.ProductionDetails.Count; i++)
                {
                    if (this.ProductionDetails.ElementAt(i).INMasterID == detail.INMasterID)
                    {
                        index = i;
                        break;
                    }
                }

                // move filled line to the bottom, greyed out.
                this.ProductionDetails.Move(index, this.ProductionDetails.Count - 1);
            }
        }

        /// <summary>
        /// Handles a selected production order.
        /// </summary>
        /// <param name="p">The incoming order.</param>
        protected void HandleProductionOrder(ProductionData p, bool ignoreStartUpChecks)
        {
            this.Log.LogInfo(this.GetType(), $"HandleProductionOrder: Production Type:{p.ProductionType} ");
            try
            {
                this.AutoBoxCount = 0;
                ApplicationSettings.ResetRecipeQtyOnOrdersScreen =
                    p.ProductionType.CompareIgnoringCaseAndWhitespace(Constant.Receipe);
                if (p.ProductionType.CompareIgnoringCaseAndWhitespace(Constant.Receipe))
                {
                    try
                    {
                        foreach (var item in p.ProductionDetails)
                        {
                            item.RecipeQuantityOrdered = item.QuantityOrdered;
                            item.RecipeWeightOrdered = item.WeightOrdered;
                        }

                        decimal percentageToApply = 0;
                        this.TypicalBatchSize = 0;
                        if (this.receipeProduct == null)
                        {
                            this.receipeProduct = p.ProductionDetails.FirstOrDefault(x => x.IsHeaderProduct.HasValue);
                        }

                        if (this.receipeProduct != null)
                        {
                            var localProduct = this.DataManager.GetRecipeOrderMethodByProductId(this.receipeProduct.INMasterID);
                            int? localOrderMethodId = 6;
                            if (localProduct != null)
                            {
                                this.AttributeReference = localProduct.Name;
                                localOrderMethodId = localProduct.NouOrderMethodID;
                            }

                            this.mixesRequired = this.receipeProduct.Mixes;
                            this.MixCount = this.receipeProduct.MixCount == 0 ? 1 : this.receipeProduct.MixCount;
                            if (this.MixCount > this.mixesRequired)
                            {

                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    NouvemMessageBox.Show(Message.RecipeProductionCompleteMessage, touchScreen: true);
                                }));
                               
                                return;
                            }

                            if (localOrderMethodId == 1)
                            {
                                this.TypicalBatchSize = this.receipeProduct.QuantityOrdered.ToDecimal();
                                this.receipeProduct.NouOrderMethod = Constant.Quantity;
                            }
                            else
                            {
                                this.TypicalBatchSize = this.receipeProduct.WeightOrdered.ToDecimal();
                                this.receipeProduct.NouOrderMethod = Constant.Receipe;
                            }

                            if (this.TypicalBatchSize <= 0)
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    SystemMessage.Write(MessageType.Issue, string.Format(Message.RecipeZeroQuantityIssue, this.TypicalBatchSize));
                                    NouvemMessageBox.Show(string.Format(Message.RecipeZeroQuantityIssue, this.TypicalBatchSize), touchScreen: true);
                                }));
                       
                                return;
                            }

                            this.receipeProduct.TypicalBatchSize = this.TypicalBatchSize;
                            var typicalMixQty = p.Order.TypicalMixQty.ToDecimal();
                            percentageToApply = typicalMixQty.ToDecimalGetPercentage(this.typicalBatchSize.ToDecimal());
                        }

                        if (this.MixCount > this.mixesRequired)
                        {
                            return;
                        }
                        if (this.MixCount == this.mixesRequired)
                        {
                            if (this.mixesRequired > 1)
                            {
                                // final mix, so set what's remaining
                                foreach (var item in p.ProductionDetails)
                                {
                                    var localMix = this.mixesRequired - 1;
                                    var localQty = item.QuantityOrdered.ToDecimal()
                                        .ToDecimalApplyPercentage(percentageToApply) * localMix;

                                    var localWgt = item.WeightOrdered.ToDecimal()
                                        .ToDecimalApplyPercentage(percentageToApply) * localMix;

                                    item.QuantityOrdered = (item.QuantityOrdered.ToDecimal() - localQty).ZeroIfNegative();
                                    item.WeightOrdered = (item.WeightOrdered.ToDecimal() - localWgt).ZeroIfNegative();
                                    item.QuantityIntoProduction = item.QuantityIntoProduction;
                                    item.WeightIntoProduction = item.WeightIntoProduction;
                                }
                            }
                        }
                        else if (percentageToApply > 0)
                        {
                            // apply typical mix to typical batch size percentage.
                            foreach (var item in p.ProductionDetails)
                            {
                                item.QuantityOrdered = item.QuantityOrdered.ToDecimal()
                                    .ToDecimalApplyPercentage(percentageToApply);

                                item.WeightOrdered = item.WeightOrdered.ToDecimal()
                                    .ToDecimalApplyPercentage(percentageToApply);
                                item.QuantityIntoProduction = item.QuantityIntoProduction;
                                item.WeightIntoProduction = item.WeightIntoProduction;
                            }
                        }

                        p.ProductionDetails = p.ProductionDetails.Where(x => x.IsHeaderProduct.IsNullOrFalse()).ToList();
                        //foreach (var item in p.ProductionDetails)
                        //{
                        //    if (!item.WeightIntoProduction.IsNullOrZero() || !item.QuantityIntoProduction.IsNullOrZero())
                        //    {
                        //        item.IgnoreRecipeTolerance = true;
                        //    }
                        //}

                        this.recipeDetails = p.ProductionDetails.ToList();
                        if (!ignoreStartUpChecks)
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                this.Locator.ProcessSelection.SetProcess(ProcessType.Recipe);
                            }));
                        }
                    }
                    finally
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            if (this.MainViewModel != this.Locator.IntoProductionMainGrid)
                            {
                                this.MainViewModel = this.Locator.IntoProductionMainGrid;
                            }
                        }));
                    }
                }
                else
                {
                    this.Locator.ProcessSelection.SetProcess(this.entryProcessType);
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing)
                {
                    if (p.Order.WarehouseID == NouvemGlobal.WarehouseButcheryId ||
                        p.Order.WarehouseID == NouvemGlobal.WarehouseOutOfButcheryId)
                    {
                        var localProcess = ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                            ? Strings.IntoSlicing
                            : Strings.IntoJointing;
                        var msg = string.Format(Message.ButcheryBatchAtSlicingJointingBlock, localProcess);
                        SystemMessage.Write(MessageType.Issue, msg);

                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(msg, touchScreen: true);
                        }));

                        return;
                    }
                }

                //this.ClearAttributes();
                this.ClearAttributeControls();
                this.SelectedOrder = p;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.SetOrderLinesStatus();
                });

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard)
                {
                    this.SelectedProductionDetail = null;
                }

                //if (this.selectedOrder != null)
                //{
                //    this.selectedOrder.LastBatchAttribute = this.DataManager.GetLastBatchAttributeData(this.selectedOrder.Order.BatchNumberID,
                //        NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.TransactionTypeProductionIssueId);
                //    this.selectedOrder.ProductionAttributesSet = false;
                //}

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
                {
                    this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault();
                    this.AttributeReference = string.Empty;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                    {
                        this.SelectedProductionDetail = null;
                    }));
                }
            }
            finally
            {
                if (!p.Order.AttributeTemplateRecordID.HasValue)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        // no haccp filled for this order, so check that none are required
                        var checks =
                            this.DataManager.GetProcessModuleStartUpChecks(
                                this.Locator.ProcessSelection.SelectedProcess.ProcessID, 0, p.Order.INMasterID.ToInt());

                        if (checks != null)
                        {
                            this.MissingProcessStartUpCheckMessage = string.Format(Message.HACCPNotCompleted, " this production", checks.Item2);
                            this.DisableModule = true;
                            this.Locator.Workflow.SetTemplate(checks.Item1, this.SelectedOrder.Order.PROrderID);
                            this.Locator.Touchscreen.CreateWorkflow();
                        }
                    }));
                }
            }
        }

        /// <summary>
        /// Verifys that a wgt is within tolerance levels.
        /// </summary>
        /// <param name="wgt">The current weight.</param>
        /// <param name="ordered">The product line order amt.</param>
        /// <param name="minusTolerance">The lower tolerance level.</param>
        /// <param name="plusTolerance">The upper tolerance level.</param>
        /// <returns>Flag, as to whether a weight falls within an acceptable tolerance range.</returns>
        private bool IsAmountWithinToleranceLevels(decimal wgt, decimal ordered, decimal currentWgt, decimal plusTolerance)
        {
            //var start = ordered - (ordered / 100 * minusTolerance);
            var end = ordered + (ordered / 100 * plusTolerance);

            return wgt + currentWgt <= end; //wgt >= start &&
        }
        
        /// <summary>
        /// Gets the recipe order methods.
        /// </summary>
        private void GetRecipeOrderMethods()
        {
            this.recipeOrderMethods = this.DataManager.GetRecipeOrderMethods();
        }

        /// <summary>
        /// Handles a toppings stock scan.
        /// </summary>
        /// <param name="data"></param>
        private void HandleRecipeReweigh(string data)
        {
            this.recipeLostWeightTransaction = this.DataManager.GetStockTransactionUncomsumed(data.ToInt());
            if (this.recipeLostWeightTransaction != null)
            {
                this.BatchNumber = new BatchNumber
                {
                    BatchNumberID = this.recipeLostWeightTransaction.BatchNumberID.ToInt(),
                    Number = this.recipeLostWeightTransaction.BatchNumberID.ToInt().ToString()
                };

                SystemMessage.Write(MessageType.Priority, Message.ReweighStockFound);
                if (this.receipeProduct?.INMasterID == this.recipeLostWeightTransaction.INMasterID)
                {
                    this.reworkStock = true;
                }

                var trans = this.HandleScannerDataReweigh(this.recipeLostWeightTransaction.TransactionWeight, false);
                if (trans != null)
                {
                    this.recipeLostWeightTransaction.StockTransactionID = trans.StockTransactionID;
                }
               
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.ReweighStockFound, touchScreen: true);
                }));
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.StockNotFound);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.StockNotFound, touchScreen: true);
                    }));
            }
        }

        private void CheckAutoBoxing()
        {
            if (this.AutoBoxCount == 0)
            {
                return;
            }

            if (this.AutoBoxCount == 1 && !this.SelectedProductionDetail.CompleteAutoBoxing)
            {
                //this.AutoBoxCount--;
                if (this.SelectedProductionDetail != null)
                {
                    this.SelectedProductionDetail.CompleteAutoBoxing = true;
                }

                this.IndicatorManager.IgnoreValidation(true);
                this.RecordWeight();
                this.IndicatorManager.IgnoreValidation(false);
                return;
            }

            if (this.SelectedProductionDetail != null)
            {
                if (this.SelectedProductionDetail.CompleteAutoBoxing)
                {
                    this.SelectedProductionDetail.QuantityIntoProduction =
                        this.SelectedProductionDetail.QuantityIntoProduction.ToDecimal() +
                        this.SelectedProductionDetail.AutoBoxQty;
                    this.SelectedProductionDetail.CompleteAutoBoxing = false;
                }
            }

            SystemMessage.Write(MessageType.Priority, "Label scanned");
            this.AutoBoxCount--;
        }

        /// <summary>
        /// Reworks left over stock into the batch.
        /// </summary>
        private void ReworkRecipeStock()
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductionBatchSelected);
                return;
            }

            #endregion

            int? prOrderId = this.SelectedOrder.Order.ReworkNonAdjust.IsTrue() ? this.SelectedOrder.Order.PROrderID : (int?) null;

            try
            {
                var reworkWgt = this.indicatorWeight;
                var reworkQty = this.pieces;
                var product = this.GetProductName(this.receipeProduct.INMasterID);

                var msg = string.Format(Message.ReworkStockPrompt, reworkQty, reworkWgt, product);
                NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                var transaction = this.HandleScannerDataReweigh(this.indicatorWeight, true, prOrderId);
                var reworked = string.Empty;
                if (!prOrderId.HasValue)
                {
                    reworked = this.DataManager.ReworkRecipeStock(transaction.StockTransactionID, this.SelectedOrder.Order.PROrderID, NouvemGlobal.DeviceId.ToInt(),
                        NouvemGlobal.UserId.ToInt());
                }

                if (string.IsNullOrEmpty(reworked))
                {
                    SystemMessage.Write(MessageType.Priority, Message.StockReworkedIntoBatch);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.StockReworkedIntoBatch, flashMessage: true);
                    }));

                    this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
                    this.Locator.TouchscreenProductionOrders.SetOrder(this.SelectedOrder.Order.PROrderID);
                    this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, reworked);
                }
            }
            finally
            {
                this.reworkStock = false;
                if (this.ScannerStockMode != ScannerMode.Scan)
                {
                    this.ScannerStockMode = ScannerMode.Scan;
                }
            }
        }

        /// <summary>
        /// Handles the batch scan.
        /// </summary>
        /// <param name="data">The label scanned data.</param>
        private void HandleScannerDataForIntakeBatchSelection(int productId, int batchId)
        {
            this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == productId);
            if (this.SelectedProductionDetail == null)
            {
                var msg = string.Format(Message.WrongBatchProductScannedAtIntoProduction, this.GetProductName(productId));
                SystemMessage.Write(MessageType.Issue, msg);
                NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                var localProduct =
                    NouvemGlobal.InventoryItems.FirstOrDefault(x => x.INMasterID == productId);
                if (localProduct == null)
                {
                    return;
                }

                this.HandleProductSelection(localProduct);
                this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == productId);
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.Locator.TouchscreenIntakeBatches.GetAllOrdersNoThread(productId);
                this.Locator.TouchscreenIntakeBatches.SelectedOrder
                    =
                    this.Locator.TouchscreenIntakeBatches.Orders.FirstOrDefault(
                        x => x.BatchID == batchId);

                if (this.Locator.TouchscreenIntakeBatches.SelectedOrder != null)
                {
                    this.Locator.TouchscreenIntakeBatches.HandleSelectedOrder();
                }
                else
                {
                    ViewModelLocator.ClearTouchscreenIntakeBatches();
                }
            }));
        }

        private void ResetRecipeAmounts(decimal baseQty)
        {
            var localQty = this.TypicalBatchSize.ToDecimal();

            this.receipeProduct.Mixes =
                localQty.ToMixCount(this.SelectedOrder.Order.TypicalMixQty.ToDecimal());
            this.receipeProduct.QuantityOrdered = localQty;

            var percentage = Math.Round(((localQty / baseQty) * 100).ToDecimal(), 2);
            foreach (var ingredient in this.productionDetails)
            {
                ingredient.QuantityOrdered = Math.Round((ingredient.RecipeQuantityOrdered.ToDecimal() / 100) * percentage, 0);
                ingredient.WeightOrdered = Math.Round((ingredient.RecipeWeightOrdered.ToDecimal() / 100) * percentage, 2);
            }

            this.SelectedOrder.Order.BatchWeight = localQty;
            var localDetails = this.productionDetails;
            localDetails.Add(this.receipeProduct);
            this.SelectedOrder.ProductionDetails = localDetails;
            this.SelectedOrder.BatchWeight = localQty;
            if (this.DataManager.UpdateRecipeOrderAmounts(this.SelectedOrder))
            {
                this.Locator.TouchscreenProductionOrders.SelectedOrder =
                    this.Locator.TouchscreenProductionOrders.AllOrders.FirstOrDefault(x =>
                        x.Order.PROrderID == this.SelectedOrder.Order.PROrderID);
                this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                SystemMessage.Write(MessageType.Priority, Message.RecipeQtyChanged);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.RecipeQtyChanged, flashMessage: true);
                }));
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue,Message.RecipeQtyChangeError);
                    NouvemMessageBox.Show(Message.RecipeQtyChangeError, flashMessage: true);
                }));
            }
        }
        
        #endregion

        #endregion
    }
}


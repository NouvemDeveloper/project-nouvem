﻿// -----------------------------------------------------------------------
// <copyright file="BatchEditViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using DevExpress.Xpf.Grid;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Production
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class BatchEditViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// The batches to edit.
        /// </summary>
        private HashSet<Sale> batchesToUpdate = new HashSet<Sale>();

        /// <summary>
        /// The products.
        /// </summary>
        private ObservableCollection<InventoryItem> products;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchEditViewModel"/> class.
        /// </summary>
        public BatchEditViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.TransactionEditingComplete, s => this.Refresh());

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                if (string.IsNullOrWhiteSpace(s))
                {
                    return;
                }

                if (this.FilteredSales != null && this.FilteredSales.Any() && this.FilteredSales.Last().SaleID == 0)
                {
                    this.FilteredSales.Last().PopUpNote = s;
                    return;
                }

                if (this.SelectedSale != null)
                {
                    this.SelectedSale.PopUpNote = s;
                    this.SetControlMode(ControlMode.Update);

                    var localBatch = this.batchesToUpdate.FirstOrDefault(x => x.SaleID == this.SelectedSale.SaleID);
                    if (localBatch != null)
                    {
                        this.batchesToUpdate.Remove(localBatch);
                    }

                    this.batchesToUpdate.Add(this.SelectedSale);
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowBatchEditNotesWindow, s => this.ShowNotesWindow());

            #endregion

            #region command registration

            this.NewRowCommmand = new RelayCommand(() =>
            {
                this.SelectedSale = null;

            });

            this.UpdateCommmand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                if (e != null && e.Row == null)
                {
                    return;
                }

                if (this.SelectedSale != null && (e.Cell.Property.Equals("BaseDocumentReferenceID") || e.Cell.Property.Equals("BaseDocumentReferenceID2") ||
                     e.Cell.Property.Equals("BaseDocumentReferenceID3")))
                {
                    if (e.Cell.Property.Equals("BaseDocumentReferenceID"))
                    {
                        var intake = this.Intakes.FirstOrDefault(x => x.SaleID == (int?)e.Value);
                        if (intake != null)
                        {
                            var localSupplier =
                               this.Suppliers.FirstOrDefault(x => x.BPMasterID == intake.BPMasterID);
                            if (localSupplier != null)
                            {
                                this.SelectedSale.BPMasterID = localSupplier.BPMasterID;
                            }
                        }
                    }

                    if (e.Cell.Property.Equals("BaseDocumentReferenceID2"))
                    {
                        var intake = this.Intakes.FirstOrDefault(x => x.SaleID == (int?)e.Value);
                        if (intake != null)
                        {
                            var localSupplier =
                                this.Suppliers.FirstOrDefault(x => x.BPMasterID == intake.BPMasterID);
                            if (localSupplier != null)
                            {
                                this.SelectedSale.ReferenceID = localSupplier.BPMasterID;
                            }
                        }
                    }

                    if (e.Cell.Property.Equals("BaseDocumentReferenceID3"))
                    {
                        var intake = this.Intakes.FirstOrDefault(x => x.SaleID == (int?)e.Value);
                        if (intake != null)
                        {
                            var localSupplier =
                                this.Suppliers.FirstOrDefault(x => x.BPMasterID == intake.BPMasterID);
                            if (localSupplier != null)
                            {
                                this.SelectedSale.ReferenceID2 = localSupplier.BPMasterID;
                            }
                        }
                    }

                    var localWorkFlow = this.batchesToUpdate.FirstOrDefault(x => x.SaleID == this.SelectedSale.SaleID);
                    if (localWorkFlow != null)
                    {
                        this.batchesToUpdate.Remove(localWorkFlow);
                    }

                    this.batchesToUpdate.Add(this.SelectedSale);
                    this.SetControlMode(ControlMode.Update);
                    return;
                }

                if (this.SelectedSale != null && this.SelectedSale.NouDocStatusID.HasValue)
                {
                    if ((e.Cell.Property.Equals("ColdWeight") || e.Cell.Property.Equals("Quantity") || e.Cell.Property.Equals("INMasterID")))
                    {
                        if (!ApplicationSettings.CanEditBatches)
                        {
                            return;
                        }

                        this.SelectedSale.EditingBatches = true;
                    }

                    var localWorkFlow = this.batchesToUpdate.FirstOrDefault(x => x.SaleID == this.SelectedSale.SaleID);
                    if (localWorkFlow != null)
                    {
                        this.batchesToUpdate.Remove(localWorkFlow);
                    }

                    this.batchesToUpdate.Add(this.SelectedSale);
                }

                this.SetControlMode(ControlMode.Update);
            });

            this.RefreshCommand = new RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            });

            #endregion

            ApplicationSettings.TouchScreenMode = false;
            this.FromDate = ApplicationSettings.BatchEditSearchFromDate;
            if (this.FromDate > DateTime.Today)
            {
                this.ToDate = this.FromDate;
            }
            else
            {
                this.ToDate = DateTime.Today;
            }

            this.ShowAllOrders = ApplicationSettings.BatchEditSearchShowAllOrders;
            this.FormName = Strings.BatchEdit;
            this.HasWriteAuthorisation = true;
            this.GetProducts();
            this.GetIntakes();
            this.GetISuppliers();
            this.GetProcesses();

            this.NouDocStatus = new List<NouDocStatu>
            {
                NouvemGlobal.NouDocStatusActive, 
                NouvemGlobal.NouDocStatusComplete, 
                NouvemGlobal.NouDocStatusCancelled
            };
        }

        #endregion

        #region public interface

        #region property

        public bool SearchMode { get; set; }

        /// <summary>
        /// Gets or sets the intakes.
        /// </summary>
        public IList<Sale> Intakes { get; set; }

        /// <summary>
        /// Gets or sets the isuppliers.
        /// </summary>
        public IList<ViewBusinessPartner> Suppliers { get; set; }

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public ObservableCollection<InventoryItem> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.products = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the doc statuses.
        /// </summary>
        public IList<NouDocStatu> NouDocStatus { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand NewRowCommmand { get; private set; }

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommmand { get; private set; }

        /// <summary>
        /// Gets the refresh command.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void HandleSelectedSale(Sale sale)
        {
            this.SetSale(sale);
            this.RaisePropertyChanged("SelectedSale");
        }

        /// <summary>
        /// Drill down to the transaction editor.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            #region validation

            if (this.SelectedSale == null)
            {
                return;
            }

            if (!this.AuthorisationsManager.AllowUserToEditTransactions)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                return;
            }

            #endregion

            this.Locator.EditTransaction.SetTransactions(ViewType.Production, this.SelectedSale.SaleID);
            Messenger.Default.Send(ViewType.TransactionEditor);
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.Close();
                    break;

                case ControlMode.Update:
                    var salesToAdd = this.FilteredSales.Where(x => x.SaleID == 0).ToList();

                    if (salesToAdd.Any())
                    {
                        this.batchesToUpdate.Clear();
                    }

                    foreach (var sale in salesToAdd)
                    {
                        var localInMasterId = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Name == sale.INMaster);
                        if (localInMasterId != null)
                        {
                            sale.INMasterID = localInMasterId.Master.INMasterID;
                        }

                        if (localInMasterId != null)
                        {
                            sale.INMasterID = localInMasterId.Master.INMasterID;
                        }

                        var defaultProcess = this.Processes.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Injection));
                        if (defaultProcess != null)
                        {
                            sale.ProcessID = defaultProcess.ProcessID;
                        }

                        this.batchesToUpdate.Add(sale);
                    }

                    if (this.batchesToUpdate.Any())
                    {
                        foreach (var sale in this.batchesToUpdate)
                        {
                            // we can't use the INMasterID on the UI as it exports to the report rather than the name.
                            var localInMasterId = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Name == sale.INMaster);
                            if (localInMasterId != null)
                            {
                                sale.INMasterID = localInMasterId.Master.INMasterID;
                            }
                        }

                        if (this.DataManager.UpdateBatches(this.batchesToUpdate))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                            this.batchesToUpdate.Clear();
                            this.Refresh();
                            this.SetControlMode(ControlMode.OK);
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale != null)
            {
                if (this.SearchMode)
                {
                    Messenger.Default.Send(this.SelectedSale, Token.ProductionSelected);
                    this.SearchMode = false;
                    this.Close();
                    return;
                }

                this.IsBusy = true;
                Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(150);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        var localBatch = this.DataManager.GetPROrderById(this.SelectedSale.SaleID);
                        if (localBatch == null)
                        {
                            return;
                        }

                        localBatch.ProductionDetails =
                              this.DataManager.GetStockMovementTransactionData(localBatch.Order.PROrderID);

                        ApplicationSettings.TouchScreenMode = true;
                        this.Locator.Touchscreen.SetModule(ViewType.StockMovement);
                        this.Locator.Touchscreen.PanelViewModel = this.Locator.StockMovementPanel;
                        ViewModelLocator.TouchscreenStatic.Module = Strings.StockMovement;
                        Messenger.Default.Send(localBatch, Token.ProductionBatchSelected);
                        Messenger.Default.Send(ViewType.FactoryTouchScreen);

                        if (!this.keepVisible)
                        {
                            this.Close();
                        }
                    });
                }).ContinueWith(task => this.IsBusy = false);

            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.GetBatches();
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            ApplicationSettings.BatchEditSearchFromDate = this.FromDate;
            ApplicationSettings.BatchEditSearchShowAllOrders = this.ShowAllOrders;
            this.IsFormLoaded = false;

            this.Close();
        }


        /// <summary>
        /// Refreshes the lairages.
        /// </summary>
        protected override void Refresh()
        {
            this.GetBatches();
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        protected override void ShowNotesWindow()
        {
            if (this.SelectedSale == null || this.SelectedSale.SaleID == 0)
            {
                return;
            }

            var notes = this.SelectedSale?.PopUpNote;
            if (ApplicationSettings.AccumulateNotesAtBatchEdit)
            {
                notes = string.Empty;
            }
           
            Messenger.Default.Send(notes, Token.DisplayNotes);
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearBatchEdit();
            Messenger.Default.Send(Token.Message, Token.CloseBatchEditWindow);
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the intakes.
        /// </summary>
        private void GetIntakes()
        {
            this.Intakes = this.DataManager.GetAllAPReceiptsForBatchEdit();
        }

        /// <summary>
        /// Gets the suppliers.
        /// </summary>
        private void GetISuppliers()
        {
            this.Suppliers = NouvemGlobal.SupplierPartners.Select(x => x.Details).OrderBy(x => x.Name).ToList();
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        private void GetProducts()
        {
            this.Products = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems.Where(x => x.Master.ProductionProduct == true));
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        private void GetBatches()
        {
            var docStatusesAll = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID,
            };

            var docStatusesActive = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID
            };

            if (this.ShowAllOrders)
            {
                var localBatches = this.DataManager.GetAllBatches(docStatusesAll, this.FromDate.Date, this.ToDate.Date)
                    .OrderByDescending(x => x.CreationDate).ToList();
                foreach (var localBatch in localBatches)
                {
                    var loc = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == localBatch.INMasterID);
                    if (loc != null)
                    {
                        localBatch.INMaster = loc.Name;
                    }

                    if (localBatch.PopUpNote.Replace(",", "").Replace(" ", "") == string.Empty)
                    {
                        localBatch.PopUpNote = string.Empty;
                        localBatch.Notes = null;
                    }
                }

                this.FilteredSales = new ObservableCollection<Sale>(localBatches);
            }
            else
            {
                var localBatches = this.DataManager
                    .GetAllBatches(docStatusesActive, this.FromDate.Date, this.ToDate.Date)
                    .OrderByDescending(x => x.CreationDate).ToList();
                foreach (var localBatch in localBatches)
                {
                    var loc = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == localBatch.INMasterID);
                    if (loc != null)
                    {
                        localBatch.INMaster = loc.Name;
                    }

                    if (localBatch.PopUpNote.Replace(",", "").Replace(" ", "") == string.Empty)
                    {
                        localBatch.PopUpNote = string.Empty;
                        localBatch.Notes = null;
                    }
                }

                this.FilteredSales = new ObservableCollection<Sale>(localBatches);
            }
        }

        #endregion
    }
}





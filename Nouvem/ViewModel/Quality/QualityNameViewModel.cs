﻿// -----------------------------------------------------------------------
// <copyright file="QualityNameViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Quality
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class QualityNameViewModel : NouvemViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the QualityNameViewModel class.
        /// </summary>
        public QualityNameViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.QualityTemplateNames = new ObservableCollection<QualityTemplateName>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessGS1);
            this.GetQualityTemplateNames();
        }

        #endregion

        #region property

        /// <summary>
        /// Get the date Masters.
        /// </summary>
        public ObservableCollection<QualityTemplateName> QualityTemplateNames { get; private set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateQualityTemplateNames();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the date template names group.
        /// </summary>
        private void UpdateQualityTemplateNames()
        {
            try
            {
                if (this.DataManager.AddOrUpdateQualityTemplateNames(this.QualityTemplateNames))
                {
                    SystemMessage.Write(MessageType.Priority, Message.QualityTemplateNamesUpdated);
                    Messenger.Default.Send(Token.Message, Token.QualityTemplateNamesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.QualityTemplateNamesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.QualityTemplateNamesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearQualityName();
            Messenger.Default.Send(Token.Message, Token.CloseQualityTemplateNameWindow);
        }

        /// <summary>
        /// Gets the application date template names information.
        /// </summary>
        private void GetQualityTemplateNames()
        {
            this.QualityTemplateNames.Clear();
            foreach (var date in this.DataManager.GetQualityTemplateNames())
            {
                this.QualityTemplateNames.Add(date);
            }
        }

        #endregion
    }
}


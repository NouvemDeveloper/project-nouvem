﻿// -----------------------------------------------------------------------
// <copyright file="QualityViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using DevExpress.Xpf.Grid;

namespace Nouvem.ViewModel.Quality
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class QualityViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected quality master.
        /// </summary>
        private ViewQualityMaster selectedQualityMaster;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the QualityViewModel class.
        /// </summary>
        public QualityViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);

            #region message registration

            Messenger.Default.Register<string>(this, Token.SqlScript, this.HandleIncomingSql);

            Messenger.Default.Register<string>(this, Token.DataGenerated, this.HandleIncomingData);

            Messenger.Default.Register<string>(this, Token.ShowSqlWindow, s => this.ShowSqlWindow());

            Messenger.Default.Register<string>(this, Token.ShowColWindow, s => this.ShowColWindow());

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>(this.UpdateCommandExecute);

            #endregion

            #region instantiation

            this.QualitysMasters = new ObservableCollection<ViewQualityMaster>();

            #endregion

            this.GetQualitysMasters();
            this.GetGs1Masters();
            this.GetQualityTypes();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the selected quality master.
        /// </summary>
        public ViewQualityMaster SelectedQualityMaster
        {
            get
            {
                return this.selectedQualityMaster;
            }

            set
            {
                this.selectedQualityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the dates Masters.
        /// </summary>
        public ObservableCollection<ViewQualityMaster> QualitysMasters { get; private set; }

        /// <summary>
        /// Get the gs1 Masters.
        /// </summary>
        public ObservableCollection<ViewGs1Master> Gs1AiMasters { get; private set; }

        /// <summary>
        /// Get the system date types.
        /// </summary>
        public ObservableCollection<NouQualityType> QualityTypes { get; private set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateQualityMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command

        /// <summary>
        /// Handle a cell value change.
        /// </summary>
        /// <param name="e">The event arg.</param>
        private void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            if (!this.CanUpdate())
            {
                return;
            }

            this.SetControlMode(ControlMode.Update);

            if (e.Cell.Property.Equals("NouQualityTypeID"))
            {
                this.ClearData((int)e.Value);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Clears the sql and collection data on traceability type change.
        /// </summary>
        private void ClearData(int newValueId)
        {
            /* ViewQuyalityMaster is an entity object, so we can't implement the INotifyPropertyChanged interface to update immediately.
             * This more long winded approach works, but is not ideal */
            var pos = 0;
            var localItem = this.selectedQualityMaster;
            for (int i = 0; i < this.QualitysMasters.Count; i++)
            {
                var item = this.QualitysMasters.ElementAt(i);
                if (item.QualityMasterID ==
                    this.selectedQualityMaster.QualityMasterID)
                {
                    localItem.SQL = string.Empty;
                    localItem.Collection = string.Empty;
                    localItem.NouQualityTypeID = newValueId;
                    pos = i;
                    break;
                }
            }

            if (pos > 0)
            {
                this.QualitysMasters.Remove(this.selectedQualityMaster);
                this.QualitysMasters.Insert(pos, localItem);
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowSqlWindow()
        {
            if (this.SelectedQualityMaster == null || this.SelectedQualityMaster.NouQualityTypeID == 0)
            {
                return;
            }

            var traceability = this.SelectedQualityMaster;
            if (traceability.NouQualityTypeID !=
                    NouvemGlobal.QualityTypes.First(x => x.QualityType.Equals(Constant.SQL)).NouQualityTypeID)
            {
                // Non sql type. Exit.
                SystemMessage.Write(MessageType.Issue, Message.NonSqlTraceabilityType);
                return;
            }

            var sql = this.selectedQualityMaster.SQL ?? string.Empty;
            Messenger.Default.Send(sql, Token.DisplaySql);
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowColWindow()
        {
            if (this.SelectedQualityMaster == null || this.SelectedQualityMaster.NouQualityTypeID == 0)
            {
                return;
            }

            var traceability = this.SelectedQualityMaster;
            if (traceability.NouQualityTypeID !=
                    NouvemGlobal.QualityTypes.First(x => x.QualityType.Equals(Constant.Collection)).NouQualityTypeID)
            {
                // Non collection type. Exit.
                SystemMessage.Write(MessageType.Issue, Message.NonCollectionDataType);
                return;
            }

            var data = this.selectedQualityMaster.Collection ?? string.Empty;
            Messenger.Default.Send(data, Token.DisplayCol);
        }

        /// <summary>
        /// Updates the date master group.
        /// </summary>
        private void UpdateQualityMaster()
        {
            #region validation

            foreach (var quality in this.QualitysMasters)
            {
                if (quality.NouQualityTypeID == 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoTypeSelected);
                    return;
                }

                if (string.IsNullOrWhiteSpace(quality.QualityCode))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoCodeSelected);
                    return;
                }

                if (quality.NouQualityTypeID ==
                   NouvemGlobal.QualityTypes.First(x => x.QualityType.Equals(Constant.SQL)).NouQualityTypeID)
                {
                    // If it's a sql type, ensure that a valid sql statement has been entered.
                    if (string.IsNullOrWhiteSpace(quality.SQL))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.SqlNotEntered);
                        return;
                    }
                }

                if (quality.NouQualityTypeID ==
                    NouvemGlobal.QualityTypes.First(x => x.QualityType.Equals(Constant.Collection)).NouQualityTypeID)
                {
                    // If it's a sql type, ensure that a valid sql statement has been entered.
                    if (string.IsNullOrWhiteSpace(quality.Collection))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.CollectionDataNotEntered);
                        return;
                    }
                }
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdateQualityMasters(this.QualitysMasters))
                {
                    SystemMessage.Write(MessageType.Priority, Message.QualityMasterUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.QualityMasterNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.QualityMasterNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearQuality();
            Messenger.Default.Send(Token.Message, Token.CloseQualityWindow);
        }

        /// <summary>
        /// Gets the application dates masters information.
        /// </summary>
        private void GetQualitysMasters()
        {
            this.QualitysMasters.Clear();
            foreach (var date in this.DataManager.GetQualityMasters())
            {
                this.QualitysMasters.Add(date);
            }
        }

        /// <summary>
        /// Gets the application Gs1 information.
        /// </summary>
        private void GetGs1Masters()
        {
            this.Gs1AiMasters = new ObservableCollection<ViewGs1Master>(this.DataManager.GetGs1MasterView());
        }

        /// <summary>
        /// Gets the application date types information.
        /// </summary>
        private void GetQualityTypes()
        {
            this.QualityTypes = new ObservableCollection<NouQualityType>(this.DataManager.GetQualityTypes());
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        /// <summary>
        /// Handles the incoming sql generation.
        /// </summary>
        /// <param name="sql">The incoming sql.</param>
        private void HandleIncomingSql(string sql)
        {
            /* ViewTraceabilityMaster is an entity object, so we can't implement the INotifyPropertyChanged interface to update immediately.
             * This more long winded approach works, but is not ideal */
            var pos = 0;
            var localItem = this.selectedQualityMaster;
            for (int i = 0; i < this.QualitysMasters.Count; i++)
            {
                var item = this.QualitysMasters.ElementAt(i);
                if (item.QualityMasterID ==
                    this.selectedQualityMaster.QualityMasterID)
                {
                    localItem.SQL = sql;
                    pos = i;
                    break;
                }
            }

            if (pos > 0)
            {
                this.QualitysMasters.Remove(this.selectedQualityMaster);
                this.QualitysMasters.Insert(pos, localItem);
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Handles the incoming data generation.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        private void HandleIncomingData(string data)
        {
            /* ViewTraceabilityMaster is an entity object, so we can't implement the INotifyPropertyChanged interface to update immediately.
             * This more long winded approach works, but is not ideal */
            var pos = 0;
            var localItem = this.selectedQualityMaster;
            for (int i = 0; i < this.QualitysMasters.Count; i++)
            {
                var item = this.QualitysMasters.ElementAt(i);
                if (item.QualityMasterID ==
                    this.selectedQualityMaster.QualityMasterID)
                {
                    localItem.Collection = data;
                    pos = i;
                    break;
                }
            }

            if (pos > 0)
            {
                this.QualitysMasters.Remove(this.selectedQualityMaster);
                this.QualitysMasters.Insert(pos, localItem);
                this.SetControlMode(ControlMode.Update);
            }
        }

        #endregion

        #endregion
    }
}

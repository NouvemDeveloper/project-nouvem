﻿// -----------------------------------------------------------------------
// <copyright file="QualityTemplateAllocationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.ViewModel.Quality
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class QualityTemplateAllocationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The template names reference.
        /// </summary>
        private ObservableCollection<QualityTemplateName> dateTemplateNames;

        /// <summary>
        /// The selected template name
        /// </summary>
        private QualityTemplateName selectedTemplateName;

        /// <summary>
        /// The selected available date reference.
        /// </summary>
        private ViewQualityMaster selectedAvailableQualityMaster;

        /// <summary>
        /// The selected assigned date reference.
        /// </summary>
        private ViewQualityMaster selectedAssignedQualityMaster;

        /// <summary>
        /// The available date masters reference.
        /// </summary>
        private ObservableCollection<ViewQualityMaster> availableQualitys;

        /// <summary>
        /// The assigned date masters reference.
        /// </summary>
        private ObservableCollection<ViewQualityMaster> assignedQualitys;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the QualityTemplateAllocationViewModel class.
        /// </summary>
        public QualityTemplateAllocationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // register fot a date master data change.
            Messenger.Default.Register<string>(this, Token.QualityTemplateNamesUpdated, s => this.GetQualityTemplateNames(true));

            #endregion

            #region command registration

            // Command to move the selected date.
            this.MoveQualityCommand = new RelayCommand<string>(this.MoveQualityCommandExecute);

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.QualityTemplateNames = new ObservableCollection<QualityTemplateName>();
            this.QualityMasters = new ObservableCollection<ViewQualityMaster>();
            this.AvailableQualityMasters = new ObservableCollection<ViewQualityMaster>();
            this.AssignedQualityMasters = new ObservableCollection<ViewQualityMaster>();
            this.QualitysTemplateAllocations = new List<QualityTemplateAllocation>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);
            this.GetQualitysMasters();
            this.GetTemplateAllocations();
            this.GetQualityTemplateNames();
        }

        #endregion

        #region property

        /// <summary>
        /// Get the dates template allocations.
        /// </summary>
        public IList<QualityTemplateAllocation> QualitysTemplateAllocations { get; private set; }

        /// <summary>
        /// Get the dates Masters.
        /// </summary>
        public ObservableCollection<ViewQualityMaster> QualityMasters { get; private set; }

        /// <summary>
        /// Get or sets the available dates Masters.
        /// </summary>
        public ObservableCollection<ViewQualityMaster> AvailableQualityMasters
        {
            get
            {
                return this.availableQualitys;
            }

            set
            {
                this.availableQualitys = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the assigned dates Masters.
        /// </summary>
        public ObservableCollection<ViewQualityMaster> AssignedQualityMasters
        {
            get
            {
                return this.assignedQualitys;
            }

            set
            {
                this.assignedQualitys = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the date Masters.
        /// </summary>
        public ObservableCollection<QualityTemplateName> QualityTemplateNames
        {
            get
            {
                return this.dateTemplateNames;
            }

            set
            {
                this.dateTemplateNames = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected date template name.
        /// </summary>
        public QualityTemplateName SelectedTemplateName
        {
            get
            {
                return this.selectedTemplateName;
            }

            set
            {
                this.selectedTemplateName = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedTemplateName();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected available date master.
        /// </summary>
        public ViewQualityMaster SelectedAvailableQualityMaster
        {
            get
            {
                return this.selectedAvailableQualityMaster;
            }

            set
            {
                this.selectedAvailableQualityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected assigned date master.
        /// </summary>
        public ViewQualityMaster SelectedAssignedQualityMaster
        {
            get
            {
                return this.selectedAssignedQualityMaster;
            }

            set
            {
                this.selectedAssignedQualityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to move the dates.
        /// </summary>
        public ICommand MoveQualityCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// drill down to the date names.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.QualityTemplateName, 0), Token.DrillDown);
            this.AvailableQualityMasters.Clear();
            this.AssignedQualityMasters.Clear();
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    //this.UpdateQualityTemplateNames();
                    this.UpdateTemplate();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the current template.
        /// </summary>
        private void UpdateTemplate()
        {
            if (this.DataManager.AddQualitysToTemplate(this.AssignedQualityMasters,
                this.selectedTemplateName.QualityTemplateNameID))
            {
                SystemMessage.Write(MessageType.Priority, Message.QualityAddedToTemplate);
                this.Refresh();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.QualityNotAddedToTemplate);
            }
        }

        /// <summary>
        /// Updates the date template names group.
        /// </summary>
        private void UpdateQualityTemplateNames()
        {
            try
            {
                if (this.DataManager.AddOrUpdateQualityTemplateNames(this.QualityTemplateNames))
                {
                    SystemMessage.Write(MessageType.Priority, Message.QualityTemplateNamesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.QualityTemplateNamesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.QualityTemplateNamesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearQualityTemplateAllocation();
            Messenger.Default.Send(Token.Message, Token.CloseQualityTemplateAllocationWindow);
        }

        /// <summary>
        /// Gets the application date template names information.
        /// </summary>
        /// <param name="selectRecentlyAdded">Flg to determine if we are selecting the most recently added template.</param>
        private void GetQualityTemplateNames(bool selectRecentlyAdded = false)
        {
            this.QualityTemplateNames.Clear();
            foreach (var date in this.DataManager.GetQualityTemplateNames())
            {
                this.QualityTemplateNames.Add(date);
            }

            this.QualityTemplateNames.Add(new QualityTemplateName { Name = Strings.DefineNew });

            // Select the first, or most recently added template.
            this.SelectedTemplateName = selectRecentlyAdded
                ? this.QualityTemplateNames.ElementAt(this.dateTemplateNames.Count - 2)
                : this.QualityTemplateNames.First();
        }

        /// <summary>
        /// Handles the selection of a date template name.
        /// </summary>
        private void HandleSelectedTemplateName()
        {
            // check if we are defining a new template name
            if (this.selectedTemplateName.Name.Equals(Strings.DefineNew))
            {
                // new..shortcut to the template name view.
                Messenger.Default.Send(ViewType.QualityTemplateName);
                this.AvailableQualityMasters.Clear();
                this.AssignedQualityMasters.Clear();
                return;
            }

            this.AvailableQualityMasters.Clear();
            this.AssignedQualityMasters.Clear();
            this.AvailableQualityMasters = new ObservableCollection<ViewQualityMaster>(this.QualityMasters);

            this.QualitysTemplateAllocations.ToList().ForEach(x =>
            {
                if (x.QualityTemplateNameID == this.selectedTemplateName.QualityTemplateNameID)
                {
                    var localQualityMaster =
                        this.QualityMasters.FirstOrDefault(dateMaster => dateMaster.QualityMasterID == x.QualityMasterID);

                    if (localQualityMaster != null)
                    {
                        localQualityMaster.Batch = x.Batch.ToBool();
                        localQualityMaster.Transaction = x.Transaction.ToBool();
                        localQualityMaster.Required = x.Required.ToBool();
                        localQualityMaster.Reset = x.Reset.ToBool();
                        this.AssignedQualityMasters.Add(localQualityMaster);
                        this.AvailableQualityMasters.Remove(localQualityMaster);
                    }
                }
            });
        }

        /// <summary>
        /// Gets the application dates masters information.
        /// </summary>
        private void GetQualitysMasters()
        {
            this.QualityMasters.Clear();
            foreach (var date in this.DataManager.GetQualityMasters())
            {
                this.QualityMasters.Add(date);
            }
        }

        /// <summary>
        /// Gets the date template allocations.
        /// </summary>
        private void GetTemplateAllocations()
        {
            this.QualitysTemplateAllocations = this.DataManager.GetQualityTemplateAllocations();
        }

        /// <summary>
        /// handles the allocation/deallocation of dates to and from the selected template.
        /// </summary>
        /// <param name="direction">The allocation direction.</param>
        private void MoveQualityCommandExecute(string direction)
        {
            #region validation

            if (!this.CanMoveQuality(direction))
            {
                return;
            }

            #endregion

            this.SetControlMode(ControlMode.Update);

            if (direction.Equals(Constant.MoveForward))
            {
                this.AssignedQualityMasters.Add(this.selectedAvailableQualityMaster);
                this.AvailableQualityMasters.Remove(this.selectedAvailableQualityMaster);
      
                return;
            }

            this.AvailableQualityMasters.Add(this.selectedAssignedQualityMaster);
            this.AssignedQualityMasters.Remove(this.selectedAssignedQualityMaster);
        }

        /// <summary>
        /// Determines if a move can be made.
        /// </summary>
        /// <returns>A flag, which indicates whether a move can be made.</returns>
        private bool CanMoveQuality(string direction)
        {
            if (direction.Equals(Constant.MoveForward) && this.selectedAvailableQualityMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            if (direction.Equals(Constant.MoveBack) && this.selectedAssignedQualityMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetTemplateAllocations();
            this.SetControlMode(ControlMode.OK);
        }

        #endregion
    }
}



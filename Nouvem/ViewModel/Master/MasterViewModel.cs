// -----------------------------------------------------------------------
// <copyright file="MasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;
using Nouvem.Shared;
using System.ComponentModel;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Master
{
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.ReportService;
    using Nouvem.Shared.Localisation;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// The master view model.
    /// </summary>
    public class MasterViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// Field to keep track of the open windows.
        /// </summary>
        private static int windowsOpen;

        /// <summary>
        /// The report menu item.
        /// </summary>
        private ApplicationMenu reports;

        /// <summary>
        /// The report menu item nodes.
        /// </summary>
        private ObservableCollection<ApplicationMenu> reportMenuModes = new ObservableCollection<ApplicationMenu>();

        /// <summary>
        /// The currently selected view model reference.
        /// </summary>
        private NouvemViewModelBase currentViewModel;

        /// <summary>
        /// The main menu item reference.
        /// </summary>
        private ApplicationMenu applicationMenu = new ApplicationMenu();

        /// <summary>
        /// The report service web service helper object reference.
        /// </summary>
        private ReportingService2010 reportingService = new ReportingService2010();

        /// <summary>
        /// The report menu item indent.
        /// </summary>
        private int indent = 30;

        /// <summary>
        /// The report id.
        /// </summary>
        private int nodeId = 1;

        /// <summary>
        /// The ribbon tab enabled field.
        /// </summary>
        private bool enableRibbonBar;

        /// <summary>
        /// The name of the administration module.
        /// </summary>
        private string administrationName;

        /// <summary>
        /// The name of the invoice module.
        /// </summary>
        private string invoiceName;

        /// <summary>
        /// The name of the invoice module.
        /// </summary>
        private string invoiceExportName;

        /// <summary>
        /// The name of the invoice module.
        /// </summary>
        private string containerInvoiceExportName;

        /// <summary>
        /// The name of the batch invoice name.
        /// </summary>
        private string batchInvoiceName;

        /// <summary>
        /// The name of the user module.
        /// </summary>
        private string userName;

        /// <summary>
        /// The name of the group module.
        /// </summary>
        private string groupName;

        /// <summary>
        /// The name of the group set up module.
        /// </summary>
        private string groupSetUpName;

        /// <summary>
        /// The name of the handheld emulator module.
        /// </summary>
        private string handheldEmulatorName;

        /// <summary>
        /// The name of the authorisations module.
        /// </summary>
        private string authorisationsName;

        /// <summary>
        /// The name of the switch user module.
        /// </summary>
        private string switchUserName;

        /// <summary>
        /// The name of the currency module.
        /// </summary>
        private string currencyName;

        /// <summary>
        /// The name of the order module.
        /// </summary>
        private string orderName;

        /// <summary>
        /// The name of the device set up module.
        /// </summary>
        private string deviceSetUpName;

        /// <summary>
        /// The name of the licencing module.
        /// </summary>
        private string licencingName;

        /// <summary>
        /// The name of the user set up module.
        /// </summary>
        private string userSetUpName;

        /// <summary>
        /// The name of the admin module.
        /// </summary>
        private string adminName;

        /// <summary>
        /// The name of the import module.
        /// </summary>
        private string importName;

        /// <summary>
        /// The name of the stocktake module.
        /// </summary>
        private string stockTake;

        /// <summary>
        /// The name of the business partner module.
        /// </summary>
        private string businessPartnerName;

        /// <summary>
        /// The name of the sales module.
        /// </summary>
        private string salesName;

        /// <summary>
        /// The name of the purchasing module.
        /// </summary>
        private string purchasingName;

        /// <summary>
        /// The name of the stock management module.
        /// </summary>
        private string stockManagementName;

        /// <summary>
        /// The name of the production module.
        /// </summary>
        private string productionName;

        /// <summary>
        /// The name of the map module.
        /// </summary>
        private string mapName;

        /// <summary>
        /// The name of the edit transactions module.
        /// </summary>
        private string editTransactionsName;

        /// <summary>
        /// The name of the master data set up module.
        /// </summary>
        private string masterDataName;

        /// <summary>
        /// The name of the dispatch module.
        /// </summary>
        private string dispatchName;

        /// <summary>
        /// The name of the report to display on the report viewer.
        /// </summary>
        private string reportToDisplay;

        /// <summary>
        /// The name of the label design module.
        /// </summary>
        private string labelDesignName;

        /// <summary>
        /// The name of the bp group set up module.
        /// </summary>
        private string bpGroupSetUpName;

        /// <summary>
        /// The name of the production module.
        /// </summary>
        private string reportsName;

        /// <summary>
        /// The name of the gS1AI module.
        /// </summary>
        private string gS1AIName;

        /// <summary>
        /// The name of the dates set up module.
        /// </summary>
        private string datesSetUpName;

        /// <summary>
        /// The name of the dates master module.
        /// </summary>
        private string datesMasterName;

        /// <summary>
        /// The name of the dates template allocation module.
        /// </summary>
        private string datesTemplateAllocationName;

        /// <summary>
        /// The name of the dates template name module.
        /// </summary>
        private string datesTemplateName;

        /// <summary>
        /// The name of the dates template name module.
        /// </summary>
        private string qualityAssuranceName;

        /// <summary>
        /// The name of the dates template name module.
        /// </summary>
        private string lairageName;

        /// <summary>
        /// The name of the dates template name module.
        /// </summary>
        private string sequencerName;

        /// <summary>
        /// The name of the dates template name module.
        /// </summary>
        private string graderName;

        /// <summary>
        /// The name of the dates template name module.
        /// </summary>
        private string killLineName;

        /// <summary>
        /// The name of the traceability set up module.
        /// </summary>
        private string traceabilitySetUpName;

        /// <summary>
        /// The name of the traceability master module.
        /// </summary>
        private string traceabilityMasterName;

        /// <summary>
        /// The name of the traceability template allocation module.
        /// </summary>
        private string traceabilityTemplateAllocationName;

        /// <summary>
        /// The name of the  template name module.
        /// </summary>
        private string traceabilityTemplateName;

        /// <summary>
        /// The name of the  country set up module.
        /// </summary>
        private string countrySetUpName;

        /// <summary>
        /// The name of the  container set up module.
        /// </summary>
        private string containerSetUpName;

        /// <summary>
        /// The name of the partner properties set up module.
        /// </summary>
        private string bPPropertySetUpName;

        /// <summary>
        /// The name of the accounts module.
        /// </summary>
        private string accountsName;

        /// <summary>
        /// The name of the partner master module.
        /// </summary>
        private string bPMasterName;

        /// <summary>
        /// The name of the item master properties set up module.
        /// </summary>
        private string itemMasterPropertySetUpName;

        /// <summary>
        /// The name of the plant set up module.
        /// </summary>
        private string plantSetUpName;

        /// <summary>
        /// The name of the uom set up module.
        /// </summary>
        private string uomSetUpName;

        /// <summary>
        /// The name of the pricing set up module.
        /// </summary>
        private string pricingSetUpName;

        /// <summary>
        /// The name of the inventory module.
        /// </summary>
        private string inventoryName;

        /// <summary>
        /// The name of the inventory master module.
        /// </summary>
        private string inventoryMasterName;

        /// <summary>
        /// The name of the inventory group module.
        /// </summary>
        private string inventoryGroupName;

        /// <summary>
        /// The name of the EPOS module.
        /// </summary>
        private string eposName;

        /// <summary>
        /// The name of the vat set up module.
        /// </summary>
        private string vatSetUpName;

        /// <summary>
        /// The name of the report designer module.
        /// </summary>
        private string reportName;

        /// <summary>
        /// The name of the design module.
        /// </summary>
        private string designName;

        /// <summary>
        /// The name of the warehouse set up module.
        /// </summary>
        private string warehouseSetUpName;

        /// <summary>
        /// The name of the document type module.
        /// </summary>
        private string documentTypeName;

        /// <summary>
        /// The name of the document number module.
        /// </summary>
        private string documentNumberName;

        /// <summary>
        /// The name of the document set up name.
        /// </summary>
        private string documentSetUpName;

        /// <summary>
        /// The name of the region set up name.
        /// </summary>
        private string regionName;

        /// <summary>
        /// The name of the ap receipt name.
        /// </summary>
        private string apReceiptName;

        /// <summary>
        /// The name of the ap receipt touchscreen name.
        /// </summary>
        private string apReceiptTouchscreenName;

        /// <summary>
        /// The name of the scanner name.
        /// </summary>
        private string scannerName;

        /// <summary>
        /// The name of the ap receipt desktop name.
        /// </summary>
        private string apReceiptDesktopName;

        /// <summary>
        /// The name of the route set up.
        /// </summary>
        private string routeName;

        /// <summary>
        /// The name of the label association.
        /// </summary>
        private string labelAssociationName;

        /// <summary>
        /// The quote name.
        /// </summary>
        private string quoteName;

        /// <summary>
        /// The quality name.
        /// </summary>
        private string quality;

        /// <summary>
        /// The quality name.
        /// </summary>
        private string qualityName;

        /// <summary>
        /// The epos main name.
        /// </summary>
        private string eposMain;

        /// <summary>
        /// The epos settings name.
        /// </summary>
        private string eposSettings;

        /// <summary>
        /// The warehouse name.
        /// </summary>
        private string warehouse;

        /// <summary>
        /// The warehouse location name.
        /// </summary>
        private string warehouseLocation;

        /// <summary>
        /// The into production name.
        /// </summary>
        private string intoProduction;

        /// <summary>
        /// The reconciliation name.
        /// </summary>
        private string reconciliation;

        /// <summary>
        /// The out of production name.
        /// </summary>
        private string outOfProduction;

        /// <summary>
        /// The price lists name.
        /// </summary>
        private string priceLists;

        /// <summary>
        /// The all prices name.
        /// </summary>
        private string allPrices;

        /// <summary>
        /// The special prices name.
        /// </summary>
        private string specialPrices;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the MasterViewModel class.
        /// </summary>
        public MasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.ShortcutMenuSelected, this.HandleShortcutSelection);

            // Register for an incoming report search sale selection.
            Messenger.Default.Register<Sale>(this, Token.SearchSaleSelectedForReport, s =>
            {
                this.Locator.ReportSalesSearchData.SetData(false);
                this.ShowReport(s, false, s.Print);
            });

            // Register for an incoming report search sale selection.
            Messenger.Default.Register<Sale>(this, Token.SearchSaleSelectedForTouchscreenReport, s =>
            {
                this.Locator.ReportSalesSearchData.SetData(true);
                this.ShowReport(s, true, s.Print);
            });

            // Register for an incoming report search sale selection.
            Messenger.Default.Register<List<Sale>>(this, Token.SearchSaleSelectedForReport,
                o => this.ShowReport(o, false));

            // Register for an incoming report search sale selection.
            Messenger.Default.Register<List<Sale>>(this, Token.SearchSaleSelectedForMultiSelectReport,
                o => this.ShowReport(o, true));

            // Register for a drill down.
            Messenger.Default.Register<Tuple<ViewType, int>>(this, Token.DrillDown, this.DrillDown);

            // Register for a drill down.
            Messenger.Default.Register<Tuple<ViewType, int, int>>(this, Token.DrillDown, this.DrillDown);

            // Register for a drill down.
            Messenger.Default.Register<Tuple<ViewType, int, int, int>>(this, Token.DrillDown, this.DrillDown);

            // register to keep track of windows opening/closing. (Ribbon is enabled if 1 or more windows open, otherwise disabled)
            Messenger.Default.Register<bool>(this, Token.WindowStatus,
                x =>
                {
                    windowsOpen = x ? windowsOpen += 1 : windowsOpen -= 1;
                    this.EnableRibbonBar = windowsOpen > 0;

                    if (windowsOpen == 0)
                    {
                        Messenger.Default.Send(Token.Message, Token.BringToFront);
                    }
                });

            #endregion

            #region command registration

            this.SalesSearchDataCommand = new RelayCommand<string>((s) =>
            {
                if (s.CompareIgnoringCaseAndWhitespace("Sale Order"))
                {
                    this.Locator.Order.ShowAllSales();
                }
                else if (s.CompareIgnoringCaseAndWhitespace("Dispatch"))
                {
                    this.Locator.ARDispatch.ShowAllSales();
                }
                else if (s.CompareIgnoringCaseAndWhitespace("Sale Invoice"))
                {
                    this.Locator.Invoice.ShowAllSales();
                }
                else if (s.CompareIgnoringCaseAndWhitespace("Purchase Order"))
                {
                    this.Locator.APOrder.ShowAllSales();
                }
                else if (s.CompareIgnoringCaseAndWhitespace("Intake"))
                {
                    this.Locator.APReceipt.ShowAllSales();
                }
                else if (s.CompareIgnoringCaseAndWhitespace("Purchase Invoice"))
                {
                    this.Locator.APInvoice.ShowAllSales();
                }
                //Messenger.Default.Send(Token.Message, Token.ShowSalesSearchScreen);
            });

            this.SpecsCommand = new RelayCommand(this.SpecsCommandExecute);
            this.BatchEditCommand = new RelayCommand(this.BatchEditCommandExecute);
            this.BatchSetUpCommand = new RelayCommand(this.BatchSetUpCommandExecute);
            this.ReceipeCommand = new RelayCommand(this.ReceipeCommandExecute);
            this.SpecificationsCommand = new RelayCommand(this.SpecificationsCommandExecute);

            this.DisplayShortcutReportCommand = new RelayCommand<string>(this.DisplayShortcutReportCommandExecute);

            this.StoreShortcutMenuCommand = new RelayCommand<string>(s =>
            {
                if (!ViewModelLocator.IsSalesSearchDataNull() && this.Locator.SalesSearchData.IsFormLoaded)
                {
                    var localModule = "SalesSearchData";
                    if (this.Locator.SalesSearchData.AssociatedView == ViewType.ARDispatch)
                    {
                        localModule = "SalesSearchDataDispatch";
                    }
                    else if (this.Locator.SalesSearchData.AssociatedView == ViewType.Invoice)
                    {
                        localModule = "SalesSearchDataSaleInvoice";
                    }
                    else if (this.Locator.SalesSearchData.AssociatedView == ViewType.APOrder)
                    {
                        localModule = "SalesSearchDataPurchaseOrder";
                    }
                    else if (this.Locator.SalesSearchData.AssociatedView == ViewType.APReceipt)
                    {
                        localModule = "SalesSearchDataIntake";
                    }
                    else if (this.Locator.SalesSearchData.AssociatedView == ViewType.PurchaseInvoice)
                    {
                        localModule = "SalesSearchDataPurchaseInvoice";
                    }

                    Messenger.Default.Send(Tuple.Create(new ApplicationMenu{TreeNodeText = localModule }, s.ToBool()), Token.AddMenuToShortcut);
                    return;
                }

                if (this.CurrentNode != null)
                {
                    if (this.CurrentNode.IsReport)
                    {
                        this.Report1 = this.CurrentNode.TreeNodeText;
                    }

                    Messenger.Default.Send(Tuple.Create(this.CurrentNode,s.ToBool()), Token.AddMenuToShortcut);
                }
            });

            this.EnableRibbonBarCommand = new RelayCommand(() =>
            {
                this.EnableRibbonBar = true;
            });

            // Handle the stock reconciiation.
            this.StockAdjustCommand = new RelayCommand(this.StockAdjustCommandExecute);

            // Handle the stock reconciiation.
            this.StockMoveCommand = new RelayCommand(this.StockMoveCommandExecute);

            // Handle the call to lairage order.
            this.LairageOrderCommand = new RelayCommand(this.LairageOrderCommandExecute);

            // Handle the call to lairage intake.
            this.LairageIntakeCommand = new RelayCommand(this.LairageIntakeCommandExecute);

            // Handle the call to lairage intake.
            this.LairageIntakeTouchscreenCommand = new RelayCommand(this.LairageIntakeTouchscreenCommandExecute);

            // Handle the call to lairage intake.
            this.SequencerCommand = new RelayCommand(this.SequencerCommandExecute);

            // Handle the call to lairage intake.
            this.GraderCommand = new RelayCommand(this.GraderCommandExecute);

            this.TelesalesSetUpCommand = new RelayCommand(this.TelesalesSetUpCommandExecute);

            this.TelesalesCommand = new RelayCommand(this.TelesalesCommandExecute);

            // Handle the call to lairage intake.
            this.KillInformationCommand = new RelayCommand(this.KillInformationCommandExecute);

            // Handle the call to lairage intake.
            this.PaymentDeductionsCommand = new RelayCommand(this.PaymentDeductionsCommandExecute);

            // Handle the call to creat a payment proposal.
            this.PaymentProposalCreationCommand = new RelayCommand(this.PaymentProposalCreationCommandExecute);

            // Handle the call to open a proposal.
            this.PaymentProposalOpenCommand = new RelayCommand(this.PaymentProposalOpenCommandExecute);

            // Handle the call to open a proposal.
            this.PaymentCommand = new RelayCommand(this.PaymentCommandExecute);

            this.PricingMatrixCommand = new RelayCommand(this.PricingMatrixCommandExecute);

            // Handle the stock reconciiation.
            this.HandheldEmulatorCommand = new RelayCommand(this.HandheldEmulatorCommandExecute);

            this.FTraceCommand = new RelayCommand(this.FTraceCommandExecute);

            this.QuickOrderCommand = new RelayCommand(this.QuickOrderCommandExecute);
            this.CarcassDispatchCommand = new RelayCommand(this.CarcassDispatchCommandExecute);

            // Handle the stock reconciiation.
            this.StockReconciliationCommand = new RelayCommand(this.StockReconciliationCommandExecute);

            // Handle the stock reconciiation.
            this.StockTakeCommand = new RelayCommand(this.StockTakeCommandExecute);

            // Handle the stock reconciiation.
            this.DispatchContainerCommand = new RelayCommand(this.DispatchContainerCommandExecute);

            // Handle the accounts selection module.
            this.AccountsCommand = new RelayCommand<string>(this.AccountsCommandExecute);

            // Handle the accounts selection module.
            this.SyncPartnersCommand = new RelayCommand<string>(this.SyncPartnersCommandExecute);

            // Handle the accounts selection module.
            this.SyncProductsCommand = new RelayCommand<string>(this.SyncProductsCommandExecute);

            // Handle the accounts selection module.
            this.SyncPricesCommand = new RelayCommand<string>(this.SyncPricesCommandExecute);

            // Handle the accounts selection module.
            this.AccountsPurchasesCommand = new RelayCommand<string>(this.AccountsPurchasesCommandExecute);

            // Handle the accounts selection module.
            this.AccountsCreditsCommand = new RelayCommand<string>(this.AccountsCreditReturnsCommandExecute);

            // Handle the accounts purchases selection module.
            this.ContainerAccountsCommand = new RelayCommand<string>(this.ContainerAccountsCommandExecute);

            // Handle the scanner selection module.
            this.SelectedScannerModuleCommand = new RelayCommand<string>(this.SelectedScannerModuleCommandExecute);

            // Handler for ribbon navigation buttons selection. (We notify all subscribers)
            this.NavigationButtonCommand =
                new RelayCommand<string>(
                    x =>
                        Messenger.Default.Send((RibbonCommand) Enum.Parse(typeof(RibbonCommand), x),
                            Token.NavigationCommand));

            // Handler for ribbon navigation report buttons selection. (We notify all subscribers)
            this.ReportButtonCommand = new RelayCommand<string>(x => Messenger.Default.Send(x, Token.ReportCommand));

            // Handler for the control value switch.
            this.SwitchControlCommand = new RelayCommand<string>(this.SwitchControlCommandExecute);

            // Handler for the label association selection module.
            this.LabelAssociationCommand = new RelayCommand(this.LabelAssociationCommandExecute);

            // Handler for the label association selection module.
            this.ReportSetUpCommand = new RelayCommand(this.ReportSetUpCommandExecute);

            this.DepartmentCommand = new RelayCommand(this.DepartmentCommandExecute);

            // Handler for the label association selection module.
            this.ReportFoldersCommand = new RelayCommand(this.ReportFoldersCommandExecute);
 
            this.ReportProcessCommand = new RelayCommand(this.ReportProcessCommandExecute);

            // Handler for the invoice selection module.
            this.InvoiceCommand = new RelayCommand(this.InvoiceCommandExecute);

            // Handler for the invoice selection module.
            this.PurchaseInvoiceCommand = new RelayCommand(this.PurchaseInvoiceCommandExecute);

            // Handler for the invoice selection module.
            this.QualityAssuranceCommand = new RelayCommand(this.QualityAssuranceCommandExecute);

            // Handler for the invoice selection module.
            this.LairageDetailsCommand = new RelayCommand(this.LairageDetailsCommandExecute);

            // Handler for the batch invoice selection module.
            this.BatchInvoiceCommand = new RelayCommand(this.BatchInvoiceCommandExecute);

            // Handler for the batch invoice selection module.
            this.CreateCreditNoteCommand = new RelayCommand(this.CreateCreditNoteCommandExecute);

            this.CreditNoteCommand = new RelayCommand(this.CreditNoteCommandExecute);

            // Handler for the batch invoice selection module.
            this.PurchaseBatchInvoiceCommand = new RelayCommand(this.PurchaseBatchInvoiceCommandExecute);

            // Handler for the form load event.
            this.OnLoadedCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand(() => this.IsFormLoaded = true);

            // Handler for the application un load.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecuted);

            // Gets the command to set the current view model to the business partner viewmodel.
            this.SelectContactCommand = new RelayCommand(this.SelectContactCommandExecute);

            // Gets the command to set the current view model to the region viewmodel.
            this.RegionCommand = new RelayCommand(this.RegionCommandExecute);

            // Gets the command to set the current view model to the edit transactions viewmodel.
            this.EditTransactionsCommand = new RelayCommand(this.EditTransactionsCommandExecute);

            // Gets the command to set the current view model to the edit transactions viewmodel.
            this.IdentigenCommand = new RelayCommand(this.IdentigenCommandExecute);
            
            // Gets the command to set the current view model to the edit transactions viewmodel.
            this.ScotBeefCommand = new RelayCommand(this.ScotBeefCommandExecute);

            // Gets the command to set the current view model to the edit transactions viewmodel.
            this.KillDetailsCommand = new RelayCommand(this.KillDetailsCommandExecute);

            // Gets the command to set the current view model to the edit transactions viewmodel.
            this.AnimalMovementsCommand = new RelayCommand(this.AnimalMovementsCommandExecute);

            // Gets the command to set the current view model to the edit transactions viewmodel.
            this.AnimalPricingCommand = new RelayCommand(this.AnimalPricingCommandExecute);

            // Gets the command to set the current view model to the region viewmodel.
            this.QuoteCommand = new RelayCommand(this.QuoteCommandExecute);

            // Gets the command to set the current view model to the map viewmodel.
            this.MapCommand = new RelayCommand(this.MapCommandExecute);

            // Gets the command to set the current view model to the order viewmodel.
            this.OrderCommand = new RelayCommand(this.OrderCommandExecute);

            // Gets the command to set the current view model to the route viewmodel.
            this.RouteCommand = new RelayCommand(this.RouteCommandExecute);

            // Gets the command to set the current view model to the document type viewmodel.
            this.DocumentTypeCommand = new RelayCommand(this.DocumentTypeCommandExecute);

            // Gets the command to set the current view model to the document number viewmodel.
            this.DocumentNumberCommand = new RelayCommand(this.DocumentNumberCommandExecute);

            // Gets the command to open the label design screen.
            this.SelectLabelDesignCommand = new RelayCommand(this.SelectLabelDesignCommandExecute);

            // Gets the command to set the current view model to the user group viewmodel.
            this.SelectGroupCommand = new RelayCommand(this.SelectGroupCommandExecute);

            // Gets the command to set the current view model to the business partner viewmodel.
            this.SelectUserSetUpCommand = new RelayCommand(this.SelectUserSetUpCommandExecute);

            // Gets the command to set the current view model to the login viewmodel.
            this.SwitchUserCommand = new RelayCommand(this.SwitchUserCommandExecute);

            // Gets the command to set the current view model to the login viewmodel.
            this.SelectAuthorisationsCommand = new RelayCommand(this.SelectAuthorisationCommandExecute);

            // Gets the command to set the current view model to the login viewmodel.
            this.DeviceSetUpCommand = new RelayCommand(this.DeviceSetUpCommandExecute);

            // Gets the command to set the current view model to the login viewmodel.
            this.DeviceSettingsCommand = new RelayCommand(this.DeviceSettingsCommandExecute);

            // Gets the command to set the current view model to the warehouse viewmodel.
            this.WarehouseSetUpCommand = new RelayCommand(this.WarehouseSetUpCommandExecute);

            // Gets the command to set the current view model to the warehouse viewmodel.
            this.AttributeMasterSetUpCommand = new RelayCommand(this.AttributeMasterSetUpCommandExecute);

            // Gets the command to set the current view model to the warehouse viewmodel.
            this.TemplateAllocationCommand = new RelayCommand(this.TemplateAllocationCommandExecute);

            // Gets the command to set the current view model to the warehouse viewmodel.
            this.TabSetUpCommand = new RelayCommand(this.TabSetUpCommandExecute);

            // Gets the command to set the current view model to the warehouse location viewmodel.
            this.WarehouseLocationCommand = new RelayCommand(this.WarehouseLocationCommandExecute);

            // Gets the command to set the current view model to the warehouse location viewmodel.
            this.WeightBandSetUpCommand = new RelayCommand(this.WeightBandSetUpCommandExecute);

            // Gets the command to set the current view model to the uom viewmodel.
            this.UOMSetUpCommand = new RelayCommand(this.UOMSetUpCommandExecute);

            // Gets the command to set the current view model to the country master viewmodel.
            this.CountrySetUpCommand = new RelayCommand(this.CountrySetUpCommandExecute);

            // Gets the command to set the current view model to the container viewmodel.
            this.ContainerSetUpCommand = new RelayCommand(this.ContainerSetUpCommandExecute);

            // Gets the command to set the current view model to the currency viewmodel.
            this.CurrencyCommand = new RelayCommand(this.CurrencyCommandExecute);

            // Gets the command to set the current view model to the plant viewmodel.
            this.PlantSetUpCommand = new RelayCommand(this.PlantSetUpCommandExecute);

            // Gets the command to set the current view model to the bp group set up viewmodel.
            this.BPGroupSetUpCommand = new RelayCommand(this.BPGroupsetUpCommandExecute);

            // Gets the command to set the current view model to the bp properties set up viewmodel.
            this.BPPropertySetUpCommand = new RelayCommand(this.BPPropertySetUpCommandExecute);

            // Gets the command to set the current view model to the item master properties set up viewmodel.
            this.ItemMasterPropertySetUpCommand = new RelayCommand(this.ItemMasterPropertySetUpCommandExecute);

            // Gets the command to set the current view model to the license admin viewmodel.
            this.LicenseAdminCommand = new RelayCommand(this.LicenceAdminCommandExecute);

            // Gets the command to set the current view model to the pricing master viewmodel.
            this.PricingListCommand = new RelayCommand(this.PricingListCommandExecute);

            // Gets the command to set the current view model to the pricing master viewmodel.
            this.AllPricesCommand = new RelayCommand(this.AllPricesCommandExecute);

            // Gets the command to set the current view model to the pricing master viewmodel.
            this.SpecialPricesCommand = new RelayCommand(this.SpecialPricesCommandExecute);

            // Gets the command to set the current view model to the epos viewmodel.
            this.EposCommand = new RelayCommand(this.EposCommandExecute);

            this.MrpCommand = new RelayCommand(this.MrpCommandExecute);

            // Gets the command to set the current view model to the epos viewmodel.
            this.EposSettingsCommand = new RelayCommand(this.EposSettingsCommandExecute);

            // Gets the command to set the current view model to the license admin viewmodel.
            this.ImportLicenseCommand = new RelayCommand(this.ImportLicenceCommandExecute);

            // Gets the command to set the current view model to the license admin viewmodel.
            this.Gs1aiIdentifierCommand = new RelayCommand(this.Gs1AiIdentifierCommandExecute);

            // Gets the command to set the current view model to the dates set up viewmodel.
            this.DatesSetUpCommand = new RelayCommand(this.DatesSetUpCommandExecute);

            // Gets the command to set the current view model to the dates template allocation viewmodel.
            this.DatesTemplateAllocationCommand = new RelayCommand(this.DatesTemplateAllocationCommandExecute);

            // Gets the command to set the current view model to the dates template name viewmodel.
            this.DatesTemplateNameCommand = new RelayCommand(this.DatesTemplateNameCommandExecute);

            // Gets the command to set the current view model to the traceability template viewmodel.
            this.TraceabilityTemplateCommand = new RelayCommand(this.TraceabilityTemplateCommandExecute);

            // Gets the command to set the current view model to the traceability template allocation viewmodel.
            this.TraceabilityTemplateAllocationCommand =
                new RelayCommand(this.TraceabilityTemplateallocationCommandExecute);

            // Gets the command to set the current view model to the traceability template name viewmodel.
            this.TraceabilityTemplateNameCommand = new RelayCommand(this.TraceabilityTemplateNameCommandExecute);

            // Gets the command to set the current view model to the quality template viewmodel.
            this.QualityTemplateCommand = new RelayCommand(this.QualityTemplateCommandExecute);

            // Gets the command to set the current view model to the quality template allocation viewmodel.
            this.QualityTemplateAllocationCommand = new RelayCommand(this.QualityTemplateallocationCommandExecute);

            // Gets the command to set the current view model to the quality template name viewmodel.
            this.QualityTemplateNameCommand = new RelayCommand(this.QualityTemplateNameCommandExecute);

            // Gets the command to set the current view model to the inventorygroup viewmodel.
            this.InventoryGroupCommand = new RelayCommand(this.InventoryGroupCommandExecute);

            // Gets the command to set the current view model to the business partner viewmodel.
            this.VatSetUpCommand = new RelayCommand(this.VatSetUpCommandExecute);

            // Gets the command to set the current view model to the business partner viewmodel.
            this.WorkflowCommand = new RelayCommand(this.WorkflowCommandExecute);

            // Gets the command to set the current view model to the business partner viewmodel.
            this.WorkflowSearchCommand = new RelayCommand(this.WorkflowSearchCommandExecute);

            // Gets the command to set the current view model to the business partner viewmodel.
            this.WorkflowAlertsCommand = new RelayCommand(this.WorkflowAlertsCommandExecute);

            // Gets the command to set the current view model to the inventory master viewmodel.
            this.InventoryMasterCommand = new RelayCommand(this.InventoryMasterCommandExecute);

            // Gets the command to set the current view model to the inventory group set up viewmodel.
            this.InventoryGroupSetUpCommand = new RelayCommand(this.InventoryGroupSetUpCommandExecute);

            // Gets the command to load the ap quote view.
            this.APQuoteCommand = new RelayCommand(this.APQuoteCommandExecute);

            // Gets the command to load the ap order view.
            this.APOrderCommand = new RelayCommand(this.APOrderCommandExecute);

            // Gets the command to load the ap receipt touchscreen view.
            this.APReceiptTouchscreenCommand = new RelayCommand(this.APReceiptTouchscreenCommandExecute);

            // Gets the command to load the ap receipt desktop view.
            this.APReceiptDesktopCommand = new RelayCommand(this.APReceiptDesktopCommandExecute);

            // Gets the command to load the ar dispatch touchscreen view.
            this.ARDispatchTouchscreenCommand = new RelayCommand(this.ARDispatchTouchscreenCommandExecute);

            // Gets the command to load the ar dispatch desktop view.
            this.ARDispatchDesktopCommand = new RelayCommand(this.ARDispatchDesktopCommandExecute);

            this.ARReturnsDesktopCommand = new RelayCommand(this.ARReturnsDesktopCommandExecute);
            this.ARReturnsTouchscreenCommand = new RelayCommand(this.ARReturnsTouchscreenCommandExecute);

            // Gets the command to load the into production touchscreen view.
            this.IntoProductionCommand = new RelayCommand(this.IntoProductionCommandExecute);

            // Gets the command to load the out of production touchscreen view.
            this.OutOfProductionCommand = new RelayCommand(this.OutOfProductionCommandExecute);

            // Gets the command to load the exit ui.
            this.CloseApplicationCommand = new RelayCommand(this.CloseApplicationCommandExecute);

            #endregion

            this.CurrentViewModel = this.Locator.Entry;

            try
            {
                this.reportingService.Credentials = System.Net.CredentialCache.DefaultCredentials;
                this.reportingService.Url = ApplicationSettings.SSRSWebServiceURL;
                this.reportingService.Credentials = new NetworkCredential(ApplicationSettings.SSRSUserName,
                    ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);
            }
            catch (Exception ex)
            {
                this.Log.LogDebug(this.GetType(), ex.Message);
            }

            this.SetModuleNames();
            this.SetMainMenu();
            this.SetProgressMessage();
            this.CheckLicenceExpiryDate();
            this.AlertCheckSetUp();
        }

        #endregion

        #region public interface

        #region properties

        public IList<UserGroup> AllowedTouchscreenModules { get; set; }
        public IList<UserGroup> AllowedHandheldModules { get; set; }

        private string report1;
        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public string Report1
        {
            get
            {
                return this.report1;
            }

            set
            {
                this.report1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current node.
        /// </summary>
        public ApplicationMenu CurrentNode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the ribbon bar is enabled or not.
        /// </summary>
        public bool EnableRibbonBar
        {
            get { return this.enableRibbonBar; }

            set
            {
                this.enableRibbonBar = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current view model to be displayed in the content area.
        /// </summary>
        public NouvemViewModelBase CurrentViewModel
        {
            get { return this.currentViewModel; }

            set
            {
                if (this.currentViewModel == value)
                {
                    return;
                }

                this.EnableRibbonBar = value != this && value != this.Locator.Entry;
                this.currentViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application menu collection.
        /// </summary>
        public ApplicationMenu ApplicationMenu
        {
            get { return this.applicationMenu; }

            set
            {
                if (value != null)
                {
                    this.applicationMenu = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to load the batch specs view.
        /// </summary>
        public ICommand SpecificationsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch set up view.
        /// </summary>
        public ICommand BatchSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch set up view.
        /// </summary>
        public ICommand ReceipeCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the into production view.
        /// </summary>
        public ICommand BatchEditCommand { get; private set; }

        /// <summary>
        /// Gets the command to enable the ribbon bar.
        /// </summary>
        public ICommand StoreShortcutMenuCommand { get; private set; }

        /// <summary>
        /// Gets the command to enable the ribbon bar.
        /// </summary>
        public ICommand EnableRibbonBarCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the emulator view.
        /// </summary>
        public ICommand HandheldEmulatorCommand { get; private set; }

        public ICommand SalesSearchDataCommand { get; private set; }

        public ICommand FTraceCommand { get; private set; }

        public ICommand QuickOrderCommand { get; private set; }

        public ICommand CarcassDispatchCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the report view.
        /// </summary>
        public ICommand DisplayReportCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the report view.
        /// </summary>
        public ICommand DisplayShortcutReportCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the contact view.
        /// </summary>
        public ICommand SelectContactCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the label design view.
        /// </summary>
        public ICommand SelectLabelDesignCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the reconcilaition view.
        /// </summary>
        public ICommand StockReconciliationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the stock take view.
        /// </summary>
        public ICommand StockTakeCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the user set up view.
        /// </summary>
        public ICommand SelectUserSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to switch the user, calling the login views.
        /// </summary>
        public ICommand SwitchUserCommand { get; private set; }

        /// <summary>
        /// Gets the command to switch the user, calling the login views.
        /// </summary>
        public ICommand ReportSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to switch the user, calling the login views.
        /// </summary>
        public ICommand ReportFoldersCommand { get; private set; }

        /// <summary>
        /// Gets the command to switch the user, calling the login views.
        /// </summary>
        public ICommand ReportProcessCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the pricing list view.
        /// </summary>
        public ICommand PricingListCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the all prices view.
        /// </summary>
        public ICommand AllPricesCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the special prices list view.
        /// </summary>
        public ICommand SpecialPricesCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the route view.
        /// </summary>
        public ICommand RouteCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the quote view.
        /// </summary>
        public ICommand QuoteCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the order view.
        /// </summary>
        public ICommand OrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ap quote view.
        /// </summary>
        public ICommand APQuoteCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ap order view.
        /// </summary>
        public ICommand APOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ap receipt view.
        /// </summary>
        public ICommand APReceiptCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ap receipt touchscreen view.
        /// </summary>
        public ICommand APReceiptTouchscreenCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the into production view.
        /// </summary>
        public ICommand IntoProductionCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the accounts view.
        /// </summary>
        public ICommand AccountsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the accounts view.
        /// </summary>
        public ICommand SyncPartnersCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the accounts view.
        /// </summary>
        public ICommand SyncProductsCommand { get; private set; }
        /// <summary>
        /// Gets the command to load the accounts view.
        /// </summary>
        public ICommand SyncPricesCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the accounts purchases view.
        /// </summary>
        public ICommand AccountsPurchasesCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the accounts purchases view.
        /// </summary>
        public ICommand AccountsCreditsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the accounts view.
        /// </summary>
        public ICommand ContainerAccountsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the into production view.
        /// </summary>
        public ICommand OutOfProductionCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ap receipt desktop view.
        /// </summary>
        public ICommand APReceiptDesktopCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ar dispatch touchscreen view.
        /// </summary>
        public ICommand ARDispatchTouchscreenCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ar dispatch touchscreen view.
        /// </summary>
        public ICommand DispatchContainerCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the invoice view.
        /// </summary>
        public ICommand InvoiceCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch invoice view.
        /// </summary>
        public ICommand BatchInvoiceCommand { get; private set; }

        public ICommand CreditNoteCommand { get; private set; }

        public ICommand CreateCreditNoteCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the invoice view.
        /// </summary>
        public ICommand PurchaseInvoiceCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch invoice view.
        /// </summary>
        public ICommand PurchaseBatchInvoiceCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch invoice view.
        /// </summary>
        public ICommand TelesalesSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch invoice view.
        /// </summary>
        public ICommand TelesalesCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch invoice view.
        /// </summary>
        public ICommand QualityAssuranceCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the batch invoice view.
        /// </summary>
        public ICommand LairageDetailsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the lairage order view.
        /// </summary>
        public ICommand LairageOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the lairage order view.
        /// </summary>
        public ICommand StockMoveCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the lairage order view.
        /// </summary>
        public ICommand StockAdjustCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the lairage intake view.
        /// </summary>
        public ICommand LairageIntakeCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the lairage intake view.
        /// </summary>
        public ICommand LairageIntakeTouchscreenCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the sequencer view.
        /// </summary>
        public ICommand SequencerCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the grader view.
        /// </summary>
        public ICommand GraderCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the grader view.
        /// </summary>
        public ICommand KillInformationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the grader view.
        /// </summary>
        public ICommand PaymentDeductionsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the payment creation view.
        /// </summary>
        public ICommand PaymentProposalCreationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the payment creation view.
        /// </summary>
        public ICommand PaymentProposalOpenCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the payment creation view.
        /// </summary>
        public ICommand PaymentCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the payment creation view.
        /// </summary>
        public ICommand PricingMatrixCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the ar dispatch desktop view.
        /// </summary>
        public ICommand ARDispatchDesktopCommand { get; private set; }

        public ICommand ARReturnsDesktopCommand { get; private set; }
        public ICommand ARReturnsTouchscreenCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the region view.
        /// </summary>
        public ICommand RegionCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the document type view.
        /// </summary>
        public ICommand DocumentTypeCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the document number view.
        /// </summary>
        public ICommand DocumentNumberCommand { get; private set; }

        /// <summary>
        /// Gets the command to navigate through a collection type.
        /// </summary>
        public ICommand NavigationButtonCommand { get; private set; }

        /// <summary>
        /// Gets the command to print/preview a report.
        /// </summary>
        public ICommand ReportButtonCommand { get; private set; }

        /// <summary>
        /// Gets the command to notify subscribers of a control switch.
        /// </summary>
        public ICommand SwitchControlCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the group view.
        /// </summary>
        public ICommand SelectGroupCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the group authorisations view.
        /// </summary>
        public ICommand SelectAuthorisationsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the device set up view.
        /// </summary>
        public ICommand DeviceSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the device set up view.
        /// </summary>
        public ICommand DeviceSettingsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the currency view.
        /// </summary>
        public ICommand DepartmentCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the currency view.
        /// </summary>
        public ICommand CurrencyCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the country set up view.
        /// </summary>
        public ICommand CountrySetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the container set up view.
        /// </summary>
        public ICommand ContainerSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the bp properties set up view.
        /// </summary>
        public ICommand BPPropertySetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the bp group set up view.
        /// </summary>
        public ICommand BPGroupSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the item master properties set up view.
        /// </summary>
        public ICommand SpecsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the item master properties set up view.
        /// </summary>
        public ICommand ItemMasterPropertySetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the warehouse set up view.
        /// </summary>
        public ICommand WarehouseSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the warehouse location view.
        /// </summary>
        public ICommand WarehouseLocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the warehouse location view.
        /// </summary>
        public ICommand WeightBandSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the warehouse location view.
        /// </summary>
        public ICommand AttributeMasterSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the warehouse location view.
        /// </summary>
        public ICommand TemplateAllocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the warehouse location view.
        /// </summary>
        public ICommand TabSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the vat set up view.
        /// </summary>
        public ICommand VatSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the vat set up view.
        /// </summary>
        public ICommand WorkflowCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the vat set up view.
        /// </summary>
        public ICommand WorkflowSearchCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the vat set up view.
        /// </summary>
        public ICommand WorkflowAlertsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the plant set up view.
        /// </summary>
        public ICommand PlantSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the edit transactions view.
        /// </summary>
        public ICommand EditTransactionsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the edit transactions view.
        /// </summary>
        public ICommand IdentigenCommand { get; private set; }
        /// <summary>
        /// Gets the command to load the edit transactions view.
        /// </summary>
        public ICommand ScotBeefCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the edit transactions view.
        /// </summary>
        public ICommand KillDetailsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the edit transactions view.
        /// </summary>
        public ICommand AnimalMovementsCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the edit transactions view.
        /// </summary>
        public ICommand AnimalPricingCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the uom set up view.
        /// </summary>
        public ICommand UOMSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the license admin view.
        /// </summary>
        public ICommand LicenseAdminCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the import license view.
        /// </summary>
        public ICommand ImportLicenseCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the map view.
        /// </summary>
        public ICommand MapCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the gs1 identifier view.
        /// </summary>
        public ICommand Gs1aiIdentifierCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the label association view.
        /// </summary>
        public ICommand LabelAssociationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the dates set up view.
        /// </summary>
        public ICommand DatesSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the dates template allocation view.
        /// </summary>
        public ICommand DatesTemplateAllocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the dates template name view.
        /// </summary>
        public ICommand DatesTemplateNameCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the traceability template view.
        /// </summary>
        public ICommand TraceabilityTemplateCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the Traceability template allocation view.
        /// </summary>
        public ICommand TraceabilityTemplateAllocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the Traceability template name view.
        /// </summary>
        public ICommand TraceabilityTemplateNameCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the traceability template view.
        /// </summary>
        public ICommand QualityTemplateCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the Quality template allocation view.
        /// </summary>
        public ICommand QualityTemplateAllocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the Quality template name view.
        /// </summary>
        public ICommand QualityTemplateNameCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the inventory group view.
        /// </summary>
        public ICommand InventoryGroupCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the inventory group set up view.
        /// </summary>
        public ICommand InventoryGroupSetUpCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the inventory master view.
        /// </summary>
        public ICommand InventoryMasterCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the epos view.
        /// </summary>
        public ICommand EposCommand { get; private set; }

        /// <summary>
        /// Gets the command to load the epos settings view.
        /// </summary>
        public ICommand EposSettingsCommand { get; private set; }

        public ICommand MrpCommand { get; private set; }

        /// <summary>
        /// Gets the command tohandle the scanner module selection.
        /// </summary>
        public ICommand SelectedScannerModuleCommand { get; private set; }

        /// <summary>
        /// Gets the command to close the application.
        /// </summary>
        public ICommand CloseApplicationCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        #endregion

        #region method

        public void ShowDispatchScreen(Telesale telesale)
        {
            this.ARDispatchCommandExecute();
            Messenger.Default.Send(telesale, Token.BusinessPartnerSelected);
        }

        public void DisplayTouchscreenReport(ReportData reportData)
        {
            var name = reportData.Name;
            var path = reportData.Path;

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            var reportPath = path;
            var localReportName = name;
            this.DisplayTouchscreenReportSearchScreen(reportPath, localReportName);
        }

        /// <summary>
        /// Displays the search screen corresponding to the selected report.
        /// </summary>
        /// <param name="path">The selected report path.</param>
        /// <param name="report">The selected report name.</param>
        private void DisplayTouchscreenReportSearchScreen(string path, string report)
        {
            this.Locator.ReportSalesSearchData.AssociatedView = ViewType.APOrder;
            ApplicationSettings.SalesSearchShowAllOrders = true;
            this.Locator.ReportSalesSearchData.SetData(true);
            this.Log.LogDebug(this.GetType(),
                string.Format("DisplayTouchscreeenReportSearchScreen(): Path:{0}, Report:{1}", path, report));
            var displayProductionSearchScreen = false;
            var displayTransactionSearchScreen = false;
            this.reportToDisplay = report;

            var orderStatuses = new List<int?>
            {
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
            };

            IList<Sale> reportItems = null;

            if (report.Equals(ReportName.DispatchDocket)
                || report.Equals((ReportName.DispatchDocketDetail))
                || report.Equals((ReportName.DispatchDocketGrouped))
                || report.Equals(ReportName.DispatchDocketExVAT)
                || report.Equals(ReportName.ColdstoreDispatchDocket)
                || report.Equals(ReportName.PalletReport)
                || report.Equals(ReportName.DispatchDocketIncVAT)
                || report.Equals(ReportName.DispatchDocketNoPrice)
                || report.Equals(ReportName.BloorsDispatchDocket)
                || report.Equals(ReportName.BloorsDispatchSummary)
                || report.Equals(ReportName.WoolleysDispatchDocket)
                || report.Equals(ReportName.WoolleysCarcassExport)
                || report.Equals(ReportName.DunleavysCarcassExport)
                || report.Equals(ReportName.BallonCarcassExport)
                || report.Equals(ReportName.CarcassExport)
                || report.Equals(ReportName.WoolleysDispatchDocketDetail)
                || report.Equals(ReportName.DunleavysDispatchDocket)
                || report.Equals(ReportName.DunleavysCMR)
                || report.Equals(ReportName.CMRDispatchPallets)
                || report.Equals(ReportName.BallonCMR)
                || report.Equals(ReportName.CMR)
                || report.Equals(ReportName.DunleavysDispatchDocketDetail)
                || report.Equals(ReportName.CarcassDispatchReport)
                || report.Equals(ReportName.CarcassDispatchExport)
                || report.Equals(ReportName.DispatchSummaryReport)
                )
            {
                //orderStatuses = new List<int?>
                //{
                //   NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                //};

                reportItems = this.DataManager.GetAllARDispatchesForReport(orderStatuses);
                this.Locator.ReportSalesSearchData.ShowAllOrders = true;
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.ARDispatch;
            }
            else if (report.Equals(ReportName.BloorsSaleOrder))
            {
                //orderStatuses = new List<int?>
                //{
                //   NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                //};

                reportItems = this.DataManager.GetAllOrdersForReport(orderStatuses);
                this.Locator.ReportSalesSearchData.ShowAllOrders = true;
            }
            else if (report.Equals(ReportName.InvoiceDocket) || report.Equals(ReportName.WoolleysInvoice))
            {
                if (this.Locator.ReportSalesSearchData.FromDate < DateTime.Today.AddYears(-100))
                {
                    this.Locator.ReportSalesSearchData.FromDate = DateTime.Today;
                }

                //reportItems = this.DataManager.GetAllInvoicesByDate(orderStatuses, this.Locator.ReportSalesSearchData.FromDate, DateTime.Today);
                reportItems = new List<Sale>();
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.Invoice;
            }
            else if (report.Equals(ReportName.IntakeDocket)
                     || report.Equals(ReportName.IntakeDocketTotal)
                || report.Equals(ReportName.IntakeDetails)
                     || report.Equals(ReportName.IntakeReportKillNo)
                     || report.Equals(ReportName.ColdstoreGoodsIn)
                     || report.Equals(ReportName.IntakeReportProduct))
            {
                reportItems = this.DataManager.GetAllAPReceiptsForReport(orderStatuses);
            }
            else if (report.Equals(ReportName.BatchYield)
                     || report.Equals(ReportName.BatchYieldPriced)
                     || report.Equals(ReportName.BatchYieldDetail)
                     || report.Equals(ReportName.BatchYieldDetailPriced)
                     || report.Equals(ReportName.WoolleysBatchDetailsReport)
                     || report.Equals(ReportName.WoolleysBatchDetailsTraceReport)
                     || report.Equals(ReportName.WoolleysBatchYield)
                     || report.Equals(ReportName.DunleavysBatchDetailsReport)
                     || report.Equals(ReportName.BallonBatchDetails)
                     || report.Equals(ReportName.BatchCosting)
                     || report.Equals(ReportName.BatchDetails)
                     || report.Equals(ReportName.BatchDetailReport)
                     || report.Equals(ReportName.BatchDetailTraceReport)
                     || report.Equals(ReportName.BatchInput)
                     || report.Equals(ReportName.BatchOutput)
                     || report.Equals(ReportName.BallonBatchInput)
                     || report.Equals(ReportName.BallonbatchOutput)
                     || report.Equals(ReportName.DunleavysBatchDetailsTraceReport)
                     || report.Equals(ReportName.BallonBatchDetails)
                     || report.Equals(ReportName.BallonBatchInput)
                     || report.Equals(ReportName.BallonbatchOutput)
                     || report.Equals(ReportName.DunleavysBatchYield)
                     || report.Equals(ReportName.BallonBatchYield)
                     || report.Equals(ReportName.BloorsBatchDetail))
            {
                reportItems = new List<Sale>();
                displayProductionSearchScreen = true;
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.BatchEdit;
            }
            else if (report.Equals(ReportName.BloorsBatchDetailNew))
            {
                reportItems = new List<Sale>();
                displayProductionSearchScreen = true;
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.BatchEditOrdered;
            }
            else if (report.Equals(ReportName.LabelData)
                     || report.Equals(ReportName.StockTraceReport)
                     || report.Equals(ReportName.BloorsLabelTrace))
            {
                reportItems =
                    new List<Sale>();
                displayTransactionSearchScreen = true;
            }
            else if (report.Equals(ReportName.ProductReport))
            {
                Messenger.Default.Send(ViewType.ReportProductData);
                return;
            }
            else if (report.Equals(ReportName.SalesAnalysisReport))
            {
                Messenger.Default.Send(ViewType.ReportTransactionData);
                return;
            }
            else if (report.Equals(ReportName.DispatchSummaryReport))
            {
                Messenger.Default.Send(ViewType.ReportTransactionData);
                return;
            }
            else
            {
                // non search grid reports
                this.ReportManager.PreviewReport(new ReportData {Path = path, Name = report}, true);
                return;
            }

            var token = displayProductionSearchScreen
                ? Token.SearchForTouchscreenProductionSale
                : displayTransactionSearchScreen
                    ? Token.SearchForTouchscreenTransactionSale
                    : Token.SearchForTouchscreenReportSale;
            Messenger.Default.Send(reportItems, token);
        }

        /// <summary>
        /// Creates the touchscreen start up modue.
        /// </summary>
        /// <param name="module">The module to fire up.</param>
        public void CreateTouchscreenModule(ViewType module)
        {
            switch (module)
            {
                case ViewType.APReceipt:
                    this.APReceiptCommandExecute();
                    break;

                case ViewType.IntoProduction:
                    this.IntoProductionCommandExecute();
                    break;

                case ViewType.OutOfProduction:
                    this.OutOfProductionCommandExecute();
                    break;

                case ViewType.ARDispatch:
                    this.ARDispatchTouchscreenCommandExecute();
                    break;

                case ViewType.Sequencer:
                    this.SequencerCommandExecute();
                    break;

                case ViewType.Grader:
                    this.GraderCommandExecute();
                    break;

                case ViewType.Workflow:
                    this.WorkflowCommandExecute();
                    break;
            }

            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenModuleOnlyMode);
        }

        #endregion

        #endregion

        #region protected override

        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            // Let all subscribers know that a control mode change has been created from the master window.
            this.SwitchControlCommandExecute(mode);

            // Let all subscribers know that a ribbon command has been executed.
            Messenger.Default.Send(mode, Token.SetControl);
        }

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Opens the stock reconciliation screen.
        /// </summary>
        private void StockAdjustCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.StockTake))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.StockAdjust);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.StockAdjust);

        }

        /// <summary>
        /// Opens the stock reconciliation screen.
        /// </summary>
        private void StockMoveCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.StockTake))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.StockMove);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.StockMove);

        }

        /// <summary>
        /// Opens the licence admin screen.
        /// </summary>
        private void LicenceAdminCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.LicenseAdmin))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.adminName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.LicenseAdmin);
                SystemMessage.Write(MessageType.Background, Message.LicenseAdminLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.LicenseAdmin))
            {
                SystemMessage.Write(MessageType.Background, Message.LicenseAdminLoaded);
            }
        }

        /// <summary>
        /// Opens the licence admin screen.
        /// </summary>
        private void WeightBandSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.LicenseAdmin))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.WeightBandSetUp);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.WeightBand);
                SystemMessage.Write(MessageType.Background, Message.WeightBandSetUpLoaded);
                return;
            }
        }

        /// <summary>
        /// Opens the lairage order screen.
        /// </summary>
        private void LairageOrderCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.LairageOrder);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.LairageOrder);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void LairageIntakeTouchscreenCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = true;
 
            Messenger.Default.Send(ViewType.FactoryTouchScreen);
            //ViewModelLocator.ClearStockMovementTouchscreen();
            ViewModelLocator.ClearAPReceipt();
            ViewModelLocator.ClearIntoProduction();
            ViewModelLocator.ClearOutOfProduction();
            ViewModelLocator.ClearARDispatchTouchscreen();
            ViewModelLocator.ClearARDispatchDetails();
            ViewModelLocator.ClearGrader();
            ViewModelLocator.ClearProductionProductsCards();
            this.Locator.Touchscreen.SetModule(ViewType.LairageIntake);
            this.Locator.Touchscreen.Module = Strings.Lairage;

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.LairagePanelStatic;
            }));
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void LairageIntakeCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.LairageIntake);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.LairageIntake);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void SequencerCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}

            //this.SwitchView(this.Locator.Blank);
            this.Locator.Touchscreen.SetModule(ViewType.Sequencer);
            //this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.SequencerPanelStatic;

            ViewModelLocator.TouchscreenStatic.Module = Strings.Sequencer;
            SystemMessage.Write(MessageType.Background, Message.SequencerLoaded);
            Messenger.Default.Send(ViewType.FactoryTouchScreen);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void GraderCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}

            //this.SwitchView(this.Locator.Blank);
            this.Locator.Touchscreen.SetModule(ViewType.Grader);
            //this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.GraderPanelStatic;

            ViewModelLocator.TouchscreenStatic.Module = Strings.Grader;
            SystemMessage.Write(MessageType.Background, Message.GraderLoaded);
            Messenger.Default.Send(ViewType.FactoryTouchScreen);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void KillInformationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}

            //this.SwitchView(this.Locator.Blank);
            this.Locator.LairageAnimalsSearch.Mode = ViewType.LairageKillInformation;
            Messenger.Default.Send(ViewType.LairageAnimalsSearch);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void PaymentProposalCreationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.CreateBatchPaymentPaymentProposal);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.PaymentProposalCreation);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void PaymentProposalOpenCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.PaymentProposalOpen);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void PaymentCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.PaymentProposal);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Payment);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void PricingMatrixCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.PricingMatrix);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.PricingMatrix);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void TelesalesCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Telesales);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Telesales);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void TelesalesSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.TelesalesSetUp);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.TelesalesSetUp);
        }

        /// <summary>
        /// Opens the lairage intake screen.
        /// </summary>
        private void PaymentDeductionsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.PaymentDeductions);
        }

        /// <summary>
        /// Opens the stock reconciliation screen.
        /// </summary>
        private void HandheldEmulatorCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Handheld);
            this.SwitchView(this.Locator.Blank);
            ApplicationSettings.ScannerEmulatorMode = true;
            ApplicationSettings.ScannerMode = true;
            Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
        }

        /// <summary>
        /// Opens the stock reconciliation screen.
        /// </summary>
        private void FTraceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.FTrace);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.FTrace);
        }

        private void QuickOrderCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.QuickOrder);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.QuickOrder);
        }

        private void CarcassDispatchCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.StockReconciliation))
            //{
            //    return;
            //}
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, "Carcass Dispatch");
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.CarcassDispatch);
        }

        /// <summary>
        /// Opens the stock take screen.
        /// </summary>
        private void StockTakeCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.StockTake))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.stockTake);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.StockTake);
            SystemMessage.Write(MessageType.Background, Message.StockTakeScreenLoaded);
        }

        /// <summary>
        /// Opens the stock take screen.
        /// </summary>
        private void DispatchContainerCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.ARDispatch))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Container);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.DispatchContainer);
            SystemMessage.Write(MessageType.Background, Message.ContainerScreenLoaded);
        }

        /// <summary>
        /// Opens the stock reconciliation screen.
        /// </summary>
        private void StockReconciliationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.StockReconciliation))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.reconciliation);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.StockReconciliation);
            SystemMessage.Write(MessageType.Background, Message.StockReconciliationScreenLoaded);
        }

        /// <summary>
        /// Opens the item master property screen.
        /// </summary>
        private void ItemMasterPropertySetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.IMPropertySetUp))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.IMPropertySetUp);
                SystemMessage.Write(MessageType.Background, Message.IMPropertySetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Container))
            {
                SystemMessage.Write(MessageType.Background, Message.IMPropertySetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the property screen.
        /// </summary>
        private void BPPropertySetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.BPPropertySetUp))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.bPPropertySetUpName);

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.BPPropertySetUp);
                SystemMessage.Write(MessageType.Background, Message.BPPropertySetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.BPPropertySetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.BPPropertySetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the group set up screen.
        /// </summary>
        private void BPGroupsetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.BPGroupSetUp))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.bpGroupSetUpName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.BPGroupSetUp);
                SystemMessage.Write(MessageType.Background, Message.BPGroupSetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.BPGroupSetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.BPGroupSetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the ap quote screen.
        /// </summary>
        private void APQuoteCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.APQuote))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.quoteName);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "APQuote";
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.APQuote);
                SystemMessage.Write(MessageType.Background, Message.QuoteScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.APQuote))
            {
                SystemMessage.Write(MessageType.Background, Message.QuoteScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the ap order screen.
        /// </summary>
        private void APOrderCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.APOrder))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.orderName);
       
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "APOrder";
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.APOrder);
                SystemMessage.Write(MessageType.Background, Message.OrderScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.APOrder))
            {
                SystemMessage.Write(MessageType.Background, Message.OrderScreenLoaded);
            }
        }

        /// <summary>
        /// Set touchscreen goods in mode.
        /// </summary>
        private void APReceiptTouchscreenCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = true;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.apReceiptTouchscreenName);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "APReceiptTouchscreen";
            }

            this.APReceiptCommandExecute();
        }

        /// <summary>
        /// Set desktop goods in mode.
        /// </summary>
        private void APReceiptDesktopCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.apReceiptDesktopName);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "APReceiptDesktop";
            }

            this.APReceiptCommandExecute();
        }

        /// <summary>
        /// Opens the ap receipt screen.
        /// </summary>
        private void APReceiptCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.APReceipt))
            {
                return;
            }

            ViewModelLocator.ClearARDispatchDetails();
       
            ViewModelLocator.CreateAPReceiptDetails();

            if (true)
            {
                this.SwitchView(this.Locator.Blank);

                if (ViewModelLocator.IsAPReceiptDetailsNull())
                {
                    ViewModelLocator.CreateAPReceiptDetails();
                }

                if (ApplicationSettings.TouchScreenMode)
                {
                    ViewModelLocator.ClearARDispatchDetails();
                 
                    ViewModelLocator.ClearAPReceipt();
                    ViewModelLocator.ClearIntoProduction();
                    ViewModelLocator.ClearOutOfProduction();
                    ViewModelLocator.ClearProductionProductsCards();
                    ViewModelLocator.ClearARDispatchTouchscreen();
                    this.Locator.Touchscreen.SetModule(ViewType.APReceipt);
                    this.Locator.TouchscreenOrders.SetModule(ViewType.APReceipt);
                    //if (ApplicationSettings.IntakeMode == IntakeMode.Standard)
                    //{
                    //    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenReceiptPanelStatic;
                    //}
                    //else if (ApplicationSettings.IntakeMode == IntakeMode.StandardWithPallet)
                    //{
                    //    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenReceiptPalletPanelStatic;
                    //}
                    //else
                    //{
                    //    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenReceiptProductionPanelStatic;
                    //}
                   
                    ViewModelLocator.TouchscreenStatic.Module = Strings.Intake;
                    SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);
                    Messenger.Default.Send(ViewType.FactoryTouchScreen);
                }
                else
                {
                    ViewModelLocator.ClearIntoProduction();
                    ViewModelLocator.ClearOutOfProduction();
                    ViewModelLocator.ClearAPReceiptTouchscreen();
                    ViewModelLocator.ClearARReturnTouchscreen();
                    Messenger.Default.Send(ViewType.APReceipt);
                }

                SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.APReceipt))
            {
                SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);
            }
        }

        /// <summary>
        /// Set touchscreen into production mode.
        /// </summary>
        private void IntoProductionCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = true;

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Production))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.intoProduction);

            ViewModelLocator.ClearAPReceiptDetails();
            ViewModelLocator.ClearARDispatchDetails();
         

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearTouchscreenProductionOrders();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearProductionProductsCards();

                if (ApplicationSettings.TouchScreenMode)
                {
                    ViewModelLocator.ClearARDispatchDetails();
                    ViewModelLocator.ClearAPReceiptDetails();
                    this.Locator.Touchscreen.SetModule(ViewType.IntoProduction);
                    this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntoProductionMode ==
                                                              IntoProductionMode.IntoProductionStandard
                        ? ViewModelLocator.TouchscreenProductionPanelStatic
                        : ViewModelLocator.TouchscreenProductionRecallPanelStatic;
                    ViewModelLocator.TouchscreenStatic.Module = Strings.IntoProduction;
                    SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
                    Messenger.Default.Send(ViewType.FactoryTouchScreen);
                }
                else
                {

                    //SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
                    //Messenger.Default.Send(ViewType.ARDispatch);
                }

                return;
            }

            if (this.SwitchView(this.Locator.ARDispatch))
            {
                SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
            }
        }

        /// <summary>
        /// Set touchscreen out of production mode.
        /// </summary>
        private void OutOfProductionCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = true;

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.Production))
            //{
            //    return;
            //}

            ViewModelLocator.ClearAPReceiptDetails();
            ViewModelLocator.ClearARDispatchDetails();
   

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.outOfProduction);

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearTouchscreenOrders();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearIntoProduction();

                if (ApplicationSettings.TouchScreenMode)
                {
                    ViewModelLocator.ClearARDispatchDetails();
                    ViewModelLocator.ClearAPReceiptDetails();
                    this.Locator.Touchscreen.SetModule(ViewType.OutOfProduction);
                    //if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfProductionStandard 
                    //    || ApplicationSettings.OutOfProductionPanelMode == OutOfProductionPanel.Standard)
                    //{
                    //    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionPanelStatic;

                    //}
                    //else
                    //{
                    //    this.Locator.Touchscreen.PanelViewModel =
                    //        ViewModelLocator.TouchscreenOutOfProductionWithPiecesPanelStatic;
                    //}

                    ViewModelLocator.TouchscreenStatic.Module = Strings.OutOfProduction;
                    SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
                    Messenger.Default.Send(ViewType.FactoryTouchScreen);
                }
                else
                {

                    //SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
                    //Messenger.Default.Send(ViewType.ARDispatch);
                }

                return;
            }

            if (this.SwitchView(this.Locator.ARDispatch))
            {
                SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
            }
        }

        /// <summary>
        /// Set touchscreen dispatch mode.
        /// </summary>
        private void ARDispatchTouchscreenCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = true;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Touchscreen);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "ARDispatchTouchscreen";
            }

            this.ARDispatchCommandExecute();
        }

        /// <summary>
        /// Set touchscreen dispatch mode.
        /// </summary>
        private void ARReturnsTouchscreenCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = true;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Touchscreen);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "ARReturnsTouchscreen";
            }

            this.ARReturnsCommandExecute();
        }

        /// <summary>
        /// Set desktop dispatch mode.
        /// </summary>
        private void ARDispatchDesktopCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Desktop);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "ARDispatchDesktop";
            }

            this.ARDispatchCommandExecute();
        }

        /// <summary>
        /// Set desktop dispatch mode.
        /// </summary>
        private void ARReturnsDesktopCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Desktop);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "AReturnsDesktop";
            }

            this.ARReturnsCommandExecute();
        }

        /// <summary>
        /// Opens the ap receipt screen.
        /// </summary>
        private void ARReturnsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }
     
            if (true)
            {
                this.SwitchView(this.Locator.Blank);

                if (ApplicationSettings.TouchScreenMode)
                {
                    ViewModelLocator.ClearAPReceipt();
                    ViewModelLocator.ClearAPReceiptTouchscreen();
                    ViewModelLocator.ClearIntoProduction();
                    ViewModelLocator.ClearOutOfProduction();
                    ViewModelLocator.ClearProductionProductsCards();
                    ViewModelLocator.ClearARDispatch();
                    ViewModelLocator.ClearARDispatch2();
                    ViewModelLocator.ClearARDispatchTouchscreen();
                    this.Locator.Touchscreen.SetModule(ViewType.ARReturns);
                    this.Locator.TouchscreenOrders.SetModule(ViewType.ARReturns);
                   
                    ViewModelLocator.TouchscreenStatic.Module = Strings.Returns;
                    Messenger.Default.Send(ViewType.FactoryTouchScreen);
                }
                else
                {
                    ViewModelLocator.ClearAPReceipt();
                    ViewModelLocator.ClearAPReceiptTouchscreen();
                    ViewModelLocator.ClearARDispatch2();
                    ViewModelLocator.ClearARDispatch();
                    ViewModelLocator.ClearARDispatchTouchscreen();
                    ViewModelLocator.ClearIntoProduction();
                    ViewModelLocator.ClearOutOfProduction();
                    Messenger.Default.Send(ViewType.ARReturns);
                }

                return;
            }

            if (this.SwitchView(this.Locator.ARDispatch))
            {
                SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the ap receipt screen.
        /// </summary>
        private void ARDispatchCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.ARDispatch))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);

                if (ApplicationSettings.TouchScreenMode)
                {
                    ViewModelLocator.ClearARReturnTouchscreen();
                    ViewModelLocator.ClearAPReceiptTouchscreen();
                    ViewModelLocator.ClearARReturnTouchscreen();
                    ViewModelLocator.ClearIntoProduction();
                    ViewModelLocator.ClearOutOfProduction();
                    ViewModelLocator.ClearProductionProductsCards();
                    ViewModelLocator.ClearARDispatch();
                    ViewModelLocator.ClearARDispatch2();
                    this.Locator.Touchscreen.SetModule(ViewType.ARDispatch);
                    this.Locator.TouchscreenOrders.SetModule(ViewType.ARDispatch);
                    //if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight ||
                    //    ApplicationSettings.ScanOrWeighAtDispatch)
                    //{
                    //    if (ApplicationSettings.Customer == Customer.Woolleys || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithNotes)
                    //    {
                    //        this.Locator.Touchscreen.PanelViewModel =
                    //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
                    //    }
                    //    else if (ApplicationSettings.Customer == Customer.Ballon || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithShipping)
                    //    {
                    //        this.Locator.Touchscreen.PanelViewModel =
                    //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
                    //    }
                    //    else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
                    //    {
                    //        this.Locator.Touchscreen.PanelViewModel =
                    //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                    //    }
                    //    else
                    //    {
                    //        this.Locator.Touchscreen.PanelViewModel =
                    //            ViewModelLocator.TouchscreenDispatchPanelStatic;
                    //    }
                    //}
                    //else
                    //{
                    //    //if (ApplicationSettings.Customer == Customer.Woolleys || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithNotes)
                    //    //{
                    //    //    this.Locator.Touchscreen.PanelViewModel =
                    //    //        ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
                    //    //}
                    //    //else if (ApplicationSettings.Customer == Customer.Ballon || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithShipping)
                    //    //{
                    //    //    this.Locator.Touchscreen.PanelViewModel =
                    //    //        ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
                    //    //}
                    //    //else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
                    //    //{
                    //    //    this.Locator.Touchscreen.PanelViewModel =
                    //    //        ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                    //    //}
                    //    //else
                    //    //{
                    //    //    this.Locator.Touchscreen.PanelViewModel =
                    //    //        ViewModelLocator.TouchscreenDispatchPanelStatic;
                    //    //}
                    //}

                    //this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.DispatchMode == DispatchMode.ScanStock ? ViewModelLocator.TouchscreenDispatchPanelStatic : ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                    ViewModelLocator.TouchscreenStatic.Module = Strings.Dispatch;
                    SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
                    Messenger.Default.Send(ViewType.FactoryTouchScreen);
                }
                else
                {
                    ViewModelLocator.ClearARDispatchTouchscreen();
                    ViewModelLocator.ClearIntoProduction();
                    ViewModelLocator.ClearOutOfProduction();
                    SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
                    Messenger.Default.Send(ViewType.ARDispatch);
                }

                return;
            }

            if (this.SwitchView(this.Locator.ARDispatch))
            {
                SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
            }
        }

        /// <summary>
        /// Closes the application.
        /// </summary>
        private void CloseApplicationCommandExecute()
        {
            try
            {
                this.DataManager.ApplicationClosingHandler();
                Messenger.Default.Send(Token.Message, Token.CloseAllWindows);
                if (this.SwitchView(this.Locator.Exit))
                {
                    SystemMessage.Write(MessageType.Background, Message.ApplicationClosing);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("CloseApplicationCommandExecute():{0}", ex.Message));
            }
        }

        /// <summary>
        /// Opens the inventory master screen.
        /// </summary>
        private void InventoryMasterCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.InventoryMaster))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Master);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "INMaster";
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.InventoryMaster);
                SystemMessage.Write(MessageType.Background, Message.INMasterScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.BPMaster))
            {
                SystemMessage.Write(MessageType.Background, Message.INMasterScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the vat set up screen.
        /// </summary>
        private void WorkflowCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.VatSetUp))
            //{
            //    return;
            //}

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(Token.Message, Token.CreateWorkflow);
        }

        /// <summary>
        /// Opens the vat set up screen.
        /// </summary>
        private void WorkflowSearchCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.VatSetUp))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.HACCPSearch);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.WorkflowSearch);
        }

        /// <summary>
        /// Opens the vat set up screen.
        /// </summary>
        private void WorkflowAlertsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.VatSetUp))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Alerts);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.WorkflowAlerts);
        }

        /// <summary>
        /// Opens the vat set up screen.
        /// </summary>
        private void VatSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.VatSetUp))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.VatSetUp);
                SystemMessage.Write(MessageType.Background, Message.VatSetUpScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.VatSetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.VatSetUpScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the inventory group screen.
        /// </summary>
        private void InventoryGroupCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.InventoryGroup))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Group);
            if (this.CurrentNode != null)
            {
                this.CurrentNode.TreeNodeText = "INGroup";
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.InventoryGroup);
                SystemMessage.Write(MessageType.Background, Message.INGroupScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.InventoryGroup))
            {
                SystemMessage.Write(MessageType.Background, Message.INGroupScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the inventory group set up screen.
        /// </summary>
        private void InventoryGroupSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.InventoryGroupSetUp))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.InventoryGroupSetUp);
                SystemMessage.Write(MessageType.Background, Message.InventoryGroupSetUpScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.InventoryGroupSetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.InventoryGroupSetUpScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the traceability template name screen.
        /// </summary>
        private void TraceabilityTemplateNameCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.TraceabilityTemplateName))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.TraceabilityTemplateName);
                SystemMessage.Write(MessageType.Background, Message.AddNewTemplateScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.TraceabilityName))
            {
                SystemMessage.Write(MessageType.Background, Message.AddNewTemplateScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the traceability template allocation screen.
        /// </summary>
        private void TraceabilityTemplateallocationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.TraceabilityTemplateAllocation))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.TraceabilityTemplateAllocation);
                SystemMessage.Write(MessageType.Background, Message.TraceabilityTemplateAllocationScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.TraceabilityTemplateAllocation))
            {
                SystemMessage.Write(MessageType.Background, Message.TraceabilityTemplateAllocationScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the traceability template allocation screen.
        /// </summary>
        private void TraceabilityTemplateCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Traceability))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Traceability);
                SystemMessage.Write(MessageType.Background, Message.TraceabilityTemplateScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.TraceabilityMaster))
            {
                SystemMessage.Write(MessageType.Background, Message.TraceabilityTemplateScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the traceability template name screen.
        /// </summary>
        private void QualityTemplateNameCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.QualityTemplateName))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.QualityTemplateName);
                SystemMessage.Write(MessageType.Background, Message.AddNewTemplateScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.QualityName))
            {
                SystemMessage.Write(MessageType.Background, Message.AddNewTemplateScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the traceability template allocation screen.
        /// </summary>
        private void QualityTemplateallocationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.QualityTemplateAllocation))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.QualityTemplateAllocation);
                SystemMessage.Write(MessageType.Background, Message.QualityTemplateAllocationScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.QualityTemplateAllocation))
            {
                SystemMessage.Write(MessageType.Background, Message.QualityTemplateAllocationScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the traceability template allocation screen.
        /// </summary>
        private void QualityTemplateCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Quality))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Quality);
                SystemMessage.Write(MessageType.Background, Message.QualityTemplateScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Quality))
            {
                SystemMessage.Write(MessageType.Background, Message.QualityTemplateScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the dates template name screen.
        /// </summary>
        private void DatesTemplateNameCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.DateTemplateName))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.DateTemplateName);
                SystemMessage.Write(MessageType.Background, Message.AddNewTemplateScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.DateName))
            {
                SystemMessage.Write(MessageType.Background, Message.AddNewTemplateScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the dates template allocation screen.
        /// </summary>
        private void DatesTemplateAllocationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.DateTemplateAllocation))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.DateTemplateAllocation);
                SystemMessage.Write(MessageType.Background, Message.DatesSetUpScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Date))
            {
                SystemMessage.Write(MessageType.Background, Message.DatesSetUpScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the dates set up screen.
        /// </summary>
        private void DatesSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Date))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Date);
                SystemMessage.Write(MessageType.Background, Message.DatesSetUpScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Date))
            {
                SystemMessage.Write(MessageType.Background, Message.DatesSetUpScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the gsi identifier screen.
        /// </summary>
        private void Gs1AiIdentifierCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.GS1AI))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.GS1AI);
                SystemMessage.Write(MessageType.Background, Message.GS1ScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.LicenseImport))
            {
                SystemMessage.Write(MessageType.Background, Message.GS1ScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the import licence screen.
        /// </summary>
        private void ImportLicenceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.LicenseImport))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.importName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.LicenseImport);
                SystemMessage.Write(MessageType.Background, Message.LicenseImportLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.LicenseImport))
            {
                SystemMessage.Write(MessageType.Background, Message.LicenseImportLoaded);
            }
        }

        /// <summary>
        /// Opens the epos settings screen.
        /// </summary>
        private void EposSettingsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.EposSettings))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);

                Messenger.Default.Send(ViewType.EposSettings);
                SystemMessage.Write(MessageType.Background, Message.EposSettingsScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.EposSettings))
            {
                SystemMessage.Write(MessageType.Background, Message.EposSettingsScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the epos screen.
        /// </summary>
        private void EposCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Epos))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);

                if (this.Locator.Epos != null)
                {
                    this.Locator.Epos.UpdateProductData();
                }

                Messenger.Default.Send(ViewType.Epos);
                SystemMessage.Write(MessageType.Background, Message.EposLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Epos))
            {
                SystemMessage.Write(MessageType.Background, Message.EposLoaded);
            }
        }

        /// <summary>
        /// Opens the epos screen.
        /// </summary>
        private void MrpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            Messenger.Default.Send(ViewType.MRP);
        }

        /// <summary>
        /// Opens the price list screen.
        /// </summary>
        private void PricingListCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.PricingMaster))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.priceLists);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.PricingMaster);
                SystemMessage.Write(MessageType.Background, Message.PricingMasterLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.PricingMaster))
            {
                SystemMessage.Write(MessageType.Background, Message.PricingMasterLoaded);
            }
        }

        /// <summary>
        /// Opens the all price screen.
        /// </summary>
        private void AllPricesCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.PricingMaster))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.allPrices);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.AllPrices);
                this.Locator.PriceListDetail.GetAllDetails();
                SystemMessage.Write(MessageType.Background, Message.PricingMasterLoaded);
                return;
            }
        }

        /// <summary>
        /// Opens the price list screen.
        /// </summary>
        private void SpecialPricesCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.SpecialPrices))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.specialPrices);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.SpecialPrices);
                SystemMessage.Write(MessageType.Background, Message.SpecialPricesLoaded);
                return;
            }
        }

        /// <summary>
        /// Opens the plant set up screen.
        /// </summary>
        private void PlantSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Plant))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Plant);
                SystemMessage.Write(MessageType.Background, Message.PlantSetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Container))
            {
                SystemMessage.Write(MessageType.Background, Message.PlantSetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the currency screen.
        /// </summary>
        private void CurrencyCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Currency))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Currency);
                SystemMessage.Write(MessageType.Background, Message.CurrencyScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Container))
            {
                SystemMessage.Write(MessageType.Background, Message.CurrencyScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the container screen.
        /// </summary>
        private void ContainerSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.ContainerSetUp))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.ContainerSetUp);
                SystemMessage.Write(MessageType.Background, Message.ContainerSetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Container))
            {
                SystemMessage.Write(MessageType.Background, Message.ContainerSetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the uom screen.
        /// </summary>
        private void UOMSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.UOM))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.UOM);
                SystemMessage.Write(MessageType.Background, Message.UOMSetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.UOM))
            {
                SystemMessage.Write(MessageType.Background, Message.UOMSetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the authorisation screen.
        /// </summary>
        private void SelectAuthorisationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Authorisations))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.authorisationsName);

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Authorisations);
                SystemMessage.Write(MessageType.Background, Message.AuthorisationsLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Login))
            {
                SystemMessage.Write(MessageType.Background, Message.AuthorisationsLoaded);
            }
        }

        /// <summary>
        /// Opens the group screen.
        /// </summary>
        private void SelectGroupCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.UserGroup))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.UserGroup);
                SystemMessage.Write(MessageType.Background, Message.UserGroupScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.UserGroup))
            {
                SystemMessage.Write(MessageType.Background, Message.UserGroupScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the country set up screen.
        /// </summary>
        private void CountrySetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.CountrySetUp))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.CountrySetUp);
                SystemMessage.Write(MessageType.Background, Message.CountrySetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.CountryMaster))
            {
                SystemMessage.Write(MessageType.Background, Message.CountrySetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the device set up screen.
        /// </summary>
        private void DeviceSettingsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.DeviceSettings);
            return;
        }

        /// <summary>
        /// Opens the device set up screen.
        /// </summary>
        private void DeviceSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.DeviceSetUp))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.DeviceSetUp);
                SystemMessage.Write(MessageType.Background, Message.DeviceSetUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.DeviceSetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.DeviceSetUpLoaded);
            }
        }

        /// <summary>
        /// Opens the user set up screen.
        /// </summary>
        private void SelectUserSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.UserSetUp))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.userSetUpName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.UserSetUp);
                SystemMessage.Write(MessageType.Background, Message.UserSetUpScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.UserMaster))
            {
                SystemMessage.Write(MessageType.Background, Message.UserSetUpScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the warehouse screen.
        /// </summary>
        private void WarehouseSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Warehouse))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Warehouse);
                SystemMessage.Write(MessageType.Background, Message.WarehouseSetpUpLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.DeviceSetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.WarehouseSetpUpLoaded);
            }
        }

        /// <summary>
        /// Opens the warehouse screen.
        /// </summary>
        private void AttributeMasterSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.Warehouse))
            //{
            //    return;
            //}

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.AttributeMasterSetUp);
            SystemMessage.Write(MessageType.Background, Message.AttributeSetUpLoaded);
        }

        /// <summary>
        /// Opens the warehouse screen.
        /// </summary>
        private void TemplateAllocationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.Warehouse))
            //{
            //    return;
            //}

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.TemplateAllocation);
            SystemMessage.Write(MessageType.Background, Message.TemplateAllocationScreenLoaded);
        }

        /// <summary>
        /// Opens the warehouse screen.
        /// </summary>
        private void TabSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.Warehouse))
            //{
            //    return;
            //}

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Tabs);
        }


        /// <summary>
        /// Opens the warehouse location screen.
        /// </summary>
        private void WarehouseLocationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.WarehouseLocation))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.WarehouseLocation);
                SystemMessage.Write(MessageType.Background, Message.WarehouseLocationLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.DeviceSetUp))
            {
                SystemMessage.Write(MessageType.Background, Message.WarehouseLocationLoaded);
            }
        }

        /// <summary>
        /// Opens the switch user screen.
        /// </summary>
        private void SwitchUserCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Login))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.switchUserName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Login);
                SystemMessage.Write(MessageType.Background, Message.LoginLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Login))
            {
                SystemMessage.Write(MessageType.Background, Message.LoginLoaded);
            }
        }

        /// <summary>
        /// Opens the label design screen.
        /// </summary>
        private void SelectLabelDesignCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.LabelDesign))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.labelDesignName);
            var assembly = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var labeller = Path.Combine(assembly, "Nouvem.LabelDesigner.exe");

            if (File.Exists(labeller))
            {
                System.Diagnostics.Process.Start(labeller);
            }
        }

        /// <summary>
        /// Opens the document type screen.
        /// </summary>
        private void DocumentTypeCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.DocumentType))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.DocumentType);
                SystemMessage.Write(MessageType.Background, Message.DocumentTypeLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.DocumentType))
            {
                SystemMessage.Write(MessageType.Background, Message.DocumentTypeLoaded);
            }
        }

        /// <summary>
        /// Opens the document number screen.
        /// </summary>
        private void DocumentNumberCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.DocumentNumber))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.DocumentNumber);
                SystemMessage.Write(MessageType.Background, Message.DocumentNumberLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.DocumentNumber))
            {
                SystemMessage.Write(MessageType.Background, Message.DocumentNumberLoaded);
            }
        }

        /// <summary>
        /// Opens the map screen.
        /// </summary>
        private void MapCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Map))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.mapName);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Map);
            SystemMessage.Write(MessageType.Background, Message.MapScreenLoaded);
        }

        /// <summary>
        /// Opens the label association screen.
        /// </summary>
        private void LabelAssociationCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.LabelAssociation))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.labelAssociationName);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.LabelAssociation);
            SystemMessage.Write(MessageType.Background, Message.LabelAssociationScreenLoaded);
        }

        /// <summary>
        /// Opens the label association screen.
        /// </summary>
        private void ReportSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.LabelAssociation))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.ReportSetUp);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.ReportSetUp);
            SystemMessage.Write(MessageType.Background, Message.ReportSetUpScreenLoaded);
        }

        /// <summary>
        /// Opens the label association screen.
        /// </summary>
        private void DepartmentCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.LabelAssociation))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Departments);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Department);
        }

        /// <summary>
        /// Opens the label association screen.
        /// </summary>
        private void ReportFoldersCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.LabelAssociation))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.ReportFolders);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.ReportFolders);
            //SystemMessage.Write(MessageType.Background, Message.ReportSetUpScreenLoaded);
        }

        /// <summary>
        /// Opens the label association screen.
        /// </summary>
        private void ReportProcessCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.LabelAssociation))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.ReportProcess);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.ReportProcess);
            //SystemMessage.Write(MessageType.Background, Message.ReportSetUpScreenLoaded);
        }

        /// <summary>
        /// Opens the invoice screen.
        /// </summary>
        private void InvoiceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Invoice))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.invoiceName);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Invoice);
            SystemMessage.Write(MessageType.Background, Message.InvoiceScreenLoaded);
        }

        /// <summary>
        /// Opens the invoice screen.
        /// </summary>
        private void PurchaseInvoiceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, "Purchase Invoice");
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.PurchaseInvoice);
            SystemMessage.Write(MessageType.Background, Message.InvoiceScreenLoaded);
        }

        /// <summary>
        /// Opens the invoice screen.
        /// </summary>
        private void QualityAssuranceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.QualityAssuranceView))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.qualityAssuranceName);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.QualityAssuranceView);
            //SystemMessage.Write(MessageType.Background, Message.InvoiceScreenLoaded);
        }

        /// <summary>
        /// Opens the invoice screen.
        /// </summary>
        private void LairageDetailsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.QualityAssuranceView))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.LairageDetails);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.LairageSearch);
            this.Locator.LairageSearch.Mode = ViewType.LairageIntake;
            //SystemMessage.Write(MessageType.Background, Message.InvoiceScreenLoaded);
        }

        /// <summary>
        /// Opens the batch invoice screen.
        /// </summary>
        private void BatchInvoiceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.BatchInvoice))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.batchInvoiceName);
            this.SwitchView(this.Locator.Blank);

            this.Locator.Invoice.DisplayDispatches();
        }

        /// <summary>
        /// Opens the invoice screen.
        /// </summary>
        private void CreditNoteCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.CreditNote);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.CreditNote);
        }

        /// <summary>
        /// Opens the batch invoice screen.
        /// </summary>
        private void CreateCreditNoteCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.CreateCreditNote);
            this.SwitchView(this.Locator.Blank);

            this.Locator.ARReturn.DisplayReturns();
        }

        /// <summary>
        /// Opens the batch invoice screen.
        /// </summary>
        private void PurchaseBatchInvoiceCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.PurchaseInvoiceCreation);
            this.SwitchView(this.Locator.Blank);

            this.Locator.APInvoice.DisplayIntakes();
        }

        /// <summary>
        /// Opens the quote screen.
        /// </summary>
        private void QuoteCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Quote))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.quoteName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Quote);
                SystemMessage.Write(MessageType.Background, Message.QuoteScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Quote))
            {
                SystemMessage.Write(MessageType.Background, Message.QuoteScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the region screen.
        /// </summary>
        private void RegionCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Region))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Region);
                SystemMessage.Write(MessageType.Background, Message.RegionScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Region))
            {
                SystemMessage.Write(MessageType.Background, Message.RegionScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void EditTransactionsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.TransactionEditor))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.editTransactionsName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.TransactionEditor);
                SystemMessage.Write(MessageType.Background, Message.EditTransactionsScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.EditTransaction))
            {
                SystemMessage.Write(MessageType.Background, Message.EditTransactionsScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void IdentigenCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.EditTransaction))
            //{
            //    return;
            //}

           

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Identigen);
            this.SwitchView(this.Locator.Blank);
            this.Locator.LairageAnimalsSearch.Mode = ViewType.Identigen;
            Messenger.Default.Send(ViewType.LairageAnimalsSearch);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void ScotBeefCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.EditTransaction))
            //{
            //    return;
            //}this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.ScotBeefExport);
            this.SwitchView(this.Locator.Blank);
            this.Locator.LairageAnimalsSearch.Mode = ViewType.BeefReport;
            Messenger.Default.Send(ViewType.LairageAnimalsSearch);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void KillDetailsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.EditTransaction))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.KillDetails);
            this.SwitchView(this.Locator.Blank);
            this.Locator.LairageAnimalsSearch.Mode = ViewType.KillDetails;
            Messenger.Default.Send(ViewType.LairageAnimalsSearch);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void AnimalMovementsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.EditTransaction))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.AnimalMovements);
            this.SwitchView(this.Locator.Blank);
            this.Locator.LairageAnimalsSearch.Mode = ViewType.AnimalMovements;
            Messenger.Default.Send(ViewType.LairageAnimalsSearch);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void AnimalPricingCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            //if (!this.CheckAccess(ViewType.EditTransaction))
            //{
            //    return;
            //}

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.AnimalPricing);
            this.SwitchView(this.Locator.Blank);
            this.Locator.LairageAnimalsSearch.Mode = ViewType.AnimalPricing;
            Messenger.Default.Send(ViewType.LairageAnimalsSearch);
        }

        /// <summary>
        /// Opens the route screen.
        /// </summary>
        private void RouteCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Route))
            {
                return;
            }

            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Route);
                SystemMessage.Write(MessageType.Background, Message.RouteScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Route))
            {
                SystemMessage.Write(MessageType.Background, Message.RouteScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the order screen.
        /// </summary>
        private void OrderCommandExecute()
        {
            ApplicationSettings.TouchScreenMode = false;
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.orderName);
            Messenger.Default.Send(this.CurrentNode, Token.AddMenuToShortcut);

            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Order))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.orderName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.Order);
                SystemMessage.Write(MessageType.Background, Message.OrderScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.Order))
            {
                SystemMessage.Write(MessageType.Background, Message.OrderScreenLoaded);
            }
        }

        /// <summary>
        /// Opens the business partner screen.
        /// </summary>
        private void SelectContactCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.BPMaster))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.bPMasterName);
            if (true)
            {
                this.SwitchView(this.Locator.Blank);
                Messenger.Default.Send(ViewType.BPMaster);
                SystemMessage.Write(MessageType.Background, Message.BPScreenLoaded);
                return;
            }

            if (this.SwitchView(this.Locator.BPMaster))
            {
                SystemMessage.Write(MessageType.Background, Message.BPScreenLoaded);
            }
        }

        /// <summary>
        /// Display the report module.
        /// </summary>
        /// <param name="reportName">The report data(Path and Name).</param>
        private void DisplayShortcutReportCommandExecute(string reportName)
        {
            //var userShortcuts = NouvemGlobal.LoggedInUser.UserShortcuts.Where(x => !string.IsNullOrEmpty(x.ReportPath)).ToList();
            var userShortcuts = NouvemGlobal.LoggedInUser.UserShortcuts.Where(x => x.Deleted == null).ToList();
            var reportShortcut = userShortcuts.FirstOrDefault(x => x.Name.CompareIgnoringCase(reportName));
            if (reportShortcut != null)
            {
                var data = reportShortcut.ReportPath;
                var localReport = this.ReportManager.Reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(reportName));
                if (localReport != null)
                {
                    if (string.IsNullOrEmpty(reportShortcut.ReportPath))
                    {
                        reportShortcut.ReportPath = localReport.Path;
                        data = reportShortcut.ReportPath;
                    }

                    var path = reportShortcut.ReportPath;
                    var pathData = reportShortcut.ReportPath.Split(',');
                    if (pathData.Length > 1)
                    {
                        path = pathData.ElementAt(0);
                    }

                    data = string.Format("{0},{1},{2},{3}", path, localReport.Name,
                        localReport.Alias, localReport.IsHistoric.ToBool());
                }

                this.DisplayReportCommandExecute(data);
            }
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void SpecsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }
            
            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Specs);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Specs);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void BatchEditCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }
            if (!this.CheckAccess(ViewType.IntoProduction))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.BatchEdit);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.BatchEditor);
            SystemMessage.Write(MessageType.Background, Message.BatchEditScreenLoaded);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void BatchSetUpCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }
            if (!this.CheckAccess(ViewType.IntoProduction))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.BatchSetUp);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.BatchSetUp);
            SystemMessage.Write(MessageType.Background, Message.BatchSetUpScreenLoaded);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void ReceipeCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }
            if (!this.CheckAccess(ViewType.IntoProduction))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Receipe);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Recipe);
            SystemMessage.Write(MessageType.Background, Message.ReceipeSetUpLoaded);
        }

        /// <summary>
        /// Opens the edit transactions screen.
        /// </summary>
        private void SpecificationsCommandExecute()
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }
            if (!this.CheckAccess(ViewType.IntoProduction))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Specifications);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Specifications);
            SystemMessage.Write(MessageType.Background, Message.SpecificationsScreenLoaded);
        }

        /// <summary>
        /// Display the report module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void DisplayReportCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            var data = reportData.Split(',');
            var reportPath = data[0];
            var localReportName = data[1];
            var displayName = data.Length > 2 ? data[2] : string.Empty;
            var isHistoric = data.Length > 3 && data[3].ToBool();
            this.CurrentNode = new ApplicationMenu {TreeNodeText = displayName, DisplayName = localReportName, IsReport = true, ReportPath = reportData, IsHistoric = isHistoric};

            if (!isHistoric)
            {
                this.ReportManager.ProcessReport(reportPath, localReportName);
            }
            else
            {
                this.DisplayReportSearchScreen(reportPath, localReportName);
            }
        }

        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void AccountsCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Accounts))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, this.accountsName);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.Accounts);
            SystemMessage.Write(MessageType.Background, Message.AccountsScreenLoaded);
        }

        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void SyncPartnersCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Accounts))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.SyncPartners);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.SyncPartners);
            //SystemMessage.Write(MessageType.Background, Message.AccountsScreenLoaded);
        }

        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void SyncProductsCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Accounts))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.SyncProducts);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.SyncProducts);
            //SystemMessage.Write(MessageType.Background, Message.AccountsScreenLoaded);
        }
        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void SyncPricesCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Accounts))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.SyncPrices);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.SyncPrices);
            //SystemMessage.Write(MessageType.Background, Message.AccountsScreenLoaded);
        }

        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void AccountsPurchasesCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.Accounts))
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Purchases);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.PurchasesAccounts);
            SystemMessage.Write(MessageType.Background, Message.AccountsScreenLoaded);
        }

        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void AccountsCreditReturnsCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            this.GetNode(this.ApplicationMenu.ApplicationMenuNodes, Strings.Credits);
            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.ReturnsAccounts);
            SystemMessage.Write(MessageType.Background, Message.AccountsScreenLoaded);
        }

        /// <summary>
        /// Display the accounts module.
        /// </summary>
        /// <param name="reportData">The report data(Path and Name).</param>
        private void ContainerAccountsCommandExecute(string reportData)
        {
            if (!this.CheckForProcessingCompletion())
            {
                return;
            }

            if (!this.CheckAccess(ViewType.ContainerAccountsView))
            {
                return;
            }

            this.SwitchView(this.Locator.Blank);
            Messenger.Default.Send(ViewType.ContainerAccountsView);
            SystemMessage.Write(MessageType.Background, Message.ContainerAccountsScreenLoaded);
        }

        /// <summary>
        /// Handle the scanner module selection.
        /// </summary>
        /// <param name="module">The module to select.</param>
        private void SelectedScannerModuleCommandExecute(string module)
        {
            if (!module.Equals(Strings.Close))
            {
                this.IsBusy = true;
            }

            Messenger.Default.Send(Token.Message, Token.SetScannerSize);
            var worker = new BackgroundWorker();
            worker.RunWorkerCompleted += (sender, args) => this.IsBusy = false;
            worker.DoWork += (sender, args) =>
            {
                System.Threading.Thread.Sleep(500);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (module.Equals(Constant.Dispatch))
                    {
                        ApplicationSettings.TouchScreenMode = true;

                        if (!this.CheckForProcessingCompletion())
                        {
                            return;
                        }

                        if (!this.CheckAccess(ViewType.ARDispatch))
                        {
                            return;
                        }

                        ViewModelLocator.ClearARReturnTouchscreen();
                        ViewModelLocator.ClearAPReceiptTouchscreen();
                        ViewModelLocator.ClearIntoProduction();
                        ViewModelLocator.ClearOutOfProduction();
                        ViewModelLocator.ClearProductionProductsCards();
                        ViewModelLocator.ClearARDispatch();
                        ViewModelLocator.ClearARDispatch2();
                        ViewModelLocator.ClearARDispatchTouchscreen();
                        this.Locator.Touchscreen.SetModule(ViewType.ScannerDispatch);
                        this.Locator.TouchscreenOrders.SetModule(ViewType.ARDispatch);

                        Messenger.Default.Send(Token.Message, Token.CreateScannerFactory);
                        ViewModelLocator.ClearARDispatchTouchscreen();
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                    else if (module.Equals(Constant.Database))
                    {
                        //return;
                        Messenger.Default.Send(Token.Message, Token.CreateServerSetUpScanner);
                    }
                    else if (module.Equals(Constant.Sequencer))
                    {
                        //return;
                        Messenger.Default.Send(Token.Message, Token.CreateScannerFactorySequencer);
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                    else if (module.Equals(Constant.IntoProduction))
                    {
                        ViewModelLocator.ClearAPReceiptDetails();
                        ViewModelLocator.ClearARDispatchDetails();
                        ViewModelLocator.ClearAPReceiptDetails();
                        ViewModelLocator.ClearAPReceiptTouchscreen();
                        ViewModelLocator.ClearARReturnTouchscreen();
                        ViewModelLocator.ClearIntoProduction();
                        ViewModelLocator.ClearOutOfProduction();
                        ViewModelLocator.ClearProductionProductsCards();
                        ViewModelLocator.ClearARDispatch();
                        ViewModelLocator.ClearARDispatch2();
                        ViewModelLocator.ClearARDispatchTouchscreen();
                        ViewModelLocator.ClearScannerMainDispatch();
                   
                        Messenger.Default.Send(Token.Message, Token.CreateScannerIntoProduction);
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                    else if (module.Equals(Constant.StockTake))
                    {
                        Messenger.Default.Send(Token.Message, Token.CreateScannerFactoryStockTake);
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                    else if (module.Equals(Constant.Movement))
                    {
                        Messenger.Default.Send(Token.Message, Token.CreateScannerStockMovement);
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                    else if (module.Equals(Constant.Pallet))
                    {
                        Messenger.Default.Send(Token.Message, Token.CreatePalletisation);
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                    else
                    {
                        if (ApplicationSettings.ScannerEmulatorMode)
                        {
                            ApplicationSettings.ScannerMode = false;
                            ApplicationSettings.ScannerEmulatorMode = false;
                            Messenger.Default.Send(Token.Message, Token.CloseScannerFactory);
                            Messenger.Default.Send(Token.Message, Token.CloseScannerModuleSelection);
                        }
                        else
                        {
                            this.DataManager.SaveDeviceSettings();
                            Application.Current.Dispatcher.Invoke(Application.Current.Shutdown);
                        }
                    }

                    Messenger.Default.Send(Token.Message, Token.CloseScannerModuleSelection);
                }));
            };

            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Handler, that notifies all subscribers to switch control mode to that of the master window selected mode.
        /// </summary>
        /// <param name="mode">The mode to switch to.</param>
        private void SwitchControlCommandExecute(string mode)
        {
            Messenger.Default.Send(mode, Token.SwitchControl);
        }

        /// <summary>
        /// Application closing handler.
        /// </summary>
        private void OnClosingCommandExecuted()
        {
            this.DataManager.ApplicationClosingHandler();
        }

        #endregion

        #region helper

        #region module names

        /// <summary>
        /// Set the module names.
        /// </summary>
        public void SetModuleNames()
        {
            this.administrationName = Strings.Administration;
            this.handheldEmulatorName = Strings.HandheldEmulator;
            this.userSetUpName = Strings.User;
            this.userName = Strings.User;
            this.groupName = Strings.Group;
            this.authorisationsName = Strings.Authorisations;
            this.switchUserName = Strings.SwitchUser;
            this.deviceSetUpName = Strings.DeviceSetUp;
            this.reconciliation = Strings.Reconciliation;
            this.licencingName = Strings.Licencing;
            this.adminName = Strings.Admin;
            this.importName = Strings.Import;
            this.businessPartnerName = Strings.BusinessPartner;
            this.bPMasterName = Strings.Master;
            this.qualityAssuranceName = Strings.QualityAssurance;
            this.lairageName = Strings.Lairage;
            this.sequencerName = Strings.Sequencer;
            this.graderName = Strings.Grader;
            this.killLineName = Strings.KillLine;
            this.salesName = Strings.Sales;
            this.purchasingName = Strings.Purchasing;
            this.stockManagementName = Strings.StockManagement;
            this.productionName = Strings.Production;
            this.labelDesignName = Strings.Label;
            this.scannerName = Strings.Scanner;
            this.reportsName = Strings.Reports;
            this.gS1AIName = Strings.GS1AISetUp;
            this.datesSetUpName = Strings.Dates;
            this.datesMasterName = Strings.DatesMaster;
            this.stockTake = Strings.StockTake;
            this.datesTemplateName = Strings.TemplateName;
            this.datesTemplateAllocationName = Strings.TemplateAllocation;
            this.traceabilitySetUpName = Strings.Traceability;
            this.traceabilityMasterName = Strings.TraceabilityMaster;
            this.traceabilityTemplateName = Strings.TemplateName;
            this.traceabilityTemplateAllocationName = Strings.TemplateAllocation;
            this.countrySetUpName = Strings.CountryMasterSetUp;
            this.containerSetUpName = Strings.ContainerSetUp;
            this.bPPropertySetUpName = Strings.Property;
            this.itemMasterPropertySetUpName = Strings.Property;
            this.plantSetUpName = Strings.PlantSetUp;
            this.uomSetUpName = Strings.UOMSetUp;
            this.pricingSetUpName = Strings.Pricing;
            this.inventoryName = Strings.Inventory;
            this.inventoryMasterName = Strings.Master;
            this.inventoryGroupName = Strings.Group;
            this.eposName = Strings.EPOS;
            this.vatSetUpName = Strings.VatSetUp;
            this.warehouseSetUpName = Strings.WarehouseSetUp;
            this.documentTypeName = Strings.DocumentType;
            this.documentNumberName = Strings.DocumentNumber;
            this.documentSetUpName = Strings.DocumentSetUp;
            this.regionName = Strings.Region;
            this.routeName = Strings.Route;
            this.quoteName = Strings.Quote;
            this.eposMain = Strings.Main;
            this.eposSettings = Strings.EposSettings;
            this.orderName = Strings.Order;
            this.quality = Strings.Quality;
            this.dispatchName = Strings.Dispatch;
            this.bpGroupSetUpName = Strings.Group;
            this.reportName = Strings.Report;
            this.designName = Strings.Design;
            this.mapName = Strings.Map;
            this.currencyName = Strings.Currency;
            this.qualityName = Strings.QualityMaster;
            this.groupSetUpName = Strings.GroupSetUp;
            this.apReceiptName = Strings.Intake;
            this.apReceiptTouchscreenName = Strings.Touchscreen;
            this.apReceiptDesktopName = Strings.Desktop;
            this.editTransactionsName = Strings.EditTransactions;
            this.masterDataName = Strings.MasterDataSetUp;
            this.warehouse = Strings.Warehouse;
            this.warehouseLocation = Strings.WarehouseLocation;
            this.intoProduction = Strings.IntoProduction;
            this.outOfProduction = Strings.OutOfProduction;
            this.labelAssociationName = Strings.LabelAssociation;
            this.invoiceName = Strings.Invoice;
            this.batchInvoiceName = Strings.CreateInvoice;
            this.accountsName = Strings.Accounts;
            this.specialPrices = Strings.SpecialPrices;
            this.priceLists = Strings.PriceLists;
            this.allPrices = Strings.AllPrices;
            this.containerInvoiceExportName = Strings.ContainerInvoiceExport;
            this.invoiceExportName = Strings.InvoiceExport;
        }

        #endregion

        #region main menu

        /// <summary>
        /// Method that sets the main menu tree.
        /// </summary>
        private void SetMainMenu()
        {
            var userModules = this.DataManager.GetUserGroupsAndModules(this.ReportManager.Reports);
            var groupid = NouvemGlobal.LoggedInUser.UserMaster.UserGroupID;

            IList<string> allowableReports = null;
            var localUserModule = userModules.FirstOrDefault(x => x.UserGroupID == groupid);
            var allowAllModules = true;
            var allowedModules = new List<UserGroup>();
            if (localUserModule != null)
            {
                allowedModules = localUserModule.Modules.Where(x => x.IsSelected).ToList();
                allowAllModules = !localUserModule.Modules.Any();
                var reportModule = localUserModule.Modules.FirstOrDefault(x => x.Description.CompareIgnoringCase(Strings.Reports));
                if (reportModule != null)
                {
                    if (reportModule.Modules.Any())
                    {
                        allowableReports = reportModule.Modules.Where(x => x.IsSelected).Select(x => x.Description.Trim()).ToList();
                    }
                }
            }

            var touchscreenModules =
                allowedModules.FirstOrDefault(x => x.Description.CompareIgnoringCase("Touchscreen"));
            if (touchscreenModules != null)
            {
                this.AllowedTouchscreenModules = touchscreenModules.Modules.Where(x => x.IsSelected).ToList();
            }

            var handheldModules =
                allowedModules.FirstOrDefault(x => x.Description.CompareIgnoringCase("Handheld"));
            if (handheldModules != null)
            {
                this.AllowedHandheldModules = handheldModules.Modules.Where(x => x.IsSelected).ToList();
            }

            var menuNodes = new ObservableCollection<ApplicationMenu>();

            #region administration

            var admin = new ApplicationMenu
            {
                TreeNodeText = this.administrationName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Admin.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var adminModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 1);

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 16 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.masterDataName,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/MasterData.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = adminModules.Modules.FirstOrDefault(x => x.UserGroupID == 16 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 25 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.containerSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Container.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ContainerSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 26 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.countrySetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Country.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.CountrySetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 27 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.currencyName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Euro.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.CurrencyCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 28 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Departments,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Departments.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.DepartmentCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 29 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.deviceSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Devices.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.DeviceSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 151 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = "Device Settings",
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Devices.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.DeviceSettingsCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 30 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.documentSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Document.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                    };

                    var subsubModules = subModules.Modules.FirstOrDefault(x => x.UserGroupID == 30 && x.IsSelected);
                    if (subsubModules != null && subsubModules.Modules.Any(x => x.UserGroupID == 118 && x.IsSelected) ||
                        allowAllModules)
                    {
                        var localSubSubModule = new ApplicationMenu
                        {
                            TreeNodeText = this.documentNumberName,
                            DisplayImage =
                                new BitmapImage(
                                    new Uri("pack://application:,,,/Design/Image/DocumentNumber.png",
                                        UriKind.Absolute)),
                            Indent = new Thickness(90, 0, 0, 0),
                            Command = this.DocumentNumberCommand
                        };
                        
                        localSubModule.ApplicationMenuNodes.Add(localSubSubModule);
                    }

                    if (subsubModules != null && subsubModules.Modules.Any(x => x.UserGroupID == 119 && x.IsSelected) ||
                        allowAllModules)
                    {
                        var localSubSubModule = new ApplicationMenu
                        {
                            TreeNodeText = this.documentTypeName,
                            DisplayImage =
                                new BitmapImage(
                                    new Uri("pack://application:,,,/Design/Image/DocumentType.png",
                                        UriKind.Absolute)),
                            Indent = new Thickness(90, 0, 0, 0),
                            Command = this.DocumentTypeCommand
                        };

                        localSubModule.ApplicationMenuNodes.Add(localSubSubModule);
                    }

                    if (localSubModule.ApplicationMenuNodes.Any())
                    {
                        localModule.ApplicationMenuNodes.Add(localSubModule);
                    }
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 31 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.gS1AIName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Gs1ai.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.Gs1aiIdentifierCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 32 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.PaymentDeductions,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/PaymentDeductions.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.PaymentDeductionsCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 33 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.plantSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Plant.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.PlantSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 34 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.regionName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Region.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.RegionCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 35 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.routeName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Route.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.RouteCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 36 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.uomSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/UOM.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.UOMSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 37 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.vatSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/VatSetUp.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.VatSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 38 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.warehouseSetUpName,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/WarehouseMain.png", UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                    };

                    var subsubModules = subModules.Modules.FirstOrDefault(x => x.UserGroupID == 38 && x.IsSelected);
                    if (subsubModules != null && subsubModules.Modules.Any(x => x.UserGroupID == 120 && x.IsSelected) ||
                        allowAllModules)
                    {
                        var localSubSubModule = new ApplicationMenu
                        {
                            TreeNodeText = this.warehouse,
                            DisplayImage =
                                new BitmapImage(
                                    new Uri("pack://application:,,,/Design/Image/Warehouse.png",
                                        UriKind.Absolute)),
                            Indent = new Thickness(90, 0, 0, 0),
                            Command = this.WarehouseSetUpCommand
                        };

                        localSubModule.ApplicationMenuNodes.Add(localSubSubModule);
                    }

                    if (subsubModules != null && subsubModules.Modules.Any(x => x.UserGroupID == 121 && x.IsSelected) ||
                        allowAllModules)
                    {
                        var localSubSubModule = new ApplicationMenu
                        {
                            TreeNodeText = this.warehouseLocation,
                            DisplayImage =
                                new BitmapImage(
                                    new Uri(
                                        "pack://application:,,,/Design/Image/WarehouseLocation.png",
                                        UriKind.Absolute)),
                            Indent = new Thickness(90, 0, 0, 0),
                            Command = this.WarehouseLocationCommand
                        };

                        localSubModule.ApplicationMenuNodes.Add(localSubSubModule);
                    }

                    if (localSubModule.ApplicationMenuNodes.Any())
                    {
                        localModule.ApplicationMenuNodes.Add(localSubModule);
                    }
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 39 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.WeightBandSetUp,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/WeightBand.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.WeightBandSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    admin.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 17 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Attributes,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/DatesSetUp.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = adminModules.Modules.FirstOrDefault(x => x.UserGroupID == 17 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 40 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.AttributeSetUp,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Date.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.AttributeMasterSetUpCommand
                    };
                    
                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 41 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.TemplateAllocation,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/TemplateAllocation.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.TemplateAllocationCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 42 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.TabsSetUp,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/Tabs.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.TabSetUpCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    admin.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 18 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.editTransactionsName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/EditTransactions.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.EditTransactionsCommand
                };

                admin.ApplicationMenuNodes.Add(localModule);
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 19 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.labelAssociationName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/LabelAssociation.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.LabelAssociationCommand
                };

                admin.ApplicationMenuNodes.Add(localModule);
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 20 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.licencingName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Licencing.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = adminModules.Modules.FirstOrDefault(x => x.UserGroupID == 20 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 43 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.adminName,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/LicencingAdmin.png", UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.LicenseAdminCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 44 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.importName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Import.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ImportLicenseCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    admin.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 21 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.mapName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Map.png", UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.MapCommand,
                };

                admin.ApplicationMenuNodes.Add(localModule);
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 22 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.ReportSetUp,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/ReportSetUp.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = adminModules.Modules.FirstOrDefault(x => x.UserGroupID == 22 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 45 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Report,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Report.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ReportSetUpCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 46 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Folders,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Folder.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ReportFoldersCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 47 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.ReportProcess,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Data.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ReportProcessCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    admin.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 23 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.switchUserName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/SwitchUser.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.SwitchUserCommand
                };

                admin.ApplicationMenuNodes.Add(localModule);
            }

            if (adminModules != null && adminModules.Modules.Any(x => x.UserGroupID == 24 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.userSetUpName,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/UserSetUp.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = adminModules.Modules.FirstOrDefault(x => x.UserGroupID == 24 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 48 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.userName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/User.png",
                                UriKind.Absolute)),
                        Command = this.SelectUserSetUpCommand,
                        Indent = new Thickness(60, 0, 0, 0)
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 49 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.groupName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Group.png",
                                UriKind.Absolute)),
                        Command = this.SelectGroupCommand,
                        Indent = new Thickness(60, 0, 0, 0),
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 50 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.authorisationsName,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/Authorisation.png", UriKind.Absolute)),
                        Command = this.SelectAuthorisationsCommand,
                        Indent = new Thickness(60, 0, 0, 0),
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    admin.ApplicationMenuNodes.Add(localModule);
                }
            }

            #endregion

            #region Business partner

            var businessPartner = new ApplicationMenu
            {
                TreeNodeText = this.businessPartnerName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/BusinessPartner.png",
                        UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var partnerModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 2);

            if (partnerModules != null && partnerModules.Modules.Any(x => x.UserGroupID == 51 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.bPMasterName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/BPMaster.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.SelectContactCommand
                };

                businessPartner.ApplicationMenuNodes.Add(localModule);
            }

            if (partnerModules != null && partnerModules.Modules.Any(x => x.UserGroupID == 52 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.bpGroupSetUpName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/BPGroup.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.BPGroupSetUpCommand
                };

                businessPartner.ApplicationMenuNodes.Add(localModule);
            }

            if (partnerModules != null && partnerModules.Modules.Any(x => x.UserGroupID == 53 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.bPPropertySetUpName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Properties.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.BPPropertySetUpCommand
                };

                businessPartner.ApplicationMenuNodes.Add(localModule);
            }

            #endregion

            #region inventory

            var inventory = new ApplicationMenu
            {
                TreeNodeText = this.inventoryName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Inventory.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var inventoryModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 3);

            if (inventoryModules != null && inventoryModules.Modules.Any(x => x.UserGroupID == 54 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.inventoryMasterName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/InventoryMaster.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.InventoryMasterCommand
                };

                inventory.ApplicationMenuNodes.Add(localModule);
            }

            if (inventoryModules != null && inventoryModules.Modules.Any(x => x.UserGroupID == 55 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.inventoryGroupName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/InventoryGroup.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.InventoryGroupCommand
                };

                inventory.ApplicationMenuNodes.Add(localModule);
            }

            if (inventoryModules != null && inventoryModules.Modules.Any(x => x.UserGroupID == 56 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.groupSetUpName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/DatesSetUp.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.InventoryGroupSetUpCommand
                };

                inventory.ApplicationMenuNodes.Add(localModule);
            }

            if (inventoryModules != null && inventoryModules.Modules.Any(x => x.UserGroupID == 57 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.pricingSetUpName,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Pricing.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = inventoryModules.Modules.FirstOrDefault(x => x.UserGroupID == 57 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 60 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.priceLists,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/PriceLists.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.PricingListCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 61 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.allPrices,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/AllPrices.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.AllPricesCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 62 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.specialPrices,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/SpecialPrices.png", UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.SpecialPricesCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    inventory.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (inventoryModules != null && inventoryModules.Modules.Any(x => x.UserGroupID == 58 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.itemMasterPropertySetUpName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Properties.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.ItemMasterPropertySetUpCommand
                };

                inventory.ApplicationMenuNodes.Add(localModule);
            }

            if (inventoryModules != null && inventoryModules.Modules.Any(x => x.UserGroupID == 59 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Specs,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Specs.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.SpecsCommand
                };

                inventory.ApplicationMenuNodes.Add(localModule);
            }

            #endregion

            #region sales

            var sales = new ApplicationMenu
            {
                TreeNodeText = this.salesName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Sales.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var salesModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 5);

            if (salesModules != null && salesModules.Modules.Any(x => x.UserGroupID == 63 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.quoteName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Quote.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.QuoteCommand
                };

                sales.ApplicationMenuNodes.Add(localModule);
            }

            if (salesModules != null && salesModules.Modules.Any(x => x.UserGroupID == 64 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.orderName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Order.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.OrderCommand
                };

                sales.ApplicationMenuNodes.Add(localModule);
            }

            if (salesModules != null && salesModules.Modules.Any(x => x.UserGroupID == 65 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.dispatchName,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Dispatch.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = salesModules.Modules.FirstOrDefault(x => x.UserGroupID == 65 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 68 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.apReceiptDesktopName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/WindowsLogo.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ARDispatchDesktopCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 69 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.apReceiptTouchscreenName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Touchscreen.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ARDispatchTouchscreenCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 70 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Handheld,
                        DisplayImage =
                            new BitmapImage(
                                new Uri("pack://application:,,,/Design/Image/Handheld.png",
                                    UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.HandheldEmulatorCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 71 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Container,
                        DisplayImage =
                            new BitmapImage(
                                new Uri("pack://application:,,,/Design/Image/DispatchContainer.png",
                                    UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.DispatchContainerCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 72 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.FTrace,
                        DisplayImage =
                            new BitmapImage(
                                new Uri("pack://application:,,,/Design/Image/FTrace.png",
                                    UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.FTraceCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 150 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.QuickOrder,
                        DisplayImage =
                            new BitmapImage(
                                new Uri("pack://application:,,,/Design/Image/QuickOrder.png",
                                    UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.QuickOrderCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 152 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = "Carcass Dispatch",
                        DisplayImage =
                            new BitmapImage(
                                new Uri("pack://application:,,,/Design/Image/CarcassDispatch.png",
                                    UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.CarcassDispatchCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    sales.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (salesModules != null && salesModules.Modules.Any(x => x.UserGroupID == 66 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.invoiceName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Invoice.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = salesModules.Modules.FirstOrDefault(x => x.UserGroupID == 66 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 73 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.invoiceName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Quote.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.InvoiceCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 74 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.batchInvoiceName,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/CreateInvoice.png", UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.BatchInvoiceCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    sales.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (salesModules != null && salesModules.Modules.Any(x => x.UserGroupID == 67 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Telesales,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Telesales.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = salesModules.Modules.FirstOrDefault(x => x.UserGroupID == 67 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 75 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.SetUp,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/TelesalesSetUp.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Identifier = Strings.TelesalesSetUp,
                        Command = this.TelesalesSetUpCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 76 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Telesales,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Telesales.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.TelesalesCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    sales.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (salesModules != null && salesModules.Modules.Any(x => x.UserGroupID == 142 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Returns,
                    DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Returns.png",
                                UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = salesModules.Modules.FirstOrDefault(x => x.UserGroupID == 142 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 143 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.apReceiptDesktopName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/WindowsLogo.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ARReturnsDesktopCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 144 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.apReceiptTouchscreenName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Touchscreen.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ARReturnsTouchscreenCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                //if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 146 && x.IsSelected) ||
                //    allowAllModules)
                //{
                //    var localSubModule = new ApplicationMenu
                //    {
                //        TreeNodeText = Strings.CreditNote,
                //        DisplayImage =
                //            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Quote.png",
                //                UriKind.Absolute)),
                //        Indent = new Thickness(60, 0, 0, 0),
                //        Command = this.CreditNoteCommand,
                //    };

                //    localModule.ApplicationMenuNodes.Add(localSubModule);
                //}

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 147 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.CreateCreditNote,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/CreateInvoice.png", UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.CreateCreditNoteCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    sales.ApplicationMenuNodes.Add(localModule);
                }
            }

            #endregion

            #region purchasing

            var purchasing = new ApplicationMenu
            {
                TreeNodeText = this.purchasingName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Purchasing.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var purchaseModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 6);

            if (purchaseModules != null && purchaseModules.Modules.Any(x => x.UserGroupID == 77 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.quoteName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Quote.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.APQuoteCommand
                };

                purchasing.ApplicationMenuNodes.Add(localModule);
            }

            if (purchaseModules != null && purchaseModules.Modules.Any(x => x.UserGroupID == 78 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.orderName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Order.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.APOrderCommand
                };

                purchasing.ApplicationMenuNodes.Add(localModule);
            }

            if (purchaseModules != null && purchaseModules.Modules.Any(x => x.UserGroupID == 79 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.apReceiptName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/GoodsIn.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = purchaseModules.Modules.FirstOrDefault(x => x.UserGroupID == 79 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 80 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.apReceiptDesktopName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/WindowsLogo.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.APReceiptDesktopCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 81 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.apReceiptTouchscreenName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Touchscreen.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.APReceiptTouchscreenCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (purchasing.ApplicationMenuNodes.Any())
                {
                    purchasing.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (purchaseModules != null && purchaseModules.Modules.Any(x => x.UserGroupID == 139 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.invoiceName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Invoice.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = purchaseModules.Modules.FirstOrDefault(x => x.UserGroupID == 139 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 140 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.invoiceName,
                        Identifier = "Purchase Invoice",
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Quote.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.PurchaseInvoiceCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 141 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = this.batchInvoiceName,
                        DisplayImage =
                            new BitmapImage(new Uri(
                                "pack://application:,,,/Design/Image/CreateInvoice.png", UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.PurchaseBatchInvoiceCommand,
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    purchasing.ApplicationMenuNodes.Add(localModule);
                }
            }

            #endregion

            #region production

            var production = new ApplicationMenu
            {
                TreeNodeText = this.productionName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Production.png", UriKind.Absolute)),
                //DisplayImage = new BitmapImage(new Uri("pack://application:,,,/Images/appbar.list.creater.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var productionModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 9);

            if (productionModules != null && productionModules.Modules.Any(x => x.UserGroupID == 98 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.intoProduction,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/IntoProduction.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.IntoProductionCommand,
                };

                production.ApplicationMenuNodes.Add(localModule);
            }

            if (productionModules != null && productionModules.Modules.Any(x => x.UserGroupID == 99 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.outOfProduction,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/OutOfProduction.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.OutOfProductionCommand,
                };

                production.ApplicationMenuNodes.Add(localModule);
            }

            if (productionModules != null && productionModules.Modules.Any(x => x.UserGroupID == 100 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.BatchEdit,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/BatchEdit.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.BatchEditCommand,
                };

                production.ApplicationMenuNodes.Add(localModule);
            }

            if (productionModules != null && productionModules.Modules.Any(x => x.UserGroupID == 101 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Specifications,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Specifications.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.SpecificationsCommand,
                };

                production.ApplicationMenuNodes.Add(localModule);
            }

            if (productionModules != null && productionModules.Modules.Any(x => x.UserGroupID == 102 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Receipe,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Receipe.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.ReceipeCommand,
                };

                production.ApplicationMenuNodes.Add(localModule);
            }

            if (productionModules != null && productionModules.Modules.Any(x => x.UserGroupID == 103 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.BatchSetUp,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/BatchSetUp.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.BatchSetUpCommand,
                };

                production.ApplicationMenuNodes.Add(localModule);
            }

            #endregion

            #region accounts

            var accounts = new ApplicationMenu
            {
                TreeNodeText = this.accountsName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Accounts.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var accountsModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 10);

            if (accountsModules != null && accountsModules.Modules.Any(x => x.UserGroupID == 104 && x.IsSelected) || allowAllModules)
            {
                var accountsSales = new ApplicationMenu
                {
                    TreeNodeText = Strings.Sales,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/SalesAccounts.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.AccountsCommand,
                };

                accounts.ApplicationMenuNodes.Add(accountsSales);
            }

            if (accountsModules != null && accountsModules.Modules.Any(x => x.UserGroupID == 105 && x.IsSelected) || allowAllModules)
            {
                var purchases = new ApplicationMenu
                {
                    TreeNodeText = Strings.Purchases,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/PurchasesAccounts.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.AccountsPurchasesCommand,
                };

                accounts.ApplicationMenuNodes.Add(purchases);
            }

            if (accountsModules != null && accountsModules.Modules.Any(x => x.UserGroupID == 148 && x.IsSelected) || allowAllModules)
            {
                var purchases = new ApplicationMenu
                {
                    TreeNodeText = Strings.Credits,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/PurchasesAccounts.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.AccountsCreditsCommand,
                };

                accounts.ApplicationMenuNodes.Add(purchases);
            }

            if (accountsModules != null && accountsModules.Modules.Any(x => x.UserGroupID == 106 && x.IsSelected) || allowAllModules)
            {
                var syncData = new ApplicationMenu
                {
                    TreeNodeText = Strings.SyncData,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/LeftRight.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var syncDataModules = accountsModules.Modules.FirstOrDefault(x => x.UserGroupID == 106 && x.IsSelected);
                if (syncDataModules != null && syncDataModules.Modules.Any(x => x.UserGroupID == 107 && x.IsSelected) ||
                    allowAllModules)
                {
                    var syncPartners = new ApplicationMenu
                    {
                        TreeNodeText = Strings.SyncPartners,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Partners.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.SyncPartnersCommand,
                    };

                    syncData.ApplicationMenuNodes.Add(syncPartners);
                }

                if (syncDataModules != null && syncDataModules.Modules.Any(x => x.UserGroupID == 108 && x.IsSelected) ||
                    allowAllModules)
                {
                    var syncProducts = new ApplicationMenu
                    {
                        TreeNodeText = Strings.SyncProducts,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Sync.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.SyncProductsCommand,
                    };

                    syncData.ApplicationMenuNodes.Add(syncProducts);
                }

                if (syncDataModules != null && syncDataModules.Modules.Any(x => x.UserGroupID == 109 && x.IsSelected) ||
                    allowAllModules)
                {
                    var syncPrices = new ApplicationMenu
                    {
                        TreeNodeText = Strings.SyncPrices,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/SyncPrices.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.SyncPricesCommand,
                    };

                    syncData.ApplicationMenuNodes.Add(syncPrices);
                }

                if (syncData.ApplicationMenuNodes.Any())
                {
                    accounts.ApplicationMenuNodes.Add(syncData);
                }
            }

            #endregion

            #region stock management

            var stockManagement = new ApplicationMenu
            {
                TreeNodeText = this.stockManagementName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/StockManagement.png",
                        UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var stockModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 11);

            if (stockModules != null && stockModules.Modules.Any(x => x.UserGroupID == 110 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.stockTake,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/CreateStockTake.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.StockTakeCommand,
                };

                stockManagement.ApplicationMenuNodes.Add(localModule);
            }

            if (stockModules != null && stockModules.Modules.Any(x => x.UserGroupID == 111 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.reconciliation,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Reconciliation.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.StockReconciliationCommand,
                };

                stockManagement.ApplicationMenuNodes.Add(localModule);
            }

            if (stockModules != null && stockModules.Modules.Any(x => x.UserGroupID == 112 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.StockMove,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/ArrowRight.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.StockMoveCommand
                };

                stockManagement.ApplicationMenuNodes.Add(localModule);
            }

            if (stockModules != null && stockModules.Modules.Any(x => x.UserGroupID == 113 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.StockAdjust,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/authorisation.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.StockAdjustCommand,
                };

                stockManagement.ApplicationMenuNodes.Add(localModule);
            }

            #endregion

            #region kill line

            var killLine = new ApplicationMenu
            {
                TreeNodeText = this.killLineName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/KillLine.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var killModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 7);

            if (killModules != null && killModules.Modules.Any(x => x.UserGroupID == 82 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.lairageName,
                    DisplayImage =
                                new BitmapImage(new Uri("pack://application:,,,/Design/Image/Lairage.png",
                                    UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = killModules.Modules.FirstOrDefault(x => x.UserGroupID == 82 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 86 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Order,
                        Identifier = Strings.LairageOrder,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Lairage.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.LairageOrderCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 87 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Intake,
                        Identifier = Strings.LairageIntake,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/LairageIntake.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.LairageIntakeCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 88 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Touchscreen,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Touchscreen.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.LairageIntakeTouchscreenCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    killLine.ApplicationMenuNodes.Add(localModule);
                }
            }

            if (killModules != null && killModules.Modules.Any(x => x.UserGroupID == 83 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.sequencerName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Sequencer.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.SequencerCommand
                };

                killLine.ApplicationMenuNodes.Add(localModule);
            }

            if (killModules != null && killModules.Modules.Any(x => x.UserGroupID == 84 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.graderName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Grader.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.GraderCommand
                };

                killLine.ApplicationMenuNodes.Add(localModule);
            }

            if (killModules != null && killModules.Modules.Any(x => x.UserGroupID == 85 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.Data,
                    DisplayImage =
                                new BitmapImage(new Uri("pack://application:,,,/Design/Image/Data.png",
                                    UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
                };

                var subModules = killModules.Modules.FirstOrDefault(x => x.UserGroupID == 85 && x.IsSelected);
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 89 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.LairageDetails,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Information.png",
                                UriKind.Absolute)),
                        Command = this.LairageDetailsCommand,
                        Indent = new Thickness(60, 0, 0, 0),
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }
          
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 90 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.KillDetails,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/KillDetails.png",
                                UriKind.Absolute)),
                        Command = this.KillDetailsCommand,
                        Indent = new Thickness(60, 0, 0, 0),
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }
       
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 91 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.AnimalMovements,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Movement.png",
                                UriKind.Absolute)),
                        Command = this.AnimalMovementsCommand,
                        Indent = new Thickness(60, 0, 0, 0),
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }
           
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 92 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.AnimalPricing,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Prices.png",
                                UriKind.Absolute)),
                        Command = this.AnimalPricingCommand,
                        Indent = new Thickness(60, 0, 0, 0),
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }
             
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 93 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.Identigen,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Identigen.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.IdentigenCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }
      
                if (subModules != null && subModules.Modules.Any(x => x.UserGroupID == 94 && x.IsSelected) ||
                    allowAllModules)
                {
                    var localSubModule = new ApplicationMenu
                    {
                        TreeNodeText = Strings.ScotBeefExport,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/EposSettings.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(60, 0, 0, 0),
                        Command = this.ScotBeefCommand
                    };

                    localModule.ApplicationMenuNodes.Add(localSubModule);
                }

                if (localModule.ApplicationMenuNodes.Any())
                {
                    killLine.ApplicationMenuNodes.Add(localModule);
                }
            }

            #endregion

            #region payments

            var payments = new ApplicationMenu
            {
                TreeNodeText = Strings.Payments,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Payments.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var paymentModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 8);

            if (paymentModules != null && paymentModules.Modules.Any(x => x.UserGroupID == 95 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.PricingMatrix,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Matrix.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.PricingMatrixCommand
                };

                payments.ApplicationMenuNodes.Add(localModule);
            }

            if (paymentModules != null && paymentModules.Modules.Any(x => x.UserGroupID == 96 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.CreateBatchPaymentPaymentProposal,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/PaymentCreation.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.PaymentProposalCreationCommand
                };

                payments.ApplicationMenuNodes.Add(localModule);
            }

            if (paymentModules != null && paymentModules.Modules.Any(x => x.UserGroupID == 97 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = Strings.PaymentProposal,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Add.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.PaymentCommand
                };

                payments.ApplicationMenuNodes.Add(localModule);
            }

            #endregion

            #region design

            var design = new ApplicationMenu
            {
                TreeNodeText = this.designName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Design.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var designModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 12);

            if (designModules != null && designModules.Modules.Any(x => x.UserGroupID == 114 && x.IsSelected) || allowAllModules)
            {
                var localModule = new ApplicationMenu
                {
                    TreeNodeText = this.labelDesignName,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Barcode.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.SelectLabelDesignCommand
                };

                design.ApplicationMenuNodes.Add(localModule);
            }

            #endregion

            #region epos

            var epos = new ApplicationMenu
            {
                TreeNodeText = this.eposName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Epos.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>
                {
                    new ApplicationMenu
                    {
                        TreeNodeText = this.eposMain,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/EposMain.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(30, 0, 0, 0),
                        Command = this.EposCommand
                    },

                    new ApplicationMenu
                    {
                        TreeNodeText = this.eposSettings,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/EposSettings.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(30, 0, 0, 0),
                        Command = this.EposSettingsCommand
                    }
                }
            };

            #endregion

            #region MRP

            var mrp = new ApplicationMenu
            {
                TreeNodeText = Strings.MRP,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Calculator.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                Command = this.MrpCommand
            };

            #endregion

            #region haccp

            var haccp = new ApplicationMenu
            {
                TreeNodeText = Strings.HACCP,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Workflow.png",
                        UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0),
                ApplicationMenuNodes = new ObservableCollection<ApplicationMenu>()
            };

            var haccpModules = allowedModules.FirstOrDefault(x => x.UserGroupID == 14);

            if (haccpModules != null && haccpModules.Modules.Any(x => x.UserGroupID == 115 && x.IsSelected) || allowAllModules)
            {
                var localHaccp = new ApplicationMenu
                {
                    TreeNodeText = Strings.HACCP,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Workflow.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.WorkflowCommand
                };

                haccp.ApplicationMenuNodes.Add(localHaccp);
            }

            if (haccpModules != null && haccpModules.Modules.Any(x => x.UserGroupID == 116 && x.IsSelected) || allowAllModules)
            {
                var localHaccp = new ApplicationMenu
                {
                    TreeNodeText = Strings.HACCPSearch,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Find.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.WorkflowSearchCommand
                };

                haccp.ApplicationMenuNodes.Add(localHaccp);
            }

            if (haccpModules != null && haccpModules.Modules.Any(x => x.UserGroupID == 117 && x.IsSelected) || allowAllModules)
            {
                var localHaccp = new ApplicationMenu
                {
                    TreeNodeText = Strings.Alerts,
                    DisplayImage =
                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Information.png",
                            UriKind.Absolute)),
                    Indent = new Thickness(30, 0, 0, 0),
                    Command = this.WorkflowAlertsCommand
                };

                haccp.ApplicationMenuNodes.Add(localHaccp);
            }

            #endregion

            #region reports

            this.reports = new ApplicationMenu
            {
                TreeNodeText = this.reportsName,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Reports.png", UriKind.Absolute)),
                //DisplayImage = new BitmapImage(new Uri("pack://application:,,,/Images/appbar.list.creater.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0)
            };

            if (ApplicationSettings.ConnectToSSRS)
            {
                try
                {
                    var folderItems = this.reportingService.ListChildren("/", true).Where(item => item.TypeName.Equals("Report") && !item.Name.StartsWith("SubReport")).ToArray();
                    this.SetReportMenu(folderItems, this.reportMenuModes, 30, allowableReports, this.ReportManager.ReportFolders);
                    var folderReports = this.ReportManager.Reports.Where(x => x.ReportFolderID.HasValue)
                        .Select(x => x.Name).ToList();
                    foreach (var catalogItem in folderItems)
                    {
                        if (!folderReports.Contains(catalogItem.Name))
                        {
                            if (allowableReports != null && !allowableReports.Contains(catalogItem.Name.Trim()))
                            {
                                continue;
                            }

                            var displayName = catalogItem.Name;
                            var isHistoric = false;
                            var localReport = this.ReportManager.Reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(catalogItem.Name));
                            if (localReport != null)
                            {
                                isHistoric = localReport.IsHistoric.ToBool();
                                if (!string.IsNullOrEmpty(localReport.Alias))
                                {
                                    displayName = localReport.Alias;
                                }
                            }

                            this.reportMenuModes.Add(new ApplicationMenu
                            {
                                DisplayName = catalogItem.Name,
                                TreeNodeText = displayName,
                                DisplayImage =
                                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Report.png",
                                        UriKind.Absolute)),
                                Indent = new Thickness(30, 0, 0, 0),
                                IsHistoric = isHistoric,
                                CommandParameter = string.Format("{0},{1},{2},{3}", catalogItem.Path, catalogItem.Name, displayName, isHistoric),
                                Command = new RelayCommand<string>(this.DisplayReportCommandExecute)
                            });
                        }
                    }
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                this.reports.ApplicationMenuNodes = this.reportMenuModes;
            }

            #endregion

            #region close

            var close = new ApplicationMenu
            {
                TreeNodeText = Strings.Close,
                Command = this.CloseApplicationCommand,
                DisplayImage =
                    new BitmapImage(new Uri("pack://application:,,,/Design/Image/Exit.png", UriKind.Absolute)),
                Indent = new Thickness(0, 0, 0, 0)
            };

            #endregion

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Administration")) || allowAllModules)
            {
                menuNodes.Add(admin);
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Business Partner")) || allowAllModules)
            {
                menuNodes.Add(businessPartner);
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Inventory")) || allowAllModules)
            {
                menuNodes.Add(inventory);
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Sales")) || allowAllModules)
            {
                if (sales.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(sales);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Purchasing")) || allowAllModules)
            {
                if (purchasing.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(purchasing);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Kill Line")) || allowAllModules)
            {
                menuNodes.Add(killLine);
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Payments")) || allowAllModules)
            {
                menuNodes.Add(payments);
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Production")) || allowAllModules)
            {
                if (production.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(production);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("MRP")) || allowAllModules)
            {
                //if (mrp.ApplicationMenuNodes.Any())
                //{
                    menuNodes.Add(mrp);
                //}
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Accounts")) || allowAllModules)
            {
                if (accounts.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(accounts);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Stock Management")) || allowAllModules)
            {
                if (stockManagement.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(stockManagement);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Design")) || allowAllModules)
            {
                if (design.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(design);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("EPOS")) || allowAllModules)
            {
                menuNodes.Add(epos);
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("HACCP")) || allowAllModules)
            {
                if (haccp.ApplicationMenuNodes.Any())
                {
                    menuNodes.Add(haccp);
                }
            }

            if (allowedModules.Any(x => x.Description.CompareIgnoringCase("Reports")) || allowAllModules)
            {
                menuNodes.Add(this.reports);
            }

            menuNodes.Add(close);

            this.ApplicationMenu = new ApplicationMenu { ApplicationMenuNodes = menuNodes };
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets up the alert checker.
        /// </summary>
        private void AlertCheckSetUp()
        {
            if (ApplicationSettings.CheckForAlerts)
            {
                this.CheckAlerts();
                var time = ApplicationSettings.CheckForAlertsTime == 0 ? 5 : ApplicationSettings.CheckForAlertsTime;
                var alertTimer = new DispatcherTimer { Interval = TimeSpan.FromMinutes(time) };
                alertTimer.Start();
                alertTimer.Tick += (sender, args) =>
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(this.CheckAlerts));
                };
            }
        }

        /// <summary>
        /// Checks for alerts.
        /// </summary>
        private void CheckAlerts()
        {
            var usersToCheck = new HashSet<int?>();
            usersToCheck.Add(NouvemGlobal.UserId);
            var userAlerts = this.DataManager.GetUserLookUps(NouvemGlobal.UserId.ToInt());
            foreach (var userAlertsLookUp in userAlerts)
            {
                if (!userAlertsLookUp.IsGroup)
                {
                    usersToCheck.Add(userAlertsLookUp.UserOrGroupID);
                }
                else
                {
                    var localUsers =
                        NouvemGlobal.Users.Where(x => x.UserMaster.UserGroupID == userAlertsLookUp.UserOrGroupID);
                    foreach (var localUser in localUsers)
                    {
                        usersToCheck.Add(localUser.UserMaster.UserMasterID);
                    }
                }
            }

            var alerts = this.DataManager.AreThereNewAlerts(usersToCheck, ApplicationSettings.DisplayNewAlertsMessageUntilAlertOpened);
            if (alerts)
            {
                Messenger.Default.Send(true, Token.NewAlerts);
            }
        }

        private void HandleShortcutSelection(string shortcut)
        {
            switch (shortcut)
            {
                case "Order":
                    this.OrderCommandExecute();
                    break;
            }
        }

        /// <summary>
        /// Get the current menu node.
        /// </summary>
        /// <param name="nodes">The nodes to recursively traverse.</param>
        /// <param name="name">The node name to search for.</param>
        private void GetNode(ObservableCollection<ApplicationMenu> nodes, string name)
        {
            if (nodes == null || !nodes.Any())
            {
                return;
            }

            foreach (var menu in nodes)
            {
                if (menu.Identifier.Equals(name))
                {
                    this.CurrentNode = new ApplicationMenu
                    {
                        TreeNodeText = menu.Identifier,
                        DisplayImage = menu.DisplayImage,
                        Indent = menu.Indent,
                        ReportPath = menu.ReportPath,
                        IsReport = menu.IsReport,
                        Command = menu.Command,
                        CommandParameter = menu.CommandParameter
                    };

                    return;
                }
                else
                {
                    this.GetNode(menu.ApplicationMenuNodes, name);
                }
            }
        }

        private void SetReportMenu(CatalogItem[] folderItems, ObservableCollection<ApplicationMenu> nodes, int indent, IList<string> allowableReports,IList<ReportFolder> folders, ApplicationMenu reportNode = null)
        {
            if (folderItems == null)
            {
                folderItems = this.reportingService.ListChildren("/", false);
            }
            
            foreach (var reportFolder in folders)
            {
                if (reportFolder.ParentID.HasValue && folders.Select(x => x.ReportFolderID).Distinct().Count() > 1)
                {
                    continue;
                }

                var folderNode = new ApplicationMenu
                {
                    DisplayName = reportFolder.Name,
                    TreeNodeText = reportFolder.Name,
                    DisplayImage =
               new BitmapImage(new Uri("pack://application:,,,/Design/Image/Folder.png",
                   UriKind.Absolute)),
                    Indent = new Thickness(indent, 0, 0, 0),
                    //ApplicationMenuNodes = localNodes
                };

                var folderReports =
                    this.ReportManager.Reports.Where(x => x.ReportFolderID == reportFolder.ReportFolderID);
                foreach (var folderReport in folderReports)
                {
                    if (allowableReports != null && !allowableReports.Contains(folderReport.Name.Trim()))
                    {
                        continue;
                    }

                    var path = string.Empty;
                    var ssrsItem = folderItems.FirstOrDefault(x =>
                        x.TypeName.Equals("Report") && x.Name.CompareIgnoringCase(folderReport.Name));
                    if (ssrsItem != null)
                    {
                        path = ssrsItem.Path;
                    }

                    var displayName = folderReport.Name;
                    if (!string.IsNullOrEmpty(folderReport.Alias))
                    {
                        displayName = folderReport.Alias;
                    }

                    var localNode = new ApplicationMenu
                    {
                        DisplayName = folderReport.Name,
                        TreeNodeText = displayName,
                        DisplayImage =
                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Report.png",
                                UriKind.Absolute)),
                        Indent = new Thickness(indent + 30, 0, 0, 0),
                        IsHistoric = folderReport.IsHistoric,
                        CommandParameter = string.Format("{0},{1},{2},{3}", path, folderReport.Name, displayName, folderReport.IsHistoric.ToBool()),
                        Command = new RelayCommand<string>(this.DisplayReportCommandExecute)
                    };

                    folderNode.ApplicationMenuNodes.Add(localNode);
                }

                if (!folderNode.ApplicationMenuNodes.Any())
                {
                    continue;
                }

                if (reportNode == null)
                {
                    nodes.Add(folderNode);
                }
                else
                {
                    reportNode.ApplicationMenuNodes.Add(folderNode);
                }
           
                var localIndent = indent + 30;
                var localFolders = this.ReportManager.ReportFolders.Where(x => x.ParentID == reportFolder.ReportFolderID).ToList();
                if (localFolders.Any())
                {
                    this.SetReportMenu(folderItems, nodes, localIndent, allowableReports, localFolders, folderNode);
                }
            }
        }
        
        ///// <summary>
        ///// Recursively populates the report structure based on the server folders and reports.
        ///// </summary>
        ///// <param name="folderItems">The report server items to iterate.</param>
        ///// <param name="nodes">The hierarchical nodes to populate.</param>
        ///// <param name="indent">The indent value.</param>
        //private void SetReportMenu(CatalogItem[] folderItems, ObservableCollection<ApplicationMenu> nodes, int indent, IList<string> allowableReports)
        //{
        //    try
        //    {
        //        if (folderItems == null)
        //        {
        //            folderItems = this.reportingService.ListChildren("/", false);
        //        }

        //        foreach (var item in folderItems)
        //        {
        //            // if it's a report parent folder, recursively iterate it's contents.
        //            if (item.TypeName.Equals("Folder") && item.Name != "Data Sources" && item.Name != "Datasets" && !item.Name.StartsWith("SubReport"))
        //            {
        //                var localNodes = new ObservableCollection<ApplicationMenu>();
        //                var isNouvemRoot = item.Name.Equals("Nouvem.Reports");

        //                if (!isNouvemRoot)
        //                {
        //                    nodes.Add(new ApplicationMenu
        //                    {
        //                        DisplayName = item.Name,
        //                        TreeNodeText = item.Name,
        //                        DisplayImage =
        //                            new BitmapImage(new Uri("pack://application:,,,/Design/Image/Folder.png",
        //                                UriKind.Absolute)),
        //                        Indent = new Thickness(indent, 0, 0, 0),
        //                        ApplicationMenuNodes = localNodes
        //                    });
        //                }

        //                var nextNodes = isNouvemRoot ? nodes : localNodes;
        //                var localIndent = isNouvemRoot ? 0 : 30;

        //                var localItems = this.reportingService.ListChildren(item.Path, false);
        //                this.SetReportMenu(localItems, nextNodes, indent + localIndent, allowableReports);
        //            }
        //            else if (item.TypeName.Equals("Report") && !item.Name.StartsWith("SubReport") && (allowableReports == null || allowableReports.Contains(item.Name.Trim())))
        //            {
        //                var displayName = item.Name;
        //                var correspondingItem =
        //                    this.ReportManager.Reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(item.Name));

        //                if (correspondingItem != null && !string.IsNullOrWhiteSpace(correspondingItem.Alias))
        //                {
        //                    displayName = correspondingItem.Alias;
        //                }

        //                var localNode = new ApplicationMenu
        //                {
        //                    DisplayName = item.Name,
        //                    TreeNodeText = displayName,
        //                    DisplayImage =
        //                        new BitmapImage(new Uri("pack://application:,,,/Design/Image/Report.png",
        //                            UriKind.Absolute)),
        //                    Indent = new Thickness(indent, 0, 0, 0),
        //                    CommandParameter = string.Format("{0},{1},{2}", item.Path, item.Name,displayName),
        //                    Command = new RelayCommand<string>(this.DisplayReportCommandExecute)
        //                };

        //                nodes.Add(localNode);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SystemMessage.Write(MessageType.Issue, ex.Message);
        //        this.Log.LogError(this.GetType(), string.Format("SetReportMenu(): Ex:{0}, Inner:{1}", ex.Message, ex.InnerException));
        //    }
        //}

        /// <summary>
        /// Disallows entry, until application processing complete.
        /// </summary>
        /// <returns>A flag, indicating initial application processing completion, or not.</returns>
        private bool CheckForProcessingCompletion()
        {
            //if (!NouvemGlobal.ApplicationLoaded)
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.ApplicationLoading);
            //    return false;
            //}

            return true;
        }

        /// <summary>
        /// Sets the initial application message.
        /// </summary>
        private void SetProgressMessage()
        {
            if (ProgressBar.IsProgressComplete())
            {
                SystemMessage.Write(MessageType.Priority, Message.ApplicationLoaded);
            }
            else
            {
                SystemMessage.Write(MessageType.Background, Message.ApplicationLoading);
            }
        }

        /// <summary>
        /// Handles the vm commands to drill down.
        /// </summary>
        /// <param name="data">The drill down data.</param>
        private void DrillDown(Tuple<ViewType, int> data)
        {
            var type = data.Item1;
            var value = data.Item2;

            switch (type)
            {
                 case ViewType.BPMaster:
                    this.SelectContactCommandExecute();

                    var partner = NouvemGlobal.BusinessPartners.FirstOrDefault(
                                                 x => x.Details.BPMasterID == value);

                    Messenger.Default.Send(partner, Token.BusinessPartnerSelected);
                    break;

                case ViewType.UserSetUp:
                     Messenger.Default.Send(ViewType.UserSetUp);
                     Messenger.Default.Send(value, Token.UserSelected);
                    break;

                case ViewType.BPGroupSetUp:
                    this.BPGroupsetUpCommandExecute();
                    break;

                case ViewType.Currency:
                    this.CurrencyCommandExecute();
                    break;

                case ViewType.Route:
                    this.RouteCommandExecute();
                    break;

                case ViewType.PriceListDetail:
                    Messenger.Default.Send(ViewType.PriceListDetail);
                    Messenger.Default.Send(value, Token.DisplayPriceList);
                    break;

                case ViewType.Warehouse:
                    Messenger.Default.Send(ViewType.Warehouse);
                    Messenger.Default.Send(value, Token.DisplaySelectedWarehouse);
                    break;

                case ViewType.DateTemplateName:
                    this.DatesTemplateNameCommandExecute();
                    break;

                case ViewType.QualityTemplateName:
                    this.QualityTemplateNameCommandExecute();
                    break;

                case ViewType.TraceabilityTemplateName:
                    this.TraceabilityTemplateNameCommandExecute();
                    break;

                case ViewType.VatSetUp:
                    this.VatSetUpCommandExecute();
                    break;

                case ViewType.InventoryGroup:
                    this.InventoryGroupCommandExecute();
                    Messenger.Default.Send(value, Token.DisplayInventoryGroup);
                    break;

                case ViewType.UserGroup:
                    this.SelectGroupCommandExecute();
                    break;

                case ViewType.InventoryMaster:
                    this.InventoryMasterCommandExecute();

                    if (this.Locator.InventoryMaster != null)
                    {
                        this.Locator.InventoryMaster.SelectedInventoryItem =
                        NouvemGlobal.InventoryItems.FirstOrDefault(
                          x => x.Master.INMasterID == value);
                    }

                    break;

                case ViewType.InventoryGroupSetUp:
                    this.InventoryGroupSetUpCommandExecute();
                    break;

                case ViewType.ContainerSetUp:
                    this.ContainerSetUpCommandExecute();
                    Messenger.Default.Send(value, Token.SetSelectedContainer);
                    break;
            }
        }

        /// <summary>
        /// Handles the vm commands to drill down. (overloading the above to include extra parameter).
        /// </summary>
        /// <param name="data">The drill down data.</param>
        private void DrillDown(Tuple<ViewType, int, int> data)
        {
            var type = data.Item1;
            var value = data.Item2;
            var value2 = data.Item3;

            switch (type)
            {
                case ViewType.PriceListDetail:
                    Messenger.Default.Send(ViewType.PriceListDetail);
                    Messenger.Default.Send(Tuple.Create(value,value2), Token.DisplayPriceList);
                    break;
            }
        }

        /// <summary>
        /// Handles the vm commands to drill down. (overloading the above to include extra parameter).
        /// </summary>
        /// <param name="data">The drill down data.</param>
        private void DrillDown(Tuple<ViewType, int, int, int> data)
        {
            var type = data.Item1;
            var value = data.Item2;
            var value2 = data.Item3;
            var value3 = data.Item4;

            switch (type)
            {
                case ViewType.SpecialPrices:
                    this.SpecialPricesCommandExecute();
                    Messenger.Default.Send(Tuple.Create(value,value2,value3), Token.SetSpecialPrice);
                    break;
            }
        }

        /// <summary>
        /// Checks the user authorisation for the input view module.
        /// </summary>
        /// <param name="view">The module to check a user authorisation for.</param>
        /// <returns>A flag, indicating whether the user is authorised to access the input module.</returns>
        private bool CheckAccess(ViewType view)
        {
            return true;
        }

        /// <summary>
        /// Displays the search screen corresponding to the selected report.
        /// </summary>
        /// <param name="path">The selected report path.</param>
        /// <param name="report">The selected report name.</param>
        private void DisplayReportSearchScreen(string path, string report)
        {
            this.Locator.ReportSalesSearchData.AssociatedView = ViewType.APOrder;
            var orderStatuses = new List<int?>
            {
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID, 
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
            };

            IList<Sale> reportItems = null;

            this.Locator.ReportSalesSearchData.SetData(false);
            this.Log.LogDebug(this.GetType(), string.Format("DisplayReportSearchScreen(): Path:{0}, Report:{1}", path, report));
            var displayProductionSearchScreen = false;
            var displayTransactionSearchScreen = false;
            this.reportToDisplay = report;
            if (report.Equals(ReportName.CarcassTrace))
            {
                this.Locator.ReportSalesSearchData.FromCarcassDate = DateTime.Today;
                this.Locator.ReportSalesSearchData.ToCarcassDate = DateTime.Today;
                var fromDate = this.Locator.ReportSalesSearchData.FromCarcassDate;
                var toDate = this.Locator.ReportSalesSearchData.ToCarcassDate;
                
                reportItems = this.DataManager.GetCarcasses(fromDate,toDate);
                Messenger.Default.Send(reportItems, Token.SearchForCarcassSale);
                this.Locator.ReportSalesSearchData.SetGroups(reportItems);
                return;
            }

            if (report.Equals(ReportName.LabelKillInformation))
            {
                this.Locator.LairageAnimalsSearch.Mode = ViewType.LairageKillInformation;
                Messenger.Default.Send(ViewType.LairageAnimalsSearch);
                return;
            }

            if (report.CompareIgnoringCase(ReportName.RPAExportReport))
            {
                this.Locator.LairageAnimalsSearch.Mode = ViewType.RPAReport;
                Messenger.Default.Send(ViewType.LairageAnimalsSearch);
                return;
            }

            if (report.Equals(ReportName.ChequePaymentReport)
                || report.Equals(ReportName.ChequePaymentReport_Sheep)
                || report.Equals(ReportName.ChequePaymentReport_Pig))
            {
                this.Locator.PaymentProposalSearch.Mode = ViewType.ChequePaymentReport;
                Messenger.Default.Send(ViewType.PaymentProposalOpen);
                return;
            }

            if (report.Equals(ReportName.KillReport) 
                || report.CompareIgnoringCase(ReportName.KillReportBySupplier)
                || report.CompareIgnoringCase(ReportName.EndOfDayReport)
                || report.CompareIgnoringCase(ReportName.KillReportScotBeef)
                 || report.CompareIgnoringCase(ReportName.BeefKillReport)
           || report.CompareIgnoringCase(ReportName.SheepKillReport)
           || report.CompareIgnoringCase(ReportName.PigKillReport)
                || report.CompareIgnoringCase(ReportName.DailyKillSummary))
            {
                this.Locator.LairageSearch.CurrentReport = report;
                Messenger.Default.Send(ViewType.LairageSearch);
                return;
            }

            if (report.Equals(ReportName.DispatchDocket)
                || report.Equals((ReportName.DispatchDocketDetail))
                || report.Equals(ReportName.DispatchDocketExVAT)
                || report.Equals((ReportName.DispatchDocketGrouped))
                || report.Equals(ReportName.ColdstoreDispatchDocket)
                || report.Equals(ReportName.DispatchDocketIncVAT)
                || report.Equals(ReportName.ColdstoreDispatchDocket)
                || report.Equals(ReportName.DispatchDocketNoPrice)
                || report.Equals(ReportName.BloorsDispatchDocket)
                || report.Equals(ReportName.WoolleysDispatchDocket)
                || report.Equals(ReportName.WoolleysCarcassExport)
                 || report.Equals(ReportName.DunleavysCarcassExport)
                  || report.Equals(ReportName.BallonCarcassExport)
                    || report.Equals(ReportName.CarcassExport)
                || report.Equals(ReportName.WoolleysDispatchDocketDetail)
                || report.Equals(ReportName.DunleavysDispatchDocket)
                 || report.Equals(ReportName.DunleavysCMR)
                || report.Equals(ReportName.CMRDispatchPallets)
                 || report.Equals(ReportName.BallonCMR)
                  || report.Equals(ReportName.CMR)
                || report.Equals(ReportName.DunleavysDispatchDocketDetail)
                || report.Equals(ReportName.CarcassDispatchReport)
                || report.Equals(ReportName.CarcassDispatchExport)
                || report.Equals(ReportName.BloorsDispatchSummary)
                )
            {
                //orderStatuses = new List<int?>
                //{
                //   NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                //};

                reportItems = new List<Sale>();
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.ARDispatch;
            }
            else if (report.Equals("Pallets Card"))
            {
                reportItems = new List<Sale>();
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.PalletsCard;
            }
            else if (report.Equals("Pallets Card Multi Batch"))
            {
                reportItems = new List<Sale>();
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.PalletsCardMultiBatch;
            }
            else if (report.Equals(ReportName.PalletReport))
            {
                reportItems = new List<Sale>();
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.Pallets;
            }
            else if (report.Equals(ReportName.SlicingProductionListByCustomer)
            || report.Equals(ReportName.SlicingProductionListByProduct)
            || report.Equals(ReportName.JointsProductionListByCustomer))
            {
                reportItems = this.DataManager.GetAllOrdersForReport(orderStatuses);
                this.Locator.SalesSearchData.AssociatedView = ViewType.MultiSelectReportDisplay;
                this.Locator.SalesSearchData.ReportMode = true;
                Messenger.Default.Send(reportItems, Token.SearchForSaleMultiSelect);
                return;
            }
            else if (report.Equals(ReportName.BloorsSaleOrder))
            {
                //orderStatuses = new List<int?>
                //{
                //   NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                //};

                reportItems = this.DataManager.GetAllOrdersForReport(orderStatuses);
                //this.Locator.ReportSalesSearchData.AssociatedView = ViewType.Invoice;
            }
            else if (report.Equals(ReportName.InvoiceDocket) 
                || report.Equals(ReportName.WoolleysInvoice)
                 || report.Equals(ReportName.BallonInvoice)
                   || report.Equals(ReportName.Invoice)
                 || report.Equals(ReportName.BallonInvoiceDetail)
                || report.Equals(ReportName.BallonInvoiceDetailNoPrice)
                     || report.Equals(ReportName.BallonInvoiceDetailDates)
                 || report.Equals(ReportName.InvoiceDetail)
                 || report.Equals(ReportName.BallonInvoiceFiltered) 
                || report.Equals(ReportName.DunleavysInvoice))
            {
                if (this.Locator.ReportSalesSearchData.FromDate < DateTime.Today.AddYears(-100))
                {
                    this.Locator.ReportSalesSearchData.FromDate = DateTime.Today;
                }

                //reportItems = this.DataManager.GetAllInvoicesByDate(orderStatuses, this.Locator.ReportSalesSearchData.FromDate, DateTime.Today);
                reportItems = new List<Sale>();
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.Invoice;
            }
            else if (report.Equals(ReportName.IntakeDocket)
                     || report.Equals(ReportName.IntakeDocketTotal)
                  || report.Equals(ReportName.IntakeDetails)
                     || report.Equals(ReportName.IntakeReport)
                     || report.Equals(ReportName.IntakeReportKillNo)
                     || report.Equals(ReportName.ColdstoreGoodsIn)
                     || report.Equals(ReportName.IntakeReportProduct))
            {
                reportItems = new List<Sale>();
                ViewModelLocator.ReportSalesSearchDataStatic.AssociatedView = ViewType.APReceipt;
            }
            else if (report.Equals(ReportName.BatchYield)
                     || report.Equals(ReportName.BatchYieldPriced)
                     || report.Equals(ReportName.BatchYieldDetail)
                 || report.Equals(ReportName.BatchYieldDetailPriced)
                     || report.Equals(ReportName.BatchCosting)
                      || report.Equals(ReportName.BatchYieldDetail)
                      || report.Equals(ReportName.WoolleysBatchDetailsReport)
                       || report.Equals(ReportName.WoolleysBatchDetailsTraceReport)
                      || report.Equals(ReportName.WoolleysBatchYield)
                      || report.Equals(ReportName.DunleavysBatchDetailsReport)
                 || report.Equals(ReportName.BallonBatchDetails)
                || report.Equals(ReportName.BatchDetails)
                     || report.Equals(ReportName.BatchDetailReport)
                     || report.Equals(ReportName.BatchDetailTraceReport)
                     || report.Equals(ReportName.BatchInput)
                 || report.Equals(ReportName.BatchOutput)
                || report.Equals(ReportName.BallonBatchInput)
                || report.Equals(ReportName.BallonbatchOutput)
                       || report.Equals(ReportName.BallonBatchDetails)
                || report.Equals(ReportName.BallonBatchInput)
                || report.Equals(ReportName.BallonbatchOutput)
                       || report.Equals(ReportName.DunleavysBatchDetailsTraceReport)
                      || report.Equals(ReportName.DunleavysBatchYield)
                    || report.Equals(ReportName.BallonBatchYield)
                      || report.Equals(ReportName.BloorsBatchDetail))
            {
                reportItems = new List<Sale>();
                displayProductionSearchScreen = true;
                this.Locator.ReportSalesSearchData.AssociatedView = ViewType.BatchEdit;
            }
            else if (report.Equals(ReportName.LabelData) 
                || report.Equals(ReportName.StockTraceReport)
                || report.Equals(ReportName.BloorsLabelTrace))
            {
                reportItems =
                    new List<Sale>();
                displayTransactionSearchScreen = true;
            }
            else if (report.Equals(ReportName.ProductReport))
            {
                Messenger.Default.Send(ViewType.ReportProductData);
                return;
            }
            else if (report.Equals(ReportName.SalesAnalysisReport))  
            {
                Messenger.Default.Send(ViewType.ReportTransactionData);
                return;
            }
            else if (report.Equals(ReportName.DispatchSummaryReport))  
            {
                Messenger.Default.Send(ViewType.ReportTransactionData);
                return;
            }
            else
            {
                // non search grid reports
                if (report.ContainsIgnoringCase("Stock"))
                {
                    this.Log.LogDebug(this.GetType(),"Checking stock..");
                    this.DataManager.CheckStock();
                }

                this.ReportManager.PreviewReport(new ReportData { Path = path, Name = report });
                return;
            }

            var token = displayProductionSearchScreen ? Token.SearchForProductionSale : displayTransactionSearchScreen ? Token.SearchForTransactionSale : Token.SearchForReportSale;
            Messenger.Default.Send(reportItems, token);
        }

        /// <summary>
        /// Displays the report using the incoming report selection.
        /// </summary>
        /// <param name="selectedReportData">The selected data to run the report on.</param>
        /// <param name="isTouchscreen">Is it a touchscreen report.</param>
        private void ShowReport(Sale selectedReportData, bool isTouchscreen, bool print = false)
        {
            if (this.reportToDisplay == null)
            {
                return;
            }

            var reportId = selectedReportData.SaleID.ToString();

            if (!string.IsNullOrEmpty(selectedReportData.SaleIDs))
            {
                reportId = selectedReportData.SaleIDs;
            }

            if (this.reportToDisplay.Equals(ReportName.ChequePaymentReport) || this.reportToDisplay.Equals(ReportName.ChequePaymentReport_Sheep)
                || this.reportToDisplay.Equals(ReportName.ChequePaymentReport_Pig))
            {
                var reportParam = new List<ReportParameter>
                    {
                        new ReportParameter { Name = "KillPaymentID", Values = { reportId.ToString() } }
                    };

                var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };

                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {

                    this.ReportManager.PrintReport(reportData);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.LabelKillInformation))
            {
                var reportParam = new List<ReportParameter>
                    {
                        new ReportParameter { Name = "Serial", Values = { reportId.ToString() } }
                    };

                var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };

                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {

                    this.ReportManager.PrintReport(reportData);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.KillReportScotBeef)
                  || this.reportToDisplay.CompareIgnoringCase(ReportName.BeefKillReport)
                   || this.reportToDisplay.CompareIgnoringCase(ReportName.PigKillReport)
                    || this.reportToDisplay.CompareIgnoringCase(ReportName.SheepKillReport)
                    || this.reportToDisplay.CompareIgnoringCase(ReportName.KillReportBySupplier))
            {
                var reportParam = new List<ReportParameter>
                    {
                        new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } }
                    };

                var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };

                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {

                    this.ReportManager.PrintReport(reportData);
                }

                this.Locator.ReportViewer.AttachmentType = "CSV";

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.EndOfDayReport)
                || this.reportToDisplay.CompareIgnoringCase(ReportName.RPAExportReport)
                    || this.reportToDisplay.CompareIgnoringCase(ReportName.DailyKillSummary))
            {
                var start = selectedReportData.DeliveryDate;
                var end = selectedReportData.CreationDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "Start", Values = { start.ToString() } },
                    new ReportParameter { Name = "End", Values = { end.ToString() } }
                };

                var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };

                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {

                    this.ReportManager.PrintReport(reportData);
                }

                this.Locator.ReportViewer.AttachmentType = "CSV";

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.JointsProductionListByCustomer)
                    || this.reportToDisplay.Equals(ReportName.SlicingProductionListByCustomer)
                    || this.reportToDisplay.Equals(ReportName.SlicingProductionListByProduct))
            {
                var reportParam = new List<ReportParameter>
                    {
                        new ReportParameter { Name = "OrderID", Values = { reportId.ToString() } }
                    };

                var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };

                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {

                    this.ReportManager.PrintReport(reportData);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.SalesAnalysisReport))
            {
                var customerId = selectedReportData.SaleID;
                var start = selectedReportData.DeliveryDate;
                var end = selectedReportData.CreationDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "Start", Values = { start.ToString() } },
                    new ReportParameter { Name = "End", Values = { end.ToString() } },
                    new ReportParameter { Name = "CustomerId", Values = { customerId.ToString() } }
                };

                var reportData = new ReportData { Name = ReportName.SalesAnalysisReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchSummaryReport))
            {
                var customerId = selectedReportData.SaleID;
                var start = selectedReportData.DeliveryDate;
                var end = selectedReportData.CreationDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "Start", Values = { start.ToString() } },
                    new ReportParameter { Name = "End", Values = { end.ToString() } },
                    new ReportParameter { Name = "CustomerId", Values = { customerId.ToString() } }
                };

                var reportData = new ReportData { Name = ReportName.DispatchSummaryReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.ProductReport))
            {
                var customerId = selectedReportData.SaleID;
                var start = selectedReportData.DeliveryDate;
                var end = selectedReportData.CreationDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "CustomerID", Values = { customerId.ToString() } },
                    new ReportParameter { Name = "Start", Values = { start.ToString() } },
                    new ReportParameter { Name = "End", Values = { end.ToString() } }
                };

                var reportData = new ReportData { Name = ReportName.ProductReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.InvoiceDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.InvoiceDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysInvoice))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysInvoice, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysInvoice))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysInvoice, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonInvoice))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonInvoice, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.Invoice))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.Invoice, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonInvoiceDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonInvoiceDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonInvoiceDetailNoPrice))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonInvoiceDetailNoPrice, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonInvoiceDetailDates))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonInvoiceDetailDates, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.InvoiceDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.InvoiceDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.InvoiceDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.InvoiceDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonInvoiceFiltered))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonInvoiceFiltered, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.PalletReport))
            {
                var palletId = selectedReportData.SaleID;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "PalletID", Values = { palletId.ToString() } }
                };

                var reportData = new ReportData { Name = ReportName.PalletReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.CMRDispatchPallets))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.CMRDispatchPallets, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchDocketGrouped))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocketGrouped, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.PalletReport))
            {
                var palletId = selectedReportData.SaleID;
                var start = selectedReportData.DeliveryDate;
                var end = selectedReportData.CreationDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "PalletID", Values = { palletId.ToString() } },
                    new ReportParameter { Name = "Start", Values = { start.ToString() } },
                    new ReportParameter { Name = "End", Values = { end.ToString() } }
                };

                var reportData = new ReportData { Name = ReportName.PalletReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.CarcassTrace))
            {
                var carcass = selectedReportData.Breed;
                var localDate = selectedReportData.CreationDate;
                var groupId = selectedReportData.GroupId;
                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "Carcass", Values = { carcass } },
                    new ReportParameter { Name = "Date", Values = { localDate.ToString() } },
                    new ReportParameter { Name = "GroupID", Values = { groupId.ToString() } },
                };

                var reportData = new ReportData { Name = ReportName.CarcassTrace, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysDispatchDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysDispatchDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysDispatchDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysDispatchDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysCMR))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysCMR, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.CMR))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.CMR, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonCMR))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonCMR, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysCarcassExport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysCarcassExport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysCarcassExport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysCarcassExport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonCarcassExport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonCarcassExport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.CarcassExport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.CarcassExport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BloorsDispatchDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BloorsDispatchDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BloorsSaleOrder))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "SalesID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BloorsSaleOrder, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BloorsDispatchSummary))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Dispatchid", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BloorsDispatchSummary, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchDocketNoPrice))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocketNoPrice, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchDocketDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocketDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysDispatchDocketDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysDispatchDocketDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysDispatchDocketDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysDispatchDocketDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.CarcassDispatchExport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.CarcassDispatchExport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.CarcassDispatchReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.CarcassDispatchReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchDocketExVAT))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "ReportParameterDispatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocketExVAT, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.ColdstoreDispatchDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.ColdstoreDispatchDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DispatchDocketIncVAT))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "ReportParameterDispatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocketIncVAT, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.IntakeDetails))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.IntakeDetails, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.IntakeDocket))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.IntakeDocket, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.ColdstoreGoodsIn))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.ColdstoreGoodsIn, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.IntakeDocketTotal))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.IntakeDocketTotal, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.IntakeReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.IntakeReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.IntakeReportKillNo))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.IntakeReportKillNo, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.IntakeReportProduct))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.IntakeReportProduct, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchCosting))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { selectedReportData.PROrderID.ToString() } } };
                
                var reportData = new ReportData { Name = ReportName.BatchCosting, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchYieldPriced))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Batches", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchYieldPriced, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchYield))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Batches", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchYield, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysBatchYield))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Batches", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysBatchYield, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysBatchYield))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Batches", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysBatchYield, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonBatchYield))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "Batches", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonBatchYield, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchYieldDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchYieldDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchYieldDetailPriced))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchYieldDetailPriced, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BloorsBatchDetail))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchID", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BloorsBatchDetail, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysBatchDetailsReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysBatchDetailsReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysBatchDetailsReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysBatchDetailsReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonBatchDetails))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonBatchDetails, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchDetailReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchDetailReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchDetailTraceReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchDetailTraceReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchDetails))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchDetails, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchInput))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchInput, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonBatchInput))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonBatchInput, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BallonbatchOutput))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BallonbatchOutput, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BatchOutput))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.BatchOutput, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.WoolleysBatchDetailsTraceReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.WoolleysBatchDetailsTraceReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.DunleavysBatchDetailsTraceReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "BatchId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DunleavysBatchDetailsTraceReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.StockTraceReport))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "LabelId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.StockTraceReport, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }

            if (this.reportToDisplay.Equals(ReportName.BloorsLabelTrace))
            {
                var fromId = selectedReportData.DocumentNumberingID;
                var toId = selectedReportData.Number;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "FromId", Values = { fromId.ToString() } },
                    new ReportParameter { Name = "ToId", Values = { toId.ToString() } }
                };

                var reportData = new ReportData { Name = ReportName.BloorsLabelTrace, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

            }

            if (this.reportToDisplay.Equals(ReportName.LabelData))
            {
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "LabelId", Values = { reportId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.LabelData, ReportParameters = reportParam };
                if (!print)
                {
                    this.ReportManager.PreviewReport(reportData, isTouchscreen);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData, isTouchscreen);
                }

                return;
            }
        }

        /// <summary>
        /// Displays the report using the incoming report selection.
        /// </summary>
        /// <param name="selectedReportData">The selected data to run the report on.</param>
        private void ShowReport(IList<Sale> selectedReportDatas, bool multiSelect = false)
        {
            if (multiSelect)
            {
                var OrderID = string.Join(",", selectedReportDatas.Select(x => x.SaleID));

                if (this.reportToDisplay.Equals(ReportName.JointsProductionListByCustomer)
                    || this.reportToDisplay.Equals(ReportName.SlicingProductionListByCustomer)
                    || this.reportToDisplay.Equals(ReportName.SlicingProductionListByProduct))
                {
                    var reportParam = new List<ReportParameter>
                    {
                        new ReportParameter { Name = "OrderID", Values = { OrderID } }
                    };

                    var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };
                    this.ReportManager.PreviewReport(reportData);
                    return;
                }

                if (this.reportToDisplay.Equals(ReportName.KillReport))
                {
                    var reportParam = new List<ReportParameter>
                    {
                        new ReportParameter { Name = "OrderID", Values = { OrderID } }
                    };

                    var reportData = new ReportData { Name = this.reportToDisplay, ReportParameters = reportParam };
                    this.ReportManager.PreviewReport(reportData);
                    return;
                }

                return;
            }

            var reportDatas = new List<ReportData>();
            foreach (var selectedReportData in selectedReportDatas)
            {
                if (this.reportToDisplay == null)
                {
                    return;
                }

                var reportId = selectedReportData.SaleID;

                if (this.reportToDisplay.Equals(ReportName.DispatchDocketExVAT))
                {
                    var reportParam = new List<ReportParameter> { new ReportParameter { Name = "ReportParameterDispatchId", Values = { reportId.ToString() } } };
                    var reportData = new ReportData { Name = ReportName.DispatchDocketExVAT, ReportParameters = reportParam };
                    reportDatas.Add(reportData);
                }

                if (this.reportToDisplay.Equals(ReportName.ColdstoreDispatchDocket))
                {
                    var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { reportId.ToString() } } };
                    var reportData = new ReportData { Name = ReportName.ColdstoreDispatchDocket, ReportParameters = reportParam };
                    reportDatas.Add(reportData);
                }

                if (this.reportToDisplay.Equals(ReportName.KillReportScotBeef))
                {
                    var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } } };
                    var reportData = new ReportData { Name = ReportName.KillReportScotBeef, ReportParameters = reportParam };
                    reportDatas.Add(reportData);
                }

                if (this.reportToDisplay.Equals(ReportName.KillReportBySupplier))
                {
                    var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } } };
                    var reportData = new ReportData { Name = ReportName.KillReportBySupplier, ReportParameters = reportParam };
                    reportDatas.Add(reportData);
                }

                if (this.reportToDisplay.Equals(ReportName.KillReport))
                {
                    var reportParam = new List<ReportParameter> { new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } } };
                    var reportData = new ReportData { Name = ReportName.KillReport, ReportParameters = reportParam };
                    reportDatas.Add(reportData);
                }
            }

            this.ReportManager.PreviewReports(reportDatas);
        }

        #endregion

        #endregion

        #endregion
    }
}
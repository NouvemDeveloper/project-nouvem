﻿// -----------------------------------------------------------------------
// <copyright file="GraderViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.KillLine.Grader
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class GraderViewModel : KillLineViewModelBase
    {
        #region field

        /// <summary>
        /// The incoming manually set weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The current tare.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The sheep tag.
        /// </summary>
        private string sheepTag;

        /// <summary>
        /// The name of the curren grade system;
        /// </summary>
        private string gradeSystemName;
      
        /// <summary>
        /// The carcass attribute to edit id.
        /// </summary>
        private string pigUIText;

        /// <summary>
        /// The carcass attribute to edit id.
        /// </summary>
        private int attributeEditId;

        /// <summary>
        /// The beef/sheep panel content.
        /// </summary>
        private string beefSheepContent;

        /// <summary>
        /// Holds the last x weights.
        /// </summary>
        private IList<decimal> weights = new List<decimal>();

        /// <summary>
        /// The current tare.
        /// </summary>
        private bool weighingInProgress;

        /// <summary>
        /// The edit mode flag.
        /// </summary>
        private bool editMode;

        /// <summary>
        /// The refresh timer.
        /// </summary>
        private DispatcherTimer refreshTimer = new DispatcherTimer();

        /// <summary>
        /// The grade view to display.
        ///  </summary>
        private ViewModelBase gradeDisplayViewModel;

        /// <summary>
        /// The current carcass.
        /// </summary>
        private StockDetail currentCarcass;

        /// <summary>
        /// The current carcass.
        /// </summary>
        private int? SupplierId;

        /// <summary>
        /// The previous mode.
        /// </summary>
        private GraderMode previousGraderMode = GraderMode.Beef;

        /// <summary>
        /// The confirmation score coming from the auto grader
        /// </summary>
        private string conformationScore;

        /// <summary>
        /// The fat score coming from the auto grader
        /// </summary>
        private string fatScore;
        
        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldEIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldUIsSelected;
      
        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldPIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldUPlusIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldMinusUIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldRIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldOIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldOPlusIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldMinusOIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldOMinusIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldPPlusIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldMinusPIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool conformanceOldPMinusIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld1IsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld2IsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld3IsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld4HIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld4LIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld5HIsSelected;

         /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld5LIsSelected;

        /// <summary>
        /// Old grade
        /// </summary>
        private bool fatScoreOld5IsSelected;

        /// <summary>
        /// The first character in the manual conformance score
        /// </summary>
        private string manualConfFirstChar;

        /// <summary>
        /// The second character in the manual conformance score
        /// </summary>
        private string manualConfSecondChar;

        /// <summary>
        /// The first character in the manual fat score
        /// </summary>
        private string manualFatFirstChar;

        /// <summary>
        /// The second character in the manual fat score
        /// </summary>
        private string manualFatSecondChar;

        /// <summary>
        /// The conformance E field.
        /// </summary>
        private bool conformanceEIsSelected;

        /// <summary>
        /// The conformance U field.
        /// </summary>
        private bool conformanceUIsSelected;

        /// <summary>
        /// The conformance R field.
        /// </summary>
        private bool conformanceRIsSelected;

        /// <summary>
        /// The conformance O field.
        /// </summary>
        private bool conformanceOIsSelected;

        /// <summary>
        /// The conformance P field.
        /// </summary>
        private bool conformancePIsSelected;

        /// <summary>
        /// The conformance Minus field.
        /// </summary>
        private bool conformanceMinusIsSelected;

        /// <summary>
        /// The conformance Equals field.
        /// </summary>
        private bool conformanceEqualsIsSelected;

        /// <summary>
        /// The conformance Plus field.
        /// </summary>
        private bool conformancePlusIsSelected;

        /// <summary>
        /// The fat score 1 field.
        /// </summary>
        private bool fatScore1IsSelected;

        /// <summary>
        /// The fat score 2 field.
        /// </summary>
        private bool fatScore2IsSelected;

        /// <summary>
        /// The fat score 3 field.
        /// </summary>
        private bool fatScore3IsSelected;

        /// <summary>
        /// The fat score 4 field.
        /// </summary>
        private bool fatScore4IsSelected;

        /// <summary>
        /// The fat score 5 field.
        /// </summary>
        private bool fatScore5IsSelected;

        /// <summary>
        /// The fat score Minus field.
        /// </summary>
        private bool fatScoreMinusIsSelected;

        /// <summary>
        /// The fat score Equals field.
        /// </summary>
        private bool fatScoreEqualsIsSelected;

        /// <summary>
        /// The fat score Plus field.
        /// </summary>
        private bool fatScorePlusIsSelected;

        /// <summary>
        /// Line re-ordering enabled flag.
        /// </summary>
        private bool enableLineReordering;

        /// <summary>
        /// The user entered eartag.
        /// </summary>
        private string enteredEartag;

        /// <summary>
        /// The user entered eartag.
        /// </summary>
        private string enteredHerdNumber;

        /// <summary>
        /// The next carcass number.
        /// </summary>
        private int nextCarcassNumber;

        /// <summary>
        /// The next kill number.
        /// </summary>
        private int nextKillNumber;

        /// <summary>
        /// Age band 4 selection.
        /// </summary>
        private bool isInvalidCategory;

        /// <summary>
        /// The manual grading mode.
        /// </summary>
        private bool manualGradingModeOn;

        /// <summary>
        /// The category b.
        /// </summary>
        private int? categoryB;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GraderViewModel"/> class.
        /// </summary>
        public GraderViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.DisplayGenericScreenSelection, s =>
            {
                if (ViewModelLocator.IsTransactionManagerLoaded())
                {
                    Messenger.Default.Send(Token.Message, Token.CreatePrinterSelection);
                    return;
                }

                Messenger.Default.Send(Token.Message, Token.DisplayGenericSelection);
            });

            // Register for the incoming supplier.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.SupplierSelected, this.HandleSelectedSupplier);
            
            // Register for the incoming supplier order.
            Messenger.Default.Register<Tuple<Sale, ViewBusinessPartner>>(this, Token.SaleSelected, this.HandleSelectedOrder);

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<Tuple<int,int>>(this, Token.GraderEditMode, i =>
            {
                if (i.Item1 > 0)
                {
                    this.EditMode = true;
                    this.NextCarcassNumber = i.Item1;
                    this.attributeEditId = i.Item2;
                    this.FindCarcassForEdit();

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(string.Format(Message.EditModeOn, i.Item1), touchScreen:true);
                    }));
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.Log.LogInfo(this.GetType(), string.Format("Grader-Scan Manual:{0}", s));
                    this.HandleScannerData(s);
                }
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                this.Log.LogInfo(this.GetType(), string.Format("Grader-Scan:{0}",s));
                this.HandleScannerData(s);
            });

            // Reprints a label on a weight change.
            Messenger.Default.Register<Tuple<int, int?, int, KillType, string,string>>(this, Token.ReprintLabel, o =>
            {
                LabelType labelType;
                if (ApplicationSettings.UseRiskToDeterminePrinterAtGrader)
                {
                    var redLabel = o.Item6.CompareIgnoringCase("Control OTM");
                    labelType = redLabel
                        ? LabelType.Item
                        : LabelType.Box;
                }
                else
                {
                    var age = o.Item3;
                    labelType = age <= ApplicationSettings.GraderLabelUOMAge
                        ? LabelType.Item
                        : LabelType.Box;
                }

                var supplierId = o.Item2;
                if (o.Item5.CompareIgnoringCase("Welsh"))
                {
                    supplierId = ApplicationSettings.LairageRadioButtonOptionsPartnerId1;
                }
                else if (o.Item5.CompareIgnoringCase("Organic"))
                {
                    supplierId = ApplicationSettings.LairageRadioButtonOptionsPartnerId3;
                }
                else if (o.Item5.CompareIgnoringCase("SelectFarm"))
                {
                    supplierId = ApplicationSettings.LairageRadioButtonOptionsPartnerId2;
                }
                else if (o.Item5.CompareIgnoringCase("Continental"))
                {
                    supplierId = ApplicationSettings.LairageRadioButtonOptionsPartnerId4;
                }

                this.Print(o.Item1, partnerId: supplierId, weightChanged: true, labelType: labelType, killType: o.Item4);
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals(Constant.Sheep))
                {
                    this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == c.ID);
                    this.CreateSheepIntake();
                    return;
                }

                if (c.Identifier.Equals(Constant.TraceabilityAttribute))
                {
                    this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == c.ID);
                    return;
                }

                if (c.Identifier.Equals(Constant.InternalGrade))
                {
                    this.SelectedInternalGrade = this.InternalGrades.FirstOrDefault(x => x.InternalGradeID == c.ID);
                    return;
                }

                if (c.Identifier.Equals(Constant.RearedIn))
                {
                    this.RearedIn = this.Countries.FirstOrDefault(x => x.CountryID == c.ID);
                    return;
                }

                if (c.Identifier.Equals(Constant.Origin))
                {
                    this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.CountryID == c.ID);
                    return;
                }

                if (c.Identifier.Equals(Constant.FA))
                {
                    this.FarmAssured = c.Data;
                    return;
                }

                if (c.Identifier.Equals(Constant.Private))
                {
                    this.PrivateKill = c.Data;
                    return;
                }

                if (c.Identifier.Equals(Constant.DressSpec))
                {
                    this.SelectedDressSpec = this.DressSpecs.FirstOrDefault(x => x.ID == c.ID);
                    return;
                }

                if (c.Identifier.Equals(Constant.KillingType))
                {
                    this.SelectedKillingType = this.KillingTypes.FirstOrDefault(x => x.ID == c.ID);
                    return;
                }
            });

            // Register for the incoming keyboard selection.
            Messenger.Default.Register<string>(this, Token.KeyboardValueEntered, s =>
            {
                this.Eartag = s;
                this.sheepTag = s;
            });

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                this.manualWeight = false;
                this.RecordWeight();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;
                this.RecordWeight();
            });

            // Register for the indicator tare weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.CarcassChange, s =>
            {
                var carcassNo = s.ToInt();
                if (carcassNo > 0)
                {
                    this.NextCarcassNumber = carcassNo;
                    this.FindNextCarcass();
                }
            });

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.KillNoChange, s =>
            {
                var killNo = s.ToInt();
                if (killNo > 0)
                {
                    this.NextKillNumber = killNo;
                }
            });

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowGraderAttributes = b);

            #endregion

            #region command handler

            this.NextPigLotCommand = new RelayCommand(this.NextPigLotCommandExecute);

            // Handler to call up the order screen.
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);

            this.SwapViewCommand = new RelayCommand(() =>
            {
                this.HandleToggle();
            });

            this.SwitchGradeCommand = new RelayCommand(() =>
            {
                if (ApplicationSettings.GradeSystem == GradeSystem.EUROP)
                {
                    ApplicationSettings.GradeSystem = GradeSystem.FivePoint;
                }
                else if (ApplicationSettings.GradeSystem == GradeSystem.FivePoint)
                {
                    ApplicationSettings.GradeSystem = GradeSystem.FifteenPoint;
                }
                else
                {
                    ApplicationSettings.GradeSystem = GradeSystem.EUROP;
                }

                this.SetGradeDisplay();

            }, () => ApplicationSettings.AllowGradeViewChange);

            this.ChangeCarcassNumberCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.CarcassChange));

            this.ChangeKillNumberCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.KillNoChange));
           
            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(this.RemoveItemCommandExecute);

            // Find the next carcass number.
            this.FindCarcassCommand = new RelayCommand(this.FindCarcassCommandExecute);

            #endregion

            this.GetProcesses();
            this.GetCategories();
            this.GetCountries();
            this.GetBreeds();
            this.GetInternalGrades();
            this.GetSexes();
            this.GetFAValues();
            this.GetDestinations();
            this.GetDressSpecs();
            this.GetKillingTypes();
            this.CloseHiddenWindows();
            this.SetGradeDisplay();
            this.ManualGradingModeOn = true;
            this.CarcassSupplier = Strings.NoSupplier;
            this.StockLabelsToPrint = 1;

            if (ApplicationSettings.GraderMode == GraderMode.Beef)
            {
                this.SetGradeDisplay();
                this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPanel;
                this.BeefSheepContent = Strings.Beef;
            }
            else if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                this.GradeDisplayViewModel = this.Locator.Grade;
                this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPanel;
                this.BeefSheepContent = Strings.Sheep;
            }
            else
            {
                this.GradeDisplayViewModel = this.Locator.Pig;
                this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPigPanel;
                this.GetNextQueuedHerd();
            }

            this.SetLotCount(0, 0);
            
            this.GetLocalDocNumberings();
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowGraderAttributes;
            this.Locator.Touchscreen.CornerViewModel = ViewModelLocator.IndicatorStatic;
            this.LairageGraderCombo = ApplicationSettings.LairageGraderCombo;
            this.AccumulateWeightToTareAtGrader = ApplicationSettings.AccumulateWeightToTareAtGrader;
            this.GetNextKillNumber();
            this.FindNextCarcass();
            this.GetCarcassDetails();
            this.SetTare();
            this.SetUpRefreshTimer();
            this.SetUpTimer();
            this.SetUpTimers();
            this.CloseOtherKillModules(ViewType.Grader);
            this.SetUiLabel();
            this.PigUIText = this.LairageGraderCombo ? Strings.LotCount : Strings.Remaining;
            var bCat = this.Categories.FirstOrDefault(x => x.CAT == "B");
            if (bCat != null)
            {
                this.categoryB = bCat.CategoryID;
            }
        }

        #endregion

        #region public interface

        #region property

        public bool CanSelectSupplier
        {
            get
            {
                return this.LairageGraderCombo && (ApplicationSettings.GraderMode == GraderMode.Sheep ||
                       ApplicationSettings.GraderMode == GraderMode.Pig);
            }
        }
       
        /// <summary>
        /// Gets or sets the edit mode.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.editMode;
            }

            set
            {
                this.editMode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the panel beef\sheep content.
        /// </summary>
        public string PigUIText
        {
            get
            {
                return this.pigUIText;
            }

            set
            {
                this.pigUIText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the grade system name.
        /// </summary>
        public string GradeSystemName
        {
            get
            {
                return this.gradeSystemName;
            }

            set
            {
                this.gradeSystemName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the panel beef\sheep content.
        /// </summary>
        public string BeefSheepContent
        {
            get
            {
                return this.beefSheepContent;
            }

            set
            {
                this.beefSheepContent = value;
                this.RaisePropertyChanged();
            }
        }
     
        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ViewModelBase GradeDisplayViewModel
        {
            get
            {
                return this.gradeDisplayViewModel;
            }

            set
            {
                this.gradeDisplayViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the grade.
        /// </summary>
        public string Grade
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ConformationScore) && !string.IsNullOrEmpty(this.FatScore))
                {
                    return string.Format("{0}{1}", this.ConformationScore, this.FatScore);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the grade.
        /// </summary>
        public bool NoGradeEntered
        {
            get
            {
                return string.IsNullOrEmpty(this.ConformationScore) && string.IsNullOrEmpty(this.FatScore);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in manual grading mode.
        /// </summary>
        public bool ManualGradingModeOn 
        {
            get
            {
                return this.manualGradingModeOn;
            }

            set
            {
                this.manualGradingModeOn = value;
                this.RaisePropertyChanged();
            }
        }

        #region conf/fat scores

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldEIsSelected
        {
            get
            {
                return this.conformanceOldEIsSelected;
            }

            set
            {
                this.conformanceOldEIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                   

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "E";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldUIsSelected
        {
            get
            {
                return this.conformanceOldUIsSelected;
            }

            set
            {
                this.conformanceOldUIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformationScore = "U";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldOIsSelected
        {
            get
            {
                return this.conformanceOldOIsSelected;
            }

            set
            {
                this.conformanceOldOIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "O";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldPIsSelected
        {
            get
            {
                return this.conformanceOldPIsSelected;
            }

            set
            {
                this.conformanceOldPIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "P";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldUPlusIsSelected
        {
            get
            {
                return this.conformanceOldUPlusIsSelected;
            }

            set
            {
                this.conformanceOldUPlusIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                   

                    // ensure the other conformances are not selected
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "U+";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

       /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldMinusUIsSelected
        {
            get
            {
                return this.conformanceOldMinusUIsSelected;
            }

            set
            {
                this.conformanceOldMinusUIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "-U";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldRIsSelected
        {
            get
            {
                return this.conformanceOldRIsSelected;
            }

            set
            {
                this.conformanceOldRIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "R";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldOPlusIsSelected
        {
            get
            {
                return this.conformanceOldOPlusIsSelected;
            }

            set
            {
                this.conformanceOldOPlusIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "O+";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldOMinusIsSelected
        {
            get
            {
                return this.conformanceOldOMinusIsSelected;
            }

            set
            {
                this.conformanceOldOMinusIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "O-";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldPMinusIsSelected
        {
            get
            {
                return this.conformanceOldPMinusIsSelected;
            }

            set
            {
                this.conformanceOldPMinusIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                   

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "P-";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

       

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldMinusOIsSelected
        {
            get
            {
                return this.conformanceOldMinusOIsSelected;
            }

            set
            {
                this.conformanceOldMinusOIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "-O";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        } 

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldPPlusIsSelected
        {
            get
            {
                return this.conformanceOldPPlusIsSelected;
            }

            set
            {
                this.conformanceOldPPlusIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldMinusPIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "P+";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool ConformanceOldMinusPIsSelected
        {
            get
            {
                return this.conformanceOldMinusPIsSelected;
            }

            set
            {
                this.conformanceOldMinusPIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.ConformanceOldUPlusIsSelected = false;
                    this.ConformanceOldMinusUIsSelected = false;
                    this.ConformanceOldRIsSelected = false;
                    this.ConformanceOldOPlusIsSelected = false;
                    this.ConformanceOldMinusOIsSelected = false;
                    this.ConformanceOldPPlusIsSelected = false;
                    this.ConformanceOldEIsSelected = false;
                    this.ConformanceOldPMinusIsSelected = false;
                    this.ConformanceOldOMinusIsSelected = false;
                    this.ConformanceOldOIsSelected = false;
                    this.ConformanceOldPIsSelected = false;
                    this.ConformanceOldUIsSelected = false;
                    this.ConformationScore = "-P";
                }
                else
                {
                    this.ConformationScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld1IsSelected
        {
            get
            {
                return this.fatScoreOld1IsSelected;
            }

            set
            {
                this.fatScoreOld1IsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "1";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld2IsSelected
        {
            get
            {
                return this.fatScoreOld2IsSelected;
            }

            set
            {
                this.fatScoreOld2IsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "2";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld3IsSelected
        {
            get
            {
                return this.fatScoreOld3IsSelected;
            }

            set
            {
                this.fatScoreOld3IsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "3";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld4HIsSelected
        {
            get
            {
                return this.fatScoreOld4HIsSelected;
            }

            set
            {
                this.fatScoreOld4HIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                   

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "4H";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld4LIsSelected
        {
            get
            {
                return this.fatScoreOld4LIsSelected;
            }

            set
            {
                this.fatScoreOld4LIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "4L";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld5HIsSelected
        {
            get
            {
                return this.fatScoreOld5HIsSelected;
            }

            set
            {
                this.fatScoreOld5HIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "5H";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld5LIsSelected
        {
            get
            {
                return this.fatScoreOld5LIsSelected;
            }

            set
            {
                this.fatScoreOld5LIsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5IsSelected = false;
                    this.FatScore = "5L";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Old grade
        /// </summary>
        public bool FatScoreOld5IsSelected
        {
            get
            {
                return this.fatScoreOld5IsSelected;
            }

            set
            {
                this.fatScoreOld5IsSelected = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    

                    // ensure the other conformances are not selected
                    this.FatScoreOld1IsSelected = false;
                    this.FatScoreOld2IsSelected = false;
                    this.FatScoreOld3IsSelected = false;
                    this.FatScoreOld4HIsSelected = false;
                    this.FatScoreOld4LIsSelected = false;
                    this.FatScoreOld5HIsSelected = false;
                    this.FatScoreOld5LIsSelected = false;
                    this.FatScore = "5";
                }
                else
                {
                    this.FatScore = string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets the auto grader conformation score
        /// </summary>
        public string ConformationScore
        {
            get
            {
                return this.conformationScore;
            }

            set
            {
                this.conformationScore = value;

                // Update bindings.
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance E value is selected.
        /// </summary>
        public bool ConformanceEIsSelected
        {
            get
            {
                return this.conformanceEIsSelected;
            }

            set
            {
                this.conformanceEIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("ConE", true);

                    // ensure the other conformances are not selected
                    this.ConformanceUIsSelected = false;
                    this.ConformanceRIsSelected = false;
                    this.ConformanceOIsSelected = false;
                    this.ConformancePIsSelected = false;
                }
                else
                {
                    this.GradeSelection("ConE", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance U value is selected.
        /// </summary>
        public bool ConformanceUIsSelected
        {
            get
            {
                return this.conformanceUIsSelected;
            }

            set
            {
                this.conformanceUIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("ConU", true);

                    // ensure the other related checkboxes are unticked.
                    this.ConformanceEIsSelected = false;
                    this.ConformanceRIsSelected = false;
                    this.ConformanceOIsSelected = false;
                    this.ConformancePIsSelected = false;
                }
                else
                {
                    this.GradeSelection("ConU", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance R value is selected.
        /// </summary>
        public bool ConformanceRIsSelected
        {
            get
            {
                return this.conformanceRIsSelected;
            }

            set
            {
                this.conformanceRIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("ConR", true);

                    // ensure the other related checkboxes are unticked.
                    this.ConformanceEIsSelected = false;
                    this.ConformanceUIsSelected = false;
                    this.ConformanceOIsSelected = false;
                    this.ConformancePIsSelected = false;
                }
                else
                {
                    this.GradeSelection("ConR", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance O value is selected.
        /// </summary>
        public bool ConformanceOIsSelected
        {
            get
            {
                return this.conformanceOIsSelected;
            }

            set
            {
                this.conformanceOIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("ConO", true);

                    // ensure the other related checkboxes are unticked.
                    this.ConformanceEIsSelected = false;
                    this.ConformanceUIsSelected = false;
                    this.ConformanceRIsSelected = false;
                    this.ConformancePIsSelected = false;
                }
                else
                {
                    this.GradeSelection("ConO", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance P value is selected.
        /// </summary>
        public bool ConformancePIsSelected
        {
            get
            {
                return this.conformancePIsSelected;
            }

            set
            {
                this.conformancePIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("ConP", true);

                    // ensure the other related checkboxes are unticked.
                    this.ConformanceEIsSelected = false;
                    this.ConformanceUIsSelected = false;
                    this.ConformanceRIsSelected = false;
                    this.ConformanceOIsSelected = false;
                }
                else
                {
                    this.GradeSelection("ConP", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance minus value is selected.
        /// </summary>
        public bool ConformanceMinusIsSelected
        {
            get
            {
                return this.conformanceMinusIsSelected;
            }

            set
            {
                this.conformanceMinusIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("Con-", true);

                    // ensure the other related checkboxes are unticked.                    
                    this.ConformanceEqualsIsSelected = false;
                    this.ConformancePlusIsSelected = false;
                }
                else
                {
                    this.GradeSelection("Con-", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance equals value is selected.
        /// </summary>
        public bool ConformanceEqualsIsSelected
        {
            get
            {
                return this.conformanceEqualsIsSelected;
            }

            set
            {
                this.conformanceEqualsIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("Con=", true);

                    // ensure the other related checkboxes are unticked.
                    this.ConformanceMinusIsSelected = false;
                    this.ConformancePlusIsSelected = false;
                }
                else
                {
                    this.GradeSelection("Con=", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conformance plus value is selected.
        /// </summary>
        public bool ConformancePlusIsSelected
        {
            get
            {
                return this.conformancePlusIsSelected;
            }

            set
            {
                this.conformancePlusIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value == true)
                {
                    this.GradeSelection("Con+", true);

                    // ensure the other related checkboxes are unticked.
                    this.ConformanceMinusIsSelected = false;
                    this.ConformanceEqualsIsSelected = false;
                }
                else
                {
                    this.GradeSelection("Con+", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets the auto grader fat score
        /// </summary>
        public string FatScore
        {
            get
            {
                return this.fatScore;
            }

            set
            {
                this.fatScore = value;

                // Update bindings.
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score 1 value is selected.
        /// </summary>
        public bool FatScore1IsSelected
        {
            get
            {
                return this.fatScore1IsSelected;
            }

            set
            {
                this.fatScore1IsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat1", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScore2IsSelected = false;
                    this.FatScore3IsSelected = false;
                    this.FatScore4IsSelected = false;
                    this.FatScore5IsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat1", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score 2 value is selected.
        /// </summary>
        public bool FatScore2IsSelected
        {
            get
            {
                return this.fatScore2IsSelected;
            }

            set
            {
                this.fatScore2IsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat2", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScore1IsSelected = false;
                    this.FatScore3IsSelected = false;
                    this.FatScore4IsSelected = false;
                    this.FatScore5IsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat2", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score 3 value is selected.
        /// </summary>
        public bool FatScore3IsSelected
        {
            get
            {
                return this.fatScore3IsSelected;
            }

            set
            {
                this.fatScore3IsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat3", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScore1IsSelected = false;
                    this.FatScore2IsSelected = false;
                    this.FatScore4IsSelected = false;
                    this.FatScore5IsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat3", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score 4 value is selected.
        /// </summary>
        public bool FatScore4IsSelected
        {
            get
            {
                return this.fatScore4IsSelected;
            }

            set
            {
                this.fatScore4IsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat4", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScore1IsSelected = false;
                    this.FatScore2IsSelected = false;
                    this.FatScore3IsSelected = false;
                    this.FatScore5IsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat4", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score 5 value is selected.
        /// </summary>
        public bool FatScore5IsSelected
        {
            get
            {
                return this.fatScore5IsSelected;
            }

            set
            {
                this.fatScore5IsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat5", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScore1IsSelected = false;
                    this.FatScore2IsSelected = false;
                    this.FatScore3IsSelected = false;
                    this.FatScore4IsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat5", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score minus value is selected.
        /// </summary>
        public bool FatScoreMinusIsSelected
        {
            get
            {
                return this.fatScoreMinusIsSelected;
            }

            set
            {
                this.fatScoreMinusIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat-", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScoreEqualsIsSelected = false;
                    this.FatScorePlusIsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat-", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score equals value is selected.
        /// </summary>
        public bool FatScoreEqualsIsSelected
        {
            get
            {
                return this.fatScoreEqualsIsSelected;
            }

            set
            {
                this.fatScoreEqualsIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat=", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScoreMinusIsSelected = false;
                    this.FatScorePlusIsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat=", false);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fat score plus value is selected.
        /// </summary>
        public bool FatScorePlusIsSelected
        {
            get
            {
                return this.fatScorePlusIsSelected;
            }

            set
            {
                this.fatScorePlusIsSelected = value;

                // Update bindings.
                this.RaisePropertyChanged();

                if (value)
                {
                    this.GradeSelection("Fat+", true);

                    // ensure the other related checkboxes are unticked.
                    this.FatScoreMinusIsSelected = false;
                    this.FatScoreEqualsIsSelected = false;
                }
                else
                {
                    this.GradeSelection("Fat+", false);
                }
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets the next carcass no.
        /// </summary>
        public int NextCarcassNumber
        {
            get
            {
                return this.nextCarcassNumber;
            }

            set
            {
                this.nextCarcassNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next kill number.
        /// </summary>
        public int NextKillNumber
        {
            get
            {
                return this.nextKillNumber;
            }

            set
            {
                this.nextKillNumber = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand NextPigLotCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchGradeCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to swap the grade view.
        /// </summary>
        public ICommand SwapViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to move carcasses up and down the line.
        /// </summary>
        public ICommand ChangeCarcassNumberCommand { get; private set; }

        /// <summary>
        /// Gets the command to move carcasses up and down the line.
        /// </summary>
        public ICommand ChangeKillNumberCommand { get; private set; }

        /// <summary>
        /// Gets the command to find a carcass.
        /// </summary>
        public ICommand FindCarcassCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleStockLabels(string data)
        {
            this.StockLabelsToPrint = data.ToInt();
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void DeleteTransaction(int stockId, string barcode = "")
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to delete grader label:{stockId}");
            var error = this.DataManager.DeleteGraderStock(stockId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());
            if (string.IsNullOrEmpty(error))
            {
                this.Log.LogInfo(this.GetType(), "Label deleted");
                SystemMessage.Write(MessageType.Priority, Message.StockRemoved);
            }
            else
            {
                this.Log.LogError(this.GetType(), $"Label deletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void UndeleteTransaction(int stockId)
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to undelete into prod label:{stockId}");
            var error = this.DataManager.UndeleteStock(stockId, NouvemGlobal.TransactionTypeGoodsReceiptId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());

            if (string.IsNullOrEmpty(error))
            {
                this.Log.LogInfo(this.GetType(), "Label undeleted");
                SystemMessage.Write(MessageType.Priority, Message.StockUndeleted);
                this.Log.LogInfo(this.GetType(), "Label deleted");
            }
            else
            {
                this.Log.LogError(this.GetType(), $"Label undeletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
        }

        /// <summary>
        /// Gets the next herd queued.
        /// </summary>
        protected override void GetNextQueuedHerd()
        {
            if (!ApplicationSettings.UsingKillLineQueueNumbers)
            {
                return;
            }

            ViewBusinessPartner localSupplier = null;
            var intake = this.DataManager.GetTouchscreenReceiptByNextQueueNo(Constant.Pig);
            if (intake == null)
            {
                return;
            }

            if (intake.BPCustomer != null)
            {
                var localPartner =
                    NouvemGlobal.SupplierPartners.FirstOrDefault(
                        x => x.Details.BPMasterID == intake.BPCustomer.BPMasterID);

                if (localPartner != null)
                {
                    localSupplier = localPartner.Details;
                }
            }

            this.HandleSelectedPigOrder(Tuple.Create(intake, localSupplier));
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            #region validation

            if (string.IsNullOrEmpty(data))
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);
                return;
            }

            if (ViewModelLocator.IsTransactionManagerLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                return;
            }

            #endregion

            data = data.Replace("\r", "");
            if (this.currentCarcass == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCarcassBeingProcessed);
                return;
            }

            if (data.Length != 9 || !data.IsNumericSequence())
            {
                var message = string.Format(Message.InvalidIdentigenbarcode, data);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                if (this.DataManager.MinusScanIdentigen(data))
                {
                    if (this.Identigen == data)
                    {
                        this.Identigen = string.Empty;
                    }

                    SystemMessage.Write(MessageType.Priority, Message.IdentigenRemoved);
                }
                else
                {
                    if (this.Identigen == data)
                    {
                        this.Identigen = string.Empty;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.IdentigenNotRemoved);
                    }
                }

                return;
            }

            var localCarcassNo = this.DataManager.HasIdentigenBeenAssigned(data);
            if (localCarcassNo > 0)
            {
                var message = string.Format(Message.IdentigenBarcodeAlreadyAssigned, data, localCarcassNo);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            this.Identigen = data.Trim();
        }

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }

        /// <summary>
        /// Handle the selected stock detail parse/edit.
        /// </summary>
        /// <param name="detail">The detail to parse for edit.</param>
        protected override void HandleSelectedStockDetail(StockDetail detail)
        {
            this.EntitySelectionChange = true;
            try
            {
                this.Eartag = detail.Eartag;
                if (detail.APGoodsReceiptDetailID.HasValue)
                {
                    detail.SupplierID = this.DataManager.GetSupplierId(detail.APGoodsReceiptDetailID.ToInt());
                }

                if (this.Breeds == null)
                {
                    this.GetBreeds();
                }
        
                this.CarcassSupplier = detail.Supplier;
                this.AgeInMonths = detail.AgeInMonths.ToInt();
                this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == detail.CategoryID);
                this.Breed = this.Breeds.FirstOrDefault(x => x.NouBreedID == detail.BreedID);
                this.FarmAssured = this.YesNoValues.FirstOrDefault(x => x.Equals(detail.FarmAssuredYesNo));
                //this.PrivateKill = this.YesNoValues.FirstOrDefault(x => x.Equals(detail.Attribute245));
                //this.SelectedDressSpec =  this.DressSpecs.FirstOrDefault(x => x.Data.CompareIgnoringCase(this.Attribute246));
                //this.SelectedKillingType = this.KillingTypes.FirstOrDefault(x => x.Data.CompareIgnoringCase(this.Attribute247));
                this.DOB = detail.DOB;
                //this.SelectedDestination = detail.Generic2;
                //this.SelectedCustomer =
                //    this.Customers.FirstOrDefault(x => x.BPMasterID == detail.CustomerID);
                this.Identigen = detail.Identigen;
                this.Sex = detail.Sex;
                this.CountryOfOrigin =
                    this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(detail.CountryOfOrigin));
                this.RearedIn =
                    this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(detail.RearedIn));
            }
            finally
            {
                this.EntitySelectionChange = false;
            }
        }

        /// <summary>
        /// Open the detain/condemn window.
        /// </summary>
        protected override void DetainCondemnCommandExecute()
        {
            base.DetainCondemnCommandExecute();
            this.Locator.DetainCondemn.KillNumber = this.NextKillNumber.ToString();
            this.Locator.DetainCondemn.CarcassNumber = this.NextCarcassNumber.ToString();
            this.Locator.DetainCondemn.Supplier = this.CarcassSupplier;
            this.Locator.DetainCondemn.Eartag = this.Eartag;
            this.Locator.DetainCondemn.SelectedCategory = this.SelectedCategory;
        }

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="category">The scanned passport data barcode.</param>
        protected override void HandleCategorySelection(NouCategory category)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }
           
            var previousCat = this.PreviousCategory;
            if (category.CAT == "A" && this.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
            {
                this.isInvalidCategory = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    var message = string.Format(Message.InvalidCategoryAge, ApplicationSettings.YoungBullMaxAge);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);

                    this.IgnoreCategoryCheck = true;
                    this.SelectedCategory = previousCat;
                    this.IgnoreCategoryCheck = false;
                }));
            }

            var localSex = this.DepartmentBodiesManager.FindSex(category.CAT);
            if (string.IsNullOrEmpty(this.Sex))
            {
                this.Sex = localSex;
            }
            //else
            //{
            //    var isValidSex = this.DepartmentBodiesManager.IsValidSex(category.CAT, this.Sex);
            //    if (!isValidSex)
            //    {
            //        if (this.Sex == Strings.Male)
            //        {
            //            this.Sex = Strings.Female;
            //        }
            //        else
            //        {
            //            this.Sex = Strings.Male;
            //        }

            //        return;
            //    }
            //}
        }

        /// <summary>
        /// Handles the incoming detained condemned data.
        /// </summary>
        /// <param name="detail">The incoming data.</param>
        protected override void HandleDetainedCondemnedData(StockDetail detail)
        {
            base.HandleDetainedCondemnedData(detail);
            if (this.currentCarcass != null)
            {
                this.currentCarcass.CondemnedSide1 = detail.CondemnedSide1;
                this.currentCarcass.CondemnedSide2 = detail.CondemnedSide2;
                this.currentCarcass.DetainedSide1 = detail.DetainedSide1;
                this.currentCarcass.DetainedSide2 = detail.DetainedSide2;
                this.currentCarcass.TBYes = detail.TBYes;
                this.currentCarcass.NotInsured = detail.NotInsured;
                this.currentCarcass.Casualty = detail.Casualty;
                this.currentCarcass.BurstBelly = detail.BurstBelly;
                this.currentCarcass.FatColour = detail.FatColour;
            }
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.OpenScanner();
            this.refreshTimer.Start();
            Messenger.Default.Send(ViewType.Grader, Token.SetDataContext);
            Messenger.Default.Send(true, Token.DisableLegacyAttributesView);
            if (this.currentCarcass == null)
            {
                this.ClearAttributes();
            }

            //if (this.EditMode)
            //{
            //    this.ManualGradingModeOn = true;
            //    this.GetCarcassDetails();
            //    this.FindNextCarcass();
            //    this.EditMode = false;
            //}
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.refreshTimer.Stop();
            if (this.EditMode)
            {
                this.EditComplete();
            }
        }

        /// <summary>
        /// The the lot count for combo sheep.
        /// </summary>
        /// <param name="killed"></param>
        /// <param name="total"></param>
        protected override void SetSheepLotCount(int killed, int total)
        {
            if (this.LairageGraderCombo && ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                this.SetLotCount(killed, total);
            }
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.CarcassNumber)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextCarcassNumberBeef = this.DataManager.GetLastcarcassNumber(this.SelectedDocNumbering.NextNumber, Constant.Beef);
            }

            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.CarcassNumberSheep)));
            this.SelectedSheepDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedSheepDocNumbering != null)
            {
                this.NextCarcassNumberSheep = this.DataManager.GetLastcarcassNumber(this.SelectedSheepDocNumbering.NextNumber, Constant.Sheep);
            }

            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.CarcassNumberPig)));
            this.SelectedPigDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedPigDocNumbering != null)
            {
                this.NextCarcassNumberPig = this.DataManager.GetLastcarcassNumber(this.SelectedPigDocNumbering.NextNumber, Constant.Pig);
            }

            if (ApplicationSettings.GraderMode == GraderMode.Beef)
            {
                this.NextCarcassNumber = this.NextCarcassNumberBeef;
            }
            else if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                this.NextCarcassNumber = this.NextCarcassNumberSheep;
            }
            else
            {
                this.NextCarcassNumber = this.NextCarcassNumberPig;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearLairageOrder();
            Messenger.Default.Send(Token.Message, Token.CloseLairageOrderWindow);
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            // not implemented
        }

        /// <summary>
        /// Removes an item from the grid.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            this.RemoveItem(ViewType.LairageOrder);
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            // not implemented
        }

        protected override void ClearAttributes()
        {
            base.ClearAttributes();
            this.ConformanceEIsSelected = false;
            this.ConformanceOIsSelected = false;
            this.ConformanceUIsSelected = false;
            this.ConformanceRIsSelected = false;
            this.ConformancePIsSelected = false;
            this.ConformanceEqualsIsSelected = false;
            this.ConformanceMinusIsSelected = false;
            this.ConformancePlusIsSelected = false;
            this.FatScore1IsSelected = false;
            this.FatScore2IsSelected = false;
            this.FatScore3IsSelected = false;
            this.FatScore4IsSelected = false;
            this.FatScore5IsSelected = false;
            this.FatScoreMinusIsSelected = false;
            this.FatScoreEqualsIsSelected = false;
            this.FatScorePlusIsSelected = false;
            this.ConformanceOldUIsSelected = false;
            this.ConformanceOldOIsSelected = false;
            this.ConformanceOldEIsSelected = false;
            this.ConformanceOldPIsSelected = false;
            this.ConformanceOldUPlusIsSelected = false;
            this.ConformanceOldMinusUIsSelected = false;
            this.ConformanceOldRIsSelected = false;
            this.ConformanceOldOPlusIsSelected = false;
            this.ConformanceOldMinusOIsSelected = false;
            this.ConformanceOldPPlusIsSelected = false;
            this.ConformanceOldMinusPIsSelected = false;
            this.ConformanceOldOMinusIsSelected = false;
            this.ConformanceOldPMinusIsSelected = false;
            this.FatScoreOld1IsSelected = false;
            this.FatScoreOld2IsSelected = false;
            this.FatScoreOld3IsSelected = false;
            this.FatScoreOld4HIsSelected = false;
            this.FatScoreOld4LIsSelected = false;
            this.FatScoreOld5HIsSelected = false;
            this.FatScoreOld5LIsSelected = false;
            this.FatScoreOld5IsSelected = false;
            this.ConformationScore = string.Empty;
            this.FatScore = string.Empty;
            this.SelectedCustomer = null;
            this.CarcassSupplier = string.Empty;
            this.ThirdParty = Strings.No;
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Marks the current lot as processed, moving to the next queued lot.
        /// </summary>
        private void NextPigLotCommandExecute()
        {
            if (!ApplicationSettings.UsingKillLineQueueNumbers || this.CurrentIntake == null)
            {
                return;
            }

            NouvemMessageBox.Show(Message.ProcessCurrentLotPrompt, NouvemMessageBoxButtons.YesNo, true);
            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
            {
                this.DataManager.SetIntakeProcessing(this.CurrentIntake.SaleID, true);
                this.GetNextQueuedHerd();
            }
        }

        /// <summary>
        /// Switch the view.
        /// </summary>
        private void SwitchViewCommandExecute(string view)
        {
            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view == Constant.Suppliers)
                    {
                        if (!this.CanSelectSupplier)
                        {
                            return;
                        }

                        this.Locator.TouchscreenSuppliers.MasterModule = ViewType.LairageIntakeTouchscreen;
                        if (!this.PartnersScreenCreated)
                        {
                            this.PartnersScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                            //this.Locator.TouchscreenOrders.OnEntry();
                            return;
                        }

                        //Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplayPartnerSales);
                        Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                        this.Locator.TouchscreenSuppliers.OnEntry();
                        return;
                    }

                    var type = ApplicationSettings.GraderMode == GraderMode.Sheep ? Constant.Sheep : Constant.Pig;
                    this.Locator.TouchscreenOrders.SetModule(ViewType.Grader);
                    this.Locator.TouchscreenOrders.GetAllOrders(type);
                    if (!this.OrdersScreenCreated)
                    {
                        this.OrdersScreenCreated = true;
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                        return;
                    }

                    Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
                    this.Locator.TouchscreenOrders.OnEntry();
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        /// <summary>
        /// Gets the next carcass details.
        /// </summary>
        private void FindCarcassCommandExecute()
        {
            if (this.currentCarcass != null && this.currentCarcass.CarcassSide == StockDetail.Side.Side2)
            {
                SystemMessage.Write(MessageType.Issue, Message.CarcassProcessingInProgress);
                NouvemMessageBox.Show(Message.CarcassProcessingInProgress, touchScreen: true);
                return;
            }

            if (!this.FindNextCarcass())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.CarcassNotInKillQueue, this.NextCarcassNumber));
                NouvemMessageBox.Show(string.Format(Message.CarcassNotInKillQueue, this.NextCarcassNumber), touchScreen:true);
                return;
            }

            this.GetCarcassDetails();
        }

        #endregion

        #region helper

        /// <summary>
        /// Opens the indicator port.
        /// </summary>
        private void OpenPort()
        {
            this.IndicatorManager.OpenIndicatorPort();
            this.manualWeight = false;
            this.Locator.Indicator.ManualWeight = false;
        }

        /// <summary>
        /// Sets the tare.
        /// </summary>
        private void SetTare()
        {
            if (ApplicationSettings.GraderMode == GraderMode.Beef)
            {
                var id = this.currentCarcass != null && this.currentCarcass.INMasterID > 0
                    ? this.currentCarcass.INMasterID
                    : this.BeefIntakeProductID;
                var beefProduct =
                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (beefProduct != null)
                {
                    this.Locator.Indicator.Tare = beefProduct.PiecesTareWeight;
                }

                return;
            }

            if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                var id = this.currentCarcass != null && this.currentCarcass.INMasterID > 0
                    ? this.currentCarcass.INMasterID
                    : this.SheepIntakeProductID;

                var sheepProduct =
                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (sheepProduct != null)
                {
                    this.Locator.Indicator.Tare = sheepProduct.PiecesTareWeight;

                    ////if (!ApplicationSettings.EweTare.IsNullOrZero() && this.currentCarcass != null && this.currentCarcass.CategoryName.CompareIgnoringCase(Constant.Ewe))
                    ////{
                    //    this.Locator.Indicator.Tare = ApplicationSettings.EweTare.ToDouble();
                    ////}
                    ////else if (!ApplicationSettings.LambTare.IsNullOrZero() && this.currentCarcass != null && this.currentCarcass.CategoryName.ContainsIgnoringCase(Constant.Lamb))
                    ////{
                    // this.Locator.Indicator.Tare = ApplicationSettings.LambTare.ToDouble();
                    ////}
                    ////else
                    ////{
                    //   this.Locator.Indicator.Tare = sheepProduct.PiecesTareWeight;
                    ////}
                }

                return;
            }

            if (ApplicationSettings.GraderMode == GraderMode.Pig)
            {
                var id = this.currentCarcass != null && this.currentCarcass.INMasterID > 0
                    ? this.currentCarcass.INMasterID
                    : this.PigIntakeProductID;
                var pigProduct =
                 NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == id);
                if (pigProduct != null)
                {
                    this.Locator.Indicator.Tare = pigProduct.PiecesTareWeight;
                }

                return;
            }
        }

        /// <summary>
        /// Sets the tare.
        /// </summary>
        //private void SetTare()
        //{
        //    if (ApplicationSettings.GraderMode == GraderMode.Beef)
        //    {
        //        var beefProduct =
        //        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.BeefIntakeProductID);
        //        if (beefProduct != null)
        //        {
        //            this.Locator.Indicator.Tare = beefProduct.PiecesTareWeight;
        //        }

        //        return;
        //    }

        //    if (ApplicationSettings.GraderMode == GraderMode.Sheep)
        //    {
        //        var sheepProduct =
        //        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.SheepIntakeProductID);
        //        if (sheepProduct != null)
        //        {
        //            if (!ApplicationSettings.EweTare.IsNullOrZero() && this.currentCarcass != null && this.currentCarcass.CategoryName.CompareIgnoringCase(Constant.Ewe))
        //            {
        //                this.Locator.Indicator.Tare = ApplicationSettings.EweTare.ToDouble();
        //            }
        //            else if (!ApplicationSettings.LambTare.IsNullOrZero() && this.currentCarcass != null && this.currentCarcass.CategoryName.ContainsIgnoringCase(Constant.Lamb))
        //            {
        //                this.Locator.Indicator.Tare = ApplicationSettings.LambTare.ToDouble();
        //            }
        //            else
        //            {
        //                this.Locator.Indicator.Tare = sheepProduct.PiecesTareWeight;
        //            }
        //        }

        //        return;
        //    }

        //    if (ApplicationSettings.GraderMode == GraderMode.Pig)
        //    {
        //         var pigProduct =
        //         NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.PigIntakeProductID);
        //        if (pigProduct != null)
        //        {
        //            this.Locator.Indicator.Tare = pigProduct.PiecesTareWeight;
        //        }

        //        return;
        //    }
        //}

        /// <summary>
        /// Accumulates previous weight on to the tare.
        /// </summary>
        private void SetTareAccummulation(decimal? wgt)
        {
            if (this.AccumulateWeightToTareAtGrader)
            {
                this.Locator.Indicator.Tare += wgt.ToDouble();
            }
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.ScannerStockMode = ScannerMode.Scan;
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += this.TimerOnTick;
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            this.DontChangeMode = true;
            this.Timer.Stop();
            Keypad.Show(KeypadTarget.ManualSerial);
        }

        /// <summary>
        /// Gets the current days kill.
        /// </summary>
        private void GetCarcassDetails()
        {
            if (ApplicationSettings.GraderMode == GraderMode.Beef)
            {
                if (this.GradeDisplayViewModel != this.Locator.Grade)
                {
                    this.SetGradeDisplay();
                    this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPanel;
                }

                this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetTodaysCarcasses(Constant.Beef)
                    .Where(x => x != null).OrderByDescending(x => x.KillNumber));
            }
            else if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                if (this.GradeDisplayViewModel != this.Locator.Grade)
                {
                    this.GradeDisplayViewModel = this.Locator.Grade;
                    this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPanel;
                }

                var allDetails = this.DataManager.GetTodaysCarcasses(Constant.Sheep)
                    .Where(x => x != null).OrderByDescending(x => x.KillNumber).ToList();

                if (ApplicationSettings.KilledSheepToAppearAtTopOfGrid > 0)
                {
                    var localCarcass = allDetails.FirstOrDefault(x => x.CarcassNumber == this.NextCarcassNumber);
                    if (localCarcass != null)
                    {
                        var pos = allDetails.IndexOf(localCarcass);
                        Messenger.Default.Send(pos + 3, Token.ScrollToRow);
                    }
                }

                this.StockDetails = new ObservableCollection<StockDetail>(allDetails);
            }
            else
            {
                if (this.GradeDisplayViewModel != this.Locator.Pig)
                {
                    this.SetGradeDisplay();
                    this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPigPanel;
                }

                this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetTodaysCarcasses(Constant.Pig)
                    .Where(x => x != null).OrderByDescending(x => x.KillNumber));
            }
            
            this.SelectedStockDetail = this.StockDetails.FirstOrDefault(x => x.CarcassNumber == this.NextCarcassNumber);
        }

        /// <summary>
        /// Gets the next carcass in the line.
        /// </summary>
        private bool FindCarcassForEdit()
        {
            this.ClearAttributes();
            Messenger.Default.Send(Token.Message, Token.ClearDetainCondemnData);

            this.currentCarcass = this.DataManager.GetCarcassForEdit(this.attributeEditId);
         
            if (this.currentCarcass == null)
            {
                this.SelectedStockDetail = null;
                this.ClearAttributes();
                return false;
            }

            this.SetGrades(this.currentCarcass.Grade);
            this.NextKillNumber = this.currentCarcass.KillNumber.ToInt();
            this.Eartag = this.currentCarcass.Eartag;
            this.Sex = this.currentCarcass.Sex;
            this.RearedIn = this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(this.currentCarcass.RearedIn));
            this.CarcassSupplier = this.currentCarcass.Supplier;
            this.AgeInMonths = this.currentCarcass.AgeInMonths.ToInt();
            this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == this.currentCarcass.CategoryID);
            this.Breed = this.Breeds.FirstOrDefault(x => x.NouBreedID == this.currentCarcass.BreedID);
            this.FarmAssured = this.YesNoValues.FirstOrDefault(x => x.Equals(this.currentCarcass.FarmAssuredYesNo));
            this.PrivateKill = this.YesNoValues.FirstOrDefault(x => x.Equals(this.currentCarcass.Attribute245));
            this.SelectedDressSpec = this.DressSpecs.FirstOrDefault(x => x.Data.CompareIgnoringCase(this.currentCarcass.Attribute246));
            this.SelectedKillingType = this.KillingTypes.FirstOrDefault(x => x.Data.CompareIgnoringCase(this.currentCarcass.Attribute247));
            //this.SelectedDestination = this.currentCarcass.Generic2;
            this.SelectedCustomer =
                this.Customers.FirstOrDefault(x => x.BPMasterID == this.currentCarcass.CustomerID);
            this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.Name == this.currentCarcass.CountryOfOrigin);
            this.Identigen = this.currentCarcass.Identigen;

            this.StockDetails.Clear();
            this.StockDetails.Add(this.currentCarcass);
            this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
            this.ManualGradingModeOn = true;
            return true;
        }

        /// <summary>
        /// Gets the next carcass in the line.
        /// </summary>
        private bool FindNextCarcass()
        {
            this.ClearAttributes();
            Messenger.Default.Send(Token.Message, Token.ClearDetainCondemnData);

            this.currentCarcass = this.DataManager.GetCarcass(this.NextCarcassNumber, ApplicationSettings.GraderMode.ToString());

            if (this.currentCarcass == null)
            {
                this.SelectedStockDetail = null;
                this.ClearAttributes();
                this.GetNextKillNumber();
                this.SetLotCount(0,0);
                return false;
            }

            if (!string.IsNullOrEmpty(this.currentCarcass.Error))
            {
                // crash recovery, so set the grade for side 2.
                this.SetGrades(this.currentCarcass.Error);
                this.currentCarcass.Error = string.Empty;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SetKillCount(this.currentCarcass.APGoodsReceiptDetailID.ToInt());
            }));

            this.NextKillNumber = this.currentCarcass.KillNumber.ToInt();
            this.Eartag = this.currentCarcass.Eartag;
            this.CarcassSupplier = this.currentCarcass.Supplier;
            this.AgeInMonths = this.currentCarcass.AgeInMonths.ToInt();
            this.RearedIn = this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(this.currentCarcass.RearedIn));
            this.Sex = this.currentCarcass.Sex;
            this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == this.currentCarcass.CategoryID);
            this.Breed = this.Breeds.FirstOrDefault(x => x.NouBreedID == this.currentCarcass.BreedID);
            this.FarmAssured = this.YesNoValues.FirstOrDefault(x => x.Equals(this.currentCarcass.FarmAssuredYesNo));
            //this.PrivateKill = this.YesNoValues.FirstOrDefault(x => x.Equals(this.currentCarcass.Attribute245));
            //this.SelectedDressSpec = this.DressSpecs.FirstOrDefault(x => x.Data.CompareIgnoringCase(this.currentCarcass.Attribute246));
            //this.SelectedKillingType = this.KillingTypes.FirstOrDefault(x => x.Data.CompareIgnoringCase(this.currentCarcass.Attribute247));
            //this.SelectedDestination = this.currentCarcass.Generic2;
            this.SelectedCustomer =
                this.Customers.FirstOrDefault(x => x.BPMasterID == this.currentCarcass.CustomerID);
            this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.Name == this.currentCarcass.CountryOfOrigin);
            this.Identigen = this.currentCarcass.Identigen;

            if (this.currentCarcass.Attribute200.CompareIgnoringCase("Welsh"))
            {
                this.SelectedDestination = ApplicationSettings.WelshDestination;
            }
            else if (this.currentCarcass.Attribute200.CompareIgnoringCase("Organic"))
            {
                this.SelectedDestination = ApplicationSettings.OrganicDestination;
            }
            else if (this.currentCarcass.Attribute200.CompareIgnoringCase("SelectFarm"))
            {
                this.SelectedDestination = ApplicationSettings.SelectFarmDestination;
            }
            else if (this.currentCarcass.Attribute200.CompareIgnoringCase("Continental"))
            {
                this.SelectedDestination = ApplicationSettings.ContinentalDestination;
            }
            else
            {
                this.SelectedDestination = ApplicationSettings.StandardDestination;
            }

            var localMode = ApplicationSettings.GraderMode;
            if (this.currentCarcass.KillType == Constant.Beef)
            {
                ApplicationSettings.GraderMode = GraderMode.Beef;
                if (ApplicationSettings.DisplayUTMOTMMessage)
                {
                    var ageValue = this.currentCarcass.IsUTM ? "UTM" : "OTM";
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(string.Format(Message.OTMUTMMessage, ageValue), touchScreen:true);
                    }));
                }
            }

            if (this.currentCarcass.KillType == Constant.Sheep)
            {
                ApplicationSettings.GraderMode = GraderMode.Sheep;
            }

            if (localMode != ApplicationSettings.GraderMode)
            {
                this.GetCarcassDetails();
            }

            this.SetTare();

            return true;
        }
      
        /// <summary>
        /// Method that handles the UI manual grade selection.
        /// </summary>
        /// <param name="selection">the grade selection value</param>
        /// <param name="isSelected">flag to indicate selection or deselection</param>
        private void GradeSelection(string selection, bool isSelected)
        {
            if (ApplicationSettings.GradeSystem != GradeSystem.EUROP)
            {
                return;
            }

            if (this.ManualGradingModeOn)
            {
                if (isSelected)
                {
                    switch (selection)
                    {
                        case "ConE":
                            this.manualConfFirstChar = "E";
                            break;
                        case "ConU":
                            this.manualConfFirstChar = "U";
                            break;
                        case "ConR":
                            this.manualConfFirstChar = "R";
                            break;
                        case "ConO":
                            this.manualConfFirstChar = "O";
                            break;
                        case "ConP":
                            this.manualConfFirstChar = "P";
                            break;
                        case "Con-":
                            this.manualConfSecondChar = "-";
                            break;
                        case "Con=":
                            this.manualConfSecondChar = "=";
                            break;
                        case "Con+":
                            this.manualConfSecondChar = "+";
                            break;
                        case "Fat1":
                            this.manualFatFirstChar = "1";
                            break;
                        case "Fat2":
                            this.manualFatFirstChar = "2";
                            break;
                        case "Fat3":
                            this.manualFatFirstChar = "3";
                            break;
                        case "Fat4":
                            this.manualFatFirstChar = "4";
                            break;
                        case "Fat5":
                            this.manualFatFirstChar = "5";
                            break;
                        case "Fat-":
                            this.manualFatSecondChar = "-";
                            break;
                        case "Fat=":
                            this.manualFatSecondChar = "=";
                            break;
                        case "Fat+":
                            this.manualFatSecondChar = "+";
                            break;
                    }
                }
                else
                {
                    switch (selection)
                    {
                        case "ConE":
                            if (this.manualConfFirstChar == "E")
                            {
                                this.manualConfFirstChar = string.Empty;
                            }

                            break;
                        case "ConU":
                            if (this.manualConfFirstChar == "U")
                            {
                                this.manualConfFirstChar = string.Empty;
                            }

                            break;
                        case "ConR":
                            if (this.manualConfFirstChar == "R")
                            {
                                this.manualConfFirstChar = string.Empty;
                            }

                            break;
                        case "ConO":
                            if (this.manualConfFirstChar == "O")
                            {
                                this.manualConfFirstChar = string.Empty;
                            }

                            break;
                        case "ConP":
                            if (this.manualConfFirstChar == "P")
                            {
                                this.manualConfFirstChar = string.Empty;
                            }

                            break;
                        case "Con-":
                            if (this.manualConfSecondChar == "-")
                            {
                                this.manualConfSecondChar = string.Empty;
                            }

                            break;
                        case "Con=":
                            if (this.manualConfSecondChar == "=")
                            {
                                this.manualConfSecondChar = string.Empty;
                            }

                            break;
                        case "Con+":
                            if (this.manualConfSecondChar == "+")
                            {
                                this.manualConfSecondChar = string.Empty;
                            }

                            break;
                        case "Fat1":
                            if (this.manualFatFirstChar == "1")
                            {
                                this.manualFatFirstChar = string.Empty;
                            }

                            break;
                        case "Fat2":
                            if (this.manualFatFirstChar == "2")
                            {
                                this.manualFatFirstChar = string.Empty;
                            }

                            break;
                        case "Fat3":
                            if (this.manualFatFirstChar == "3")
                            {
                                this.manualFatFirstChar = string.Empty;
                            }

                            break;
                        case "Fat4":
                            if (this.manualFatFirstChar == "4")
                            {
                                this.manualFatFirstChar = string.Empty;
                            }

                            break;
                        case "Fat5":
                            if (this.manualFatFirstChar == "5")
                            {
                                this.manualFatFirstChar = string.Empty;
                            }

                            break;
                        case "Fat-":
                            if (this.manualFatSecondChar == "-")
                            {
                                this.manualFatSecondChar = string.Empty;
                            }

                            break;
                        case "Fat=":
                            if (this.manualFatSecondChar == "=")
                            {
                                this.manualFatSecondChar = string.Empty;
                            }

                            break;
                        case "Fat+":
                            if (this.manualFatSecondChar == "+")
                            {
                                this.manualFatSecondChar = string.Empty;
                            }

                            break;
                    }
                }

                this.FormatGrading();
            }
        }       

        /// <summary>
        /// Method that formats the manual grading scores into the correct order.
        /// </summary>
        private void FormatGrading()
        {
            this.ConformationScore = string.Format("{0}{1}", this.manualConfFirstChar, this.manualConfSecondChar);
            this.FatScore = string.Format("{0}{1}", this.manualFatFirstChar, this.manualFatSecondChar);
        }

        /// <summary>
        /// Method that determines if we have a valid grade.
        /// </summary>
        /// <returns>a flag indicating a valid grade or not</returns>
        private bool IsValidGrade()
        {
            if (this.currentCarcass != null && this.currentCarcass.GradeWholeBeefAnimal)
            {
                return true;
            }

            if (!string.IsNullOrEmpty(this.conformationScore) && !string.IsNullOrEmpty(this.fatScore))
            {
                if (ApplicationSettings.GradeSystem == GradeSystem.FifteenPoint || ApplicationSettings.GradeSystem == GradeSystem.FivePoint)
                {
                    if (this.conformationScore.Length >= 1 && this.fatScore.Length >= 1)
                    {
                        return true;
                    }
                }
                else
                {
                    if (this.conformationScore.Length + this.fatScore.Length == 4)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        private void RecordSheepWeight()
        {
            if (this.LairageGraderCombo)
            {
                this.RecordGraderCreatedSheepWeight();
                return;
            }

            try
            {
                #region validation

                if (this.weighingInProgress)
                {
                    return;
                }

                this.weighingInProgress = true;

                if (this.currentCarcass == null)
                {
                    return;
                }

                #endregion
              
                if (!this.manualWeight)
                {
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.Log.LogDebug(this.GetType(), string.Format("Carcass No:{0} - Weight Recorded:{1}", this.nextCarcassNumber, this.indicatorWeight));
                        this.manualWeight = false;
                    }
                    else if (saveWeightResult.Equals(Message.ScalesInMotion) && ApplicationSettings.WaitForMotionAtGrader)
                    {
                        // motion on scales, so move processing to the tick event and wait for scales stability.
                        //this.waitForStabilityTraceabilityData = data;
                        this.scalesMotionTimer.Start();
                        return;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true);
                        return;
                    }
                }

                #region identical weights check

                var localWgt = Math.Round(this.indicatorWeight, 1);
                this.weights.Add(localWgt);
                if (this.weights.Count > 2)
                {
                    if (this.weights.Count > 4)
                    {
                        if (this.weights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsStop, localWgt), touchScreen: true);
                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS STOP --- Carcass no:{0}. 5th identical weight of:{1} recorded. Aborting transaction",
                                    this.NextCarcassNumber, localWgt));
                            this.weights.RemoveAt(0);
                            return;
                        }
                        else
                        {
                            var localWeights = this.weights.Reverse().Take(3);
                            if (localWeights.Distinct().Count() == 1)
                            {
                                NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                                {
                                    this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                    this.weights.RemoveAt(0);
                                    return;
                                }

                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                            }
                        }

                        this.weights.RemoveAt(0);
                    }
                    else
                    {
                        var localWeights = this.weights.Reverse().Take(3);
                        if (localWeights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                return;
                            }

                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                        }
                    }
                }

                #endregion

                this.currentCarcass.Detained = this.currentCarcass.DetainedSide1 == true ||
                                               this.currentCarcass.DetainedSide2 == true;
                this.currentCarcass.Condemned = this.currentCarcass.CondemnedSide1 == true ||
                                               this.currentCarcass.CondemnedSide2 == true;
                this.currentCarcass.Generic2 = this.SelectedDestination;
                this.currentCarcass.Tare = this.tare;
                this.currentCarcass.ManualWeight = this.manualWeight;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.Grade = this.Grade ?? string.Empty;
                this.currentCarcass.CarcassNumber = this.NextCarcassNumber;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty;
                this.currentCarcass.FarmAssured = this.FarmAssured.YesNoToBool();

                if (this.DataManager.UpdateGraderSheepTransaction(this.currentCarcass))
                {
                    var labelType = this.currentCarcass.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge
                        ? LabelType.Item
                        : LabelType.Box;

                    var labelsToPrint = 1;
                    if (!ApplicationSettings.EweLabelsToPrint.IsNullOrZero()
                        && (this.currentCarcass.CategoryName.CompareIgnoringCase(Constant.Ewe) || this.currentCarcass.CategoryName.CompareIgnoringCase(Constant.Ram)))
                    {
                        labelsToPrint = ApplicationSettings.EweLabelsToPrint.ToInt();
                    }

                    int localPrinter = 1;
                    for (int i = 0; i < this.StockLabelsToPrint; i++)
                    {
                        localPrinter = this.Print(this.currentCarcass.StockTransactionID.ToInt(), labelType, killType: KillType.Sheep, labelsToPrint: labelsToPrint);
                    }
                   
                    if (ApplicationSettings.PrintingTrailLabelAtGrader)
                    {
                        if (this.SelectedCustomer != null)
                        {
                            this.Print(this.currentCarcass.StockTransactionID.ToInt(), LabelType.Pallet, killType: KillType.Sheep, printer: localPrinter);
                        }
                    }

                    if (ApplicationSettings.PrintFreezerLabelAtGrader)
                    {
                        if (!string.IsNullOrWhiteSpace(this.currentCarcass.FreezerType))
                        {
                            this.Print(this.currentCarcass.StockTransactionID.ToInt(), LabelType.Shipping, printer: localPrinter);
                        }
                    }

                    var message = string.Format(Message.SheepCarcassWeightRecorded, this.indicatorWeight);
                    SystemMessage.Write(MessageType.Priority, message);
                    NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                }

                if (this.NextCarcassNumber < this.SelectedSheepDocNumbering.LastNumber)
                {
                    this.NextCarcassNumber++;
                }
                else
                {
                    this.NextCarcassNumber = 1;
                }

                this.NextKillNumber++;
                this.ManualGradingModeOn = true;
                this.GetCarcassDetails();
                this.FindNextCarcass();

                this.SelectedStockDetail = this.StockDetails.FirstOrDefault(x => x.CarcassNumber == this.NextCarcassNumber);
            }
            finally
            {
                this.weighingInProgress = false;
                this.OpenPort();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.CheckForNewEntities(EntityType.BusinessPartner);
                }));
            }
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        private void RecordGraderCreatedSheepWeight()
        {
            try
            {
                #region validation

                if (this.weighingInProgress)
                {
                    return;
                }

                this.weighingInProgress = true;

                if (this.currentCarcass == null)
                {
                    return;
                }

                if (this.RemainingCount == 0)
                {
                    if (this.CurrentIntake != null)
                    {
                        this.DataManager.AddNewSheepCarcass(this.CurrentIntake.SaleDetails.First().SaleDetailID);
                        this.GetNextSheepCarcass();
                    }
                    else
                    {
                        return;
                    }
                }

                #endregion

                if (!this.manualWeight)
                {
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.Log.LogDebug(this.GetType(), string.Format("Carcass No:{0} - Weight Recorded:{1}", this.nextCarcassNumber, this.indicatorWeight));
                        this.manualWeight = false;
                    }
                    else if (saveWeightResult.Equals(Message.ScalesInMotion) && ApplicationSettings.WaitForMotionAtGrader)
                    {
                        // motion on scales, so move processing to the tick event and wait for scales stability.
                        //this.waitForStabilityTraceabilityData = data;
                        this.scalesMotionTimer.Start();
                        return;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true);
                        return;
                    }
                }

                #region identical weights check

                var localWgt = Math.Round(this.indicatorWeight, 1);
                this.weights.Add(localWgt);
                if (this.weights.Count > 2)
                {
                    if (this.weights.Count > 4)
                    {
                        if (this.weights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsStop, localWgt), touchScreen: true);
                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS STOP --- Carcass no:{0}. 5th identical weight of:{1} recorded. Aborting transaction",
                                    this.NextCarcassNumber, localWgt));
                            this.weights.RemoveAt(0);
                            return;
                        }
                        else
                        {
                            var localWeights = this.weights.Reverse().Take(3);
                            if (localWeights.Distinct().Count() == 1)
                            {
                                NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                                {
                                    this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                    this.weights.RemoveAt(0);
                                    return;
                                }

                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                            }
                        }

                        this.weights.RemoveAt(0);
                    }
                    else
                    {
                        var localWeights = this.weights.Reverse().Take(3);
                        if (localWeights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                return;
                            }

                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                        }
                    }
                }

                #endregion

                this.currentCarcass.SequencedDate = DateTime.Now;
                this.currentCarcass.Detained = this.currentCarcass.DetainedSide1 == true ||
                                               this.currentCarcass.DetainedSide2 == true;
                this.currentCarcass.Condemned = this.currentCarcass.CondemnedSide1 == true ||
                                               this.currentCarcass.CondemnedSide2 == true;
                this.currentCarcass.Generic2 = this.SelectedDestination;
                this.currentCarcass.Tare = this.tare;
                this.currentCarcass.DOB = this.DOB;
                this.currentCarcass.ManualWeight = this.manualWeight;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.Grade = this.Grade ?? string.Empty;
                this.currentCarcass.RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty;
                this.currentCarcass.CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty;
                this.currentCarcass.Sex = this.Sex;
                this.currentCarcass.FarmAssured = this.FarmAssured.YesNoToBool();
                this.currentCarcass.Attribute245 = this.PrivateKill;
                this.currentCarcass.Attribute246 = this.SelectedDressSpec != null ? this.SelectedDressSpec.Data : string.Empty;
                this.currentCarcass.Attribute247 = this.SelectedKillingType != null ? this.SelectedKillingType.Data : string.Empty;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.KillNumber = this.UpdateKillNumber();
                var docNumber = this.DataManager.SetDocumentNumber(this.SelectedSheepDocNumbering.DocumentNumberingID);
                this.currentCarcass.CarcassNumber = docNumber.CurrentNumber;
                this.currentCarcass.Eartag = this.sheepTag;
                this.currentCarcass.Update = true;

                if (this.DataManager.UpdateGraderSheepTransaction(this.currentCarcass))
                {
                    this.SetTareAccummulation(this.currentCarcass.TransactionWeight);
                    var labelType = this.currentCarcass.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge
                        ? LabelType.Item
                        : LabelType.Box;

                    var labelsToPrint = 1;
                    if (!ApplicationSettings.EweLabelsToPrint.IsNullOrZero()
                        && (this.currentCarcass.CategoryName.CompareIgnoringCase(Constant.Ewe) || this.currentCarcass.CategoryName.CompareIgnoringCase(Constant.Ram)))
                    {
                        labelsToPrint = ApplicationSettings.EweLabelsToPrint.ToInt();
                    }

                    var localPrinter = this.Print(this.currentCarcass.StockTransactionID.ToInt(), labelType, killType: KillType.Sheep, labelsToPrint: labelsToPrint);
                    if (ApplicationSettings.PrintingTrailLabelAtGrader)
                    {
                        if (this.SelectedCustomer != null)
                        {
                            this.Print(this.currentCarcass.StockTransactionID.ToInt(), LabelType.Pallet, killType: KillType.Sheep, printer: localPrinter);
                        }
                    }

                    if (ApplicationSettings.PrintFreezerLabelAtGrader)
                    {
                        if (!string.IsNullOrWhiteSpace(this.currentCarcass.FreezerType))
                        {
                            this.Print(this.currentCarcass.StockTransactionID.ToInt(), LabelType.Shipping, printer: localPrinter);
                        }
                    }

                    var message = string.Format(Message.SheepCarcassWeightRecorded, this.indicatorWeight);
                    SystemMessage.Write(MessageType.Priority, message);
                    NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                }

                if (this.NextCarcassNumber < this.SelectedSheepDocNumbering.LastNumber)
                {
                    this.NextCarcassNumber++;
                }
                else
                {
                    this.NextCarcassNumber = 1;
                }

                this.NextKillNumber++;
                this.ManualGradingModeOn = true;
                this.RemainingCount -= 1;
                this.KillCount++;
                this.Eartag = string.Empty;
                this.sheepTag = string.Empty;

                if (this.RemainingCount == 0)
                {
                    if (!this.LairageGraderCombo)
                    {
                        SystemMessage.Write(MessageType.Priority, Message.PigLotProcessed);
                        NouvemMessageBox.Show(Message.PigLotProcessed, touchScreen: true, flashMessage: true);
                    }

                    this.GetCarcassDetails();
                    this.SelectedStockDetail = null;

                    if (this.CurrentIntake != null)
                    {
                        this.DataManager.SetIntakeProcessing(this.CurrentIntake.SaleID, true);
                    }
                }
                else
                {
                    this.GetNextSheepCarcass();
                }
            }
            finally
            {
                this.weighingInProgress = false;
                this.OpenPort();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.CheckForNewEntities(EntityType.BusinessPartner);
                }));
            }
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        private void RecordPigWeight()
        {
            try
            {
                #region validation

                if (this.weighingInProgress)
                {
                    return;
                }

                this.weighingInProgress = true;

                if (this.currentCarcass == null)
                {
                    return;
                }

                if (this.RemainingCount == 0)
                {
                    if (this.CurrentIntake != null)
                    {
                        this.DataManager.AddNewPigCarcass(this.CurrentIntake.SaleDetails.First().SaleDetailID);
                        this.GetNextPigCarcass();
                    }
                    else
                    {
                        return;
                    }
                }

                #endregion

                if (!this.manualWeight)
                {
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.Log.LogDebug(this.GetType(), string.Format("Carcass No:{0} - Weight Recorded:{1}", this.nextCarcassNumber, this.indicatorWeight));
                        this.manualWeight = false;
                    }
                    else if (saveWeightResult.Equals(Message.ScalesInMotion) && ApplicationSettings.WaitForMotionAtGrader)
                    {
                        // motion on scales, so move processing to the tick event and wait for scales stability.
                        //this.waitForStabilityTraceabilityData = data;
                        this.scalesMotionTimer.Start();
                        return;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true);
                        return;
                    }
                }

                #region identical weights check

                var localWgt = Math.Round(this.indicatorWeight, 1);
                this.weights.Add(localWgt);
                if (this.weights.Count > 2)
                {
                    if (this.weights.Count > 4)
                    {
                        if (this.weights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsStop, localWgt), touchScreen: true);
                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS STOP --- Carcass no:{0}. 5th identical weight of:{1} recorded. Aborting transaction",
                                    this.NextCarcassNumber, localWgt));
                            this.weights.RemoveAt(0);
                            return;
                        }
                        else
                        {
                            var localWeights = this.weights.Reverse().Take(3);
                            if (localWeights.Distinct().Count() == 1)
                            {
                                NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                                {
                                    this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                    this.weights.RemoveAt(0);
                                    return;
                                }

                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                            }
                        }

                        this.weights.RemoveAt(0);
                    }
                    else
                    {
                        var localWeights = this.weights.Reverse().Take(3);
                        if (localWeights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                return;
                            }

                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                        }
                    }
                }

                #endregion

                this.currentCarcass.Detained = this.currentCarcass.DetainedSide1 == true ||
                                               this.currentCarcass.DetainedSide2 == true;
                this.currentCarcass.Condemned = this.currentCarcass.CondemnedSide1 == true ||
                                               this.currentCarcass.CondemnedSide2 == true;
                this.currentCarcass.Generic2 = this.SelectedDestination;
                this.currentCarcass.Tare = this.tare;
                this.currentCarcass.ManualWeight = this.manualWeight;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.Grade = this.Grade ?? string.Empty;
                this.currentCarcass.RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty;
                this.currentCarcass.CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty;
                this.currentCarcass.Sex = this.Sex;
                this.currentCarcass.FarmAssured = this.FarmAssured.YesNoToBool();
                this.currentCarcass.Attribute245 = this.PrivateKill;
                this.currentCarcass.Attribute246 = this.SelectedDressSpec != null ? this.SelectedDressSpec.Data : string.Empty;
                this.currentCarcass.Attribute247 = this.SelectedKillingType != null ? this.SelectedKillingType.Data : string.Empty;

                this.currentCarcass.KillNumber = this.UpdateKillNumber();
                var docNumber = this.DataManager.SetDocumentNumber(this.SelectedPigDocNumbering.DocumentNumberingID);
                this.currentCarcass.CarcassNumber = docNumber.CurrentNumber;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;

                if (this.DataManager.UpdateGraderPigTransaction(this.currentCarcass))
                {
                    this.SetTareAccummulation(this.currentCarcass.TransactionWeight);
                    var labelType = this.currentCarcass.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge
                        ? LabelType.Item
                        : LabelType.Box;

                    int localPrinter = 1;
                    for (int i = 0; i < this.StockLabelsToPrint; i++)
                    {
                        localPrinter = this.Print(this.currentCarcass.StockTransactionID.ToInt(), labelType, killType: KillType.Pig);
                    }
                 
                    if (ApplicationSettings.PrintingTrailLabelAtGrader)
                    {
                        if (this.SelectedCustomer != null)
                        {
                            this.Print(this.currentCarcass.StockTransactionID.ToInt(), LabelType.Pallet, killType: KillType.Pig, printer: localPrinter);
                        }
                    }

                    if (ApplicationSettings.PrintFreezerLabelAtGrader)
                    {
                        if (!string.IsNullOrWhiteSpace(this.currentCarcass.FreezerType))
                        {
                            this.Print(this.currentCarcass.StockTransactionID.ToInt(), LabelType.Shipping, printer: localPrinter);
                        }
                    }

                    var message = string.Format(Message.SheepCarcassWeightRecorded, this.indicatorWeight);
                    SystemMessage.Write(MessageType.Priority, message);
                    NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                }

                if (this.NextCarcassNumber < this.SelectedPigDocNumbering.LastNumber)
                {
                    this.NextCarcassNumber++;
                }
                else
                {
                    this.NextCarcassNumber = 1;
                }

                this.NextKillNumber++;
                this.ManualGradingModeOn = true;
                this.RemainingCount-= 1;
                this.KillCount++;

                if (this.RemainingCount == 0)
                {
                    if (!this.LairageGraderCombo)
                    {
                        SystemMessage.Write(MessageType.Priority, Message.PigLotProcessed);
                        NouvemMessageBox.Show(Message.PigLotProcessed, touchScreen: true, flashMessage: true);
                    }
                   
                    this.GetCarcassDetails();
                    this.SelectedStockDetail = null;

                    if (this.CurrentIntake != null)
                    {
                        this.DataManager.SetIntakeProcessing(this.CurrentIntake.SaleID, true);
                    }
                }
                else
                {
                    this.GetNextPigCarcass();
                }
            }
            finally
            {
                this.weighingInProgress = false;
                this.OpenPort();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.CheckForNewEntities(EntityType.BusinessPartner);
                }));
            }
        }

        /// <summary>
        /// Opens a carcss for editing.
        /// </summary>
        private void EditCarcass()
        {
            try
            {
                #region validation

                if (this.weighingInProgress)
                {
                    return;
                }

                this.weighingInProgress = true;

                if (this.currentCarcass == null)
                {
                    return;
                }

                if (!this.IsValidGrade())
                {
                    SystemMessage.Write(MessageType.Issue, Message.InvalidGrade);
                    NouvemMessageBox.Show(Message.InvalidGrade, touchScreen: true);
                    return;
                }

                #endregion

                if (!this.manualWeight)
                {
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.Log.LogDebug(this.GetType(), string.Format("Carcass No:{0} - Weight Recorded:{1}", this.nextCarcassNumber, this.indicatorWeight));
                        this.manualWeight = false;

                        var carcassWeighingsDifference = this.CarcassSidesWeightDifferenceTooHigh();
                        if (carcassWeighingsDifference > 0)
                        {
                            var message = string.Format(Message.CarcassWeighingsOutsideOfWeightRange,
                                carcassWeighingsDifference);
                            SystemMessage.Write(MessageType.Issue, message);
                            NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                return;
                            }
                        }
                    }
                    else if (saveWeightResult.Equals(Message.ScalesInMotion) && ApplicationSettings.WaitForMotionAtGrader)
                    {
                        // motion on scales, so move processing to the tick event and wait for scales stability.
                        //this.waitForStabilityTraceabilityData = data;
                        this.scalesMotionTimer.Start();
                        return;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true);
                        return;
                    }
                }
                else
                {
                    var carcassWeighingsDifference = this.CarcassSidesWeightDifferenceTooHigh();
                    if (carcassWeighingsDifference > 0)
                    {
                        var message = string.Format(Message.CarcassWeighingsOutsideOfWeightRange,
                            carcassWeighingsDifference);
                        SystemMessage.Write(MessageType.Issue, message);
                        NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.No)
                        {
                            return;
                        }
                    }
                }

                this.currentCarcass.RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty;
                this.currentCarcass.Identigen = this.Identigen;
                this.currentCarcass.Generic2 = this.SelectedDestination;
                this.currentCarcass.Tare = this.tare;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.Grade = this.Grade;
                this.currentCarcass.Category = this.SelectedCategory;
                this.currentCarcass.Sex = this.Sex;
                this.currentCarcass.CarcassNumber = this.NextCarcassNumber;
                if (ApplicationSettings.InvalidateCatBFarmAssurance)
                {
                    if (this.currentCarcass.Category != null && this.currentCarcass.Category.CategoryID == this.categoryB)
                    {
                        this.currentCarcass.FarmAssured = false;
                    }
                }

                if (this.currentCarcass.CarcassSide == StockDetail.Side.Side1)
                {
                    this.currentCarcass.WeightSide1 = this.indicatorWeight;
                    this.currentCarcass.Detained = this.currentCarcass.DetainedSide1;

                }
                else
                {
                    this.currentCarcass.WeightSide2 = this.indicatorWeight;
                    this.currentCarcass.Detained = this.currentCarcass.DetainedSide2;
                }

                if (this.DataManager.EditGraderTransaction(this.currentCarcass))
                {
                    this.DataManager.UpdateCarcassWeight(this.currentCarcass.CarcassNumber.ToInt(), this.currentCarcass.KillType);
                    LabelType labelType;
                    if (ApplicationSettings.UseRiskToDeterminePrinterAtGrader)
                    {
                        labelType = this.currentCarcass.RedLabel
                            ? LabelType.Item
                            : LabelType.Box;
                    }
                    else
                    {
                        labelType = this.currentCarcass.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge
                            ? LabelType.Item
                            : LabelType.Box;
                    }

                    this.Print(this.currentCarcass.LabelID.ToInt(), labelType);
                    var message = string.Format(Message.CarcassDataRecorded, this.indicatorWeight,
                        this.currentCarcass.CarcassSideNo);
              
                    SystemMessage.Write(MessageType.Priority, message);
                    NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                }

                if (this.currentCarcass.CarcassSide == StockDetail.Side.Side1)
                {
                    this.FindCarcassForEdit();
                    this.currentCarcass.CarcassSide = StockDetail.Side.Side2;
                    this.ManualGradingModeOn = false;
                    this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
                }
                else
                {
                    NouvemMessageBox.Show(Message.CarcassEditingComplete, NouvemMessageBoxButtons.YesNo, touchScreen:true);
                    if (NouvemMessageBox.UserSelection == UserDialogue.No)
                    {
                        this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);
                    }

                    this.EditComplete();
                }
            }
            finally
            {
               
                this.weighingInProgress = false;
                this.OpenPort();
            }
        }

        /// <summary>
        /// Clean up for moving out of edit mode.
        /// </summary>
        private void EditComplete()
        {
            this.EditMode = false;
            this.GetLocalDocNumberings();
            this.ManualGradingModeOn = true;
            this.GetCarcassDetails();
            this.FindNextCarcass();
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        private void RecordWeight()
        {
            //if (this.currentCarcass != null && this.SelectedCategory != null)
            //{
            //    this.currentCarcass.INMasterID = this.DataManager.GetKillProductByCategory(this.SelectedCategory);
            //}

            if (this.currentCarcass != null && this.currentCarcass.NouKillType == KillType.Sheep)
            {
                this.RecordSheepWeight();
                return;
            }

            if (this.currentCarcass != null && this.currentCarcass.NouKillType == KillType.Pig)
            {
                this.RecordPigWeight();
                return;
            }

            if (this.editMode)
            {
                this.EditCarcass();
                return;
            }

            try
            {
                #region validation

                if (this.weighingInProgress)
                {
                    return;
                }

                this.weighingInProgress = true;

                if (this.currentCarcass == null)
                {
                    return;
                }

                if (!this.IsValidGrade())
                {
                    SystemMessage.Write(MessageType.Issue, Message.InvalidGrade);
                    NouvemMessageBox.Show(Message.InvalidGrade, touchScreen: true);
                    return;
                }

                if (this.currentCarcass.CarcassSide == StockDetail.Side.Side2)
                {
                    if (ApplicationSettings.ScotBeefEnforceIdentigen &&
                        this.currentCarcass.SupplierID == ApplicationSettings.ScotBeefSupplierID && string.IsNullOrWhiteSpace(this.Identigen))
                    {
                        SystemMessage.Write(MessageType.Issue,  Message.NoIdentigenScanned);
                        NouvemMessageBox.Show(Message.NoIdentigenScanned, touchScreen:true);
                        return;
                    }
                }

                if (ApplicationSettings.DressSpecRequiredAtGrader)
                {
                    if (this.SelectedDressSpec == null)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoDressSpecEntered);
                        NouvemMessageBox.Show(Message.NoDressSpecEntered, touchScreen: true);
                        return;
                    }

                    if (this.SelectedKillingType == null)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoKillTypeEntered);
                        NouvemMessageBox.Show(Message.NoKillTypeEntered, touchScreen: true);
                        return;
                    }

                    if (string.IsNullOrEmpty(this.PrivateKill))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoPrivateKillEntered);
                        NouvemMessageBox.Show(Message.NoPrivateKillEntered, touchScreen: true);
                        return;
                    }
                }

                #endregion

                //this.scalesMotionTimer.Start();
                //return;

                if (!this.manualWeight)
                {
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.Log.LogDebug(this.GetType(), string.Format("Carcass No:{0} - Weight Recorded:{1}", this.nextCarcassNumber, this.indicatorWeight));
                        this.manualWeight = false;

                        var carcassWeighingsDifference = this.CarcassSidesWeightDifferenceTooHigh();
                        if (carcassWeighingsDifference > 0)
                        {
                            var message = string.Format(Message.CarcassWeighingsOutsideOfWeightRange,
                                carcassWeighingsDifference);
                            SystemMessage.Write(MessageType.Issue, message);
                            NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                return;
                            }
                        }
                    }
                    else if (saveWeightResult.Equals(Message.ScalesInMotion) && ApplicationSettings.WaitForMotionAtGrader)
                    {
                        // motion on scales, so move processing to the tick event and wait for scales stability.
                        //this.waitForStabilityTraceabilityData = data;
                        this.scalesMotionTimer.Start();
                        return;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true);
                        return;
                    }
                }
                else
                {
                    var carcassWeighingsDifference = this.CarcassSidesWeightDifferenceTooHigh();
                    if (carcassWeighingsDifference > 0)
                    {
                        var message = string.Format(Message.CarcassWeighingsOutsideOfWeightRange,
                            carcassWeighingsDifference);
                        SystemMessage.Write(MessageType.Issue, message);
                        NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.No)
                        {
                            return;
                        }
                    }
                }

                #region identical weights check

                var localWgt = Math.Round(this.indicatorWeight, 1);
                this.weights.Add(localWgt);
                if (this.weights.Count > 2)
                {
                    if (this.weights.Count > 4)
                    {
                        if (this.weights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsStop,localWgt),touchScreen: true);
                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS STOP --- Carcass no:{0}. 5th identical weight of:{1} recorded. Aborting transaction",
                                    this.NextCarcassNumber, localWgt));
                            this.weights.RemoveAt(0);
                            return;
                        }
                        else
                        {
                            var localWeights = this.weights.Reverse().Take(3);
                            if (localWeights.Distinct().Count() == 1)
                            {
                                NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt),NouvemMessageBoxButtons.YesNo, touchScreen: true);
                                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                                {
                                    this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                    this.weights.RemoveAt(0);
                                    return;
                                }

                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                            }
                        }

                        this.weights.RemoveAt(0);
                    }
                    else
                    {
                        var localWeights = this.weights.Reverse().Take(3);
                        if (localWeights.Distinct().Count() == 1)
                        {
                            NouvemMessageBox.Show(string.Format(Message.ConsecutiveWeightsWarning, localWgt),NouvemMessageBoxButtons.YesNo, touchScreen: true);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. Aborting transaction", this.NextCarcassNumber, localWgt));
                                return;
                            }

                            this.Log.LogDebug(this.GetType(), string.Format("---- IDENTICAL WEIGHTS WARNING --- Carcass no:{0}. 3rd identical weight of:{1}kg recorded. User opting to proceed", this.NextCarcassNumber, localWgt));
                        }
                    }
                }

                #endregion

                this.currentCarcass.Identigen = this.Identigen;
                this.currentCarcass.Generic2 = this.SelectedDestination;
                this.currentCarcass.Tare = this.tare;
                this.currentCarcass.ManualWeight = this.manualWeight;
                this.currentCarcass.TransactionWeight = this.indicatorWeight;
                this.currentCarcass.Grade = this.Grade;
                //if (this.SelectedCategory != null)
                //{
                //    this.Sex = this.DepartmentBodiesManager.FindSex(this.SelectedCategory.CAT);
                //}
                //else
                //{
                //    this.currentCarcass.Sex = this.Sex;
                //}
                
                //this.currentCarcass.Category = this.SelectedCategory;
                this.currentCarcass.RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty;
                this.currentCarcass.Attribute244 = this.SelectedInternalGrade != null ? this.SelectedInternalGrade.Description : string.Empty;
                this.currentCarcass.Attribute245 = this.PrivateKill;
                this.currentCarcass.Attribute246 = this.SelectedDressSpec != null ? this.SelectedDressSpec.Data : string.Empty;
                this.currentCarcass.Attribute247 = this.SelectedKillingType != null ? this.SelectedKillingType.Data : string.Empty;
                //this.currentCarcass.KillNumber = this.NextKillNumber;
                this.currentCarcass.CarcassNumber = this.NextCarcassNumber;

                if (this.currentCarcass.CarcassSide == StockDetail.Side.Side1)
                {
                    this.currentCarcass.WeightSide1 = this.indicatorWeight;
                    this.currentCarcass.Detained = this.currentCarcass.DetainedSide1;
                    this.currentCarcass.Condemned = this.currentCarcass.CondemnedSide1;

                }
                else
                {
                    this.currentCarcass.WeightSide2 = this.indicatorWeight;
                    this.currentCarcass.Detained = this.currentCarcass.DetainedSide2;
                    this.currentCarcass.Condemned = this.currentCarcass.CondemnedSide2;
                }

                if (ApplicationSettings.InvalidateCatBFarmAssurance)
                {
                    if (this.currentCarcass.Category != null && this.currentCarcass.Category.CategoryID == this.categoryB)
                    {
                        this.currentCarcass.FarmAssured = false;
                    }
                }

                if (this.DataManager.AddGraderTransaction(this.currentCarcass))
                {
                    LabelType labelType;
                    if (ApplicationSettings.UseRiskToDeterminePrinterAtGrader)
                    {
                        labelType = this.currentCarcass.RedLabel
                            ? LabelType.Item
                            : LabelType.Box;
                    }
                    else
                    {
                        labelType = this.currentCarcass.AgeInMonths <= ApplicationSettings.GraderLabelUOMAge
                            ? LabelType.Item
                            : LabelType.Box;
                    }

                    int localPrinter = 1;
                    for (int i = 0; i < this.StockLabelsToPrint; i++)
                    {
                        localPrinter = this.Print(this.currentCarcass.LabelID.ToInt(), labelType);
                    }

                    if (ApplicationSettings.PrintingTrailLabelAtGrader)
                    {
                        if (this.SelectedCustomer != null)
                        {
                            this.Print(this.currentCarcass.LabelID.ToInt(), LabelType.Pallet, printer:localPrinter);
                        }
                    }

                    if (ApplicationSettings.PrintFreezerLabelAtGrader)
                    {
                        if (!string.IsNullOrWhiteSpace(this.currentCarcass.FreezerType))
                        {
                            if (!this.currentCarcass.FreezerType.CompareIgnoringCase(Strings.Full))
                            {
                                if (this.currentCarcass.CarcassSide == StockDetail.Side.Side1)
                                {
                                    this.Print(this.currentCarcass.LabelID.ToInt(), LabelType.Shipping, printer: localPrinter);
                                }
                            }
                            else
                            {
                                this.Print(this.currentCarcass.LabelID.ToInt(), LabelType.Shipping, printer: localPrinter);
                            }
                        }
                    }

                    var message = string.Format(Message.CarcassDataRecorded, this.indicatorWeight,
                        this.currentCarcass.CarcassSideNo);
                    SystemMessage.Write(MessageType.Priority, message);
                    NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                }

                if (this.currentCarcass.CarcassSide == StockDetail.Side.Side1 && !this.currentCarcass.GradeWholeBeefAnimal)
                {
                    this.currentCarcass.CarcassSide = StockDetail.Side.Side2;
                    this.ManualGradingModeOn = false;
                    this.GetCarcassDetails();
                }
                else
                {
                    if (this.NextCarcassNumber < this.SelectedDocNumbering.LastNumber)
                    {
                        this.NextCarcassNumber++;
                    }
                    else
                    {
                        this.NextCarcassNumber = 1;
                    }
              
                    this.ManualGradingModeOn = true;
                    this.GetCarcassDetails();
                    this.FindNextCarcass();
                }

                this.SelectedStockDetail = this.StockDetails.FirstOrDefault(x => x.CarcassNumber == this.NextCarcassNumber);
            }
            finally
            {
                this.weighingInProgress = false;
                this.OpenPort();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.CheckForNewEntities(EntityType.BusinessPartner);
                }));
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <param name="labelType">The label type.</param>
        private int Print(int stockTransactionId, LabelType labelType = LabelType.Item, int labelsToPrint = 1, int? partnerId = null, bool weightChanged = false, KillType killType = KillType.Beef, int? printer = null)
        {
            var localPrinter = 1;
            int? customerId = this.currentCarcass == null ? (int?)null : this.currentCarcass.SupplierID;
            if (ApplicationSettings.UseCustomerForGraderLabel)
            {
                customerId = this.SelectedCustomer != null ? this.SelectedCustomer.BPMasterID : (int?)null;
            }
     
            if (partnerId.HasValue)
            {
                customerId = partnerId;
            }

            if (ApplicationSettings.LairageRadioButtonOptionsPartnerId.HasValue && this.currentCarcass != null && this.currentCarcass.SupplierID ==
                ApplicationSettings.LairageRadioButtonOptionsPartnerId)
            {
                if (!string.IsNullOrEmpty(this.currentCarcass.Attribute200))
                {
                    if (this.currentCarcass.Attribute200.CompareIgnoringCase("Welsh"))
                    {
                        customerId = ApplicationSettings.LairageRadioButtonOptionsPartnerId1;
                    }
                    else if (this.currentCarcass.Attribute200.CompareIgnoringCase("Organic"))
                    {
                        customerId = ApplicationSettings.LairageRadioButtonOptionsPartnerId3;
                    }
                    else if (this.currentCarcass.Attribute200.CompareIgnoringCase("SelectFarm"))
                    {
                        customerId = ApplicationSettings.LairageRadioButtonOptionsPartnerId2;
                    }
                    else if (this.currentCarcass.Attribute200.CompareIgnoringCase("Continental"))
                    {
                        customerId = ApplicationSettings.LairageRadioButtonOptionsPartnerId4;
                    }
                }
            }
                
            int? customerGroupId = this.SelectedCustomer == null ? (int?)null : this.SelectedCustomer.BPGroupID;
         
            int? productId = this.BeefIntakeProductID;
            if (killType == KillType.Sheep)
            {
                productId = this.SheepIntakeProductID;
            }

            if (killType == KillType.Pig)
            {
                productId = this.PigIntakeProductID;
            }

            int? productGroupId = null;
            var localProcess = this.Processes.FirstOrDefault(x => x.Name.CompareIgnoringCase(ProcessType.Grader));

            try
            {
                var labels = this.PrintManager.ProcessPrinting(customerId, customerGroupId, productId, productGroupId, ViewType.Grader, labelType, stockTransactionId, labelsToPrint, printer:printer,process:localProcess);
                localPrinter = labels.Item3;

                if (weightChanged)
                {
                    // weight changed, so we refresh queue.
                    this.GetCarcassDetails();
                }

                if (labels.Item1.Any() && labelType != LabelType.Pallet && labelType != LabelType.Shipping)
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(stockTransactionId, string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(ex.Message, touchScreen: true, flashMessage: true);
                }));

                if (weightChanged)
                {
                    // weight changed, so we refresh queue.
                    this.GetCarcassDetails();
                }

                this.Log.LogError(this.GetType(), ex.Message);
            }

            return localPrinter;
        }

        /// <summary>
        /// Sets the grades.
        /// </summary>
        /// <param name="grade">The grade to set.</param>
        private void SetGrades(string grade)
        {
            try
            {
                if (ApplicationSettings.GradeSystem == GradeSystem.FifteenPoint)
                {
                    if (grade.Substring(0, 1) == "E")
                    {
                        this.ConformanceOldEIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "R")
                    {
                        this.ConformanceOldRIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "U")
                    {
                        this.ConformanceOldUPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "O")
                    {
                        this.ConformanceOldOPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "P")
                    {
                        this.ConformanceOldPPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "-U")
                    {
                        this.ConformanceOldMinusUIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "-O")
                    {
                        this.ConformanceOldMinusOIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "-P")
                    {
                        this.ConformanceOldMinusPIsSelected = true;
                    }

                    if (grade.Substring(grade.Length -1 , 1) == "1")
                    {
                        this.FatScoreOld1IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 1, 1) == "2")
                    {
                        this.FatScoreOld2IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 1, 1) == "3")
                    {
                        this.FatScoreOld3IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 2, 2) == "4L")
                    {
                        this.FatScoreOld4LIsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 2, 2) == "4H")
                    {
                        this.FatScoreOld4HIsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 2, 2) == "5L")
                    {
                        this.FatScoreOld5LIsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 2, 2) == "5H")
                    {
                        this.FatScoreOld5HIsSelected = true;
                    }
                }
                else if (ApplicationSettings.GradeSystem == GradeSystem.FivePoint)
                {
                    if (grade.Substring(0, 1) == "E")
                    {
                        this.ConformanceOldEIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "R")
                    {
                        this.ConformanceOldRIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "U")
                    {
                        this.ConformanceOldUPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "O")
                    {
                        this.ConformanceOldOPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 1) == "P")
                    {
                        this.ConformanceOldPPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "O-")
                    {
                        this.ConformanceOldOMinusIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "O+")
                    {
                        this.ConformanceOldOPlusIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "P-")
                    {
                        this.ConformanceOldPMinusIsSelected = true;
                    }
                    else if (grade.Substring(0, 2) == "P+")
                    {
                        this.ConformanceOldPPlusIsSelected = true;
                    }

                    if (grade.Substring(grade.Length - 1, 1) == "1")
                    {
                        this.FatScoreOld1IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 1, 1) == "2")
                    {
                        this.FatScoreOld2IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 1, 1) == "5")
                    {
                        this.FatScoreOld5IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 1, 1) == "3")
                    {
                        this.FatScoreOld3IsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 2, 2) == "4L")
                    {
                        this.FatScoreOld4LIsSelected = true;
                    }
                    else if (grade.Substring(grade.Length - 2, 2) == "4H")
                    {
                        this.FatScoreOld4HIsSelected = true;
                    }
                }
                else
                {
                    var confAlpha = grade.Substring(0, 1);
                    var confNum = grade.Substring(1, 1);
                    var fatAlpha = grade.Substring(2, 1);
                    var fatNum = grade.Substring(3, 1);

                    if (confAlpha == "E")
                    {
                        this.ConformanceEIsSelected = true;
                    }
                    else if (confAlpha == "U")
                    {
                        this.ConformanceUIsSelected = true;
                    }
                    else if (confAlpha == "R")
                    {
                        this.ConformanceRIsSelected = true;
                    }
                    else if (confAlpha == "O")
                    {
                        this.ConformanceOIsSelected = true;
                    }
                    else if (confAlpha == "P")
                    {
                        this.ConformancePIsSelected = true;
                    }

                    if (confNum == "+")
                    {
                        this.ConformancePlusIsSelected = true;
                    }
                    else if (confNum == "=")
                    {
                        this.ConformanceEqualsIsSelected = true;
                    }
                    else if (confNum == "-")
                    {
                        this.ConformanceMinusIsSelected = true;
                    }

                    if (fatAlpha == "1")
                    {
                        this.FatScore1IsSelected = true;
                    }
                    else if (fatAlpha == "2")
                    {
                        this.FatScore2IsSelected = true;
                    }
                    else if (fatAlpha == "3")
                    {
                        this.FatScore3IsSelected = true;
                    }
                    else if (fatAlpha == "4")
                    {
                        this.FatScore4IsSelected = true;
                    }
                    else if (fatAlpha == "5")
                    {
                        this.FatScore5IsSelected = true;
                    }

                    if (fatNum == "+")
                    {
                        this.FatScorePlusIsSelected = true;
                    }
                    else if (fatNum == "=")
                    {
                        this.FatScoreEqualsIsSelected = true;
                    }
                    else if (fatNum == "-")
                    {
                        this.FatScoreMinusIsSelected = true;
                    }
                }

                this.ManualGradingModeOn = false;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Gets the percentage difference between the carcass weighings.
        /// </summary>
        /// <returns>The , if the weighings are outside of the normal range.</returns>
        private decimal CarcassSidesWeightDifferenceTooHigh()
        {
            if (ApplicationSettings.CarcassSideWeightDifference == 0)
            {
                return 0;
            }

            if (this.currentCarcass.CarcassSide == StockDetail.Side.Side1)
            {
                return 0;
            }

            // weighing second side
            var side1Wgt = this.currentCarcass.WeightSide1;
            var side2Wgt = this.indicatorWeight;
            if (side1Wgt <= 0)
            {
                return 0;
            }

            var difference = Math.Abs((side1Wgt - side2Wgt) / Math.Abs(side1Wgt) * 100);

            if (difference > ApplicationSettings.CarcassSideWeightDifference)
            {
                // the weighing percentage difference is greater than the norm, so return it.
                return Math.Round(difference, 2);
            }

            return 0;
        }

        /// <summary>
        /// Sets the grades to display.
        /// </summary>
        private void SetGradeDisplay()
        {
            if (ApplicationSettings.GradeSystem == GradeSystem.EUROP)
            {
                this.GradeDisplayViewModel = this.Locator.Grade;
                this.GradeSystemName = Strings.EUROP;
            }
            else if (ApplicationSettings.GradeSystem == GradeSystem.FivePoint)
            {
                this.GradeDisplayViewModel = this.Locator.GradeFivePoint;
                this.GradeSystemName = Strings.FivePoint;
            }
            else
            {
                this.GradeDisplayViewModel = this.Locator.GradeHistoric;
                this.GradeSystemName = Strings.FifteenPoint;
            }
        }

        /// <summary>
        /// Sets up the grader refresh
        /// </summary>
        private void SetUpRefreshTimer()
        {
            this.refreshTimer = new DispatcherTimer();
            var localTime = ApplicationSettings.GraderRefreshTime == 0 ? 20 : ApplicationSettings.GraderRefreshTime;
            this.refreshTimer.Interval = TimeSpan.FromSeconds(localTime);
            this.refreshTimer.Tick += this.RefreshTimerOnTick;
        }

        private void RefreshTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (ApplicationSettings.GraderMode == GraderMode.Pig)
            {
                return;
            }

            if (this.NoGradeEntered && this.currentCarcass == null && this.Locator.Indicator.Weight < 5)
            {
                // no carcass on the scales, so refresh.
                this.GetCarcassDetails();
                this.FindNextCarcass();
            }
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimers()
        {
            this.scalesMotionTimer.Interval = TimeSpan.FromMilliseconds(5);
            this.scalesMotionTimer.Tick += this.ScalesMotionTimerOnTick;
        }

        private void ScalesMotionTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (this.scalesStabilityWaitTicks == ApplicationSettings.WaitForScalesToStable)
            {
                this.scalesStabilityWaitTicks = 0;
                this.scalesMotionTimer.Stop();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.ScalesInMotion, touchScreen: true);
                }));

                return;
            }

            if (!this.Locator.Indicator.ScalesInMotion)
            {
                this.scalesMotionTimer.Stop();
                this.scalesStabilityWaitTicks = 0;

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.RecordWeight();
                }));

                return;
            }
            else
            {
                this.scalesStabilityWaitTicks++;
            }
        }

        /// <summary>
        /// Gets the next kill number.
        /// </summary>
        private void GetNextKillNumber()
        {
            if (ApplicationSettings.GraderMode == GraderMode.Beef)
            {
                this.NextKillNumber = this.DataManager.GetNextKillNumber(false);
                return;
            }

            if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                this.NextKillNumber = this.DataManager.GetNextKillNumberSheep(false);
                return;
            }

            if (ApplicationSettings.GraderMode == GraderMode.Pig)
            {
                this.NextKillNumber = this.DataManager.GetNextKillNumberPig(false);
                return;
            }
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        private int UpdateKillNumber()
        {
            if (ApplicationSettings.GraderMode == GraderMode.Beef)
            {
                return this.DataManager.UpdateKillNumber();
            }

            if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                return this.DataManager.UpdateKillNumberSheep();
            }

            return this.DataManager.UpdateKillNumberPig();
        }

        /// <summary>
        /// Handles a selected partner.
        /// </summary>
        /// <param name="s">The selected partner.</param>
        private void HandleSelectedSupplier(ViewBusinessPartner s)
        {
            if (ApplicationSettings.GraderMode == GraderMode.Pig)
            {
                var id = this.Locator.LairageGraderCombo.CreatePigIntakeFromGrader(s);
                if (id > 0)
                {
                    var intake = this.DataManager.GetTouchscreenReceiptById(id);
                    this.HandleSelectedOrder(Tuple.Create(intake, s));
                    this.KillCount = 0;
                }

                return;
            }

            if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                this.SelectedPartner = s;
                var items = new List<CollectionData>();
                foreach (var item in this.Categories.Where(x => x.CatType.CompareIgnoringCase(Constant.Sheep)))
                {
                    items.Add(new CollectionData { Data = string.Format("{0} - {1}", item.CAT, item.Name), ID = item.CategoryID, Identifier = Constant.Sheep });
                }

                // Send our combo collection to the display module.
                Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }
        }

        private void CreateSheepIntake()
        {
            var id = this.Locator.LairageGraderCombo.CreateSheepIntakeFromGrader(this.SelectedPartner, this.SelectedCategory);
            if (id > 0)
            {
                var intake = this.DataManager.GetTouchscreenReceiptById(id);
                this.HandleSelectedSheepOrder(Tuple.Create(intake, this.SelectedPartner));
                this.KillCount = 0;
            }
        }

        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        private void HandleSelectedOrder(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            if (ApplicationSettings.GraderMode == GraderMode.Pig)
            {
                this.HandleSelectedPigOrder(orderDetails);
                return;
            }

            if (ApplicationSettings.GraderMode == GraderMode.Sheep)
            {
                this.HandleSelectedSheepOrder(orderDetails);
                return;
            }
        }

        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        private void HandleSelectedPigOrder(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            var order = orderDetails.Item1;
            this.LotNumber = order.Reference ?? string.Empty;
            if (order.SaleDetails.IsNullOrEmpty())
            {
                return;
            }

            this.CurrentIntake = order;
            this.GetNextPigCarcass();
            this.KillCount = this.CurrentIntake.SaleDetails.First().TransactionCount;
            this.CarcassSupplier = orderDetails.Item2.Name;
        }

        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        private void HandleSelectedSheepOrder(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            var order = orderDetails.Item1;
            this.LotNumber = order.Reference ?? string.Empty;
            this.CurrentIntake = order;
            this.GetNextSheepCarcass();
            if (this.CurrentIntake.SaleDetails.IsNullOrEmpty())
            {
                return;
            }

            this.KillCount = this.CurrentIntake.SaleDetails.First().TransactionCount;
            this.CarcassSupplier = orderDetails.Item2.Name;
        }

        /// <summary>
        /// Retrieves the next pig in the lot for processing.
        /// </summary>
        private void GetNextPigCarcass()
        {
            this.GetCarcassDetails();
            if (this.CurrentIntake == null || this.CurrentIntake.SaleDetails == null)
            {
                this.CarcassSupplier = Strings.NoSupplier;
                this.LotNumber = string.Empty;
                this.PigCount = 0;
                return;
            }

            this.currentCarcass = this.DataManager.GetNextPigCarcassByIntakeId(this.CurrentIntake.SaleDetails.First().SaleDetailID);
            this.SelectedCustomer = this.Customers.FirstOrDefault(x => x.BPMasterID == this.currentCarcass.CustomerID);
            this.RemainingCount = this.currentCarcass.Count;
            this.CarcassSupplier = this.currentCarcass.Supplier;
            this.StockDetails.Insert(0, this.currentCarcass);
            this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the next pig in the lot for processing.
        /// </summary>
        private void GetNextSheepCarcass()
        {
            this.GetCarcassDetails();
            this.currentCarcass = this.DataManager.GetNextSheepCarcassByIntakeId(this.CurrentIntake.SaleDetails.First().SaleDetailID);
            this.SelectedCustomer = this.Customers.FirstOrDefault(x => x.BPMasterID == this.currentCarcass.CustomerID);
            this.RemainingCount = this.currentCarcass.Count;
            this.CarcassSupplier = this.currentCarcass.Supplier;
            this.StockDetails.Insert(0, this.currentCarcass);
            this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
        }

        /// <summary>
        /// Handles the ui toggling.
        /// </summary>
        private void HandleToggle()
        {
            try
            {
                // beef only
                if (ApplicationSettings.AllowGraderBeefToggle && !ApplicationSettings.AllowGraderSheepToggle &&
                    !ApplicationSettings.AllowGraderPigToggle)
                {
                    if (ApplicationSettings.GraderMode != GraderMode.Beef)
                    {
                        this.BeefSheepContent = Strings.Beef;
                        ApplicationSettings.GraderMode = GraderMode.Beef;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.FindNextCarcass();
                    }

                    return;
                }

                // sheep only
                if (!ApplicationSettings.AllowGraderBeefToggle && ApplicationSettings.AllowGraderSheepToggle &&
                    !ApplicationSettings.AllowGraderPigToggle)
                {
                    if (ApplicationSettings.GraderMode != GraderMode.Sheep)
                    {
                        this.BeefSheepContent = Strings.Sheep;
                        ApplicationSettings.GraderMode = GraderMode.Sheep;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.FindNextCarcass();
                    }

                    return;
                }

                // pig only
                if (!ApplicationSettings.AllowGraderBeefToggle && !ApplicationSettings.AllowGraderSheepToggle &&
                    ApplicationSettings.AllowGraderPigToggle)
                {
                    if (ApplicationSettings.GraderMode != GraderMode.Pig)
                    {
                        this.GradeDisplayViewModel = this.Locator.Pig;
                        this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPigPanel;
                        ApplicationSettings.GraderMode = GraderMode.Pig;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.GetNextQueuedHerd();
                    }

                    return;
                }

                // beef/sheep/pig
                if ((ApplicationSettings.AllowGraderBeefToggle && ApplicationSettings.AllowGraderSheepToggle &&
                     ApplicationSettings.AllowGraderPigToggle)
                    ||
                    (!ApplicationSettings.AllowGraderBeefToggle && !ApplicationSettings.AllowGraderSheepToggle &&
                     !ApplicationSettings.AllowGraderPigToggle))
                {
                    if (ApplicationSettings.GraderMode == GraderMode.Beef)
                    {
                        this.BeefSheepContent = Strings.Sheep;
                        ApplicationSettings.GraderMode = GraderMode.Sheep;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.FindNextCarcass();
                    }
                    else if (ApplicationSettings.GraderMode == GraderMode.Sheep)
                    {
                        this.GradeDisplayViewModel = this.Locator.Pig;
                        this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPigPanel;
                        ApplicationSettings.GraderMode = GraderMode.Pig;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.GetNextQueuedHerd();
                    }
                    else
                    {
                        this.BeefSheepContent = Strings.Beef;
                        ApplicationSettings.GraderMode = GraderMode.Beef;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.FindNextCarcass();
                    }

                    return;
                }

                // beef/sheep
                if (ApplicationSettings.AllowGraderBeefToggle && ApplicationSettings.AllowGraderSheepToggle && !ApplicationSettings.AllowGraderPigToggle)
                {
                    if (ApplicationSettings.GraderMode == GraderMode.Beef)
                    {
                        this.BeefSheepContent = Strings.Sheep;
                        ApplicationSettings.GraderMode = GraderMode.Sheep;
                    }
                    else
                    {
                        this.BeefSheepContent = Strings.Beef;
                        ApplicationSettings.GraderMode = GraderMode.Beef;
                    }

                    this.GetLocalDocNumberings();
                    this.GetNextKillNumber();
                    this.GetCarcassDetails();
                    this.FindNextCarcass();
                    this.SetTare();
                    return;
                }

                // beef/pig
                if (ApplicationSettings.AllowGraderBeefToggle && !ApplicationSettings.AllowGraderSheepToggle && ApplicationSettings.AllowGraderPigToggle)
                {
                    if (ApplicationSettings.GraderMode == GraderMode.Beef)
                    {
                        this.GradeDisplayViewModel = this.Locator.Pig;
                        this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPigPanel;
                        ApplicationSettings.GraderMode = GraderMode.Pig;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.GetNextQueuedHerd();
                    }
                    else
                    {
                        this.BeefSheepContent = Strings.Beef;
                        ApplicationSettings.GraderMode = GraderMode.Beef;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.FindNextCarcass();
                        this.SetTare();
                    }

                    return;
                }

                // sheep/pig
                if (!ApplicationSettings.AllowGraderBeefToggle && ApplicationSettings.AllowGraderSheepToggle && ApplicationSettings.AllowGraderPigToggle)
                {
                    if (ApplicationSettings.GraderMode == GraderMode.Sheep)
                    {
                        this.GradeDisplayViewModel = this.Locator.Pig;
                        this.Locator.Touchscreen.PanelViewModel = this.Locator.GraderPigPanel;
                        ApplicationSettings.GraderMode = GraderMode.Pig;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.SetTare();
                        this.GetNextQueuedHerd();
                    }
                    else
                    {
                        this.BeefSheepContent = Strings.Sheep;
                        ApplicationSettings.GraderMode = GraderMode.Sheep;
                        this.GetLocalDocNumberings();
                        this.GetNextKillNumber();
                        this.GetCarcassDetails();
                        this.FindNextCarcass();
                        this.SetTare();
                    }

                    return;
                }
            }
            finally
            {
                this.SetUiLabel();
            }
        }

        /// <summary>
        /// Shows the orders button in combo sheep mode.
        /// </summary>
        private void SetUiLabel()
        {
            if (!this.LairageGraderCombo || ApplicationSettings.GraderMode != GraderMode.Sheep)
            {
                Messenger.Default.Send(true, Token.GraderPanelLabel);
                return;
            }

            Messenger.Default.Send(false, Token.GraderPanelLabel);
        }

        #endregion

        #endregion
    }
}



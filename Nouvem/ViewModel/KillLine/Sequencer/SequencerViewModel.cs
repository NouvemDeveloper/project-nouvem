﻿// -----------------------------------------------------------------------
// <copyright file="LairageOrderViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.KillLine.Sequencer
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class SequencerViewModel : KillLineViewModelBase
    {
        #region field

        /// <summary>
        /// The refresh timer.
        /// </summary>
        private DispatcherTimer refreshTimer = new DispatcherTimer();

        /// <summary>
        /// Line re-ordering enabled flag.
        /// </summary>
        private bool enableLineReordering;

        private int labelsToPrint;

        /// <summary>
        /// The user entered eartag.
        /// </summary>
        private string enteredEartag;

        /// <summary>
        /// The user entered eartag.
        /// </summary>
        private string enteredHerdNumber;

        /// <summary>
        /// The next carcass number.
        /// </summary>
        private int nextCarcassNumber;

        /// <summary>
        /// The next kill number.
        /// </summary>
        private int nextKillNumber;

        /// <summary>
        /// The next kill number.
        /// </summary>
        private bool completeAddToQueue;

        /// <summary>
        /// The next kill number.
        /// </summary>
        private bool completeFarmAssured;

        /// <summary>
        /// Age band 1 selection.
        /// </summary>
        private bool ageBand1Selected;

        /// <summary>
        /// Age band 2 selection.
        /// </summary>
        private bool ageBand2Selected;

        /// <summary>
        /// Age band 3 selection.
        /// </summary>
        private bool ageBand3Selected;

        /// <summary>
        /// Age band 4 selection.
        /// </summary>
        private bool ageBand4Selected;

        /// <summary>
        /// Age band 4 selection.
        /// </summary>
        private bool isInvalidCategory;

        /// <summary>
        /// The selected band.
        /// </summary>
        private AgeBand selectedAgeBand;

        /// <summary>
        /// The age bands.
        /// </summary>
        private IList<AgeBand> ageBands;

        /// <summary>
        /// The ui top view.
        /// </summary>
        private ViewModelBase topViewModel;

        /// <summary>
        /// The selected scanned stock details.
        /// </summary>
        private StockDetail sheepLotDetails;

        /// <summary>
        /// The selected scanned stock details.
        /// </summary>
        private StockDetail selectedScannedEartagStockDetail;

        /// <summary>
        /// The scanner handheld categories.
        /// </summary>
        private ObservableCollection<CollectionData> scannerCategories = new ObservableCollection<CollectionData>();

        /// <summary>
        /// The scanner handheld categories.
        /// </summary>
        private CollectionData selectedScannerCategory;

        /// <summary>
        /// The scanned stock details.
        /// </summary>
        private ObservableCollection<StockDetail> scannedEartagStockDetails = new ObservableCollection<StockDetail>();

        private bool categorySelected;
        private string variableLabel;
        private string addToQueueText;

        private bool isScannerCategoryLoaded;

        /// <summary>
        /// Flag, as to whether a herd number is being entered as opposed to an eartag.
        /// </summary>
        protected bool enteringHerdNo;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SequencerViewModel"/> class.
        /// </summary>
        public SequencerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Wand, s =>
            {
                this.HandleWandData(s);
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.Sequencer, s =>
            {
                try
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        this.HandleScannerData(s);
                    }
                }
                finally
                {
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                this.HandleScannerData(s);
            });

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowSequencerAttributes = b);

            Messenger.Default.Register<string>(this, Token.TouchscreenKeyPadEntry, s =>
            {
                this.EnableReorderLine = false;
                this.HandleKeyPadEntry(s);
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals(Constant.TraceabilityAttribute) && c.ID > 0)
                {
                    try
                    {
                        this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == c.ID);
                        Messenger.Default.Send(Token.Message, Token.FocusToButton);

                        if (this.SelectedCategory != null && !this.isInvalidCategory)
                        {
                            if (this.ScannedEartagStockDetails != null && this.ScannedEartagStockDetails.Any())
                            {
                                this.Log.LogInfo(this.GetType(), $"{this.ScannedEartagStockDetails.First().Eartag} change category to {this.SelectedCategory.CategoryID}");
                                this.ScannedEartagStockDetails.First().LogCategoryChange = true;
                            }
                        }

                        if (this.SelectedCategory != null && this.completeAddToQueue && !this.isInvalidCategory)
                        {
                            if (this.ScannedEartagStockDetails != null && this.ScannedEartagStockDetails.Any())
                            {
                                this.Log.LogInfo(this.GetType(), $"{this.ScannedEartagStockDetails.First().Eartag} change category to {this.SelectedCategory.CategoryID}");
                                this.ScannedEartagStockDetails.First().LogCategoryChange = true;
                            }

                            this.categorySelected = true;
                            this.AddToQueueCommandExecute();
                        }
                    }
                    finally
                    {
                        this.completeAddToQueue = false;
                        this.isInvalidCategory = false;
                    }

                    return;
                }

                if (c.Identifier.Equals(Constant.FarmAssured))
                {
                    this.FarmAssured = c.Data.CompareIgnoringCaseAndWhitespace(Strings.FAYes)
                        ? Constant.Yes
                        : Constant.No;
                    this.AddToQueueCommandExecute();
                    return;
                }

                if (c.Identifier.Equals(Constant.TouchscreenTraceability))
                {
                    this.Sex = c.Data;
                    return;
                }

                if (c.Identifier.Equals(Constant.FA))
                {
                    this.FarmAssured = c.Data;
                    return;
                }

                if (c.Identifier.Equals(Constant.Breed))
                {
                    this.Breed = this.Breeds.FirstOrDefault(x => x.NouBreedID == c.ID);
                    Messenger.Default.Send(Token.Message, Token.FocusToButton);
                }

                if (c.Identifier.Equals(Constant.Origin))
                {
                    this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.CountryID == c.ID);
                    return;
                }
            });

            #endregion

            #region command handler

            this.ConnectToWandCommand = new RelayCommand(() =>
            {
                this.OpenWand();
                if (this.HardwareManager.IsWandPortOpen())
                {
                    SystemMessage.Write(MessageType.Priority, "Wand Connected");
                }
            });

            this.RefreshQueueCommand = new RelayCommand(() =>
            {
                this.RefreshQueue();
                SystemMessage.Write(MessageType.Priority, Message.QueueRefreshed);
            });

            this.OnScannerCategoryLoadingCommand = new RelayCommand(() =>
            {
                this.SelectedScannerCategory = null;
                this.isScannerCategoryLoaded = true;
            });

            this.MoveBackCategoryCommand = new RelayCommand(this.CloseScannerCategory);

            this.MoveBackCommand = new RelayCommand<string>(s =>
            {
                if (s.Equals(Strings.Menu))
                {
                    Messenger.Default.Send(Token.Message, Token.CloseWindow);
                    Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                    Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
                    ViewModelLocator.ClearSequencer();
                    return;
                }

                // back to main
                Messenger.Default.Send(Token.Message, Token.CloseWindow);
            });

            this.ManualScanCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.Sequencer, true));

            this.SwapViewCommand = new RelayCommand(() =>
            {
                if (ApplicationSettings.UsingHideStationAtSequencer)
                {
                    ApplicationSettings.HidePullTerminalAtSequencer = !ApplicationSettings.HidePullTerminalAtSequencer;
                    this.AddToQueueText = ApplicationSettings.HidePullTerminalAtSequencer ? Strings.Print : Strings.AddToQueue;
                    this.SelectedScannedEartagStockDetail = null;
                    this.ScannedEartagStockDetails?.Clear();
                    this.GetLocalDocNumberings();
                    this.GetCurrentSequencedCarcasses();
                    this.EnteredHerdNumber = string.Empty;
                    this.EnteredEartag = string.Empty;
                    var msg =  ApplicationSettings.HidePullTerminalAtSequencer ? Token.FocusToHerdNo : Token.FocusToEartag;
                    Messenger.Default.Send(Token.Message, msg);
                    return;
                }

                if (this.TopViewModel == this.Locator.BeefTop)
                {
                    this.TopViewModel = this.Locator.SheepTop;
                    this.Locator.Touchscreen.PanelViewModel = this.Locator.SequencerSheepPanel;
                    ApplicationSettings.SequencerMode = SequencerMode.Sheep;
                    this.GetNextQueuedHerd();
                }
                else
                {
                    this.TopViewModel = this.Locator.BeefTop;
                    this.Locator.Touchscreen.PanelViewModel = this.Locator.SequencerPanel;
                    ApplicationSettings.SequencerMode = SequencerMode.Beef;
                }

                this.GetLocalDocNumberings();
                this.GetCurrentSequencedCarcasses();

            }, () => ApplicationSettings.AllowSequencerViewChange);

            // Handler to call up the order screen.
            this.SwitchViewCommand = new RelayCommand(this.SwitchViewCommandExecute);

            // Handler to search for a sale using the user input search text.
            this.AddToQueueCommand = new RelayCommand(this.AddToQueueCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(this.RemoveItemCommandExecute);

            // Command to handle a missing carcass tag.
            this.MissingTagCommand = new RelayCommand(this.MissingTagCommandExecute);

            // Command to handle a missing carcass tag.
            this.ScrollCommand = new RelayCommand<string>(this.ScrollCommandExecute);

            // Register for the incoming supplier order.
            Messenger.Default.Register<Tuple<Sale, ViewBusinessPartner>>(this, Token.SaleSelected, this.HandleSelectedOrder);

            // Command to handle the manual keyboard eartag enter/tab event.
            this.EartagEnteredCommand = new RelayCommand<string>(s =>
            {
                if (!ViewModelLocator.SequencerStatic.LairageSequencerCombo)
                {
                    this.HandleKeyPadEntry("Enter");
                }
                else
                {
                    Messenger.Default.Send(this.enteredEartag, Token.ManualTagEnteredAtSequencer);
                }
            });

            this.OnGotFocusCommand = new RelayCommand<string>(s =>
            {
                if (s.CompareIgnoringCase("Eartag"))
                {
                    this.enteringHerdNo = false;
                }
                else
                {
                    this.enteringHerdNo = true;
                }
            });

            #endregion

            this.GetProcesses();
            this.GetCategories();
            this.GetAgeBands();
            this.GetFAValues();
            this.GetBreeds();
            this.GetSexes();
            this.GetCountries();
            this.CloseHiddenWindows();
            this.SetUpTimer();

            if (ApplicationSettings.SequencerMode == SequencerMode.Sheep)
            {
                this.TopViewModel = this.Locator.SheepTop;
                this.Locator.Touchscreen.PanelViewModel = this.Locator.SequencerSheepPanel;
                this.GetNextQueuedHerd();
            }
            else
            {
                this.TopViewModel = this.Locator.BeefTop;
                this.Locator.Touchscreen.PanelViewModel = this.Locator.SequencerPanel;
            }
     
            this.GetLocalDocNumberings();
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowSequencerAttributes;
            this.Locator.Touchscreen.CornerViewModel = this.Locator.SequencerMainData;
            this.GetNextKillNumber();
            this.GetCurrentSequencedCarcasses();
            this.CanReorderLine = this.AuthorisationsManager.AllowUserToReOrderCarcassesAtSequencer;
            Messenger.Default.Send(Token.Message, Token.FocusToEartag);
            this.SetUpRefreshTimer();
            this.VariableLabel = ApplicationSettings.SearchByEartagAndKillNoAtSequencer ? Strings.AnimalKillNo : string.Empty;
            this.AddToQueueText = ApplicationSettings.HidePullTerminalAtSequencer ? Strings.Print:Strings.AddToQueue;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the age band 2 text.
        /// </summary>
        public string ScanIndicator { get; set; } = "N";

        /// <summary>
        /// Gets or sets a value indicating whether we are in lairage\sequencer combo mode..
        /// </summary>
        public bool LairageSequencerCombo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the scanning is to be disabled.
        /// </summary>
        public bool DisableScannning { get; set; }

        /// <summary>
        /// Gets or sets the top vm.
        /// </summary>
        public ViewModelBase TopViewModel
        {
            get
            {
                return this.topViewModel;
            }

            set
            {
                this.topViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the top vm.
        /// </summary>
        public string VariableLabel
        {
            get
            {
                return this.variableLabel;
            }

            set
            {
                this.variableLabel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the top vm.
        /// </summary>
        public string AddToQueueText
        {
            get
            {
                return this.addToQueueText;
            }

            set
            {
                this.addToQueueText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scanner categories.
        /// </summary>
        public CollectionData SelectedScannerCategory
        {
            get
            {
                return this.selectedScannerCategory;
            }

            set
            {
                this.selectedScannerCategory = value;
                this.RaisePropertyChanged();
                if (this.isScannerCategoryLoaded && value != null)
                {
                    try
                    {
                        this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == value.ID);
                        Messenger.Default.Send(Token.Message, Token.FocusToButton);

                        if (this.SelectedCategory != null && this.completeAddToQueue && !this.isInvalidCategory)
                        {
                            this.categorySelected = true;
                            this.AddToQueueCommandExecute();
                        }
                    }
                    finally
                    {
                        this.completeAddToQueue = false;
                        this.isInvalidCategory = false;
                        this.CloseScannerCategory();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the scanner categories.
        /// </summary>
        public ObservableCollection<CollectionData> ScannerCategories
        {
            get
            {
                return this.scannerCategories;
            }

            set
            {
                this.scannerCategories = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the logged in user can re-order the line.
        /// </summary>
        public StockDetail SelectedScannedEartagStockDetail
        {
            get
            {
                return this.selectedScannedEartagStockDetail;
            }

            set
            {
                this.selectedScannedEartagStockDetail = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the logged in user can re-order the line.
        /// </summary>
        public bool EnableReorderLine
        {
            get
            {
                return this.enableLineReordering;
            }

            set
            {
                this.enableLineReordering = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the logged in user can re-order the line.
        /// </summary>
        public bool CanReorderLine { get; set; }

        /// <summary>
        /// Gets or sets the age band 1 text.
        /// </summary>
        public string AgeBand1Text { get; set; }

        /// <summary>
        /// Gets or sets the age band 2 text.
        /// </summary>
        public string AgeBand2Text { get; set; }

        /// <summary>
        /// Gets or sets the age band 3 text.
        /// </summary>
        public string AgeBand3Text { get; set; }

        /// <summary>
        /// Gets or sets the age band 4 text.
        /// </summary>
        public string AgeBand4Text { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether age band 1 is selected.
        /// </summary>
        public bool AgeBand1Selected
        {
            get
            {
                return this.ageBand1Selected;
            }

            set
            {
                this.ageBand1Selected = value;
                this.RaisePropertyChanged();
                if (this.ageBands.Any())
                {
                    this.selectedAgeBand = this.ageBands.First();
                }

                if (value)
                {
                    this.AgeBand2Selected = false;
                    this.AgeBand3Selected = false;
                    this.AgeBand4Selected = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether age band 2 is selected.
        /// </summary>
        public bool AgeBand2Selected
        {
            get
            {
                return this.ageBand2Selected;
            }

            set
            {
                this.ageBand2Selected = value;
                this.RaisePropertyChanged();
                if (this.ageBands.Count > 1)
                {
                    this.selectedAgeBand = this.ageBands.ElementAt(1);
                }

                if (value)
                {
                    this.AgeBand1Selected = false;
                    this.AgeBand3Selected = false;
                    this.AgeBand4Selected = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether age band 3 is selected.
        /// </summary>
        public bool AgeBand3Selected
        {
            get
            {
                return this.ageBand3Selected;
            }

            set
            {
                this.ageBand3Selected = value;
                this.RaisePropertyChanged();
                if (this.ageBands.Count > 2)
                {
                    this.selectedAgeBand = this.ageBands.ElementAt(2);
                }

                if (value)
                {
                    this.AgeBand1Selected = false;
                    this.AgeBand2Selected = false;
                    this.AgeBand4Selected = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether age band 4 is selected.
        /// </summary>
        public bool AgeBand4Selected
        {
            get
            {
                return this.ageBand4Selected;
            }

            set
            {
                this.ageBand4Selected = value;
                this.RaisePropertyChanged();
                if (this.ageBands.Count > 3)
                {
                    this.selectedAgeBand = this.ageBands.ElementAt(3);
                }

                if (value)
                {
                    this.AgeBand1Selected = false;
                    this.AgeBand3Selected = false;
                    this.AgeBand2Selected = false;
                }
            }
        }

        /// <summary>
        /// The scanned passport details.
        /// </summary>
        public ObservableCollection<StockDetail> ScannedEartagStockDetails
        {
            get
            {
                return this.scannedEartagStockDetails;
            }

            set
            {
                this.scannedEartagStockDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next carcass no.
        /// </summary>
        public int NextCarcassNumber
        {
            get
            {
                return this.nextCarcassNumber;
            }

            set
            {
                this.nextCarcassNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next kill number.
        /// </summary>
        public int NextKillNumber
        {
            get
            {
                return this.nextKillNumber;
            }

            set
            {
                this.nextKillNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the entered eartag.
        /// </summary>
        public string EnteredEartag
        {
            get
            {
                return this.enteredEartag;
            }

            set
            {
                this.enteredEartag = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the entered herd number.
        /// </summary>
        public string EnteredHerdNumber
        {
            get
            {
                return this.enteredHerdNumber;
            }

            set
            {
                this.enteredHerdNumber = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand ConnectToWandCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand RefreshQueueCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand OnScannerCategoryLoadingCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand MoveBackCategoryCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand MoveBackCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; set; }

        /// <summary>
        /// Gets the command to swap the grade view.
        /// </summary>
        public ICommand SwapViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to move carcasses up and down the line.
        /// </summary>
        public ICommand OnGotFocusCommand { get; private set; }

        /// <summary>
        /// Gets the command to move carcasses up and down the line.
        /// </summary>
        public ICommand ScrollCommand { get; private set; }

        /// <summary>
        /// Gets the command to add a missing tag to the carcass.
        /// </summary>
        public ICommand MissingTagCommand { get; private set; }

        /// <summary>
        /// Gets the command to add a carcass to the line.
        /// </summary>
        public ICommand AddToQueueCommand { get; private set; }

        #endregion

        #region method

        public void HandleEartagInput(string eartag)
        {
            this.EnteredEartag = eartag;
            this.HandleEartagInput();
            this.EnteredEartag = string.Empty;
            ViewModelLocator.SequencerStatic.EnteredEartag = string.Empty;
        }

        /// <summary>
        /// Handler for adding a carcass to the queue.
        /// </summary>
        public void AddSheepToQueue(int id)
        {
            var carcass = this.DataManager.GetCarcassById(id);
            var localKillNo = this.DataManager.UpdateKillNumberSheep();
            var docNumber = this.DataManager.SetDocumentNumber(this.SelectedSheepDocNumbering.DocumentNumberingID);
            var localcarcassNo = docNumber.CurrentNumber;

            carcass.KillNumber = localKillNo;
            carcass.CarcassNumber = localcarcassNo;
            this.DataManager.UpdateStockAttribute(carcass);

            this.NextKillNumber = localKillNo + 1;
            this.NextCarcassNumber = docNumber.NextNumber;
        }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Switch the view.
        /// </summary>
        protected virtual void SwitchViewCommandExecute()
        {
            this.Locator.TouchscreenOrders.SetModule(ViewType.Sequencer);
            this.Locator.TouchscreenOrders.GetAllOrders();
            if (!this.OrdersScreenCreated)
            {
                this.OrdersScreenCreated = true;
                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                return;
            }

            Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
            this.Locator.TouchscreenOrders.OnEntry();
        }

        /// <summary>
        /// Gets the next herd queued.
        /// </summary>
        protected override void GetNextQueuedHerd()
        {
            if (!ApplicationSettings.UsingKillLineQueueNumbers)
            {
                return;
            }

            var nextInQueue = this.DataManager.GetTouchscreenReceiptByNextQueueNo(Constant.Sheep);
            if (nextInQueue == null)
            {
                return;
            }

            this.CurrentIntake = nextInQueue;
            this.Supplier = this.CurrentIntake.Supplier;
            this.LotNumber = this.CurrentIntake.Reference ?? string.Empty;
            var carcass = this.DataManager.GetNextCarcassByIntakeId(this.CurrentIntake.SaleDetails.First().SaleDetailID);
            this.RemainingCount = carcass.Count;
            Messenger.Default.Send(Token.Message, Token.FocusToEartag);
        }

        /// <summary>
        /// Handle the selected stock detail parse/edit.
        /// </summary>
        /// <param name="detail">The detail to parse for edit.</param>
        protected override void HandleSelectedStockDetail(StockDetail detail)
        {
            this.EntitySelectionChange = true;
            try
            {
                this.Eartag = detail.Eartag;
                this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == detail.CategoryID);
                this.Breed = this.Breeds.FirstOrDefault(x => x.NouBreedID == detail.BreedID);
                this.FarmAssured = this.YesNoValues.FirstOrDefault(x => x.Equals(detail.FarmAssuredYesNo));
                this.Sex = this.Sexes.FirstOrDefault(x => x == detail.Sex);
                this.DOB = detail.DOB;
            }
            finally
            {
                this.EntitySelectionChange = false;
            }
        }

        /// <summary>
        /// Gets the animal sexes.
        /// </summary>
        protected override void GetCategories()
        {
            this.Categories = new ObservableCollection<NouCategory>(this.DataManager.GetCategories());
        }

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected override void ScannerModeHandler()
        {
            this.ScannerStockMode = ScannerMode.Scan;
        }

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="category">The scanned passport data barcode.</param>
        protected override void HandleCategorySelection(NouCategory category)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            var previousCat = this.PreviousCategory;
            if (category.CAT == "A" && this.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
            {
                this.isInvalidCategory = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    var message = string.Format(Message.InvalidCategoryAge, ApplicationSettings.YoungBullMaxAge);
                    SystemMessage.Write(MessageType.Issue, message);

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(message, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(message, touchScreen: true);
                    }

                    this.IgnoreCategoryCheck = true;
                    this.SelectedCategory = previousCat;
                    this.IgnoreCategoryCheck = false;
                }));
            }

            var localSex = this.DepartmentBodiesManager.FindSex(category.CAT);
            if (string.IsNullOrEmpty(this.Sex))
            {
                this.Sex = localSex;
            }
            //else
            //{
            //    var isValidSex = this.DepartmentBodiesManager.IsValidSex(category.CAT, this.Sex);
            //    if (!isValidSex)
            //    {
            //        this.isInvalidCategory = true;
            //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //        {
            //            var message = string.Format(Message.InvalidCategorySexMatch, category.CAT, this.Sex);
            //            SystemMessage.Write(MessageType.Issue, message);
            //            if (ApplicationSettings.ScannerMode)
            //            {
            //                NouvemMessageBox.Show(message, scanner: true);
            //            }
            //            else
            //            {
            //                NouvemMessageBox.Show(message, touchScreen: true);
            //            }

            //            this.IgnoreCategoryCheck = true;
            //            this.SelectedCategory = previousCat;
            //            this.IgnoreCategoryCheck = false;
            //        }));

            //        return;
            //    }
            //}
        }

        /// <summary>
        /// Opens the scanner port.
        /// </summary>
        protected override void OpenScanner()
        {
            if (!ApplicationSettings.ConnectToScanner)
            {
                return;
            }

            if (this.HardwareManager.IsScannerPortOpen())
            {
                return;
            }

            try
            {
                this.HardwareManager.ConnectToScanner();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.OpenScanner();
            this.OpenWand();
            Messenger.Default.Send(ViewType.Sequencer, Token.SetDataContext);
            Messenger.Default.Send(true, Token.DisableLegacyAttributesView);
            this.CloseOtherKillModules(ViewType.Sequencer);
            this.ScannedEartagStockDetails.Clear();
            this.EnteredEartag = string.Empty;
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.CarcassNumber)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextCarcassNumberBeef = this.SelectedDocNumbering.NextNumber;
            }

            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.CarcassNumberSheep)));
            this.SelectedSheepDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedSheepDocNumbering != null)
            {
                this.NextCarcassNumberSheep = this.SelectedSheepDocNumbering.NextNumber;
            }

            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.CarcassNumberPig)));
            this.SelectedPigDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedPigDocNumbering != null)
            {
                this.NextCarcassNumberPig = this.SelectedPigDocNumbering.NextNumber;
            }

            if (ApplicationSettings.SequencerMode == SequencerMode.Beef)
            {
                this.NextCarcassNumber = this.NextCarcassNumberBeef;
            }
            else
            {
                this.NextCarcassNumber = this.NextCarcassNumberSheep;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearLairageOrder();
            Messenger.Default.Send(Token.Message, Token.CloseLairageOrderWindow);
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            // not implemented
        }

        /// <summary>
        /// Removes an item from the grid.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.StockDetails == null || !this.StockDetails.Any())
            {
                return;
            }

            var itemToRemove = this.StockDetails.First();

            if (ApplicationSettings.ScannerMode)
            {
                NouvemMessageBox.Show(string.Format(Message.RemoveSequencedCarcassPrompt, itemToRemove.KillNumber), NouvemMessageBoxButtons.OKCancel, scanner: true);
            }
            else
            {
                NouvemMessageBox.Show(string.Format(Message.RemoveSequencedCarcassPrompt, itemToRemove.KillNumber), NouvemMessageBoxButtons.OKCancel, touchScreen: true);
            }

            if (NouvemMessageBox.UserSelection != UserDialogue.OK)
            {
                return;
            }

            var docId = this.SelectedDocNumbering.DocumentNumberingID;
            var error = this.DataManager.UndoSequencedCarcass(itemToRemove, docId);
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);

                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(error, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(error, touchScreen: true);
                }

                return;
            }

            SystemMessage.Write(MessageType.Priority, string.Format(Message.CarcassUnsequenced, itemToRemove.CarcassNumber));
            this.NextCarcassNumber -= 1;
            this.NextKillNumber -= 1;
            this.GetCurrentSequencedCarcasses();
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            // not implemented
        }

        #endregion

        #region method

        /// <summary>
        /// Handler for the key button selection.
        /// </summary>
        /// <param name="entry">The keypad selection.</param>
        protected void HandleKeyPadEntry(string entry)
        {
            if (this.LairageSequencerCombo)
            {
                return;
            }

            if (ApplicationSettings.HidePullTerminalAtSequencer)
            {
                this.enteringHerdNo = true;
            }

            if (entry.CompareIgnoringCase("Enter"))
            {
                if (this.enteringHerdNo && !ApplicationSettings.SearchByEartagAndKillNoAtSequencer)
                {
                    this.HandleHerdNumberEntry();
                }
                else
                {
                    this.HandleEartagInput();
                }

                return;
            }

            if (entry.CompareIgnoringCase("Clear"))
            {
                if (this.enteringHerdNo)
                {
                    this.EnteredHerdNumber = string.Empty;
                }
                else
                {
                    this.EnteredEartag = string.Empty;
                }

                return;
            }

            if (entry.CompareIgnoringCase("Back"))
            {
                if (this.enteringHerdNo)
                {
                    if (!string.IsNullOrEmpty(this.EnteredHerdNumber) && this.EnteredHerdNumber.Length > 0)
                    {
                        this.EnteredHerdNumber = this.EnteredHerdNumber.Substring(0, this.EnteredHerdNumber.Length - 1);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.EnteredEartag) && this.EnteredEartag.Length > 0)
                    {
                        this.EnteredEartag = this.EnteredEartag.Substring(0, this.EnteredEartag.Length - 1);
                    }
                }

                return;
            }

            if (entry.CompareIgnoringCase("Space"))
            {
                entry = " ";
            }

            if (entry.CompareIgnoringCase("Hyphen"))
            {
                entry = "-";
            }

            this.ScanIndicator = "N";
            if (this.enteringHerdNo)
            {
                this.EnteredHerdNumber += entry;
            }
            else
            {
                this.EnteredEartag += entry;
            }
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Re-orders the selected carcass up or down.
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        private void ScrollCommandExecute(string direction)
        {
            if (this.SelectedStockDetail != null)
            {
                switch (direction)
                {
                    case "Up":
                        this.SwapCarcassesInLine(this.SelectedStockDetail, false);
                        break;

                    case "Down":
                        this.SwapCarcassesInLine(this.SelectedStockDetail, true);
                        break;
                }
            }
        }

        /// <summary>
        /// Creates a missing tag record for a missing tag carcass.
        /// </summary>
        private void MissingTagCommandExecute()
        {
            if (ApplicationSettings.ScannerMode)
            {
                NouvemMessageBox.Show(Message.MissingTag, NouvemMessageBoxButtons.YesNo, scanner: true);
            }
            else
            {
                NouvemMessageBox.Show(Message.MissingTag, NouvemMessageBoxButtons.YesNo, touchScreen: true);
            }

            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
            {
                var missingTag = string.Format("MT{0}", DateTime.Now.ToString("yyyyMMddHHmmss"));
                var stock = new Model.DataLayer.Attribute { Eartag = missingTag };
                var trans = new StockTransaction
                {
                    TransactionDate = DateTime.Now,
                    TransactionQTY = 1,
                    IsBox = true,
                    INMasterID = this.BeefIntakeProductID,
                    NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionReceiptId,
                    WarehouseID = NouvemGlobal.WarehouseIntakeId,
                    DeviceMasterID = NouvemGlobal.DeviceId,
                    UserMasterID = NouvemGlobal.UserId
                };

                var data = new StocktransactionDetail { Attribute = stock, StockTransaction = trans };

                this.DataManager.AddStockAttribute(data);
                this.EnteredEartag = missingTag;
                this.HandleEartagInput();
            }
        }

        /// <summary>
        /// Handler for adding a carcass to the queue.
        /// </summary>
        private void AddToQueueCommandExecute()
        {
            if (ApplicationSettings.HidePullTerminalAtSequencer)
            {
                if (this.SelectedScannedEartagStockDetail != null)
                {
                    var trans = this.DataManager.GetStockTransactionFromAttribute(this.SelectedScannedEartagStockDetail.AttributeID
                        .ToInt());

                    if (trans != null)
                    {
                        var labelType = this.SelectedScannedEartagStockDetail.ScanningProductionStock
                            ? LabelType.Item
                            : LabelType.Shipping;
                        this.Print(trans.StockTransactionID, this.SelectedScannedEartagStockDetail.ProductID, labelType);
                    }
                }
        
                return;
            }

            if (ApplicationSettings.SequencerMode == SequencerMode.Sheep)
            {
                if (ApplicationSettings.UsingSheepWand)
                {
                    this.HandleEartagInputForWandSheep();
                    return;
                }

                this.HandleEartagInputForSheep();
                return;
            }

            if (!this.ScannedEartagStockDetails.Any())
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                return;
            }

            var carcass = this.ScannedEartagStockDetails.First();
            var localCategories = this.Categories.Where(x => x.CatType.CompareIgnoringCase(Constant.Beef)).ToList();

            if (ApplicationSettings.ResettingVealCategoriesAtSequencer)
            {
                if (carcass.AgeInMonths < ApplicationSettings.YoungVealAge)
                {
                    this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CAT == "V");
                }
                else if (carcass.AgeInMonths < ApplicationSettings.OldVealAge)
                {
                    this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CAT == "Z");
                }
            }

            if (ApplicationSettings.SelectingCategoryAtLairage && this.SelectedCategory != null)
            {
                // do nothing
            }
            else if ((this.SelectedCategory == null || this.SelectedCategory.CAT == "C") && !this.categorySelected
                && !string.IsNullOrEmpty(carcass.Eartag) && !carcass.Eartag.StartsWith("MT"))
            {
                if (this.SelectedCategory != null && this.SelectedCategory.CAT == "C")
                {
                    this.SelectedCategory = null;
                    localCategories.Clear();
                    if (carcass.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
                    {
                        var oldBull = this.Categories.FirstOrDefault(x => x.CAT == "B");
                        localCategories.Add(oldBull);
                    }
                    else
                    {
                        var youngBull = this.Categories.FirstOrDefault(x => x.CAT == "A");
                        localCategories.Add(youngBull);
                    }

                    var steer = this.Categories.FirstOrDefault(x => x.CAT == "C");
                    var veal = this.Categories.FirstOrDefault(x => x.CAT == "V");
                    var vealOld = this.Categories.FirstOrDefault(x => x.CAT == "Z");
                    localCategories.Add(steer);

                    if (veal != null)
                    {
                        localCategories.Add(veal);
                    }

                    if (vealOld != null)
                    {
                        localCategories.Add(vealOld);
                    }
                }

                var data = (from cat in localCategories
                            select new CollectionData
                            {
                                Data = string.Format("{0} - {1}", cat.CAT, cat.Name),
                                ID = cat.CategoryID,
                                Identifier = Constant.TraceabilityAttribute
                            }).ToList();

                this.completeAddToQueue = true;

                if (ApplicationSettings.ScannerMode)
                {
                    this.ScannerCategories.Clear();
                    foreach (var collectionData in data)
                    {
                        this.ScannerCategories.Add(collectionData);
                    }

                    Messenger.Default.Send(Token.Message, Token.CreateSequencerCategory);
                }
                else
                {
                    Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }

                return;
            }

            if (ApplicationSettings.ShowFarmAssuredAtSequencer && !this.completeFarmAssured)
            {
                var data = new List<CollectionData>
                {
                    new CollectionData{Data = Strings.FAYes, Identifier = Constant.FarmAssured},
                    new CollectionData{Data = Strings.FANo, Identifier = Constant.FarmAssured },
                };

                this.completeFarmAssured = true;
                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
                return;
            }

            this.completeFarmAssured = false;
            this.categorySelected = false;
            carcass.Category = this.SelectedCategory;
            carcass.Halal = this.Halal.YesNoToNonNullableBool();
            carcass.Breed= this.Breed;
            carcass.FarmAssured = this.FarmAssured.YesNoToBool();
            carcass.Sex = this.Sex;
            carcass.DOB = this.DOB;
            carcass.CountryOfOrigin = this.CountryOfOrigin?.Name;
            if (this.LairageSequencerCombo)
            {
                carcass.AgeInMonths = carcass.DOB.ToDate().AddDays(ApplicationSettings.UOMAddDays).DateDifferenceInMonths(DateTime.Today);
            }

            var localKillNo = this.DataManager.UpdateKillNumber();
            var docNumber = this.DataManager.SetDocumentNumber(this.SelectedDocNumbering.DocumentNumberingID);
            var localcarcassNo = docNumber.CurrentNumber;

            carcass.KillNumber = localKillNo;
            carcass.CarcassNumber = localcarcassNo;
            carcass.FarmAssured = this.FarmAssured.YesNoToBool();
            this.DataManager.UpdateStockAttribute(carcass);
            SystemMessage.Write(MessageType.Priority, string.Format(Message.CarcassAddedToKillQueue, this.EnteredEartag));

            if (ApplicationSettings.PrintSequencerLabel)
            {
                this.Print(carcass.StockTransactionID.ToInt(), carcass.ProductID);
            }

            this.NextKillNumber = localKillNo + 1;
            this.NextCarcassNumber = docNumber.NextNumber;

            this.StockDetails.Add(carcass);
            this.ScannedEartagStockDetails.Clear();
            this.EnteredEartag = string.Empty;
            this.EnteredHerdNumber = string.Empty;
            this.GetCurrentSequencedCarcasses();
            this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
            Messenger.Default.Send(Token.Message, Token.FocusToEartag);

            if (this.CheckSequencedAnimals())
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.CheckSequencedAnimalsPrompt, ApplicationSettings.CheckSequencedAnimalsCount), scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.CheckSequencedAnimalsPrompt, ApplicationSettings.CheckSequencedAnimalsCount), touchScreen: true);
                    }

                    Messenger.Default.Send(Token.Message, Token.FocusToEartag);
                }));
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Opens the scanner port.
        /// </summary>
        private void OpenWand()
        {
            if (!ApplicationSettings.ConnectToWand)
            {
                return;
            }

            try
            {
                this.HardwareManager.CloseWandPort();
                this.HardwareManager.OpenWandPort();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Handles the incoming wand read.
        /// </summary>
        /// <param name="data">The data to parse.</param>
        private void HandleWandData(string data)
        {
            this.Log.LogInfo(this.GetType(), $"Wand Data: |{data}|");
#if DEBUG
            data = $"123456{Environment.NewLine}123456";
#endif
            if (data.ContainsIgnoringCase("eS"))
            {
                return;
            }

            if (data.Contains(Environment.NewLine))
            {
                var pos = data.IndexOf(Environment.NewLine);
                data = data.Substring(0, pos);
            }

            this.EnteredEartag = data;
            this.ScanIndicator = "Y";

            try
            {
                if (this.HardwareManager.SheepWand.IsPortOpen)
                {
                    this.HardwareManager.SheepWand.WriteToPort(
                        ApplicationSettings.WandPortEraseString + Environment.NewLine);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }
        }

        /// <summary>
        /// Sets up the grader refresh
        /// </summary>
        private void SetUpRefreshTimer()
        {
            if (!ApplicationSettings.RefreshSequencerQueue)
            {
                return;
            }

            this.refreshTimer = new DispatcherTimer();
            var localTime = ApplicationSettings.GraderRefreshTime == 0 ? 20 : ApplicationSettings.GraderRefreshTime;
            this.refreshTimer.Interval = TimeSpan.FromSeconds(localTime);
            this.refreshTimer.Tick += this.RefreshTimerOnTick;
            this.refreshTimer.Start();
        }

        private void RefreshTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (this.ScannedEartagStockDetails != null && this.ScannedEartagStockDetails.Any())
            {
                return;
            }

            this.RefreshQueue();
        }

        /// <summary>
        /// Close the scanner category ui.
        /// </summary>
        private void CloseScannerCategory()
        {
            this.isScannerCategoryLoaded = false;
            Messenger.Default.Send(Token.Message, Token.CloseScannerCategoryWindow);
            Messenger.Default.Send(Token.Message, Token.FocusToButton);
        }

        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        private void HandleSelectedOrder(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            if (ApplicationSettings.UsingSheepWand)
            {
                this.HandleSelectedOrderTogether(orderDetails);
                return;
            }

            var order = orderDetails.Item1;
            this.Supplier = order.Supplier;
            this.LotNumber = order.Reference ?? string.Empty;
            this.CurrentIntake = order;
            var carcass = this.DataManager.GetNextCarcassByIntakeId(this.CurrentIntake.SaleDetails.First().SaleDetailID);
            this.RemainingCount = carcass.Count;
            Messenger.Default.Send(Token.Message, Token.FocusToEartag);
        }

        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        private void HandleSelectedOrderTogether(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            var order = orderDetails.Item1;
            this.Supplier = order.Supplier;
            this.LotNumber = order.Reference ?? string.Empty;
            this.CurrentIntake = order;
            this.sheepLotDetails = this.DataManager.GetNextCarcassWithEartagsByIntakeId(this.CurrentIntake.SaleDetails.First().SaleDetailID);
            this.RemainingCount = this.sheepLotDetails.Count;
            Messenger.Default.Send(Token.Message, Token.FocusToEartag);
        }

        /// <summary>
        /// Checks, if a check previous x carcasses message is to be displayed.
        /// </summary>
        /// <returns></returns>
        private bool CheckSequencedAnimals()
        {
            if (ApplicationSettings.CheckSequencedAnimalsCount == 0)
            {
                return false;
            }

            if (this.NextKillNumber <= ApplicationSettings.CheckSequencedAnimalsCount)
            {
                return false;
            }

            return this.NextKillNumber % ApplicationSettings.CheckSequencedAnimalsCount == 1;
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.ScannerStockMode = ScannerMode.Scan;
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                Keypad.Show(KeypadTarget.ManualSerial);
            };
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            this.Log.LogInfo(this.GetType(), $"HandleScannerData:|{data}|");

            #region validation

            if (this.DisableScannning)
            {
                return;
            }

            if (ApplicationSettings.HidePullTerminalAtSequencer)
            {
                this.SelectedScannedEartagStockDetail = null;
                this.ScannedEartagStockDetails.Clear();
                this.StockDetails?.Clear();
                var localSerial = this.DataManager.GetSerialByCarcassNo(data);
                var localCarcass = this.DataManager.GetCarcassByStockId(localSerial.ToInt());
                if (localCarcass != null)
                {
                    localCarcass.ScanningProductionStock = true;
                    this.ScannedEartagStockDetails.Add(localCarcass);
                    this.SelectedScannedEartagStockDetail = localCarcass;
                    this.labelsToPrint = 1;
                }

                return;
            }

            #endregion
         
            if (this.ScannedEartagStockDetails.Any())
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.EartagNotProcessedPrompt, this.EnteredEartag));
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.EartagNotProcessedPrompt, this.EnteredEartag), scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.EartagNotProcessedPrompt, this.EnteredEartag), touchScreen: true);
                    }
                }));

                return;
            }

            data = data.Replace("\r", "");

            if (data.Length > 12 && data.StartsWith("372"))
            {
                data = data.Substring(3, data.Length - 3);
            }

            this.enteringHerdNo = false;
            this.EnteredEartag = data;
            this.HandleKeyPadEntry("Enter");
        }

        /// <summary>
        /// Swaps 2 carcsses in the kill queue.
        /// </summary>
        /// <param name="localCarcass">The selected carcass.</param>
        /// <param name="moveUp">The move up/down flag.</param>
        private void SwapCarcassesInLine(StockDetail localCarcass, bool moveUp)
        {
            StockDetail carcassToIncrement;
            bool reordered = false;
            var docResetNumber = this.SelectedDocNumbering.LastNumber;

            if (moveUp)
            {
                // selected carcass is moving up the line.. get the carcass ahead in the line
                var localNextCarcassNumber = localCarcass.CarcassNumber == 1 ? docResetNumber : localCarcass.CarcassNumber - 1;

                carcassToIncrement =
                      this.StockDetails.FirstOrDefault(carcassItem => carcassItem.CarcassNumber == localNextCarcassNumber);

                if (carcassToIncrement != null)
                {
                    reordered = this.DataManager.ReOrderCarcasses(localCarcass, carcassToIncrement, docResetNumber);
                }
            }
            else
            {
                // selected carcass is moving down the line.. get the carcass behind in the line
                var localNextCarcassNumber = localCarcass.CarcassNumber == docResetNumber ? 1 : localCarcass.CarcassNumber + 1;

                carcassToIncrement =
                      this.StockDetails.FirstOrDefault(carcassItem => carcassItem.CarcassNumber == localNextCarcassNumber);

                if (carcassToIncrement != null)
                {
                    reordered = this.DataManager.ReOrderCarcasses(carcassToIncrement, localCarcass, docResetNumber);
                }
            }

            if (reordered)
            {
                this.GetCurrentSequencedCarcasses();
                this.SelectedStockDetail =
                    this.StockDetails.FirstOrDefault(x => x.AttributeID == localCarcass.AttributeID);
            }

            Messenger.Default.Send(Token.Message, Token.FocusToEartag);
        }

        /// <summary>
        /// Gets the age bands.
        /// </summary>
        private void GetAgeBands()
        {
            this.ageBands = this.DataManager.GetAgeBands();
            this.selectedAgeBand = null;

            if (this.ageBands.Any())
            {
                this.AgeBand1Text = string.Format("({0}-{1} {2})", this.ageBands.First().Min, this.ageBands.First().Max, Strings.Months);
            }

            if (this.ageBands.Count > 1)
            {
                this.AgeBand2Text = string.Format("({0}-{1} {2})", this.ageBands.ElementAt(1).Min, this.ageBands.ElementAt(1).Max, Strings.Months);
            }

            if (this.ageBands.Count > 2)
            {
                this.AgeBand3Text = string.Format("({0}-{1} {2})", this.ageBands.ElementAt(2).Min, this.ageBands.ElementAt(2).Max, Strings.Months);
            }

            if (this.ageBands.Count > 3)
            {
                this.AgeBand4Text = string.Format("({0}-{1} {2})", this.ageBands.ElementAt(3).Min, this.ageBands.ElementAt(3).Max, Strings.Months);
            }
        }

        /// <summary>
        /// Handles the eartag selection.
        /// </summary>
        private void HandleHerdNumberEntry()
        {
            if (ApplicationSettings.SearchByEartagAndKillNoAtSequencer)
            {
                return;
            }

            this.ScannedEartagStockDetails.Clear();
            if (string.IsNullOrWhiteSpace(this.EnteredHerdNumber))
            {
                return;
            }

            if (this.EnteredHerdNumber.Length < ApplicationSettings.MinSequencerHerdNoLength)
            {
                var message = string.Format(Message.HerdNumberLengthTooShort, ApplicationSettings.MinSequencerHerdNoLength);
                SystemMessage.Write(MessageType.Issue, message);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                }

                return;
            }

            var localTag = string.Format("{0}{1}", this.EnteredHerdNumber, this.EnteredEartag);
            var stock = this.DataManager.GetLairageIntakeStockByEartag(localTag);

            if (!string.IsNullOrEmpty(stock.Error))
            {
                if (stock.Error.Equals(Message.EartagNotFound))
                {
                    SystemMessage.Write(MessageType.Issue, Message.EartagWithHerdNotFound);
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.EartagWithHerdNotFound, NouvemMessageBoxButtons.YesNo, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(Message.EartagWithHerdNotFound, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    }

                    if (NouvemMessageBox.UserSelection == UserDialogue.No)
                    {
                        this.enteringHerdNo = false;
                    }

                    this.EnteredHerdNumber = string.Empty;
                    Messenger.Default.Send(Token.Message, Token.FocusToHerdNo);
                    return;
                }

                // multiple eartags found
                var message = string.Format(Message.MultipleEartagsWithHerdFound, stock.Error.ElementAt(0));
                SystemMessage.Write(MessageType.Issue, message);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                }

                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    this.enteringHerdNo = false;
                    this.EnteredHerdNumber = string.Empty;
                    this.EnteredEartag = string.Empty;
                    Messenger.Default.Send(Token.Message, Token.FocusToEartag);
                }

                this.EnteredHerdNumber = string.Empty;
                Messenger.Default.Send(Token.Message, Token.FocusToHerdNo);
                return;
            }

            this.enteringHerdNo = false;
            this.AgeInMonths = stock.AgeInMonths.ToInt();
            this.Sex = stock.Sex;
            this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == stock.CategoryID);

            if (stock.SequencedDate.HasValue)
            {
                var message = string.Format(Message.CarcassAlreadySequenced, stock.Eartag, stock.SequencedDate,
                    stock.CarcassNumber);
                SystemMessage.Write(MessageType.Issue, message);

                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                }

                return;
            }

            if (!this.CheckAgeValidity(stock.AgeInMonths))
            {
                // outside age range. give warning.
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.CarcassOutsideOfAgeBand, NouvemMessageBoxButtons.YesNo, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.CarcassOutsideOfAgeBand, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                }

                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    this.EnteredEartag = string.Empty;
                    this.EnteredHerdNumber = string.Empty;
                    Messenger.Default.Send(Token.Message, Token.FocusToEartag);
                    return;
                }
            }

            this.ScannedEartagStockDetails.Add(stock);
            this.SelectedScannedEartagStockDetail = stock;
            Messenger.Default.Send(Token.Message, Token.FocusToButton);
        }

        /// <summary>
        /// Handles the eartag selection.
        /// </summary>
        private void HandleEartagInput()
        {
            this.ScannedEartagStockDetails.Clear();
            if (string.IsNullOrWhiteSpace(this.EnteredEartag) && !ApplicationSettings.HidePullTerminalAtSequencer)
            {
                return;
            }

            if (ApplicationSettings.SequencerMode == SequencerMode.Sheep)
            {
                this.HandleEartagInputForSheep();
                return;
            }

            if (this.EnteredEartag.Length < ApplicationSettings.MinSequencerEartagLength && !ApplicationSettings.HidePullTerminalAtSequencer)
            {
                var message = string.Format(Message.SequencerEartaglengthTooShort, ApplicationSettings.MinSequencerEartagLength);
                SystemMessage.Write(MessageType.Issue, message);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                }

                return;
            }

            if (ApplicationSettings.SearchByEartagAndKillNoAtSequencer && string.IsNullOrWhiteSpace(this.EnteredHerdNumber))
            {
                var message = Message.NoKillNoEntered;
                SystemMessage.Write(MessageType.Issue, message);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                }

                return;
            }

            StockDetail stock;
            if (!ApplicationSettings.SearchByEartagAndKillNoAtSequencer)
            {
                stock = this.DataManager.GetLairageIntakeStockByEartag(this.EnteredEartag);
            }
            else if (!ApplicationSettings.HidePullTerminalAtSequencer)
            {
                stock = this.DataManager.GetLairageIntakeStockByEartag(this.EnteredEartag, this.EnteredHerdNumber);
                if (!stock.AgeInMonths.HasValue)
                {
                    stock.AgeInMonths = stock.DOB.ToDate().AddDays(ApplicationSettings.UOMAddDays).DateDifferenceInMonths(DateTime.Today);
                }
            }
            else
            {
                stock = this.StockDetails.FirstOrDefault(x => x.ExternalKillNumber == this.EnteredHerdNumber);
                if (stock == null)
                {
                    stock = new StockDetail {Error = Message.EartagNotFound };
                }
                else
                {
                    var authorised = this.AuthorisationsManager.AllowUserToSelectAnyAnimalAtSequencerHideStation;
                    if (stock != this.StockDetails.First())
                    {
                        if (!authorised)
                        {
                            SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToSelectAnyCarcassAtHideStation);
                            NouvemMessageBox.Show(Message.NotAuthorisedToSelectAnyCarcassAtHideStation, touchScreen: true);
                            return;
                        }
                    }

                    this.labelsToPrint = ApplicationSettings.LabelsToPrintAtSequencer;
                    this.ScannedEartagStockDetails.Add(stock);
                    this.SelectedScannedEartagStockDetail = stock;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(stock.Error))
            {
                if (stock.Error.Equals(Message.EartagNotFound))
                {
                    SystemMessage.Write(MessageType.Issue, Message.EartagNotFound);
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.EartagNotFound, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(Message.EartagNotFound, touchScreen: true);
                    }

                    return;
                }

                // multiple eartags found
                SystemMessage.Write(MessageType.Issue, stock.Error);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(stock.Error, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(stock.Error, touchScreen: true);
                }

                this.enteringHerdNo = true;
                Messenger.Default.Send(Token.Message, Token.FocusToHerdNo);
                return;
            }

            if (ApplicationSettings.OnlyAllowAuthorisedAnimalsAtSequencer && !stock.LotAuthorisedDate.HasValue)
            {
                var msg = string.Format(Message.AnimalFromUnauthorisedLot, stock.Number);
                SystemMessage.Write(MessageType.Issue, msg);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(msg, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(msg, touchScreen: true);
                    }
                }));

                return;
            }

            //if (ApplicationSettings.OnlyAllowCompletedLotAnimalsToBeSequenced && !stock.CompletedLot)
            //{
            //    var msg = string.Format(Message.AnimalFromUncompletedLot, stock.Number);
            //    SystemMessage.Write(MessageType.Issue, msg);
            //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //    {
            //        if (ApplicationSettings.ScannerMode)
            //        {
            //            NouvemMessageBox.Show(msg, scanner: true);
            //        }
            //        else
            //        {
            //            NouvemMessageBox.Show(msg, touchScreen: true);
            //        }
            //    }));

            //    return;
            //}

            this.AgeInMonths = stock.AgeInMonths.ToInt();
            this.Sex = stock.Sex;
            this.Halal = stock.Halal.BoolToYesNo();
            this.Eartag = stock.Eartag;
            this.FarmAssured = stock.FarmAssuredYesNo;
            this.Breed = this.Breeds.FirstOrDefault(x => x.NouBreedID == stock.BreedID);
            this.DOB = stock.DOB;
            this.CountryOfOrigin =
                this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(stock.CountryOfOrigin));

            this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == stock.CategoryID);
            if (ApplicationSettings.ResetCategoryAtSequencer)
            {
                this.SelectedCategory = null;
            }

            if (stock.SequencedDate.HasValue)
            {
                var message = string.Format(Message.CarcassAlreadySequenced, stock.Eartag, stock.SequencedDate,
                    stock.CarcassNumber);
                SystemMessage.Write(MessageType.Issue, message);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(message, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(message, touchScreen: true);
                }

                return;
            }

            if (!this.CheckAgeValidity(stock.AgeInMonths))
            {
                // outside age range. give warning.
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.CarcassOutsideOfAgeBand, NouvemMessageBoxButtons.YesNo, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.CarcassOutsideOfAgeBand, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                }

                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    this.EnteredEartag = string.Empty;
                    return;
                }
            }

            this.ScannedEartagStockDetails.Add(stock);
            this.SelectedScannedEartagStockDetail = stock;
            //if (this.LairageSequencerCombo)
            //{
            //    this.SelectedStockDetail = null;
            //    this.ClearAttributes();
            //}
      
            Messenger.Default.Send(Token.Message, Token.FocusToButton);
        }

        /// <summary>
        /// Handles the eartag selection.
        /// </summary>
        private void HandleEartagInputForSheep()
        {
            if (string.IsNullOrWhiteSpace(this.EnteredEartag))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.LotNumber))
            {
                return;
            }

            if (this.RemainingCount == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.SheepSequenceLotComplete);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.SheepSequenceLotComplete, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.SheepSequenceLotComplete, touchScreen: true);
                }

                return;
            }

            if (!ApplicationSettings.AllowDuplicateSheepEartagsAtSequencer)
            {
                var existingTag = this.DataManager.IsSheepEartagInSystem(this.EnteredEartag,
                this.CurrentIntake.SaleDetails.First().SaleDetailID);
                if (existingTag)
                {
                    var message = string.Format(Message.EartagAlreadyInLot, this.EnteredEartag);
                    SystemMessage.Write(MessageType.Issue, message);
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(message, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(message, touchScreen: true);
                    }

                    this.Eartag = string.Empty;

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        Messenger.Default.Send(Token.Message, Token.FocusToEartag);
                    }));

                    return;
                }
            }

            var carcass = this.DataManager.GetNextCarcassByIntakeId(this.CurrentIntake.SaleDetails.First().SaleDetailID);
            this.RemainingCount = carcass.Count;

            carcass.Eartag = this.EnteredEartag;

            var localKillNo = this.DataManager.UpdateKillNumberSheep();
            var docNumber = this.DataManager.SetDocumentNumber(this.SelectedSheepDocNumbering.DocumentNumberingID);
            var localcarcassNo = docNumber.CurrentNumber;

            carcass.KillNumber = localKillNo;
            carcass.CarcassNumber = localcarcassNo;
            carcass.Attribute180 = this.ScanIndicator;
            if (this.DataManager.UpdateStockAttribute(carcass))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.CarcassAddedToKillQueue, this.EnteredEartag));
                this.ScanIndicator = "N";
                this.NextKillNumber = localKillNo + 1;
                this.NextCarcassNumber = docNumber.NextNumber;

                this.StockDetails.Add(carcass);
                this.ScannedEartagStockDetails.Clear();
                this.EnteredEartag = string.Empty;
                this.EnteredHerdNumber = string.Empty;
                this.GetCurrentSequencedCarcasses();
                this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
                Messenger.Default.Send(Token.Message, Token.FocusToEartag);
                this.RemainingCount -= 1;

                if (this.RemainingCount == 0)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.SheepSequenceLotComplete, scanner: true, flashMessage: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(Message.SheepSequenceLotComplete, touchScreen: true, flashMessage: true);
                    }

                    if (this.CurrentIntake != null)
                    {
                        this.DataManager.SetIntakeProcessing(this.CurrentIntake.SaleID, true);
                    }

                    this.SelectedStockDetail = null;
                    this.GetNextQueuedHerd();
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.CarcassNotAddedToQueue, this.EnteredEartag));
            }
        }

        /// <summary>
        /// Handles the eartag selection.
        /// </summary>
        private void HandleEartagInputForWandSheep()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            this.OrderUpdateInProgress = true;
            try
            {
                //if (string.IsNullOrWhiteSpace(this.LotNumber))
                //{
                //    return;
                //}

                if (this.sheepLotDetails == null)
                {
                    return;
                }

                this.RemainingCount = this.sheepLotDetails.Count;

                if (this.RemainingCount == 0)
                {
                    NouvemMessageBox.Show(Message.NoAnimalsLeftToSequence, touchScreen: true);
                    return;
                }

                NouvemMessageBox.Show(string.Format(Message.SequenceWandLotPrompt, this.RemainingCount, this.LotNumber),
                    NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                foreach (var carcass in this.sheepLotDetails.StockDetails)
                {
                    if (carcass.SequencedDate.HasValue)
                    {
                        continue;
                    }

                    var localKillNo = this.DataManager.UpdateKillNumberSheep();
                    var docNumber = this.DataManager.SetDocumentNumber(this.SelectedSheepDocNumbering.DocumentNumberingID);
                    var localcarcassNo = docNumber.CurrentNumber;
                    carcass.KillNumber = localKillNo;
                    carcass.CarcassNumber = localcarcassNo;
                    if (this.DataManager.UpdateStockAttribute(carcass))
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.CarcassAddedToKillQueue, this.EnteredEartag));
                        this.RemainingCount -= 1;
                        this.NextKillNumber = localKillNo + 1;
                        this.NextCarcassNumber = docNumber.NextNumber;
                        this.StockDetails.Add(carcass);
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, string.Format(Message.CarcassNotAddedToQueue, this.EnteredEartag));
                    }
                }

                this.ScannedEartagStockDetails.Clear();
                this.EnteredEartag = string.Empty;
                this.EnteredHerdNumber = string.Empty;
                this.GetCurrentSequencedCarcasses();
                this.SelectedStockDetail = this.StockDetails.FirstOrDefault();
                Messenger.Default.Send(Token.Message, Token.FocusToEartag);

                if (this.RemainingCount == 0)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.SheepSequenceLotComplete, scanner: true, flashMessage: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(Message.SheepSequenceLotComplete, touchScreen: true, flashMessage: true);
                    }

                    if (this.CurrentIntake != null)
                    {
                        this.DataManager.SetIntakeProcessing(this.CurrentIntake.SaleID, true);
                    }

                    this.SelectedStockDetail = null;
                    this.GetNextQueuedHerd();
                    this.sheepLotDetails = null;
                    this.Supplier = string.Empty;
                    this.LotNumber = string.Empty;
                }
            }
            finally
            {
                this.OrderUpdateInProgress = false;
            }
        }

        /// <summary>
        /// Gets the next kill number.
        /// </summary>
        private void GetNextKillNumber()
        {
            if (ApplicationSettings.SequencerMode == SequencerMode.Beef)
            {
                this.NextKillNumber = this.DataManager.GetNextKillNumber(true);
                return;
            }

            if (ApplicationSettings.SequencerMode == SequencerMode.Sheep)
            {
                this.NextKillNumber = this.DataManager.GetNextKillNumberSheep(true);
                return;
            }
        }

        /// <summary>
        /// Checks if the incoming carcass is within the selected age parameters.
        /// </summary>
        /// <param name="ageInMonths">The age to check.</param>
        /// <returns>Flag, as to whether the incoming carcass is within the selected age parameters.</returns>
        private bool CheckAgeValidity(int? ageInMonths)
        {
            if (this.selectedAgeBand == null || !this.IsAgeBandSelected() || !ageInMonths.HasValue)
            {
                return true;
            }

            var minAge = this.selectedAgeBand.Min;
            var maxAge = this.selectedAgeBand.Max;

            return ageInMonths >= minAge && ageInMonths < maxAge;
        }

        /// <summary>
        /// Gets the current sequenced stock.
        /// </summary>
        private void GetCurrentSequencedCarcasses()
        {
            if (ApplicationSettings.SequencerMode == SequencerMode.Beef)
            {
                this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetCurrentSequencedStock()
                    .Where(x => x.KillType.CompareIgnoringCase(Constant.Beef))
                    .OrderByDescending(x => x.KillNumber));
            }
            else
            {
                this.StockDetails = new ObservableCollection<StockDetail>(this.DataManager.GetCurrentSequencedStock()
                    .Where(x => x.KillType.CompareIgnoringCase(Constant.Sheep))
                    .OrderByDescending(x => x.KillNumber));
            }
        }

        /// <summary>
        /// Is an age band selected.
        /// </summary>
        /// <returns>Age band selected flag.</returns>
        private bool IsAgeBandSelected()
        {
            return this.AgeBand1Selected || this.AgeBand2Selected || this.AgeBand3Selected || this.AgeBand4Selected;
        }

        /// <summary>
        /// Refreshes the queue.
        /// </summary>
        private void RefreshQueue()
        {
            this.GetLocalDocNumberings();
            this.GetNextKillNumber();
            this.GetCurrentSequencedCarcasses();
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        private void Print(int stockTransactionId, int? inmasterid, LabelType labelType = LabelType.Item)
        {
            try
            {
                var localProcess = this.Processes.FirstOrDefault(x => x.Name.CompareIgnoringCase(ProcessType.Sequencer));
                var labels = this.PrintManager.ProcessPrinting(null, null, inmasterid, null, ViewType.Sequencer, labelType,
                    stockTransactionId, this.labelsToPrint, process: localProcess);

                if (labels.Item1.Any() && labelType != LabelType.Pallet && labelType != LabelType.Shipping)
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(stockTransactionId, string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(ex.Message, touchScreen: true, flashMessage: true);
                }));

                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        #endregion

        #endregion
    }
}










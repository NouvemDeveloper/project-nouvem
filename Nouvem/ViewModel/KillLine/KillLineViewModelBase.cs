﻿// -----------------------------------------------------------------------
// <copyright file="KillLineViewModelBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;

namespace Nouvem.ViewModel.KillLine
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class KillLineViewModelBase : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The dept response animal details file.
        /// </summary>
        private string deptResponseXml;

        /// <summary>
        /// The dept response animal details file.
        /// </summary>
        private bool fileTransfered;

        /// <summary>
        /// The dept response animal details file.
        /// </summary>
        private bool lotAuthorised;

        /// <summary>
        /// The selected kill type.
        /// </summary>
        private NouKillType selectedKillType;

        /// <summary>
        /// The lot count.
        /// </summary>
        private string lotKillCount;

        /// <summary>
        /// The system kill types.
        /// </summary>
        private ObservableCollection<NouKillType> killTypes;

        /// <summary>
        /// The system kill types.
        /// </summary>
        private ObservableCollection<Sale> lots;

        /// <summary>
        /// The system kill types.
        /// </summary>
        private Sale selectedLot;

        /// <summary>
        /// The total kill received at intake.
        /// </summary>
        private string totalKillReceived;

        /// <summary>
        /// The user entered eartag.
        /// </summary>
        private string lotNumber;

        /// <summary>
        /// The user entered eartag.
        /// </summary>
        private string supplier;

        /// <summary>
        /// The supplier herds.
        /// </summary>
        private ObservableCollection<SupplierHerd> herds;
     
        /// <summary>
        /// The supplier herds.
        /// </summary>
        private SupplierHerd selectedHerd;

        /// <summary>
        /// The supplier herds.
        /// </summary>
        private bool herdSelected;

        /// <summary>
        /// The carcass attribute to edit id.
        /// </summary>
        private int pigCount;

        /// <summary>
        /// The farm movements.
        /// </summary>
        private ObservableCollection<FarmMovement> movements;

        /// <summary>
        /// The remaining amount to be killed from the current lot.
        /// </summary>
        private int remainingCount;

        /// <summary>
        /// The current lot kill count.
        /// </summary>
        private int killCount;

        /// <summary>
        /// The selected kill type.
        /// </summary>
        protected int BeefIntakeProductID;

        /// <summary>
        /// The selected kill type.
        /// </summary>
        protected int SheepIntakeProductID;

        /// <summary>
        /// The selected kill type.
        /// </summary>
        protected int PigIntakeProductID;

        /// <summary>
        /// The kill number ordered.
        /// </summary>
        protected int KillNumberOrdered;
   
        /// <summary>
        /// The request/response data.
        /// </summary>
        protected AimsData aimsData;

        /// <summary>
        /// The current selected intake.
        /// </summary>
        protected Sale CurrentIntake;

        /// <summary>
        /// The sheep doc no.
        /// </summary>
        protected DocNumber SelectedSheepDocNumbering;

        /// <summary>
        /// The pig doc no.
        /// </summary>
        protected DocNumber SelectedPigDocNumbering;

        /// <summary>
        /// The next sheep carcass beef.
        /// </summary>
        protected int NextCarcassNumberBeef;

        /// <summary>
        /// The next sheep carcass number.
        /// </summary>
        protected int NextCarcassNumberSheep;

        /// <summary>
        /// The next sheep carcass pig.
        /// </summary>
        protected int NextCarcassNumberPig;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KillLineViewModelBase"/> class.
        /// </summary>
        public KillLineViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message

            Messenger.Default.Register<StockDetail>(this, Token.DetainCondemnSelected, this.HandleDetainedCondemnedData);

            #endregion

            #region command

            // open the detain/condemn window.
            this.DetainCondemnCommand = new RelayCommand(this.DetainCondemnCommandExecute);

            #endregion

            this.GetKillTypes();
            this.SelectedKillType = this.KillTypes.FirstOrDefault();
            this.SetTotalKillReceived(0);
            if (NouvemGlobal.SupplierHerds == null)
            {
                NouvemGlobal.SupplierHerds = this.DataManager.GetSupplierHerds();
            }
            
            this.StockDetails = new ObservableCollection<StockDetail>();
            this.Herds = new ObservableCollection<SupplierHerd>(NouvemGlobal.SupplierHerds);
            this.HandleKillType();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the panel beef\sheep content.
        /// </summary>
        public bool FileTransfered
        {
            get
            {
                return this.fileTransfered;
            }

            set
            {
                this.fileTransfered = value;
                this.RaisePropertyChanged();
                if (value && !this.EntitySelectionChange)
                {
                    this.HandleFileTransfer();
                }
            }
        }

        /// <summary>
        /// Gets or sets the panel beef\sheep content.
        /// </summary>
        public bool LotAuthorised
        {
            get
            {
                return this.lotAuthorised;
            }

            set
            {
                this.lotAuthorised = value;
                this.RaisePropertyChanged();
                this.HandleLotAuthorisation();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are accumulating the last weighing on to the tare.
        /// </summary>
        public bool AccumulateWeightToTareAtGrader { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are in lairage creation/grading mode.
        /// </summary>
        public bool LairageGraderCombo { get; set; }

        /// <summary>
        /// Gets or sets the panel beef\sheep content.
        /// </summary>
        public int PigCount
        {
            get
            {
                return this.pigCount;
            }

            set
            {
                this.pigCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm movements
        /// </summary>
        public string DeptResponseXml
        {
            get
            {
                return this.deptResponseXml;
            }

            set
            {
                this.deptResponseXml = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the lot count.
        /// </summary>
        public string LotKillCount
        {
            get
            {
                return this.lotKillCount;
            }

            set
            {
                this.lotKillCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the remaining sheep to be processed in the lot.
        /// </summary>
        public int RemainingCount
        {
            get
            {
                return this.remainingCount;
            }

            set
            {
                if (value < 0)
                {
                    value = 0;
                }

                this.remainingCount = value;
                this.RaisePropertyChanged();
                if (!this.LairageGraderCombo)
                {
                    this.PigCount = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the remaining sheep to be processed in the lot.
        /// </summary>
        public int KillCount
        {
            get
            {
                return this.killCount;
            }

            set
            {
                this.killCount = value;
                this.RaisePropertyChanged();
                if (this.LairageGraderCombo)
                {
                    this.PigCount = value;
                    this.SetSheepLotCount(value, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the entered herd number.
        /// </summary>
        public string Supplier
        {
            get
            {
                return this.supplier;
            }

            set
            {
                this.supplier = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the entered herd number.
        /// </summary>
        public string LotNumber
        {
            get
            {
                return this.lotNumber;
            }

            set
            {
                this.lotNumber = value;
                this.RaisePropertyChanged();
            }
        }
      
        /// <summary>
        /// Gets or sets the total kill received at intake.
        /// </summary>
        public string TotalKillReceived
        {
            get
            {
                return this.totalKillReceived;
            }

            set
            {
                this.totalKillReceived = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the lots
        /// </summary>
        public ObservableCollection<Sale> Lots
        {
            get
            {
                return this.lots;
            }

            set
            {
                this.lots = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected lot.
        /// </summary>
        public Sale SelectedLot
        {
            get
            {
                return this.selectedLot;
            }

            set
            {
                this.selectedLot = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm movements
        /// </summary>
        public ObservableCollection<FarmMovement> Movements
        {
            get
            {
                return this.movements;
            }

            set
            {
                this.movements = value;
                this.RaisePropertyChanged();
            }
        }
   
        /// <summary>
        /// Gets or sets the selected kill type.
        /// </summary>
        public NouKillType SelectedKillType
        {
            get
            {
                return this.selectedKillType;
            }

            set
            {
                this.SetMode(value, this.selectedKillType);
                this.selectedKillType = value;
                this.RaisePropertyChanged();
                this.HandleKillType();
            }
        }

        /// <summary>
        /// Gets or sets the supplier herds.
        /// </summary>
        public SupplierHerd SelectedHerd
        {
            get
            {
                return this.selectedHerd;
            }

            set
            {
                this.selectedHerd = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.herdSelected = true;
                    this.SelectedPartner = this.Suppliers.FirstOrDefault(x => x.BPMasterID == value.BPMasterID);
                    this.herdSelected = false;
                    this.HandleSelectedHerd();
                }
            }
        }

        /// <summary>
        /// Gets or sets the supplier herds.
        /// </summary>
        public ObservableCollection<SupplierHerd> Herds
        {
            get
            {
                return this.herds;
            }

            set
            {
                this.herds = value;
                this.RaisePropertyChanged();
            }
        }
        
        #endregion

        #region command

        /// <summary>
        /// Gets the command to move carcasses up and down the line.
        /// </summary>
        public ICommand EartagEnteredCommand { get; set; }

        /// <summary>
        /// Gets the command to detain/condemn a carcass.
        /// </summary>
        public ICommand DetainCondemnCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        #region virtual

        /// <summary>
        /// Handles a file transfer at lairage.
        /// </summary>
        protected virtual void HandleFileTransfer() { }

        /// <summary>
        /// Handles the authorisation of a lot.
        /// </summary>
        protected virtual void HandleLotAuthorisation() { }

        /// <summary>
        /// Gets the next herd queued.
        /// </summary>
        protected virtual void GetNextQueuedHerd(){}

        /// <summary>
        /// Handles a herd selection.
        /// </summary>
        protected virtual void ReCheckQas()
        {
        }

        /// <summary>
        /// Handles a herd selection.
        /// </summary>
        protected virtual void HandleSelectedHerd()
        {
        }

        /// <summary>
        /// Handles the partner properties.
        /// </summary>
        /// <param name="properties">The property id's.</param>
        /// <param name="bpMasterId">The prartner id.</param>
        protected virtual void HandlePartnerProperties(IList<int> properties, int bpMasterId)
        {
        }

        protected virtual void GetHerds()
        {
            NouvemGlobal.SupplierHerds = this.DataManager.GetSupplierHerds();
            this.Herds = new ObservableCollection<SupplierHerd>(NouvemGlobal.SupplierHerds);
        }

        protected void SetPartnerHerds(int partnerId)
        {
            if (NouvemGlobal.SupplierHerds == null)
            {
                return;
            }

            var partnerHerds = this.DataManager.GetSupplierPartnerHerds(partnerId);
            foreach (var herd in partnerHerds)
            {
                var localHerd = NouvemGlobal.SupplierHerds.FirstOrDefault(x => x.SupplierHerdID == herd.SupplierHerdID);
                if (localHerd != null)
                {
                    localHerd.HerdNumber = herd.HerdNumber;
                    localHerd.Beef = herd.Beef;
                    localHerd.Sheep = herd.Sheep;
                }
                else
                {
                    NouvemGlobal.SupplierHerds.Add(herd);
                    this.Herds = new ObservableCollection<SupplierHerd>(NouvemGlobal.SupplierHerds);
                }
            }
        }

        /// <summary>
        /// Gets the associated kill categories.
        /// </summary>
        protected virtual void HandleKillType()
        {
            if (this.SelectedKillType == null)
            {
                return;
            }

            if (this.SelectedKillType.Name.CompareIgnoringCase(Constant.Beef))
            {
                this.Categories = new ObservableCollection<NouCategory>(
                    this.DataManager.GetCategories().Where(x => x.CatType.CompareIgnoringCase(Constant.Beef)).ToList());
                this.KillType = KillType.Beef;
                this.CountryOfOrigin = null;
                this.RearedIn = null;
                return;
            }

            if (this.SelectedKillType.Name.CompareIgnoringCase(Constant.Pig))
            {
                this.Categories = new ObservableCollection<NouCategory>(
                    this.DataManager.GetCategories().Where(x => x.CatType.CompareIgnoringCase(Constant.Pig)).ToList());
                this.KillType = KillType.Pig;
                this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Ireland));
                this.RearedIn = this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Ireland));
                return;
            }

            this.Categories = new ObservableCollection<NouCategory>(
                    this.DataManager.GetCategories().Where(x => x.CatType.CompareIgnoringCase(Constant.Sheep)).ToList());
            this.KillType = KillType.Sheep;
            this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Ireland));
            this.RearedIn = this.Countries.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Ireland));
            return;
        }

        /// <summary>
        /// Opens the detain condemn window.
        /// </summary>
        protected virtual void DetainCondemnCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.ShowDetainCondemn);
        }

        /// <summary>
        /// Handles the incoming detained condemned data.
        /// </summary>
        /// <param name="detail">The incoming data.</param>
        protected virtual void HandleDetainedCondemnedData(StockDetail detail)
        {
            this.Detained = detail.DetainedSide1.ToBool();
            this.DetainedSide2 = detail.DetainedSide2.ToBool();
            this.Condemned = detail.CondemnedSide1.ToBool();
            this.CondemnedSide2 = detail.CondemnedSide2.ToBool();
            this.TBYes = detail.TBYes.ToBool();
            this.NotInsured = detail.NotInsured.ToBool();
            this.BurstBelly = detail.BurstBelly.ToBool();
            this.IsCasualty = detail.Casualty.ToBool();
            this.SelectedFatColour = detail.FatColour;
            this.LegMissing = detail.LegMissing.ToBool();
            this.Abscess = detail.Abscess.ToBool();
        }

        #endregion

        #region method

        /// <summary>
        /// Closes the other kill modules.
        /// </summary>
        protected virtual void CloseOtherKillModules(ViewType currentModule)
        {
            if (currentModule == ViewType.LairageIntakeTouchscreen)
            {
                ViewModelLocator.ClearGrader();
                return;
            }

            if (currentModule == ViewType.LairageIntake)
            {
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                return;
            }

            if (currentModule == ViewType.Sequencer)
            {
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearLairage();
                ViewModelLocator.ClearLairageTouchscreen();
                Messenger.Default.Send(Token.Message, Token.CloseLairageIntakeWindow);
                return;
            }

            if (currentModule == ViewType.Grader)
            {
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearLairage();
                ViewModelLocator.ClearLairageTouchscreen();
                Messenger.Default.Send(Token.Message, Token.CloseLairageIntakeWindow);
                return;
            }
        }

        /// <summary>
        /// Sets the application countries.
        /// </summary>
        protected override void GetCountries()
        {
            this.Countries = this.DataManager.GetCountryMaster();
            this.Countries.Add(new Country { Name = Constant.Unknown });
        }

        /// <summary>
        /// Gets the country matching the input code.
        /// </summary>
        /// <param name="code">The code to search for.</param>
        /// <returns>The country matching the input code.</returns>
        protected string GetCountryOfOrigin(string code)
        {
            var country = this.Countries.FirstOrDefault(x => x.PassportCode.CompareIgnoringCase(code));
            if (country != null)
            {
                return country.Name;
            }

            return Constant.Unknown;
        }

        /// <summary>
        /// Sets the lot kill count.
        /// </summary>
        /// <param name="lotId">The header id.</param>
        protected void SetKillCount(int lotId)
        {
            var data = this.DataManager.GetLotCount(lotId);
            var killed = data.Count(x => x.HasValue) + 1; // include the current animal
            var count = data.Count;
            this.SetLotCount(killed, count);
        }

        /// <summary> 
        /// Sets the lot kill count.
        /// </summary>
        /// <param name="killed">The lot kill count.</param>
        protected void SetLotCount(int killed, int total)
        {
            this.LotKillCount = string.Format("{0}/{1}", killed, total);
        }

        /// <summary>
        /// The the lot count for combo sheep.
        /// </summary>
        /// <param name="killed"></param>
        /// <param name="total"></param>
        protected virtual void SetSheepLotCount(int killed, int total) { }

        /// <summary> 
        /// Sets the kill received/ordered values.
        /// </summary>
        /// <param name="received">The kill received.</param>
        protected void SetTotalKillReceived(int received)
        {
            this.TotalKillReceived = string.Format("{0}{1}/{2}", Strings.TotalReceived, received, this.KillNumberOrdered);
        }

        #endregion

        #region override

        /// <summary>
        /// Gets the inventory items.
        /// </summary>
        protected override void GetInventoryItems()
        {
            this.GetIntakeProductIds();
            this.SaleItems = new ObservableCollection<InventoryItem>();
            var lairageitems = new List<int> { this.BeefIntakeProductID, this.SheepIntakeProductID, this.PigIntakeProductID };

            this.PurchaseItems =
                new ObservableCollection<InventoryItem>(
                    NouvemGlobal.PurchaseItems.Where(x => lairageitems.Contains(x.Master.INMasterID)));
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            //base.ParsePartner();

            // now get the associated customer quotes.
            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            var supplier =
               NouvemGlobal.SupplierPartners.FirstOrDefault(
                   x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
            this.SelectedDeliveryAddressFull = string.Empty;

            if (supplier != null)
            {
                supplier = this.DataManager.GetBusinessPartner(supplier.Details.BPMasterID);
                if (supplier.Addresses != null)
                {
                    var deliveryAddress = supplier.Addresses.FirstOrDefault(x => x.Details.Shipping == true);
                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                        this.SelectedDeliveryAddressFull = deliveryAddress.FullAddressMultiLine;
                    }
                }

                if (supplier.PartnerProperties.Any())
                {
                    this.HandlePartnerProperties(supplier.PartnerProperties.Select(x => x.Details.BPPropertyID).ToList(), supplier.Details.BPMasterID);
                }

                this.SetPartnerHerds(supplier.Details.BPMasterID);
            }

          
            if (!this.herdSelected)
            {
                this.SetHerd();
                if (this.SelectedHerd == null)
                {
                    this.SetHerd();
                }
            }
        }

        /// <summary>
        /// Resets the ui.
        /// </summary>
        /// <param name="clearSelectedCustomer">Flag, indicating whether the selected customer data is cleared.</param>
        protected override void ClearForm(bool clearSelectedCustomer = true)
        {
            base.ClearForm(clearSelectedCustomer);
            this.aimsData = null;
            this.Movements = null;
            this.SelectedAgent = null;
            this.SelectedHerd = null;
            this.SelectedDocStatus =
                this.DocStatusItems.FirstOrDefault(
                    x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
            this.Generic1 = string.Empty;
            if (this.StockDetails != null)
            {
                this.StockDetails.Clear();
            }
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems
                               .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                   || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                                || x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ||
                                x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Gets the suppliers.
        /// </summary>
        protected override void GetSuppliers()
        {
            NouvemGlobal.UpdateBusinessPartners();
            this.Suppliers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.BusinessPartners
                 .Where(x => (x.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier)
                     || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock))
                     && ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                       || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)))
                .OrderBy(x => x.Details.Name)
                .Select(x => x.Details));
        }

        protected override void CheckForNewEntities(string entityType = "")
        {
            try
            {
                var newEntities = this.DataManager.CheckForNewEntities(NouvemGlobal.DeviceId.ToInt(), entityType);
                var partnerEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.BusinessPartner));
                foreach (var partnerEntity in partnerEntities)
                {
                    if (NouvemGlobal.BusinessPartners.Any(x => x.Details.BPMasterID == partnerEntity.EntityID))
                    {
                        continue;
                    }

                    var localPartner = this.DataManager.GetBusinessPartner(partnerEntity.EntityID);
                    if (localPartner != null)
                    {
                        NouvemGlobal.AddNewPartner(localPartner);
                        this.allPartners.Add(localPartner.Details);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        /// <summary>
        /// Overrides the selected order.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            base.HandleSelectedSale();
            if (this.Sale.NouKillTypeID.HasValue)
            {
                this.SelectedKillType = this.KillTypes.FirstOrDefault(x => x.NouKillTypeID == this.Sale.NouKillTypeID);
            }

            if (this.Sale.SaleDetails != null)
            {
                var localStockDetails =
             this.Sale.SaleDetails.Where(x => x.StockDetails != null).SelectMany(x => x.StockDetails).OrderByDescending(x => x.AttributeID);
                this.StockDetails = new ObservableCollection<StockDetail>(localStockDetails);

                if (this.Sale.SaleDetails.Any())
                {
                    this.KillNumberOrdered = this.Sale.SaleDetails.First().QuantityOrdered.ToInt();
                }
            }

            var intakeReceived = 0;
            if (this.StockDetails != null)
            {
                intakeReceived = this.StockDetails.Count;
            }

            this.SetTotalKillReceived(intakeReceived);
        }

        protected override void CreateSale()
        {
            base.CreateSale();
            if (this.selectedKillType != null)
            {
                this.Sale.NouKillTypeID = this.selectedKillType.NouKillTypeID;
            }
        }

        protected override void CopyDocumentTo()
        {
            //throw new NotImplementedException();
        }

        protected override void CopyDocumentFrom()
        {
            //throw new NotImplementedException();
        }

        protected override void FindSale()
        {
            //throw new NotImplementedException();
        }

        protected override void FindSaleCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void DirectSearchCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void AddSale()
        {
            //throw new NotImplementedException();
        }

        protected override void UpdateSale()
        {
            //throw new NotImplementedException();
        }

        protected override void Close()
        {
            //throw new NotImplementedException();
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Sets the herd, looking for the specific herd selection first, and if not found then using the first non specified herd.
        /// </summary>
        protected void SetHerd()
        {
            if (this.Herds == null || this.SelectedPartner == null)
            {
                return;
            }

            SupplierHerd localHerd = null;
            if (this.SelectedKillType.Name.CompareIgnoringCase("BEEF"))
            {
                localHerd = this.Herds.FirstOrDefault(x => x.BPMasterID == this.SelectedPartner.BPMasterID && x.Beef.IsTrue());
            }
            else if (this.SelectedKillType.Name.CompareIgnoringCase("SHEEP"))
            {
                localHerd = this.Herds.FirstOrDefault(x => x.BPMasterID == this.SelectedPartner.BPMasterID && x.Sheep.IsTrue());
            }

            if (localHerd == null)
            {
                localHerd = this.Herds.FirstOrDefault(x => x.BPMasterID == this.SelectedPartner.BPMasterID && x.Sheep.IsNullOrFalse() && x.Beef.IsNullOrFalse());
            }

            this.SelectedHerd = localHerd;
        }

        /// <summary>
        /// Gets the system intake products.
        /// </summary>
        private void GetIntakeProductIds()
        {
            var beef =
                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Name.CompareIgnoringCase(Constant.BeefIntake));
            if (beef != null)
            {
                this.BeefIntakeProductID = beef.Master.INMasterID;
                ApplicationSettings.BeefIntakeID = this.BeefIntakeProductID;
            }

            var sheep =
                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Name.CompareIgnoringCase(Constant.SheepIntake));
            if (sheep != null)
            {
                this.SheepIntakeProductID = sheep.Master.INMasterID;
            }

            var pig =
                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Name.CompareIgnoringCase(Constant.PigIntake));
            if (pig != null)
            {
                this.PigIntakeProductID = pig.Master.INMasterID;
            }
        }

        #endregion
    }
}

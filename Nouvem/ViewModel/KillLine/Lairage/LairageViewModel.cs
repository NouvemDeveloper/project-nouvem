﻿// -----------------------------------------------------------------------
// <copyright file="LairageViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using DevExpress.Xpf.Grid;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class LairageViewModel : KillLineViewModelBase
    {
        #region field

        private bool dontFormatTags;

        /// <summary>
        /// Show the radio button options.
        /// </summary>
        private bool showRadioOptions;

        private ControlMode previousMode;

        private bool scanInProgress;

        private bool showWebServiceOption;

        private string testScan;

        /// <summary>
        /// Show wand button.
        /// </summary>
        private bool showWandButton;

        /// <summary>
        /// The wand time out counter.
        /// </summary>
        private int wandTimeOut;

        /// <summary>
        /// The selected tab.
        /// </summary>
        private int selectedTabItem;

        /// <summary>
        /// The herd number.
        /// </summary>
        private string permitNo;

        /// <summary>
        /// The herd number.
        /// </summary>
        private string presentingHerdNo;

        /// <summary>
        /// Is the herd aims apprived flag.
        /// </summary>
        private bool aimsHerdApproved;

        /// <summary>
        /// The intake vhaulier charge.
        /// </summary>
        private decimal? haulierCharge;

        /// <summary>
        /// Is the herd bord bia apprived flag.
        /// </summary>
        private bool bordBiaHerdApproved;

        /// <summary>
        /// Is the herd bord bia apprived flag.
        /// </summary>
        private bool usingAims;

        /// <summary>
        /// Is the herd bord bia apprived flag.
        /// </summary>
        private bool usingBordBia;

        /// <summary>
        /// The animal check/move dept to query.
        /// </summary>
        private string animalCheckDept;

        /// <summary>
        /// The herd check dept to query.
        /// </summary>
        private string herdCheckDept;

        /// <summary>
        /// The aims carcass data.
        /// </summary>
        private StockDetail aimsCarcass;

        /// <summary>
        /// The collection of animals.
        /// </summary>
        private IList<Sale> lotsToUpdate = new List<Sale>();

        /// <summary>
        /// The collection of animals.
        /// </summary>
        private ObservableCollection<Carcass> animals = new ObservableCollection<Carcass>();

        /// <summary>
        /// The selected animal.
        /// </summary>
        private Carcass selectedAnimal;

        /// <summary>
        /// The cop to collection.
        /// </summary>
        private IList<string> copyToDocuments;

        private DispatcherTimer wandTimer;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LairageViewModel"/> class.
        /// </summary>
        public LairageViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.DeleteLairageAnimal, S => this.RemoveAnimalCommandExecute());

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                this.HandleAttributeValueSelection(c);
            });

            // Register for the incoming collection screen closed without category selection.
            Messenger.Default.Register<string>(this, Token.CollectionScreenMoveBack, c =>
            {
                // no category selected, so we use the aims steer category.
                if (this.IsFormLoaded)
                {
                    this.HandleAimsCarcassCategorySelection();
                }
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                this.HandleScannerData(s);
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Wand, s =>
            {
                this.HandleWandData(s);
            });

            #endregion

            #region command handler

            this.VerifyWandTagsCommand = new RelayCommand(this.VerifyWandTagsCommandExecute);
            this.EraseEartagsFromWandCommand = new RelayCommand(this.EraseEartagsFromWandCommandExecute);
            this.GetEartagsFromWandCommand = new RelayCommand(this.GetEartagsFromWandCommandExecute);
            this.RemoveAnimalCommand = new RelayCommand(this.RemoveAnimalCommandExecute);
            this.VerifyHerdCommand = new RelayCommand(this.VerifyHerdCommandExecute);
            this.VerifyAnimalCommand = new RelayCommand(this.VerifyAnimalCommandExecute);
            this.EartagEnteredCommand = new RelayCommand<string>(s =>
            {
                Messenger.Default.Send(Token.Message, Token.FocusToButton);
            }, (s) => this.KillType == KillType.Sheep);

            #endregion

            this.GetFreezerTypes();
            this.SetDepartments();
            this.GetLocalDocNumberings();
            this.CanCopyFrom = true;
            this.GetBreeds();
            this.GetSexes();
            //this.GetCategories();
            this.GetCustomers();
            this.GetFAValues();
            this.GetCountries();
            this.GetCleanlinesses();
            this.ClearAttributes();
            this.HerdFarmAssured = Constant.No;
            this.DOB = DateTime.Today;
            this.UsingAims = ApplicationSettings.UsingAims;
            this.UsingBordBia = ApplicationSettings.UsingBordBia;
            this.CloseOtherKillModules(ViewType.LairageIntake);
            this.CopyToDocuments = new List<string> { Strings.NewIntake };
            this.SetUpWandTimer();
            this.ShowWebServiceOption = !ApplicationSettings.DontShowWebServiceOption;
            this.CurrentItem = null;
            this.LairageTypeNo = ApplicationSettings.LairageType == LairageType.Ireland ? Strings.DispatchDocketNo : Strings.PermitNo;

            if (!this.HardwareManager.IsScannerPortOpen())
            {
                this.HardwareManager.ConnectToScanner();
            }
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the lairge docket no (Permit or Dispatch serial no.)
        /// </summary>
        public string LairageTypeNo { get; set; } 

        private bool? currentItem;

        /// <summary>
        /// Gets or sets a value indicating whether the sheep wand button is visible. 
        /// </summary>
        public bool? CurrentItem
        {
            get
            {
                return this.currentItem;
            }

            set
            {
                this.currentItem = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the sheep wand button is visible. 
        /// </summary>
        public bool ShowWandButton
        {
            get
            {
                return this.showWandButton;
            }

            set
            {
                this.showWandButton = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the web service checkboxes are visible. 
        /// </summary>
        public bool ShowWebServiceOption
        {
            get
            {
                return this.showWebServiceOption;
            }

            set
            {
                this.showWebServiceOption = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the animal check/move dept to query. 
        /// </summary>
        public IList<string> CopyToDocuments
        {
            get
            {
                return this.copyToDocuments;
            }

            set
            {
                this.copyToDocuments = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the multi select grid items.
        /// </summary>
        public IList<StockDetail> SelectedStockDetails { get; set; } = new List<StockDetail>();

        /// <summary>
        /// Gets or sets the animal check/move dept to query. 
        /// </summary>
        public int SelectedTabItem
        {
            get
            {
                return this.selectedTabItem;
            }

            set
            {
                this.selectedTabItem = value;
                this.RaisePropertyChanged();
                this.HandleSelectedTabItem(value);
            }
        }

        /// <summary>
        /// Gets or sets the animal check/move dept to query. 
        /// </summary>
        public bool ShowRadioOptions
        {
            get
            {
                return this.showRadioOptions;
            }

            set
            {
                this.showRadioOptions = value;
                this.RaisePropertyChanged();

                this.IsOrganic = false;
                this.IsWelsh = false;
                this.IsContinental = false;
                this.IsSelectFarm = false;
                this.IsStandard = true;
            }
        }

        /// <summary>
        /// Gets or sets the animal check/move dept to query. 
        /// </summary>
        public string AnimalCheckDept
        {
            get
            {
                return this.animalCheckDept;
            }

            set
            {
                this.animalCheckDept = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the animal check/move dept to query. 
        /// </summary>
        public string TestScan
        {
            get
            {
                return this.testScan;
            }

            set
            {
                this.testScan = value;
                this.RaisePropertyChanged();
                if (NouvemGlobal.IsNouvemUser && value != null)
                {
                    this.HandleScannerData(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the herd check dept to query. 
        /// </summary>
        public string HerdCheckDept
        {
            get
            {
                return this.herdCheckDept;
            }

            set
            {
                this.herdCheckDept = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the intake haulier charge. 
        /// </summary>
        public decimal? HaulierCharge
        {
            get
            {
                return this.haulierCharge;
            }

            set
            {
                this.haulierCharge = value;
                this.RaisePropertyChanged();
            }
        }

        public Carcass SelectedAnimal
        {
            get
            {
                return this.selectedAnimal;
            }

            set
            {
                this.selectedAnimal = value;
                this.RaisePropertyChanged();
            }
        }

        public ObservableCollection<Carcass> Animals
        {
            get
            {
                return this.animals;
            }

            set
            {
                this.animals = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the herd number.
        /// </summary>
        public string PresentingHerdNo
        {
            get
            {
                return this.presentingHerdNo;
            }

            set
            {
                this.presentingHerdNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the aims is being used.
        /// </summary>
        public bool UsingAims
        {
            get
            {
                return this.usingAims;
            }

            set
            {
                this.usingAims = value;
                this.RaisePropertyChanged();
                ApplicationSettings.UsingAims = value;
            }
        }

        /// <summary>
        /// Gets or sets the animal check/move dept to query. 
        /// </summary>
        public string PermitNo
        {
            get
            {
                return this.permitNo;
            }

            set
            {
                this.permitNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the aims is being used.
        /// </summary>
        public bool UsingBordBia
        {
            get
            {
                return this.usingBordBia;
            }

            set
            {
                this.usingBordBia = value;
                this.RaisePropertyChanged();
                ApplicationSettings.UsingBordBia = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the current herd is bord bia approved.
        /// </summary>
        public bool BordBiaHerdApproved
        {
            get
            {
                return this.bordBiaHerdApproved;
            }

            set
            {
                this.bordBiaHerdApproved = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the current herd is bord bia approved.
        /// </summary>
        public bool AimsHerdApproved
        {
            get
            {
                return this.aimsHerdApproved;
            }

            set
            {
                this.aimsHerdApproved = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to verify the animal.
        /// </summary>
        public ICommand EraseEartagsFromWandCommand { get; set; }

        /// <summary>
        /// Gets the command to verify the animal.
        /// </summary>
        public ICommand VerifyWandTagsCommand { get; set; }

        /// <summary>
        /// Gets the command to verify the animal.
        /// </summary>
        public ICommand GetEartagsFromWandCommand { get; set; }

        /// <summary>
        /// Gets the command to verify the animal.
        /// </summary>
        public ICommand RemoveAnimalCommand { get; set; }

        /// <summary>
        /// Gets the command to verify the herd number.
        /// </summary>
        public ICommand VerifyHerdCommand { get; set; }

        /// <summary>
        /// Gets the command to verify the animal.
        /// </summary>
        public ICommand VerifyAnimalCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageIntakeByLastEdit();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetLairageIntakeByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetLairageIntakeById(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetLairageIntakeByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetLairageIntakeById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageIntakeByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageIntakeByFirstLast(false);
        }

        /// <summary>
        /// Refreshes the serach screen.
        /// </summary>
        public void RefreshSales()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager
                    .GetAllLairageIntakes(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, Strings.ProposedKilldate);
            }
            else
            {
                this.CustomerSales =
                    this.DataManager.GetAllLairageIntakes(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, Strings.ProposedKilldate);
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.LairageIntake), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.LairageIntake);
            }
        }

        /// <summary>
        /// Creates a lairage intake from an order.
        /// </summary>
        /// <param name="id">Them id to add.</param>
        public void CreateLairgeFromOrder(int id)
        {
            var searchSale = this.DataManager.GetLairageOrderByID(id);
            var localDocNumbering = this.SelectedDocNumbering;
            this.Sale = searchSale;
            this.SelectedDocNumbering = localDocNumbering;
            this.KillNumberOrdered =
                searchSale.SaleDetails.Sum(x => x.QuantityReceivedOTM.ToInt() + x.QuantityReceivedUTM.ToInt());
            this.SetTotalKillReceived(0);
            base.CreateSale();
            if (this.Sale.SaleDetails != null && this.Sale.SaleDetails.Any())
            {
                this.Sale.SaleDetails.First().QuantityOrdered = this.KillNumberOrdered;
            }

            this.AddSale();
        }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(82, this.Sale.SaleID.ToString(), !preview);
            return;
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        protected override void HandleSelectedLairageCustomer()
        {
            if (this.Sale != null && this.Sale.SaleID > 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        protected override void HandleFileTransfer()
        {
            if (this.Sale != null && this.Sale.SaleID > 0)
            {
                if (this.StockDetails == null || !this.StockDetails.Any())
                {
                    SystemMessage.Write(MessageType.Issue, "You cannot mark the lairage for transfer as there are no animals in the lot.");
                    this.FileTransfered = false;
                    return;
                }

                var lotProcessed = this.StockDetails.All(x => x.GradingDate.HasValue);
                if (!lotProcessed)
                {
                    SystemMessage.Write(MessageType.Issue, "You cannot mark the lairage for transfer as not all the animals in the lot have been fully processed.");
                    this.FileTransfered = false;
                    return;
                }

                this.SetControlMode(ControlMode.Update);
                return;
            }

            SystemMessage.Write(MessageType.Issue, "You cannot mark the lairage for transfer as there are no animals in the lot.");
            this.FileTransfered = false;
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        protected override void HandleLotAuthorisation()
        {
            if (this.Sale != null && this.Sale.SaleID > 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Copys the selected animals to another docket.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            if (string.IsNullOrEmpty(this.SelectedDocumentCopyTo))
            {
                return;
            }

            var animalsToMove = this.SelectedStockDetails.Where(x => !x.SequencedDate.HasValue).ToList();
            NouvemMessageBox.Show(string.Format(Message.MoveAnimalsPrompt, animalsToMove.Count()), NouvemMessageBoxButtons.YesNo);
            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
            {
                this.UpdateDocNumbering();
                this.Sale.StockDetails = animalsToMove;
                ProgressBar.SetUp(0, animalsToMove.Count);
                var newSaleId = this.DataManager.MoveLairageIntake(this.Sale);
                ProgressBar.Reset();

                if (newSaleId > 0)
                {
                    this.Sale = this.DataManager.GetLairageIntakeById(newSaleId);
                    SystemMessage.Write(MessageType.Priority, Message.AnimalsMoved);
                }
            }

            this.SelectedDocumentCopyTo = null;
        }

        /// <summary>
        /// Handles a grid field change.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            #region validation

            if (e.Cell == null || this.SelectedLot == null)
            {
                return;
            }

            #endregion

            try
            {
                if (e.Cell.Property.Equals("ReferenceID"))
                {
                    var inUpdateList = this.lotsToUpdate.Any(x => x.SaleID == this.SelectedLot.SaleID);
                    if (!inUpdateList)
                    {
                        this.lotsToUpdate.Add(this.SelectedLot);
                    }

                    if (this.CurrentMode != ControlMode.Update)
                    {
                        this.previousMode = this.CurrentMode;
                    }

                    this.SetControlMode(ControlMode.Update);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogDebug(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles the partner properties.
        /// </summary>
        /// <param name="properties">The property id's.</param>
        /// <param name="bpMasterId">The prartner id.</param>
        protected override void HandlePartnerProperties(IList<int> properties, int bpMasterId)
        {
            if (NouvemGlobal.BusinessPartnerPropertySelections == null)
            {
                NouvemGlobal.BusinessPartnerPropertySelections = this.DataManager.GetPropertySelection();
            }

            foreach (var id in properties)
            {
                var partnerProperty = NouvemGlobal.BusinessPartnerPropertySelections.First(x => x.BPPropertyID == id && x.BPMasterID == bpMasterId);
                if (!string.IsNullOrWhiteSpace(partnerProperty.DisplayMessage))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(partnerProperty.DisplayMessage);
                    }));
                }
            }
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems
                               .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                   || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                                   || x.NouDocStatusID == NouvemGlobal.NouDocStatusMoved.NouDocStatusID
                                || x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ||
                                x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Sets the default form dates.
        /// </summary>
        protected override void SetDefaultDates()
        {
            this.DocumentDate = DateTime.Today;
            this.DeliveryDate = DateTime.Today.Add(TimeSpan.FromDays(ApplicationSettings.LairageDeliveryDaysForward));
            if (ApplicationSettings.ProposedKillDateDaysForward < 0)
            {
                this.ProposedKillDate = null;
            }
            else
            {
                this.ProposedKillDate = DateTime.Today.Add(TimeSpan.FromDays(ApplicationSettings.ProposedKillDateDaysForward));
            }
        }

        /// <summary>
        /// Opens the wand port, and sends a command to retrieve the eartags.
        /// </summary>
        private void GetEartagsFromWandCommandExecute()
        {
            #region validation

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoSupplierSelected);
                return;
            }

            if (this.SelectedCategory == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCategoryEntered);
                return;
            }

            #endregion

#if DEBUG
            this.HandleWandData("");
            return;
#endif
            try
            {
                this.wandTimeOut = 0;
                this.HardwareManager.OpenWandPort();
                this.wandTimer.Start();
            }
            catch (Exception e)
            {
                this.wandTimer.Stop();
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
        }

        /// <summary>
        /// Verifys the herd tags with afais.
        /// </summary>
        private void VerifyWandTagsCommandExecute()
        {
            #region validation

            if (this.Sale == null || this.StockDetails == null || !this.StockDetails.Any() || this.SelectedHerd == null)
            {
                return;
            }

            #endregion

            this.Sale.PermitNo = this.PermitNo;
            this.Sale.Herd = this.SelectedHerd.HerdNumber;
            this.Sale.StockDetails = this.StockDetails.ToList();
            var result = this.DepartmentBodiesManager.SheepEIDCheck(this.Sale);
            var error = result.Item1;
            var reference = result.Item2;
            var invalidTags = result.Item3;

            if (!string.IsNullOrEmpty(error))
            {
                this.Log.LogError(this.GetType(), $"VerifyWandTagsCommandExecute:{error}, Reference:{reference}");
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error);
                return;
            }

            if (invalidTags.Any())
            {
                var msg = $"The following tags are not resident in the presenting flock:{Environment.NewLine}";
                foreach (var tag in invalidTags)
                {
                    var tagData = $"{tag.Data}  Flock:{tag.Identifier}{Environment.NewLine}";
                    msg += tagData;
                }
         
                SystemMessage.Write(MessageType.Issue, msg);
                NouvemMessageBox.Show(msg, isPopUp:true);
                this.SelectedStockDetail = this.StockDetails.FirstOrDefault(x =>
                    x.Eartag.CompareIgnoringCaseAndWhitespace(invalidTags.First().Data));
                return;
            }

            SystemMessage.Write(MessageType.Priority, "All tags are valid and are resident in the presenting flock");
            NouvemMessageBox.Show("All tags are valid and are resident in the presenting flock");
        }

        /// <summary>
        /// Opens the wand port, and sends a command to retrieve the eartags.
        /// </summary>
        private void EraseEartagsFromWandCommandExecute()
        {
            NouvemMessageBox.Show("You are about to erase data from the wand. Do you wish to proceed?", NouvemMessageBoxButtons.YesNo);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            this.Log.LogInfo(this.GetType(), $"Erasing wand data - sending :{ApplicationSettings.WandPortEraseString}, Port Open:{this.HardwareManager.SheepWand.IsPortOpen.BoolToYesNo()}");

            try
            {
                //if (this.HardwareManager.SheepWand.IsPortOpen)
                //{
                //    this.HardwareManager.CloseWandPort();
                //    System.Threading.Thread.Sleep(200);
                //}

                this.HardwareManager.OpenWandPort();
                if (this.HardwareManager.SheepWand.IsPortOpen)
                {
                    this.HardwareManager.SheepWand.WriteToPort(
                        ApplicationSettings.WandPortEraseString + Environment.NewLine);
                    SystemMessage.Write(MessageType.Priority, "Data erased");
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, "Port closed");
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                try
                {
                    this.HardwareManager.CloseWandPort();
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                    SystemMessage.Write(MessageType.Issue, e.Message);
                }
            }
        }

        /// <summary>
        /// Removes an animal from the intake.
        /// </summary>
        private void RemoveAnimalCommandExecute()
        {
            if (this.SelectedStockDetail != null)
            {
                try
                {
                    var name = this.SelectedPartner?.Name;
                    NouvemMessageBox.Show($"You are about to remove {this.SelectedStockDetail.Eartag} from this lot:{name}. Do you wish to proceed?", NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        //var details = this.StockDetails.ToList();
                        //this.StockDetails.Remove(this.SelectedStockDetail);
                        //this.StockDetails = new ObservableCollection<StockDetail>(details);
                        return;
                    }

                    var error = this.DataManager.RemoveLairageIntakeItem(this.SelectedStockDetail.AttributeID.ToInt());
                    if (error == string.Empty)
                    {
                        SystemMessage.Write(MessageType.Priority, Message.AnimalRemovedFromIntake);
                        this.StockDetails.Remove(this.SelectedStockDetail);
                        this.SetTotalKillReceived(this.StockDetails.Count);
                        this.ClearAttributes();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, error);
                        NouvemMessageBox.Show(error);
                    }
                }
                finally
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.SelectedStockDetail = null;
                    }));
                }
            }
        }

        /// <summary>
        /// Overide the selected sale, to reinstate the sale id.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            base.HandleSelectedSale();
            if (this.SelectedPartner == null && this.Sale?.BPCustomer != null)
            {
                this.GetHerds();
                var localPartner = this.DataManager.GetBusinessPartner(this.Sale.BPCustomer.BPMasterID);
                if (localPartner != null)
                {
                    this.Suppliers.Add(localPartner.Details);
                    this.allPartners.Add(localPartner.Details);
                    this.SelectedPartner = localPartner.Details;
                }
            }

            if (this.Customers != null)
            {
                this.SelectedLairageCustomer = this.Customers.FirstOrDefault(x => x.BPMasterID == this.Sale.CustomerID);
            }

            this.EntitySelectionChange = true;
            this.HerdFarmAssured = this.Sale.HerdQAS.BoolToYesNo();
            this.HaulierCharge = this.Sale.HaulageCharge;
            this.LotAuthorised = this.Sale.LotAuthorisedDate.ToBool();
            this.FileTransfered = this.Sale.FileTransferedDate.ToBool();
            this.PermitNo = this.Sale.PermitNo;
            this.EntitySelectionChange = false;
            if (this.Sale.BPHaulier != null)
            {
                this.SelectedHaulier = this.Hauliers.FirstOrDefault(x => x.BPMasterID == this.Sale.BPHaulier.BPMasterID);
            }
            else
            {
                this.SelectedHaulier = null;
            }

            if (this.Sale.BPAgent != null)
            {
                this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == this.Sale.BPAgent.BPMasterID);
            }
            else
            {
                if (!ApplicationSettings.DoNotResetLairageAgent)
                {
                    this.SelectedAgent = null;
                }
            }

            this.InwardMovementReferenceId = this.Sale.OtherReference;
            this.MoveToLairageReference = this.Sale.MoveToLairageReference;
            if (this.Customers != null)
            {
                this.SelectedCustomer = this.Customers.FirstOrDefault(x => x.Name.Equals(Strings.NoCustomer));
            }

            if (this.Sale != null && this.Sale.SaleDetails != null && this.Sale.SaleDetails.Any())
            {
                var detail = this.Sale.SaleDetails.First();
                if (detail.StockDetails != null && detail.StockDetails.Any())
                {
                    var lastEntry = detail.StockDetails.Last();
                    this.Generic1 = lastEntry.Generic1;
                    this.EntitySelectionChange = true;
                    this.SelectedKillType =
                        this.KillTypes.FirstOrDefault(
                            x => x.Name.CompareIgnoringCase(detail.StockDetails.First().KillType));
                    this.EntitySelectionChange = false;
                    this.Attribute200 = lastEntry.Attribute200;
                    if (this.Attribute200.CompareIgnoringCase("Welsh"))
                    {
                        this.IsWelsh = true;
                    }
                    else if (this.Attribute200.CompareIgnoringCase("Organic"))
                    {
                        this.IsOrganic = true;
                    }
                    else if (this.Attribute200.CompareIgnoringCase("SelectFarm"))
                    {
                        this.IsSelectFarm = true;
                    }
                    else if (this.Attribute200.CompareIgnoringCase("Continental"))
                    {
                        this.IsContinental = true;
                    }
                    else
                    {
                        this.IsStandard = true;
                    }
                }
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SelectedStockDetail = null;
                this.CurrentItem = null;
                base.ClearAttributes();
                this.Attribute120 = Constant.No;
                this.Attribute210 = Constant.No;
            }));
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                if (searchSale.SaleType == ViewType.LairageOrder)
                {
                    searchSale = this.DataManager.GetLairageOrderByID(searchSale.SaleID);
                    this.Sale = searchSale;
                    this.KillNumberOrdered =
                        searchSale.SaleDetails.Sum(x => x.QuantityReceivedOTM.ToInt() + x.QuantityReceivedUTM.ToInt());
                    this.SetTotalKillReceived(0);
                }
                else
                {
                    this.Sale = this.DataManager.GetLairageIntakeById(searchSale.SaleID);
                }
            }
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoSupplierSelected);
                return;
            }

            #endregion

            var orders = this.DataManager.GetLairageOrdersBySupplier(this.SelectedPartner.BPMasterID);

            if (!orders.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Orders));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(orders, ViewType.LairageIntake), Token.SearchForSale);
            this.Locator.SalesSearchData.SetView(ViewType.LairageIntake);

            CopyingDocument = true;
        }

        /// <summary>
        /// Handle the selected stock detail parse/edit.
        /// </summary>
        /// <param name="detail">The detail to parse for edit.</param>
        protected override void HandleSelectedStockDetail(StockDetail detail)
        {
            if (detail == null)
            {
                return;
            }

            this.EntitySelectionChange = true;
            try
            {
                this.Eartag = detail.Eartag;
                this.HoldingNumber = detail.HoldingNumber;
                this.HerdNo = detail.HerdNo;
                this.DOB = detail.DOB;
                this.AgeInMonths = detail.AgeInMonths.ToInt();
                this.AgeInDays = detail.AgeInDays.ToInt();
                this.DaysOfResidencyCurrent = detail.CurrentResidency.ToInt();
                this.DaysOfResidencyTotal = detail.TotalResidency.ToInt();
                this.NumberOfMoves = detail.NumberOfMoves.ToInt();
                this.Attribute220 = detail.Attribute220;
                this.LastMoveDate = detail.DateOfLastMove;

                if (detail.Movements != null)
                {
                    this.Movements = new ObservableCollection<FarmMovement>(detail.Movements);
                }

                if (detail.AimsData != null)
                {
                    this.DeptResponseXml = detail.AimsData.ResponseXml;
                }
                else
                {
                    this.DeptResponseXml = string.Empty;
                }

                if (detail.Category != null)
                {
                    this.IgnoreCategoryCheck = true;
                    this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == detail.Category.CategoryID);
                    this.IgnoreCategoryCheck = false;
                }
                else
                {
                    this.SelectedCategory = null;
                }

                this.SelectedCleanliness = this.Cleanlinesses.FirstOrDefault(x => x.Name == detail.CleanlinessRating);
                if (detail.Breed != null)
                {
                    this.Breed = this.Breeds.FirstOrDefault(x => x.Name == detail.Breed.Name);
                }

                this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.Name == detail.CountryOfOrigin);
                this.RearedIn = this.Countries.FirstOrDefault(x => x.Name == detail.RearedIn);
                this.Sex = detail.Sex;
                this.Generic1 = detail.Generic1;
                this.FarmAssured = detail.FarmAssured.BoolToYesNo();

                this.SelectedFreezerType = this.FreezerTypes.FirstOrDefault(x => x.Data.Equals(detail.FreezerType));
                if (this.SelectedFreezerType == null && this.FreezerTypes != null)
                {
                    this.SelectedFreezerType = this.FreezerTypes.FirstOrDefault();
                }

                this.SelectedCustomer = this.Customers.FirstOrDefault(x => x.BPMasterID == detail.CustomerID);
                this.Clipped = detail.ClippedYesNo;
                this.Lame = detail.LameYesNo;
                this.Casualty = detail.CasualtyYesNo;
                this.Imported = detail.ImportedYesNo;
                this.ThirdParty = detail.ThirdParty;
                this.Attribute210 = detail.Attribute210;
                this.Attribute120 = detail.Attribute120;
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Handles a change od sex.
        /// </summary>
        protected override void HandleSexSelection()
        {
            if (this.EntitySelectionChange || this.SelectedCategory == null)
            {
                return;
            }

            var localSex = this.DepartmentBodiesManager.FindSex(this.SelectedCategory.CAT);
            if (localSex != this.Sex)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Sex = localSex;
                }));
            }
        }

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="category">The scanned passport data barcode.</param>
        protected override void HandleCategorySelection(NouCategory category)
        {
            if (this.EntitySelectionChange )
            {
                return;
            }

            var localMode = this.CurrentMode;

            try
            {
                var previousCat = this.PreviousCategory;
                if (localMode == ControlMode.Update)
                {
                    if (!string.IsNullOrEmpty(this.Sex))
                    {
                        if (this.Sex == "Male" && (category.CAT == "D" || category.CAT == "E"))
                        {
                            SystemMessage.Write(MessageType.Issue,"Animal is a male. You cannot change to a female category");

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                this.EntitySelectionChange = true;
                                this.SelectedCategory = previousCat;
                                this.EntitySelectionChange = false;
                            }));

                            return;
                        }

                        if (this.Sex == "Female" && (category.CAT == "A" || category.CAT == "B" || category.CAT == "C"))
                        {
                            SystemMessage.Write(MessageType.Issue, "Animal is a female. You cannot change to a male category");

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                this.EntitySelectionChange = true;
                                this.SelectedCategory = previousCat;
                                this.EntitySelectionChange = false;
                            }));

                            return;
                        }
                    }
                }

                if (category.CAT == "A" && this.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
                {
                    var message = string.Format(Message.InvalidCategoryAge, ApplicationSettings.YoungBullMaxAge);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.IgnoreCategoryCheck = true;
                        this.SelectedCategory = previousCat;
                        this.IgnoreCategoryCheck = false;
                    }));
                }

                var localSex = this.DepartmentBodiesManager.FindSex(category.CAT);
                this.Sex = localSex;
                //if (string.IsNullOrEmpty(this.Sex))
                //{
                //    this.Sex = localSex;
                //}
                //else
                //{
                //    var isValidSex = this.DepartmentBodiesManager.IsValidSex(category.CAT, this.Sex);
                //    if (!isValidSex)
                //    {
                //        var message = string.Format(Message.InvalidCategorySexMatch, category.CAT, this.Sex);
                //        SystemMessage.Write(MessageType.Issue, message);
                //        NouvemMessageBox.Show(message);

                //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                //        {
                //            this.IgnoreCategoryCheck = true;
                //            this.SelectedCategory = previousCat;
                //            this.IgnoreCategoryCheck = false;
                //        }));

                //        return;
                //    }
                //}
            }
            finally
            {
                this.CurrentMode = localMode;

                if (this.KillType == KillType.Sheep)
                {
                    if (ApplicationSettings.ManuallySequenceSheep)
                    {
                        this.SetControlMode(ControlMode.Add);
                    }
                }
            }
        }

        /// <summary>
        /// Uk passport barcode parser.
        /// </summary>
        /// <param name="scannedData">The scanned data to parse.</param>
        protected void HandleEUScannedPassportData(string scannedData)
        {
            // remove the 01
            var localData = scannedData.Substring(0, scannedData.Length - 2).Trim();

            var data = string.Empty;
            if (localData.Length == 28)
            {
                data = localData.Substring(localData.Length - 14, 14);
            }
            else if (localData.Length == 27)
            {
                data = localData.Substring(localData.Length - 13, 13);
            }
            else if (localData.Length == 26)
            {
                data = localData.Substring(localData.Length - 12, 12);
            }
            else
            {
                data = localData.Substring(localData.Length - 11, 11);
            }


            //  FR56282  5167016122012FHF   01
            //  DE13046  9681402082013FHF   01
            //  NL5288   4355027112008FHF   01
            //  DK011993 0311922022013FDR   01
            //  UK20024350240729032010FBRBX 01
            //  IE28112083253220022011FBF   01
            //  UK10556240083330092015FLIMX 01



            // UK14302020007023032011MLIM  01
            var tag = localData.Substring(0, 14).Trim();
            var existingTag = this.DataManager.IsEartagInSystem(tag);
            if (existingTag > 0)
            {
                var message = string.Format(Message.EartagAlreadyInSystem, tag, existingTag);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message);
                return;
            }

            this.Eartag = tag;
            var localScannedBreed = data.Substring(9, data.Length - 9);
            var localBreed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localScannedBreed));
            if (localBreed == null)
            {
                // not in the system, so add it.
                var breedToAdd = new NouBreed { Name = localScannedBreed };
                this.DataManager.AddBreed(breedToAdd);
                this.GetBreeds();
                this.Breed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localScannedBreed));
            }
            else
            {
                this.Breed = localBreed;
            }

            var localSex = data.Substring(8, 1);
            if (localSex.CompareIgnoringCase(Constant.M))
            {
                this.Sex = Strings.Male;
            }
            else
            {
                this.Sex = Strings.Female;
            }

            this.DOB = data.Substring(0, 8).ToDate();
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

            if (this.SelectedCategory != null)
            {
                if (!this.CheckCategory(this.SelectedCategory))
                {
                    return;
                }
            }

            this.AddTransaction();
        }

        ///// <summary>
        ///// Uk passport barcode parser.
        ///// </summary>
        ///// <param name="data">The scanned data to parse.</param>
        //protected void HandleUKScannedPassportData(string scannedData)
        //{
        //    var localData = scannedData.Split(' ');
        //    var data = scannedData;
        //    if (localData.Length > 1)
        //    {
        //        data = localData.First();
        //    }
        //   // UK20024350240729032010FBRBX 01

        //   // UK14302020007023032011MLIM  01
        //    var tag = data.Substring(0, 14);
        //    var existingTag = this.DataManager.IsEartagInSystem(tag);
        //    if (existingTag > 0)
        //    {
        //        var message = string.Format(Message.EartagAlreadyInSystem, tag, existingTag);
        //        SystemMessage.Write(MessageType.Issue, message);
        //        NouvemMessageBox.Show(message);
        //        return;
        //    }

        //    this.Eartag = tag;
        //    var localScannedBreed = data.Substring(23, data.Length - 23);
        //    var localBreed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localScannedBreed));
        //    if (localBreed == null)
        //    {
        //        // not in the system, so add it.
        //        var breedToAdd = new NouBreed { Name = localScannedBreed };
        //        this.DataManager.AddBreed(breedToAdd);
        //        this.GetBreeds();
        //        this.Breed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localScannedBreed));
        //    }
        //    else
        //    {
        //        this.Breed = localBreed;
        //    }

        //    var localSex = data.Substring(22, 1);
        //    if (localSex.CompareIgnoringCase(Constant.M))
        //    {
        //        this.Sex = Strings.Male;
        //    }
        //    else
        //    {
        //        this.Sex = Strings.Female;
        //    }

        //    this.DOB = data.Substring(14, 8).ToDate();
        //    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

        //    if (this.SelectedCategory != null)
        //    {
        //        if (!this.CheckCategory(this.SelectedCategory))
        //        {
        //           return;
        //        }
        //    }

        //    this.AddTransaction();
        //}

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="data">The scanned passport data barcode.</param>
        protected override void HandleScannedPassportData(string data)
        {
            if (this.EntitySelectionChange || ApplicationSettings.UsingAims)
            {
                return;
            }

            if (data.Last().ToString().CompareIgnoringCase(Constant.MixedBreed))
            {
                // mixed breed (scanned data ends in X, 3 characters long)
                var localMixedBreed = data.Substring(data.Length - 3, 3);
                var localBreed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localMixedBreed));
                if (localBreed == null)
                {
                    // not in the system, so add it.
                    var breedToAdd = new NouBreed { Name = localMixedBreed };
                    this.DataManager.AddBreed(breedToAdd);
                    this.GetBreeds();
                    this.Breed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localMixedBreed));
                }
                else
                {
                    this.Breed = localBreed;
                }

                var localSex = data.Substring(data.Length - 4, 1);
                if (localSex.CompareIgnoringCase(Constant.M))
                {
                    this.Sex = Strings.Male;
                }
                else
                {
                    this.Sex = Strings.Female;
                }
            }
            else
            {
                // pure breed (scanned data doesn't end in X, 2 characters long)
                var localPureBreed = data.Substring(data.Length - 2, 2);
                var localBreed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localPureBreed));
                if (localBreed == null)
                {
                    // not in the system, so add it.
                    var breedToAdd = new NouBreed { Name = localPureBreed };
                    this.DataManager.AddBreed(breedToAdd);
                    this.GetBreeds();
                    this.Breed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(localPureBreed));
                }
                else
                {
                    this.Breed = localBreed;
                }

                var localSex = data.Substring(data.Length - 3, 1);
                if (localSex.CompareIgnoringCase(Constant.M))
                {
                    this.Sex = Strings.Male;
                }
                else
                {
                    this.Sex = Strings.Female;
                }
            }

            this.DOB = data.Substring(0, 8).ToDate();
            this.HerdNo = string.Empty;
            Messenger.Default.Send(Token.Message, Token.FocusToHerdNo);

            if (this.SelectedCategory != null)
            {
                if (!this.CheckCategory(this.SelectedCategory))
                {
                    return;
                }
            }

            this.AddTransaction();
        }

        /// <summary>
        /// Searches for a lairage order.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Validates a time (must be HH:MM)
        /// </summary>
        /// <param name="time">The time to validate.</param>
        /// <returns>Flag, as to valid time or not.</returns>
        protected override bool ValidateTime(string time)
        {
            if (string.IsNullOrEmpty(time))
            {
                return true;
            }

            if (!time.IsValidHoursAndMins())
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidTransitTime);
                NouvemMessageBox.Show(Message.InvalidTransitTime);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a lairage intake.
        /// </summary>
        protected override void CreateSale()
        {
            base.CreateSale();

            var localCategory = this.SelectedCategory;
            if (this.SelectedKillType.Name.CompareIgnoringCase("Beef"))
            {
                if (this.aimsCarcass?.Category != null)
                {
                    localCategory = this.aimsCarcass.Category;
                }
            }

            this.Sale.KillType = this.KillType.ToString();
            this.Sale.HerdQAS = this.HerdFarmAssured.YesNoToBool();
            this.Sale.Haulier = this.SelectedHaulier;
            this.Sale.Agent = this.SelectedAgent;
            this.Sale.HaulageCharge = this.HaulierCharge;
            this.Sale.TimeInTransit = this.TimeInTransit;
            this.Sale.DeliveryTime = this.DeliveryTime;
            this.Sale.PermitNo = this.PermitNo;
            this.Sale.LotAuthorisedDate = this.LotAuthorised ? DateTime.Now : (DateTime?)null;
            this.Sale.FileTransferedDate = this.FileTransfered ? DateTime.Now : (DateTime?)null;
            this.Sale.CustomerID = this.SelectedLairageCustomer != null && this.SelectedLairageCustomer.BPMasterID > 0  ? this.SelectedLairageCustomer.BPMasterID : (int?)null;

            if (this.SelectedHerd != null)
            {
                this.Sale.Reference = this.SelectedHerd.HerdNumber;
            }
            else
            {
                this.Sale.Reference = this.HerdNo ?? string.Empty;
            }

            var localINMasterID = this.BeefIntakeProductID;
            if (this.SelectedKillType.Name.CompareIgnoringCase("Sheep"))
            {
                localINMasterID = this.SheepIntakeProductID;
            }
            else if (this.SelectedKillType.Name.CompareIgnoringCase("Pig"))
            {
                localINMasterID = this.PigIntakeProductID;
            }

            var saleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localINMasterID);
            if (saleDetail == null)
            {
                saleDetail = new SaleDetail { INMasterID = localINMasterID };
                saleDetail.SetInventoryItem();
                this.SaleDetails.Add(saleDetail);
            }
            else
            {
                this.SaleDetails.Clear();
                this.SaleDetails.Add(saleDetail);
            }

            if (localCategory != null)
            {
                if (ApplicationSettings.ResettingVealCategoriesAtSequencer && this.KillType == KillType.Beef)
                {
                    if (this.AgeInMonths < ApplicationSettings.YoungVealAge)
                    {
                        this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CAT == "V");
                        localCategory = this.Categories.FirstOrDefault(x => x.CAT == "V");
                    }
                    else if (this.AgeInMonths < ApplicationSettings.OldVealAge)
                    {
                        this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CAT == "Z");
                        localCategory = this.Categories.FirstOrDefault(x => x.CAT == "Z");
                    }
                }

                var catId = this.DataManager.GetMappedProductCategory(localCategory);
                if (catId > 0)
                {
                    localINMasterID = catId;
                }

                if (localCategory != null)
                {
                    this.Sex = this.DepartmentBodiesManager.FindSex(localCategory.CAT);
                }
            }

            saleDetail.QuantityOrdered = this.KillNumberOrdered;
            saleDetail.StockTransactionDetails = new List<StocktransactionDetail>();
            var localBatch = saleDetail.Batch;
            if (saleDetail.Batch == null)
            {
                localBatch = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                saleDetail.Batch = localBatch;
            }

            var customerId = this.SelectedCustomer != null && !this.SelectedCustomer.Name.Equals(Strings.NoCustomer) ? this.SelectedCustomer.BPMasterID : (int?)null;

            if (!string.IsNullOrWhiteSpace(this.Eartag) && this.Eartag.Length >= 2)
            {
                if (this.CountryOfOrigin == null)
                {
                    var code = this.Eartag.Substring(0, 2);
                    var localCountry = this.GetCountryOfOrigin(code);
                    this.CountryOfOrigin = this.Countries.FirstOrDefault(x => x.Name == localCountry);
                }

                if (this.CountryOfOrigin != null && this.RearedIn == null)
                {
                    this.RearedIn = this.Countries.FirstOrDefault(x => x.Name == this.CountryOfOrigin.Name);
                }
            }

            var trans = new StockTransaction
            {
                BatchNumberID = localBatch.BatchNumberID,
                MasterTableID = saleDetail.SaleDetailID,
                TransactionDate = DateTime.Now,
                TransactionQTY = 1,
                IsBox = true,
                INMasterID = localINMasterID,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId,
                WarehouseID = NouvemGlobal.WarehouseIntakeId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                BPMasterID = customerId
            };

            var localFreezerType = this.SelectedFreezerType != null && this.SelectedFreezerType.Data != Strings.NoneSelected ? this.SelectedFreezerType.Data : string.Empty;
            if (this.FreezerIntakeNumber > 0)
            {
                localFreezerType = Strings.Full;
            }

            var mAnds = string.Empty;
            if (this.IsWelsh)
            {
                mAnds = "Welsh";
            }
            else if (this.IsOrganic)
            {
                mAnds = "Organic";
            }
            else if (this.IsSelectFarm)
            {
                mAnds = "SelectFarm";
            }
            else if (this.IsContinental)
            {
                mAnds = "Continental";
            }

            var cleanlinessId = this.SelectedCleanliness != null ? this.SelectedCleanliness.NouCleanlinessID : (int?)null;
            var attribute = new Nouvem.Model.DataLayer.Attribute
            {
                Eartag = this.Eartag,
                Herd = this.HerdNo,
                DOB = this.DOB,
                DateOfLastMove = this.LastMoveDate,
                AgeInMonths = this.AgeInMonths,
                AgeInDays = this.AgeInDays,
                Category = localCategory != null ? localCategory.CategoryID : (int?)null,
                FreezerType = localFreezerType,
                Breed = this.Breed != null ? this.Breed.NouBreedID : (int?)null,
                Sex = this.Sex,
                FarmAssured = this.FarmAssured.YesNoToBool(),
                Clipped = this.Clipped.YesNoToBool(),
                Casualty = this.Casualty.YesNoToBool(),
                Imported = this.Imported.YesNoToBool(),
                Lame = this.Lame.YesNoToBool(),
                Customer = customerId,
                SupplierID = this.SelectedPartner.BPMasterID,
                Cleanliness = cleanlinessId,
                Generic1 = this.Generic1,
                CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty,
                RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty,
                KillType = this.KillType.ToString(),
                CurrentResidency = this.DaysOfResidencyCurrent,
                TotalResidency = this.DaysOfResidencyTotal,
                NoOfMoves = this.NumberOfMoves,
                HoldingNumber = this.HoldingNumber,
                Attribute120 = this.Attribute120,
                Attribute199 = this.ThirdParty,
                Attribute200 = mAnds,
                Attribute210 = this.Attribute210,
                Attribute220 = this.Attribute220,
                SequencedDate = this.SelectedStockDetail?.SequencedDate,
                RPA = this.SelectedStockDetail != null && this.SelectedStockDetail.RPA,
                Total = this.SelectedStockDetail != null && this.SelectedStockDetail.TotalExclVat.HasValue ? this.SelectedStockDetail.TotalExclVat : null
            };

            var detail = new StocktransactionDetail { StockTransaction = trans, Attribute = attribute, AimsData = this.aimsData, Movements = this.Movements };

            saleDetail.KillQuantityReceived = saleDetail.KillQuantityReceived.ToInt() + 1;
            saleDetail.StockTransactionDetails.Add(detail);
            this.Sale.SaleDetails = this.SaleDetails;

            var stockDetail = new StockDetail
            {
                Eartag = this.Eartag,
                HerdNo = this.HerdNo,
                DOB = this.DOB,
                AgeInMonths = this.AgeInMonths,
                AgeInDays = this.AgeInDays,
                Category = localCategory,
                DateOfLastMove = this.LastMoveDate,
                Breed = this.Breed,
                Sex = this.Sex,
                FarmAssured = this.FarmAssured.YesNoToBool(),
                FreezerType = localFreezerType,
                Clipped = this.Clipped.YesNoToBool(),
                Casualty = this.Casualty.YesNoToBool(),
                Imported = this.Imported.YesNoToBool(),
                Lame = this.Lame.YesNoToBool(),
                CustomerID = customerId,
                Cleanliness = this.SelectedCleanliness,
                CurrentResidency = this.DaysOfResidencyCurrent,
                TotalResidency = this.DaysOfResidencyTotal,
                NumberOfMoves = this.NumberOfMoves,
                Generic1 = this.Generic1,
                AimsData = this.aimsData,
                ThirdParty = this.ThirdParty,
                Attribute210 = this.Attribute210,
                Attribute120 = this.Attribute120,
                Attribute220 = this.Attribute220,
                CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty,
                RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty,
                HoldingNumber = this.HoldingNumber,
                Movements = this.Movements,
                RPA = this.SelectedStockDetail != null && this.SelectedStockDetail.RPA,
                TotalExclVat = this.SelectedStockDetail != null && this.SelectedStockDetail.TotalExclVat.HasValue ? this.SelectedStockDetail.TotalExclVat : null
            };

            this.IgnoreStockDetailEdit = true;
            var index = 0;
            if (this.CurrentMode == ControlMode.Update && this.SelectedStockDetail != null)
            {
                attribute.AttributeID = this.SelectedStockDetail.AttributeID.ToInt();
                index = this.StockDetails.ToList().IndexOf(this.SelectedStockDetail);
                this.StockDetails.Remove(this.SelectedStockDetail);
            }

            this.SetControlMode(ControlMode.Add);
            this.StockDetails.Insert(index, stockDetail);
            this.SelectedStockDetail = this.StockDetails.ElementAt(index);
            this.IgnoreStockDetailEdit = false;
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override async void AddSale()
        {
            this.Log.LogDebug(this.GetType(), "AddSale(): adding a new order");
            this.UpdateDocNumbering();
            var receipt = this.DataManager.AddLairageIntake(this.Sale);
            var newSaleId = receipt.Item1;
            var transactionId = receipt.Item2;

            if (newSaleId > 0)
            {
                if (this.IntakeNumber == null || this.IntakeNumber == 1)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.GoodsInCreated, this.NextNumber));
                }

                if (ApplicationSettings.DoNotResetLairageAgent)
                {
                    this.SetAgent();
                }

                this.Sale.SaleID = newSaleId;

                if (this.SelectedStockDetail != null)
                {
                    this.SelectedStockDetail.AttributeID = transactionId;
                    this.SelectedStockDetail.StockTransactionID = this.Sale.ReturnedStockTransactionId;
                }

                var localCategory = this.SelectedCategory;

                if (this.KillType == KillType.Beef || !ApplicationSettings.ManuallySequenceSheep)
                {
                    this.ClearAttributes();
                }

                this.SetTotalKillReceived(this.StockDetails.Count);
                this.SetControlMode(ControlMode.OK);

                if (this.KillType == KillType.Sheep && !ApplicationSettings.ManuallySequenceSheep)
                {
                    this.SelectedCategory = localCategory;
                    if (!ApplicationSettings.UsingSheepWand)
                    {
                        this.Locator.Sequencer.AddSheepToQueue(transactionId);
                    }
              
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }

                await this.HandleSelectedTabItem(this.selectedTabItem);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.GoodsInNotCreated);
            }
        }

        protected virtual void HandleTouchscreenEntry(string eartag) { }

        /// <summary>
        /// Handles an attribute value selectionn.
        /// </summary>
        /// <param name="c">The attribute selection data.</param>
        protected virtual void HandleAttributeValueSelection(CollectionData c)
        {
            if (c.Identifier.Equals(Constant.TraceabilityAttribute) && this.aimsCarcass != null)
            {
                var category = this.Categories.FirstOrDefault(x => x.CategoryID == c.ID);
                if (category != null)
                {
                    this.aimsCarcass.Category = category;
                }

                this.HandleAimsCarcassCategorySelection();
            }
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            string error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var result = this.DataManager.UpdateLairageIntake(this.Sale);
            var success = result.Item1;
            var transactionId = result.Item2;

            if (success)
            {
                if (this.SelectedStockDetail != null)
                {
                    this.SelectedStockDetail.AttributeID = transactionId;
                    this.SelectedStockDetail.StockTransactionID = this.Sale.ReturnedStockTransactionId;
                }

                if (this.IntakeNumber == null || this.IntakeNumber == 1)
                {
                    SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                }

                var localCategory = this.SelectedCategory;
                if (ApplicationSettings.DoNotResetLairageAgent)
                {
                    this.SetAgent();
                }

                if (this.KillType == KillType.Beef || !ApplicationSettings.ManuallySequenceSheep)
                {
                    this.ClearAttributes();
                }

                this.SetTotalKillReceived(this.StockDetails.Count);

                if (this.KillType == KillType.Sheep && !ApplicationSettings.ManuallySequenceSheep)
                {
                    this.SelectedCategory = localCategory;
                    if (!ApplicationSettings.UsingSheepWand)
                    {
                        this.Locator.Sequencer.AddSheepToQueue(transactionId);
                    }

                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.GoodsInNotCreated, this.NextNumber));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// reset the attributes.
        /// </summary>
        protected override void ClearAttributes()
        {
            base.ClearAttributes();
            this.InwardMovementReferenceId = string.Empty;
            this.MoveToLairageReference = string.Empty;
            this.Movements = null;
            this.Attribute210 = Constant.No;
            this.Attribute120 = Constant.No;
        }

        /// <summary>
        /// Override, to handle a control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddTransaction();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:

                    if (this.lotsToUpdate.Any())
                    {
                        this.HandleLotQueue();
                        return;
                    }

                    if (this.Sale == null)
                    {
                        return;
                    }

                    if (this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                        && this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                    {
                        this.SetDocStatus(NouvemGlobal.NouDocStatusComplete.NouDocStatusID);
                    }
                    else if (this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusActive.NouDocStatusID
                        && this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                    {
                        this.SetDocStatus(this.SelectedDocStatus.NouDocStatusID);
                    }
                    else if (this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID
                         && this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                    {
                        this.SetDocStatus(this.SelectedDocStatus.NouDocStatusID);
                    }
                    else if (this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusMoved.NouDocStatusID
                                           && this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusMoved.NouDocStatusID)
                    {
                        this.SetDocStatus(this.SelectedDocStatus.NouDocStatusID);
                    }
                    else
                    {
                        var id = this.Sale.SaleID;
                        var paymentStatus = this.DataManager.GetLairagePaymentStatus(id);
                        if (paymentStatus.HasValue)
                        {
                            var msg = string.Format(Message.PaymentCreated, paymentStatus);
                            SystemMessage.Write(MessageType.Issue, msg);
                            NouvemMessageBox.Show(msg);
                            return;
                        }

                        this.UpdatePrices();
                        if (this.SelectedPartner != null && this.Sale != null)
                        {
                            if (this.SelectedStockDetail != null)
                            {
                                if (this.SelectedStockDetail != null)
                                {
                                    this.SelectedStockDetail.SequencedDate =
                                        this.DataManager.GetAnimalSequenceDate(this.SelectedStockDetail.AttributeID
                                            .ToInt());
                                }

                                this.AddTransaction();
                            }

                            this.Sale.DeliveryDate = this.DeliveryDate;
                            this.Sale.DocumentDate = this.DocumentDate;
                            this.Sale.ProposedKillDate = this.ProposedKillDate;
                            this.Sale.Haulier = this.SelectedHaulier;
                            this.Sale.HaulageCharge = this.HaulierCharge;
                            this.Sale.TimeInTransit = this.TimeInTransit;
                            this.Sale.DeliveryTime = this.DeliveryTime;
                            this.Sale.PermitNo = this.PermitNo;
                            this.Sale.Agent = this.SelectedAgent;
                            this.SetAgent();
                            this.Sale.CustomerID = this.SelectedLairageCustomer != null && this.SelectedLairageCustomer.BPMasterID > 0 ? this.SelectedLairageCustomer.BPMasterID : (int?)null;
                            if (this.IsWelsh)
                            {
                                this.Attribute200 = "Welsh";
                            }
                            else if (this.IsOrganic)
                            {
                                this.Attribute200 = "Organic";
                            }
                            else if (this.IsSelectFarm)
                            {
                                this.Attribute200 = "SelectFarm";
                            }
                            else if (this.IsContinental)
                            {
                                this.Attribute200 = "Continental";
                            }
                            else
                            {
                                this.Attribute200 = string.Empty;
                            }

                            this.Sale.Attribute = this.Attribute200;
                            this.Sale.Attribute2 = this.Generic1;
                            this.Sale.LotAuthorisedDate = this.LotAuthorised ? DateTime.Now : (DateTime?)null;
                            this.Sale.FileTransferedDate = this.FileTransfered ? DateTime.Now : (DateTime?)null;
                       
                            //this.Sale.CustomerID = this.SelectedCustomer?.BPMasterID;
                            
                            this.DataManager.UpdateLairageIntakeSupplier(this.Sale, this.SelectedPartner.BPMasterID);

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                            }));

                            if (this.KillType == KillType.Beef && this.Sale != null
                                && this.Sale.SaleID > 0 && this.StockDetails != null && this.StockDetails.Any())
                            {
                                //this.ReCheckQas();
                            }
                        }

                        this.Sale = this.DataManager.GetLairageIntakeById(id);
                    }

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handles a herd selection.
        /// </summary>
        protected override void ReCheckQas()
        {
            if (!this.UsingAims)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.RecheckingQA);
            System.Threading.Thread.Sleep(50);

            try
            {
                ProgressBar.SetUp(0, this.StockDetails.Count * 5);
                foreach (var stockDetail in this.StockDetails)
                {
                    stockDetail.FarmAssured = this.HandleEartagEntryForEdit(stockDetail.Eartag);
                }
            }
            finally
            {
                ProgressBar.Reset();
            }

            this.DataManager.UpdateFarmAssured(this.StockDetails);
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                SystemMessage.Write(MessageType.Priority, Message.AimsRechecked);
            }));
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            //var orderStatuses = this.DocStatusesOpen;
            //if (ApplicationSettings.SalesSearchShowAllOrders)
            //{
            //    orderStatuses = this.DocStatusesAll;
            //    this.CustomerSales = 
            //        this.DataManager.GetAllLairageIntakes(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            //}
            //else
            //{
            //    this.CustomerSales = 
            //        this.DataManager.GetAllLairageIntakes(orderStatuses, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            //}

            //this.SetActiveDocument(this);
            Messenger.Default.Send(ViewType.LairageSearch);
            this.Locator.LairageSearch.Mode = ViewType.LairageIntake;

            //if (this.Sale != null)
            //{
            //    this.Locator.SalesSearchData.SetView(ViewType.LairageIntake);
            //}
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.LairageIntake)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Handles a herd selection.
        /// </summary>
        protected override void HandleSelectedHerd()
        {
            if (this.UsingBordBia && !this.EntitySelectionChange && this.KillType == KillType.Beef)
            {
                var herdNumber = string.Empty;
                if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                {
                    if (string.IsNullOrEmpty(this.HoldingNumber))
                    {
                        return;
                    }

                    herdNumber = this.HoldingNumber;
                }
                else
                {
                    if (this.SelectedHerd == null)
                    {
                        return;
                    }

                    herdNumber = this.SelectedHerd.HerdNumber;
                }

                try
                {
                    ProgressBar.SetUp(0, 3);
                    ProgressBar.Run();
                    System.Threading.Thread.Sleep(50);
                    var status = this.DepartmentBodiesManager.IsHerdApproved(herdNumber);
                    if (status.IsValidCert)
                    {
                        this.HerdFarmAssured = Constant.Yes;
                        SystemMessage.Write(MessageType.Priority, status.Message);
                    }
                    else
                    {
                        this.HerdFarmAssured = Constant.No;
                        SystemMessage.Write(MessageType.Issue, status.Message);
                    }
                }
                finally
                {
                    ProgressBar.Reset();
                }
            }
        }

        /// <summary>
        /// Handle the eartag entry.
        /// </summary>
        protected override void HandleEartagEntry()
        {
            if (!string.IsNullOrWhiteSpace(this.Eartag))
            {
                this.SetControlMode(ControlMode.Add);
            }
        
            if (string.IsNullOrWhiteSpace(this.Eartag) || this.EntitySelectionChange || this.SelectedKillType == null || !this.SelectedKillType.Name.CompareIgnoringCase("BEEF"))
            {
                return;
            }

            this.SetControlMode(ControlMode.Add);
            this.aimsData = null;
            this.Movements = null;

            var tag = this.Eartag;
            if (this.ProposedKillDate == null)
            {
                var error = Message.NoProposedKillDateEntered;
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error, touchScreen: true);
                this.Eartag = string.Empty;
                return;
            }

            if (this.Eartag.Length == 12 && this.Eartag.IsNumericSequence())
            {
                // Old Irish eartag only scan
                tag = string.Format("IE{0}", this.Eartag);
            }

            var existingTag = this.DataManager.IsEartagInSystem(tag);
            if (existingTag > 0)
            {
                var message = string.Format(Message.EartagAlreadyInSystem, tag, existingTag);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message);
                this.Eartag = string.Empty;
                return;
            }

            if (this.KillType == KillType.Beef && this.UsingAims && !this.EntitySelectionChange)
            {
                if (!this.UsingBordBia)
                {
                    NouvemMessageBox.Show(string.Format(Message.HerdCertQueryTurnedOff,this.HerdCheckDept), NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }
                }

                var presentingHerdNo = this.SelectedHerd != null ? this.SelectedHerd.HerdNumber : string.Empty;
                if (ApplicationSettings.UsingUKDepartmentServiceQueries && !string.IsNullOrEmpty(this.HoldingNumber))
                {
                    presentingHerdNo = this.HoldingNumber;
                }

                var herdQA = this.HerdFarmAssured.YesNoToNonNullableBool();
                this.aimsCarcass = new StockDetail { Eartag = tag, HerdFarmAssured = herdQA, PresentingHerdNo = presentingHerdNo };
                if (this.SelectedCustomer != null && !this.SelectedCustomer.Name.CompareIgnoringCase(Strings.NoCustomer))
                {
                    this.aimsCarcass.CustomerID = this.SelectedCustomer.BPMasterID;
                }

                if (this.SelectedPartner != null)
                {
                    this.aimsCarcass.SupplierID = this.SelectedPartner.BPMasterID;
                }

                try
                {
                    ProgressBar.SetUp(0, 5);
                    ProgressBar.Run();
                    System.Threading.Thread.Sleep(100);

                    this.aimsCarcass.DontQueryHerdCert = !this.UsingBordBia;
                    this.DepartmentBodiesManager.CallAim(this.aimsCarcass);
                    this.aimsData = this.aimsCarcass.AimsData;
                    if (this.aimsData != null)
                    {
                        if (this.aimsData.NouCategoryID.HasValue)
                        {
                            this.aimsCarcass.Category = this.Categories.FirstOrDefault(x => x.CategoryID == this.aimsData.NouCategoryID);
                            this.IgnoreCategoryCheck = true;
                            this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == this.aimsData.NouCategoryID);
                            this.IgnoreCategoryCheck = false;
                            this.Log.LogInfo(this.GetType(), $"AIMS - Eartag:{this.Eartag}, AIMS Category:{this.aimsData.NouCategoryID}, Sex:{this.aimsData.Sex}, Category:{this.aimsCarcass?.Category?.CategoryID}");
                        }

                        this.DeptResponseXml = this.aimsData.ResponseXml;
                        this.DataManager.AddAIMsData(this.aimsCarcass.AimsData);
                    }

                    if (!string.IsNullOrEmpty(this.aimsCarcass.Error))
                    {
                        this.Log.LogError(this.GetType(), string.Format("AIMS Error : {0}", this.aimsCarcass.Error));
                        SystemMessage.Write(MessageType.Issue, this.aimsCarcass.Error);
                        NouvemMessageBox.Show(this.aimsCarcass.Error);
                    }
                    else
                    {
                        this.Movements = new ObservableCollection<FarmMovement>(this.aimsCarcass.Movements);

                        if (string.IsNullOrEmpty(this.HoldingNumber))
                        {
                            this.HoldingNumber = this.aimsCarcass.HoldingNumber;
                        }

                        if (ApplicationSettings.ResetCategoryAtSequencer)
                        {
                            this.aimsCarcass.Category = null;
                            this.SelectedCategory = null;
                        }

                        this.aimsCarcass.Sex = this.aimsCarcass.Sex.CompareIgnoringCase("M") ? Strings.Male : Strings.Female;
                        var localBreed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(this.aimsCarcass.BreedName));
                        if (localBreed == null)
                        {
                            // not in the system, so add it.
                            var breedToAdd = new NouBreed { Name = this.aimsCarcass.BreedName };
                            this.DataManager.AddBreed(breedToAdd);
                            this.GetBreeds();
                            this.aimsCarcass.Breed = this.Breeds.FirstOrDefault(x => x.Name.CompareIgnoringCase(this.aimsCarcass.BreedName));
                        }
                        else
                        {
                            this.aimsCarcass.Breed = localBreed;
                        }

                        if (this.SelectedCategory != null && this.aimsCarcass.Category == null)
                        {
                            if (!this.CheckManuallyEnteredCategory(this.SelectedCategory, this.aimsCarcass))
                            {
                                return;
                            }

                            this.aimsCarcass.Category = this.SelectedCategory;
                            this.Log.LogInfo(this.GetType(), $"Manually selecting category - Eartag:{this.Eartag}, Category:{this.aimsCarcass.Category?.CategoryID}");
                        }

                        if (this.SelectedFreezerType != null)
                        {
                            this.aimsCarcass.FreezerType = this.SelectedFreezerType.Data;
                        }

                        if (this.Clipped != null)
                        {
                            this.aimsCarcass.Clipped = this.Clipped.YesNoToBool();
                        }

                        if (this.CountryOfOrigin != null)
                        {
                            this.aimsCarcass.CountryOfOrigin = this.CountryOfOrigin.Name;
                        }

                        if (this.RearedIn != null)
                        {
                            this.aimsCarcass.RearedIn = this.RearedIn.Name;
                        }

                        this.aimsCarcass.Generic1 = this.Generic1;

                        #region lairage category selection

                        if (ApplicationSettings.SelectingCategoryAtLairage)
                        {
                            var localCategories = this.Categories.Where(x => x.CatType.CompareIgnoringCase(Constant.Beef)).ToList();
                            if (this.aimsCarcass.CategoryCAT == "C")
                            {
                                localCategories.Clear();
                                if (this.aimsCarcass.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
                                {
                                    var oldBull = this.Categories.FirstOrDefault(x => x.CAT == "B");
                                    localCategories.Add(oldBull);
                                }
                                else
                                {
                                    var youngBull = this.Categories.FirstOrDefault(x => x.CAT == "A");
                                    localCategories.Add(youngBull);
                                }

                                var steer = this.Categories.FirstOrDefault(x => x.CAT == "C");
                                localCategories.Add(steer);

                                var data = (from cat in localCategories
                                            select new CollectionData
                                            {
                                                Data = string.Format("{0} - {1}", cat.CAT, cat.Name),
                                                ID = cat.CategoryID,
                                                Identifier = Constant.TraceabilityAttribute
                                            }).ToList();

                                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                                Messenger.Default.Send(ViewType.CollectionDisplayDesktop);
                                return;
                            }

                            if (ApplicationSettings.LairageType == LairageType.UK && (this.aimsCarcass.CategoryCAT == "D" || this.aimsCarcass.CategoryCAT == "E"))
                            {
                                localCategories.Clear();
                                var cow = this.Categories.FirstOrDefault(x => x.CAT == "D");
                                localCategories.Add(cow);

                                var heifer = this.Categories.FirstOrDefault(x => x.CAT == "E");
                                localCategories.Add(heifer);

                                var data = (from cat in localCategories
                                    select new CollectionData
                                    {
                                        Data = string.Format("{0} - {1}", cat.CAT, cat.Name),
                                        ID = cat.CategoryID,
                                        Identifier = Constant.TraceabilityAttribute
                                    }).ToList();

                                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                                Messenger.Default.Send(ViewType.CollectionDisplayDesktop);
                                return;
                            }
                        }

                        #endregion

                        this.HandleSelectedStockDetail(this.aimsCarcass);
                        this.SetControlMode(ControlMode.Add);
                        this.AddTransaction();
                    }
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    NouvemMessageBox.Show(ex.Message);
                }
                finally
                {
                    this.aimsCarcass = null;
                    ProgressBar.Reset();
                }
            }

            if (this.KillType == KillType.Sheep)
            {
                this.AddTransaction();
            }
        }

        /// <summary>
        /// Handle the aims animal recheck.
        /// </summary>
        protected bool HandleEartagEntryForEdit(string eartag)
        {
            this.aimsData = null;
            this.Movements = null;

            var tag = eartag;
            if (eartag.Length == 12 && eartag.IsNumericSequence())
            {
                // Old Irish eartag only scan
                tag = string.Format("IE{0}", eartag);
            }

            var presentingHerdNo = this.SelectedHerd != null ? this.SelectedHerd.HerdNumber : string.Empty;
            var herdQA = this.HerdFarmAssured.YesNoToNonNullableBool();
            var carcass = new StockDetail { Eartag = tag, HerdFarmAssured = herdQA, PresentingHerdNo = presentingHerdNo };
            try
            {
                ProgressBar.Run();
                System.Threading.Thread.Sleep(50);
                this.DepartmentBodiesManager.CallAim(carcass);
                this.aimsData = carcass.AimsData;

                return carcass.FarmAssured.ToBool();
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Handle a partner change.
        /// </summary>
        protected override void HandlePartnerChange()
        {
            if (!this.EntitySelectionChange && (this.Sale == null || this.Sale.SaleID == 0))
            {
                this.ClearForm(false);
                this.HaulierCharge = null;
                this.SelectedHaulier = null;
            }

            this.PartnerName = this.SelectedPartner.Name;
            this.ParsePartner();
            if (this.SelectedPartner != null && ApplicationSettings.DoNotResetLairageAgent)
            {
                this.SelectedAgent = this.Agents.FirstOrDefault(x => x.BPMasterID == this.SelectedPartner.BPMasterID_Agent);
            }

            this.ShowRadioOptions = ApplicationSettings.LairageRadioButtonOptionsPartnerId.HasValue
                && this.SelectedPartner != null
                && this.SelectedPartner.BPMasterID == ApplicationSettings.LairageRadioButtonOptionsPartnerId;

            if (this.ShowRadioOptions)
            {
                if (this.Attribute200.CompareIgnoringCase("Welsh"))
                {
                    this.IsWelsh = true;
                }
                else if (this.Attribute200.CompareIgnoringCase("Organic"))
                {
                    this.IsOrganic = true;
                }
                else if (this.Attribute200.CompareIgnoringCase("SelectFarm"))
                {
                    this.IsSelectFarm = true;
                }
                else if (this.Attribute200.CompareIgnoringCase("Continental"))
                {
                    this.IsContinental = true;
                }
                else
                {
                    this.IsStandard = true;
                }
            }

            this.CanCopyFrom = true;
        }

        protected override void HandleGeneric1(string oldvalue)
        {
            this.SetMode(this.Generic1, oldvalue);
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                if (this.Sale.BaseDocumentReferenceID.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                    return;
                }

                //this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Gets the associated kill categories.
        /// </summary>
        protected override void HandleKillType()
        {
            if (this.SelectedKillType == null)
            {
                return;
            }

            base.HandleKillType();
            this.GetLairages();
            this.ShowWandButton =
                this.SelectedKillType != null && this.SelectedKillType.Name.CompareIgnoringCase("SHEEP");
            this.SetHerd();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (this.StockDetails != null)
            {
                this.StockDetails.Clear();
            }

            if (this.wandTimer != null)
            {
                this.wandTimer.Tick -= this.WandTimerOnTick;
            }
           
            this.StockDetails = null;
            ViewModelLocator.ClearLairage();
            Messenger.Default.Send(Token.Message, Token.CloseLairageIntakeWindow);
        }

        /// <summary>
        /// cancels the current ui operation.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        protected override void GetCustomers()
        {
            this.Customers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.CustomerPartners
                .Where(x => ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                             || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)) &&
                            !x.Details.Type.StartsWithIgnoringCase(Strings.Haulier))
                .OrderBy(x => x.Details.Name)
                .Select(x => x.Details));
            this.Customers.Insert(0, new ViewBusinessPartner { Name = Strings.NoCustomer });
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected virtual void AddTransaction()
        {
            this.Log.LogDebug(this.GetType(), "AddTransaction(): Adding a transaction");

            #region validation

            var error = string.Empty;

            if (!this.ValidateTime(this.TimeInTransit))
            {
                return;
            }

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoSupplierSelected;
            }
            else if (this.ProposedKillDate == null)
            {
                error = Message.NoProposedKillDateEntered;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                error = Message.LairageCompleted;
            }
            else if (ApplicationSettings.MustSelectAgentAtLairage && this.SelectedAgent == null)
            {
                error = Message.NoAgentSelected;
            }

            if (this.KillType == KillType.Beef)
            {
                if (ApplicationSettings.MaxLairageBeefIntake > 0 &&
                         this.StockDetails.Count == ApplicationSettings.MaxLairageBeefIntake)
                {
                    error = string.Format(Message.LairageIntakeMaxReached, ApplicationSettings.MaxLairageBeefIntake);
                    var localError = error;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(localError);
                    }));
                }
                else if (this.Breed == null)
                {
                    error = Message.NoBreedEntered;
                }
                else if (string.IsNullOrWhiteSpace(this.Eartag))
                {
                    error = Message.NoEartagEntered;
                }
                //else if (string.IsNullOrWhiteSpace(this.Sex))
                //{
                //    error = Message.NoSexEntered;
                //}
                else if (this.DOB.ToDate() < DateTime.Today.AddYears(-30) || this.DOB.ToDate() > DateTime.Today.AddDays(-1))
                {
                    error = Message.InvalidDOB;
                }
            }

            if (this.KillType == KillType.Sheep)
            {
                if (this.SelectedCategory == null)
                {
                    error = Message.NoCategoryEntered;
                }

                if (!ApplicationSettings.ManuallySequenceSheep)
                {
                    if (string.IsNullOrWhiteSpace(this.Eartag))
                    {
                        error = Message.NoEartagEntered;
                    }
                }

                if (ApplicationSettings.ManuallySequenceSheep)
                {
                    if (this.IntakeNumber.IsNullOrZero())
                    {
                        error = Message.NoSheepIntakeNumberEntered;
                    }

                    //if (this.FreezerIntakeNumber.IsNullOrZero())
                    //{
                    //    error = Message.NoFreezerIntakeNumberEntered;
                    //}
                }
            }

            if (this.KillType == KillType.Pig)
            {
                if (this.SelectedCategory == null)
                {
                    error = Message.NoCategoryEntered;
                }

                if (this.IntakeNumber.IsNullOrZero() && this.CurrentMode != ControlMode.Update)
                {
                    error = Message.NoSheepIntakeNumberEntered;
                }
            }

            if (error != string.Empty)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: true);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, error);
                }

                return;
            }

            #endregion

            if (!this.dontFormatTags && ApplicationSettings.UsingSheepWand &&
                this.SelectedKillType.Name.CompareIgnoringCase("Sheep"))
            {
                var localTag = this.FormatWandEartag(this.Eartag);
                if (localTag != string.Empty && localTag != this.Eartag)
                {
                    this.Eartag = localTag;
                }
            }

            var transactionCount = 1;
            if (this.KillType == KillType.Sheep && ApplicationSettings.ManuallySequenceSheep)
            {
                transactionCount = this.IntakeNumber.ToInt();
            }

            if (this.KillType == KillType.Pig && this.CurrentMode != ControlMode.Update)
            {
                transactionCount = this.IntakeNumber.ToInt();
            }

            if (transactionCount > 1)
            {
                ProgressBar.SetUp(1, transactionCount);
            }

            for (int i = 0; i < transactionCount; i++)
            {
                ProgressBar.Run();
                var tag = this.Eartag;
                this.CreateSale();
                if (this.Sale.SaleID == 0)
                {
                    this.AddSale();
                }
                else
                {
                    this.UpdateSale();
                }

                if (this.FreezerIntakeNumber.HasValue)
                {
                    this.FreezerIntakeNumber--;
                }

                this.HandleTouchscreenEntry(tag);
            }

            ProgressBar.Reset();

            if (transactionCount > 1)
            {
                SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                this.ClearAttributes();
            }

            this.SelectedStockDetail = null;
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Handles a selected tab, regenerating the lairages if the lot tab selected.
        /// </summary>
        /// <param name="tab">The tab index.</param>
        private Task HandleSelectedTabItem(int tab)
        {
            return
                Task.Factory.StartNew(() =>
                {
                    if (tab == 2)
                    {
                        Application.Current.Dispatcher.Invoke(this.GetLairages);
                    }
                });
        }

        /// <summary>
        /// Handler for a lot queue no change.
        /// </summary>
        private void HandleLotQueue()
        {
            #region validation

            var ids = this.Lots.Where(x => x.ReferenceID.HasValue && x.ReferenceID != 999).Select(x => x.ReferenceID).ToList();
            var distinctIds = ids.Distinct();
            if (ids.Count() != distinctIds.Count())
            {
                var msg = Message.DuplicateLotQueueNumbers;
                SystemMessage.Write(MessageType.Issue, msg);
                NouvemMessageBox.Show(msg);
                return;
            }

            #endregion

            try
            {
                NouvemMessageBox.Show(Message.LotQueueNoPrompt, NouvemMessageBoxButtons.YesNo);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    if (this.DataManager.UpdateLotQueue(this.lotsToUpdate))
                    {
                        SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                    }
                }
            }
            finally
            {
                this.GetLairages();
                this.lotsToUpdate.Clear();
                this.SetControlMode(this.previousMode);
            }
        }

        /// <summary>
        /// Sets the departmentnames.
        /// </summary>
        private void SetDepartments()
        {
            if (ApplicationSettings.UsingUKDepartmentServiceQueries)
            {
                this.AnimalCheckDept = Strings.CallBcms;
                this.HerdCheckDept = Strings.CallRedTractor;
                return;
            }

            this.AnimalCheckDept = Strings.CallAim;
            this.HerdCheckDept = Strings.CallBordBia;
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.LairageIntake);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        /// <summary>
        /// Updates the prices.
        /// </summary>
        private void UpdatePrices()
        {
            this.DataManager.UpdateLairagePrices(this.StockDetails.Where(x => !x.AttributeID.IsNullOrZero()).ToList());
        }

        //private static int count = 1;
        /// <summary>
        /// Handles the incoming scanner read.
        /// </summary>
        /// <param name="data">The data to parse.</param>
        protected override void HandleScannerData(string data)
        {
            if (this.scanInProgress)
            {
                return;
            }

            this.scanInProgress = true;

#if DEBUG
            //data = "DE05379  72224";
#endif
            // UK20024350240729032010FBRBX 01

            //if (count == 1)
            //{
            //    data = "UK24309020097003032011MLIM  01";
            //}
            //else
            //{
            //    data = "UK24392025007023032011MLIM  01";
            //}
            //count++;


            //data = "281120832532";

            //data = "FR56282  5167016122012FHF   01";
            //DE13046  9681402082013FHF   01
            //data = "IE28112083253220022011FBF   01";
            //UK10556240083330092015FLIMX 01
            //data = "49/316/0104";
            //data = "UK322561400229";
            try
            {
                data = data.Replace("\r", "");
                //data = "09/251/0217";
                this.Log.LogInfo(this.GetType(), string.Format("Lairage passport scan data: |{0}|", data));

                try
                {
                    if (data.Contains("/"))
                    {
                        // holding number scan
                        this.HoldingNumber = data;
                        //if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                        //{
                        //    this.VerifyHerd(this.HoldingNumber);
                        //}

                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                        return;
                    }

                    if (data.Length < 10)
                    {
                        // sheep lairage docket herd number
                        this.SelectedHerd = this.Herds.FirstOrDefault(x => x.HerdNumber.CompareIgnoringCase(data));
                        return;
                    }

                    if (data.Length == 12 && data.IsNumericSequence())
                    {
                        // Irish eartag only scan
                        var tag = string.Format("IE{0}", data);
                        var existingTag = this.DataManager.IsEartagInSystem(tag);
                        if (existingTag > 0)
                        {
                            var message = string.Format(Message.EartagAlreadyInSystem, tag, existingTag);
                            SystemMessage.Write(MessageType.Issue, message);
                            NouvemMessageBox.Show(message);
                            return;
                        }

                        this.Eartag = tag;
                        return;
                    }

                    if (data.Length == 14 && data.StartsWith("UK") && data.Substring(2, data.Length - 2).IsNumericSequence())
                    {
                        // UK eartag only scan
                        var existingTag = this.DataManager.IsEartagInSystem(data);
                        if (existingTag > 0)
                        {
                            var message = string.Format(Message.EartagAlreadyInSystem, data, existingTag);
                            SystemMessage.Write(MessageType.Issue, message);
                            NouvemMessageBox.Show(message);
                            return;
                        }

                        this.Eartag = data;
                        return;
                    }

                    if (data.StartsWith("IE") && !data.EndsWith("01"))
                    {
                        this.HandleScannedPassportData(data);
                        return;
                    }

                    if (data.Length <= 14)
                    {
                        var existingTag = this.DataManager.IsEartagInSystem(data);
                        if (existingTag > 0)
                        {
                            var message = string.Format(Message.EartagAlreadyInSystem, data, existingTag);
                            SystemMessage.Write(MessageType.Issue, message);
                            NouvemMessageBox.Show(message);
                            return;
                        }

                        this.Eartag = data;
                        return;
                    }

                    this.HandleEUScannedPassportData(data);
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), string.Format("HandleScannerData(): {0}", ex.Message));
                    SystemMessage.Write(MessageType.Issue, Message.PassportNotScanned);
                    NouvemMessageBox.Show(Message.PassportNotScanned);
                }
            }
            finally
            {
                this.scanInProgress = false;
            }
        }
        
        /// <summary>
        /// Handles the incoming wand read.
        /// </summary>
        /// <param name="data">The data to parse.</param>
        private void HandleWandData(string data)
        {
            this.Log.LogInfo(this.GetType(), $"Wand Data: |{data}|");
            if (this.scanInProgress)
            {
                return;
            }

            this.scanInProgress = true;

#if DEBUG
            data = "82005D09E407900D|A|030419105806|#82005D09E407900E|A|030419105808|#82005D09E4079010|A|030419105813|#82005D09E407900F|A|030419105819|#8200CEA8FE84B71C|A|030419105821|#8200CEA8FE84B73A|A|030419105822|#8000CEA858B44165|A|030419105823|#8000CEA858B44168|A|030419105825|#8200CEA84AAF69BC|A|030419105826|#8000CEA858B44166|A|030419105827|#8200CEA8FDB09723|A|030419105828|#8200CEA81028D81C|A|030419105829|(rS)";
#endif
            try
            {
                data = data.Replace("\r", "");
                this.Log.LogInfo(this.GetType(), string.Format("Wand data: {0}", data));

                try
                {
                    var eartags = data.Split('#');
                    
                    foreach (var eartag in eartags)
                    {
                        var pos = eartag.IndexOf("|");
                        var hexstring = eartag.Substring(0, pos);
                        var strBinary = hexstring.ToBinary();

                        var strCountry = Convert.ToInt64(strBinary.Substring(16, 10), 2).ToString();
                        if (strCountry.Length > 4)
                        {
                            strCountry = strCountry.Substring(strCountry.Length - 4, 4);
                        }
                        else
                        {
                            strCountry = strCountry.PadLeft(4, '0');
                        }

                        var strAnimal = Convert.ToInt64(strBinary.Substring(26, 38), 2).ToString();
                        if (strAnimal.Length > 12)
                        {
                            strAnimal = strAnimal.Substring(strAnimal.Length - 12, 12);
                        }
                        else
                        {
                            strAnimal = strAnimal.PadLeft(12, '0');
                        }

                        var localCountry = strCountry.StartsWith("0372") || strCountry.StartsWith("372") ? "IE" : "UK";
                        var strDecimalEID = localCountry + " " + strAnimal.Substring(0,7) + " " + strAnimal.Substring(7,5);
                        //var strDecimalEIDISO = strCountry.Substring(strCountry.Length - 3, 3) + " " + strAnimal;
                        //var lngRetagCount = Convert.ToInt64(strBinary.Substring(1, 3), 2).ToString();
                        //var lngSpecies = Convert.ToInt64(strBinary.Substring(4, 5), 2).ToString();

                        //var strFullISOEID = strDecimalEIDISO + " " +
                        //                    lngRetagCount.PadLeft(1, '0') + " " +
                        //                    lngSpecies.PadLeft(2, '0') + " " +
                        //                    Convert.ToInt64(strBinary.Substring(9, 6), 2).ToString().PadLeft(2, '0') + strBinary.Substring(15, 1);

                        //var a = strFullISOEID;
                        this.Eartag = strDecimalEID;
                        this.dontFormatTags = true;
                        this.AddTransaction();
                        this.dontFormatTags = false;
                    }

                    //this.Eartag = "UK 1720987 01724";
                    //this.AddTransaction();
                    //this.Eartag = "UK 1720987 01753";
                    //this.AddTransaction();
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), string.Format("HandleWandData(): {0}", ex.Message));
                    SystemMessage.Write(MessageType.Issue, Message.PassportNotScanned);
                    NouvemMessageBox.Show(Message.PassportNotScanned);
                }
            }
            finally
            {
                this.scanInProgress = false;
                this.wandTimer.Stop();
                try
                {
                    this.HardwareManager.CloseWandPort();
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                    SystemMessage.Write(MessageType.Issue, e.Message);
                }
            }
        }

        private string FormatWandEartag(string eartag)
        {
            // UK173289100216   UK 1732891 00216
            if (eartag == null)
            {
                return string.Empty;
            }

            var formattedTag = string.Empty;
            var tag = eartag.Replace(" ", "");
            if (tag.Length == 14)
            {
                formattedTag = tag.Substring(0, 2).ToUpper() + " " + tag.Substring(2, 7) + " " + tag.Substring(9, 5);
            }

            return formattedTag;
        }

        /// <summary>
        /// Verifys the herd with the departments.
        /// </summary>
        private void VerifyHerdCommandExecute()
        {
            if (this.SelectedHerd == null)
            {
                return;
            }

            ProgressBar.SetUp(0, 3);
            ProgressBar.Run();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                var errors = string.Empty;
                try
                {
                    var bordBiaStatus = this.DepartmentBodiesManager.CheckHerdStatus(this.SelectedHerd.HerdNumber.Trim(), ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword, ApplicationSettings.BordBiaResponsePath);
                    if (bordBiaStatus.Error != string.Empty)
                    {
                        this.Log.LogDebug(this.GetType(), bordBiaStatus.Error);
                        SystemMessage.Write(MessageType.Issue, bordBiaStatus.Error);
                        this.BordBiaHerdApproved = false;
                        errors = "Cannot connect to bord bia";
                    }
                    else
                    {
                        this.BordBiaHerdApproved = bordBiaStatus.IsValidCert;
                    }

                    ProgressBar.Run();
                    var aimsStatus = this.DepartmentBodiesManager.VerifyAimHerdDesignator(this.SelectedHerd.HerdNumber.Trim(), 3, ApplicationSettings.AimsLogPath, "", "", "");
                    if (aimsStatus.Error != string.Empty)
                    {
                        this.Log.LogDebug(this.GetType(), aimsStatus.Error);
                        SystemMessage.Write(MessageType.Issue, aimsStatus.Error);
                        this.AimsHerdApproved = false;
                        errors = string.Format("{0}{1}Cannot connect to AIMS", errors, Environment.NewLine);
                    }
                    else
                    {
                        this.AimsHerdApproved = aimsStatus.Status == Constant.Approved;
                    }

                    ProgressBar.Run();
                    var message = string.Format("Bord Bia herd approval:{0}{1}Aims herd approval:{2}",
                        this.BordBiaHerdApproved.BoolToYesNo(), Environment.NewLine, this.AimsHerdApproved.BoolToYesNo());

                    var localMessage = string.Format("Bord Bia herd approval:{0}  Aims herd approval:{1}",
                        this.BordBiaHerdApproved.BoolToYesNo(), this.AimsHerdApproved.BoolToYesNo());
                    SystemMessage.Write(MessageType.Priority, localMessage);

                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    if (errors != string.Empty)
                    {
                        SystemMessage.Write(MessageType.Issue, errors);
                        NouvemMessageBox.Show(errors);
                    }
                    else
                    {
                        NouvemMessageBox.Show(message, flashMessage: true);
                    }
                }
                finally
                {
                    ProgressBar.Reset();
                }
            }));

            //DepartmentBodiesManager.Instance.QueryAIMForEartag(this.TextBoxHerd.Text);
        }

        /// <summary>
        /// Verifys the herd with the departments.
        /// </summary>
        private void VerifyHerd(string holdingNo)
        {
            if (this.SelectedCustomer == null)
            {
                return;
            }

            ProgressBar.SetUp(0, 2);
            ProgressBar.Run();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                var errors = string.Empty;
                try
                {
                    ProgressBar.Run();
                    var bordBiaStatus = this.DepartmentBodiesManager.CheckHerdStatus(holdingNo.Trim(), ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword, ApplicationSettings.BordBiaResponsePath);
                    if (bordBiaStatus.Error != string.Empty)
                    {
                        this.Log.LogDebug(this.GetType(), bordBiaStatus.Error);
                        SystemMessage.Write(MessageType.Issue, bordBiaStatus.Error);
                        this.BordBiaHerdApproved = false;
                        errors = bordBiaStatus.Error;
                    }
                    else
                    {
                        this.BordBiaHerdApproved = bordBiaStatus.IsValidCert;
                    }

                    this.HerdFarmAssured = this.BordBiaHerdApproved.BoolToYesNo();

                    ProgressBar.Run();
                    var localMessage = string.Format("Red Tractor herd approval:{0}", this.BordBiaHerdApproved.BoolToYesNo());
                    SystemMessage.Write(MessageType.Priority, localMessage);

                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    if (errors != string.Empty)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            SystemMessage.Write(MessageType.Issue, errors);
                            NouvemMessageBox.Show(errors);
                        }));
                    }
                    else
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(localMessage, flashMessage: true);
                        }));
                    }
                }
                finally
                {
                    ProgressBar.Reset();
                }
            }));
        }

        /// <summary>
        /// Updates the doc status.
        /// </summary>
        /// <param name="statusId">The status id.</param>
        private void SetDocStatus(int statusId)
        {
            if (statusId == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
            {
                NouvemMessageBox.Show(Message.IntakeCompletionRequest, NouvemMessageBoxButtons.YesNo);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    this.Sale.NouDocStatusID = statusId;
                    if (this.DataManager.UpdateLairageIntakeStatus(this.Sale))
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                        this.ClearForm();
                        this.SelectedCustomer = null;
                        this.SelectedHerd = null;
                    }
                }

                return;
            }

            if (statusId == NouvemGlobal.NouDocStatusMoved.NouDocStatusID)
            {
                if (ApplicationSettings.DontAllowLairageMove)
                {
                    return;
                }

                if (this.SelectedKillType != null && this.SelectedKillType.Name.CompareIgnoringCase("BEEF"))
                {
                    var msg = ApplicationSettings.LairageType == LairageType.UK
                        ? Message.MoveUKRequest
                        : Message.MovementRequest;
                    NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        try
                        {
                            ProgressBar.SetUp(0, 3);
                            ProgressBar.Run();
                            System.Threading.Thread.Sleep(50);
                            var herdNo = this.SelectedHerd != null ? this.SelectedHerd.HerdNumber : string.Empty;
                            this.aimsData = new AimsData { APGoodsReceiptID = this.Sale.SaleID };
                            var response = this.DepartmentBodiesManager.MoveBatch(this.StockDetails.ToList(), herdNo,
                                this.Sale.Number, this.aimsData);
                            var moveReference = response.Item1;
                            var error = response.Item2;

                            this.DataManager.LogAimsData(this.aimsData);

                            if (error != string.Empty)
                            {
                                this.Log.LogError(this.GetType(), string.Format("Move Request Error:{0}", error));
                                SystemMessage.Write(MessageType.Issue, error);
                                NouvemMessageBox.Show(error);
                                return;
                            }

                            if (!string.IsNullOrWhiteSpace(moveReference))
                            {
                                this.Sale.NouDocStatusID = statusId;
                                this.Sale.OtherReference = moveReference;
                                if (this.DataManager.UpdateLairageIntakeStatus(this.Sale))
                                {
                                    SystemMessage.Write(MessageType.Priority,
                                        string.Format(Message.IntakeMoved, this.Sale.Number));
                                    this.ClearForm();
                                    this.SelectedCustomer = null;
                                    this.SelectedHerd = null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), string.Format("Move Request Error:{0}", ex.Message));
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                            NouvemMessageBox.Show(ex.Message);
                            return;
                        }
                        finally
                        {
                            ProgressBar.Reset();
                        }
                    }

                    return;
                }

                if (this.SelectedKillType != null && this.SelectedKillType.Name.CompareIgnoringCase("SHEEP") && ApplicationSettings.LairageType == LairageType.Ireland)
                {
                    if (string.IsNullOrWhiteSpace(this.PermitNo))
                    {
                        SystemMessage.Write(MessageType.Issue, "No dispatch serial number has been entered");
                        NouvemMessageBox.Show("No dispatch serial number has been entered");
                        return;
                    }

                    NouvemMessageBox.Show(Message.MovementRequest, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        try
                        {
                            ProgressBar.SetUp(0, 3);
                            ProgressBar.Run();
                            System.Threading.Thread.Sleep(50);
                            var herdNo = this.SelectedHerd != null ? this.SelectedHerd.HerdNumber : string.Empty;
                            this.aimsData = new AimsData { APGoodsReceiptID = this.Sale.SaleID };
                            var response = this.DepartmentBodiesManager.MoveSheepBatch(this.StockDetails.ToList(), herdNo,
                                this.Sale.Number, this.aimsData, this.PermitNo);
                            var moveReference = response.Item1;
                            var error = response.Item2;

                            this.DataManager.LogAimsData(this.aimsData);

                            if (error != string.Empty)
                            {
                                this.Log.LogError(this.GetType(), string.Format("Move Request Error:{0}", error));
                                SystemMessage.Write(MessageType.Issue, error);
                                NouvemMessageBox.Show(error);
                                return;
                            }

                            if (!string.IsNullOrWhiteSpace(moveReference))
                            {
                                this.Sale.NouDocStatusID = statusId;
                                this.Sale.OtherReference = moveReference;
                                if (this.DataManager.UpdateLairageIntakeStatus(this.Sale))
                                {
                                    SystemMessage.Write(MessageType.Priority,
                                        string.Format(Message.IntakeMoved, this.Sale.Number));
                                    this.ClearForm();
                                    this.SelectedCustomer = null;
                                    this.SelectedHerd = null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), string.Format("Move Request Error:{0}", ex.Message));
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                            NouvemMessageBox.Show(ex.Message);
                            return;
                        }
                        finally
                        {
                            ProgressBar.Reset();
                        }
                    }

                    return;
                }

                if (ApplicationSettings.LairageType == LairageType.NI)
                {
                    if (string.IsNullOrWhiteSpace(this.PermitNo))
                    {
                        SystemMessage.Write(MessageType.Issue, "No permit number has been entered");
                        NouvemMessageBox.Show("No permit number has been entered");
                        return;
                    }

                    NouvemMessageBox.Show(Message.MovementRequestSheep, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        try
                        {
                            ProgressBar.SetUp(0, 3);
                            ProgressBar.Run();
                            System.Threading.Thread.Sleep(50);
                            this.Sale.Herd = this.SelectedHerd != null ? this.SelectedHerd.HerdNumber : string.Empty;
                            this.Sale.PermitNo = this.PermitNo;
                            this.Sale.StockDetails = this.StockDetails.ToList();

                            this.Log.LogInfo(this.GetType(), $"| Attempting to move sheep to lairage. Herd No:{this.Sale.Herd}, Permit No:{this.Sale.PermitNo} |");
                            if (string.IsNullOrEmpty(this.MoveToLairageReference))
                            {
                                var result = this.DepartmentBodiesManager.MoveSheepBatchToLairage(this.Sale);
                                var error = result.Item1;
                                var reference = result.Item2;

                                if (!string.IsNullOrEmpty(error))
                                {
                                    this.Log.LogError(this.GetType(), $"Move Request to lairage Error:{error}, Reference:{reference}");
                                    SystemMessage.Write(MessageType.Issue, $"Move To Lairage:{error}. AFAIS Ref ID:{reference}");
                                    NouvemMessageBox.Show($"Move To Lairage:{error}. AFAIS Ref ID:{reference}");
                                    return;
                                }

                                this.Sale.MoveToLairageReference = reference;
                                this.DataManager.UpdateLairageIntakeStatus(this.Sale);
                                this.Log.LogInfo(this.GetType(), $"Move to lairage successful:Reference:{reference}");
                            }

                            this.Log.LogInfo(this.GetType(), $"| Attempting to assign batch number to animals: Batch No:{this.Sale.Number}, Permit No:{this.Sale.PermitNo} |");
                            var localResult = this.DepartmentBodiesManager.SheepAssignBatchNumber(this.Sale);
                            var assignError = localResult.Item1;
                            var assignRef = localResult.Item2;
                            if (!string.IsNullOrEmpty(assignError))
                            {
                                this.Log.LogError(this.GetType(), $"Assign Batch Number Error:{assignError}. Reference:{assignRef}");
                                SystemMessage.Write(MessageType.Issue, $"Assign Batch:{assignError}");
                                //NouvemMessageBox.Show($"Assign Batch:{assignError}");
                                //return;
                            }
                            else
                            {
                                this.Log.LogInfo(this.GetType(), $"Assigning of batch number successful:Reference:{assignRef}");
                            }

                            this.Log.LogInfo(this.GetType(), $"| Attempting to move batch to abbatoir: Batch No:{this.Sale.Number}, Permit No:{this.Sale.PermitNo} |");
                            var data = this.DepartmentBodiesManager.MoveSheepBatchToAbbatoir(this.Sale);
                            var moveError = data.Item1;
                            var moveReference = data.Item2;
                            if (!string.IsNullOrEmpty(moveError))
                            {
                                this.Log.LogError(this.GetType(), $"Move Request to abbatoir Error:{moveError}, Reference:{moveReference}");
                                SystemMessage.Write(MessageType.Issue, $"Move To Abbatoir:{moveError}. AFAIS Ref ID:{moveReference}");
                                NouvemMessageBox.Show($"Move To Abbatoir:{moveError}. AFAIS Ref ID:{moveReference}");
                                return;
                            }

                            this.Log.LogInfo(this.GetType(), $"Move to abbatoir successful:Reference:{moveReference}");
                            this.Sale.OtherReference = moveReference;
                            this.Sale.NouDocStatusID = statusId;
                            if (this.DataManager.UpdateLairageIntakeStatus(this.Sale))
                            {
                                SystemMessage.Write(MessageType.Priority,
                                    string.Format(Message.IntakeMoved, this.Sale.Number));
                                this.ClearForm();
                                this.SelectedCustomer = null;
                                this.SelectedHerd = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), string.Format("Move Request Error:{0}", ex.Message));
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                            NouvemMessageBox.Show(ex.Message);
                            return;
                        }
                        finally
                        {
                            ProgressBar.Reset();
                        }
                    }

                    return;
                }
            }

            this.Sale.NouDocStatusID = statusId;
            if (this.DataManager.UpdateLairageIntakeStatus(this.Sale))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.Sale.Number));
                this.ClearForm();
                this.SelectedCustomer = null;
                this.SelectedHerd = null;
            }
        }

        /// <summary>
        /// Validates the manually entered category against the AIMS data..
        /// </summary>
        /// <param name="category">Flag, as to validated category.</param>
        private bool CheckManuallyEnteredCategory(NouCategory category, StockDetail carcass)
        {
            if (category.CAT == "A" && carcass.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
            {
                var message = string.Format(Message.InvalidCategoryAge, ApplicationSettings.YoungBullMaxAge);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message);
                this.Sex = string.Empty;
                return false;
            }

            //var localSex = this.DepartmentBodiesManager.FindSex(category.CAT);
            //var isValidSex = this.DepartmentBodiesManager.IsValidSex(category.CAT, carcass.Sex);
            //if (!isValidSex)
            //{
            //    var message = string.Format(Message.InvalidCategorySexMatch, category.CAT, carcass.Sex);
            //    SystemMessage.Write(MessageType.Issue, message);
            //    NouvemMessageBox.Show(message);
            //    this.Sex = string.Empty;
            //    return false;
            //}

            return true;
        }

        /// <summary>
        /// Completes the aims processing on a category selection.
        /// </summary>
        private void HandleAimsCarcassCategorySelection()
        {
            try
            {
                this.HandleSelectedStockDetail(this.aimsCarcass);
                this.SetControlMode(ControlMode.Add);
                this.AddTransaction();
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Verifys the animal with the departments.
        /// </summary>
        private void VerifyAnimalCommandExecute()
        {
            //if (this.SelectedHerd == null)
            //{
            //    SystemMessage.Write(MessageType.Issue, "No herd no entered");
            //    return;
            //}

            //if (string.IsNullOrWhiteSpace(this.Eartag))
            //{
            //    SystemMessage.Write(MessageType.Issue, "No eartag entered");
            //    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            //    return;
            //}

            //ProgressBar.SetUp(0, 5);
            //ProgressBar.Run();

            //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //{
            //    try
            //    {
            //        var carcass = this.DepartmentBodiesManager.QueryAIMForEartag(this.Eartag, this.SelectedHerd.HerdNumber);

            //        if (carcass != null)
            //        {
            //            if (string.IsNullOrEmpty(carcass.Error))
            //            {
            //                this.FarmAssured = carcass.IsQAS.BoolToYesNo();
            //                this.Animals.Insert(0, carcass);
            //            }
            //            else
            //            {
            //                this.FarmAssured = false.BoolToYesNo();
            //                SystemMessage.Write(MessageType.Priority, carcass.Error);
            //                NouvemMessageBox.Show(carcass.Error);
            //                return;
            //            }
            //        }
            //        else
            //        {
            //            this.FarmAssured = false.BoolToYesNo();
            //        }

            //        this.SelectedAnimal = this.Animals.FirstOrDefault();
            //        var localMessage = string.Format("BLQAS:{0}", this.FarmAssured);
            //        SystemMessage.Write(MessageType.Priority, localMessage);
            //        NouvemMessageBox.Show(localMessage, flashMessage: true);
            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //    finally
            //    {
            //        ProgressBar.Reset();
            //        this.Eartag = string.Empty;
            //        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            //    }
            //}));
        }

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="category">The scanned passport data barcode.</param>
        private bool CheckCategory(NouCategory category)
        {
            var localMode = this.CurrentMode;

            try
            {
                var previousCat = this.PreviousCategory;
                if (category.CAT == "A" && this.AgeInMonths > ApplicationSettings.YoungBullMaxAge)
                {
                    var message = string.Format(Message.InvalidCategoryAge, ApplicationSettings.YoungBullMaxAge);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.IgnoreCategoryCheck = true;
                        this.SelectedCategory = previousCat;
                        this.IgnoreCategoryCheck = false;
                    }));

                    return false;
                }

                //if (!string.IsNullOrEmpty(this.Sex))
                //{
                //    var isValidSex = this.DepartmentBodiesManager.IsValidSex(category.CAT, this.Sex);
                //    if (!isValidSex)
                //    {
                //        var message = string.Format(Message.InvalidCategorySexMatch, category.CAT, this.Sex);
                //        SystemMessage.Write(MessageType.Issue, message);
                //        NouvemMessageBox.Show(message);

                //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                //        {
                //            this.IgnoreCategoryCheck = true;
                //            this.SelectedCategory = previousCat;
                //            this.IgnoreCategoryCheck = false;
                //        }));

                //        return false;
                //    }
                //}
            }
            finally
            {
                this.CurrentMode = localMode;
            }

            return true;
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        private void GetLairages()
        {
            var docStatusesAll = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID,
                NouvemGlobal.NouDocStatusMoved.NouDocStatusID
            };

            var sales = this.DataManager.GetAllLairageIntakes(docStatusesAll, DateTime.Today,
                DateTime.Today, this.SelectedSearchType).Where(x => x.KillType.CompareIgnoringCase(this.SelectedKillType.Name));

            this.Lots = new ObservableCollection<Sale>(sales);
        }

        /// <summary>
        /// Sets a partner agent.
        /// </summary>
        private void SetAgent()
        {
            if (this.SelectedAgent != null && this.SelectedPartner != null)
            {
                var agentId = this.SelectedAgent.BPMasterID;
                var partnerAgentId = this.SelectedPartner.BPMasterID_Agent;
                if (agentId != partnerAgentId)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        if (this.DataManager.UpdatePartnerAgent(this.SelectedPartner.BPMasterID, agentId))
                        {
                            this.SelectedPartner.BPMasterID_Agent = agentId;
                        }
                    }));
                }
            }
        }

        private void SetUpWandTimer()
        {
            this.wandTimer = new DispatcherTimer();
            this.wandTimer.Interval = TimeSpan.FromSeconds(1);
            this.wandTimer.Tick += WandTimerOnTick;
        }
       
        private void WandTimerOnTick(object sender, EventArgs e)
        {
            if (this.HardwareManager.SheepWand.IsPortOpen)
            {
                this.HardwareManager.SheepWand.WriteToPort(ApplicationSettings.WandPortSendString + Environment.NewLine);
                this.wandTimeOut = 0;
                this.wandTimer.Stop();
            }

            this.wandTimeOut++;
            if (this.wandTimeOut >= ApplicationSettings.WandPortTimeOut)
            {
                this.wandTimeOut = 0;
                this.wandTimer.Stop();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue, Message.WandPortTimeout);
                    NouvemMessageBox.Show(Message.WandPortTimeout);
                }));
            }
        }

        #endregion
    }
}








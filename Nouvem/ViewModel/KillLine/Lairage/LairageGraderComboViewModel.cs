﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    public class LairageGraderComboViewModel : LairageViewModel
    {
        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LairageGraderComboViewModel"/> class.
        /// </summary>
        public LairageGraderComboViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            Messenger.Default.Send(true, Token.DisableLegacyAttributesView);
            Messenger.Default.Send(true, Token.EnableEartagEntryOnGrader);
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #region method

        /// <summary>
        /// Creates an empty pig intake.
        /// </summary>
        /// <param name="partner">The partner to create the intake for.</param>
        public int CreateSheepIntakeFromGrader(ViewBusinessPartner partner, NouCategory category)
        {
            try
            {
                this.SelectedKillType = this.KillTypes.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Sheep));
                this.SelectedPartner = this.Suppliers.FirstOrDefault(x => x.BPMasterID == partner.BPMasterID);
                this.IntakeNumber = 1;
                this.SelectedCategory = this.Categories.FirstOrDefault(x => x.CategoryID == category.CategoryID);
                this.AddTransaction();
                return this.Sale.SaleID;
            }
            finally
            {
                this.ClearForm();
            }
        }

        /// <summary>
        /// Creates an empty pig intake.
        /// </summary>
        /// <param name="partner">The partner to create the intake for.</param>
        public int CreatePigIntakeFromGrader(ViewBusinessPartner partner)
        {
            try
            {
                this.SelectedKillType = this.KillTypes.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.Pig));
                this.SelectedPartner = this.Suppliers.FirstOrDefault(x => x.BPMasterID == partner.BPMasterID);
                this.IntakeNumber = 1;
                this.SelectedCategory = this.Categories.FirstOrDefault();
                this.AddTransaction();
                return this.Sale.SaleID;
            }
            finally
            {
                this.ClearForm();
            }
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            this.Log.LogDebug(this.GetType(), "AddSale(): adding a new order");
            this.UpdateDocNumbering();

            var receipt = this.DataManager.AddLairageIntake(this.Sale);
            var newSaleId = receipt.Item1;
            var transactionId = receipt.Item2;

            if (newSaleId > 0)
            {
                if (this.IntakeNumber == null || this.IntakeNumber == 1)
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.GoodsInCreated, this.NextNumber));
                }

                this.Sale.SaleID = newSaleId;

                if (this.SelectedStockDetail != null)
                {
                    this.SelectedStockDetail.AttributeID = transactionId;
                }

                var localCategory = this.SelectedCategory;

                if (this.KillType == KillType.Beef || !ApplicationSettings.ManuallySequenceSheep)
                {
                    this.ClearAttributes();
                }

                this.SetTotalKillReceived(this.StockDetails.Count);
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.GoodsInNotCreated);
            }
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddTransaction()
        {
            this.Log.LogDebug(this.GetType(), "AddTransaction(): Adding a transaction");

            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoSupplierSelected;
            }
            else if (this.ProposedKillDate == null)
            {
                error = Message.NoProposedKillDateEntered;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }

            if (this.KillType == KillType.Sheep)
            {
                if (this.SelectedCategory == null)
                {
                    error = Message.NoCategoryEntered;
                }
            }

            var localCategory = this.SelectedCategory;
            if (this.KillType == KillType.Pig)
            {
                if (this.SelectedCategory == null)
                {
                    error = Message.NoCategoryEntered;
                }

                if (this.IntakeNumber.IsNullOrZero() && this.CurrentMode != ControlMode.Update)
                {
                    error = Message.NoSheepIntakeNumberEntered;
                }
            }

            if (error != string.Empty)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: true);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, error);
                }

                return;
            }

            #endregion

            var transactionCount = 1;
            if (this.KillType == KillType.Sheep && ApplicationSettings.ManuallySequenceSheep)
            {
                transactionCount = this.IntakeNumber.ToInt();
            }

            if (this.KillType == KillType.Pig && this.CurrentMode != ControlMode.Update)
            {
                transactionCount = this.IntakeNumber.ToInt();
            }

            if (transactionCount > 1)
            {
                ProgressBar.SetUp(1, transactionCount);
            }

            for (int i = 0; i < transactionCount; i++)
            {
                ProgressBar.Run();
                var tag = this.Eartag;
                this.SelectedCategory = localCategory;
                this.CreateSale();
                if (this.Sale.SaleID == 0)
                {
                    this.AddSale();
                }
                else
                {
                    this.UpdateSale();
                }

                if (this.FreezerIntakeNumber.HasValue)
                {
                    this.FreezerIntakeNumber--;
                }

                this.HandleTouchscreenEntry(tag);
            }

            ProgressBar.Reset();

            if (transactionCount > 1)
            {
                SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                this.ClearAttributes();
            }

            this.SelectedStockDetail = null;
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        /// <summary>
        /// Closes the other kill modules.
        /// </summary>
        protected override void CloseOtherKillModules(ViewType currentModule) { }

        #endregion

        #region private



        #endregion
    }
}

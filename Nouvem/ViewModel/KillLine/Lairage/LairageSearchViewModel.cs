﻿// -----------------------------------------------------------------------
// <copyright file="LairageSearchViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class LairageSearchViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime fromLairageDate;

        /// <summary>
        /// The search to date.
        /// </summary>
        private DateTime toLairageDate;

        /// <summary>
        /// The qa only flag.
        /// </summary>
        private bool showFarmAssuredOnly;

        /// <summary>
        /// The print/preview mode.
        /// </summary>
        private string reportMode;

        /// <summary>
        /// The ui name.
        /// </summary>
        private string formName;

        /// <summary>
        /// The report values.
        /// </summary>
        private IList<MultiKillReportValue> multivalues = new List<MultiKillReportValue>();

        /// <summary>
        /// Gets or sets the report.
        /// </summary>
        private string currentReport;

        /// <summary>
        /// Gets or sets the report.
        /// </summary>
        private ViewType mode { get; set; }

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LairageSearchViewModel"/> class.
        /// </summary>
        public LairageSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<List<Sale>>(this, Token.SendingLairages, this.HandleFilteredLairages);

            #endregion

            #region command registration

            // Inform the report viewer to attach the current report to the selected email account and open.
            this.EmailReportCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.AttachReportToEmail));

            this.RefreshCommand = new RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            });

            #endregion

            this.Mode = ViewType.ReportViewer;
            this.fromLairageDate = ApplicationSettings.LairageSearchFromDate;
            if (this.FromLairageDate > DateTime.Today)
            {
                this.ToLairageDate = this.FromLairageDate;
            }
            else
            {
                this.ToLairageDate = DateTime.Today;
            }

            this.ShowAllOrders = ApplicationSettings.LairageSearchShowAllOrders;
            this.KeepVisible = ApplicationSettings.LairageSearchShowKeepVisible;
            this.ShowFarmAssuredOnly = ApplicationSettings.LairageSearchShowFAOnly;
            this.FormName = Strings.LairageSearch;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the report.
        /// </summary>
        public string CurrentReport
        {
            get
            {
                return this.currentReport;
            }

            set
            {
                this.currentReport = value;
                this.FormName = value;
                this.Mode = ViewType.ReportViewer;
                this.Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the report.
        /// </summary>
        public ViewType Mode
        {
            get
            {
                return this.mode;
            }

            set
            {
                this.mode = value;
                if (value == ViewType.LairageIntake)
                {
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.LairageIntakeSearchType)
                        ? Strings.ProposedKilldate
                        : ApplicationSettings.LairageIntakeSearchType;
                }
                else if (value == ViewType.ReportViewer)
                {
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.LairageReportSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.LairageReportSearchType;
                }
                else if (value == ViewType.PaymentProposalCreation)
                {
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.PaymentCreationSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.PaymentCreationSearchType;
                }
                else
                {
                    this.SelectedSearchType = Strings.ProposedKilldate;
                }
            }
        }
     
        /// <summary>
        /// Gets or sets a value indicating whether qa only animals are to be displayed on the report.
        /// </summary>
        public bool ShowFarmAssuredOnly
        {
            get
            {
                return this.showFarmAssuredOnly;
            }

            set
            {
                this.showFarmAssuredOnly = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromLairageDate
        {
            get
            {
                if (this.fromLairageDate < DateTime.Today.AddYears(-100))
                {
                    this.fromLairageDate = DateTime.Today;
                }

                return this.fromLairageDate;
            }

            set
            {
                this.fromLairageDate = value;
                this.RaisePropertyChanged();

                if (value > this.toLairageDate)
                {
                    this.ToLairageDate = value;
                    return;
                }

                if (this.IsFormLoaded)
                {
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToLairageDate
        {
            get
            {
                if (this.toLairageDate < DateTime.Today.AddYears(-100))
                {
                    this.toLairageDate = DateTime.Today;
                }
        
                return this.toLairageDate;
            }

            set
            {
                this.toLairageDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.Refresh();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the refresh command.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }
        
        /// <summary>
        /// Gets the refresh command.
        /// </summary>
        public ICommand EmailReportCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            this.reportMode = mode;

            if (this.reportMode.Equals(Constant.Email))
            {
                this.CreateBulkEmail();
                return;
            }

            // send message to lairage search view to return the filtered grid lairages.
            Messenger.Default.Send(Token.Message, Token.RequestLairages);

            try
            {
                if (this.SelectedSales == null || !this.SelectedSales.Any())
                {
                    return;
                }

                var localReport = ReportName.KillReportFiltered;
                if (this.currentReport.CompareIgnoringCase(ReportName.SheepKillReport))
                {
                    localReport = ReportName.SheepKillReportFiltered;
                }
                else if (this.currentReport.CompareIgnoringCase(ReportName.PigKillReport))
                {
                    localReport = ReportName.PigKillReportFiltered;
                }

                //var reportsPrinted = this.DataManager.GetKillReportIds(this.FromLairageDate, this.ToLairageDate);
                this.Log.LogDebug(this.GetType(), string.Format("Print Report(): Beef Kill Ids:{0}", this.SelectedSales.Count));

                this.multivalues = new List<MultiKillReportValue>();
                foreach (var selectedKill in this.SelectedSales)
                {
                    this.multivalues.Add(new MultiKillReportValue { ID = selectedKill.SaleID });
                }

                this.DataManager.UpdateMultiKillValueReports(this.multivalues);

                if (this.reportMode.CompareIgnoringCase(Constant.Print))
                {
                    foreach (var sale in this.SelectedSales)
                    {
                        var id = sale.SaleID;
                        var param = new List<ReportParameter>
                        {
                            new ReportParameter { Name = "FarmAssured", Values = { null } },
                            new ReportParameter { Name = "GoodsInID", Values = { id.ToString() } }
                        };

                        var data = new ReportData { Name = localReport, ReportParameters = param };
                        this.ReportManager.PrintReport(data);
                    }

                    return;
                }

                var reportId = this.SelectedSales.First().SaleID;
                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter { Name = "FarmAssured", Values = { null } },
                    new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } }
                };

                var reportData = new ReportData { Name = localReport, ReportParameters = reportParam };
                this.ReportManager.PreviewReport(reportData);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale != null)
            {
                var localSale = this.SelectedSale;
                localSale.DeliveryDate = this.fromLairageDate;
                localSale.CreationDate = this.toLairageDate;

                if (this.Mode == ViewType.ReportViewer)
                {
                    Messenger.Default.Send(localSale, Token.SearchSaleSelectedForReport);
                }
                else
                {
                    Messenger.Default.Send(ViewType.LairageIntake);
                    Messenger.Default.Send(localSale, Token.SearchSaleSelected);
                }
            }

            if (!this.keepVisible)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            //this.GetLairages();
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            ApplicationSettings.LairageSearchFromDate = this.fromLairageDate;
            ApplicationSettings.LairageSearchShowAllOrders = this.ShowAllOrders;
            ApplicationSettings.LairageSearchShowKeepVisible = this.keepVisible;
            ApplicationSettings.LairageSearchShowFAOnly = this.showFarmAssuredOnly;
            this.SaveSearchType();
            this.IsFormLoaded = false;

            this.Close();
        }

       
        /// <summary>
        /// Refreshes the lairages.
        /// </summary>
        protected override void Refresh()
        {
            this.GetLairages();
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        protected override void Close()
        {
                ViewModelLocator.ClearLairageSearch();
                Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void GetSearchTypes()
        {
            this.SearchTypes = new List<string> { Strings.KillDate, Strings.ProposedKilldate, };
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void HandleSelectedSearchType()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Refresh();
        }

        #endregion

        #region private

        /// <summary>
        /// Handles the selected lairages, sending to the report viewer.
        /// </summary>
        /// <param name="lairages">The selected lairages.</param>
        private void HandleFilteredLairages(List<Sale> lairages)
        {
            var preview = this.reportMode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (!lairages.Any())
            {
                return;
            }

            #endregion

            if (this.CurrentReport.CompareIgnoringCase(ReportName.KillReport))
            {
                var intakeId = string.Join(",", lairages.Select(x => x.SaleID));
                var farmAssuredOnly = this.ShowFarmAssuredOnly;
                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter {Name = "GoodsInID", Values = {intakeId}},
                    new ReportParameter {Name = "FarmAssured", Values = {farmAssuredOnly.ToString()}}
                };

                var reportData = new ReportData { Name = ReportName.KillReport, ReportParameters = reportParam };

                try
                {
                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                return;
            }

            if (this.CurrentReport.CompareIgnoringCase(ReportName.EndOfDayReport))
            {
                var start = this.FromLairageDate;
                var end = this.ToLairageDate;
                var farmAssuredOnly = this.ShowFarmAssuredOnly;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter {Name = "Start", Values = {start.ToString()}},
                     new ReportParameter {Name = "End", Values = {end.ToString()}},
                      new ReportParameter {Name = "FarmAssured", Values = {farmAssuredOnly.ToString()}}
                };

                var reportData = new ReportData { Name = ReportName.EndOfDayReport, ReportParameters = reportParam };

                try
                {
                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                return;
            }

            if (this.CurrentReport.CompareIgnoringCase(ReportName.RPAExportReport))
            {
                var start = this.FromLairageDate;
                var end = this.ToLairageDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter {Name = "Start", Values = {start.ToString()}},
                     new ReportParameter {Name = "End", Values = {end.ToString()}}
                };

                var reportData = new ReportData { Name = ReportName.RPAExportReport, ReportParameters = reportParam };

                try
                {
                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }

                    this.Locator.ReportViewer.AttachmentType = "CSV";
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                return;
            }
        }

        /// <summary>
        /// Sends to master vm to create multiple reports and attch to an email.
        /// </summary>
        private void CreateBulkEmail()
        {
            if (this.SelectedSales != null && this.SelectedSales.Any())
            {
                Messenger.Default.Send(this.SelectedSales.ToList(), Token.SearchSaleSelectedForReport);
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        private void GetLairages()
        {
            var docStatusesAll = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID,
                NouvemGlobal.NouDocStatusMoved.NouDocStatusID
            };

            var docStatusesNonCancelled = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID,
                NouvemGlobal.NouDocStatusMoved.NouDocStatusID
            };

            if (this.ShowAllOrders)
            {
                var sales = this.DataManager.GetAllLairageIntakes(docStatusesAll, this.fromLairageDate.Date,
                    this.toLairageDate.Date, this.SelectedSearchType);
                if (this.Mode == ViewType.ReportViewer)
                {
                    if (this.CurrentReport == ReportName.SheepKillReport)
                    {
                        sales = sales.Where(x => x.KillType.CompareIgnoringCase(Constant.Sheep)).ToList();
                    }
                    else if (this.CurrentReport == ReportName.BeefKillReport)
                    {
                        sales = sales.Where(x => x.KillType.CompareIgnoringCase(Constant.Beef)).ToList();
                    }
                    else if (this.CurrentReport == ReportName.PigKillReport)
                    {
                        sales = sales.Where(x => x.KillType.CompareIgnoringCase(Constant.Pig)).ToList();
                    }
                }

                this.FilteredSales = new ObservableCollection<Sale>(sales);
            }
            else
            {
                var sales = this.DataManager.GetAllLairageIntakes(docStatusesNonCancelled, this.fromLairageDate.Date,
                    this.toLairageDate.Date, this.SelectedSearchType);
                if (this.Mode == ViewType.ReportViewer)
                {
                    if (this.CurrentReport == ReportName.SheepKillReport)
                    {
                        sales = sales.Where(x => x.KillType.CompareIgnoringCase(Constant.Sheep)).ToList();
                    }
                    else if (this.CurrentReport == ReportName.BeefKillReport)
                    {
                        sales = sales.Where(x => x.KillType.CompareIgnoringCase(Constant.Beef)).ToList();
                    }
                    else if (this.CurrentReport == ReportName.PigKillReport)
                    {
                        sales = sales.Where(x => x.KillType.CompareIgnoringCase(Constant.Pig)).ToList();
                    }
                }

                this.FilteredSales = new ObservableCollection<Sale>(sales);
            }

            this.SelectedSale = this.FilteredSales.FirstOrDefault();
        }

        /// <summary>
        /// Save the search type.
        /// </summary>
        private void SaveSearchType()
        {
            if (this.Mode == ViewType.LairageIntake)
            {
                ApplicationSettings.LairageIntakeSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.ReportViewer)
            {
                ApplicationSettings.LairageReportSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.PaymentProposalCreation)
            {
                ApplicationSettings.PaymentCreationSearchType = this.SelectedSearchType;
            }
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="DetainOrCondemnViewModel.cs" company="DEM Machines Limited">
// Copyright (c) DEM Machines Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------


using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Attribute;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class that handles the condemn or detain functionality.
    /// </summary>
    public class DetainCondemnViewModel : AttributesViewModel
    {

        #region field

        /// <summary>
        /// The field enabled flag.
        /// </summary>
        private bool fieldEnabled;

        /// <summary>
        /// The supplier.
        /// </summary>
        private string supplier;

        /// <summary>
        /// The kill no.
        /// </summary>
        private string killNumber;

        /// <summary>
        /// The carcass no.
        /// </summary>
        private string carcassNumber;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DetainCondemnViewModel"/> class.
        /// </summary>
        public DetainCondemnViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region messages

            Messenger.Default.Register<string>(this, Token.ClearDetainCondemnData, s => this.ClearForm());

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals("DetainCondemn"))
                {
                    this.SelectedFatColour = this.FatColours.FirstOrDefault(x => x.NouFatColourID == c.ID);
                }
            });

            #endregion

            #region command

            this.SendDataCommand = new RelayCommand(this.SendData);
            this.CancelCommand= new RelayCommand(this.Close);

            #endregion

            this.GetFatColours();
            this.FieldEnabled = true;
        }

        #endregion

        #region Public Interface

        #region Properties

        /// <summary>
        /// Gets or sets the kill number.
        /// </summary>
        public string KillNumber
        {
            get
            {
                return this.killNumber;
            }

            set
            {
                if (value != null)
                {
                    this.killNumber = string.Format("{0}:{1}", Strings.KillNo, value);
                }
                else
                {
                    this.killNumber = null;
                }

                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the carcass number.
        /// </summary>
        public string CarcassNumber
        {
            get
            {
                return this.carcassNumber;
            }

            set
            {
                if (value != null)
                {
                    this.carcassNumber = string.Format("{0}:{1}",Strings.CarcassNo, value);
                }
                else
                {
                    this.carcassNumber = null;
                }

                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the supplier name.
        /// </summary>
        public string Supplier
        {
            get
            {
                return this.supplier;
            }

            set
            {
                this.supplier = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the burst belly, tb, and casualty checkboxes are enabled
        /// </summary>
        public bool FieldEnabled
        {
            get
            {
                return this.fieldEnabled;
            }

            set
            {
                this.fieldEnabled = value;
                this.RaisePropertyChanged();
            }
        }
        
        #endregion

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command to complete the form
        /// </summary>
        public RelayCommand SendDataCommand { get; private set; }

        /// <summary>
        /// Gets the command to cancel the form
        /// </summary>
        public RelayCommand CancelCommand { get; private set; }

        #endregion

        #region command methods

        /// <summary>
        /// Send the data and close.
        /// </summary>
        private void SendData()
        {
            var stockDetail = new StockDetail
            {
                CondemnedSide1 = this.Condemned,
                CondemnedSide2 = this.CondemnedSide2,
                DetainedSide1 = this.Detained,
                DetainedSide2 = this.DetainedSide2,
                NotInsured = this.NotInsured,
                TBYes = this.TBYes,
                BurstBelly = this.BurstBelly,
                Casualty = this.IsCasualty,
                FatColour = this.SelectedFatColour,
                LegMissing = this.LegMissing,
                Abscess = this.Abscess
            };
           
            Messenger.Default.Send(stockDetail, Token.DetainCondemnSelected);
            this.Close();
        }

        /// <summary>
        /// Close the form.        
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseDetainCondemnWindow);
        }

        /// <summary>
        /// Clears the ui.
        /// </summary>
        private void ClearForm()
        {
            this.ClearAttributes();
            this.KillNumber = string.Empty;
            this.CarcassNumber = string.Empty;
            this.Supplier = string.Empty;
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="ContainerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Nouvem.ViewModel.Container
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Win32;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class ContainerViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected container.
        /// </summary>
        private Container selectedContainer;

        /// <summary>
        /// The containers collection.
        /// </summary>
        private ObservableCollection<Container> containers;

        /// <summary>
        /// The containers to update collection.
        /// </summary>
        private HashSet<Container> containersToUpdate = new HashSet<Container>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the ContainerViewModel class.
        /// </summary>
        public ContainerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration

            Messenger.Default.Register<int>(this, Token.SetSelectedContainer, i =>
            {
                this.SelectedContainer = this.Containers.FirstOrDefault(x => x.ContainerID == i);
            });

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);

                if (this.SelectedContainer != null)
                {
                    this.containersToUpdate.Add(this.selectedContainer);
                }

            }, this.CanUpdate);

            // Command handler to upload an item image.
            this.UploadImageCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);
                this.UploadImageCommandExecute();
            });

            #endregion

            #region instantiation

            this.Containers = new ObservableCollection<Container>();

            #endregion

            this.GetContainers();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the container types.
        /// </summary>
        public IList<ContainerType> ContainerTypes { get; set; }

        /// <summary>
        /// Get or sets the containers.
        /// </summary>
        public ObservableCollection<Container> Containers
        {
            get
            {
                return this.containers;
            }

            set
            {
                this.containers = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the selected container.
        /// </summary>
        public Container SelectedContainer
        {
            get
            {
                return this.selectedContainer;
            }

            set
            {
                this.selectedContainer = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to upload an item image.
        /// </summary>
        public ICommand UploadImageCommand { get; private set; }

        /// <summary>
        /// Gets the command to update the containers.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedContainer != null)
            {
                this.selectedContainer.Deleted = true;
                this.UpdateContainers();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.Containers.Remove(this.selectedContainer);
            }
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateContainers();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handler to upload an item image
        /// </summary>
        private void UploadImageCommandExecute()
        {
            #region validation

            if (this.selectedContainer == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoContainerSelected);
                return;
            }

            #endregion

            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "ALL FILES|*.*|PNG|*.png|BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|TIFF|*.tif;*.tiff";

            if (openFileDialog.ShowDialog() == true)
            {
                this.SelectedContainer.Photo = File.ReadAllBytes(openFileDialog.FileName);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Updates the containers group.
        /// </summary>
        private void UpdateContainers()
        {
            foreach (var localContainer in this.containersToUpdate.Where(x => x != null))
            {
                if (localContainer.ContainerTypeID == 0)
                {
                    localContainer.ContainerTypeID =
                    this.ContainerTypes.First(x => x.Name.Equals(Constant.Piece)).ContainerTypeID;
                }
            }

            try
            {
                if (this.DataManager.AddOrUpdateContainers(this.containersToUpdate.Where(x => x != null).ToList()))
                {
                    this.containersToUpdate.Clear();
                    SystemMessage.Write(MessageType.Priority, Message.ContainerUpdated);

                    // refresh the corresponding inventory collection.
                    Messenger.Default.Send(Token.Message, Token.RefreshInventory);

                    this.GetContainers();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.ContainerNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.ContainerNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearContainer();
            Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
        }

        /// <summary>
        /// Gets the application containers.
        /// </summary>
        private void GetContainers()
        {
            this.Containers = new ObservableCollection<Container>(this.DataManager.GetContainers());
            this.ContainerTypes = NouvemGlobal.ContainerTypes;
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="GS1AIViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Label
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class GS1AIViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The gs1 collection.
        /// </summary>
        private ObservableCollection<Gs1AIMaster> gs1Masters;

        /// <summary>
        /// The selected gs1.
        /// </summary>
        private Gs1AIMaster selectedGs1Master;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the GS1AIViewModel class.
        /// </summary>
        public GS1AIViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessGS1);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.Gs1Masters = new ObservableCollection<Gs1AIMaster>();

            #endregion
        
            this.GetGs1Masters();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected gs1Master.
        /// </summary>
        public Gs1AIMaster SelectedGs1Master
        {
            get
            {
                return this.selectedGs1Master;
            }

            set
            {
                this.selectedGs1Master = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the gs1Masters.
        /// </summary>
        public ObservableCollection<Gs1AIMaster> Gs1Masters
        {
            get
            {
                return this.gs1Masters;
            }

            set
            {
                this.gs1Masters = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the gsi masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedGs1Master != null)
            {
                this.selectedGs1Master.Deleted = true;
                this.UpdateGsiMaster();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.Gs1Masters.Remove(this.selectedGs1Master);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateGsiMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the gsi master group.
        /// </summary>
        private void UpdateGsiMaster()
        {
            try
            {
                if (this.DataManager.AddOrUpdateGs1Masters(this.Gs1Masters))
                {
                    SystemMessage.Write(MessageType.Priority, Message.Gs1Updated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.Gs1NotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.Gs1NotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearGS1AI();
            Messenger.Default.Send(Token.Message, Token.CloseGS1AIWindow);
        }

        /// <summary>
        /// Gets the application gs1 identifier information..
        /// </summary>
        private void GetGs1Masters()
        {
            this.Gs1Masters.Clear();
            foreach (var gsi in this.DataManager.GetGs1Master())
            {
                this.Gs1Masters.Add(gsi);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

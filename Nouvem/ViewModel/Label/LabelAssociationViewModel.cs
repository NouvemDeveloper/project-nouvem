﻿// -----------------------------------------------------------------------
// <copyright file="LabelAssociationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.ViewModel.Label
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class LabelAssociationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The label associations.
        /// </summary>
        private ObservableCollection<LabelsAssociation> labelAssociations;

        /// <summary>
        /// The label associations to update.
        /// </summary>
        private IList<LabelsAssociation> labelAssociationsToUpdate = new List<LabelsAssociation>();

        /// <summary>
        /// The selected label associations.
        /// </summary>
        private LabelsAssociation selectedLabelAssociation;

        /// <summary>
        /// The customer groups.
        /// </summary>
        private ObservableCollection<BPGroup> customerGroups;

        /// <summary>
        /// The customers.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.BusinessPartner> customers;

        /// <summary>
        /// The inventory groups.
        /// </summary>
        private ObservableCollection<INGroup> inventoryGroups;

        /// <summary>
        /// The products.
        /// </summary>
        private ObservableCollection<InventoryItem> saleItems;

        /// <summary>
        /// The products.
        /// </summary>
        private ObservableCollection<Printer> printers;

        /// <summary>
        /// The labels.
        /// </summary>
        private ObservableCollection<Model.DataLayer.Label> labels;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelAssociationViewModel"/> class.
        /// </summary>
        public LabelAssociationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>(this.UpdateCommandExecute);
            this.DrillDownToMasterItemCommand = new RelayCommand(this.DrillDownToMasterItemCommandExecute);
            this.DrillDownToPartnerCommand = new RelayCommand(this.DrillDownToPartnerCommandExecute);

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetCustomers();
            this.GetCustomerGroups();
            this.GetInventoryGroups();
            this.GetProducts();
            this.GetLabels();
            this.GetLabelAssociations();
            this.GetPrinters();
            this.GetProcesses();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the label associations.
        /// </summary>
        public ObservableCollection<LabelsAssociation> LabelAssociations
        {
            get
            {
                return this.labelAssociations;
            }

            set
            {
                this.labelAssociations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected label association.
        /// </summary>
        public LabelsAssociation SelectedLabelAssociation
        {
            get
            {
                return this.selectedLabelAssociation;
            }

            set
            {
                this.selectedLabelAssociation = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customer groups.
        /// </summary>
        public ObservableCollection<BPGroup> CustomerGroups
        {
            get
            {
                return this.customerGroups;
            }

            set
            {
                this.customerGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.BusinessPartner> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.customers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryGroups
        {
            get
            {
                return this.inventoryGroups;
            }

            set
            {
                this.inventoryGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        public ObservableCollection<InventoryItem> SaleItems
        {
            get
            {
                return this.saleItems;
            }

            set
            {
                this.saleItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the printers.
        /// </summary>
        public ObservableCollection<Printer> Printers
        {
            get
            {
                return this.printers;
            }

            set
            {
                this.printers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the labels.
        /// </summary>
        public ObservableCollection<Model.DataLayer.Label> Labels
        {
            get
            {
                return this.labels;
            }

            set
            {
                this.labels = value;
                this.RaisePropertyChanged();
            }
        }


        #endregion

        #region command

        /// <summary>
        /// Drill down to partner command.
        /// </summary>
        public ICommand DrillDownToPartnerCommand { get; set; }

        /// <summary>
        /// Drill down to item command.
        /// </summary>
        public ICommand DrillDownToMasterItemCommand { get; set; }

        /// <summary>
        /// Gets the update command.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Remove the label association.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedLabelAssociation != null)
            {
                this.selectedLabelAssociation.Deleted = DateTime.Now;
                this.UpdateLabelAssociations(new List<LabelsAssociation> { this.selectedLabelAssociation });
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.GetLabelAssociations();
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateLabelAssociations(this.labelAssociationsToUpdate);
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        private void DrillDownToPartnerCommandExecute()
        {
            if (this.selectedLabelAssociation == null)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.BPMaster, this.SelectedLabelAssociation.BPMasterID.ToInt()), Token.DrillDown);
        }

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        private void DrillDownToMasterItemCommandExecute()
        {
            if (this.SelectedLabelAssociation == null)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedLabelAssociation.INMasterID.ToInt()), Token.DrillDown);
        }


        /// <summary>
        /// Handles the cell change.
        /// </summary>
        /// <param name="e">The event parameters.</param>
        private void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            #region validation

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property))
            {
                return;
            }

            #endregion

            if (this.SelectedLabelAssociation == null)
            {
                return;
            }

            if (!this.labelAssociationsToUpdate.Contains(this.selectedLabelAssociation))
            {
                this.labelAssociationsToUpdate.Add(this.selectedLabelAssociation);
            }

            this.SetControlMode(ControlMode.Update);
            var isNoneSelected = !e.Cell.Property.Equals("ItemLabelsForCombo")
                                 && !e.Cell.Property.Equals("BoxLabelsForCombo")
                                 && !e.Cell.Property.Equals("LabelProcessesForCombo")
                                 && !e.Cell.Property.Equals("PieceLabelsForCombo")
                                 && !e.Cell.Property.Equals("ShippingLabelsForCombo")
                                 && !e.Cell.Property.Equals("PalletLabelsForCombo")
                                 && !e.Cell.Property.Equals("PieceLabelQty")
                                 && !e.Cell.Property.Equals("ItemLabelQty")
                                 && !e.Cell.Property.Equals("BoxLabelQty")
                                 && !e.Cell.Property.Equals("PalletLabelQty")
                                 && !e.Cell.Property.Equals("ShippingLabelQty")
                                 && (int?)e.Value == -2;

            const int noneSelected = -2;

            if (e.Cell.Property.Equals("BPGroupID"))
            {
                if (isNoneSelected)
                {
                    this.SelectedLabelAssociation.IsCustomerEnabled = true;
                }
                else
                {
                    this.SelectedLabelAssociation.IsCustomerEnabled = false;
                    this.SelectedLabelAssociation.BPMasterID = noneSelected;
                }
            }
            else if (e.Cell.Property.Equals("BPMasterID"))
            {
                if (isNoneSelected)
                {
                    this.SelectedLabelAssociation.IsCustomerGroupEnabled = true;
                }
                else
                {
                    this.SelectedLabelAssociation.IsCustomerGroupEnabled = false;
                    this.SelectedLabelAssociation.BPGroupID = noneSelected;
                }
            }
            else if (e.Cell.Property.Equals("INMasterID"))
            {
                if (isNoneSelected)
                {
                    this.SelectedLabelAssociation.IsProductGroupEnabled = true;
                }
                else
                {
                    this.SelectedLabelAssociation.IsProductGroupEnabled = false;
                    this.SelectedLabelAssociation.INGroupID = noneSelected;
                }
            }
            else if (e.Cell.Property.Equals("INGroupID"))
            {
                if (isNoneSelected)
                {
                    this.SelectedLabelAssociation.IsProductEnabled = true;
                }
                else
                {
                    this.SelectedLabelAssociation.IsProductEnabled = false;
                    this.SelectedLabelAssociation.INMasterID = noneSelected;
                }
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Adds/updates the label associations.
        /// </summary>
        /// <param name="associations">The associations to update.</param>
        private void UpdateLabelAssociations(IList<LabelsAssociation> associations)
        {
            foreach (var item in associations)
            {
                if (item.BPGroupID <= 0)
                {
                    item.BPGroupID = null;
                }

                if (item.BPMasterID <= 0)
                {
                    item.BPMasterID = null;
                }

                if (item.INGroupID <= 0)
                {
                    item.INGroupID = null;
                }

                if (item.INMasterID <= 0)
                {
                    item.INMasterID = null;
                }

                item.PieceLabels.Clear();
                item.ItemLabels.Clear();
                item.BoxLabels.Clear();
                item.PalletLabels.Clear();
                item.ShippingLabels.Clear();
                item.LabelProcesses.Clear();

                foreach (var localLabel in item.LabelProcessesForCombo)
                {
                    var localLabelId = (int)localLabel;
                    var label = this.Processes.FirstOrDefault(x => x.ProcessID == localLabelId);

                    if (label != null)
                    {
                        item.LabelProcesses.Add(new LabelProcess { ProcessID = localLabelId });
                    }
                }

                foreach (var localLabel in item.PieceLabelsForCombo)
                {
                    var localLabelId = (int)localLabel;
                    var label = this.labels.FirstOrDefault(x => x.LabelID == localLabelId);

                    if (label != null)
                    {
                        item.PieceLabels.Add(label);
                    }
                }

                foreach (var localLabel in item.ItemLabelsForCombo)
                {
                    var localLabelId = (int)localLabel;
                    var label = this.labels.FirstOrDefault(x => x.LabelID == localLabelId);

                    if (label != null)
                    {
                        item.ItemLabels.Add(label);
                    }
                }

                foreach (var localLabel in item.BoxLabelsForCombo)
                {
                    var localLabelId = (int)localLabel;
                    var label = this.labels.FirstOrDefault(x => x.LabelID == localLabelId);

                    if (label != null)
                    {
                        item.BoxLabels.Add(label);
                    }
                }

                foreach (var localLabel in item.PalletLabelsForCombo)
                {
                    var localLabelId = (int)localLabel;
                    var label = this.labels.FirstOrDefault(x => x.LabelID == localLabelId);

                    if (label != null)
                    {
                        item.PalletLabels.Add(label);
                    }
                }

                foreach (var localLabel in item.ShippingLabelsForCombo)
                {
                    var localLabelId = (int)localLabel;
                    var label = this.labels.FirstOrDefault(x => x.LabelID == localLabelId);

                    if (label != null)
                    {
                        item.ShippingLabels.Add(label);
                    }
                }
            }

            if (this.DataManager.AddOrUpdateLabelAssociations(associations))
            {
                SystemMessage.Write(MessageType.Priority, Message.LabelAssociationsUpdated);
                this.labelAssociationsToUpdate.Clear();
                this.SetControlMode(ControlMode.OK);
                this.Refresh();
                Messenger.Default.Send(Token.Message, Token.LabelAssociationsUpdated);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.LabelAssociationsNotUpdated);
            }
        }

        /// <summary>
        /// Refresh the associations.
        /// </summary>
        private void Refresh()
        {
            this.GetLabelAssociations();
        }

        /// <summary>
        /// Gets the label associations.
        /// </summary>
        private void GetLabelAssociations()
        {
            this.LabelAssociations
                = new ObservableCollection<LabelsAssociation>(this.DataManager.GetLabelAssociations());
        }

        /// <summary>
        /// Gets the printers.
        /// </summary>
        private void GetPrinters()
        {
            this.Printers
                = new ObservableCollection<Printer>(this.DataManager.GetPrinters());
        }

        /// <summary>
        /// Gets the customer groups.
        /// </summary>
        private void GetCustomerGroups()
        {
            this.CustomerGroups = new ObservableCollection<BPGroup>(this.DataManager.GetBusinessPartnerGroups());
            this.CustomerGroups.Insert(0, new BPGroup { BPGroupName = Strings.AllGroups, BPGroupID = -1 });
            this.CustomerGroups.Insert(0, new BPGroup { BPGroupName = Strings.NoneSelected, BPGroupID = -2 });
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        private void GetCustomers()
        {
            this.Customers = new ObservableCollection<Model.BusinessObject.BusinessPartner>(NouvemGlobal.BusinessPartners.OrderBy(x => x.Details.Name));
            this.Customers.Insert(0, new Model.BusinessObject.BusinessPartner { Details = new ViewBusinessPartner { Name = Strings.AllCustomers, BPMasterID = -1 } });
            this.Customers.Insert(0, new Model.BusinessObject.BusinessPartner { Details = new ViewBusinessPartner { Name = Strings.NoneSelected, BPMasterID = -2 } });
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        private void GetProducts()
        {
            this.SaleItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems);
            this.SaleItems.Insert(0, new InventoryItem { Master = new INMaster { Name = Strings.AllProducts, INMasterID = -1 } });
            this.SaleItems.Insert(0, new InventoryItem { Master = new INMaster { Name = Strings.NoneSelected, INMasterID = -2 } });
        }

        /// <summary>
        /// Gets the inventory groups.
        /// </summary>
        private void GetInventoryGroups()
        {
            this.InventoryGroups = new ObservableCollection<INGroup>(this.DataManager.GetInventoryGroups());
            this.InventoryGroups.Insert(0, new INGroup { Name = Strings.AllGroups, INGroupID = -1 });
            this.InventoryGroups.Insert(0, new INGroup { Name = Strings.NoneSelected, INGroupID = -2 });
        }

        /// <summary>
        /// Gets the labels.
        /// </summary>
        private void GetLabels()
        {
            this.Labels = new ObservableCollection<Model.DataLayer.Label>(this.DataManager.GetLabels());
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearLabelAssociation();
            Messenger.Default.Send(Token.Message, Token.CloseLabelAssociationWindow);
        }

        #endregion

        #endregion
    }
}
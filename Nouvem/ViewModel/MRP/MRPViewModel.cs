﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.MRP
{
    public class MRPViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The product stock data.
        /// </summary>
        private ObservableCollection<ProductStockData> mrpOrderData;

        /// <summary>
        /// The product stock data.
        /// </summary>
        private ProductStockData selectedMrpOrderData;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MRPViewModel"/> class.
        /// </summary>
        public MRPViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.ShowOrderCommand = new RelayCommand(this.ShowOrderCommandExecute);
            this.GetMRPCommand = new RelayCommand(this.GetMRPCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the stock modes.
        /// </summary>
        public ObservableCollection<ProductStockData> MRPOrderData
        {
            get
            {
                return this.mrpOrderData;
            }

            set
            {
                this.mrpOrderData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock modes.
        /// </summary>
        public ProductStockData SelectedMRPOrderData
        {
            get
            {
                return this.selectedMrpOrderData;
            }

            set
            {
                this.selectedMrpOrderData = value;
                this.RaisePropertyChanged();
                this.HandleSelectedMRPOrderData();
            }
        }


        #endregion

        #region command

        public ICommand ShowOrderCommand { get; set; }
        public ICommand GetMRPCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
               
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Generates the mrp requirement orders.
        /// </summary>
        private void GetMRPCommandExecute()
        {
            this.MRPOrderData = new ObservableCollection<ProductStockData>();
            bool keepChecking;
          
            do
            {
                ProgressBar.Reset();
                keepChecking = false;
                var recipeProducts = NouvemGlobal.InventoryItems.Where(x => x.Master.MinimumStockLevel.HasValue && x.Master.ReceipeProduct.IsTrue()).ToList();
                var ingredients = NouvemGlobal.InventoryItems.Where(x => x.Master.MinimumStockLevel.HasValue && x.Master.ReceipeProduct.IsNullOrFalse()).ToList();

                ProgressBar.SetUp(0,recipeProducts.Count() + ingredients.Count());
                
                foreach (var product in recipeProducts)
                {
                    ProgressBar.Run();
                    this.GetProductStockData(product);
                    Messenger.Default.Send(product.Master.NouStockModeID, Token.StockModeChange);
                    foreach (var productStockData in this.MRPStockData)
                    {
                        if (product.Master.NouStockModeID == 5)
                        {
                            if (productStockData.ClosingWgt.ToDecimal() < product.Master.MinimumStockLevel
                                && product.Master.PROrderID_Copy.HasValue)
                            {
                                // create production order
                                var scheduledDate =
                                    productStockData.ReferenceDate.AddBusinessDays(-product.Master.OrderLeadTime.ToInt());
                                if (scheduledDate < DateTime.Today)
                                {
                                    scheduledDate = DateTime.Today;
                                }

                                var result =  this.DataManager.CopyProductionOrder(product.Master.PROrderID_Copy.ToInt(), NouvemGlobal.UserId.ToInt(),NouvemGlobal.DeviceId.ToInt(), scheduledDate);
                                if (!string.IsNullOrEmpty(result.Error))
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, result.Error);
                                    return;
                                }

                                if (result.AllocatedWgt.ToInt() <= 0)
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ZeroWgtRecipeError, product.Master.Reference));
                                    return;
                                }

                                result.ProductData = product;
                                result.Product = product.Master.Name;
                                result.Code = product.Master.Code;
                                result.ReferenceDate = scheduledDate;
                                this.MRPOrderData.Add(result);
                                keepChecking = true;
                            }
                        }
                        else if (product.Master.NouStockModeID == 4)
                        {
                            if (productStockData.ClosingQty.ToDecimal() < product.Master.MinimumStockLevel
                                 && product.Master.PROrderID_Copy.HasValue)
                            {
                                var scheduledDate =
                                    productStockData.ReferenceDate.AddBusinessDays(-product.Master.OrderLeadTime.ToInt());
                                if (scheduledDate < DateTime.Today)
                                {
                                    scheduledDate = DateTime.Today;
                                }

                                var result = this.DataManager.CopyProductionOrder(product.Master.PROrderID_Copy.ToInt(), NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(),scheduledDate);
                                if (!string.IsNullOrEmpty(result.Error))
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, result.Error);
                                    return;
                                }

                                if (result.AllocatedQty.ToInt() <= 0)
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ZeroQtyRecipeError, product.Master.Reference));
                                    return;
                                }

                                result.ProductData = product;
                                result.Product = product.Master.Name;
                                result.Code = product.Master.Code;
                                result.ReferenceDate = scheduledDate;
                                this.MRPOrderData.Add(result);
                                keepChecking = true;
                            }
                        }
                    }
                }

                foreach (var product in ingredients)
                {
                    ProgressBar.Run();
                    this.GetProductStockData(product);
                    Messenger.Default.Send(product.Master.NouStockModeID, Token.StockModeChange);
                    foreach (var productStockData in this.MRPStockData)
                    {
                        if (product.Master.NouStockModeID == 5)
                        {
                            if (productStockData.ClosingWgt.ToDecimal() < product.Master.MinimumStockLevel
                                && product.Master.APOrderID_Copy.HasValue)
                            {
                                // create purchase order
                                var scheduledDate =
                                    productStockData.ReferenceDate.AddBusinessDays(-product.Master.OrderLeadTime.ToInt());
                                if (scheduledDate < DateTime.Today)
                                {
                                    scheduledDate = DateTime.Today;
                                }

                                var result = this.DataManager.CopyPurchaseOrder(product.Master.APOrderID_Copy.ToInt(), product.Master.INMasterID, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(), scheduledDate);
                                if (!string.IsNullOrEmpty(result.Error))
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, result.Error);
                                    return;
                                }

                                if (result.AllocatedWgt.ToInt() <= 0)
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ZeroWgtPOError, product.Master.APOrderID_Copy.ToInt(), product.Master.Name));
                                    return;
                                }

                                result.ProductData = product;
                                result.Product = product.Master.Name;
                                result.Code = product.Master.Code;
                                result.ReferenceDate = scheduledDate;
                                this.MRPOrderData.Add(result);
                              
                                keepChecking = true;
                            }
                        }
                        else if (product.Master.NouStockModeID == 4)
                        {
                            if (productStockData.ClosingQty.ToDecimal() < product.Master.MinimumStockLevel
                                 && product.Master.APOrderID_Copy.HasValue)
                            {
                                var scheduledDate =
                                    productStockData.ReferenceDate.AddBusinessDays(-product.Master.OrderLeadTime.ToInt());
                                if (scheduledDate < DateTime.Today)
                                {
                                    scheduledDate = DateTime.Today;
                                }

                                var result = this.DataManager.CopyPurchaseOrder(product.Master.APOrderID_Copy.ToInt(), product.Master.INMasterID, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(), scheduledDate);
                                if (!string.IsNullOrEmpty(result.Error))
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, result.Error);
                                    return;
                                }

                                if (result.AllocatedWgt.ToInt() <= 0)
                                {
                                    ProgressBar.Reset();
                                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ZeroQtyPOError, product.Master.APOrderID_Copy.ToInt(), product.Master.Name));
                                    return;
                                }

                                result.ProductData = product;
                                result.Product = product.Master.Name;
                                result.Code = product.Master.Code;
                                result.ReferenceDate = scheduledDate;
                                this.MRPOrderData.Add(result);

                                keepChecking = true;
                            }
                        }
                    }
                }
            } while (keepChecking);

            ProgressBar.Reset();
            var productionOrdersCreated = this.MRPOrderData.Count(x => x.OrderType == Strings.Production);
            var purchaseOrdersCreated = this.MRPOrderData.Count(x => x.OrderType == Strings.Intake);
            SystemMessage.Write(MessageType.Priority, string.Format(Message.MRPComplete, productionOrdersCreated,purchaseOrdersCreated));
            this.MRPStockData?.Clear();
            this.SelectedMRPOrderData = this.MRPOrderData.FirstOrDefault();
        }

        private void ShowOrderCommandExecute()
        {
            #region validation

            if (this.selectedMrpOrderData == null)
            {
                return;
            }

            #endregion

            if (this.selectedMrpOrderData.OrderType == Strings.Production)
            {
                Messenger.Default.Send(ViewType.BatchSetUp);
                Messenger.Default.Send(this.selectedMrpOrderData.OrderId, Token.ProductionSelected);
                return;
            }

            Messenger.Default.Send(ViewType.APOrder);
            var poOrder = this.DataManager.GetAPOrderByID(this.selectedMrpOrderData.OrderId);
            this.Locator.APOrder.Sale = poOrder;
        }

        /// <summary>
        /// Get the selected products stock data.
        /// </summary>
        private void GetProductStockData(InventoryItem product)
        {
            var data = this.DataManager.GetProductStockData(product.Master.INMasterID);
            var wgt = data.Sum(x => x.ProductStock.Wgt);
            var qty = data.Sum(x => x.ProductStock.Qty);
            this.MRPStockData = new ObservableCollection<ProductStockData>(this.DataManager.GetMRPData(product.Master.INMasterID, wgt.ToDecimal(), qty.ToDecimal()));
        }

        /// <summary>
        /// Handles a selected mrp order item.
        /// </summary>
        private void HandleSelectedMRPOrderData()
        {
            if (this.selectedMrpOrderData == null)
            {
                return;
            }

            this.GetProductStockData(this.selectedMrpOrderData.ProductData);
        }

        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseMRPWindow);
        }

        #endregion
    }
}

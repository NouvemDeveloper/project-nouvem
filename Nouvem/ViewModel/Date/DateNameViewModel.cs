﻿// -----------------------------------------------------------------------
// <copyright file="DateNameViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Date
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class DateNameViewModel : NouvemViewModelBase
    {
        #region field

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the DateNameViewModel class.
        /// </summary>
        public DateNameViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.DateTemplateNames = new ObservableCollection<DateTemplateName>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessGS1);
            this.GetDateTemplateNames();
        }

        #endregion

        #region property

        /// <summary>
        /// Get the date Masters.
        /// </summary>
        public ObservableCollection<DateTemplateName> DateTemplateNames { get; private set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateDateTemplateNames();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the date template names group.
        /// </summary>
        private void UpdateDateTemplateNames()
        {
            try
            {
                if (this.DataManager.AddOrUpdateDateTemplateNames(this.DateTemplateNames))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DateTemplateNamesUpdated);
                    Messenger.Default.Send(Token.Message, Token.DateTemplateNamesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DateTemplateNamesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DateTemplateNamesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDateName();
            Messenger.Default.Send(Token.Message, Token.CloseDateTemplateNameWindow);
        }

        /// <summary>
        /// Gets the application date template names information.
        /// </summary>
        private void GetDateTemplateNames()
        {
            this.DateTemplateNames.Clear();
            foreach (var date in this.DataManager.GetDateTemplateNames())
            {
                this.DateTemplateNames.Add(date);
            }
        }

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="DateTemplateAllocationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Date
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class DateTemplateAllocationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The template names reference.
        /// </summary>
        private ObservableCollection<DateTemplateName> dateTemplateNames;

        /// <summary>
        /// The selected template name
        /// </summary>
        private DateTemplateName selectedTemplateName;

        /// <summary>
        /// The selected available date reference.
        /// </summary>
        private ViewDatesMaster selectedAvailableDateMaster;

        /// <summary>
        /// The selected assigned date reference.
        /// </summary>
        private ViewDatesMaster selectedAssignedDateMaster;

        /// <summary>
        /// The available date masters reference.
        /// </summary>
        private ObservableCollection<ViewDatesMaster> availableDates;

        /// <summary>
        /// The assigned date masters reference.
        /// </summary>
        private ObservableCollection<ViewDatesMaster> assignedDates;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the DateTemplateAllocationViewModel class.
        /// </summary>
        public DateTemplateAllocationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // register fot a date master data change.
            Messenger.Default.Register<string>(this, Token.DateTemplateNamesUpdated, s => this.GetDateTemplateNames(true));

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            // Command to move the selected date.
            this.MoveDateCommand= new RelayCommand<string>(this.MoveDateCommandExecute);

            #endregion

            #region instantiation

            this.DateTemplateNames = new ObservableCollection<DateTemplateName>();
            this.DatesMasters = new ObservableCollection<ViewDatesMaster>();
            this.AvailableDatesMasters = new ObservableCollection<ViewDatesMaster>();
            this.AssignedDatesMasters = new ObservableCollection<ViewDatesMaster>();
            this.DatesTemplateAllocations = new List<DateTemplateAllocation>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessGS1);
            this.GetDatesMasters();
            this.GetTemplateAllocations();
            this.GetDateTemplateNames();
        }

        #endregion

        #region property

        /// <summary>
        /// Get the dates template allocations.
        /// </summary>
        public IList<DateTemplateAllocation> DatesTemplateAllocations { get; private set; }

        /// <summary>
        /// Get the dates Masters.
        /// </summary>
        public ObservableCollection<ViewDatesMaster> DatesMasters { get; private set; }

        /// <summary>
        /// Get or sets the available dates Masters.
        /// </summary>
        public ObservableCollection<ViewDatesMaster> AvailableDatesMasters
        {
            get
            {
                return this.availableDates;
            }

            set
            {
                this.availableDates = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the assigned dates Masters.
        /// </summary>
        public ObservableCollection<ViewDatesMaster> AssignedDatesMasters
        {
            get
            {
                return this.assignedDates;
            }

            set
            {
                this.assignedDates = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the date Masters.
        /// </summary>
        public ObservableCollection<DateTemplateName> DateTemplateNames
        {
            get
            {
                return this.dateTemplateNames;
            }

            set
            {
                this.dateTemplateNames = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected date template name.
        /// </summary>
        public DateTemplateName SelectedTemplateName
        {
            get
            {
                return this.selectedTemplateName;
            }

            set
            {
                this.selectedTemplateName = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedTemplateName();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected available date master.
        /// </summary>
        public ViewDatesMaster SelectedAvailableDateMaster
        {
            get
            {
                return this.selectedAvailableDateMaster;
            }

            set
            {
                this.selectedAvailableDateMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected assigned date master.
        /// </summary>
        public ViewDatesMaster SelectedAssignedDateMaster
        {
            get
            {
                return this.selectedAssignedDateMaster;
            }

            set
            {
                this.selectedAssignedDateMaster = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to move the dates.
        /// </summary>
        public ICommand MoveDateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// drill down to the date names.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.DateTemplateName, 0), Token.DrillDown);
            this.AvailableDatesMasters.Clear();
            this.AssignedDatesMasters.Clear();
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    //this.UpdateDateTemplateNames();
                    this.UpdateTemplate();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the current template.
        /// </summary>
        private void UpdateTemplate()
        {
            if (this.DataManager.AddDatesToTemplate(this.AssignedDatesMasters,
                this.selectedTemplateName.DateTemplateNameID))
            {
                SystemMessage.Write(MessageType.Priority, Message.DateAddedToTemplate);
                this.Refresh();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DateNotAddedToTemplate);
            }
        }

        /// <summary>
        /// Updates the date template names group.
        /// </summary>
        private void UpdateDateTemplateNames()
        {
            try
            {
                if (this.DataManager.AddOrUpdateDateTemplateNames(this.DateTemplateNames))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DateTemplateNamesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DateTemplateNamesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DateTemplateNamesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDateTemplateAllocation();
            Messenger.Default.Send(Token.Message, Token.CloseDateTemplateAllocationWindow);
        }

        /// <summary>
        /// Gets the application date template names information.
        /// </summary>
        /// <param name="selectRecentlyAdded">Flg to determine if we are selecting the most recently added template.</param>
        private void GetDateTemplateNames(bool selectRecentlyAdded = false)
        {
            this.DateTemplateNames.Clear();
            foreach (var date in this.DataManager.GetDateTemplateNames())
            {
                this.DateTemplateNames.Add(date);
            }

            this.DateTemplateNames.Add(new DateTemplateName { Name = Strings.DefineNew });

            // Select the first, or most recently added template.
            this.SelectedTemplateName = selectRecentlyAdded
                ? this.DateTemplateNames.ElementAt(this.dateTemplateNames.Count - 2)
                : this.DateTemplateNames.First();
        }

        /// <summary>
        /// Handles the selection of a date template name.
        /// </summary>
        private void HandleSelectedTemplateName()
        {
            // check if we are defining a new template name
            if (this.selectedTemplateName.Name.Equals(Strings.DefineNew))
            {
                // new..shortcut to the template name view.
                Messenger.Default.Send(ViewType.DateTemplateName);
                this.AvailableDatesMasters.Clear();
                this.AssignedDatesMasters.Clear();
                return;
            }

            this.AvailableDatesMasters.Clear();
            this.AssignedDatesMasters.Clear();
            this.AvailableDatesMasters = new ObservableCollection<ViewDatesMaster>(this.DatesMasters);
         
            this.DatesTemplateAllocations.ToList().ForEach(x =>
            {
                if (x.DateTemplateNameID == this.selectedTemplateName.DateTemplateNameID)
                {
                    var localDateMaster =
                        this.DatesMasters.FirstOrDefault(dateMaster => dateMaster.DateMasterID == x.DateMasterID);

                    if (localDateMaster != null)
                    {
                        localDateMaster.Batch = x.Batch.ToBool();
                        localDateMaster.Transaction = x.Transaction.ToBool();
                        localDateMaster.Required = x.Transaction.ToBool();
                        localDateMaster.Reset = x.Transaction.ToBool();
                        this.AssignedDatesMasters.Add(localDateMaster);
                        this.AvailableDatesMasters.Remove(localDateMaster);
                    }
                }
            });
        }

        /// <summary>
        /// Gets the application dates masters information.
        /// </summary>
        private void GetDatesMasters()
        {
            this.DatesMasters.Clear();
            foreach (var date in this.DataManager.GetDateMasters())
            {
                this.DatesMasters.Add(date);
            }
        }

        /// <summary>
        /// Gets the date template allocations.
        /// </summary>
        private void GetTemplateAllocations()
        {
            this.DatesTemplateAllocations = this.DataManager.GetDateTemplateAllocations();
        }

        /// <summary>
        /// handles the allocation/deallocation of dates to and from the selected template.
        /// </summary>
        /// <param name="direction">The allocation direction.</param>
        private void MoveDateCommandExecute(string direction)
        {
            #region validation

            if (!this.CanMoveDate(direction))
            {
                return;
            }

            #endregion

            this.SetControlMode(ControlMode.Update);

            if (direction.Equals(Constant.MoveForward))
            {
                this.AssignedDatesMasters.Add(this.selectedAvailableDateMaster);
                this.AvailableDatesMasters.Remove(this.selectedAvailableDateMaster);

                return;
            }

            this.AvailableDatesMasters.Add(this.selectedAssignedDateMaster);
            this.AssignedDatesMasters.Remove(this.selectedAssignedDateMaster);
        }

        /// <summary>
        /// Determines if a move can be made.
        /// </summary>
        /// <returns>A flag, which indicates whether a move can be made.</returns>
        private bool CanMoveDate(string direction)
        {
            if (direction.Equals(Constant.MoveForward) && this.selectedAvailableDateMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            if (direction.Equals(Constant.MoveBack) && this.selectedAssignedDateMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetTemplateAllocations();
            this.SetControlMode(ControlMode.OK);
        }

        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.AccountsIntegration.BusinessObject;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.BusinessPartner;
using Quartz.Util;

namespace Nouvem.ViewModel.Account
{
    public class SyncPricesViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The incoming suppliers.
        /// </summary>
        private ObservableCollection<Prices> filteredPrices = new ObservableCollection<Prices>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncPartnersViewModel"/> class.
        /// </summary>
        public SyncPricesViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            #endregion

            this.SelectedPrices = new List<Prices>();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the incoming products.
        /// </summary>
        public ObservableCollection<Prices> FilteredPrices
        {
            get
            {
                return this.filteredPrices;
            }

            set
            {
                this.filteredPrices = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected products.
        /// </summary>
        public IList<Prices> SelectedPrices { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a grid selection change.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a grid selection change.
        /// </summary>
        public ICommand OnLoadingCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.IsBusy = true;

            if (!ApplicationSettings.SyncPartnersLoadedForFirstTime)
            {
                ApplicationSettings.SyncPartnersLoadedForFirstTime = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    System.Threading.Thread.Sleep(150);
                    Task.Factory.StartNew(() =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            this.GetAccountsPrices();
                            this.SetControlMode(ControlMode.OK);
                        });
                    }).ContinueWith(x =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.IsBusy = false;
                        }));
                    });
                }));

                return;
            }

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.GetAccountsPrices();
                    this.SetControlMode(ControlMode.OK);
                });
            }).ContinueWith(x =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.IsBusy = false;
                }));
            });
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Import:
                    if (this.AccountsManager.CanConnectToAccounts)
                    {
                        this.ImportPrices();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoAccountsAccess);
                    }

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handles the ui cancellation.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Retrieves the accounts partners.
        /// </summary>
        private void GetAccountsPrices()
        {
            try
            {
                this.AccountsManager.Connect();
                var localPrices = this.AccountsManager.GetAccountPrices().OrderBy(x => x.Code).ToList();
                foreach (var localPrice in localPrices)
                {
                    if (string.IsNullOrWhiteSpace(localPrice.Product))
                    {
                        var localProduct =
                            NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Code == localPrice.Code);
                        if (localProduct != null)
                        {
                            localPrice.Product = localProduct.Master.Name;
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), $" No Product Match:|{localPrice.Code}|");
                        }
                    }

                    if (string.IsNullOrWhiteSpace(localPrice.PartnerName))
                    {
                        var localPartner =
                            NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.Code == localPrice.PartnerCode);
                        if (localPartner != null)
                        {
                            localPrice.PartnerName = localPartner.Details.Name;
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), $" No Business Partner Match:|{localPrice.PartnerCode}|");
                        }
                    }
                }

                this.FilteredPrices = new ObservableCollection<Prices>(localPrices);
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
            finally
            {
                this.AccountsManager.Logout();
            }
        }

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedPrices == null || !this.SelectedPrices.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.SetControlMode(ControlMode.Import);
        }

        /// <summary>
        /// Imports the selected partners.
        /// </summary>
        private void ImportPrices()
        {
            var error = string.Empty;
            var codes = new List<string>();

            try
            {
                ProgressBar.SetUp(0, this.SelectedPrices.Count);
                ProgressBar.Run();
                this.DataManager.AddOrUpdatePrices(this.SelectedPrices);
                foreach (var selectedProduct in this.SelectedPrices)
                {
                    try
                    {
                        codes.Add(selectedProduct.Code);
                        ProgressBar.Run();
                    }
                    catch (Exception ex)
                    {
                        error = Message.OneOrMoreProductsNotImported;
                        this.Log.LogError(this.GetType(), ex.Message);
                        SystemMessage.Write(MessageType.Issue, error);
                    }
                }

                this.FilteredPrices.Clear();
            }
            finally
            {
                ProgressBar.Reset();
                if (error == string.Empty)
                {
                    SystemMessage.Write(MessageType.Priority, Message.ProductsImported);
                }
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSyncPrices);
        }

        #endregion
    }
}



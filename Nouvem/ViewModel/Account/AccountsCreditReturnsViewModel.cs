﻿// -----------------------------------------------------------------------
// <copyright file="AccountsPurchasesViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Payments;
using Nouvem.ViewModel.Sales.ARReturn;

namespace Nouvem.ViewModel.Account
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class AccountsCreditReturnsViewModel : CreditNoteCreationViewModel
    {
        #region private

        /// <summary>
        /// The selected grid sales.
        /// </summary>
        private IList<Sale> allInvoices = new List<Sale>();

        /// <summary>
        /// The from search date.
        /// </summary>
        private DateTime fromPurchasesDate;

        /// <summary>
        /// The to search date.
        /// </summary>
        private DateTime toPurchasesDate;

        /// <summary>
        /// The running total.
        /// </summary>
        private decimal runningTotal;

        #endregion

        #region constructor

        public AccountsCreditReturnsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            this.RefreshCommand = new RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            });

            #endregion

            this.ToPurchasesDate = DateTime.Today;
        }

        #endregion

        #region public

        #region property

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromPurchasesDate
        {
            get
            {
                return this.fromPurchasesDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.fromPurchasesDate = DateTime.Today;
                }
                else
                {
                    this.fromPurchasesDate = value;
                }

                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    ApplicationSettings.FromPurchasesDate = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToPurchasesDate
        {
            get
            {
                return this.toPurchasesDate;
            }

            set
            {
                this.toPurchasesDate = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the running total.
        /// </summary>
        public decimal RunningTotal
        {
            get
            {
                return this.runningTotal;
            }

            set
            {
                this.runningTotal = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to refresh the payments.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Refreshes the intakes data.
        /// </summary>
        protected override void Refresh()
        {
            this.RefreshARInvoices();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Export:
                    if (this.AccountsManager.CanConnectToAccounts)
                    {
                        this.ExportInvoices();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoAccountsAccess);
                    }

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        protected override void HandleShowAllOrders()
        {
            this.Refresh();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.ShowAllOrders = ApplicationSettings.FromCreditReturnsShowAllOrders;
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.FilteredSales?.Clear();
            this.FilteredSales = null;
            ApplicationSettings.FromCreditReturnsShowAllOrders = this.ShowAllOrders;
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void RefreshARInvoices()
        {
            this.allInvoices = this.DataManager.GetARReturnInvoices().Where(x => x.DocumentDate != null).ToList();

            if (this.ShowAllOrders)
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.allInvoices);
            }
            else
            {
                var invoicesReady =
                    this.allInvoices.Where(x => x.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID
                                                && x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
                this.FilteredSales = new ObservableCollection<Sale>(invoicesReady.Where(x => !x.ExcludeFromAccounts));
            }
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseAccountsReturns);
        }

        #endregion

        #region private

        #region helper

        /// <summary>
        /// Creates the invoices.
        /// </summary>
        private void ExportInvoices()
        {
            this.Log.LogDebug(this.GetType(), "ExportInvoices(): Exporting purchases..");
            List<int> invoiceNos = null;
            List<int> invoiceNosToDisplay = new List<int>();

            try
            {
                //var invoicesToExclude =
                //        this.FilteredSales.Where(
                //            x => x.ExcludeFromAccounts).ToList();

                //if (invoicesToExclude.Any())
                //{
                //    this.DataManager.ExcludePaymentsFromAccounts(invoicesToExclude);
                //}

                if (this.SelectedSales != null)
                {
                    var localInvoices =
                        this.SelectedSales.Where(
                            x => x.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID && !x.ExcludeFromAccounts).ToList();
                    if (localInvoices.Any())
                    {
                        invoiceNos = new List<int>();

                        ProgressBar.SetUp(0, localInvoices.Count * 2);

                        var message = this.AccountsManager.Connect();
                        if (message != string.Empty)
                        {
                            SystemMessage.Write(MessageType.Issue, message);
                            this.Log.LogError(this.GetType(), message);
                            return;
                        }

                        foreach (var invoiceToExport in localInvoices.OrderBy(x => x.Number))
                        {
                            var localInvoice = this.DataManager.GetARReturnInvoiceByID(invoiceToExport.SaleID);
                            ProgressBar.Run();

                            var postMessage = this.AccountsManager.PostSalesCreditNote(localInvoice);
                            if (!postMessage.Equals(string.Empty))
                            {
                                SystemMessage.Write(MessageType.Issue, postMessage);
                            }
                            else
                            {
                                invoiceNos.Add(localInvoice.SaleID);
                                invoiceNosToDisplay.Add(localInvoice.Number);
                            }

                            ProgressBar.Run();
                        }

                        if (invoiceNos.Any() && this.DataManager.ExportARReturnInvoice(invoiceNos))
                        {
                            var numbers = invoiceNosToDisplay.Count == 1 ? invoiceNosToDisplay.First().ToString() : string.Join(",", invoiceNosToDisplay.Select(x => x.ToString()));
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.CreditNoteExported, numbers));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                try
                {
                    ProgressBar.Reset();
                    this.SetControlMode(ControlMode.OK);
                    this.AccountsManager.Logout();
                    this.Refresh();
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        protected override void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedSales == null || !this.SelectedSales.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.RunningTotal = this.SelectedSales.Sum(x => x.GrandTotalIncVAT).ToDecimal();

            this.SetControlMode(ControlMode.Export);
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #endregion
    }
}




﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.BusinessPartner;
using Quartz.Util;

namespace Nouvem.ViewModel.Account
{
    public class SyncProductsViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The incoming suppliers.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.InventoryItem> filteredProducts = new ObservableCollection<Model.BusinessObject.InventoryItem>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncPartnersViewModel"/> class.
        /// </summary>
        public SyncProductsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            #endregion

            this.SelectedProducts = new List<Model.BusinessObject.InventoryItem>();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the incoming products.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.InventoryItem> FilteredProducts
        {
            get
            {
                return this.filteredProducts;
            }

            set
            {
                this.filteredProducts = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected products.
        /// </summary>
        public IList<Model.BusinessObject.InventoryItem> SelectedProducts { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a grid selection change.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a grid selection change.
        /// </summary>
        public ICommand OnLoadingCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.IsBusy = true;

            if (!ApplicationSettings.SyncPartnersLoadedForFirstTime)
            {
                ApplicationSettings.SyncPartnersLoadedForFirstTime = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    System.Threading.Thread.Sleep(150);
                    Task.Factory.StartNew(() =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            this.GetAccountsProducts();
                            this.SetControlMode(ControlMode.OK);
                        });
                    }).ContinueWith(x =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.IsBusy = false;
                        }));
                    });
                }));

                return;
            }

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.GetAccountsProducts();
                    this.SetControlMode(ControlMode.OK);
                });
            }).ContinueWith(x =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.IsBusy = false;
                }));
            });
        }
       
        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Import:
                    if (this.AccountsManager.CanConnectToAccounts)
                    {
                        this.ImportProducts();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoAccountsAccess);
                    }

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handles the ui cancellation.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Retrieves the accounts partners.
        /// </summary>
        private void GetAccountsProducts()
        {
            try
            {
                this.AccountsManager.Connect();
                this.FilteredProducts = new ObservableCollection<InventoryItem>(this.AccountsManager.GetAccountProducts().OrderBy(x => x.Name));
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
            finally
            {
                this.AccountsManager.Logout();
            }
        }

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedProducts == null || !this.SelectedProducts.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.SetControlMode(ControlMode.Import);
        }

        /// <summary>
        /// Imports the selected partners.
        /// </summary>
        private void ImportProducts()
        {
            var error = string.Empty;
            var codes = new List<string>();

            try
            {
                ProgressBar.SetUp(0, this.SelectedProducts.Count);
                ProgressBar.Run();
                foreach (var selectedProduct in this.SelectedProducts)
                {
                    try
                    {
                        this.Locator.InventoryMaster.AddItem(selectedProduct);
                        codes.Add(selectedProduct.Code);
                        ProgressBar.Run();
                    }
                    catch (Exception ex)
                    {
                        error = Message.OneOrMoreProductsNotImported;
                        this.Log.LogError(this.GetType(), ex.Message);
                        SystemMessage.Write(MessageType.Issue, error);
                    }
                }

                foreach (var code in codes)
                {
                    var importedProduct =
                        this.FilteredProducts.FirstOrDefault(x => x.Code == code);
                    if (importedProduct!= null)
                    {
                        this.FilteredProducts.Remove(importedProduct);
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                if (error == string.Empty)
                {
                    SystemMessage.Write(MessageType.Priority, Message.ProductsImported);
                }
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSyncProducts);
        }

        #endregion
    }
}


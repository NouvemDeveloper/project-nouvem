﻿// -----------------------------------------------------------------------
// <copyright file="WarehouseViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Plant
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class WarehouseViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected warehouse.
        /// </summary>
        private Warehouse selectedWarehouse;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the WarehouseViewModel class.
        /// </summary>
        public WarehouseViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration

            // Register to display only the incoming warehouse.
            Messenger.Default.Register<int>(this, Token.DisplaySelectedWarehouse, i =>
            {
                var warehouseToDisplay = this.Warehouses.FirstOrDefault(x => x.WarehouseID == i);
                this.Warehouses = new ObservableCollection<Warehouse> {warehouseToDisplay};
            });

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            // Handler, to remove the selected item.
            this.RemoveItemCommand = new RelayCommand(this.RemoveItemCommandExecute);

            #endregion

            #region instantiation

            this.Warehouses = new ObservableCollection<Warehouse>();

            #endregion

            this.GetWarehouses();
        }

        #endregion

        #region property
   
        /// <summary>
        /// Gets or sets the selected warehouse.
        /// </summary>
        public Warehouse SelectedWarehouse
        {
            get
            {
                return this.selectedWarehouse;
            }

            set
            {
                this.selectedWarehouse = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the warehouses.
        /// </summary>
        public ObservableCollection<Warehouse> Warehouses { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the warehouses.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Drill down to locations.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            if (this.selectedWarehouse != null)
            {
                SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
                Messenger.Default.Send(ViewType.WarehouseLocation);
                Messenger.Default.Send(this.selectedWarehouse, Token.DisplayWarehouse);
            }
        }

        /// <summary>
        /// Removes the selected
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.SelectedWarehouse != null)
            {
                if (this.DataManager.RemoveWarehouse(this.selectedWarehouse))
                {
                    SystemMessage.Write(MessageType.Priority, Message.WarehouseRemoved);
                    this.Warehouses.Remove(this.SelectedWarehouse);
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.WarehouseNotRemoved);
            }
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateWarehouses();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the warehouses.
        /// </summary>
        private void UpdateWarehouses()
        {
            try
            {
                if (this.DataManager.AddOrUpdateWarehouses(this.Warehouses))
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DataUpdated, "Warehouses"));
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.DataNotUpdated, "Warehouses"));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, string.Format(Message.DataNotUpdated, "Warehouses"));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearWarehouse();
            Messenger.Default.Send(Token.Message, Token.CloseWarehouseWindow);
        }

        /// <summary>
        /// Gets the application containers.
        /// </summary>
        private void GetWarehouses()
        {
            this.Warehouses.Clear();
            foreach (var warehouse in this.DataManager.GetWarehouses())
            {
                this.Warehouses.Add(warehouse);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}
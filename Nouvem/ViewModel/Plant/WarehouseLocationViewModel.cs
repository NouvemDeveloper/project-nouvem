﻿// -----------------------------------------------------------------------
// <copyright file="WarehouseLocationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Plant
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class WarehouseLocationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected warehouse.
        /// </summary>
        private Warehouse selectedWarehouse;

        /// <summary>
        /// The selected warehouse location.
        /// </summary>
        private WarehouseLocation selectedWarehouseLocation;

        /// <summary>
        /// The warehouse locations.
        /// </summary>
        private ObservableCollection<WarehouseLocation> warehouseLocations;

        /// <summary>
        /// All the warehouse locations.
        /// </summary>
        private ObservableCollection<WarehouseLocation> allWarehouseLocations;

        /// <summary>
        /// The warehouses.
        /// </summary>
        private ObservableCollection<Warehouse> warehouses;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the WarehouseLocationViewModel class.
        /// </summary>
        public WarehouseLocationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration

            // Register for the incoming warehouse.
            Messenger.Default.Register<Warehouse>(this, Token.DisplayWarehouse, w => this.SelectedWarehouse = this.Warehouses.FirstOrDefault(x => x.WarehouseID == w.WarehouseID));
          
            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            // Handle the ui loaded event.
            this.OnLoadingCommand = new RelayCommand(this.GetWarehouses);

            // Handler, to remove the selected item.
            this.RemoveItemCommand = new RelayCommand(this.RemoveItemCommandExecute);

            #endregion

            #region instantiation

            this.Warehouses = new ObservableCollection<Warehouse>();
            this.WarehouseLocations = new ObservableCollection<WarehouseLocation>();
            this.allWarehouseLocations = new ObservableCollection<WarehouseLocation>();

            #endregion
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the selected warehouse.
        /// </summary>
        public Warehouse SelectedWarehouse
        {
            get
            {
                return this.selectedWarehouse;
            }

            set
            {
                this.selectedWarehouse = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.WarehouseLocations = new ObservableCollection<WarehouseLocation>(this.allWarehouseLocations.Where(x => x.WarehouseID == value.WarehouseID));
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected warehouse locatio.
        /// </summary>
        public WarehouseLocation SelectedWarehouseLocation
        {
            get
            {
                return this.selectedWarehouseLocation;
            }

            set
            {
                this.selectedWarehouseLocation = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the warehouse locations.
        /// </summary>
        public ObservableCollection<WarehouseLocation> WarehouseLocations
        {
            get
            {
                return this.warehouseLocations;
            }

            set
            {
                this.warehouseLocations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the warehouses.
        /// </summary>
        public ObservableCollection<Warehouse> Warehouses
        {
            get
            {
                return this.warehouses;
            }

            set
            {
                this.warehouses = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the warehouses.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the loaded event.
        /// </summary>
        public ICommand OnLoadingCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Removes the selected item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.SelectedWarehouseLocation != null)
            {
                if (this.DataManager.RemoveWarehouseLocation(this.selectedWarehouseLocation))
                {
                    SystemMessage.Write(MessageType.Priority, Message.WarehouseLocationRemoved);
                    this.WarehouseLocations.Remove(this.SelectedWarehouseLocation);
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.WarehouseLocationNotRemoved);
            }
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateWarehouseLocations();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the warehouse locations.
        /// </summary>
        private void UpdateWarehouseLocations()
        {
            #region validation

            if (this.selectedWarehouse == null)
            {
                return;
            }

            #endregion

            try
            {
                foreach (var location in this.warehouseLocations)
                {
                    location.WarehouseID = this.selectedWarehouse.WarehouseID;
                }

                if (this.DataManager.AddOrUpdateWarehouseLocations(this.WarehouseLocations))
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DataUpdated, "Warehouse locations"));
                    this.RefreshAllLocations();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.DataNotUpdated, "Warehouse locations"));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, string.Format(Message.DataNotUpdated, "Warehouse locations"));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearWarehouseLocation();
            Messenger.Default.Send(Token.Message, Token.CloseWarehouseLocationWindow);
        }

        /// <summary>
        /// Gets the application warehouses and locations.
        /// </summary>
        private void GetWarehouses()
        {
            this.Warehouses.Clear();
            foreach (var warehouse in this.DataManager.GetWarehouses())
            {
                this.Warehouses.Add(warehouse);
            }

            this.allWarehouseLocations = new ObservableCollection<WarehouseLocation>(this.DataManager.GetWarehouseLocations());
        }

        /// <summary>
        /// Refreshes the application loctaions.
        /// </summary>
        private void RefreshAllLocations()
        {
            this.allWarehouseLocations = new ObservableCollection<WarehouseLocation>(this.DataManager.GetWarehouseLocations());
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

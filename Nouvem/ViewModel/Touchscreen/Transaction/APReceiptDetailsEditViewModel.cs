﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptDetailsEditViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Transaction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Purchases.APReceipt;

    public class APReceiptDetailsEditViewModel : APReceiptDetailsViewModel
    {
        //#region field



        //#endregion

        //#region constructor

        ///// <summary>
        ///// Initializes a new instance of the <see cref="APReceiptDetailsEditViewModel"/> class.
        ///// </summary>
        //public APReceiptDetailsEditViewModel()
        //{
        //    if (this.IsInDesignMode)
        //    {
        //        return;
        //    }

        //    #region message registration

        //    // register for the incoming traceability results.
        //    Messenger.Default.Register<Tuple<string, List<TraceabilityResult>>>(this, Token.TraceabilityEditValues, this.ParseTraceabilityValues);

        //    #endregion

        //    #region command handler

        //    #endregion
        //}

        //#endregion

        //#region public interface

        //#region property

        

        ///// <summary>
        ///// Override the handling of the control selection.
        ///// </summary>
        //protected override void ControlSelectionCommandExecute()
        //{
        //    switch (this.CurrentMode)
        //    {
        //        case ControlMode.Add:
        //            //this.AddSale();
        //            break;

        //        case ControlMode.Find:
        //            // this.FindSale();
        //            break;

        //        case ControlMode.Update:
        //            this.SaveEdit();
        //            break;

        //        case ControlMode.OK:
        //            this.GetTraceabilityData();
        //            break;
        //    }
        //}

        ///// <summary>
        ///// Close and clean up.
        ///// </summary>
        //protected override void CancelSelectionCommandExecute()
        //{
        //    this.Close();
        //}

        //#endregion

        //#region command



        //#endregion

        //#endregion

        //#region protected

        ///// <summary>
        ///// Close and clean up.
        ///// </summary>
        //protected override void Close()
        //{
        //    ViewModelLocator.ClearAPReceiptDetailsEdit();
        //    Messenger.Default.Send(Token.Message, Token.CloseTransactionDetailsWindow);
        //}

        ///// <summary>
        ///// Direct the incoming sale detail.
        ///// </summary>
        //protected override void DirectSaleDetail()
        //{
        //    if (this.SelectedSaleDetail.StockMode == StockMode.EditTransaction)
        //    {
        //        // We are editing existing transaction data.
        //        this.HandleSaleDetailForTransactionsEdit();
        //    }
        //}

        //#endregion

        //#region private

        //#region edit

        ///// <summary>
        ///// Handles the incoming sale detail which is being used to edit transactions.
        ///// </summary>
        //private void HandleSaleDetailForTransactionsEdit()
        //{
        //    this.Refresh();
        //    int? traceabilityTemplateNameId = 0;
        //    int? qualityTemplateNameId = 0;
        //    int? dateTemplateNameId = 0;

        //    #region insurance

        //    // this did happen (null inItem), but shouldn't happen now. Leaving this in, just in case.
        //    if (this.SelectedSaleDetail.InventoryItem == null)
        //    {
        //        this.SelectedSaleDetail.InventoryItem =
        //            NouvemGlobal.InventoryItems.FirstOrDefault(
        //                x => x.Master.INMasterID == this.SelectedSaleDetail.INMasterID);

        //        if (this.SelectedSaleDetail.InventoryItem == null)
        //        {
        //            // should never happen, but just in case.
        //            this.Log.LogError(this.GetType(), string.Format("HandlesaleDetail():{0}", Message.InventoryItemNotFound));
        //            SystemMessage.Write(MessageType.Issue, Message.InventoryItemNotFound);
        //            return;
        //        }
        //    }

        //    #endregion

        //    var item = this.SelectedSaleDetail.InventoryItem.Master;

        //    this.ItemNo = item.Code;
        //    this.ItemDescription = item.Name;
        //    traceabilityTemplateNameId = item.TraceabilityTemplateNameID;
        //    qualityTemplateNameId = item.QualityTemplateNameID;
        //    dateTemplateNameId = item.DateTemplateNameID;

        //    var traceability = this.traceabilityData.FirstOrDefault(x => x.Key.Item1 == traceabilityTemplateNameId && x.Key.Item2.Equals(Constant.Traceability)).Value;
        //    var quality = this.traceabilityData.FirstOrDefault(x => x.Key.Item1 == qualityTemplateNameId && x.Key.Item2.Equals(Constant.Quality)).Value;
        //    var date = this.traceabilityData.FirstOrDefault(x => x.Key.Item1 == dateTemplateNameId && x.Key.Item2.Equals(Constant.Date)).Value;

        //    if (traceability == null)
        //    {
        //        traceability = new List<TraceabilityData>();
        //    }

        //    if (quality == null)
        //    {
        //        quality = new List<TraceabilityData>();
        //    }

        //    if (date == null)
        //    {
        //        date = new List<TraceabilityData>();
        //    }

        //    var data = traceability.Concat(quality).Concat(date);

        //    var localBatchNo = this.SelectedSaleDetail.GoodsReceiptDatas.Last().BatchNumber;

        //    // add the generated batch number to our attributes.
        //    foreach (var localTraceabilityData in data)
        //    {
        //        localTraceabilityData.BatchNumber = localBatchNo;
        //    }

        //    this.BatchNumber = localBatchNo;

        //    var batchAttributes = new List<TraceabilityData>();
        //    var serialAttributes = new List<TraceabilityData>();

        //    // Divide the traceability attributes into batch and serial (can be both)
        //    foreach (var localData in data)
        //    {
        //        if (localData.Batch == true)
        //        {
        //            var value = this.GetTraceabilityValue(localData);
        //            localData.TraceabilityValue = value.Item1;
        //            localData.TraceabilityId = value.Item2;
        //            batchAttributes.Add(localData);
        //        }

        //        if (localData.Transaction == true)
        //        {
        //            var localSerialData = this.CreateDeepTraceData(localData);
        //            var value = this.GetSerialTraceabilityValue(localSerialData);
        //            localSerialData.TraceabilityValue = value.Item1;
        //            localSerialData.TraceabilityId = value.Item2;
        //            serialAttributes.Add(localSerialData);
        //        }
        //    }

        //    Messenger.Default.Send(Token.Message, Token.ClearData);

        //    // send the batch attributes
        //    if (batchAttributes.Any())
        //    {
        //        Messenger.Default.Send(batchAttributes, Token.CreateEditDynamicControls);
        //    }

        //    // send the serial attributes
        //    if (serialAttributes.Any())
        //    {
        //        Messenger.Default.Send(serialAttributes, Token.CreateEditDynamicSerialControls);
        //    }
        //}

        //#endregion


        ///// <summary>
        ///// Saves an edited transaction data.
        ///// </summary>
        //private void SaveEdit()
        //{
        //    this.Log.LogDebug(this.GetType(), "SaveEdit(): Attempting to save edited transaction data");
        //    if (this.SelectedGoodsInDetail.SerialNumber == null)
        //    {
        //        this.storedTransactionWeight = 0;
        //        this.weightDifference = 0;
        //        this.SetControlMode(ControlMode.OK);
        //        return;
        //    }

        //    // send a message to the view to send it's dynamically created batch traceability control data back here.
        //    Messenger.Default.Send(Token.Message, Token.GetTraceabilityDataForEdit);
        //}


        //private void GetTraceabilityData()
        //{
        //    // fetch the traceability values
        //    Messenger.Default.Send(Token.Message, Token.GetEditTraceabilityData);
        //}

        ///// <summary>
        ///// Handle the item traceability data, completing the weight recording process.
        ///// </summary>
        ///// <param name="dataResults">The incoming traceability data.</param>
        //private void ParseTraceabilityValues(Tuple<string, List<TraceabilityResult>> dataResults)
        //{
        //    this.Log.LogDebug(this.GetType(), "ParseTraceabilityValues: Parsing values started");
        //    var error = dataResults.Item1;
        //    var data = dataResults.Item2;

        //    #region validation

        //    if (error != string.Empty)
        //    {
        //        // value hasn't been selected/entered by the user, so exit here.
        //        this.Log.LogDebug(this.GetType(), "Missing value. Not selected by user");
        //        SystemMessage.Write(MessageType.Issue, Message.TraceabilityValuesNotSet);
        //        return;
        //    }

        //    #endregion

        //    var localGoodsReceipt =
        //        this.SelectedSaleDetail.GoodsReceiptDatas.First();

        //    var transactions = data.Where(x => !x.BatchAttribute).ToList();
        //    if (transactions.Any())
        //    {
        //        var localTransaction = localGoodsReceipt.TransactionData.FirstOrDefault();
        //        if (localTransaction != null)
        //        {
        //            foreach (var trans in localTransaction.TransactionTraceabilities)
        //            {
        //                var dataToEdit =
        //                    transactions.FirstOrDefault(x => x.TraceabilityId == trans.TransactionTraceabilityID);
        //                if (dataToEdit != null)
        //                {
        //                    trans.Value = dataToEdit.TraceabilityValue;
        //                }
        //            }
        //        }
        //    }

        //    var batchTraceabilityData = data.Where(x => x.BatchAttribute).ToList();
        //    if (batchTraceabilityData.Any())
        //    {
        //        var localTransactions = localGoodsReceipt.BatchTraceabilities;
        //        if (localTransactions != null)
        //        {
        //            foreach (var trans in localTransactions)
        //            {
        //                var dataToEdit =
        //                    batchTraceabilityData.FirstOrDefault(x => x.TraceabilityId == trans.BatchTraceabilityID);
        //                if (dataToEdit != null)
        //                {
        //                    trans.Value = dataToEdit.TraceabilityValue;
        //                }
        //            }
        //        }
        //    }

        //    //this.Log.LogDebug(this.GetType(), string.Format("Current Wgt:{0}. Adding weight:{1}", this.SelectedSaleDetail.WeightReceived, this.indicatorWeight));
        //    Messenger.Default.Send(this.SelectedSaleDetail, Token.TransactionsEdited);
        //    this.Close();
        //}

        //#endregion
            
    }
}

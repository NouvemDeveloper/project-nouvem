﻿// -----------------------------------------------------------------------
// <copyright file="EditTransactionViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;

namespace Nouvem.ViewModel.Transaction
{
    using System;
    using System.Collections.ObjectModel;
    using Nouvem.Model.BusinessObject;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class EditTransactionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The retrieved db transactions.
        /// </summary>
        private ObservableCollection<StockDetail> transactions;

        /// <summary>
        /// The retrieved db transactions.
        /// </summary>
        private IList<StockDetail> allTransactions = new List<StockDetail>();

        /// <summary>
        /// The selected transaction.
        /// </summary>
        private StockDetail selectedTransaction;

        /// <summary>
        /// The sale detail data.
        /// </summary>
        private SaleDetail saleDetail = new SaleDetail();

        private IList<StockDetail> transactionsToUpdate = new List<StockDetail>();

        /// <summary>
        /// The products.
        /// </summary>
        private ObservableCollection<InventoryItem> products;

        private int? docNumber;

        private ViewType module;

        /// <summary>
        /// The show all flag.
        /// </summary>
        private bool showAll;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EditTransactionViewModel"/> class.
        /// </summary>
        public EditTransactionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the incoming edited transaction data.
            Messenger.Default.Register<SaleDetail>(this, Token.TransactionsEdited, this.HandleEditedTransactions);


            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                if (string.IsNullOrWhiteSpace(s))
                {
                    return;
                }

                //if (this.FilteredSales != null && this.FilteredSales.Any() && this.FilteredSales.Last().SaleID == 0)
                //{
                //    this.FilteredSales.Last().PopUpNote = s;
                //    return;
                //}

                if (this.SelectedTransaction != null)
                {
                    this.SelectedTransaction.Notes = s;
                    this.SetControlMode(ControlMode.Update);

                    var localTrans = this.transactionsToUpdate.FirstOrDefault(x => x.StockTransactionID == this.SelectedTransaction.StockTransactionID);
                    if (localTrans != null)
                    {
                        this.transactionsToUpdate.Remove(localTrans);
                    }

                    this.transactionsToUpdate.Add(this.SelectedTransaction);
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowTransactionEditNotesWindow, s => this.ShowNotesWindow());

            #endregion

            #region command handler

            // Handle the ui load event.
            this.OnLoadedCommand = new RelayCommand(this.OnLoadedCommandExecute);

            // Handle the grid change event.
            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                if (e != null && e.Row == null)
                {
                    return;
                }

                if (this.SelectedTransaction == null)
                {
                    return;
                }

                var localTrans = this.transactionsToUpdate.FirstOrDefault(x => x.StockTransactionID == this.SelectedTransaction.StockTransactionID);
                if (localTrans != null)
                {
                    this.transactionsToUpdate.Remove(localTrans);
                }

                this.transactionsToUpdate.Add(this.SelectedTransaction);
                this.SetControlMode(ControlMode.Update);
            });

            #endregion

            this.HasWriteAuthorisation = true;
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the show all flag.
        /// </summary>
        public bool ShowAll
        {
            get
            {
                return this.showAll;
            }

            set
            {
                this.showAll = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    this.Transactions = new ObservableCollection<StockDetail>(this.allTransactions);
                }
                else
                {
                    this.Transactions = new ObservableCollection<StockDetail>(this.allTransactions.Where(x => !x.Deleted.HasValue));
                }
            }
        }

        /// <summary>
        /// Gets or sets the db transactions.
        /// </summary>
        public ObservableCollection<StockDetail> Transactions
        {
            get
            {
                return this.transactions;
            }

            set
            {
                this.transactions = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected transaction.
        /// </summary>
        public StockDetail SelectedTransaction
        {
            get
            {
                return this.selectedTransaction;
            }

            set
            {
                this.selectedTransaction = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory sale items.
        /// </summary>
        public ObservableCollection<InventoryItem> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.products = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the ui loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the grid cell change event.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the transactions to retrieve.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="documentNo"></param>
        public void SetTransactions(ViewType type, int? documentNo)
        {
            this.docNumber = documentNo;
            this.module = type;
            this.GetProducts(type);
            this.GetTransactions();
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateTransaction();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handle the drilldown command.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            //if (this.SelectedTransaction == null)
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
            //    return;
            //}

            //SystemMessage.Write(MessageType.Priority, Message.DrillingDown);

            //this.saleDetail = new SaleDetail
            //{
            //    INMasterID = this.SelectedTransaction.Transaction.INMasterID,
            //    EditTransaction = true
            //};

            //var data = new GoodsReceiptData();
            //data.TransactionData = new List<StockTransactionData>();
            //data.TransactionData.Add(this.selectedTransaction);
            //this.DataManager.GetTransactionData(data);

            //this.saleDetail.GoodsReceiptDatas = new List<GoodsReceiptData> { data };

            //Messenger.Default.Send(ViewType.TransactionDetails);
            //Messenger.Default.Send(this.saleDetail, Token.SaleDetailSelected);
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the ui load event.
        /// </summary>
        private void OnLoadedCommandExecute()
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SelectedTransaction = null;
            }));
        }

        #endregion

        #region helper

        private void GetTransactions()
        {
            switch (this.module)
            {
                case ViewType.Production:
                    this.allTransactions = new List<StockDetail>(this.DataManager.GetTransactions(null, this.docNumber, null, null, null, null));
                    break;
            }

            if (this.ShowAll)
            {
                this.Transactions = new ObservableCollection<StockDetail>(this.allTransactions);
            }
            else
            {
                this.Transactions = new ObservableCollection<StockDetail>(this.allTransactions.Where(x => !x.Deleted.HasValue));
            }
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowNotesWindow()
        {
            if (this.SelectedTransaction == null)
            {
                return;
            }

            var notes = string.Empty;
            Messenger.Default.Send(notes, Token.DisplayNotes);
        }

        /// <summary>
        /// Handles the edited transactions.
        /// </summary>
        /// <param name="saledetail">The edited transaction data.</param>
        private void HandleEditedTransactions(SaleDetail saledetail)
        {
            this.saleDetail = saledetail;
            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Updates  the selected transaction.
        /// </summary>
        private void UpdateTransaction()
        {

            try
            {
                if (this.DataManager.UpdateTransactions(this.transactionsToUpdate))
                {
                    this.SetControlMode(ControlMode.OK);
                    SystemMessage.Write(MessageType.Priority, Message.TransactionUpdated);
                    this.Refresh();
                    Messenger.Default.Send(Token.Message, Token.TransactionEditingComplete);
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, Message.TransactionNotUpdated);
                }
            }
            finally
            {
                this.transactionsToUpdate.Clear();
            }
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        private void GetProducts(ViewType mode)
        {
            switch (mode)
            {
                case ViewType.Production:
                    this.Products = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems.Where(x => x.Master.ProductionProduct == true));
                    break;
            }
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetTransactions();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearEditTransaction();
            Messenger.Default.Send(Token.Message, Token.CloseEditTransactionWindow);
        }

        #endregion

        #endregion
    }
}


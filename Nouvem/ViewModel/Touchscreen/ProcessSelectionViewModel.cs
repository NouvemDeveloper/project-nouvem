﻿// -----------------------------------------------------------------------
// <copyright file="ProcessSelectionViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Touchscreen
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class ProcessSelectionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The process.
        /// </summary>
        private ObservableCollection<Model.DataLayer.Process> processes;

        /// <summary>
        /// Reset process after print flag.
        /// </summary>
        private bool saveAsDefaultProcess;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Nouvem.ViewModel.Touchscreen.ProcessSelectionViewModel"/> class.
        /// </summary>
        public ProcessSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region message

            Messenger.Default.Register<string>(this, Token.DisplayProcessSelection, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.SelectedProcess = this.Processes.FirstOrDefault(x => x.Name.CompareIgnoringCase(s));
                }
            });

            //Messenger.Default.Register<bool>(this, Token.ResetProcess, b => this.SaveAsDefaultProcess = b);

            #endregion

            #region command handler

            // Close the window.
            this.MoveBackCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.CloseProcess));
            this.ProcessSelectedCommand = new RelayCommand(this.HandleSelectedProcess);

            #endregion

            this.GetProcesses();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the process is to be reset after the next print.
        /// </summary>
        public bool SaveAsDefaultProcess
        {
            get
            {
                return this.saveAsDefaultProcess;
            }

            set
            {
                this.saveAsDefaultProcess = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the ui close.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui close.
        /// </summary>
        public ICommand ProcessSelectedCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Seta the module processes.
        /// </summary>
        /// <param name="displayProcesses">The processes to display.</param>
        /// <param name="defaultProcess">The default process.</param>
        public void SetDefaultProcesses(string displayProcesses, string defaultProcess)
        {
            if (!string.IsNullOrWhiteSpace(displayProcesses))
            {
                var names = displayProcesses.Split(',').Select(x => x.Replace(" ","").Trim());
                var localProcesses = this.AllProcesses.Where(x => names.Contains(x.Name.Replace(" ","").Trim()));
                this.Processes = new ObservableCollection<Process>(localProcesses);
            }

            if (!string.IsNullOrWhiteSpace(defaultProcess))
            {
                this.SelectedProcess = this.Processes.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(defaultProcess));
                this.HandleSelectedProcess();
            }
        }

        /// <summary>
        /// Seta the module processes.
        /// </summary>
        /// <param name="process">The default process.</param>
        public void SetProcess(string process)
        {
            this.SelectedProcess = this.AllProcesses.FirstOrDefault(x => x.Name.CompareIgnoringCaseAndWhitespace(process));
            this.HandleSelectedProcess();
        }

        /// <summary>
        /// Set the module processes.
        /// </summary>
        /// <param name="processId">The process id to search for and select.</param>
        public void SetProcess(int processId)
        {
            this.SelectedProcess = this.AllProcesses.FirstOrDefault(x => x.ProcessID == processId);
            this.HandleSelectedProcess();
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        /// <summary>
        /// Handles a process selection.
        /// </summary>
        private void HandleSelectedProcess()
        {
            if (this.SelectedProcess != null)
            {
                Messenger.Default.Send(this.SelectedProcess, Token.ProcessSelected);
                Messenger.Default.Send(Token.Message, Token.CloseProcess);
            }
        }

        #endregion
    }
}

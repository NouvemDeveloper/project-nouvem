﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Scanner;

namespace Nouvem.ViewModel.Touchscreen
{
    public class TouchscreenStockTakeViewModel : StockTakeViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenStockTakeViewModel"/> class.
        /// </summary>
        public TouchscreenStockTakeViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion
            
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Gets the stock takes.
        /// </summary>
        protected override void GetStockTakes()
        {
            this.StockTakes = new ObservableCollection<StockTake>(this.DataManager.GetStockTakes()
                .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                            || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID));

            this.IsFormLoaded = true;
            this.SelectedStockTake = this.StockTakes.FirstOrDefault();
        }

        protected override void MoveBackCommandExecute(string s)
        {
            Messenger.Default.Send(Token.Message, Token.CloseTouchscreenStockTake);
        }

        #endregion

        #region private



        #endregion


    }
}

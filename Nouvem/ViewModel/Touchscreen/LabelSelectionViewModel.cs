﻿// -----------------------------------------------------------------------
// <copyright file="LabelSelectionViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Properties;

namespace Nouvem.ViewModel.Touchscreen
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class LabelSelectionViewModel : NouvemViewModelBase
    {
       #region field

        /// <summary>
        /// The labels.
        /// </summary>
        private ObservableCollection<Model.DataLayer.Label> labels;

        /// <summary>
        /// The selected label.
        /// </summary>
        private Model.DataLayer.Label selectedLabel;

        /// <summary>
        /// Reset label after print flag.
        /// </summary>
        private bool resetLabelAfterPrint;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelSelectionViewModel"/> class.
        /// </summary>
        public LabelSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region message

            Messenger.Default.Register<string>(this, Token.LabelAssociationsUpdated, s =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(this.GetLabels));
            });

            Messenger.Default.Register<bool>(this, Token.ResetLabel, b => this.ResetLabelAfterPrint = b);

            #endregion

            #region command handler

            // Close the window.
            this.MoveBackCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.CloseLabelSelectionWindow));

            #endregion

            this.GetLabels();
            this.ResetLabelAfterPrint = ApplicationSettings.ResetLabelOnEveryPrint;
        }

        #endregion

        #region public interface

        #region property
    
        /// <summary>
        /// Gets or sets a value indicating whether the label is to be reset after the next print.
        /// </summary>
        public bool ResetLabelAfterPrint
        {
            get
            {
                return this.resetLabelAfterPrint;
            }

            set
            {
                this.resetLabelAfterPrint = value;
                this.RaisePropertyChanged();
                ApplicationSettings.ResetLabelOnEveryPrint = value;
            }
        }

        /// <summary>
        /// Gets or sets the labels.
        /// </summary>
        public ObservableCollection<Model.DataLayer.Label> Labels
        {
            get
            {
                return this.labels;
            }

            set
            {
                this.labels = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected label.
        /// </summary>
        public Model.DataLayer.Label SelectedLabel
        {
            get
            {
                return this.selectedLabel;
            }

            set
            {
                this.selectedLabel = value;
                //this.RaisePropertyChanged();

                if (value != null)
                {
                    Messenger.Default.Send(value, Token.LabelChange);
                    Messenger.Default.Send(value, Token.ChangeDisplayLabelName);
                    Messenger.Default.Send(Token.Message, Token.CloseLabelSelectionWindow);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the ui close.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }
        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the labels.
        /// </summary>
        private void GetLabels()
        {
            this.Labels = new ObservableCollection<Model.DataLayer.Label>(this.DataManager.GetLabels());
        }

        #endregion
    }
}

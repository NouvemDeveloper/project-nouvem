﻿// -----------------------------------------------------------------------
// <copyright file="WarehouseSelectionViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Touchscreen
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class WarehouseSelectionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The product stock locations.
        /// </summary>
        private IList<Warehouse> stockLocations;

        /// <summary>
        /// The selected location.
        /// </summary>
        private Warehouse selectedStockLocation;

        /// <summary>
        /// Reset label after print flag.
        /// </summary>
        private bool resetLocationAfterPrint;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelSelectionViewModel"/> class.
        /// </summary>
        public WarehouseSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            // Close the window.
            this.MoveBackCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.CloseWarehouseSelectionWindow));

            this.HandleSelectedLocationCommand = new RelayCommand(() =>
            {
                if (this.selectedStockLocation != null)
                {
                    this.Locator.Touchscreen.UIText = this.selectedStockLocation.Name;
                    Messenger.Default.Send(this.selectedStockLocation, Token.WarehouseSelected);
                }
                else
                {
                    this.Locator.Touchscreen.UIText = Strings.TerminalLocation;
                    Messenger.Default.Send(this.StockLocations.First(), Token.WarehouseSelected);
                }


                if (this.IsFormLoaded)
                {
                    Messenger.Default.Send(Token.Message, Token.CloseWarehouseSelectionWindow);
                }
            });

            #endregion

            this.ResetLocationAfterPrint = true;
            this.IsFormLoaded = false;
            this.GetStockLocations();
            this.IsFormLoaded = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the stock locations.
        /// </summary>
        public IList<Warehouse> StockLocations
        {
            get
            {
                return this.stockLocations;
            }

            set
            {
                this.stockLocations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the location is to be reset after the next print.
        /// </summary>
        public bool ResetLocationAfterPrint
        {
            get
            {
                return this.resetLocationAfterPrint;
            }

            set
            {
                this.resetLocationAfterPrint = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected location.
        /// </summary>
        public Warehouse SelectedStockLocation
        {
            get
            {
                return this.selectedStockLocation;
            }

            set
            {
                this.selectedStockLocation = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the ui close.
        /// </summary>
        public ICommand HandleSelectedLocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui close.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the warehouse.
        /// </summary>
        /// <param name="location">The warehouse location to set.</param>
        public void SetWarehouse(Warehouse location)
        {
            if (!this.ResetLocationAfterPrint)
            {
                return;
            }

            if (location == null)
            {
                this.SelectedStockLocation = this.StockLocations.First();
                return;
            }

            this.SelectedStockLocation = this.StockLocations.FirstOrDefault(x => x.WarehouseID == location.WarehouseID);
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        /// <summary>
        /// Sets the stock locations.
        /// </summary>
        private void GetStockLocations()
        {
            this.StockLocations = new List<Warehouse>(NouvemGlobal.WarehouseLocations);
            this.StockLocations.Insert(0, new Warehouse { Name = Strings.TerminalLocation });
            this.SelectedStockLocation = this.StockLocations.First();
        }

        #endregion
    }
}


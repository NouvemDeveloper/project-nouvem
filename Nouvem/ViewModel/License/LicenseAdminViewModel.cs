﻿// -----------------------------------------------------------------------
// <copyright file="LicenseAdminViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.License
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class that handles the licensing administation.
    /// </summary>
    public class LicenseAdminViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The license details collection reference.
        /// </summary>
        private ObservableCollection<LicenseDetails> licenseDetails;

        /// <summary>
        /// The selected license details.
        /// </summary>
        private LicenseDetails selectedLicenseDetails;

        /// <summary>
        /// The application dlicensees.
        /// </summary>
        private ObservableCollection<Licensee> licensees;

        /// <summary>
        /// The selected licensee
        /// </summary>
        private Licensee selectedLicensee;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseAdminViewModel"/> class.
        /// </summary>
        public LicenseAdminViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message

            // register for a refresh apon a licence import.
            Messenger.Default.Register<string>(this, Token.RefreshLicencingTotals, x => this.GetLicenseTotals());

            #endregion

            #region command

            // Handle the allocation of a license
            this.DeallocateLicenceCommand = new RelayCommand(this.LicenseAllocationCommandExecute, this.CanDeallocateLicences);

            // Handle the allocation of a license
            this.LicenseAllocationCommand = new RelayCommand(this.LicenseAllocationCommandExecute, this.CanAllocateLicense);

            // handle the command to open the import view.
            this.ImportLicenseCommand = new RelayCommand(() => Messenger.Default.Send(ViewType.LicenseImport));

            #endregion

            this.LicenseDetails = new ObservableCollection<LicenseDetails>();
            this.Licensees = new ObservableCollection<Licensee>();
            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessLicencing);
            this.SetControlMode(ControlMode.OK);
            this.GetLicenseTotals();
            this.GetLicensees();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the application licensees
        /// </summary>
        public ObservableCollection<Licensee> Licensees
        {
            get
            {
                return this.licensees;
            }

            set
            {
                this.licensees = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected device.
        /// </summary>
        public Licensee SelectedLicensee
        {
            get
            {
                return this.selectedLicensee;
            }

            set
            {
                this.selectedLicensee = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected license details.
        /// </summary>
        public LicenseDetails SelectedLicenseDetails
        {
            get
            {
                return this.selectedLicenseDetails; 
            }

            set
            {
                this.selectedLicenseDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the license details.
        /// </summary>
        public ObservableCollection<LicenseDetails> LicenseDetails
        {
            get
            {
                return this.licenseDetails;
            }

            set
            {
                this.licenseDetails = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to deallocate a license.
        /// </summary>
        public ICommand DeallocateLicenceCommand { get; private set; }

        /// <summary>
        /// Gets the command to allocate a license.
        /// </summary>
        public ICommand LicenseAllocationCommand { get; private set; }

        /// <summary>
        /// Gets the command to open the import license view.
        /// </summary>
        public ICommand ImportLicenseCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode != ControlMode.OK || this.CurrentMode != ControlMode.Update)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Override the control button selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.LicenceAllocationHandler();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Clean up, and close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Update the control, based on a license type selection.
        /// </summary>
        private void LicenseAllocationCommandExecute()
        {
            this.SetControlMode(ControlMode.Update);
        }
      
        /// <summary>
        /// Determine if a license can be allocated.
        /// </summary>
        /// <returns>A falg, indicating whether a license can be allocated.</returns>
        private bool CanAllocateLicense()
        {
            var error = string.Empty;
            if (this.selectedLicensee == null)
            {
                error = Message.NoLicenseeSelected;
            }
            else if (this.selectedLicensee.LicenseType.LicenceName.Equals(this.selectedLicenseDetails.Details.LicenceName))
            {
                error = Message.AssignDuplicateLicenseError;
            }
            else if (!this.selectedLicensee.IsDevice && this.SelectedLicenseDetails.Details.IsDevice)
            {
                error = Message.DeviceLicenseToUserError;
            }
            else if (this.selectedLicensee.IsDevice && !this.SelectedLicenseDetails.Details.IsDevice)
            {
                error = Message.DeviceUserToDeviceError;
            }
            else if (this.selectedLicenseDetails.Details.Quantity == 0)
            {
                error = Message.NoRemainingLicenses;
            }
            else if (this.LicenceDeallocationsPending())
            {
                error = Message.LicenceDeallocationsPending;
            }

            if (error != string.Empty)
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Issue, error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determine if licence(s) can be deallocated.
        /// </summary>
        /// <returns>A flag, indicating whether licences can be deallocated.</returns>
        private bool CanDeallocateLicences()
        {
            foreach (var licensee in this.Licensees)
            {
                if (licensee.DeallocateLicence && licensee.LicenceStatus != LicenceStatus.Valid)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLicenceToDeallocate);
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region helper

        /// <summary>
        /// Determine if there are any pending licence deallocations.
        /// </summary>
        /// <returns>A falg, indicating any pending licence deallocations.</returns>
        private bool LicenceDeallocationsPending()
        {
            return this.licensees.Any(x => x.DeallocateLicence);
        }

        /// <summary>
        /// Handler for the licencing allocation/deallocation.
        /// </summary>
        private void LicenceAllocationHandler()
        {
            var deallocations = this.Licensees.Where(x => x.DeallocateLicence).ToList();
            if (deallocations.Any())
            {
                if (!this.CanDeallocateLicences())
                {
                    return;
                }

                this.DeallocateLicences(deallocations);
                return;
            }

            this.AssignLicense();
        }

        /// <summary>
        /// Method which deallocates the licences from the selected licensees.
        /// </summary>
        /// <param name="deallocations">The licensees to deallocate licences from.</param>
        private void DeallocateLicences(IList<Licensee> deallocations)
        {
            if (this.DataManager.DeallocateLicences(deallocations))
            {
                SystemMessage.Write(MessageType.Priority, Message.LicenseDeAllocatedSuccessful);
                this.Refresh();
                return;
            }

            SystemMessage.Write(MessageType.Issue, Message.LicenseDeAllocatedUnsuccessful);
        }

        /// <summary>
        /// Assigns a license type to a device.
        /// </summary>
        private void AssignLicense()
        {
            var allocationMessage = this.DataManager.AssignLicense(this.selectedLicensee, this.selectedLicenseDetails);
            if (allocationMessage == string.Empty)
            {
                SystemMessage.Write(MessageType.Priority, Message.LicenseAllocated);
                this.Refresh();
                return;
            }

            SystemMessage.Write(MessageType.Issue, allocationMessage);
        }
        
        /// <summary>
        /// Gets the license details and totals.
        /// </summary>
        private void GetLicenseTotals()
        {
            this.licenseDetails.Clear();
            var details = this.DataManager.GetLicenseTotals();
            this.LicenseDetails = new ObservableCollection<LicenseDetails>(details);
        }

        /// <summary>
        /// Gets the application licensees.
        /// </summary>
        private void GetLicensees()
        {
            var localLicensees = this.DataManager.GetLicensees();
            if (localLicensees != null)
            {
                this.Licensees = new ObservableCollection<Licensee>(localLicensees);
            }
        }

        /// <summary>
        /// Refresh the license totals.
        /// </summary>
        private void Refresh()
        {
            this.GetLicenseTotals();
            this.GetLicensees();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Clean up, and close.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearLicenseAdmin();
            Messenger.Default.Send(Token.Message, Token.CloseLicenseAdminWindow);
        }

        #endregion

        #endregion
    }
}

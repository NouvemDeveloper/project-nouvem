﻿// -----------------------------------------------------------------------
// <copyright file="LicenseSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;
using Nouvem.Model.BusinessObject;

namespace Nouvem.ViewModel.License
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.ViewModel.Interface;

    /// <summary>
    /// Class that handles the license set up
    /// </summary>
    public class DeviceSetUpViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The device mac address.
        /// </summary>
        private string macAddress;

        /// <summary>
        /// The device name.
        /// </summary>
        private string currentVersion;

        /// <summary>
        /// The device name.
        /// </summary>
        private string previousVersion;

        /// <summary>
        /// The device name.
        /// </summary>
        private string deviceName;

         /// <summary>
        /// The device ip address.
        /// </summary>
        private string deviceIp = string.Empty;

         /// <summary>
        /// The device alias name.
        /// </summary>
        private string aliasName;

         /// <summary>
        /// The device remarks.
        /// </summary>
        private string remarks;

        /// <summary>
        /// The device id.
        /// </summary>
        private int? deviceID;

        /// <summary>
        /// The device active from date.
        /// </summary>
        private DateTime? deviceActiveFromDate;

        /// <summary>
        /// The device active to date.
        /// </summary>
        private DateTime? deviceActiveToDate;

        /// <summary>
        /// The device inactive from date.
        /// </summary>
        private DateTime? deviceInactiveFromDate;

        /// <summary>
        /// The device inactive from date.
        /// </summary>
        private DateTime? deviceInactiveToDate;

        /// <summary>
        /// The selected copy from device
        /// </summary>
        private DeviceMaster selectedCopyDevice;

        /// <summary>
        /// The selected device
        /// </summary>
        private DeviceMaster selectedDevice;

        /// <summary>
        /// The device departments.
        /// </summary>
        private ObservableCollection<DepartmentLookup> departments = new ObservableCollection<DepartmentLookup>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceSetUpViewModel"/> class.
        /// </summary>
        public DeviceSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                    // Only change control if the device set up container is the active window.
                    //if (this.IsFormActive)
                    //{
                    //    this.ControlChange(x);
                    //}
                });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            #endregion

            #region command

            this.UpdateCommand = new RelayCommand(() =>
            {
                if (this.SelectedDevice.DeviceID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            });

            // Handle the ui activation/deactivation events.
            this.OnActiveChangedCommand = new RelayCommand<string>(x =>
            {
                this.IsFormActive = x.Equals(Constant.Activated);
            });

             // Handle the user search
            this.FindDeviceCommand = new RelayCommand(this.FindDeviceCommandExecute, this.InFindMode);

            // Handle the window load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the window close.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion
           
            //this.GetDevices();
            this.SelectedDevice = new DeviceMaster();
            this.GetDepartments();
            this.GetDevices();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the application devices.
        /// </summary>
        public IList<DeviceMaster> Devices { get; set; }

        /// <summary>
        /// Gets or sets the selected device.
        /// </summary>
        public DeviceMaster SelectedCopyDevice
        {
            get
            {
                return this.selectedCopyDevice;
            }

            set
            {
                this.selectedCopyDevice = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    if (this.SelectedDevice.DeviceID == 0)
                    {
                        this.SetControlMode(ControlMode.Add);
                    }
                    else
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected device.
        /// </summary>
        public DeviceMaster SelectedDevice
        {
            get
            {
                return this.selectedDevice;
            }

            set
            {
                this.selectedDevice = value;
                if (value != null)
                {
                    this.HandleSelectedDevice();
                }
            }
        }

        //protected List<object> _SelectedItems = new  List<object>();
        public List<object> SelectedItems { get; set; } = new List<object>();


        //public ObservableCollection<DepartmentLookup> SelectedItems
        //{
        //    get { return selectedItems; }
        //    set
        //    {
        //        selectedItems = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        /// <summary>
        /// Gets or sets the selected device.
        /// </summary>
        public ObservableCollection<DepartmentLookup> Departments
        {
            get
            {
                return this.departments;
            }

            set
            {
                this.departments = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device id.
        /// </summary>
        public int? DeviceID
        {
            get
            {
                return this.deviceID;
            }

            set
            {
                this.deviceID = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device id.
        /// </summary>
        public string CurrentVersion
        {
            get
            {
                return this.currentVersion;
            }

            set
            {
                this.currentVersion = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device id.
        /// </summary>
        public string PreviousVersion
        {
            get
            {
                return this.previousVersion;
            }

            set
            {
                this.previousVersion = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device mac address.
        /// </summary>
        public string MacAddress
        {
            get
            {
                return this.macAddress;
            }

            set
            {
                this.SetMode(value, this.macAddress);
                this.macAddress = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device name.
        /// </summary>
        public string DeviceName
        {
            get
            {
                return this.deviceName;
            }

            set
            {
                this.SetMode(value, this.deviceName);
                this.deviceName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the devive ip.
        /// </summary>
        public string DeviceIP
        {
            get
            {
                return this.deviceIp;
            }

            set
            {
                this.SetMode(value, this.deviceIp);
                this.deviceIp = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the devive alias.
        /// </summary>
        public string AliasName
        {
            get
            {
                return this.aliasName;
            }

            set
            {
                this.SetMode(value, this.aliasName);
                this.aliasName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the devive remarks.
        /// </summary>
        public string Remarks
        {
            get
            {
                return this.remarks;
            }

            set
            {
                this.SetMode(value, this.remarks);
                this.remarks = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device active from date.
        /// </summary>
        public DateTime? DeviceActiveFromDate
        {
            get
            {
                return this.deviceActiveFromDate;
            }

            set
            {
                this.SetMode(value, this.deviceActiveFromDate);
                this.deviceActiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device active to date.
        /// </summary>
        public DateTime? DeviceActiveToDate
        {
            get
            {
                return this.deviceActiveToDate;
            }

            set
            {
                this.SetMode(value, this.deviceActiveToDate);
                this.deviceActiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device inactive from date.
        /// </summary>
        public DateTime? DeviceInactiveFromDate
        {
            get
            {
                return this.deviceInactiveFromDate;
            }

            set
            {
                this.SetMode(value, this.deviceInactiveFromDate);
                this.deviceInactiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the device inactive to date.
        /// </summary>
        public DateTime? DeviceInactiveToDate
        {
            get
            {
                return this.deviceInactiveToDate;
            }

            set
            {
                this.SetMode(value, this.deviceInactiveToDate);
                this.deviceInactiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the window activation/deactivation.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the window activation/deactivation.
        /// </summary>
        public ICommand OnActiveChangedCommand { get; private set; }

        /// <summary>
        /// Gets the find device command.
        /// </summary>
        public ICommand FindDeviceCommand { get; private set; }

        /// <summary>
        /// Gets the loading event command.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the closing event command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedDevice == null)
            {
                return;
            }
          
            var localDevices = NouvemGlobal.Devices;

            var previousDevice =
                localDevices.TakeWhile(
                    device => device.DeviceID != this.SelectedDevice.DeviceID)
                    .LastOrDefault();

            if (previousDevice != null)
            {
                this.SelectedDevice = previousDevice;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedDevice == null)
            {
                return;
            }
            
            var localDevices = NouvemGlobal.Devices;

            var nextDevice =
                localDevices.SkipWhile(
                    device => device.DeviceID != this.SelectedDevice.DeviceID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextDevice != null)
            {
                this.SelectedDevice = nextDevice;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedDevice == null)
            {
                return;
            }
            
            var localDevices = NouvemGlobal.Devices;
            var firstDevice = localDevices.FirstOrDefault();

            if (firstDevice != null)
            {
                this.SelectedDevice = firstDevice;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.SelectedDevice == null)
            {
                return;
            }
        
            var localDevices = NouvemGlobal.Devices;
            var lastDevice = localDevices.LastOrDefault();

            if (lastDevice != null)
            {
                this.SelectedDevice = lastDevice;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLastEdit()
        {
            if (this.SelectedDevice == null)
            {
                return;
            }

            var localDevices = NouvemGlobal.Devices;
            var lastDevice = localDevices.LastOrDefault();

            if (lastDevice != null)
            {
                this.SelectedDevice = lastDevice;
            }
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new device mode, so clear the device fields.
                this.ClearForm();
                this.MacAddress = System.Environment.MachineName;
                this.DeviceName = System.Environment.MachineName;
                this.DeviceIP = NouvemGlobal.GetIpAddress();
                this.SetControlMode(ControlMode.Add);
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                return;
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into find devivce mode, so clear the device fields.
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Method that sets up the ui to add a new user.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddNewDevice();
                    break;

                case ControlMode.Find:
                    this.FindDevice();
                    break;

                case ControlMode.Update:
                    this.UpdateDevice();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Method that clears the form if in emdedded user control mode,
        /// or closes the form if in pop up window mode.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.ClearForm();
            this.SetControlMode(ControlMode.Add);
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

            // If we are in pop up window mode, close it.
            this.Close();
        }

        /// <summary>
        /// Checks for the existance of a current selected device.
        /// </summary>
        /// <returns>A flag, indicating the existence of a selected device.</returns>
        protected override bool CheckForEntity()
        {
            return this.SelectedDevice != null && this.SelectedDevice.DeviceID > 0;
        }

        #endregion

        #region private

        #region command handler

        /// <summary>
        /// Handler for the device search.
        /// </summary>
        private void FindDeviceCommandExecute()
        {
            this.FindDevice();
        }

        /// <summary>
        /// Handler for the form load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessUsers);
            var startMode = this.HasWriteAuthorisation ? ControlMode.Add : ControlMode.OK;
            this.SetControlMode(startMode);
           
            this.MacAddress = !string.IsNullOrWhiteSpace(NouvemGlobal.RDPIdentifier) ? NouvemGlobal.RDPIdentifier : System.Environment.MachineName;
            this.DeviceName = !string.IsNullOrWhiteSpace(NouvemGlobal.RDPIdentifier) ? NouvemGlobal.RDPIdentifier : System.Environment.MachineName;
            this.DeviceIP = NouvemGlobal.GetIpAddress();
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// Handler for the form load event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.CancelSelectionCommandExecute();
            this.IsFormLoaded = false;
        }

        #endregion

        #region helper

        private void GetDepartments()
        {
            var deviceId = 0;
            if (this.SelectedDevice != null)
            {
                deviceId = this.SelectedDevice.DeviceID;
            }
            //this.SelectedItems.Clear();
            this.Departments = new ObservableCollection<DepartmentLookup>(this.DataManager.GetDeviceDepartments(deviceId));
            //foreach (var department in this.Departments.Where(x => x.IsSelected))
            //{
            //    this.SelectedItems.Add(department.DepartmentID);
            //}
        }

        /// <summary>
        /// handles the selection of a new device, parsing to the ui.
        /// </summary>
        private void HandleSelectedDevice()
        {
            //this.GetDepartments();
            this.DeviceID = this.selectedDevice.DeviceID;
            this.MacAddress = this.selectedDevice.DeviceMAC;
            this.DeviceIP = this.selectedDevice.DeviceIP;
            this.DeviceName = this.selectedDevice.DeviceName;
            this.AliasName = this.selectedDevice.DeviceAlias;
            this.Remarks = this.selectedDevice.Remark;
            this.DeviceActiveFromDate = this.selectedDevice.ActiveFrom;
            this.DeviceActiveToDate = this.selectedDevice.ActiveTo;
            this.DeviceInactiveFromDate = this.selectedDevice.InActiveFrom;
            this.DeviceInactiveToDate = this.selectedDevice.InActiveTo;
            this.CurrentVersion = this.selectedDevice.CurrentVersion;
            this.PreviousVersion = this.selectedDevice.PreviousVersion;
            this.SelectedItems.Clear();
            if (this.SelectedDevice.DeviceID > 0)
            {
                var lookups = this.DataManager.GetDepartmentLookUps(this.SelectedDevice.DeviceID);
                foreach (var departmentLookUp in lookups)
                {
                    this.SelectedItems.Add(departmentLookUp.DepartmentID);
                }
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// validation for add/update.
        /// </summary>
        /// <returns>An error, if validation failure, otherwise an empty string.</returns>
        private string Validate()
        {
            var error = string.Empty;
            if (string.IsNullOrWhiteSpace(this.macAddress))
            {
                error = Message.MacAddressBlank;
            }
            else if (string.IsNullOrWhiteSpace(this.DeviceName))
            {
                error = Message.DeviceNameBlank;
            }
            else if (this.SelectedDevice.DeviceID == 0 && this.DataManager.IsDeviceNameInSystem(this.macAddress))
            {
                error = string.Format(Message.DeviceNameAlreadyInSystem, this.macAddress);
            }

            return error;
        }

        /// <summary>
        /// Adds a new device.
        /// </summary>
        private void AddNewDevice()
        {
            #region validation

            var error = this.Validate();
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var device = this.CreateOrUpdateDevice();
            var copyId = this.SelectedCopyDevice?.DeviceID;
            try
            {
                if (this.DataManager.AddOrUpdateDevice(device, copyId))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DeviceAdded);
                    NouvemGlobal.Devices = this.DataManager.GetDevices();
                    this.ClearForm();
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.DeviceNotAdded);
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }
        }

        private void FindDevice()
        {
            #region validation

            //if (!this.IsSearchValid())
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.EnterSearchTerm);
            //    return;
            //}

            #endregion

            this.Locator.DeviceSearch.SetFilteredUsers(NouvemGlobal.Devices);
            Messenger.Default.Send(ViewType.DeviceSearch);
        }

        private void GetDevices()
        {
            this.Devices = NouvemGlobal.Devices.OrderBy(x => x.DeviceMAC).ToList();
        }

        /// <summary>
        /// Method that determines if a search can be made.
        /// </summary>
        /// <returns>A flag, indicating a valid search.</returns>
        private bool IsSearchValid()
        {
            return !(string.IsNullOrWhiteSpace(this.MacAddress) && string.IsNullOrWhiteSpace(this.DeviceIP) && string.IsNullOrWhiteSpace(this.DeviceName));
        }

        /// <summary>
        /// Update a selected device.
        /// </summary>
        private void UpdateDevice()
        {
            #region validation

            var error = this.Validate();
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var device = this.CreateOrUpdateDevice();
            var lookups = new List<int>();
            foreach (var selectedItem in this.SelectedItems)
            {
                lookups.Add((int)selectedItem);
            }

            try
            {
                var copyId = this.SelectedCopyDevice?.DeviceID;
                if (this.DataManager.AddOrUpdateDeviceWithLookups(device, lookups, copyId))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DeviceUpdated);
                    this.ClearForm();
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.DeviceNotUpdated);
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }
        }

        ///// <summary>
        ///// Retrieves the application devices.
        ///// </summary>
        //private void GetDevices()
        //{
        //    if (NouvemGlobal.Devices != null)
        //    {
        //        // Devices already retried and stored, so exit.
        //        return;
        //    }

        //    try
        //    {
        //        NouvemGlobal.Devices = this.DataManager.GetDevices();
        //    }
        //    catch (Exception)
        //    {
        //        SystemMessage.Write(MessageType.Issue,  Message.DatabaseError);
        //    }
        //}

        /// <summary>
        /// Creates, or updates, a device.
        /// </summary>
        /// <returns>The created, or updated device.</returns>
        private DeviceMaster CreateOrUpdateDevice()
        {
            this.SelectedDevice.DeviceMAC = this.macAddress;
            this.SelectedDevice.DeviceName = this.deviceName;
            this.SelectedDevice.DeviceIP = this.deviceIp;
            this.SelectedDevice.DeviceAlias = this.aliasName ?? string.Empty;
            this.SelectedDevice.CurrentVersion = this.CurrentVersion;
            this.SelectedDevice.PreviousVersion = this.PreviousVersion;
            this.SelectedDevice.Remark = this.remarks ?? string.Empty;
            this.SelectedDevice.ActiveFrom = this.DeviceActiveFromDate;
            this.SelectedDevice.ActiveTo = this.DeviceActiveToDate;
            this.SelectedDevice.InActiveFrom = this.DeviceInactiveFromDate;
            this.SelectedDevice.InActiveTo = this.DeviceInactiveToDate;
            return this.SelectedDevice;
        }

        /// <summary>
        /// Clears the ui.
        /// </summary>
        private void ClearForm()
        {
            this.DeviceID = null;
            this.MacAddress = string.Empty;
            this.DeviceIP = string.Empty;
            this.DeviceName = string.Empty;
            this.AliasName = string.Empty;
            this.Remarks = string.Empty;
            this.CurrentVersion = string.Empty;
            this.PreviousVersion = string.Empty;
            this.DeviceActiveFromDate = null;
            this.DeviceActiveToDate = null;
            this.DeviceInactiveFromDate = null;
            this.DeviceInactiveToDate = null;
            this.SelectedDevice = new DeviceMaster();
            this.GetDepartments();
            this.SelectedItems.Clear();
            this.SelectedCopyDevice = null;
        }

        /// <summary>
        /// Close the ui, and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDeviceSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseDeviceSetUpWindow);
        }

        #endregion

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="LicenseImportViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.License
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Windows.Forms;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class responsible for the import and display of licences.
    /// </summary>
    public class LicenseImportViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The license details reference.
        /// </summary>
        private ObservableCollection<ViewLicenseDetail> licenseDetails;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseImportViewModel"/> class.
        /// </summary>
        public LicenseImportViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // Handle the import of a licence.
            this.ImportLicenseCommand = new RelayCommand(this.ImportLicenseCommandExecute);

            #endregion

            this.GetLicenseDetails();
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the license details.
        /// </summary>
        public ObservableCollection<ViewLicenseDetail> LicenseDetails
        {
            get
            {
                return this.licenseDetails;
            }

            set
            {
                this.licenseDetails = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion
     
        #region command

        /// <summary>
        /// Gets the command to import a license.
        /// </summary>
        public ICommand ImportLicenseCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            // no adding/editing in this module
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override the control button selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Clean up, and close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command

        /// <summary>
        /// Handles the importation of a license.
        /// </summary>
        private void ImportLicenseCommandExecute()
        {
            var dialog = new OpenFileDialog();
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    var file = dialog.FileName;
                    var validationResult = this.LicenceManager.ImportLicence(file);
                    if (validationResult != string.Empty)
                    {
                        SystemMessage.Write(MessageType.Issue, Shared.Localisation.Message.LicenceValidationError);
                        this.Log.LogError(this.GetType(), validationResult);
                        return;
                    }

                    SystemMessage.Write(MessageType.Priority, Shared.Localisation.Message.LicenceValidated);
                    this.Refresh();

                    // If the licence admin screen is open, refresh the totals.
                    Messenger.Default.Send(Token.Message, Token.RefreshLicencingTotals);
                }
                catch (IOException ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    this.Log.LogError(this.GetType(), ex.Message);
                }
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Refresh the licence details.
        /// </summary>
        private void Refresh()
        {
            this.GetLicenseDetails();
        }

        /// <summary>
        /// Gets the application license details.
        /// </summary>
        private void GetLicenseDetails()
        {
            this.LicenseDetails = new ObservableCollection<ViewLicenseDetail>(this.DataManager.GetLicenseDetails());
        }

        /// <summary>
        /// Clean up, and close.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearLicenseImport();
            Messenger.Default.Send(Token.Message, Token.CloseLicenseImportWindow);
        }

        #endregion

        #endregion
    }
}

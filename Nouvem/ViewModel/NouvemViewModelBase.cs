﻿// -----------------------------------------------------------------------
// <copyright file="NouvemViewModelBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;

namespace Nouvem.ViewModel
{
    using System;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Global;
    using Nouvem.Logging;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Application base view model.
    /// </summary>
    public abstract class NouvemViewModelBase : ViewModelBase
    {
        #region field

        /// <summary>
        /// The partner currency.
        /// </summary>
        private string partnerCurrency;

        /// <summary>
        /// The stock labels to print.
        /// </summary>
        private int stockLabelsToPrint;
        
        /// <summary>
        /// The selected process.
        /// </summary>
        private Model.DataLayer.Process selectedProcess;

        /// <summary>
        /// The product stock data.
        /// </summary>
        private ObservableCollection<ProductStockData> mrpStockData;

        /// <summary>
        /// The control button content.
        /// </summary>
        private string controlButtonContent;

        /// <summary>
        /// The scanned barcode text.
        /// </summary>
        private string scannerText;

        /// <summary>
        /// The selected agent reference.
        ///  </summary>
        private ViewBusinessPartner selectedAgent;

        /// <summary>
        /// The application processes.
        /// </summary>
        private ObservableCollection<Model.DataLayer.Process> processes;

        /// <summary>
        /// The busy indicator backing field.
        /// </summary>
        private bool isBusy;

        private int autoBoxCount;

        /// <summary>
        /// The hauliers.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> agents = new ObservableCollection<ViewBusinessPartner>();

        /// <summary>
        /// The hauliers.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> hauliers = new ObservableCollection<ViewBusinessPartner>();

        /// <summary>
        /// The application processes.
        /// </summary>
        protected List<Model.DataLayer.Process> AllProcesses;

        /// <summary>
        /// The scanner stock mode.
        /// </summary>
        private ScannerMode scannerStockMode;

        /// <summary>
        /// The scanner mode button text.
        /// </summary>
        private string scannerModeText;

        /// <summary>
        /// The ui name.
        /// </summary>
        private string formName;

        /// <summary>
        /// The selected search type.
        /// </summary>
        private string selectedSearchType;

        /// <summary>
        /// The search types.
        /// </summary>
        private IList<string> searchTypes;

        /// <summary>
        /// Flag, as to whether the mode is to be changed.
        /// </summary>
        protected bool DontChangeMode;

        /// <summary>
        /// The current animal kill type.
        /// </summary>
        protected KillType KillType;

        /// <summary>
        /// Timer used to determine a manual serial entry operation.
        /// </summary>
        protected readonly DispatcherTimer Timer = new DispatcherTimer();

        /// <summary>
        /// Generic reference holder.
        /// </summary>
        protected string ReferenceValue;

        /// <summary>
        /// Flag, as to whether a scan is in progress.
        /// </summary>
        protected bool Scanning;

        #endregion

        #region protected field

        /// <summary>
        /// Gets the singleton data manager reference.
        /// </summary>
        protected DataManager DataManager = DataManager.Instance;

        /// <summary>
        /// Gets the singleton print manager reference.
        /// </summary>
        protected PrintManager PrintManager = PrintManager.Instance;

        /// <summary>
        /// Gets the singleton accounts manager reference.
        /// </summary>
        protected AccountsManager AccountsManager = AccountsManager.Instance;

        /// <summary>
        /// Gets the singleton indicator manager reference.
        /// </summary>
        protected IndicatorManager IndicatorManager = IndicatorManager.Instance;

        /// <summary>
        /// Gets the singleton licence manager reference.
        /// </summary>
        protected LicenceManager LicenceManager = LicenceManager.Instance;

        /// <summary>
        /// Gets the singleton report manager reference.
        /// </summary>
        protected ReportManager ReportManager = ReportManager.Instance;

        /// <summary>
        /// Gets the singleton production manager reference.
        /// </summary>
        protected ProductionManager ProductionManager = ProductionManager.Instance;

        /// <summary>
        /// Gets the singleton department bodies manager reference.
        /// </summary>
        protected DepartmentBodiesManager DepartmentBodiesManager = DepartmentBodiesManager.Instance;

        /// <summary>
        /// Gets the singleton inventory manager reference.
        /// </summary>
        protected InventoryManager InventoryManager = InventoryManager.Instance;

        /// <summary>
        /// Gets the singleton batch manager reference.
        /// </summary>
        protected BatchManager BatchManager = BatchManager.Instance;

        /// <summary>
        /// Gets the singleton hardware manager reference.
        /// </summary>
        protected HardwareManager HardwareManager = HardwareManager.Instance;

        /// <summary>
        /// Gets the singleton label manager reference.
        /// </summary>
        protected LabelManager LabelManager = LabelManager.Instance;

        /// <summary>
        /// Gets the singleton label manager reference.
        /// </summary>
        protected AuthorisationsManager AuthorisationsManager = AuthorisationsManager.Instance;

        /// <summary>
        /// The current control mode.
        /// </summary>
        protected ControlMode CurrentMode;

        /// <summary>
        /// Flag that holds the changing of an entity determination value.
        /// </summary>
        protected bool EntitySelectionChange;

        /// <summary>
        /// Flag that holds the changing of an entity determination value.
        /// </summary>
        protected bool ProcessingData;

        /// <summary>
        /// Flag, to signify that we want the sales serach to clear.
        /// </summary>
        protected bool clearingSalesSearch;

        /// <summary>
        /// Used to hold the hind\fore product id when auto carcass splitting.
        /// </summary>
        protected int? AutoSplitProductId;

        private bool productionOrdersScreenCreated;
        /// <summary>
        /// Flag, to signify that production orders screen has been created.
        /// </summary>
        protected bool ProductionOrdersScreenCreated
        {
            get { return this.productionOrdersScreenCreated; }
            set
            {
                this.productionOrdersScreenCreated = value;

            }
        }

        /// <summary>
        /// Flag, to signify that production orders screen has been created.
        /// </summary>
        protected bool IntakeBatchesScreenCreated;

        /// <summary>
        /// Flag, to signify that production orders screen has been created.
        /// </summary>
        protected bool NotesScreenCreated;

        /// <summary>
        /// The incoming indicator weight.
        /// </summary>
        protected decimal indicatorWeight;

        /// <summary>
        /// The base intake batch id.
        /// </summary>
        protected int? BatchIdBase;

        /// <summary>
        /// The base intake batch id.
        /// </summary>
        protected int? IntakeId;

        /// <summary>
        /// The base intake batch id.
        /// </summary>
        protected int? BPMasterId;

        /// <summary>
        /// Flag, as to whether the warehouse screen has been created.
        /// </summary>
        protected bool WareHouseScreenCreated;

        /// <summary>
        /// Start up check not made message.
        /// </summary>
        protected string MissingProcessStartUpCheckMessage;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NouvemViewModelBase"/> class.
        /// </summary>
        protected NouvemViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            // register for a master window ribbon control mode change.
            Messenger.Default.Register<ControlMode>(this, Token.SetControl, this.SetControlMode);

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.StockLabels, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleStockLabels(s);
                }
            });

            Messenger.Default.Register<string>(this, KeypadTarget.AutoBoxCount, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.AutoBoxCount = s.ToInt();
                    Messenger.Default.Send(s, KeypadTarget.Quantity);
                }
            });

            #region command registration

            this.AutoBoxCountCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.AutoBoxCount));

            this.ShowCalculatorCommand = new RelayCommand(() =>
            {
                try
                {
                    if (File.Exists(ApplicationSettings.WindowsCalculatorPath))
                    {
                        System.Diagnostics.Process.Start(ApplicationSettings.WindowsCalculatorPath);
                    }
                }
                catch (Exception e)
                {
                    SystemMessage.Write(MessageType.Issue, e.Message);
                    this.Log.LogError(this.GetType(), e.Message);
                }
            });

            // Handle the stock labels request.
            this.SetStockLabelsToPrintCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.StockLabels));

            // Switch the control mode.
            this.ControlButtonCommand = new RelayCommand<string>(this.ControlCommandExecute);

            // Handle the selection of the control button.
            this.ControlButtonSelectedCommand = new RelayCommand(this.ControlSelectionCommandExecute);

            // Handle the selection of the cancel button.
            this.CancelButtonSelectedCommand = new RelayCommand(this.CancelSelectionCommandExecute);

            // Handle the audit display.
            this.AuditDisplayCommand = new RelayCommand<string>(this.AuditDisplayCommandExecute);

            // Handle the drill down execution.
            this.DrillDownCommand = new RelayCommand(this.DrillDownCommandExecute);

            // handle the print/preview control display.
            this.PrintPreviewCommand = new RelayCommand(this.PrintPreviewCommandExecute);

            this.RemoveItemCommand = new RelayCommand(this.RemoveItemCommandExecute, () => this.IsFormLoaded = true);

            // Handle the mode button touch down, starting the timer.
            this.ModeButtonTouchDownCommand = new RelayCommand(() =>
            {
                this.DontChangeMode = false;
                this.Timer.Start();
            });

            // Handle the mode button touch up, starting the timer.
            this.ModeButtonTouchUpCommand = new RelayCommand(() =>
            {
                this.Timer.Stop();
                if (!this.DontChangeMode)
                {
                    this.ScannerModeHandler();
                }
            });

            #endregion

            NouvemGlobal.SalesSearchScreenOpen = false;
            this.Log = new Logger();
            this.Locator = new ViewModelLocator();
            this.SetControlMode(ControlMode.OK);
            this.SetYesNos();
            this.GetSearchTypes();
            this.PartnerCurrency = ApplicationSettings.DefaultCurrency;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the hauliers.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Hauliers
        {
            get
            {
                return this.hauliers;
            }

            set
            {
                this.hauliers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the agents.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Agents
        {
            get
            {
                return this.agents;
            }

            set
            {
                this.agents = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected partner.
        /// </summary>
        public ViewBusinessPartner SelectedAgent
        {
            get
            {
                return this.selectedAgent;
            }

            set
            {
                this.SetMode(value, this.selectedAgent);
                this.selectedAgent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock modes.
        /// </summary>
        public ObservableCollection<ProductStockData> MRPStockData
        {
            get
            {
                return this.mrpStockData;
            }

            set
            {
                this.mrpStockData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public string PartnerCurrency
        {
            get
            {
                return this.partnerCurrency;
            }

            set
            {
                this.partnerCurrency = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected process.
        /// </summary>
        public Model.DataLayer.Process SelectedProcess
        {
            get
            {
                return this.selectedProcess;
            }

            set
            {
                this.HandleSelectedProcess(value);
                this.selectedProcess = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the issue types.
        /// </summary>
        public IList<UOMMaster> NouUOMs { get; set; }

        public IList<NouOrderMethod> OrderMethods { get; set; }

        /// <summary>
        /// Gets or sets the processs.
        /// </summary>
        public ObservableCollection<Model.DataLayer.Process> Processes
        {
            get
            {
                return this.processes;
            }

            set
            {
                this.processes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set a flag indicating whether the current module is disabled.
        /// </summary>
        public bool DisableModule { get; set; }

        /// <summary>
        /// Flag, to signify that orders screen has been created.
        /// </summary>
        public bool OrdersScreenCreated { get; set; }

        /// <summary>
        /// Flag, to signify that orders screen has been created.
        /// </summary>
        public bool PartnersScreenCreated { get; set; }

        /// <summary>
        /// Gets or sets the selected search type.
        /// </summary>
        public string SelectedSearchType
        {
            get
            {
                return this.selectedSearchType;
            }

            set
            {
                this.selectedSearchType = value;
                this.RaisePropertyChanged();
                this.HandleSelectedSearchType();
            }
        }

        /// <summary>
        /// Gets or sets the selected search type.
        /// </summary>
        public IList<string> SearchTypes
        {
            get
            {
                return this.searchTypes;
            }

            set
            {
                this.searchTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets opr sets a vlaue indicating whether the tolerance weight has been reached on the scales.
        /// </summary>
        public bool TolerantWeightReached { get; set; }

        /// <summary>
        /// Gets or sets the scanner mode text.
        /// </summary>
        public string ScannerModeText
        {
            get
            {
                return this.scannerModeText;
            }

            set
            {
                this.scannerModeText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scanner stock mode.
        /// </summary>
        public ScannerMode ScannerStockMode
        {
            get
            {
                return this.scannerStockMode;
            }

            set
            {
                this.scannerStockMode = value;
                if (value == ScannerMode.Scan)
                {
                    this.ScannerModeText = "+";
                }
                else if (value == ScannerMode.MinusScan)
                {
                    this.ScannerModeText = "-";
                }
                else if (value == ScannerMode.Reprint)
                {
                    this.ScannerModeText = Strings.Reprint;
                }
                else if (value == ScannerMode.ReWeigh)
                {
                    if (!ApplicationSettings.ScanningExternalCarcass)
                    {
                        this.ScannerModeText = Strings.ReWeigh;
                    }
                    else if (ApplicationSettings.ScannerMode)
                    {
                        this.ScannerModeText = Strings.PlusCarcassScanner;
                    }
                    else
                    {
                        this.ScannerModeText = Strings.PlusCarcass;
                    }
                }
                else if (value == ScannerMode.Split)
                {
                    this.ScannerModeText = Strings.Split;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the form is loaded.
        /// </summary>
        public bool IsFormLoaded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the form is active.
        /// </summary>
        public bool IsFormActive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a touchscreen module loaded.
        /// </summary>
        public bool TouchscreenModuleLoaded { get; set; }

        /// <summary>
        /// Gets or sets the control button content.
        /// </summary>
        public string ControlButtonContent
        {
            get
            {
                return this.controlButtonContent;
            }

            set
            {
                this.controlButtonContent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the busy indicator is running.
        /// </summary>
        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                this.isBusy = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current control mode is a 'write' mode.
        /// </summary>
        public bool ControlModeWrite
        {
            get
            {
                return this.CurrentMode == ControlMode.Add || this.CurrentMode == ControlMode.Update;
            }
        }

        /// <summary>
        /// Gets or sets the yes no values;
        /// </summary>
        public IList<YesNo> YesNos { get; set; }

        #endregion

        #region command

        public ICommand AutoBoxCountCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the Windows calculator.
        /// </summary>
        public ICommand ShowCalculatorCommand { get; private set; }

        /// <summary>
        /// Gets the command to set the multiple stock labels/transaction.
        /// </summary>
        public ICommand SetStockLabelsToPrintCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command to handle the mode button touch down event.
        /// </summary>
        public ICommand ModeButtonTouchDownCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to handle the mode button touch up event.
        /// </summary>
        public ICommand ModeButtonTouchUpCommand { get; set; }

        /// <summary>
        /// Gets the command to display the print preview control.
        /// </summary>
        public ICommand PrintPreviewCommand { get; set; }

        /// <summary>
        /// Gets the command to delete an item.
        /// </summary>
        public ICommand RemoveItemCommand { get; set; }

        /// <summary>
        /// Gets the command to put the data into 'add' mode.
        /// </summary>
        public ICommand ControlButtonCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selection of the control button.
        /// </summary>
        public ICommand ControlButtonSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selection of the cancel button.
        /// </summary>
        public ICommand CancelButtonSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the audit display.
        /// </summary>
        public ICommand AuditDisplayCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the drill down functionality.
        /// </summary>
        public ICommand DrillDownCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the window activation/deactivation.
        /// </summary>
        //public ICommand OnActiveChangedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        #region property

        public HashSet<int> AutoBoxLabels { get; set; }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public int AutoBoxCount
        {
            get
            {
                return this.autoBoxCount;
            }

            set
            {
                if (value < 0)
                {
                    value = 0;
                }

                if (value == 0)
                {
                    this.AutoBoxLabels?.Clear();
                }

                this.autoBoxCount = value;
                this.RaisePropertyChanged();
            }
        }


        public bool AutoBoxing
        {
            get { return this.AutoBoxCount > 0 && this.CheckForAutoBoxing(); }
        }

        /// <summary>
        /// Gets or sets the stock labels to print.
        /// </summary>
        public int StockLabelsToPrint
        {
            get
            {
                return this.stockLabelsToPrint;
            }

            set
            {
                if (value == 0)
                {
                    value = 1;
                }

                this.stockLabelsToPrint = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the scanned barcode.
        /// </summary>
        public string ScannerText
        {
            get
            {
                return this.scannerText;
            }

            set
            {
                this.scannerText = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrEmpty(value))
                {
                    this.Log.LogDebug(this.GetType(), string.Format("ScannerText(): Data:{0}, Data length:{1}", value, value.Length));
                    if (value.EndsWithIgnoringCase(ApplicationSettings.KeyboardWedgeTerminator))
                    {
                        if (value.Length == 1)
                        {
                            this.scannerText = string.Empty;
                        }
                        else
                        {
                            this.HandleBarcode();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the ui name.
        /// </summary>
        public string FormName
        {
            get
            {
                return this.formName;
            }

            set
            {
                this.formName = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets the ViewModelLocator reference.
        /// </summary>
        protected ViewModelLocator Locator { get; private set; }

        /// <summary>
        /// Gets the application logger.
        /// </summary>
        protected ILogger Log { get; private set; }

        private bool hasWriteAuthorisation;

        /// <summary>
        /// Gets or sets the local write authorisation.
        /// </summary>
        public bool HasWriteAuthorisation
        {
            get { return true; }
            set { this.hasWriteAuthorisation = value; }
        }

        #endregion

        #region command execution

        /// <summary>
        /// Method that handles the change of control mode.
        /// </summary>
        /// <param name="mode">The new mode.</param>
        protected virtual void ControlCommandExecute(string mode)
        {
            var controlMode = (ControlMode)Enum.Parse(typeof(ControlMode), mode);
            this.SetControlMode(controlMode);
        }

        /// <summary>
        /// Overridden, to handle a ui item audit trail.
        /// </summary>
        /// <param name="s"></param>
        protected virtual void AuditDisplayCommandExecute(string s)
        {
        }

        /// <summary>
        /// Overridden, to handle a request to display the grid print/preview control.
        /// </summary>
        protected virtual void PrintPreviewCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.DisplayPrintPreview);
        }

        /// <summary>
        /// Overridden, to delete the child grid item.
        /// </summary>
        protected virtual void RemoveItemCommandExecute()
        {
        }

        /// <summary>
        /// Overridden, to print the module report.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected virtual void PrintReport(string mode)
        {
        }

        /// <summary>
        /// Overridden, to handle the drill down functionality.
        /// </summary>
        protected virtual void DrillDownCommandExecute()
        {
        }

        #endregion

        #region virtual

        /// <summary>
        /// Checks for a test mode weight, applying it if so.
        /// </summary>
        protected virtual void CheckForTestWeight() { }

        /// <summary>
        /// Handle the stock labels selection.
        /// </summary>
        protected virtual void HandleStockLabels(string data) { }

        /// <summary>
        /// Set up the timers.
        /// </summary>
        protected virtual void HandleBarcode()
        {
            try
            {
                var data = this.scannerText;
                this.ScannerText = string.Empty;

                Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        this.HandleScannerData(data);
                    }));
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }
        }

        /// <summary>
        /// Handles a gs1 barcode.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected string HandleGs1Barcode(string data)
        {
            var gs1Data = this.IsGs1Barcode(data, ApplicationSettings.Gs1Identifier, ApplicationSettings.Gs1IdentifierLength);
            if (gs1Data != string.Empty)
            {
                data = gs1Data.RemoveNonIntegers().ToInt().ToString();
            }

            return data;
        }

        protected void GetAgents()
        {
            var localAgents =
                NouvemGlobal.BusinessPartners.Where(
                    x => x.Details.Type != null && x.Details.Type.StartsWithIgnoringCase(Strings.Agent)).Select(x => x.Details);
            this.Agents = new ObservableCollection<ViewBusinessPartner>(localAgents.OrderBy(x => x.Name));
        }

        protected void GetHauliers()
        {
            var localHauliers =
                NouvemGlobal.BusinessPartners.Where(
                    x => x.Details.Type != null && x.Details.Type.StartsWithIgnoringCase(Strings.Haulier)).Select(x => x.Details);
            this.Hauliers = new ObservableCollection<ViewBusinessPartner>(localHauliers);
        }

        /// <summary>
        /// Checks for carcass auto split barcode.
        /// </summary>
        /// <param name="data">The data string.</param>
        /// <returns>Formatted split string</returns>
        protected string CheckForSplitStock(string data)
        {
            if (!ApplicationSettings.AutoSplitCarcassAtDispatch)
            {
                return data;
            }

            if (data.Length > 0 && data.Last().ToString() == ApplicationSettings.KeyboardWedgeTerminator)
            {
                data = data.Substring(0, data.Length - 1);
            }

            try
            {
                this.AutoSplitProductId = null;
                if (data.Length == 18 && data.IsNumericSequence())
                {
                    var serial = this.DataManager.GetSerialByCarcassNo(data);
                    if (serial.IsNullOrZero())
                    {
                        return data;
                    }

                    var hqFqChar = data.Substring(8,1).ToInt();
                    if (hqFqChar == 2 || hqFqChar == 4)
                    {
                        this.AutoSplitProductId = ApplicationSettings.HindProduct;
                    }
                    else
                    {
                        this.AutoSplitProductId = ApplicationSettings.ForeProduct;
                    }

                    return serial.ToString();
                }

                if (!data.ContainsIgnoringCase(ApplicationSettings.Gs1Identifier))
                {
                    return data;
                }

                if (data.ContainsIgnoringCase(Constant.HQ))
                {
                    this.AutoSplitProductId = ApplicationSettings.HindProduct;
                }
                else if (data.ContainsIgnoringCase(Constant.FQ))
                {
                    this.AutoSplitProductId = ApplicationSettings.ForeProduct;
                }
                else
                {
                    return data;
                }

                var index = data.IndexOf("21");
                return data.Substring(index + 2, data.Length - (index + 2)).RemoveNonIntegers();
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"CheckForSplitStock:{e.Message} : data:{data}");
                return data;
            }
        }

        /// <summary>
        /// Checks for carcass auto split barcode.
        /// </summary>
        /// <param name="data">The data string.</param>
        /// <returns>Formatted split string</returns>
        protected string RetrieveSerialNoFromBarcode (string data)
        {
            if (!ApplicationSettings.RetrieveSerialNoFromBarcode)
            {
                return data;
            }

            var serial = this.DataManager.GetSerialByCarcassNo(data);
            if (serial.IsNullOrZero())
            {
                return data;
            }

            return serial.ToString();
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected virtual void HandleScannerData(string data){}

        protected virtual void HandleSelectedProcess(Model.DataLayer.Process value) { }

        /// <summary>
        /// Show the batch report.
        /// </summary>
        protected virtual void ShowReportCommandExecute(){}

        /// <summary>
        /// Opens the scanner port.
        /// </summary>
        protected virtual void OpenScanner()
        {
            if (!ApplicationSettings.ConnectToScanner)
            {
                return;
            }

            if (this.HardwareManager.IsScannerPortOpen())
            {
                return;
            }

            try
            {
                this.HardwareManager.ConnectToScanner();
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Closes the connection to the scanner.
        /// </summary>
        protected void CloseScanner()
        {
            this.HardwareManager.CloseScannerPort();
        }

        /// <summary>
        /// Set the control mode on user data entry.
        /// </summary>
        /// <typeparam name="T">The generic type parameter.</typeparam>
        /// <param name="newValue">The generic new value to compare.</param>
        /// <param name="oldValue">The generic old value to compare.</param>
        protected virtual void SetMode<T>(T newValue, T oldValue)
        {
            // ignore if we in add or update mode.
            if (this.CurrentMode == ControlMode.Add || this.CurrentMode == ControlMode.Update)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.OK || this.CurrentMode == ControlMode.Find)
            {
                if (newValue is ValueType && !newValue.Equals(oldValue))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                    return;
                }

                // check for a change of value
                if ((oldValue != null && newValue != null) && !newValue.Equals(oldValue))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                    return;
                }

                if ((oldValue == null && newValue != null))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                }
            }
        }
        
        /// <summary>
        /// Determine if an entity has been selected.
        /// </summary>
        /// <returns>A flag as to whether an entity is selected.</returns>
        /// <remarks>This must be overridden in the calling class, to check for it's local selected entity.</remarks>
        protected virtual bool CheckForEntity()
        {
            return true;
        }

        /// <summary>
        /// Handles the scanner mode change.
        /// </summary>
        protected virtual void ScannerModeHandler()
        {
            if (this.ScannerStockMode == ScannerMode.Scan)
            {
                this.ScannerStockMode = ScannerMode.MinusScan;
            }
            else if (this.ScannerStockMode == ScannerMode.MinusScan)
            {
                this.ScannerStockMode = ScannerMode.Reprint;
            }
            else if (this.ScannerStockMode == ScannerMode.Reprint)
            {
                this.ScannerStockMode = ScannerMode.Scan;
            }
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected virtual void GetSearchTypes()
        {
            this.SearchTypes = new List<string> { Strings.DocumentDate, Strings.DeliveryDate, Strings.ProposedKilldate, Strings.KillDate };
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected virtual void HandleSelectedSearchType()
        {
        }

        #endregion

        #region method

        /// <summary>
        /// Gets a product stock mode.
        /// </summary>
        /// <param name="productId">The product to search for.</param>
        /// <returns>It's nou stock mode id.</returns>
        protected int GetStockMode(int productId)
        {
            var product = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.INMasterID == productId);
            if (product != null)
            {
                return product.Master.NouStockModeID ?? 1;
            }

            return 1;
        }

        protected virtual bool CheckForAutoBoxing()
        {
            return true;
        }

        /// <summary>
        /// Gets a product name.
        /// </summary>
        /// <param name="id">The inmasterid to search for.</param>
        /// <returns>A product name.</returns>
        protected string GetProductName(int id)
        {
            if (NouvemGlobal.InventoryItems == null)
            {
                return string.Empty;
            }

            var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x =>
                x.INMasterID == id);

            if (localProduct != null)
            {
                return localProduct.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves a product from a scanned ean 13 barcode.
        /// </summary>
        /// <param name="data">The scanned barcode.</param>
        /// <param name="startPos">Product start pos.</param>
        /// <param name="length">Length of code.</param>
        /// <returns>A matching ean 13 product.</returns>
        protected InventoryItem RetrieveProductFromEan13Barcode(string data, int startPos, int length)
        {
            if (data.Length < startPos + length)
            {
                return null;
            }

            var productCode = data.Substring(startPos,length);
            return NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.Code.CompareIgnoringCase(productCode));
        }

        /// <summary>
        /// Determines if the barcode is a scotbeef barcode.
        /// </summary>
        /// <param name="data">The scanned data.</param>
        /// <returns>Flag, as to whether it's a ascotbeef barcode or not.</returns>
        protected string IsGs1Barcode(string data, string identifier, int identifierLength)
        {
            if (string.IsNullOrEmpty(identifier) || string.IsNullOrWhiteSpace(data))
            {
                return string.Empty;
            }

            if (data.EndsWithIgnoringCase("x"))
            {
                data = data.Substring(0, data.Length - 1);
            }

            if (data.Length >= 10 && data.StartsWith(identifier))
            {
                return data.Substring(data.Length - identifierLength, identifierLength);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the processs.
        /// </summary>
        protected virtual void GetProcesses()
        {
            this.AllProcesses = this.DataManager.GetProcesses().ToList();
            this.Processes = new ObservableCollection<Process>(this.AllProcesses);
        }

        protected virtual void CheckForNewEntities(string entityType = "")
        {
            try
            {
                if (entityType == string.Empty)
                {
                    var newEntities = this.DataManager.CheckForNewEntities(NouvemGlobal.DeviceId.ToInt());
                    var partnerEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.BusinessPartner));
                    foreach (var partnerEntity in partnerEntities)
                    {
                        if (NouvemGlobal.BusinessPartners.Any(x => x.Details.BPMasterID == partnerEntity.EntityID))
                        {
                            continue;
                        }

                        var localPartner = this.DataManager.GetBusinessPartner(partnerEntity.EntityID);
                        if (localPartner != null)
                        {
                            NouvemGlobal.AddNewPartner(localPartner);
                        }
                    }

                    var productEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.INMaster));
                    foreach (var productEntity in productEntities)
                    {
                        if (NouvemGlobal.InventoryItems.Any(x => x.Master.INMasterID == productEntity.EntityID))
                        {
                            continue;
                        }

                        var localProduct = this.DataManager.GetInventoryItemById(productEntity.EntityID);
                        if (localProduct != null)
                        {
                            NouvemGlobal.AddInventoryItem(localProduct);
                        }
                    }

                    return;
                }

                if (entityType == EntityType.BusinessPartner)
                {
                    var newEntities = this.DataManager.CheckForNewEntities(NouvemGlobal.DeviceId.ToInt(), entityType);
                    var partnerEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.BusinessPartner));
                    foreach (var partnerEntity in partnerEntities)
                    {
                        if (NouvemGlobal.BusinessPartners.Any(x => x.Details.BPMasterID == partnerEntity.EntityID))
                        {
                            continue;
                        }

                        var localPartner = this.DataManager.GetBusinessPartner(partnerEntity.EntityID);
                        if (localPartner != null)
                        {
                            NouvemGlobal.AddNewPartner(localPartner);
                        }
                    }

                    return;
                }

                if (entityType == EntityType.INMaster)
                {
                    var newEntities = this.DataManager.CheckForNewEntities(NouvemGlobal.DeviceId.ToInt(), entityType);
                    var productEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.INMaster));
                    foreach (var productEntity in productEntities)
                    {
                        if (NouvemGlobal.InventoryItems.Any(x => x.Master.INMasterID == productEntity.EntityID))
                        {
                            continue;
                        }

                        var localProduct = this.DataManager.GetInventoryItemById(productEntity.EntityID);
                        if (localProduct != null)
                        {
                            NouvemGlobal.AddInventoryItem(localProduct);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                
            }
           
        }

        /// <summary>
        /// Logs to database.
        /// </summary>
        /// <param name="logType">The log type.</param>
        /// <param name="identifier">The primary identifier.</param>
        /// <param name="identifier2">The secondary identifier.</param>
        /// <param name="message">The log message.</param>
        protected void LogToDatabase(string logType, string identifier, string identifier2, string message)
        {
            var log = new Model.DataLayer.Log
            {
                LogType = logType,
                Identifier = identifier,
                Identifier2 = identifier2,
                LogMessage = message,
                LogTime = DateTime.Now,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId
            };

            this.DataManager.LogToDatabase(log);
        }

        /// <summary>
        /// Closes any open hidden windows.
        /// </summary>
        protected void CloseHiddenWindows()
        {
            Messenger.Default.Send(Token.Message, Token.CloseHiddenWindows);
        }

        /// <summary>
        /// Checks if the weight tolerance has been reached on the scales between weighings.
        /// </summary>
        protected void CheckDisturbance()
        {
            if (ApplicationSettings.ToleranceWeight <= 0 || this.indicatorWeight <= 0)
            {
                this.TolerantWeightReached = false;
                return;
            }

            var disturbance = this.Locator.Indicator.Weight.GetDifference(this.indicatorWeight);
            if (disturbance > ApplicationSettings.ToleranceWeight)
            {
                this.TolerantWeightReached = true;
            }
        }

        /// <summary>
        /// Method that switches the view, by setting it's view model as the current view model.
        /// </summary>
        /// <param name="newView"></param>
        protected bool SwitchView(NouvemViewModelBase newView)
        {
            if (this.Locator.Master.CurrentViewModel != newView)
            {
                this.Locator.Master.CurrentViewModel = newView;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks the licence validity date, warning the user if it's due to expire within 30 days.
        /// </summary>
        protected virtual void CheckLicenceExpiryDate()
        {
            if (NouvemGlobal.IsNouvemUser)
            {
                // nouvem engineer/installer...ignore.
                return;
            }

            if (NouvemGlobal.Licensee.LicenceStatus != LicenceStatus.Valid)
            {
                // user has no licence
                SystemMessage.Write(MessageType.Issue, Message.NoLicence);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.NoLicence);
                    Environment.Exit(0);
                }));

                return;
            }

            var daysLeft = this.LicenceManager.GetLicenceDaysRemaining();
            if (daysLeft <= 30)
            {
                // warn the user
                SystemMessage.Write(MessageType.Issue, string.Format("{0} {1} {2}", Message.LicenceExpiryWarning, daysLeft, Message.Days));
            }
        }

        ///// <summary>
        ///// Gets the authorisation, and handles the issue display if the required authorisation has not been detected.
        ///// </summary>
        ///// <param name="authorisation">The authorisation to check.</param>
        ///// <param name="minimumRequiredAuthorisationLevel">The required authorisation level.</param>
        ///// <returns>A flag, indicating whether the user has sufficient privileges for the task.</returns>
        //protected bool AuthorisationCheck(string authorisation, string minimumRequiredAuthorisationLevel)
        //{
        //    this.Log.LogInfo(this.GetType(), $"Checking authorisation:{authorisation}. Minimum required level:{minimumRequiredAuthorisationLevel}");
        //    if (ApplicationSettings.DisableAuthorisationCheck)
        //    {
        //        this.Log.LogInfo(this.GetType(), "Authorisation checks disabled. Allowing");
        //        return true;
        //    }
            
        //    var userAuthorisation = NouvemGlobal.CheckAuthorisation(authorisation);
        //    if (minimumRequiredAuthorisationLevel.Equals(Constant.ReadOnly))
        //    {
        //        if (userAuthorisation.Equals(Constant.NoAccess))
        //        {
        //            this.Log.LogInfo(this.GetType(), "Authorisation value returned: No access");
        //            //SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
        //            return false;
        //        }

        //        return true;
        //    }

        //    if (minimumRequiredAuthorisationLevel.Equals(Constant.FullAccess))
        //    {
        //        if (userAuthorisation.Equals(Constant.NoAccess) || userAuthorisation.Equals(Constant.ReadOnly))
        //        {
        //            this.Log.LogInfo(this.GetType(), $"Authorisation value returned: {userAuthorisation}");
        //            //SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
        //            return false;
        //        }

        //        return true;
        //    }

        //    this.Log.LogInfo(this.GetType(), "Authorisation granted");
        //    return true;
        //}

        /// <summary>
        /// Determine if we are in find mode.
        /// </summary>
        /// <returns>Flag, as to whether we are in find mode.</returns>
        protected bool InFindMode()
        {
            return this.CurrentMode == ControlMode.Find;
        }


        /// <summary>
        /// Sets the busy indicator.
        /// </summary>
        /// <param name="indicatorOn">The on/off flag.</param>
        protected void SetBusyIndicator(bool indicatorOn)
        {
            this.Locator.Master.IsBusy = indicatorOn;
        }

        /// <summary>
        /// Sets the local authorisation write value i.e. user must have full access.
        /// </summary>
        /// <param name="authorisation">The authorisation to check.</param>
        protected void SetWriteAuthorisation(string authorisation)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Checking write authorisation:{0}", authorisation));
            this.HasWriteAuthorisation = true; //NouvemGlobal.CheckAuthorisation(authorisation) == Constant.FullAccess;

            this.Log.LogDebug(this.GetType(), string.Format("Result:{0}", this.HasWriteAuthorisation));
        }

        /// <summary>
        /// Method that sets the application mode.
        /// </summary>
        /// <param name="mode">The mode to set.</param>
        /// <remarks>Note - Add authorisations checks are handled in the overriding ControlCommandExecute methods, 
        ///          but update authorisation checks need to handled here, as there is no cntr U.</remarks> 
        protected virtual void SetControlMode(ControlMode mode)
        {
            switch (mode)
            {
                case ControlMode.OK:
                    this.CurrentMode = ControlMode.OK;
                    this.ControlButtonContent = Strings.OK;
                    break;

                case ControlMode.Add:
                    this.CurrentMode = ControlMode.Add;
                    this.ControlButtonContent = Strings.Add;
                    break;

                case ControlMode.Find:
                    this.CurrentMode = ControlMode.Find;
                    this.ControlButtonContent = Strings.Find;
                    break;

                case ControlMode.Reconcile:
                    this.CurrentMode = ControlMode.Reconcile;
                    this.ControlButtonContent = Strings.Reconcile;
                    break;

                case ControlMode.Copy:
                    this.CurrentMode = ControlMode.Copy;
                    this.ControlButtonContent = Strings.Copy;
                    break;

                case ControlMode.Update:
                    if (this.HasWriteAuthorisation || NouvemGlobal.IsNouvemUser)
                    {
                        this.CurrentMode = ControlMode.Update;
                        this.ControlButtonContent = Strings.Update;
                    }
                    else
                    {
                        //SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    }

                    break;

                case ControlMode.Export:
                    this.CurrentMode = ControlMode.Export;
                    this.ControlButtonContent = Strings.Export;
                    break;

                case ControlMode.Import:
                    this.CurrentMode = ControlMode.Import;
                    this.ControlButtonContent = Strings.Import;
                    break;
            }
        }

        /// <summary>
        /// Sets the yes no values.
        /// </summary>
        protected void SetYesNos()
        {
            this.YesNos = new List<YesNo> { new YesNo { Value = Strings.Yes }, new YesNo { Value = Strings.No } };
        }

        #endregion

        #region abstract

        /// <summary>
        /// Abstract control selection method.
        /// </summary>
        protected abstract void ControlSelectionCommandExecute();

        /// <summary>
        /// Abstract cancel selection command.
        /// </summary>
        protected abstract void CancelSelectionCommandExecute();

        #endregion

        #endregion
    }
}

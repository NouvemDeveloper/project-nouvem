﻿// -----------------------------------------------------------------------
// <copyright file="WeightBandGroupSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class WeightBandGroupSetUpViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The bands collection.
        /// </summary>
        private ObservableCollection<WeightBandGroup> weightBandGroups;

        /// <summary>
        /// The selected group.
        /// </summary>
        private WeightBandGroup selectedWeightBandGroup;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the WeightBandGroupSetUpViewModel class.
        /// </summary>
        public WeightBandGroupSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.WeightBandGroups = new ObservableCollection<WeightBandGroup>();

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetWeightBandGroups();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected band.
        /// </summary>
        public WeightBandGroup SelectedWeightBandGroup
        {
            get
            {
                return this.selectedWeightBandGroup;
            }

            set
            {
                this.selectedWeightBandGroup = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the bands.
        /// </summary>
        public ObservableCollection<WeightBandGroup> WeightBandGroups
        {
            get
            {
                return this.weightBandGroups;
            }

            set
            {
                this.weightBandGroups = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedWeightBandGroup != null)
            {
                this.selectedWeightBandGroup.Deleted = DateTime.Now;
                this.UpdateWeightBandGroups();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.WeightBandGroups.Remove(this.selectedWeightBandGroup);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateWeightBandGroups();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the weight bands.
        /// </summary>
        private void UpdateWeightBandGroups()
        {
            try
            {
                if (this.DataManager.AddOrUpdateWeightBandGroups(this.WeightBandGroups))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                    Messenger.Default.Send(Token.Message, Token.WeightBandGroupUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearWeightBandGroupSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseWeightBandGroupWindow);
        }

        /// <summary>
        /// Gets the application weight bands.
        /// </summary>
        private void GetWeightBandGroups()
        {
            this.WeightBandGroups.Clear();
            foreach (var band in this.DataManager.GetWeightBandGroups())
            {
                this.WeightBandGroups.Add(band);
            }
        }

        #endregion
    }
}









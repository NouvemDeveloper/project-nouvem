﻿// -----------------------------------------------------------------------
// <copyright file="WeightBandSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.Model.BusinessObject;

namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class WeightBandSetUpViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The bands collection.
        /// </summary>
        private ObservableCollection<WeightBandData> weightBands;

        /// <summary>
        /// The selected group.
        /// </summary>
        private WeightBandData selectedWeightBand;

        /// <summary>
        /// The template names collection.
        /// </summary>
        private bool defineNewGroup;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the WeightBandSetUpViewModel class.
        /// </summary>
        public WeightBandSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.WeightBandGroupUpdated, s =>
            {
                this.GetWeightBandGroups();
                this.DefineNewGroup = false;
            });

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.WeightBandGroups = new List<WeightBandGroup>();
            this.WeightBands = new ObservableCollection<WeightBandData>();

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetWeightBandGroups();
            this.GetWeightBands();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the new group instruction.
        /// </summary>
        public bool DefineNewGroup
        {
            get
            {
                return this.defineNewGroup;
            }

            set
            {
                this.defineNewGroup = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    Messenger.Default.Send(ViewType.WeightBandGroup);
                }
            }
        }


        /// <summary>
        /// Get or sets the selected band.
        /// </summary>
        public WeightBandData SelectedWeightBand
        {
            get
            {
                return this.selectedWeightBand;
            }

            set
            {
                this.selectedWeightBand = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the bands.
        /// </summary>
        public ObservableCollection<WeightBandData> WeightBands
        {
            get
            {
                return this.weightBands;
            }

            set
            {
                this.weightBands = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the groups.
        /// </summary>
        public IList<WeightBandGroup> WeightBandGroups { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedWeightBand != null)
            {
                this.selectedWeightBand.Deleted = DateTime.Now;
                this.UpdateWeightBands();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.WeightBands.Remove(this.selectedWeightBand);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateWeightBands();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the weight bands.
        /// </summary>
        private void UpdateWeightBands()
        {
            try
            {
                if (this.DataManager.AddOrUpdateWeightBands(this.WeightBands))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearWeightBandSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseWeightBandWindow);
        }

        /// <summary>
        /// Gets the application weight bands.
        /// </summary>
        private void GetWeightBands()
        {
            this.WeightBands.Clear();
            foreach (var band in this.DataManager.GetWeightBands())
            {
                this.WeightBands.Add(band);
            }
        }

        /// <summary>
        /// Gets the application weight band groups.
        /// </summary>
        private void GetWeightBandGroups()
        {
            this.WeightBandGroups.Clear();
            foreach (var band in this.DataManager.GetWeightBandGroups())
            {
                this.WeightBandGroups.Add(band);
            }
        }

        #endregion
    }
}





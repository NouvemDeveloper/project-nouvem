﻿// -----------------------------------------------------------------------
// <copyright file="PaymentProposalSearchViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class PaymentProposalSearchViewModel : PaymentProposalCreationViewModel
    {
        #region field

        /// <summary>
        /// The payments to update collection.
        /// </summary>
        private IList<Sale> paymentsToUpdate = new List<Sale>();

        /// <summary>
        /// The ref.
        /// </summary>
        private string reference;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentProposalSearchViewModel"/> class.
        /// </summary>
        public PaymentProposalSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            Messenger.Default.Register<int>(this, Token.PaymentSelected, i =>
            {
                this.SelectedSale = this.FilteredSales.FirstOrDefault(x => x.SaleID == i);
                if (this.SelectedSale != null)
                {
                    this.UpdatePayments();
                }
            });

            this.UpdateCommand = new RelayCommand(this.UpdateCommandExecute);
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public
   
        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public string Reference
        {
            get
            {
                return this.reference;
            }

            set
            {
                this.reference = value;
                this.RaisePropertyChanged();
                if (string.IsNullOrWhiteSpace(value))
                {
                    this.SetControlMode(ControlMode.OK);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        public ViewType Mode { get; set; }

        /// <summary>
        /// Gets the command to update reference.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion

        #region protected

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdatePayments();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedSales == null || !this.SelectedSales.Any())
            {
                return;
            }

            #endregion

            foreach (var sale in this.SelectedSales)
            {
                sale.Print = !preview;
                Messenger.Default.Send(sale, Token.SearchSaleSelectedForReport);
            }

            if (ApplicationSettings.AutoCompletePaymentOnReportPrint)
            {
                this.DataManager.UpdatePaymentsStatus(
                    this.SelectedSales.Where(x => x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList(), 
                    NouvemGlobal.NouDocStatusComplete.NouDocStatusID);
                this.Refresh();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.ToDate = DateTime.Today;
            this.FromDate = ApplicationSettings.PaymentProposalFromDate;
            this.ShowAllOrders = ApplicationSettings.PaymentProposalShowAllOrders;
            this.KeepVisible = ApplicationSettings.PaymentProposalKeepVisible;
            this.IsFormLoaded = true;
            this.GetProposals();
            this.SelectedSale = null;
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            ApplicationSettings.PaymentProposalFromDate = this.FromDate;
            ApplicationSettings.PaymentProposalShowAllOrders = this.ShowAllOrders;
            ApplicationSettings.PaymentProposalKeepVisible = this.KeepVisible;
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.ClosePaymentSearchWindow);
            ViewModelLocator.ClearPaymentProposalSearch();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale != null)
            {
                if (this.Mode == ViewType.ChequePaymentReport)
                {
                    Messenger.Default.Send(this.SelectedSale, Token.SearchSaleSelectedForReport);
                }
                else
                {
                    Messenger.Default.Send(ViewType.Payment);
                    Messenger.Default.Send(this.SelectedSale, Token.PaymentProposalSelected);
                }
      
                this.Close();
            }
        }

        /// <summary>
        /// Retrieves the proposal data.
        /// </summary>
        protected override void GetProposals()
        {
            if (this.ShowAllOrders)
            {
                var localSales = this.DataManager
                    .GetPaymentProposals(true, this.FromDate.Date,
                        this.ToDate.Date);

                this.FilteredSales = new ObservableCollection<Sale>(localSales);

            }
            else
            {
                var localSales = this.DataManager
                    .GetPaymentProposals(false, this.FromDate.Date,
                        this.ToDate.Date);

                this.FilteredSales = new ObservableCollection<Sale>(localSales);
            }
        }

        protected override void HandleSelectedSale(Sale sale)
        {
            try
            {
                this.selectedSale = sale;
                this.RaisePropertyChanged("SelectedSale");
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.ClosePaymentSearchWindow);
                ViewModelLocator.ClearPaymentProposalSearch();
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        private void UpdateCommandExecute()
        {
            this.allowNullSale = true;
            this.SelectedSale = null;
            this.allowNullSale = false;
        }

        /// <summary>
        /// Updates the payments.
        /// </summary>
        private void UpdatePayments()
        {
            this.paymentsToUpdate.Clear();
            if (this.SelectedSale == null)
            {
                Messenger.Default.Send(Token.Message, Token.GetPaymentSelected);
                return;
            }

            if (this.SelectedSale != null)
            {
                this.SelectedSale.Reference = this.Reference;
                this.paymentsToUpdate.Add(this.SelectedSale);

                if (this.paymentsToUpdate.Any())
                {
                    if (this.DataManager.UpdatePaymentsOnly(this.paymentsToUpdate))
                    {
                        SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                        this.paymentsToUpdate.Clear();
                        this.SelectedSale = null;
                        return;
                    }

                    SystemMessage.Write(MessageType.Priority, Message.DataUpdateError);
                }
            }
        }

        #endregion
    }
}

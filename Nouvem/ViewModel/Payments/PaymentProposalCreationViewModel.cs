﻿// -----------------------------------------------------------------------
// <copyright file="PaymentProposalCreationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class PaymentProposalCreationViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// The consolidated payment flag.
        /// </summary>
        private bool consolidatedPayment;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentProposalCreationViewModel"/> class.
        /// </summary>
        public PaymentProposalCreationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            // The handler for the grid selection changed event.
            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);

            this.PaymentSelectedCommand = new RelayCommand(this.CreatePaymentProposals);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a vlaue indicating whether a payment is consolidated.
        /// </summary>
        public bool ConsolidatedPayment
        {
            get
            {
                return this.consolidatedPayment;
            }

            set
            {
                this.consolidatedPayment = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected payment.
        /// </summary>
        public Sale SelectedPayment { get; set; }
      
        #endregion

        #region method

        /// <summary>
        /// Sets the current invoices.
        /// </summary>
        /// <param name="sales">The invoices to set.</param>
        public void SetInvoices(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleID));
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand PaymentSelectedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Retrieves the proposal data.
        /// </summary>
        protected virtual void GetProposals()
        {
            var orderStatuses = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusMoved.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID, 
                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID
            };

            if (this.ShowAllOrders)
            {
                var localSales = this.DataManager
                    .GetAllLairageIntakes(orderStatuses, this.FromDate,
                        this.ToDate, Strings.KillDate);

                this.FilteredSales = new ObservableCollection<Sale>(localSales);
            }
            else
            {
                var localSales = this.DataManager.GetAllLairageIntakes(new List<int?> { NouvemGlobal.NouDocStatusFilled.NouDocStatusID },
                    this.FromDate, this.ToDate, Strings.KillDate);

                this.FilteredSales = new ObservableCollection<Sale>(localSales);
            }
        }

        /// <summary>
        /// Handles the filter sales update, setting the control mode.
        /// </summary>
        protected override void HandleFilteredSales()
        {
            if (this.SelectedSales.Any())
            {
                this.SetControlMode(ControlMode.Add);
            }
            else
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Refreshes the intakes data.
        /// </summary>
        protected override void Refresh()
        {
            this.GetProposals();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.ToDate = DateTime.Today;
            this.FromDate = ApplicationSettings.PaymentProposalCreateFromDate;
            this.ShowAllOrders = ApplicationSettings.PaymentProposalCreateShowAllOrders;
            this.IsFormLoaded = true;
            this.SelectedSales.Clear();
            this.GetProposals();
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            ApplicationSettings.PaymentProposalCreateFromDate = this.FromDate;
            ApplicationSettings.PaymentProposalCreateShowAllOrders = this.ShowAllOrders;
        }

        /// <summary>
        /// Override, to only allow update/ok.
        /// </summary>
        /// <param name="mode">The current mode.</param>
        protected override void ControlCommandExecute(string mode)
        {
            if (this.CurrentMode == ControlMode.Find || this.CurrentMode == ControlMode.Update)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.CreatePaymentProposals();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
            ViewModelLocator.ClearPaymentProposalCreation();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
                ViewModelLocator.ClearPaymentProposalCreation();
            }
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        protected virtual void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedSales == null || !this.SelectedSales.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }
            
            this.SetControlMode(ControlMode.Add);
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region helper

        /// <summary>
        /// Creates the proposals.
        /// </summary>
        private void CreatePaymentProposals()
        {
            try
            {
                if (this.SelectedSales != null)
                {
                    var unFilledSales = this.SelectedSales.Where(x => x.NouDocStatusID != NouvemGlobal.NouDocStatusFilled.NouDocStatusID).ToList();
                    if (unFilledSales.Any())
                    {
                        var message = string.Empty;
                        var completedCount = unFilledSales.Count(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID);
                  
                        if (completedCount > 0)
                        {
                            if (completedCount == 1)
                            {
                                NouvemMessageBox.Show(Message.LairageBatchCompletedWarning);
                                return;
                            }

                            NouvemMessageBox.Show(Message.LairageBatchesCompletedWarning);
                            return;
                        }

                        var cancelledCount = unFilledSales.Count(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID);
                      
                        if (cancelledCount > 0)
                        {
                            if (cancelledCount == 1)
                            {
                                NouvemMessageBox.Show(Message.LairageBatchCancelledWarning);
                                return;
                            }

                            NouvemMessageBox.Show(Message.LairageBatchesCancelledWarning);
                            return;
                        }

                        if (unFilledSales.Count == 1)
                        {
                            message = string.Format(Message.AddingUnfilledLairageToPaymentWarning,
                                unFilledSales.First().Number);
                        }
                        else
                        {
                            var batches = string.Join(",", unFilledSales.Select(x => x.Number));
                            message = string.Format(Message.AddingUnfilledLairagesToPaymentWarning, batches);
                        }

                        NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.No)
                        {
                            return;
                        }
                    }

                    var filledSales = this.SelectedSales;
                    if (filledSales.Any())
                    {
                        if (this.ConsolidatedPayment)
                        {
                            this.ConsolidatePayment();
                            return;
                        }

                        var paymentNos = new List<int>();

                        ProgressBar.SetUp(0, this.SelectedSales.Count * 2);
                        foreach (var localPaymentToCreate in this.SelectedSales)
                        {
                            var paymentToCreate = this.DataManager.GetLairageIntakeById(localPaymentToCreate.SaleID, false);
                            paymentToCreate.DocumentDate = DateTime.Today;
                            paymentToCreate.DeliveryDate = DateTime.Today;
                            ProgressBar.Run();

                            paymentToCreate.DocumentDate = DateTime.Today;
                            paymentToCreate.DeliveryDate = DateTime.Today;
                            paymentToCreate.BaseDocumentReferenceID = paymentToCreate.SaleID;
                            paymentToCreate.CreationDate = DateTime.Now;
                            paymentToCreate.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                            var newSale = this.DataManager.AddNewPaymentProposal(paymentToCreate);
                            var newSaleId = newSale.Item1;
                            if (newSaleId > 0)
                            {
                                paymentNos.Add(paymentToCreate.Number);
                            }
                            else
                            {
                                SystemMessage.Write(MessageType.Issue, Message.PaymentProposalNotCreated);
                            }

                            ProgressBar.Run();
                        }

                        if (paymentNos.Any())
                        {
                            var numbers = paymentNos.Count == 1 ? paymentNos.First().ToString() : string.Join(",", paymentNos.Select(x => x.ToString()));
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.PaymentProposalsCreated, numbers));
                            this.Refresh();
                        }
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Consolidates a payment.
        /// </summary>
        private void ConsolidatePayment()
        {
            #region validation

            var suppliers = this.SelectedSales.Select(x => x.Customer.BPMasterID).ToList();
            if (suppliers.Distinct().Count() != 1)
            {
                // attempting to consolidate payments from different suppliers, so exit with error message
                SystemMessage.Write(MessageType.Issue, Message.ConsolidatePaymentError);
                return;
            }

            var killTypes = this.SelectedSales.Select(x => x.KillType).ToList();
            if (killTypes.Distinct().Count() != 1)
            {
                // attempting to consolidate payments from different kill types, so exit with error message
                SystemMessage.Write(MessageType.Issue, Message.ConsolidatePaymentAnimalTypeError);
                return;
            }

            #endregion

            try
            {
                var paymentNos = new List<int>();
                ProgressBar.SetUp(0, this.SelectedSales.Count * 2);
                var animals = new List<StockDetail>();
                var paymentToCreate = new Sale();
                var baseIds = new List<int>();
                foreach (var localPaymentToCreate in this.SelectedSales)
                {
                    paymentToCreate = this.DataManager.GetLairageIntakeById(localPaymentToCreate.SaleID, false);
                    baseIds.Add(paymentToCreate.SaleID);
                    if (paymentToCreate.SaleDetails != null && paymentToCreate.SaleDetails.Any())
                    {
                        if (paymentToCreate.SaleDetails.First().StockDetails != null)
                        {
                            foreach (var stockDetail in paymentToCreate.SaleDetails.First().StockDetails)
                            {
                                animals.Add(stockDetail);
                            }
                        }
                    }

                    ProgressBar.Run();
                }

                if (paymentToCreate.SaleDetails == null)
                {
                    paymentToCreate.SaleDetails = new List<SaleDetail>();
                    paymentToCreate.SaleDetails.Add(new SaleDetail());
                }

                paymentToCreate.IdsToMarkAsInvoiced = baseIds;
                paymentToCreate.SaleDetails.First().StockDetails = animals;
                paymentToCreate.DocumentDate = DateTime.Today;
                paymentToCreate.DeliveryDate = DateTime.Today;
                paymentToCreate.DocumentDate = DateTime.Today;
                paymentToCreate.DeliveryDate = DateTime.Today;
                paymentToCreate.BaseDocumentReferenceID = paymentToCreate.SaleID;
                paymentToCreate.CreationDate = DateTime.Now;
                paymentToCreate.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;

                var newSale = this.DataManager.AddNewPaymentProposal(paymentToCreate);
                var newSaleId = newSale.Item1;
                if (newSaleId > 0)
                {
                    paymentNos.Add(paymentToCreate.Number);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PaymentProposalNotCreated);
                }

                if (paymentNos.Any())
                {
                    var numbers = paymentNos.Count == 1 ? paymentNos.First().ToString() : string.Join(",", paymentNos.Select(x => x.ToString()));
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.PaymentProposalsCreated, numbers));
                    this.Refresh();
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SetControlMode(ControlMode.OK);
                this.ConsolidatedPayment = false;
            }
        }

        #endregion

        #endregion
    }
}

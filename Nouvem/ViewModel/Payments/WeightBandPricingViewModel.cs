﻿// -----------------------------------------------------------------------
// <copyright file="WeightBandPricingViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Nouvem.Model.BusinessObject;

namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class WeightBandPricingViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected group.
        /// </summary>
        private WeightBandGroup selectedWeightBandGroup;

        /// <summary>
        /// The weight bands.
        /// </summary>
        public ObservableCollection<WeightBandData> weightBands = new ObservableCollection<WeightBandData>();

        /// <summary>
        /// The weight bands.
        /// </summary>
        public IList<WeightBandData> allWeightBands;
      
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the WeightBandPricingViewModel class.
        /// </summary>
        public WeightBandPricingViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            #endregion

            #region instantiation

            this.WeightBandGroups = new List<WeightBandGroup>();
            this.WeightBands = new ObservableCollection<WeightBandData>();

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetWeightBandGroups();
            this.GetWeightBands();
        }

        #endregion

        #region property
       
        /// <summary>
        /// Get or sets the selected band.
        /// </summary>
        public WeightBandGroup SelectedWeightBandGroup
        {
            get
            {
                return this.selectedWeightBandGroup;
            }

            set
            {
                this.selectedWeightBandGroup = value;
                this.RaisePropertyChanged();
                this.WeightBands.Clear();

                if (value != null)
                {
                    var localBands = this.allWeightBands.Where(x => x.WeightBandGroupId == value.WeightBandGroupID);
                    foreach (var weightBandData in localBands)
                    {
                        this.WeightBands.Add(weightBandData);
                    }
                }
            }
        }

        /// <summary>
        /// Get or sets the groups.
        /// </summary>
        public IList<WeightBandGroup> WeightBandGroups { get; set; }


        /// <summary>
        /// Gets or sets the bands.
        /// </summary>
        public ObservableCollection<WeightBandData> WeightBands
        {
            get
            {
                return this.weightBands;
            }

            set
            {
                this.weightBands = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override
       
        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.ApplyWeightBandPrices();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the weight bands.
        /// </summary>
        private void ApplyWeightBandPrices()
        {
            Messenger.Default.Send(this.WeightBands.ToList(), Token.ApplyWeightBandPrices);
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearWeightBandPricing();
            Messenger.Default.Send(Token.Message, Token.CloseWeightBandPricingWindow);
        }

        /// <summary>
        /// Gets the application weight bands.
        /// </summary>
        private void GetWeightBands()
        {
            this.allWeightBands = this.DataManager.GetWeightBands();
        }

        /// <summary>
        /// Gets the application weight band groups.
        /// </summary>
        private void GetWeightBandGroups()
        {
            this.WeightBandGroups.Clear();
            foreach (var band in this.DataManager.GetWeightBandGroups())
            {
                this.WeightBandGroups.Add(band);
            }
        }

        #endregion
    }
}





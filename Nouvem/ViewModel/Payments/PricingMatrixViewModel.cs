﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Payments
{
    public class PricingMatrixViewModel : PaymentsViewModelBase
    {

        #region field

        private StockDetail pricingData = new StockDetail();
        private decimal? cell1;
        private decimal? cell2;
        private decimal? cell3;
        private decimal? cell4;
        private decimal? cell5;
        private decimal? cell6;
        private decimal? cell7;
        private decimal? cell8;
        private decimal? cell9;
        private decimal? cell10;
        private decimal? cell11;
        private decimal? cell12;
        private decimal? cell13;
        private decimal? cell14;
        private decimal? cell15;
        private decimal? cell16;
        private decimal? cell17;
        private decimal? cell18;
        private decimal? cell19;
        private decimal? cell20;
        private decimal? cell21;
        private decimal? cell22;
        private decimal? cell23;
        private decimal? cell24;
        private decimal? cell25;
        private decimal? cell26;
        private decimal? cell27;
        private decimal? cell28;
        private decimal? cell29;
        private decimal? cell30;
        private decimal? cell31;
        private decimal? cell32;
        private decimal? cell33;
        private decimal? cell34;
        private decimal? cell35;
        private decimal? cell36;
        private decimal? cell37;
        private decimal? cell38;
        private decimal? cell39;
        private decimal? cell40;
        private decimal? cell41;
        private decimal? cell42;
        private decimal? cell43;
        private decimal? cell44;
        private decimal? cell45;
        private decimal? cell46;
        private decimal? cell47;
        private decimal? cell48;
        private decimal? cell49;
        private decimal? cell50;
        private decimal? cell51;
        private decimal? cell52;
        private decimal? cell53;
        private decimal? cell54;
        private decimal? cell55;
        private decimal? cell56;
        private decimal? cell57;
        private decimal? cell58;
        private decimal? cell59;
        private decimal? cell60;
        private decimal? cell61;
        private decimal? cell62;
        private decimal? cell63;
        private decimal? cell64;
        private decimal? cell65;
        private decimal? cell66;
        private decimal? cell67;
        private decimal? cell68;
        private decimal? cell69;
        private decimal? cell70;
        private decimal? cell71;
        private decimal? cell72;
        private decimal? cell73;
        private decimal? cell74;
        private decimal? cell75;
        private decimal? cell76;
        private decimal? cell77;
        private decimal? cell78;
        private decimal? cell79;
        private decimal? cell80;
        private decimal? cell81;
        private decimal? cell82;
        private decimal? cell83;
        private decimal? cell84;
        private decimal? cell85;
        private decimal? cell86;
        private decimal? cell87;
        private decimal? cell88;
        private decimal? cell89;
        private decimal? cell90;
        private decimal? cell91;
        private decimal? cell92;
        private decimal? cell93;
        private decimal? cell94;
        private decimal? cell95;
        private decimal? cell96;
        private decimal? cell97;
        private decimal? cell98;
        private decimal? cell99;
        private decimal? cell100;
        private decimal? cell101;
        private decimal? cell102;
        private decimal? cell103;
        private decimal? cell104;
        private decimal? cell105;
        private decimal? cell106;
        private decimal? cell107;
        private decimal? cell108;
        private decimal? cell109;
        private decimal? cell110;
        private decimal? cell111;
        private decimal? cell112;
        private decimal? cell113;
        private decimal? cell114;
        private decimal? cell115;
        private decimal? cell116;
        private decimal? cell117;
        private decimal? cell118;
        private decimal? cell119;
        private decimal? cell120;
        private decimal? cell121;
        private decimal? cell122;
        private decimal? cell123;
        private decimal? cell124;
        private decimal? cell125;
        private decimal? cell126;
        private decimal? cell127;
        private decimal? cell128;
        private decimal? cell129;
        private decimal? cell130;
        private decimal? cell131;
        private decimal? cell132;
        private decimal? cell133;
        private decimal? cell134;
        private decimal? cell135;
        private decimal? cell136;
        private decimal? cell137;
        private decimal? cell138;
        private decimal? cell139;
        private decimal? cell140;
        private decimal? cell141;
        private decimal? cell142;
        private decimal? cell143;
        private decimal? cell144;
        private decimal? cell145;
        private decimal? cell146;
        private decimal? cell147;
        private decimal? cell148;
        private decimal? cell149;
        private decimal? cell150;
        private decimal? cell151;
        private decimal? cell152;
        private decimal? cell153;
        private decimal? cell154;
        private decimal? cell155;
        private decimal? cell156;
        private decimal? cell157;
        private decimal? cell158;
        private decimal? cell159;
        private decimal? cell160;
        private decimal? cell161;
        private decimal? cell162;
        private decimal? cell163;
        private decimal? cell164;
        private decimal? cell165;
        private decimal? cell166;
        private decimal? cell167;
        private decimal? cell168;
        private decimal? cell169;
        private decimal? cell170;
        private decimal? cell171;
        private decimal? cell172;
        private decimal? cell173;
        private decimal? cell174;
        private decimal? cell175;
        private decimal? cell176;
        private decimal? cell177;
        private decimal? cell178;
        private decimal? cell179;
        private decimal? cell180;
        private decimal? cell181;
        private decimal? cell182;
        private decimal? cell183;
        private decimal? cell184;
        private decimal? cell185;
        private decimal? cell186;
        private decimal? cell187;
        private decimal? cell188;
        private decimal? cell189;
        private decimal? cell190;
        private decimal? cell191;
        private decimal? cell192;
        private decimal? cell193;
        private decimal? cell194;
        private decimal? cell195;
        private decimal? cell196;
        private decimal? cell197;
        private decimal? cell198;
        private decimal? cell199;
        private decimal? cell200;
        private decimal? cell201;
        private decimal? cell202;
        private decimal? cell203;
        private decimal? cell204;
        private decimal? cell205;
        private decimal? cell206;
        private decimal? cell207;
        private decimal? cell208;
        private decimal? cell209;
        private decimal? cell210;
        private decimal? cell211;
        private decimal? cell212;
        private decimal? cell213;
        private decimal? cell214;
        private decimal? cell215;
        private decimal? cell216;
        private decimal? cell217;
        private decimal? cell218;
        private decimal? cell219;
        private decimal? cell220;
        private decimal? cell221;
        private decimal? cell222;
        private decimal? cell223;
        private decimal? cell224;
        private decimal? cell225;
        private decimal? cell226;
        private decimal? cell227;
        private decimal? cell228;
        private decimal? cell229;
        private decimal? cell230;
        private decimal? cell231;
        private decimal? cell232;
        private decimal? cell233;
        private decimal? cell234;
        private decimal? cell235;
        private decimal? cell236;
        private decimal? cell237;
        private decimal? cell238;
        private decimal? cell239;
        private decimal? cell240;
        private decimal? cell241;
        private decimal? cell242;
        private decimal? cell243;
        private decimal? cell244;
        private decimal? cell245;
        private decimal? cell246;
        private decimal? cell247;
        private decimal? cell248;
        private decimal? cell249;
        private decimal? cell250;
        private decimal? cell251;
        private decimal? cell252;
        private decimal? cell253;
        private decimal? cell254;
        private decimal? cell255;
        private decimal? cell256;
        private decimal? cell257;
        private decimal? cell258;
        private decimal? cell259;
        private decimal? cell260;
        private decimal? cell261;
        private decimal? cell262;
        private decimal? cell263;
        private decimal? cell264;
        private decimal? cell265;
        private decimal? cell266;
        private decimal? cell267;
        private decimal? cell268;
        private decimal? cell269;
        private decimal? cell270;
        private decimal? cell271;
        private decimal? cell272;
        private decimal? cell273;
        private decimal? cell274;
        private decimal? cell275;
        private decimal? cell276;
        private decimal? cell277;
        private decimal? cell278;
        private decimal? cell279;


        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PricingMatrixViewModel"/> class.
        /// </summary>
        public PricingMatrixViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.GetPricingMatrix();
            this.SetControlMode(ControlMode.Update);
        }

        #endregion

        #region public interface

        #region property

        public decimal? Cell1 { get { return this.cell1; } set { this.cell1 = value.ToNullableDecimalFormatted(2).ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell2 { get { return this.cell2; } set { this.cell2 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell3 { get { return this.cell3; } set { this.cell3 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell4 { get { return this.cell4; } set { this.cell4 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell5 { get { return this.cell5; } set { this.cell5 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell6 { get { return this.cell6; } set { this.cell6 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell7 { get { return this.cell7; } set { this.cell7 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell8 { get { return this.cell8; } set { this.cell8 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell9 { get { return this.cell9; } set { this.cell9 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell10 { get { return this.cell10; } set { this.cell10 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell11 { get { return this.cell11; } set { this.cell11 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell12 { get { return this.cell12; } set { this.cell12 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell13 { get { return this.cell13; } set { this.cell13 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell14 { get { return this.cell14; } set { this.cell14 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell15 { get { return this.cell15; } set { this.cell15 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell16 { get { return this.cell16; } set { this.cell16 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell17 { get { return this.cell17; } set { this.cell17 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell18 { get { return this.cell18; } set { this.cell18 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell19 { get { return this.cell19; } set { this.cell19 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell20 { get { return this.cell20; } set { this.cell20 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell21 { get { return this.cell21; } set { this.cell21 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell22 { get { return this.cell22; } set { this.cell22 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell23 { get { return this.cell23; } set { this.cell23 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell24 { get { return this.cell24; } set { this.cell24 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell25 { get { return this.cell25; } set { this.cell25 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell26 { get { return this.cell26; } set { this.cell26 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell27 { get { return this.cell27; } set { this.cell27 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell28 { get { return this.cell28; } set { this.cell28 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell29 { get { return this.cell29; } set { this.cell29 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell30 { get { return this.cell30; } set { this.cell30 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell31 { get { return this.cell31; } set { this.cell31 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell32 { get { return this.cell32; } set { this.cell32 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell33 { get { return this.cell33; } set { this.cell33 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell34 { get { return this.cell34; } set { this.cell34 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell35 { get { return this.cell35; } set { this.cell35 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell36 { get { return this.cell36; } set { this.cell36 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell37 { get { return this.cell37; } set { this.cell37 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell38 { get { return this.cell38; } set { this.cell38 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell39 { get { return this.cell39; } set { this.cell39 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell40 { get { return this.cell40; } set { this.cell40 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell41 { get { return this.cell41; } set { this.cell41 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell42 { get { return this.cell42; } set { this.cell42 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell43 { get { return this.cell43; } set { this.cell43 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell44 { get { return this.cell44; } set { this.cell44 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell45 { get { return this.cell45; } set { this.cell45 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell46 { get { return this.cell46; } set { this.cell46 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell47 { get { return this.cell47; } set { this.cell47 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell48 { get { return this.cell48; } set { this.cell48 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell49 { get { return this.cell49; } set { this.cell49 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell50 { get { return this.cell50; } set { this.cell50 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell51 { get { return this.cell51; } set { this.cell51 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell52 { get { return this.cell52; } set { this.cell52 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell53 { get { return this.cell53; } set { this.cell53 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell54 { get { return this.cell54; } set { this.cell54 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell55 { get { return this.cell55; } set { this.cell55 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell56 { get { return this.cell56; } set { this.cell56 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell57 { get { return this.cell57; } set { this.cell57 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell58 { get { return this.cell58; } set { this.cell58 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell59 { get { return this.cell59; } set { this.cell59 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell60 { get { return this.cell60; } set { this.cell60 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell61 { get { return this.cell61; } set { this.cell61 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell62 { get { return this.cell62; } set { this.cell62 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell63 { get { return this.cell63; } set { this.cell63 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell64 { get { return this.cell64; } set { this.cell64 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell65 { get { return this.cell65; } set { this.cell65 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell66 { get { return this.cell66; } set { this.cell66 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell67 { get { return this.cell67; } set { this.cell67 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell68 { get { return this.cell68; } set { this.cell68 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell69 { get { return this.cell69; } set { this.cell69 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell70 { get { return this.cell70; } set { this.cell70 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell71 { get { return this.cell71; } set { this.cell71 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell72 { get { return this.cell72; } set { this.cell72 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell73 { get { return this.cell73; } set { this.cell73 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell74 { get { return this.cell74; } set { this.cell74 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell75 { get { return this.cell75; } set { this.cell75 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell76 { get { return this.cell76; } set { this.cell76 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell77 { get { return this.cell77; } set { this.cell77 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell78 { get { return this.cell78; } set { this.cell78 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell79 { get { return this.cell79; } set { this.cell79 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell80 { get { return this.cell80; } set { this.cell80 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell81 { get { return this.cell81; } set { this.cell81 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell82 { get { return this.cell82; } set { this.cell82 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell83 { get { return this.cell83; } set { this.cell83 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell84 { get { return this.cell84; } set { this.cell84 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell85 { get { return this.cell85; } set { this.cell85 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell86 { get { return this.cell86; } set { this.cell86 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell87 { get { return this.cell87; } set { this.cell87 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell88 { get { return this.cell88; } set { this.cell88 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell89 { get { return this.cell89; } set { this.cell89 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell90 { get { return this.cell90; } set { this.cell90 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell91 { get { return this.cell91; } set { this.cell91 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell92 { get { return this.cell92; } set { this.cell92 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell93 { get { return this.cell93; } set { this.cell93 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell94 { get { return this.cell94; } set { this.cell94 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell95 { get { return this.cell95; } set { this.cell95 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell96 { get { return this.cell96; } set { this.cell96 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell97 { get { return this.cell97; } set { this.cell97 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell98 { get { return this.cell98; } set { this.cell98 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell99 { get { return this.cell99; } set { this.cell99 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell100 { get { return this.cell100; } set { this.cell100 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell101 { get { return this.cell101; } set { this.cell101 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell102 { get { return this.cell102; } set { this.cell102 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell103 { get { return this.cell103; } set { this.cell103 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell104 { get { return this.cell104; } set { this.cell104 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell105 { get { return this.cell105; } set { this.cell105 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell106 { get { return this.cell106; } set { this.cell106 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell107 { get { return this.cell107; } set { this.cell107 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell108 { get { return this.cell108; } set { this.cell108 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell109 { get { return this.cell109; } set { this.cell109 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell110 { get { return this.cell110; } set { this.cell110 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell111 { get { return this.cell111; } set { this.cell111 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell112 { get { return this.cell112; } set { this.cell112 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell113 { get { return this.cell113; } set { this.cell113 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell114 { get { return this.cell114; } set { this.cell114 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell115 { get { return this.cell115; } set { this.cell115 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell116 { get { return this.cell116; } set { this.cell116 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell117 { get { return this.cell117; } set { this.cell117 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell118 { get { return this.cell118; } set { this.cell118 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell119 { get { return this.cell119; } set { this.cell119 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell120 { get { return this.cell120; } set { this.cell120 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell121 { get { return this.cell121; } set { this.cell121 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell122 { get { return this.cell122; } set { this.cell122 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell123 { get { return this.cell123; } set { this.cell123 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell124 { get { return this.cell124; } set { this.cell124 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell125 { get { return this.cell125; } set { this.cell125 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell126 { get { return this.cell126; } set { this.cell126 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell127 { get { return this.cell127; } set { this.cell127 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell128 { get { return this.cell128; } set { this.cell128 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell129 { get { return this.cell129; } set { this.cell129 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell130 { get { return this.cell130; } set { this.cell130 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell131 { get { return this.cell131; } set { this.cell131 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell132 { get { return this.cell132; } set { this.cell132 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell133 { get { return this.cell133; } set { this.cell133 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell134 { get { return this.cell134; } set { this.cell134 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell135 { get { return this.cell135; } set { this.cell135 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell136 { get { return this.cell136; } set { this.cell136 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell137 { get { return this.cell137; } set { this.cell137 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell138 { get { return this.cell138; } set { this.cell138 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell139 { get { return this.cell139; } set { this.cell139 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell140 { get { return this.cell140; } set { this.cell140 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell141 { get { return this.cell141; } set { this.cell141 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell142 { get { return this.cell142; } set { this.cell142 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell143 { get { return this.cell143; } set { this.cell143 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell144 { get { return this.cell144; } set { this.cell144 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell145 { get { return this.cell145; } set { this.cell145 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell146 { get { return this.cell146; } set { this.cell146 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell147 { get { return this.cell147; } set { this.cell147 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell148 { get { return this.cell148; } set { this.cell148 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell149 { get { return this.cell149; } set { this.cell149 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell150 { get { return this.cell150; } set { this.cell150 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell151 { get { return this.cell151; } set { this.cell151 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell152 { get { return this.cell152; } set { this.cell152 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell153 { get { return this.cell153; } set { this.cell153 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell154 { get { return this.cell154; } set { this.cell154 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell155 { get { return this.cell155; } set { this.cell155 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell156 { get { return this.cell156; } set { this.cell156 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell157 { get { return this.cell157; } set { this.cell157 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell158 { get { return this.cell158; } set { this.cell158 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell159 { get { return this.cell159; } set { this.cell159 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell160 { get { return this.cell160; } set { this.cell160 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell161 { get { return this.cell161; } set { this.cell161 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell162 { get { return this.cell162; } set { this.cell162 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell163 { get { return this.cell163; } set { this.cell163 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell164 { get { return this.cell164; } set { this.cell164 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell165 { get { return this.cell165; } set { this.cell165 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell166 { get { return this.cell166; } set { this.cell166 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell167 { get { return this.cell167; } set { this.cell167 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell168 { get { return this.cell168; } set { this.cell168 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell169 { get { return this.cell169; } set { this.cell169 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell170 { get { return this.cell170; } set { this.cell170 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell171 { get { return this.cell171; } set { this.cell171 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell172 { get { return this.cell172; } set { this.cell172 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell173 { get { return this.cell173; } set { this.cell173 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell174 { get { return this.cell174; } set { this.cell174 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell175 { get { return this.cell175; } set { this.cell175 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell176 { get { return this.cell176; } set { this.cell176 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell177 { get { return this.cell177; } set { this.cell177 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell178 { get { return this.cell178; } set { this.cell178 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell179 { get { return this.cell179; } set { this.cell179 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell180 { get { return this.cell180; } set { this.cell180 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell181 { get { return this.cell181; } set { this.cell181 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell182 { get { return this.cell182; } set { this.cell182 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell183 { get { return this.cell183; } set { this.cell183 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell184 { get { return this.cell184; } set { this.cell184 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell185 { get { return this.cell185; } set { this.cell185 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell186 { get { return this.cell186; } set { this.cell186 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell187 { get { return this.cell187; } set { this.cell187 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell188 { get { return this.cell188; } set { this.cell188 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell189 { get { return this.cell189; } set { this.cell189 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell190 { get { return this.cell190; } set { this.cell190 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell191 { get { return this.cell191; } set { this.cell191 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell192 { get { return this.cell192; } set { this.cell192 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell193 { get { return this.cell193; } set { this.cell193 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell194 { get { return this.cell194; } set { this.cell194 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell195 { get { return this.cell195; } set { this.cell195 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell196 { get { return this.cell196; } set { this.cell196 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell197 { get { return this.cell197; } set { this.cell197 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell198 { get { return this.cell198; } set { this.cell198 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell199 { get { return this.cell199; } set { this.cell199 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell200 { get { return this.cell200; } set { this.cell200 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell201 { get { return this.cell201; } set { this.cell201 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell202 { get { return this.cell202; } set { this.cell202 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell203 { get { return this.cell203; } set { this.cell203 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell204 { get { return this.cell204; } set { this.cell204 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell205 { get { return this.cell205; } set { this.cell205 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell206 { get { return this.cell206; } set { this.cell206 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell207 { get { return this.cell207; } set { this.cell207 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell208 { get { return this.cell208; } set { this.cell208 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell209 { get { return this.cell209; } set { this.cell209 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell210 { get { return this.cell210; } set { this.cell210 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell211 { get { return this.cell211; } set { this.cell211 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell212 { get { return this.cell212; } set { this.cell212 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell213 { get { return this.cell213; } set { this.cell213 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell214 { get { return this.cell214; } set { this.cell214 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell215 { get { return this.cell215; } set { this.cell215 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell216 { get { return this.cell216; } set { this.cell216 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell217 { get { return this.cell217; } set { this.cell217 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell218 { get { return this.cell218; } set { this.cell218 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell219 { get { return this.cell219; } set { this.cell219 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell220 { get { return this.cell220; } set { this.cell220 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell221 { get { return this.cell221; } set { this.cell221 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell222 { get { return this.cell222; } set { this.cell222 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell223 { get { return this.cell223; } set { this.cell223 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell224 { get { return this.cell224; } set { this.cell224 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell225 { get { return this.cell225; } set { this.cell225 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }

        public decimal? Cell226 { get { return this.cell226; } set { this.cell226 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell227 { get { return this.cell227; } set { this.cell227 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell228 { get { return this.cell228; } set { this.cell228 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell229 { get { return this.cell229; } set { this.cell229 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell230 { get { return this.cell230; } set { this.cell230 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell231 { get { return this.cell231; } set { this.cell231 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell232 { get { return this.cell232; } set { this.cell232 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell233 { get { return this.cell233; } set { this.cell233 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell234 { get { return this.cell234; } set { this.cell234 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell235 { get { return this.cell235; } set { this.cell235 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell236 { get { return this.cell236; } set { this.cell236 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell237 { get { return this.cell237; } set { this.cell237 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell238 { get { return this.cell238; } set { this.cell238 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell239 { get { return this.cell239; } set { this.cell239 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell240 { get { return this.cell240; } set { this.cell240 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell241 { get { return this.cell241; } set { this.cell241 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell242 { get { return this.cell242; } set { this.cell242 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell243 { get { return this.cell243; } set { this.cell243 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell244 { get { return this.cell244; } set { this.cell244 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell245 { get { return this.cell245; } set { this.cell245 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell246 { get { return this.cell246; } set { this.cell246 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell247 { get { return this.cell247; } set { this.cell247 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell248 { get { return this.cell248; } set { this.cell248 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell249 { get { return this.cell249; } set { this.cell249 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell250 { get { return this.cell250; } set { this.cell250 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell251 { get { return this.cell251; } set { this.cell251 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell252 { get { return this.cell252; } set { this.cell252 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell253 { get { return this.cell253; } set { this.cell253 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell254 { get { return this.cell254; } set { this.cell254 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell255 { get { return this.cell255; } set { this.cell255 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell256 { get { return this.cell256; } set { this.cell256 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell257 { get { return this.cell257; } set { this.cell257 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell258 { get { return this.cell258; } set { this.cell258 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell259 { get { return this.cell259; } set { this.cell259 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell260 { get { return this.cell260; } set { this.cell260 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell261 { get { return this.cell261; } set { this.cell261 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell262 { get { return this.cell262; } set { this.cell262 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell263 { get { return this.cell263; } set { this.cell263 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell264 { get { return this.cell264; } set { this.cell264 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell265 { get { return this.cell265; } set { this.cell265 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell266 { get { return this.cell266; } set { this.cell266 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell267 { get { return this.cell267; } set { this.cell267 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell268 { get { return this.cell268; } set { this.cell268 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell269 { get { return this.cell269; } set { this.cell269 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell270 { get { return this.cell270; } set { this.cell270 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell271 { get { return this.cell271; } set { this.cell271 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell272 { get { return this.cell272; } set { this.cell272 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell273 { get { return this.cell273; } set { this.cell273 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell274 { get { return this.cell274; } set { this.cell274 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell275 { get { return this.cell275; } set { this.cell275 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell276 { get { return this.cell276; } set { this.cell276 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell277 { get { return this.cell277; } set { this.cell277 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell278 { get { return this.cell278; } set { this.cell278 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }
        public decimal? Cell279 { get { return this.cell279; } set { this.cell279 = value.ToNullableDecimalFormatted(2); this.RaisePropertyChanged(); } }



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Override, to handle a control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                   
                    break;

                case ControlMode.Find:
                  
                    break;

                case ControlMode.Update:
                    this.UpdatePricingMatrix();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new or find partner, clearing the form.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var controlMode = (ControlMode)Enum.Parse(typeof(ControlMode), mode);
            this.SetControlMode(controlMode);
        }

        #endregion

        #region private

        protected override void GetPricingMatrix()
        {
            var pricing = this.DataManager.GetPricingMatrix();
            if (!pricing.Any())
            {
                return;
            }

            #region set prices

            this.Cell1 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-1-")).PriceAdjust;
            this.Cell2 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-1=")).PriceAdjust;
            this.Cell3 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-1+")).PriceAdjust;
            this.Cell4 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-2-")).PriceAdjust;
            this.Cell5 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-2=")).PriceAdjust;
            this.Cell6 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-2+")).PriceAdjust;
            this.Cell7 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-3-")).PriceAdjust;
            this.Cell8 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-3=")).PriceAdjust;
            this.Cell9 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-3+")).PriceAdjust;
            this.Cell10 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-4-")).PriceAdjust;
            this.Cell11 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-4=")).PriceAdjust;
            this.Cell12 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-4+")).PriceAdjust;
            this.Cell13 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-5-")).PriceAdjust;
            this.Cell14 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-5=")).PriceAdjust;
            this.Cell15 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E-5+")).PriceAdjust;
            this.Cell16 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=1-")).PriceAdjust;
            this.Cell17 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=1=")).PriceAdjust;
            this.Cell18 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=1+")).PriceAdjust;
            this.Cell19 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=2-")).PriceAdjust;
            this.Cell20 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=2=")).PriceAdjust;
            this.Cell21 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=2+")).PriceAdjust;
            this.Cell22 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=3-")).PriceAdjust;
            this.Cell23 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=3=")).PriceAdjust;
            this.Cell24 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=3+")).PriceAdjust;
            this.Cell25 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=4-")).PriceAdjust;
            this.Cell26 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=4=")).PriceAdjust;
            this.Cell27 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=4+")).PriceAdjust;
            this.Cell28 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=5-")).PriceAdjust;
            this.Cell29 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=5=")).PriceAdjust;
            this.Cell30 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E=5+")).PriceAdjust;
            this.Cell31 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+1-")).PriceAdjust;
            this.Cell32 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+1=")).PriceAdjust;
            this.Cell33 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+1+")).PriceAdjust;
            this.Cell34 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+2-")).PriceAdjust;
            this.Cell35 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+2=")).PriceAdjust;
            this.Cell36 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+2+")).PriceAdjust;
            this.Cell37 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+3-")).PriceAdjust;
            this.Cell38 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+3=")).PriceAdjust;
            this.Cell39 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+3+")).PriceAdjust;
            this.Cell40 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+4-")).PriceAdjust;
            this.Cell41 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+4=")).PriceAdjust;
            this.Cell42 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+4+")).PriceAdjust;
            this.Cell43 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+5-")).PriceAdjust;
            this.Cell44 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+5=")).PriceAdjust;
            this.Cell45 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E+5+")).PriceAdjust;
            this.Cell46 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-1-")).PriceAdjust;
            this.Cell47 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-1=")).PriceAdjust;
            this.Cell48 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-1+")).PriceAdjust;
            this.Cell49 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-2-")).PriceAdjust;
            this.Cell50 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-2=")).PriceAdjust;
            this.Cell51 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-2+")).PriceAdjust;
            this.Cell52 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-3-")).PriceAdjust;
            this.Cell53 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-3=")).PriceAdjust;
            this.Cell54 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-3+")).PriceAdjust;
            this.Cell55 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-4-")).PriceAdjust;
            this.Cell56 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-4=")).PriceAdjust;
            this.Cell57 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-4+")).PriceAdjust;
            this.Cell58 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-5-")).PriceAdjust;
            this.Cell59 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-5=")).PriceAdjust;
            this.Cell60 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U-5+")).PriceAdjust;
            this.Cell61 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=1-")).PriceAdjust;
            this.Cell62 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=1=")).PriceAdjust;
            this.Cell63 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=1+")).PriceAdjust;
            this.Cell64 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=2-")).PriceAdjust;
            this.Cell65 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=2=")).PriceAdjust;
            this.Cell66 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=2+")).PriceAdjust;
            this.Cell67 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=3-")).PriceAdjust;
            this.Cell68 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=3=")).PriceAdjust;
            this.Cell69 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=3+")).PriceAdjust;
            this.Cell70 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=4-")).PriceAdjust;
            this.Cell71 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=4=")).PriceAdjust;
            this.Cell72 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=4+")).PriceAdjust;
            this.Cell73 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=5-")).PriceAdjust;
            this.Cell74 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=5=")).PriceAdjust;
            this.Cell75 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U=5+")).PriceAdjust;
            this.Cell76 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+1-")).PriceAdjust;
            this.Cell77 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+1=")).PriceAdjust;
            this.Cell78 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+1+")).PriceAdjust;
            this.Cell79 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+2-")).PriceAdjust;
            this.Cell80 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+2=")).PriceAdjust;
            this.Cell81 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+2+")).PriceAdjust;
            this.Cell82 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+3-")).PriceAdjust;
            this.Cell83 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+3=")).PriceAdjust;
            this.Cell84 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+3+")).PriceAdjust;
            this.Cell85 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+4-")).PriceAdjust;
            this.Cell86 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+4=")).PriceAdjust;
            this.Cell87 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+4+")).PriceAdjust;
            this.Cell88 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+5-")).PriceAdjust;
            this.Cell89 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+5=")).PriceAdjust;
            this.Cell90 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U+5+")).PriceAdjust;
            this.Cell91 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-1-")).PriceAdjust;
            this.Cell92 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-1=")).PriceAdjust;
            this.Cell93 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-1+")).PriceAdjust;
            this.Cell94 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-2-")).PriceAdjust;
            this.Cell95 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-2=")).PriceAdjust;
            this.Cell96 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-2+")).PriceAdjust;
            this.Cell97 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-3-")).PriceAdjust;
            this.Cell98 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-3=")).PriceAdjust;
            this.Cell99 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-3+")).PriceAdjust;
            this.Cell100 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-4-")).PriceAdjust;
            this.Cell101 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-4=")).PriceAdjust;
            this.Cell102 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-4+")).PriceAdjust;
            this.Cell103 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-5-")).PriceAdjust;
            this.Cell104 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-5=")).PriceAdjust;
            this.Cell105 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R-5+")).PriceAdjust;
            this.Cell106 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=1-")).PriceAdjust;
            this.Cell107 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=1=")).PriceAdjust;
            this.Cell108 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=1+")).PriceAdjust;
            this.Cell109 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=2-")).PriceAdjust;
            this.Cell110 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=2=")).PriceAdjust;
            this.Cell111 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=2+")).PriceAdjust;
            this.Cell112 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=3-")).PriceAdjust;
            this.Cell113 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=3=")).PriceAdjust;
            this.Cell114 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=3+")).PriceAdjust;
            this.Cell115 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=4-")).PriceAdjust;
            this.Cell116 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=4=")).PriceAdjust;
            this.Cell117 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=4+")).PriceAdjust;
            this.Cell118 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=5-")).PriceAdjust;
            this.Cell119 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=5=")).PriceAdjust;
            this.Cell120 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R=5+")).PriceAdjust;
            this.Cell121 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+1-")).PriceAdjust;
            this.Cell122 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+1=")).PriceAdjust;
            this.Cell123 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+1+")).PriceAdjust;
            this.Cell124 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+2-")).PriceAdjust;
            this.Cell125 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+2=")).PriceAdjust;
            this.Cell126 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+2+")).PriceAdjust;
            this.Cell127 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+3-")).PriceAdjust;
            this.Cell128 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+3=")).PriceAdjust;
            this.Cell129 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+3+")).PriceAdjust;
            this.Cell130 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+4-")).PriceAdjust;
            this.Cell131 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+4=")).PriceAdjust;
            this.Cell132 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+4+")).PriceAdjust;
            this.Cell133 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+5-")).PriceAdjust;
            this.Cell134 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+5=")).PriceAdjust;
            this.Cell135 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R+5+")).PriceAdjust;
            this.Cell136 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-1-")).PriceAdjust;
            this.Cell137 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-1=")).PriceAdjust;
            this.Cell138 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-1+")).PriceAdjust;
            this.Cell139 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-2-")).PriceAdjust;
            this.Cell140 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-2=")).PriceAdjust;
            this.Cell141 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-2+")).PriceAdjust;
            this.Cell142 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-3-")).PriceAdjust;
            this.Cell143 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-3=")).PriceAdjust;
            this.Cell144 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-3+")).PriceAdjust;
            this.Cell145 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-4-")).PriceAdjust;
            this.Cell146 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-4=")).PriceAdjust;
            this.Cell147 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-4+")).PriceAdjust;
            this.Cell148 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-5-")).PriceAdjust;
            this.Cell149 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-5=")).PriceAdjust;
            this.Cell150 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-5+")).PriceAdjust;
            this.Cell151 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=1-")).PriceAdjust;
            this.Cell152 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=1=")).PriceAdjust;
            this.Cell153 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=1+")).PriceAdjust;
            this.Cell154 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=2-")).PriceAdjust;
            this.Cell155 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=2=")).PriceAdjust;
            this.Cell156 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=2+")).PriceAdjust;
            this.Cell157 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=3-")).PriceAdjust;
            this.Cell158 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=3=")).PriceAdjust;
            this.Cell159 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=3+")).PriceAdjust;
            this.Cell160 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=4-")).PriceAdjust;
            this.Cell161 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=4=")).PriceAdjust;
            this.Cell162 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=4+")).PriceAdjust;
            this.Cell163 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=5-")).PriceAdjust;
            this.Cell164 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=5=")).PriceAdjust;
            this.Cell165 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O=5+")).PriceAdjust;
            this.Cell166 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+1-")).PriceAdjust;
            this.Cell167 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+1=")).PriceAdjust;
            this.Cell168 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+1+")).PriceAdjust;
            this.Cell169 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+2-")).PriceAdjust;
            this.Cell170 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+2=")).PriceAdjust;
            this.Cell171 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+2+")).PriceAdjust;
            this.Cell172 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+3-")).PriceAdjust;
            this.Cell173 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+3=")).PriceAdjust;
            this.Cell174 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+3+")).PriceAdjust;
            this.Cell175 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+4-")).PriceAdjust;
            this.Cell176 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+4=")).PriceAdjust;
            this.Cell177 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+4+")).PriceAdjust;
            this.Cell178 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+5-")).PriceAdjust;
            this.Cell179 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+5=")).PriceAdjust;
            this.Cell180 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+5+")).PriceAdjust;
            this.Cell181 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-1-")).PriceAdjust;
            this.Cell182 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-1=")).PriceAdjust;
            this.Cell183 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-1+")).PriceAdjust;
            this.Cell184 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-2-")).PriceAdjust;
            this.Cell185 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-2=")).PriceAdjust;
            this.Cell186 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-2+")).PriceAdjust;
            this.Cell187 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-3-")).PriceAdjust;
            this.Cell188 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-3=")).PriceAdjust;
            this.Cell189 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-3+")).PriceAdjust;
            this.Cell190 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-4-")).PriceAdjust;
            this.Cell191 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-4=")).PriceAdjust;
            this.Cell192 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-4+")).PriceAdjust;
            this.Cell193 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-5-")).PriceAdjust;
            this.Cell194 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-5=")).PriceAdjust;
            this.Cell195 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-5+")).PriceAdjust;
            this.Cell196 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=1-")).PriceAdjust;
            this.Cell197 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=1=")).PriceAdjust;
            this.Cell198 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=1+")).PriceAdjust;
            this.Cell199 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=2-")).PriceAdjust;
            this.Cell200 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=2=")).PriceAdjust;
            this.Cell201 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=2+")).PriceAdjust;
            this.Cell202 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=3-")).PriceAdjust;
            this.Cell203 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=3=")).PriceAdjust;
            this.Cell204 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=3+")).PriceAdjust;
            this.Cell205 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=4-")).PriceAdjust;
            this.Cell206 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=4=")).PriceAdjust;
            this.Cell207 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=4+")).PriceAdjust;
            this.Cell208 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=5-")).PriceAdjust;
            this.Cell209 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=5=")).PriceAdjust;
            this.Cell210 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P=5+")).PriceAdjust;
            this.Cell211 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+1-")).PriceAdjust;
            this.Cell212 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+1=")).PriceAdjust;
            this.Cell213 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+1+")).PriceAdjust;
            this.Cell214 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+2-")).PriceAdjust;
            this.Cell215 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+2=")).PriceAdjust;
            this.Cell216 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+2+")).PriceAdjust;
            this.Cell217 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+3-")).PriceAdjust;
            this.Cell218 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+3=")).PriceAdjust;
            this.Cell219 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+3+")).PriceAdjust;
            this.Cell220 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+4-")).PriceAdjust;
            this.Cell221 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+4=")).PriceAdjust;
            this.Cell222 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+4+")).PriceAdjust;
            this.Cell223 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+5-")).PriceAdjust;
            this.Cell224 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+5=")).PriceAdjust;
            this.Cell225 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+5+")).PriceAdjust;


            this.Cell226 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E1")).PriceAdjust;
            this.Cell227 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E2")).PriceAdjust;
            this.Cell228 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E3")).PriceAdjust;
            this.Cell229 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E4H")).PriceAdjust;
            this.Cell230 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E4L")).PriceAdjust;
            this.Cell231 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("E5")).PriceAdjust;

            this.Cell232 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U1")).PriceAdjust;
            this.Cell233 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U2")).PriceAdjust;
            this.Cell234 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U3")).PriceAdjust;
            this.Cell235 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U4H")).PriceAdjust;
            this.Cell236 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U4L")).PriceAdjust;
            this.Cell237 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("U5")).PriceAdjust;

            this.Cell238 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R1")).PriceAdjust;
            this.Cell239 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R2")).PriceAdjust;
            this.Cell240 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R3")).PriceAdjust;
            this.Cell241 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R4H")).PriceAdjust;
            this.Cell242 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R4L")).PriceAdjust;
            this.Cell243 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("R5")).PriceAdjust;

            this.Cell244 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O1")).PriceAdjust;
            this.Cell245 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O2")).PriceAdjust;
            this.Cell246 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O3")).PriceAdjust;
            this.Cell247 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O4H")).PriceAdjust;
            this.Cell248 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O4L")).PriceAdjust;
            this.Cell249 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O5")).PriceAdjust;

            this.Cell250 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P1")).PriceAdjust;
            this.Cell251 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P2")).PriceAdjust;
            this.Cell252 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P3")).PriceAdjust;
            this.Cell253 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P4H")).PriceAdjust;
            this.Cell254 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P4L")).PriceAdjust;
            this.Cell255 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P5")).PriceAdjust;

            this.Cell256 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-1")).PriceAdjust;
            this.Cell257 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-2")).PriceAdjust;
            this.Cell258 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-3")).PriceAdjust;
            this.Cell259 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-4H")).PriceAdjust;
            this.Cell260 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-4L")).PriceAdjust;
            this.Cell261 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O-5")).PriceAdjust;

            this.Cell262 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+1")).PriceAdjust;
            this.Cell263 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+2")).PriceAdjust;
            this.Cell264 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+3")).PriceAdjust;
            this.Cell265 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+4H")).PriceAdjust;
            this.Cell266 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+4L")).PriceAdjust;
            this.Cell267 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("O+5")).PriceAdjust;

            this.Cell268 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-1")).PriceAdjust;
            this.Cell269 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-2")).PriceAdjust;
            this.Cell270 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-3")).PriceAdjust;
            this.Cell271 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-4H")).PriceAdjust;
            this.Cell272 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-4L")).PriceAdjust;
            this.Cell273 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P-5")).PriceAdjust;

            this.Cell274 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+1")).PriceAdjust;
            this.Cell275 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+2")).PriceAdjust;
            this.Cell276 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+3")).PriceAdjust;
            this.Cell277 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+4H")).PriceAdjust;
            this.Cell278 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+4L")).PriceAdjust;
            this.Cell279 = pricing.First(x => x.Grade.CompareIgnoringCaseAndWhitespace("P+5")).PriceAdjust;

            #endregion
        }

        private void UpdatePricingMatrix()
        {
            this.pricingData.Attribute1 = this.cell1.ToNullableString();
            this.pricingData.Attribute2 = this.cell2.ToNullableString();
            this.pricingData.Attribute3 = this.cell3.ToNullableString();
            this.pricingData.Attribute4 = this.cell4.ToNullableString();
            this.pricingData.Attribute5 = this.cell5.ToNullableString();
            this.pricingData.Attribute6 = this.cell6.ToNullableString();
            this.pricingData.Attribute7 = this.cell7.ToNullableString();
            this.pricingData.Attribute8 = this.cell8.ToNullableString();
            this.pricingData.Attribute9 = this.cell9.ToNullableString();
            this.pricingData.Attribute10 = this.cell10.ToNullableString();
            this.pricingData.Attribute11 = this.cell11.ToNullableString();
            this.pricingData.Attribute12 = this.cell12.ToNullableString();
            this.pricingData.Attribute13 = this.cell13.ToNullableString();
            this.pricingData.Attribute14 = this.cell14.ToNullableString();
            this.pricingData.Attribute15 = this.cell15.ToNullableString();
            this.pricingData.Attribute16 = this.cell16.ToNullableString();
            this.pricingData.Attribute17 = this.cell17.ToNullableString();
            this.pricingData.Attribute18 = this.cell18.ToNullableString();
            this.pricingData.Attribute19 = this.cell19.ToNullableString();
            this.pricingData.Attribute20 = this.cell20.ToNullableString();
            this.pricingData.Attribute21 = this.cell21.ToNullableString();
            this.pricingData.Attribute22 = this.cell22.ToNullableString();
            this.pricingData.Attribute23 = this.cell23.ToNullableString();
            this.pricingData.Attribute24 = this.cell24.ToNullableString();
            this.pricingData.Attribute25 = this.cell25.ToNullableString();
            this.pricingData.Attribute26 = this.cell26.ToNullableString();
            this.pricingData.Attribute27 = this.cell27.ToNullableString();
            this.pricingData.Attribute28 = this.cell28.ToNullableString();
            this.pricingData.Attribute29 = this.cell29.ToNullableString();
            this.pricingData.Attribute30 = this.cell30.ToNullableString();
            this.pricingData.Attribute31 = this.cell31.ToNullableString();
            this.pricingData.Attribute32 = this.cell32.ToNullableString();
            this.pricingData.Attribute33 = this.cell33.ToNullableString();
            this.pricingData.Attribute34 = this.cell34.ToNullableString();
            this.pricingData.Attribute35 = this.cell35.ToNullableString();
            this.pricingData.Attribute36 = this.cell36.ToNullableString();
            this.pricingData.Attribute37 = this.cell37.ToNullableString();
            this.pricingData.Attribute38 = this.cell38.ToNullableString();
            this.pricingData.Attribute39 = this.cell39.ToNullableString();
            this.pricingData.Attribute40 = this.cell40.ToNullableString();
            this.pricingData.Attribute41 = this.cell41.ToNullableString();
            this.pricingData.Attribute42 = this.cell42.ToNullableString();
            this.pricingData.Attribute43 = this.cell43.ToNullableString();
            this.pricingData.Attribute44 = this.cell44.ToNullableString();
            this.pricingData.Attribute45 = this.cell45.ToNullableString();
            this.pricingData.Attribute46 = this.cell46.ToNullableString();
            this.pricingData.Attribute47 = this.cell47.ToNullableString();
            this.pricingData.Attribute48 = this.cell48.ToNullableString();
            this.pricingData.Attribute49 = this.cell49.ToNullableString();
            this.pricingData.Attribute50 = this.cell50.ToNullableString();
            this.pricingData.Attribute51 = this.cell51.ToNullableString();
            this.pricingData.Attribute52 = this.cell52.ToNullableString();
            this.pricingData.Attribute53 = this.cell53.ToNullableString();
            this.pricingData.Attribute54 = this.cell54.ToNullableString();
            this.pricingData.Attribute55 = this.cell55.ToNullableString();
            this.pricingData.Attribute56 = this.cell56.ToNullableString();
            this.pricingData.Attribute57 = this.cell57.ToNullableString();
            this.pricingData.Attribute58 = this.cell58.ToNullableString();
            this.pricingData.Attribute59 = this.cell59.ToNullableString();
            this.pricingData.Attribute60 = this.cell60.ToNullableString();
            this.pricingData.Attribute61 = this.cell61.ToNullableString();
            this.pricingData.Attribute62 = this.cell62.ToNullableString();
            this.pricingData.Attribute63 = this.cell63.ToNullableString();
            this.pricingData.Attribute64 = this.cell64.ToNullableString();
            this.pricingData.Attribute65 = this.cell65.ToNullableString();
            this.pricingData.Attribute66 = this.cell66.ToNullableString();
            this.pricingData.Attribute67 = this.cell67.ToNullableString();
            this.pricingData.Attribute68 = this.cell68.ToNullableString();
            this.pricingData.Attribute69 = this.cell69.ToNullableString();
            this.pricingData.Attribute70 = this.cell70.ToNullableString();
            this.pricingData.Attribute71 = this.cell71.ToNullableString();
            this.pricingData.Attribute72 = this.cell72.ToNullableString();
            this.pricingData.Attribute73 = this.cell73.ToNullableString();
            this.pricingData.Attribute74 = this.cell74.ToNullableString();
            this.pricingData.Attribute75 = this.cell75.ToNullableString();
            this.pricingData.Attribute76 = this.cell76.ToNullableString();
            this.pricingData.Attribute77 = this.cell77.ToNullableString();
            this.pricingData.Attribute78 = this.cell78.ToNullableString();
            this.pricingData.Attribute79 = this.cell79.ToNullableString();
            this.pricingData.Attribute80 = this.cell80.ToNullableString();
            this.pricingData.Attribute81 = this.cell81.ToNullableString();
            this.pricingData.Attribute82 = this.cell82.ToNullableString();
            this.pricingData.Attribute83 = this.cell83.ToNullableString();
            this.pricingData.Attribute84 = this.cell84.ToNullableString();
            this.pricingData.Attribute85 = this.cell85.ToNullableString();
            this.pricingData.Attribute86 = this.cell86.ToNullableString();
            this.pricingData.Attribute87 = this.cell87.ToNullableString();
            this.pricingData.Attribute88 = this.cell88.ToNullableString();
            this.pricingData.Attribute89 = this.cell89.ToNullableString();
            this.pricingData.Attribute90 = this.cell90.ToNullableString();
            this.pricingData.Attribute91 = this.cell91.ToNullableString();
            this.pricingData.Attribute92 = this.cell92.ToNullableString();
            this.pricingData.Attribute93 = this.cell93.ToNullableString();
            this.pricingData.Attribute94 = this.cell94.ToNullableString();
            this.pricingData.Attribute95 = this.cell95.ToNullableString();
            this.pricingData.Attribute96 = this.cell96.ToNullableString();
            this.pricingData.Attribute97 = this.cell97.ToNullableString();
            this.pricingData.Attribute98 = this.cell98.ToNullableString();
            this.pricingData.Attribute99 = this.cell99.ToNullableString();
            this.pricingData.Attribute100 = this.cell100.ToNullableString();
            this.pricingData.Attribute101 = this.cell101.ToNullableString();
            this.pricingData.Attribute102 = this.cell102.ToNullableString();
            this.pricingData.Attribute103 = this.cell103.ToNullableString();
            this.pricingData.Attribute104 = this.cell104.ToNullableString();
            this.pricingData.Attribute105 = this.cell105.ToNullableString();
            this.pricingData.Attribute106 = this.cell106.ToNullableString();
            this.pricingData.Attribute107 = this.cell107.ToNullableString();
            this.pricingData.Attribute108 = this.cell108.ToNullableString();
            this.pricingData.Attribute109 = this.cell109.ToNullableString();
            this.pricingData.Attribute110 = this.cell110.ToNullableString();
            this.pricingData.Attribute111 = this.cell111.ToNullableString();
            this.pricingData.Attribute112 = this.cell112.ToNullableString();
            this.pricingData.Attribute113 = this.cell113.ToNullableString();
            this.pricingData.Attribute114 = this.cell114.ToNullableString();
            this.pricingData.Attribute115 = this.cell115.ToNullableString();
            this.pricingData.Attribute116 = this.cell116.ToNullableString();
            this.pricingData.Attribute117 = this.cell117.ToNullableString();
            this.pricingData.Attribute118 = this.cell118.ToNullableString();
            this.pricingData.Attribute119 = this.cell119.ToNullableString();
            this.pricingData.Attribute120 = this.cell120.ToNullableString();
            this.pricingData.Attribute121 = this.cell121.ToNullableString();
            this.pricingData.Attribute122 = this.cell122.ToNullableString();
            this.pricingData.Attribute123 = this.cell123.ToNullableString();
            this.pricingData.Attribute124 = this.cell124.ToNullableString();
            this.pricingData.Attribute125 = this.cell125.ToNullableString();
            this.pricingData.Attribute126 = this.cell126.ToNullableString();
            this.pricingData.Attribute127 = this.cell127.ToNullableString();
            this.pricingData.Attribute128 = this.cell128.ToNullableString();
            this.pricingData.Attribute129 = this.cell129.ToNullableString();
            this.pricingData.Attribute130 = this.cell130.ToNullableString();
            this.pricingData.Attribute131 = this.cell131.ToNullableString();
            this.pricingData.Attribute132 = this.cell132.ToNullableString();
            this.pricingData.Attribute133 = this.cell133.ToNullableString();
            this.pricingData.Attribute134 = this.cell134.ToNullableString();
            this.pricingData.Attribute135 = this.cell135.ToNullableString();
            this.pricingData.Attribute136 = this.cell136.ToNullableString();
            this.pricingData.Attribute137 = this.cell137.ToNullableString();
            this.pricingData.Attribute138 = this.cell138.ToNullableString();
            this.pricingData.Attribute139 = this.cell139.ToNullableString();
            this.pricingData.Attribute140 = this.cell140.ToNullableString();
            this.pricingData.Attribute141 = this.cell141.ToNullableString();
            this.pricingData.Attribute142 = this.cell142.ToNullableString();
            this.pricingData.Attribute143 = this.cell143.ToNullableString();
            this.pricingData.Attribute144 = this.cell144.ToNullableString();
            this.pricingData.Attribute145 = this.cell145.ToNullableString();
            this.pricingData.Attribute146 = this.cell146.ToNullableString();
            this.pricingData.Attribute147 = this.cell147.ToNullableString();
            this.pricingData.Attribute148 = this.cell148.ToNullableString();
            this.pricingData.Attribute149 = this.cell149.ToNullableString();
            this.pricingData.Attribute150 = this.cell150.ToNullableString();
            this.pricingData.Attribute151 = this.cell151.ToNullableString();
            this.pricingData.Attribute152 = this.cell152.ToNullableString();
            this.pricingData.Attribute153 = this.cell153.ToNullableString();
            this.pricingData.Attribute154 = this.cell154.ToNullableString();
            this.pricingData.Attribute155 = this.cell155.ToNullableString();
            this.pricingData.Attribute156 = this.cell156.ToNullableString();
            this.pricingData.Attribute157 = this.cell157.ToNullableString();
            this.pricingData.Attribute158 = this.cell158.ToNullableString();
            this.pricingData.Attribute159 = this.cell159.ToNullableString();
            this.pricingData.Attribute160 = this.cell160.ToNullableString();
            this.pricingData.Attribute161 = this.cell161.ToNullableString();
            this.pricingData.Attribute162 = this.cell162.ToNullableString();
            this.pricingData.Attribute163 = this.cell163.ToNullableString();
            this.pricingData.Attribute164 = this.cell164.ToNullableString();
            this.pricingData.Attribute165 = this.cell165.ToNullableString();
            this.pricingData.Attribute166 = this.cell166.ToNullableString();
            this.pricingData.Attribute167 = this.cell167.ToNullableString();
            this.pricingData.Attribute168 = this.cell168.ToNullableString();
            this.pricingData.Attribute169 = this.cell169.ToNullableString();
            this.pricingData.Attribute170 = this.cell170.ToNullableString();
            this.pricingData.Attribute171 = this.cell171.ToNullableString();
            this.pricingData.Attribute172 = this.cell172.ToNullableString();
            this.pricingData.Attribute173 = this.cell173.ToNullableString();
            this.pricingData.Attribute174 = this.cell174.ToNullableString();
            this.pricingData.Attribute175 = this.cell175.ToNullableString();
            this.pricingData.Attribute176 = this.cell176.ToNullableString();
            this.pricingData.Attribute177 = this.cell177.ToNullableString();
            this.pricingData.Attribute178 = this.cell178.ToNullableString();
            this.pricingData.Attribute179 = this.cell179.ToNullableString();
            this.pricingData.Attribute180 = this.cell180.ToNullableString();
            this.pricingData.Attribute181 = this.cell181.ToNullableString();
            this.pricingData.Attribute182 = this.cell182.ToNullableString();
            this.pricingData.Attribute183 = this.cell183.ToNullableString();
            this.pricingData.Attribute184 = this.cell184.ToNullableString();
            this.pricingData.Attribute185 = this.cell185.ToNullableString();
            this.pricingData.Attribute186 = this.cell186.ToNullableString();
            this.pricingData.Attribute187 = this.cell187.ToNullableString();
            this.pricingData.Attribute188 = this.cell188.ToNullableString();
            this.pricingData.Attribute189 = this.cell189.ToNullableString();
            this.pricingData.Attribute190 = this.cell190.ToNullableString();
            this.pricingData.Attribute191 = this.cell191.ToNullableString();
            this.pricingData.Attribute192 = this.cell192.ToNullableString();
            this.pricingData.Attribute193 = this.cell193.ToNullableString();
            this.pricingData.Attribute194 = this.cell194.ToNullableString();
            this.pricingData.Attribute195 = this.cell195.ToNullableString();
            this.pricingData.Attribute196 = this.cell196.ToNullableString();
            this.pricingData.Attribute197 = this.cell197.ToNullableString();
            this.pricingData.Attribute198 = this.cell198.ToNullableString();
            this.pricingData.Attribute199 = this.cell199.ToNullableString();
            this.pricingData.Attribute200 = this.cell200.ToNullableString();
            this.pricingData.Attribute201 = this.cell201.ToNullableString();
            this.pricingData.Attribute202 = this.cell202.ToNullableString();
            this.pricingData.Attribute203 = this.cell203.ToNullableString();
            this.pricingData.Attribute204 = this.cell204.ToNullableString();
            this.pricingData.Attribute205 = this.cell205.ToNullableString();
            this.pricingData.Attribute206 = this.cell206.ToNullableString();
            this.pricingData.Attribute207 = this.cell207.ToNullableString();
            this.pricingData.Attribute208 = this.cell208.ToNullableString();
            this.pricingData.Attribute209 = this.cell209.ToNullableString();
            this.pricingData.Attribute210 = this.cell210.ToNullableString();
            this.pricingData.Attribute211 = this.cell211.ToNullableString();
            this.pricingData.Attribute212 = this.cell212.ToNullableString();
            this.pricingData.Attribute213 = this.cell213.ToNullableString();
            this.pricingData.Attribute214 = this.cell214.ToNullableString();
            this.pricingData.Attribute215 = this.cell215.ToNullableString();
            this.pricingData.Attribute216 = this.cell216.ToNullableString();
            this.pricingData.Attribute217 = this.cell217.ToNullableString();
            this.pricingData.Attribute218 = this.cell218.ToNullableString();
            this.pricingData.Attribute219 = this.cell219.ToNullableString();
            this.pricingData.Attribute220 = this.cell220.ToNullableString();
            this.pricingData.Attribute221 = this.cell221.ToNullableString();
            this.pricingData.Attribute222 = this.cell222.ToNullableString();
            this.pricingData.Attribute223 = this.cell223.ToNullableString();
            this.pricingData.Attribute224 = this.cell224.ToNullableString();
            this.pricingData.Attribute225 = this.cell225.ToNullableString();

            this.pricingData.Attribute226 = this.cell226.ToNullableString();
            this.pricingData.Attribute227 = this.cell227.ToNullableString();
            this.pricingData.Attribute228 = this.cell228.ToNullableString();
            this.pricingData.Attribute229 = this.cell229.ToNullableString();
            this.pricingData.Attribute230 = this.cell230.ToNullableString();
            this.pricingData.Attribute231 = this.cell231.ToNullableString();
            this.pricingData.Attribute232 = this.cell232.ToNullableString();
            this.pricingData.Attribute233 = this.cell233.ToNullableString();
            this.pricingData.Attribute234 = this.cell234.ToNullableString();
            this.pricingData.Attribute235 = this.cell235.ToNullableString();
            this.pricingData.Attribute236 = this.cell236.ToNullableString();
            this.pricingData.Attribute237 = this.cell237.ToNullableString();
            this.pricingData.Attribute238 = this.cell238.ToNullableString();
            this.pricingData.Attribute239 = this.cell239.ToNullableString();
            this.pricingData.Attribute240 = this.cell240.ToNullableString();
            this.pricingData.Attribute241 = this.cell241.ToNullableString();
            this.pricingData.Attribute242 = this.cell242.ToNullableString();
            this.pricingData.Attribute243 = this.cell243.ToNullableString();
            this.pricingData.Attribute244 = this.cell244.ToNullableString();
            this.pricingData.Attribute245 = this.cell245.ToNullableString();
            this.pricingData.Attribute246 = this.cell246.ToNullableString();
            this.pricingData.Attribute247 = this.cell247.ToNullableString();
            this.pricingData.Attribute248 = this.cell248.ToNullableString();
            this.pricingData.Attribute249 = this.cell249.ToNullableString();
            this.pricingData.Attribute250 = this.cell250.ToNullableString();
            this.pricingData.Attribute251 = this.cell251.ToNullableString();
            this.pricingData.Attribute252 = this.cell252.ToNullableString();
            this.pricingData.Attribute253 = this.cell253.ToNullableString();
            this.pricingData.Attribute254 = this.cell254.ToNullableString();
            this.pricingData.Attribute255 = this.cell255.ToNullableString();
            this.pricingData.Attribute256 = this.cell256.ToNullableString();
            this.pricingData.Attribute257 = this.cell257.ToNullableString();
            this.pricingData.Attribute258 = this.cell258.ToNullableString();
            this.pricingData.Attribute259 = this.cell259.ToNullableString();
            this.pricingData.Attribute260 = this.cell260.ToNullableString();
            this.pricingData.Attribute261 = this.cell261.ToNullableString();
            this.pricingData.Attribute262 = this.cell262.ToNullableString();
            this.pricingData.Attribute263 = this.cell263.ToNullableString();
            this.pricingData.Attribute264 = this.cell264.ToNullableString();
            this.pricingData.Attribute265 = this.cell265.ToNullableString();
            this.pricingData.Attribute266 = this.cell266.ToNullableString();
            this.pricingData.Attribute267 = this.cell267.ToNullableString();
            this.pricingData.Attribute268 = this.cell268.ToNullableString();
            this.pricingData.Attribute269 = this.cell269.ToNullableString();
            this.pricingData.Attribute270 = this.cell270.ToNullableString();
            this.pricingData.Attribute271 = this.cell271.ToNullableString();
            this.pricingData.Attribute272 = this.cell272.ToNullableString();
            this.pricingData.Attribute273 = this.cell273.ToNullableString();
            this.pricingData.Attribute274 = this.cell274.ToNullableString();
            this.pricingData.Attribute275 = this.cell275.ToNullableString();
            this.pricingData.Attribute276 = this.cell276.ToNullableString();
            this.pricingData.Attribute277 = this.cell277.ToNullableString();
            this.pricingData.Attribute278 = this.cell278.ToNullableString();
            this.pricingData.Attribute279 = this.cell279.ToNullableString();


            if (this.DataManager.UpdatePricingMatrix(this.pricingData))
            {
                SystemMessage.Write(MessageType.Priority, Message.PriceAdjustmentsUpdated);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.PriceAdjustmentsNotUpdated);
            }
        }

        protected override void Close()
        {
            Messenger.Default.Unregister(this);
            Messenger.Default.Send(Token.Message, Token.ClosePricingMatrixWindow);
        }

        #endregion

    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="StockAdjustViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Stock
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class StockAdjustViewModel : NouvemViewModelBase
    {
        #region field
        
        /// <summary>
        /// The stock.
        /// </summary>
        private ObservableCollection<StockDetail> stock;

        /// <summary>
        /// The selectedstock.
        /// </summary>
        private StockDetail selectedStock;

        /// <summary>
        /// The stock to update.
        /// </summary>
        private IList<StockDetail> stockToUpdate = new List<StockDetail>();

        /// <summary>
        /// The show all products flag.
        /// </summary>
        private bool showAll;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StockAdjustViewModel"/> class.
        /// </summary>
        public StockAdjustViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.OnLoadingCommand = new RelayCommand(() => this.IsFormLoaded = true);
            this.OnClosingCommand = new RelayCommand(this.Close);
            this.UpdateCommand = new RelayCommand(() =>
            {
                if (this.selectedStock == null)
                {
                    return;
                }

                var localStock =
                    this.stockToUpdate.FirstOrDefault(x => x.ProductID == this.selectedStock.ProductID && x.BatchNumberID == this.selectedStock.BatchNumberID);
                if (localStock == null)
                {
                    this.stockToUpdate.Add(this.selectedStock);
                }

                this.SetControlMode(ControlMode.Update);
            });

            #endregion
         
            this.HasWriteAuthorisation = true;
            this.SetControlMode(ControlMode.OK);
            this.GetStock();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the show all flag.
        /// </summary>
        public bool ShowAll
        {
            get
            {
                return this.showAll;
            }

            set
            {
                this.showAll = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    this.GetStock();
                }
            }
        }

        /// <summary>
        /// Gets or sets the stock.
        /// </summary>
        public ObservableCollection<StockDetail> Stock
        {
            get
            {
                return this.stock;
            }

            set
            {
                this.stock = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the slected stock.
        /// </summary>
        public StockDetail SelectedStock
        {
            get
            {
                return this.selectedStock;
            }

            set
            {
                this.selectedStock = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the load event command handler.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the load event command handler.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the close event command handler.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateStock();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the tsock levels.
        /// </summary>
        private void UpdateStock()
        {
            if (this.stockToUpdate.Any())
            {
                if (this.DataManager.UpdateStock(this.stockToUpdate))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                }
            }

            this.stockToUpdate.Clear();
            this.GetStock();
        }

        /// <summary>
        /// Gets the batch/product stock data.
        /// </summary>
        private void GetStock()
        {
            var allStock = this.DataManager.GetProductStock(NouvemGlobal.InventoryItems).OrderBy(x => x.ProductName);
            if (this.ShowAll)
            {
                this.Stock = new ObservableCollection<StockDetail>(allStock);
            }
            else
            {
                this.Stock = new ObservableCollection<StockDetail>(allStock.Where(x => !x.NotInStock));
            }
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseStockAdjustWindow);
        }
       
        #endregion
    }
}

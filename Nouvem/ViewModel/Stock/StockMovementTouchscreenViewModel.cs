﻿// -----------------------------------------------------------------------
// <copyright file="StockMovementTouchscreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.ViewModel.Attribute;

namespace Nouvem.ViewModel.Stock
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class StockMovementTouchscreenViewModel : AttributesViewModel
    {
        #region field

        /// <summary>
        /// The selected order.
        /// </summary>
        private ProductionData selectedOrder;

        /// <summary>
        /// The selected location.
        /// </summary>
        private Location selectedLocation;

        /// <summary>
        /// The tare wgt.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The qty.
        /// </summary>
        private decimal qty;

        /// <summary>
        /// The incoming manually set weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The splitting stock flag.
        /// </summary>
        private bool splittingStock;

        /// <summary>
        /// Has save been pressed flag.
        /// </summary>
        private bool saveRecorded;

        /// <summary>
        /// Has record been pressed flag.
        /// </summary>
        private bool weightRecorded;

        /// <summary>
        /// Has print barcode been pressed flag.
        /// </summary>
        private bool printRecorded;

        /// <summary>
        /// The process selection disabling flag.
        /// </summary>
        private bool disableProcessSelection;

        /// <summary>
        /// The production details collection.
        /// </summary>
        private ObservableCollection<SaleDetail> productionDetails;

        /// <summary>
        /// The selected product detail.
        /// </summary>
        private SaleDetail selectedProductionDetail;

        /// <summary>
        /// The selected product detail.
        /// </summary>
        private SaleDetail productToSplit;

        /// <summary>
        /// The order spec name.
        /// </summary>
        private string spec;

        /// <summary>
        /// The current batch location.
        /// </summary>
        private string currentLocation;

        /// <summary>
        /// The location to move to
        /// </summary>
        private string moveLocation;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StockMovementTouchscreenViewModel"/> class.
        /// </summary>
        public StockMovementTouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.PostSelectionMacroRefresh, s =>
            {
                if (this.IsFormLoaded)
                {
                    if (this.SelectedProductionDetail != null)
                    {
                        this.SelectedProductionDetail.WeightIntoProduction = s.ToDecimal();
                    }
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.PrintBarcode, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintBarcode();
                }
            });

            // Register for the incoming product selection.
            Messenger.Default.Register<InventoryItem>(this, Token.ProductSelected, o =>
            {
                this.disableProcessSelection = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (this.splittingStock)
                    {
                        this.HandleSplitStock(o);
                    }
                    else
                    {
                        this.HandleProductSelection(o);
                    }
                }));
            });

            // Register for the incoming pieces.
            Messenger.Default.Register<decimal>(this, Token.QuantityUpdate, i =>
            {
                this.qty = i.ToDecimal();
            });

            // Register for the indicator tare weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                this.manualWeight = false;
                this.RecordWeight();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;
                this.RecordWeight();
            });

            Messenger.Default.Register<Location>(this, Token.LocationSelected, location =>
            {
                this.selectedLocation = location;
                if (this.selectedLocation.UsingLocationsForBatches)
                {
                    this.Locator.TouchscreenProductionOrders.SetModule(ViewType.StockMovement);
                    this.Locator.TouchscreenProductionOrders.GetAllOrdersByLocation(this.selectedLocation.WarehouseId);
                    var clear = ApplicationSettings.ClearProductionOrdersOnEntry;
                    ApplicationSettings.ClearProductionOrdersOnEntry = false;
                    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                    ApplicationSettings.ClearProductionOrdersOnEntry = clear;
                    return;
                }

                this.MoveLocation = location.FullName;
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.HandleScannerData(s);
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (this.IsFormLoaded && !string.IsNullOrWhiteSpace(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // Register for the label change name.
            Messenger.Default.Register<Model.DataLayer.Process>(this, Token.ProcessSelected, i =>
            {
                var checkNotMade = this.DataManager.GetProcessStartUpChecks(i.ProcessID);
                if (checkNotMade.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.MissingProcessStartUpCheckMessage = string.Format(Message.HACCPNotCompleted, i.Name.Trim(), checkNotMade.First());
                        SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                        NouvemMessageBox.Show(this.MissingProcessStartUpCheckMessage, touchScreen: true);
                    }));

                    this.DisableModule = true;
                    return;
                }

                this.DisableModule = false;

                this.SetStockMovementMode(i);
                if (this.Locator.ProcessSelection.SaveAsDefaultProcess)
                {
                    this.DataManager.SetDefaultModuleProcess(ViewType.StockMovement, ApplicationSettings.StockMovementMode.ToString(), NouvemGlobal.DeviceId.ToInt());
                }

                // Trigger the attributes for the new process selection.
                this.SelectedProductionDetail = null;
                if (this.ProductionDetails != null)
                {
                    this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault();
                }
            });

            Messenger.Default.Register<ProductionData>(this, Token.ProductionBatchSelected, p =>
            {
                //this.ClearAttributes();
                this.ClearAttributeControls();

                if (this.SelectedOrder == null || this.SelectedOrder.Order.PROrderID != p.Order.PROrderID)
                {
                    this.ResetFlags();
                }

                this.SelectedOrder = p;

                if (this.selectedOrder != null)
                {
                    //this.selectedOrder.LastBatchAttribute = this.DataManager.GetLastBatchAttributeData(this.selectedOrder.Order.BatchNumberID,
                    //    NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.TransactionTypeProductionIssueId);
                    //this.selectedOrder.ProductionAttributesSet = false;

                    //var data = this.DataManager.GetBatchStockLocation(this.selectedOrder.Order.PROrderID);

                    this.SelectedOrder.WarehouseId = p.Order.WarehouseID.ToInt();
                    this.SelectedOrder.SubLocationId = p.Order.WarehouseLocationID;

                    if (this.SelectedOrder.WarehouseId > 0)
                    {
                        var warehouse = NouvemGlobal.WarehouseLocations.FirstOrDefault(x => x.WarehouseID == this.SelectedOrder.WarehouseId);
                        if (warehouse != null)
                        {
                            var sub = string.Empty;
                            if (this.SelectedOrder.SubLocationId.HasValue)
                            {
                                var subLocation = this.DataManager.GetSubLocation(this.SelectedOrder.SubLocationId.ToInt());
                                if (subLocation != null)
                                {
                                    sub = subLocation.Name;
                                }
                            }

                            if (sub != string.Empty)
                            {
                                this.CurrentLocation = string.Format("{0} - {1}", warehouse.Name, sub);
                            }
                            else
                            {
                                this.CurrentLocation = warehouse.Name;
                            }
                        }
                        else
                        {
                            this.CurrentLocation = string.Empty;
                        }
                    }
                    else
                    {
                        this.CurrentLocation = string.Empty;
                    }
                }

                if (this.ProductionDetails != null)
                {
                    if (this.ProductionDetails.Count == 1)
                    {
                        this.SelectedProductionDetail = this.ProductionDetails.FirstOrDefault();
                    }
                    else
                    {
                        this.SelectedProductionDetail = null;
                    }
                }
            });

            #endregion

            #region command handler


            this.MoveAllBatchesCommand = new RelayCommand(this.MoveAllBatchesCommandExecute);
            this.SaveAllBatchesCommand = new RelayCommand(this.SaveAllBatchAttributes);
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);
            this.OnLoadedCommand = new RelayCommand(this.OnLoadedCommandExecute);
            this.OnUnloadedCommand = new RelayCommand(this.OnUnloadedCommandExecute);
            this.MoveBatchCommand = new RelayCommand(this.MoveBatchCommandExecute);
            this.SplitBatchCommand = new RelayCommand(this.SplitBatchCommandExecute);

            #endregion

            this.GetAlertUsers();
            this.GetAttributeLookUps();
            this.GetAttributeAllocationData();
            this.ClearAttributes();
            this.ClearAttributeControls();
            this.GetDateDays();
            this.Spec = Message.SelectBatch;
            this.Locator.ProcessSelection.SetDefaultProcesses(ApplicationSettings.DefaultStockMovementModes, ApplicationSettings.StockMovementMode.ToString());
            this.CloseHiddenWindows();
            this.IndicatorManager.IgnoreValidation(true);
            if (ApplicationSettings.AlwaysShowAttributes)
            {
                this.Locator.Touchscreen.ShowAttributes = true;
            }
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the sale detail.
        /// </summary>
        public SaleDetail SelectedProductionDetail
        {
            get
            {
                return this.selectedProductionDetail;
            }

            set
            {
                this.selectedProductionDetail = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSaleDetail(this.SelectedProductionDetail, this);

                    //if (this.SelectedOrder != null && !this.SelectedOrder.ProductionAttributesSet && this.SelectedOrder.LastBatchAttribute != null)
                    //{
                    //    this.SetAttributeValues(this.SelectedOrder.LastBatchAttribute);
                    //    this.HandleSaleDetailForTransaction(value, this);
                    //    this.SelectedOrder.ProductionAttributesSet = true;
                    //}

                    this.Locator.Touchscreen.Quantity = value.QuantityIntoProduction.ToDecimal();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected order.
        /// </summary>
        public ProductionData SelectedOrder
        {
            get
            {
                return this.selectedOrder;
            }

            set
            {
                this.selectedOrder = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard)
                    {
                        this.Spec = value.Spec;
                    }
                    else
                    {
                        this.Spec = value.Reference;
                    }

                    if (value.ProductionDetails != null)
                    {
                        this.ProductionDetails = new ObservableCollection<SaleDetail>(value.ProductionDetails);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the production details.
        /// </summary>
        public ObservableCollection<SaleDetail> ProductionDetails
        {
            get
            {
                return this.productionDetails;
            }

            set
            {
                this.productionDetails = value;
                this.RaisePropertyChanged();

                foreach (var saleDetail in value)
                {
                    // shouldn't happen..but just in case
                    if (saleDetail.InventoryItem == null)
                    {
                        saleDetail.InventoryItem =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == saleDetail.INMasterID);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the spec name.
        /// </summary>
        public string Spec
        {
            get
            {
                return this.spec;
            }

            set
            {
                this.spec = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current location of the batch.
        /// </summary>
        public string CurrentLocation
        {
            get
            {
                return this.currentLocation;
            }

            set
            {
                this.currentLocation = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the location to move the batch to.
        /// </summary>
        public string MoveLocation
        {
            get
            {
                return this.moveLocation;
            }

            set
            {
                this.moveLocation = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a scanner move back to main ui request.
        /// </summary>
        public ICommand MoveAllBatchesCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a scanner move back to main ui request.
        /// </summary>
        public ICommand SaveAllBatchesCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a scanner manual scan.
        /// </summary>
        public ICommand SplitBatchCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a scanner move back to main ui request.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a scanner manual scan.
        /// </summary>
        public ICommand ManualScanCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unloaded event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unloaded event.
        /// </summary>
        public ICommand MoveBatchCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int GetWorkflowDocketId()
        {
            return this.SelectedOrder != null && this.SelectedOrder.Order != null ? this.SelectedOrder.Order.PROrderID : 0;
        }

        /// <summary>
        /// Gets the current attribute product id.
        /// </summary>
        /// <returns>THe corresponding product id.</returns>
        protected override int GetWorkflowProductId()
        {
            return this.SelectedProductionDetail != null ? this.SelectedProductionDetail.INMasterID : 0;
        }

        /// <summary>
        /// Show the batch report.
        /// </summary>
        protected override void ShowReportCommandExecute()
        {
            if (!this.IsFormLoaded || this.SelectedOrder == null)
            {
                return;
            }

            var reportData = new Sale
            {
                Reference = this.SelectedOrder.Order.PROrderID.ToString()
            };

            Messenger.Default.Send(reportData, Token.DisplayTouchscreenBatchReport);
        }

        /// <summary>
        /// Saves the batch attributes.
        /// </summary>
        protected override void SaveBatchAttributes(int? parentId = null, bool ignoreChecks = false)
        {
            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            if (!ignoreChecks)
            {
                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);
                    return;
                }
            }

            #endregion

            this.UnsavedAttributes = false;
            var attributeId = this.SaveBatchData(this.SelectedOrder.Order.BatchNumberID);
            if (attributeId > 0)
            {
                if (this.NonStandardBatchResponses != null && this.NonStandardBatchResponses.Any() && this.SelectedOrder != null && this.SelectedOrder.Order.PROrderID > 0)
                {
                    foreach (var response in this.NonStandardBatchResponses)
                    {
                        this.LogAlerts(response, prOrderID: this.SelectedOrder.Order.PROrderID, processId: this.Locator.ProcessSelection.SelectedProcess.ProcessID);
                    }
                }

                base.SaveBatchAttributes(attributeId);
                SystemMessage.Write(MessageType.Priority, Message.BatchAttributesSaved);

                if (!ignoreChecks)
                {
                    NouvemMessageBox.Show(Message.BatchAttributesSaved, touchScreen: true, flashMessage: true);
                }

                this.saveRecorded = true;
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchAttributesNotSaved);
            }
        }

        /// <summary>
        /// Sets the process flag (visibility/edit)
        /// </summary>
        /// <param name="data">The attribute allocation data.</param>
        protected override Tuple<bool, bool> SetProcessData(AttributeAllocationData data)
        {
            var visible = false;
            var editable = false;
            if (ApplicationSettings.StockMovementMode == StockMovementMode.LowRiskCook)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Cookers));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Cookers));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.Smokers)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Smokers));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Smokers));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.HighRiskCook)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Chillers));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Chillers));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.Packing)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Packing));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Packing));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.HighRiskJoints)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.HighRiskJoints));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.HighRiskJoints));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.HighRiskSlicing)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.HighRiskSlicing));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.HighRiskSlicing));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.SplitBatch)
            {
                visible = data.AttributeVisibleProcesses != null &&
                     data.AttributeVisibleProcesses.Any(
                         x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.SplitBatch));

                editable = data.AttributeEditProcesses != null &&
                          data.AttributeEditProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.SplitBatch));
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.Coldstores)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Coldstores));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCase(ProcessType.Coldstores));
            }

            return Tuple.Create(visible, editable);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the stock aplit.
        /// </summary>
        private void SplitBatchCommandExecute()
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                return;
            }

            if (this.productionDetails == null || !this.productionDetails.Any())
            {
                return;
            }

            if (ApplicationSettings.StockMovementMode != StockMovementMode.SplitBatch)
            {
                SystemMessage.Write(MessageType.Issue, Message.NotInSplitBatchMode);
                NouvemMessageBox.Show(Message.NotInSplitBatchMode, touchScreen: true);
                return;
            }

            if (this.selectedProductionDetail == null && this.productionDetails.Count > 1)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBatchLineSelected);
                NouvemMessageBox.Show(Message.NoBatchLineSelected, touchScreen: true);
                return;
            }

            if (this.selectedProductionDetail == null)
            {
                this.productToSplit = this.ProductionDetails.First();
            }
            else
            {
                this.productToSplit = this.selectedProductionDetail;
            }

            #endregion

            this.SwitchViewCommandExecute(Constant.Products);
            this.splittingStock = true;
        }

        /// <summary>
        /// Handles the stock movement.
        /// </summary>
        private void MoveBatchCommandExecute()
        {
            var splittingBatch = false;
            var movingAllItemsInBatch = false;

            #region validation

            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            if (this.SelectedOrder == null)
            {
                return;
            }

            if (this.ProductionDetails == null || !this.ProductionDetails.Any())
            {
                return;
            }

            if (this.selectedLocation == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoMoveLocationSelected);
                return;
            }

            if (ApplicationSettings.StockMovementMode == StockMovementMode.LowRiskCook)
            {
                var missingActions = this.CheckMoveProcesses();
                if (missingActions != string.Empty)
                {
                    NouvemMessageBox.Show(string.Format(Message.LowRiskCookActionsMissing, missingActions), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }
                }
            }

            #region attribute validation

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            this.CheckForUnsavedAttributes();

            #endregion

            if (this.selectedLocation.WarehouseLocationId > 0 && this.DataManager.IsAnotherBatchInLocation(this.SelectedOrder.Order.BatchNumberID, this.selectedLocation))
            {
                // warn the user
                NouvemMessageBox.Show(string.Format(Message.AnotherBatchInLocation, this.selectedLocation.FullName), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }
            }

            if (this.SelectedProductionDetail == null)
            {
                if (this.ProductionDetails.Count == 1)
                {
                    this.SelectedProductionDetail = this.ProductionDetails.First();
                }
            }

            if (this.SelectedProductionDetail != null && this.ProductionDetails.Count > 1)
            {
                splittingBatch = true;
                NouvemMessageBox.Show(string.Format(Message.MovingSplitBatchPrompt, this.selectedLocation.FullName), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }
            }

            if (this.selectedLocation.PrimaryWarehouseName.ContainsIgnoringCase(Constant.Cooker))
            {
                if (this.selectedLocation.WarehouseLocationId > 0)
                {
                    var data = this.DataManager.IsLighterBatchInCooker(this.selectedProductionDetail.WeightIntoProduction.ToDecimal(),
                        this.selectedLocation);

                    if (data != null)
                    {
                        // warn the user
                        NouvemMessageBox.Show(string.Format(Message.PrecedingCookerRackWgtLighter, data.Item1, data.Item2, data.Item3), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }
                }
            }

            #endregion

            var localReference = this.SelectedOrder.Order.Reference;
            bool result;
            this.selectedLocation.ProcessId =
                    this.Locator.ProcessSelection.SelectedProcess != null
                        ? this.Locator.ProcessSelection.SelectedProcess.ProcessID
                        : (int?)null;

            try
            {
                if (!this.ValidateData(
                    this.traceabilityDataMoves,
                    this.SelectedOrder.Order.PROrderID,
                    0, 0,
                    this.SelectedProductionDetail.INMasterID,
                    this.selectedLocation.WarehouseId,
                    this.selectedLocation.ProcessId.ToInt(),
                    Constant.Batch,
                    this.selectedLocation.WarehouseLocationId.ToInt().ToString()))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            if (splittingBatch)
            {
                var localId = this.SelectedOrder.Order.PROrderID;
                this.selectedLocation.StockMoveMode = ApplicationSettings.StockMovementMode;
                result = this.DataManager.MoveSplitBatchStock(this.SelectedOrder.Order,
                    this.SelectedProductionDetail, this.selectedLocation);

                if (result)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataManager.UpdateBatchWeight(localId);
                    }));
                }
            }
            else
            {
                result = this.DataManager.MoveBatchStock(this.SelectedOrder.Order.PROrderID, this.selectedLocation);
            }

            if (result)
            {
                var message = string.Format(Message.BatchStockMoved, localReference,
                    this.selectedLocation.FullName);
                if (splittingBatch)
                {
                    message = string.Format(Message.BatchSplitStockMoved, localReference, this.SelectedOrder.Order.Reference,
                     this.selectedLocation.FullName);
                }

                SystemMessage.Write(MessageType.Priority, message);
                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);

                this.Refresh(this.SelectedOrder.Order.PROrderID);
                this.MoveLocation = Strings.SelectBatchLocation;
                this.CurrentLocation = this.selectedLocation.FullName;
                this.selectedLocation = null;
                this.ResetFlags();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataManager.UpdateBatchWeight(this.SelectedOrder.Order.PROrderID);
                }));
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchStockNotMoved);
            }
        }

        /// <summary>
        /// Handles the ui load event.
        /// </summary>
        private void OnLoadedCommandExecute()
        {
            ViewModelLocator.ClearARReturnTouchscreen();
            ViewModelLocator.ClearAPReceiptTouchscreen();
            ViewModelLocator.ClearIntoProduction();
            ViewModelLocator.ClearOutOfProduction();
            ViewModelLocator.ClearARDispatchTouchscreen();
            this.IsBusy = false;
            this.IsFormLoaded = true;
            this.Locator.FactoryScreen.ModuleName = Strings.StockMovement;
            this.ScannerStockMode = ScannerMode.Scan;
            this.OpenScanner();

            if (!this.disableProcessSelection)
            {
                this.Locator.Touchscreen.DisplayProcessSelection();
            }

            this.MoveLocation = Strings.SelectBatchLocation;
            this.SetUpTimer();
            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.StockMovement);
            this.disableProcessSelection = false;
        }

        /// <summary>
        /// Handles the ui unload event.
        /// </summary>
        private void OnUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
            this.Timer.Tick -= this.TimerOnTick;
        }

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private void SwitchViewCommandExecute(string view)
        {
            this.IsBusy = true;
            this.splittingStock = false;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view.Equals(Constant.Order))
                    {
                        this.Locator.TouchscreenWarehouse.UsingLocationsForBatches = true;
                        if (!this.WareHouseScreenCreated)
                        {
                            this.WareHouseScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenWarehouse);
                            return;
                        }

                        this.Locator.TouchscreenWarehouse.OnEntry();
                        Messenger.Default.Send(false, Token.CloseTouchscreenWarehouseWindow);
                        return;
                    }

                    if (view.Equals(Constant.Location))
                    {
                        if (!this.WareHouseScreenCreated)
                        {
                            this.WareHouseScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenWarehouse);
                            return;
                        }

                        this.Locator.TouchscreenWarehouse.UsingLocationsForBatches = false;
                        this.Locator.TouchscreenWarehouse.OnEntry();
                        Messenger.Default.Send(false, Token.CloseTouchscreenWarehouseWindow);
                        return;
                    }

                    if (view.Equals(Constant.Products))
                    {
                        this.Locator.TouchscreenProducts.Module = ViewType.IntoProduction;
                        this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenProducts;
                        return;
                    }
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        /// <summary>
        /// Moves all the batches.
        /// </summary>
        private void MoveAllBatchesCommandExecute()
        {
            #region validation

            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            if (this.SelectedOrder == null)
            {
                return;
            }

            if (this.ProductionDetails == null || !this.ProductionDetails.Any())
            {
                return;
            }

            if (this.selectedLocation == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoMoveLocationSelected);
                return;
            }

            var fromLocation = new Location
            {
                WarehouseId = this.SelectedOrder.WarehouseId,
                WarehouseLocationId = this.SelectedOrder.SubLocationId
            };

            NouvemMessageBox.Show(string.Format(Message.MoveAllBatchesPrompt, this.SelectedOrder.Warehouse, this.selectedLocation.FullName), NouvemMessageBoxButtons.YesNo, touchScreen: true);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            if (this.selectedLocation.WarehouseLocationId > 0 && this.DataManager.IsAnotherBatchInLocation(this.SelectedOrder.Order.BatchNumberID, this.selectedLocation))
            {
                // warn the user
                NouvemMessageBox.Show(string.Format(Message.AnotherBatchInLocation, this.selectedLocation.FullName), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }
            }

            #endregion

            this.selectedLocation.ProcessId =
                    this.Locator.ProcessSelection.SelectedProcess != null
                        ? this.Locator.ProcessSelection.SelectedProcess.ProcessID
                        : (int?)null;
            var result = this.DataManager.MoveAllBatchStock(fromLocation, this.selectedLocation);

            if (result)
            {
                var message = string.Format(Message.AllBatchesMoved, this.CurrentLocation,
                    this.selectedLocation.FullName);

                SystemMessage.Write(MessageType.Priority, message);
                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);

                this.Refresh(this.SelectedOrder.Order.PROrderID);
                this.MoveLocation = Strings.SelectBatchLocation;
                this.CurrentLocation = this.selectedLocation.FullName;
                this.selectedLocation = null;
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchStockNotMoved);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Saves the batch attributes.
        /// </summary>
        private void SaveAllBatchAttributes()
        {
            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderHasBeenSelected);
                return;
            }

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            NouvemMessageBox.Show(string.Format(Message.UpdatingAllBatchesInLocationPrompt, this.SelectedOrder.Warehouse), NouvemMessageBoxButtons.YesNo, touchScreen: true);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            #endregion

            var localLocation = new Location
            {
                WarehouseId = this.SelectedOrder.WarehouseId,
                WarehouseLocationId = this.SelectedOrder.SubLocationId
            };

            var batchIds = this.DataManager.GetBatchesInLocation(localLocation);

            var stockDetail = new StockDetail { BatchNumberID = this.SelectedOrder.Order.BatchNumberID };
            this.CreateAttribute(stockDetail);

            if (this.DataManager.UpdateAllBatchAttributes(stockDetail, batchIds))
            {
                SystemMessage.Write(MessageType.Priority, Message.BatchAttributesSaved);
                NouvemMessageBox.Show(Message.BatchAttributesSaved, touchScreen: true, flashMessage: true);
                this.saveRecorded = true;
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchAttributesNotSaved);
            }
        }

        /// <summary>
        /// Reset the processes.
        /// </summary>
        private void ResetFlags()
        {
            this.saveRecorded = false;
            this.weightRecorded = false;
            this.printRecorded = false;
        }

        /// <summary>
        /// Checks that the stipulated move batch actions have been taken.
        /// </summary>
        /// <returns>Empty string if ok, else the missing actions.</returns>
        private string CheckMoveProcesses()
        {
            var result = string.Empty;

            if (!this.saveRecorded)
            {
                result = Strings.SaveAttributes;
            }

            //if (!this.weightRecorded)
            //{
            //    if (result == string.Empty)
            //    {
            //        result = Strings.RecordWeight;
            //    }
            //    else
            //    {
            //        result = string.Format("{0},{1}", result, Strings.RecordWeight);
            //    }
            //}

            if (!this.printRecorded)
            {
                if (result == string.Empty)
                {
                    result = Strings.PrintBarcode;
                }
                else
                {
                    result = string.Format("{0},{1}", result, Strings.PrintBarcode);
                }
            }

            return result;
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        private void HandleScannerData(string data)
        {
            #region validation

            if (string.IsNullOrEmpty(data))
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);
                return;
            }

            if (ViewModelLocator.IsProductionOrdersLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.ProductionOrderSearch);
                return;
            }

            if (ViewModelLocator.IsTransactionManagerLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                return;
            }

            #endregion

            this.ReferenceValue = data;
            var localData = data.RemoveNonDecimals().ToInt();
            this.Log.LogDebug(this.GetType(), string.Format("HandleScannerData(): {0}", localData));

            var trans = this.DataManager.GetStockTransactionIncDeleted(localData);
            if (trans == null)
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.BatchNotFound, localData));
                return;
            }

            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.StockMovement);
            this.Locator.TouchscreenProductionOrders.GetAllOrdersOnly();
            var localBatch = this.Locator.TouchscreenProductionOrders.AllOrders.FirstOrDefault(
                x => x.Order.PROrderID == trans.MasterTableID);

            if (localBatch == null)
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.BatchNotFound, data));
                NouvemMessageBox.Show(string.Format(Message.BatchNotFound, data), touchScreen: true);
                return;
            }

            this.Locator.TouchscreenProductionOrders.SelectedOrder = localBatch;
            this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
        }

        /// <summary>
        /// Refreshes the batch.
        /// </summary>
        private void Refresh(int prOrderId)
        {
            this.Locator.TouchscreenProductionOrders.SetModule(ViewType.StockMovement);
            this.Locator.TouchscreenProductionOrders.GetAllOrdersOnly();
            var localBatch = this.Locator.TouchscreenProductionOrders.AllOrders.FirstOrDefault(
                x => x.Order.PROrderID == prOrderId);

            this.Locator.TouchscreenProductionOrders.SelectedOrder = localBatch;
            this.Locator.TouchscreenProductionOrders.HandleSelectedStockOrder();
        }

        /// <summary>
        /// Sets the mode.
        /// </summary>
        /// <param name="process">The incoming process selection.</param>
        private void SetStockMovementMode(Nouvem.Model.DataLayer.Process process)
        {
            try
            {
                ApplicationSettings.StockMovementMode =
                    (StockMovementMode)Enum.Parse(typeof(StockMovementMode), process.Name.Replace(" ", ""));
                this.GetAttributeAllocationApplicationData(process.ProcessID);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("SetIntoProductionMode: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordWeight()
        {
            this.IndicatorManager.OpenIndicatorPort();

            try
            {
                #region validation

                if (this.DisableModule)
                {
                    SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                    return;
                }

                if (this.SelectedOrder == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBatchSelected);
                    return;
                }

                if (this.ProductionDetails == null || !this.ProductionDetails.Any())
                {
                    return;
                }

                if (this.selectedProductionDetail == null && this.productionDetails.Count > 1)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBatchLineSelected);
                    NouvemMessageBox.Show(Message.NoBatchSelected, touchScreen: true);
                    return;
                }

                if (this.selectedProductionDetail == null)
                {
                    this.selectedProductionDetail = this.ProductionDetails.First();
                }

                #region attribute validation

                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);
                    return;
                }

                this.CheckForUnsavedAttributes();

                #endregion

                #endregion

                if (this.splittingStock)
                {
                    this.SplitStock();
                    return;
                }

                if (!this.manualWeight)
                {
                    var saveWeightResult = this.IndicatorManager.SaveWeight();
                    if (saveWeightResult == string.Empty)
                    {
                        this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                        this.manualWeight = false;
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, saveWeightResult);
                        NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                        return;
                    }
                }

                NouvemMessageBox.Show(Message.ChangeWeightDataPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    return;
                }

                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataTransactions,
                        this.SelectedOrder.Order.PROrderID,
                        this.indicatorWeight, this.qty,
                        this.SelectedProductionDetail.INMasterID,
                        this.selectedLocation.WarehouseId,
                        this.selectedLocation.ProcessId.ToInt(),
                        Constant.Batch, this.ReferenceValue))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                if (this.DataManager.ChangeBatchData(
                    this.SelectedOrder.Order.PROrderID,
                    this.indicatorWeight,
                    this.qty == 0 ? 1 : this.qty,
                    this.SelectedProductionDetail.INMasterID))
                {
                    var message = Message.BatchDataUpdated;
                    SystemMessage.Write(MessageType.Priority, message);
                    NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    this.Refresh(this.SelectedOrder.Order.PROrderID);
                    this.PrintBarcode();
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataManager.UpdateBatchWeight(this.SelectedOrder.Order.PROrderID);
                    }));
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.BatchDataNotUpdated);
                }

                this.weightRecorded = true;
            }
            finally
            {
                this.Locator.Indicator.Weight = 0;
                this.IndicatorManager.OpenIndicatorPort();
            }
        }

        /// <summary>
        /// Handle the splitting of stock.
        /// </summary>
        private void SplitStock()
        {
            this.IndicatorManager.OpenIndicatorPort();

            #region validation

            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            if (this.SelectedOrder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBatchSelected);
                return;
            }

            if (this.ProductionDetails == null || !this.ProductionDetails.Any())
            {
                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }

            this.splittingStock = false;
            this.SelectedProductionDetail.TransactionWeight = this.indicatorWeight.ToDecimal();
            this.SelectedProductionDetail.TransactionQty = this.qty.ToDecimal();

            if (this.ProductionDetails.First().WeightIntoProduction <
                this.selectedProductionDetail.TransactionWeight
                ||
                this.ProductionDetails.First().QuantityIntoProduction < this.selectedProductionDetail.TransactionQty)
            {
                NouvemMessageBox.Show(Message.SplitBatchAmtWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                {
                    this.ProductionDetails.Remove(this.SelectedProductionDetail);
                    return;
                }
            }

            var msg = string.Format(Message.SplitStockPrompt, this.productToSplit.InventoryItem.Name,
                this.selectedProductionDetail.InventoryItem.Name, this.indicatorWeight, this.qty);
            NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo, touchScreen: true);
            if (NouvemMessageBox.UserSelection == UserDialogue.No)
            {
                this.ProductionDetails.Remove(this.SelectedProductionDetail);
                return;
            }

            if (this.DataManager.SplitBatchStock(
                this.SelectedOrder.Order.PROrderID,
                this.productToSplit,
                this.selectedProductionDetail))
            {
                var message = Message.StockSplit;
                SystemMessage.Write(MessageType.Priority, message);
                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                this.Refresh(this.SelectedOrder.Order.PROrderID);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataManager.UpdateBatchWeight(this.SelectedOrder.Order.PROrderID);
                }));
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.StockNotSplit);
            }
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.ScannerStockMode = ScannerMode.Scan;
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += this.TimerOnTick;
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            this.DontChangeMode = true;
            this.Timer.Stop();
            Global.Keypad.Show(KeypadTarget.ManualSerial);
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void HandleProductSelection(InventoryItem product)
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                return;
            }

            if (this.ProductionDetails == null || !this.ProductionDetails.Any())
            {
                return;
            }

            if (this.selectedProductionDetail == null && this.productionDetails.Count > 1)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBatchLineSelected);
                NouvemMessageBox.Show(Message.NoBatchSelected, touchScreen: true);
                return;
            }

            if (this.selectedProductionDetail == null)
            {
                this.selectedProductionDetail = this.ProductionDetails.First();
            }

            #endregion

            NouvemMessageBox.Show(Message.ChangeProductPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true);
            if (NouvemMessageBox.UserSelection == UserDialogue.No)
            {
                return;
            }

            if (this.DataManager.ChangeBatchProduct(
                this.SelectedOrder.Order.PROrderID,
                this.selectedProductionDetail.INMasterID,
                product.Master.INMasterID))
            {
                var message = Message.BatchDataUpdated;
                SystemMessage.Write(MessageType.Priority, message);
                NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                this.Refresh(this.SelectedOrder.Order.PROrderID);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchDataNotUpdated);
            }
        }

        /// <summary>
        /// Handle the splitting of stock
        /// </summary>
        private void HandleSplitStock(InventoryItem product)
        {
            var localSaleDetail = new SaleDetail
            {
                InventoryItem = product,
                INMasterID = product.Master.INMasterID,
                NouStockMode = product.StockMode,
                WeightOrdered = 0,
                WeightReceived = 0,
                QuantityOrdered = 0,
                QuantityReceived = 0,
                WeightOutstanding = 0,
                QuantityOutstanding = 0
            };

            var localWgt = 0M;
            if (this.Locator.Indicator.ManualWeight)
            {
                localWgt = this.Locator.Indicator.Weight;
            }

            this.Locator.Touchscreen.Quantity = 1;

            if (localWgt > 0)
            {
                this.Locator.Indicator.Weight = localWgt;
            }

            this.ProductionDetails.Add(localSaleDetail);
            this.SelectedProductionDetail = localSaleDetail;

            //var localDetail = this.ProductionDetails.FirstOrDefault(x => x.INMasterID == product.Master.INMasterID);
            //if (localDetail == null)
            //{
            //    this.ProductionDetails.Add(localSaleDetail);
            //    this.SelectedProductionDetail = localSaleDetail;
            //}
            //else
            //{
            //    this.SelectedProductionDetail = localDetail;
            //}
        }

        /// <summary>
        /// Prints a batch barcode.
        /// </summary>
        private void PrintBarcode()
        {
            #region validation

            if (this.SelectedOrder == null)
            {
                return;
            }

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            try
            {
                if (!this.ValidateData(this.traceabilityDataLabel, this.SelectedOrder.Order.PROrderID,
                    0, 0, 0,
                    NouvemGlobal.ProductionId, this.Locator.ProcessSelection.SelectedProcess.ProcessID, Constant.Batch))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            var productId = NouvemGlobal.InventoryItems.First().Master.INMasterID;
            if (this.ProductionDetails != null && this.ProductionDetails.Any())
            {
                productId = this.ProductionDetails.Last().INMasterID;
            }

            this.printRecorded = true;

            // fetch the traceability values
            var stockTransaction = new StockTransaction();
            stockTransaction.TransactionWeight = 0;
            stockTransaction.GrossWeight = 0;
            stockTransaction.Tare = 0;
            stockTransaction.MasterTableID = this.SelectedOrder.Order.PROrderID;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = 0;
            stockTransaction.BatchNumberID = this.SelectedOrder.Order.BatchNumberID;
            stockTransaction.BatchID_Base = this.BatchIdBase;
            stockTransaction.INMasterID = productId;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeStockMoveId.ToInt();
            stockTransaction.WarehouseID = NouvemGlobal.ProductionId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;
            stockTransaction.Deleted = DateTime.Now;
            stockTransaction.Consumed = DateTime.Now;
            stockTransaction.Reference = -1;
            stockTransaction.Comments = ApplicationSettings.StockMovementMode.ToString();

            var id = this.DataManager.AddTransaction(stockTransaction);
            if (id > 0)
            {
                this.Print(id, LabelType.Shipping, this.Locator.ProcessSelection.SelectedProcess, batchLabel:true);
                SystemMessage.Write(MessageType.Priority, Message.BatchBarcodePrinted);
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The newly created stock transaction id.</param>
        protected void Print(int stockTransactionId, LabelType labelType = LabelType.Item, Process process = null, bool batchLabel = false)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Print(): Attempting to print Stocktransactionid:{0}", stockTransactionId));

            #region validation

            if (stockTransactionId == 0)
            {
                return;
            }

            #endregion

            try
            {
                var labels = this.PrintManager.ProcessPrinting(null, null, null, null, ViewType.StockMovement, labelType, stockTransactionId, 
                    ApplicationSettings.GoodsInLabelsToPrint, process, batchLabel:batchLabel);
                if (labels.Item1.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.DataManager.AddLabelToTransaction(stockTransactionId,
                            string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                    }), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        #endregion

        #endregion
    }
}



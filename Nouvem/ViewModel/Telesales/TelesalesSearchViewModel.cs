﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowAlertsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Telesales
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.ViewModel.Sales;

    public class TelesalesSearchViewModel : SalesSearchDataViewModel
    {
        #region field       

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TelesalesSearchViewModel"/> class.
        /// </summary>
        public TelesalesSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command registration            

            #endregion

            #region instantiation

            #endregion

            if (ApplicationSettings.WorkflowSearchFromDate < DateTime.Today.AddYears(-100))
            {
                this.FromDate = DateTime.Today;
            }
            else
            {
                this.FromDate = ApplicationSettings.WorkflowSearchFromDate;
            }

            this.ShowAllOrders = ApplicationSettings.WorkflowSearchShowAllOrders;

            this.ToDate = DateTime.Today;
            this.NouDocStatus = new List<NouDocStatu>
            {
                NouvemGlobal.NouDocStatusActive, 
                NouvemGlobal.NouDocStatusInProgress,
                NouvemGlobal.NouDocStatusComplete, 
                NouvemGlobal.NouDocStatusCancelled
            };

            this.GetWorkflows();
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the doc statuses.
        /// </summary>
        public IList<NouDocStatu> NouDocStatus { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommmand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.Close();
                    break;               
            }
        }

        /// <summary>
        /// Abstract cancel selection command.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale == null)
            {
                return;
            }

            var workflow = this.DataManager.GetTelesaleById(this.SelectedSale.SaleID);
           
            if (!this.KeepVisible)
            {
                this.Close();
            }

            Messenger.Default.Send(this.SelectedSale, Token.CreateTelesale);
        }

        /// <summary>
        /// Refreshes the lairages.
        /// </summary>
        protected override void Refresh()
        {
            if (this.IsFormLoaded)
            {
                this.GetWorkflows();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }       

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
            ViewModelLocator.ClearTelesalesSearch();
        }

        #endregion

        #region private

        /// <summary>
        /// Retrieves the workflows.
        /// </summary>
        private void GetWorkflows()
        {       
            if (this.ShowAllOrders)
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetSearchTelesales());
            }
            else
            {
                this.FilteredSales = 
                    new ObservableCollection<Sale>(this.DataManager.GetSearchTelesales()
                        .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID));
            }
        }

        #endregion
    }
}



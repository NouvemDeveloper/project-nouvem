﻿// -----------------------------------------------------------------------
// <copyright file="TelesalesSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;

namespace Nouvem.ViewModel.Telesales
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Interface;

    public class TelesalesSetUpViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The selected partner.
        /// </summary>
        private ViewBusinessPartner selectedCustomer;

        /// <summary>
        /// The selected partner.
        /// </summary>
        private Model.BusinessObject.User selectedUser;

        /// <summary>
        /// Show unallocated customers flag.
        /// </summary>
        private bool showUnallocatedCustomersOnly;

        /// <summary>
        /// Call time when none specified.
        /// </summary>
        private string defaultCallTime = "00:01";

        /// <summary>
        /// The selected document status.
        /// </summary>
        private NouDocStatu selectedDocStatus;

        /// <summary>
        /// The selected call frequencies.
        /// </summary>
        private NouCallFrequency selectedCallFrequency;

        /// <summary>
        /// The next date to call on.
        /// </summary>
        private DateTime nextCallDate;

        /// <summary>
        /// All the customers.
        /// </summary>
        private IList<ViewBusinessPartner> allCustomers = new List<ViewBusinessPartner>();

        /// <summary>
        /// The customers.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> customers = new ObservableCollection<ViewBusinessPartner>();

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string nextCallTime;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeMonday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeTuesday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeWednesday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeThursday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeFriday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeSaturday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string callTimeSunday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeMonday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeTuesday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeWednesday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeThursday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeFriday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeSaturday;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private bool includeSunday;

        /// <summary>
        /// The current telesale.
        /// </summary>
        private Telesale telesale = new Telesale();
        
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TelesalesSetUpViewModel"/> class.
        /// </summary>
        public TelesalesSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Nouvem.Model.BusinessObject.Sale>(this, Token.CreateTelesale, s =>
            {
                this.TeleSale = this.DataManager.GetTelesaleById(s.SaleID);
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window change control command.
            Messenger.Default.Register<string>(this, Token.SwitchControl, this.ControlChange);

            #endregion

            #region command handler

            this.OnClosingCommand = new RelayCommand(() =>
            {
                ApplicationSettings.ShowUnallocatedCustomersOnly = this.ShowUnallocatedCustomersOnly;
            });

            #endregion

            this.ShowUnallocatedCustomersOnly = ApplicationSettings.ShowUnallocatedCustomersOnly;
            this.HasWriteAuthorisation = true;
            this.GetAllCustomers();
            this.GetCustomers();
            this.GetUsers();
            this.GetDocStatusItems();
            this.GetCallFrequencies();
            this.ClearForm(false);
            this.IsFormLoaded = true;
        }

        #endregion

        #region public interface

        #region property
       
        /// <summary>
        /// Gets or sets a value indicating whether only unallocated customers are displayed.
        /// </summary>
        public bool ShowUnallocatedCustomersOnly
        {
            get
            {
                return this.showUnallocatedCustomersOnly;
            }

            set
            {
                this.showUnallocatedCustomersOnly = value;
                this.RaisePropertyChanged();
                this.ClearForm();
            }
        }

        /// <summary>
        /// Gets or sets the selected document status.
        /// </summary>
        public NouDocStatu SelectedDocStatus
        {
            get
            {
                return this.selectedDocStatus;
            }

            set
            {
                this.selectedDocStatus = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleDocStatusChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the nou doc status items.
        /// </summary>
        public IList<NouDocStatu> DocStatusItems { get; set; }

        /// <summary>
        /// Gets or sets the current telesale.
        /// </summary>
        public Telesale TeleSale
        {
            get
            {
                return this.telesale;
            }

            set
            {
                this.telesale = value;
                if (value != null)
                {
                    this.HandleTeleSale();
                }
            }
        }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.customers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public IList<Model.BusinessObject.User> Users { get; set; }

        /// <summary>
        /// Gets or sets the call frequencies.
        /// </summary>
        public IList<NouCallFrequency> CallFrequencies { get; set; }

        /// <summary>
        /// Gets or sets the selected customer.
        /// </summary>
        public ViewBusinessPartner SelectedCustomer
        {
            get
            {
                return this.selectedCustomer;
            }

            set
            {
                this.selectedCustomer = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customer.
        /// </summary>
        public Model.BusinessObject.User SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                this.selectedUser = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected customer.
        /// </summary>
        public NouCallFrequency SelectedCallFrequency
        {
            get
            {
                return this.selectedCallFrequency;
            }

            set
            {
                this.selectedCallFrequency = value;
                this.RaisePropertyChanged();
            }
        }

        public DateTime NextCallDate
        {
            get
            {
                return this.nextCallDate;
            }

            set
            {
                this.SetMode(value, this.nextCallDate);
                this.nextCallDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next call time.
        /// </summary>
        public string NextCallTime
        {
            get
            {
                return this.nextCallTime;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.nextCallTime);
                this.nextCallTime = value;
                this.RaisePropertyChanged();

                this.CallTimeMonday = value;
                this.CallTimeTuesday = value;
                this.CallTimeWednesday = value;
                this.CallTimeThursday = value;
                this.CallTimeFriday = value;
                this.CallTimeSaturday = value;
                this.CallTimeSunday = value;
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeMonday
        {
            get
            {
                return this.callTimeMonday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeMonday);
                this.callTimeMonday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeTuesday
        {
            get
            {
                return this.callTimeTuesday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeTuesday);
                this.callTimeTuesday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeWednesday
        {
            get
            {
                return this.callTimeWednesday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeWednesday);
                this.callTimeWednesday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeThursday
        {
            get
            {
                return this.callTimeThursday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeThursday);
                this.callTimeThursday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeFriday
        {
            get
            {
                return this.callTimeFriday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeFriday);
                this.callTimeFriday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeSaturday
        {
            get
            {
                return this.callTimeSaturday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeSaturday);
                this.callTimeSaturday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next daily call time.
        /// </summary>
        public string CallTimeSunday
        {
            get
            {
                return this.callTimeSunday;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }

                this.SetMode(value, this.callTimeSunday);
                this.callTimeSunday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeMonday
        {
            get
            {
                return this.includeMonday;
            }

            set
            {
                this.SetMode(value, this.includeMonday);
                this.includeMonday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeTuesday
        {
            get
            {
                return this.includeTuesday;
            }

            set
            {
                this.SetMode(value, this.includeTuesday);
                this.includeTuesday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeWednesday
        {
            get
            {
                return this.includeWednesday;
            }

            set
            {
                this.SetMode(value, this.includeWednesday);
                this.includeWednesday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeThursday
        {
            get
            {
                return this.includeThursday;
            }

            set
            {
                this.SetMode(value, this.includeThursday);
                this.includeThursday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeFriday
        {
            get
            {
                return this.includeFriday;
            }

            set
            {
                this.SetMode(value, this.includeFriday);
                this.includeFriday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeSaturday
        {
            get
            {
                return this.includeSaturday;
            }

            set
            {
                this.SetMode(value, this.includeSaturday);
                this.includeSaturday = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the day is to be included from calls.
        /// </summary>
        public bool IncludeSunday
        {
            get
            {
                return this.includeSunday;
            }

            set
            {
                this.SetMode(value, this.includeSunday);
                this.includeSunday = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Checks for the existance of a current selected user.
        /// </summary>
        /// <returns>A flag, indicating the existence of a selected user.</returns>
        protected override bool CheckForEntity()
        {
            return this.telesale != null && this.telesale.TelesalesID > 0;
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddOrUpdateTelesale();
                    break;

                case ControlMode.Update:
                    this.AddOrUpdateTelesale();
                    break;

                case ControlMode.Find:
                    Messenger.Default.Send(ViewType.TelesalesSearch);
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.TeleSale = this.DataManager.GetTelesaleByLastEdit();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.TeleSale == null || this.TeleSale.TelesalesID == 0)
            {
                this.TeleSale = this.DataManager.GetTelesaleByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetTelesaleById(this.TeleSale.TelesalesID - 1);
            if (localSale != null)
            {
                this.TeleSale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.TeleSale == null || this.TeleSale.TelesalesID == 0)
            {
                this.TeleSale = this.DataManager.GetTelesaleByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetTelesaleById(this.TeleSale.TelesalesID + 1);
            if (localSale != null)
            {
                this.TeleSale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.TeleSale = this.DataManager.GetTelesaleByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.TeleSale = this.DataManager.GetTelesaleByFirstLast(false);
        }
        
        #endregion

        #region private

        /// <summary>
        /// Get the local document status types.
        /// </summary>
        private void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems.Where(x => x == NouvemGlobal.NouDocStatusActive || x == NouvemGlobal.NouDocStatusComplete).ToList();
        }

        private void AddOrUpdateTelesale()
        {
            #region validation

            if (this.SelectedCustomer == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            if (this.SelectedCallFrequency == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCallFrequencySelected);
                return;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeMonday))
            {
                this.callTimeMonday = this.defaultCallTime;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeTuesday))
            {
                this.callTimeTuesday = this.defaultCallTime;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeWednesday))
            {
                this.callTimeWednesday = this.defaultCallTime;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeThursday))
            {
                this.callTimeThursday = this.defaultCallTime;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeFriday))
            {
                this.callTimeFriday = this.defaultCallTime;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeSaturday))
            {
                this.callTimeSaturday = this.defaultCallTime;
            }

            if (string.IsNullOrWhiteSpace(this.CallTimeSunday))
            {
                this.callTimeSunday = this.defaultCallTime;
            }

            #endregion

            this.telesale.BPMasterID = this.SelectedCustomer.BPMasterID;
            this.telesale.UserMasterID = this.SelectedUser.UserMaster.UserMasterID;
            this.telesale.NouCallFrequencyID = this.SelectedCallFrequency.NouCallFrequencyID;
            this.telesale.NextCallDate = this.nextCallDate;
            this.telesale.NextCallTime = this.nextCallTime ?? this.defaultCallTime;
            this.telesale.CallTimeMonday = !string.IsNullOrWhiteSpace(this.callTimeMonday) && this.includeMonday ? this.callTimeMonday : null;
            this.telesale.CallTimeTuesday = !string.IsNullOrWhiteSpace(this.callTimeTuesday) && this.includeTuesday? this.callTimeTuesday : null;
            this.telesale.CallTimeWednesday = !string.IsNullOrWhiteSpace(this.callTimeWednesday) && this.includeWednesday ? this.callTimeWednesday : null;
            this.telesale.CallTimeThursday = !string.IsNullOrWhiteSpace(this.callTimeThursday) && this.includeThursday ? this.callTimeThursday: null;
            this.telesale.CallTimeFriday = !string.IsNullOrWhiteSpace(this.callTimeFriday) && this.includeFriday ? this.callTimeFriday: null;
            this.telesale.CallTimeSaturday = !string.IsNullOrWhiteSpace(this.callTimeSaturday) && this.includeSaturday ? this.callTimeSaturday : null;
            this.telesale.CallTimeSunday = !string.IsNullOrWhiteSpace(this.callTimeSunday) && this.includeSunday ? this.CallTimeSunday : null;
            this.telesale.CreationDate = DateTime.Now;
            this.telesale.EditDate = DateTime.Now;
            this.telesale.UserMasterID_CreatedBy = NouvemGlobal.UserId;
            this.telesale.DeviceMasterID = NouvemGlobal.DeviceId;
            this.telesale.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;

            if (this.DataManager.AddOrUpdateTelesale(this.telesale))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Handles a document status change.
        /// </summary>
        private void HandleDocStatusChange()
        {
            if (!this.EntitySelectionChange)
            {
                if (this.TeleSale != null && this.TeleSale.TelesalesID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Handles a selected telesale.
        /// </summary>
        private void HandleTeleSale()
        {
            this.EntitySelectionChange = true;
            try
            {
                var localCustomer = this.allCustomers.FirstOrDefault(x => x.BPMasterID == this.telesale.BPMasterID);
                if (localCustomer == null)
                {
                    return;
                }

                if (!this.Customers.Any(x => x.BPMasterID == localCustomer.BPMasterID))
                {
                    this.Customers.Add(localCustomer);
                }

                this.SelectedCustomer = this.Customers.FirstOrDefault(x => x.BPMasterID == localCustomer.BPMasterID);
                this.SelectedUser = this.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.telesale.UserMasterID);
                this.SelectedCallFrequency =
                    this.CallFrequencies.FirstOrDefault(x => x.NouCallFrequencyID == this.telesale.NouCallFrequencyID);
                this.NextCallDate = this.telesale.NextCallDate.ToDate();
                this.NextCallTime = this.telesale.NextCallTime;
                this.CallTimeMonday = this.telesale.CallTimeMonday;
                this.CallTimeTuesday = this.telesale.CallTimeTuesday;
                this.CallTimeWednesday = this.telesale.CallTimeWednesday;
                this.CallTimeThursday = this.telesale.CallTimeThursday;
                this.CallTimeFriday = this.telesale.CallTimeFriday;
                this.CallTimeSaturday = this.telesale.CallTimeSaturday;
                this.CallTimeSunday = this.telesale.CallTimeSunday;
                this.IncludeMonday = !string.IsNullOrEmpty(this.callTimeMonday);
                this.IncludeTuesday = !string.IsNullOrEmpty(this.callTimeTuesday);
                this.IncludeWednesday = !string.IsNullOrEmpty(this.callTimeWednesday);
                this.IncludeThursday = !string.IsNullOrEmpty(this.callTimeThursday);
                this.IncludeFriday = !string.IsNullOrEmpty(this.callTimeFriday);
                this.IncludeSaturday = !string.IsNullOrEmpty(this.callTimeSaturday);
                this.IncludeSunday = !string.IsNullOrEmpty(this.callTimeSunday);

                this.SelectedDocStatus =
                   NouvemGlobal.NouDocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.telesale.NouDocStatusID);

            }
            finally
            {
                this.EntitySelectionChange = false;
            }
        }

        /// <summary>
        /// Clears the ui.
        /// </summary>
        private void ClearForm(bool refreshCustomers = true)
        {
            if (refreshCustomers)
            {
                this.GetCustomers();
            }
          
            this.SelectedCustomer = null;
            if (this.Users != null)
            {
                this.SelectedUser = this.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == NouvemGlobal.UserId);
            }
         
            this.SelectedCallFrequency = null;
            this.NextCallDate = DateTime.Today;
            this.NextCallTime = null;
            this.CallTimeMonday = null;
            this.CallTimeTuesday = null;
            this.CallTimeWednesday = null;
            this.CallTimeThursday = null;
            this.CallTimeFriday = null;
            this.CallTimeSaturday = null;
            this.CallTimeSunday = null;
            this.IncludeMonday = false;
            this.IncludeTuesday = false;
            this.IncludeWednesday = false;
            this.IncludeThursday = false;
            this.IncludeFriday = false;
            this.IncludeSaturday = false;
            this.IncludeSunday = false;
            this.telesale = new Telesale();
            this.SelectedDocStatus =
                    NouvemGlobal.NouDocStatusItems.FirstOrDefault(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID);
            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Gets all the customers.
        /// </summary>
        private void GetAllCustomers()
        {
            this.allCustomers = NouvemGlobal.CustomerPartners
                .Where(x =>
                    ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                         || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)) &&
                                         !x.Details.Type.StartsWithIgnoringCase(Strings.Haulier))
                .OrderBy(x => x.Details.Name)
                .Select(x => x.Details).ToList();
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        private void GetCustomers()
        {
            this.Customers.Clear();
            if (this.ShowUnallocatedCustomersOnly)
            {
                var allocated =
                this.DataManager.GetSearchTelesales().Where(x => x.Customer != null).Select(x => x.Customer.BPMasterID).ToList();

                var localAllocatedCustomers = this.allCustomers
                    .Where(x => !allocated.Contains(x.BPMasterID));

                foreach (var viewBusinessPartner in localAllocatedCustomers)
                {
                    this.Customers.Add(viewBusinessPartner);
                }

                return;
            }
           
            foreach (var viewBusinessPartner in this.allCustomers)
            {
                this.Customers.Add(viewBusinessPartner);
            }
        }

        /// <summary>
        /// Gets the application users.
        /// </summary>
        private void GetUsers()
        {
            this.Users = NouvemGlobal.Users;
        }

        /// <summary>
        /// Gets the application call frequencies.
        /// </summary>
        private void GetCallFrequencies()
        {
            this.CallFrequencies = this.DataManager.GetCallFrequencies();
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            if (this.CurrentMode == ControlMode.Add)
            {
                this.ClearForm();
            }
        }

        /// <summary>
        /// Validates a time (must be HH:MM)
        /// </summary>
        /// <param name="time">The time to validate.</param>
        /// <returns>Flag, as to valid time or not.</returns>
        private bool ValidateTime(string time)
        {
            if (!time.Is24HourTime())
            {
                SystemMessage.Write(MessageType.Issue, Message.IncorrectTimeFormat);
                NouvemMessageBox.Show(Message.IncorrectTimeFormat);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Close, and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTelesalesSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseTelesalesSetUpWindow);
        }

        #endregion
    }
}

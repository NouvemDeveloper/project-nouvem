﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityTemplateAllocationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Traceability
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class TraceabilityTemplateAllocationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The template names reference.
        /// </summary>
        private ObservableCollection<TraceabilityTemplateName> traceabilityTemplateNames;

        /// <summary>
        /// The selected template name
        /// </summary>
        private TraceabilityTemplateName selectedTemplateName;

        /// <summary>
        /// The selected available traceability reference.
        /// </summary>
        private ViewTraceabilityMaster selectedAvailableTraceabilityMaster;

        /// <summary>
        /// The selected assigned traceability reference.
        /// </summary>
        private ViewTraceabilityMaster selectedAssignedTraceabilityMaster;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TraceabilityTemplateAllocationViewModel class.
        /// </summary>
        public TraceabilityTemplateAllocationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // register fot a date master data change.
            Messenger.Default.Register<string>(this, Token.TraceabilityTemplateNamesUpdated, s => this.GetTraceabilityTemplateNames(true));

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            // Command to move the selected date.
            this.MoveTraceabilityCommand = new RelayCommand<string>(this.MoveTraceabilityCommandExecute);

            #endregion

            #region instantiation

            this.TraceabilityTemplateNames = new ObservableCollection<TraceabilityTemplateName>();
            this.TraceabilityMasters = new ObservableCollection<ViewTraceabilityMaster>();
            this.AvailableTraceabilityMasters = new ObservableCollection<ViewTraceabilityMaster>();
            this.AssignedTraceabilityMasters = new ObservableCollection<ViewTraceabilityMaster>();
            this.TraceabilityTemplateAllocations = new List<TraceabilityTemplateAllocation>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);
            this.GetTraceabilityMasters();
            this.GetTemplateAllocations();
            this.GetTraceabilityTemplateNames();
        }

        #endregion

        #region property

        /// <summary>
        /// Get the Traceability template allocations.
        /// </summary>
        public IList<TraceabilityTemplateAllocation> TraceabilityTemplateAllocations { get; private set; }

        /// <summary>
        /// Get the Traceability Masters.
        /// </summary>
        public ObservableCollection<ViewTraceabilityMaster> TraceabilityMasters { get; private set; }

        /// <summary>
        /// The available Traceability masters reference.
        /// </summary>
        private ObservableCollection<ViewTraceabilityMaster> availableTraceability;

        /// <summary>
        /// The assigned Traceability masters reference.
        /// </summary>
        private ObservableCollection<ViewTraceabilityMaster> assignedTraceability;

        /// <summary>
        /// Get or sets the available Traceability Masters.
        /// </summary>
        public ObservableCollection<ViewTraceabilityMaster> AvailableTraceabilityMasters
        {
            get
            {
                return this.availableTraceability;
            }

            set
            {
                this.availableTraceability = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the assigned Traceability Masters.
        /// </summary>
        public ObservableCollection<ViewTraceabilityMaster> AssignedTraceabilityMasters
        {
            get
            {
                return this.assignedTraceability;
            }

            set
            {
                this.assignedTraceability = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Traceability Masters.
        /// </summary>
        public ObservableCollection<TraceabilityTemplateName> TraceabilityTemplateNames
        {
            get
            {
                return this.traceabilityTemplateNames;
            }

            set
            {
                this.traceabilityTemplateNames = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected Traceability template name.
        /// </summary>
        public TraceabilityTemplateName SelectedTemplateName
        {
            get
            {
                return this.selectedTemplateName;
            }

            set
            {
                this.selectedTemplateName = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedTemplateName();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected available Traceabilitymaster.
        /// </summary>
        public ViewTraceabilityMaster SelectedAvailableTraceabilityMaster
        {
            get
            {
                return this.selectedAvailableTraceabilityMaster;
            }

            set
            {
                this.selectedAvailableTraceabilityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected assigned Traceability master.
        /// </summary>
        public ViewTraceabilityMaster SelectedAssignedTraceabilityMaster
        {
            get
            {
                return this.selectedAssignedTraceabilityMaster;
            }

            set
            {
                this.selectedAssignedTraceabilityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to move the Traceability.
        /// </summary>
        public ICommand MoveTraceabilityCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Drill down to the template names.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.TraceabilityTemplateName, 0), Token.DrillDown);
            this.AvailableTraceabilityMasters.Clear();
            this.AssignedTraceabilityMasters.Clear();
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    //this.UpdateTraceabilityTemplateNames();
                    this.UpdateTemplate();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the current template.
        /// </summary>
        private void UpdateTemplate()
        {
            if (this.DataManager.AddTraceabilitysToTemplate(this.AssignedTraceabilityMasters,
                this.selectedTemplateName.TraceabilityTemplateNameID))
            {
                SystemMessage.Write(MessageType.Priority, Message.TraceabilityAddedToTemplate);
                this.Refresh();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.TraceabilityNotAddedToTemplate);
            }
        }

        /// <summary>
        /// Updates the Traceability template names group.
        /// </summary>
        private void UpdateTraceabilityTemplateNames()
        {
            try
            {
                if (this.DataManager.AddOrUpdateTraceabilityTemplateNames(this.TraceabilityTemplateNames))
                {
                    SystemMessage.Write(MessageType.Priority, Message.TraceabilityTemplateNamesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.TraceabilityTemplateNamesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.TraceabilityTemplateNamesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTraceabilityTemplateAllocation();
            Messenger.Default.Send(Token.Message, Token.CloseTraceabilityTemplateAllocationWindow);
        }

        /// <summary>
        /// Gets the application Traceability template names information.
        /// </summary>
        /// <param name="selectRecentlyAdded">Flag to determine if we are selecting the most recently added template.</param>
        private void GetTraceabilityTemplateNames(bool selectRecentlyAdded = false)
        {
            this.TraceabilityTemplateNames.Clear();
            foreach (var traceability in this.DataManager.GetTraceabilityTemplateNames())
            {
                this.TraceabilityTemplateNames.Add(traceability);
            }

            this.TraceabilityTemplateNames.Add(new TraceabilityTemplateName { Name = Strings.DefineNew });

            // Select the first, or most recently added template.
            this.SelectedTemplateName = selectRecentlyAdded
                ? this.TraceabilityTemplateNames.ElementAt(this.TraceabilityTemplateNames.Count - 2)
                : this.TraceabilityTemplateNames.First();
        }

        /// <summary>
        /// Handles the selection of a Traceability template name.
        /// </summary>
        private void HandleSelectedTemplateName()
        {
            // check if we are defining a new template name
            if (this.selectedTemplateName.Name.Equals(Strings.DefineNew))
            {
                // new..shortcut to the template name view.
                Messenger.Default.Send(ViewType.TraceabilityTemplateName);
                this.AvailableTraceabilityMasters.Clear();
                this.AssignedTraceabilityMasters.Clear();
                return;
            }

            this.AvailableTraceabilityMasters.Clear();
            this.AssignedTraceabilityMasters.Clear();
            this.AvailableTraceabilityMasters = new ObservableCollection<ViewTraceabilityMaster>(this.TraceabilityMasters);

            this.TraceabilityTemplateAllocations.ToList().ForEach(x =>
            {
                if (x.TraceabilityTemplateNameID == this.selectedTemplateName.TraceabilityTemplateNameID)
                {
                    var localTraceabilityMaster =
                        this.TraceabilityMasters.FirstOrDefault(traceabilityMaster => traceabilityMaster.TraceabilityTemplatesMasterID == x.TraceabilityTemplateMasterID);

                    if (localTraceabilityMaster != null)
                    {
                        localTraceabilityMaster.Batch = x.Batch.ToBool();
                        localTraceabilityMaster.Transaction = x.Transaction.ToBool();
                        localTraceabilityMaster.Required = x.Required.ToBool();
                        localTraceabilityMaster.Reset = x.Reset.ToBool();
                        this.AssignedTraceabilityMasters.Add(localTraceabilityMaster);
                        this.AvailableTraceabilityMasters.Remove(localTraceabilityMaster);
                    }
                }
            });
        }

        /// <summary>
        /// Gets the application Traceability masters information.
        /// </summary>
        private void GetTraceabilityMasters()
        {
            this.TraceabilityMasters.Clear();
            foreach (var date in this.DataManager.GetTraceabilityMasters())
            {
                this.TraceabilityMasters.Add(date);
            }
        }

        /// <summary>
        /// Gets the Traceability template allocations.
        /// </summary>
        private void GetTemplateAllocations()
        {
            this.TraceabilityTemplateAllocations = this.DataManager.GetTraceabilityTemplateAllocations();
        }

        /// <summary>
        /// handles the allocation/deallocation of Traceabilitys to and from the selected template.
        /// </summary>
        /// <param name="direction">The allocation direction.</param>
        private void MoveTraceabilityCommandExecute(string direction)
        {
            #region validation

            if (!this.CanMoveDate(direction))
            {
                return;
            }

            #endregion

            this.SetControlMode(ControlMode.Update);

            if (direction.Equals(Constant.MoveForward))
            {
                this.AssignedTraceabilityMasters.Add(this.selectedAvailableTraceabilityMaster);
                this.AvailableTraceabilityMasters.Remove(this.selectedAvailableTraceabilityMaster);

                return;
            }

            this.AvailableTraceabilityMasters.Add(this.selectedAssignedTraceabilityMaster);
            this.AssignedTraceabilityMasters.Remove(this.selectedAssignedTraceabilityMaster);
        }

        /// <summary>
        /// Determines if a move can be made.
        /// </summary>
        /// <returns>A flag, which indicates whether a move can be made.</returns>
        private bool CanMoveDate(string direction)
        {
            if (direction.Equals(Constant.MoveForward) && this.selectedAvailableTraceabilityMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            if (direction.Equals(Constant.MoveBack) && this.selectedAssignedTraceabilityMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetTemplateAllocations();
            this.SetControlMode(ControlMode.OK);
        }

        #endregion
    }
}



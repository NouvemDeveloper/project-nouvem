﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityNameViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Traceability
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class TraceabilityNameViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected template name.
        /// </summary>
        private TraceabilityTemplateName selectedTraceabilityTemplateName;

        /// <summary>
        /// The template names collection.
        /// </summary>
        private ObservableCollection<TraceabilityTemplateName> traceabilityTemplateNames { get; set; }

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TraceabilityNameViewModel class.
        /// </summary>
        public TraceabilityNameViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            this.OnClosingCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = false;
                Messenger.Default.Unregister(this);
                this.Close();
            });

            #endregion

            #region instantiation

            this.TraceabilityTemplateNames = new ObservableCollection<TraceabilityTemplateName>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);
            this.GetTraceabilityTemplateNames();
            this.IsFormLoaded = true;
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the selected traceability template name.
        /// </summary>
        public TraceabilityTemplateName SelectedTraceabilityTemplateName
        {
            get
            {
                return this.selectedTraceabilityTemplateName;
            }

            set
            {
                this.selectedTraceabilityTemplateName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the traceability Masters.
        /// </summary>
        public ObservableCollection<TraceabilityTemplateName> TraceabilityTemplateNames
        {
            get
            {
                return this.traceabilityTemplateNames;
            }

            set
            {
                this.traceabilityTemplateNames = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand OnClosingCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedTraceabilityTemplateName != null)
            {
                this.selectedTraceabilityTemplateName.Deleted = true;
                this.UpdateTraceabilityTemplateNames();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.TraceabilityTemplateNames.Remove(this.selectedTraceabilityTemplateName);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateTraceabilityTemplateNames();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the traceability template names group.
        /// </summary>
        private void UpdateTraceabilityTemplateNames()
        {
            try
            {
                if (this.DataManager.AddOrUpdateTraceabilityTemplateNames(this.TraceabilityTemplateNames))
                {
                    SystemMessage.Write(MessageType.Priority, Message.TraceabilityTemplateNamesUpdated);
                    Messenger.Default.Send(Token.Message, Token.TraceabilityTemplateNamesUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.TraceabilityTemplateNamesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.TraceabilityTemplateNamesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTraceabilityName();
            Messenger.Default.Send(Token.Message, Token.CloseTraceabilityNameWindow);
        }

        /// <summary>
        /// Gets the application traceability template names information.
        /// </summary>
        private void GetTraceabilityTemplateNames()
        {
            this.TraceabilityTemplateNames.Clear();
            foreach (var traceability in this.DataManager.GetTraceabilityTemplateNames())
            {
                this.TraceabilityTemplateNames.Add(traceability);
            }
        }

        #endregion
    }
}


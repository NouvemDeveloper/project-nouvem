﻿// -----------------------------------------------------------------------
// <copyright file="LabelManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.IO;
using Nouvem.Shared;
using Nouvem.ViewModel;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Label = Nouvem.Model.DataLayer.Label;

    public class LabelManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly LabelManager Manager = new LabelManager();

        /// <summary>
        /// The label associations.
        /// </summary>
        private IList<LabelsAssociation> labelAssociations = new List<LabelsAssociation>();

        /// <summary>
        /// The label associations.
        /// </summary>
        private IList<LabelsAssociation> labelAssociationsProductGroups = new List<LabelsAssociation>();

        /// <summary>
        /// The label associations.
        /// </summary>
        private IList<LabelsAssociation> labelAssociationsProducts = new List<LabelsAssociation>(); 

        /// <summary>
        /// The application labels.
        /// </summary>
        private IList<Label> labels = new List<Label>();

        /// <summary>
        /// The application external label look ups.
        /// </summary>
        private IList<ExternalLabelDataLookUp> externalLabelLookUps = new List<ExternalLabelDataLookUp>();

        /// <summary>
        /// The application product groups.
        /// </summary>
        private IList<INGroup> productGroups = new List<INGroup>();

        /// <summary>
        /// Flag, as to whether we need to check for process associations.
        /// </summary>
        private bool checkForProcessAssociations;

        #endregion

        #region constructor

        private LabelManager()
        {
            Messenger.Default.Register<string>(this, Token.LabelAssociationsUpdated, s => Task.Factory.StartNew(this.GetLabelData));
            this.GetLabelData();
            this.GetProductGroups();
            this.GetExternalLabelData();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the label manager singleton.
        /// </summary>
        public static LabelManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Gets the current label and displays on the ui.
        /// </summary>
        /// <param name="customerId">The customer id to search for.</param>
        /// <param name="customerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <returns>The current associated label(s).</returns>
        public void GetLabelAndDisplay(int? customerId, int? customerGroupId, int? productId, int? productGroupId, LabelType labelType, int? warehouseId = null)
        {
            var localLabels = this.GetLabels(customerId, customerGroupId, productId, productGroupId, labelType, wareHouseId:warehouseId);
            if (localLabels != null && localLabels.Any())
            {
                Messenger.Default.Send(localLabels.Last().Label, Token.ChangeDisplayLabelName);
            }
        }

        /// <summary>
        /// Gets the current label association.
        /// </summary>
        /// <param name="customerId">The customer id to search for.</param>
        /// <param name="customerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <returns>The current associated label(s).</returns>
        public IList<LabelData> GetLabels(int? customerId, int? customerGroupId, int? productId, int? productGroupId, LabelType labelType, Process process = null, int? wareHouseId = null, bool batchLabel = false)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to find label for customerId:{0}, customerGroupId:{1}, productId:{2}, productGroupId:{3}, LabelType:{4}",
                                                            customerId, customerGroupId, productId, productGroupId, labelType));

            if (process == null)
            {
                process = ViewModelLocator.ProcessSelectionStatic.SelectedProcess;
            }

            var labelAssoc = this.GetAssociatedLabel(customerId, customerGroupId, productId, productGroupId, process, wareHouseId:wareHouseId, batchLabel:batchLabel);

            if (labelAssoc == null)
            {
                this.Log.LogDebug(this.GetType(), "No matching label found to print");
                return null;
            }

            var localLabelDatas = new List<LabelData>();
            List<Label> localLabels;
            var labelQty = 1;
            Printer localPrinter = null;
            switch (labelType)
            {
                case LabelType.Box:
                    localLabels = labelAssoc.BoxLabels.ToList();
                    labelQty = labelAssoc.BoxLabelQty.IsNullOrZero() ? 1 : labelAssoc.BoxLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterBox;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                case LabelType.Item:
                    localLabels = labelAssoc.ItemLabels.ToList();
                    labelQty = labelAssoc.ItemLabelQty.IsNullOrZero() ? 1 : labelAssoc.ItemLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterItem;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                case LabelType.Piece:
                    localLabels = labelAssoc.PieceLabels.ToList();
                    labelQty = labelAssoc.PieceLabelQty.IsNullOrZero() ? 1 : labelAssoc.PieceLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterPiece;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                case LabelType.Pallet:
                    localLabels = labelAssoc.PalletLabels.ToList();
                    labelQty = labelAssoc.PalletLabelQty.IsNullOrZero() ? 1 : labelAssoc.PalletLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterPallet;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;

                default:
                    localLabels = labelAssoc.ShippingLabels.ToList();
                    labelQty = labelAssoc.ShippingLabelQty.IsNullOrZero() ? 1 : labelAssoc.ShippingLabelQty.ToInt();
                    localPrinter = labelAssoc.PrinterShipping;

                    foreach (var localLabel in localLabels)
                    {
                        localLabelDatas.Add(new LabelData { Label = localLabel, LabelQty = labelQty, Printer = localPrinter });
                    }

                    break;
            }

            return localLabelDatas;
        }

        /// <summary>
        /// Gets a gs1 label.
        /// </summary>
        /// <param name="name">The gs1 name.</param>
        /// <returns></returns>
        public Label GetGs1PalletLabel(string name)
        {
            var label = this.labels.FirstOrDefault(x => x.Name.CompareIgnoringCase(name));

            if (label == null)
            {
                this.Log.LogInfo(this.GetType(), "No matching label found to print");
                return null;
            }

            return label;
        }

        /// <summary>
        /// Gets the application labels.
        /// </summary>
        public void GetLabels()
        {
            this.labels = this.DataManager.GetLabels();
        }

        /// <summary>
        /// Gets the matching label.
        /// </summary>
        /// <param name="labelId">The label id to search for.</param>
        public Label GetLabel(int labelId)
        {
            return this.labels.FirstOrDefault(x => x.LabelID == labelId);
        }
       
        #endregion

        #endregion

        #region private

        /// <summary>
        /// Gets the current label association.
        /// </summary>
        /// <param name="partnerId">The customer id to search for.</param>
        /// <param name="partnerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <returns>The current label association.</returns>
        private LabelsAssociation GetAssociatedLabel(
            int? partnerId, 
            int? partnerGroupId,
            int? productId, 
            int? productGroupId, 
            Process process = null,
            int? wareHouseId = null,
            bool batchLabel = false)
        {
            this.Log.LogDebug(this.GetType(), "GetAssociatedLabel(): processing..");

            if (batchLabel && process != null)
            {
                return this.labelAssociations.FirstOrDefault(x => x.LabelProcesses.Select(proc => proc.ProcessID).Contains(process.ProcessID));
            }

            if (this.checkForProcessAssociations && process != null)
            {
                var processLabel =
                    this.GetAssociatedLabelByProcess(partnerId, partnerGroupId, productId, productGroupId, process);
                if (processLabel != null)
                {
                    return processLabel;
                }
            }

            IList<int> hierarchicalProductGroupIds = new List<int>();
            if (!productGroupId.IsNullOrZero())
            {
                // get the product group and it's parent ids (if any)
                hierarchicalProductGroupIds = this.GetParentGroups(productGroupId.ToInt());
            }

            var localLabelAssociations = this.labelAssociations;
            //if (wareHouseId.HasValue)
            //{
            //    localLabelAssociations = this.labelAssociations.Where(x => x.WarehouseID == wareHouseId).ToList();
            //}

            // check product and partner match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && partnerId == assoc.BPMasterID)
                {
                    return assoc;
                }
            }

            // check product and partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && partnerGroupId == assoc.BPGroupID)
                {
                    return assoc;
                }
            }

            // check product and all partner/partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && (assoc.BPMasterID == -1 || assoc.BPGroupID == -1))
                {
                    return assoc;
                }
            }

            // check product group and partner match
            foreach (var assoc in localLabelAssociations)
            {
                if (partnerId == assoc.BPMasterID && hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if (partnerId == localAssoc.BPMasterID &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // check product group and partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (partnerGroupId == assoc.BPGroupID && hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if (partnerGroupId == localAssoc.BPGroupID &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // check product group all partner/partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()) && (assoc.BPMasterID == -1 || assoc.BPGroupID == -1))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                       return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if ((localAssoc.BPMasterID == -1 || localAssoc.BPGroupID == -1) &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // get the all products/groups associations
            var nonProductAssociations =
                localLabelAssociations.Where(assoc => assoc.INMasterID < 0 && assoc.INGroupID < 0).ToList();

            // check partner match against all products.
            foreach (var assoc in nonProductAssociations)
            {
                if (partnerId == assoc.BPMasterID)
                {
                    return assoc;
                }
            }

            // check partner group match against all products.
            foreach (var assoc in nonProductAssociations)
            {
                if (partnerGroupId == assoc.BPGroupID)
                {
                    return assoc;
                }
            }

            // return default 
            var defaultLabelAssoc = localLabelAssociations.FirstOrDefault(x => x.BPMasterID < 0 
                                                                               && x.BPGroupID < 0 && x.INMasterID < 0 && x.INGroupID < 0
                                                                               && (x.LabelProcesses == null || !x.LabelProcesses.Any()));
            return defaultLabelAssoc;
        }

        /// <summary>
        /// Gets the current label association.
        /// </summary>
        /// <param name="partnerId">The customer id to search for.</param>
        /// <param name="partnerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <returns>The current label association.</returns>
        private LabelsAssociation GetAssociatedLabelByProcess(
            int? partnerId,
            int? partnerGroupId,
            int? productId,
            int? productGroupId,
            Process process)
        {
            IList<int> hierarchicalProductGroupIds = new List<int>();
            if (!productGroupId.IsNullOrZero())
            {
                // get the product group and it's parent ids (if any)
                hierarchicalProductGroupIds = this.GetParentGroups(productGroupId.ToInt());
            }

            var localLabelAssociations = this.labelAssociations.Where(x => x.LabelProcesses != null && x.LabelProcesses.Any(proc => proc.ProcessID == process.ProcessID)).ToList();

            // check product and partner match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && partnerId == assoc.BPMasterID)
                {
                    return assoc;
                }
            }

            // check product and partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && partnerGroupId == assoc.BPGroupID)
                {
                    return assoc;
                }
            }

            // check product and all partner/partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (productId == assoc.INMasterID && (assoc.BPMasterID == -1 || assoc.BPGroupID == -1))
                {
                    return assoc;
                }
            }

            // check product group and partner match
            foreach (var assoc in localLabelAssociations)
            {
                if (partnerId == assoc.BPMasterID && hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if (partnerId == localAssoc.BPMasterID &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // check product group and partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (partnerGroupId == assoc.BPGroupID && hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if (partnerGroupId == localAssoc.BPGroupID &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // check product group all partner/partner group match
            foreach (var assoc in localLabelAssociations)
            {
                if (hierarchicalProductGroupIds.Contains(assoc.INGroupID.ToInt()) && (assoc.BPMasterID == -1 || assoc.BPGroupID == -1))
                {
                    if (hierarchicalProductGroupIds.Count == 1)
                    {
                        return assoc;
                    }

                    // we need to ensure the child group takes precedence
                    foreach (var localGroupId in hierarchicalProductGroupIds)
                    {
                        foreach (var localAssoc in localLabelAssociations)
                        {
                            if ((localAssoc.BPMasterID == -1 || localAssoc.BPGroupID == -1) &&
                                localGroupId == localAssoc.INGroupID.ToInt())
                            {
                                return localAssoc;
                            }
                        }
                    }
                }
            }

            // get the all products/groups associations
            var nonProductAssociations =
                localLabelAssociations.Where(assoc => assoc.INMasterID < 0 && assoc.INGroupID < 0).ToList();

            // check partner match against all products.
            foreach (var assoc in nonProductAssociations)
            {
                if (partnerId == assoc.BPMasterID)
                {
                    return assoc;
                }
            }

            // check partner group match against all products.
            foreach (var assoc in nonProductAssociations)
            {
                if (partnerGroupId == assoc.BPGroupID)
                {
                    return assoc;
                }
            }

            // return default 
            var defaultLabelAssoc = localLabelAssociations.FirstOrDefault(x => x.BPMasterID < 0 && x.BPGroupID < 0 && x.INMasterID < 0 && x.INGroupID < 0 
                                                                               && x.LabelProcesses.Select(proc => proc.ProcessID).Contains(process.ProcessID));
            return defaultLabelAssoc;
        }

        /// <summary>
        /// Gets the label associations.
        /// </summary>
        private void GetLabelAssociations()
        {
            this.labelAssociations = this.DataManager.GetLabelAssociations().OrderBy(x => x.WarehouseID).ToList();
            this.checkForProcessAssociations =
                this.labelAssociations.Any(x => x.LabelProcesses != null && x.LabelProcesses.Any());
        }

        /// <summary>
        /// Retrieves the label data.
        /// </summary>
        private void GetLabelData()
        {
            this.GetLabels();
            this.GetLabelAssociations();
        }

        /// <summary>
        /// Retrieves the product groups.
        /// </summary>
        private void GetProductGroups()
        {
            this.productGroups = this.DataManager.GetInventoryGroups();
        }

        /// <summary>
        /// Gets all the parent ids in the group hierarchy.
        /// </summary>
        /// <param name="groupId">The group id to check.</param>
        /// <returns>Returns all the parent ids in the group hierarchy.</returns>
        private IList<int> GetParentGroups(int groupId)
        {
            int? childsParentId = null;
            var childsGroup = this.productGroups.FirstOrDefault(x => x.INGroupID == groupId);
            if (childsGroup != null)
            {
                childsParentId = childsGroup.ParentInGroupID;
            }

            var hierarchyIds = new List<int> { groupId };

            while (childsParentId != null)
            {
                var parent = this.productGroups.FirstOrDefault(x => x.INGroupID == childsParentId);
                if (parent != null)
                {
                    hierarchyIds.Add(parent.INGroupID);
                    childsParentId = parent.ParentInGroupID;
                }
                else
                {
                    childsParentId = null;
                }
            }

            return hierarchyIds;
        }

        /// <summary>
        /// method that creates a string that can be printed
        /// </summary>
        /// <returns>the label string to be printed</returns>
        public string CreateLabelString(string data, DataTable dt)
        {
            var columns = dt.Columns;
            //data = File.ReadAllText(@"C:\Nouvem\Label\test.txt");

            foreach (var lookUp in this.externalLabelLookUps)
            {
                if (columns.Contains("Item1") && lookUp.DataField.Equals("Item1"))
                {
                    var field = dt.Rows[0]["Item1"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item2") && lookUp.DataField.Equals("Item2"))
                {
                    var field = dt.Rows[0]["Item2"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item3") && lookUp.DataField.Equals("Item3"))
                {
                    var field = dt.Rows[0]["Item3"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item4") && lookUp.DataField.Equals("Item4"))
                {
                    var field = dt.Rows[0]["Item4"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item5") && lookUp.DataField.Equals("Item5"))
                {
                    var field = dt.Rows[0]["Item5"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item6") && lookUp.DataField.Equals("Item6"))
                {
                    var field = dt.Rows[0]["Item6"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item7") && lookUp.DataField.Equals("Item7"))
                {
                    var field = dt.Rows[0]["Item7"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item8") && lookUp.DataField.Equals("Item8"))
                {
                    var field = dt.Rows[0]["Item8"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item9") && lookUp.DataField.Equals("Item9"))
                {
                    var field = dt.Rows[0]["Item9"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item10") && lookUp.DataField.Equals("Item10"))
                {
                    var field = dt.Rows[0]["Item10"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item11") && lookUp.DataField.Equals("Item11"))
                {
                    var field = dt.Rows[0]["Item11"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item12") && lookUp.DataField.Equals("Item12"))
                {
                    var field = dt.Rows[0]["Item12"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item13") && lookUp.DataField.Equals("Item13"))
                {
                    var field = dt.Rows[0]["Item13"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item14") && lookUp.DataField.Equals("Item14"))
                {
                    var field = dt.Rows[0]["Item14"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item15") && lookUp.DataField.Equals("Item15"))
                {
                    var field = dt.Rows[0]["Item15"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item16") && lookUp.DataField.Equals("Item16"))
                {
                    var field = dt.Rows[0]["Item16"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item17") && lookUp.DataField.Equals("Item17"))
                {
                    var field = dt.Rows[0]["Item17"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item18") && lookUp.DataField.Equals("Item18"))
                {
                    var field = dt.Rows[0]["Item18"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item19") && lookUp.DataField.Equals("Item19"))
                {
                    var field = dt.Rows[0]["Item19"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item20") && lookUp.DataField.Equals("Item20"))
                {
                    var field = dt.Rows[0]["Item20"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item21") && lookUp.DataField.Equals("Item21"))
                {
                    var field = dt.Rows[0]["Item21"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item22") && lookUp.DataField.Equals("Item22"))
                {
                    var field = dt.Rows[0]["Item22"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item23") && lookUp.DataField.Equals("Item23"))
                {
                    var field = dt.Rows[0]["Item23"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item24") && lookUp.DataField.Equals("Item24"))
                {
                    var field = dt.Rows[0]["Item24"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item25") && lookUp.DataField.Equals("Item25"))
                {
                    var field = dt.Rows[0]["Item25"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item26") && lookUp.DataField.Equals("Item26"))
                {
                    var field = dt.Rows[0]["Item26"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item27") && lookUp.DataField.Equals("Item27"))
                {
                    var field = dt.Rows[0]["Item27"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item28") && lookUp.DataField.Equals("Item28"))
                {
                    var field = dt.Rows[0]["Item28"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item29") && lookUp.DataField.Equals("Item29"))
                {
                    var field = dt.Rows[0]["Item29"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item30") && lookUp.DataField.Equals("Item30"))
                {
                    var field = dt.Rows[0]["Item30"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item31") && lookUp.DataField.Equals("Item31"))
                {
                    var field = dt.Rows[0]["Item31"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item32") && lookUp.DataField.Equals("Item32"))
                {
                    var field = dt.Rows[0]["Item32"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item33") && lookUp.DataField.Equals("Item33"))
                {
                    var field = dt.Rows[0]["Item33"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item34") && lookUp.DataField.Equals("Item34"))
                {
                    var field = dt.Rows[0]["Item34"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item35") && lookUp.DataField.Equals("Item35"))
                {
                    var field = dt.Rows[0]["Item35"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item36") && lookUp.DataField.Equals("Item36"))
                {
                    var field = dt.Rows[0]["Item36"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item37") && lookUp.DataField.Equals("Item37"))
                {
                    var field = dt.Rows[0]["Item37"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item38") && lookUp.DataField.Equals("Item38"))
                {
                    var field = dt.Rows[0]["Item38"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item39") && lookUp.DataField.Equals("Item39"))
                {
                    var field = dt.Rows[0]["Item39"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Item40") && lookUp.DataField.Equals("Item40"))
                {
                    var field = dt.Rows[0]["Item40"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("EAN128ITALIAN") && lookUp.DataField.Equals("EAN128ITALIAN"))
                {
                    var field = dt.Rows[0]["EAN128ITALIAN"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Serial Number") && lookUp.DataField.Equals("Serial Number"))
                {
                    var field = dt.Rows[0]["Serial Number"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Cut In") && lookUp.DataField.Equals("Cut In"))
                {
                    var field = dt.Rows[0]["Cut In"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Reared In") && lookUp.DataField.Equals("Reared In"))
                {
                    var field = dt.Rows[0]["Reared In"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Serial Number") && lookUp.DataField.Equals("Stock Barcode"))
                {
                    var field = dt.Rows[0]["Serial Number"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Kill Date") && lookUp.DataField.Equals("Kill Date"))
                {
                    var field = dt.Rows[0]["Kill Date"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Supplier") && lookUp.DataField.Equals("Supplier"))
                {
                    var field = dt.Rows[0]["Supplier"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Display Until Date") && lookUp.DataField.Equals("Display Until Date"))
                {
                    var field = dt.Rows[0]["Display Until Date"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("#Text1") && lookUp.DataField.Equals("Text1"))
                {
                    var field = dt.Rows[0]["#Text1"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("#Text2") && lookUp.DataField.Equals("Text2"))
                {
                    var field = dt.Rows[0]["#Text2"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("NominalWeight") && lookUp.DataField.Equals("Nominal Weight"))
                {
                    var field = dt.Rows[0]["NominalWeight"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Country Of Origin") && lookUp.DataField.Equals("Country Of Origin"))
                {
                    var field = dt.Rows[0]["Country Of Origin"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Slaughtered In") && lookUp.DataField.Equals("Slaughtered In"))
                {
                    var field = dt.Rows[0]["Slaughtered In"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Tare") && lookUp.DataField.Equals("Tare"))
                {
                    var field = dt.Rows[0]["Tare"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Product Net Weight in kg") && lookUp.DataField.Equals("Product Net Weight in kg"))
                {
                    var field = dt.Rows[0]["Product Net Weight in kg"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Product Name") && lookUp.DataField.Equals("Product Name"))
                {
                    var field = dt.Rows[0]["Product Name"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Product Name") && lookUp.DataField.Equals("Product Description"))
                {
                    var field = dt.Rows[0]["Product Name"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Reference") && lookUp.DataField.Equals("Reference"))
                {
                    var field = dt.Rows[0]["Reference"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Reference") && lookUp.DataField.Equals("Stock Batch Code"))
                {
                    var field = dt.Rows[0]["Reference"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Reference") && lookUp.DataField.Equals("Batch"))
                {
                    var field = dt.Rows[0]["Reference"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Packed On Date") && lookUp.DataField.Equals("Packed On Date"))
                {
                    var field = dt.Rows[0]["Packed On Date"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Best Before Date") && lookUp.DataField.Equals("Best Before Date"))
                {
                    var field = dt.Rows[0]["Best Before Date"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Use By Date") && lookUp.DataField.Equals("Use By Date"))
                {
                    var field = dt.Rows[0]["Use By Date"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Country Of Origin") && lookUp.DataField.Equals("Country Of Origin"))
                {
                    var field = dt.Rows[0]["Country Of Origin"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Pieces") && lookUp.DataField.Equals("Pieces"))
                {
                    var field = dt.Rows[0]["Pieces"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Customer") && lookUp.DataField.Equals("Customer"))
                {
                    var field = dt.Rows[0]["Customer"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Customer Address") && lookUp.DataField.Equals("Customer Address"))
                {
                    var field = dt.Rows[0]["Customer Address"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Haulier") && lookUp.DataField.Equals("Haulier"))
                {
                    var field = dt.Rows[0]["Haulier"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("Container Reference") && lookUp.DataField.Equals("Container Reference"))
                {
                    var field = dt.Rows[0]["Container Reference"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }

                if (columns.Contains("PO Reference") && lookUp.DataField.Equals("PO Reference"))
                {
                    var field = dt.Rows[0]["PO Reference"].ToString();
                    data = data.Replace(lookUp.PlaceholderData, field);
                    continue;
                }
            }
         
            //File.WriteAllText(@"C:\Nouvem\Label\LabelText.txt", data);

           // b.Number AS 'Batch/Lot Number',
           //s.Serial AS 'Serial Number',	
           //s.Pieces,	
           //o.Reference,   
           //CONVERT(DECIMAL(18,2), s.TransactionWeight) AS 'Product Net Weight in kg',
           //CONVERT(DECIMAL(18,2), s.Tare) AS 'Tare',		
           //CONVERT(DECIMAL(18,2), s.GrossWeight) AS 'Gross Wgt',
           //CONVERT(DECIMAL(18,0),s.TransactionQTY) as 'TransactionQty',
           //m.Code AS 'Product Code',
           //m.Name as 'Product Name',
           //bp.Name as 'Partner Name',
           //CONVERT(DECIMAL(18,2),m.NominalWeight) as 'NominalWeight',

           // try
           // {
           //     data = data.Replace("#COUNTRY_OF_SLAUGHTER#", serial);
           //     data = data.Replace("#COUNTRY_OF_ORIGIN#", this.CountryOfOrigin);
           //     data = data.Replace("#WEIGHT#", this.Weight);
           //     data = data.Replace("#CAT#", this.Category);
           //     data = data.Replace("#CONF#", this.ConformanceScore);
           //     data = data.Replace("#FAT#", this.FatScore);
           //     data = data.Replace("#CARCASS_NUMBER#", this.CarcassNumber);
           //     data = data.Replace("#BREED#", this.Breed);
           //     data = data.Replace("#GRADER#", this.Grader);
           //     data = data.Replace("#DATE#", this.KillDate);
           //     data = data.Replace("#BAND#", this.WeightBand);
           //     data = data.Replace("#REG_EL#", this.RegionEligibility);
           //     data = data.Replace("#RATING#", this.Rating);
           //     data = data.Replace("#BARCODE#", this.Barcode);
           //     data = data.Replace("#ELIGIBLE_CUSTOMERS#", this.EligibleCustomers);
           //     data = data.Replace("#SIDE#", this.Side);
           // }
           // catch (Exception ex)
           // {
           //     return string.Empty;
           // }

           return data;
        }

        /// <summary>
        /// method that creates a string that can be printed
        /// </summary>
        /// <returns>the label string to be printed</returns>
        //public string CreateLabelString(string fileName, string filePath)
        //{
        //    string stringToSend = string.Empty;
        //    string fullPath = string.Format("{0}\\{1}", filePath, fileName);

        //    try
        //    {
        //        if (System.IO.File.Exists(fullPath))
        //        {
        //            var file = new System.IO.StreamReader(fullPath);
        //            string line;

        //            stringToSend = file.Re

        //            while (!file.EndOfStream)
        //            {
        //                line = file.ReadLine() + Environment.NewLine;
        //                stringToSend += line;
        //            }

        //            stringToSend = stringToSend.Replace("#COUNTRY_OF_SLAUGHTER#", this.CountryOfSlaughter);
        //            stringToSend = stringToSend.Replace("#COUNTRY_OF_ORIGIN#", this.CountryOfOrigin);
        //            stringToSend = stringToSend.Replace("#WEIGHT#", this.Weight);
        //            stringToSend = stringToSend.Replace("#CAT#", this.Category);
        //            stringToSend = stringToSend.Replace("#CONF#", this.ConformanceScore);
        //            stringToSend = stringToSend.Replace("#FAT#", this.FatScore);
        //            stringToSend = stringToSend.Replace("#CARCASS_NUMBER#", this.CarcassNumber);
        //            stringToSend = stringToSend.Replace("#BREED#", this.Breed);
        //            stringToSend = stringToSend.Replace("#GRADER#", this.Grader);
        //            stringToSend = stringToSend.Replace("#DATE#", this.KillDate);
        //            stringToSend = stringToSend.Replace("#BAND#", this.WeightBand);
        //            stringToSend = stringToSend.Replace("#REG_EL#", this.RegionEligibility);
        //            stringToSend = stringToSend.Replace("#RATING#", this.Rating);
        //            stringToSend = stringToSend.Replace("#BARCODE#", this.Barcode);
        //            stringToSend = stringToSend.Replace("#ELIGIBLE_CUSTOMERS#", this.EligibleCustomers);
        //            stringToSend = stringToSend.Replace("#SIDE#", this.Side);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Error = ex.ToString();
        //        this.logger.LogException(ex.GetType(), ex);
        //        return string.Empty;
        //    }

        //    return stringToSend;
        //}
       

        /// <summary>
        /// Retrieve the external label data.
        /// </summary>
        private void GetExternalLabelData()
        {
            this.externalLabelLookUps = this.DataManager.GetExternalLabelDataLookUps();
        }

        #endregion
    }
}

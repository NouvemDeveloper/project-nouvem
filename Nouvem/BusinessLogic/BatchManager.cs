﻿// -----------------------------------------------------------------------
// <copyright file="BatchManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Global;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;

    public class BatchManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly BatchManager Manager = new BatchManager();

        #endregion

        #region constructor

        private BatchManager()
        {
            this.BatchNumbers = new List<BatchNumber>();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the batch manager singleton.
        /// </summary>
        public static BatchManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets or sets the batch numbers.
        /// </summary>
        public IList<BatchNumber> BatchNumbers { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Generates a batch reference.
        /// </summary>
        /// <param name="batchOption">The input option.</param>
        /// <returns>A batch reference.</returns>
        public string GenerateBatchReference(string batchOption)
        {
            return string.Format("{0}{1}", DateTime.Today.ToJulianDate(), batchOption);
        }

        /// <summary>
        /// Generates a colour based reference.
        /// </summary>
        /// <param name="colour"></param>
        /// <returns>A colour based batch number.</returns>
        public BatchNumber GenerateColourBatchNumber(string colour)
        {
            var reference = string.Format("{0}-{1}-{2}", colour, DateTime.Today.GetIso8601WeekOfYear(), DateTime.Today.Year.ToString().Substring(2, 2));
            var localBatchNumber = this.DataManager.GetBatchNumber(reference);
            if (localBatchNumber != null)
            {
                return localBatchNumber;
            }

            return this.GenerateManualEntryNumber(reference);
        }
       
        /// <summary>
        /// Generates a batch number depending on the requested mode.
        /// </summary>
        /// <returns>A generated batch number.</returns>
        public BatchNumber GenerateBatchNumber(bool manualMode, string reference, string manualBatchNo = "", string colour = "Orange", bool referenceOnly = false)
        {
            if (ApplicationSettings.BatchNumberType == BatchNumberType.Colour)
            {
                return this.GenerateColourBatchNumber(colour);
            }

            if (manualMode)
            {
                return this.GenerateManualEntryNumber(manualBatchNo);
            }

            return this.GenerateSequentialNumber(reference, referenceOnly);
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Generates a sequential batch number.
        /// </summary>
        /// <returns>A sequential batch number.</returns>
        private BatchNumber GenerateSequentialNumber(string reference = "", bool referenceOnly = false)
        {
            if (ApplicationSettings.UseHistoricProductionBatchNo)
            {
                reference = string.Empty;
                referenceOnly = false;
            }

            return this.DataManager.GenerateSequentialNumber(reference, referenceOnly);
        }

        /// <summary>
        /// Generates a manual entry batch number.
        /// </summary>
        /// <param name="batchNo">The manual batch number entered by the operator.</param>
        /// <returns>A neww generated batch number.</returns>
        private BatchNumber GenerateManualEntryNumber(string batchNo)
        {
            var batchNumber = new BatchNumber {Number = batchNo, CreatedDate = DateTime.Today};
            return this.DataManager.GenerateManualEntryNumber(batchNumber);
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="SchedulerManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.Global;
using Nouvem.Properties;

namespace Nouvem.BusinessLogic
{
    using System.Diagnostics;
    using System.Windows;
    using Quartz.Impl;
    using Quartz;

    public class SchedulerManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly SchedulerManager Manager = new SchedulerManager();

        #endregion

        #region constructor

        private SchedulerManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the batch manager singleton.
        /// </summary>
        public static SchedulerManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Checks for future prices to be implemented.
        /// </summary>
        public void RestartApplication()
        {
            if (!ApplicationSettings.RestartApplication)
            {
                return;
            }

            try
            {
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var specialPricesJob = JobBuilder.Create<RestartApplication>()
                    .WithIdentity("RestartApplicationJob")
                    .Build();

                var specialPricesTrigger = TriggerBuilder.Create()
                  .WithIdentity("RestartApplicationTrigger")
                  .StartNow()
                  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.RestartApplicationHour, ApplicationSettings.RestartApplicationMin))
                  .ForJob("RestartApplicationJob")
                  .Build();

                sched.ScheduleJob(specialPricesJob, specialPricesTrigger);

                this.Log.LogDebug(this.GetType(), string.Format("Application set to restart at:{0}:{1}", ApplicationSettings.RestartApplicationHour, ApplicationSettings.RestartApplicationMin));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        #endregion

        #endregion

        #region private

       
        #endregion
    }

    public class RestartApplication : IJob
    {
        /// <summary>
        /// Restarts the application.
        /// </summary>
        /// <param name="context">The job context.</param>
        public void Execute(IJobExecutionContext context)
        {
            this.LogOut();
        }

        /// <summary>
        /// Restarts the application.
        /// </summary>
        private void LogOut()
        {
            Settings.Default.AllowMultipleInstancesOfApplication = true;
            Settings.Default.Save();
            //ViewModelLocator.CleanUpTouchscreens();
            var data = DataManager.Instance;
            data.LogOut();
            //this.DataManager.SaveDeviceSettings();
            System.Windows.Forms.Application.Restart();
            Application.Current.Shutdown();
            Process.GetCurrentProcess().Kill();
        }
    }
}

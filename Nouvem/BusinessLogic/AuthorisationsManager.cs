﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.BusinessLogic
{
    public class AuthorisationsManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly AuthorisationsManager Manager = new AuthorisationsManager();

        #endregion

        #region constructor

        private AuthorisationsManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the accounts manager singleton.
        /// </summary>
        public static AuthorisationsManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can dispatch products with no price.
        /// </summary>
        public bool AllowUserToDispatchZeroPricedProduct { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can change the delivery date on a dispatch on the touchscreen.
        /// </summary>
        public bool AllowUserToChangeDeliveryDateOnDispatchOrderAtTouchscreen { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can remove a production order product.
        /// </summary>
        public bool AllowUserToRemoveProductFromProductionOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can dispatch over the order line limits.
        /// </summary>
        public bool AllowUserToDispatchOverOrderAmounts { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can complete a purchase order.
        /// </summary>
        public bool AllowUserToCompletePurchaseOrders { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can enter an animal at lairage that hasn't been on the current farm long enough.
        /// </summary>
        public bool AllowUserEnterAnimalThatDoesNotHaveRequiredCurrentFarmResidency { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update a partner.
        /// </summary>
        public bool AllowUserToAddOrUpdateBusinessPartners { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update a purchase orders.
        /// </summary>
        public bool AllowUserToAddOrUpdatePurchaseOrders { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update intakes.
        /// </summary>
        public bool AllowUserToAddOrUpdateIntakes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update intakes.
        /// </summary>
        public bool AllowUserToEditCarcass { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update sale orders.
        /// </summary>
        public bool AllowUserToAddOrUpdateSaleOrders { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update dispatches.
        /// </summary>
        public bool AllowUserToAddOrUpdateDispatches { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add or update products.
        /// </summary>
        public bool AllowUserToAddOrUpdateProducts { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can edit transactions.
        /// </summary>
        public bool AllowUserToEditTransactions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user remove sale order products.
        /// </summary>
        public bool AllowUserToRemoveProductFromSaleOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can override credit limits.
        /// </summary>
        public bool AllowUserToOverrideOrderLimit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can create orders over the credit limit.
        /// </summary>
        public bool AllowUserToCreateSaleOrderExceedingCreditLimit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can create dispatches.
        /// </summary>
        public bool AllowUserToCreateDispatches { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can complete dispatches.
        /// </summary>
        public bool AllowUserToCompleteDispatches { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can add products to dispatches.
        /// </summary>
        public bool AllowUserToAddProductsToDispatches { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can complete an unfilled order.
        /// </summary>
        public bool AllowUserToCompleteAnUnfilledOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can re-order the kill line.
        /// </summary>
        public bool AllowUserToReOrderCarcassesAtSequencer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can delete labels.
        /// </summary>
        public bool AllowUserToDeleteLabel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can select any animal to print a label for at the hide station.
        /// </summary>
        public bool AllowUserToSelectAnyAnimalAtSequencerHideStation { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the user can delete labels.
        /// </summary>
        public bool AllowUserToDeleteLabelBySelectingTileInLabelHistory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can close nouvem on the touchscreen.
        /// </summary>
        public bool AllowUserToShutdownTouchscreenPC { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can make a concession at dispatch.
        /// </summary>
        public bool AllowUserToMakeConcessions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can enter a manual weight on the touchscreens.
        /// </summary>
        public bool AllowUserToEnterManualWeightOnTouchscreen { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the users authorisations.
        /// </summary>
        public void SetAuthorisations()
        {
            this.AllowUserToAddOrUpdateBusinessPartners = this.GetAuthorisation(Authorisation.AllowUserToAddOrUpdateBusinessPartners).AuthorisationToBool();
            this.AllowUserToEditTransactions = this.GetAuthorisation(Authorisation.AllowUserToEditTransactions).AuthorisationToBool();
            this.AllowUserToAddOrUpdatePurchaseOrders = this.GetAuthorisation(Authorisation.AllowUserToAddOrUpdatePurchaseOrder).AuthorisationToBool();
            this.AllowUserToAddOrUpdateIntakes = this.GetAuthorisation(Authorisation.AllowUserToAddOrUpdateIntakes).AuthorisationToBool();
            this.AllowUserToAddOrUpdateSaleOrders = this.GetAuthorisation(Authorisation.AllowUserToAddOrUpdateSaleOrders).AuthorisationToBool();
            this.AllowUserToAddOrUpdateDispatches = this.GetAuthorisation(Authorisation.AllowUserToAddOrUpdateDispatches).AuthorisationToBool();
            this.AllowUserToAddOrUpdateProducts = this.GetAuthorisation(Authorisation.AllowUserToAddOrUpdateProducts).AuthorisationToBool();
            this.AllowUserToRemoveProductFromSaleOrder = this.GetAuthorisation(Authorisation.AllowUserToRemoveProductFromSaleOrder).AuthorisationToBool();
            this.AllowUserToOverrideOrderLimit = this.GetAuthorisation(Authorisation.AllowUserToOverrideOrderLimit).AuthorisationToBool();
            this.AllowUserToCreateSaleOrderExceedingCreditLimit = this.GetAuthorisation(Authorisation.AllowUserToCreateSaleOrderExceedingCreditLimit).AuthorisationToBool();
            this.AllowUserToCreateDispatches = this.GetAuthorisation(Authorisation.AllowUserToCreateDispatches).AuthorisationToBool();
            this.AllowUserToCompleteDispatches = this.GetAuthorisation(Authorisation.AllowUserToCompleteDisptaches).AuthorisationToBool();
            this.AllowUserToAddProductsToDispatches = this.GetAuthorisation(Authorisation.AllowUserToAddProductToDispatchOrder).AuthorisationToBool();
            this.AllowUserToCompleteAnUnfilledOrder = this.GetAuthorisation(Authorisation.AllowUserToCompleteAnUnfilledOrder).AuthorisationToBool();
            this.AllowUserToReOrderCarcassesAtSequencer = this.GetAuthorisation(Authorisation.AllowUserToReorderCarcassesAtSequencer).AuthorisationToBool();
            this.AllowUserToDeleteLabel = this.GetAuthorisation(Authorisation.AllowUserToDeleteLabel).AuthorisationToBool();
            this.AllowUserToShutdownTouchscreenPC = this.GetAuthorisation(Authorisation.AllowUserToShutDownPC).AuthorisationToBool();
            this.AllowUserToMakeConcessions = this.GetAuthorisation(Authorisation.AllowUserToMakeConcession).AuthorisationToBool();
            this.AllowUserToRemoveProductFromProductionOrder = this.GetAuthorisation(Authorisation.AllowUserToRemoveProductFromProductionOrder).AuthorisationToBool();
            this.AllowUserToSelectAnyAnimalAtSequencerHideStation = this.GetAuthorisation(Authorisation.AllowUserToSelectAnyAnimalAtSequencerHideStation).AuthorisationToBool();
            this.AllowUserToEditCarcass = this.GetAuthorisation(Authorisation.AllowUserToEditCarcass).AuthorisationToBool();
            this.AllowUserToChangeDeliveryDateOnDispatchOrderAtTouchscreen = this.GetAuthorisation(Authorisation.AllowUserToChangeDeliveryDateOnDispatchOrderAtTouchscreen).AuthorisationToBool();
            this.AllowUserToEnterManualWeightOnTouchscreen = this.GetAuthorisation(Authorisation.AllowUserToEnterManualWeightOnTouchscreen).AuthorisationToBool();
            this.AllowUserToDeleteLabelBySelectingTileInLabelHistory = this.GetAuthorisation(Authorisation.AllowUserToDeleteLabelBySelectingTileInLabelHistory).AuthorisationToBool();
            this.AllowUserToDispatchZeroPricedProduct = this.GetAuthorisation(Authorisation.AllowUserToDispatchZeroPricedProduct).AuthorisationToBool();
            this.AllowUserToCompletePurchaseOrders = this.GetAuthorisation(Authorisation.AllowUserToCompletePurchaseOrders).AuthorisationToBool();
            this.AllowUserEnterAnimalThatDoesNotHaveRequiredCurrentFarmResidency = this.GetAuthorisation(Authorisation.AllowUserEnterAnimalThatDoesNotHaveRequiredCurrentFarmResidency).AuthorisationToBool();
            this.AllowUserToDispatchOverOrderAmounts = this.GetAuthorisation(Authorisation.AllowUserToDispatchOverOrderAmounts).AuthorisationToBool();
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Check an individual authorisation.
        /// </summary>
        /// <param name="authorisationToCheck">The individual authorisation to check.</param>
        /// <returns>A flag, indicating the authorisation level.</returns>
        private string GetAuthorisation(string authorisationToCheck)
        {
            var companyAuthorisationValue = Constant.NoAccess;

            try
            {
                if (NouvemGlobal.IsNouvemUser)
                {
                    return Constant.FullAccess;
                }

                if (NouvemGlobal.Licensee == null)
                {
                    return Constant.NoAccess;
                }

                if (NouvemGlobal.LoggedInUser == null)
                {
                    return Constant.NoAccess;
                }

                if ((!NouvemGlobal.Licensee.IsDevice && NouvemGlobal.Licensee.LicenceStatus != LicenceStatus.Valid))
                {
                    return Constant.NoAccess;
                }

                // get the overriding licence authorisation.
                //var authorisations = NouvemGlobal.Licensee.Authorisations;

                //if (authorisations == null)
                //{
                //    return Constant.NoAccess;
                //}

                //var localAuthorisation = authorisations.FirstOrDefault(x => x.Key.Equals(authorisationToCheck));

                //if (localAuthorisation.Key == null)
                //{
                //    // authorisation doesn't exist for current customer, so allow.
                //    return Constant.FullAccess;
                //}

                //if (localAuthorisation.Value == null)
                //{
                //    return Constant.NoAccess;
                //}

                //if (localAuthorisation.Value.Equals(Constant.NoAccess))
                //{
                //    // licence says no access, so no need to check the company authorisations.
                //    return Constant.NoAccess;
                //}

                // get the company authorisation value.
                var companyAuthorisation =
                     NouvemGlobal.GroupAuthorisations.FirstOrDefault(x => x.AuthorisationListDescription.Equals(authorisationToCheck) && x.UserGroupID == NouvemGlobal.LoggedInUser.Group.UserGroupID);

                if (companyAuthorisation != null)
                {
                    companyAuthorisationValue = companyAuthorisation.AuthorisationValueDescription;
                }
                else
                {
                    companyAuthorisationValue = Constant.NoAccess;
                }

                //if (localAuthorisation.Value.Equals(Constant.ReadOnly))
                //{
                //    // licence says read only, so we need to check the company authorisations for a no access.
                //    return companyAuthorisationValue.Equals(Constant.NoAccess) ? Constant.NoAccess : Constant.ReadOnly;
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }

            // license is full access, so return the company authorisation value.
            return companyAuthorisationValue;
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="PrintManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Model.BusinessObject;
using Nouvem.View.Utility;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO.Ports;
    using System.Linq;
    using System.Net;
    using GalaSoft.MvvmLight.Messaging;
    using Neodynamic.SDK.Printing;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Global;
    using Nouvem.Hardware;
    using Nouvem.Shared;

    /// <summary>
    /// Manager class for the application hardware.
    /// </summary>
    public class PrintManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly PrintManager Manager = new PrintManager();

        /// <summary>
        /// The printers.
        /// </summary>
        private IList<Nouvem.Model.DataLayer.Printer> printers = new List<Nouvem.Model.DataLayer.Printer>();

        /// <summary>
        /// The printer settings.
        /// </summary>
        private PrinterSettings printerSettings = new PrinterSettings();

        /// <summary>
        /// The printer2 settings.
        /// </summary>
        private PrinterSettings printer2Settings = new PrinterSettings();

        /// <summary>
        /// The printer2 settings.
        /// </summary>
        private PrinterSettings printer3Settings = new PrinterSettings();

        /// <summary>
        /// The loaded in label.
        /// </summary>
        private ThermalLabel loadedLabel = new ThermalLabel();

        /// <summary>
        /// External printer reference to non nouvem printer.
        /// </summary>
        private Nouvem.Hardware.Printer externalPrinter;

        /// <summary>
        /// External printer reference to non nouvem printer2.
        /// </summary>
        private Nouvem.Hardware.Printer externalPrinter2;

        /// <summary>
        /// External printer reference to non nouvem printer2.
        /// </summary>
        private Nouvem.Hardware.Printer externalPrinter3;

        /// <summary>
        /// The manually changed label flag.
        /// </summary>
        private bool labelManuallyChanged;

        /// <summary>
        /// The manually changed label template.
        /// </summary>
        private Label manuallyChangedLabel;

        private ImageView imageViewer;

        #endregion

        #region constructor

        private PrintManager()
        {
            this.SetUpPrinter();
            this.GetPrinters();

            Messenger.Default.Register<string>(this, Token.ResetLabel, s => this.labelManuallyChanged = false);
            Messenger.Default.Register<string>(this, Token.LabelViewerClosed, s => this.imageViewer = null);

            // Register for the label change.
            Messenger.Default.Register<Label>(this, Token.LabelChange, l =>
            {
                this.manuallyChangedLabel = l;
                this.labelManuallyChanged = true;
            });
        }

        #endregion

        #region property

        /// <summary>
        /// Gets the print manager singleton.
        /// </summary>
        public static PrintManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the printer is connected.
        /// </summary>
        public bool IsPrinterConnected { get; set; }

        #endregion

        #region method
        public void TestPrint(string path = @"C:\Nouvem\Label")
        {
            if (this.imageViewer != null)
            {
                this.imageViewer.Image.Source = null;
            }

            var name = $"LabelImage{DateTime.Now.ToString("ddMMyyhhmmss")}.Png";
            this.ExportToImage(this.loadedLabel, false, "Png", path,name);
            if (this.imageViewer == null)
            {
                this.imageViewer = new ImageView();
                this.imageViewer.Show();
            }

            path = Path.Combine(path, name);
            this.imageViewer.ImagePath = path;
        }

        public void ExportLabelImage(Label label, int id, string path)
        {
            this.loadedLabel = new ThermalLabel();
            this.loadedLabel.LoadXmlTemplate(label.LabelFile);

            System.Data.DataTable dataSource = this.DataManager.GetPalletCardData(id);
            this.Log.LogInfo(this.GetType(), "Data source retrieved");

            this.loadedLabel.DataSource = dataSource;
            this.ExportToImage(this.loadedLabel, false, "Png", path);
        }

        public void ExportLabelImageMultiBatch(Label label, int id, string path, decimal wgt, decimal qty, int batchNumberId, string imageName)
        {
            this.loadedLabel = new ThermalLabel();
            this.loadedLabel.LoadXmlTemplate(label.LabelFile);

            System.Data.DataTable dataSource = this.DataManager.GetPalletCardDataMultiBatch(id, wgt, qty, batchNumberId);
            this.Log.LogInfo(this.GetType(), "Data source retrieved");

            this.loadedLabel.DataSource = dataSource;
            this.ExportToImage(this.loadedLabel, false, "Png", path, imageName);
        }

        /// <summary>
        /// Exports a label to image.
        /// </summary>
        /// <param name="label">The label to export.</param>
        /// <param name="print">Flag, which indicates whether the label is to be printed.</param>
        public string ExportToImage(ThermalLabel label, bool print, string type, string path,string imageName = "LabelImage.Png")
        {
            try
            {

                var fileName = Path.Combine(path, imageName);
                //Create a PrintJob object
                //using (PrintJob pj = new PrintJob())
                //{
                //    //Set number of copies
                //    pj.Copies = 1;
                //    //Save output to image...
                //    pj.ExportToImage(tLabel, fileName, new ImageSettings(ImageFormat.Png), 300);
                //    return "";
                //}


                var imageFormat = (ImageFormat)Enum.Parse(typeof(ImageFormat), type);
                using (var printJob = new PrintJob())
                {

                    printJob.ThermalLabel = label;
                    printJob.ExportToImage(fileName, new ImageSettings(imageFormat), 203);
                    //this.Print();

                    if (print)
                    {
                        this.PrintLabel(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                return Message.LabelExportError;
            }

            return string.Empty;
        }

        /// <summary>
        /// Prints a label image.
        /// </summary>
        /// <param name="file">The file path of the label to print.</param>
        public void PrintLabel(string file)
        {
            var pd = new PrintDocument();

            // Disable the print pop up.
            pd.PrintController = new StandardPrintController();

            // Set the default margins
            pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            pd.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

            try
            {
                pd.PrintPage += (sender, args) =>
                {
                    System.Drawing.Image i = System.Drawing.Image.FromFile(file);

                    // Adjust the size of the image to the page to print the full image without losing any part of the image.
                    System.Drawing.Rectangle m = args.MarginBounds;

                    // Logic below maintains the aspect ratio.
                    if ((double)i.Width / (double)i.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)i.Height / (double)i.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)i.Width / (double)i.Height * (double)m.Height);
                    }

                    // Calculating optimal orientation.
                    pd.DefaultPageSettings.Landscape = m.Width > m.Height;

                    // Putting image in center of page.
                    m.Y = (int)((((PrintDocument)(sender)).DefaultPageSettings.PaperSize.Height - m.Height) / 2);
                    m.X = (int)((((PrintDocument)(sender)).DefaultPageSettings.PaperSize.Width - m.Width) / 2);
                    args.Graphics.DrawImage(i, m);
                };

                pd.Print();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Sends a command to the printer.
        /// </summary>
        /// <param name="printerCommand">The command to send.</param>
        /// <param name="allPrinters">Sending to all printers flag.</param>
        /// <param name="printerNo">The printer no.</param>
        public void SendCommand(string printerCommand, bool allPrinters, int printerNo = 1)
        {
            var command = printerCommand ?? string.Empty;
            if (allPrinters)
            {
                if (this.printerSettings != null)
                {
                    PrintUtils.PrinterSettings = this.printerSettings;
                    PrintUtils.ExecuteCommand(command);
                    this.Log.LogInfo(this.GetType(), string.Format("SendCommand(): {0} sent to printer 1", command));
                }

                if (this.printer2Settings != null)
                {
                    PrintUtils.PrinterSettings = this.printer2Settings;
                    PrintUtils.ExecuteCommand(command);
                    this.Log.LogInfo(this.GetType(), string.Format("SendCommand(): {0} sent to printer 2", command));
                }

                return;
            }

            if (printerNo == 1)
            {
                if (this.printerSettings != null)
                {
                    PrintUtils.PrinterSettings = this.printerSettings;
                    PrintUtils.ExecuteCommand(command);
                    this.Log.LogInfo(this.GetType(), string.Format("SendCommand(): {0} sent to printer 1", command));
                }

                return;
            }

            if (printerNo == 2)
            {
                if (this.printer2Settings != null)
                {
                    PrintUtils.PrinterSettings = this.printer2Settings;
                    PrintUtils.ExecuteCommand(command);
                    this.Log.LogInfo(this.GetType(), string.Format("SendCommand(): {0} sent to printer 2", command));
                }

                return;
            }
        }

        public void OpenPrinterPort()
        {
            if (!ApplicationSettings.UseExternalLabels)
            {
                return;
            }

            try
            {
                this.Log.LogInfo(this.GetType(), "Opening Printer Port..");
                try
                {
                    this.externalPrinter.OpenPort();
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), string.Format("OpenPrinterPort() Printer1:{0}", ex.Message));
                }
                
                this.Log.LogInfo(this.GetType(), string.Format("External printer port open:{0}", this.externalPrinter.IsPortOpen));

                if (ApplicationSettings.UseExternalLabels2)
                {
                    this.Log.LogInfo(this.GetType(), "Opening Printer Port 2..");
                    try
                    {
                        this.externalPrinter2.OpenPort();
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), string.Format("OpenPrinterPort() Printer2:{0}", ex.Message));
                    }

                    this.Log.LogInfo(this.GetType(), string.Format("External printer 2 port open:{0}", this.externalPrinter2.IsPortOpen));
                }

                if (ApplicationSettings.UseExternalLabels3)
                {
                    this.Log.LogInfo(this.GetType(), "Opening Printer Port 3..");
                    try
                    {
                        this.externalPrinter3.OpenPort();
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), string.Format("OpenPrinterPort() Printer3:{0}", ex.Message));
                    }

                    this.Log.LogInfo(this.GetType(), string.Format("External printer 3 port open:{0}", this.externalPrinter3.IsPortOpen));
                }
            }
            catch (Exception ex)
            {
               this.Log.LogError(this.GetType(), string.Format("OpenPrinterPort():{0}", ex.Message));
            }
        }

        /// <summary>
        /// Sets up the printer settings.
        /// </summary>
        public void SetUpPrinter()
        {
            this.SetUpPrinter1();
            this.SetUpPrinter2();
            this.SetUpPrinter3();
        }

        /// <summary>
        /// Sets up the printer settings.
        /// </summary>
        public void SetUpPrinter1()
        {
            this.Log.LogInfo(this.GetType(), "SetUpPrinter1(): Setting up printer..");
            try
            {
                if (string.IsNullOrEmpty(ApplicationSettings.PrinterCommunicationType))
                {
                    this.Log.LogInfo(this.GetType(), "Printer1 not in use.");
                    return;
                }

                var communication = (CommunicationType)Enum.Parse(typeof(CommunicationType), ApplicationSettings.PrinterCommunicationType);
                if (communication == CommunicationType.Network)
                {
                    this.Log.LogInfo(this.GetType(), string.Format("Communication:{0}, IP:{1}, DPI:{2}, In Use:{3}", communication, ApplicationSettings.PrinterSettings, ApplicationSettings.PrinterDPI, ApplicationSettings.IsPrinterInUse));
                    var settings = ApplicationSettings.PrinterSettings.Split(',');
                    var networkAddress = settings[0];
                    var port = settings[1].ToInt();

                    this.printerSettings.Communication.CommunicationType = communication;
                    this.printerSettings.Communication.NetworkIPAddress = IPAddress.Parse(networkAddress);
                    this.printerSettings.Communication.NetworkPort = port;
                    this.printerSettings.Dpi = ApplicationSettings.PrinterDPI;
                    this.printerSettings.ProgrammingLanguage = ApplicationSettings.PrinterLanguage;
                    return;
                }

                if (communication == CommunicationType.USB)
                {
                    this.printerSettings.Communication.CommunicationType = communication;
                    this.printerSettings.PrinterName = ApplicationSettings.PrinterSettings;
                    this.printerSettings.UseDefaultPrinter = false;
                    this.printerSettings.Dpi = ApplicationSettings.PrinterDPI;
                    this.printerSettings.ProgrammingLanguage = ApplicationSettings.PrinterLanguage;
                    this.Log.LogInfo(this.GetType(), string.Format("Communication:{0}, Printer Name:{1}, DPI:{2}, In Use:{3}", communication, ApplicationSettings.PrinterSettings, ApplicationSettings.PrinterDPI, ApplicationSettings.IsPrinterInUse));
                    return;
                }

                if (communication == CommunicationType.Serial)
                {
                    var settings = ApplicationSettings.PrinterSettings.Split(',');
                    var portName = settings[0];
                    var baud = settings[1].ToInt();
                    var parity = (Parity)Enum.Parse(typeof(Parity), settings[2]);
                    var dataBits = settings[3].ToInt();
                    var stopBits = (StopBits)Enum.Parse(typeof(StopBits), settings[4]);
                    var handshake = (Handshake)Enum.Parse(typeof(Handshake), settings[5]);

                    this.printerSettings.Communication.CommunicationType = communication;
                    this.printerSettings.Communication.SerialPortName = portName;
                    this.printerSettings.Communication.SerialPortBaudRate = baud;
                    this.printerSettings.Communication.SerialPortParity = parity;
                    this.printerSettings.Communication.SerialPortDataBits = dataBits;
                    this.printerSettings.Communication.SerialPortStopBits = stopBits;
                    this.printerSettings.Communication.SerialPortFlowControl = handshake;
                    this.printerSettings.Dpi = ApplicationSettings.PrinterDPI;
                    this.printerSettings.ProgrammingLanguage = ApplicationSettings.PrinterLanguage;

                    this.Log.LogInfo(this.GetType(), string.Format("Using external printer:{0}, COM:{1}, Baud:{2}, Parity:{3}, Databits:{4}, Stopbits:{5}, Handshake:{6}", 
                        ApplicationSettings.UseExternalLabels, portName,baud,parity,dataBits,stopBits,handshake));
                    if (ApplicationSettings.UseExternalLabels)
                    {
                        try
                        {
                            this.externalPrinter = new Nouvem.Hardware.Printer(portName, baud.ToString(), settings[2], settings[4], settings[3]);
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Sets up the printer settings.
        /// </summary>
        public void SetUpPrinter2()
        {
            this.Log.LogInfo(this.GetType(), "SetUpPrinter2(): Setting up printer2..");
            
            try
            {
                if (string.IsNullOrEmpty(ApplicationSettings.Printer2CommunicationType))
                {
                    this.Log.LogInfo(this.GetType(), "Printer2 not in use.");
                    return;
                }

                var communication = (CommunicationType)Enum.Parse(typeof(CommunicationType), ApplicationSettings.Printer2CommunicationType);
                if (communication == CommunicationType.Network)
                {
                    this.Log.LogInfo(this.GetType(), string.Format("Communication:{0}, IP:{1}, DPI:{2}, In Use:{3}", communication, ApplicationSettings.Printer2Settings, ApplicationSettings.Printer2DPI, ApplicationSettings.IsPrinter2InUse));
                    var settings = ApplicationSettings.Printer2Settings.Split(',');
                    var networkAddress = settings[0];
                    var port = settings[1].ToInt();

                    this.printer2Settings.Communication.CommunicationType = communication;
                    this.printer2Settings.Communication.NetworkIPAddress = IPAddress.Parse(networkAddress);
                    this.printer2Settings.Communication.NetworkPort = port;
                    this.printer2Settings.Dpi = ApplicationSettings.Printer2DPI;
                    this.printer2Settings.ProgrammingLanguage = ApplicationSettings.Printer2Language;
                    return;
                }

                if (communication == CommunicationType.USB)
                {
                    this.printer2Settings.Communication.CommunicationType = communication;
                    this.printer2Settings.PrinterName = ApplicationSettings.Printer2Settings;
                    this.printer2Settings.UseDefaultPrinter = false;
                    this.printer2Settings.Dpi = ApplicationSettings.Printer2DPI;
                    this.printer2Settings.ProgrammingLanguage = ApplicationSettings.Printer2Language;
                    this.Log.LogInfo(this.GetType(), string.Format("Communication:{0}, Printer Name:{1}, DPI:{2}, In Use:{3}", communication, ApplicationSettings.Printer2Settings, ApplicationSettings.Printer2DPI, ApplicationSettings.IsPrinter2InUse));
                    return;
                }

                if (communication == CommunicationType.Serial)
                {
                    var settings = ApplicationSettings.Printer2Settings.Split(',');
                    var portName = settings[0];
                    var baud = settings[1].ToInt();
                    var parity = (Parity)Enum.Parse(typeof(Parity), settings[2]);
                    var dataBits = settings[3].ToInt();
                    var stopBits = (StopBits)Enum.Parse(typeof(StopBits), settings[4]);
                    var handshake = (Handshake)Enum.Parse(typeof(Handshake), settings[5]);

                    this.printer2Settings.Communication.CommunicationType = communication;
                    this.printer2Settings.Communication.SerialPortName = portName;
                    this.printer2Settings.Communication.SerialPortBaudRate = baud;
                    this.printer2Settings.Communication.SerialPortParity = parity;
                    this.printer2Settings.Communication.SerialPortDataBits = dataBits;
                    this.printer2Settings.Communication.SerialPortStopBits = stopBits;
                    this.printer2Settings.Communication.SerialPortFlowControl = handshake;
                    this.printer2Settings.Dpi = ApplicationSettings.Printer2DPI;
                    this.printer2Settings.ProgrammingLanguage = ApplicationSettings.Printer2Language;

                    this.Log.LogInfo(this.GetType(), string.Format("Using external printer2:{0}, COM:{1}, Baud:{2}, Parity:{3}, Databits:{4}, Stopbits:{5}, Handshake:{6}",
                        ApplicationSettings.UseExternalLabels2, portName, baud, parity, dataBits, stopBits, handshake));
                    if (ApplicationSettings.UseExternalLabels2)
                    {
                        try
                        {
                            this.externalPrinter2 = new Nouvem.Hardware.Printer(portName, baud.ToString(), settings[2], settings[4], settings[3]);
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Sets up the printer settings.
        /// </summary>
        public void SetUpPrinter3()
        {
            this.Log.LogInfo(this.GetType(), "SetUpPrinter3(): Setting up printer2..");

            try
            {
                if (string.IsNullOrEmpty(ApplicationSettings.Printer3CommunicationType))
                {
                    this.Log.LogInfo(this.GetType(), "Printer3 not in use.");
                    return;
                }

                var communication = (CommunicationType)Enum.Parse(typeof(CommunicationType), ApplicationSettings.Printer3CommunicationType);
                if (communication == CommunicationType.Network)
                {
                    this.Log.LogInfo(this.GetType(), string.Format("Communication:{0}, IP:{1}, DPI:{2}, In Use:{3}", communication, ApplicationSettings.Printer3Settings, ApplicationSettings.Printer2DPI, ApplicationSettings.IsPrinter3InUse));
                    var settings = ApplicationSettings.Printer3Settings.Split(',');
                    var networkAddress = settings[0];
                    var port = settings[1].ToInt();

                    this.printer3Settings.Communication.CommunicationType = communication;
                    this.printer3Settings.Communication.NetworkIPAddress = IPAddress.Parse(networkAddress);
                    this.printer3Settings.Communication.NetworkPort = port;
                    this.printer3Settings.Dpi = ApplicationSettings.Printer3DPI;
                    this.printer3Settings.ProgrammingLanguage = ApplicationSettings.Printer3Language;
                    return;
                }

                if (communication == CommunicationType.USB)
                {
                    this.printer3Settings.Communication.CommunicationType = communication;
                    this.printer3Settings.PrinterName = ApplicationSettings.Printer3Settings;
                    this.printer3Settings.UseDefaultPrinter = false;
                    this.printer3Settings.Dpi = ApplicationSettings.Printer3DPI;
                    this.printer3Settings.ProgrammingLanguage = ApplicationSettings.Printer3Language;
                    this.Log.LogInfo(this.GetType(), string.Format("Communication:{0}, Printer Name:{1}, DPI:{2}, In Use:{3}", communication, ApplicationSettings.Printer3Settings, ApplicationSettings.Printer3DPI, ApplicationSettings.IsPrinter3InUse));
                    return;
                }

                if (communication == CommunicationType.Serial)
                {
                    var settings = ApplicationSettings.Printer3Settings.Split(',');
                    var portName = settings[0];
                    var baud = settings[1].ToInt();
                    var parity = (Parity)Enum.Parse(typeof(Parity), settings[2]);
                    var dataBits = settings[3].ToInt();
                    var stopBits = (StopBits)Enum.Parse(typeof(StopBits), settings[4]);
                    var handshake = (Handshake)Enum.Parse(typeof(Handshake), settings[5]);

                    this.printer3Settings.Communication.CommunicationType = communication;
                    this.printer3Settings.Communication.SerialPortName = portName;
                    this.printer3Settings.Communication.SerialPortBaudRate = baud;
                    this.printer3Settings.Communication.SerialPortParity = parity;
                    this.printer3Settings.Communication.SerialPortDataBits = dataBits;
                    this.printer3Settings.Communication.SerialPortStopBits = stopBits;
                    this.printer3Settings.Communication.SerialPortFlowControl = handshake;
                    this.printer3Settings.Dpi = ApplicationSettings.Printer3DPI;
                    this.printer3Settings.ProgrammingLanguage = ApplicationSettings.Printer3Language;

                    this.Log.LogInfo(this.GetType(), string.Format("Using external printer3:{0}, COM:{1}, Baud:{2}, Parity:{3}, Databits:{4}, Stopbits:{5}, Handshake:{6}",
                        ApplicationSettings.UseExternalLabels3, portName, baud, parity, dataBits, stopBits, handshake));
                    if (ApplicationSettings.UseExternalLabels3)
                    {
                        try
                        {
                            this.externalPrinter3 = new Nouvem.Hardware.Printer(portName, baud.ToString(), settings[2], settings[4], settings[3]);
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Closes the port.
        /// </summary>
        public void ClosePrinterPort()
        {
            this.Log.LogDebug(this.GetType(), "ClosePrinterPort(): Closing the printer connection");
            if (this.externalPrinter != null && this.externalPrinter.IsPortOpen)
            {
                try
                {
                    this.externalPrinter.ClosePort();
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), string.Format("Close Printer 1 Port:{0}", e.Message));
                }

                this.Log.LogDebug(this.GetType(), "Port closed");
            }

            if (this.externalPrinter2 != null && this.externalPrinter2.IsPortOpen)
            {
                try
                {
                    this.externalPrinter2.ClosePort();
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), string.Format("Close Printer 2 Port:{0}", e.Message));
                }

                this.Log.LogDebug(this.GetType(), "Port closed");
            }

            if (this.externalPrinter3 != null && this.externalPrinter3.IsPortOpen)
            {
                try
                {
                    this.externalPrinter3.ClosePort();
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), string.Format("Close Printer 3 Port:{0}", e.Message));
                }

                this.Log.LogDebug(this.GetType(), "Port closed");
            }
        }

        /// <summary>
        /// Process the print operation.
        /// </summary>
        /// <param name="stockTransactionId">The transaction id record to retrieve.</param>
        /// <returns>A collection of the label ids printed.</returns>
        public void ReprintLabel(int stockTransactionId, int? selectedPrinterNo = null)
        {
            //this.Log.LogInfo(this.GetType(), string.Format("ReprintLabel(): labelID:{0}, mode:{1}, labelType:{2}, transID:{3}", labelId, mode, labelType, stockTransactionId));

            if (ApplicationSettings.UseExternalLabels)
            {
                this.ReprintExternalLabel(stockTransactionId);
                return;
            }

            var localLabel = this.DataManager.GetLabelImages(stockTransactionId);
           
            if (localLabel == null)
            {
                this.Log.LogError(this.GetType(), "Label match not found");
                Messenger.Default.Send(new Label { Name = Message.NoMatchingLabel }, Token.ChangeDisplayLabelName);
                return;
            }

            var printerNo = localLabel.PrinterNo.IsNullOrZeroOrOne() ? 1 : localLabel.PrinterNo.ToInt();
            if (selectedPrinterNo.HasValue)
            {
                printerNo = selectedPrinterNo.ToInt();
            }

            if (ApplicationSettings.IsPrinterInUse)
            {
                if (printerNo == 1)
                {
                    PrintUtils.PrinterSettings = this.printerSettings;
                }
                else
                {
                    PrintUtils.PrinterSettings = this.printer2Settings;
                }

                if (localLabel.Label1 != null)
                {
                    var convertedLabel = localLabel.Label1.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label2 != null)
                {
                    var convertedLabel = localLabel.Label2.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label3 != null)
                {
                    var convertedLabel = localLabel.Label3.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label4 != null)
                {
                    var convertedLabel = localLabel.Label4.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label5 != null)
                {
                    var convertedLabel = localLabel.Label5.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label6 != null)
                {
                    var convertedLabel = localLabel.Label6.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label7 != null)
                {
                    var convertedLabel = localLabel.Label7.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }

                if (localLabel.Label8 != null)
                {
                    var convertedLabel = localLabel.Label8.ToStringFromStream();
                    PrintUtils.ExecuteCommand(convertedLabel);
                }
            }
        }

        /// <summary>
        /// Process the print operation.
        /// </summary>
        /// <param name="stockTransactionId">The transaction id record to retrieve.</param>
        /// <returns>A collection of the label ids printed.</returns>
        public void ViewLabel(int stockTransactionId)
        {
            var localLabel = this.DataManager.GetLabelImages(stockTransactionId);

            if (localLabel == null)
            {
                this.Log.LogError(this.GetType(), "Label match not found");
                Messenger.Default.Send(new Label { Name = Message.NoMatchingLabel }, Token.ChangeDisplayLabelName);
                return;
            }

            if (localLabel.Label1 != null)
            {
                var convertedLabel = localLabel.Label1.ToStringFromStream();
                PrintUtils.ExecuteCommand(convertedLabel);
            }

            if (localLabel.Label2 != null)
            {
                var convertedLabel = localLabel.Label2.ToStringFromStream();
                PrintUtils.ExecuteCommand(convertedLabel);
            }
        }

        /// <summary>
        /// Process the print operation.
        /// </summary>
        /// <param name="stockTransactionId">The transaction id record to retrieve.</param>
        /// <returns>A collection of the label ids printed.</returns>
        public void ReprintExternalLabel(int stockTransactionId)
        {
            var localLabel = this.DataManager.GetLabelImages(stockTransactionId);
            if (localLabel == null)
            {
                this.Log.LogError(this.GetType(), "Label match not found");
                Messenger.Default.Send(new Label { Name = Message.NoMatchingLabel }, Token.ChangeDisplayLabelName);
                return;
            }

            var printerNo = localLabel.PrinterNo.IsNullOrZeroOrOne() ? 1 : localLabel.PrinterNo.ToInt();

            if (ApplicationSettings.IsPrinterInUse)
            {
                if (localLabel.Label1 != null)
                {
                    var convertedLabel = localLabel.Label1.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label2 != null)
                {
                    var convertedLabel = localLabel.Label2.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label3 != null)
                {
                    var convertedLabel = localLabel.Label3.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label4 != null)
                {
                    var convertedLabel = localLabel.Label4.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label5 != null)
                {
                    var convertedLabel = localLabel.Label5.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label6 != null)
                {
                    var convertedLabel = localLabel.Label6.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label7 != null)
                {
                    var convertedLabel = localLabel.Label7.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }

                if (localLabel.Label8 != null)
                {
                    var convertedLabel = localLabel.Label8.ToStringFromStream();
                    this.RePrint(convertedLabel, printerNo);
                }
            }
        }

        /// <summary>
        /// Reprints the label.
        /// </summary>
        /// <param name="numCopies">The number of labels to print.</param>
        public string RePrint(string data, int printerNo = 1, int numCopies = 1)
        {
            var labelText = data;

            try
            {
                for (int i = 0; i < numCopies; i++)
                {
                    if (printerNo == 1)
                    {
                        this.externalPrinter.WriteToPort(labelText);
                    }
                    else if (printerNo == 2)
                    {
                        this.externalPrinter2.WriteToPort(labelText);
                    }
                    else
                    {
                        this.externalPrinter3.WriteToPort(labelText);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Ex:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw new Exception(Message.PrinterConnectionIssue);
            }

            return labelText;
        }

        /// <summary>
        /// Process the print operation.
        /// </summary>
        /// <param name="labelId">The label id.</param>
        /// <param name="mode">The station we are printng at.</param>
        /// <param name="labelType">The label type required.</param>
        /// <param name="stockTransactionId">The transaction id record to retrieve.</param>
        /// <returns>A collection of the label ids printed.</returns>
        [Obsolete]
        public void ReprintLabel(int labelId, ViewType mode, LabelType labelType, int stockTransactionId)
        {
            this.Log.LogInfo(this.GetType(), string.Format("ReprintLabel(): labelID:{0}, mode:{1}, labelType:{2}, transID:{3}", labelId, mode, labelType, stockTransactionId));

            var localLabel = LabelManager.Instance.GetLabel(labelId);
            if (localLabel == null)
            {
                this.Log.LogError(this.GetType(), "Label match not found");
                Messenger.Default.Send(new Label { Name = Message.NoMatchingLabel }, Token.ChangeDisplayLabelName);
                return;
            }

            Messenger.Default.Send(localLabel, Token.ChangeDisplayLabelName);

            this.loadedLabel = new ThermalLabel();
            this.loadedLabel.LoadXmlTemplate(localLabel.LabelFile);
            this.Log.LogInfo(this.GetType(), "Label loaded");

            System.Data.DataTable dataSource = null;
            if (mode == ViewType.APReceipt)
            {
                dataSource = this.DataManager.GetIntakeLabelData(stockTransactionId);
            }
            else if (mode == ViewType.OutOfProduction)
            {
                dataSource = this.DataManager.GetOutOfProductionLabelData(stockTransactionId);
            }
            else if (mode == ViewType.ARDispatch)
            {
                dataSource = this.DataManager.GetDispatchLabelData(stockTransactionId);
            }

            this.loadedLabel.DataSource = dataSource; 
            this.SetMultiLineText(dataSource);

            var orientation = PrintOrientation.Portrait;
            if (!string.IsNullOrEmpty(localLabel.Orientation))
            {
                 orientation =  (PrintOrientation)Enum.Parse(typeof(PrintOrientation), localLabel.Orientation);
            }

            if (ApplicationSettings.IsPrinterInUse)
            {
                this.Print(orientation:orientation);
            }
        }

        /// <summary>
        /// Process the print operation.
        /// </summary>
        /// <param name="customerId">The customer id to search for.</param>
        /// <param name="customerGroupId">The customer group id to search for.</param>
        /// <param name="productId">The product id to search for.</param>
        /// <param name="productGroupId">The product group id to search for.</param>
        /// <param name="mode">The station we are printng at.</param>
        /// <param name="labelType">The label type required.</param>
        /// <param name="stockTransactionId">The transaction id record to retrieve.</param>
        /// <param name="numCopies">The number of labels required.</param>
        /// <returns>A collection of the label ids printed.</returns>
        public Tuple<List<int>, List<byte[]>, int> ProcessPrinting(
            int? customerId,
            int? customerGroupId,
            int? productId,
            int? productGroupId,
            ViewType mode,
            LabelType labelType,
            int stockTransactionId,
            int numCopies = 1,
            Process process = null,
            int? printer = null,
            int? warehouseId = null,
            bool batchLabel = false)
        {
            var labelids = new List<int>();
            var printerNo = 1;
            var storedLabels = new List<byte[]>();
        
            try
            {
                var localLabels = new List<LabelData>();
                if (this.labelManuallyChanged)
                {
                    localLabels.Add(new LabelData { Label = this.manuallyChangedLabel, LabelQty = 1 });
                }
                else
                {
                    var lab = LabelManager.Instance.GetLabels(customerId, customerGroupId, productId, productGroupId,
                        labelType, process, warehouseId, batchLabel);

                    if (lab != null)
                    {
                        localLabels = lab.ToList();
                    }
                    else
                    {
                        this.Log.LogError(this.GetType(), "Label match not found");
                        Messenger.Default.Send(new Label { Name = Message.NoMatchingLabel }, Token.ChangeDisplayLabelName);
                        throw new Exception(Message.NoLabelFound);
                    }
                }

                foreach (var localLabelData in localLabels)
                {
                    var localLabel = localLabelData.Label;
                    var labelQty = localLabelData.LabelQty;
                    var localPrinter = localLabelData.Printer;

                    if (numCopies > labelQty)
                    {
                        // piece label set to the default 1 in the association, so we use the product pieces value
                        labelQty = numCopies;
                    }

                    if (localLabel != null)
                    {
                        Messenger.Default.Send(localLabel, Token.ChangeDisplayLabelName);

                        this.loadedLabel = new ThermalLabel();

                        if (!ApplicationSettings.UseExternalLabels && !ApplicationSettings.UseExternalLabels2)
                        {
                            this.loadedLabel.LoadXmlTemplate(localLabel.LabelFile);
                            this.Log.LogDebug(this.GetType(), "Label loaded");
                        }

                        System.Data.DataTable dataSource = null;
                        if (mode == ViewType.APReceipt)
                        {
                            dataSource = this.DataManager.GetIntakeLabelData(stockTransactionId);
                        }
                        else if (mode == ViewType.IntoProduction)
                        {
                            dataSource = this.DataManager.GetIntoProductionLabelData(stockTransactionId);
                        }
                        else if (mode == ViewType.OutOfProduction)
                        {
                            dataSource = this.DataManager.GetOutOfProductionLabelData(stockTransactionId);
                        }
                        else if (mode == ViewType.ARDispatch)
                        {
                            dataSource = this.DataManager.GetDispatchLabelData(stockTransactionId);
                        }
                        else if (mode == ViewType.Grader)
                        {
                            dataSource = this.DataManager.GetGraderLabelData(stockTransactionId);
                        }
                        else if (mode == ViewType.Sequencer)
                        {
                            dataSource = this.DataManager.GetSequencerLabelData(stockTransactionId);
                        }
                        else if (mode == ViewType.StockMovement)
                        {
                            dataSource = this.DataManager.GetStockMovementData(stockTransactionId);
                        }
                        else if (mode == ViewType.Palletisation)
                        {
                            dataSource = this.DataManager.GetPalletLabelData(stockTransactionId);
                        }

                        this.loadedLabel.DataSource = dataSource;
                        this.SetMultiLineText(dataSource);

                        var orientation = PrintOrientation.Portrait;
                        if (!string.IsNullOrEmpty(localLabel.Orientation))
                        {
                            orientation = (PrintOrientation)Enum.Parse(typeof(PrintOrientation), localLabel.Orientation);
                        }

                        var label = string.Empty;

                        if (ApplicationSettings.IsPrinterInUse)
                        {
                            if (printer.HasValue)
                            {
                                printerNo = printer.ToInt();
                            }
                            else if (localPrinter == null || localPrinter.PrinterNumber == 1)
                            {
                                printerNo = 1;
                            }
                            else
                            {
                                printerNo = localPrinter.PrinterNumber;
                            }

                            if (!ApplicationSettings.UseExternalLabels && !ApplicationSettings.UseExternalLabels2)
                            {
                                if (!ApplicationSettings.TestMode)
                                {
                                    label = this.Print(labelQty, orientation, printerNo);
                                }
                                else
                                {
                                    this.TestPrint();
                                }
                            }
                            else
                            {
                                var labelData = localLabel.LabelFile;
                                var data = string.Empty;
                                if (labelData != null)
                                {
                                    //data = Encoding.ASCII.GetString(labelData);
                                    data = labelData;
                                }

                                label = this.Print(data, dataSource, labelQty, printerNo);
                            }
                        }

                        if (!string.IsNullOrEmpty(label))
                        {
                            var convertedLabel = label.ToBytes();
                            if (convertedLabel != null)
                            {
                                storedLabels.Add(convertedLabel);
                            }
                        }

                        labelids.Add(localLabel.LabelID);
                    }
                    else
                    {
                        this.Log.LogError(this.GetType(), "Label match not found");
                        Messenger.Default.Send(new Label { Name = Message.NoMatchingLabel }, Token.ChangeDisplayLabelName);
                        throw new Exception(Message.NoLabelFound);
                    }
                }
            }
            finally
            {
                if (ApplicationSettings.ResetLabelOnEveryPrint)
                {
                    this.labelManuallyChanged = false;
                }
            }

            return Tuple.Create(labelids, storedLabels, printerNo);
        }

        /// <summary> Prints the label.
        /// </summary>
        /// <param name="numCopies">The number of labels to print.</param>
        public string Print(int numCopies = 1, PrintOrientation orientation = PrintOrientation.Portrait, int printerNo = 1)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to print label");
            var localPrinterSettings = this.printerSettings;
            if (printerNo == 2)
            {
                localPrinterSettings = this.printer2Settings;
            }
            else if (printerNo == 3)
            {
                localPrinterSettings = this.printer3Settings;
            }

            try
            {
                using (var printJob = new PrintJob(localPrinterSettings))
                {
                    for (int i = 0; i < numCopies; i++)
                    {
                        if (i >= ApplicationSettings.ExternalLabelsToPrintBeforeSlowDown)
                        {
                            System.Threading.Thread.Sleep(ApplicationSettings.ExternalLabelsSlowDownTime);
                        }

                        printJob.ThermalLabel = this.loadedLabel;
                        printJob.PrintOrientation = orientation;

                        #region licence failsafe

                        ThermalLabel.LicenseOwner = "nouvem limited-Ultimate Edition-Developer License";
                        ThermalLabel.LicenseKey = "TGYVQ5YUM3758TPAJS26FKM5ZERNVWKQQ4J8FTDTTAHB4UUGBZ5Q";

                        #endregion

                        printJob.Print();

                        this.Log.LogDebug(this.GetType(), "Label printed");

                        if (i + 1 == numCopies)
                        {
                            return printJob.GetNativePrinterCommands();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = $"Printer Error:Connecting to IP:{localPrinterSettings.Communication.NetworkIPAddress} Error:{ex.Message}";
                this.Log.LogError(this.GetType(), string.Format("Ex:{0} Inner:{1}", error, ex.InnerException));
                throw new Exception(error);
            }

            return string.Empty;
        }

        /// <summary>
        /// Prints the label.
        /// </summary>
        /// <param name="numCopies">The number of labels to print.</param>
        public string Print(string data, DataTable dt, int numCopies = 1, int printerNo = 1)
        {
            var labelText = LabelManager.Instance.CreateLabelString(data, dt);

            try
            {
                if (printerNo == 1)
                {
                    this.Log.LogDebug(this.GetType(),
                        string.Format("Printing external label. Port Open:{0}, Printer No:{1}, No Copies:{2}, Com Port:{3}",
                            this.externalPrinter.IsPortOpen, printerNo, numCopies, this.externalPrinter.SerialPort.PortName));

                    if (!this.externalPrinter.IsPortOpen)
                    {
                        System.Threading.Thread.Sleep(200);
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to re-open: ComPort:{0}, Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}", 
                            this.externalPrinter.SerialPort.PortName,
                            this.externalPrinter.SerialPort.BaudRate,
                            this.externalPrinter.SerialPort.Parity,
                            this.externalPrinter.SerialPort.StopBits,
                            this.externalPrinter.SerialPort.DataBits));

                        this.externalPrinter.OpenPort();
                    }
                }
                else if (printerNo == 2)
                {
                    this.Log.LogDebug(this.GetType(),
                   string.Format("Printing external label. Port Open:{0}, Printer No:{1}, No Copies:{2}, Com Port:{3}",
                       this.externalPrinter2.IsPortOpen, printerNo, numCopies, this.externalPrinter2.SerialPort.PortName));

                    if (!this.externalPrinter2.IsPortOpen)
                    {
                        System.Threading.Thread.Sleep(200);
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to re-open: Com Port:{0}, Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}",
                            this.externalPrinter2.SerialPort.PortName,
                            this.externalPrinter2.SerialPort.BaudRate,
                            this.externalPrinter2.SerialPort.Parity,
                            this.externalPrinter2.SerialPort.StopBits,
                            this.externalPrinter2.SerialPort.DataBits));

                        this.externalPrinter2.OpenPort();
                    }
                }
                else
                {

                    this.Log.LogDebug(this.GetType(),
                   string.Format("Printing external label. Port Open:{0}, Printer No:{1}, No Copies:{2}, Com Port:{3}",
                       this.externalPrinter3.IsPortOpen, printerNo, numCopies, this.externalPrinter3.SerialPort.PortName));

                    if (!this.externalPrinter3.IsPortOpen)
                    {
                        System.Threading.Thread.Sleep(200);
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to re-open: Com Port:{0}, Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}",
                            this.externalPrinter3.SerialPort.PortName,
                            this.externalPrinter3.SerialPort.BaudRate,
                            this.externalPrinter3.SerialPort.Parity,
                            this.externalPrinter3.SerialPort.StopBits,
                            this.externalPrinter3.SerialPort.DataBits));

                        this.externalPrinter3.OpenPort();
                    }
                }

                if (numCopies > ApplicationSettings.ExternalLabelsToPrintBeforeSlowDown)
                {
                    for (int i = 0; i < numCopies; i++)
                    {
                        System.Threading.Thread.Sleep(ApplicationSettings.ExternalLabelsSlowDownTime);
                        if (printerNo == 1)
                        {
                            this.externalPrinter.WriteToPort(labelText);
                        }
                        else if (printerNo == 2)
                        {
                            this.externalPrinter2.WriteToPort(labelText);
                        }
                        else
                        {
                            this.externalPrinter3.WriteToPort(labelText);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < numCopies; i++)
                    {
                        if (printerNo == 1)
                        {
                            this.externalPrinter.WriteToPort(labelText);
                        }
                        else if (printerNo == 2)
                        {
                            this.externalPrinter2.WriteToPort(labelText);
                        }
                        else
                        {
                            this.externalPrinter3.WriteToPort(labelText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = $"Printer Error:{ex.Message}";
                this.Log.LogError(this.GetType(), string.Format("Ex:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw new Exception(error);
            }

            return labelText;
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the application printers.
        /// </summary>
        private void GetPrinters()
        {
            this.printers = this.DataManager.GetPrinters();
        }

        /// <summary>
        /// Sets the multi line text.
        /// </summary>
        /// <param name="dataSource">The datasource.</param>
        private void SetMultiLineText(DataTable dataSource)
        {
            foreach (var item in this.loadedLabel.Items)
            {
                if (item.DataField.Equals("#MultiLine Text1"))
                {
                    if (item is TextItem && dataSource != null && dataSource.Rows.Count > 0)
                    {
                        (item as TextItem).Text = dataSource.Rows[0]["#MultiLine Text1"].ToString();
                        (item as TextItem).DataField = string.Empty;
                    }

                    continue;
                }

                if (item.DataField.Equals("#MultiLine Text2"))
                {
                    if (item is TextItem && dataSource != null && dataSource.Rows.Count > 0)
                    {
                        (item as TextItem).Text = dataSource.Rows[0]["#MultiLine Text2"].ToString();
                        (item as TextItem).DataField = string.Empty;
                    }
                }

                if (item.DataField.Equals("#Text1"))
                {
                    if (item is TextItem && dataSource != null && dataSource.Rows.Count > 0)
                    {
                        (item as TextItem).Text = dataSource.Rows[0]["#Text1"].ToString();
                        (item as TextItem).DataField = string.Empty;
                    }
                }
            }
        }

        #endregion
    }
}

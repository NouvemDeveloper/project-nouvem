﻿// -----------------------------------------------------------------------
// <copyright file="AccountsManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Reflection;
using Microsoft.Win32;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.AccountsIntegration;
    using AccountsIntegration.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;

    public class AccountsManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly AccountsManager Manager = new AccountsManager();

        private const string REG_PATH = @"Software\Sage\MMS";
        private const string REGKEY_VALUE = "ClientInstallLocation";
        private const string DEFAULT_ASSEMBLY = "Sage.Common.dll";
        private const string ASSEMBLY_RESOLVER = "Sage.Common.Utilities.AssemblyResolver";
        private const string RESOLVER_METHOD = "GetResolver";

        /// <summary>
        /// The accounts integration reference.
        /// </summary>
        private IAccountsIntegration accounts;

        private bool sage200AssembliesLoaded;

        #endregion

        #region constructor

        private AccountsManager()
        {
            this.GetAccountsPackage();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the accounts manager singleton.
        /// </summary>
        public static AccountsManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the local machine can access the accounts package (null indicates no).
        /// </summary>
        public bool CanConnectToAccounts
        {
            get
            {
                return this.accounts != null;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Connects to the accounts db.
        /// </summary>
        /// <returns>An empty string if successful, otherwise the error message.</returns>
        public string Connect()
        {
            var accountType = this.accounts.GetType();
            this.Log.LogInfo(this.GetType(), string.Format("Connect(): Attempting to log in to accounts. Path:{0}, User name:{1}, Password:{2}, Account Type:{3}", ApplicationSettings.AccountsDbPath, ApplicationSettings.AccountsDbUserName,
                       ApplicationSettings.AccountsDbPassword, accountType));

            var logInMessage = this.accounts.Connect(ApplicationSettings.AccountsDbPath,
                ApplicationSettings.AccountsDbUserName,
                ApplicationSettings.AccountsDbPassword);

            this.Log.LogInfo(this.GetType(), logInMessage.Equals(string.Empty) ? "Log in successful" : logInMessage);
            return logInMessage;
        }

        /// <summary>
        /// Logs out from the db.
        /// </summary>
        /// <returns>An empty string if successful, otherwise the error message.</returns>
        public void Logout()
        {
            this.Log.LogDebug(this.GetType(), "Logging out");
            this.accounts.Logout();
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns></returns>
        public IList<Prices> GetAccountPrices()
        {
            var prices = new List<Prices>();
            var accountsPrices = this.accounts.GetPrices(string.Empty);

            var dbPrices = accountsPrices.Item1;
            var error = accountsPrices.Item2;
            if (error != string.Empty)
            {
                throw new Exception(error);
            }

            //foreach (var inventoryItem in dbPrices)
            //{
            //    var existing = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Code.Equals(inventoryItem.Code));
            //    if (existing != null)
            //    {
            //        if (existing.Name == inventoryItem.Name)
            //        {
            //            continue;
            //        }

            //        existing.Master.Name = inventoryItem.Name;
            //        products.Add(existing);
            //    }
            //    else
            //    {
            //        products.Add(new InventoryItem
            //        {
            //            Master = new INMaster { Name = inventoryItem.Name, Code = inventoryItem.Code },
            //            Group = new INGroup { Name = inventoryItem.GroupName },
            //            ShelfLife = inventoryItem.ShelfLife
            //        });
            //    }
            //}

            return dbPrices;
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns></returns>
        public IList<InventoryItem> GetAccountProducts()
        {
            var products = new List<Model.BusinessObject.InventoryItem>();
            var accountsProducts = this.accounts.GetProducts(string.Empty);

            var dbProducts = accountsProducts.Item1;
            var error = accountsProducts.Item2;
            if (error != string.Empty)
            {
                throw new Exception(error);
            }

            foreach (var inventoryItem in dbProducts)
            {
                var existing = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Code.Equals(inventoryItem.Code));
                if (existing != null)
                {
                    if (existing.Name == inventoryItem.Name)
                    {
                        continue;
                    }

                    existing.Master.Name = inventoryItem.Name;
                    products.Add(existing);
                }
                else
                {
                    products.Add(new InventoryItem
                    {
                        Master = new INMaster { Name = inventoryItem.Name, Code = inventoryItem.Code },
                        Group = new INGroup { Name = inventoryItem.GroupName },
                        ShelfLife = inventoryItem.ShelfLife
                    });
                }
            }

            return products;
        }

        /// <summary>
        /// Syncs the business partners.
        /// </summary>
        public IList<Model.BusinessObject.BusinessPartner> GetAccountsBusinessPartners()
        {
            //if (ApplicationSettings.AccountsPackage == AccountsPackage.Exchequer)
            //{
            //    this.accounts.GetPartners(ApplicationSettings.AccountsDirectory);
            //    return;
            //}
        
            var partnersToReturn = new List<Model.BusinessObject.BusinessPartner>();
            var partnerTypes = this.DataManager.GetBusinessPartnerTypes();
            var customerType = partnerTypes.First(x => x.Type.Equals(BusinessPartnerType.Customer));
            var supplierType = partnerTypes.First(x => x.Type.Equals(BusinessPartnerType.Supplier));
            var livestockType = partnerTypes.First(x => x.Type.Equals(BusinessPartnerType.LivestockSupplier));
            var customerAndLivestockType = partnerTypes.First(x => x.Type.Equals(BusinessPartnerType.CustomerAndLivestock));
            var customerAndSupplierType = partnerTypes.First(x => x.Type.Equals(Constant.CustomerAndSupplier));

            if (ApplicationSettings.AccountsPackage == AccountsPackage.Sage50 || ApplicationSettings.AccountsPackage == AccountsPackage.Sage200)
            {
                var partnerData = this.accounts.GetPartners(string.Empty);
                var error = partnerData.Item2;
                //var partners = partnerData.Item1.Where(x => !string.IsNullOrEmpty(x.Analysis1)).ToList();
                var partners = partnerData.Item1.ToList();

                foreach (var businessPartner in partners)
                {
                    var typeId = customerType.NouBPTypeID;
                    var partnerType = customerType;
                    if (!businessPartner.IsCustomer && businessPartner.IsSupplier)
                    {
                        if (businessPartner.Analysis1.CompareIgnoringCase("CATTLE"))
                        {
                            typeId = livestockType.NouBPTypeID;
                            partnerType = livestockType;
                        }
                        else
                        {
                            typeId = supplierType.NouBPTypeID;
                            partnerType = supplierType;
                        }
                    }
                    else if (businessPartner.IsCustomer && businessPartner.IsSupplier)
                    {
                        if (businessPartner.Analysis1.CompareIgnoringCase("CATTLE"))
                        {
                            typeId = customerAndLivestockType.NouBPTypeID;
                            partnerType = customerAndLivestockType;
                        }
                        else
                        {
                            typeId = customerAndSupplierType.NouBPTypeID;
                            partnerType = customerAndSupplierType;
                        }
                    }

                    var existingPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.Code.Equals(businessPartner.Code));

                    if (existingPartner != null)
                    {
                        if (existingPartner.Details.Name == businessPartner.Name
                            && existingPartner.Details.CreditLimit == businessPartner.CreditLimit
                            && existingPartner.Details.Balance == businessPartner.AccountBalance)
                        {
                            continue;
                        }
                    }

                    //if (localPartner == null)
                   // {
                        var partner = new Model.BusinessObject.BusinessPartner();
                        partner.PartnerType = partnerType;
                        partner.PartnerGroup = NouvemGlobal.BusinessPartnerGroups.FirstOrDefault(x => x.BPGroupName.CompareIgnoringCase("No Group"));
                        partner.ExistingPartner = (existingPartner != null).BoolToYesNo();

                        var currency = NouvemGlobal.BusinessPartnerCurrencies.FirstOrDefault(x => x.Symbol.CompareIgnoringCaseAndWhitespace(businessPartner.Currency));
                        if (currency != null)
                        {
                            partner.PartnerCurrency = currency;
                        }
                        else
                        {
                            partner.PartnerCurrency = NouvemGlobal.BusinessPartnerCurrencies.FirstOrDefault();
                        }
                        
                        partner.Details = new ViewBusinessPartner
                        {
                            Name = businessPartner.Name,
                            Code = businessPartner.Code,
                            Tel = businessPartner.Phone,
                            FAX = businessPartner.Fax,
                            CreditLimit = businessPartner.CreditLimit,
                            Balance = businessPartner.AccountBalance,
                            OnHold = businessPartner.AccountStatus == 0,
                            NouBPTypeID = typeId,
                            BPCurrencyID = partner.PartnerCurrency?.BPCurrencyID 
                        };

                        partner.Addresses = new List<BusinessPartnerAddress>();
                        var address = new BusinessPartnerAddress
                        {
                            Details = new BPAddress
                            {
                                AddressLine1 = businessPartner.AddressLine1,
                                AddressLine2 = businessPartner.AddressLine2,
                                AddressLine3 = businessPartner.AddressLine3,
                                AddressLine4 = businessPartner.AddressLine4,
                                PostCode = businessPartner.PostCode,
                                Billing = true,
                                Shipping = true
                            }
                        };

                        partner.Addresses.Add(address);

                        partner.Contacts = new List<BusinessPartnerContact>();
                        if (!string.IsNullOrEmpty(businessPartner.ContactName))
                        {
                            var contact = new BusinessPartnerContact
                            {
                                Details = new BPContact
                                {
                                    FirstName = businessPartner.ContactName,
                                    Email = string.Empty,
                                    Phone = string.Empty,
                                    Mobile = string.Empty,
                                    PrimaryContact = true
                                }
                            };

                            partner.Contacts.Add(contact);
                        }

                        partnersToReturn.Add(partner);
                    //}
                }
            }

            return partnersToReturn;
        }

        /// <summary>
        /// Post the sale invoice to the AutoCopyrelevant accounts package.
        /// </summary>
        /// <param name="sale">The invoice data to post.</param>
        public string PostSalesInvoice(Sale sale)
        {
            var details = new List<AccountsIntegration.BusinessObject.InvoiceDetail>();
            foreach (var detail in sale.SaleDetails)
            {
                float localQty;
                if (detail.PriceByTypicalWeight && ApplicationSettings.AccountsPackage == AccountsPackage.TASBooks)
                {
                    var localProduct = detail.InventoryItem;
                    if (localProduct == null)
                    {
                        localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                    }

                    localQty = detail.QuantityDelivered.ToFloat() * localProduct.Master.NominalWeight.ToFloat();
                }
                else
                {
                    localQty = detail.PriceMethod == PriceMethod.Weight
                        ? detail.WeightDelivered.ToFloat()
                        : detail.QuantityDelivered.ToFloat();

                    if (detail.PriceByTypicalWeight)
                    {
                        localQty = detail.NominalWeight.ToFloat() * detail.QuantityDelivered.ToFloat();
                    }
                }

                var localProductCode = detail.InventoryItem.Code;
                var productDescription = detail.InventoryItem.Name;
                if (ApplicationSettings.AccountsCustomer.CompareIgnoringCase(Customer.Woolleys))
                {
                    productDescription = detail.InventoryItem.Name;
                    localProductCode = "BONEDBEEF";
                }

                details.Add(new AccountsIntegration.BusinessObject.InvoiceDetail
                {
                    ProductCode = localProductCode,
                    Quantity = localQty,
                    QuantityDelivered = detail.QuantityDelivered,
                    WeightDelivered = detail.WeightDelivered,
                    UnitPrice = detail.UnitPrice.ToDecimal(),
                    Description = productDescription,
                    Vat = detail.VAT.ToDecimal(),
                    TotalExVat = detail.TotalExVAT.ToDecimal(),
                    TotalIncVat = detail.TotalIncVAT.ToDecimal(),
                    UnitOfSale = detail.QuantityDelivered.ToDouble().ToString(),
                    NominalCode = detail.InventoryItem != null && detail.InventoryItem.Master != null 
                    ? detail.InventoryItem.Master.SalesNominalCode : string.Empty
                });
            }

            var localRef = sale.BaseDocumentNumber.ToString();
            var description = sale.CustomerPOReference ?? string.Empty;
            description = description.Replace("\r\n", "");
            var vat = sale.VAT.ToDecimal();
            var localInvoiceNo = sale.Number;
            var invoiceDate = sale.DeliveryDate.ToDate();
            var saleOrderNo = sale.SaleOrderNumber.ToString();
            if (ApplicationSettings.AccountsPackage == AccountsPackage.TASBooks)
            {
                localInvoiceNo = sale.SaleID;
                invoiceDate = sale.CreationDate.ToDate();
            }

            if (ApplicationSettings.AccountsCustomer.CompareIgnoringCase(Customer.Woolleys))
            {
                if (sale.BaseDocumentCreationDate.HasValue)
                {
                    invoiceDate = sale.BaseDocumentCreationDate.ToDate();
                    saleOrderNo = string.Format("N {0}", sale.BaseDocumentNumber);
                }

                localInvoiceNo = 0;
            }

            var header = new AccountsIntegration.BusinessObject.InvoiceHeader
            {
                TradingPartnerCode = sale.BPCustomer.Code,
                TradingPartnerName = sale.BPCustomer.Name,
                CustomerOrderNo = description,
                InvoiceNo = localInvoiceNo,
                UseTASInvoiceNumbering = true,
                InvoiceDate = invoiceDate,
                Description = localRef,
                Vat = vat,
                Total = sale.SubTotalExVAT.ToDecimal(),
                FilePath = ApplicationSettings.AccountsFilePath,
                DispatchDocketNo = sale.BaseDocumentNumber.ToString(),
                SaleOrderNo = saleOrderNo,
                SalesPersonCode = ApplicationSettings.AccountsSalesPersonCode,
                AddressLine1 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine1 : string.Empty,
                AddressLine2 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine2 : string.Empty,
                AddressLine3 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine3 : string.Empty,
                AddressLine4 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine4 : string.Empty,
                PostCode = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.PostCode : string.Empty,
                DeliveryAddressLine1 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine1 : string.Empty,
                DeliveryAddressLine2 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine2 : string.Empty,
                DeliveryAddressLine3 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine3 : string.Empty,
                DeliveryAddressLine4 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine4 : string.Empty,
                Details = details
            };

            this.Log.LogDebug(this.GetType(), "About to post invoice...");

            if (this.accounts == null)
            {
                this.Log.LogDebug(this.GetType(), "accounts null. Reconnecting...");
                this.Connect();
            }

            var postMessage = string.Empty;
            if (ApplicationSettings.AccountsPackage == AccountsPackage.TASBooks)
            {
                if (ApplicationSettings.PrePostInvoice)
                {
                    postMessage = this.accounts.PostSaleOrder(header);
                }
                else
                {
                    postMessage = this.accounts.PostSalesInvoice(header);
                }
            }
            else
            {
                postMessage = this.accounts.PostSalesInvoice(header);
            }
          
            this.Log.LogDebug(this.GetType(), postMessage.Equals(string.Empty) ? "Invoice posting successful" : postMessage);
            return postMessage;
        }

        /// <summary>
        /// Post the sale invoice to the AutoCopyrelevant accounts package.
        /// </summary>
        /// <param name="sale">The invoice data to post.</param>
        public string PostSalesCreditNote(Sale sale)
        {
            var details = new List<AccountsIntegration.BusinessObject.InvoiceDetail>();
            foreach (var detail in sale.SaleDetails)
            {
                if (detail.InventoryItem == null)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }

                float localQty;
                localQty = detail.PriceMethod == PriceMethod.Weight
                    ? detail.WeightDelivered.ToFloat()
                    : detail.QuantityDelivered.ToFloat();

                if (detail.PriceByTypicalWeight)
                {
                    localQty = detail.NominalWeight.ToFloat() * detail.QuantityDelivered.ToFloat();
                }

                var localProductCode = detail.InventoryItem.Code;
                var productDescription = detail.InventoryItem.Name;

                details.Add(new AccountsIntegration.BusinessObject.InvoiceDetail
                {
                    ProductCode = localProductCode,
                    Quantity = localQty,
                    QuantityDelivered = detail.QuantityDelivered,
                    WeightDelivered = detail.WeightDelivered,
                    UnitPrice = detail.UnitPrice.ToDecimal(),
                    Description = productDescription,
                    Vat = detail.VAT.ToDecimal(),
                    TotalExVat = detail.TotalExVAT.ToDecimal(),
                    TotalIncVat = detail.TotalIncVAT.ToDecimal(),
                    UnitOfSale = detail.QuantityDelivered.ToDouble().ToString(),
                    NominalCode = detail.InventoryItem != null && detail.InventoryItem.Master != null
                    ? detail.InventoryItem.Master.SalesNominalCode : string.Empty
                });
            }

            var localRef = sale.BaseDocumentNumber.ToString();
            var description = sale.CustomerPOReference ?? string.Empty;
            description = description.Replace("\r\n", "");
            var vat = sale.VAT.ToDecimal();
            var localInvoiceNo = sale.Number;
            var invoiceDate = sale.DeliveryDate.ToDate();
            var saleOrderNo = sale.SaleOrderNumber.ToString();

            var header = new AccountsIntegration.BusinessObject.InvoiceHeader
            {
                TradingPartnerCode = sale.BPCustomer.Code,
                TradingPartnerName = sale.BPCustomer.Name,
                CustomerOrderNo = description,
                InvoiceNo = localInvoiceNo,
                UseTASInvoiceNumbering = true,
                InvoiceDate = invoiceDate,
                Description = localRef,
                Vat = vat,
                Total = sale.SubTotalExVAT.ToDecimal(),
                FilePath = ApplicationSettings.AccountsFilePath,
                DispatchDocketNo = sale.BaseDocumentNumber.ToString(),
                SaleOrderNo = saleOrderNo,
                SalesPersonCode = ApplicationSettings.AccountsSalesPersonCode,
                AddressLine1 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine1 : string.Empty,
                AddressLine2 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine2 : string.Empty,
                AddressLine3 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine3 : string.Empty,
                AddressLine4 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine4 : string.Empty,
                PostCode = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.PostCode : string.Empty,
                DeliveryAddressLine1 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine1 : string.Empty,
                DeliveryAddressLine2 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine2 : string.Empty,
                DeliveryAddressLine3 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine3 : string.Empty,
                DeliveryAddressLine4 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine4 : string.Empty,
                Details = details
            };

            this.Log.LogDebug(this.GetType(), "About to post invoice...");

            if (this.accounts == null)
            {
                this.Log.LogDebug(this.GetType(), "accounts null. Reconnecting...");
                this.Connect();
            }
          
            var postMessage = this.accounts.PostSalesCreditNote(header);

            this.Log.LogDebug(this.GetType(), postMessage.Equals(string.Empty) ? "Invoice posting successful" : postMessage);
            return postMessage;
        }

        /// <summary>
        /// Post the sale invoice to the AutoCopyrelevant accounts package.
        /// </summary>
        /// <param name="sale">The invoice data to post.</param>
        public string PostPurchaseOrder(Sale sale)
        {
            var details = new List<AccountsIntegration.BusinessObject.InvoiceDetail>();
            foreach (var detail in sale.SaleDetails)
            {
                float localQty;
                localQty = detail.PriceMethod == PriceMethod.Weight
                    ? detail.WeightDelivered.ToFloat()
                    : detail.QuantityDelivered.ToFloat();

                if (detail.PriceByTypicalWeight)
                {
                    localQty = detail.NominalWeight.ToFloat() * detail.QuantityDelivered.ToFloat();
                }
            
                if (detail.InventoryItem == null)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }

                var localProductCode = detail.InventoryItem.Code;
                var productDescription = detail.InventoryItem.Name;
                details.Add(new AccountsIntegration.BusinessObject.InvoiceDetail
                {
                    ProductCode = localProductCode,
                    Quantity = localQty,
                    QuantityDelivered = detail.QuantityDelivered,
                    WeightDelivered = detail.WeightDelivered,
                    UnitPrice = detail.UnitPrice.ToDecimal(),
                    Description = productDescription,
                    Vat = detail.VAT.ToDecimal(),
                    TotalExVat = detail.TotalExVAT.ToDecimal(),
                    TotalIncVat = detail.TotalIncVAT.ToDecimal(),
                    NominalCode = detail.InventoryItem != null && detail.InventoryItem.Master != null
                    ? detail.InventoryItem.Master.PurchaseNominalCode : string.Empty
                });
            }

            var localDate = sale.ProposedKillDate.HasValue
                ? sale.ProposedKillDate.ToDate()
                : sale.DocumentDate.HasValue
                    ? sale.DocumentDate.ToDate()
                    : DateTime.Today;
            if (ApplicationSettings.UseDeliveryDateForPurchaseOrder)
            {
                localDate = sale.DeliveryDate.HasValue ? sale.DeliveryDate.ToDate() : DateTime.Today;
            }

            var header = new AccountsIntegration.BusinessObject.InvoiceHeader
            {
                TradingPartnerCode = sale.BPCustomer.Code,
                TradingPartnerName = sale.BPCustomer.Name,
                Total = sale.SubTotalExVAT.ToDecimal(),
                Reference = sale.Number.ToString(),
                InvoiceNo = sale.Number,
                InvoiceDate = localDate,
                UseSageNumbers = ApplicationSettings.UseSagePurchaseOrderNumbers,
                AddressLine1 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine1 : string.Empty,
                AddressLine2 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine2 : string.Empty,
                AddressLine3 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine3 : string.Empty,
                AddressLine4 = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.AddressLine4 : string.Empty,
                PostCode = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.PostCode : string.Empty,
                DeliveryAddressLine1 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine1 : string.Empty,
                DeliveryAddressLine2 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine2 : string.Empty,
                DeliveryAddressLine3 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine3 : string.Empty,
                DeliveryAddressLine4 = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.AddressLine4 : string.Empty,
                Details = details
            };

            this.Log.LogDebug(this.GetType(), "About to post purchase order..");

            if (this.accounts == null)
            {
                this.Log.LogDebug(this.GetType(), "accounts null. Reconnecting...");
                this.Connect();
            }

            var postMessage = this.accounts.PostPurchaseOrder(header);
            this.Log.LogDebug(this.GetType(), postMessage.Equals(string.Empty) ? "Invoice purchase order posting successful" : postMessage);
            return postMessage;
        }

        /// <summary>
        /// Post the sale invoice to the AutoCopyrelevant accounts package.
        /// </summary>
        /// <param name="sale">The invoice data to post.</param>
        public string PostPurchaseInvoice(Sale sale)
        {
            DateTime localDate;
            if (ApplicationSettings.UseProposedKillDateForPurchaseInvoice)
            {
                localDate = sale.ProposedKillDate.HasValue
                    ? sale.ProposedKillDate.ToDate()
                    : sale.DocumentDate.HasValue
                        ? sale.DocumentDate.ToDate()
                        : DateTime.Today;
            }
            else
            {
                localDate = sale.DocumentDate.HasValue
                    ? sale.DocumentDate.ToDate()
                    : DateTime.Today;
            }

            var header = new AccountsIntegration.BusinessObject.InvoiceHeader
            {
                TradingPartnerCode = sale.BPCustomer.Code,
                TradingPartnerName = sale.BPCustomer.Name,
                Currency = sale.PartnerCurrency?.Symbol,
                Total = sale.SubTotalExVAT.ToDecimal(),
                NetTotal = sale.GrandTotalIncVAT.ToDecimal(),
                Vat = sale.VAT.ToDecimal(),
                DeductionsVat= sale.DeductionsVAT.ToDecimal(),
                Description = string.Format("Cattle-{0}", sale.Number),
                Reference = sale.Number.ToString(),
                InvoiceDate = localDate,
                FilePath = ApplicationSettings.AccountsFilePath,
                InvoiceNo = sale.Number,
                Details = new List<AccountsIntegration.BusinessObject.InvoiceDetail>()
            };

            foreach (var saleStockDetail in sale.StockDetails)
            {
                header.Details.Add(new AccountsIntegration.BusinessObject.InvoiceDetail
                {
                    TotalExVat = saleStockDetail.TotalExclVat.ToDecimal(),
                    TotalIncVat= saleStockDetail.TotalIncVat.ToDecimal(),
                    Vat = saleStockDetail.VAT.ToDecimal(),
                    UnitPrice = saleStockDetail.UnitPrice.ToDecimal(),
                    QuantityDelivered = saleStockDetail.PaidWeight,
                    Description = "Payment"
                });
            }

            foreach (var saleStockDetail in sale.PaymentDeductionItems)
            {
                header.Details.Add(new AccountsIntegration.BusinessObject.InvoiceDetail
                {
                    TotalExVat = saleStockDetail.SubTotalExclVAT.ToDecimal(),
                    TotalIncVat = saleStockDetail.TotalIncVAT.ToDecimal(),
                    Vat = saleStockDetail.Vat.ToDecimal(),
                    UnitPrice = saleStockDetail.Price.ToDecimal(),
                    QuantityDelivered = 1,
                    Description = "Deduction"
                });
            }

            this.Log.LogDebug(this.GetType(), "About to post purchase invoice...");

            if (this.accounts == null)
            {
                this.Log.LogDebug(this.GetType(), "accounts null. Reconnecting...");
                this.Connect();
            }

            var postMessage = this.accounts.PostPurchaseInvoice(header);
            this.Log.LogDebug(this.GetType(), postMessage.Equals(string.Empty) ? "Invoice purchase posting successful" : postMessage);
            return postMessage;
        }

        /// <summary>
        /// Post the sale invoice to the AutoCopyrelevant accounts package.
        /// </summary>
        /// <param name="sale">The invoice data to post.</param>
        public string PostPurchaseInvoiceCredit(Sale sale)
        {
            var header = new AccountsIntegration.BusinessObject.InvoiceHeader
            {
                TradingPartnerCode = sale.BPCustomer.Code,
                Total = sale.SubTotalExVAT.ToDecimal(),
                Vat = sale.VAT.ToDecimal(),
                Description = string.Format("Levy-{0}", sale.Number),
                Reference = sale.Number.ToString(),
                InvoiceDate = sale.ProposedKillDate.HasValue ? sale.ProposedKillDate.ToDate() : DateTime.Today
            };

            this.Log.LogDebug(this.GetType(), "About to post purchase invoice credit...");

            if (this.accounts == null)
            {
                this.Log.LogDebug(this.GetType(), "accounts null. Reconnecting...");
                this.Connect();
            }

            var postMessage = this.accounts.PostPurchaseInvoiceCredit(header);
            this.Log.LogDebug(this.GetType(), postMessage.Equals(string.Empty) ? "Invoice purchase posting successful" : postMessage);
            return postMessage;
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Gets the application accounts package.
        /// </summary>
        public void GetAccountsPackage()
        {
            this.Log.LogInfo(this.GetType(), String.Format("GetsAccountsPackage(): Retrieving the application accounts package.:{0}", ApplicationSettings.AccountsPackage));
            try
            {
                if (ApplicationSettings.AccountsPackage == AccountsPackage.TASBooks)
                {
                    this.accounts = new TASBooks();
                }
                else if (ApplicationSettings.AccountsPackage == AccountsPackage.Sage50)
                {
                    this.accounts = new Sage50();
                }
                else if (ApplicationSettings.AccountsPackage == AccountsPackage.Sage200)
                {
                    this.FindSage200Assemblies();
                    this.accounts = new Sage200{ConnectionString = ApplicationSettings.ApplicationConnectionString};
                }
                else if (ApplicationSettings.AccountsPackage == AccountsPackage.Exchequer)
                {
                    this.accounts = new Exchequer();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Locates and invokes assemblies from the client folder at runtime.
        /// </summary>
        private void FindSage200Assemblies()
        {
            if (this.sage200AssembliesLoaded)
            {
                return;
            }

            // get registry info for Sage 200 server path
            var path = string.Empty;
            var root = Registry.CurrentUser;
            var key = root.OpenSubKey(REG_PATH);

            if (key != null)
            {
                object value = key.GetValue(REGKEY_VALUE);
                if (value != null)
                    path = value as string;
            }

            // refer to all installed assemblies based on location of default one
            if (string.IsNullOrEmpty(path) == false)
            {
                string commonDllAssemblyName = System.IO.Path.Combine(path, DEFAULT_ASSEMBLY);

                if (System.IO.File.Exists(commonDllAssemblyName))
                {
                    System.Reflection.Assembly defaultAssembly = System.Reflection.Assembly.LoadFrom(commonDllAssemblyName);
                    Type type = defaultAssembly.GetType(ASSEMBLY_RESOLVER);
                    MethodInfo method = type.GetMethod(RESOLVER_METHOD);
                    method.Invoke(null, null);
                }
            }

            this.sage200AssembliesLoaded = true;
        }

        #endregion
    }
}


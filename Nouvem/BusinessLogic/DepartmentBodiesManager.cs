﻿// -----------------------------------------------------------------------
// <copyright file="DepartmentBodiesManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Outlook;
using Nouvem.AIMS.BusinessLogic;
using Nouvem.AIMS.Model;
using Nouvem.AIMS.SheepAssignBatchNo;
using Nouvem.AIMS.SheepToAbbatoir;
using Nouvem.AIMS.SheepToLairage;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.QAS.Model;
using Nouvem.QualityAssurance.BusinessLogic;
using Nouvem.QualityAssurance.Model.BusinessObject;
using Nouvem.Shared.Localisation;
using Nouvem.QualityAssurance;
using Movement = Nouvem.QualityAssurance.Model.BusinessObject.Movement;
using ResidencyCalculation = Nouvem.QualityAssurance.Model.BusinessObject.ResidencyCalculation;
using SheepBatches = Nouvem.AIMS.SheepToLairage.SheepBatches;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Identigen;
    using Nouvem.Shared;

    public class DepartmentBodiesManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly DepartmentBodiesManager Manager = new DepartmentBodiesManager();

        /// <summary>
        /// The webservice time out.
        /// </summary>
        private bool webServiceTimeout;

        /// <summary>
        /// The identigen wrappper.
        /// </summary>
        private Nouvem.Identigen.IdentigenWrapper identigen = new IdentigenWrapper();

        #endregion

        #region constructor

        private DepartmentBodiesManager()
        {
            this.ResidencyCalculations = new List<Model.BusinessObject.ResidencyCalculation>();
            this.Categories = this.DataManager.GetCategories();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the manager singleton.
        /// </summary>
        public static DepartmentBodiesManager Instance
        {
            get
            {
                return Manager;
            }
        }

        public IList<NouCategory> Categories { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current herd is bord bia approved.
        /// </summary>
        public bool BordBiaHerdApproved { get; set; }

        /// <summary>
        /// Gets or sets the total days of residency of the animal. Note that this is the total days on the last two farms, assuming both herds are valid.
        /// </summary>
        public int TotalDaysOfResidency { get; set; }


        /// <summary>
        /// Gets or sets the days of residency of the animal on the previous farm.
        /// </summary>
        public int DaysOfResidencyPreviousFarm { get; set; }


        /// <summary>
        /// Gets or sets the days of residency of the animal on the current farm. 
        /// </summary>
        public int DaysOfResidencyCurrentFarm { get; set; }


        public IList<Nouvem.Model.BusinessObject.ResidencyCalculation> ResidencyCalculations;
        public string BordBiaHerdStatus { get; set; }

        private string lastMovementDate;
        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public string LastMovementDate
        {
            get
            {
                return this.lastMovementDate;
            }

            set
            {
                this.lastMovementDate = value;

            }
        }

        #endregion

        #region method

        /// <summary>
        /// Sends the animal movements.
        /// </summary>
        /// <param name="details">The animals to send.</param>
        public void MoveAnimals(IList<StockDetail> details)
        {
            var count = details.Count * 2;
            var time = DateTime.Now.ToLongTimeString().Replace(":", ""); ;
            var header = string.Format("CTSSIS|2.01|1000773|{0}|{1}{2}{3}{4}", count, DateTime.Today.Year, DateTime.Today.Month.PadLeft('0', 2), DateTime.Today.Day.PadLeft('0', 2), time);
            var sb = new StringBuilder();
            sb.AppendLine(header);
            sb.AppendLine();

            var plantNo = !string.IsNullOrEmpty(ApplicationSettings.PlantCode)
                ? ApplicationSettings.PlantCode.Substring(2, ApplicationSettings.PlantCode.Length - 2) : string.Empty;

            // intake animals
            foreach (var stockDetail in details)
            {
                var line = string.Format("{0}|||{1}||2|{2}", stockDetail.Eartag, plantNo, stockDetail.TransactionDate.ToString("dd/MM/yyyy"));
                sb.AppendLine(line);
            }

            // same animals again, only with kill date
            foreach (var stockDetail in details)
            {
                var line = string.Format("{0}|||{1}||7|{2}", stockDetail.Eartag, plantNo, stockDetail.GradingDate.ToDate().ToString("dd/MM/yyyy"));
                sb.AppendLine(line);
            }

            //Write report out to  txt file
            var filename = Path.Combine(Settings.Default.ToAimsPath, string.Format("AllMovements{0}.txt", DateTime.Today.ToString("ddMMyyyy")));
            File.WriteAllText(filename, sb.ToString());

            if (this.DataManager.ChangeCarcassStatus(details, NouvemGlobal.NouDocStatusMoved.NouDocStatusID))
            {
                this.AttachReportToEmail(filename);
            }
        }

        /// <summary>
        /// Sends the animal movements.
        /// </summary>
        /// <param name="details">The animals to send.</param>
        public void SendAnimalPrices(IList<StockDetail> details)
        {
            var sb = new StringBuilder();

            // intake animals
            foreach (var stockDetail in details)
            {
                var carcassNo = stockDetail.CarcassNumber.ToInt().ToString();
                var deptCarcassNo = "    ";
                if (carcassNo.Length >= 4)
                {
                    deptCarcassNo = carcassNo.Substring(carcassNo.Length - 4, 4);
                }

                var unitPrice = Math.Round(stockDetail.UnitPrice.ToDecimal(), 3).ToString().Replace(".", "").PadRight(4, '0');
                var conf = "  ";
                var fat = "  ";
                var grade = stockDetail.Grade ?? string.Empty;
                var animalStatus = "N";
                if (stockDetail.TBYes.ToBool())
                {
                    animalStatus = "X";
                }
                else if (stockDetail.Casualty.ToBool() || stockDetail.Condemned.ToBool())
                {
                    animalStatus = "Y";
                }

                if (grade.Length == 4)
                {
                    conf = grade.Substring(0, 2);
                    fat = grade.Substring(2, 2);
                }

                var side1Wgt = Math.Round(stockDetail.WeightSide1, 1).ToString().Replace(".", "").PadLeft(4, '0');
                var side2Wgt = Math.Round(stockDetail.WeightSide2, 1).ToString().Replace(".", "").PadLeft(4, '0');
                var classifier = stockDetail.Classifier ?? string.Empty;
                var category = " ";
                if (stockDetail.Category != null && !string.IsNullOrEmpty(stockDetail.Category.CAT) && stockDetail.Category.CAT.Length >= 1)
                {
                    category = stockDetail.Category.CAT.Substring(0, 1);
                }

                var herd = stockDetail.HerdNo ?? string.Empty;
                var thirdParty = string.Empty;
                if (stockDetail.ThirdParty.CompareIgnoringCase(Constant.Yes))
                {
                    thirdParty = "Y";
                }

                var line = string.Format("1,{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}",
                    ApplicationSettings.DeptFactoryCode,
                    stockDetail.TransactionDate.ToString("dd/MM/yyyy"),
                    classifier.PadLeft(3, '0'),
                    deptCarcassNo,
                    category,
                    conf,
                    fat,
                    side1Wgt,
                    side2Wgt,
                    stockDetail.Eartag.PadLeft(14, ' '),
                    animalStatus,
                    herd.PadLeft(10, ' '),
                    unitPrice,
                    thirdParty
                    );
                sb.AppendLine(line);
            }

            //Write report out to  txt file
            var filename = Path.Combine(ApplicationSettings.DeptAnimalPricingPath, string.Format("AnimalPrices{0}.{1}", DateTime.Now.ToString("ddMMyyyy"), ApplicationSettings.DeptFileFormat));
            File.WriteAllText(filename, sb.ToString());

            this.AttachReportToEmail(filename);
            this.DataManager.ExportCarcasses(details, false);
        }

        /// <summary>
        /// Sends the carcass data (Boyne Valley).
        /// </summary>
        /// <param name="details">The data to send.</param>
        /// <remarks>Uses the Exported2 flag.</remarks>
        public void ExportDispatchData(IList<Sale> details)
        {
            foreach (var dispatch in details)
            {
                var doc = new XDocument(new XDeclaration("1.0", "utf-8", null));

                var header =
                    new XElement("carcaseTransaction",
                        new XElement("transactiontype", "POP"),
                        new XElement("abp_plant_name", ""),
                        new XElement("abp_plant_no", ""),
                        new XElement("supplier_name", ApplicationSettings.Customer),
                        new XElement("abp_supplier_code", ApplicationSettings.PlantCode),
                        new XElement("order_no", dispatch.Number)
                        );

                foreach (var detail in dispatch.StockDetails)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        header.Add(
                            new XElement("carcase_data",
                                new XElement("kill_date", detail.GradingDate),
                                new XElement("carcase_no", detail.CarcassNumber),
                                new XElement("eur_code", detail.Grade),
                                new XElement("mpqas", detail.FarmAssured.ToBool().ToString()),
                                new XElement("tot_bqas_residency", detail.TotalResidency),
                                new XElement("num_residencies", detail.CurrentResidency),
                                new XElement("cold_weight", detail.ColdWeight),
                                new XElement("breed", detail.BreedName),
                                new XElement("dob_date", detail.DOB),
                                new XElement("dolm_date", detail.DateOfLastMove),
                                new XElement("herd_no", detail.HerdNo),
                                new XElement("barcode", detail.Serial),
                                new XElement("cat_code", detail.CatName),
                                new XElement("eartag", detail.Eartag),
                                new XElement("lot_no", detail.LotNo)
                                //new XElement("Reference1", detail.Attribute100),
                                //new XElement("Reference2", detail.Attribute101),
                                //new XElement("Reference3", detail.Attribute102),
                                //new XElement("Reference4", detail.Attribute103),
                                //new XElement("Reference5", detail.Attribute104),
                                //new XElement("Reference6", detail.Attribute105),
                                //new XElement("Reference7", detail.Attribute106),
                                //new XElement("Reference8", detail.Attribute107),
                                //new XElement("Reference9", detail.Attribute108),
                                //new XElement("Reference10", detail.Attribute109)
                            ));
                    }
                }

                doc.Add(header);

                var filename = Path.Combine(ApplicationSettings.DispatchFilePath, string.Format("{0}{1}.xml", dispatch.Number, DateTime.Now.ToString("ddMMyyyyHHmm")));
                File.WriteAllText(filename, doc.ToString());
                this.AttachReportToEmail(filename);
            }
        }

        /// <summary>
        /// Export pallet data to xml (Keypak).
        /// </summary>
        /// <param name="dispatch">The docket id.</param>
        public void ExportDispatchPalletData(Sale dispatch)
        {
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", null));

            var header =
                new XElement("TRANSACTION",
                    new XElement("SUPPLIER_NAME", dispatch.PartnerName),
                    new XElement("SUPPLIER_CODE", dispatch.PartnerCode),
                    new XElement("SUPPLIER_ORDER_NO", dispatch.Number),
                    new XElement("TRANSACTIONTYPE", "PO"),
                    new XElement("RECEIVING_PLANT_CODE", "3722038"),
                    new XElement("PO_NUMBER", dispatch.CustomerPOReference),
                    new XElement("DELIVERY_DATE", dispatch.DeliveryDate.ToDate().ToShortDateString().Replace("/", "")),
                    new XElement("DELIVERY_TIME", dispatch.DeliveryTime ?? string.Empty),
                    new XElement("TOTAL_NO_PALLETS", dispatch.PalletCount),
                    new XElement("TOTAL_CASES_ON_ORDER", dispatch.BoxCount)
                    );

            foreach (var detail in dispatch.StockDetails)
            {
                var palletHeader =
                    new XElement("PALLET_NO",
                        new XElement("PALLET_SERIAL", detail.PalletSerial),
                        new XElement("QTY_OF_CASES_ON_PALLET", detail.TransactionQty.ToInt())
                    );

                foreach (var box in detail.StockDetails)
                {
                    palletHeader.Add(
                        new XElement("BOX_DETAILS",
                            new XElement("CASE_UID", box.CaseUID),
                            new XElement("CUST_PLU_CODE", box.PLU),
                            new XElement("ANA_CODE", box.AnaCode),
                            new XElement("PIECES", box.TransactionQty.ToInt().ToString()),
                            new XElement("BATCH_NO", box.DocumentReference),
                            new XElement("COUNTRY_OF_SLAUGHTER", box.CountryOfOrigin ?? string.Empty),
                            new XElement("FACTORY_OF_SLAUGHTER", box.FactoryOfSlaughter ?? string.Empty),
                            new XElement("COUNTRY_OF_DEBONING", box.CountryOfOrigin ?? string.Empty),
                            new XElement("FACTORY_OF_DEBONING", box.FactoryOfSlaughter ?? string.Empty),
                            new XElement("COUNTRY_OF_PACKING", box.CountryOfOrigin ?? string.Empty),
                            new XElement("FACTORY_OF_PACKING", box.FactoryOfSlaughter ?? string.Empty),
                            new XElement("UOM", box.UOM),
                            new XElement("FIXED_WEIGHT", box.FixedWeight),
                            new XElement("NET_WEIGHT", box.TransactionWeight.ToDecimal().ToString()),
                            new XElement("CASE_GROSS_WEIGHT", box.GrossWeight.ToString()),
                            new XElement("CASE_TARE_WGT", box.Tare.ToString()),
                            new XElement("KILL_DATE", box.KillDate.HasValue ? box.KillDate.ToDate().ToShortDateString().Replace("/", "") : box.PackDate.Replace("/", "")),
                            new XElement("PACK_DATE", box.PackDate.Replace("/", "") ?? string.Empty),
                            new XElement("DNO_BY_DATE", box.PackDate.Replace("/", "") ?? string.Empty),
                            new XElement("USE_BY_DATE", box.UseBy.Replace("/", "") ?? string.Empty),
                            new XElement("DISPLAY_UNTIL", box.PackDate.Replace("/", "") ?? string.Empty),
                            new XElement("DISPATCH_BY", box.PackDate.Replace("/", "") ?? string.Empty),
                            new XElement("PROCESS_BY", box.PackDate.Replace("/", "") ?? string.Empty),
                            new XElement("FREEZED", box.PackDate.Replace("/", "") ?? string.Empty)
                            )
                        );
                }

                header.Add(palletHeader);
            }

            doc.Add(header);

            var filename = Path.Combine(ApplicationSettings.LabelImagePath, string.Format("{0}{1}.xml", dispatch.Number, DateTime.Now.ToString("ddMMyyyyHHmm")));
            File.WriteAllText(filename, doc.ToString());
            this.AttachReportToEmail(filename);
        }

        /// <summary>
        /// Re-attaches the animal movements file to an email.
        /// </summary>
        public void SendEmail(string filename)
        {
            this.AttachReportToEmail(filename);
        }

        /// <summary>
        /// Sends the scot beef data.
        /// </summary>
        /// <param name="details">The data to send.</param>
        /// <remarks>Uses the Exported2 flag.</remarks>
        public void ExportBeefData(IList<StockDetail> details)
        {
            var sb = new StringBuilder();
            //sb.AppendLine("KS|KillNumber|ScottBeef Farm Referance|Carcass No|LeftHotWt|RightHotWT|RebateWT|CondemedWT|ColdWT|KillDate|FA,Grade|CAT|Breed|LeftID|RightID|Destination");
            //sb.AppendLine("ABC1234,01/09/2015,111111111,1,HF,XY12,O+4L,UK111111111,FALSE,335.9,349.2,C,10/07/2013,2,Y,Y");
            //sb.AppendLine("ABC1234,01/09/2015,222222222,2,HF,XY12,O+4L,UK222222222,FALSE,305.6,325.0,C,19/08/2012,1,N,Y");
            //sb.AppendLine("ABC1234,01/09/2015,333333333,3,HF,XY12,O+4L,UK333333333,FALSE,345.4,310.6,C,05/04/2013,3,Y,N");

            foreach (var stockDetail in details)
            {
                var rebateWgt = stockDetail.HotWeight - stockDetail.ColdWeight;
                var condemned = 0;
                var loadingTime = stockDetail.CreationDate;
                var unLoadingTime = stockDetail.CreationDate;
                var localTimeInTransit = stockDetail.Notes;
                if (!string.IsNullOrEmpty(localTimeInTransit))
                {
                    var splitTime = localTimeInTransit.Split(':');
                    if (splitTime.Length == 2)
                    {
                        loadingTime = loadingTime.ToDate().AddHours(-splitTime[0].ToDouble());
                        loadingTime = loadingTime.ToDate().AddMinutes(-splitTime[1].ToDouble());
                    }
                    else
                    {
                        loadingTime = loadingTime.ToDate().AddHours(-localTimeInTransit.ToInt());
                    }
                }

                var line = string.Format("KS|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}|{23}",
                    stockDetail.Number.ToInt(), stockDetail.Generic1, stockDetail.CarcassNumber.ToInt(),
                    stockDetail.WeightSide1, stockDetail.WeightSide2,
                    rebateWgt, condemned, stockDetail.ColdWeight, stockDetail.GradingDate.ToDate().ToString("ddMMyyyy"),
                    stockDetail.FarmAssuredYesNo,
                    stockDetail.Grade, stockDetail.CategoryCAT, stockDetail.BreedType, stockDetail.LabelIDSide1,
                    stockDetail.LabelIDSide2, stockDetail.Generic2, stockDetail.Eartag, stockDetail.DOB.ToDate().ToString("ddMMyyyy"),
                    stockDetail.CurrentResidency.ToInt(),
                    (stockDetail.TotalResidency.ToInt() >= 30).BoolToYesNo(),
                    stockDetail.Attribute200.CompareIgnoringCase("Welsh").BoolToYesNo(),
                    stockDetail.NumberOfMoves + 1,
                    loadingTime.ToDate().ToString("ddMMyyyyhhmm"),
                    unLoadingTime.ToDate().ToString("ddMMyyyyhhmm"));
                sb.AppendLine(line);
            }

            var filename = Path.Combine(Settings.Default.KillDataPath, string.Format("ScotBeef{0}.csv", DateTime.Today.ToString("ddMMyyyy")));
            File.WriteAllText(filename, sb.ToString());

            this.AttachReportToEmail(filename);
            this.DataManager.ExportCarcasses(details, false);
        }

        /// <summary>
        /// Uploads the sample file to identigen.
        /// </summary>
        /// <param name="details">The animals to send.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <param name="killdate">The kill date.</param>
        /// <param name="location">The factory code/name.</param>
        /// <remarks>Uses the Exported flag.</remarks>
        public void UploadSamples(IList<StockDetail> details, string userName, string password, DateTime killdate, string location)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Kill_Location,Kill_Date,BC,CN,Declared_Breed,GradeEU,ET,organic,HotWt,ColdWt,Category,DOB,Movements,FAS,Eligibility,Select Farms,Holding Number");
            //sb.AppendLine("ABC1234,01/09/2015,111111111,1,HF,XY12,O+4L,UK111111111,FALSE,335.9,349.2,C,10/07/2013,2,Y,Y");
            //sb.AppendLine("ABC1234,01/09/2015,222222222,2,HF,XY12,O+4L,UK222222222,FALSE,305.6,325.0,C,19/08/2012,1,N,Y");
            //sb.AppendLine("ABC1234,01/09/2015,333333333,3,HF,XY12,O+4L,UK333333333,FALSE,345.4,310.6,C,05/04/2013,3,Y,N");

            foreach (var stockDetail in details)
            {
                var selectFarms = !string.IsNullOrEmpty(stockDetail.Generic1) ? "Y" : "N";
                var holding = stockDetail.HoldingNumber;
                var line = string.Format("{0},{1},{2},{3},{4},{5},{6},N,{7},{8},{9},{10},{11},{12},{13},{14},{15}",
                              ApplicationSettings.PlantCode, stockDetail.GradingDate.ToDate().ToShortDateString()
                              , stockDetail.Identigen, stockDetail.CarcassNumber.ToInt(), stockDetail.BreedType, stockDetail.Grade,
                              stockDetail.Eartag, stockDetail.HotWeight, stockDetail.ColdWeight, stockDetail.CategoryCAT,
                              stockDetail.DOB.ToDate().ToShortDateString(), "0", stockDetail.FarmAssured.BoolToYesNoShort(), stockDetail.Generic2, selectFarms, holding);

                sb.AppendLine(line);
            }

            this.identigen.UploadSamples(userName, password, sb.ToString(), killdate, location);
            this.DataManager.ExportCarcasses(details, true);
        }


        /// <summary>
        /// Call the aims web sevice for the animal details.
        /// </summary>
        public void CallAim(StockDetail carcass)
        {
            var aimError = string.Empty;

            ProgressBar.Run();
            var animalDetail = this.VerifyAimAnimalDetail(
                carcass.Eartag,
                3,
                ApplicationSettings.AimsLogPath,
                ApplicationSettings.AimsURL,
                ApplicationSettings.AimsCertPath,
                ApplicationSettings.AimsPassword,
                carcass,
                ref aimError);

            ProgressBar.Run();

            if (animalDetail == null)
            {
                carcass.Error = "Error";
                return;
            }

            if (!string.IsNullOrEmpty(aimError))
            {

                carcass.Error = aimError;
                return;
            }

            this.ParseAnimalData(animalDetail, carcass);
        }

        /// <summary>
        /// Generate the animal movements to be sent to AIM.
        /// </summary>
        /// <param name="herdDetail">The detail of the herd.</param>
        /// <returns>A collection of animal movements to be sent to AIM.</returns>
        private IList<MovementTag> GenerateTags(IList<StockDetail> herdDetail)
        {
            var movementCollection = new List<MovementTag>();

            foreach (var item in herdDetail)
            {
                var move = new MovementTag();

                move.AnimalAction = Constant.InsertAnimalAIM;
                move.SoftwareAnimalId = Constant.SoftwareID;
                move.TagNum = item.Eartag;
                move.ScannedIndicator = item.Attribute180;
                move.Category = item.CategoryName;
                move.Location = item.HoldingNumber;
                movementCollection.Add(move);
            }

            return movementCollection;
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        public Tuple<string, string> MoveSheepBatchToLairage(Sale lot)
        {
            if (ApplicationSettings.AfaisEnvironment.CompareIgnoringCase("Test"))
            {
                return this.MoveSheepBatchToLairageTest(lot);
            }

            return this.MoveSheepBatchToLairageLive(lot);
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        public Tuple<string,string> MoveSheepBatchToAbbatoir(Sale lot)
        {
            if (ApplicationSettings.AfaisEnvironment.CompareIgnoringCase("Test"))
            {
                return this.MoveSheepBatchToAbbatoirTest(lot);
            }

            return this.MoveSheepBatchToAbbatoirLive(lot);
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        public Tuple<string, string> SheepAssignBatchNumber(Sale lot)
        {
            if (ApplicationSettings.AfaisEnvironment.CompareIgnoringCase("Test"))
            {
                return this.SheepAssignBatchNumberTest(lot);
            }

            return this.SheepAssignBatchNumberLive(lot);
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        public Tuple<string, string, List<CollectionData>> SheepEIDCheck(Sale lot)
        {
            if (ApplicationSettings.AfaisEnvironment.CompareIgnoringCase("Test"))
            {
                return this.SheepEIDCheckTest(lot);
            }

            return this.SheepEIDCheckLive(lot);
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string,string> MoveSheepBatchToLairageTest(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepToLairage.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var batch = new SheepBatches
            {
                batchNo = lot.Number.ToString(),
                noOfAnimals = lot.StockDetails.Count,
                noOfAnimalsSpecified = true
                //lotNo = lot.Herd
            };

            SheepBatches[] batches = { batch };

            var localDetails = lot.StockDetails.GroupBy(x => x.Category);
            var details = new SheepDetails[localDetails.Count()];

            var categoryCount = 0;
            foreach (var categoryDetails in localDetails)
            {
                var catCount = categoryDetails.Count();
                var eidList = new string[catCount];
                for (int i = 0; i < catCount; i++)
                {
                    eidList[i] = categoryDetails.ElementAt(i).Eartag;
                }

                var sheepDetails = new SheepDetails
                {
                    lotNumber = lot.Herd.ToInt(),
                    noOfAnimals = categoryDetails.Count(),
                    noOfAnimalsSpecified = true,
                    type = this.AfaisSheepCategoryConverter(categoryDetails.First().Category),
                    eidList = eidList
                };

                details[categoryCount] = sheepDetails;
                categoryCount++;
            }

            var sheepMoveData = new SheepMoveToLairageInput
            {
                genericInput = genericInput,
                batchNo = lot.Number.ToString(),
                fromFlockNo = lot.Herd,
                noOfAnimals = lot.StockDetails.Count,
                noOfAnimalsSpecified = true,
                permitNo = lot.PermitNo, // "F002248935",
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true,
                movementType = "EID",
                sheepDetailList = details,
                sheepBatchList = batches
            };

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.MoveSheepToLairageTest(sheepMoveData);
                return Tuple.Create(result.error ?? string.Empty, result.requestId.ToString());
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"MoveSheepBatchToLairage(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string> MoveSheepBatchToLairageLive(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepMoveToLairageLive.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var batch = new Nouvem.AIMS.SheepMoveToLairageLive.SheepBatches
            {
                batchNo = lot.Number.ToString(),
                noOfAnimals = lot.StockDetails.Count,
                noOfAnimalsSpecified = true
                //lotNo = lot.Herd
            };

            Nouvem.AIMS.SheepMoveToLairageLive.SheepBatches[] batches = { batch };

            var localDetails = lot.StockDetails.GroupBy(x => x.Category);
            var details = new Nouvem.AIMS.SheepMoveToLairageLive.SheepDetails[localDetails.Count()];

            var categoryCount = 0;
            foreach (var categoryDetails in localDetails)
            {
                var catCount = categoryDetails.Count();
                var eidList = new string[catCount];
                for (int i = 0; i < catCount; i++)
                {
                    eidList[i] = categoryDetails.ElementAt(i).Eartag;
                }

                var sheepDetails = new Nouvem.AIMS.SheepMoveToLairageLive.SheepDetails
                {
                    lotNumber = lot.Herd.ToInt(),
                    noOfAnimals = categoryDetails.Count(),
                    noOfAnimalsSpecified = true,
                    type = this.AfaisSheepCategoryConverter(categoryDetails.First().Category),
                    eidList = eidList
                };

                details[categoryCount] = sheepDetails;
                categoryCount++;
            }

            var sheepMoveData = new Nouvem.AIMS.SheepMoveToLairageLive.SheepMoveToLairageInput
            {
                genericInput = genericInput,
                batchNo = lot.Number.ToString(),
                fromFlockNo = lot.Herd,
                noOfAnimals = lot.StockDetails.Count,
                noOfAnimalsSpecified = true,
                permitNo = lot.PermitNo, // "F002248935",
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true,
                movementType = "EID",
                sheepDetailList = details,
                sheepBatchList = batches
            };

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.MoveSheepToLairage(sheepMoveData);
                return Tuple.Create(result.error ?? string.Empty, result.requestId.ToString());
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"MoveSheepBatchToLairage(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string> MoveSheepBatchToAbbatoirTest(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepToAbbatoir.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var sheepMoveData = new SheepMoveToAbatInput
            {
                batchNo = lot.Number.ToString(),
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true,
                genericInput = genericInput
            };

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.MoveSheepToAbbatoirTest(sheepMoveData);
                return Tuple.Create(result.error ?? string.Empty, result.requestId.ToString());
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"MoveSheepBatchToAbbatoir(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string> MoveSheepBatchToAbbatoirLive(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepMoveToAbbatoirLive.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                password = ApplicationSettings.AfaisPassword
            };

            var sheepMoveData = new Nouvem.AIMS.SheepMoveToAbbatoirLive.SheepMoveToAbatInput
            {
                batchNo = lot.Number.ToString(),
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true,
                genericInput = genericInput
            };

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.MoveSheepToAbbatoir(sheepMoveData);
                return Tuple.Create(result.error ?? string.Empty, result.requestId.ToString());
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"MoveSheepBatchToAbbatoir(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string> SheepAssignBatchNumberTest(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepAssignBatchNo.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var localDetails = lot.StockDetails;
            var eidList = new string[localDetails.Count];
            for (int i = 0; i < localDetails.Count; i++)
            {
                eidList[i] = localDetails.ElementAt(i).Eartag;
            }

            var batch = new Nouvem.AIMS.SheepAssignBatchNo.SheepBatches
            {
                batchNo = lot.Number.ToString(),
                noOfAnimals = lot.StockDetails.Count,
                noOfAnimalsSpecified = true,
                lotNo = lot.Herd,
                eidList = eidList
            };

            Nouvem.AIMS.SheepAssignBatchNo.SheepBatches[] batches = { batch };

            var sheepMoveData = new SheepAssignBatchNoInput
            {
                genericInput = genericInput,
                fromFlockNo = lot.Herd,
                permitNo = lot.PermitNo, // "F002248935",
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true,
                sheepBatchList = batches
            };

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.SheepAssignBatchNumber(sheepMoveData);
                return Tuple.Create(result.error ?? string.Empty, result.requestId.ToString());
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"MoveSheepBatchToLairage(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string> SheepAssignBatchNumberLive(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepAssignBatchNoLive.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var localDetails = lot.StockDetails;
            var eidList = new string[localDetails.Count];
            for (int i = 0; i < localDetails.Count; i++)
            {
                eidList[i] = localDetails.ElementAt(i).Eartag;
            }

            var batch = new Nouvem.AIMS.SheepAssignBatchNoLive.SheepBatches
            {
                batchNo = lot.Number.ToString(),
                noOfAnimals = lot.StockDetails.Count,
                noOfAnimalsSpecified = true,
                lotNo = lot.Herd,
                eidList = eidList
            };

            Nouvem.AIMS.SheepAssignBatchNoLive.SheepBatches[] batches = { batch };

            var sheepMoveData = new AIMS.SheepAssignBatchNoLive.SheepAssignBatchNoInput
            {
                genericInput = genericInput,
                fromFlockNo = lot.Herd,
                permitNo = lot.PermitNo, // "F002248935",
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true,
                sheepBatchList = batches
            };

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.SheepAssignBatchNumberLive(sheepMoveData);
                return Tuple.Create(result.error ?? string.Empty, result.requestId.ToString());
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"MoveSheepBatchToLairage(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string, List<CollectionData>> SheepEIDCheckTest(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepEIDCheck.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var localDetails = lot.StockDetails;
            var eidList = new string[localDetails.Count];
            for (int i = 0; i < localDetails.Count; i++)
            {
                eidList[i] = localDetails.ElementAt(i).Eartag;
            }

            var inferredMove = new Nouvem.AIMS.SheepEIDCheck.InferredMoveCheck
            {
                flockNo = lot.Herd,
                tagNoList = eidList,
                moveDocNo = lot.PermitNo
            };

            var inferredMoveCheck = new Nouvem.AIMS.SheepEIDCheck.InferredMoveCheck[1];
            inferredMoveCheck[0] = inferredMove;

            var sheepMoveData = new Nouvem.AIMS.SheepEIDCheck.SheepEidCheckInput
            {
                genericInput = genericInput,
                //eidList = eidList,
                inferredMoveCheck = inferredMoveCheck,
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true
            };

            var invalidTags = new List<CollectionData>();

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.SheepEIDCheck(sheepMoveData);
                var error = result.error ?? string.Empty;

                if (error == string.Empty)
                {
                    var tags = result.eidInfoList;
                    foreach (var eidInfo in tags)
                    {
                        if (eidInfo.causeInferredMove.CompareIgnoringCaseAndWhitespace("Not resident in presenting flock") || eidInfo.FQStatus.CompareIgnoringCase("UNKNOWN"))
                        {
                            invalidTags.Add(new CollectionData { Data = eidInfo.eid, Identifier = eidInfo.flockNo });
                        }
                    }
                }

                return Tuple.Create(error, result.requestId.ToString(), invalidTags);
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"SheepEIDCheckLive(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty, invalidTags);
            }
        }

        /// <summary>
        /// Moves a sheep lot into lairage on the AFAIS system.
        /// </summary>
        /// <param name="lot">The lot to move.</param>
        /// <returns></returns>
        private Tuple<string, string, List<CollectionData>> SheepEIDCheckLive(Sale lot)
        {
            var genericInput = new Nouvem.AIMS.SheepEIDCheckLive.GenericInput
            {
                callingLocation = "Meat",
                channel = "Meat",
                clntRefNo_BusId = ApplicationSettings.AfaisBusinessId,
                environment = ApplicationSettings.AfaisEnvironment,
                herdNo = ApplicationSettings.AfaisAbbatoirCode,
                username = ApplicationSettings.AfaisUserName,
                //password = ApplicationSettings.AfaisPassword
            };

            var localDetails = lot.StockDetails;
            var eidList = new string[localDetails.Count];
            for (int i = 0; i < localDetails.Count; i++)
            {
                eidList[i] = localDetails.ElementAt(i).Eartag;
            }

            var inferredMove = new Nouvem.AIMS.SheepEIDCheckLive.InferredMoveCheck
            {
                flockNo = lot.Herd,
                tagNoList = eidList,
                moveDocNo = lot.PermitNo
            };

            var inferredMoveCheck = new Nouvem.AIMS.SheepEIDCheckLive.InferredMoveCheck[1];
            inferredMoveCheck[0] = inferredMove;

            var sheepMoveData = new Nouvem.AIMS.SheepEIDCheckLive.SheepEidCheckInput
            {
                genericInput = genericInput,
                //eidList = eidList,
                inferredMoveCheck = inferredMoveCheck,
                pinNo = ApplicationSettings.AfaisPinNo,
                pinNoSpecified = true
            };

            var invalidTags = new List<CollectionData>();

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                var result = serviceCall.SheepEIDCheckLive(sheepMoveData);
                var error = result.error ?? string.Empty;
              
                if (error == string.Empty)
                {
                    var tags = result.eidInfoList;
                    foreach (var eidInfo in tags)
                    {
                        if (eidInfo.causeInferredMove.CompareIgnoringCaseAndWhitespace("Not resident in presenting flock") || eidInfo.FQStatus.CompareIgnoringCase("UNKNOWN"))
                        {
                            invalidTags.Add(new CollectionData { Data = eidInfo.eid, Identifier = eidInfo.flockNo});
                        }
                    }
                }

                return Tuple.Create(error, result.requestId.ToString(), invalidTags);
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"SheepEIDCheckLive(): {e.Message}");
                return Tuple.Create(e.Message, string.Empty, invalidTags);
            }
        }

        public Tuple<string, string> MoveBatch(IList<StockDetail> batchDetails, string herdNo, int batchId, AimsData aimsData)
        {
            var movementCollection = this.GenerateTags(batchDetails);

            // Retrieve the country for which the eligibility of the herd needs to be checked with AIM.
            var countryCodes = ApplicationSettings.AIMMovementCountryCodes;

            // Register the movement of the herd with AIM.
            var moveResponse = this.MoveHerd(batchId, herdNo, countryCodes,
                movementCollection, ApplicationSettings.AimsLogPath, ApplicationSettings.AimsURL, ApplicationSettings.AimsCertPath,
                ApplicationSettings.AimsPassword, 3, aimsData, ApplicationSettings.AimSenderId);

            if (moveResponse != null && moveResponse.InwardMovementRefId != null &&
                moveResponse.InwardMovementRefId.ToInt() > 0)
            {
                // Store the movement response id.
                return Tuple.Create(moveResponse.InwardMovementRefId, string.Empty);
            }
            else
            {
                if (moveResponse?.ValidationErrors != null)
                {
                    moveResponse.Error = moveResponse.Error + Environment.NewLine;
                    foreach (var error in moveResponse?.ValidationErrors)
                    {
                        moveResponse.Error += $"{error.Item1} {error.Item2}{Environment.NewLine}";
                    }
                }

                ApplicationSettings.CurrentMOVLotNo += 500;
                return Tuple.Create(string.Empty, moveResponse.Error);
            }
        }

        public Tuple<string, string> MoveSheepBatch(IList<StockDetail> batchDetails, string herdNo, int batchId, AimsData aimsData, string permitNo)
        {
            var movementCollection = this.GenerateTags(batchDetails);

            var catB = movementCollection.Count(x => x.Cleanliness == "B");
            var catC = movementCollection.Count(x => x.Cleanliness == "C");
            var catDead = 0;

            // Register the movement of the herd with AIM.
            var moveResponse = this.MoveSheepHerd(batchId, herdNo,
                movementCollection, ApplicationSettings.AimsSheepLogPath, ApplicationSettings.AimsURL, ApplicationSettings.AimsCertPath,
                ApplicationSettings.AimsPassword, 3, aimsData, permitNo, catDead, catC, catB);

            if (moveResponse != null && moveResponse.InwardMovementRefId != null &&
                moveResponse.InwardMovementRefId.ToInt() > 0)
            {
                // Store the movement response id.
                return Tuple.Create(moveResponse.InwardMovementRefId, string.Empty);
            }
            else
            {
                ApplicationSettings.CurrentMOVLotNo += 500;
                return Tuple.Create(string.Empty, moveResponse.Error);
            }
        }

        /// <summary>
        /// Move a herd of animals from a farm to another location through AIM.
        /// </summary>
        /// <param name="batchId">The id of the batch of animals being moved.</param>
        /// <param name="herdNo">The herd number.</param>
        /// <param name="countryCodes">The country codes for which validity needs to be verified.</param>
        /// <param name="movementCollection">The movements of the animals.</param>
        /// <param name="logPathToAim">The path on the disk at which the AIM xml interactions are stored.</param>
        /// <param name="aimURL">The url to which the AIM xml request is to be sent.</param>
        /// <param name="certPath">The path at which the AIM security cert is located.</param>
        /// <param name="aimPassword">The password used to unlock the AIM security cert.</param>
        /// <param name="connectionAttempts">The number of times the request is to be sent to AIM, in the event that it fails.</param>
        /// <param name="error">The errors.</param>
        /// <returns>The move response from AIM.</returns>
        private MovementRequest MoveHerd(int batchId, string herdNo, string countryCodes, IList<MovementTag> movementCollection, string logPathToAim, 
            string aimURL, string certPath, string aimPassword, int connectionAttempts, AimsData aimsData, string userName = "")
        {
            var moveResponse = new MovementRequest();

            try
            {
                int serviceTryCount = 0;

                // Try hitting the web service.
                while (serviceTryCount < connectionAttempts && (moveResponse.InwardMovementRefId == null || moveResponse.InwardMovementRefId.Length == 0))
                {
                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("Attempting to call the AIM herd designator service with the try count set to {0}", serviceTryCount));
                    moveResponse = this.CheckAnimalMovementDetail(batchId.ToString(), herdNo, countryCodes, movementCollection, logPathToAim, aimURL, certPath, aimPassword, aimsData,userName);

                    if (!string.IsNullOrEmpty(moveResponse.Error))
                    {
                        this.Log.LogError(this.GetType(), string.Format(
                            "Aims movement error on attempt {0}:{1}, Lot No:{2}, InwardMOvementRefId:{3}, Move date: {4}, Status: {5}, NumTags: {6}",
                            serviceTryCount,
                            moveResponse.Error,
                            moveResponse.LotNum,
                            moveResponse.InwardMovementRefId,
                            moveResponse.MoveDate,
                            moveResponse.Status,
                         moveResponse.NumTags));
                    }

                    serviceTryCount++;
                }

                ProgressBar.Run();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("The call to AIM to move the herd failed with the following error : {0}", ex.Message));
            }

            return moveResponse;
        }

        /// <summary>
        /// Move a herd of animals from a farm to another location through AIM.
        /// </summary>
        /// <param name="batchId">The id of the batch of animals being moved.</param>
        /// <param name="herdNo">The herd number.</param>
        /// <param name="countryCodes">The country codes for which validity needs to be verified.</param>
        /// <param name="movementCollection">The movements of the animals.</param>
        /// <param name="logPathToAim">The path on the disk at which the AIM xml interactions are stored.</param>
        /// <param name="aimURL">The url to which the AIM xml request is to be sent.</param>
        /// <param name="certPath">The path at which the AIM security cert is located.</param>
        /// <param name="aimPassword">The password used to unlock the AIM security cert.</param>
        /// <param name="connectionAttempts">The number of times the request is to be sent to AIM, in the event that it fails.</param>
        /// <param name="error">The errors.</param>
        /// <returns>The move response from AIM.</returns>
        private MovementRequest MoveSheepHerd(int batchId, string herdNo, 
            IList<MovementTag> movementCollection, string logPathToAim, string aimURL, 
            string certPath, string aimPassword, int connectionAttempts, 
            AimsData aimsData, string permitNo, int noTagsDead, int noTagsC, int noTagsB)
        {
            var moveResponse = new MovementRequest();

            try
            {
                int serviceTryCount = 0;

                // Try hitting the web service.
                while (serviceTryCount < connectionAttempts && (moveResponse.InwardMovementRefId == null || moveResponse.InwardMovementRefId.Length == 0))
                {
                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("Attempting to call the AIM herd designator service with the try count set to {0}", serviceTryCount));
                    moveResponse = this.CheckSheepMovementDetail(batchId.ToString(), herdNo, 
                        movementCollection, logPathToAim, aimURL, certPath, aimPassword, aimsData, permitNo, noTagsDead, noTagsC, noTagsB);

                    if (!string.IsNullOrEmpty(moveResponse.Error))
                    {
                        this.Log.LogError(this.GetType(), string.Format(
                            "Aims movement error on attempt {0}:{1}, Lot No:{2}, InwardMOvementRefId:{3}, Move date: {4}, Status: {5}, NumTags: {6}",
                            serviceTryCount,
                            moveResponse.Error,
                            moveResponse.LotNum,
                            moveResponse.InwardMovementRefId,
                            moveResponse.MoveDate,
                            moveResponse.Status,
                         moveResponse.NumTags));
                    }

                    serviceTryCount++;
                }

                ProgressBar.Run();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("The call to AIM to move the herd failed with the following error : {0}", ex.Message));
            }

            return moveResponse;
        }


        /// <summary>
        /// Retrieve the animal movement details from AIM for a batch of animals.
        /// </summary>
        /// <param name="batchId">The id of the batch.</param>
        /// <param name="herdNo">The herd no as it denotes where the animal has come from.</param>
        /// <param name="countryCode">The country code for the request.</param>
        /// <param name="movementTag">The movement tag description.</param>
        /// <param name="aimLogPath">The path to the AIM xml files.</param>
        /// <param name="serviceUrl">The url of the service.</param>
        /// <param name="certPath">The location at which the cert is stored.</param>
        /// <param name="password">The password used to open the cert.</param>
        /// <returns>AIMWebServiceWrapper.Model.MovementRequest detailing the animal movements.</returns>
        public MovementRequest CheckAnimalMovementDetail(
            string batchId,
            string herdNo,
            string countryCode,
            IList<MovementTag> movementTag,
            string aimLogPath,
            string serviceUrl,
            string certPath,
            string password,
            AimsData aimsData,
            string userName = "")
        {
            var movementResponse = MovementRequest.CreateNewMovementRequest();

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();

                var numTrans = "1";
                var speciesId = "1";
                var transType = "MOV";
                var dateOfRequest = DateTime.Now.ToString("yyyy-MM-dd");
                var lotAction = "I";
                var numTags = movementTag != null ? movementTag.Count.ToString() : "0";
                var senderId = ApplicationSettings.AimSenderId;
                var lotNum = batchId;
                var softwareId = batchId;
                var fileName = string.Format("{0}-{1}{2}{3}", transType, senderId, DateTime.Now.ToString("yyyyMMddhhmmssffff"), ".xml");

                ProgressBar.Run();
                if (ApplicationSettings.LairageType == LairageType.UK)
                {
                    movementResponse = serviceCall.QueryUKMovementRequest(fileName, dateOfRequest, numTrans, senderId, transType, speciesId, herdNo, dateOfRequest, lotNum, softwareId,
                        countryCode, numTags, lotAction, movementTag.ToList(), serviceUrl, certPath, password, true, aimLogPath, userName:userName);
                }
                else
                {
                    movementResponse = serviceCall.QueryMovementRequest(fileName, dateOfRequest, numTrans, senderId, transType, speciesId, herdNo, dateOfRequest, lotNum, softwareId, 
                        countryCode, numTags, lotAction, movementTag.ToList(), serviceUrl, certPath, password, true, aimLogPath);
                }

                // Record the request and response in the db.
                var requestFileName = string.Format("{0}{1}\\{2}", aimLogPath, "toAims", fileName);
                var responseFileName = string.Format("{0}{1}\\{2}", aimLogPath, "fromAims", fileName);
                aimsData.RequestType = "MOV";
                aimsData.RequestXml = requestFileName.ReadFile();
                aimsData.ResponseXml = responseFileName.ReadFile();
                aimsData.CreationDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AIM animal movement service request failed with the following error : {0}", ex.Message));
            }

            return movementResponse;
        }

        /// <summary>
        /// Retrieve the animal movement details from AIM for a batch of animals.
        /// </summary>
        /// <param name="batchId">The id of the batch.</param>
        /// <param name="herdNo">The herd no as it denotes where the animal has come from.</param>
        /// <param name="movementTag">The movement tag description.</param>
        /// <param name="aimLogPath">The path to the AIM xml files.</param>
        /// <param name="serviceUrl">The url of the service.</param>
        /// <param name="certPath">The location at which the cert is stored.</param>
        /// <param name="password">The password used to open the cert.</param>
        /// <returns>AIMWebServiceWrapper.Model.MovementRequest detailing the animal movements.</returns>
        public MovementRequest CheckSheepMovementDetail(
            string batchId,
            string herdNo,
            IList<MovementTag> movementTag,
            string aimLogPath,
            string serviceUrl,
            string certPath,
            string password,
            AimsData aimsData,
            string permitNo, 
            int noTagsDead = 0,
            int noTagsC = 0,
            int noTagsB = 0
           )
        {
            var movementResponse = MovementRequest.CreateNewMovementRequest();

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();

                var numTrans = "1";
                var speciesId = "4";
                var transType = "MOV";
                var dateOfRequest = DateTime.Now.ToString("yyyy-MM-dd");
                var lotAction = "I";
                var numTags = movementTag != null ? movementTag.Count.ToString() : "0";
                var senderId = ApplicationSettings.AimSenderId;
                var lotNum = batchId;
                var softwareId = batchId;
                var fileName = string.Format("{0}-{1}{2}{3}", transType, senderId, DateTime.Now.ToString("yyyyMMddhhmmssffff"), ".xml");

                // Call the service.
                ProgressBar.Run();
                movementResponse = serviceCall.QuerySheepMovementRequest(fileName, dateOfRequest, numTrans, senderId, 
                    transType, speciesId, herdNo, dateOfRequest, lotNum, softwareId, numTags, lotAction, 
                    movementTag.ToList(), serviceUrl, certPath, password, true, aimLogPath,permitNo, noTagsDead, noTagsC, noTagsB);

                // Record the request and response in the db.
                var requestFileName = string.Format("{0}{1}\\{2}", aimLogPath, "toAims", fileName);
                var responseFileName = string.Format("{0}{1}\\{2}", aimLogPath, "fromAims", fileName);
                aimsData.RequestType = "MOV";
                aimsData.RequestXml = requestFileName.ReadFile();
                aimsData.ResponseXml = responseFileName.ReadFile();
                aimsData.CreationDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AIM animal movement service request failed with the following error : {0}", ex.Message));
            }

            return movementResponse;
        }


        /// <summary>
        /// Contact AIM to retrieve verification on the animal detail.
        /// </summary>
        /// <param name="eartag">The eartag to be sent to AIM.</param>
        /// <param name="connectionAttempts">The number of times the request is to be sent to AIM, in the event that it fails.</param>
        /// <param name="logPathToAim">The path on the disk at which the AIM xml interactions are stored.</param>
        /// <param name="aimURL">The url to which the AIM xml request is to be sent.</param>
        /// <param name="certPath">The path at which the AIM security cert is located.</param>
        /// <param name="aimPassword">The password used to unlock the AIM security cert.</param>
        /// <param name="error">The errors.</param>
        /// <returns>The animal detail response from AIM.</returns>
        public AnimalDetail VerifyAimAnimalDetail(string eartag, int connectionAttempts, string logPathToAim, string aimURL, string certPath, string aimPassword, StockDetail carcass, ref string error)
        {
            var animalDetail = new AnimalDetail();

            try
            {
                var serviceTryCount = 0;
                while (serviceTryCount < connectionAttempts && string.IsNullOrEmpty(animalDetail.Breed) && string.IsNullOrEmpty(animalDetail.Error))
                {
                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("Attempting to call the AIM animal detail service for eartag {0} with the try count set to {1}", eartag, serviceTryCount));
                    animalDetail = this.CheckAnimalDetails(eartag, logPathToAim, aimURL, certPath, aimPassword, carcass);
                    serviceTryCount++;

                    this.Log.LogDebug(this.GetType(), string.Format("Response: DOB:{0}, Breed:{1}, Error:{2}", animalDetail.DOB, animalDetail.Breed, animalDetail.Error));
                }

                error = animalDetail.Error ?? string.Empty;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                this.Log.LogError(this.GetType(), ex.Message);
            }

            return animalDetail;
        }

        /// <summary>
        /// Retrieve the animal details from AIM for an animal.
        /// </summary>
        /// <param name="eartag">The tag for which the details are sought.</param>
        /// <param name="aimLogPath">The path to the aim file location.</param>
        /// <param name="serviceUrl">The url of the service.</param>
        /// <param name="certPath">The location at which the cert is stored.</param>
        /// <param name="password">The password used to open the cert.</param>
        /// <returns>AIMWebServiceWrapper.Model.AnimalDetails object detailing the animal details.</returns>
        public Nouvem.AIMS.BusinessLogic.AnimalDetail CheckAnimalDetails(string eartag, string aimLogPath, string serviceUrl, string certPath, string password, StockDetail carcass)
        {
            var animalDetails = AIMS.BusinessLogic.AnimalDetail.CreateNewAnimalDetails();

            try
            {
                var serviceCall = new AIMS.BusinessLogic.WSWrapper();
                this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the AIM animal details for eartag {0}", eartag));

                var numTrans = "1";
                var speciesId = "1";
                var transType = Constant.AnimalDetailsRequest;
                var dateOfRequest = DateTime.Now.ToString("yyyy-MM-dd");
                var fileName = string.Format("{0}-{1}{2}{3}", Constant.AnimalDetailsRequest, ApplicationSettings.AimSenderId, DateTime.Now.ToString("yyyyMMddhhmmssffff"), ".xml");

                // Call the AIM service wrapper.
                if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                {
                    animalDetails = serviceCall.QueryUKAnimalDetails(
                        fileName, dateOfRequest, numTrans, transType,
                        speciesId, eartag, serviceUrl, certPath, password, true, aimLogPath,
                        ApplicationSettings.AimSenderId, ApplicationSettings.AimsPassword);
                }
                else
                {
                    animalDetails = serviceCall.QueryAnimalDetails(fileName, dateOfRequest, numTrans, transType, speciesId, eartag, serviceUrl, certPath, password, true, aimLogPath);
                }

                // Record the request and response in the db.
                var requestFileName = string.Format("{0}{1}\\{2}", aimLogPath, "toAims", fileName);
                var responseFileName = string.Format("{0}{1}\\{2}", aimLogPath, "fromAims", fileName);

                var data = new AimsData
                {
                    Eartag = eartag,
                    RequestType = Constant.AnimalDetailsRequest,
                    RequestXml = requestFileName.ReadFile(),
                    ResponseXml = responseFileName.ReadFile(),
                    CreationDate = DateTime.Now
                };

                carcass.AimsData = data;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AIM animal details service request failed with the following error : {0}", ex.Message));
            }

            return animalDetails;
        }

        /// <summary>
        /// Load the animal detail into the view model variables.
        /// </summary>
        /// <param name="animalDetail">The animal to edit.</param>
        private void ParseAnimalData(Nouvem.AIMS.BusinessLogic.AnimalDetail animalDetail, StockDetail carcass)
        {
            var animalMovements = new List<AIMS.Model.Movement>();
            if (animalDetail != null)
            {
                if (string.IsNullOrEmpty(animalDetail.DOB))
                {
                    animalDetail.DOB = "01/01/1997";
                }

                carcass.DOB = animalDetail.DOB.ToDate();
                animalMovements = animalDetail.AnimalMovements;
                carcass.AgeInMonths = carcass.DOB.ToDate().AddDays(ApplicationSettings.UOMAddDays).DateDifferenceInMonths(DateTime.Today);
                carcass.AgeInDays = carcass.DOB.ToDate().DateDifferenceInDays(DateTime.Today);
                carcass.BreedName = animalDetail.Breed;
                carcass.DamDOB = animalDetail.DamDOB.ToDate();
                carcass.DamBreedName = animalDetail.DamBreed;
                carcass.TagOfOffspring = animalDetail.TagNumDam;
                carcass.Sex = animalDetail.Sex;
                carcass.HasOffSpring = string.IsNullOrEmpty(animalDetail.DamEvent) ? Constant.NoShort.ToUpper().YesNoShortToBool() : animalDetail.DamEvent.YesNoShortToBool();
                carcass.DateOfImport = this.FindImportDate(animalMovements);
                carcass.Imported = carcass.DateOfImport.HasValue;
                carcass.AIMStatus = animalDetail.Status;
                carcass.CountryOfOrigin = this.FindCountryOfOrigin(animalDetail.TagNum, carcass.IsAnimalImported);
                carcass.HerdOfOrigin = animalDetail.HerdOfOrigin;
                carcass.DamEvent = animalDetail.DamEvent;
                carcass.HerdNo = animalDetail.HerdOfOrigin;

                // Find the category and sex.
                var isFemale = carcass.Sex != null && carcass.Sex.CompareIgnoringCase(Constant.F);
                var hasOffspring = carcass.DamEvent == "Y";
                carcass.Category = this.FindCarcassCategory(isFemale, hasOffspring);

                if (carcass.AimsData != null)
                {
                    carcass.AimsData.Sex = carcass.Sex.CompareIgnoringCase("M") ? Strings.Male : Strings.Female;
                    carcass.AimsData.HasOffspring = carcass.DamEvent;
                    carcass.AimsData.NouCategoryID = carcass.Category?.CategoryID;
                }

                // Set the last movement date, which triggers the residency calculations.
                if (carcass.DontQueryHerdCert)
                {
                    carcass.Movements = new List<FarmMovement>();
                    return;
                }

                carcass.DateOfLastMovement = this.FindLastMoveDate(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword, 10, 1, animalDetail.HerdOfOrigin, animalDetail.TagNum, animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration);
                this.CalculateDaysOfResidency(animalMovements, carcass);

                if (ApplicationSettings.RequiredDaysOnCurrentFarm.HasValue && !AuthorisationsManager.Instance.AllowUserEnterAnimalThatDoesNotHaveRequiredCurrentFarmResidency)
                {
                    if (carcass.CurrentFarmResidency.ToInt() < ApplicationSettings.RequiredDaysOnCurrentFarm.ToInt())
                    {
                        carcass.Error = string.Format(Message.RequireCurrentFarmResidencyNotMet,carcass.CurrentFarmResidency, ApplicationSettings.RequiredDaysOnCurrentFarm);
                    }
                }
            }
            else
            {
                this.Log.LogError(this.GetType(), string.Format("AIM did not return valid animal detail for animal with eartag {0}", carcass.Eartag));
            }
        }

        /// <summary>
        /// Calculate the days residency for the animal.
        /// </summary>
        private StockDetail CalculateDaysOfResidency(IList<AIMS.Model.Movement> animalMovements, StockDetail animalDetail)
        {
            IList<Model.BusinessObject.ResidencyCalculation> farmResidency = new List<Model.BusinessObject.ResidencyCalculation>();

            var farmError = string.Empty;
            if (animalDetail.HerdFarmAssured && ApplicationSettings.UsingAims)
            {
                farmResidency = this.FindDaysResidency(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword,
                    10, 1, animalDetail.HerdNo, animalDetail.Eartag,
                    animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration,ref farmError, animalDetail.HerdOfOrigin, animalDetail.PresentingHerdNo);
            }
            else if (animalDetail.HerdFarmAssured)
            {
                animalMovements.Clear();

                // Take the last movement date for the days residency calculate when AIM is disabled. 
                var movement = new AIMS.Model.Movement { MoveFromID = "FXXXXXX", MoveToID = animalDetail.HerdNo, MovementDate = this.LastMovementDate };
                animalMovements.Add(movement);

                //this.Logger.LogTraceMessage(this.GetType(), "Calculating the days residency with AIM disabled.");
                farmResidency = this.FindDaysResidency(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword,
                    10, 1, animalDetail.HerdNo, animalDetail.Eartag,
                    animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration, ref farmError, animalDetail.HerdOfOrigin, animalDetail.PresentingHerdNo);
            }
            else if (!animalDetail.HerdFarmAssured && !ApplicationSettings.UsingAims)
            {
                animalMovements.Clear();

                // Take the last movement date for the days residency calculate when AIM is disabled. 
                var movement = new AIMS.Model.Movement { MoveFromID = "FXXXXXX", MoveToID = animalDetail.HerdNo, MovementDate = this.LastMovementDate };
                animalMovements.Add(movement);

                farmResidency = this.FindDaysResidency(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword,
                 10, 1, animalDetail.HerdNo, animalDetail.Eartag,
                 animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration, ref farmError, animalDetail.HerdOfOrigin, animalDetail.PresentingHerdNo);
            }
            else if (!animalDetail.HerdFarmAssured)
            {
                //farmResidency = this.animalManager.FindDaysResidency(this.animalMovements, wmLairageConfig.BordBiaUserName, wmLairageConfig.BordBiaPassword, wmLairageConfig.BordBiaFarmCount.ToInt32Local(), batchId, this.HerdNo, this.Eartag, this.DateOfBirth.Value, Properties.Settings.Default.QasCertDuration, this.HerdOfOrigin);
                farmResidency = this.FindDaysResidency(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword,
                10, 1, animalDetail.HerdNo, animalDetail.Eartag,
                animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration, ref farmError, animalDetail.HerdOfOrigin, animalDetail.PresentingHerdNo);
            }

            if (!string.IsNullOrEmpty(farmError))
            {
                // null indicates a web service time out, so we must invalidate the current process
                animalDetail.Error = farmError;
                return null;
            }

            if (farmResidency == null)
            {
                // null indicates a web service time out, so we must invalidate the current process
                this.webServiceTimeout = true;
                return null;
            }

            ProgressBar.Run();

            // Remove the existing calculations for the tag.
            var calculationsToDelete = this.ResidencyCalculations.Where(x => x.Eartag == animalDetail.Eartag).ToList();
            var purgedCalculations = new List<Nouvem.Model.BusinessObject.ResidencyCalculation>();

            foreach (var residencyCalculation in this.ResidencyCalculations)
            {
                if (!calculationsToDelete.Contains(residencyCalculation))
                {
                    purgedCalculations.Add(residencyCalculation);
                }

                this.ResidencyCalculations = purgedCalculations;
            }

            // Assign the current, previous and total farms residency to each variable and the session.
            if (!this.ResidencyCalculations.Any(item => item.Eartag == animalDetail.Eartag))
            {
                this.ResidencyCalculations = this.ResidencyCalculations.AddToEnumeration<Model.BusinessObject.ResidencyCalculation>(farmResidency);
            }

            animalDetail.CurrentResidency = 0;
            animalDetail.PreviousResidency = 0;
            animalDetail.TotalResidency = 0;
            animalDetail.CurrentFarmResidency = 0;
            if (farmResidency.Any(item => item.CalculationType == ResidencyCalculationType.CurrentFarm))
            {
                var current = farmResidency.FirstOrDefault(item => item.CalculationType == ResidencyCalculationType.CurrentFarm && item.UseForCalculations);
                if (current != null)
                {
                    animalDetail.CurrentResidency = current.DaysAllocated;
                    animalDetail.CurrentFarmResidency = current.DaysAllocated;
                }
                else
                {
                    current = farmResidency.FirstOrDefault(item => item.CalculationType == ResidencyCalculationType.CurrentFarm);
                    if (current != null)
                    {
                        animalDetail.CurrentFarmResidency = current.DaysAllocated;
                    }
                }
            }

            if (farmResidency.Any(item => item.CalculationType == ResidencyCalculationType.PreviousFarm))
            {
                var previous = farmResidency.FirstOrDefault(item => item.CalculationType == ResidencyCalculationType.PreviousFarm && item.UseForCalculations);
                if (previous != null)
                {
                    animalDetail.PreviousResidency = previous.DaysAllocated;
                }
            }

            if (farmResidency.Any(item => item.CalculationType == ResidencyCalculationType.TotalFarm))
            {
                animalDetail.TotalResidency = farmResidency.Where(item => item.CalculationType == ResidencyCalculationType.TotalFarm && item.UseForCalculations).Select(item => item.DaysAllocated).Sum();
            }

            string numMovements = string.Empty;
            if (ApplicationSettings.UsingAims)
            {
                // Set the number of valid moves.
                numMovements = this.FindNumberOfValidMoves(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword,
                    10, 1, animalDetail.HerdNo, animalDetail.Eartag, animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration).ToString();
            }

            // Set the last herd number.
            var lastHerdNumber = this.FindLastHerdNumber(animalMovements, ApplicationSettings.BordBiaUserName, ApplicationSettings.BordBiaPassword,
                10, 1, animalDetail.HerdNo, animalDetail.Eartag, animalDetail.DOB.ToDate(), Settings.Default.QASCertDuration);

            ProgressBar.Run();
            animalDetail.FactoryHerdMatch = true;
            if (!ApplicationSettings.UsingUKDepartmentServiceQueries)
            {
                animalDetail.FactoryHerdMatch = lastHerdNumber == animalDetail.PresentingHerdNo;
            }
            else
            {
                animalDetail.HoldingNumber = lastHerdNumber;
            }

            animalDetail.NumberOfMoves = numMovements.ToInt();

            // create the movements to store
            var moves = new List<FarmMovement>();
            var validMoves = farmResidency.Where(x => x.CalculationType == ResidencyCalculationType.TotalFarm).ToList();
            foreach (var animalMovement in validMoves)
            {
                var farmMove = new FarmMovement
                {
                    FromFarm = animalMovement.FromId,
                    ToFarm = animalMovement.ToId,
                    MovedDate = animalMovement.MoveFromDate,
                    DaysOnFarm = animalMovement.DaysAllocated,
                    CertExpirydate = animalMovement.CertExpiryDate,
                    UsedForCalculations = animalMovement.UseForCalculations.BoolToYesNo()
                };

                moves.Add(farmMove);
            }

            // add the mart moves
            //var martMoves =
            //    animalMovements.Where(x => (x.MoveFromID.StartsWithIgnoringCase("M") && x.MoveFromID.Length == 4)
            //                               || (x.MoveToID.StartsWithIgnoringCase("M") && x.MoveToID.Length == 4));

            //foreach (var movement in martMoves)
            //{
            //    var farmMove = new FarmMovement
            //    {
            //        FromFarm = movement.MoveFromID,
            //        ToFarm = movement.MoveToID,
            //        MovedDate = movement.MovementDate.ToDate(),
            //        UsedForCalculations = Constant.No
            //    };

            //    moves.Add(farmMove);
            //}

            animalDetail.Movements = moves.OrderBy(x => x.MovedDate).ToList();

            // Verify whether the animal is BQAS approved.
            if (ApplicationSettings.UsingUKDepartmentServiceQueries)
            {
                animalDetail.FarmAssured = this.IsAnimalBCMSApproved(animalDetail);
            }
            else
            {
                animalDetail.FarmAssured = this.IsAnimalBQASApproved(animalDetail);
            }

            //if (this.QasApproved == Constants.YesFull)
            //{
            //    // Set the animal movements.
            //    this.SetAnimalMovements(this.animalMovements, farmResidency);
            //}
            //else
            //{
            //    this.BonusApproved = Constants.NoFull;

            //    // Reset the totals to zero.
            //    this.DaysOfResidencyCurrentFarm = 0;
            //    this.DaysOfResidencyPreviousFarm = 0;
            //    this.TotalDaysOfResidency = 0;

            //    // Reset the animal movements.
            //    this.SetAnimalMovements(this.animalMovements, farmResidency);
            //}

            /* Get the bonus rating (Y/N)
               Note: This should really only be evaluated if the animal is qas, but qas is now a setting on the matrix (for visual and auditing purposes) */
            //this.BonusApproved = this.IsAnimalBonusApproved();
            return animalDetail;
        }

        /// <summary>
        /// Method that checks the animals details against the Bord Bia approval criteria.
        /// </summary>
        /// <returns>a yes or no result as to whether the animal is Bord Bia approved.</returns>
        private bool IsAnimalBQASApproved(StockDetail carcass)
        {
            // if the bonus has not been set up in the database then return "No"
            //if (this.matrixBonusCustomerID == 0)
            //{
            //    return Constant.NoFull;
            //}

            // Create a carcass object with just the relevant Bord Bia check fields.
            //var carcass = Carcass.CreateNewCarcass();
            //carcass.Eartag = this.Eartag;
            //carcass.DateOfBirth = this.DateOfBirth;
            //carcass.Breed = this.Breed;
            //carcass.AgeInMonths = this.AgeMonths.ToInt32Local();
            //carcass.Category = this.Category;
            //carcass.Sex = this.Sex;
            //carcass.NumberOfMoves = this.NumMovements.ToInt32Local();
            //carcass.DateOfLastMovement = this.LastMovementDate;
            //carcass.IsClipped = this.Clipped.YesNoToBool();
            //carcass.Cleanliness = this.Cleanliness;
            //carcass.CountryOfOrigin = this.CountryOfOrigin;
            //carcass.IsLame = this.Lame.YesNoToBool();
            //carcass.IsCasuality = this.Casualty.YesNoToBool();
            //carcass.HasOffspring = this.DamEvent.YesNoToBool();
            //carcass.TagOfOffSpring = this.DamTagNum;

            //carcass.HerdNo = this.herdNo;
            //carcass.IsAnimalImport = this.animalImported;
            //carcass.IsClipped = this.Clipped.YesNoToBool();
            //carcass.IsLame = this.Lame.YesNoToBool();

            //if (this.numMovements.IsNumeric())
            //{
            //    carcass.NumberOfMoves = this.numMovements.ToInt32Local();
            //}

            //var factoryHerdNumbersMatch = this.lastHerdNumber == this.herdNo;
            //carcass.FactoryHerdMatch = this.FactoryHerdNumbersMatch;

            // Do a pre grade check for the BQAS only.
            //var result = EligibilityCheck.CheckBordBiaEligibility(carcass, bordBiaCertDate: this.BordBiaCertExpiryDate);

            //return result.BoolToYesNoFull();
            //if (ApplicationSettings.Customer == "Ballon")
            //{
            //    if (carcass.NumberOfMoves.ToInt() > 4 || carcass.CurrentResidency.ToInt() < 21)
            //    {
            //        return false;
            //    }
            //}

            return carcass.Category != null && carcass.Category.CAT != Constant.CategoryOldBull
                    && carcass.TotalResidency >= ApplicationSettings.BordBiaQAResidency && (carcass.Imported == null || carcass.Imported == false) && carcass.HerdFarmAssured;
        }

        /// <summary>
        /// Method that checks the animals details against the Bord Bia approval criteria.
        /// </summary>
        /// <returns>a yes or no result as to whether the animal is Bord Bia approved.</returns>
        private bool IsAnimalBCMSApproved(StockDetail carcass)
        {
            if (carcass.SupplierID == ApplicationSettings.ScotBeefSupplierID)
            {
                var result = carcass.TotalResidency >= 90 && (carcass.Imported == null || carcass.Imported == false)
                                                          && carcass.NumberOfMoves < 4 && carcass.CurrentResidency >= 30;
                if (carcass.CurrentResidency == 0)
                {
                    carcass.CurrentResidency = carcass.CurrentFarmResidency;
                    carcass.TotalResidency = carcass.CurrentFarmResidency;
                }

                return result;
            }

            return carcass.TotalResidency >= 90 && (carcass.Imported == null || carcass.Imported == false);
        }

        /// <summary>
        /// Find the last herd on which the animal resided.
        /// </summary>
        /// <param name="movementCollection">The collection of movements.</param>
        /// <param name="bordBiaUserName">The username used to connect to BordBia.</param>
        /// <param name="bordBiaPassword">The password used to connect to Bord Bia.</param>
        /// <param name="numberOfFarmsAllowed">The number of farms for which the days is to be calculated. This is not zero based.</param>
        /// <param name="batchId">The identifier for the batch.</param>
        /// <param name="herdNo">The current herd No.</param>
        /// <param name="eartag">The eartag.</param>
        /// <param name="dateOfBirth">The date of birth of the animal.</param>
        /// <param name="certLength">The duration of the certificate in months.</param>
        /// <returns>A collection of residency results.</returns>
        public string FindLastHerdNumber(IList<AIMS.Model.Movement> movementCollection, string bordBiaUserName, string bordBiaPassword, int numberOfFarmsAllowed, int batchId, string herdNo, string eartag, DateTime dateOfBirth, int certLength)
        {
            string lastMoveID = string.Empty;

            var certificateHistory = new List<CertificateHistory>();
            var movements = new List<Movement>();
            //var qualityAssuranceRepository = new QualityAssuranceRepository();

            #region Movement Initialisation

            foreach (var movement in movementCollection)
            {
                movements.Add(Movement.CreateMovement(movement.MoveFromID, movement.MoveToID, movement.MovementDate.ToDate(), moveOff: movement.MovementOffDate.ToDate(), subLocation: movement.SubLocation));

                // Store the histories for the herd number.
                var histories = this.DataManager.FindQualityAssuranceEntries(movement.MoveFromID);

                foreach (var history in histories)
                {
                    certificateHistory.Add(CertificateHistory.CreateCertificateHistory(history.HerdNumber, history.HerdOwner, history.CertExpiryDate, certLength));
                }
            }

            #endregion

            #region Residency Initialisation

            var animalDetail = new AnimalMovementDetail(movements, certificateHistory, herdNo, dateOfBirth, eartag, bordBiaUserName, bordBiaPassword, usingUK: ApplicationSettings.UsingUKDepartmentServiceQueries);

            #endregion

            if (animalDetail.Movements.Any())
            {
                if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                {
                    lastMoveID = animalDetail.Movements.Last().FromId;
                }
                else
                {
                    lastMoveID = animalDetail.Movements.Last().ToId;
                }
            }
            else
            {
                lastMoveID = animalDetail.LotHerdNo;
            }

            return lastMoveID;
        }

        /// <summary>
        /// Find the number of valid farm, non-mart moves.
        /// </summary>
        /// <param name="movementCollection">The collection of movements.</param>
        /// <param name="bordBiaUserName">The username used to connect to BordBia.</param>
        /// <param name="bordBiaPassword">The password used to connect to Bord Bia.</param>
        /// <param name="numberOfFarmsAllowed">The number of farms for which the days is to be calculated. This is not zero based.</param>
        /// <param name="batchId">The identifier for the batch.</param>
        /// <param name="herdNo">The current herd No.</param>
        /// <param name="eartag">The eartag.</param>
        /// <param name="dateOfBirth">The date of birth of the animal.</param>
        /// <param name="certLength">The duration of the certificate in months.</param>
        /// <returns>A collection of residency results.</returns>
        public int FindNumberOfValidMoves(IList<AIMS.Model.Movement> movementCollection, string bordBiaUserName, string bordBiaPassword, int numberOfFarmsAllowed, int batchId, string herdNo, string eartag, DateTime dateOfBirth, int certLength)
        {
            var results = new Dictionary<int, string>();

            int movementCount = 0;
            string lastMoveID = string.Empty;

            var certificateHistory = new List<CertificateHistory>();
            var movements = new List<Movement>();
            // var qualityAssuranceRepository = new QualityAssuranceRepository();

            #region Movement Initialisation

            foreach (var movement in movementCollection)
            {
                movements.Add(Movement.CreateMovement(movement.MoveFromID, movement.MoveToID, movement.MovementDate.ToDate(), moveOff: movement.MovementOffDate.ToDate(), subLocation: movement.SubLocation));

                // Store the histories for the herd number.
                var histories = this.DataManager.FindQualityAssuranceEntries(movement.MoveFromID);

                foreach (var history in histories)
                {
                    certificateHistory.Add(CertificateHistory.CreateCertificateHistory(history.HerdNumber, history.HerdOwner, history.CertExpiryDate, certLength));
                }
            }

            #endregion

            #region Residency Initialisation

            var animalDetail = new AnimalMovementDetail(movements, certificateHistory, herdNo, dateOfBirth, eartag, bordBiaUserName, bordBiaPassword, usingUK: ApplicationSettings.UsingUKDepartmentServiceQueries);

            #endregion

            if (animalDetail != null && animalDetail.Movements.Any())
            {
                animalDetail.UKCheck = ApplicationSettings.UsingUKDepartmentServiceQueries;
                var filteredMovements = animalDetail.FilterMovements();
                movementCount = filteredMovements.Count;
                if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                {
                    movementCount--;
                }
            }

            return movementCount;
        }


        /// <summary>
        /// Find the days residency. 
        /// </summary>
        /// <param name="movementCollection">The collection of movements.</param>
        /// <param name="bordBiaUserName">The username used to connect to BordBia.</param>
        /// <param name="bordBiaPassword">The password used to connect to Bord Bia.</param>
        /// <param name="numberOfFarmsAllowed">The number of farms for which the days is to be calculated. This is not zero based.</param>
        /// <param name="batchId">The identifier for the batch.</param>
        /// <param name="herdNo">The current herd No.</param>
        /// <param name="eartag">The eartag.</param>
        /// <param name="dateOfBirth">The date of birth of the animal.</param>
        /// <param name="certLength">The duration of the certificate in months.</param>
        /// <param name="herdOfOrigin">The herd of origin.</param>
        /// <returns>A collection of residency results.</returns>
        public IList<Model.BusinessObject.ResidencyCalculation> FindDaysResidency(IList<Nouvem.AIMS.Model.Movement> movementCollection, string bordBiaUserName, string bordBiaPassword, int numberOfFarmsAllowed, int batchId, string herdNo, string eartag, DateTime dateOfBirth, int certLength, ref string error,  string herdOfOrigin = "", string presentingHerdNo = "")
        {
            var certificateHistory = new List<CertificateHistory>();
            var movements = new List<Movement>();
            var calculations = new List<Model.BusinessObject.ResidencyCalculation>();

            #region Residency Initialisation

            foreach (var movement in movementCollection)
            {
                movements.Add(Movement.CreateMovement(movement.MoveFromID, movement.MoveToID, movement.MovementDate.ToDate(), moveOff: movement.MovementOffDate.ToDate(), subLocation: movement.SubLocation));

                // Store the histories for the herd number.
                var histories = this.DataManager.FindQualityAssuranceEntries(movement.MoveFromID);

                foreach (var history in histories)
                {
                    certificateHistory.Add(CertificateHistory.CreateCertificateHistory(history.HerdNumber, history.HerdOwner, history.CertExpiryDate, certLength));
                }
            }

            #endregion

            #region Residency and movement calculation

            var animalDetail = new AnimalMovementDetail(movements, certificateHistory, herdNo, dateOfBirth, eartag, bordBiaUserName, bordBiaPassword, false, ApplicationSettings.UsingUKDepartmentServiceQueries);

            var previousFarmResidency = new ResidencyResult();
            var totalFarmResidency = new ResidencyResult();

            // Find the residency result for the current, previous and totals farms.
            var currentFarmResidency = ApplicationSettings.UsingUKDepartmentServiceQueries ?
                animalDetail.FindCurrentUKFarmResidency(herdOfOrigin, presentingHerdNo) :
                animalDetail.FindCurrentFarmResidency(herdOfOrigin, presentingHerdNo);
            ProgressBar.Run();

            if (currentFarmResidency.Eartag.Equals(Constant.WebServiceTimeout))
            {
                // There was a web service time out, so exit with a null
                error = Constant.WebServiceTimeout;
                return null;
            }

            if (!string.IsNullOrEmpty(currentFarmResidency.Error))
            {
                // There was a web service time out, so exit with a null
                error = currentFarmResidency.Error;
                return null;
            }

            previousFarmResidency = ApplicationSettings.UsingUKDepartmentServiceQueries ?
                animalDetail.FindPreviousUKFarmResidency() :
                animalDetail.FindPreviousFarmResidency();

            if (previousFarmResidency.Eartag.Equals(Constant.WebServiceTimeout))
            {
                // There was a web service time out, so exit with a null
                error = Constant.WebServiceTimeout;
                return null;
            }

            if (!string.IsNullOrEmpty(currentFarmResidency.Error))
            {
                // There was a web service time out, so exit with a null
                error = currentFarmResidency.Error;
                return null;
            }

            totalFarmResidency = ApplicationSettings.UsingUKDepartmentServiceQueries ?
                animalDetail.FindTotalUKFarmResidency(numberOfFarmsAllowed) :
                animalDetail.FindTotalFarmResidency(numberOfFarmsAllowed);

            if (totalFarmResidency.Eartag.Equals(Constant.WebServiceTimeout))
            {
                // There was a web service time out, so exit with a null
                error = Constant.WebServiceTimeout;
                return null;
            }

            if (!string.IsNullOrEmpty(currentFarmResidency.Error))
            {
                // There was a web service time out, so exit with a null
                error = currentFarmResidency.Error;
                return null;
            }

            #endregion

            #region Residency Storage

            // Store the residency calculations for logging when the lot is saved.
            foreach (var currentFarmCalculation in currentFarmResidency.ResidencyCalculations)
            {
                calculations.Add(
                   Nouvem.Model.BusinessObject.ResidencyCalculation.CreateResidencyCalculation(
                    ResidencyCalculationType.CurrentFarm,
                    batchId.ToString(),
                    eartag,
                    currentFarmCalculation.MovingFromFarmId,
                    currentFarmCalculation.MovingToFarmId,
                    currentFarmCalculation.MovedToFarmDate,
                    currentFarmCalculation.MovingFromFarmDate,
                    currentFarmCalculation.CertStartDate,
                    currentFarmCalculation.CertExpiryDate,
                    currentFarmCalculation.IsCertDateUsed,
                    currentFarmCalculation.IsHistoricalDataUsed,
                    currentFarmCalculation.DaysAllocated,
                    numberOfFarmsAllowed,
                    currentFarmCalculation.Message,
                    currentFarmCalculation.UseForCalculations));
            }

            foreach (var previousFarmCalculation in previousFarmResidency.ResidencyCalculations)
            {
                calculations.Add(
                    Nouvem.Model.BusinessObject.ResidencyCalculation.CreateResidencyCalculation(
                    ResidencyCalculationType.PreviousFarm,
                    batchId.ToString(),
                    eartag,
                    previousFarmCalculation.MovingFromFarmId,
                    previousFarmCalculation.MovingToFarmId,
                    previousFarmCalculation.MovedToFarmDate,
                    previousFarmCalculation.MovingFromFarmDate,
                    previousFarmCalculation.CertStartDate,
                    previousFarmCalculation.CertExpiryDate,
                    previousFarmCalculation.IsCertDateUsed,
                    previousFarmCalculation.IsHistoricalDataUsed,
                    previousFarmCalculation.DaysAllocated,
                    numberOfFarmsAllowed,
                    previousFarmCalculation.Message,
                    previousFarmCalculation.UseForCalculations));
            }

            foreach (var totalFarmCalculation in totalFarmResidency.ResidencyCalculations)
            {
                calculations.Add(
                    Nouvem.Model.BusinessObject.ResidencyCalculation.CreateResidencyCalculation(
                    ResidencyCalculationType.TotalFarm,
                    batchId.ToString(),
                    eartag,
                    totalFarmCalculation.MovingFromFarmId,
                    totalFarmCalculation.MovingToFarmId,
                    totalFarmCalculation.MovedToFarmDate,
                    totalFarmCalculation.MovingFromFarmDate,
                    totalFarmCalculation.CertStartDate,
                    totalFarmCalculation.CertExpiryDate,
                    totalFarmCalculation.IsCertDateUsed,
                    totalFarmCalculation.IsHistoricalDataUsed,
                    totalFarmCalculation.DaysAllocated,
                    numberOfFarmsAllowed,
                    totalFarmCalculation.Message,
                    totalFarmCalculation.UseForCalculations));
            }

            #endregion

            return calculations;
        }


        /// <summary>
        /// Method to find the category of the animal.
        /// </summary>
        /// <param name="isFemale">Flag to indicate if the animal is female.</param>
        /// <param name="hasOffSpring">Flag to indicate if the animal has offspring.</param>
        /// <returns>The category of the animal.</returns>
        public NouCategory FindCarcassCategory(bool isFemale, bool hasOffSpring)
        {
            var code = isFemale && hasOffSpring ? Constant.CategoryFemaleWithOffspring : isFemale
                && !hasOffSpring ? Constant.CategoryFemale : !isFemale ? Constant.CategoryMale : string.Empty;

            //this.Logger.LogTraceMessage(this.GetType(), string.Format("The carcass category is {0}", categoryFlag));

            return this.Categories.First(x => x.CAT == code);
        }

        /// <summary>
        /// Find the import date for the animal, if one exists.
        /// </summary>
        /// <param name="movementCollection">The collection of movements for the animal.</param>
        /// <returns>The importation date of the animal as a string, or string.Empty if it doesn't exist.</returns>
        /// <remarks>The criteria for a valid import has now change, as a result of the conversation with Trevor on 25/08. The from only needs to be E999.</remarks>
        public DateTime? FindImportDate(IList<Nouvem.AIMS.Model.Movement> movementCollection)
        {
            DateTime? importDate = null;

            try
            {
                var importMovement = (from movement in movementCollection
                                      where movement.MoveFromID == ApplicationSettings.DeptOfAgricultureImportCode
                                      || movement.MoveToID == ApplicationSettings.DeptOfAgricultureImportFarm
                                      select movement).FirstOrDefault();

                if (importMovement != null)
                {
                    importDate = importMovement.MovementDate.ToDate();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }

            return importDate;
        }

        /// <summary>
        /// Find the last movement date for an animal.
        /// </summary>
        /// <param name="movementCollection">The collection of movements.</param>
        /// <param name="bordBiaUserName">The username used to connect to BordBia.</param>
        /// <param name="bordBiaPassword">The password used to connect to Bord Bia.</param>
        /// <param name="numberOfFarmsAllowed">The number of farms for which the days is to be calculated. This is not zero based.</param>
        /// <param name="batchId">The identifier for the batch.</param>
        /// <param name="herdNo">The current herd No.</param>
        /// <param name="eartag">The eartag.</param>
        /// <param name="dateOfBirth">The date of birth of the animal.</param>
        /// <param name="certLength">The duration of the certificate in months.</param>
        /// <returns>A collection of residency results.</returns>
        public string FindLastMoveDate(IList<AIMS.Model.Movement> movementCollection, string bordBiaUserName, string bordBiaPassword, int numberOfFarmsAllowed, int batchId, string herdNo, string eartag, DateTime dateOfBirth, int certLength)
        {
            var lastMoveDate = string.Empty;

            var certificateHistory = new List<CertificateHistory>();
            var movements = new List<Movement>();
            var calculations = new List<ResidencyCalculation>();

            #region Movement Initialisation

            var index = 1;

            foreach (var movement in movementCollection)
            {
                movements.Add(Movement.CreateMovement(movement.MoveFromID, movement.MoveToID, movement.MovementDate.ToDate(), index, moveOff: movement.MovementOffDate.ToDate(), subLocation: movement.SubLocation));
                index++;

                // Store the histories for the herd number.
                var histories = this.DataManager.FindQualityAssuranceEntries(movement.MoveFromID);

                foreach (var history in histories)
                {
                    certificateHistory.Add(CertificateHistory.CreateCertificateHistory(history.HerdNumber, history.HerdOwner, history.CertExpiryDate, certLength));
                }
            }

            #endregion

            #region Residency Initialisation

            var animalDetail = new AnimalMovementDetail(movements, certificateHistory, herdNo, dateOfBirth, eartag, bordBiaUserName, bordBiaPassword, usingUK: ApplicationSettings.UsingUKDepartmentServiceQueries);

            #endregion

            if (animalDetail.Movements.Any())
            {
                var moveDate = animalDetail.FindLastMovementDate();
                lastMoveDate = moveDate.HasValue ? moveDate.Value.ToStringYYYYMMDD() : string.Empty;
            }

            return lastMoveDate;
        }

        /// <summary>
        /// Gets an animal sex.
        /// </summary>
        /// <param name="category">The animal sex.</param>
        /// <returns>The animals sex.</returns>
        public string FindSex(string category)
        {
            return category.Equals("A") || category.Equals("B") || category.Equals("C")
            || category.Equals("V") || category.Equals("Z") ? Strings.Male : Strings.Female;
        }

        /// <summary>
        /// Determines if the sex and category of an animal match.
        /// </summary>
        /// <param name="category">The animal category.</param>
        /// <param name="sex">The animal sex.</param>
        /// <returns>Flag, as to whether there is match or not.</returns>
        public bool IsValidSex(string category, string sex)
        {
            if ((category.Equals("A") || category.Equals("B") || category.Equals("C")) && sex.Equals(Strings.Female))
            {
                return false;
            }

            if ((category.Equals("D") || category.Equals("E") || category.Equals("F")) && sex.Equals(Strings.Male))
            {
                return false;
            }

            return true;
        }

        #region Bord Bia and AIM calls

        /// <summary>
        /// Method to check the status of the herd with Bord Bia.
        /// </summary>
        public HerdStatus IsHerdApproved(string herdNo)
        {
            var useDBForVerification = false;
            ProgressBar.Run();

            var herdStatus = this.CheckHerdStatus(
                herdNo,
                ApplicationSettings.BordBiaUserName,
                ApplicationSettings.BordBiaPassword,
                ApplicationSettings.BordBiaResponsePath,
                true,
                ref useDBForVerification);

            ProgressBar.Run();

            // If we have an error, update the Bord Bia status label to indicate the service isn't available.
            if (!string.IsNullOrEmpty(herdStatus.Error) || herdStatus.CertValidUntil == null || herdStatus.CertValidUntil < DateTime.Now.Date)
            {
                if (!string.IsNullOrEmpty(herdStatus.Error))
                {
                    var bord = ApplicationSettings.UsingUKDepartmentServiceQueries ? "Red Tractor" : "Bord Bia";
                    herdStatus.Message = string.Format("{0} Failure: {1}", bord, herdStatus.Error);
                }
                else if (herdStatus.CertValidUntil < DateTime.Today)
                {
                    herdStatus.Message =
                        string.Format("Current cert expiry date for herd no {0} is {1} and has expired.", herdNo, herdStatus.CertValidUntil);

                }
                else if (!herdStatus.CertValidUntil.HasValue)
                {
                    // Differentiate between a genuine bord bia check qas fail, and a bord bia disabled database cert check
                    if (true) //ViewModelLocator.LairageConfigStatic.EnableBordBiaService)
                    {
                        herdStatus.Message = string.Format("Herd no {0} does not have a current valid cert.", herdNo);
                    }
                    else
                    {
                        herdStatus.Message = string.Format("Bord Bia is currently unavailable. Herd no {0} does not have a current valid cert stored in the database", herdNo);
                    }
                }
                else if (string.IsNullOrEmpty(herdStatus.HerdOwner))
                {
                    var bord = ApplicationSettings.UsingUKDepartmentServiceQueries ? "Red Tractor" : "Bord Bia";
                    herdStatus.Message = string.Format("{0} Failure: No herd owner found for {1}", bord, herdNo);
                }
            }
            else
            {
                var bordBiaLocalDate = herdStatus.CertValidUntil.GetValueOrDefault(DateTime.Now.Date);

                string localMessage;
                if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                {
                    localMessage = "The holding number {0} has been approved by Red Tractor";
                }
                else
                {
                    localMessage = useDBForVerification
                        ? Message.BordBiaDBValidation
                        : Message.BordBiaValidation;
                }

                herdStatus.Message = string.Format(localMessage, herdNo);
                var bordBiaCertExpiryDate = bordBiaLocalDate == DateTime.Now.Date ? string.Empty : bordBiaLocalDate.ToStringYYYYMMDD();
                var bordBiaHerdOwner = herdStatus.HerdOwner;
                if (bordBiaCertExpiryDate.ToNullableDate().HasValue)
                {
                    this.DataManager.AddQualityAssurance(herdNo, bordBiaHerdOwner, bordBiaCertExpiryDate.ToDate());
                }
            }

            ProgressBar.Run();
            return herdStatus;
        }

        /// <summary>
        /// Check the status of a herd with Bord Bia.
        /// </summary>
        /// <param name="herdNo">The number of the herd to be verified.</param>
        /// <param name="username">The username used to access Bord Bia.</param>
        /// <param name="password">The password used to access Bord Bia.</param>
        /// <param name="responsePath">The path at which the response will be stored.</param>
        /// <param name="useService">Flag to indicate if the service is to be used.</param>
        /// <param name="useDBforVerification">Flag to indicate if db is being used for verification.</param>
        /// <returns>BordBiaWSWrapper.HerdStatus object determining the status of the herd.</returns>
        public Nouvem.QAS.Model.HerdStatus CheckHerdStatus(string herdNo, string username, string password, string responsePath, bool useService, ref bool useDBforVerification)
        {
            var herdStatus = Nouvem.QAS.Model.HerdStatus.CreateNewHerdStatus();

            // Retrieve the herd status from Bord Bia if it's enabled.
            if (useService)
            {
                herdStatus = this.CheckHerdStatus(herdNo, username, password, responsePath);
            }

            // If the service is unavailable, check the database.
            if (!string.IsNullOrEmpty(herdStatus.Error) || herdStatus.CertValidUntil == null || !useService)
            {
                useDBforVerification = true;
                var herdCertificates = this.DataManager.FindQualityAssuranceEntries(herdNo);

                if (herdCertificates.Any())
                {
                    var latestCertificate = herdCertificates.OrderByDescending(item => item.CertExpiryDate).First();

                    herdStatus.HerdNo = herdNo;
                    herdStatus.HerdOwner = latestCertificate.HerdOwner;
                    herdStatus.CertValidUntil = latestCertificate.CertExpiryDate;
                }
            }

            return herdStatus;
        }


        /// <summary>
        /// Check the status of a herd with Bord Bia.
        /// </summary>
        /// <param name="herdNo">The number of the herd to be verified.</param>
        /// <param name="username">The username used to access Bord Bia.</param>
        /// <param name="password">The password used to access Bord Bia.</param>
        /// <param name="responsePath">The path at which the response will be stored.</param>
        /// <returns>BordBiaWSWrapper.HerdStatus object determining the status of the herd.</returns>
        public Nouvem.QAS.Model.HerdStatus CheckHerdStatus(string herdNo, string username, string password, string responsePath, DateTime? checkDate = null)
        {
            var herdStatus = QAS.Model.HerdStatus.CreateNewHerdStatus();

            try
            {
                var serviceCall = new QAS.BusinessLogic.WSWrapper();
                var requestFileName = string.Format("{0}{1}.xml", Constant.BordBiaRequestFilePrefix, DateTime.Now.ToString("yyyyMMddhhss"));
                var responseFileName = string.Format("{0}{1}.xml", Constant.BordBiaResponseFilePrefix, DateTime.Now.ToString("yyyyMMddhhss"));
                var responseFilePath = string.Format("{0}{1}{2}.xml", responsePath, Constant.BordBiaResponseFilePrefix, DateTime.Now.ToString("yyyyMMddhhss"));

                // Log the request being sent in the database.
                this.DataManager.LogBordBiaRequest(herdNo, DateTime.Now.ToString(), username, requestFileName);

                if (ApplicationSettings.UsingUKDepartmentServiceQueries)
                {
                    herdStatus = serviceCall.QueryUKBeefService(username, password, herdNo, responseFilePath, checkDate);
                }
                else
                {
                    herdStatus = serviceCall.QueryBeefService(username, password, herdNo, responseFilePath);
                }

                // Log the response being sent in the database.
                if (herdStatus != null)
                {
                    this.DataManager.LogBordBiaResponse(herdNo, DateTime.Now.ToString(), responseFileName, herdStatus.HerdOwner, herdStatus.CertValidUntil.ToString());
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }

            this.BordBiaHerdApproved = herdStatus != null && herdStatus.Error == string.Empty && herdStatus.IsValidCert;
            return herdStatus;
        }



        /// <summary>
        /// Method to check the status of the herd with AIM.
        /// </summary>
        private void VerifyAimHerdStatus(string herdNo)
        {
            if (!string.IsNullOrEmpty(herdNo))
            {
                //if (!File.Exists(vm.CertPath))
                //{
                //    // wrong cert path being used
                //    //this.statusBarManager.LogWrite(DateTime.Now, InformationType.Error, string.Format("{0} {1}", Messages.AimCertPathInvalid, vm.CertPath));
                //    //this.FindStatusInformationRecords();
                //    return;
                //}

                var herdDesignatorStatus = this.VerifyAimHerdDesignator(
                    herdNo,
                    3,
                    ApplicationSettings.AimsLogPath,
                    ApplicationSettings.AimsURL,
                    ApplicationSettings.AimsCertPath,
                    ApplicationSettings.AimsPassword);

                if (herdDesignatorStatus != null && !string.IsNullOrEmpty(herdDesignatorStatus.RegionId) && !string.IsNullOrEmpty(herdDesignatorStatus.HerdWithinRegionId) && string.IsNullOrEmpty(herdDesignatorStatus.Error))
                {
                    //this.AimStatus = herdDesignatorStatus.Status;
                    //this.GetAimsStatus();

                    //this.statusBarManager.LogWrite(DateTime.Now, InformationType.Information, string.Format(Messages.AIMHerdNumberMsg, this.HerdNo));

                    //if (!this.ShowProgressBarError)
                    //{
                    //    this.ShowProgressBarError = false;
                    //}
                }
                else
                {
                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("AIM validation of herd number {0} failed.", this.HerdNo));

                    //if (!string.IsNullOrEmpty(herdDesignatorStatus.Error) && herdDesignatorStatus.Error.StartsWith(Constants.CannotContactAimError))
                    //{
                    //    this.statusBarManager.LogWrite(DateTime.Now, InformationType.Error, Messages.CannotContactAimMessage);
                    //}
                    //else if (!string.IsNullOrEmpty(herdDesignatorStatus.Error) && herdDesignatorStatus.Error.StartsWith(Constants.AimWebServiceError))
                    //{
                    //    this.statusBarManager.LogWrite(DateTime.Now, InformationType.Error, Messages.AimWebServiceError);
                    //}
                    //else
                    //{
                    //    this.statusBarManager.LogWrite(DateTime.Now, InformationType.Warning, string.Format(Messages.AimHerdNumberErrMsg, this.herdNo));
                    //}

                    herdDesignatorStatus = null;

                    //this.GetAimsStatus();
                    //this.AimStatus = Constants.ServiceNotApproved;
                    //this.ShowProgressBarError = true;
                }

                //this.FindStatusInformationRecords();
            }
            else
            {
                //this.AimStatus = Constants.ServiceNotApproved;
            }

            // Update the service calls.
            //var serviceCalls = this.batchManager.FindAllServiceActions(this.BatchNo.ToInt32Local());
            //this.ServiceCalls = this.LoadServiceCalls(serviceCalls);
        }

        /// <summary>
        /// Validate the herd with AIM.
        /// </summary>
        /// <param name="herdNo">The herd number to be validated.</param>
        /// <param name="connectionAttempts">The number of times the request is to be sent to AIM, in the event that it fails.</param>
        /// <param name="logPathToAim">The path on the disk at which the AIM xml interactions are stored.</param>
        /// <param name="aimURL">The url to which the AIM xml request is to be sent.</param>
        /// <param name="certPath">The path at which the AIM security cert is located.</param>
        /// <param name="aimPassword">The password used to unlock the AIM security cert.</param>
        /// <returns>The herd designator response from AIM.</returns>
        public Nouvem.AIMS.BusinessLogic.HerdDesignator VerifyAimHerdDesignator(string herdNo, int connectionAttempts, string logPathToAim, string aimURL, string certPath, string aimPassword)
        {
            var herdDesignatorStatus = new Nouvem.AIMS.BusinessLogic.HerdDesignator();

            try
            {
                int serviceTryCount = 0;

                // Try hitting the web service.
                while (serviceTryCount < connectionAttempts && string.IsNullOrEmpty(herdDesignatorStatus.HerdWithinRegionId))
                {
                    herdDesignatorStatus = this.CheckHerdDesignator(herdNo, logPathToAim, aimURL, certPath, aimPassword);
                    serviceTryCount++;
                }

                // If the attempts limit is reached, AIM is set for disabling. 
                if (serviceTryCount == connectionAttempts)
                {
                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("AIM animal detail service for herd number {0} was not found.", herdNo));
                }
                else
                {
                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("AIM animal detail for herd number {0} found and returned.", herdNo));
                }
            }
            catch (Exception verifyHerdEx)
            {
                herdDesignatorStatus = null;
                //this.Logger.LogError(this.GetType(), string.Format("The call to AIM to verify the herd detail failed with the following error : {0}", verifyHerdEx.ToString()));
            }

            return herdDesignatorStatus;
        }

        /// <summary>
        /// Check the herd designator.
        /// </summary>
        /// <param name="herdNo">The number of the herd to be verified.</param>
        /// <param name="aimLogPath">The path of the AIM logging.</param>
        /// <param name="serviceUrl">The url of the service.</param>
        /// <param name="certPath">The location at which the cert is stored.</param>
        /// <param name="password">The password used to open the cert.</param>
        /// <returns>AIMWebServiceWrapper.Model.HerdDesignators object detailing the designator of the herd.</returns>
        public Nouvem.AIMS.BusinessLogic.HerdDesignator CheckHerdDesignator(string herdNo, string aimLogPath, string serviceUrl, string certPath, string password)
        {
            var herdDesignator = Nouvem.AIMS.BusinessLogic.HerdDesignator.CreateNewHerdDesignator();

            try
            {
                var serviceCall = new Nouvem.AIMS.BusinessLogic.WSWrapper();

                var numTrans = "1";
                var speciesId = "1";
                var transType = Constant.HerdDesignatorRequest;
                var dateOfRequest = DateTime.Now.ToString("yyyy-MM-dd");
                var fileName = string.Format("{0}-{1}{2}{3}", Constant.HerdDesignatorRequest, Settings.Default.AimSenderId, DateTime.Now.ToString("yyyyMMddhhmmssffff"), ".xml");

                // Call the AIM service wrapper.
                herdDesignator = serviceCall.QueryHerdDesignator(
                    fileName,
                    dateOfRequest,
                    numTrans,
                    transType,
                    speciesId,
                    herdNo,
                    ApplicationSettings.AimsURL,
                    ApplicationSettings.AimsCertPath,
                    ApplicationSettings.AimsPassword,
                    true,
                    aimLogPath);

                // Record the request and response in the db.
                var requestFileName = string.Format("{0}{1}\\{2}", aimLogPath, "toAims", fileName);
                var responseFileName = string.Format("{0}{1}\\{2}", aimLogPath, "fromAims", fileName);





                // Append the request and response to the session to write to the database on saving.
                //var xmlRequest = AimXml.CreateAimXmlQuery(0, AimXmlRequest.HerdDesignator, DateTime.Now, string.Empty, requestFileName.ReadFile(), responseFileName.ReadFile());
                //ApplicationSession.LairageSession.XmlInteractions.Clear();
                //ApplicationSession.LairageSession.XmlInteractions.Add(xmlRequest);

                //this.logger.LogTraceMessage(this.GetType(), string.Format("Herd status successfully returned with for herdNo {0} with region Id {1}, herd with region {2}", herdNo, herdDesignator.RegionId, herdDesignator.HerdWithinRegionId));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AIM herd designator service request failed with the following error : {0}", ex.ToString()));
            }

            return herdDesignator;
        }



        /// <summary>
        /// Find the country of origin based on the ear tag.
        /// </summary>
        /// <param name="eartag">The ear tag which is used to decipher the country of origin.</param>
        /// <param name="imported">The eartag imported determination.</param>
        /// <returns>The country name associated with the code.</returns>
        public string FindCountryOfOrigin(string eartag, bool imported)
        {
            // An old irish tag wont have an IE prefix, will be less than 14 characters, and won't be an import
            if (eartag.Length < Settings.Default.IrishEartagLength && !imported)
            {
                //this.Logger.LogTraceMessage(this.GetType(), string.Format("Eartag {0} is an old Irish eartag", eartag));
                return Constant.Ireland;
            }

            if (eartag.Length >= 3 && eartag.StartsWith("372"))
            {
                return Constant.Ireland;
            }

            var countryOfOrigin = string.Empty;

            try
            {
                //this.Logger.LogTraceMessage(this.GetType(), string.Format("Attempting to find the country of origin for eartag {0}", eartag));

                // The country code is the first two characters in the ear tag.
                if (eartag.Length > 2)
                {
                    var countryPrefix = eartag.Substring(0, 2).ToUpper();

                    //this.Logger.LogTraceMessage(this.GetType(), string.Format("The prefix of the country is {0}", countryPrefix));

                    countryOfOrigin = this.DataManager.FindCountryByCode(countryPrefix);

                    if (string.IsNullOrEmpty(countryOfOrigin))
                    {
                        countryOfOrigin = Constant.Unknown;
                    }
                }
                else
                {
                    //this.Logger.LogWarningMessage(this.GetType(), string.Format("Country code length must be more than two. {0} is invalid.", eartag));
                }
            }
            catch (Exception findCountryofOrigin)
            {
                //this.Logger.LogError(this.GetType(), string.Format("Country of origin generation failed with the following error : {0}", findCountryofOrigin.ToString()));
            }

            return countryOfOrigin;
        }

        #endregion


        #endregion

        #endregion

        #region private

        /// <summary>
        /// Converst the nouvem category to corresponding afais category.
        /// </summary>
        /// <param name="category">The Nouvem category to convert.</param>
        /// <returns>The corresponding afais category.</returns>
        private string AfaisSheepCategoryConverter(NouCategory category)
        {
            if (category.Name.CompareIgnoringCase("Ewe"))
            {
                return "EWES";
            }

            if (category.Name.ContainsIgnoringCase("Lamb"))
            {
                return "LAMBS";
            }

            if (category.Name.CompareIgnoringCase("Goat"))
            {
                return "GOATS";
            }

            if (category.Name.CompareIgnoringCase("Ewe"))
            {
                return "EWES";
            }

            return "RAMS";
        }

        /// <summary>
        /// Attach report to the selected email account.
        /// </summary>
        private void AttachReportToEmail(string file)
        {
            try
            {
                this.Log.LogDebug(this.GetType(), string.Format("AttachReportToEmail(): Type:{0} To:{1}, Subject:{2}, Body:{3}",
                                                                ApplicationSettings.SSRSEmailType, ApplicationSettings.SSRSEmailTo,
                                                                ApplicationSettings.SSRSEmailSubject, ApplicationSettings.SSRSEmailBody));

                if (ApplicationSettings.SSRSEmailType.Equals("Outlook"))
                {
                    Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                    var mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
                    mailItem.To = ApplicationSettings.SSRSEmailTo;
                    mailItem.Subject = ApplicationSettings.SSRSEmailSubject;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = ApplicationSettings.SSRSEmailBody;

                    mailItem.Attachments.Add(file, OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                    mailItem.Display(true);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        #endregion
    }
}




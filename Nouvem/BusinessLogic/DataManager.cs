﻿// -----------------------------------------------------------------------
// <copyright file="DataManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using Neodynamic.SDK.Printing;

using Nouvem.Model;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Xml.Linq;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.DataLayer.Repository;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.ReportService;
    using Nouvem.Security;

    /// <summary>
    /// Manager class for the data logic.
    /// </summary>
    public sealed class DataManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly DataManager Manager = new DataManager();

        /// <summary>
        /// The business partner repository reference.
        /// </summary>
        private readonly BusinessPartnerRepository businessPartnerRepository = new BusinessPartnerRepository();

        /// <summary>
        /// The user repository reference.
        /// </summary>
        private readonly IUserRepository userRepository = new UserRepository();

        /// <summary>
        /// The workflow repository reference.
        /// </summary>
        private readonly IWorkflowRepository workflowRepository = new WorkflowRepository();

        /// <summary>
        /// The attribute repository reference.
        /// </summary>
        private readonly IAttributeRepository attributeRepository = new AttributeRepository();

        /// <summary>
        /// The lairage repository reference.
        /// </summary>
        private readonly ILairageRepository lairageRepository = new LairageRepository();

        /// <summary>
        /// The sales repository reference.
        /// </summary>
        private readonly ISalesRepository salesRepository = new SalesRepository();

        /// <summary>
        /// The device repository reference.
        /// </summary>
        private readonly IDeviceRepository deviceRepository = new DeviceRepository();

        /// <summary>
        /// The payments repository reference.
        /// </summary>
        private readonly IPaymentsRepository paymentsRepository = new PaymentsRepository();

        /// <summary>
        /// The department bodies repository reference.
        /// </summary>
        private readonly IDepartmentBodiesRepository departmentBodiesRepository = new DepartmentBodiesRepository();

        /// <summary>
        /// The transaction repository reference.
        /// </summary>
        private readonly ITransactionRepository transactionRepository = new TransactionRepository();

        /// <summary>
        /// The purchases repository reference.
        /// </summary>
        private readonly IPurchasesRepository purchasesRepository = new PurchasesRepository();

        /// <summary>
        /// The stock repository reference.
        /// </summary>
        private readonly IStockRepository stockRepository = new StockRepository();

        /// <summary>
        /// The audit repository reference.
        /// </summary>
        private readonly IAuditRepository auditRepository = new AuditRepository();

        /// <summary>
        /// The license repository reference.
        /// </summary>
        private readonly ILicenseRepository licenseRepository = new LicenseRepository();

        /// <summary>
        /// The system message repository reference.
        /// </summary>
        private readonly SystemMessageRepository systemMessageRepository = new SystemMessageRepository();

        /// <summary>
        /// The label repository reference.
        /// </summary>
        private readonly ILabelRepository labelRepository = new LabelRepository();

        /// <summary>
        /// The document repository reference.
        /// </summary>
        private readonly IDocumentRepository documentRepository = new DocumentRepository();

        /// <summary>
        /// The production repository reference.
        /// </summary>
        private readonly IProductionRepository productionRepository = new ProductionRepository();

        /// <summary>
        /// The dates repository reference.
        /// </summary>
        private readonly IDatesRepository datesRepository = new DatesRepository();

        /// <summary>
        /// The quality repository reference.
        /// </summary>
        private readonly IQualityRepository qualityRepository = new QualityRepository();

        /// <summary>
        /// The traceability repository reference.
        /// </summary>
        private readonly ITraceabilityRepository traceabilityRepository = new TraceabilityRepository();

        /// <summary>
        /// The country repository reference.
        /// </summary>
        private readonly ICountryRepository countryRepository = new CountryRepository();

        /// <summary>
        /// The container repository reference.
        /// </summary>
        private readonly IContainerRepository containerRepository = new ContainerRepository();

        /// <summary>
        /// The region repository reference.
        /// </summary>
        private readonly IRegionRepository regionRepository = new RegionRepository();

        /// <summary>
        /// The region repository reference.
        /// </summary>
        private readonly IRouteRepository routeRepository = new RouteRepository();

        /// <summary>
        /// The container repository reference.
        /// </summary>
        private readonly IItemMasterRepository itemMasterRepository = new ItemMasterRepository();

        /// <summary>
        /// The plant repository reference.
        /// </summary>
        private readonly IPlantRepository plantRepository = new PlantRepository();

        /// <summary>
        /// The uom repository reference.
        /// </summary>
        private readonly IUOMRepository uomRepository = new UOMRepository();

        /// <summary>
        /// The pricing repository reference.
        /// </summary>
        private readonly IPricingRepository pricingRepository = new PricingRepository();

        /// <summary>
        /// The invoice repository reference.
        /// </summary>
        private readonly IInvoiceRepository invoiceRepository = new InvoiceRepository();

        /// <summary>
        /// The report repository reference.
        /// </summary>
        private readonly IReportRepository reportRepository = new ReportRepository();

        /// <summary>
        /// The dynamic repository reference.
        /// </summary>
        private readonly IDynamicRepository dynamicRepository = new DynamicRepository();

        /// <summary>
        /// The batch repository reference.
        /// </summary>
        private readonly IBatchRepository batchRepository = new BatchRepository();

        #endregion

        #region constructor

        private DataManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the data manager singleton.
        /// </summary>
        public static DataManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        #region attribute

        /// <summary>
        /// Gets the cleanliness values.
        /// </summary>
        /// <returns>The application cleanliness values.</returns>
        public IList<NouCleanliness> GetCleanlinesses()
        {
            return this.attributeRepository.GetCleanlinesses();
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>The application categories.</returns>
        public IList<NouCategory> GetCategories()
        {
            return this.attributeRepository.GetCategories();
        }

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        /// <returns>The attribute look ups.</returns>
        public IList<AttributeLookUpData> GetAttributeLookUps()
        {
            return this.attributeRepository.GetAttributeLookUps();
        }

        /// <summary>
        /// Gets the sp names.
        /// </summary>
        /// <returns>The application stored procedure names.</returns>
        public IList<string> GetSPNames()
        {
            return this.attributeRepository.GetSPNames();
        }

        /// <summary>
        /// Adds a new attribute.
        /// </summary>
        /// <param name="attributeData">The attribute and associated data to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddAttribute(AttributeSetUp attributeData)
        {
            return this.attributeRepository.AddAttribute(attributeData);
        }

        /// <summary>
        /// Sets a modules haccp complete flag.
        /// </summary>
        /// <param name="moduleid">The module id.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool SetWorkflowModule(int moduleid, int workflowId)
        {
            return this.attributeRepository.SetWorkflowModule(moduleid, workflowId);
        }

        /// <summary>
        /// Gets the start up checks for a particular process/module.
        /// </summary>
        /// <returns>The relevant start up checks.</returns>
        public Tuple<int, string> GetProcessModuleStartUpChecks(int processId, int moduleid, int inmasterid)
        {
            var checks = this.attributeRepository.GetProcessModuleStartUpChecks(processId, moduleid,inmasterid);
            if (checks != null)
            {
                return Tuple.Create(checks.AttributeTemplateID, checks.Name);
            }

            return null;
        }

        /// <summary>
        /// Adds a range of attributes.
        /// </summary>
        /// <param name="attributes">The attributes to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddAttributes(IList<Model.DataLayer.Attribute> attributes)
        {
            return this.attributeRepository.AddAttributes(attributes);
        }

        /// <summary>
        /// Gets the dress specs.
        /// </summary>
        /// <returns>The application dress specs.</returns>
        public IList<CollectionData> GetDressSpecs()
        {
            return (from spec in this.attributeRepository.GetDressSpecs()
                select new CollectionData {Data = spec.Name, ID = spec.DressSpecID, Identifier = Constant.DressSpec}).ToList();
        }

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        public IList<CollectionData> GetKillingTypes()
        {
            return (from spec in this.attributeRepository.GetKillingType()
                select new CollectionData { Data = spec.Name, ID = spec.KillingTypeID, Identifier = Constant.KillingType}).ToList();
        }

        /// <summary>
        /// Adds/Updates tabs.
        /// </summary>
        /// <param name="tabs">The tabs to update.</param>
        /// <returns>Flag, as to successful add/update.</returns>
        public bool UpdateTabs(IList<AttributeTabName> tabs)
        {
            return this.attributeRepository.UpdateTabs(tabs);
        }

        /// <summary>
        /// Copies a template.
        /// </summary>
        /// <param name="templateToCopyId">The template id to copy from.</param>
        /// <param name="name">The new template name.</param>
        /// <returns>Newly created template id.</returns>
        public int CopyTemplate(int templateToCopyId, string name)
        {
            return this.attributeRepository.CopyTemplate(templateToCopyId, name);
        }

        /// <summary>
        /// Retrieve all the system attribute allocations permissions.
        /// </summary>
        /// <returns>A collection of attribute allocation permissions.</returns>
        public void GetAttributeAllocationPermissions(IList<AttributeAllocationData> allocations, int userId,
            int userGroupId)
        {
            this.attributeRepository.GetAttributeAllocationPermissions(allocations, userId, userGroupId);
        }

        /// <summary>
        /// Retrieve all the system attribute allocations.
        /// </summary>
        /// <returns>A collection of attribute allocations.</returns>
        public void GetAttributeAllocations(IList<AttributeAllocationData> allocations)
        {
            this.attributeRepository.GetAttributeAllocations(allocations);
        }

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public Model.DataLayer.Attribute GetAttributeByBatchId(int id)
        {
            return this.attributeRepository.GetAttributeByBatchId(id);
        }

        /// <summary>
        /// Gets all the batches in a location.
        /// </summary>
        /// <param name="location">The location to check.</param>
        /// <returns>All the batches in a location.</returns>
        public IList<int?> GetBatchesInLocation(Location location)
        {
            return this.attributeRepository.GetBatchesInLocation(location);
        }

        /// <summary>
        /// Gets the start up checks, and completed daily workflows, checking for any unstarted or uncompleted checks.
        /// </summary>
        /// <returns>A list of process required workflows not started.or completed.</returns>
        public IList<string> GetProcessStartUpChecks(int processId)
        {
            return this.attributeRepository.GetProcessStartUpChecks(processId);
        }

        /// <summary>
        /// Gets the attribute by base id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public Model.DataLayer.Attribute GetAttributeByBaseBatchId(int id)
        {
            return this.attributeRepository.GetAttributeByBaseBatchId(id);
        }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>The application attributes.</returns>
        public IList<AttributeMasterData> GetAttributes()
        {
            return this.attributeRepository.GetAttributes();
        }

        /// <summary>
        /// Gets the non standard response authorised groups.
        /// </summary>
        /// <returns>A collection of non standard response authorised groups.</returns>
        public IList<UserGroupNonStandard> GetUserGroupsNonStandard()
        {
            return this.attributeRepository.GetUserGroupsNonStandard();
        }

        /// <summary>
        /// Retrieve all the system traceability template names.
        /// </summary>
        /// <returns>A collection of traceability template names.</returns>
        public IList<AttributeTemplate> GetAttributeTemplateNames()
        {
            return this.attributeRepository.GetAttributeTemplateNames();
        }

        /// <summary>
        /// Retrieve all the system traceability template groups.
        /// </summary>
        /// <returns>A collection of traceability template groups.</returns>
        public IList<AttributeTemplateGroup> GetAttributeTemplateGroups()
        {
            return this.attributeRepository.GetAttributeTemplateGroups();
        }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>The application attributes.</returns>
        public IList<Sale> GetAttributesForSearch()
        {
            return this.attributeRepository.GetAttributesForSearch();
        }

        /// <summary>
        /// Gets the attribute processes.
        /// </summary>
        /// <returns>The application attribute processes.</returns>
        public IList<AttributeProcess> GetAttributeProcesses()
        {
            return this.attributeRepository.GetAttributeProcesses();
        }

        /// <summary>
        /// Gets the  processes.
        /// </summary>
        /// <returns>The application processes.</returns>
        public IList<Process> GetProcesses()
        {
            return this.attributeRepository.GetProcesses();
        }

        /// <summary>
        /// Gets the internal grades.
        /// </summary>
        /// <returns>The internal grades.</returns>
        public IList<InternalGrade> GetInternalGrades()
        {
            return this.attributeRepository.GetInternalGrades();
        }

        /// <summary>
        /// Adds or updates a batch attribute.
        /// </summary>
        /// <returns>Flag, as to successful add or update..</returns>
        public int AddOrUpdateBatchAttributes(StockDetail details)
        {
            return this.attributeRepository.AddOrUpdateBatchAttributes(details);
        }

        /// <summary>
        /// Adds or updates a batch attribute.
        /// </summary>
        /// <returns>Flag, as to successful update.</returns>
        public bool UpdateAllBatchAttributes(StockDetail detail, IList<int?> batchIds)
        {
            return this.attributeRepository.UpdateAllBatchAttributes(detail, batchIds);
        }

        /// <summary>
        /// Adds or updates traceability names.
        /// </summary>
        /// <param name="traceabilityNames">The traceability names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateAttributeTemplateNames(IList<AttributeTemplate> traceabilityNames)
        {
            return this.attributeRepository.AddOrUpdateAttributeTemplateNames(traceabilityNames);
        }

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        /// <returns>A collection of tabs.</returns>
        public IList<AttributeTabName> GetAttributeTabNames()
        {
            return this.attributeRepository.GetAttributeTabNames();
        }

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        /// <returns>A collection of tabs.</returns>
        public IList<NouAttributeReset> GetAttributeResets()
        {
            return this.attributeRepository.GetAttributeResets();
        }

        /// <summary>
        /// Updates an attribute.
        /// </summary>
        /// <param name="attributeData">The attribute and associated data to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateAttribute(AttributeSetUp attributeData)
        {
            return this.attributeRepository.UpdateAttribute(attributeData);
        }

        /// <summary>
        /// Retrieve the allocation data for the input template id.
        /// </summary>
        /// <returns>An ttribute allocation.</returns>
        public IList<AttributeAllocationData> GetAttributeAllocationForTemplate(int templateId)
        {
            return this.attributeRepository.GetAttributeAllocationForTemplate(templateId);
        }

        /// <summary>
        /// Adds an attribute to a template.
        /// </summary>
        /// <param name="traceabilityMasters">The collection of the attributes to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddAttributesToTemplate(IList<AttributeMasterData> traceabilityMasters, int templateId, bool isWorkflow, string notes, bool inactive, string attributeType)
        {
            return this.attributeRepository.AddAttributesToTemplate(traceabilityMasters, templateId, isWorkflow, notes, inactive, attributeType);
        }

        /// <summary>
        /// Validates a transaction.
        /// </summary>
        /// <param name="spName">The sp macro.</param>
        /// <param name="id">The document id.</param>
        /// <param name="param1">param 1</param>
        /// <param name="param2">param 2</param>
        /// <param name="param3">param 3</param>
        /// <param name="param4">param 4</param>
        /// <returns>A validation issue message, or empty if validated.</returns>
        public string GetSPDataResultReturn(string spName, int id, decimal param1, decimal param2, int inmasterid, int param3,
            int param4, string reference, string reference2 = "", string reference3 = "", string reference4 = "")
        {
            return this.dynamicRepository.GetSPDataResultReturn(
                spName, id, param1, param2, inmasterid, param3, param4, 
                NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt(), reference, reference2, reference3,reference4);
        }

        /// <summary>
        /// Retrieve all the system attribute allocations.
        /// </summary>
        /// <returns>A collection of attribute allocations.</returns>
        public IList<AttributeAllocationData> GetAttributeAllocations()
        {
            return this.attributeRepository.GetAttributeAllocations();
        }

        /// <summary>
        /// Adds or updates traceability groups.
        /// </summary>
        /// <param name="traceabilityNames">The traceability groups to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateAttributeTemplateGroups(IList<AttributeTemplateGroup> traceabilityNames)
        {
            return this.attributeRepository.AddOrUpdateAttributeTemplateGroups(traceabilityNames);
        }

        /// <summary>
        /// Gets the last attribute.
        /// </summary>
        /// <returns>The last edited attribute.</returns>
        public AttributeSetUp GetAttributeByLastEdit()
        {
            return this.attributeRepository.GetAttributeByLastEdit();
        }

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public AttributeSetUp GetAttributeById(int id)
        {
            return this.attributeRepository.GetAttributeById(id);
        }

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public AttributeSetUp GetAttributeByFirstLast(bool first)
        {
            return this.attributeRepository.GetAttributeByFirstLast(first);
        }

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        public IList<NouBreed> GetBreeds()
        {
            return this.attributeRepository.GetBreeds();
        }

        /// <summary>
        /// Gets the fat colours.
        /// </summary>
        /// <returns>The application fat colours.</returns>
        public IList<NouFatColour> GetFatColours()
        {
            return this.attributeRepository.GetFatColours();
        }

        /// <summary>
        /// Gets the destinations.
        /// </summary>
        /// <returns>The application destinations.</returns>
        public IList<Destination> GetDestinations()
        {
            return this.attributeRepository.GetDestinations();
        }

        /// <summary>
        /// Adds a breed.
        /// </summary>
        /// <returns>Flag, indicating successful breed addition.</returns>
        public bool AddBreed(NouBreed breed)
        {
            return this.attributeRepository.AddBreed(breed);
        }

        #endregion

        #region audit

        /// <summary>
        /// Gets the audit details.
        /// </summary>
        /// <returns>A collection of audit details.</returns>
        public IList<Audit> GetAuditDetails()
        {
            return this.auditRepository.GetAuditDetails();
        }

        /// <summary>
        /// Gets the audit details for the current day.
        /// </summary>
        /// <returns>A collection of todays audit details.</returns>
        public IList<Audit> GetTodaysAuditDetails()
        {
            return this.auditRepository.GetTodaysAuditDetails();
        }

        #endregion

        #region batch

        /// <summary>
        /// Generates a sequential batch number.
        /// </summary>
        /// <returns>A sequential batch number.</returns>
        public BatchNumber GenerateSequentialNumber(string reference = "", bool referenceOnly = false)
        {
            return this.batchRepository.GenerateSequentialNumber(reference, referenceOnly);
        }

        /// <summary>
        /// Gets a batch number.
        /// </summary>
        /// <returns>A batch number.</returns>
        public BatchNumber GetBatchNumber(string batchNumber)
        {
            return this.batchRepository.GetBatchNumber(batchNumber);
        }

        /// <summary>
        /// Gets the batches.
        /// </summary>
        /// <returns>The collection of batch options.</returns>
        public IList<BatchData> GetBatches(int inmasterid)
        {
            var batches = this.batchRepository.GetBatches(inmasterid);
            if (batches == null)
            {
                return new List<BatchData>();
            }

            return
                (from batch in batches
                    select new BatchData
                    {
                        BatchNumberID = batch.BatchNumberID,
                        Reference = batch.Reference,
                        Product = batch.Product,
                        INMasterID = batch.INMasterID
                    }).ToList();
        }

        /// <summary>
        /// Gets the used batch weight for a recipe batch line.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <param name="inMasterId">The line product id.</param>
        /// <returns>Used batch data for a recipe batch.</returns>
        public ProductionData GetLastRecipeUsedBatchWeight(int batchId, int inMasterId)
        {
            var data = this.batchRepository.GetLastRecipeUsedBatchWeight(batchId, inMasterId);
            if (data != null)
            {
                return new ProductionData
                {
                    BatchWeight = data.ProductionWeight,
                    IntakeWeight = data.WeightReceived,
                    BatchQty = data.ProductionQty,
                    IntakeQty = data.QuantityReceived
                };
            }

            return null;
        }

        /// <summary>
        /// Generates a manually entered batch number.
        /// </summary>
        /// <param name="batchNo">The manually generated batch number.</param>
        /// <returns>The newly generated batch number.</returns>
        public BatchNumber GenerateManualEntryNumber(BatchNumber batchNo)
        {
            return this.batchRepository.GenerateManualEntryNumber(batchNo);
        }

        /// <summary>
        /// Returns the last batch number associated with a batch product.
        /// </summary>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>The last associated product batch.</returns>
        public BatchNumber GetLastProductBatch(int inMasterId)
        {
            return this.batchRepository.GetLastProductBatch(inMasterId);
        }

        /// <summary>
        /// Gets the last recipe batch data for a recipe ingredient line.
        /// </summary>
        /// <param name="prOrderID">The recipe batch.</param>
        /// <param name="inMasterId">The ingredient id.</param>
        /// <returns>The last recipe batch data for a recipe ingredient line.</returns>
        public App_GetLastRecipeProductBatch_Result GetLastRecipeProductBatch(int prOrderID, int inMasterId)
        {
            return this.batchRepository.GetLastRecipeProductBatch(prOrderID, inMasterId);
        }

        /// <summary>
        /// Gets the batch options.
        /// </summary>
        /// <returns>The collection of batch options.</returns>
        public IList<BatchOption> GetBatchOptions()
        {
            return this.batchRepository.GetBatchOptions();
        }

        /// <summary>
        /// Gets a batch number.
        /// </summary>
        /// <returns>A batch number.</returns>
        public BatchNumber GetBatchNumber(int batchNumberId)
        {
            return this.batchRepository.GetBatchNumber(batchNumberId);
        }

        #endregion

        #region business partner

        /// <summary>
        /// Gets the partner matching in input id..
        /// </summary>
        /// <returns>A matching partner.</returns>
        public BusinessPartner GetBusinessPartner(int partnerId)
        {
            return this.businessPartnerRepository.GetBusinessPartner(partnerId);
        }

        /// <summary>
        /// Gets the partner matching in input id..
        /// </summary>
        /// <returns>A matching partner.</returns>
        public ViewBusinessPartner GetBusinessPartnerShort(int partnerId)
        {
            return this.businessPartnerRepository.GetBusinessPartnerShort(partnerId);
        }

        /// <summary>
        /// Method that retrieves the master parner entities.
        /// </summary>
        /// <returns>A collection of master partner entities.</returns>
        public IList<ViewBusinessPartner> GetBusinessPartnerMaster()
        {
            return this.businessPartnerRepository.GetBusinessPartners();
        }

        /// <summary>
        /// Method that retrieves the last 100 business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public IList<ViewBusinessPartner> GetRecentBusinessPartners()
        {
            return this.businessPartnerRepository.GetRecentBusinessPartners();
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnersToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateBusinessPartnerPricing(IList<BusinessPartner> partnersToUpdate)
        {
            return this.businessPartnerRepository.UpdateBusinessPartnerPricing(partnersToUpdate);
        }

        /// <summary>
        /// Method that retrieves the parner address entities.
        /// </summary>
        /// <returns>A collection of partner address entities.</returns>
        public IList<BusinessPartnerAddress> GetBusinessPartnerAddresses()
        {
            return this.businessPartnerRepository.GetBusinessPartnerAddresses();
        }

        /// <summary>
        /// Gets the inactive partners.
        /// </summary>
        /// <returns>The inactive partners.</returns>
        public void GetInactivePartners()
        {
            var inactivePartners = this.businessPartnerRepository.GetInactivePartners();
            foreach (var businessPartner in NouvemGlobal.BusinessPartners)
            {
                var localPartner =
                    inactivePartners.FirstOrDefault(x => x.BPMasterID == businessPartner.Details.BPMasterID);
                if (localPartner != null)
                {
                    businessPartner.Details.InActiveFrom = localPartner.InActiveFrom;
                    businessPartner.Details.InActiveTo = localPartner.InActiveTo;
                }
                else
                {
                    businessPartner.Details.InActiveFrom = null;
                    businessPartner.Details.InActiveTo = null;
                }

                var localCustomer = NouvemGlobal.CustomerPartners.FirstOrDefault(x =>
                    x.Details.BPMasterID == businessPartner.Details.BPMasterID);
                if (localCustomer != null)
                {
                    if (localPartner != null)
                    {
                        localCustomer.Details.InActiveFrom = localPartner.InActiveFrom;
                        localCustomer.Details.InActiveTo = localPartner.InActiveTo;
                    }
                    else
                    {
                        localCustomer.Details.InActiveFrom = null;
                        localCustomer.Details.InActiveTo = null;
                    }
                }

                var localSupplier = NouvemGlobal.SupplierPartners.FirstOrDefault(x =>
                    x.Details.BPMasterID == businessPartner.Details.BPMasterID);
                if (localSupplier != null)
                {
                    if (localPartner != null)
                    {
                        localSupplier.Details.InActiveFrom = localPartner.InActiveFrom;
                        localSupplier.Details.InActiveTo = localPartner.InActiveTo;
                    }
                    else
                    {
                        localSupplier.Details.InActiveFrom = null;
                        localSupplier.Details.InActiveTo = null;
                    }
                }
            }
        }

        /// <summary>
        /// Updates a partner agent.
        /// </summary>
        /// <param name="partnerId">The partner to update.</param>
        /// <param name="agentId">The agent id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePartnerAgent(int partnerId, int? agentId)
        {
            return this.businessPartnerRepository.UpdatePartnerAgent(partnerId, agentId);
        }

        /// <summary>
        /// Method that retrieves the parner currency entities.
        /// </summary>
        /// <returns>A collection of partner currency entities.</returns>
        public IList<BPCurrency> GetBusinessPartnerCurrencies()
        {
            return this.businessPartnerRepository.GetBusinessPartnerCurrencies();
        }

        /// <summary>
        /// Method that retrieves the parner attachment entities.
        /// </summary>
        /// <returns>A collection of partner attachment entities.</returns>
        public IList<BPAttachment> GetBusinessPartnerAttachments()
        {
            return this.businessPartnerRepository.GetBusinessPartnerAttachments();
        }

        /// <summary>
        /// Method that retrieves the parner entities.
        /// </summary>
        /// <returns>A collection of contact entities.</returns>
        public IList<BusinessPartnerContact> GetBusinessPartnerContacts()
        {
            return this.businessPartnerRepository.GetBusinessPartnerContacts();
        }

        /// <summary>
        /// Method which returns the non deleted business partners contacts.
        /// </summary>
        /// <returns>A collection of non deleted business partner contacts.</returns>
        public IList<BusinessPartnerContact> GetBusinessPartnerContactsById(int partnerId)
        {
            return this.businessPartnerRepository.GetBusinessPartnerContactsById(partnerId);
        }

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="partnerId">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public int GetPartnerPriceListId(int partnerId)
        {
            return this.businessPartnerRepository.GetPartnerPriceListId(partnerId);
        }

        /// <summary>
        /// Method that retrieves the parner group entities.
        /// </summary>
        /// <returns>A collection of group entities.</returns>
        public IList<BPGroup> GetBusinessPartnerGroups()
        {
            return this.businessPartnerRepository.GetGroups();
        }

        /// <summary>
        /// Method that retrieves the parner group types.
        /// </summary>
        /// <returns>A collection of group types.</returns>
        public IList<NouBPType> GetBusinessPartnerTypes()
        {
            return this.businessPartnerRepository.GetTypes();
        }

        /// <summary>
        /// Method that retrieves the parner group types.
        /// </summary>
        /// <returns>A collection of group types.</returns>
        public IList<PartnerType> GetPartnerTypes()
        {
            return this.businessPartnerRepository.GetPartnerTypes();
        }

        /// <summary>
        /// Method that retrieves the parner properties.
        /// </summary>
        /// <returns>A collection of partner properties.</returns>
        public IList<BPPropertySelection> GetBusinessPartnerProperties()
        {
            return this.businessPartnerRepository.GetProperties();
        }

        /// <summary>
        /// Method which returns the non deleted business partner label fields.
        /// </summary>
        /// <returns>A collection of non deleted business partner label fields.</returns>
        public IList<BPLabelField> GetLabelFields()
        {
            return this.businessPartnerRepository.GetLabelFields();
        }

        /// <summary>
        /// Method that retrieves the properties.
        /// </summary>
        /// <returns>A collection of properties.</returns>
        public IList<BPProperty> GetProperties()
        {
            return this.businessPartnerRepository.GetBPProperties();
        }

        /// <summary>
        /// Add or updates the partner groups list.
        /// </summary>
        /// <param name="groups">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePartnerGroups(ref List<PartnerGroup> groups)
        {
            return this.businessPartnerRepository.AddOrUpdatePartnerGroups(ref groups);
        }

        /// <summary>
        /// Update the global groups.
        /// </summary>
        /// <param name="groups">The group data.</param>
        public void UpdateGlobalGroups(IList<PartnerGroup> groups)
        {
            NouvemGlobal.BusinessPartnerGroups = (from partnerGroup in groups
                                                  select new BPGroup
                                                  {
                                                      BPGroupID = partnerGroup.PartnerId,
                                                      BPGroupName = partnerGroup.Name
                                                  }).ToList();
        }

        /// <summary>
        /// Add or updates the partner types list.
        /// </summary>
        /// <param name="types">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePartnerTypes(ref List<PartnerType> types)
        {
            return this.businessPartnerRepository.AddOrUpdatePartnerTypes(ref types);
        }

        /// <summary>
        /// Determines is a partner code already exists in the db.
        /// </summary>
        /// <param name="code">The code to check.</param>
        /// <returns>Flag, as to whether the code exists.</returns>
        public bool DoesPartnerCodeExist(string code)
        {
            return this.businessPartnerRepository.DoesPartnerCodeExist(code);
        }

        /// <summary>
        /// Update the global types.
        /// </summary>
        /// <param name="types">The types data.</param>
        public void UpdateGlobalTypes(IList<PartnerType> types)
        {
            NouvemGlobal.BusinessPartnerTypes = (from partnerType in types
                                                 select new NouBPType
                                                 {
                                                     NouBPTypeID = partnerType.PartnerTypeId,
                                                     Type = partnerType.Type
                                                 }).ToList();
        }

        /// <summary>
        /// Method that retrieves the partner groups.
        /// </summary>
        /// <returns>A collection of partner groups.</returns>
        public IList<PartnerGroup> GetPartnerGroups()
        {
            return this.businessPartnerRepository.GetPartnerGroups();
        }

        /// <summary>
        /// Add or updates the currencies list.
        /// </summary>
        /// <param name="currencies">The currencies to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateCurrencies(IList<BPCurrency> currencies)
        {
            return this.businessPartnerRepository.AddOrUpdateCurrencies(currencies);
        }

        /// <summary>
        /// Method that retrieves the property selections..
        /// </summary>
        /// <returns>A collection of property selections.</returns>
        public IList<BPPropertySelection> GetPropertySelection()
        {
            return this.businessPartnerRepository.GetProperties();
        }

        /// <summary>
        /// Method that returns the business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public Tuple<List<BusinessPartner>, IList<BPGroup>, IList<BPCurrency>, IList<NouBPType>> GetBusinessPartners()
        {
            var businessPartners = new List<BusinessPartner>();
            //var localContacts = this.GetBusinessPartnerContacts();
            //var localAddresses = this.GetBusinessPartnerAddresses();
            //var localAttachments = this.GetBusinessPartnerAttachments();
            var localPartners = this.GetBusinessPartnerMaster();
            var localGroups = this.GetBusinessPartnerGroups();
            var localCurrencies = this.GetBusinessPartnerCurrencies();
            var localTypes = this.GetBusinessPartnerTypes();
            //var localPropertySelection = this.GetPropertySelection();
            //var localProperties = this.GetProperties();
            //var localLabels = this.GetLabelFields();

            localPartners.ToList().ForEach(
                localPartner =>
                {
                    var partner = new BusinessPartner
                    {
                        Details = localPartner,
                        //Addresses = localAddresses.Where(x => x.Details.BPMasterID == localPartner.BPMasterID && !x.Details.Deleted).ToList(),
                        //Contacts = localContacts.Where(x => x.Details.BPMasterID == localPartner.BPMasterID && !x.Details.Deleted).ToList(),
                        //Attachments = localAttachments.Where(x => x.BPMasterID == localPartner.BPMasterID && !x.Deleted).ToList(),
                        PartnerGroup = localGroups.FirstOrDefault(x => x.BPGroupID == localPartner.BPGroupID),
                        //PartnerCurrency = localCurrencies.FirstOrDefault(x => x.BPCurrencyID == localPartner.BPCurrencyID),
                        PartnerType = localTypes.FirstOrDefault(x => x.NouBPTypeID == localPartner.NouBPTypeID),
                        //PartnerProperties = localPropertySelection.Where(x => x.BPMasterID == localPartner.BPMasterID)
                        //                                          .Select(x => new BusinessPartnerProperty
                        //                                          {
                        //                                              Details = new BPProperty { BPPropertyID = x.BPPropertyID },
                        //                                              DisplayMessage = x.DisplayMessage
                        //                                          }).ToList(),
                        //LabelField = localLabels.FirstOrDefault(x => x.BPLabelFieldID == localPartner.BPLabelFieldID)
                    };

                    businessPartners.Add(partner);
                });
      
            return Tuple.Create(businessPartners, localGroups, localCurrencies, localTypes);
        }

        /// <summary>
        /// Method that returns the business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public Tuple<List<BusinessPartner>, IList<BPGroup>, IList<BPCurrency>, IList<NouBPType>, IList<BusinessPartnerContact>, IList<BPProperty>> GetUpdatedBusinessPartners()
        {
            var localPartners = this.GetRecentBusinessPartners();
            if (localPartners.Count == 0)
            {
                return null;
            }

            var businessPartners = new List<BusinessPartner>();
            var localContacts = this.GetBusinessPartnerContacts();
            var localAddresses = this.GetBusinessPartnerAddresses();
            var localAttachments = this.GetBusinessPartnerAttachments();
            var localGroups = NouvemGlobal.BusinessPartnerGroups;
            var localCurrencies = NouvemGlobal.BusinessPartnerCurrencies;
            var localTypes = NouvemGlobal.BusinessPartnerTypes;
            var localPropertySelection = this.GetPropertySelection();
            var localProperties = this.GetProperties();

            localPartners.ToList().ForEach(
                localPartner =>
                {
                    var partner = new BusinessPartner
                    {
                        Details = localPartner,
                        Addresses = localAddresses.Where(x => x.Details.BPMasterID == localPartner.BPMasterID && !x.Details.Deleted).ToList(),
                        Contacts = localContacts.Where(x => x.Details.BPMasterID == localPartner.BPMasterID && !x.Details.Deleted).ToList(),
                        Attachments = localAttachments.Where(x => x.BPMasterID == localPartner.BPMasterID && !x.Deleted).ToList(),
                        PartnerGroup = localGroups.FirstOrDefault(x => x.BPGroupID == localPartner.BPGroupID),
                        PartnerCurrency = localCurrencies.FirstOrDefault(x => x.BPCurrencyID == localPartner.BPCurrencyID),
                        PartnerType = localTypes.FirstOrDefault(x => x.NouBPTypeID == localPartner.NouBPTypeID),
                        PartnerProperties = localPropertySelection.Where(x => x.BPMasterID == localPartner.BPMasterID)
                                                                   .Select(x => new BusinessPartnerProperty
                                                                   {
                                                                       Details = new BPProperty { BPPropertyID = x.BPPropertyID },
                                                                       DisplayMessage = x.DisplayMessage
                                                                   }).ToList(),
                        Route = NouvemGlobal.Routes.FirstOrDefault(x => x.RouteID == localPartner.RouteMasterID)
                    };

                    businessPartners.Add(partner);
                });

            return Tuple.Create(businessPartners, localGroups, localCurrencies, localTypes, localContacts, localProperties);
        }

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="attachment">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool RemoveAttachment(BPAttachment attachment)
        {
            return this.businessPartnerRepository.DeleteAttachment(attachment);
        }

        /// <summary>
        /// Method that adds a new business partner.
        /// </summary>
        /// <param name="partner">The partner to add.</param>
        /// <returns>The newly added partner id.</returns>
        public int AddBusinessPartner(BusinessPartner partner)
        {
            return this.businessPartnerRepository.AddBusinessPartner(partner);
        }

        /// <summary>
        /// Returns a partners on hold status.
        /// </summary>
        /// <param name="partnerID">The partner id.</param>
        /// <returns>The partners on hold status.</returns>
        public bool GetPartnerOnHoldStatus(int partnerID)
        {
            return this.businessPartnerRepository.GetPartnerOnHoldStatus(partnerID);
        }

        /// <summary>
        /// Returns a partners on hold status.
        /// </summary>
        /// <param name="partnerID">The partner id.</param>
        /// <returns>The partners on hold status.</returns>
        public int IsPartnerLiveOrderOnSystem(int partnerID)
        {
            return this.businessPartnerRepository.IsPartnerLiveOrderOnSystem(partnerID);
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnerToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateBusinessPartner(BusinessPartner partnerToUpdate)
        {
            return this.businessPartnerRepository.UpdateBusinessPartner(partnerToUpdate);
        }

        /// <summary>
        /// Method that makes a repository call to add or update a new partner.
        /// </summary>
        /// <param name="partner">The business partner to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateBusinessPartner(BusinessPartner partner, BusinessPartner partnerSnapshot)
        {
            // Create the audit data
            //var audit = new Audit
            //{
            //    TableName = Constant.BusinessPartner,
            //    TableID = partner.Details.BPMasterID,
            //    TransactionType = Constant.Add,
            //    UserMasterID = NouvemGlobal.LoggedInUser.UserMaster.UserMasterID,
            //    UserName = NouvemGlobal.LoggedInUser.UserMaster.UserName,
            //    UserFullName = NouvemGlobal.LoggedInUser.UserMaster.FullName,
            //    TransactionDate = DateTime.Now,
            //    Deleted = false
            //};

            //partner.Audit = audit;

            if (partner.Details.BPMasterID == 0)
            {
                partner.Details.BPMasterID = this.businessPartnerRepository.AddBusinessPartner(partner);
                return partner.Details.BPMasterID > 0;
            }

            //partner.Audit.TransactionType = Constant.Update;
            //var changes = new Dictionary<object, object>
            //{
            //    {partner.Details, partnerSnapshot.Details},
            //    {partner.Addresses, partnerSnapshot.Addresses},
            //    {partner.Contacts, partnerSnapshot.Contacts},
            //    {partner.Attachments, partnerSnapshot.Attachments},
            //    {partner.PartnerProperties, partnerSnapshot.PartnerProperties}
            //};

            //var changedProperties = this.GetChangedProperties(changes);
            //partner.Audit.DataChanges = this.CreateAuditXml(changedProperties);

            // Update
            return this.businessPartnerRepository.UpdateBusinessPartner(partner);
        }

        /// <summary>
        /// Gets a bp partner.
        /// </summary>
        /// <param name="partnerId">The partner id to search for.</param>
        /// <returns>A bp partner.</returns>
        public BPMaster GetPartner(int partnerId)
        {
            return this.businessPartnerRepository.GetPartner(partnerId);
        }

        /// <summary>  Add or updates the properties list.
        /// </summary>
        /// <param name="properties">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateProperties(IList<BPProperty> properties)
        {
            return this.businessPartnerRepository.AddOrUpdateProperties(properties);
        }

        #endregion

        #region container

        /// <summary>
        /// Retrieve all the containers.
        /// </summary>
        /// <returns>A collection of containers.</returns>
        public IList<Container> GetContainers()
        {
            return this.containerRepository.GetContainers();
        }

        /// <summary>
        /// Retrieve all the containers types.
        /// </summary>
        /// <returns>A collection of container types.</returns>
        public IList<ContainerType> GetContainerTypes()
        {
            return this.containerRepository.GetContainerTypes();
        }

        /// <summary>
        /// Add or updates the containers list.
        /// </summary>
        /// <param name="containers">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateContainers(IList<Container> containers)
        {
            return this.containerRepository.AddOrUpdateContainers(containers);
        }

        /// <summary>
        /// Retrieve all the product containers.
        /// </summary>
        /// <returns>A collection of containers.</returns>
        public IList<TareCalculator> GetProductContainers()
        {
            var linkedTares = new List<TareCalculator>();
            var tares = this.containerRepository.GetProductContainers();
            var tareProducts = NouvemGlobal.InventoryItems.Where(x => !x.Master.TareProductContainerID.IsNullOrZero()).ToList();
            foreach (var tare in tares)
            {
                var localTareProduct =
                    tareProducts.FirstOrDefault(
                        x =>
                            x.LinkedProductTareContainer != null &&
                            x.LinkedProductTareContainer.ContainerID == tare.ContainerID);

                if (localTareProduct != null)
                {
                    tare.INMasterID = localTareProduct.Master.INMasterID;
                    linkedTares.Add(tare);
                }
            }

            return linkedTares;
        }

        #endregion

        #region country

        /// <summary>
        /// Retrieve all the countries.
        /// </summary>
        /// <returns>A collection of countries.</returns>
        public IList<Country> GetCountryMaster()
        {
            return this.countryRepository.GetCountryMaster();
        }

        /// <summary>
        /// Find the country code for a particular country code from an ear tag.
        /// </summary>
        /// <param name="code">The code of the country as determine by the ear tag.</param>
        /// <returns>The country relating to the code.</returns>
        public string FindCountryByCode(string code)
        {
            return this.countryRepository.FindCountryByCode(code);
        }

        /// <summary>
        /// Add or updates the countries list.
        /// </summary>
        /// <param name="countries">The countries to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateCountryMasters(IList<Country> countries)
        {
            return this.countryRepository.AddOrUpdateCountryMasters(countries);
        }

        #endregion

        #region date

        /// <summary>
        /// Adds or updates date days.
        /// </summary>
        /// <param name="dateDays">The date days to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateDateDays(IList<DateDay> dateDays)
        {
            return this.datesRepository.AddOrUpdateDateDays(dateDays);
        }

        /// <summary>
        /// retrieves the date days.
        /// </summary>
        /// <returns>A collection of date days.</returns>
        public IList<DateDay> GetDateDays()
        {
            return this.datesRepository.GetDateDays();
        }

        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        public IList<ViewDatesMaster> GetDateMasters()
        {
            return this.datesRepository.GetDateMasters();
        }

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        public IList<ViewGs1Master> GetGs1MasterView()
        {
            return this.labelRepository.GetGs1MasterView();
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletCardData(int stockTransactionId)
        {
            return this.labelRepository.GetPalletCardData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletCardDataMultiBatch(int stockTransactionId, decimal wgt, decimal qty,
            int batchNumberId)
        {
            return this.labelRepository.GetPalletCardDataMultiBatch(stockTransactionId, wgt, qty, batchNumberId);
        }

        /// <summary>
        /// Retrieve all the system date types.
        /// </summary>
        /// <returns>A collection of date types.</returns>
        public IList<NouDateType> GetDateTypes()
        {
            return this.datesRepository.GetDateTypes();
        }

        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        public IList<SearchDateType> GetSearchDateTypes()
        {
            return this.datesRepository.GetSearchDateTypes();
        }

        /// <summary>
        /// Adds or updates date masters.
        /// </summary>
        /// <param name="dateMasters">The date masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateDateMasters(IList<ViewDatesMaster> dateMasters)
        {
            return this.datesRepository.AddOrUpdateDateMasters(dateMasters);
        }

        /// <summary>
        /// Retrieve all the system date template names.
        /// </summary>
        /// <returns>A collection of date template names.</returns>
        public IList<DateTemplateName> GetDateTemplateNames()
        {
            return this.datesRepository.GetDateTemplateNames();
        }

        /// <summary>
        /// Adds or updates date names.
        /// </summary>
        /// <param name="dateNames">The date names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateDateTemplateNames(IList<DateTemplateName> dateNames)
        {
            return this.datesRepository.AddOrUpdateDateTemplateNames(dateNames);
        }

        /// <summary>
        /// Retrieve all the system date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        public IList<DateTemplateAllocation> GetDateTemplateAllocations()
        {
            return this.datesRepository.GetDateTemplateAllocations();
        }


        /// <summary>
        /// Retrieve all the view date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        public IList<ViewDateTemplateAllocation> GetViewDateTemplateAllocations()
        {
            return this.datesRepository.GetViewDateTemplateAllocations();
        }

        /// <summary>
        /// Adds a date master to a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddDateToTemplate(int dateMasterId, int templateId)
        {
            return this.datesRepository.AddDateToTemplate(dateMasterId, templateId);
        }

        /// <summary>
        /// Removes a date master from a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        public bool RemoveDateFromTemplate(int dateMasterId, int templateId)
        {
            return this.datesRepository.RemoveDateFromTemplate(dateMasterId, templateId);
        }

        /// <summary>
        /// Adds a date mastesr to a template.
        /// </summary>
        /// <param name="dateMasters">The collection of the date masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddDatesToTemplate(IList<ViewDatesMaster> dateMasters, int templateId)
        {
            return this.datesRepository.AddDatesToTemplate(dateMasters, templateId);
        }

        #endregion

        #region department bodies

        /// <summary>
        /// Find the historical quality assurance records for a herd number.
        /// </summary>
        /// <param name="herdNumber">The herd number.</param>
        /// <returns>A collection of quality assurance expiration records for the herd number.</returns>
        public IList<QualityAssuranceHistory> FindQualityAssuranceEntries(string herdNumber)
        {
            return this.departmentBodiesRepository.FindQualityAssuranceEntries(herdNumber);
        }

        /// <summary>
        /// Log the Bord Bia request in the database.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="requestDate">The date of the request.</param>
        /// <param name="userName">The username for which the request is sent.</param>
        /// <param name="fileName">The file name associated with the request.</param>
        public bool LogBordBiaRequest(string herdNo, string requestDate, string userName, string fileName)
        {
            return this.departmentBodiesRepository.LogBordBiaRequest(herdNo, requestDate, userName, fileName);
        }

        /// <summary>
        /// Log the Bord Bia response in the database.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="responseDate">The date on which the response was received.</param>
        /// <param name="fileName">The file name associated with the response.</param>
        /// <param name="herdOwner">The owner of the herd.</param>
        /// <param name="certDate">The date of expiry of the validation cert.</param>
        public bool LogBordBiaResponse(string herdNo, string responseDate, string fileName, string herdOwner,
            string certDate)
        {
            return this.departmentBodiesRepository.LogBordBiaResponse(herdNo, responseDate, fileName, herdOwner,
                certDate);
        }

        /// <summary>
        /// Add a new quality assurance history entry for a herd number.
        /// </summary>
        /// <param name="herdNumber">The herd number.</param>
        /// <param name="herdOwner">The herd owner.</param>
        /// <param name="certExpiryDate">The expiry date for the certificate.</param>
        /// <returns>The id of the new entry.</returns>
        public int AddQualityAssurance(string herdNumber, string herdOwner, DateTime certExpiryDate)
        {
            return this.departmentBodiesRepository.AddQualityAssurance(herdNumber, herdOwner, certExpiryDate);
        }

        #endregion

        #region device

        /// <summary>
        /// Sets the default module process.
        /// </summary>
        /// <param name="module">The module to set.</param>
        /// <param name="process">The default process.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool SetDefaultModuleProcess(ViewType module, string process, int deviceId)
        {
            var localModule = "IntakeMode";
            if (module == ViewType.IntoProduction)
            {
                localModule = "IntoProductionMode";
            }
            else if (module == ViewType.OutOfProduction)
            {
                localModule = "OutOfProductionMode";
            }
            else if (module == ViewType.ARDispatch)
            {
                localModule = "DispatchMode";
            }
            else if (module == ViewType.StockMovement)
            {
                localModule = "StockMovement";
            }

            return this.deviceRepository.SetDefaultModuleProcess(localModule, process, deviceId);
        }

        /// <summary>
        /// Sets the local device settings.
        /// </summary>
        public void SetDeviceSettings()
        {
            var settings = this.deviceRepository.GetDeviceSettings(NouvemGlobal.DeviceId.ToInt());
            this.Log.LogInfo(this.GetType(), string.Format("SetDeviceSettings(): Setting up settings for device id:{0}", NouvemGlobal.DeviceId.ToInt()));
            ApplicationSettings.ComboBoxSearchType = "Contains";
            ApplicationSettings.TouchscreenOutOfProductionGroupMenuWidth = 50;
            ApplicationSettings.YoungBullMaxAge = 24;
            ApplicationSettings.GraderRefreshTime = 20;
            ApplicationSettings.GraderLabelUOMAge = 29;
            ApplicationSettings.MinSequencerEartagLength = 5;
            ApplicationSettings.MinSequencerHerdNoLength = 6;
            ApplicationSettings.TouchscreenKeyboardHeight = 400;
            ApplicationSettings.TouchscreenKeyboardWidth = 800;
            ApplicationSettings.TouchscreenAlternateRowColour = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFF9F8FB"));

            foreach (var setting in settings)
            {
                #region settings

                if (setting.Name.Equals("LoginData"))
                {
                    ApplicationSettings.LoggedInUser = setting.Value1;
                    ApplicationSettings.LoggedInUserName = setting.Value2;
                    ApplicationSettings.LoggedInUserIdentifier = setting.Value3;
                    ApplicationSettings.UserPasswordMustBeUnique = setting.Value4.ToBool();
                    ApplicationSettings.DBPassword = setting.Value5;
                    continue;
                }

                if (setting.Name.Equals("TestMode"))
                {
                    ApplicationSettings.TestMode = setting.Value1.ToBool();
                    ApplicationSettings.TestModeIncrementTime = setting.Value2.ToDouble() <= 0 ? 2 : setting.Value2.ToDouble();
                    ApplicationSettings.TestModeMaxIncrement = setting.Value3.ToDecimal() <= 0 ? 2m : setting.Value3.ToDecimal();
                    continue;
                }

                if (setting.Name.Equals("SystemInformation"))
                {
                    ApplicationSettings.AlibiCertNo = string.IsNullOrEmpty(setting.Value1) ? "GB 15-97" : setting.Value1;
                    ApplicationSettings.AlibiVersionNo = string.IsNullOrEmpty(setting.Value2) ? "1.1" : setting.Value2;
                    ApplicationSettings.AlibiChecksum = string.IsNullOrEmpty(setting.Value3) ? "0E8B23A32F5DE14C076D86F2BD9ECC08" : setting.Value3;
                    ApplicationSettings.NAWISerialNo = setting.Value4;
                    continue;
                }

                if (setting.Name.Equals("PanelMode"))
                {
                    try
                    {
                        ApplicationSettings.OutOfProductionPanelMode = (OutOfProductionPanel)Enum.Parse(typeof(OutOfProductionPanel), setting.Value1);
                    }
                    catch (Exception)
                    {
                        ApplicationSettings.OutOfProductionPanelMode = OutOfProductionPanel.Standard;
                    }

                    try
                    {
                        ApplicationSettings.DispatchPanelMode = (DispatchPanel)Enum.Parse(typeof(DispatchPanel), setting.Value2);
                    }
                    catch (Exception)
                    {
                        ApplicationSettings.DispatchPanelMode = DispatchPanel.Standard;
                    }

                    ApplicationSettings.DisplayOldGrades = setting.Value3.ToBool();
                    ApplicationSettings.CurrentIndicator = setting.Value4.ToInt();
                    ApplicationSettings.StockMoveGrid = setting.Value5;

                    try
                    {
                        ApplicationSettings.GradeSystem = (GradeSystem)Enum.Parse(typeof(GradeSystem), setting.Value6);
                    }
                    catch (Exception)
                    {
                        ApplicationSettings.GradeSystem = GradeSystem.EUROP;
                    }

                    continue;
                }

                if (setting.Name.Equals("ComboBoxSearchType"))
                {
                    if (!string.IsNullOrEmpty(setting.Value1))
                    {
                        ApplicationSettings.ComboBoxSearchType = setting.Value1;
                    }
                  
                    continue;
                }

                if (setting.Name.Equals("CheckRoutesAtOrder"))
                {
                    ApplicationSettings.CheckRoutesAtOrder = setting.Value1.ToBool();
                    ApplicationSettings.UseDisplayPassword = setting.Value2.ToBool();
                    ApplicationSettings.CheckForExtraDispatchLines = setting.Value3.ToBool();
                    ApplicationSettings.ShowSpecsAtDispatch = setting.Value4.ToBool();
                    ApplicationSettings.SpecHeight = setting.Value5.ToDouble() <= 0 ? 700 : setting.Value5.ToDouble();
                    ApplicationSettings.SpecWidth = setting.Value6.ToDouble() <= 0 ? 700 : setting.Value6.ToDouble();
                    ApplicationSettings.ShowPartnerPopUpNoteAtDesktopModules = setting.Value7.ToBool();
                    ApplicationSettings.AlwaysManuallySelectBatchOnMix1 = setting.Value8.ToBool();
                    ApplicationSettings.TileDeliveryAddressLength =
                        setting.Value10.ToInt() == 0 ? 25 : setting.Value10.ToInt();
                    continue;
                }

                if (setting.Name.Equals("KeyboardSearch"))
                {
                    ApplicationSettings.KeyboardSearchHeight = setting.Value1.ToDouble();
                    ApplicationSettings.KeyboardSearchWidth = setting.Value2.ToDouble();
                    ApplicationSettings.KeyboardSearchTop = setting.Value3.ToDouble();
                    ApplicationSettings.KeyboardSearchLeft = setting.Value4.ToDouble();

                    var height = setting.Value5.ToDouble();
                    var width = setting.Value6.ToDouble();
                    if (height <= 0)
                    {
                        height = 400;
                    }

                    if (width <= 0)
                    {
                        width = 800;
                    }

                    ApplicationSettings.TouchscreenKeyboardHeight = height;
                    ApplicationSettings.TouchscreenKeyboardWidth = width;
                    continue;
                }

                if (setting.Name.Equals("MessageBoxCoordinates"))
                {
                    ApplicationSettings.MessageBoxTop = setting.Value1.ToDouble();
                    ApplicationSettings.MessageBoxLeft = setting.Value2.ToDouble();

                    if (ApplicationSettings.MessageBoxTop <= 0)
                    {
                        ApplicationSettings.MessageBoxTop = 50;
                    }

                    if (ApplicationSettings.MessageBoxLeft <= 0)
                    {
                        ApplicationSettings.MessageBoxLeft = 50;
                    }

                    var popUpNoteHeight = setting.Value3.ToDouble();
                    if (popUpNoteHeight <= 0)
                    {
                        ApplicationSettings.PopUpNoteHeight = 500;
                    }
                    else
                    {
                        ApplicationSettings.PopUpNoteHeight = popUpNoteHeight;
                    }


                    var popUpNoteWidth = setting.Value4.ToDouble();
                    if (popUpNoteWidth <= 0)
                    {
                        ApplicationSettings.PopUpNoteWidth = 900;
                    }
                    else
                    {
                        ApplicationSettings.PopUpNoteWidth = popUpNoteWidth;
                    }

                    var popUpNoteFontSize = setting.Value5.ToDouble();
                    if (popUpNoteFontSize <= 0)
                    {
                        ApplicationSettings.PopUpNoteFontSize = 28;
                    }
                    else
                    {
                        ApplicationSettings.PopUpNoteFontSize = popUpNoteFontSize;
                    }

                    var touchscreenHeight = setting.Value6.ToDouble();
                    if (touchscreenHeight <= 0)
                    {
                        ApplicationSettings.TouchscreenMessageBoxHeight = 300;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMessageBoxHeight = touchscreenHeight;
                    }

                    var touchscreenWidth = setting.Value7.ToDouble();
                    if (touchscreenWidth <= 0)
                    {
                        ApplicationSettings.TouchscreenMessageBoxWidth = 900;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMessageBoxWidth = touchscreenWidth;
                    }

                    var touchscreenFontSize = setting.Value8.ToDouble();
                    if (touchscreenFontSize <= 0)
                    {
                        ApplicationSettings.TouchscreenMessageBoxFontSize = 28;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMessageBoxFontSize = touchscreenFontSize;
                    }

                    ApplicationSettings.SystemMessageBoxHeight =
                        setting.Value9.ToDouble() <= 0 ? 75 : setting.Value9.ToDouble();

                    var localHeight = setting.Value10.ToDouble();
                    if (localHeight <= 0)
                    {
                        ApplicationSettings.MessageBoxHeight = 330;
                    }
                    else
                    {
                        ApplicationSettings.MessageBoxHeight = localHeight;
                    }

                    var localWidth = setting.Value11.ToDouble();
                    if (localWidth <= 0)
                    {
                        ApplicationSettings.MessageBoxWidth = 900;
                    }
                    else
                    {
                        ApplicationSettings.MessageBoxWidth = localWidth;
                    }

                    var localFontSize = setting.Value12.ToDouble();
                    if (localFontSize <= 0)
                    {
                        ApplicationSettings.MessageBoxFontSize = 28;
                    }
                    else
                    {
                        ApplicationSettings.MessageBoxFontSize = localFontSize;
                    }

                    var scannerHeight = setting.Value13.ToDouble();
                    if (scannerHeight <= 0)
                    {
                        ApplicationSettings.ScannerMessageBoxHeight = 180;
                    }
                    else
                    {
                        ApplicationSettings.ScannerMessageBoxHeight = scannerHeight;
                    }

                    var scannerWidth = setting.Value14.ToDouble();
                    if (scannerWidth <= 0)
                    {
                        ApplicationSettings.ScannerMessageBoxWidth = 230;
                    }
                    else
                    {
                        ApplicationSettings.ScannerMessageBoxWidth = scannerWidth;
                    }

                    var scannerFontSize = setting.Value15.ToDouble();
                    if (scannerFontSize <= 0)
                    {
                        ApplicationSettings.ScannerMessageBoxFontSize = 12;
                    }
                    else
                    {
                        ApplicationSettings.ScannerMessageBoxFontSize = scannerFontSize;
                    }

                    ApplicationSettings.AllowLeftMessageBox = setting.Value16.ToBool();

                    continue;
                }

                if (setting.Name.Equals("RestartApplication"))
                {
                    ApplicationSettings.RestartApplication = setting.Value1.ToBool();
                    ApplicationSettings.RestartApplicationHour = setting.Value2.ToInt();
                    ApplicationSettings.RestartApplicationMin = setting.Value3.ToInt();
                    ApplicationSettings.LogOutOnApplicationIdle = setting.Value4.ToBool();
                    var logOutTime = setting.Value5.ToInt();
                    if (logOutTime == 0)
                    {
                        logOutTime = 30;
                    }

                    ApplicationSettings.LogOutOnApplicationIdleTime = logOutTime;

                    ApplicationSettings.LogOutOnWorkflowIdle = setting.Value6.ToBool();
                    var logOutWorkTime = setting.Value7.ToInt();
                    if (logOutWorkTime == 0)
                    {
                        logOutWorkTime = 30;
                    }

                    ApplicationSettings.LogOutOnWorkflowTime = logOutWorkTime;

                    ApplicationSettings.DisableAuthorisationCheck = setting.Value9.ToBool();

                    continue;
                }

                if (setting.Name.Equals("ScanningExternalCarcass"))
                {
                    ApplicationSettings.ScanningExternalCarcass = setting.Value1.ToBool();
                    ApplicationSettings.MarkCompletedDispatchesAsActiveEdit = setting.Value2.ToBool();
                    ApplicationSettings.UseLegacyUseByAttribute = setting.Value3.ToBool();
                    ApplicationSettings.UseKeypadSidesAtDispatch = setting.Value4.ToBool();
                    ApplicationSettings.ScanningForBatchAtProductionOrders = setting.Value5.ToBool();
                    ApplicationSettings.UseKeypadSidesAtIntake = setting.Value6.ToBool();
                    ApplicationSettings.UseKeypadSidesAtIntoProduction = setting.Value7.ToBool();
                    ApplicationSettings.UseNewScanFunctionalityAtDispatch = setting.Value10.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DontAutoFilterProductionBatchSearch"))
                {
                    ApplicationSettings.DontAutoFilterProductionBatchSearch = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("UseRDPIdentifierForSettings"))
                {
                    ApplicationSettings.UseRDPIdentifierForSettings = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("CheckForExcessCarcassSplitWeighings"))
                {
                    ApplicationSettings.CheckForExcessCarcassSplitWeighings = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DefaultGroupTemplates"))
                {
                    ApplicationSettings.SetDefaultGroupTemplates = setting.Value1.ToBool();
                    ApplicationSettings.TraceabilityDefaultTemplateId = setting.Value2.ToInt();
                    ApplicationSettings.QualityDefaultTemplateId = setting.Value3.ToInt();
                    ApplicationSettings.DatesDefaultTemplateId = setting.Value4.ToInt();
                    ApplicationSettings.UseNeedleEcoTheme = setting.Value5.ToBool();
                    continue;
                }

                if (setting.Name.Equals("PricingItemByThePiece"))
                {
                    ApplicationSettings.PricingItemByThePiece = setting.Value1.ToBool();
                    ApplicationSettings.IntakeItemByThePiece = setting.Value2.ToBool();
                    ApplicationSettings.BoxQtyAlwaysOne = setting.Value3.ToBool();
                    ApplicationSettings.PriceTo5DecimalPlaces = setting.Value4.ToBool();
                    ApplicationSettings.RetrieveLivePriceOnRecentOrders = setting.Value5.ToBool();
                    continue;
                }

                //if (setting.Name.Equals("DisplayReportsOnTouchscreen"))
                //{
                //    ApplicationSettings.DisplayReportsOnTouchscreen = setting.Value1.ToBool();
                //    ApplicationSettings.ReportsOnTouchscreen = setting.Value2;
                //    ApplicationSettings.TouchscreenReportsCopiesToPrint = setting.Value3.ToInt() == 0
                //        ? 1
                //        : setting.Value3.ToInt();
                //    ApplicationSettings.PrintDispatchDocketOnCompletion = setting.Value4.ToBool();

                //    continue;
                //}

                if (setting.Name.Equals("IgnoreTrimGroupsForConcessions"))
                {
                    ApplicationSettings.IgnoreTrimGroupsForConcessions = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("CheckForLinkedDatabaseScannedDispatchSerial"))
                {
                    ApplicationSettings.CheckForLinkedDatabaseScannedDispatchSerial = setting.Value1.ToBool();
                    ApplicationSettings.LinkedDatabaseScannedDispatchSerialIdentity = setting.Value2.ToInt();
                    ApplicationSettings.LinkedDatabaseScannedDispatchSerialStart = setting.Value3.ToInt();
                    continue;
                }

                if (setting.Name.Equals("DisallowCustomerSelectionAtDispatch"))
                {
                    ApplicationSettings.DisallowCustomerSelectionAtDispatch = setting.Value1.ToBool();
                    ApplicationSettings.AllowCustomerChangeOnOrders = setting.Value2.ToBool();
                    ApplicationSettings.CheckAvailableStockOnSaleOrders = setting.Value3.ToBool();
                    ApplicationSettings.CheckForImportedLabelAtStockTake = setting.Value4.ToBool();
                    ApplicationSettings.DisableManualWeighingAtIntake = setting.Value5.ToInt();
                    continue;
                }

                if (setting.Name.Equals("DisallowTouchscreenModuleChange"))
                {
                    ApplicationSettings.DisallowTouchscreenModuleChange = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("RetainTouchscreenFilter"))
                {
                    ApplicationSettings.RetainTouchscreenOrdersFilter = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AutoCreateCustomerPriceList"))
                {
                    ApplicationSettings.AutoCreateCustomerPriceList = setting.Value1.ToBool();
                    ApplicationSettings.IgnoreCustomerWithNoPriceListCheck = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DispatchOrderNotFilledWarning"))
                {
                    ApplicationSettings.DispatchOrderNotFilledWarning = setting.Value1.ToBool();
                    ApplicationSettings.UnfilledDispatchCompletionAuthorisationCheck = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("RouteMustBeSetOnOrder"))
                {
                    ApplicationSettings.RouteMustBeSetOnOrder = setting.Value1.ToBool();
                    ApplicationSettings.DisableChequeReportAfterFirstPrint = setting.Value2.ToBool();
                    ApplicationSettings.InsuranceAge = setting.Value3.ToInt() == 0 ? 36 : setting.Value3.ToInt();
               
                    continue;
                }

                if (setting.Name.Equals("CustomerCurrency"))
                {
                    if (setting.Value1 != null)
                    {
                        ApplicationSettings.CustomerCurrency = setting.Value1.ToString();
                    }
                  
                    continue;
                }

                if (setting.Name.Equals("CheckForPartBoxesAtDispatch"))
                {
                    ApplicationSettings.CheckForPartBoxesAtDispatch = setting.Value1.ToBool();
                    ApplicationSettings.CheckForOutstandingOrders = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("CheckWeightBoundaries"))
                {
                    ApplicationSettings.CheckWeightBoundaryAtDispatch = setting.Value1.ToBool();
                    ApplicationSettings.CheckWeightBoundaryAtCarcassDispatch = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AutoCreateInvoiceOnOrderCompletion"))
                {
                    ApplicationSettings.AutoCreateInvoiceOnOrderCompletion = setting.Value1.ToBool();
                    ApplicationSettings.NouProrderTypeID = setting.Value2.ToNullableInt();
                    ApplicationSettings.AutoUpdateRecipePrice = setting.Value3.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ScrollBarVerticalOffset"))
                {
                    ApplicationSettings.TouchScreenSuppliersVerticalOffset = setting.Value1.ToDouble() <= 0 ? 50 : setting.Value1.ToDouble();
                    ApplicationSettings.TouchScreenOrdersVerticalOffset = setting.Value2.ToDouble() <= 0 ? 50 : setting.Value2.ToDouble();
                    ApplicationSettings.TouchScreenProductsVerticalOffset = setting.Value3.ToDouble() <= 0 ? 50 : setting.Value3.ToDouble();
                    ApplicationSettings.TouchScreenProductionProductsVerticalOffset = setting.Value4.ToDouble() <= 0 ? 50 : setting.Value4.ToDouble();
                    continue;
                }

                if (setting.Name.Equals("AutoWeigh"))
                {
                    ApplicationSettings.AutoWeightAtIntake = setting.Value1.ToBool();
                    ApplicationSettings.AutoWeighAtOutOfProduction = setting.Value2.ToBool();
                    ApplicationSettings.AutoWeightAtDispatch = setting.Value3.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ResetSerialAttributesManually"))
                {
                    ApplicationSettings.ResetSerialAttributesManually = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("GetLiveOrderStatus"))
                {
                    ApplicationSettings.GetLiveOrderStatusIntake = setting.Value1.ToBool();
                    ApplicationSettings.GetLiveOrderStatusDispatch = setting.Value2.ToBool();
                    ApplicationSettings.OnlyShowDispatchedWeightsAtScannerDispatch = setting.Value3.ToBool();
                    ApplicationSettings.ShowBoxCountAtScannerDispatch = setting.Value4.ToBool();
                    continue;
                }

                if (setting.Name.Equals("SSRSEmailSettings"))
                {
                    ApplicationSettings.SSRSEmailType = setting.Value1;
                    ApplicationSettings.SSRSEmailTo = setting.Value2;
                    ApplicationSettings.SSRSEmailBody = setting.Value3;
                    ApplicationSettings.SSRSEmailSubject = setting.Value4;
                    ApplicationSettings.SSRSEmailExportFormat = setting.Value5;
                    ApplicationSettings.SSRSZoom = setting.Value6.ToInt();
                    continue;
                }

                if (setting.Name.Equals("SearchTypes"))
                {
                    ApplicationSettings.LairageKillInformationSearchType = setting.Value1;
                    ApplicationSettings.BeefReportSearchType = setting.Value2;
                    ApplicationSettings.IdentigenSearchType = setting.Value3;
                    ApplicationSettings.KillDetailsSearchType = setting.Value4;
                    ApplicationSettings.RPAReportSearchType = setting.Value5;
                    ApplicationSettings.AnimalMovermentsSearchType = setting.Value6;
                    ApplicationSettings.PaymentsSearchType = setting.Value7;
                    ApplicationSettings.LairageIntakeSearchType = setting.Value8;
                    ApplicationSettings.LairageReportSearchType = setting.Value9;
                    ApplicationSettings.PaymentCreationSearchType = setting.Value10;
                    ApplicationSettings.AnimalPricingSearchType = setting.Value11;
                    ApplicationSettings.DispatchOrdersDateType = string.IsNullOrEmpty(setting.Value12)
                        ? Strings.DeliveryDate
                        : setting.Value12;
                    ApplicationSettings.DispatchOrdersDateSort = string.IsNullOrEmpty(setting.Value13)
                        ? Strings.Ascending
                        : setting.Value13;
                    continue;
                }

                if (setting.Name.Equals("DispatchMode"))
                {
                    try
                    {
                        ApplicationSettings.DispatchMode = (DispatchMode)Enum.Parse(typeof(DispatchMode), setting.Value1);
                    }
                    catch
                    {
                        ApplicationSettings.DispatchMode = DispatchMode.Dispatch;
                    }

                    ApplicationSettings.DefaultDispatchModes = setting.Value3;
                    ApplicationSettings.ChangePriceOnDispatchTouchscreen = setting.Value4.ToBool();
                    ApplicationSettings.ShowBatchDispatchTouchscreenLine = setting.Value5.ToBool();
                    ApplicationSettings.ShowSupplierDispatchTouchscreenLine = setting.Value6.ToBool();

                    continue;
                }

                if (setting.Name.Equals("IntakeMode"))
                {
                    try
                    {
                        ApplicationSettings.IntakeMode = (IntakeMode)Enum.Parse(typeof(IntakeMode), setting.Value1);
                    }
                    catch
                    {
                        ApplicationSettings.IntakeMode = IntakeMode.Intake;
                    }

                    ApplicationSettings.DefaultIntakeMode = ApplicationSettings.IntakeMode;
                    ApplicationSettings.DefaultIntakeModes = setting.Value3;

                    continue;
                }

                if (setting.Name.Equals("IntoProductionMode"))
                {
                    try
                    {
                        ApplicationSettings.IntoProductionMode = (IntoProductionMode)Enum.Parse(typeof(IntoProductionMode), setting.Value1);
                    }
                    catch
                    {
                        ApplicationSettings.IntoProductionMode = IntoProductionMode.IntoProductionStandard;
                    }

                    ApplicationSettings.IntoProductionSelectProductScanAndWeighSeparately = setting.Value2.ToBool();
                    ApplicationSettings.DefaultIntoProductionModes = setting.Value3;

                    try
                    {
                        ApplicationSettings.StockMovementMode = (StockMovementMode)Enum.Parse(typeof(StockMovementMode), setting.Value4);
                    }
                    catch
                    {
                        ApplicationSettings.StockMovementMode = StockMovementMode.LowRiskCook;
                    }

                    ApplicationSettings.DefaultStockMovementModes = setting.Value5;
                    ApplicationSettings.AutoTypicalWeightSetWait =
                        setting.Value5.ToInt() == 0 ? 700 : setting.Value5.ToInt();
                    ApplicationSettings.UseHistoricProductionBatchNo = setting.Value6.ToBool();
                    ApplicationSettings.RecipeNegativeBatchStockMessage = setting.Value7 ?? string.Empty;
                    ApplicationSettings.MinQtyForRecipeFinishedProductManualComplete = setting.Value8.ToInt();
                    ApplicationSettings.CarcassSplitReweighing = setting.Value9.ToBool();
                    ApplicationSettings.AutoCarcassSplitReweighingAtDispatch = setting.Value10.ToBool();
                    continue;
                }

                if (setting.Name.Equals("OutOfProductionMode"))
                {
                    try
                    {
                        ApplicationSettings.OutOfProductionMode = (OutOfProductionMode)Enum.Parse(typeof(OutOfProductionMode), setting.Value1);
                    }
                    catch
                    {
                        ApplicationSettings.OutOfProductionMode = OutOfProductionMode.OutOfProductionStandard;
                    }

                    ApplicationSettings.DefaultOutOfProductionModes = setting.Value3;

                    continue;
                }

                if (setting.Name.Equals("AllowAllProductsOutOfProduction"))
                {
                    ApplicationSettings.AllowAllProductsOutOfProduction = setting.Value1.ToBool();
                    ApplicationSettings.DisplayReportPrintMessageOnCopyTemplate = setting.Value2.ToBool();
                    ApplicationSettings.UseDeliveryDateForIntakeReportSearch = setting.Value3.ToBool();
                    continue;
                }

                if (setting.Name.Equals("Pricing"))
                {
                    ApplicationSettings.DefaultCurrency = string.IsNullOrEmpty(setting.Value1) ? "€" : setting.Value1;
                    ApplicationSettings.OnlyShowSupplierProductsAtIntake = setting.Value2.ToBool();
                    ApplicationSettings.ShowMessageBoxOnSaleOrderAdd = setting.Value3.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AccountsPackage"))
                {
                    try
                    {
                        ApplicationSettings.AccountsPackage = (AccountsPackage)Enum.Parse(typeof(AccountsPackage), setting.Value1);
                    }
                    catch
                    {
                        ApplicationSettings.AccountsPackage = AccountsPackage.Sage50;
                    }

                    ApplicationSettings.AccountsSalesPersonCode = !string.IsNullOrEmpty(setting.Value2) ? setting.Value2 : "01";
                    ApplicationSettings.AccountsDbPath = setting.Value3;
                    ApplicationSettings.AccountsDbUserName = setting.Value4;
                    ApplicationSettings.AccountsDbPassword = setting.Value5;
                    ApplicationSettings.PrePostInvoice = setting.Value6.ToBool();
                    ApplicationSettings.AccountsCustomer = setting.Value7 ?? string.Empty;
                    ApplicationSettings.AccountsFilePath = setting.Value8;
                    ApplicationSettings.AccountsDirectory = setting.Value9;
                    ApplicationSettings.UseLairagesForPurchaseAccounts = setting.Value10.ToBool();
                    ApplicationSettings.PostToPurchaseInvoice = setting.Value11.ToBool();
                    ApplicationSettings.UseDeliveryDateForPurchaseOrder = setting.Value12.ToBool();
                    ApplicationSettings.PartPostPurchaseOrder = setting.Value13.ToBool();
                    ApplicationSettings.UseSagePurchaseOrderNumbers = setting.Value14.ToBool();
                    ApplicationSettings.PriceInvoiceDocket = setting.Value15.ToBool();
                    ApplicationSettings.PricePurchaseInvoiceDocket = setting.Value16.ToBool();
                    ApplicationSettings.UseProposedKillDateForPurchaseInvoice = setting.Value17.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AutoCopyToInvoice"))
                {
                    ApplicationSettings.AutoCopyToInvoice = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("UseDeliveryDateForInvoiceDocumentDate"))
                {
                    ApplicationSettings.UseDeliveryDateForInvoiceDocumentDate = setting.Value1.ToBool();
                    ApplicationSettings.ShippingDateMustBeAfterDeliveryDate = setting.Value2.ToBool();
                    ApplicationSettings.DefaultShippingDateToPreviousDay = setting.Value3.ToBool();
                    ApplicationSettings.DeliveryDateCannotBeInThePast = setting.Value4.ToBool();
                    ApplicationSettings.ShowBoxingAtDispatch = setting.Value5.ToBool();
                    continue;
                }

                if (setting.Name.Equals("Bord Bia"))
                {
                    ApplicationSettings.BordBiaUserName = setting.Value1;
                    ApplicationSettings.BordBiaPassword = setting.Value2;
                    ApplicationSettings.BordBiaResponsePath = setting.Value3;
                    ApplicationSettings.TrackingGroupGuid = setting.Value5;
                    continue;
                }

                if (setting.Name.Equals("Aims"))
                {
                    ApplicationSettings.AimsPassword = setting.Value1;
                    ApplicationSettings.AimsCertPath = setting.Value2;
                    ApplicationSettings.AimsLogPath = setting.Value3;
                    ApplicationSettings.AimsURL = setting.Value4;
                    ApplicationSettings.AimSenderId = setting.Value5;
                    ApplicationSettings.RequiredDaysOnCurrentFarm = setting.Value6.ToNullableInt();
                    ApplicationSettings.AfaisUserName = setting.Value7;
                    ApplicationSettings.AfaisPassword = setting.Value8;
                    ApplicationSettings.AfaisBusinessId = setting.Value9;
                    ApplicationSettings.AfaisEnvironment = setting.Value10;
                    ApplicationSettings.AfaisPinNo = setting.Value11.ToInt();
                    ApplicationSettings.AfaisAbbatoirCode = setting.Value12;
                    ApplicationSettings.AimsSheepLogPath = setting.Value13;
                    continue;
                }

                if (setting.Name.Equals("TouchscreenStaticButtonFontSize"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenStaticButtonFontSize = 26;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenStaticButtonFontSize = setting.Value1.ToDouble();
                    }

                    if (!string.IsNullOrWhiteSpace(setting.Value2))
                    {
                        try
                        {
                            ApplicationSettings.TouchscreenAlternateRowColour = (SolidColorBrush)(new BrushConverter().ConvertFrom(setting.Value2));
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }

                    ApplicationSettings.TouchscreenQtyMask = string.IsNullOrEmpty(setting.Value3)
                        ? "N0"
                        : $"N{setting.Value3}";

                    ApplicationSettings.TouchscreenQtyMaskNo =
                        string.IsNullOrEmpty(setting.Value3) ? 0 : setting.Value3.ToInt();

                    ApplicationSettings.TouchscreenWgtMask = string.IsNullOrEmpty(setting.Value4)
                        ? "N2"
                        : $"N{setting.Value4}";

                    ApplicationSettings.TouchscreenWgtMaskNo =
                        string.IsNullOrEmpty(setting.Value4) ? 2 : setting.Value4.ToInt();

                    ApplicationSettings.TouchscreenPricingMask = string.IsNullOrEmpty(setting.Value5)
                        ? "N2"
                        : $"N{setting.Value5}";

                    ApplicationSettings.TouchscreenPricingMaskNo =
                        string.IsNullOrEmpty(setting.Value5) ? 2 : setting.Value5.ToInt();

                    continue;
                }

                if (setting.Name.Equals("MessageBoxFlashTime"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.MessageBoxFlashTime = 2;
                    }
                    else
                    {
                        ApplicationSettings.MessageBoxFlashTime = setting.Value1.ToDouble();
                    }

                    ApplicationSettings.CheckForAlerts = setting.Value2.ToBool();
                    ApplicationSettings.CheckForAlertsTime = setting.Value3.ToInt();

                    continue;
                }

                if (setting.Name.Equals("WaitForScalesToStable"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.WaitForScalesToStable = 400;
                    }
                    else
                    {
                        ApplicationSettings.WaitForScalesToStable = setting.Value1.ToInt();
                    }

                    continue;
                }

                if (setting.Name.Equals("OutOfProductionProductsHomeMenuWidth"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.OutOfProductionProductsHomeMenuWidth = 150;
                    }
                    else
                    {
                        ApplicationSettings.OutOfProductionProductsHomeMenuWidth = setting.Value1.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenMenuButtonWidth"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMenuButtonWidth = 100;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMenuButtonWidth = setting.Value1.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenMenuButtonHeight"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMenuButtonHeight = 70;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMenuButtonHeight = setting.Value1.ToDouble();
                    }

                    if (string.IsNullOrEmpty(setting.Value2) || setting.Value2.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMessageBarHeight = 40;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMessageBarHeight = setting.Value2.ToDouble();
                    }

                    if (string.IsNullOrEmpty(setting.Value3) || setting.Value3.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMessageBarFontSize = 14;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMessageBarFontSize = setting.Value3.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenMenuButtonFontSize"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMenuButtonFontSize = 12;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMenuButtonFontSize = setting.Value1.ToInt();
                    }

                    if (string.IsNullOrEmpty(setting.Value2) || setting.Value2.Equals("0"))
                    {
                        ApplicationSettings.DispatchBoxCountFontSize = 250;
                    }
                    else
                    {
                        ApplicationSettings.DispatchBoxCountFontSize = setting.Value2.ToInt();
                    }

                    if (string.IsNullOrEmpty(setting.Value3) || setting.Value3.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMenuButtonFontSizeLogIn = 22;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMenuButtonFontSizeLogIn = setting.Value3.ToInt();
                    }

                    if (string.IsNullOrEmpty(setting.Value4) || setting.Value4.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenLogInWidth= 180;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenLogInWidth= setting.Value4.ToInt();
                    }

                    if (string.IsNullOrEmpty(setting.Value5) || setting.Value5.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenLogInHeight = 120;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenLogInHeight = setting.Value5.ToInt();
                    }

                    if (string.IsNullOrEmpty(setting.Value6) || setting.Value6.Equals("0"))
                    {
                        ApplicationSettings.DispatchBoxCountHandheldFontSize = 130;
                    }
                    else
                    {
                        ApplicationSettings.DispatchBoxCountHandheldFontSize = setting.Value6.ToInt();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenTraceabilityControlsWidth"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsWidth = 140;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsWidth = setting.Value1.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenTraceabilityControlsLabelWidth"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth = 140;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth = setting.Value1.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenCardsFontSize"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenCardsFontSize = 12;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenCardsFontSize = setting.Value1.ToDouble();
                    }

                    ApplicationSettings.DisplayNewAlertsMessageUntilAlertOpened = setting.Value2.ToBool();
                    ApplicationSettings.WorkflowProceedOnSelection = setting.Value3.ToBool();
                    ApplicationSettings.WorkflowDisableBackArrow = setting.Value4.ToBool();
                    ApplicationSettings.DisallowStandardAttributeChange = setting.Value5.ToBool();
                    ApplicationSettings.WorkflowDisableForwardArrow = setting.Value6.ToBool();
                    ApplicationSettings.PreviousWorkflowTimeCheck = setting.Value7.ToInt();
                    ApplicationSettings.SearchIntakeByReference = setting.Value8.ToBool();

                    continue;
                }

                if (setting.Name.Equals("TouchscreenTraceabilityControlsHeight"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsHeight = 40;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsHeight = setting.Value1.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("TouchscreenTraceabilityControlsFontSize"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsFontSize = 20;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsFontSize = setting.Value1.ToDouble();
                    }

                    continue;
                }

                if (setting.Name.Equals("PriceListDefaults"))
                {
                    ApplicationSettings.PriceListDefaultRounding = setting.Value1.ToInt();
                    ApplicationSettings.PriceListDefaultRoundingOption = setting.Value2.ToInt();
                    ApplicationSettings.PriceListDefaultCurrency = setting.Value3.ToInt();
                    continue;
                }

                if (setting.Name.Equals("Modules"))
                {
                    ApplicationSettings.ModuleEnableKillLine = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("Customer"))
                {
                    ApplicationSettings.Customer = setting.Value1;
                    ApplicationSettings.PlantCode = setting.Value2;
                    ApplicationSettings.DefaultAnimalCountry = setting.Value3;
                    ApplicationSettings.ScotBeefSupplierID = setting.Value4.ToInt();
                    ApplicationSettings.ScotBeefEnforceIdentigen = setting.Value5.ToBool();
                    ApplicationSettings.GenerateDispatchFile = setting.Value6.ToBool();
                    ApplicationSettings.DispatchFilePath = setting.Value7;
                    ApplicationSettings.LabelImagePath = setting.Value8;
                    ApplicationSettings.ExportReportPath = string.IsNullOrEmpty(setting.Value9) ? @"C:\Nouvem\Report" : setting.Value9;
                    ApplicationSettings.DontCreateCSVForPalletCard = setting.Value10.ToBool();
                    ApplicationSettings.KeypakReport = setting.Value11;
                    ApplicationSettings.FTracePath = string.IsNullOrEmpty(setting.Value12) ? @"C:\Nouvem\Transfer" : setting.Value12;
                    ApplicationSettings.FTraceURL = string.IsNullOrEmpty(setting.Value13) ? @"https://access-onb.ftrace.com/ftrace.epcis.capture/CaptureServiceProxyServlet" : setting.Value13;
                    ApplicationSettings.FTraceUserName = setting.Value14;
                    ApplicationSettings.FTracePassword = setting.Value15;
                    try
                    {
                        if (string.IsNullOrEmpty(setting.Value16))
                        {
                            ApplicationSettings.LairageType = LairageType.Ireland;
                        }
                        else
                        {
                            ApplicationSettings.LairageType = (LairageType)Enum.Parse(typeof(LairageType), setting.Value16);
                        }
                    }
                    catch 
                    {
                        ApplicationSettings.LairageType = LairageType.Ireland;
                    }

                    ApplicationSettings.BordBiaQAResidency =
                        setting.Value17.ToInt() == 0 ? 60 : setting.Value17.ToInt();

                    continue;
                }

                if (setting.Name.Equals("OrderAmountToTake"))
                {
                    ApplicationSettings.OrderAmountToTake = setting.Value1.ToInt() == 0 ? 100 : setting.Value1.ToInt();
                    ApplicationSettings.RecentOrdersAmtToTake = setting.Value2.ToInt() == 0 ? 20 : setting.Value2.ToInt();
                    ApplicationSettings.RecentOrdersAtDispatchByDelivered = setting.Value3.ToBool();
                    ApplicationSettings.DefaultToFindAtDesktopDispatch= setting.Value4.ToBool();
                    ApplicationSettings.ShowPOAtDispatch = setting.Value5.ToBool();
                    continue;
                }

                if (setting.Name.Equals("UseDefaultProductAtOutOfProduction"))
                {
                    ApplicationSettings.UseDefaultProductAtOutOfProduction = setting.Value1.ToBool();
                    ApplicationSettings.DefaultProductAtOutOfProductionID = setting.Value2.ToInt();
                    continue;
                }

                if (setting.Name.Equals("TouchscreenOutOfProductionGroupMenuWidth"))
                {
                    ApplicationSettings.TouchscreenOutOfProductionGroupMenuWidth = setting.Value1.ToInt() == 0 ? 50 : setting.Value1.ToInt();
                    continue;
                }

                if (setting.Name.Equals("TouchscreenTraceabilityControlsStackedNumber"))
                {
                    if (string.IsNullOrEmpty(setting.Value1) || setting.Value1.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber = 2;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber = setting.Value1.ToInt();
                    }

                    continue;
                }

                if (setting.Name.Equals("SalesSearchScreenData"))
                {
                    ApplicationSettings.SalesSearchFromDate = DateTime.Today;
                    ApplicationSettings.SalesSearchToDate = DateTime.Today;
                    ApplicationSettings.SalesSearchShowAllOrders = setting.Value3.ToBool();
                    ApplicationSettings.SalesSearchKeepVisible = setting.Value4.ToBool();

                    ApplicationSettings.AnimalsSearchFromDate = DateTime.Today;
                    ApplicationSettings.AnimalsSearchToDate = DateTime.Today;
                    ApplicationSettings.AnimalsSearchShowAllOrders = setting.Value7.ToBool();
                    ApplicationSettings.AnimalsSearchShowKeepVisible = setting.Value8.ToBool();

                    ApplicationSettings.LairageSearchFromDate = DateTime.Today;
                    ApplicationSettings.LairageSearchToDate = DateTime.Today;
                    ApplicationSettings.LairageSearchShowAllOrders = setting.Value11.ToBool();
                    ApplicationSettings.LairageSearchShowKeepVisible = setting.Value12.ToBool();
                    ApplicationSettings.LairageSearchShowFAOnly= setting.Value13.ToBool();

                    ApplicationSettings.FromPurchasesDate = DateTime.Today;
                    ApplicationSettings.FromPurchasesShowAllOrders = setting.Value15.ToBool();
                    ApplicationSettings.SalesSearchDateDispatch = string.IsNullOrEmpty(setting.Value16) ? Strings.DocumentDate : setting.Value16;
                    ApplicationSettings.SalesSearchDateSaleOrder= string.IsNullOrEmpty(setting.Value18) ? Strings.DocumentDate : setting.Value18;
                    ApplicationSettings.SalesSearchDateInvoice = string.IsNullOrEmpty(setting.Value19) ? Strings.DocumentDate : setting.Value19;
                    ApplicationSettings.ShowAllStock = setting.Value20.ToBool();
                    continue;
                }

                if (setting.Name.Equals("SalesSearchScreenData2"))
                {
                    ApplicationSettings.PaymentProposalFromDate = DateTime.Today;
                    ApplicationSettings.PaymentProposalShowAllOrders = setting.Value2.ToBool();
                    ApplicationSettings.PaymentProposalKeepVisible = setting.Value3.ToBool();
                    ApplicationSettings.PaymentProposalCreateFromDate = DateTime.Today;
                    ApplicationSettings.PaymentProposalCreateShowAllOrders = setting.Value5.ToBool();

                    ApplicationSettings.AlertsFromDate = DateTime.Today;
                    ApplicationSettings.AlertsShowAllOrders = setting.Value7.ToBool();
                    ApplicationSettings.AlertsKeepVisible = setting.Value8.ToBool();


                    ApplicationSettings.WorkflowSearchShowAllOrders = setting.Value9.ToBool();
                    ApplicationSettings.WorkflowSearchFromDate = DateTime.Today;

                    ApplicationSettings.ShowAlertsForAllUsers = setting.Value11.ToBool();
                    ApplicationSettings.BatchEditSearchFromDate = setting.Value12.ToDate();
                    ApplicationSettings.BatchEditSearchToDate = DateTime.Today;
                    ApplicationSettings.BatchEditSearchShowAllOrders = setting.Value14.ToBool();
                    ApplicationSettings.ShowUnallocatedCustomersOnly = setting.Value15.ToBool();
                    ApplicationSettings.BatchSetupShowAllOrders = setting.Value16.ToBool();
                    ApplicationSettings.BatchSetUpFromDate = DateTime.Today;
                    ApplicationSettings.FromCreditReturnsShowAllOrders = setting.Value18.ToBool();

                    continue;
                }

                if (setting.Name.Equals("ReportSearchScreenData"))
                {
                    ApplicationSettings.ReportsSearchFromDate = DateTime.Today;
                    ApplicationSettings.ReportsSearchToDate = DateTime.Today;
                    ApplicationSettings.ReportSalesSearchShowAllOrders = setting.Value3.ToBool();
                    ApplicationSettings.ReportSalesSearchDataKeepVisible = setting.Value4.ToBool();
                    continue;
                }

                if (setting.Name.Equals("PrinterSettings"))
                {
                    ApplicationSettings.PrinterCommunicationType = setting.Value1;
                    ApplicationSettings.PrinterSettings = setting.Value2;
                    ApplicationSettings.PrinterLabelOrientation = setting.Value3;
                    ApplicationSettings.IsPrinterInUse = setting.Value4.ToBool();
                    ApplicationSettings.UseDefaultPrinter = setting.Value5.ToBool();
                    ApplicationSettings.PrinterDPI = string.IsNullOrWhiteSpace(setting.Value6) ? 203 : setting.Value6.ToDouble();
                    ApplicationSettings.UseExternalLabels = setting.Value7.ToBool();
                  
                    if (string.IsNullOrEmpty(setting.Value8))
                    {
                        ApplicationSettings.PrinterLanguage = ProgrammingLanguage.ZPL;
                    }
                    else
                    {
                        try
                        {
                            ApplicationSettings.PrinterLanguage = (ProgrammingLanguage) Enum.Parse(typeof(ProgrammingLanguage), setting.Value8.ToUpper());
                        }
                        catch 
                        {
                            ApplicationSettings.PrinterLanguage = ProgrammingLanguage.ZPL;
                        }
                    }

                    continue;
                }

                if (setting.Name.Equals("Printer2Settings"))
                {
                    ApplicationSettings.Printer2CommunicationType = setting.Value1;
                    ApplicationSettings.Printer2Settings = setting.Value2;
                    //ApplicationSettings.PrinterLabelOrientation = setting.Value3;
                    ApplicationSettings.IsPrinter2InUse = setting.Value4.ToBool();
                    //ApplicationSettings.UseDefaultPrinter = setting.Value5.ToBool();
                    ApplicationSettings.Printer2DPI = string.IsNullOrWhiteSpace(setting.Value6) ? 203 : setting.Value6.ToDouble();
                    ApplicationSettings.UseExternalLabels2 = setting.Value7.ToBool();
                    if (string.IsNullOrEmpty(setting.Value8))
                    {
                        ApplicationSettings.Printer2Language = ProgrammingLanguage.ZPL;
                    }
                    else
                    {
                        try
                        {
                            ApplicationSettings.Printer2Language = (ProgrammingLanguage)Enum.Parse(typeof(ProgrammingLanguage), setting.Value8.ToUpper());
                        }
                        catch
                        {
                            ApplicationSettings.Printer2Language = ProgrammingLanguage.ZPL;
                        }
                    }

                    continue;
                }

                if (setting.Name.Equals("Printer3Settings"))
                {
                    ApplicationSettings.Printer3CommunicationType = setting.Value1;
                    ApplicationSettings.Printer3Settings = setting.Value2;
                    //ApplicationSettings.PrinterLabelOrientation = setting.Value3;
                    ApplicationSettings.IsPrinter3InUse = setting.Value4.ToBool();
                    //ApplicationSettings.UseDefaultPrinter = setting.Value5.ToBool();
                    ApplicationSettings.Printer3DPI = string.IsNullOrWhiteSpace(setting.Value6) ? 203 : setting.Value6.ToDouble();
                    ApplicationSettings.UseExternalLabels3 = setting.Value7.ToBool();
                    if (string.IsNullOrEmpty(setting.Value8))
                    {
                        ApplicationSettings.Printer3Language = ProgrammingLanguage.ZPL;
                    }
                    else
                    {
                        try
                        {
                            ApplicationSettings.Printer3Language = (ProgrammingLanguage)Enum.Parse(typeof(ProgrammingLanguage), setting.Value8.ToUpper());
                    }
                        catch
                        {
                            ApplicationSettings.Printer3Language = ProgrammingLanguage.ZPL;
                        }
                    }

                    continue;
                }

                if (setting.Name.Equals("IndicatorSettings"))
                {
                    ApplicationSettings.IndicatorCommunicationType = setting.Value1;
                    ApplicationSettings.IndicatorSettings = setting.Value2;
                    ApplicationSettings.IndicatorModel = setting.Value3;
                    ApplicationSettings.IndicatorAlibiPath = setting.Value4;
                    ApplicationSettings.ConnectToIndicator = setting.Value5.ToBool();

                    if (string.IsNullOrEmpty(setting.Value6) || setting.Value6.Equals("0"))
                    {
                        ApplicationSettings.IndicatorFontSize = 52;
                    }
                    else
                    {
                        ApplicationSettings.IndicatorFontSize = setting.Value6.ToDouble();
                    }

                    if (string.IsNullOrEmpty(setting.Value7) || setting.Value7.Equals("0"))
                    {
                        ApplicationSettings.IndicatorFontSizeSmall = 18;
                    }
                    else
                    {
                        ApplicationSettings.IndicatorFontSizeSmall = setting.Value7.ToDouble();
                    }

                    ApplicationSettings.PollScalesString = setting.Value8;
                    ApplicationSettings.ToleranceWeight = setting.Value9.ToDecimal();
                    ApplicationSettings.IndicatorStableReads = setting.Value10.ToInt();
                    ApplicationSettings.IndicatorWaitTime = setting.Value11.ToInt();
                    ApplicationSettings.IndicatorStringVariables = setting.Value12;
                    ApplicationSettings.UseStableReadsForMotion = setting.Value13.ToBool();
                    ApplicationSettings.IgnoreNoMovementmentOnScalesWarning = setting.Value14.ToBool();
                    ApplicationSettings.DisturbBy = setting.Value15.ToDecimal();
                    ApplicationSettings.ZeroScalesCommand = setting.Value16;
                    ApplicationSettings.DropBelow = setting.Value17.ToNullableDecimal();
                    ApplicationSettings.ScalesDivision = setting.Value18.ToDecimal() == 0 ? 0.01m : setting.Value18.ToDecimal();
                    ApplicationSettings.MinWeightInDivisions = setting.Value19.ToInt();
                    ApplicationSettings.PollScalesForStringInterval = setting.Value20.ToNullableInt();
                    continue;
                }

                if (setting.Name.Equals("IndicatorSettings2"))
                {
                    ApplicationSettings.Indicator2CommunicationType = setting.Value1;
                    ApplicationSettings.Indicator2Settings = setting.Value2;
                    ApplicationSettings.Indicator2Model = setting.Value3;
                    ApplicationSettings.IndicatorAlibiPath2 = setting.Value4;
                    ApplicationSettings.ConnectToIndicator2 = setting.Value5.ToBool();

                    if (string.IsNullOrEmpty(setting.Value6) || setting.Value6.Equals("0"))
                    {
                        ApplicationSettings.IndicatorFontSize2 = 52;
                    }
                    else
                    {
                        ApplicationSettings.IndicatorFontSize2 = setting.Value6.ToDouble();
                    }

                    if (string.IsNullOrEmpty(setting.Value7) || setting.Value7.Equals("0"))
                    {
                        ApplicationSettings.IndicatorFontSizeSmall2 = 18;
                    }
                    else
                    {
                        ApplicationSettings.IndicatorFontSizeSmall2 = setting.Value7.ToDouble();
                    }

                    ApplicationSettings.PollScalesString2 = setting.Value8;
                    ApplicationSettings.ToleranceWeight2 = setting.Value9.ToDecimal();
                    ApplicationSettings.IndicatorStableReads2 = setting.Value10.ToInt();
                    ApplicationSettings.IndicatorWaitTime2 = setting.Value11.ToInt();
                    ApplicationSettings.IndicatorStringVariables2 = setting.Value12;
                    ApplicationSettings.UseStableReadsForMotion2 = setting.Value13.ToBool();
                    ApplicationSettings.IgnoreNoMovementmentOnScalesWarning2 = setting.Value14.ToBool();
                    ApplicationSettings.DisturbBy2 = setting.Value15.ToDecimal();
                    ApplicationSettings.ZeroScalesCommand2 = setting.Value16;
                    ApplicationSettings.DropBelow2 = setting.Value17.ToNullableDecimal();
                    ApplicationSettings.ScalesDivision2 = setting.Value18.ToDecimal() == 0 ? 0.01m : setting.Value18.ToDecimal();
                    ApplicationSettings.MinWeightInDivisions2 = setting.Value19.ToInt();
                    ApplicationSettings.PollScalesForStringInterval2 = setting.Value20.ToNullableInt();

                    continue;
                }

                if (setting.Name.Equals("IndicatorSettings3"))
                {
                    ApplicationSettings.Indicator3CommunicationType = setting.Value1;
                    ApplicationSettings.Indicator3Settings = setting.Value2;
                    ApplicationSettings.Indicator3Model = setting.Value3;
                    ApplicationSettings.IndicatorAlibiPath3 = setting.Value4;
                    ApplicationSettings.ConnectToIndicator3 = setting.Value5.ToBool();

                    if (string.IsNullOrEmpty(setting.Value6) || setting.Value6.Equals("0"))
                    {
                        ApplicationSettings.IndicatorFontSize3 = 52;
                    }
                    else
                    {
                        ApplicationSettings.IndicatorFontSize3 = setting.Value6.ToDouble();
                    }

                    if (string.IsNullOrEmpty(setting.Value7) || setting.Value7.Equals("0"))
                    {
                        ApplicationSettings.IndicatorFontSizeSmall3 = 18;
                    }
                    else
                    {
                        ApplicationSettings.IndicatorFontSizeSmall3 = setting.Value7.ToDouble();
                    }

                    ApplicationSettings.PollScalesString3 = setting.Value8;
                    ApplicationSettings.ToleranceWeight3 = setting.Value9.ToDecimal();
                    ApplicationSettings.IndicatorStableReads3 = setting.Value10.ToInt();
                    ApplicationSettings.IndicatorWaitTime3 = setting.Value11.ToInt();
                    ApplicationSettings.IndicatorStringVariables3 = setting.Value12;
                    ApplicationSettings.UseStableReadsForMotion3 = setting.Value13.ToBool();
                    ApplicationSettings.IgnoreNoMovementmentOnScalesWarning3 = setting.Value14.ToBool();
                    ApplicationSettings.DisturbBy3 = setting.Value15.ToDecimal();
                    ApplicationSettings.ZeroScalesCommand3 = setting.Value16;
                    ApplicationSettings.DropBelow3 = setting.Value17.ToNullableDecimal();
                    ApplicationSettings.ScalesDivision3 = setting.Value18.ToDecimal() == 0 ? 0.01m : setting.Value18.ToDecimal();
                    ApplicationSettings.MinWeightInDivisions3 = setting.Value19.ToInt();
                    ApplicationSettings.PollScalesForStringInterval3 = setting.Value20.ToNullableInt();

                    continue;
                }

                if (setting.Name.Equals("ShowAttributes"))
                {
                    ApplicationSettings.ShowIntakeAttributes = setting.Value1.ToBool();
                    ApplicationSettings.ShowIntoProductionAttributes = setting.Value2.ToBool();
                    ApplicationSettings.ShowOutOfProductionAttributes = setting.Value3.ToBool();
                    ApplicationSettings.ShowDispatchAttributes = setting.Value4.ToBool();
                    ApplicationSettings.ShowSequencerAttributes = setting.Value5.ToBool();
                    ApplicationSettings.ShowGraderAttributes = setting.Value6.ToBool();
                    ApplicationSettings.AlwaysShowAttributes = setting.Value7.ToBool();
                    ApplicationSettings.CheckForUnsavedBatchAttributes = setting.Value8.ToBool();
                    ApplicationSettings.ShowLairageAttributes = setting.Value9.ToBool();
                    ApplicationSettings.AttributesExpanderHeight = setting.Value10.ToDouble() <= 0 ? 30 : setting.Value10.ToDouble();
                    ApplicationSettings.ResetQtyToZeroAtIntake = setting.Value11.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ScannerMode"))
                {
                    ApplicationSettings.ScannerMode = setting.Value1.ToBool();
                    ApplicationSettings.RetrieveAttributeOnDispatchScan = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("KillLine"))
                {
                    ApplicationSettings.YoungBullMaxAge = setting.Value1.ToInt();
                    ApplicationSettings.GraderLabelUOMAge = setting.Value2.ToInt();
                    ApplicationSettings.MinSequencerEartagLength = setting.Value3.ToInt();
                    ApplicationSettings.MinSequencerHerdNoLength = setting.Value4.ToInt();
                    ApplicationSettings.CarcassSideWeightDifference = setting.Value5.ToInt();
                    ApplicationSettings.ProductIdYoungBull = setting.Value6.ToInt();
                    ApplicationSettings.ProductIdBull = setting.Value7.ToInt();
                    ApplicationSettings.ProductIdSteer = setting.Value8.ToInt();
                    ApplicationSettings.ProductIdCow = setting.Value9.ToInt();
                    ApplicationSettings.ProductIdHeifer = setting.Value10.ToInt();
                    ApplicationSettings.GraderRefreshTime = setting.Value11.ToInt();
                    ApplicationSettings.AllowGradeViewChange = setting.Value12.ToBool();
                    ApplicationSettings.IdentigenUserName = setting.Value13;
                    ApplicationSettings.IdentigenPassword = setting.Value14;
                    ApplicationSettings.IdentigenLocation = setting.Value15;
                    ApplicationSettings.WaitForMotionAtGrader = setting.Value16.ToBool();
                    ApplicationSettings.LairageGraderCombo = setting.Value17.ToBool();
                    ApplicationSettings.AccumulateWeightToTareAtGrader = setting.Value18.ToBool();
                    ApplicationSettings.LairageRadioButtonOptionsPartnerId4 = setting.Value19.ToNullableInt();
                    ApplicationSettings.ContinentalDestination = setting.Value20;
                    continue;
                }

                if (setting.Name.Equals("KillLine2"))
                {
                    ApplicationSettings.UseCustomerForGraderLabel = setting.Value1.ToBool();
                    ApplicationSettings.DeptOfAgricultureImportFarm = setting.Value2;
                    ApplicationSettings.DeptOfAgricultureImportCode = setting.Value3;
                    ApplicationSettings.AIMMovementCountryCodes = string.IsNullOrWhiteSpace(setting.Value4) ? "IE" : setting.Value4;
                    ApplicationSettings.AllowPrinterSelectionAtGraderReprint = setting.Value5.ToBool();
                    ApplicationSettings.PrintingTrailLabelAtGrader = setting.Value6.ToBool();
                    ApplicationSettings.ProductIdEwe = setting.Value7.ToInt();
                    ApplicationSettings.ProductIdLamb = setting.Value8.ToInt();
                    ApplicationSettings.ProductIdYSpringLamb = setting.Value9.ToInt();
                    ApplicationSettings.ProductIdHogget = setting.Value10.ToInt();
                    ApplicationSettings.ProductIdRam = setting.Value11.ToInt();
                    ApplicationSettings.ProductIdGoat = setting.Value12.ToInt();
                    //ApplicationSettings.ResetCategoryAtSequencer = setting.Value13.ToBool();
                    ApplicationSettings.ProductIdVealYoung = setting.Value13.ToInt();
                    ApplicationSettings.ProductIdVealOld = setting.Value14.ToInt();
                    ApplicationSettings.SendPrinterCommandOnStartUp = setting.Value15.ToBool();
                    ApplicationSettings.PrinterCommand = setting.Value16;
                    ApplicationSettings.GradeVealAsWholeAnimal = setting.Value17.ToBool();
                    ApplicationSettings.DeptAnimalPricingPath = setting.Value18;
                    ApplicationSettings.DeptFileFormat = setting.Value19;
                    ApplicationSettings.DeptFactoryCode = setting.Value20;
                    continue;
                }

                if (setting.Name.Equals("KillLine3"))
                {
                    ApplicationSettings.ResetCategoryAtSequencer = setting.Value1.ToBool();
                    ApplicationSettings.ManuallySequenceSheep = setting.Value2.ToBool();
                    ApplicationSettings.AllowSequencerViewChange = setting.Value3.ToBool();
                    ApplicationSettings.UsingHistoricGrades = setting.Value4.ToBool();
                    ApplicationSettings.ProductIdPig = setting.Value5.ToInt();
                    ApplicationSettings.ProductIdPiglet = setting.Value6.ToInt();
                    ApplicationSettings.PrintFreezerLabelAtGrader = setting.Value7.ToBool();
                    ApplicationSettings.SelectingCategoryAtLairage = setting.Value8.ToBool();
                    ApplicationSettings.AllowDuplicateSheepEartagsAtSequencer = setting.Value9.ToBool();
                    ApplicationSettings.PrintKillReportAtPayments = setting.Value10.ToBool();
                    var graderAnimals = setting.Value11.ToInt();
                    if (graderAnimals == 0)
                    {
                        ApplicationSettings.GraderAnimalsToRetrieve = 50;
                    }
                    else
                    {
                        ApplicationSettings.GraderAnimalsToRetrieve = graderAnimals;
                    }

                    ApplicationSettings.PrintKillReportAtPayments = setting.Value10.ToBool();
                    ApplicationSettings.EweTare = setting.Value11.ToDouble();
                    ApplicationSettings.LambTare = setting.Value12.ToDouble();
                    ApplicationSettings.EweLabelsToPrint = setting.Value13.ToInt();
                    ApplicationSettings.AllowGraderBeefToggle = setting.Value14.ToBool();
                    ApplicationSettings.AllowGraderSheepToggle = setting.Value15.ToBool();
                    ApplicationSettings.AllowGraderPigToggle = setting.Value16.ToBool();
                    ApplicationSettings.ResettingVealCategoriesAtSequencer = setting.Value17.ToBool();
                    ApplicationSettings.YoungVealAge = setting.Value18.ToInt() > 0 ? setting.Value18.ToInt() : 8;
                    ApplicationSettings.OldVealAge = setting.Value19.ToInt() > 0 ? setting.Value19.ToInt() : 12;
                    ApplicationSettings.LairageRadioButtonOptionsPartnerId3 = setting.Value20.ToNullableInt();
                    continue;
                }

                if (setting.Name.Equals("KillLine4"))
                {
                    ApplicationSettings.MaxLairageBeefIntake = setting.Value1.ToInt();
                    ApplicationSettings.MustSelectAgentAtLairage = setting.Value2.ToBool();
                    ApplicationSettings.UsingUKDepartmentServiceQueries = setting.Value3.ToBool();
                    ApplicationSettings.AutoCompletePaymentOnReportPrint = setting.Value4.ToBool();
                    ApplicationSettings.DoNotResetLairageAgent = setting.Value5.ToBool();
                    ApplicationSettings.RefreshSequencerQueue = setting.Value6.ToBool();
                    ApplicationSettings.HindProduct = setting.Value9.ToInt();
                    ApplicationSettings.ForeProduct = setting.Value10.ToInt();
                    ApplicationSettings.ImportWarehouseId = setting.Value11.ToInt();
                    ApplicationSettings.LairageRadioButtonOptionsPartnerId = setting.Value12.ToNullableInt();
                    ApplicationSettings.LairageRadioButtonOptionsPartnerId1 = setting.Value13.ToNullableInt();
                    ApplicationSettings.LairageRadioButtonOptionsPartnerId2 = setting.Value14.ToNullableInt();
                    ApplicationSettings.InvalidateCatBFarmAssurance = setting.Value15.ToBool();
                    ApplicationSettings.WelshDestination = setting.Value16;
                    ApplicationSettings.OrganicDestination = setting.Value17;
                    ApplicationSettings.StandardDestination = setting.Value18;
                    ApplicationSettings.UsingKillLineQueueNumbers = setting.Value19.ToBool();
                    ApplicationSettings.SelectFarmDestination = setting.Value20;
                    continue;
                }

                if (setting.Name.Equals("KillLine5"))
                {
                    ApplicationSettings.DisplayUTMOTMMessage = setting.Value1.ToBool();
                    ApplicationSettings.ShowFarmAssuredAtSequencer = setting.Value2.ToBool();
                    ApplicationSettings.ShowIdentigenAtGrader = setting.Value3.ToBool();
                    ApplicationSettings.ShowDressSpecAtGrader = setting.Value4.ToBool();
                    ApplicationSettings.ShowPrivateAtGrader = setting.Value5.ToBool();
                    ApplicationSettings.ShowKillTypeAtGrader = setting.Value6.ToBool();
                    ApplicationSettings.ShowDestinationsAtGrader = setting.Value7.ToBool();
                    ApplicationSettings.SearchByEartagAndKillNoAtSequencer = setting.Value8.ToBool();
                    ApplicationSettings.HidePullTerminalAtSequencer = setting.Value9.ToBool();
                    ApplicationSettings.LabelsToPrintAtSequencer = setting.Value10.ToInt() == 0 ? 3 : setting.Value10.ToInt();
                    ApplicationSettings.UseRiskToDeterminePrinterAtGrader = setting.Value11.ToBool();
                    ApplicationSettings.ApplySelectedCustomerToAllLairageAnimals = setting.Value12.ToBool();
                    ApplicationSettings.DontShowWebServiceOption = setting.Value13.ToBool();
                    ApplicationSettings.UsingHideStationAtSequencer = setting.Value14.ToBool();
                    ApplicationSettings.UsingSheepWand = setting.Value15.ToBool();
                    ApplicationSettings.ShowFreezerTypeAtLairage = setting.Value16.ToBool();
                    ApplicationSettings.DressSpecRequiredAtGrader = setting.Value17.ToBool();
                    ApplicationSettings.FullCarcassProduct = setting.Value18.ToInt();
                    ApplicationSettings.UseFullCarcassProduct = setting.Value19.ToBool();
                    ApplicationSettings.PrintSequencerLabel = setting.Value20.ToBool();
                    continue;
                }

                if (setting.Name.Equals("KillLine6"))
                {
                    ApplicationSettings.HideAllKillTouchscreenAttributes = setting.Value1.ToBool();
                    ApplicationSettings.DontAllowLairageMove = setting.Value2.ToBool();
                    ApplicationSettings.GradeVealAsWholeAnimalWithZ = setting.Value3.ToBool();
                    continue;
                }

                if (setting.Name.Equals("UpdateKillLine"))
                {
                    try
                    {
                        ApplicationSettings.SequencerMode = (SequencerMode)Enum.Parse(typeof(SequencerMode), setting.Value1);
                    }
                    catch
                    {
                        ApplicationSettings.SequencerMode = SequencerMode.Beef;
                    }

                    try
                    {
                        ApplicationSettings.GraderMode = (GraderMode)Enum.Parse(typeof(GraderMode), setting.Value2);
                    }
                    catch
                    {
                        ApplicationSettings.GraderMode = GraderMode.Beef;
                    }

                    ApplicationSettings.UsingBordBia = setting.Value3.ToBool();
                    ApplicationSettings.UsingAims = setting.Value4.ToBool();

                    continue;
                }

                if (setting.Name.Equals("MustSelectContainerForDispatch"))
                {
                    ApplicationSettings.MustSelectContainerForDispatch = setting.Value1.ToBool();
                    ApplicationSettings.CheckForShippingContainerDateMatch = setting.Value2.ToBool();
                    ApplicationSettings.DisallowInvoiceReOpening = setting.Value3.ToBool();
                    ApplicationSettings.GradesNotRequiredForVeal = setting.Value4.ToBool();
                    ApplicationSettings.ProductIdSheepFull = setting.Value20.ToInt();
                    ApplicationSettings.DisableCategoryChangeAtGrader = setting.Value18.ToBool();
                    continue;
                }

                if (setting.Name.Equals("UsingTareCalculator"))
                {
                    ApplicationSettings.UsingTareCalculator = setting.Value1.ToBool();
                    ApplicationSettings.ZeroTareOutsideOfDispatch = setting.Value2.ToBool();
                    ApplicationSettings.DisableContainerTareSelection = setting.Value3.ToBool();
                    ApplicationSettings.DisableTouchscreenGenericSelection = setting.Value4.ToBool();
                    ApplicationSettings.NeedleTargetWeightSize =
                        setting.Value5.ToDouble() <= 0 ? 30 : setting.Value5.ToDouble();
                    ApplicationSettings.UOMAddDays = setting.Value6.ToInt();
                    continue;
                }


                if (setting.Name.Equals("DefaultStockLocations"))
                {
                    ApplicationSettings.DefaultIntakeLocationId = setting.Value1.ToInt();
                    ApplicationSettings.DefaultOutOfProductionLocationId = setting.Value2.ToInt();
                    ApplicationSettings.DefaultPalletLocationId = setting.Value3.ToInt();
                    ApplicationSettings.DefaultIntoProductionLocationId = setting.Value4.ToInt();
                    ApplicationSettings.DefaultDispatchLocationId = setting.Value5.ToInt();
                    ApplicationSettings.DefaultReturnsLocationId = setting.Value6.ToInt();
                    continue;
                }

                if (setting.Name.Equals("PromptForReweighAtDispatch"))
                {
                    ApplicationSettings.PromptForReweighAtDispatch = setting.Value1.ToBool();
                    ApplicationSettings.SelectingProductInReweighMode = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("StartDispatchInReweighMode"))
                {
                    ApplicationSettings.StartDispatchInReweighMode = setting.Value1.ToBool();
                    ApplicationSettings.PrintReportAtPalletisation = setting.Value2.ToBool();
                    ApplicationSettings.WindowsCalculatorPath = string.IsNullOrEmpty(setting.Value3) ? @"C:\Windows\System32\calc.exe" : setting.Value3;
                    continue;
                }

                if (setting.Name.Equals("CanSelectCustomersAtOutOfProduction"))
                {
                    ApplicationSettings.CanSelectCustomersAtOutOfProduction = setting.Value1.ToBool();
                    ApplicationSettings.HideProcessSelection = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ResetLabelOnEveryPrint"))
                {
                    ApplicationSettings.ResetLabelOnEveryPrint = setting.Value1.ToBool();
                    ApplicationSettings.ResetLabelOnProductSelection = setting.Value2.ToBool();
                    ApplicationSettings.ShowExpandedProductGroups = setting.Value3.ToBool();
                    ApplicationSettings.ProductGroupName = setting.Value4;
                    ApplicationSettings.MustEnterPalletNumberAtIntake = setting.Value5.ToBool();
                    ApplicationSettings.ResetLocationsAtStockMovement = setting.Value6.ToBool();
                    ApplicationSettings.SearchProductsBycode = setting.Value7.ToBool();
                    ApplicationSettings.EnforceAutoBoxing = setting.Value15.ToBool();
                    ApplicationSettings.AutoBoxProductGroups = setting.Value16.ToInt();
                    continue;
                }

                if (setting.Name.Equals("UseShippingDate"))
                {
                    ApplicationSettings.UseShippingDate = setting.Value1.ToBool();
                    try
                    {
                        ApplicationSettings.BatchNumberType =
                            (BatchNumberType) Enum.Parse(typeof(BatchNumberType), setting.Value2);
                    }
                    catch 
                    {
                        ApplicationSettings.BatchNumberType = BatchNumberType.Standard;
                    }

                    ApplicationSettings.ShowRoutesAtTouchscreenDispatch = setting.Value3.ToBool();
                    ApplicationSettings.CreateProductionStockAtDispatch = setting.Value4.ToBool();
                    
                    continue;
                }

                if (setting.Name.Equals("ScannerEmulatorMode"))
                {
                    ApplicationSettings.ScannerEmulatorMode = setting.Value1.ToBool();
                    ApplicationSettings.ScannerEmulatorHeight = setting.Value2.ToDouble() <= 0 ? 800 : setting.Value1.ToDouble();
                    ApplicationSettings.ScannerEmulatorWidth = setting.Value3.ToDouble() <= 0 ? 500 : setting.Value1.ToDouble();
                    ApplicationSettings.ScannerEmulatorTop = setting.Value4.ToDouble();
                    ApplicationSettings.ScannerEmulatorLeft = setting.Value5.ToDouble();
                    continue;
                }

                if (setting.Name.Equals("ConnectToSSRS"))
                {
                    ApplicationSettings.ConnectToSSRS = setting.Value1.ToBool();
                    ApplicationSettings.UseHistoricDispatchMode = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("LabelHistoryTransactionsNumberToRetrieve"))
                {
                    ApplicationSettings.LabelHistoryTransactionsNumberToRetrieve = setting.Value1.ToInt();
                    if (ApplicationSettings.LabelHistoryTransactionsNumberToRetrieve == 0)
                    {
                        ApplicationSettings.LabelHistoryTransactionsNumberToRetrieve = 100;
                    }

                    continue;
                }

                if (setting.Name.Equals("GoodsInLabelsToPrint"))
                {
                    ApplicationSettings.GoodsInLabelsToPrint = setting.Value1.ToInt();
                    if (ApplicationSettings.GoodsInLabelsToPrint == 0)
                    {
                        ApplicationSettings.GoodsInLabelsToPrint = 1;
                    }

                    ApplicationSettings.DisablePrintingAtIntake = setting.Value2.ToBool();
                    ApplicationSettings.ExternalLabelsToPrintBeforeSlowDown =
                        setting.Value3.ToInt() == 0 ? 10 : setting.Value3.ToInt();
                    ApplicationSettings.ExternalLabelsSlowDownTime =
                        setting.Value4.ToInt() == 0 ? 200 : setting.Value4.ToInt();

                    continue;
                }

                if (setting.Name.Equals("DispatchLabelsToPrint"))
                {
                    ApplicationSettings.DispatchLabelsToPrint = setting.Value1.ToInt();
                    if (ApplicationSettings.DispatchLabelsToPrint == 0)
                    {
                        ApplicationSettings.DispatchLabelsToPrint = 1;
                    }

                    ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatch = setting.Value2.ToBool();
                    ApplicationSettings.AllowBatchSelectionForSerialStockAtDispatch = setting.Value3.ToBool();
                    ApplicationSettings.DisablePrintLabelAtDispatch = setting.Value4.ToBool();
                    ApplicationSettings.AllowBoxesOnlyToBeSelectedAtDispatchGroups = setting.Value5;

                    continue;
                }

                if (setting.Name.Equals("IntoProductionLabelsToPrint"))
                {
                    ApplicationSettings.IntoProductionLabelsToPrint = setting.Value1.ToInt();
                    if (ApplicationSettings.IntoProductionLabelsToPrint == 0)
                    {
                        ApplicationSettings.IntoProductionLabelsToPrint = 1;
                    }

                    continue;
                }

                if (setting.Name.Equals("OutOfProductionLabelsToPrint"))
                {
                    ApplicationSettings.OutOfProductionLabelsToPrint = setting.Value1.ToInt();
                    if (ApplicationSettings.OutOfProductionLabelsToPrint == 0)
                    {
                        ApplicationSettings.OutOfProductionLabelsToPrint = 1;
                    }

                    ApplicationSettings.DisablePrintingAtOutOfProduction = setting.Value2.ToBool();
                    ApplicationSettings.UsingItemLabelsForPieceLabelsAtOutOfProduction = setting.Value3.ToBool();

                    continue;
                }

                if (setting.Name.Equals("TransactionsToTakeForReportSearch"))
                {
                    ApplicationSettings.TransactionsToTakeForReportSearch = setting.Value1.ToInt();
                    if (ApplicationSettings.TransactionsToTakeForReportSearch == 0)
                    {
                        ApplicationSettings.TransactionsToTakeForReportSearch = 1000;
                    }

                    ApplicationSettings.CheckSequencedAnimalsCount = setting.Value2.ToInt();

                    continue;
                }

                if (setting.Name.Equals("TouchScreenModeOnly"))
                {
                    ApplicationSettings.TouchScreenModeOnly = setting.Value1.ToBool();
                    ApplicationSettings.AllowTouchScreenResize = setting.Value2.ToBool();
                    ApplicationSettings.PasswordMustBeUnique = setting.Value3.ToBool();
                    ApplicationSettings.CanEditBatches = setting.Value4.ToBool();
                    ApplicationSettings.EposMode = setting.Value10.ToBool();
                    ApplicationSettings.PasswordNotRequired = setting.Value5.ToBool();
                    ApplicationSettings.GetBoxCountAtDispatch = setting.Value6.ToBool();
                    ApplicationSettings.SelectIntakeBatchesAtDispatch = setting.Value7.ToBool();
                    ApplicationSettings.UseKeyboardWedgeAtTouchscreen = setting.Value8.ToBool();
                    continue;
                }

                if (setting.Name.Equals("TouchScreenModeOnlyIntake"))
                {
                    ApplicationSettings.TouchScreenModeOnlyIntake = setting.Value1.ToBool();
                    ApplicationSettings.TouchScreenModeOnlySequencer = setting.Value2.ToBool();
                    ApplicationSettings.TouchScreenModeOnlyGrader = setting.Value3.ToBool();
                    ApplicationSettings.TouchScreenModeOnlyWorkflow = setting.Value4.ToBool();
                    continue;
                }

                if (setting.Name.Equals("TouchScreenModeOnlyIntoProduction"))
                {
                    ApplicationSettings.TouchScreenModeOnlyIntoProduction = setting.Value1.ToBool();
                    ApplicationSettings.CreatingTouchscreenRecipes = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("TouchScreenModeOnlyOutOfProduction"))
                {
                    ApplicationSettings.TouchScreenModeOnlyOutOfProduction = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("TouchScreenModeOnlyDispatch"))
                {
                    ApplicationSettings.TouchScreenModeOnlyDispatch = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ScanningForExternalBarcodeReadOnlyAtDispatch"))
                {
                    ApplicationSettings.ScanningForExternalBarcodeReadOnlyAtDispatch = setting.Value1.ToBool();
                    ApplicationSettings.ScanningForExternalBarcodeReadOnlyAtIntoProduction = setting.Value2.ToBool();
                    ApplicationSettings.ScanningForScotBeefBarcodeReadAtDispatch = setting.Value3.ToBool();
                    ApplicationSettings.Gs1Identifier = setting.Value5;
                    ApplicationSettings.Gs1IdentifierLength = setting.Value6.ToInt();
                    ApplicationSettings.ClearBatchOnDispatchProductChange = setting.Value7.ToBool();
                    ApplicationSettings.AutoSplitCarcassAtDispatch = setting.Value8.ToBool();
                    ApplicationSettings.ScanningForIntakeBatchesAtDispatch = setting.Value9.ToBool();
                    ApplicationSettings.RetrieveSerialNoFromBarcode = setting.Value10.ToBool();

                    continue;
                }

                if (setting.Name.Equals("MinWeightAllowed"))
                {
                    ApplicationSettings.MinWeightAllowedIntake = setting.Value1.ToDecimal();
                    ApplicationSettings.MinWeightAllowedIntoProduction = setting.Value2.ToDecimal();
                    ApplicationSettings.MinWeightAllowedOutOfProduction = setting.Value3.ToDecimal();
                    ApplicationSettings.MinWeightAllowedDispatch = setting.Value4.ToDecimal();
                    continue;
                }

                if (setting.Name.Equals("AutoCopyToGoodsIn"))
                {
                    ApplicationSettings.AutoCopyToGoodsIn = setting.Value1.ToBool();
                    //ApplicationSettings.UseDeliveryDateForIntakeReportSearch = setting.Value3.ToBool();
                    ApplicationSettings.BatchAppendDDMM = setting.Value4.ToBool();
                    ApplicationSettings.PrintBatchLabelOnItakeCompletion = setting.Value5.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox"))
                {
                    ApplicationSettings.DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox"))
                {
                    ApplicationSettings.DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AllowManualBatchCreation"))
                {
                    ApplicationSettings.AllowManualBatchCreation = setting.Value1.ToBool();
                    ApplicationSettings.AccumulateNotesAtBatchEdit = setting.Value2.ToBool();
                    ApplicationSettings.KilledSheepToAppearAtTopOfGrid = setting.Value3.ToInt();
                    continue;
                }

                if (setting.Name.Equals("AllowIntakeOverAmount"))
                {
                    ApplicationSettings.AllowIntakeOverAmount = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AllowDispatchOverAmount"))
                {
                    ApplicationSettings.AllowDispatchOverAmount = setting.Value1.ToBool();
                    ApplicationSettings.DispatchOverAmountHardStop = setting.Value2.ToBool();
                    ApplicationSettings.ReprintStockMoveItem = setting.Value3.ToBool();
                    ApplicationSettings.ReprintStockMoveBoxItem = setting.Value4.ToBool();
                    ApplicationSettings.DisallowProductRemovalAtDispatch = setting.Value5.ToBool();
                    ApplicationSettings.ShowProductRemarksAtSaleOrder = setting.Value6.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ClearProductionOrdersOnEntry"))
                {
                    ApplicationSettings.ClearProductionOrdersOnEntry = setting.Value1.ToBool();
                    ApplicationSettings.WarnIfDuplicateProductionReference = setting.Value2.ToBool();
                    ApplicationSettings.CheckDispatchBatchAgainstProduct = setting.Value3.ToBool();
                    ApplicationSettings.ManualBatchDefaultLocationID = setting.Value4.ToInt();
                    ApplicationSettings.CheckPartnerForLiveOrder = setting.Value5.ToBool();
                    ApplicationSettings.DisallowSlashesInProductionName = setting.Value6.ToBool();
                }

                if (setting.Name.Equals("ClearPartnersOnEntry"))
                {
                    ApplicationSettings.ClearPartnersOnEntry = setting.Value1.ToBool();
                    ApplicationSettings.FilterPartnerCountOnSearchScreen = setting.Value2.ToInt() == 0 ? 56 : setting.Value2.ToInt();
                    ApplicationSettings.TouchscreenPartnerSearchByIncludes = setting.Value3.ToBool();
                    ApplicationSettings.ShowOrHideGroupPartners = setting.Value4.ToNullableInt();
                    ApplicationSettings.ShowOrHidePartnerGroupID = setting.Value5.ToInt();
                    if (string.IsNullOrWhiteSpace(setting.Value6) ||
                        setting.Value6.CompareIgnoringCase(Constant.Standard))
                    {
                        ApplicationSettings.DispatchDocketNumberType = Constant.Standard;
                    }
                    else
                    {
                        ApplicationSettings.DispatchDocketNumberType = Constant.Internal;
                    }

                    if (string.IsNullOrWhiteSpace(setting.Value7) ||
                        setting.Value7.CompareIgnoringCase(Constant.Standard))
                    {
                        ApplicationSettings.IntakeDocketNumberType = Constant.Standard;
                    }
                    else
                    {
                        ApplicationSettings.IntakeDocketNumberType = Constant.Internal;
                    }

                    ApplicationSettings.FilterProductionOrdersByDepartment = setting.Value8.ToBool();
                    
                    continue;
                }

                if (setting.Name.Equals("AllowDispatchProductNotOnOrder"))
                {
                    ApplicationSettings.AllowDispatchProductNotOnOrder = setting.Value1.ToBool();
                    ApplicationSettings.OnlyShowLivestockSuppliersAtBusinessPartner = setting.Value2.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DisableSaleOrderDiscounts"))
                {
                    ApplicationSettings.DisableSaleOrderDiscounts = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("ResetSerialAttributes"))
                {
                    ApplicationSettings.ResetSerialAttributes = setting.Value1.ToBool();
                    ApplicationSettings.SaveBatchAttributesOnEntry = setting.Value2.ToBool();
                    ApplicationSettings.OnlyAllowAuthorisedAnimalsAtSequencer = setting.Value3.ToBool();
                    continue;
                }

                if (setting.Name.Equals("HandheldFontSize"))
                {
                    ApplicationSettings.HandheldFontSize = setting.Value1.ToDouble() <= 0 ? 18: setting.Value1.ToDouble();
                    ApplicationSettings.HandheldGridFontSize = setting.Value2.ToDouble() <= 0 ? 16 : setting.Value2.ToDouble();
                    ApplicationSettings.HandheldDispatchGridRowSize = setting.Value3.ToDouble() <= 0 ? 50 : setting.Value3.ToDouble();
                    ApplicationSettings.HandheldOrderCountFontSize = setting.Value4.ToDouble() <= 0 ? 14 : setting.Value4.ToDouble();
                    if (ApplicationSettings.HandheldGridFontSize <= 0)
                    {
                        ApplicationSettings.HandheldGridFontSize = 12;
                    }

                    if (ApplicationSettings.HandheldFontSize <= 0)
                    {
                        ApplicationSettings.HandheldFontSize = 14;
                    }

                    continue;
                }

                

                if (setting.Name.Equals("ScannerSettings"))
                {
                    ApplicationSettings.ScannerSettings = setting.Value1;
                    ApplicationSettings.ConnectToScanner = setting.Value2.ToBool();
                    ApplicationSettings.ScannerPortDataWaitTime = setting.Value3.ToInt() == 0 ? 100 : setting.Value3.ToInt();
                    ApplicationSettings.HandheldScannerDataWaitTime = setting.Value4.ToInt() == 0 ? 250 : setting.Value4.ToInt();
                    ApplicationSettings.KeyboardWedgeTerminator = string.IsNullOrEmpty(setting.Value5) ? "x" : setting.Value5;

                    continue;
                }

                if (setting.Name.Equals("WandSettings"))
                {
                    ApplicationSettings.WandSettings = setting.Value1;
                    ApplicationSettings.ConnectToWand = setting.Value2.ToBool();
                    ApplicationSettings.WandPortDataWaitTime = setting.Value3.ToInt() == 0 ? 400 : setting.Value3.ToInt();
                    ApplicationSettings.WandPortSendString = string.IsNullOrWhiteSpace(setting.Value4) ? "r" : setting.Value4;
                    ApplicationSettings.WandPortTimeOut = setting.Value5.ToInt() == 0 ? 20 : setting.Value5.ToInt();
                    ApplicationSettings.WandPortEraseString = string.IsNullOrWhiteSpace(setting.Value6) ? "e" : setting.Value6;
                    continue;
                }

                if (setting.Name.Equals("ReportServerPath"))
                {
                    ApplicationSettings.ReportServerPath = setting.Value1;
                    continue;
                }

                if (setting.Name.Equals("RecallCustomerFromProductAtOutOfProduction"))
                {
                    ApplicationSettings.RecallCustomerFromProductAtOutOfProduction = setting.Value1.ToBool();
                    continue;
                }

                if (setting.Name.Equals("QtyMustBeEnteredOnSaleOrderLine"))
                {
                    ApplicationSettings.QtyMustBeEnteredOnSaleOrderLine = setting.Value1.ToBool();
                    ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarning = setting.Value2.ToBool();
                    ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate = setting.Value3.ToBool();
                    ApplicationSettings.AllowDuplicateProductOnOrder = setting.Value4.ToBool();
                    ApplicationSettings.DisablePriceChecksOnSaleOrders = setting.Value5.ToBool();
                    continue;
                }

                if (setting.Name.Equals("AutoCopyToDispatch"))
                {
                    ApplicationSettings.AutoCopyToDispatch = setting.Value1.ToBool();
                    ApplicationSettings.AlwaysCopyToDispatch = setting.Value2.ToBool();
                    ApplicationSettings.IgnoreCompleteDispatchCheck = setting.Value3.ToBool();
                    ApplicationSettings.AutoCopyToLairage = setting.Value4.ToBool();
                    ApplicationSettings.AlwaysCopyToGoodsIn = setting.Value5.ToBool();
                }

                if (setting.Name.Equals("SaleOrderDeliveryDaysForward"))
                {
                    ApplicationSettings.SaleOrderDeliveryDaysForward = setting.Value1.ToInt();
                    ApplicationSettings.LairageDeliveryDaysForward = setting.Value2.ToInt();
                    ApplicationSettings.ProposedKillDateDaysForward = setting.Value3.ToInt();
                    continue;
                }

                if (setting.Name.Equals("SaleOrderValidUntilDaysForward"))
                {
                    ApplicationSettings.SaleOrderValidUntilDaysForward = setting.Value1.ToInt();
                    continue;
                }

                if (setting.Name.Equals("SSRSWebServiceURL"))
                {
                    ApplicationSettings.SSRSWebServiceURL = setting.Value1;
                    continue;
                }

                if (setting.Name.Equals("SSRSUserName"))
                {
                    ApplicationSettings.SSRSUserName = setting.Value1;
                    ApplicationSettings.SSRSCompanyID = setting.Value2.ToInt();
                    continue;
                }

                if (setting.Name.Equals("SSRSPassword"))
                {
                    ApplicationSettings.SSRSPassword = setting.Value1;
                    continue;
                }

                if (setting.Name.Equals("SSRSDomain"))
                {
                    ApplicationSettings.SSRSDomain = setting.Value1;
                    continue;
                }

                if (setting.Name.Equals("WeightsPrecision"))
                {
                    //ApplicationSettings.WeightsPrecision = setting.Value1.ToInt();
                    ApplicationSettings.RecallBatchAtIntakeScan = setting.Value4.ToBool();
                    ApplicationSettings.RecallProductAtScan = setting.Value5.ToBool();
                    ApplicationSettings.RecallProductAtScanStartPos = setting.Value6.ToInt();
                    ApplicationSettings.RecallProductAtScanLength = setting.Value7.ToInt();
                    continue;
                }

                if (setting.Name.Equals("CardViewCardWidth"))
                {
                    ApplicationSettings.CardViewCardWidth = setting.Value1.ToDouble();
                    if (ApplicationSettings.CardViewCardWidth <= 0)
                    {
                        ApplicationSettings.CardViewCardWidth = 160;
                    }
               
                    ApplicationSettings.CardViewCardWidthIntakeBatches = setting.Value2.ToDouble();
                    if (ApplicationSettings.CardViewCardWidthIntakeBatches <= 0)
                    {
                        ApplicationSettings.CardViewCardWidthIntakeBatches = 160;
                    }

                    //Settings.Default.CardViewCardWidthItem = ApplicationSettings.CardViewCardWidth * 1.02;

                    continue;
                }

                if (setting.Name.Equals("CardViewNarrowCardWidth"))
                {
                    ApplicationSettings.CardViewNarrowCardWidth = setting.Value1.ToDouble();
                    if (ApplicationSettings.CardViewNarrowCardWidth <= 0)
                    {
                        ApplicationSettings.CardViewNarrowCardWidth = 150;
                    }

                    Settings.Default.CardViewCardNarrowWidthItem = ApplicationSettings.CardViewNarrowCardWidth * 1.02;

                    continue;
                }

                if (setting.Name.Equals("CardViewCardHeight"))
                {
                    ApplicationSettings.CardViewCardHeight = setting.Value1.ToDouble();
                    if (ApplicationSettings.CardViewCardHeight <= 0)
                    {
                        ApplicationSettings.CardViewCardHeight = 100;
                    }

                    //Settings.Default.CardViewCardHeightItem = ApplicationSettings.CardViewCardHeight * 1.02;

                    ApplicationSettings.CardViewCardHeightIntakeBatches = setting.Value2.ToDouble();
                    if (ApplicationSettings.CardViewCardHeightIntakeBatches <= 0)
                    {
                        ApplicationSettings.CardViewCardHeightIntakeBatches = 100;
                    }

                    ApplicationSettings.CardViewPartnersCardHeight = setting.Value3.ToDouble();
                    if (ApplicationSettings.CardViewPartnersCardHeight <= 0)
                    {
                        ApplicationSettings.CardViewPartnersCardHeight = 100;
                    }

                    ApplicationSettings.CardViewPartnersCardWidth= setting.Value4.ToDouble();
                    if (ApplicationSettings.CardViewPartnersCardWidth <= 0)
                    {
                        ApplicationSettings.CardViewPartnersCardWidth = 160;
                    }
                    //Settings.Default.CardViewCardHeightItem = ApplicationSettings.CardViewCardHeight * 1.02;

                    continue;
                }

                if (setting.Name.Equals("CardViewMenuCardHeight"))
                {
                    ApplicationSettings.CardViewMenuCardHeight = setting.Value1.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardHeight <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardHeight = 250;
                    }

                    ApplicationSettings.CardViewMenuCardHeightItem = ApplicationSettings.CardViewMenuCardHeight * 1.02;

                    continue;
                }

                if (setting.Name.Equals("CardViewMenuCardWidth"))
                {
                    ApplicationSettings.CardViewMenuCardWidth = setting.Value1.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardWidth <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardWidth = 250;
                    }

                    ApplicationSettings.CardViewMenuCardWidthItem = ApplicationSettings.CardViewMenuCardWidth * 1.02;

                    ApplicationSettings.CardViewMenuCardWidthCollection = setting.Value2.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardWidthCollection <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardWidthCollection = 100;
                    }

                    ApplicationSettings.CardViewMenuCardHeightCollection = setting.Value3.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardHeightCollection <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardHeightCollection = 70;
                    }

                    if (string.IsNullOrEmpty(setting.Value4) || setting.Value4.Equals("0"))
                    {
                        ApplicationSettings.TouchscreenMenuButtonFontSizeCollection = 12;
                    }
                    else
                    {
                        ApplicationSettings.TouchscreenMenuButtonFontSizeCollection = setting.Value4.ToInt();
                    }

                    ApplicationSettings.CardViewMenuCardWidthRoutes = setting.Value5.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardWidthRoutes <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardWidthRoutes = 140;
                    }

                    ApplicationSettings.CardViewMenuCardHeightRoutes = setting.Value6.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardHeightRoutes <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardHeightRoutes = 100;
                    }

                    ApplicationSettings.CardViewMenuCardRoutesFontSize = setting.Value7.ToDouble();
                    if (ApplicationSettings.CardViewMenuCardRoutesFontSize <= 0)
                    {
                        ApplicationSettings.CardViewMenuCardRoutesFontSize = 14;
                    }

                    continue;
                }

                if (setting.Name.Equals("CardViewNarrowCardHeight"))
                {
                    ApplicationSettings.CardViewNarrowCardHeight = setting.Value1.ToDouble();
                    if (ApplicationSettings.CardViewNarrowCardHeight <= 0)
                    {
                        ApplicationSettings.CardViewNarrowCardHeight = 150;
                    }

                    Settings.Default.CardViewCardNarrowHeightItem = ApplicationSettings.CardViewNarrowCardHeight * 1.02;

                    continue;
                }

                if (setting.Name.Equals("DisplayData"))
                {
                    ApplicationSettings.DisplayIntakeData = setting.Value1;
                    ApplicationSettings.DisplayProductionData = setting.Value2;
                    ApplicationSettings.DisplayDispatchData = setting.Value3;
                    ApplicationSettings.DisplayOutOfProductionData = setting.Value4;
                    ApplicationSettings.DisplayWorkflowData = setting.Value5;
                    ApplicationSettings.DisplayStockProductionData = setting.Value6;
                    ApplicationSettings.DisplayProductionBatchData = setting.Value7;
                    ApplicationSettings.DisplayLabelData = setting.Value8;
                    ApplicationSettings.DisplayRoutesData = setting.Value9;
                }

                if (setting.Name.Equals("ManuallyEnterBatchNumber"))
                {
                    ApplicationSettings.ManuallyEnterBatchNumber = setting.Value1.ToBool();
                    ApplicationSettings.DefaultQty = setting.Value2.ToInt();
                    continue;
                }

                if (setting.Name.Equals("DatabasePollingInterval"))
                {
                    ApplicationSettings.DatabasePollingInterval = setting.Value1.ToInt();
                    if (ApplicationSettings.DatabasePollingInterval <= 0)
                    {
                        ApplicationSettings.DatabasePollingInterval = 10;
                    }

                    continue;
                }

                if (setting.Name.Equals("AlwaysShowButcheryBatch"))
                {
                    ApplicationSettings.AlwaysShowButcheryBatch = setting.Value1.ToBool();
                    ApplicationSettings.AutoCreateProductCode = setting.Value2.ToBool();
                    ApplicationSettings.AutoTickScrapie = setting.Value3.ToBool();
                    ApplicationSettings.AutoCompleteRecipeMix = setting.Value4.ToBool();
                    ApplicationSettings.ShowPriceAtDispatch = setting.Value5.ToBool();
                    continue;
                }

                if (setting.Name.Equals("DisplayReportsOnTouchscreen"))
                {
                    ApplicationSettings.DisplayReportsOnTouchscreen = setting.Value1.ToBool();
                    ApplicationSettings.ReportsOnTouchscreen = setting.Value2;
                    ApplicationSettings.TouchscreenReportsCopiesToPrint = setting.Value3.ToInt() == 0
                        ? 1
                        : setting.Value3.ToInt();
                    ApplicationSettings.PrintReportOnCompletion = setting.Value4.ToBool();
                    ApplicationSettings.AutoPrintDispatchReport = setting.Value5.ToBool();
                    ApplicationSettings.AutoPrintDispatchDetailReport = setting.Value6.ToBool();
                    ApplicationSettings.AutoPrintInvoiceReport = setting.Value7.ToBool();

                    continue;
                }

                if (setting.Name.Equals("BatchCreationMode"))
                {
                    if (string.IsNullOrEmpty(setting.Value1))
                    {
                        ApplicationSettings.BatchCreationMode = BatchCreationMode.Manual;
                    }
                    else
                    {
                        ApplicationSettings.BatchCreationMode = (BatchCreationMode)Enum.Parse(typeof(BatchCreationMode), setting.Value1);
                    }

                    ApplicationSettings.PrintQALabelAfterFirstLabelInBatch = setting.Value2.ToBool();
                    ApplicationSettings.CanOnlyBoxOffProductQtyAtProduction = setting.Value3.ToBool();
                    continue;
                }

                #endregion
            }

            Settings.Default.Save();
            this.Log.LogInfo(this.GetType(), "Settings set up complete");
        }

        /// <summary>
        /// Logs data to the db.
        /// </summary>
        /// <param name="log">The data to log.</param>
        /// <returns>Flag, as to successful log or not.</returns>
        public bool LogToDatabase(Log log)
        {
            return this.deviceRepository.LogToDatabase(log);
        }

        /// <summary>
        /// Updates the local device settings.
        /// </summary>
        public void SaveDeviceSettings()
        {
            this.Log.LogInfo(this.GetType(), "SaveDeviceSettings(): Saving settings..");
            var settings = new List<DeviceSetting>();
            var deviceId = NouvemGlobal.DeviceId.ToInt();
            var rdpId = NouvemGlobal.RDPIdentifier;

            #region add

            settings.Add(new DeviceSetting
            {
                Name = "Aims",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TestMode",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SystemInformation",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DontAutoFilterProductionBatchSearch",
                Value1 = ApplicationSettings.DontAutoFilterProductionBatchSearch.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CheckRoutesAtOrder",
                Value1 = ApplicationSettings.CheckRoutesAtOrder.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ComboBoxSearchType",
                Value1 = ApplicationSettings.ComboBoxSearchType.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "MustSelectContainerForDispatch",
                Value1 = ApplicationSettings.MustSelectContainerForDispatch.ToString(),
                Value2 = ApplicationSettings.CheckForShippingContainerDateMatch.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "OutOfProductionMode",
                Value1 = ApplicationSettings.OutOfProductionMode.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "UseRDPIdentifierForSettings",
                Value1 = ApplicationSettings.UseRDPIdentifierForSettings.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CheckForExcessCarcassSplitWeighings",
                Value1 = ApplicationSettings.CheckForExcessCarcassSplitWeighings.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "PriceListDefaults",
                Value1 = ApplicationSettings.PriceListDefaultRounding.ToString(),
                Value2 = ApplicationSettings.PriceListDefaultRoundingOption.ToString(),
                Value3 = ApplicationSettings.PriceListDefaultCurrency.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "OrderAmountToTake",
                Value1 = ApplicationSettings.OrderAmountToTake.ToString(),
                Value2= ApplicationSettings.RecentOrdersAmtToTake.ToString(),
                Value3 = ApplicationSettings.RecentOrdersAtDispatchByDelivered.ToString(),
                Value4 = ApplicationSettings.DefaultToFindAtDesktopDispatch.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "UseDefaultProductAtOutOfProduction",
                Value1 = ApplicationSettings.UseDefaultProductAtOutOfProduction.ToString(),
                Value2 = ApplicationSettings.DefaultProductAtOutOfProductionID.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenOutOfProductionGroupMenuWidth",
                Value1 = ApplicationSettings.TouchscreenOutOfProductionGroupMenuWidth.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "Customer",
                Value1 = ApplicationSettings.Customer,
                Value2 = ApplicationSettings.PlantCode,
                Value3 = ApplicationSettings.DefaultAnimalCountry,
          
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "Modules",
                Value1 = ApplicationSettings.ModuleEnableKillLine.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "RestartApplication",
                Value1 = ApplicationSettings.RestartApplication.ToString(),
                Value2 = ApplicationSettings.RestartApplicationHour.ToString(),
                Value3 = ApplicationSettings.RestartApplicationMin.ToString(),
                Value4 = ApplicationSettings.LogOutOnApplicationIdle.ToString(),
                Value5 = ApplicationSettings.LogOutOnApplicationIdleTime.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ScanningExternalCarcass",
                Value1 = ApplicationSettings.ScanningExternalCarcass.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "MessageBoxCoordinates",
                Value1 = ApplicationSettings.MessageBoxTop.ToString(),
                Value2 = ApplicationSettings.MessageBoxLeft.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DefaultGroupTemplates",
                Value1 = ApplicationSettings.SetDefaultGroupTemplates.ToString(),
                Value2 = ApplicationSettings.TraceabilityDefaultTemplateId.ToString(),
                Value3 = ApplicationSettings.QualityDefaultTemplateId.ToString(),
                Value4 = ApplicationSettings.DatesDefaultTemplateId.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisplayReportsOnTouchscreen",
                Value1 = ApplicationSettings.DisplayReportsOnTouchscreen.ToString(),
                Value2 = ApplicationSettings.ReportsOnTouchscreen,
                Value3 = ApplicationSettings.TouchscreenReportsCopiesToPrint.ToString(),
                Value4 = ApplicationSettings.PrintReportOnCompletion.ToString(),
                Value5 = ApplicationSettings.AutoPrintDispatchReport.ToString(),
                Value6 = ApplicationSettings.AutoPrintDispatchDetailReport.ToString(),
                Value7 = ApplicationSettings.AutoPrintInvoiceReport.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "PricingItemByThePiece",
                Value1 = ApplicationSettings.PricingItemByThePiece.ToString(),
                Value3 = ApplicationSettings.BoxQtyAlwaysOne.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IgnoreTrimGroupsForConcessions",
                Value1 = ApplicationSettings.IgnoreTrimGroupsForConcessions.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CheckForLinkedDatabaseScannedDispatchSerial",
                Value1 = ApplicationSettings.CheckForLinkedDatabaseScannedDispatchSerial.ToString(),
                Value2 = ApplicationSettings.LinkedDatabaseScannedDispatchSerialIdentity.ToString(),
                Value3 = ApplicationSettings.LinkedDatabaseScannedDispatchSerialStart.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisallowCustomerSelectionAtDispatch",
                Value1 = ApplicationSettings.DisallowCustomerSelectionAtDispatch.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisallowTouchscreenModuleChange",
                Value1 = ApplicationSettings.DisallowTouchscreenModuleChange.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "RetainTouchscreenFilter",
                Value1 = ApplicationSettings.RetainTouchscreenOrdersFilter.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AutoCreateCustomerPriceList",
                Value1 = ApplicationSettings.AutoCreateCustomerPriceList.ToString(),
                Value2 = ApplicationSettings.IgnoreCustomerWithNoPriceListCheck.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CustomerCurrency",
                Value1 = ApplicationSettings.CustomerCurrency,

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DispatchOrderNotFilledWarning",
                Value1 = ApplicationSettings.DispatchOrderNotFilledWarning.ToString(),
                Value2 = ApplicationSettings.UnfilledDispatchCompletionAuthorisationCheck.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "RouteMustBeSetOnOrder",
                Value1 = ApplicationSettings.RouteMustBeSetOnOrder.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CheckForPartBoxesAtDispatch",
                Value1 = ApplicationSettings.CheckForPartBoxesAtDispatch.ToString(),
                Value2 = ApplicationSettings.CheckForOutstandingOrders.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CheckWeightBoundaries",
                Value1 = ApplicationSettings.CheckWeightBoundaryAtDispatch.ToString(),
                Value2 = ApplicationSettings.CheckWeightBoundaryAtCarcassDispatch.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DatabasePollingInterval",
                Value1 = ApplicationSettings.DatabasePollingInterval.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisplayData",
                Value1 = ApplicationSettings.DisplayIntakeData,
                Value2 = ApplicationSettings.DisplayProductionData,
                Value3 = ApplicationSettings.DisplayDispatchData,
                Value4 = ApplicationSettings.DisplayOutOfProductionData,
                Value5 = ApplicationSettings.DisplayWorkflowData,
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ScrollBarVerticalOffset",
                Value1 = ApplicationSettings.TouchScreenSuppliersVerticalOffset.ToString(),
                Value2 = ApplicationSettings.TouchScreenOrdersVerticalOffset.ToString(),
                Value3 = ApplicationSettings.TouchScreenProductsVerticalOffset.ToString(),
                Value4 = ApplicationSettings.TouchScreenProductionProductsVerticalOffset.ToString(),
                Value5 = ApplicationSettings.TouchScreenWorkflowSelectionVerticalOffset.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AutoWeigh",
                Value1 = ApplicationSettings.AutoWeightAtIntake.ToString(),
                Value2 = ApplicationSettings.AutoWeighAtOutOfProduction.ToString(),
                Value3 = ApplicationSettings.AutoWeightAtDispatch.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ResetSerialAttributesManually",
                Value1 = ApplicationSettings.ResetSerialAttributesManually.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "GetLiveOrderStatus",
                Value1 = ApplicationSettings.GetLiveOrderStatusIntake.ToString(),
                Value2 = ApplicationSettings.GetLiveOrderStatusDispatch.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DispatchMode",
                Value1 = ApplicationSettings.DispatchMode.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IntakeMode",
                Value1 = ApplicationSettings.IntakeMode.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IntoProductionMode",
                Value1 = ApplicationSettings.IntoProductionMode.ToString(),
                Value2 = ApplicationSettings.IntoProductionSelectProductScanAndWeighSeparately.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AllowAllProductsOutOfProduction",
                Value1 = ApplicationSettings.AllowAllProductsOutOfProduction.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "Pricing",
                Value1 = string.IsNullOrEmpty(ApplicationSettings.DefaultCurrency) ? "€" : ApplicationSettings.DefaultCurrency,

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AccountsPackage",
                Value1 = ApplicationSettings.AccountsPackage.ToString(),
                Value2 = ApplicationSettings.AccountsSalesPersonCode,
                Value3 = ApplicationSettings.AccountsDbPath,
                Value4 = ApplicationSettings.AccountsDbUserName,
                Value5 = ApplicationSettings.AccountsDbPassword,
                Value6 = ApplicationSettings.PrePostInvoice.ToString(),
                Value7 = ApplicationSettings.AccountsCustomer,
                Value8 = ApplicationSettings.AccountsFilePath,
                Value9 = ApplicationSettings.AccountsDirectory,
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AutoCopyToInvoice",
                Value1 = ApplicationSettings.AutoCopyToInvoice.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "UseDeliveryDateForInvoiceDocumentDate",
                Value1 = ApplicationSettings.UseDeliveryDateForInvoiceDocumentDate.ToString(),
                Value2 = ApplicationSettings.ShippingDateMustBeAfterDeliveryDate.ToString(),
                Value3 = ApplicationSettings.DefaultShippingDateToPreviousDay.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "WaitForScalesToStable",
                Value1 = ApplicationSettings.WaitForScalesToStable.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "MessageBoxFlashTime",
                Value1 = ApplicationSettings.MessageBoxFlashTime.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenStaticButtonFontSize",
                Value1 = ApplicationSettings.TouchscreenStaticButtonFontSize.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenStaticButtonFontSize",
                Value1 = ApplicationSettings.TouchscreenStaticButtonFontSize.ToString(),
                DeviceMasterID = deviceId
            });
           
            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenMenuButtonFontSize",
                Value1 = ApplicationSettings.TouchscreenMenuButtonFontSize.ToString(),
                Value2 = ApplicationSettings.DispatchBoxCountFontSize.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenMenuButtonWidth",
                Value1 = ApplicationSettings.TouchscreenMenuButtonWidth.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenMenuButtonHeight",
                Value1 = ApplicationSettings.TouchscreenMenuButtonHeight.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenCardsFontSize",
                Value1 = ApplicationSettings.TouchscreenCardsFontSize.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenTraceabilityControlsWidth",
                Value1 = ApplicationSettings.TouchscreenTraceabilityControlsWidth.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenTraceabilityControlsLabelWidth",
                Value1 = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenTraceabilityControlsHeight",
                Value1 = ApplicationSettings.TouchscreenTraceabilityControlsHeight.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenTraceabilityControlsFontSize",
                Value1 = ApplicationSettings.TouchscreenTraceabilityControlsFontSize.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchscreenTraceabilityControlsStackedNumber",
                Value1 = ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "PrinterSettings",
                Value1 = ApplicationSettings.PrinterCommunicationType,
                Value2 = ApplicationSettings.PrinterSettings,
                Value3 = ApplicationSettings.PrinterLabelOrientation,
                Value4 = ApplicationSettings.IsPrinterInUse.ToString(),
                Value5 = ApplicationSettings.UseDefaultPrinter.ToString(),
                Value6 = ApplicationSettings.PrinterDPI.ToString(),
                Value7 = ApplicationSettings.UseExternalLabels.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "Printer2Settings",
                Value1 = ApplicationSettings.Printer2CommunicationType,
                Value2 = ApplicationSettings.Printer2Settings,
                //Value3 = ApplicationSettings.PrinterLabelOrientation,
                Value4 = ApplicationSettings.IsPrinter2InUse.ToString(),
                //Value5 = ApplicationSettings.UseDefaultPrinter.ToString(),
                Value6 = ApplicationSettings.Printer2DPI.ToString(),
                Value7 = ApplicationSettings.UseExternalLabels2.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "Printer3Settings",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IndicatorSettings",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IndicatorSettings2",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IndicatorSettings3",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ConnectToSSRS",
                Value1 = ApplicationSettings.ConnectToSSRS.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KillLine",
                Value1 = ApplicationSettings.YoungBullMaxAge.ToString(),
                Value2 = ApplicationSettings.GraderLabelUOMAge.ToString(),
                Value3 = ApplicationSettings.MinSequencerEartagLength.ToString(),
                Value4 = ApplicationSettings.MinSequencerHerdNoLength.ToString(),
                Value5 = ApplicationSettings.CarcassSideWeightDifference.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KillLine2",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KillLine3",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KillLine4",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KillLine5",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KillLine6",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "UsingTareCalculator",
                Value1 = ApplicationSettings.UsingTareCalculator.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "PromptForReweighAtDispatch",
                Value1 = ApplicationSettings.PromptForReweighAtDispatch.ToString(),
                Value2 = ApplicationSettings.SelectingProductInReweighMode.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DefaultStockLocations",
                Value1 = ApplicationSettings.DefaultIntakeLocationId.ToString(),
                Value2 = ApplicationSettings.DefaultOutOfProductionLocationId.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "StartDispatchInReweighMode",
                Value1 = ApplicationSettings.StartDispatchInReweighMode.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ScannerMode",
                Value1 = ApplicationSettings.ScannerMode.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ResetSerialAttributes",
                Value1 = ApplicationSettings.ResetSerialAttributes.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "HandheldFontSize",
                Value1 = ApplicationSettings.HandheldFontSize.ToString(),
                Value2 = ApplicationSettings.HandheldGridFontSize.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CanSelectCustomersAtOutOfProduction",
                Value1 = ApplicationSettings.CanSelectCustomersAtOutOfProduction.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "LabelHistoryTransactionsNumberToRetrieve",
                Value1 = ApplicationSettings.LabelHistoryTransactionsNumberToRetrieve.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "GoodsInLabelsToPrint",
                Value1 = ApplicationSettings.GoodsInLabelsToPrint.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DispatchLabelsToPrint",
                Value1 = ApplicationSettings.DispatchLabelsToPrint.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "IntoProductionLabelsToPrint",
                Value1 = ApplicationSettings.IntoProductionLabelsToPrint.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "OutOfProductionLabelsToPrint",
                Value1 = ApplicationSettings.OutOfProductionLabelsToPrint.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchScreenModeOnly",
                Value1 = ApplicationSettings.TouchScreenModeOnly.ToString(),
                Value2 = ApplicationSettings.AllowTouchScreenResize.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchScreenModeOnlyIntake",
                Value1 = ApplicationSettings.TouchScreenModeOnlyIntake.ToString(),
                Value2 = ApplicationSettings.TouchScreenModeOnlySequencer.ToString(),
                Value3 = ApplicationSettings.TouchScreenModeOnlyGrader.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchScreenModeOnlyIntoProduction",
                Value1 = ApplicationSettings.TouchScreenModeOnlyIntoProduction.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchScreenModeOnlyOutOfProduction",
                Value1 = ApplicationSettings.TouchScreenModeOnlyOutOfProduction.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TouchScreenModeOnlyDispatch",
                Value1 = ApplicationSettings.TouchScreenModeOnlyDispatch.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ScanningForExternalBarcodeReadOnlyAtDispatch",
                Value1 = ApplicationSettings.ScanningForExternalBarcodeReadOnlyAtDispatch.ToString(),
                Value2 = ApplicationSettings.ScanningForExternalBarcodeReadOnlyAtIntoProduction.ToString(),
                Value3 = ApplicationSettings.ScanningForExternalBarcodeReadOnlyAtIntoProduction.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "MinWeightAllowed",
                Value1 = ApplicationSettings.MinWeightAllowedIntake.ToString(),
                Value2 = ApplicationSettings.MinWeightAllowedIntoProduction.ToString(),
                Value3 = ApplicationSettings.MinWeightAllowedOutOfProduction.ToString(),
                Value4 = ApplicationSettings.MinWeightAllowedDispatch.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AutoCopyToGoodsIn",
                Value1 = ApplicationSettings.AutoCopyToGoodsIn.ToString(),
                //Value3 = ApplicationSettings.UseDeliveryDateForIntakeReportSearch.ToString(),
                Value4 = ApplicationSettings.BatchAppendDDMM.ToString(),
                Value5 = ApplicationSettings.PrintBatchLabelOnItakeCompletion.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox",
                Value1 = ApplicationSettings.DisplayIntakeOrderItemUpdatedAtAnotherTerminalMsgBox.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox",
                Value1 = ApplicationSettings.DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "TransactionsToTakeForReportSearch",
                Value1 = ApplicationSettings.TransactionsToTakeForReportSearch.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ResetLabelOnEveryPrint",
                Value1 = ApplicationSettings.ResetLabelOnEveryPrint.ToString(),
                Value2 = ApplicationSettings.ResetLabelOnProductSelection.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "UseShippingDate",
                Value1 = ApplicationSettings.UseShippingDate.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AllowManualBatchCreation",
                Value1 = ApplicationSettings.AllowManualBatchCreation.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AllowIntakeOverAmount",
                Value1 = ApplicationSettings.AllowIntakeOverAmount.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AllowDispatchOverAmount",
                Value1 = ApplicationSettings.AllowDispatchOverAmount.ToString(),
                Value2 = ApplicationSettings.DispatchOverAmountHardStop.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ClearProductionOrdersOnEntry",
                Value1 = ApplicationSettings.ClearProductionOrdersOnEntry.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ClearPartnersOnEntry",
                Value1 = ApplicationSettings.ClearPartnersOnEntry.ToString(),
                Value2 = ApplicationSettings.FilterPartnerCountOnSearchScreen.ToString(),
                Value3 = ApplicationSettings.TouchscreenPartnerSearchByIncludes.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AllowDispatchProductNotOnOrder",
                Value1 = ApplicationSettings.AllowDispatchProductNotOnOrder.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "DisableSaleOrderDiscounts",
                Value1 = ApplicationSettings.DisableSaleOrderDiscounts.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ScannerSettings",
                Value1 = ApplicationSettings.ScannerSettings,
                Value2 = ApplicationSettings.ConnectToScanner.ToString(),
                Value3 = ApplicationSettings.ScannerPortDataWaitTime.ToString(),
                Value4 = ApplicationSettings.HandheldScannerDataWaitTime.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "WandSettings",
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ReportServerPath",
                Value1 = ApplicationSettings.ReportServerPath.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "RecallCustomerFromProductAtOutOfProduction",
                Value1 = ApplicationSettings.RecallCustomerFromProductAtOutOfProduction.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "QtyMustBeEnteredOnSaleOrderLine",
                Value1 = ApplicationSettings.QtyMustBeEnteredOnSaleOrderLine.ToString(),
                Value2 = ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarning.ToString(),
                Value3 = ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate.ToString(),
                Value4 = ApplicationSettings.AllowDuplicateProductOnOrder.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SaleOrderDeliveryDaysForward",
                Value1 = ApplicationSettings.SaleOrderDeliveryDaysForward.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SaleOrderValidUntilDaysForward",
                Value1 = ApplicationSettings.SaleOrderValidUntilDaysForward.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SSRSWebServiceURL",
                Value1 = ApplicationSettings.SSRSWebServiceURL.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SSRSUserName",
                Value1 = ApplicationSettings.SSRSUserName.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SSRSPassword",
                Value1 = ApplicationSettings.SSRSPassword.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SSRSDomain",
                Value1 = ApplicationSettings.SSRSDomain.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "WeightsPrecision",
                //Value1 = ApplicationSettings.WeightsPrecision.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CardViewCardWidth",
                Value1 = ApplicationSettings.CardViewCardWidth.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CardViewNarrowCardWidth",
                Value1 = ApplicationSettings.CardViewNarrowCardWidth.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CardViewCardHeight",
                Value1 = ApplicationSettings.CardViewCardHeight.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CardViewMenuCardHeight",
                Value1 = ApplicationSettings.CardViewMenuCardHeight.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CardViewMenuCardWidth",
                Value1 = ApplicationSettings.CardViewMenuCardWidth.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "CardViewNarrowCardHeight",
                Value1 = ApplicationSettings.CardViewNarrowCardHeight.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ManuallyEnterBatchNumber",
                Value1 = ApplicationSettings.ManuallyEnterBatchNumber.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "AlwaysShowButcheryBatch",
                Value1 = ApplicationSettings.AlwaysShowButcheryBatch.ToString(),

                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "BatchCreationMode",
                Value1 = ApplicationSettings.BatchCreationMode.ToString(),
                Value2 = ApplicationSettings.PrintQALabelAfterFirstLabelInBatch.ToString(),
                DeviceMasterID = deviceId
            });

            #endregion

            this.deviceRepository.UpdateDeviceSettings(settings, false);
            this.Log.LogInfo(this.GetType(), "Settings added");
            settings.Clear();

            #region add/update

            settings.Add(new DeviceSetting
            {
                Name = "AutoCreateInvoiceOnOrderCompletion",
                Value1 = ApplicationSettings.AutoCreateInvoiceOnOrderCompletion.ToString(),
                Value2= ApplicationSettings.NouProrderTypeID.ToString(),
                Value3 = ApplicationSettings.AutoUpdateRecipePrice.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ScannerEmulatorMode",
                Value1 = ApplicationSettings.ScannerEmulatorMode.ToString(),
                Value2 = ApplicationSettings.ScannerEmulatorHeight.ToString(),
                Value3 = ApplicationSettings.ScannerEmulatorWidth.ToString(),
                Value4 = ApplicationSettings.ScannerEmulatorTop.ToString(),
                Value5 = ApplicationSettings.ScannerEmulatorLeft.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SearchTypes",
                Value1 = ApplicationSettings.LairageKillInformationSearchType,
                Value2 = ApplicationSettings.BeefReportSearchType,
                Value3 = ApplicationSettings.IdentigenSearchType,
                Value4 = ApplicationSettings.KillDetailsSearchType,
                Value5 = ApplicationSettings.RPAReportSearchType,
                Value6 = ApplicationSettings.AnimalMovermentsSearchType,
                Value7 = ApplicationSettings.PaymentsSearchType,
                Value8 = ApplicationSettings.LairageIntakeSearchType,
                Value9 = ApplicationSettings.LairageReportSearchType,
                Value10 = ApplicationSettings.PaymentCreationSearchType,
                Value11 = ApplicationSettings.AnimalPricingSearchType,
                Value12 = ApplicationSettings.DispatchOrdersDateType,
                Value13 = ApplicationSettings.DispatchOrdersDateSort,
                DeviceMasterID = deviceId
            });

          
            settings.Add(new DeviceSetting
            {
                Name = "AutoCopyToDispatch",
                Value1 = ApplicationSettings.AlwaysCopyToDispatch ? "True" : ApplicationSettings.AutoCopyToDispatch.ToString(),
                Value2 = ApplicationSettings.AlwaysCopyToDispatch.ToString(),
                Value3 = ApplicationSettings.IgnoreCompleteDispatchCheck.ToString(),
                Value4 = ApplicationSettings.AutoCopyToLairage.ToString(),
                Value5 = ApplicationSettings.AlwaysCopyToGoodsIn.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SSRSEmailSettings",
                Value1 = ApplicationSettings.SSRSEmailType,
                Value2 = ApplicationSettings.SSRSEmailTo,
                Value3 = ApplicationSettings.SSRSEmailBody,
                Value4 = ApplicationSettings.SSRSEmailSubject,
                Value5 = ApplicationSettings.SSRSEmailExportFormat,
                Value6 = ApplicationSettings.SSRSZoom.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ShowAttributes",
                Value1 = ApplicationSettings.ShowIntakeAttributes.ToString(),
                Value2 = ApplicationSettings.ShowIntoProductionAttributes.ToString(),
                Value3 = ApplicationSettings.ShowOutOfProductionAttributes.ToString(),
                Value4 = ApplicationSettings.ShowDispatchAttributes.ToString(),
                Value5 = ApplicationSettings.ShowSequencerAttributes.ToString(),
                Value6 = ApplicationSettings.ShowGraderAttributes.ToString(),
                Value9 = ApplicationSettings.ShowLairageAttributes.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "LoginData",
                Value1 = ApplicationSettings.LoggedInUser,
                Value2 = ApplicationSettings.LoggedInUserName,
                Value3 = ApplicationSettings.LoggedInUserIdentifier,
                Value5 = ApplicationSettings.DBPassword,
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "PanelMode",
                Value1 = ApplicationSettings.OutOfProductionPanelMode.ToString(),
                Value2 = ApplicationSettings.DispatchPanelMode.ToString(),
                Value3 = ApplicationSettings.DisplayOldGrades.ToString(),
                Value4 = ApplicationSettings.CurrentIndicator.ToString(),
                Value5 = ApplicationSettings.StockMoveGrid,
                Value6 = ApplicationSettings.GradeSystem.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "KeyboardSearch",
                Value1 = ApplicationSettings.KeyboardSearchHeight.ToString(),
                Value2 = ApplicationSettings.KeyboardSearchWidth.ToString(),
                Value3 = ApplicationSettings.KeyboardSearchTop.ToString(),
                Value4 = ApplicationSettings.KeyboardSearchLeft.ToString(),
                Value5 = ApplicationSettings.TouchscreenKeyboardHeight.ToString(),
                Value6 = ApplicationSettings.TouchscreenKeyboardWidth.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SalesSearchScreenData",
                Value1 = ApplicationSettings.SalesSearchFromDate.ToString(),
                Value2 = ApplicationSettings.SalesSearchToDate.ToString(),
                Value3 = ApplicationSettings.SalesSearchShowAllOrders.ToString(),
                Value4 = ApplicationSettings.SalesSearchKeepVisible.ToString(),

                Value5 = ApplicationSettings.AnimalsSearchFromDate.ToString(),
                Value6 = ApplicationSettings.AnimalsSearchToDate.ToString(),
                Value7 = ApplicationSettings.AnimalsSearchShowAllOrders.ToString(),
                Value8 = ApplicationSettings.AnimalsSearchShowKeepVisible.ToString(),

                Value9 = ApplicationSettings.LairageSearchFromDate.ToString(),
                Value10 = ApplicationSettings.LairageSearchToDate.ToString(),
                Value11 = ApplicationSettings.LairageSearchShowAllOrders.ToString(),
                Value12 = ApplicationSettings.LairageSearchShowKeepVisible.ToString(),
                Value13 = ApplicationSettings.LairageSearchShowFAOnly.ToString(),

                Value14 = ApplicationSettings.FromPurchasesDate.ToString(),
                Value15 = ApplicationSettings.FromPurchasesShowAllOrders.ToString(),
                Value16 = ApplicationSettings.SalesSearchDateDispatch,
                Value17 = ApplicationSettings.SalesSearchDateSaleOrder,
                Value18 = ApplicationSettings.SalesSearchDateInvoice,
                Value20 = ApplicationSettings.ShowAllStock.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "SalesSearchScreenData2",
                Value1 = ApplicationSettings.PaymentProposalFromDate.ToString(),
                Value2 = ApplicationSettings.PaymentProposalShowAllOrders.ToString(),
                Value3 = ApplicationSettings.PaymentProposalKeepVisible.ToString(),
                Value4 = ApplicationSettings.PaymentProposalCreateFromDate.ToString(),
                Value5 = ApplicationSettings.PaymentProposalCreateShowAllOrders.ToString(),
                Value6 = ApplicationSettings.AlertsFromDate.ToString(),
                Value7 = ApplicationSettings.AlertsShowAllOrders.ToString(),
                Value8 = ApplicationSettings.AlertsKeepVisible.ToString(),
                Value9 = ApplicationSettings.WorkflowSearchFromDate.ToString(),
                Value10 = ApplicationSettings.WorkflowSearchShowAllOrders.ToString(),
                Value11 = ApplicationSettings.ShowAlertsForAllUsers.ToString(),
                Value12 = ApplicationSettings.BatchEditSearchFromDate.ToString(),
                Value13 = ApplicationSettings.BatchEditSearchToDate.ToString(),
                Value14 = ApplicationSettings.BatchEditSearchShowAllOrders.ToString(),
                Value15 = ApplicationSettings.ShowUnallocatedCustomersOnly.ToString(),
                Value16 = ApplicationSettings.BatchSetupShowAllOrders.ToString(),
                Value17 = ApplicationSettings.BatchSetUpFromDate.ToString(),
                Value18 = ApplicationSettings.FromCreditReturnsShowAllOrders.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "ReportSearchScreenData",
                Value1 = ApplicationSettings.ReportsSearchFromDate.ToString(),
                Value2 = ApplicationSettings.ReportsSearchToDate.ToString(),
                Value3 = ApplicationSettings.ReportSalesSearchShowAllOrders.ToString(),
                Value4 = ApplicationSettings.ReportSalesSearchDataKeepVisible.ToString(),
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "Bord Bia",
                Value1 = ApplicationSettings.BordBiaUserName,
                Value2 = ApplicationSettings.BordBiaPassword,
                Value3 = ApplicationSettings.BordBiaResponsePath,
            
                Value5 = ApplicationSettings.TrackingGroupGuid,
                DeviceMasterID = deviceId
            });

            settings.Add(new DeviceSetting
            {
                Name = "UpdateKillLine",
                Value1 = ApplicationSettings.SequencerMode.ToString(),
                Value2 = ApplicationSettings.GraderMode.ToString(),
                Value3 = ApplicationSettings.UsingBordBia.ToString(),
                Value4 = ApplicationSettings.UsingAims.ToString(),
                DeviceMasterID = deviceId
            });

            #endregion

            this.deviceRepository.UpdateDeviceSettings(settings, true);
            this.Log.LogInfo(this.GetType(), "Settings updated");
        }

        /// <summary>
        /// Sets the aplication updates..
        /// </summary>
        /// <returns>Flag, as to whether the update have been updated..</returns>
        public bool SetApplicationUpdates()
        {
            return this.deviceRepository.SetApplicationUpdates();
        }

        /// <summary>
        /// Adds or updates a device to the database.
        /// </summary>
        /// <param name="device">The device to add or update.</param>
        /// <returns>A flag, indicating a successful add/update or not.</returns>
        public bool AddOrUpdateDevice(DeviceMaster device, int? copyId)
        {
            if (device.DeviceID == 0)
            {
                return this.deviceRepository.AddDevice(device, copyId);
            }

            return this.deviceRepository.UpdateDevice(device, copyId);
        }

        /// <summary>
        /// Retrieve all the database devices.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        public IList<Device> GetDeviceSettings()
        {
            var localDevices = new List<Device>();
            var devices = this.deviceRepository.GetDeviceSettings();
            var groupedDevices = devices.GroupBy(x => x.DeviceMasterID);

            foreach (var device in groupedDevices)
            {
                var deviceId = device.Key;
                var deviceName = device.First().DeviceName;

                localDevices.Add(new Device
                {
                    DeviceID = deviceId.ToInt(),
                    Name = deviceName,
                    Settings = (from setting in device
                               select new Device
                               {
                                   DeviceID = setting.DeviceMasterID.ToInt(),
                                   DeviceSettingsID = setting.DeviceSettingsID,
                                   Name = setting.Name,
                                   Value1 = setting.Value1,
                                   Value2 = setting.Value2,
                                   Value3 = setting.Value3,
                                   Value4 = setting.Value4,
                                   Value5 = setting.Value5,
                                   Value6 = setting.Value6,
                                   Value7 = setting.Value7,
                                   Value8 = setting.Value8,
                                   Value9 = setting.Value9,
                                   Value10 = setting.Value10,
                                   Value11 = setting.Value11,
                                   Value12 = setting.Value12,
                                   Value13 = setting.Value13,
                                   Value14 = setting.Value14,
                                   Value15 = setting.Value15,
                                   Value16 = setting.Value16,
                                   Value17 = setting.Value17,
                                   Value18 = setting.Value18,
                                   Value19 = setting.Value19,
                                   Value20 = setting.Value20
                               }).ToList()
                });
            }

            return localDevices;
        }

        /// <summary>
        /// Adds or updates a device to the database.
        /// </summary>
        /// <param name="device">The device to add or update.</param>
        /// <returns>A flag, indicating a successful add/update or not.</returns>
        public bool AddOrUpdateDeviceWithLookups(DeviceMaster device, IList<int> lookups, int? copyId)
        {
            if (device.DeviceID == 0)
            {
                return this.deviceRepository.AddDeviceWithLookUp(device, lookups, copyId);
            }

            return this.deviceRepository.UpdateDeviceWithLookups(device, lookups, copyId);
        }

        /// <summary>
        /// Method that updates an existing device.
        /// </summary>
        /// <param name="device">The device to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDeviceWithLookups(DeviceMaster device, IList<int> departments, int? copyId)
        {
            return this.deviceRepository.UpdateDeviceWithLookups(device, departments, copyId);
        }

        /// <summary>
        /// Add a new device to the database.
        /// </summary>
        /// <param name="device">The device to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        public bool AddDeviceWithLookUp(DeviceMaster device, IList<int> departments, int? copyId)
        {
            return this.deviceRepository.AddDeviceWithLookUp(device, departments, copyId);
        }

        /// <summary>
        /// Retrieves any new entities that need to be added locally.
        /// </summary>
        /// <param name="deviceId">The local device.</param>
        /// <returns>A list of new entities that are to be added locally.</returns>
        public IList<EntityUpdate> CheckForNewEntities(int deviceId, string entityType = "")
        {
            return this.deviceRepository.CheckForNewEntities(deviceId, entityType);
        }

        /// <summary>
        /// Determines if a device name already exists in the db.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns>Flag, as to whether the device already exists.</returns>
        public bool IsDeviceNameInSystem(string name)
        {
            return this.deviceRepository.IsDeviceNameInSystem(name);
        }

        /// <summary>
        /// Determines if there are new price edits to apply.
        /// </summary>
        /// <returns>Flag, as to whether there are new edits to apply.</returns>
        public bool AreTherePriceEdits()
        {
            return this.deviceRepository.AreTherePriceEdits();
        }

        /// <summary>
        /// Retrieve all the database devices.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        public IList<DeviceMaster> GetDevices()
        {
            return this.deviceRepository.GetDevices();
        }

        #endregion

        #region document

        /// <summary>
        /// Returns the application document numbers.
        /// </summary>
        /// <returns>The application document names.</returns>
        public IList<DocNumber> GetDocumentNumbers()
        {
            return this.documentRepository.GetDocumentNumbers();
        }

        /// <summary>
        /// Returns the application document types.
        /// </summary>
        /// <returns>The application document types.</returns>
        public IList<DocumentNumberingType> GetDocumentNumberingTypes()
        {
            return this.documentRepository.GetDocumentNumberingTypes();
        }

        /// <summary>
        /// Returns the application document names.
        /// </summary>
        /// <returns>The application document names.</returns>
        public IList<NouDocumentName> GetNouDocumentNames()
        {
            return this.documentRepository.GetNouDocumentNames();
        }

        /// <summary>
        /// Add or updates the document types list.
        /// </summary>
        /// <param name="types">The document types to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateDocumentTypes(IList<DocumentNumberingType> types)
        {
            return this.documentRepository.AddOrUpdateDocumentTypes(types);
        }

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="numbers">The document numbers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateDocumentNumbers(IList<DocNumber> numbers)
        {
            return this.documentRepository.AddOrUpdateDocumentNumbers(numbers);
        }

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        public DocNumber SetDocumentNumber(int docNumberId)
        {
            return this.documentRepository.SetDocumentNumber(docNumberId);
        }

        #endregion

        #region Dynamic

        /// <summary>
        /// Retrieves the label data result row.
        /// </summary>
        public IList<string> GetSPData(string spName)
        {
            var values = new List<string>();
            var dt = this.dynamicRepository.GetSPData(spName);
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var data = row[column];
                    if (data != null)
                    {
                        values.Add(data.ToString());
                    }
                }
            }

            return values;
        }

        /// <summary>
        /// Retrieves the search screens macro data.
        /// </summary>
        /// <param name="name">The sp name.</param>
        /// <param name="start">The search start vaue.</param>
        /// <param name="end">The seach end value.</param>
        /// <param name="showAll">The show all in search flag.</param>
        /// <returns>Search scrren macro data.</returns>
        public Tuple<DataTable, string> GetSearchScreenMacroData(string name, DateTime start, DateTime end, bool showAll, string searchText,string searchDateType)
        {
            return this.dynamicRepository.GetSearchScreenMacroData(name, start, end, showAll, searchText, searchDateType);
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public Tuple<bool, string> GetSPResult(string spName, string value, int id)
        {
            return this.dynamicRepository.GetSPResult(spName, value, id);
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public Tuple<string, string, string> GetSPResultReturn(string spName, string value, int id, int? referenceId = null, int? productID = 0)
        {
            var processId = ViewModel.ViewModelLocator.ProcessSelectionStatic?.SelectedProcess?.ProcessID;
            return this.dynamicRepository.GetSPResultReturn(spName, value, id, referenceId, processId, productID);
        }

        /// <summary>
        /// Gets the table data.
        /// </summary>
        /// <param name="command">The sql query.</param>
        /// <returns>A collection of table data.</returns>
        public IList<object> GetData(string command)
        {
            return this.dynamicRepository.GetData(command);
        }

        /// <summary>
        /// Deletes a batch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteProductionStock(string serial, int id, int userId, int deviceId)
        {
            return this.dynamicRepository.DeleteProductionStock(serial, id, userId, deviceId);
        }

        /// <summary>
        /// Undeletes a stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The transactiontype id.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string UndeleteStock(int serial, int id, int userId, int deviceId, int orderId = 0)
        {
            return this.dynamicRepository.UndeleteStock(serial, id, userId, deviceId, orderId);
        }

        /// <summary>
        /// Deletes a batch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteOutOfProductionStock(int serial, int userId, int deviceId)
        {
            return this.dynamicRepository.DeleteOutOfProductionStock(serial, userId, deviceId);
        }

        /// <summary>
        /// Deletes a grader stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteGraderStock(int serial, int userId, int deviceId)
        {
            return this.dynamicRepository.DeleteGraderStock(serial, userId, deviceId);
        }

        /// <summary>
        /// Deletes a dispatch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteDispatchStock(int serial, int id, int userId, int deviceId, int warehouseId, int inmasterId = 0)
        {
            return this.dynamicRepository.DeleteDispatchStock(serial, id, userId, deviceId, warehouseId, inmasterId);
        }

        /// <summary>
        /// Deletes an intake stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteIntakeStock(int serial, int id, int userId, int deviceId)
        {
            return this.dynamicRepository.DeleteIntakeStock(serial, id, userId, deviceId);
        }

        #endregion

        #region invoice

        /// <summary>
        /// Adds a new invoice header object to the db.
        /// </summary>
        /// <param name="header">The invoice header to add.</param>
        /// <param name="partner">The business partner (if any).</param>
        /// <returns>The newly created invoice header id.</returns>
        public Tuple<int, int?> AddInvoiceHeader(InvoiceHeader header, BusinessPartner partner)
        {
            return this.invoiceRepository.AddInvoiceHeader(header, partner);
        }

        /// <summary>
        /// Adds a collection of invoice details objects to the db.
        /// </summary>
        /// <param name="details">The invoice header to add.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful add or not.</returns>
        public string AddInvoiceItems(IDictionary<InvoiceDetail, ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse)
        {
            return this.invoiceRepository.AddInvoiceItems(details, eposTransactionType, warehouse);
        }

        /// <summary>
        /// Removes stock from the db.
        /// </summary>
        /// <param name="details">The remove stock data.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful return or not.</returns>
        public bool ProcessGoodsReturned(IList<ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse)
        {
            return this.invoiceRepository.ProcessGoodsReturned(details, eposTransactionType, warehouse);
        }

        /// <summary>
        /// Adds a collection of goods received objects to the db.
        /// </summary>
        /// <param name="details">The intake data.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful add or not.</returns>
        public bool ProcessGoodsReceived(IList<ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse)
        {
            return this.invoiceRepository.ProcessGoodsReceived(details, eposTransactionType, warehouse);
        }

        /// <summary>
        /// Gets the epos sales for the selected date.
        /// </summary>
        /// <param name="saleDate">The date to retrieve sales for.</param>
        /// <param name="invoiceNo">The invoice number to search for.</param>
        /// <returns>The sales corresponding to the input date.</returns>
        public IList<EposSale> GetEposSales(DateTime? saleDate, int? invoiceNo)
        {
            if (invoiceNo.IsNullOrZero() && !saleDate.HasValue)
            {
                return new List<EposSale>();
            }


            return this.invoiceRepository.GetEposSales(saleDate, invoiceNo);
        }

        /// <summary>
        /// Gets the unpaid account epos sales for the selected date.
        /// </summary>
        /// <returns>The sales corresponding to the upaid accounts.</returns>
        public IList<EposSale> GetUnpaidEposSales()
        {
            return this.invoiceRepository.GetUnpaidEposSales();
        }

        /// <summary>
        /// Marks the unpaid account sales as paid.
        /// </summary>
        /// <param name="sales">The sales collection.</param>
        /// <param name="paymentTypeId">The account payment type id.</param>
        /// <returns>A flag, indicating a successful processing of account sales to me marked paid.</returns>
        public bool HandleAccountSalesPaid(IList<EposSale> sales, int paymentTypeId)
        {
            return this.invoiceRepository.HandleAccountSalesPaid(sales, paymentTypeId);
        }

        /// <summary>
        /// Cancels the input epos sale, putting the acorresponding stock back into the system.
        /// </summary>
        /// <param name="sale">The sale to cancel.</param>
        /// <param name="transactionType">The transaction type.</param>
        /// <returns>A flag, indicating a successful sale cancellation or not..</returns>
        public bool CancelEposSale(EposSale sale, NouTransactionType transactionType)
        {
            return this.invoiceRepository.CancelEposSale(sale, transactionType);
        }

        /// <summary>
        /// Gets the current stock items.
        /// </summary>
        /// <returns>A collection of stock items.</returns>
        public IList<ProductData> GetEposStock()
        {
            return this.invoiceRepository.GetEposStock();
        }

        /// <summary>
        /// Gets the system delivery methods.
        /// </summary>
        /// <returns>A collection of delivery methods.</returns>
        public IList<NouDeliveryMethod> GetDeliveryMethods()
        {
            return this.invoiceRepository.GetDeliveryMethods();
        }

        /// <summary>
        /// Gets the system payment types.
        /// </summary>
        /// <returns>A collection of payment types.</returns>
        public IList<NouPaymentType> GetPaymentTypes()
        {
            return this.invoiceRepository.GetPaymentTypes();
        }

        #endregion

        #region item master

        /// <summary>
        /// Method which returns the stock modes.
        /// </summary>
        /// <returns>A collection of stock modes.</returns>
        public IList<NouStockMode> GetStockModes()
        {
            return this.itemMasterRepository.GetStockModes();
        }

        /// <summary>
        /// Method which returns the inventory items.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        public IList<InventoryItem> GetInventoryItems()
        {
            return this.itemMasterRepository.GetInventoryItems();
        }

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        public IList<INAttachment> GetInventoryAttachments()
        {
            return this.itemMasterRepository.GetInventoryAttachments();
        }

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        public IList<INAttachment> GetInventoryAttachments(int inmasterId)
        {
            return this.itemMasterRepository.GetInventoryAttachments(inmasterId);
        }

        /// <summary>
        /// Method which updates the price m,ethods.
        /// </summary>
        /// <returns>Flag, as to successful update or not..</returns>
        public bool UpdateSearchItems(IList<InventoryItem> products)
        {
            return this.itemMasterRepository.UpdateSearchItems(products);
        }

        /// <summary>
        /// Method that retrieves the last 100 inventorys.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public IList<InventoryItem> GetRecentInventoryItems()
        {
            return this.itemMasterRepository.GetRecentInventoryItems();
        }

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        public GetLinkedProduct_Result2 GetLinkedProduct(string serial)
        {
            return this.itemMasterRepository.GetLinkedProduct(serial);
        }

        /// <summary>
        /// Method that retrieves the properties.
        /// </summary>
        /// <returns>A collection of properties.</returns>
        public IList<INProperty> GetINProperties()
        {
            return this.itemMasterRepository.GetINProperties();
        }

        /// <summary>
        /// Method which returns the non deleted carcass split products.
        /// </summary>
        /// <returns>A collection of non deleted carcass split products.</returns>
        public IList<CarcassSplitOption> GetCarcassSplitProducts()
        {
            return this.itemMasterRepository.GetCarcassSplitProducts();
        }

        /// <summary>
        /// Method which returns the inventory items.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        public void GetItemsStock(IList<InventoryItem> inIitems)
        {
            //this.itemMasterRepository.GetItemsStock(inIitems);
        }

        /// <summary>
        /// Method that retrieves the property selections.
        /// </summary>
        /// <returns>A collection of property selections.</returns>
        public IList<INPropertySelection> GetINPropertySelection()
        {
            return this.itemMasterRepository.GetINPropertySelections();
        }

        /// <summary>
        /// Updates an inventory groups templates.
        /// </summary>
        /// <param name="group">The inventory group to update.</param>
        /// <param name="parentGroup">The parent group who's templates are to be applied if the useParentGroup flag is set.</param>
        /// <param name="traceabilityTemplate">The traceability template to apply.</param>
        /// <returns>An update inventory group, or null if an issue.</returns>
        public INGroup UpdateInventoryGroup(INGroup group, INGroup parentGroup, AttributeTemplate traceabilityTemplate)
        {
            if (parentGroup != null)
            {
                group.ParentInGroupID = parentGroup.INGroupID;
            }

            if (traceabilityTemplate != null && traceabilityTemplate.AttributeTemplateID > 0)
            {
                group.AttributeTemplateID = traceabilityTemplate.AttributeTemplateID;
            }
            else
            {
                group.TraceabilityTemplateNameID = null;
            }

            return this.itemMasterRepository.AddOrUpdateInventoryGroup(group);
        }

        /// <summary>
        /// Add or updates the groups list.
        /// </summary>
        /// <param name="groups">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateInventoryGroups(IList<INGroup> groups)
        {
            return this.itemMasterRepository.AddOrUpdateInventoryGroups(groups);
        }

        /// <summary>
        /// Method which returns the non deleted inventory groups.
        /// </summary>
        /// <returns>A collection of non deleted inventory groups.</returns>
        public IList<INGroup> GetInventoryGroups()
        {
            return this.itemMasterRepository.GetInventoryGroups();
        }

        /// <summary>
        /// Method which returns the non deleted item master properties.
        /// </summary>
        /// <returns>A collection of non deleted item master properties.</returns>
        public IList<INProperty> GetItemMasterProperties()
        {
            return this.itemMasterRepository.GetItemMasterProperties();
        }

        /// <summary>
        /// Method which returns the non deleted item master properties.
        /// </summary>
        /// <returns>A collection of non deleted inventory properties.</returns>
        /// <returns>The properties selected flag.</returns>
        public IList<InventoryProperty> GetInventoryProperties(bool isSelected)
        {
            var properties = new List<InventoryProperty>();
            this.itemMasterRepository.GetItemMasterProperties().ToList().ForEach(x => properties.Add(new InventoryProperty { Details = x, IsSelected = isSelected }));

            return properties;
        }

        /// <summary>
        /// Add or updates the properties list.
        /// </summary>
        /// <param name="properties">The properties to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateProperties(IList<INProperty> properties)
        {
            return this.itemMasterRepository.AddOrUpdateProperties(properties);
        }

        /// <summary>
        /// Gets the product data between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        public IList<App_ReportProductData_Result> GetProductData(int customerId, DateTime start, DateTime end)
        {
            return this.itemMasterRepository.GetProductData(customerId, start, end);
        }

        /// <summary>
        /// Method which returns the non deleted vat codes.
        /// </summary>
        /// <returns>A collection of non deleted vat codes.</returns>
        public IList<VATCode> GetVATCodes()
        {
            return this.itemMasterRepository.GetVATCodes();
        }

        /// <summary>
        /// Method which returns the non deleted vat codes.
        /// </summary>
        /// <returns>A collection of non deleted vat codes.</returns>
        public IList<Vat> GetVAT()
        {
            return this.itemMasterRepository.GetVat();
        }

        /// <summary>
        /// Add or updates the vat codes list.
        /// </summary>
        /// <param name="vatCodes">The vat codes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateVatCodes(IList<VATCode> vatCodes)
        {
            return this.itemMasterRepository.AddOrUpdateVatCodes(vatCodes);
        }

        /// <summary>
        /// Add or updates the vat codes list.
        /// </summary>
        /// <param name="vatCodes">The vat codes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateVatCodes(IList<Vat> vatCodes)
        {
            return this.itemMasterRepository.AddOrUpdateVat(vatCodes);
        }

        /// <summary>
        /// Method which returns the non deleted departments.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        public IList<Department> GetDepartments()
        {
            return this.itemMasterRepository.GetDepartments();
        }

        /// <summary>
        /// Method which returns the inventory item nproduct data.
        /// </summary>
        /// <returns>A collection of inventory epos items data.</returns>
        public IList<ProductData> GetEposItems()
        {
            return this.itemMasterRepository.GetEposItems();
        }

        /// <summary>
        /// Method which returns the transaction types.
        /// </summary>
        /// <returns>A collection of transaction types.</returns>
        public IList<NouTransactionType> GetTransactionTypes()
        {
            return this.itemMasterRepository.GetTransactionTypes();
        }

        /// <summary>
        /// Determines if there are any transactions recorded against the input product.
        /// </summary>
        /// <returns>Flag, as to whether there are transactions recorded against the input product.</returns>
        public bool AreTransactionsRecordedAgainstProduct(int productId)
        {
            return this.itemMasterRepository.AreTransactionsRecordedAgainstProduct(productId);
        }

        #endregion

        #region kill line

        #region lairage

        /// <summary>
        /// Retrieves all the lairage goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllLairageIntakes(IList<int?> orderStatuses, DateTime start, DateTime end, string searchTerm)
        {
            return this.lairageRepository.GetAllLairageIntakes(orderStatuses, start, end, searchTerm);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input id search term.
        /// </summary>
        /// <param name="apReceiptId">The order serch id.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetLairageIntakeById(int apReceiptId, bool includeMovements = true)
        {
            return this.lairageRepository.GetLairageIntakeById(apReceiptId, includeMovements);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the device last edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetLairageIntakeByLastEdit()
        {
            return this.lairageRepository.GetLairageIntakeByLastEdit();
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            return this.lairageRepository.GetKilledAnimalDetailsByStatus(start, end, status);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledAnimalDetailsByPrice(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetAllKilledAnimalDetailsByPrice(start, end);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledAnimalDetailsByPriceUnsent(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetAllKilledAnimalDetailsByPriceUnsent(start, end);
        }

        /// <summary> Adds a new lairage order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag indicating a successful order add, or not.</returns>
        public bool AddLairageOrder(Sale sale)
        {
            return this.lairageRepository.AddLairageOrder(sale);
        }

        /// <summary> Adds a new lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public int MoveLairageIntake(Sale sale)
        {
            return this.lairageRepository.MoveLairageIntake(sale);
        }

        /// <summary>
        /// Adds aims data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Flag, as to successfull add or not.</returns>
        public bool AddAIMsData(AimsData data)
        {
            return this.lairageRepository.AddAIMsData(data);
        }

        /// <summary>
        /// Updaes the whole carcass weights.
        /// </summary>
        /// <param name="carcassNo">The carcass number of the animal to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateCarcassWeight(int carcassNo, string killType)
        {
            return this.lairageRepository.UpdateCarcassWeight(carcassNo, killType);
        }

        /// <summary>
        /// Logs the aims data.
        /// </summary>
        /// <param name="data">The data to log.</param>
        /// <returns>Flag, as to successful logging or not.</returns>
        public bool LogAimsData(AimsData data)
        {
            return this.lairageRepository.LogAimsData(data);
        }

        /// <summary>
        /// Gets an intake supplier id.
        /// </summary>
        /// <param name="id">The transaction id.</param>
        /// <returns>An intake supplier id.</returns>
        public int? GetSupplierId(int id)
        {
            return this.lairageRepository.GetSupplierId(id);
        }

        /// <summary> Adds a new lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddLairageIntake(Sale sale)
        {
            return this.lairageRepository.AddLairageIntake(sale);
        }

        /// <summary>
        /// Updates a lairage lot queue no.
        /// </summary>
        /// <param name="lot">The lot to update.</param>
        /// <param name="queueNo">The queue no to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateLotQueue(IList<Sale> lots)
        {
            return this.lairageRepository.UpdateLotQueue(lots);
        }

        /// <summary>
        /// Removes a lairage intake item.
        /// </summary>
        /// <param name="attributeId">The attribute id</param>
        /// <returns>Flag, indicating a successful removal or not.</returns>
        public DateTime? GetAnimalSequenceDate(int attributeId)
        {
            return this.lairageRepository.GetAnimalSequenceDate(attributeId);
        }

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        public int GetMappedProductCategory(NouCategory category)
        {
            if (category != null)
            {
                return this.GetKillProductByCategory(category);
            }

            return 0;
        }

        /// <summary>
        /// Gets the lairage associated payment status.
        /// </summary>
        /// <param name="intakeId">The intake id.</param>
        /// <returns>The lairage associated payment status.</returns>
        public int? GetLairagePaymentStatus(int intakeId)
        {
            return this.lairageRepository.GetLairagePaymentStatus(intakeId);
        }

        /// <summary>
        /// Set the intake processed falg.
        /// </summary>
        /// <param name="intakeId">The intake id.</param>
        /// <param name="isProcessed">The processed flag.</param>
        /// <returns>Flag, st to successful update or not.</returns>
        public bool SetIntakeProcessing(int intakeId, bool isProcessed)
        {
            return this.lairageRepository.SetIntakeProcessing(intakeId, isProcessed);
        }

        /// <summary>
        /// Adds a new grader pig carcass.
        /// </summary>
        /// <param name="transId">The previous pig carcass to copy.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddNewPigCarcass(int transId)
        {
            return this.lairageRepository.AddNewPigCarcass(transId);
        }

        /// <summary>
        /// Adds a new grader sheep carcass.
        /// </summary>
        /// <param name="transId">The previous pig carcass to copy.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddNewSheepCarcass(int masterTableId)
        {
            return this.lairageRepository.AddNewSheepCarcass(masterTableId);
        }

        /// <summary>
        /// Updates the qa status of the input batch.
        /// </summary>
        /// <param name="details">The input batch of animals.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateFarmAssured(IList<StockDetail> details)
        {
            return this.lairageRepository.UpdateFarmAssured(details);
        }

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        public bool UpdateKillAttribute(IList<StockDetail> details)
        {
            return this.lairageRepository.UpdateKillAttributes(details);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcassForEdit(int carcassNo)
        {
            return this.lairageRepository.GetCarcassForEdit(carcassNo);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the device last edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetLairageIntakeByFirstLast(bool first)
        {
            return this.lairageRepository.GetLairageIntakeByFirstLast(first);
        }

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        public Tuple<bool, int> UpdateLairageIntake(Sale sale)
        {
            return this.lairageRepository.UpdateLairageIntake(sale);
        }

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        public bool UpdateLairageIntakeSupplier(Sale sale, int id)
        {
            return this.lairageRepository.UpdateLairageIntakeSupplier(sale, id);
        }

        /// <summary>
        /// Updates the lairage animal pricing/rpa.
        /// </summary>
        /// <param name="details">The details to update.</param>
        /// <returns>A flag, as to whether the updates were succcessful.</returns>
        public bool UpdateLairagePrices(IList<StockDetail> details)
        {
            return this.lairageRepository.UpdateLairagePrices(details);
        }

        /// <summary> Updates a lairage order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag indicating a successful order update, or not.</returns>
        public bool UpdateLairageOrder(Sale sale)
        {
            return this.lairageRepository.UpdateLairageOrder(sale);
        }
      
        /// <summary>
        /// Marks the exported identigen records as being exported.
        /// </summary>
        /// <param name="details">The records to export.</param>
        /// <returns>Flag, as to success export or not.</returns>
        public bool ExportCarcasses(IList<StockDetail> details, bool useExport1)
        {
            return this.lairageRepository.ExportCarcasses(details, useExport1);
        }

        /// <summary>
        /// Gets the lairage animal details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetLairageAnimalDetails(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetLairageAnimalDetails(start, end);
        }

        /// <summary>
        /// Gets all the killed carcass side details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllCarcassSides(DateTime start, DateTime end, string searchType)
        {
            return this.lairageRepository.GetAllCarcassSides(start, end, searchType);
        }

        /// <summary>
        /// Marks the exported identigen records as being exported.
        /// </summary>
        /// <param name="details">The records to export.</param>
        /// <returns>Flag, as to success export or not.</returns>
        public bool ChangeCarcassStatus(IList<StockDetail> details, int status)
        {
            return this.lairageRepository.ChangeCarcassStatus(details, status);
        }

        /// <summary>
        /// Gets all the lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllLairageAnimalDetails(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetAllLairageAnimalDetails(start, end);
        }

        /// <summary>
        /// Gets all the RPA carcasses to be sent.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllRPAAnimalDetails(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetAllRPAAnimalDetails(start, end);
        }

        /// <summary>
        /// Gets all the RPA carcasses to be sent.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetRPAAnimalDetails(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetRPAAnimalDetails(start, end);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllBeefKilledAnimalDetailsByStatus(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetAllBeefKilledAnimalDetailsByStatus(start, end);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetIdentigenKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            return this.lairageRepository.GetIdentigenKilledAnimalDetailsByStatus(start, end, status);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetBeefKilledAnimalDetailsByStatus(DateTime start, DateTime end)
        {
            return this.lairageRepository.GetBeefKilledAnimalDetailsByStatus(start, end);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllIdentigenKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            return this.lairageRepository.GetAllIdentigenKilledAnimalDetailsByStatus(start, end, status);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status)
        {
            return this.lairageRepository.GetAllKilledAnimalDetailsByStatus(start, end, status);
        }

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        public IList<StockDetail> GetAllKilledLairageAnimalDetails(DateTime start, DateTime end, string searchType)
        {
            return this.lairageRepository.GetAllKilledLairageAnimalDetails(start, end, searchType);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetLairageOrderByLastEdit()
        {
            return this.lairageRepository.GetLairageOrderByLastEdit();
        }

        /// <summary>
        /// Removes a lairage intake item.
        /// </summary>
        /// <param name="attributeId">The attribute id</param>
        /// <returns>Flag, indicating a successful removal or not.</returns>
        public string RemoveLairageIntakeItem(int attributeId)
        {
            return this.lairageRepository.RemoveLairageIntakeItem(attributeId);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetLairageOrderByFirstLast(bool first)
        {
            return this.lairageRepository.GetLairageOrderByFirstLast(first);
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="partnerId">The partner id to search orders for.</param>
        /// <returns>A collection of all orders for the input partner.</returns>
        public IList<Sale> GetLairageOrdersBySupplier(int partnerId)
        {
            return this.lairageRepository.GetLairageOrdersBySupplier(partnerId);
        }

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        public bool UpdateLairageIntakeStatus(Sale sale)
        {
            return this.lairageRepository.UpdateLairageIntakeStatus(sale);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetLairageOrderByID(int id)
        {
            return this.lairageRepository.GetLairageOrderByID(id);
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllLairageOrdersByDate(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            return this.lairageRepository.GetAllLairageOrdersByDate(orderStatuses, start, end);
        }

        /// <summary>
        /// Retrieves the kill line kill types.
        /// </summary>
        /// <returns>The kill line kill types.</returns>
        public IList<NouKillType> GetKillTypes()
        {
            return this.lairageRepository.GetKillTypes();
        }

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        public IList<SupplierHerd> GetSupplierHerds(int supplierId)
        {
            return this.lairageRepository.GetSupplierHerds(supplierId);
        }

        /// <summary>
        /// Determines if an eartag is in the system.
        /// </summary>
        /// <param name="eartag">The eartag to check.</param>
        /// <returns>Flag, as to whether an eartag is in the system.</returns>
        public bool IsSheepEartagInSystem(string eartag, int lotNo)
        {
            return this.lairageRepository.IsSheepEartagInSystem(eartag, lotNo);
        }

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        public IList<SupplierHerd> GetSupplierHerds()
        {
            return this.lairageRepository.GetSupplierHerds();
        }

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        public IList<SupplierHerd> GetSupplierPartnerHerds(int partnerId)
        {
            return this.lairageRepository.GetSupplierPartnerHerds(partnerId);
        }

        /// <summary>
        /// Determines if an eartag is in the system.
        /// </summary>
        /// <param name="eartag">The eartag to check.</param>
        /// <returns>Flag, as to whether an eartag is in the system.</returns>
        public int IsEartagInSystem(string eartag)
        {
            return this.lairageRepository.IsEartagInSystem(eartag);
        }

        #endregion

        #region sequencer

        /// <summary>
        /// Retrieves the daily sequenced, ungraded carcasses.
        /// </summary>
        /// <returns>A collection of daily sequenced, ungraded carcasses..</returns>
        public IList<StockDetail> GetCurrentSequencedStock()
        {
            return this.lairageRepository.GetCurrentSequencedStock();
        }

        /// <summary>
        /// Adds a stock attribute.
        /// </summary>
        /// <param name="stock">The attrribute to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddStockAttribute(StocktransactionDetail stock)
        {
            return this.lairageRepository.AddStockAttribute(stock);
        }

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        public int GetNextKillNumberSheep(bool isSequencer = true)
        {
            return this.lairageRepository.GetNextKillNumberSheep(isSequencer);
        }

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        public int GetNextKillNumberPig(bool isSequencer = true)
        {
            return this.lairageRepository.GetNextKillNumberPig(isSequencer);
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        public int UpdateKillNumberSheep()
        {
            return this.lairageRepository.UpdateKillNumberSheep();
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        public int UpdateKillNumberPig()
        {
            return this.lairageRepository.UpdateKillNumberPig();
        }

        /// <summary>
        /// Retrieves the stock detail corresponsing to the entered eartag.
        /// </summary>
        /// <param name="eartag">The eartag to search for.</param>
        /// <returns>A stock detail.</returns>
        public StockDetail GetLairageIntakeStockByEartag(string eartag, string killNo = "")
        {
            return this.lairageRepository.GetLairageIntakeStockByEartag(eartag, killNo);
        }

        /// <summary>
        /// Retrieves the stock detail corresponsing to the entered eartag.
        /// </summary>
        /// <param name="eartag">The eartag to search for.</param>
        /// <returns>A stock detail.</returns>
        public StockDetail VerifyEartag(string eartag)
        {
            return this.lairageRepository.VerifyEartag(eartag);
        }

        /// <summary>
        /// Edits a carcss.
        /// </summary>
        /// <param name="stocktransactionId">The stock id.</param>
        /// <param name="carcassNo">The animal carcass no.</param>
        /// <param name="side1Wgt">It's side1 wgt.</param>
        /// <param name="side2Wgt">It's side2 wgt.</param>
        /// <param name="grade">The animal grade.</param>
        /// <param name="gradingDate">The kill date.</param>
        /// <param name="categoryId">The category.</param>
        /// <param name="farmAssured">The qa status.</param>
        /// <param name="rpa">The rpa status.</param>
        /// <param name="generic2">Generic 2 value.</param>
        /// <param name="killType">The kill type (beef,sheep,pig).</param>
        /// <param name="deviceId">The device id.</param>
        /// <param name="userId">The user editing.</param>
        /// <returns>Flag, as to successful edit or not.</returns>
        public bool EditCarcass(int deviceId, int userId, int? stocktransactionId = null, int? carcassNo = null,
            decimal? side1Wgt = null, decimal? side2Wgt = null, string grade = "", DateTime? gradingDate = null,
            int? categoryId = null, bool? farmAssured = null, bool? rpa = null, string generic2 = "",
            string killType = "")
        {
            return this.lairageRepository.EditCarcass(deviceId, userId, stocktransactionId, carcassNo,
                side1Wgt, side2Wgt, grade, gradingDate, categoryId, farmAssured, rpa, generic2, killType);
        }

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The next kill number.</returns>
        public int GetNextKillNumber(bool isSequencer = true)
        {
            return this.lairageRepository.GetNextKillNumber(isSequencer);
        }

        /// <summary>
        /// Undos a carcass sequenced, reverting the numbers back 1.
        /// </summary>
        /// <returns>A string, indicating an error or not.</returns>
        public string UndoSequencedCarcass(StockDetail detail, int docNumberId)
        {
            return this.lairageRepository.UndoSequencedCarcass(detail, docNumberId);
        }

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        public int UpdateKillNumber()
        {
            return this.lairageRepository.UpdateKillNumber();
        }

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        public bool UpdateStockAttribute(StockDetail detail)
        {
            if (detail.Category != null)
            {
                var productId = this.GetKillProductByCategory(detail.Category);
                if (productId > 0)
                {
                    detail.ProductID = productId;
                }
            }

            return this.lairageRepository.UpdateStockAttribute(detail);
        }

        /// <summary>
        /// Gets the age bands.
        /// </summary>
        /// <returns>The age bands.</returns>
        public IList<AgeBand> GetAgeBands()
        {
            return this.lairageRepository.GetAgeBands();
        }

        /// <summary>
        /// Re-order the carcasses at the sequencer.
        /// </summary>
        /// <param name="carcassToMoveUp">The carcass to move up.</param>
        /// <param name="carcassToMoveDown">The carcass to move down.</param>
        /// <param name="docResetNo">The carcass document reset number.</param>
        /// <returns>A flag to indicate that the re-ordering was successful.</returns>
        public bool ReOrderCarcasses(StockDetail carcassToMoveUp, StockDetail carcassToMoveDown, int docResetNo)
        {
            return this.lairageRepository.ReOrderCarcasses(carcassToMoveUp, carcassToMoveDown, docResetNo);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="transId">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcassById(int transId)
        {
            return this.lairageRepository.GetCarcassById(transId);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="transId">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcassByStockId(int transId)
        {
            return this.lairageRepository.GetCarcassByStockId(transId);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextCarcassByIntakeId(int id)
        {
            return this.lairageRepository.GetNextCarcassByIntakeId(id);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextCarcassWithEartagsByIntakeId(int id)
        {
            return this.lairageRepository.GetNextCarcassWithEartagsByIntakeId(id);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextPigCarcassByIntakeId(int id)
        {
            return this.lairageRepository.GetNextPigCarcassByIntakeId(id);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetNextSheepCarcassByIntakeId(int id)
        {
            return this.lairageRepository.GetNextSheepCarcassByIntakeId(id);
        }

        #endregion

        #region grader

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <returns>A carcass stock record.</returns>
        public IList<StockDetail> GetTodaysCarcasses(string killType)
        {
            return this.lairageRepository.GetTodaysCarcasses(killType);
        }

        /// <summary>
        /// Adds a grader carcass side weighing.
        /// </summary>
        /// <param name="detail">The carcass to add.</param>
        /// <returns>Flag, as to a successful add or not.</returns>
        public bool AddGraderTransaction(StockDetail detail)
        {
            return this.lairageRepository.AddGraderTransaction(detail);
        }

        /// <summary>
        /// Changes the transaction weight.
        /// </summary>
        /// <param name="trans">The transaction to change.</param>
        /// <returns>Flag, as to successfull change or not.</returns>
        public Tuple<bool,KillType,string,string> ChangeTransactionWeight(StockTransaction trans)
        {
            return this.lairageRepository.ChangeTransactionWeight(trans);
        }

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        public StockDetail GetCarcass(int carcassNo, string mode)
        {
            return this.lairageRepository.GetCarcass(carcassNo, mode);
        }

        /// <summary>
        /// Gets the lot kill data.
        /// </summary>
        /// <param name="lotNo">The header record id.</param>
        /// <returns>Kill lot data (killdate, or null if not yet killed).</returns>
        public IList<DateTime?> GetLotCount(int lotNo)
        {
            return this.lairageRepository.GetLotCount(lotNo);
        }

        /// <summary>
        /// Updates a sheep record with the grading data.
        /// </summary>
        /// <param name="detail">The carcass to update.</param>
        /// <returns>Flag, as to a successful updateor not.</returns>
        public bool UpdateGraderSheepTransaction(StockDetail detail)
        {
            return this.lairageRepository.UpdateGraderSheepTransaction(detail);
        }

        /// <summary>
        /// Updates a pig record with the grading data.
        /// </summary>
        /// <param name="detail">The carcass to update.</param>
        /// <returns>Flag, as to a successful update or not.</returns>
        public bool UpdateGraderPigTransaction(StockDetail detail)
        {
            return this.lairageRepository.UpdateGraderPigTransaction(detail);
        }

        /// <summary>
        /// Adds a grader carcass side weighing.
        /// </summary>
        /// <param name="detail">The carcass to add.</param>
        /// <returns>Flag, as to a successful add or not.</returns>
        public bool EditGraderTransaction(StockDetail detail)
        {
            return this.lairageRepository.EditGraderTransaction(detail);
        }

        /// <summary>
        /// Gets the stock serial id associated with the input barcode and side.
        /// </summary>
        /// <param name="data">The scanned data to parse.</param>
        /// <returns>A stock serial id associated with the input barcode and side</returns>
        public string GetCarcassTransaction(string data)
        {
            var carcassNo = data.Substring(6, 6).ToInt();
            var side = data.Substring(12, 1);
            var carcassSide = side.CompareIgnoringCase("A") ? 1 : 2;
            return this.lairageRepository.GetCarcassTransaction(carcassNo, carcassSide).ToString();
        }

        /// <summary>
        /// Gets the next carcass number at the grader.
        /// </summary>
        /// <param name="carcassResetNo">The reset value.</param>
        /// <returns>The next carcass number at the grader.</returns>
        public int GetLastcarcassNumber(int nextCarcassNo, string killType)
        {
            return this.lairageRepository.GetLastcarcassNumber(nextCarcassNo, killType);
        }

        /// <summary>
        /// Removes the identigen bottle number.
        /// </summary>
        /// <param name="identigen">The number to serach for.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool MinusScanIdentigen(string identigen)
        {
            return this.lairageRepository.MinusScanIdentigen(identigen);
        }

        /// <summary>
        /// Determines if the identigen number has already been assigned..
        /// </summary>
        /// <param name="identigen">The number to serach for.</param>
        /// <returns>Carcass number if already assigned.</returns>
        public int HasIdentigenBeenAssigned(string identigen)
        {
            return this.lairageRepository.HasIdentigenBeenAssigned(identigen);
        }

        #endregion

        #endregion

        #region label

        /// <summary>
        /// Retrieves the label associations.
        /// </summary>
        /// <returns>A collection of label associations.</returns>
        public IList<LabelsAssociation> GetLabelAssociations()
        {
            return this.labelRepository.GetLabelAssociations();
        }

        /// <summary>
        /// Method that makes a repository call to add or update the label associations.
        /// </summary>
        /// <param name="labelAssociations">The label associations to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateLabelAssociations(IList<LabelsAssociation> labelAssociations)
        {
            return this.labelRepository.AddOrUpdateLabelAssociations(labelAssociations);
        }

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<BarcodeParse> GetBarcodeParses()
        {
            return this.labelRepository.GetBarcodeParses();
        }

        /// <summary>
        /// Gets the application printers.
        /// </summary>
        /// <returns>The application printers.</returns>
        public IList<Printer> GetPrinters()
        {
            return this.labelRepository.GetPrinters();
        }

        /// <summary>
        /// Method that makes a repository add or update the input special prices.
        /// </summary>
        /// <param name="specialPrices">The label associations to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateSpecialPrices(IList<SpecialPrice> specialPrices)
        {
            return this.pricingRepository.AddOrUpdateSpecialPrices(specialPrices);
        }

        ///// <summary>
        ///// Have the prices been edited flag.
        ///// </summary>
        ///// <returns>A flag as to whether a price has been edited.</returns>
        //public bool AreTherePriceEdits()
        //{
        //    return this.pricingRepository.AreTherePriceEdits();
        //}

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <returns>An intake label data table row.</returns>
        public DataTable GetIntakeLabelData(int stockTransactionId)
        {
            return this.labelRepository.GetIntakeLabelData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetIntoProductionLabelData(int stockTransactionId)
        {
            return this.labelRepository.GetIntoProductionLabelData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetStockMovementData(int stockTransactionId)
        {
            return this.labelRepository.GetStockMovementData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletLabelData(int stockTransactionId)
        {
            return this.labelRepository.GetPalletLabelData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the out of production data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <returns>An out of production label data table row.</returns>
        public DataTable GetOutOfProductionLabelData(int stockTransactionId)
        {
            return this.labelRepository.GetOutOfProductionLabelData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the dispatch data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <returns>An out of production label data table row.</returns>
        public DataTable GetDispatchLabelData(int stockTransactionId)
        {
            return this.labelRepository.GetDispatchLabelData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetGraderLabelData(int stockTransactionId)
        {
            return this.labelRepository.GetGraderLabelData(stockTransactionId);
        }

        /// <summary>
        /// Retrieves the sequencer data schema.
        /// </summary>
        /// <param name="atributeId">The stock transaction id to pass in.</param>
        public DataTable GetSequencerLabelData(int attributeId)
        {
            return this.labelRepository.GetSequencerLabelData(attributeId);
        }

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<Label> GetLabels()
        {
            return this.labelRepository.GetLabels();
        }

        /// <summary>
        /// Retrieve all the label look ups.
        /// </summary>
        /// <returns>A collection of label look ups.</returns>
        public IList<ExternalLabelDataLookUp> GetExternalLabelDataLookUps()
        {
            return this.labelRepository.GetExternalLabelDataLookUps();
        }

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        public IList<Gs1AIMaster> GetGs1Master()
        {
            return this.labelRepository.GetGs1Master();
        }

        /// <summary>
        /// Method that makes a repository call to add or update a gs1 master.
        /// </summary>
        /// <param name="gs1Masters">The gs1 masters to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateGs1Masters(IList<Gs1AIMaster> gs1Masters)
        {
            return labelRepository.AddOrUpdateGs1Masters(gs1Masters);
        }

        #endregion

        #region license

        /// <summary>
        /// Stores an imported licence details.
        /// </summary>
        /// <param name="details">The licence details to store.</param>
        /// <returns>The newly stored licence details.</returns>
        public IList<LicenceDetail> StoreLicenceDetails(Licencing.LicenseDetail details)
        {
            return this.licenseRepository.StoreLicenceDetails(details);
        }

        /// <summary>
        /// Assigns a licences to a group of licensee.
        /// </summary>
        /// <param name="selectedLicenses">The licensees, and thier associated licence detail to allocate.</param>
        /// <returns>A flag, indication a successful transaction or not.</returns>
        public bool AssignLicenses(IDictionary<Licensee, LicenceDetail> selectedLicenses)
        {
            return this.licenseRepository.UpgradeLicenses(selectedLicenses);
        }

        /// <summary>
        /// Method that assigns a license type to a user/device.
        /// </summary>
        /// <param name="selectedLicensee">The selected licensee.</param>
        /// <param name="selectedLicenseDetails">The licence type to allocate</param>
        /// <returns>A string, indicating a successful allocation or not.</returns>
        public string AssignLicense(Licensee selectedLicensee, LicenseDetails selectedLicenseDetails)
        {
            if (selectedLicenseDetails.Details.Quantity <= 0)
            {
                return Message.NoRemainingLicenses;
            }

            if (!this.licenseRepository.AssignLicense(selectedLicensee, selectedLicenseDetails))
            {
                return Message.DatabaseError;
            }

            return string.Empty;
        }

        /// <summary>
        /// Makes a call to deallocate the collection of licences.
        /// </summary>
        /// <param name="deallocations">The licencess to deallocate licences from.</param>
        public bool DeallocateLicences(IList<Licensee> deallocations)
        {
            return this.licenseRepository.DeallocateLicences(deallocations);
        }

        /// <summary>
        /// Retrieve all the database license totals by license type.
        /// </summary>
        /// <returns>A collection of license totals by type.</returns>
        public IList<LicenseDetails> GetLicenseTotals()
        {
            var details = this.licenseRepository.GetLicenseTotals();
            var licenseDetails = (from detail in details
                                  select new LicenseDetails { Details = detail, IsSelected = false })
                                  .ToList();

            return licenseDetails;
        }

        /// <summary>
        /// Retrieve all the database license details.
        /// </summary>
        /// <returns>A collection of license details.</returns>
        public IList<ViewLicenseDetail> GetLicenseDetails()
        {
            var details = this.licenseRepository.GetLicenseDetails();
            var licenseNumbers = new List<string>();

            // remove any duplicate license numbers, for ui display purposes.
            details.ToList().ForEach(x =>
            {
                if (licenseNumbers.Contains(x.LicenceNumber))
                {
                    x.LicenceNumber = string.Empty;
                }
                else
                {
                    licenseNumbers.Add(x.LicenceNumber);
                }
            });

            return details;
        }

        /// <summary>
        /// Checks for a licence deallocation while in session
        /// </summary>
        /// <returns>A flag, indicating a licence deallocation or not.</returns>
        public bool CheckForLicenceDeallocation()
        {
            var currentLicences = this.GetLicensees();
            var loggedInUser = NouvemGlobal.Licensee;

            if (loggedInUser != null)
            {
                var currentUser = currentLicences.FirstOrDefault(x => x.Id == loggedInUser.Id);

                if (currentUser != null)
                {
                    NouvemGlobal.Licensee = currentUser;
                    return currentUser.LicenceStatus != LicenceStatus.Valid;
                }
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the database devices, including their associated license type name.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        public IList<Licensee> GetLicensees()
        {
            var licensees = this.licenseRepository.GetLicensees();

            licensees.ToList().ForEach(licensee =>
            {
                if (licensee.LicenceStatus == LicenceStatus.Expired)
                {
                    // expired licence, so for display purposes note this.
                    licensee.LicenseType = new NouLicenceType { LicenceName = Strings.Expired };
                }
                else if (licensee.LicenceStatus == LicenceStatus.NoLicence)
                {
                    // no licence
                    licensee.LicenseType = new NouLicenceType { LicenceName = Strings.NoLicence };
                }
            });

            return licensees;
        }

        #endregion

        #region payments

        /// <summary>
        /// Adds or updates weight bands.
        /// </summary>
        /// <param name="bands">The bands to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateWeightBands(IList<WeightBandData> bands)
        {
            return this.paymentsRepository.AddOrUpdateWeightBands(bands);
        }

        /// <summary>
        /// Updates the input payments.
        /// </summary>
        /// <param name="payments">The payments to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePaymentsOnly(IList<Sale> payments)
        {
            return this.paymentsRepository.UpdatePaymentsOnly(payments);
        }

        /// <summary>
        /// Retrieves all the patment proposals (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetPaymentProposals(bool showAll, DateTime start, DateTime end)
        {
            return this.paymentsRepository.GetPaymentProposals(showAll, start, end);
        }

        /// <summary>
        /// Updates the input payments.
        /// </summary>
        /// <param name="payments">The payments to update.</param>
        /// <param name="status">The status to update the payments to.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePaymentsStatus(IList<Sale> payments, int status)
        {
            return this.paymentsRepository.UpdatePaymentsStatus(payments, status);
        }

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the input id search term.
        /// </summary>
        /// <param name="paymentId">The order search id.</param>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        public Sale GetPaymentById(int paymentId)
        {
            return this.paymentsRepository.GetPaymentById(paymentId);
        }

        /// <summary>
        /// Exports purchase invoice details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportPurchaseInvoices(IList<int> invoiceIds)
        {
            return this.paymentsRepository.ExportPurchaseInvoices(invoiceIds, ApplicationSettings.UseLairagesForPurchaseAccounts);
        }

        /// <summary>
        /// Updates a payment.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>Flag, as to successfull update or not.</returns>
        public bool UpdatePayment(Sale payment)
        {
            if (payment.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
            {
                if (payment.PaymentDeductionItems != null)
                {
                    foreach (var paymentDeductionItem in payment.PaymentDeductionItems)
                    {
                        paymentDeductionItem.Deleted = DateTime.Now;
                    }
                }

                if (payment.StockDetails != null)
                {
                    foreach (var stockDetail in payment.StockDetails)
                    {
                        stockDetail.Deleted = DateTime.Now;
                    }
                }
            }
            else
            {
                if (payment.PaymentDeductionItems != null)
                {
                    foreach (var paymentDeductionItem in payment.PaymentDeductionItems)
                    {
                        paymentDeductionItem.Deleted = null;
                    }
                }

                if (payment.StockDetails != null)
                {
                    foreach (var stockDetail in payment.StockDetails)
                    {
                        stockDetail.Deleted = null;
                    }
                }
            }

            return this.paymentsRepository.UpdatePayment(payment);
        }

        /// <summary>
        /// Updates the grades and categories of the input carcasses.
        /// </summary>
        /// <param name="details">The carcasses to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateCarcasses(IList<StockDetail> details)
        {
            return this.paymentsRepository.UpdateCarcasses(details);
        }

        /// <summary>
        /// Updates a payment, adding a cheque number.
        /// </summary>
        /// <param name="paymentId">The cheque number.</param>
        /// <returns>A payment cheque number.</returns>
        public int UpdatePaymentChequeNumber(int paymentId)
        {
            return this.paymentsRepository.UpdatePaymentChequeNumber(paymentId);
        }

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the last edit.
        /// </summary>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        public Sale GetPaymentByFirstLast(bool first)
        {
            return this.paymentsRepository.GetPaymentByFirstLast(first);
        }

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the last edit.
        /// </summary>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        public Sale GetPaymentByLastEdit()
        {
            return this.paymentsRepository.GetPaymentByLastEdit();
        }

        /// <summary>
        /// Gets the application payment apply methods.
        /// </summary>
        /// <returns>The application payment apply methods.</returns>
        public IList<NouApplyMethod> GetApplyMethods()
        {
            return this.paymentsRepository.GetApplyMethods();
        }

        /// <summary>
        /// Update the payements pricing matrix.
        /// </summary>
        /// <param name="pricing">The matrix prices.</param>
        public bool UpdatePricingMatrix(StockDetail pricing)
        {
            return this.paymentsRepository.UpdatePricingMatrix(pricing);
        }

        /// <summary>
        /// Returns the pricing.
        /// </summary>
        /// <returns></returns>
        public IList<PricingMatrix> GetPricingMatrix()
        {
            return this.paymentsRepository.GetPricingMatrix();
        }

        /// <summary>
        /// Gets a payments base intake ids.
        /// </summary>
        /// <param name="paymentid">The payment ids.</param>
        /// <returns>The base intake ids.</returns>
        public IList<int> GetPaymentConsolidatedIntakes(int paymentid)
        {
            return this.paymentsRepository.GetPaymentConsolidatedIntakes(paymentid);
        }

        /// <summary>
        /// Marks a payment as printed..
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        public bool MarkPaymentAsPrinted(int paymentId)
        {
            return this.paymentsRepository.MarkPaymentAsPrinted(paymentId);
        }

        /// <summary>
        /// Checks if a payment as printed..
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        public bool IsPaymentPrinted(int paymentId)
        {
            return this.paymentsRepository.IsPaymentPrinted(paymentId);
        }

        /// <summary>
        /// Adds or updates weight band groups.
        /// </summary>
        /// <param name="bands">The bands to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateWeightBandGroups(IList<WeightBandGroup> bands)
        {
            return this.paymentsRepository.AddOrUpdateWeightBandGroups(bands);
        }

         /// <summary>
        /// Gets the application weight bands.
        /// </summary>
        /// <returns>The application bands.</returns>
        public IList<WeightBandData> GetWeightBands()
        {
            return this.paymentsRepository.GetWeightBands();
        }

        /// <summary>
        /// Gets the application weight band groups.
        /// </summary>
        /// <returns>The application band groups.</returns>
        public IList<WeightBandGroup> GetWeightBandGroups()
        {
            return this.paymentsRepository.GetWeightBandGroups();
        }

        /// <summary>
        /// Gets the payment deductions.
        /// </summary>
        /// <returns>The application payment deductions.</returns>
        public IList<PaymentDeduction> GetPaymentDeductions()
        {
            return this.paymentsRepository.GetPaymentDeductions();
        }

        /// <summary>
        /// Adds or updates payment deductions.
        /// </summary>
        /// <returns>Flag, indicating successfull add or update.</returns>
        public bool AddOrUpdatePaymentDeductions(IList<PaymentDeduction> deductions)
        {
            return this.paymentsRepository.AddOrUpdatePaymentDeductions(deductions);
        }

        /// <summary>
        /// Cancels a payment.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>Flag, as to successfull cancellation or not.</returns>
        public bool CancelPayment(Sale payment)
        {
            return this.paymentsRepository.CancelPayment(payment);
        }

        /// <summary>
        /// Adds a new payment proposal (order and details).
        /// </summary>
        /// <param name="sale">The proposal details.</param>
        /// <returns>The newly created quote id, indicating a successful payment proposal add, or not.</returns>
        public Tuple<int, DocNumber> AddNewPaymentProposal(Sale sale)
        {
            return this.paymentsRepository.AddNewPaymentProposal(sale);
        }

        /// <summary>
        /// Removes a carcass from a payment.
        /// </summary>
        /// <param name="stocktransactionId">The carcass transaction id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool RemoveCarcassFromPayment(int stocktransactionId)
        {
            return this.paymentsRepository.RemoveCarcassFromPayment(stocktransactionId);
        }

        /// <summary>
        /// Removes a carcass from a payment.
        /// </summary>
        /// <param name="itemId">The carcass transaction id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool RemovePaymentDeductionItemFromPayment(int itemId)
        {
            return this.paymentsRepository.RemovePaymentDeductionItemFromPayment(itemId);
        }

        /// <summary>
        /// Marks the input payments as to be excluded from the pruchase accounts.
        /// </summary>
        /// <param name="payments">The input payments.</param>
        /// <returns>Flag, as to successful marking or not.</returns>
        public bool ExcludePaymentsFromAccounts(IList<Sale> payments)
        {
            return this.paymentsRepository.ExcludePaymentsFromAccounts(payments);
        }

        #endregion

        #region plant

        /// <summary>
        /// Method which returns a warehouse and it's sub warehouses.
        /// </summary>
        /// <returns>A warehouse.</returns>
        public Location GetStockWarehouse(int warehouseId)
        {
            return this.plantRepository.GetStockWarehouse(warehouseId);
        }

        /// <summary>
        /// Method which returns the warehouse location matching the input id.
        /// </summary>
        /// <returns>A warehouse sub location.</returns>
        public WarehouseLocation GetSubLocation(int id)
        {
            return this.plantRepository.GetSubLocation(id);
        }

        /// <summary>
        /// Method which returns the warehouses and sub warehouses.
        /// </summary>
        /// <returns>A collection of non deleted warehouses.</returns>
        public IList<Location> GetStockWarehouses()
        {
            return this.plantRepository.GetStockWarehouses();
        }

        /// <summary>
        /// Retrieve all the cplants.
        /// </summary>
        /// <returns>A collection of plants.</returns>
        public IList<Model.DataLayer.Plant> GetAllPlants()
        {
            return this.plantRepository.GetAllPlants();
        }

        /// <summary>
        /// Retrieve all the cplants.
        /// </summary>
        /// <returns>A collection of plants.</returns>
        public IList<ViewPlant> GetPlants()
        {
            return this.plantRepository.GetPlants();
        }

        /// <summary>
        /// Add or updates the plants.
        /// </summary>
        /// <param name="plants">The plants to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePlants(IList<ViewPlant> plants)
        {
            return this.plantRepository.AddOrUpdatePlants(plants);
        }

        /// <summary>
        /// Method which returns the warehouse locations.
        /// </summary>
        /// <returns>A collection of non deleted warehouse locations.</returns>
        public IList<Warehouse> GetWarehouses()
        {
            return this.plantRepository.GetWarehouses();
        }

        /// <summary>
        /// Add or updates the warehouses list.
        /// </summary>
        /// <param name="warehouses">The warehouses to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateWarehouses(IList<Warehouse> warehouses)
        {
            var localWarehouses = warehouses.Where(x => !string.IsNullOrEmpty(x.Name)).ToList();
            return this.plantRepository.AddOrUpdateWarehouses(localWarehouses);
        }

        /// <summary>
        /// Deleted the input warehouse.
        /// </summary>
        /// <param name="warehouse">The warehouse to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool RemoveWarehouse(Warehouse warehouse)
        {
            return this.plantRepository.RemoveWarehouse(warehouse);
        }

        /// <summary>
        /// Method which returns the warehouse locations.
        /// </summary>
        /// <returns>A collection of non deleted warehouse locations.</returns>
        public IList<WarehouseLocation> GetWarehouseLocations()
        {
            return this.plantRepository.GetWarehouseLocations();
        }

        /// <summary>
        /// Add or updates the warehouses list.
        /// </summary>
        /// <param name="locations">The warehouses to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateWarehouseLocations(IList<WarehouseLocation> locations)
        {
            return this.plantRepository.AddOrUpdateWarehouseLocations(locations);
        }

        /// <summary>
        /// Deleted the input warehouse location.
        /// </summary>
        /// <param name="location">The warehouse location to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool RemoveWarehouseLocation(WarehouseLocation location)
        {
            return this.plantRepository.RemoveWarehouseLocation(location);
        }

        #endregion

        #region pricing

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetPriceLists()
        {
            return this.pricingRepository.GetPriceLists();
        }

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public bool GetUpdatedPriceListDetails()
        {
            return this.AreTherePriceEdits();
        }

        /// <summary>
        /// Retrieves the price detail for the input data.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price detail.</returns>
        public PriceDetail GetPriceListDetail(int priceListId, int inMasterId)
        {
            return this.pricingRepository.GetPriceListDetail(priceListId, inMasterId);
        }

        /// <summary>
        /// Retrieve the first price detail for the input product.
        /// </summary>
        /// <returns>A corresponding price detail.</returns>
        public PriceDetail GetFirstProductPriceListDetail(int inMasterId)
        {
            return this.pricingRepository.GetFirstProductPriceListDetail(inMasterId);
        }

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public PriceMaster GetPriceList(int priceListId)
        {
            return this.pricingRepository.GetPriceList(priceListId);
        }

        /// <summary>
        /// Adds or updates the price lists.
        /// </summary>
        /// <param name="priceList">The priceList to add.</param>
        /// <returns>A flag, indicating a successful add.</returns>
        public PriceList AddPriceList(PriceList priceList)
        {
            if (!priceList.BasePriceListID.HasValue)
            {
                priceList.BasePriceListID = NouvemGlobal.BasePriceList.BasePriceListID;
            }

            return this.pricingRepository.AddPriceList(priceList);
        }

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceList> GetAllPriceLists()
        {
            return this.pricingRepository.GetAllPriceLists();
        }

        /// <summary>
        /// Retrieve all the special prices.
        /// </summary>
        /// <returns>A collection of special prices.</returns>
        public IList<SpecialPrice> GetSpecialPrices()
        {
            return this.pricingRepository.GetSpecialPrices();
        }

        /// <summary>
        /// Retrieve all the rounding options.
        /// </summary>
        /// <returns>A collection of rounding options.</returns>
        public IList<NouRoundingOption> GetRoundingOptions()
        {
            return this.pricingRepository.GetRoundingOptions();
        }

        /// <summary>
        /// Updates a price.
        /// </summary>
        /// <param name="price">The price to update to.</param>
        /// <param name="priceListId">The price book id.</param>
        /// <param name="inmasterId">The product id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePrice(decimal price, int priceListId, int inmasterId)
        {
            return this.pricingRepository.UpdatePrice(price, priceListId, inmasterId);
        }

        /// <summary>
        /// Determines if a product is on a price book.
        /// </summary>
        /// <param name="priceListId">The price book to check.</param>
        /// <param name="inmasterId">The product to check.</param>
        /// <returns>Flag, as to whether a product is on a price book.</returns>
        public bool IsProductOnPriceBook(int priceListId, int inmasterId)
        {
            return this.pricingRepository.IsProductOnPriceBook(priceListId, inmasterId);
        }

        /// <summary>
        /// Adds a new collection of partners to the db.
        /// </summary>
        /// <param name="partners">The partners to add.</param>
        public void AddOrUpdatePrices(IList<Nouvem.AccountsIntegration.BusinessObject.Prices> prices)
        {
             this.pricingRepository.AddOrUpdatePrices(prices);
        }

        /// <summary>
        /// Retrieve all the price list customer groups.
        /// </summary>
        /// <returns>A collection of price list customer groups.</returns>
        public IList<App_GetPriceListCustomerGroups_Result> GetPriceListCustomerGroups()
        {
            return this.pricingRepository.GetPriceListCustomerGroups();
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public PriceDetail GetPriceListDetail(int priceListDetailId)
        {
            return this.pricingRepository.GetPriceListDetail(priceListDetailId);
        }

        /// <summary>
        /// Copies a price lists details to another price list.
        /// </summary>
        /// <param name="copyFrom">The price list id we are copying from.</param>
        /// <param name="copyTo">The price list id we are copying to.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        public bool CopyPriceListDetails(int copyFrom, int copyTo, int deviceId, int userId)
        {
            return this.pricingRepository.CopyPriceListDetails(copyFrom, copyTo, deviceId, userId);
        }

        /// <summary>
        /// Retrieves the price detail for the input data.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price detail.</returns>
        public PriceDetail GetPriceListDetailByPartner(int partnerId, int inMasterId, int basePriceListId)
        {
            return this.pricingRepository.GetPriceListDetailByPartner(partnerId, inMasterId, basePriceListId);
        }

        /// <summary>
        /// Gets the price from a price list detail record.
        /// </summary>
        /// <param name="priceListId">The price list detail.</param>
        /// <returns>A corresponding price.</returns>
        public decimal? GetPartnerPrice(int priceListId, int inmasterId)
        {
            return this.pricingRepository.GetPartnerPrice(priceListId, inmasterId);
        }

        /// <summary>
        /// Retrieves the price method for the input data.
        /// </summary>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price method.</returns>
        public NouPriceMethod GetProductPriceMethod(int inMasterId)
        {
            return this.pricingRepository.GetProductPriceMethod(inMasterId);
        }

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetPriceListsAndDetails()
        {
            return this.pricingRepository.GetPriceListsAndDetails();
        }

        /// <summary>
        /// Retrieve all the rounding rules.
        /// </summary>
        /// <returns>A collection of rounding rules.</returns>
        public IList<NouRoundingRule> GetRoundingRules()
        {
            return this.pricingRepository.GetRoundingRules();
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public IList<PriceDetail> GetUpdatedPriceListDetails(int priceListId)
        {
            return this.pricingRepository.GetUpdatedPriceListDetails(priceListId);
        }

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public void GetNewPriceLists()
        {
            foreach (var newPriceList in this.pricingRepository.GetNewPriceLists())
            {
                var exists = NouvemGlobal.PriceListMasters.Any(x => x.PriceListID == newPriceList.PriceListID);
                if (!exists)
                {
                    NouvemGlobal.PriceListMasters.Add(newPriceList);
                }
            }
        }

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        public IList<PriceMaster> GetUpdatedPriceLists()
        {
            return this.pricingRepository.GetUpdatedPriceLists();
        }

        /// <summary>
        /// Adds or updates the price lists.
        /// </summary>
        /// <param name="priceLists">The priceLists to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePriceMasters(IList<PriceMaster> priceLists)
        {
            foreach (var priceMaster in priceLists)
            {
                if (!priceMaster.BasePriceListID.HasValue)
                {
                    priceMaster.BasePriceListID = NouvemGlobal.BasePriceList.BasePriceListID;
                }
            }

            return this.pricingRepository.AddOrUpdatePriceMasters(priceLists);
        }

        /// <summary>
        /// Adds or updates the price list details.
        /// </summary>
        /// <param name="priceListDetails">The priceList details to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePriceListDetails(IList<PriceDetail> priceListDetails, int progressAmt)
        {
            return this.pricingRepository.AddOrUpdatePriceListDetails(priceListDetails.Where(x => x != null).ToList(), progressAmt);
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public IList<PriceDetail> GetPriceListDetails()
        {
            return this.pricingRepository.GetPriceListDetails();
        }

        /// <summary>
        /// Retrieve all the order methods.
        /// </summary>
        /// <returns>A collection of order methods.</returns>
        public IList<NouOrderMethod> GetOrderMethods()
        {
            return this.pricingRepository.GetOrderMethods();
        }

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        public IList<NouPriceMethod> GetPriceMethods()
        {
            return this.pricingRepository.GetPriceMethods();
        }

        /// <summary>
        /// Retrieve all the inventory master products.
        /// </summary>
        /// <returns>A collection of inventory master products.</returns>
        public IList<PriceDetail> GetProducts()
        {
            return this.pricingRepository.GetProducts();
        }

        /// <summary>
        /// Gets the grouped products.
        /// </summary>
        /// <returns>A collection of grouped products.</returns>
        public IList<ProductSelection> GetGroupedProducts()
        {
            return this.pricingRepository.GetGroupedProducts();
        }

        /// <summary>
        /// Adds the products to the price list.
        /// </summary>
        /// <param name="products">The products to add.</param>
        /// <returns></returns>
        public bool AddProductsToPriceList(IList<ProductSelection> products)
        {
            return true;
        }

        /// <summary>
        /// Gets the price from a price list detail record.
        /// </summary>
        /// <param name="priceListDetailId">The price list detail.</param>
        /// <returns>A corresponding price.</returns>
        public decimal? GetPrice(int priceListDetailId)
        {
            return this.pricingRepository.GetPrice(priceListDetailId);
        }

        /// <summary>
        /// Updates a price book with the input price..
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="price">The new price.</param>
        /// <param name="productId">The product id.</param>
        /// <returns>A flag, indicating a successful change or not.</returns>
        public Tuple<bool, PriceDetail> UpdatePrice(int priceListId, int partnerPriceListId, decimal price, int productId)
        {
            return this.pricingRepository.UpdatePrice(priceListId, partnerPriceListId, price, productId);
        }

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        public IList<PriceDetail> GetPriceListDetails(int priceListId)
        {
            return this.pricingRepository.GetPriceListDetails(priceListId);
        }

        #endregion

        #region production

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetBatchOrderData(ProductionData order)
        {
            return this.productionRepository.GetBatchOrderData(order);
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetBatchOrderDataPreDispatch(ProductionData order)
        {
            return this.productionRepository.GetBatchOrderDataPreDispatch(order);
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public int? GetProductionOrderProduct(int id)
        {
            return this.productionRepository.GetProductionOrderProduct(id);
        }

        /// <summary>
        /// Gets the pr order.
        /// </summary>
        /// <param name="id">The order id.</param>
        /// <returns>A pr order.</returns>
        public PROrder GetPROrder(int id)
        {
            return this.productionRepository.GetPROrder(id);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public ProductionData GetPROrderByReference(string reference)
        {
            return this.productionRepository.GetPROrderByReference(reference);
        }

        /// <summary>
        /// Completes a Recipe mix.
        /// </summary>
        /// <param name="order">The batch/Recipe data.</param>
        /// <returns>Flag, as to successful completion or not.</returns>
        public bool CompleteMix(ProductionData order)
        {
            return this.productionRepository.CompleteMix(order);
        }

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        /// <returns>The application issue methods.</returns>
        public IList<NouIssueMethod> GetNouIssueMethods()
        {
            return this.productionRepository.GetNouIssueMethods();
        }

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        /// <returns>The application issue methods.</returns>
        public IList<UOMMaster> GetNouUOMs()
        {
            return this.productionRepository.GetNouUOMs();
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionOrdersByLocation(int locationId)
        {
            return this.productionRepository.GetProductionOrdersByLocation(locationId);
        }

        /// <summary>
        /// Returns the production orders data for the reports.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<Sale> GetProductionOrdersForReport(DateTime start, DateTime end)
        {
            return this.productionRepository.GetProductionOrdersForReport(start, end);
        }

        /// <summary>
        /// Returns the production orders data for the reports.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<Sale> GetProductionOrdersForReportByOrderId(DateTime fromDate, DateTime toDate)
        {
            return this.productionRepository.GetProductionOrdersForReportByOrderId(fromDate, toDate);
        }

        /// <summary>
        /// Gets the oldest kill date from a production batch.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <param name="batchId">The batch id.</param>
        /// <returns>The oldest kill date from a production batch, or null if none exists.</returns>
        public DateTime? GetProductionBatchOldestKillDate(int prOrderId, int batchId)
        {
            if (ApplicationSettings.CheckForLinkedDatabaseScannedDispatchSerial)
            {
                return this.productionRepository.GetProductionBatchOldestKillDateExternal(prOrderId);
            }

            return this.productionRepository.GetProductionBatchOldestKillDate(prOrderId, batchId);
        }

        /// <summary>
        /// Returns the production order for the associated batch.
        /// </summary>
        /// <returns>The associated production order.</returns>
        public ProductionData GetProductionOrderForBatch(int batchId)
        {
            return this.productionRepository.GetProductionOrderForBatch(batchId);
        }

        /// <summary>
        /// Gets the external production batch data.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <returns>The production batch data.</returns>
        public Tuple<string, string, string, string> GetBatchTraceData(int prOrderId)
        {
            this.Log.LogInfo(this.GetType(), String.Format("Getting batch trace data for ProrderID:{0}", prOrderId));
            var bornIn = "IRE";
            var rearedIn = "IRE";
            var slaughteredIn = ApplicationSettings.PlantCode;
            var cutIn = ApplicationSettings.PlantCode;
            var data = this.productionRepository.GetBatchTraceData(prOrderId);
            if (data != null)
            {
                if (!string.IsNullOrEmpty(data.Item1))
                {
                    var dbBornIn = data.Item1;
                    if (dbBornIn.ContainsIgnoringCase("Ire"))
                    {
                        bornIn = "IRE";
                    }
                    else 
                    {
                        bornIn = dbBornIn.Trim();
                    }
                }

                if (!string.IsNullOrEmpty(data.Item2))
                {
                    var dbRearedIn = data.Item2;
                    if (dbRearedIn.ContainsIgnoringCase("Ire"))
                    {
                        rearedIn = "IRE";
                    }
                    else 
                    {
                        rearedIn = dbRearedIn.Trim();
                    }
                }
            }

            return Tuple.Create(bornIn, rearedIn, slaughteredIn, cutIn);
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetStockMovementTransactionData(int orderId)
        {
            return this.productionRepository.GetStockMovementTransactionData(orderId);
        }

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="inmasterid">The product to check.</param>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        public bool HasCarcassSplitStockBeenScanned(int inmasterid, string serial)
        {
            return this.productionRepository.HasCarcassSplitStockBeenScanned(inmasterid, serial);
        }

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        public bool UpdateProductionOrder(PROrder order)
        {
            return this.productionRepository.UpdateProductionOrder(order);
        }

        /// <summary>
        /// Retrieves all the production batches.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllBatches(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            return this.productionRepository.GetAllBatches(orderStatuses, start, end);
        }

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionMultiBatchOrders(IList<ProductionData> data = null, int? inMasterId = null, bool productionLocationOnly = true)
        {
            return this.productionRepository.GetProductionMultiBatchOrders(data, inMasterId, productionLocationOnly);
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionOrders(IList<ProductionData> data = null, bool multiBatch = false, int? inmasterId = null)
        {
            if (multiBatch)
            {
                return this.productionRepository.GetProductionMultiBatchOrders(data, inmasterId);
            }

            var orders = this.productionRepository.GetProductionOrders(data);
            if (ApplicationSettings.FilterProductionOrdersByDepartment)
            {
                var departments = this.GetDeviceDepartmentIds(NouvemGlobal.DeviceId.ToInt());
                if (departments.Any())
                {
                    orders = orders.Where(x => departments.Contains(x.DepartmentID.ToInt())).ToList();
                }
            }

            return orders;
        }

        /// <summary>
        /// Reworks recipe stock into the current batch.
        /// </summary>
        /// <param name="stockTransactionId">The rework stock id.</param>
        /// <param name="prOrderId">The batch id.</param>
        /// <param name="deviceid">The device id.</param>
        /// <param name="userid">The user id.</param>
        /// <returns>Rework message.</returns>
        public string ReworkRecipeStock(int stockTransactionId, int prOrderId, int deviceid, int userid)
        {
            return this.productionRepository.ReworkRecipeStock(stockTransactionId, prOrderId, deviceid, userid);
        }

        /// <summary>
        /// Gets the recipes.
        /// </summary>
        /// <returns></returns>
        public IList<Sale> GetRecipes()
        {
            return this.productionRepository.GetRecipes();
        }

        /// <summary>
        /// Checks if a product is a recipe product.
        /// </summary>
        /// <param name="inmasterid"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool IsProductRecipeProduct(int inmasterid, int orderId)
        {
            return this.productionRepository.IsProductRecipeProduct(inmasterid, orderId);
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public SaleDetail GetProductionOrderTransactionDataForProduct(int orderId, int inMasterId, int productionStage)
        {
            return this.productionRepository.GetProductionOrderTransactionDataForProduct(orderId, inMasterId,
                productionStage);
        }

        /// <summary>
        /// Gets the application production types.
        /// </summary>
        /// <returns>The application production types.</returns>
        public IList<NouPROrderType> GetPrOrderTypes()
        {
            return this.productionRepository.GetPrOrderTypes();
        }

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        public IList<int> GetScannedCarcassSplitStock(string serial, bool import = false, int transactionTypeId = 3)
        {
            return this.productionRepository.GetScannedCarcassSplitStock(serial, import, transactionTypeId);
        }

        ///// <summary>
        ///// Determines if a carcass split scan already exists in the db.
        ///// </summary>
        ///// <param name="serial">The serial number to check.</param>
        ///// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        //public IList<int> GetScannedCarcassSplitStock(string serial, bool intake = false)
        //{
        //    return this.productionRepository.GetScannedCarcassSplitStock(serial, intake);
        //}

        /// <summary>
        /// Determines if a batch reference exists.
        /// </summary>
        /// <param name="reference">The reference to check.</param>
        /// <returns></returns>
        public DateTime? DoesPROrderByReferenceExist(string reference)
        {
            return this.productionRepository.DoesPROrderByReferenceExist(reference);
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public int AddRecipe(Sale sale)
        {
            return this.productionRepository.AddRecipe(sale);
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public bool UpdateRecipe(Sale sale)
        {
            return this.productionRepository.UpdateRecipe(sale);
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeById(int id)
        {
            return this.productionRepository.GetRecipeById(id);
        }

        /// <summary>
        /// Gets a Recipe by associated product id.
        /// </summary>
        /// <param name="id">The product id.</param>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeByProductId(int id)
        {
            return this.productionRepository.GetRecipeByProductId(id);
        }

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeByLastEdit()
        {
            return this.productionRepository.GetRecipeByLastEdit();
        }

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeByFirstLast(bool first)
        {
            return this.productionRepository.GetRecipeByFirstLast(first);
        }

        /// <summary>
        /// Adds a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was added.</returns>
        public int AddProductionOrder(ProductionData order)
        {
            return this.productionRepository.AddProductionOrder(order);
        }

        /// <summary>
        /// Returns the specifications.
        /// </summary>
        /// <returns>The application specifications.</returns>
        public IList<ProductionData> GetSpecifications()
        {
            return this.productionRepository.GetSpecifications();
        }

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        public PROrder GetPROrderLastEdit()
        {
            return this.productionRepository.GetPROrderLastEdit();
        }

        /// <summary>
        /// Edits the batch row data.
        /// </summary>
        /// <returns></returns>
        public bool EditBatchData(BatchData batchData)
        {
            return this.productionRepository.EditBatchData(batchData);
        }

        /// <summary>
        /// Gets a prorder reference.
        /// </summary>
        /// <param name="prOrderId"></param>
        /// <returns></returns>
        public string GetPROrderReference(int prOrderId)
        {
            return this.productionRepository.GetPROrderReference(prOrderId);
        }

        /// <summary>
        /// Returns the production order data.
        /// </summary>
        /// <returns>The production order data.</returns>
        public IList<BatchData> GetBatchData(int prOrderId, int transId, bool linesOnly)
        {
            return (from data in this.productionRepository.GetBatchData(prOrderId, transId, linesOnly)
                select new BatchData
                {
                    PROrderID = data.PROrderID,
                    LinesOnly = linesOnly,
                    INMasterID = data.INMasterID,
                    TransID = transId,
                    Reference = data.Reference,
                    Price = data.Price,
                    StockTransactionID = data.StockTransactionID,
                    WarehouseID = data.WarehouseID,
                    INGroupID = data.INGroupID,
                    BPGroupID = data.BPGroupID,
                    ProcessID = data.ProcessID,
                    BPMasterID = data.BPMasterID,
                    Serial = data.Serial,
                    CreationDate = data.CreationDate,
                    ScheduledDate = data.ScheduledDate, 
                    Qty = data.Qty,
                    Wgt = data.Wgt,
                    Tare = data.Tare,
                    UserName = data.UserName,
                    Device = data.Device,
                    Product = data.Product,
                    ProductCode = data.ProductCode,
                    BatchNumberID = data.BatchNumberID,
                    BatchNumber = data.BatchNumber,
                    AttributeID = data.AttributeID,
                    Attribute1 = data.Attribute1,
                     Attribute2 = data.Attribute2,
                     Attribute3 = data.Attribute3,
                      Attribute4 = data.Attribute4,
                      Attribute5 = data.Attribute5,
                      Attribute6 = data.Attribute6,
                      Attribute7 = data.Attribute7,
                      Attribute8 = data.Attribute8,
                      Attribute9 = data.Attribute9,
                      Attribute10 = data.Attribute10,
                      Attribute11 = data.Attribute11,
                      Attribute12 = data.Attribute12,
                      Attribute13 = data.Attribute13,
                      Attribute14 = data.Attribute14,
                      Attribute15 = data.Attribute15,
                      Attribute16 = data.Attribute16,
                      Attribute17 = data.Attribute17,
                      Attribute18 = data.Attribute18,
                      Attribute19 = data.Attribute19,
                      Attribute20 = data.Attribute20,
                      Attribute21 = data.Attribute21,
                      Attribute22 = data.Attribute22,
                      Attribute23 = data.Attribute23,
                      Attribute24 = data.Attribute24,
                      Attribute25 = data.Attribute25,
                      Attribute26 = data.Attribute26,
                      Attribute27 = data.Attribute27,
                      Attribute28 = data.Attribute28,
                      Attribute29 = data.Attribute29,
                      Attribute30 = data.Attribute30
                }).ToList();
        }

        /// <summary>
        /// Returns the batch stock data for a product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public IList<App_ProductStockDataByBatch_Result> GetProductStockDataByBatch(int productId)
        {
            return this.productionRepository.GetProductStockDataByBatch(productId);
        }

        /// <summary>
        /// Updates an order output.
        /// </summary>
        /// <param name="spec">The outputs to add.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        public bool UpdateRecipeOrderAmounts(ProductionData order)
        {
            return this.productionRepository.UpdateRecipeOrderAmounts(order);
        }

        /// <summary>
        /// Returns the production batch reference, or empty string if user generated.
        /// </summary>
        /// <returns>The production batch reference.</returns>
        public string GetProductionBatchReference(int deviceId, int userId, int processid, DateTime scheduledDate)
        {
            return this.productionRepository.GetProductionBatchReference(deviceId, userId, processid, scheduledDate);
        }

        ///// <summary>
        ///// Returns the production multi batch intake orders.
        ///// </summary>
        ///// <returns>The application production orders.</returns>
        //public BatchNumber GetProductionProductOldestBatch(int? inMasterId)
        //{
        //    return this.productionRepository.GetProductionProductOldestBatch(inMasterId);
        //}

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public ProductionBatchData GetProductionBatchData(int? inMasterId)
        {
            return this.productionRepository.GetProductionBatchData(inMasterId);
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrderByNumber(int orderId)
        {
            return this.productionRepository.GetProductionBatchOrderByNumber(orderId);
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeOrderMethodByProductId(int id)
        {
            return this.productionRepository.GetRecipeOrderMethodByProductId(id);
        }

        /// <summary>
        /// Copies a production order.
        /// </summary>
        /// <param name="orderNo">The order no to copy from.</param>
        /// <param name="userId">The current user.</param>
        /// <param name="deviceId">The current device.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        public ProductStockData CopyProductionOrder(int orderNo, int userId, int deviceId, DateTime scheduledDate)
        {
            return this.productionRepository.CopyProductionOrder(orderNo, userId, deviceId, scheduledDate);
        }

        /// <summary>
        /// Issues recipe backflush.
        /// </summary>
        /// <param name="orderId">The pr orderid.</param>
        /// <param name="batchId">the batch number id.</param>
        /// <param name="qty">The issue qty.</param>
        /// <param name="wgt">The issue wgt.</param>
        /// <param name="warehouseId">The warehouse id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <param name="processId">The process id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <param name="splitId">The mix count.</param>
        /// <returns>Flag , as to successful issue or not.</returns>
        public bool IssueBackflush(int orderId, int batchId, decimal qty, decimal wgt, int warehouseId, int inMasterId,
            int? processId, int userId, int deviceId, int splitId)
        {
            return this.productionRepository.IssueBackflush(orderId, batchId, qty, wgt, warehouseId, inMasterId,
                processId, userId, deviceId, splitId);
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionBatchOrdersForDesktop(IList<int> orderStatuses, DateTime start,
            DateTime end)
        {
            return this.productionRepository.GetProductionBatchOrdersForDesktop(orderStatuses, start, end);
        }

        /// <summary>
        /// Gets the recipe order methods.
        /// </summary>
        /// <returns>The recipe order methods</returns>
        public IList<App_GetRecipeOrderMethods_Result> GetRecipeOrderMethods()
        {
            return this.productionRepository.GetRecipeOrderMethods();
        }

        /// <summary>
        /// Adds a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was added.</returns>
        public int AddUniqueProductionOrder(ProductionData order)
        {
            return this.productionRepository.AddUniqueProductionOrder(order);
        }

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        public bool ChangeRecipeBatchStatus(int orderId, int? status)
        {
            return this.productionRepository.ChangeRecipeBatchStatus(orderId, status);
        }

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        public bool ChangeOrderStatus(int orderId, int nouDocStatusId)
        {
            return this.productionRepository.ChangeOrderStatus(orderId, nouDocStatusId);
        }

        /// <summary>
        /// Gets the production details.
        /// </summary>
        /// <param name="prorderid">The order id.</param>
        /// <param name="transactionTypeid">The into/out of value.</param>
        /// <returns>A production details.</returns>
        public IList<App_GetProductionDetails_Result> GetProductionDetails(int prorderid, int transactionTypeid)
        {
            return this.productionRepository.GetProductionDetails(prorderid, transactionTypeid);
        }

        /// <summary>
        /// Gets the production details.
        /// </summary>
        /// <param name="prorderid">The order id.</param>
        /// <param name="transactionTypeid">The into/out of value.</param>
        /// <returns>A production details.</returns>
        public IList<InventoryItem> GetProductionBatchDetails(int prorderid, int transactionTypeid)
        {
            var details = new List<InventoryItem>();
            var data = this.productionRepository.GetProductionDetails(prorderid, transactionTypeid);
            foreach (var product in data)
            {
                details.Add(new InventoryItem
                {
                    ProductID = product.INMasterID,
                    ProductName = product.Generic1,
                    StockWgt = product.Wgt.ToDecimal(),
                    StockQty = product.Qty.ToDecimal()
                });
            }

            return details;
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public IList<StockDetail> GetRecipeByIngredientProductId(int id)
        {
            var details = this.productionRepository.GetRecipeByIngredientProductId(id);
            return details;
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public IList<StockDetail> ParseRecipeByIngredientProducts(IList<StockDetail> details, int id, decimal wgt, int? prOrderId)
        {
            if (!details.Any() || wgt == 0)
            {
                return new List<StockDetail>();
            }

            var issueProduct = details.First(x => x.INMasterID == id);
            var issueQty = issueProduct.TypicalBatchSize.ToDecimal();
            if (issueQty == 0)
            {
                return new List<StockDetail>();
            }
            var percent = wgt / issueQty;
            foreach (var stockDetail in details)
            {
                stockDetail.MasterTableID = prOrderId;
                if (stockDetail.OrderMethod == OrderMethod.Quantity)
                {
                    stockDetail.TransactionQty = Math.Round(stockDetail.TypicalBatchSize.ToDecimal() * percent, 2);
                    stockDetail.TransactionWeight = 0;
                }
                else
                {
                    stockDetail.TransactionWeight = Math.Round(stockDetail.TypicalBatchSize.ToDecimal() * percent, 2);
                    stockDetail.TransactionQty = 0;
                }
            }

            return details;
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionBatchOrders(IList<int> orderStatuses, DateTime start, DateTime end)
        {
            return this.productionRepository.GetProductionBatchOrders(orderStatuses, start, end);
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderRecallData(int orderId)
        {
            return this.productionRepository.GetProductionOrderRecallData(orderId);
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrder(int orderId)
        {
            return this.productionRepository.GetProductionBatchOrder(orderId);
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrderLastEdit()
        {
            return this.productionRepository.GetProductionBatchOrderLastEdit();
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrderFirstLast(bool first)
        {
            return this.productionRepository.GetProductionBatchOrderFirstLast(first);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public int? GetPROrderNumber(int transactionId)
        {
            return this.productionRepository.GetPROrderNumber(transactionId);
        }

        /// <summary>
        /// Updates an order output.
        /// </summary>
        /// <param name="spec">The outputs to add.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        public bool UpdateOrderSpecification(ProductionData spec)
        {
            return this.productionRepository.UpdateOrderSpecification(spec);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public ProductionData GetPROrderById(int id)
        {
            return this.productionRepository.GetPROrderById(id);
        }

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="batches">The workflow batches.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateBatches(HashSet<Sale> batches)
        {
            return this.productionRepository.UpdateBatches(batches);
        }

        /// <summary>
        /// Updates batch weight/qty.
        /// </summary>
        /// <param name="orderId">The prorder id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateBatchWeight(int orderId)
        {
            return this.productionRepository.UpdateBatchWeight(orderId);
        }

        /// <summary>
        /// Consumes all items in a batch at the input module.
        /// </summary>
        /// <param name="prOrderId">The batch id.</param>
        /// <param name="transTypeId">The module type.</param>
        /// <returns>Flag, as to successfull consumption or not.</returns>
        public bool ConsumeProductionBatch(int prOrderId, int transTypeId)
        {
            return this.productionRepository.ConsumeProductionBatch(prOrderId, transTypeId);
        }

        /// <summary>
        /// Adds stock to a production.
        /// </summary>
        /// <returns>Flag, as to whether stock has been added to a production.</returns>
        public bool AddStockToProduction(SaleDetail product)
        {
            return this.productionRepository.AddStockToProduction(product);
        }

        /// <summary>
        /// Removes stock from a production order.
        /// </summary>
        /// <param name="product">The transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveStockFromProductionOrder(SaleDetail product)
        {
            return this.productionRepository.RemoveStockFromOrder(product);
        }

        /// <summary>
        /// Removes stock from a production order.
        /// </summary>
        /// <param name="product">The transactions to update.</param>
        /// <param name="unconsumeStock">The flag as to whether stock is to be unconsumed.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveStockFromIntoProductionOrder(StockDetail product)
        {
            return this.productionRepository.RemoveStockFromIntoProductionOrder(product);
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderTransactionData(int orderId)
        {
            return this.productionRepository.GetProductionOrderTransactionData(orderId);
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderTransactionDataNotSplit(int orderId)
        {
            return this.productionRepository.GetProductionOrderTransactionDataNotSplit(orderId);
        }

        /// <summary>
        /// Gets the spec and batch only data for the out of production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetOutOfProductionOrderData(int orderId)
        {
            return this.productionRepository.GetOutOfProductionOrderData(orderId);
        }

        /// <summary>
        /// Cancel a batch.
        /// </summary>
        /// <returns>The batch id.</returns>
        public bool CancelBatch(int batchId)
        {
            return this.productionRepository.CancelBatch(batchId);
        }

        /// <summary>
        /// Open a batch.
        /// </summary>
        /// <returns>The batch id.</returns>
        public bool OpenBatch(int batchId)
        {
            return this.productionRepository.OpenBatch(batchId);
        }

        /// <summary>
        /// Retrieves the products alloed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>A collection of allowable products.</returns>
        public IList<SaleDetail> GetProductsAllowedOutOfProduction(int prSpecId)
        {
            return this.productionRepository.GetProductsAllowedOutOfProduction(prSpecId);
        }

        /// <summary>
        /// Retrieves the products allowed out of production.
        /// </summary>
        /// <param name="prOrderId">The pr order id.</param>
        /// <returns>A collection of allowable products.</returns>
        public IList<InventoryItem> GetAllProductsAllowedOutOfProduction(int prOrderId)
        {
            return this.productionRepository.GetAllProductsAllowedOutOfProduction(prOrderId);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public bool UpdatePieceTransaction(int transactionId)
        {
            return this.productionRepository.UpdatePieceTransaction(transactionId);
        }

        /// <summary>
        /// Adds an out of production transaction data.
        /// </summary>
        /// <param name="transData">The transaction data.</param>
        /// <returns>Flag, indicating a successful addition or not.</returns>
        public int AddOutOfProductionTransaction(StockDetail transData)
        {
            return this.productionRepository.AddOutOfProductionTransaction(transData);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public Tuple<int, List<int?>> AddBoxTransaction(StockTransaction transaction, decimal boxTare = 0, int qtyPerBox = 1, bool enforceBoxQtyMatch = false)
        {
            return this.productionRepository.AddBoxTransaction(transaction, boxTare, qtyPerBox, enforceBoxQtyMatch);
        }

        /// <summary>
        /// Start of day to ensure leftover stock cannot be boxed.
        /// </summary>
        /// <returns>A flag, as to a successful procedure.</returns>
        public bool DisableAddingStockToBox()
        {
            return this.stockRepository.DisableAddingStockToBox();
        }

        /// <summary>
        /// Adds a new production spec.
        /// </summary>
        /// <param name="spec">The spec to add.</param>
        /// <returns>A flag, as to successful add or not.</returns>
        public bool AddSpecification(ProductionData spec)
        {
            return this.productionRepository.AddSpecification(spec);
        }

        /// <summary>
        /// AUpdates production spec.
        /// </summary>
        /// <param name="spec">The spec to update.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        public bool UpdateSpecification(ProductionData spec)
        {
            return this.productionRepository.UpdateSpecification(spec);
        }

        #endregion

        #region purchases

        #region general

        /// <summary>
        /// Gets the transaction count for a given header row.
        /// </summary>
        /// <param name="id">The header row id.</param>
        /// <param name="transactionTypeId">The transaction type id</param>
        /// <returns>The transaction count for a given header row.</returns>
        public int GetTransactionCount(int id, int transactionTypeId)
        {
            return this.purchasesRepository.GetTransactionCount(id, transactionTypeId);
        }

        /// <summary>
        /// Removes a sale detail.
        /// </summary>
        /// <param name="module">The sale module.</param>
        /// <param name="itemId">The sale item id.</param>
        public bool RemoveSaleDetail(ViewType module, int itemId)
        {
            switch (module)
            {
                case ViewType.Quote:
                    return this.salesRepository.RemoveArQuoteDetail(itemId);

                case ViewType.Order:
                    return true; //this.salesRepository.RemoveArOrderDetail(itemId);

                case ViewType.ARDispatch:
                    return this.salesRepository.RemoveArDispatchtDetail(itemId);

                case ViewType.APQuote:
                    return this.purchasesRepository.RemoveApQuoteDetail(itemId);

                case ViewType.APOrder:
                    return this.purchasesRepository.RemoveApOrderDetail(itemId);

                case ViewType.APReceipt:
                    return this.purchasesRepository.RemoveApReceiptDetail(itemId);

                case ViewType.PurchaseInvoice:
                    return this.purchasesRepository.RemoveApReceiptDetail(itemId);

                default:
                    return false;
            }
        }

        #endregion

        #region quote

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetAPQuotesByPartner(int partnerId, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPQuotesByPartner(partnerId, orderStatuses);
        }

        /// <summary>
        /// Retrieves all the quotes (and associated quote details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all quotes (and associated quote details).</returns>
        public IList<Sale> GetAllAPQuotes(IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAllAPQuotes(orderStatuses);
        }

        /// <summary>
        /// Gets the intake details.
        /// </summary>
        /// <param name="id">The intake id to search for.</param>
        /// <returns>An intake, and it's details.</returns>
        public Sale GetTouchscreenReceiptByNextQueueNo(string killType)
        {
            return this.purchasesRepository.GetTouchscreenReceiptByNextQueueNo(killType);
        }

        /// <summary>
        /// Prices a purchase invoice docket.
        /// </summary>
        /// <param name="intakeId">The intake id used to price it.</param>
        /// <param name="invoiceId">The invoice id if updating.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Invoice number if successful, otherwise 0.</returns>
        public int PricePurchaseInvoice(int intakeId, int invoiceId, int userId, int deviceId)
        {
            return this.purchasesRepository.PricePurchaseInvoice(intakeId, invoiceId, userId, deviceId);
        }

        /// <summary>
        /// Ckecks if a product is on a purchase order.
        /// </summary>
        /// <param name="inmasterid"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool IsProductOnPurchaseOrder(int inmasterid, int orderId)
        {
            return this.purchasesRepository.IsProductOnPurchaseOrder(inmasterid, orderId);
        }

        /// <summary>
        /// Chages the status on an intake line.
        /// </summary>
        /// <param name="id">The line id.</param>
        /// <param name="statusId">The status to change to.</param>
        /// <returns>Flag, as to sucessful change or not.</returns>
        public bool ChangeAPReceiptDetailOrderStatus(int id, int statusId)
        {
            return this.purchasesRepository.ChangeAPReceiptDetailOrderStatus(id, statusId);
        }

        /// <summary>
        /// Retrieves the intake (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetIntakeDetails_Result> GetIntakeStatusDetails(int id)
        {
            return this.purchasesRepository.GetIntakeStatusDetails(id);
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceIntakeDocket(int docketId)
        {
            return this.purchasesRepository.PriceIntakeDocket(docketId);
        }

        /// <summary>
        /// Cancels the input intake order.
        /// </summary>
        /// <param name="id">The purchase order to cancel the associated intake for.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        public bool CancelIntakeFromOrder(int id)
        {
            return this.purchasesRepository.CancelIntakeFromOrder(id);
        }

        /// <summary>
        /// Gets the batch numbers associated with an intake product line.
        /// </summary>
        /// <returns>Batch ids.</returns>
        public IList<BatchNumber> GetIntakeLineBatchNumbers(int intakeLineId)
        {
            return this.purchasesRepository.GetIntakeLineBatchNumbers(intakeLineId);
        }

        /// <summary>
        /// Gets the batch numbers associated with an intake product line.
        /// </summary>
        /// <returns>Batch ids.</returns>
        public IList<string> GetIntakeLineBatchReferences(int intakeLineId)
        {
            return this.purchasesRepository.GetIntakeLineBatchReferences(intakeLineId);
        }

        /// <summary>
        /// Removes a receipt detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApInvoiceDetail(int itemId)
        {
            return this.purchasesRepository.RemoveApInvoiceDetail(itemId);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetAPQuotes(int customerId)
        {
            return this.purchasesRepository.GetAPQuotes(customerId);
        }

        /// <summary>
        /// Gets the intake docket id relating to the input purchase order id.
        /// </summary>
        /// <param name="id">The input order id.</param>
        /// <returns>The docket id relating to the input  order id.</returns>
        public int GetIntakeIdForOrder(int id)
        {
            return this.purchasesRepository.GetIntakeIdForOrder(id);
        }

        /// <summary>
        /// Updates an existing intake docket (docket and details) when a purchaseorder has been ammended.
        /// </summary>
        /// <param name="sale">The order data.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateIntakeFromOrder(Sale sale)
        {
            return this.purchasesRepository.UpdateIntakeFromOrder(sale);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetRecentAPQuotes(int customerId)
        {
            return this.purchasesRepository.GetRecentAPQuotes(customerId);
        }

        /// <summary>
        /// Removes a quote detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApQuoteDetail(int itemId)
        {
            return this.purchasesRepository.RemoveApQuoteDetail(itemId);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetAPQuotesByName(string searchTerm, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPQuotesByName(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer id.
        /// </summary>
        /// <param name="quoteId">The quote id.</param>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        public Sale GetAPQuoteByID(int quoteId)
        {
            return this.purchasesRepository.GetAPQuoteByID(quoteId);
        }

        /// <summary>
        /// Retrieves the last local edited quote.
        /// </summary>
        /// <returns>A quote (and associated quote details).</returns>
        public Sale GetAPQuoteByLastEdit()
        {
            return this.purchasesRepository.GetAPQuoteByLastEdit();
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetAPQuotesByCode(string searchTerm, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPQuotesByCode(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>The newly created sale id, indicating a successful quote add, or not.</returns>
        public int AddAPQuote(Sale sale)
        {
            return this.purchasesRepository.AddAPQuote(sale);
        }

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>A flag, indicating a successful quote update, or not.</returns>
        public bool UpdateAPQuote(Sale sale)
        {
            return this.purchasesRepository.UpdateAPQuote(sale);
        }

        #endregion

        #region order

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPOrdersByPartner(int partnerId, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPOrdersByPartner(partnerId, orderStatuses);
        }

        /// <summary> Adds a new purchase order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        public int AddAPOrder(Sale sale)
        {
            return this.purchasesRepository.AddAPOrder(sale);
        }

        /// <summary>
        /// Updates an existing order (order and details).
        /// </summary>
        /// <param name="sale">The purchase order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPOrder(Sale sale)
        {
            return this.purchasesRepository.UpdateAPOrder(sale);
        }

        /// <summary>
        /// Retrieves all the orders (and associated orders details).
        /// </summary>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPOrders(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            return this.purchasesRepository.GetAllAPOrders(orderStatuses, start, end);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetAPOrderByID(int orderId)
        {
            return this.purchasesRepository.GetAPOrderByID(orderId);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input number.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An order (and associated order details) for the input number.</returns>
        public Sale GetAPOrderByNumber(int number)
        {
            return this.purchasesRepository.GetAPOrderByNumber(number);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetAPOrderByLastEdit()
        {
            return this.purchasesRepository.GetAPOrderByLastEdit();
        }

        /// <summary>
        /// Retrieves the live orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetAPOrders(int customerId)
        {
            return this.purchasesRepository.GetAPOrders(customerId);
        }

        /// <summary>
        /// Retrieves the live orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentAPOrders(int customerId)
        {
            return this.purchasesRepository.GetRecentAPOrders(customerId);
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPOrdersByName(string searchTerm, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPOrdersByName(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPOrdersByCode(string searchTerm, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPOrdersByCode(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input order id.
        /// </summary>
        /// <param name="apOrderId">The order id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        public Sale GetAPOrderById(int apOrderId)
        {
            return this.purchasesRepository.GetAPOrderById(apOrderId);
        }

        /// <summary>
        /// Removes an order detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApOrderDetail(int itemId)
        {
            return this.purchasesRepository.RemoveApOrderDetail(itemId);
        }

        #endregion

        #region goods in

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsForBatchEdit()
        {
            return this.purchasesRepository.GetAllAPReceiptsForBatchEdit();
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsByAttribute(string searchText)
        {
            return this.purchasesRepository.GetAllAPReceiptsByAttribute(searchText);
        }


        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentPurchaseOrders(int customerId)
        {
            return this.purchasesRepository.GetRecentPurchaseOrders(customerId);
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentPurchaseReceiptOrders(int customerId)
        {
            return this.purchasesRepository.GetRecentPurchaseReceiptOrders(customerId);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPReceiptsByPartner(int partnerId, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPReceiptsByPartner(partnerId, orderStatuses);
        }

        /// <summary> Adds a new purchase receipt (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddAPReceipt(Sale sale)
        {
            return this.purchasesRepository.AddAPReceipt(sale);
        }

        /// <summary>
        /// Adds or removes pallet stock from the order.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        /// <param name="intakeId">The order id.</param>
        /// <param name="addToOrder">Add or remove flag.</param>
        /// <returns>Flag, as to success or not.</returns>
        public bool AddOrRemoveIntakePalletStock(int? serial, int? intakeId, bool addToOrder)
        {
            return this.purchasesRepository.AddOrRemovePalletStock(serial, intakeId, addToOrder);
        }

        /// <summary>
        /// Retrieves the receipt (and associated order details) for the input ap order id.
        /// </summary>
        /// <param name="orderId">The ap order id.</param>
        /// <returns>The receipt (and associated order details) for the input ap order id.</returns>
        public Sale GetAPReceiptsByApOrderId(int orderId)
        {
            return this.purchasesRepository.GetAPReceiptsByApOrderId(orderId);
        }

        /// <summary>
        /// Retrieves all the goods received by kill type (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsByType(IList<int?> orderStatuses, string type)
        {
            return this.purchasesRepository.GetAllAPReceiptsByType(orderStatuses, type);
        }

        /// <summary>
        /// Removes a receipt detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApReceiptDetail(int itemId)
        {
            return this.purchasesRepository.RemoveApReceiptDetail(itemId);
        }

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReceipt(Sale sale)
        {
            return this.purchasesRepository.UpdateAPReceipt(sale);
        }

        /// <summary>
        /// Gets the intake details.
        /// </summary>
        /// <param name="id">The intake id to search for.</param>
        /// <returns>An intake, and it's details.</returns>
        public Sale GetTouchscreenReceiptById(int id)
        {
            return this.purchasesRepository.GetTouchscreenReceiptById(id);
        }

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateDesktopAPReceipt(Sale sale)
        {
            return this.purchasesRepository.UpdateDesktopAPReceipt(sale);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceipts(IList<int?> orderStatuses)
        {
            var orders = this.purchasesRepository.GetAllAPReceipts(orderStatuses);
            if (ApplicationSettings.ShowOrHideGroupPartners.HasValue)
            {
                return this.GetFilteredOrders(orders);
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsForReport(IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAllAPReceiptsForReport(orderStatuses);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsForReportByDates(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            return this.purchasesRepository.GetAllAPReceiptsForReportByDates(orderStatuses, start, end);
        }

        /// <summary>
        /// Retrieves the receipt (and associated receipt details) for the input receipt id.
        /// </summary>
        /// <param name="apReceiptId">The receipt id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetAPReceiptById(int apReceiptId)
        {
            return this.purchasesRepository.GetAPReceiptById(apReceiptId);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="apReceiptId">The order serch id.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetAPReceiptFullById(int apReceiptId)
        {
            return this.purchasesRepository.GetAPReceiptFullById(apReceiptId);
        }

        /// <summary>
        /// Updates a line with a batch no.
        /// </summary>
        /// <param name="id">The line id.</param>
        /// <param name="batchNo">The batch no.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateAPReceiptBatchNumber(int id, int batchNo)
        {
            return this.purchasesRepository.UpdateAPReceiptBatchNumber(id, batchNo);
        }

        /// <summary>
        /// Determines if an intake has all it's stock on sales invoices.
        /// </summary>
        /// <param name="id">The intake id.</param>
        /// <returns></returns>
        public bool HasIntakeBeenSaleInvoiced(int id)
        {
            return this.purchasesRepository.HasIntakeBeenSaleInvoiced(id);
        }

        /// <summary>
        /// Copies a purchase order.
        /// </summary>
        /// <param name="orderNo">The order no to copy from.</param>
        /// <param name="userId">The current user.</param>
        /// <param name="deviceId">The current device.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        public ProductStockData CopyPurchaseOrder(int orderNo, int productid, int userId, int deviceId, DateTime scheduledDate)
        {
            return this.purchasesRepository.CopyPurchaseOrder(orderNo, productid, userId, deviceId, scheduledDate);
        }

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReceiptStatus(Sale sale)
        {
            return this.purchasesRepository.UpdateAPReceiptStatus(sale);
        }

        /// <summary>
        /// Retrieves the receipts (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetAPReceipts(int customerId)
        {
            return this.purchasesRepository.GetAPReceipts(customerId);
        }

        /// <summary>
        /// Retrieves the receipts (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentAPReceipts(int customerId)
        {
            return this.purchasesRepository.GetRecentAPReceipts(customerId);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the last local edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details)</returns>
        public Sale GetAPReceiptFullByLastEdit()
        {
            return this.purchasesRepository.GetAPReceiptFullByLastEdit();
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPReceiptsByName(string searchTerm, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPReceiptsByName(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPReceiptsByCode(string searchTerm, IList<int?> orderStatuses)
        {
            return this.purchasesRepository.GetAPReceiptsByCode(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="batches">The workflow batches.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateIntakes(HashSet<Sale> batches, ViewType view)
        {
            return this.purchasesRepository.UpdateIntakes(batches, view);
        }

        /// <summary>
        /// Gets the batch numbers.
        /// </summary>
        /// <returns>A collection of batch numbers.</returns>
        public IList<BatchNumber> GetBatchNumbers()
        {
            return this.purchasesRepository.GetBatchNumbers();
        }

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        public bool CompleteOrCancelAPReceipt(Sale sale, bool complete = true)
        {
            return this.purchasesRepository.CompleteOrCancelAPReceipt(sale, complete);
        }

        /// <summary>
        /// Removes stock from item.
        /// </summary>
        /// <param name="sale">The sale/detail/transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveIntakeStockFromOrder(Sale sale)
        {
            return this.purchasesRepository.RemoveIntakeStockFromOrder(sale);
        }

        #endregion

        #region returns

        /// <summary> Adds a new stock return (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddAPReturn(Sale sale)
        {
            return this.purchasesRepository.AddAPReturn(sale);
        }

        /// <summary>
        /// Updates an existing return (receipt and details).
        /// </summary>
        /// <param name="sale">The return receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReturn(Sale sale)
        {
            return this.purchasesRepository.UpdateAPReturn(sale);
        }


        /// <summary>
        /// Retrieves the returns (and associated details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetReturnDetails_Result> GetAPReturnsStatusDetails(int id)
        {
            return this.purchasesRepository.GetReturnsStatusDetails(id);
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAPReturns(IList<int?> orderStatuses)
        {
            var orders = this.purchasesRepository.GetAPReturns(orderStatuses);
            foreach (var order in orders)
            {
                order.DeliveryAddress = new BusinessPartnerAddress {Details = order.Address};
                order.InvoiceAddress = new BusinessPartnerAddress { Details = order.Address };
            }

            return orders;
        }

        /// <summary>
        /// Gets the return details.
        /// </summary>
        /// <param name="id">The returns id to search for.</param>
        /// <returns>A return, and it's details.</returns>
        public Sale GetAPReturnById(int id)
        {
            return this.purchasesRepository.GetAPReturnById(id);
        }

        /// <summary>
        /// Gets the return details by last edit date.
        /// </summary>
        /// <returns>A return, and it's details.</returns>
        public Sale GetAPReturnByLastEdit()
        {
            return this.purchasesRepository.GetAPReturnByLastEdit();
        }

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        public bool UpdateAPReturnStatus(Sale sale)
        {
            return this.purchasesRepository.UpdateAPReturnStatus(sale);
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceAPReturnsDocket(int docketId)
        {
            return this.purchasesRepository.PriceReturnsDocket(docketId);
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAPReturnsNonInvoiced()
        {
            var orders = this.purchasesRepository.GetAPReturnsNonInvoiced();
            foreach (var order in orders)
            {
                order.DeliveryAddress = new BusinessPartnerAddress { Details = order.Address };
                order.InvoiceAddress = new BusinessPartnerAddress { Details = order.Address };
            }

            return orders;
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReturnInvoice(Sale sale)
        {
            return this.purchasesRepository.UpdateAPReturnInvoice(sale);
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddAPReturnInvoice(Sale sale)
        {
            return this.purchasesRepository.AddAPReturnInvoice(sale);
        }


        /// <summary>
        /// Retrieves the ap return invoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetAPReturnInvoices()
        {
            return this.purchasesRepository.GetAPReturnInvoices();
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetAPReturnInvoiceByID(int id)
        {
            return this.purchasesRepository.GetAPReturnInvoiceByID(id);
        }

        /// <summary>
        /// Exports credit note details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportAPReturnInvoice(IList<int> invoiceIds)
        {
            return this.purchasesRepository.ExportAPReturnInvoice(invoiceIds);
        }

        #endregion

        #region invoice 

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddNewAPInvoice(Sale sale)
        {
            return this.purchasesRepository.AddNewAPInvoice(sale);
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPInvoice(Sale sale)
        {
            return this.purchasesRepository.UpdateAPInvoice(sale);
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllAPInvoices(int supplierId)
        {
            return this.purchasesRepository.GetAllAPInvoices(supplierId);
        }

        /// <summary>
        /// Retrieves all the non invoiced intake data.
        /// </summary>
        /// <returns>A collection of non invoiced intake data (and associated order details).</returns>
        public IList<Sale> GetNonInvoicedIntakes(bool pricingInvoice = false)
        {
            var intakes = this.purchasesRepository.GetNonInvoicedIntakes(pricingInvoice);
            if (pricingInvoice)
            {
                var localIntakes = intakes.ToList();
                foreach (var intake in localIntakes)
                {
                    var hasBeenSaleInvoiced = this.purchasesRepository.HasIntakeBeenSaleInvoiced(intake.SaleID);
                    if (!hasBeenSaleInvoiced)
                    {
                        var localIntake = intakes.FirstOrDefault(x => x.SaleID == intake.SaleID);
                        if (localIntake != null)
                        {
                            intakes.Remove(localIntake);
                        }
                    }
                }
            }

            return intakes;
        }

        /// <summary>
        /// Retrieves the apinvoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetAPInvoices()
        {
            return this.purchasesRepository.GetAPInvoices();
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllAPInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end,
            string dateType)
        {
            return this.purchasesRepository.GetAllAPInvoicesByDate(invoiceStatuses, start, end, dateType);
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetAPInvoiceByLastEdit()
        {
            return this.purchasesRepository.GetAPInvoiceByLastEdit();
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetAPInvoiceByID(int id)
        {
            return this.purchasesRepository.GetAPInvoiceByID(id);
        }


        #endregion

        #endregion

        #region Quality

        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        public IList<ViewQualityMaster> GetQualityMasters()
        {
            return this.qualityRepository.GetQualityMasters();
        }

        /// <summary>
        /// Retrieve all the system date types.
        /// </summary>
        /// <returns>A collection of date types.</returns>
        public IList<NouQualityType> GetQualityTypes()
        {
            return this.qualityRepository.GetQualityTypes();
        }

        /// <summary>
        /// Adds or upquality date masters.
        /// </summary>
        /// <param name="dateMasters">The date masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateQualityMasters(IList<ViewQualityMaster> dateMasters)
        {
            return this.qualityRepository.AddOrUpdateQualityMasters(dateMasters);
        }

        /// <summary>
        /// Retrieve all the system date template names.
        /// </summary>
        /// <returns>A collection of date template names.</returns>
        public IList<QualityTemplateName> GetQualityTemplateNames()
        {
            return this.qualityRepository.GetQualityTemplateNames();
        }

        /// <summary>
        /// Adds or upquality date names.
        /// </summary>
        /// <param name="dateNames">The date names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateQualityTemplateNames(IList<QualityTemplateName> dateNames)
        {
            return this.qualityRepository.AddOrUpdateQualityTemplateNames(dateNames);
        }

        /// <summary>
        /// Retrieve all the system date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        public IList<QualityTemplateAllocation> GetQualityTemplateAllocations()
        {
            return this.qualityRepository.GetQualityTemplateAllocations();
        }


        /// <summary>
        /// Retrieve all the view date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        public IList<ViewQualityTemplateAllocation> GetViewQualityTemplateAllocations()
        {
            return this.qualityRepository.GetViewQualityTemplateAllocations();
        }

        /// <summary>
        /// Adds a date master to a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddQualityToTemplate(int dateMasterId, int templateId)
        {
            return this.qualityRepository.AddQualityToTemplate(dateMasterId, templateId);
        }

        /// <summary>
        /// Removes a date master from a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        public bool RemoveQualityFromTemplate(int dateMasterId, int templateId)
        {
            return this.qualityRepository.RemoveQualityFromTemplate(dateMasterId, templateId);
        }

        /// <summary>
        /// Adds a quality master to a template.
        /// </summary>
        /// <param name="qualityMasters">The collection of the quality masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddQualitysToTemplate(IList<ViewQualityMaster> qualityMasters, int templateId)
        {
            return this.qualityRepository.AddQualitysToTemplate(qualityMasters, templateId);
        }

        #endregion

        #region Region

        /// <summary>
        /// Returns the application regions.
        /// </summary>
        /// <returns>The application regions.</returns>
        public IList<Region> GetRegions()
        {
            return this.regionRepository.GetRegions();
        }

        /// <summary>
        /// Add or updates the regions.
        /// </summary>
        /// <param name="regions">The regions to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateRegions(IList<Region> regions)
        {
            return this.regionRepository.AddOrUpdateRegions(regions);
        }

        #endregion

        #region report

        /// <summary>
        /// Retrieve the epos sales data falling between the input dates.
        /// </summary>
        /// <returns>A collection of sales data.</returns>
        public IList<ProductData> GetEposSalesData(DateTime startDate, DateTime endDate)
        {
            return this.reportRepository.GetEposSalesData(startDate, endDate);
        }

        /// <summary>
        /// Updates the report email lookups.
        /// </summary>
        /// <param name="modules">The look ups to add.</param>
        /// <returns>Flag, as to successful operation or not.</returns>
        public bool UpdateReportProcesses(IList<Model.BusinessObject.Module> modules)
        {
            return this.reportRepository.UpdateReportProcesses(modules);
        }

        /// <summary>
        /// Gets the report emaiul lookups.
        /// </summary>
        /// <returns>The application report email lookups.</returns>
        public IList<ReportEmail> GetReportEmailLookups()
        {
            return this.reportRepository.GetReportEmailLookups();
        }

        /// <summary>
        /// Gets the modules.  
        /// </summary>
        /// <returns>the user reports.</returns>
        public IList<NouModule> GetModules()
        {
            return this.reportRepository.GetModules();
        }

        /// <summary>
        /// Adds or updates report data.
        /// </summary>
        /// <param name="reports">The report to add/update.</param>
        /// <returns>Flag, as to successfull add or update.</returns>
        public bool AddOrUpdateReports(IList<ReportData> reports)
        {
            return this.reportRepository.AddOrUpdateReports(reports);
        }

        /// <summary>
        /// Gets a report by last edit.
        /// </summary>
        /// <returns>A system report data.</returns>
        public ReportData GetReportByLastEdit()
        {
            return this.reportRepository.GetReportByLastEdit();
        }

        /// <summary>
        /// Adds or updates the report folders.
        /// </summary>
        /// <returns>Flag, as to successfull add or update.</returns>
        public bool AddOrUpdateReportFolders(IList<ReportsFolder> folders)
        {
            var dbFolders = new List<ReportFolder>();
            foreach (var folder in folders)
            {
                dbFolders.Add(new ReportFolder{ ReportFolderID = folder.ReportFolderId, Name = folder.Name, ParentID = folder.ParentId, Deleted = folder.Deleted});
            }

            return this.reportRepository.AddOrUpdateReportFolders(dbFolders);
        }

        /// <summary>
        /// Save a user report default values.
        /// </summary>
        /// <param name="setting">The default data.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool SaveUserReportSettings(UserReport setting)
        {
            return this.reportRepository.SaveUserReportSettings(setting);
        }

        /// <summary>
        /// Updates the multi values for the reports.
        /// </summary>
        /// <param name="values">The values to update.</param>
        public void UpdateMultiValueReports(IList<MultiReportValue> values)
        {
            this.reportRepository.UpdateMultiValueReports(values);
        }

        /// <summary>
        /// Gets the system report data setting for the input report.
        /// </summary>
        /// <returns>The system reports data setting for the input report.</returns>
        public IList<UserReport> GetReportSettings(int reportId)
        {
            return this.reportRepository.GetReportSettings(reportId);
        }

        /// <summary>
        /// Saves a report date type agianst a report and user.
        /// </summary>
        /// <param name="reportId">The report to save against.</param>
        /// <param name="dateTypeId">The selected date type.</param>
        /// <param name="userId">The user to save against.</param>
        /// <returns>Flag, as to successful save or not.</returns>
        public bool SaveReportDateType(int reportId, int dateTypeId, int userId)
        {
            return this.reportRepository.SaveReportDateType(reportId, dateTypeId, userId);
        }

        /// <summary>
        /// Gets the system report data setting for the input report and user.
        /// </summary>
        /// <returns>The system reports data setting for the input report and user.</returns>
        public UserReport GetUserReportSettings(ReportData report, int userId)
        {
            return this.reportRepository.GetUserReportSettings(report, userId);
        }

        /// <summary>
        /// Gets the system report data.
        /// </summary>
        /// <returns>The system reports data.</returns>
        public IList<ReportData> GetReports()
        {
            return this.reportRepository.GetReports();
        }

        /// <summary>
        /// Gets the report folders.
        /// </summary>
        /// <returns></returns>
        public IList<ReportFolder> GetReportFolders()
        {
            return this.reportRepository.GetReportFolders();
        }

        /// <summary>
        /// Adds or updates report data.
        /// </summary>
        /// <param name="report">The report to add/update.</param>
        /// <returns>Flag, as to successfull add or update.</returns>
        public bool AddOrUpdateReport(ReportData report)
        {
            return this.reportRepository.AddOrUpdateReport(report);
        }

        /// <summary>
        /// Gets a report by it's id.
        /// </summary>
        /// <param name="id">The id to search with.</param>
        /// <returns>A system report data.</returns>
        public ReportData GetReportById(int id)
        {
            return this.reportRepository.GetReportById(id);
        }


        /// <summary>
        /// Gets a report by first or last entered.
        /// </summary>
        /// <param name="first">First\last flag.</param>
        /// <returns>A system report data.</returns>
        public ReportData GetReportByFirstLast(bool first)
        {
            return this.reportRepository.GetReportByFirstLast(first);
        }

        /// <summary>
        /// Updates the db reports.
        /// </summary>
        /// <param name="reports">The reports to update.</param>
        /// <returns></returns>
        public bool UpdateNouReports(IList<ReportData> reports)
        {
            return this.reportRepository.UpdateNouReports(reports);
        }

        /// <summary>
        /// Updates the multi kill values for the reports.
        /// </summary>
        /// <param name="values">The values to update.</param>
        public void UpdateMultiKillValueReports(IList<MultiKillReportValue> values)
        {
            this.reportRepository.UpdateMultiKillValueReports(values);
        }

        /// <summary>
        /// Gets the intake kill ids between the input dates.
        /// </summary>
        /// <param name="start">Start date.</param>
        /// <param name="end">End date.</param>
        /// <returns>The intake kill ids between the input dates.</returns>
        public IList<int?> GetKillReportIds(DateTime start, DateTime end)
        {
            return this.reportRepository.GetKillReportIds(start, end);
        }

        /// <summary>
        /// Gets the user reports.  
        /// </summary>
        /// <returns>the user reports.</returns>
        public IList<UserReport> GetUserReports()
        {
            return this.reportRepository.GetUserReports();
        }

        #endregion

        #region Route

        /// <summary>
        /// Returns the application routes.
        /// </summary>
        /// <returns>The application routes.</returns>
        public IList<Route> GetRoutes()
        {
            return this.routeRepository.GetRoutes();
        }

        /// <summary>
        /// Add or updates the routes.
        /// </summary>
        /// <param name="routes">The routes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateRoutes(IList<Route> routes)
        {
            return this.routeRepository.AddOrUpdateRoutes(routes);
        }

        #endregion

        #region sales

        #region quote

        /// <summary>
        /// Retrieves all the quotes (and associated quote details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all quotes (and associated quote details).</returns>
        public IList<Sale> GetAllQuotes(IList<int?> orderStatuses)
        {
            return this.salesRepository.GetAllQuotes(orderStatuses);
        }

        /// <summary>
        /// Retrieves the quote (and associated quote details) for the input id.
        /// </summary>
        /// <param name="quoteId">The customer id.</param>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        public Sale GetQuoteByID(int quoteId)
        {
            return this.salesRepository.GetQuoteByID(quoteId);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetQuotesByPartner(int partnerId, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetQuotesByPartner(partnerId, orderStatuses);
        }

        /// <summary>
        /// Retrieves the quotes (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetOrdersByPartner(int partnerId, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetOrdersByPartner(partnerId, orderStatuses);
        }

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of dispatches (and associated dispatch details) for the input search term.</returns>
        public IList<Sale> GetARDispatchesByPartner(int partnerId, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetARDispatchesByPartner(partnerId, orderStatuses);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetQuotes(int customerId)
        {
            return this.salesRepository.GetQuotes(customerId);
        }

        /// <summary>
        /// Retrieves the quote (and associated quote details) for the input id.
        /// </summary>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        public Sale GetQuoteByLastEdit()
        {
            return this.salesRepository.GetQuoteByLastEdit();
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetRecentQuotes(int customerId)
        {
            return this.salesRepository.GetRecentQuotes(customerId);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetQuotesByName(string searchTerm, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetQuotesByName(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetQuotesByCode(string searchTerm, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetQuotesByCode(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>The newly created sale id, indicating a successful quote add, or not.</returns>
        public int AddQuote(Sale sale)
        {
            return this.salesRepository.AddNewQuote(sale);
        }

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>A flag, indicating a successful quote update, or not.</returns>
        public bool UpdateQuote(Sale sale)
        {
            return this.salesRepository.UpdateQuote(sale);
        }

        /// <summary>
        /// Adds a telesale call.
        /// </summary>
        /// <param name="telesale">The telesale to add or update.</param>
        /// <returns>Flag, as to a successful add/update.</returns>
        public bool AddTelesaleCall(TelesaleCall telesale)
        {
            return this.salesRepository.AddTelesaleCall(telesale);
        }

        #endregion

        #region order

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public int AddNewOrder(Sale sale)
        {
            return this.salesRepository.AddNewOrder(sale);
        }

        /// <summary>
        /// Updates an existing order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateOrder(Sale sale)
        {
            return this.salesRepository.UpdateOrder(sale);
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetOrders(int customerId)
        {
            return this.salesRepository.GetOrders(customerId);
        }

        /// <summary>
        /// Determines if a sale orders master dispatch docket has any transactions recorded against it.
        /// </summary>
        /// <param name="orderId">The order to check.</param>
        /// <returns>A flag, indicating whether the sale orders master dispatch docket has any transactions recorded against it. </returns>
        public bool DoesMasterDocumentHaveTransactions(int orderId)
        {
            return this.salesRepository.DoesMasterDocumentHaveTransactions(orderId);
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentOrders(int customerId)
        {
            return this.salesRepository.GetRecentOrders(customerId);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        public Sale GetOrderByID(int orderId)
        {
            return this.salesRepository.GetOrderByID(orderId);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        public Sale GetOrderByFirstLast(bool first)
        {
            return this.salesRepository.GetOrderByFirstLast(first);
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        public Sale GetOrderByLastEdit()
        {
            return this.salesRepository.GetOrderByLastEdit();
        }

        /// <summary>
        /// Retrieves the quotes (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetOrdersByName(string searchTerm, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetOrdersByName(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the qorders (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetOrdersByCode(string searchTerm, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetOrdersByCode(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves all the orders.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllOrdersForReport(IList<int?> orderStatuses)
        {
            return this.salesRepository.GetAllOrdersForReport(orderStatuses);
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllOrders(IList<int?> orderStatuses)
        {
            return this.salesRepository.GetAllOrders(orderStatuses);
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllOrdersByDate(IList<int?> orderStatuses, DateTime start, DateTime end, string dateType)
        {
            return this.salesRepository.GetAllOrdersByDate(orderStatuses, start, end, dateType);
        }

        #endregion

        #region dispatch

        /// <summary>
        /// Adds scanned stock to a dispatch.
        /// </summary>
        /// <param name="serial">The barcode.</param>
        /// <param name="ardispatchId">The docket Id.</param>
        /// <param name="allowDispatchProductNotOnOrder"><Can the product be added if not on order./param>
        /// <param name="allowDispatchOverAmount">Can over the line amount be dispatched.</param>
        /// <param name="allowStockAlreadyOnAnOrder">Can the stock be added to the order if on another order.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error\Message (empty if ok).</returns>
        public Tuple<int?, string, List<App_GetDispatchDetails_Result>, int?,int?> AddStockToDispatch(
            int serial,
            int ardispatchId,
            bool allowDispatchProductNotOnOrder,
            bool allowDispatchOverAmount,
            bool allowStockAlreadyOnAnOrder,
            int userId,
            int deviceId,
            int warehouseId,
            int processId,
            int customerId)
        {
            var result = this.salesRepository.AddStockToDispatch(serial, ardispatchId, allowDispatchProductNotOnOrder,
                allowDispatchOverAmount, allowStockAlreadyOnAnOrder, userId, deviceId, warehouseId, processId, customerId);

            var stockId = result.Item1;
            var msg = result.Item2;
            var dt = result.Item3;
            var dispatchId = result.Item4;
            var docketNo = result.Item5;

            var data = new List<App_GetDispatchDetails_Result>();

            if (string.IsNullOrEmpty(msg))
            {
                try
                {
                    if (dt != null)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.Add(new App_GetDispatchDetails_Result
                            {
                                QuantityOrdered = row["QuantityOrdered"].ToString().ToDecimal(),
                                WeightOrdered = row["WeightOrdered"].ToString().ToDecimal(),
                                UnitPrice = row["UnitPrice"].ToString().ToDecimal(),
                                Qty = row["Qty"].ToString().ToDecimal(),
                                Wgt = row["Wgt"].ToString().ToDecimal(),
                                BoxCount = row["BoxCount"].ToString().ToNullableInt(),
                                Generic1 = row["Generic1"].ToString(),
                                Generic2 = row["Generic2"].ToString(),
                                PriceListID = row["PriceListID"].ToString().ToNullableInt(),
                                ARDispatchID = row["ARDispatchID"].ToString().ToInt(),
                                ARDispatchDetailID = row["ARDispatchDetailID"].ToString().ToInt(),
                                INMasterID = row["INMasterID"].ToString().ToInt(),
                            });
                        }
                    }
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }
            }

            return Tuple.Create(stockId, msg, data, dispatchId,docketNo);
        }

        /// <summary>
        /// Gets pallet dispatch data.
        /// </summary>
        /// <param name="id">The dispatch id.</param>
        /// <returns>Pallet data.</returns>
        public Sale GetDispatchPalletData(int id)
        {
            var palletData = new Sale { StockDetails = new List<StockDetail>() };
            var data = this.salesRepository.GetDispatchPalletData(id);
            if (data == null)
            {
                return null;
            }

            var localData = data.GroupBy(x => x.PalletID);
            var count = 1;

            var docketData = data.First();
            palletData.PartnerName = docketData.SupplierName;
            palletData.PartnerCode = docketData.SupplierCode;
            palletData.Number = docketData.DocketNo;
            palletData.CustomerPOReference = docketData.CustomerPOReference;
            palletData.DeliveryDate = docketData.DeliveryDate;
            palletData.DeliveryTime = docketData.DeliveryTime;
            palletData.PalletCount = localData.Count();
            palletData.BoxCount = data.Count;
            palletData.Name = docketData.Column1;

            foreach (var pallet in localData)
            {
                var palletSerial = string.Empty;
                var localPallet = pallet.FirstOrDefault();
                if (localPallet != null)
                {
                    palletSerial = localPallet.PalletSerial;
                }

                var palletHeader = new StockDetail
                {
                    PalletNo = count,
                    PalletID = pallet.Key.ToInt(),
                    PalletSerial = palletSerial,
                    TransactionQty = pallet.Count(),
                    StockDetails = new List<StockDetail>()
                };

                foreach (var item in pallet)
                {
                    palletHeader.StockDetails.Add(new StockDetail
                    {
                        Serial = item.StockTransactionID,
                        PLU = item.PLU,
                        AnaCode = item.AnaCode,
                        TransactionQty = item.Pieces,
                        BatchNumberID = item.BatchNumberID,
                        DocumentReference = item.Column2,
                        CountryOfOrigin = item.CountryOfSlaughter,
                        FactoryOfSlaughter = item.FactoryOfSlaughter,
                        TransactionWeight = item.TransactionWeight,
                        Tare = item.Tare.ToDecimal(),
                        KillDate = item.KillDate,
                        PackDate = item.PackDate.ToDate().ToShortDateString(),
                        UseBy = item.UseByDate.ToDate().ToShortDateString(),
                        CaseUID = item.CaseUID,
                        PalletSerial = item.PalletSerial,
                        UOM = item.UOM,
                        FixedWeight = item.FixedWeight
                    });
                }

                palletData.StockDetails.Add(palletHeader);
                count++;
            }

            return palletData;
        }

        /// <summary> Adds a new purchase receipt (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        public int AddAPDispatch(Sale sale)
        {
            return this.salesRepository.AddAPDispatch(sale);
        }

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetDispatchDetails_Result> GetDispatchStatusDetails(int arDispatchId)
        {
            return this.salesRepository.GetDispatchStatusDetails(arDispatchId);
        }

        /// <summary>
        /// Updates an existing dispatch docket (docket and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARDispatch(Sale sale)
        {
            return this.salesRepository.UpdateARDispatch(sale);
        }

        /// <summary>
        /// Retrieves the dispatch carcass data.
        /// </summary>
        /// <returns>The dispatch carcass data.</returns>
        public IList<Sale> GetDispatchStock(IList<Sale> dispatches)
        {
            return this.salesRepository.GetDispatchStock(dispatches);
        }

        /// <summary>
        /// Adds or removes pallet stock from the order.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        /// <param name="arDispatchId">The order id.</param>
        /// <param name="addToOrder">Add or remove flag.</param>
        /// <returns>Flag, as to success or not.</returns>
        public bool AddOrRemovePalletStock(int? serial, int? arDispatchId, bool addToOrder)
        {
            return this.salesRepository.AddOrRemovePalletStock(serial, arDispatchId, addToOrder);
        }

        /// <summary>
        /// Gets pallet dispatch data.
        /// </summary>
        /// <param name="id">The dispatch id.</param>
        /// <returns>Pallet data.</returns>
        public List<string> CheckForExtraDispatchLines(int id)
        {
            var dt = this.salesRepository.CheckForExtraDispatchLines(id);
            var values = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                values.Add(row["Name"].ToString());
            }

            return values;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentDispatches(int customerId)
        {
            return this.salesRepository.GetRecentDispatches(customerId);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="id">The order id.</param>
        /// <returns>The orders (and associated order details).</returns>
        public Sale GetTouchscreenARDispatch(int id)
        {
            return this.salesRepository.GetTouchscreenARDispatch(id);
        }

        /// <summary>
        /// Updates an existing dispatch docket (docket and details) when a sale order has beem ammended.
        /// </summary>
        /// <param name="sale">The sale order data.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARDispatchFromSaleOrder(Sale sale)
        {
            if (sale.SaleDetails != null)
            {
                foreach (var detail in sale.SaleDetails)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }
            }

            return this.salesRepository.UpdateARDispatchFromSaleOrder(sale);
        }

        /// <summary>
        /// Adds a dispatch container.
        /// </summary>
        /// <param name="container">The container to add.</param>
        /// <returns>A flag, indicating a successful addition or not.</returns>
        public bool AddDispatchContainer(Model.DataLayer.DispatchContainer container)
        {
            return this.salesRepository.AddDispatchContainer(container);
        }

        /// <summary>
        /// Updates dispatch containers.
        /// </summary>
        /// <param name="containers">The containers to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDispatchContainer(IList<Model.BusinessObject.DispatchContainer> containers)
        {
            return this.salesRepository.UpdateDispatchContainer(containers);
        }

        /// <summary>
        /// Retrieves the dispatch containers.
        /// </summary>
        /// <param name="docStatusId">The doc status ofr the containers to retrieve.</param>
        /// <returns>A collection of dispatch containers.</returns>
        public IList<Model.BusinessObject.DispatchContainer> GetDispatchContainers(int docStatusId)
        {
            return this.salesRepository.GetDispatchContainers(docStatusId);
        }

        /// <summary>
        /// Checks the completed dispatch docket.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        public bool CheckDispatchDocket(int docketId)
        {
            return this.salesRepository.CheckDispatchDocket(docketId);
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceDispatchDocket(int docketId)
        {
            return this.salesRepository.PriceDispatchDocket(docketId);
        }

        /// <summary>
        /// Adds any delivery charges to the order.
        /// </summary>
        /// <param name="dispatchId">The order id.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddDeliveryCharges(int dispatchId)
        {
            return this.salesRepository.AddDeliveryCharges(dispatchId);
        }

        /// <summary>
        /// Updates dispatch containers.
        /// </summary>
        /// <param name="containerId">The container to search the dispatches for.</param>
        /// <returns>A date the container is due to go out on.</returns>
        public DateTime? GetDispatchContainerDeliveryDate(int containerId)
        {
            return this.salesRepository.GetDispatchContainerDeliveryDate(containerId);
        }

        /// <summary>
        /// Gets the dispatch total.
        /// </summary>
        /// <param name="sale">The dispatch data.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateNotes(Sale sale)
        {
            return this.salesRepository.UpdateNotes(sale);
        }

        /// <summary>
        /// Gets the telesale call instances.
        /// </summary>
        /// <returns>The acall instances.</returns>
        public IList<TelesaleCall> GetTelesaleCalls()
        {
            return this.salesRepository.GetTelesaleCalls();
        }

        /// <summary>
        /// Gets the dispatch total.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id.</param>
        /// <returns>A flag, indicating a successful retrieval or not.</returns>
        public decimal GetDispatchTotal(int arDispatchId)
        {
            return this.salesRepository.GetDispatchTotal(arDispatchId);
        }

        /// <summary>
        /// Removes an ARDispatchDetail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveArDispatchtDetail(int itemId)
        {
            return this.salesRepository.RemoveArDispatchtDetail(itemId);
        }

        /// <summary>
        /// Completes the input dispatch order.
        /// </summary>
        /// <param name="saleOrderId">The sale order to canel the associated dispatch for.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        public bool CancelARDispatchFromSaleOrder(int saleOrderId)
        {
            return this.salesRepository.CancelARDispatchFromSaleOrder(saleOrderId);
        }

        /// <summary>
        /// Removes stock from item.
        /// </summary>
        /// <param name="sale">The sale/detail/transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveStockFromOrder(Sale sale)
        {
            return this.salesRepository.RemoveStockFromOrder(sale);
        }

        /// <summary>
        /// Updates an order detail line.
        /// </summary>
        /// <param name="detail">The order detail line.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDetailTotal(CompleteOrderCheck detail)
        {
            return this.salesRepository.UpdateDetailTotal(detail);
        }

        /// <summary>
        /// Completes the input dispatch order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="orderStatusId">The order status id.</param>
        /// <returns>A flag, as to whether the order status was changed or not.</returns>
        public bool ChangeARDispatchStatus(Sale sale, int orderStatusId)
        {
            return this.salesRepository.ChangeARDispatchStatus(sale, orderStatusId);
        }

        /// <summary>
        /// Gets the dispatch box piece labels.
        /// </summary>
        /// <param name="serial">The serial number to search for.</param>
        /// <returns>The dispatch box piece labels correesponding to the dispatched box.</returns>
        public IList<GoodsIn> GetPieceLabels(int serial)
        {
            return this.salesRepository.GetPieceLabels(serial);
        }

        /// <summary>
        /// Gets the ardispatch docket id relating to the input sale order id.
        /// </summary>
        /// <param name="arOrderId">The input sale order id.</param>
        /// <returns>The ardispatch docket id relating to the input sale order id.</returns>
        public int GetARDispatchIDForSaleOrder(int arOrderId)
        {
            return this.salesRepository.GetARDispatchIDForSaleOrder(arOrderId);
        }

        /// <summary>
        /// Creates a back over from outstanding dispatch amounts.
        /// </summary>
        /// <param name="ardispatchId">The docket to create the back order for.</param>
        /// <returns></returns>
        public int CreateBackOrder(int ardispatchId)
        {
            return this.salesRepository.CreateBackOrder(ardispatchId);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatches(IList<int?> orderStatuses)
        {
            return this.salesRepository.GetAllARDispatches(orderStatuses);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatchesByDate(bool showAll, DateTime start, DateTime end, string dateType)
        {
            var sales = this.salesRepository.GetAllARDispatchesByDate(showAll, start, end, dateType);
            if (ApplicationSettings.ShowOrHideGroupPartners.HasValue)
            {
                return this.GetFilteredOrders(sales);
            }

            return sales;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetFTraceDispatches(bool showAll, DateTime start, DateTime end)
        {
            var sales = new List<Sale>();
            var dt = this.salesRepository.GetFTraceDispatches(showAll, start, end);
            foreach (DataRow row in dt.Rows)
            {
                sales.Add(new Sale
                {
                    SaleID = row["ARDispatchID"].ToString().ToInt(),
                    Number = row["Number"].ToString().ToInt(),
                    ShippingDate = row["ShippingDate"].ToString().ToDate(),
                    DeliveryDate = row["DeliveryDate"].ToString().ToDate(),
                    ExportedDate = row["ExportedDate"].ToString().ToNullableDate(),
                    NouDocStatusID = row["NouDocStatusID"].ToString().ToInt(),
                    SlaughterOnly = row["Generic2"].ToString().ToBool(),
                    DispatchAndProcessingOnly = row["Generic3"].ToString().ToBool(),
                    BPHaulier = new BPMaster { BPMasterID = row["HaulierID"].ToString().ToInt(), Name = row["HaulierName"].ToString()},
                    BPCustomer = new BPMaster { BPMasterID = row["CustomerID"].ToString().ToInt(), Name = row["CustomerName"].ToString(), Code = row["Generic1"].ToString() },
                    DeliveryAddress = new BusinessPartnerAddress
                    {
                        Details = new BPAddress
                        {
                            AddressLine1 = row["ShippingAddress1"].ToString(),
                            AddressLine2 = row["ShippingAddress2"].ToString(),
                            AddressLine3 = row["ShippingAddress3"].ToString(),
                            AddressLine4 = row["ShippingAddress4"].ToString(),
                            AddressLine5 = row["ShippingAddress5"].ToString(),
                            PostCode = row["ShippingPostCode"].ToString()
                        }
                    },

                    InvoiceAddress = new BusinessPartnerAddress
                    {
                        Details = new BPAddress
                        {
                            AddressLine1 = row["AddressLine1"].ToString(),
                            AddressLine2 = row["AddressLine2"].ToString(),
                            AddressLine3 = row["AddressLine3"].ToString(),
                            AddressLine4 = row["AddressLine4"].ToString(),
                            AddressLine5 = row["AddressLine5"].ToString(),
                            PostCode = row["PostCode"].ToString()
                        }
                    },
                });
            }

            return sales;
        }

        /// <summary>
        /// Gets the dockets the input product is on.
        /// </summary>
        /// <param name="productId">The input product.</param>
        /// <returns>A list of dockets the product is on.</returns>
        public IList<int> GetAllProductDockets(int productId)
        {
            return this.salesRepository.GetAllProductDockets(productId);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllTouchscreenARDispatches(IList<int?> orderStatuses, int? routeId = null)
        {
            var orders = this.salesRepository.GetAllTouchscreenARDispatches(orderStatuses, routeId);
            if (ApplicationSettings.ShowOrHideGroupPartners.HasValue)
            {
                return this.GetFilteredOrders(orders);
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchByLastEdit()
        {
            return this.salesRepository.GetARDispatchByLastEdit();
        }

        /// <summary>
        /// Gets the dispatch docket details for a final check before completion.
        /// </summary>
        /// <param name="ardispatchId">The docket id.</param>
        public IList<CompleteOrderCheck> CompleteOrderCheck(int ardispatchId)
        {
            return this.salesRepository.CompleteOrderCheck(ardispatchId);
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatchesForReport(IList<int?> orderStatuses)
        {
            return this.salesRepository.GetAllARDispatchesForReport(orderStatuses);
        }

        /// <summary>
        /// Retrieves all the pallets between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        /// <returns>A collection of pallets.</returns>
        public IList<Sale> GetAllPallets(DateTime start, DateTime end)
        {
            return this.salesRepository.GetAllPallets(start, end);
        }

        /// <summary>
        /// Gets an order transactions.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>The order transactions.</returns>
        public IList<StockDetail> GetStockTransactions(int orderId)
        {
            return this.salesRepository.GetTransactions(orderId);
        }

        /// <summary>
        /// Retrieves all the non invoiced dispatch data.
        /// </summary>
        /// <returns>A collection of non invoiced dispatch data (and associated order details).</returns>
        public IList<Sale> GetNonInvoicedARDispatches(bool pricingByInvoice = false)
        {
            return this.salesRepository.GetNonInvoicedARDispatches(pricingByInvoice);
        }

        /// <summary>
        /// Retrieves the dispatches (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetARDispatches(int customerId, int ordersToTake)
        {
            return this.salesRepository.GetARDispatches(customerId, ordersToTake);
        }

        /// <summary>
        /// Retrieves the dispatches (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetOpenARDispatches(int customerId)
        {
            return this.salesRepository.GetOpenARDispatches(customerId);
        }

        /// <summary>
        /// Updates an order delivery date.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="delDate">The new delivery date.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool UpdateDeliveryDate(int orderId, DateTime delDate)
        {
            return this.salesRepository.UpdateDeliveryDate(orderId, delDate);
        }

        /// <summary>
        /// Gets the transactions and attribute data associated with a dispatch line.
        /// </summary>
        /// <param name="id">The dispatch detail id.</param>
        /// <returns>The transactions and attribute data associated with a dispatch line.</returns>
        public IList<App_GetOrderLineTransactionData_Result> GetOrderLineTransactionData(int id,int transactionTypeId, int docketId)
        {
            return this.salesRepository.GetOrderLineTransactionData(id, transactionTypeId, docketId);
        }

        /// <summary>
        /// Retrieves all the non invoiced dispatch data.
        /// </summary>
        /// <returns>A collection of non invoiced dispatch data (and associated order details).</returns>
        public IList<Sale> GetNonInvoicedARDispatches(int customerId)
        {
            return this.salesRepository.GetNonInvoicedARDispatches(customerId);
        }

        // <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of dispatches (and associated dispatch details) for the input search term.</returns>
        public IList<Sale> GetARDispatchesByName(string searchTerm, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetARDispatchesByName(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetARDispatchesByCode(string searchTerm, IList<int?> orderStatuses)
        {
            return this.salesRepository.GetARDispatchesByCode(searchTerm, orderStatuses);
        }

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchesById(int arDispatchId)
        {
            return this.salesRepository.GetARDispatchesById(arDispatchId);
        }

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchesByIdWithBoxCount(int arDispatchId)
        {
            return this.salesRepository.GetARDispatchesByIdWithBoxCount(arDispatchId);
        }

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchById(int arDispatchId)
        {
            return this.salesRepository.GetARDispatchById(arDispatchId);
        }

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchByFirstLast(bool first)
        {
            return this.salesRepository.GetARDispatchByFirstLast(first);
        }

        /// <summary>
        /// Retrieves all the consolidated invoice dispatches.
        /// </summary>
        /// <returns>A collection of consolidated invoice dispatches (and associated order details).</returns>
        public IList<Sale> GetARDispatchesByInvoice(int invoiceId)
        {
            return this.salesRepository.GetARDispatchesByInvoice(invoiceId);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="qtyPerBox">The qty per box.</param>
        /// <returns>The box transaction id.</returns>
        public int AddBoxTransaction(StockTransaction transaction, int qtyPerBox)
        {
            return this.salesRepository.AddBoxTransaction(transaction, qtyPerBox);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="qtyPerBox">The qty per box.</param>
        /// <returns>The box transaction id.</returns>
        public IList<int> AddBoxTransactionOnCompleteOrder(StockTransaction transaction, int qtyPerBox)
        {
            return this.salesRepository.AddBoxTransactionOnCompleteOrder(transaction, qtyPerBox);
        }

        /// <summary>
        /// Checks the product lines for unfilled box transactions.
        /// </summary>
        /// <param name="details">The product lines to check.</param>
        /// <returns>A collection of unfilled product line boxes.</returns>
        public IList<SaleDetail> CheckBoxTransactions(IList<SaleDetail> details)
        {
            return this.salesRepository.CheckBoxTransactions(details);
        }

        #endregion

        #region invoice

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddNewInvoice(Sale sale)
        {
            return this.salesRepository.AddNewInvoice(sale);
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateInvoice(Sale sale)
        {
            return this.salesRepository.UpdateInvoice(sale);
        }

        /// <summary>
        /// Removes an invoice detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveArInvoiceDetail(int itemId)
        {
            return this.salesRepository.RemoveArInvoiceDetail(itemId);
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllInvoices(IList<int?> invoiceStatuses)
        {
            return this.salesRepository.GetAllInvoices(invoiceStatuses);
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetInvoiceByLastEdit()
        {
            return this.salesRepository.GetInvoiceByLastEdit();
        }

        /// <summary>
        /// Copies a dispatch docket to an invoice.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>A newly created invoice number.</returns>
        public int CopyDispatchToInvoice(int dispatchId, int userId, int deviceId)
        {
            return this.salesRepository.CopyDispatchToInvoice(dispatchId, userId, deviceId);
        }

        /// <summary>
        /// Prices an invoice docket, as opposed to creating it from a priced dispatch docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id used to price it.</param>
        /// <param name="invoiceId">The invoice id if updating.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Invoice number if successful, otherwise 0.</returns>
        public int PriceInvoice(int dispatchId, int invoiceId, int userId, int deviceId)
        {
            return this.salesRepository.PriceInvoice(dispatchId, invoiceId, userId, deviceId);
        }

        /// <summary>
        /// Gets pallet data.
        /// </summary>
        /// <param name="id">The pallet id.</param>
        /// <returns>Pallet data.</returns>
        public IList<App_GetPalletisationStock_Result> GetPalletisationData(int id)
        {
            return this.salesRepository.GetPalletisationData(id);
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end)
        {
            return this.salesRepository.GetAllInvoicesByDate(invoiceStatuses, start, end);
        }

        /// <summary>
        /// Retrieves the non exported invoices (and associated invoice details).
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public void SetInvoiceBaseDocuments(IList<Sale> invoices)
        {
            this.salesRepository.SetInvoiceBaseDocuments(invoices);
        }

        /// <summary>
        /// Removes an invoice detail.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportInvoices(IList<int> invoiceIds)
        {
            return this.salesRepository.ExportInvoices(invoiceIds);
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input customer.</returns>
        public IList<Sale> GetInvoices(int customerId)
        {
            return this.salesRepository.GetInvoices(customerId);
        }

        /// <summary>
        /// Retrieves the non exported invoices (and associated invoice details).
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetNonExportedInvoices()
        {
            return this.salesRepository.GetNonExportedInvoices();
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input id.</returns>
        public IList<Sale> GetInvoicesByPartner(int partnerId, IList<int?> invoiceStatuses)
        {
            return this.salesRepository.GetInvoicesByPartner(partnerId, invoiceStatuses);
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input search term.</returns>
        public IList<Sale> GetInvoicesByName(string searchTerm, IList<int?> invoiceStatuses)
        {
            return this.salesRepository.GetInvoicesByName(searchTerm, invoiceStatuses);
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="invoiceStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetInvoicesByCode(string searchTerm, IList<int?> invoiceStatuses)
        {
            return this.salesRepository.GetInvoicesByCode(searchTerm, invoiceStatuses);
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end,
            string dateType)
        {
            return this.salesRepository.GetAllInvoicesByDate(invoiceStatuses, start, end, dateType);
        }

        /// <summary>
        /// Marks invoices as printed.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to mark.</param>
        /// <returns>Flag, as to whether the invoices were marked as printed, or not.</returns>
        public bool CompleteDispatchContainer(IList<int> invoiceIds, int containerId, bool complete)
        {
            return this.salesRepository.CompleteDispatchContainer(invoiceIds, containerId, complete);
        }

        /// <summary>
        /// Gets the docket status.
        /// </summary>
        /// <param name="arDispatchId">The docket id.</param>
        /// <returns>The input docket status.</returns>
        public int? GetARDispatchOrderStatus(int arDispatchId)
        {
            return this.salesRepository.GetARDispatchOrderStatus(arDispatchId);
        }

        /// <summary>
        /// Marks invoices as printed.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to mark.</param>
        /// <returns>Flag, as to whether the invoices were marked as printed, or not.</returns>
        public bool MarkInvoicesPrinted(IList<int> invoiceIds)
        {
            return this.salesRepository.MarkInvoicesPrinted(invoiceIds);
        }

        /// <summary>
        /// Retrieves the invoice (and associated invoice details) for the input invoice id.
        /// </summary>
        /// <param name="invoiceId">The invoice id.</param>
        /// <returns>An invoice (and associated invoice details) for the input id.</returns>
        public Sale GetInvoicesById(int invoiceId)
        {
            return this.salesRepository.GetInvoicesById(invoiceId);
        }

        #endregion

        #region Telesales

        /// <summary>
        /// Gets the application call frequencies.
        /// </summary>
        /// <returns>The application call frequencies.</returns>
        public IList<NouCallFrequency> GetCallFrequencies()
        {
            return this.salesRepository.GetCallFrequencies();
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesale(int partnerId, int statusId)
        {
            return this.salesRepository.GetTelesale(partnerId, statusId);
        }

        /// <summary>
        /// Gets the telesales relating to the input agents.
        /// </summary>
        /// <returns>A telesales collection.</returns>
        public IList<BusinessPartner> GetTelesales(IList<int?> agents, DateTime date)
        {
            var frequencies = this.GetCallFrequencies();
            var calls = this.GetTelesaleCalls();
            var weeklyCall = frequencies.First(x => x.Frequency.CompareIgnoringCase(Constant.Weekly));
            var biWeeklyCall = frequencies.First(x => x.Frequency.CompareIgnoringCase(Constant.BiWeekly));
            var monthlyCall = frequencies.First(x => x.Frequency.CompareIgnoringCase(Constant.Monthly));
            var telesales = new List<BusinessPartner>();
            var sales = this.salesRepository.GetTelesales(agents, date);

            var currentDay = date.DayOfWeek;
            var today = date;
            var currentDayOfWeek = (int) currentDay;
            var sunday = today.AddDays(-currentDayOfWeek);
            var monday = sunday.AddDays(1);
           
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }

            var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();

            foreach (var telesale in sales)
            {
                var found = false;
                if (telesale.NouCallFrequencyID == weeklyCall.NouCallFrequencyID)
                {
                    if (currentDay == DayOfWeek.Monday && !string.IsNullOrEmpty(telesale.CallTimeMonday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Tuesday && !string.IsNullOrEmpty(telesale.CallTimeTuesday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Wednesday && !string.IsNullOrEmpty(telesale.CallTimeWednesday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Thursday && !string.IsNullOrEmpty(telesale.CallTimeThursday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Friday && !string.IsNullOrEmpty(telesale.CallTimeFriday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Saturday && !string.IsNullOrEmpty(telesale.CallTimeSaturday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Sunday && !string.IsNullOrEmpty(telesale.CallTimeSunday))
                    {
                        found = true;
                    }
                }

                if (telesale.NouCallFrequencyID == biWeeklyCall.NouCallFrequencyID || telesale.NouCallFrequencyID == monthlyCall.NouCallFrequencyID)
                {
                    if (!dates.Contains(telesale.NextCallDate.ToDate()))
                    {
                        // if this is a completed call we show (the next call date will have moved on)
                        if (!calls.Any(x => x.TelesaleID == telesale.TelesalesID && x.TelesaleDate == date))
                        {
                            // active completed bi-weekly\monthly call, not in current week, so ignore.
                            continue;
                        }
                    }

                    if (currentDay == DayOfWeek.Monday && !string.IsNullOrEmpty(telesale.CallTimeMonday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Tuesday && !string.IsNullOrEmpty(telesale.CallTimeTuesday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Wednesday && !string.IsNullOrEmpty(telesale.CallTimeWednesday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Thursday && !string.IsNullOrEmpty(telesale.CallTimeThursday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Friday && !string.IsNullOrEmpty(telesale.CallTimeFriday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Saturday && !string.IsNullOrEmpty(telesale.CallTimeSaturday))
                    {
                        found = true;
                    }
                    else if (currentDay == DayOfWeek.Sunday && !string.IsNullOrEmpty(telesale.CallTimeSunday))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    continue;
                }

                var partner =
                    NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == telesale.BPMasterID);
                if (partner != null)
                {
                    partner.Telesale = telesale;

                    var agent =
                        NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == telesale.UserMasterID);
                    var caller =
                        NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == telesale.UserMasterID_Agent);
                    if (agent != null)
                    {
                        partner.UserName = agent?.UserMaster.FullName;
                        partner.UserNameCaller = caller?.UserMaster.FullName;
                        partner.CallFrequency =
                            frequencies.FirstOrDefault(x => x.NouCallFrequencyID == telesale.NouCallFrequencyID);
                        partner.CallTime = telesale.NextCallTime;

                        partner.TelesaleStatus =
                            calls.Any(x => x.TelesaleID == telesale.TelesalesID && x.TelesaleDate == date)
                                ? Strings.Complete
                                : Strings.Active;

                        if (telesale.NextCallDate > date && partner.TelesaleStatus == Strings.Active)
                        {
                            // valid open telesale, but the call date preceeds the next call date.
                            continue;
                        }
                    }

                    telesales.Add(partner);
                }
            }

            return telesales;
        }

        /// <summary>
        /// Adds or updates a telesale.
        /// </summary>
        /// <param name="telesale">The telesale to add or update.</param>
        /// <returns>Flag, as to a successful add/update.</returns>
        public bool AddOrUpdateTelesale(Telesale telesale)
        {
            return this.salesRepository.AddOrUpdateTelesale(telesale);
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesaleByFirstLast(bool first)
        {
            return this.salesRepository.GetTelesaleByFirstLast(first);
        }

        /// <summary>
        /// Gets the telesales for searching.
        /// </summary>
        /// <returns>A telesales collection.</returns>
        public IList<Sale> GetSearchTelesales()
        {
            return this.salesRepository.GetSearchTelesales();
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesaleByLastEdit()
        {
            return this.salesRepository.GetTelesaleByLastEdit();
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesaleById(int id)
        {
            return this.salesRepository.GetTelesaleById(id);
        }

        #endregion

        #region shared

        /// <summary>
        /// Gets the kill carcasses by kill date.
        /// </summary>
        /// <param name="start">The start kill date to serach from.</param>
        /// <param name="end">The end kill date to search to.</param>
        /// <returns>A collection of carcass records.</returns>
        public IList<Sale> GetCarcasses(DateTime start, DateTime end)
        {
            return this.salesRepository.GetCarcasses(start,end);
        }

       
        /// <summary>
        /// Gets the sale order document trail.
        /// </summary>
        /// <param name="documentNo">The sale order document id.</param>
        /// <param name="mode">The mode.</param>
        /// <returns>A collection of master-detail sale orders.</returns>
        public IList<Sale> GetDocumentTrail(int documentNo, ViewType mode)
        {
            return this.salesRepository.GetDocumentTrail(documentNo, mode);
        }

        /// <summary>
        /// Returns the application document status items.
        /// </summary>
        /// <returns>The application document status items.</returns>
        public IList<NouDocStatu> GetNouDocStatusItems()
        {
            return this.salesRepository.GetNouDocStatusItems();
        }

        /// <summary>
        /// Gets the dispatch data for the input dispatches.
        /// </summary>
        /// <param name="dispatches"></param>
        /// <returns></returns>
        public IList<StockDetail> GetCarcassDispatchData(string dispatches)
        {
            var data = this.salesRepository.GetCarcassDispatchData(dispatches);
            if (data == null)
            {
                return new List<StockDetail>(); 
            }

            return (from stock in data
                select new StockDetail
                {
                    CarcassNumber = stock.CarcassNumber,
                    BreedName = stock.Breed,
                    HerdNo = stock.Herd,
                    Eartag = stock.Eartag,
                    Grade = stock.Grade,
                    AgeInMonths = stock.MonthsOld,
                    TransactionDate = stock.IntakeDate.ToDate(),
                    SequencedDate = stock.SequencedDate,
                    GradingDate = stock.GradingDate,
                    DOB = stock.DOB,
                    CatName = stock.Category,
                    CountryOfOrigin = stock.CountryOfOrigin,
                    RearedIn = stock.RearedIn,
                    QA = stock.FarmAssured,
                    IntakeWeight = stock.IntakeHotWeight,
                    LossWeight = stock.LossWeight,
                    PaidWeight = stock.PaidWeight,
                    TransactionWeight = stock.DispatchWeight,
                    WeightSide1 = stock.Side1Weight.ToDecimal(),
                    WeightSide2 = stock.Side2Weight.ToDecimal(),
                    DispatchNo = stock.DispatchNo,
                    IntakeNo = stock.IntakeNo,
                    UnitPrice = stock.Price,
                    TotalIncVat = stock.Total,
                    Conformation = stock.Confirmation,
                    FatScore = stock.FatScore,
                    CarcassBreed = stock.Breed,
                    PartnerName = stock.Supplier,
                    CustomerName = stock.Customer

                }).ToList();
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatchesForReportByDates(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            return this.salesRepository.GetAllARDispatchesForReportByDates(orderStatuses, start, end);
        }

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceDispatch(int dispatchId)
        {
            return this.salesRepository.GetFTraceDispatch(dispatchId);
        }

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceSlaughterOnly(int dispatchId)
        {
            return this.salesRepository.GetFTraceSlaughterOnly(dispatchId);
        }

        /// <summary>
        /// Gets the FTrace slaughter data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceSlaughter(int dispatchId)
        {
            return this.salesRepository.GetFTraceSlaughter(dispatchId);
        }

        /// <summary>
        /// Gets the batch order data for a product line.
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public IList<App_GetOrderBatchData_Result> GetOrderBatchData(int orderLineId)
        {
            return this.salesRepository.GetOrderBatchData(orderLineId);
        }

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceProcessing(int dispatchId)
        {
            return this.salesRepository.GetFTraceProcessing(dispatchId);
        }

        /// <summary>
        /// Gets the FTrace slaughter data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public bool MarkFTraceAsExported(int dispatchId)
        {
            return this.salesRepository.MarkFTraceAsExported(dispatchId);
        }


        /// <summary>
        /// Gets a sale order line product data.
        /// </summary>
        /// <param name="productId">The product to retrieve data for.</param>
        /// <returns>A summary of the products stock and order amount.</returns>
        public Tuple<string, decimal, decimal, decimal, decimal> GetProductStockAndOrderData(int productId)
        {
            return this.salesRepository.GetProductStockAndOrderData(productId);
        }

        /// <summary>
        /// Creates a collection of product data from a selected customers most recent orders.
        /// </summary>
        /// <param name="orders">The most recent orders(max 10).</param>
        /// <returns>A collection of product data from a selected customers most recent orders.</returns>
        public IList<ProductRecentOrderData> GetRecentOrderData(IList<Sale> orders, IList<Sale> customerSales = null)
        {
            var recentOrderData = new List<ProductRecentOrderData>();
            var inventoryItems = new HashSet<InventoryItem>();

            // Get all the unique product items across all the recent orders.
            List<SaleDetail> allOrderItems;
            if (customerSales == null)
            {
                allOrderItems = orders.OrderByDescending(x => x.CreationDate).SelectMany(x => x.SaleDetails).ToList();
            }
            else
            {
                allOrderItems = customerSales.OrderByDescending(x => x.CreationDate).SelectMany(x => x.SaleDetails).ToList();
            }

            allOrderItems.ForEach(x =>
            {
                x.SetInventoryItem();
                inventoryItems.Add(x.InventoryItem);
            });

            foreach (var item in inventoryItems)
            {
                if (item == null)
                {
                    continue;
                }

                var productOrderData = new ProductRecentOrderData { InventoryItem = item, OrderMethod = OrderMethod.QuantityAndWeight };
                productOrderData.SaleDetailOrderMostRecent = allOrderItems.FirstOrDefault(x => x.InventoryItem == item);
                if (orders.Count > 0)
                {
                    var order = orders.ElementAt(0);
                    productOrderData.SaleDetailOrder1 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 1)
                {
                    var order = orders.ElementAt(1);
                    productOrderData.SaleDetailOrder2 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 2)
                {
                    var order = orders.ElementAt(2);
                    productOrderData.SaleDetailOrder3 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 3)
                {
                    var order = orders.ElementAt(3);
                    productOrderData.SaleDetailOrder4 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 4)
                {
                    var order = orders.ElementAt(4);
                    productOrderData.SaleDetailOrder5 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 5)
                {
                    var order = orders.ElementAt(5);
                    productOrderData.SaleDetailOrder6 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 6)
                {
                    var order = orders.ElementAt(6);
                    productOrderData.SaleDetailOrder7 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 7)
                {
                    var order = orders.ElementAt(7);
                    productOrderData.SaleDetailOrder8 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 8)
                {
                    var order = orders.ElementAt(8);
                    productOrderData.SaleDetailOrder9 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 9)
                {
                    var order = orders.ElementAt(9);
                    productOrderData.SaleDetailOrder10 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 10)
                {
                    var order = orders.ElementAt(10);
                    productOrderData.SaleDetailOrder11 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 11)
                {
                    var order = orders.ElementAt(11);
                    productOrderData.SaleDetailOrder12 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 12)
                {
                    var order = orders.ElementAt(12);
                    productOrderData.SaleDetailOrder13 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 13)
                {
                    var order = orders.ElementAt(13);
                    productOrderData.SaleDetailOrder14 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 14)
                {
                    var order = orders.ElementAt(14);
                    productOrderData.SaleDetailOrder15 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 15)
                {
                    var order = orders.ElementAt(15);
                    productOrderData.SaleDetailOrder16 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 16)
                {
                    var order = orders.ElementAt(16);
                    productOrderData.SaleDetailOrder17 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 17)
                {
                    var order = orders.ElementAt(17);
                    productOrderData.SaleDetailOrder18 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 18)
                {
                    var order = orders.ElementAt(18);
                    productOrderData.SaleDetailOrder19 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                if (orders.Count > 19)
                {
                    var order = orders.ElementAt(19);
                    productOrderData.SaleDetailOrder20 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item);
                }

                #region overflow

                if (orders.Count > 20) { var order = orders.ElementAt(20); productOrderData.SaleDetailOrder21 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 21) { var order = orders.ElementAt(21); productOrderData.SaleDetailOrder22 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 22) { var order = orders.ElementAt(22); productOrderData.SaleDetailOrder23 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 23) { var order = orders.ElementAt(23); productOrderData.SaleDetailOrder24 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 24) { var order = orders.ElementAt(24); productOrderData.SaleDetailOrder25 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 25) { var order = orders.ElementAt(25); productOrderData.SaleDetailOrder26 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 26) { var order = orders.ElementAt(26); productOrderData.SaleDetailOrder27 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 27) { var order = orders.ElementAt(27); productOrderData.SaleDetailOrder28 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 28) { var order = orders.ElementAt(28); productOrderData.SaleDetailOrder29 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 29) { var order = orders.ElementAt(29); productOrderData.SaleDetailOrder30 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 30) { var order = orders.ElementAt(30); productOrderData.SaleDetailOrder31 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 31) { var order = orders.ElementAt(31); productOrderData.SaleDetailOrder32 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 32) { var order = orders.ElementAt(32); productOrderData.SaleDetailOrder33 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 33) { var order = orders.ElementAt(33); productOrderData.SaleDetailOrder34 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 34) { var order = orders.ElementAt(34); productOrderData.SaleDetailOrder35 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 35) { var order = orders.ElementAt(35); productOrderData.SaleDetailOrder36 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 36) { var order = orders.ElementAt(36); productOrderData.SaleDetailOrder37 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 37) { var order = orders.ElementAt(37); productOrderData.SaleDetailOrder38 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 38) { var order = orders.ElementAt(38); productOrderData.SaleDetailOrder39 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 39) { var order = orders.ElementAt(39); productOrderData.SaleDetailOrder40 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 40) { var order = orders.ElementAt(40); productOrderData.SaleDetailOrder41 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 41) { var order = orders.ElementAt(41); productOrderData.SaleDetailOrder42 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 42) { var order = orders.ElementAt(42); productOrderData.SaleDetailOrder43 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 43) { var order = orders.ElementAt(43); productOrderData.SaleDetailOrder44 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 44) { var order = orders.ElementAt(44); productOrderData.SaleDetailOrder45 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 45) { var order = orders.ElementAt(45); productOrderData.SaleDetailOrder46 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 46) { var order = orders.ElementAt(46); productOrderData.SaleDetailOrder47 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 47) { var order = orders.ElementAt(47); productOrderData.SaleDetailOrder48 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 48) { var order = orders.ElementAt(48); productOrderData.SaleDetailOrder49 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 49) { var order = orders.ElementAt(49); productOrderData.SaleDetailOrder50 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 50) { var order = orders.ElementAt(50); productOrderData.SaleDetailOrder51 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 51) { var order = orders.ElementAt(51); productOrderData.SaleDetailOrder52 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 52) { var order = orders.ElementAt(52); productOrderData.SaleDetailOrder53 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 53) { var order = orders.ElementAt(53); productOrderData.SaleDetailOrder54 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 54) { var order = orders.ElementAt(54); productOrderData.SaleDetailOrder55 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 55) { var order = orders.ElementAt(55); productOrderData.SaleDetailOrder56 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 56) { var order = orders.ElementAt(56); productOrderData.SaleDetailOrder57 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 57) { var order = orders.ElementAt(57); productOrderData.SaleDetailOrder58 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 58) { var order = orders.ElementAt(58); productOrderData.SaleDetailOrder59 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 59) { var order = orders.ElementAt(59); productOrderData.SaleDetailOrder60 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 60) { var order = orders.ElementAt(60); productOrderData.SaleDetailOrder61 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 61) { var order = orders.ElementAt(61); productOrderData.SaleDetailOrder62 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 62) { var order = orders.ElementAt(62); productOrderData.SaleDetailOrder63 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 63) { var order = orders.ElementAt(63); productOrderData.SaleDetailOrder64 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 64) { var order = orders.ElementAt(64); productOrderData.SaleDetailOrder65 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 65) { var order = orders.ElementAt(65); productOrderData.SaleDetailOrder66 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 66) { var order = orders.ElementAt(66); productOrderData.SaleDetailOrder67 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 67) { var order = orders.ElementAt(67); productOrderData.SaleDetailOrder68 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 68) { var order = orders.ElementAt(68); productOrderData.SaleDetailOrder69 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 69) { var order = orders.ElementAt(69); productOrderData.SaleDetailOrder70 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 70) { var order = orders.ElementAt(70); productOrderData.SaleDetailOrder71 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 71) { var order = orders.ElementAt(71); productOrderData.SaleDetailOrder72 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 72) { var order = orders.ElementAt(72); productOrderData.SaleDetailOrder73 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 73) { var order = orders.ElementAt(73); productOrderData.SaleDetailOrder74 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 74) { var order = orders.ElementAt(74); productOrderData.SaleDetailOrder75 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 75) { var order = orders.ElementAt(75); productOrderData.SaleDetailOrder76 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 76) { var order = orders.ElementAt(76); productOrderData.SaleDetailOrder77 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 77) { var order = orders.ElementAt(77); productOrderData.SaleDetailOrder78 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 78) { var order = orders.ElementAt(78); productOrderData.SaleDetailOrder79 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 79) { var order = orders.ElementAt(79); productOrderData.SaleDetailOrder80 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 80) { var order = orders.ElementAt(80); productOrderData.SaleDetailOrder81 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 81) { var order = orders.ElementAt(81); productOrderData.SaleDetailOrder82 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 82) { var order = orders.ElementAt(82); productOrderData.SaleDetailOrder83 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 83) { var order = orders.ElementAt(83); productOrderData.SaleDetailOrder84 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 84) { var order = orders.ElementAt(84); productOrderData.SaleDetailOrder85 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 85) { var order = orders.ElementAt(85); productOrderData.SaleDetailOrder86 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 86) { var order = orders.ElementAt(86); productOrderData.SaleDetailOrder87 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 87) { var order = orders.ElementAt(87); productOrderData.SaleDetailOrder88 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 88) { var order = orders.ElementAt(88); productOrderData.SaleDetailOrder89 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 89) { var order = orders.ElementAt(89); productOrderData.SaleDetailOrder90 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 90) { var order = orders.ElementAt(90); productOrderData.SaleDetailOrder91 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 91) { var order = orders.ElementAt(91); productOrderData.SaleDetailOrder92 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 92) { var order = orders.ElementAt(92); productOrderData.SaleDetailOrder93 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 93) { var order = orders.ElementAt(93); productOrderData.SaleDetailOrder94 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 94) { var order = orders.ElementAt(94); productOrderData.SaleDetailOrder95 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 95) { var order = orders.ElementAt(95); productOrderData.SaleDetailOrder96 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 96) { var order = orders.ElementAt(96); productOrderData.SaleDetailOrder97 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 97) { var order = orders.ElementAt(97); productOrderData.SaleDetailOrder98 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 98) { var order = orders.ElementAt(98); productOrderData.SaleDetailOrder99 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }
                
                if (orders.Count > 99) { var order = orders.ElementAt(99); productOrderData.SaleDetailOrder100 = order.SaleDetails.FirstOrDefault(x => x.InventoryItem == item); }

                #endregion

                recentOrderData.Add(productOrderData);
            }

            return recentOrderData;
        }

        /// <summary>
        /// Gets the quick order view data.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IList<App_GetQuickOrderViewData_Result> GetQuickOrderViewData(string mode)
        {
            return this.salesRepository.GetQuickOrderViewData(mode);
        }

        /// <summary>
        /// Gets the quick order stock view data.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IList<App_GetQuickOrderStockViewData_Result> GetQuickOrderStockViewData(string type, string stockType, int id)
        {
            return this.salesRepository.GetQuickOrderStockViewData(type, stockType, id);
        }

        /// <summary>
        /// Adds or updates a quick order.
        /// </summary>
        /// <param name="dispatchId">The dispatch id to update.</param>
        /// <param name="bpMasterId">The partner id.</param>
        /// <param name="stocktransactionId">The stock item to add.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>The dispatch id if newly created, and error message if one.</returns>
        public Tuple<string, int> AddOrUpdateQuickOrder(int dispatchId, int bpMasterId, int stocktransactionId, int userId, int deviceId)
        {
            return this.salesRepository.AddOrUpdateQuickOrder(dispatchId, bpMasterId, stocktransactionId, userId,
                deviceId);
        }

        #endregion

        #region returns

        /// <summary> Adds a new stock return (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddARReturn(Sale sale)
        {
            return this.salesRepository.AddARReturn(sale);
        }

        /// <summary>
        /// Updates an existing return (receipt and details).
        /// </summary>
        /// <param name="sale">The return receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARReturn(Sale sale)
        {
            return this.salesRepository.UpdateARReturn(sale);
        }

        /// <summary>
        /// Retrieves the returns (and associated details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetReturnDetails_Result> GetReturnsStatusDetails(int id)
        {
            return this.salesRepository.GetReturnsStatusDetails(id);
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetARReturns(IList<int?> orderStatuses)
        {
            return this.salesRepository.GetARReturns(orderStatuses);
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetARReturnsNonInvoiced()
        {
            return this.salesRepository.GetARReturnsNonInvoiced();
        }

        /// <summary>
        /// Gets the return details.
        /// </summary>
        /// <param name="id">The returns id to search for.</param>
        /// <returns>A return, and it's details.</returns>
        public Sale GetARReturnById(int id)
        {
            return this.salesRepository.GetARReturnById(id);
        }

        /// <summary>
        /// Gets the return details by last edit date.
        /// </summary>
        /// <returns>A return, and it's details.</returns>
        public Sale GetARReturnByLastEdit()
        {
            return this.salesRepository.GetARReturnByLastEdit();
        }

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        public bool UpdateARReturnStatus(Sale sale)
        {
            return this.salesRepository.UpdateARReturnStatus(sale);
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceReturnsDocket(int docketId)
        {
            return this.salesRepository.PriceReturnsDocket(docketId);
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddARReturnInvoice(Sale sale)
        {
            return this.salesRepository.AddARReturnInvoice(sale);
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARReturnInvoice(Sale sale)
        {
            return this.salesRepository.UpdateARReturnInvoice(sale);
        }

        /// <summary>
        /// Retrieves the ap return invoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetARReturnInvoices()
        {
            return this.salesRepository.GetARReturnInvoices();
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetARReturnInvoiceByID(int id)
        {
            return this.salesRepository.GetARReturnInvoiceByID(id);
        }

        /// <summary>
        /// Exports credit note details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportARReturnInvoice(IList<int> invoiceIds)
        {
            return this.salesRepository.ExportARReturnInvoice(invoiceIds);
        }

        #endregion

        #endregion

        #region stock

        /// <summary>
        /// Gets a stock move order by first/last.
        /// </summary>
        /// <param name="id">The id to retrieve the data for.</param>
        /// <returns>The retrieved data.</returns>
        public Sale GetStockMoveOrderByFirstLast(bool first)
        {
            return this.stockRepository.GetStockMoveOrderByFirstLast(first);
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetStockMoveOrders(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            return this.stockRepository.GetStockMoveOrders(orderStatuses, start, end);
        }

        /// <summary>
        /// Gets a stock move order by id.
        /// </summary>
        /// <param name="id">The id to retrieve the data for.</param>
        /// <returns>The retrieved data.</returns>
        public Sale GetStockMoveOrderById(int id)
        {
            return this.stockRepository.GetStockMoveOrderById(id);
        }

        /// <summary>
        /// Changes a batch data (product).
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="currentINMasterID">The id of the product to change.</param>
        /// <param name="inMasterId">The new product.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool ChangeBatchProduct(int prOrderid, int currentINMasterID, int inMasterId)
        {
            return this.stockRepository.ChangeBatchProduct(prOrderid, currentINMasterID, inMasterId);
        }

        /// <summary>
        /// Splits batch stock.
        /// </summary>
        /// <param name="prOrderid">The productionn order.</param>
        /// <param name="stockToSplit">The stock to split.</param>
        /// <param name="splitStock">The split stock.</param>
        /// <returns>Flag, as to successful split or not.</returns>
        public bool SplitBatchStock(int prOrderid, SaleDetail stockToSplit, SaleDetail splitStock)
        {
            return this.stockRepository.SplitBatchStock(prOrderid, stockToSplit, splitStock);
        }

        /// <summary>
        /// Changes a batch data (wgt,qty and product).
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="wgt">The new wgt.</param>
        /// <param name="qty">The new qty.</param>
        /// <param name="inMasterId">The new product.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool ChangeBatchData(int prOrderid, decimal wgt, decimal qty, int inMasterId)
        {
            return this.stockRepository.ChangeBatchData(prOrderid, wgt, qty, inMasterId);
        }

        /// <summary>
        /// Moves a batch to the input location.
        /// </summary>
        /// <param name="fromLocation">The move from location.</param>
        /// <param name="toLocation">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveAllBatchStock(Location fromLocation, Location toLocation)
        {
            return this.stockRepository.MoveAllBatchStock(fromLocation, toLocation);
        }

        /// <summary>
        /// Moves a batch to the input location.
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="location">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveBatchStock(int prOrderid, Location location)
        {
            return this.stockRepository.MoveBatchStock(prOrderid, location);
        }

        /// <summary>
        /// Moves a split batch to the input location, creating a replica batch.
        /// </summary>
        /// <param name="prOrder">The batch id.</param>
        /// <param name="location">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveSplitBatchStock(PROrder prOrder, SaleDetail product, Location location)
        {
            return this.stockRepository.MoveSplitBatchStock(prOrder, product, location);
        }

        /// <summary>
        /// Determines if there's a lighter batch in a lower cooker rack.
        /// </summary>
        /// <param name="batchWgt">The move batch wgt.</param>
        /// <param name="location">The move location.</param>
        /// <returns>Data, as to whether there's a lighter batch wgt.</returns>
        public Tuple<string, decimal, int> IsLighterBatchInCooker(decimal orderId, Location location)
        {
            return this.stockRepository.IsLighterBatchInCooker(orderId, location);
        }

        /// <summary>
        /// Determines if another batch is in the location.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <param name="location">The location to check.</param>
        /// <returns>Flag, as to whether another batch is in the location.</returns>
        public bool IsAnotherBatchInLocation(int batchId, Location location)
        {
            return this.stockRepository.IsAnotherBatchInLocation(batchId, location);
        }

        /// <summary>
        /// Gets the location of the batch.
        /// </summary>
        /// <returns>The batch location id.</returns>
        public Tuple<int, int?> GetBatchStockLocation(int prOrderId)
        {
            return this.stockRepository.GetBatchStockLocation(prOrderId);
        }

        /// <summary>
        /// Moves stock down the slicing/joints production line.
        /// </summary>
        /// <param name="prOrder">The batch id.</param>
        /// <param name="inMasterId">The product to split off.</param>
        /// <param name="locationId">The new location.</param>
        /// <param name="wgt">The weight to split off.</param>
        /// <param name="qty">The qty to split off.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveBatchStockIntoProduction(PROrder prOrder, int inMasterId, int locationId, decimal wgt,
            int qty, int? processId)
        {
            return this.stockRepository.MoveBatchStockIntoProduction(prOrder, inMasterId, locationId, wgt, qty, processId);
        }

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="location">The warehouse to move to.</param>
        /// <param name="subLocation">The sub warehouse to move to.</param>
        /// <param name="barcode">The scanned barcode.</param>
        /// <returns>A stock movement result.</returns>
        public IList<StockTakeData> MoveStock(int location, int? subLocation, string barcode, int orderId, bool logForReprint)
        {
            return this.stockRepository.MoveStock(location, subLocation, barcode, orderId, logForReprint);
        }

        /// <summary>
        /// Adds a stock move order.
        /// </summary>
        /// <param name="order">The order to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public int AddStockMoveOrder(Sale order)
        {
            return this.stockRepository.AddStockMoveOrder(order);
        }

        /// <summary>
        /// Updates a stock move order.
        /// </summary>
        /// <param name="order">The order to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool ChangeStockMoveOrderStatus(Sale order)
        {
            return this.stockRepository.ChangeStockMoveOrderStatus(order);
        }

        /// <summary>
        /// Updates a stock move order.
        /// </summary>
        /// <param name="order">The order to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateStockMoveOrder(Sale order)
        {
            return this.stockRepository.UpdateStockMoveOrder(order);
        }

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="data">The stock move data.</param>
        /// <returns>A stock movement result.</returns>
        public bool UndoMoveStock(StockTakeData data)
        {
            return this.stockRepository.UndoMoveStock(data);
        }

        /// <summary>
        /// Adds a new stock take.
        /// </summary>
        /// <param name="stockTake">The stock take to add.</param>
        /// <returns>A Flag, as to whether the stock take has been added.</returns>
        public bool UpdateStockTake(Model.DataLayer.StockTake stockTake)
        {
            return this.stockRepository.UpdateStockTake(stockTake);
        }

        /// <summary>
        /// Adds a new stock take.
        /// </summary>
        /// <param name="stockTake">The stock take to add.</param>
        /// <returns>A Flag, as to whether the stock take has been added.</returns>
        public bool AddStockTake(Model.DataLayer.StockTake stockTake)
        {
            return this.stockRepository.AddStockTake(stockTake);
        }

        /// <summary>
        /// Retrieves all the application stock takes.
        /// </summary>
        /// <returns>A collection of all the application stock takes.</returns>
        public IList<Nouvem.Model.BusinessObject.StockTake> GetStockTakes()
        {
            return this.stockRepository.GetStockTakes();
        }

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        public void GetStockTakeDetails(Model.BusinessObject.StockTake stockTake)
        {
            this.stockRepository.GetStockTakeDetails(stockTake);
        }

        /// <summary>
        /// Reconciles the stock.
        /// </summary>
        /// <returns>A flag, indicating a successful reconciliation or not.</returns>
        public bool ReconcileStock(Model.BusinessObject.StockTake data)
        {
            return this.stockRepository.ReconcileStock(data);
        }

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        public Tuple<List<App_GetStockTakeData_Result>, List<App_GetStockTakeData_Result>, List<App_GetStockTakeData_Result>, List<App_GetStockTakeData_Result>> GetStockTakeData(Model.BusinessObject.StockTake stockTake)
        {
            var accountedForStock = new List<App_GetStockTakeData_Result>();
            var missingStock = new List<App_GetStockTakeData_Result>();
            var recoveredStock = new List<App_GetStockTakeData_Result>();
            var unKnownStock = new List<App_GetStockTakeData_Result>();

            var data = this.stockRepository.GetStockTakeData(stockTake);
            if (data != null)
            {
                accountedForStock = data.Where(x => x.StockStatus.CompareIgnoringCaseAndWhitespace("Accounted For")).ToList();
                missingStock = data.Where(x => x.StockStatus.CompareIgnoringCaseAndWhitespace("Missing")).ToList();
                recoveredStock = data.Where(x => x.StockStatus.CompareIgnoringCaseAndWhitespace("Recovered")).ToList();
                unKnownStock = data.Where(x => x.StockStatus.CompareIgnoringCaseAndWhitespace("Unknown")).ToList();
            }

            //foreach (var detail in stockTake.StockTakeDetails)
            //{
            //    if (detail.CurrentWarehouseName == string.Empty)
            //    {
            //        // no record of this stock
            //        detail.StockDetailType = StockTakeData.DetailType.Unknown;
            //        unKnownStock.Add(detail);
            //        continue;
            //    }

            //    if (detail.CurrentWarehouseName != detail.WarehouseName || (detail.CurrentWarehouseName == detail.WarehouseName && detail.StockDeletionDate != null))
            //    {
            //        // stock from elsewhere, actually in this location
            //        detail.StockDetailType = StockTakeData.DetailType.Recovered;
            //        recoveredStock.Add(detail);
            //        continue;
            //    }

            //    // stock correctly accounted for
            //    detail.StockDetailType = StockTakeData.DetailType.AccountedFor;
            //    accountedForStock.Add(detail);
            //}

            //stockTake.CutOffDate = stockTake.StockTakeDetails.Max(x => x.CreationDate).ToDate();
            //var dbStock = this.GetStock(stockTake);
            //foreach (var stockTakeData in dbStock)
            //{
            //    if (stockTakeData.Barcode == null)
            //    {
            //        stockTakeData.Barcode = string.Empty;
            //    }

            //    if (accountedForStock.Any(x => x.Barcode.Trim() == stockTakeData.Barcode.Trim()))
            //    {
            //        continue;
            //    }

            //    stockTakeData.StockDetailType = StockTakeData.DetailType.Missing;
            //    missingStock.Add(stockTakeData);
            //}

            //this.stockRepository.StockTakeDetailCheck(missingStock);

            return Tuple.Create(missingStock, recoveredStock, unKnownStock, accountedForStock);
        }

        /// <summary>
        /// Retrieves the stock for the stock reconciliation.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        public IList<StockTakeData> GetStock(Model.BusinessObject.StockTake stockTake)
        {
            return this.stockRepository.GetStock(stockTake);
        }

        /// <summary>
        /// Updates the adjusted stock items.
        /// </summary>
        /// <param name="stock">All the stock items.</param>
        /// <param name="transactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool AdjustStock(IList<ProductData> stock, NouTransactionType transactionType, Warehouse warehouse)
        {
            return this.stockRepository.AdjustStock(stock, transactionType, warehouse);
        }

        /// <summary>
        /// Checks the dispatched stock against the production stock.
        /// </summary>
        /// <returns>Flag, as to successful run or not.</returns>
        public bool CheckStock()
        {
            return this.stockRepository.CheckStock();
        }

        /// <summary>
        /// Prices a group of stocktransactions.
        /// </summary>
        /// <param name="ids">The comman delimited stock ids.</param>
        /// <param name="price">The price to apply.</param>
        /// <returns>Flag, as to successful pricing or not.</returns>
        public bool PriceStocktransactions(string ids, decimal price)
        {
            return this.stockRepository.PriceStocktransactions(ids, price);
        }

        /// <summary>
        /// Gets the batch/product stock levels.
        /// </summary>
        /// <returns>Batch/Product stock levels.</returns>
        public int? GetSerialByCarcassNo(string carcassNo)
        {
            return this.stockRepository.GetSerialByCarcassNo(carcassNo);
        }

        /// <summary>
        /// Moves/Splits batch stock.
        /// </summary>
        /// <param name="stockdata">The batch stock data.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveBatchStock(IList<StockTakeData> stockdata)
        {
            return this.stockRepository.MoveBatchStock(stockdata);
        }

        /// <summary>
        /// Gets the batch/product stock levels.
        /// </summary>
        /// <returns>Batch/Product stock levels.</returns>
        public IList<StockDetail> GetProductStock(IList<InventoryItem> items)
        {
            var devices = this.GetDevices();
            var edits = this.stockRepository.GetTransactionEdits();
            var stock = this.stockRepository.GetStock();
            foreach (var stockDetail in stock)
            {
                stockDetail.StockDetails = edits.Where(x => x.StockDetailID == stockDetail.StockDetailID).ToList();
                foreach (var stockDetailStockDetail in stockDetail.StockDetails)
                {
                    var device = devices.FirstOrDefault(x => x.DeviceID == stockDetailStockDetail.DeviceMasterID);
                    if (device != null)
                    {
                        stockDetailStockDetail.Device = device.DeviceName;
                    }
                }
            }

            var productIds = stock.Select(x => x.ProductID).ToList();
            foreach (var item in items)
            {
                if (productIds.Contains(item.Master.INMasterID))
                {
                    continue;
                }

                stock.Add(new StockDetail
                {
                    TransactionQty = 0,
                    TransactionWeight = 0,
                    ProductID = item.Master.INMasterID,
                    NotInStock = true,
                    StockDetails = new List<StockDetail>()
                });
            }

            return stock;
        }
        
        /// <summary>
        /// Updates batch/product stock levels.
        /// </summary>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateStock(IList<StockDetail> stocks)
        {
            return this.stockRepository.UpdateStock(stocks);
        }

        #endregion

        #region system message

        /// <summary>
        /// Add a new message.
        /// </summary>
        /// <param name="category">The category of the message (eg: error)</param>
        /// <param name="message">The information message to log.</param>
        public void WriteMessage(MessageType category, string message)
        {
            this.systemMessageRepository.Write(category, message);
        }

        /// <summary>
        /// Gets the system messages.
        /// </summary>
        /// <returns>A collection of system messages.</returns>
        public IList<Information> GetMessages()
        {
            return this.systemMessageRepository.SystemMessages;
        }

        #endregion

        #region Test

        /// <summary>
        /// Test the database connection.
        /// </summary>
        /// <returns>A flag, indicating a successful connection or not.</returns>
        public bool TestDatabase()
        {
            return this.userRepository.TestDatabase();
        }

        #endregion

        #region traceability

        /// <summary>
        /// Gets the allocation data for the input template.
        /// </summary>
        /// <returns>The associated template data.</returns>
        public IList<AttributeAllocationData> GetAttributeAllocationData(int templateId)
        {
            return this.traceabilityRepository.GetAttributeAllocationData(templateId);
        }

        /// <summary>
        /// Gets the template groups.
        /// </summary>
        /// <returns>The template groups.</returns>
        public IList<AttributeTemplateGroup> GetTemplateGroups()
        {
            return this.traceabilityRepository.GetTemplateGroups();
        }

        /// <summary>
        /// Gets the templates.
        /// </summary>
        /// <returns>The template names.</returns>
        public IList<AttributeTemplateData> GetTemplateNames()
        {
            return this.traceabilityRepository.GetTemplateNames();
        }

        /// <summary>
        /// Method that makes a repository call to add or update an inventory item.
        /// </summary>
        /// <param name="item">The inventory item to add or update.</param>
        /// <param name="snapshot">The inventory item snapshot to compare.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateInventoryItem(InventoryItem item, InventoryItem snapshot)
        {
            if (item.Master.INMasterID == 0)
            {
                // New item
                item.Master.INMasterID = this.itemMasterRepository.AddInventoryItem(item);
                return item.Master.INMasterID > 0;
            }

            return this.itemMasterRepository.UpdateInventoryItem(item);
        }

        /// <summary>
        /// Gets the last edited group.
        /// </summary>
        /// <returns>The last edited group.</returns>
        public INGroup GetInventoryGroupLastEdit()
        {
            return this.itemMasterRepository.GetInventoryGroupLastEdit();
        }

        /// <summary>
        /// Gets the last edited spc.
        /// </summary>
        /// <returns>The last edited spec.</returns>
        public ProductionSpecification GetsProductSpecificationLastEdit()
        {
            return this.itemMasterRepository.GetsProductSpecificationLastEdit();
        }

        /// <summary>
        /// Gets the last edited spc.
        /// </summary>
        /// <returns>The last edited spec.</returns>
        public ProductionSpecification GetsProductSpecificationById(int id)
        {
            return this.itemMasterRepository.GetsProductSpecificationById(id);
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public byte[] GetImage(int id)
        {
            return this.itemMasterRepository.GetImage(id);
        }

        /// <summary>
        /// Gets a collection of specs linked to a product.
        /// </summary>
        /// <returns>A collection of specs linked to a product.</returns>
        public IList<Sale> GetsSearchProductSpecifications()
        {
            return this.itemMasterRepository.GetsSearchProductSpecifications();
        }

        /// <summary>
        /// Gets the product/partner spec.
        /// </summary>
        /// <returns>Gets the product/partner spec.</returns>
        public ProductionSpecification GetsProductSpecificationByPartner(int inmasterid, int bpmasterid)
        {
            return this.itemMasterRepository.GetsProductSpecificationByPartner(inmasterid, bpmasterid);
        }

        /// <summary>
        /// Gets a collection of specs linked to a product.
        /// </summary>
        /// <param name="inmasterid">The product id.</param>
        /// <returns>A collection of specs linked to a product.</returns>
        public IList<ProductionSpecification> GetsProductSpecifications(int inmasterid)
        {
            return this.itemMasterRepository.GetsProductSpecifications(inmasterid);
        }

        /// <summary>
        /// Adds a new specification.
        /// </summary>
        /// <param name="spec">The spec to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddOrUpdateProductSpecification(ProductionSpecification spec)
        {
            return this.itemMasterRepository.AddOrUpdateProductSpecification(spec);
        }

        /// <summary>
        /// Adds or updates departments..
        /// </summary>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool AddOrUpdateDepartments(IList<DepartmentLookup> departments)
        {
            return this.itemMasterRepository.AddOrUpdateDepartments(departments);
        }

        /// <summary>
        /// Method which returns the non deleted departmentlook ups.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        public IList<DepartmentLookUp> GetDepartmentLookUps(int deviceID)
        {
            return this.itemMasterRepository.GetDepartmentLookUps(deviceID);
        }

        /// <summary>
        /// Gets the associated device allowable departments.
        /// </summary>
        public HashSet<int> GetDeviceDepartmentIds(int deviceId)
        {
            var deviceDepartments = new HashSet<int>();
            var lookUps =
                this.GetDepartmentLookUps(deviceId);
            foreach (var lookup in lookUps)
            {
                deviceDepartments.Add(lookup.DepartmentID);
            };

            return deviceDepartments;
        }

        /// <summary>
        /// Method which returns the non deleted departmentlook ups.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        public IList<DepartmentLookup> GetDeviceDepartments(int deviceID)
        {
            var lookUps = this.GetDepartmentLookUps(deviceID);
            var departments = this.GetDepartments();
            return (from department in departments
                    select new DepartmentLookup
                    {
                        DepartmentID = department.DepartmentID,
                        DepartmentName = department.Name,
                        Code = department.Code,
                        IsSelected =  lookUps.Any(x => x.DeviceMasterID == deviceID && x.DepartmentID == department.DepartmentID)
                    }).ToList();
        }

        /// <summary>
        /// Swaps 2 products sort index.
        /// </summary>
        /// <param name="productA">Product A.</param>
        /// <param name="productB">Product B.</param>
        /// <returns>Flag, as to successful swap or not.</returns>
        public bool SwapSortIndexes(InventoryItem productA, InventoryItem productB)
        {
            return this.itemMasterRepository.SwapSortIndexes(productA, productB);
        }

        /// <summary>
        /// Gets the next product code in the db.
        /// </summary>
        /// <returns>The next code.</returns>
        public int? GetNextProductCode()
        {
            return this.itemMasterRepository.GetNextProductCode();
        }

        /// <summary>
        /// Swaps 2 products groups sort index.
        /// </summary>
        /// <param name="groupA">Group A.</param>
        /// <param name="groupB">Group B.</param>
        /// <returns>Flag, as to successful swap or not.</returns>
        public bool SwapGroupSortIndexes(INGroup groupA, INGroup groupB)
        {
            return this.itemMasterRepository.SwapGroupSortIndexes(groupA, groupB);
        }

        /// <summary>
        /// Method which returns the inventory item associated with the id.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        public InventoryItem GetInventoryItemById(int id)
        {
            return this.itemMasterRepository.GetInventoryItemById(id);
        }

        /// <summary>
        /// Method which returns the product linked to the input linked tare..
        /// </summary>
        /// <returns>A linked product id.</returns>
        public INMaster GetLinkedProductTareProduct(int tareId)
        {
            return this.itemMasterRepository.GetLinkedProductTareProduct(tareId);
        }

        /// <summary>
        /// Method that removes an attachment from the database. </summary>
        /// <param name="attachment">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool RemoveAttachment(INAttachment attachment)
        {
            return this.itemMasterRepository.DeleteAttachment(attachment);
        }

        /// <summary>
        /// Retrieve all the traceability template allocations.
        /// </summary>
        /// <returns>A collection of traceability template allocations.</returns>
        public IList<ViewTraceabilityTemplateAllocation> GetViewTraceabilityTemplateAllocations()
        {
            return this.traceabilityRepository.GetViewTraceabilityTemplateAllocations();
        }

        /// <summary>
        /// Retrieve all the traceability masters.
        /// </summary>
        /// <returns>A collection of traceability masters.</returns>
        public IList<ViewTraceabilityMaster> GetTraceabilityMasters()
        {
            return this.traceabilityRepository.GetTraceabilityMasters();
        }

        /// <summary>
        /// Retrieve all the system traceability types.
        /// </summary>
        /// <returns>A collection of traceability types.</returns>
        public IList<NouTraceabilityType> GetTraceabilityTypes()
        {
            return this.traceabilityRepository.GetTraceabilityTypes();
        }

        /// <summary>
        /// Adds or updates traceability masters.
        /// </summary>
        /// <param name="traceabilityMasters">The traceability masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateTraceabilityMasters(IList<ViewTraceabilityMaster> traceabilityMasters)
        {
            return this.traceabilityRepository.AddOrUpdateTraceabilityMasters(traceabilityMasters);
        }

        /// <summary>
        /// Adds or updates traceability names.
        /// </summary>
        /// <param name="traceabilityNames">The traceability names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateTraceabilityTemplateNames(IList<TraceabilityTemplateName> traceabilityNames)
        {
            return this.traceabilityRepository.AddOrUpdateTraceabilityTemplateNames(traceabilityNames);
        }

        /// <summary>
        /// Retrieve all the system traceability template names.
        /// </summary>
        /// <returns>A collection of traceability template names.</returns>
        public IList<TraceabilityTemplateName> GetTraceabilityTemplateNames()
        {
            return this.traceabilityRepository.GetTraceabilityTemplateNames();
        }

        /// <summary>
        /// Retrieve all the system traceability template allocations.
        /// </summary>
        /// <returns>A collection of traceability template allocations.</returns>
        public IList<TraceabilityTemplateAllocation> GetTraceabilityTemplateAllocations()
        {
            return this.traceabilityRepository.GetTraceabilityTemplateAllocations();
        }

        /// <summary>
        /// Adds a traceability master to a template.
        /// </summary>
        /// <param name="traceabilityMasterId">The id of the traceability master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddTraceabilityToTemplate(int traceabilityMasterId, int templateId)
        {
            return this.traceabilityRepository.AddTraceabilityToTemplate(traceabilityMasterId, templateId);
        }

        /// <summary>
        /// Removes a traceability master from a template.
        /// </summary>
        /// <param name="traceabilityMasterId">The id of the traceability master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        public bool RemoveTraceabilityFromTemplate(int traceabilityMasterId, int templateId)
        {
            return this.traceabilityRepository.RemoveTraceabilityFromTemplate(traceabilityMasterId, templateId);
        }

        /// <summary>
        /// Adds a quality master to a template.
        /// </summary>
        /// <param name="traceabilityMasters">The collection of the quality masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddTraceabilitysToTemplate(IList<ViewTraceabilityMaster> traceabilityMasters, int templateId)
        {
            return this.traceabilityRepository.AddTraceabilitysToTemplate(traceabilityMasters, templateId);
        }

        /// <summary>
        /// Retrieve all the system traceability data.
        /// </summary>
        /// <returns>A collection of traceability data.</returns>
        public Dictionary<Tuple<int, string>, List<TraceabilityData>> GetTraceabilityData()
        {
            return this.traceabilityRepository.GetTraceabilityData()
                .GroupBy(x => Tuple.Create(x.TraceabilityNameID, x.TraceabilityMethod)).ToDictionary(key => key.Key, value => value.ToList());
        }

        /// <summary>
        /// Gets the traceability methods.
        /// </summary>
        /// <returns>A collection of traceability methods.</returns>
        public IList<NouTraceabilityMethod> GetTraceabilityMethods()
        {
            return this.traceabilityRepository.GetTraceabilityMethods();
        }

        /// <summary>
        /// Retrieves the input product stock data.
        /// </summary>
        /// <param name="productId">The product to retrieve the stock data for.</param>
        /// <returns>A collection of stock data.</returns>
        public IList<ProductStockData> GetProductStockData(int productId)
        {
            return this.itemMasterRepository.GetProductStockData(productId);
        }

        /// <summary>
        /// Retrieves the input product stock data.
        /// </summary>
        /// <param name="productId">The product to retrieve the stock data for.</param>
        /// <returns>A collection of stock data.</returns>
        public IList<ProductStockData> GetMRPData(int productId, decimal openingWgt, decimal openingQty)
        {
            var mrpData = new List<ProductStockData>();
            var data = this.itemMasterRepository.GetMRPData(productId);
            if (data != null)
            {
                foreach (var result in data)
                {
                    mrpData.Add(new ProductStockData
                    {
                        ReferenceDate = result.ReferenceDate.ToDate(),
                        AllocatedQty = result.QtyDueOut,
                        AllocatedWgt = result.WgtDueOut,
                        DueInQty = result.QtytDueIn,
                        DueInWgt = result.WgtDueIn
                    });
                }
            }

            var today = mrpData.FirstOrDefault(x => x.ReferenceDate == DateTime.Today);
            if (today == null)
            {
                mrpData.Add(new ProductStockData
                {
                    ReferenceDate = DateTime.Today,
                    AllocatedQty = 0,
                    AllocatedWgt = 0,
                    DueInQty = 0,
                    DueInWgt = 0,
                    OpeningWgt = openingWgt,
                    OpeningQty = openingQty,
                    ClosingWgt = openingWgt,
                    ClosingQty = openingQty,
                });
            }
            else
            {
                today.OpeningWgt = openingWgt;
                today.OpeningQty = openingQty;
                today.ClosingWgt = (today.OpeningWgt + today.DueInWgt) -
                                                    today.AllocatedWgt;
                today.ClosingQty = (today.OpeningQty + today.DueInQty) -
                                   today.AllocatedQty;
            }

            var localData = mrpData.OrderBy(x => x.ReferenceDate).ToList();

            for (int i = 0; i < localData.Count; i++)
            {
                if (i > 0)
                {
                    localData.ElementAt(i).OpeningWgt = localData.ElementAt(i - 1).ClosingWgt;
                    localData.ElementAt(i).OpeningQty = localData.ElementAt(i - 1).ClosingQty;
                    localData.ElementAt(i).ClosingWgt = (localData.ElementAt(i).OpeningWgt + localData.ElementAt(i).DueInWgt) -
                                                        localData.ElementAt(i).AllocatedWgt;
                    localData.ElementAt(i).ClosingQty = (localData.ElementAt(i).OpeningQty + localData.ElementAt(i).DueInQty) -
                                                        localData.ElementAt(i).AllocatedQty;
                }
            }

            return mrpData;
        }

        #endregion

        #region transaction

        /// <summary>
        /// Checks if the dispatch line has transactions recorded against it.
        /// </summary>
        /// <param name="lineId">The product line.</param>
        /// <returns>The line id.</returns>
        public bool DoesProductLineContainTransactions(int lineId)
        {
            return this.transactionRepository.DoesProductLineContainTransactions(lineId);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionIncProduct(int serial)
        {
            return this.transactionRepository.GetStockTransactionIncProduct(serial);
        }

        /// <summary>
        /// Gets an out of butchery transaction data.
        /// </summary>
        /// <param name="serial">The serial number.</param>
        /// <returns>A stoc detail object containing out of butchery data.</returns>
        public StockDetail GetButcheryTransactionData(int serial)
        {
            return this.transactionRepository.GetButcheryTransactionData(serial);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionIncDeleted(int serial)
        {
            return this.transactionRepository.GetStockTransactionIncDeleted(serial);
        }

        /// <summary>
        /// Gets the last attribute for the input module and batch.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockDetail GetLastBatchAttributeData(int batchNumberId, int deviceId, int nouTransactionTypeId)
        {
            return this.transactionRepository.GetLastBatchAttributeData(batchNumberId, deviceId, nouTransactionTypeId);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionIncIntakeId(int serial)
        {
            return this.transactionRepository.GetStockTransactionIncIntakeId(serial);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransaction(int serial)
        {
            return this.transactionRepository.GetStockTransaction(serial);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionFromAttribute(int attributeId)
        {
            return this.transactionRepository.GetStockTransactionFromAttribute(attributeId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="detail">The goods receipt data containing the stock transaction.</param>
        /// <param name="viewType">The module type.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        public bool UpdateTransactionWeight(IList<GoodsIn> transactions, ViewType viewType)
        {
            return this.transactionRepository.UpdateTransactionWeight(transactions, viewType);
        }

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        /// <param name="stockTakeId">The stock take the detail belongs to.</param>
        /// <param name="warehouseId">The current location.</param>
        /// <returns>A stock take detail result.</returns>
        public StockTakeData AddToStockTake(string barcode, int stockTakeId, int warehouseId)
        {
            return this.stockRepository.AddToStockTake(barcode, stockTakeId, warehouseId);
        }

        /// <summary>
        /// Removes a stock take detail.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        /// <returns>A flag, as to a successful removal or not.</returns>
        public bool RemoveFromStockTake(string barcode)
        {
            return this.stockRepository.RemoveFromStockTake(barcode);
        }

        /// <summary>
        /// Gets the product data between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        public IList<ReportStockTransactionData_Result> GetStockTransactionData(int customerId, DateTime start, DateTime end)
        {
            return this.transactionRepository.GetStockTransactionData(customerId, start, end);
        }

        /// <summary>
        /// Retrieves the batch traceability data.
        /// </summary>
        /// <param name="batchNo">The batch no to get the for.</param>
        /// <returns>Batch trace data.</returns>
        public GoodsReceiptData GetProductionOrderBatchData(ProductionData order)
        {
            return this.transactionRepository.GetProductionOrderBatchData(order);
        }

        /// <summary>
        /// Gets and sets the transaction data for the order details.
        /// </summary>
        /// <param name="details">The order details.</param>
        /// <returns>The transaction data for the input order.</returns>
        public void GetTransactionData(IList<SaleDetail> details)
        {
            this.transactionRepository.GetTransactionData(details);
        }

        /// <summary>
        /// Gets the last transaction for the input module.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockTransactionData GetLastTransactionData(int batchNumberId)
        {
            return this.transactionRepository.GetLastTransactionData(batchNumberId);
        }

        /// <summary>
        /// Gets the last transaction for the input module.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockTransactionData GetLastTransactionDataByModule(int batchNumberId, int deviceId,
            int nouTransactionTypeId)
        {
            return this.transactionRepository.GetLastTransactionDataByModule(batchNumberId, deviceId,
                nouTransactionTypeId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="data">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        public GoodsReceiptData GetTransactionData(GoodsReceiptData data)
        {
            return this.transactionRepository.GetTransactionData(data);
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<Sale> GetTransactionsForReports(int from, int to)
        {
            return this.transactionRepository.GetTransactionsForReports(from, to);
        }

        /// <summary>
        /// Retrieves the into production transaction.
        /// </summary>
        /// <param name="serial">The label serial.</param>
        /// <returns>A stocktransaction.</returns>
        public StockTransaction GetIntoProductionTransaction(int serial)
        {
            return this.transactionRepository.GetIntoProductionTransaction(serial);
        }

        /// <summary>
        /// Retrieves the last transaction in a production batch.
        /// </summary>
        /// <param name="prOrderId">The batch id.</param>
        /// <returns>The last transaction id in a production batch.</returns>
        public int GetLastTransactionInBatch(int prOrderId)
        {
            return this.transactionRepository.GetLastTransactionInBatch(prOrderId);
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <param name="deviceId">The device id to search transactions.</param>
        /// <param name="addToStock">The add to stock flag.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<StockTransactionData> GetTransactions(int deviceId, int nouTRansactionTypeId)
        {
            var transactions = new List<StockTransactionData>();
            var trans = this.stockRepository.GetTransactions(deviceId, nouTRansactionTypeId);

            if (trans == null)
            {
                return transactions;
            }

            transactions = (from transaction in trans
                    select new StockTransactionData
                    {
                        Transaction = new StockTransaction
                        {
                            StockTransactionID = transaction.StockTransactionID,
                            Serial = transaction.Serial,
                            AttributeID = transaction.AttributeID,
                            StockTransactionID_Container = transaction.StockTransactionID_Container,
                            IsBox = transaction.IsBox,
                            TransactionWeight = transaction.TransactionWeight,
                            TransactionQTY = transaction.TransactionQTY,
                            GrossWeight = transaction.GrossWeight,
                            TransactionDate = transaction.TransactionDate,
                            INMasterID = transaction.INMasterID,
                            StoredLabelID = transaction.StoredLabelID,
                            Deleted = transaction.Deleted
                        },

                        INMasterId = transaction.INMasterID,
                        Product = transaction.Product,
                        ReferenceID = transaction.Reference,
                        Reference = transaction.Reference1,
                        Reference2 = transaction.Reference2
                    })
                .ToList();

            return transactions;
        }

        /// <summary>
        /// Adds dispatch transactions to a pallet.
        /// </summary>
        /// <param name="ids">The dispatch transactions.</param>
        /// <param name="palletId">The pallet id.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool UpdateDispatchPalletTransactions(HashSet<int> ids, int palletId)
        {
            return this.transactionRepository.UpdateDispatchPalletTransactions(ids, palletId);
        }

        /// <summary>
        /// Checks if the dispatch line has transactions recorded against it.
        /// </summary>
        /// <param name="lineId">The product line.</param>
        /// <returns>The line id.</returns>
        public bool DoesProductionProductLineContainTransactions(int orderId, int inmasterid)
        {
            return this.transactionRepository.DoesProductionProductLineContainTransactions(orderId, inmasterid);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockDetail GetStockTransactionBySerial(int serial, int nouTransactionTypeId)
        {
            return this.transactionRepository.GetStockTransactionBySerial(serial, nouTransactionTypeId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionByComments(string serialNo, int inmasterid)
        {
            return this.transactionRepository.GetTransactionByComments(serialNo, inmasterid);
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public StockTransaction RecreateTransactionWithWgt(StockTransaction transaction, decimal wgt, int warehouseId, int? prOrderId = null)
        {
            transaction.WarehouseID = warehouseId;
            transaction.TransactionWeight = wgt;
            transaction.GrossWeight = wgt + transaction.Tare.ToDecimal();
            transaction.UserMasterID = NouvemGlobal.UserId;
            transaction.DeviceMasterID = NouvemGlobal.DeviceId;
            transaction.TransactionDate = DateTime.Now;
            if (prOrderId.HasValue)
            {
                transaction.MasterTableID = prOrderId;
                transaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
            }

            return this.transactionRepository.RecreateTransactionWithWgt(transaction);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionUncomsumed(int serial)
        {
            return this.transactionRepository.GetStockTransactionUncomsumed(serial);
        }

        /// <summary>
        /// Unconsumes a stock transaction.
        /// </summary>
        /// <param name="transactionId">A flag, indicating a successful unconsumeor not.</param>
        public bool UnConsumeTransaction(int transactionId)
        {
            return this.transactionRepository.UnConsumeTransaction(transactionId);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionById(int transId)
        {
            return this.transactionRepository.GetStockTransactionById(transId);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockDetail GetStockTransactionByComments(string comments, int warehouseid)
        {
            return this.transactionRepository.GetStockTransactionByComments(comments, warehouseid);
        }
        
        /// <summary>
        /// Unconsumes a stock transaction.
        /// </summary>
        /// <param name="transactionId">A flag, indicating a successful unconsumeor not.</param>
        public bool UnConsumePalletTransaction(int transactionId)
        {
            return this.transactionRepository.UnConsumePalletTransaction(transactionId);
        }

        /// <summary>
        /// Adds a pallet transaction to goods in.
        /// </summary>
        /// <param name="goodsInId">The receipt detail id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public int CreatePalletTransaction(int goodsInId)
        {
            return this.transactionRepository.CreatePalletTransaction(goodsInId);
        }

        /// <summary>
        /// Gets the last transaction for the input id.
        /// </summary>
        /// <param name="id">The transaction id.</param>
        /// <returns>The last transaction for the input id</returns>
        public StockTransactionData GetTransactionDataById(int id)
        {
            return this.transactionRepository.GetTransactionDataById(id);
        }

        /// <summary>
        /// Adds a pallet transaction.
        /// </summary>
        /// <param name="pallet">The pallet to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddPallet(PalletStock pallet)
        {
            return this.transactionRepository.AddPallet(pallet);
        }

        /// <summary>
        /// Gets a carcass number from the input attribute id.
        /// </summary>
        /// <param name="attributeId">The label attribute id.</param>
        /// <returns>A corresponding carcass no.</returns>
        public int GetCarcassNo(int attributeId)
        {
            return this.transactionRepository.GetCarcassNo(attributeId);
        }

        /// <summary>
        /// Adds weight from a deleted transaction back to the carcass it came from.
        /// </summary>
        /// <param name="transaction">The transaction deleted.</param>
        /// <returns>A flag, indicating a successful weight addition or not.</returns>
        public bool AddWeightBackToCarcass(StockTransaction transaction)
        {
            return this.transactionRepository.AddWeightBackToCarcass(transaction);
        }

        /// <summary>
        /// Gets the stored label printer number.
        /// </summary>
        /// <param name="labelId">The label id.</param>
        /// <returns>The stored label printer number.</returns>
        public int? GetTransactionPrinter(int labelId)
        {
            return this.transactionRepository.GetTransactionPrinter(labelId);
        }

        /// <summary>
        /// Retrieves the transactions for the goods in line.
        /// </summary>
        /// <param name="apGoodsReceiptId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        public IList<StockTransactionData> GetTransactionsForIntakeLine(int apGoodsReceiptId)
        {
            return this.transactionRepository.GetTransactionsForIntakeLine(apGoodsReceiptId);
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public App_GetProductionStockData_Result GetProductionOrderStockData(string transId)
        {
            return this.transactionRepository.GetProductionOrderStockData(transId);
        }

        /// <summary>
        /// Gets the last attribute for the stock item.
        /// </summary>
        /// <param name="serial">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockDetail GetSerialAttributeData(int serial)
        {
            return this.transactionRepository.GetSerialAttributeData(serial);
        }

        /// <summary>
        /// Adds/remove stock from a pallet.
        /// </summary>
        /// <param name="id">The stock id.</param>
        /// <param name="palletId">The pallet id.</param>
        /// <param name="scanOn">The scan on/off flag.</param>
        /// <returns>Flag, as to successful scan on/off.</returns>
        public StockDetail UpdatePalletTransaction(int id, StockDetail pallet, bool scanOn, bool ignoreConsume)
        {
            return this.transactionRepository.UpdatePalletTransaction(id, pallet, scanOn, ignoreConsume);
        }

        /// <summary>
        /// Gets the stock on a pallet.
        /// </summary>
        /// <param name="id">The pallet id.</param>
        /// <returns>All the stock on a pallet.</returns>
        public IList<StockDetail> GetPalletDetails(int id, bool scanOn)
        {
            return this.transactionRepository.GetPalletDetails(id, scanOn);
        }

        /// <summary>
        /// Retrieves the transactions for the into production line.
        /// </summary>
        /// <param name="prOrderId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        public IList<StockTransactionData> GetTransactionsForIntoProductionLine(int prOrderId)
        {
            return this.transactionRepository.GetTransactionsForIntoProductionLine(prOrderId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForLinkedCarcassSerialSplit(int serialNo)
        {
            return this.transactionRepository.GetTransactionDataForLinkedCarcassSerialSplit(serialNo);
        }

        /// <summary>
        /// Retrieves the transactions for the ispatch line.
        /// </summary>
        /// <param name="dispatchLineId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        public IList<StockTransactionData> GetTransactionsForDispatchLine(int dispatchLineId)
        {
            return this.transactionRepository.GetTransactionsForDispatchLine(dispatchLineId);
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="serial">The label serial.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<StockTransactionData> GetTransaction(int serial, int transId)
        {
            return this.transactionRepository.GetTransaction(serial, transId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForLinkedCarcassSerial(int serialNo, bool uniqueBarcode = true)
        {
            return this.transactionRepository.GetTransactionDataForLinkedCarcassSerial(serialNo, uniqueBarcode);
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public bool AddStockTransaction(StockTransaction transaction)
        {
            return this.transactionRepository.AddStockTransaction(transaction);
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public int AddTransaction(StockTransaction transaction)
        {
            return this.transactionRepository.AddTransaction(transaction);
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public int AddPalletTransaction(StockTransaction transaction)
        {
            var id = this.transactionRepository.AddTransaction(transaction);
            if (id > 0)
            {
                this.AddPallet(new PalletStock {StockTransactionID = id, Consumed = DateTime.Now});
            }

            return id;
        }

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public StoredLabel GetLabelImages(int transId)
        {
            return this.transactionRepository.GetLabelImages(transId);
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<StockTransactionData> GetTransactions(int transactionsToTake)
        {
            return this.transactionRepository.GetTransactions(transactionsToTake);
        }

        /// <summary>
        /// Updates edited transactions.
        /// </summary>
        /// <param name="transactions">The transactions to edit.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateTransactions(IList<StockDetail> transactions)
        {
            return this.transactionRepository.UpdateTransactions(transactions);
        }

        /// <summary>
        /// Gets a set of transactions.
        /// </summary>
        /// <param name="intakeId">An associated intake/lairage id.</param>
        /// <param name="prOrderId">An associated production id.</param>
        /// <param name="dispatchId">An associated dispatch id.</param>
        /// <param name="serial">A individual transaction to search for.</param>
        /// <param name="start">The start of a range of dates to search from.</param>
        /// <param name="end">The end of a range od dates to search to.</param>
        /// <returns>A set of transactions.</returns>
        public IList<StockDetail> GetTransactions(
            int? intakeId = null,
            int? prOrderId = null,
            int? dispatchId = null,
            int? serial = null,
            DateTime? start = null,
            DateTime? end = null)
        {
            var devices = this.GetDevices();
            var trans = this.transactionRepository.GetTransactions(intakeId, prOrderId, dispatchId, serial, start, end);
            foreach (var tran in trans.SelectMany(x => x.StockDetails))
            {
                var device = devices.FirstOrDefault(x => x.DeviceID == tran.DeviceMasterID);
                if (device != null)
                {
                    tran.Device = device.DeviceName;
                }
            }

            return trans;
        }

        /// <summary>
        /// Deletes a transaction and it's associated traceability data.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public string DeleteTransaction(StockTransactionData transaction)
        {
            return this.transactionRepository.DeleteTransaction(transaction);
        }

        /// <summary>
        /// Deletes a transaction only. No other processing.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public bool DeleteTransactionOnly(StockTransactionData transaction)
        {
            return this.transactionRepository.DeleteTransactionOnly(transaction);
        }

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public Tuple<int, string, StockTransaction> GetTransactionAndItems(StockTransactionData transaction)
        {
            return this.transactionRepository.GetTransactionAndItems(transaction);
        }

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transactionId">The transaction to find its docket for.</param>
        /// <returns>A flag, indicating a successful find or not.</returns>
        public Tuple<int, string, StockTransaction> GetTransactionDocket(int transactionId)
        {
            return this.transactionRepository.GetTransactionDocket(transactionId);
        }

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public bool DeleteTransactionAndItems(StockTransactionData transaction)
        {
            return this.transactionRepository.DeleteTransactionAndItems(transaction);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockAttribute GetTransactionAttributesCarcassSerial(int serialNo)
        {
            return this.transactionRepository.GetTransactionAttributesCarcassSerial(serialNo);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="data">The goods receipt data containing the stock transaction.</param>
        /// <param name="useId">flag, as to whether the stocktransaction search is by id or serial..</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        public bool UpdateTransactionData(SaleDetail data, bool useId = true)
        {
            return this.transactionRepository.UpdateTransactionData(data, useId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <param name="module">The relevant module.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForSerial(int serialNo, ViewType module = ViewType.ARDispatch, bool useAttribute = false, int? splitStockId = null)
        {
            return this.transactionRepository.GetTransactionDataForSerial(serialNo, module, useAttribute, splitStockId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public bool IsCarcassSplitAnimal(string serialNo)
        {
            return this.transactionRepository.IsCarcassSplitAnimal(serialNo);
        }

        ///// <summary>
        ///// Gets and sets the transaction data for the input stock transaction serial no.
        ///// </summary>
        ///// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        ///// <returns>The transaction data for the input stock transaction serial no.</returns>
        //public GoodsReceiptData GetTransactionDataForSerialIntoProduction(int serialNo)
        //{
        //    return this.transactionRepository.GetTransactionDataForSerialIntoProduction(serialNo);
        //}

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForIntoProduction(int serialNo)
        {
            return this.transactionRepository.GetTransactionDataForIntoProduction(serialNo);
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForIntoProductionByComments(string serialNo, int inmasterid)
        {
            return this.transactionRepository.GetTransactionDataForIntoProductionByComments(serialNo, inmasterid);
        }

        /// <summary>
        /// Gets and sets the disptached transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <param name="arDispatchId">The ar dispatch id.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetDispatchedTransactionDataForSerial(int serialNo, int arDispatchId)
        {
            return this.transactionRepository.GetDispatchedTransactionDataForSerial(serialNo, arDispatchId);
        }

        /// <summary>
        /// Gets and sets the transaction data for the dispatch order details.
        /// </summary>
        /// <param name="details">The order details.</param>
        /// <returns>The transaction data for the input order.</returns>
        public void GetDispatchTransactionData(IList<SaleDetail> details)
        {
            this.transactionRepository.GetDispatchTransactionData(details);
        }

        /// <summary>
        /// Gets and sets the into production transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The serial number to search for.</param>
        /// <param name="prOrderId">The ar dispatch id.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetIntoProductionTransactionDataForSerial(int serialNo, int prOrderId)
        {
            return this.transactionRepository.GetIntoProductionTransactionDataForSerial(serialNo, prOrderId);
        }

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <param name="labelIds">The label ids.</param>
        /// <param name="images">The label images.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public bool AddLabelToTransaction(int transId, string labelIds, List<byte[]> images, int printerNo = 1)
        {
            var storedLabel = new StoredLabel();
            storedLabel.PrinterNo = printerNo;
            var imageCount = images.Count;
            if (imageCount > 0)
            {
                storedLabel.Label1 = images.ElementAt(0);
            }

            if (imageCount > 1)
            {
                storedLabel.Label2 = images.ElementAt(1);
            }

            if (imageCount > 2)
            {
                storedLabel.Label3 = images.ElementAt(2);
            }

            if (imageCount > 3)
            {
                storedLabel.Label4 = images.ElementAt(3);
            }

            if (imageCount > 4)
            {
                storedLabel.Label5 = images.ElementAt(4);
            }

            if (imageCount > 5)
            {
                storedLabel.Label6 = images.ElementAt(5);
            }

            if (imageCount > 6)
            {
                storedLabel.Label7 = images.ElementAt(6);
            }

            if (imageCount > 7)
            {
                storedLabel.Label8 = images.ElementAt(7);
            }

            return this.transactionRepository.AddLabelToTransaction(transId, labelIds, storedLabel);
        }

        #endregion

        #region uom

        /// <summary>
        /// Retrieve all the cuoms.
        /// </summary>
        /// <returns>A collection of uoms.</returns>
        public IList<UOMMaster> GetUOMMaster()
        {
            return this.uomRepository.GetUOMMaster();
        }

        /// <summary>
        /// Add or updates the uoms list.
        /// </summary>
        /// <param name="uoms">The uoms to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateUOMMasters(IList<UOMMaster> uoms)
        {
            return this.uomRepository.AddOrUpdateUOMMasters(uoms);
        }

        #endregion

        #region user

        /// <summary>
        /// Creates the window and search grid settings for the logged in user.
        /// </summary>
        /// <param name="userId">The logged in user id.</param>
        public void CreateWindowSettings(int userId)
        {
            if (ApplicationSettings.TouchScreenModeOnly)
            {
                return;
            }

            var windowSettings = this.userRepository.GetWindowSettings(userId);
            try
            {
                #region window settings

                foreach (var windowSetting in windowSettings)
                {
                    if (windowSetting.Name.Trim().Equals(ViewType.AllPrices.ToString()))
                    {
                        WindowSettings.AllPricesTop = windowSetting.PositionTop;
                        WindowSettings.AllPricesLeft = windowSetting.PositionLeft;
                        WindowSettings.AllPricesHeight = windowSetting.PositionHeight;
                        WindowSettings.AllPricesWidth = windowSetting.PositionWidth;
                        WindowSettings.AllPricesMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TransactionEditor.ToString()))
                    {
                        WindowSettings.TransactionEditorTop = windowSetting.PositionTop;
                        WindowSettings.TransactionEditorLeft = windowSetting.PositionLeft;
                        WindowSettings.TransactionEditorHeight = windowSetting.PositionHeight;
                        WindowSettings.TransactionEditorWidth = windowSetting.PositionWidth;
                        WindowSettings.TransactionEditorMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.BatchEdit.ToString()))
                    {
                        WindowSettings.BatchEditTop = windowSetting.PositionTop;
                        WindowSettings.BatchEditLeft = windowSetting.PositionLeft;
                        WindowSettings.BatchEditHeight = windowSetting.PositionHeight;
                        WindowSettings.BatchEditWidth = windowSetting.PositionWidth;
                        WindowSettings.BatchEditMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.BatchEditor.ToString()))
                    {
                        WindowSettings.BatchEditorTop = windowSetting.PositionTop;
                        WindowSettings.BatchEditorLeft = windowSetting.PositionLeft;
                        WindowSettings.BatchEditorHeight = windowSetting.PositionHeight;
                        WindowSettings.BatchEditorWidth = windowSetting.PositionWidth;
                        WindowSettings.BatchEditorMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.RecipeSearch.ToString()))
                    {
                        WindowSettings.RecipeSearchTop = windowSetting.PositionTop;
                        WindowSettings.RecipeSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.RecipeSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.RecipeSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.RecipeSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.BatchSetUp.ToString()))
                    {
                        WindowSettings.BatchSetUpTop = windowSetting.PositionTop;
                        WindowSettings.BatchSetUpLeft = windowSetting.PositionLeft;
                        WindowSettings.BatchSetUpHeight = windowSetting.PositionHeight;
                        WindowSettings.BatchSetUpWidth = windowSetting.PositionWidth;
                        WindowSettings.BatchSetUpMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Specifications.ToString()))
                    {
                        WindowSettings.SpecificationsTop = windowSetting.PositionTop;
                        WindowSettings.SpecificationsLeft = windowSetting.PositionLeft;
                        WindowSettings.SpecificationsHeight = windowSetting.PositionHeight;
                        WindowSettings.SpecificationsWidth = windowSetting.PositionWidth;
                        WindowSettings.SpecificationsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.StockMove.ToString()))
                    {
                        WindowSettings.StockMoveTop = windowSetting.PositionTop;
                        WindowSettings.StockMoveLeft = windowSetting.PositionLeft;
                        WindowSettings.StockMoveHeight = windowSetting.PositionHeight;
                        WindowSettings.StockMoveWidth = windowSetting.PositionWidth;
                        WindowSettings.StockMoveMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.StockReconciliation.ToString()))
                    {
                        WindowSettings.StockReconciliationTop = windowSetting.PositionTop;
                        WindowSettings.StockReconciliationLeft = windowSetting.PositionLeft;
                        WindowSettings.StockReconciliationHeight = windowSetting.PositionHeight;
                        WindowSettings.StockReconciliationWidth = windowSetting.PositionWidth;
                        WindowSettings.StockReconciliationMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.StockTake.ToString()))
                    {
                        WindowSettings.StockTakeTop = windowSetting.PositionTop;
                        WindowSettings.StockTakeLeft = windowSetting.PositionLeft;
                        WindowSettings.StockTakeHeight = windowSetting.PositionHeight;
                        WindowSettings.StockTakeWidth = windowSetting.PositionWidth;
                        WindowSettings.StockTakeMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.StockReconciliation.ToString()))
                    {
                        Settings.Default.StockReconciliationTop = windowSetting.PositionTop;
                        Settings.Default.StockReconciliationLeft = windowSetting.PositionLeft;
                        Settings.Default.StockReconciliationHeight = windowSetting.PositionHeight;
                        Settings.Default.StockReconciliationWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SpecialPrices.ToString()))
                    {
                        WindowSettings.SpecialPricesTop = windowSetting.PositionTop;
                        WindowSettings.SpecialPricesLeft = windowSetting.PositionLeft;
                        WindowSettings.SpecialPricesHeight = windowSetting.PositionHeight;
                        WindowSettings.SpecialPricesWidth = windowSetting.PositionWidth;
                        WindowSettings.SpecialPricesMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.VatSetUp.ToString()))
                    {
                        Settings.Default.VatSetUpTop = windowSetting.PositionTop;
                        Settings.Default.VatSetUpLeft = windowSetting.PositionLeft;
                        Settings.Default.VatSetUpHeight = windowSetting.PositionHeight;
                        Settings.Default.VatSetUpWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DocumentTrail.ToString()))
                    {
                        Settings.Default.DocumentTrailTop = windowSetting.PositionTop;
                        Settings.Default.DocumentTrailLeft = windowSetting.PositionLeft;
                        Settings.Default.DocumentTrailHeight = windowSetting.PositionHeight;
                        Settings.Default.DocumentTrailWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Invoice.ToString()))
                    {
                        WindowSettings.InvoiceTop = windowSetting.PositionTop;
                        WindowSettings.InvoiceLeft = windowSetting.PositionLeft;
                        WindowSettings.InvoiceHeight = windowSetting.PositionHeight;
                        WindowSettings.InvoiceWidth = windowSetting.PositionWidth;
                        WindowSettings.InvoiceMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PurchaseInvoice.ToString()))
                    {
                        WindowSettings.APInvoiceTop = windowSetting.PositionTop;
                        WindowSettings.APInvoiceLeft = windowSetting.PositionLeft;
                        WindowSettings.APInvoiceHeight = windowSetting.PositionHeight;
                        WindowSettings.APInvoiceWidth = windowSetting.PositionWidth;
                        WindowSettings.APInvoiceMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TransactionSearchData.ToString()))
                    {
                        WindowSettings.TransactionSearchDataTop = windowSetting.PositionTop;
                        WindowSettings.TransactionSearchDataLeft = windowSetting.PositionLeft;
                        WindowSettings.TransactionSearchDataHeight = windowSetting.PositionHeight;
                        WindowSettings.TransactionSearchDataWidth = windowSetting.PositionWidth;
                        WindowSettings.TransactionSearchDataMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ProductionSearchData.ToString()))
                    {
                        WindowSettings.ProductionSearchDataTop = windowSetting.PositionTop;
                        WindowSettings.ProductionSearchDataLeft = windowSetting.PositionLeft;
                        WindowSettings.ProductionSearchDataHeight = windowSetting.PositionHeight;
                        WindowSettings.ProductionSearchDataWidth = windowSetting.PositionWidth;
                        WindowSettings.ProductionSearchDataMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.LabelAssociation.ToString()))
                    {
                        WindowSettings.LabelAssociationTop = windowSetting.PositionTop;
                        WindowSettings.LabelAssociationLeft = windowSetting.PositionLeft;
                        WindowSettings.LabelAssociationHeight = windowSetting.PositionHeight;
                        WindowSettings.LabelAssociationWidth = windowSetting.PositionWidth;
                        WindowSettings.LabelAssociationMaximised= windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ARDispatch.ToString()))
                    {
                        WindowSettings.DispatchTop = windowSetting.PositionTop;
                        WindowSettings.DispatchLeft = windowSetting.PositionLeft;
                        WindowSettings.DispatchHeight = windowSetting.PositionHeight;
                        WindowSettings.DispatchWidth = windowSetting.PositionWidth;
                        WindowSettings.DispatchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SyncPartners.ToString()))
                    {
                        WindowSettings.SyncPartnersTop = windowSetting.PositionTop;
                        WindowSettings.SyncPartnersLeft = windowSetting.PositionLeft;
                        WindowSettings.SyncPartnersHeight = windowSetting.PositionHeight;
                        WindowSettings.SyncPartnersWidth = windowSetting.PositionWidth;
                        WindowSettings.SyncPartnersMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SyncProducts.ToString()))
                    {
                        WindowSettings.SyncProductsTop = windowSetting.PositionTop;
                        WindowSettings.SyncProductsLeft = windowSetting.PositionLeft;
                        WindowSettings.SyncProductsHeight = windowSetting.PositionHeight;
                        WindowSettings.SyncProductsWidth = windowSetting.PositionWidth;
                        WindowSettings.SyncProductsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SyncPrices.ToString()))
                    {
                        WindowSettings.SyncPricesTop = windowSetting.PositionTop;
                        WindowSettings.SyncPricesLeft = windowSetting.PositionLeft;
                        WindowSettings.SyncPricesHeight = windowSetting.PositionHeight;
                        WindowSettings.SyncPricesWidth = windowSetting.PositionWidth;
                        WindowSettings.SyncPricesMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DispatchContainer.ToString()))
                    {
                        WindowSettings.DispatchContainerTop = windowSetting.PositionTop;
                        WindowSettings.DispatchContainerLeft = windowSetting.PositionLeft;
                        WindowSettings.DispatchContainerHeight = windowSetting.PositionHeight;
                        WindowSettings.DispatchContainerWidth = windowSetting.PositionWidth;
                        WindowSettings.DispatchContainerMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ARDispatch2.ToString()))
                    {
                        WindowSettings.Dispatch2Top = windowSetting.PositionTop;
                        WindowSettings.Dispatch2Left = windowSetting.PositionLeft;
                        WindowSettings.Dispatch2Height = windowSetting.PositionHeight;
                        WindowSettings.Dispatch2Width = windowSetting.PositionWidth;
                        WindowSettings.Dispatch2Maximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ReportViewer.ToString()))
                    {
                        WindowSettings.ReportViewerTop = windowSetting.PositionTop;
                        WindowSettings.ReportViewerLeft = windowSetting.PositionLeft;
                        WindowSettings.ReportViewerHeight = windowSetting.PositionHeight;
                        WindowSettings.ReportViewerWidth = windowSetting.PositionWidth;
                        WindowSettings.ReportViewerMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ARDispatchDetails.ToString()))
                    {
                        WindowSettings.ARDispatchDetailsTop = windowSetting.PositionTop;
                        WindowSettings.ARDispatchDetailsLeft = windowSetting.PositionLeft;
                        WindowSettings.ARDispatchDetailsHeight = windowSetting.PositionHeight;
                        WindowSettings.ARDispatchDetailsWidth = windowSetting.PositionWidth;
                        WindowSettings.ARDispatchDetailsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.IntakeDetails.ToString()))
                    {
                        WindowSettings.IntakeDetailsTop = windowSetting.PositionTop;
                        WindowSettings.IntakeDetailsLeft = windowSetting.PositionLeft;
                        WindowSettings.IntakeDetailsHeight = windowSetting.PositionHeight;
                        WindowSettings.IntakeDetailsWidth = windowSetting.PositionWidth;
                        WindowSettings.IntakeDetailsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.WarehouseLocation.ToString()))
                    {
                        Settings.Default.WarehouseLocationTop = windowSetting.PositionTop;
                        Settings.Default.WarehouseLocationLeft = windowSetting.PositionLeft;
                        Settings.Default.WarehouseLocationHeight = windowSetting.PositionHeight;
                        Settings.Default.WarehouseLocationWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TransactionDetails.ToString()))
                    {
                        Settings.Default.TransactionDetailsTop = windowSetting.PositionTop;
                        Settings.Default.TransactionDetailsLeft = windowSetting.PositionLeft;
                        Settings.Default.TransactionDetailsHeight = windowSetting.PositionHeight;
                        Settings.Default.TransactionDetailsWidth = windowSetting.PositionWidth;
                        continue;
                    }                                      

                    if (windowSetting.Name.Trim().Equals(ViewType.BPGroupSetUp.ToString()))
                    {
                        Settings.Default.BPGroupSetUpTop = windowSetting.PositionTop;
                        Settings.Default.BPGroupSetUpLeft = windowSetting.PositionLeft;
                        Settings.Default.BPGroupSetUpHeight = windowSetting.PositionHeight;
                        Settings.Default.BPGroupSetUpWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.APOrder.ToString()))
                    {
                        Settings.Default.APOrderTop = windowSetting.PositionTop;
                        Settings.Default.APOrderLeft = windowSetting.PositionLeft;
                        Settings.Default.APOrderHeight = windowSetting.PositionHeight;
                        Settings.Default.APOrderWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.APQuote.ToString()))
                    {
                        Settings.Default.APQuoteTop = windowSetting.PositionTop;
                        Settings.Default.APQuoteLeft = windowSetting.PositionLeft;
                        Settings.Default.APQuoteHeight = windowSetting.PositionHeight;
                        Settings.Default.APQuoteWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.APReceipt.ToString()))
                    {
                        Settings.Default.APReceiptTop = windowSetting.PositionTop;
                        Settings.Default.APReceiptLeft = windowSetting.PositionLeft;
                        Settings.Default.APReceiptHeight = windowSetting.PositionHeight;
                        Settings.Default.APReceiptWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.APReceiptDetails.ToString()))
                    {
                        Settings.Default.APReceiptDetailsTop = windowSetting.PositionTop;
                        Settings.Default.APReceiptDetailsLeft = windowSetting.PositionLeft;
                        Settings.Default.APReceiptDetailsHeight = windowSetting.PositionHeight;
                        Settings.Default.APReceiptDetailsWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Quality.ToString()))
                    {
                        Settings.Default.QualityTop = windowSetting.PositionTop;
                        Settings.Default.QualityLeft = windowSetting.PositionLeft;
                        Settings.Default.QualityHeight = windowSetting.PositionHeight;
                        Settings.Default.QualityWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.QualityTemplateName.ToString()))
                    {
                        Settings.Default.QualityTemplateNameTop = windowSetting.PositionTop;
                        Settings.Default.QualityTemplateNameLeft = windowSetting.PositionLeft;
                        Settings.Default.QualityTemplateNameHeight = windowSetting.PositionHeight;
                        Settings.Default.QualityTemplateNameWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.QualityTemplateAllocation.ToString()))
                    {
                        Settings.Default.QualityTemplateAllocationTop = windowSetting.PositionTop;
                        Settings.Default.QualityTemplateAllocationLeft = windowSetting.PositionLeft;
                        Settings.Default.QualityTemplateAllocationHeight = windowSetting.PositionHeight;
                        Settings.Default.QualityTemplateAllocationWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Map.ToString()))
                    {
                        Settings.Default.MapTop = windowSetting.PositionTop;
                        Settings.Default.MapLeft = windowSetting.PositionLeft;
                        Settings.Default.MapHeight = windowSetting.PositionHeight;
                        Settings.Default.MapWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Quote.ToString()))
                    {
                        WindowSettings.QuoteTop = windowSetting.PositionTop;
                        WindowSettings.QuoteLeft = windowSetting.PositionLeft;
                        WindowSettings.QuoteHeight = windowSetting.PositionHeight;
                        WindowSettings.QuoteWidth = windowSetting.PositionWidth;
                        WindowSettings.QuoteMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Order.ToString()))
                    {
                        WindowSettings.OrderTop = windowSetting.PositionTop;
                        WindowSettings.OrderLeft = windowSetting.PositionLeft;
                        WindowSettings.OrderHeight = windowSetting.PositionHeight;
                        WindowSettings.OrderWidth = windowSetting.PositionWidth;
                        WindowSettings.OrderMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ARReturns.ToString()))
                    {
                        WindowSettings.ReturnsTop = windowSetting.PositionTop;
                        WindowSettings.ReturnsLeft = windowSetting.PositionLeft;
                        WindowSettings.ReturnsHeight = windowSetting.PositionHeight;
                        WindowSettings.ReturnsWidth = windowSetting.PositionWidth;
                        WindowSettings.ReturnsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.QuickOrder.ToString()))
                    {
                        WindowSettings.QuickOrderTop = windowSetting.PositionTop;
                        WindowSettings.QuickOrderLeft = windowSetting.PositionLeft;
                        WindowSettings.QuickOrderHeight = windowSetting.PositionHeight;
                        WindowSettings.QuickOrderWidth = windowSetting.PositionWidth;
                        WindowSettings.QuickOrderMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.IntakeOrder.ToString()))
                    {
                        WindowSettings.IntakeOrderTop = windowSetting.PositionTop;
                        WindowSettings.IntakeOrderLeft = windowSetting.PositionLeft;
                        WindowSettings.IntakeOrderHeight = windowSetting.PositionHeight;
                        WindowSettings.IntakeOrderWidth = windowSetting.PositionWidth;
                        WindowSettings.IntakeOrderMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Recipe.ToString()))
                    {
                        WindowSettings.RecipeTop = windowSetting.PositionTop;
                        WindowSettings.RecipeLeft = windowSetting.PositionLeft;
                        WindowSettings.RecipeHeight = windowSetting.PositionHeight;
                        WindowSettings.RecipeWidth = windowSetting.PositionWidth;
                        WindowSettings.RecipeMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.LairageOrder.ToString()))
                    {
                        WindowSettings.LairageOrderTop = windowSetting.PositionTop;
                        WindowSettings.LairageOrderLeft = windowSetting.PositionLeft;
                        WindowSettings.LairageOrderHeight = windowSetting.PositionHeight;
                        WindowSettings.LairageOrderWidth = windowSetting.PositionWidth;
                        WindowSettings.LairageOrderMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.LairageIntake.ToString()))
                    {
                        WindowSettings.LairageIntakeTop = windowSetting.PositionTop;
                        WindowSettings.LairageIntakeLeft = windowSetting.PositionLeft;
                        WindowSettings.LairageIntakeHeight = windowSetting.PositionHeight;
                        WindowSettings.LairageIntakeWidth = windowSetting.PositionWidth;
                        WindowSettings.LairageIntakeMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.LairageAnimalsSearch.ToString()))
                    {
                        WindowSettings.LairageAnimalsSearchTop = windowSetting.PositionTop;
                        WindowSettings.LairageAnimalsSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.LairageAnimalsSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.LairageAnimalsSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.LairageAnimalsSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.CarcassDispatch.ToString()))
                    {
                        WindowSettings.CarcassDispatchTop = windowSetting.PositionTop;
                        WindowSettings.CarcassDispatchLeft = windowSetting.PositionLeft;
                        WindowSettings.CarcassDispatchHeight = windowSetting.PositionHeight;
                        WindowSettings.CarcassDispatchWidth = windowSetting.PositionWidth;
                        WindowSettings.CarcassDispatchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.LairageSearch.ToString()))
                    {
                        WindowSettings.LairageSearchTop = windowSetting.PositionTop;
                        WindowSettings.LairageSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.LairageSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.LairageSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.LairageSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PaymentDeductions.ToString()))
                    {
                        WindowSettings.PaymentDeductionsTop = windowSetting.PositionTop;
                        WindowSettings.PaymentDeductionsLeft = windowSetting.PositionLeft;
                        WindowSettings.PaymentDeductionsHeight = windowSetting.PositionHeight;
                        WindowSettings.PaymentDeductionsWidth = windowSetting.PositionWidth;
                        WindowSettings.PaymentDeductionsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PricingMatrix.ToString()))
                    {
                        WindowSettings.PricingMatrixTop = windowSetting.PositionTop;
                        WindowSettings.PricingMatrixLeft = windowSetting.PositionLeft;
                        WindowSettings.PricingMatrixHeight = windowSetting.PositionHeight;
                        WindowSettings.PricingMatrixWidth = windowSetting.PositionWidth;
                        WindowSettings.PricingMatrixMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PaymentProposalCreation.ToString()))
                    {
                        WindowSettings.PaymentProposalTop = windowSetting.PositionTop;
                        WindowSettings.PaymentProposalLeft = windowSetting.PositionLeft;
                        WindowSettings.PaymentProposalHeight = windowSetting.PositionHeight;
                        WindowSettings.PaymentProposalWidth = windowSetting.PositionWidth;
                        WindowSettings.PaymentProposalMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Payment.ToString()))
                    {
                        WindowSettings.PaymentTop = windowSetting.PositionTop;
                        WindowSettings.PaymentLeft = windowSetting.PositionLeft;
                        WindowSettings.PaymentHeight = windowSetting.PositionHeight;
                        WindowSettings.PaymentWidth = windowSetting.PositionWidth;
                        WindowSettings.PaymentMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }


                    if (windowSetting.Name.Trim().Equals(ViewType.PaymentProposalOpen.ToString()))
                    {
                        WindowSettings.PaymentProposalSearchTop = windowSetting.PositionTop;
                        WindowSettings.PaymentProposalSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.PaymentProposalSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.PaymentProposalSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.PaymentProposalSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Order2.ToString()))
                    {
                        WindowSettings.Order2Top = windowSetting.PositionTop;
                        WindowSettings.Order2Left = windowSetting.PositionLeft;
                        WindowSettings.Order2Height = windowSetting.PositionHeight;
                        WindowSettings.Order2Width = windowSetting.PositionWidth;
                        WindowSettings.Order2Maximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.EposSettings.ToString()))
                    {
                        Settings.Default.EposSettingsTop = windowSetting.PositionTop;
                        Settings.Default.EposSettingsLeft = windowSetting.PositionLeft;
                        Settings.Default.EposSettingsHeight = windowSetting.PositionHeight;
                        Settings.Default.EposSettingsWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Region.ToString()))
                    {
                        Settings.Default.RegionTop = windowSetting.PositionTop;
                        Settings.Default.RegionLeft = windowSetting.PositionLeft;
                        Settings.Default.RegionHeight = windowSetting.PositionHeight;
                        Settings.Default.RegionWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Route.ToString()))
                    {
                        Settings.Default.RouteTop = windowSetting.PositionTop;
                        Settings.Default.RouteLeft = windowSetting.PositionLeft;
                        Settings.Default.RouteHeight = windowSetting.PositionHeight;
                        Settings.Default.RouteWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DocumentType.ToString()))
                    {
                        Settings.Default.DocumentTypeTop = windowSetting.PositionTop;
                        Settings.Default.DocumentTypeLeft = windowSetting.PositionLeft;
                        Settings.Default.DocumentTypeHeight = windowSetting.PositionHeight;
                        Settings.Default.DocumentTypeWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DocumentNumber.ToString()))
                    {
                        Settings.Default.DocumentNumberTop = windowSetting.PositionTop;
                        Settings.Default.DocumentNumberLeft = windowSetting.PositionLeft;
                        Settings.Default.DocumentNumberHeight = windowSetting.PositionHeight;
                        Settings.Default.DocumentNumberWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Audit.ToString()))
                    {
                        Settings.Default.AuditTop = windowSetting.PositionTop;
                        Settings.Default.AuditLeft = windowSetting.PositionLeft;
                        Settings.Default.AuditHeight = windowSetting.PositionHeight;
                        Settings.Default.AuditWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.BPMaster.ToString()))
                    {
                        WindowSettings.BPMasterTop = windowSetting.PositionTop;
                        WindowSettings.BPMasterLeft = windowSetting.PositionLeft;
                        WindowSettings.BPMasterHeight = windowSetting.PositionHeight;
                        WindowSettings.BPMasterWidth = windowSetting.PositionWidth;
                        WindowSettings.BPMasterMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ContainerSetUp.ToString()))
                    {
                        Settings.Default.ContainerTop = windowSetting.PositionTop;
                        Settings.Default.ContainerLeft = windowSetting.PositionLeft;
                        Settings.Default.ContainerHeight = windowSetting.PositionHeight;
                        Settings.Default.ContainerWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.CountrySetUp.ToString()))
                    {
                        Settings.Default.CountryMasterTop = windowSetting.PositionTop;
                        Settings.Default.CountryMasterLeft = windowSetting.PositionLeft;
                        Settings.Default.CountryMasterHeight = windowSetting.PositionHeight;
                        Settings.Default.CountryMasterWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Date.ToString()))
                    {
                        Settings.Default.DateTop = windowSetting.PositionTop;
                        Settings.Default.DateLeft = windowSetting.PositionLeft;
                        Settings.Default.DateHeight = windowSetting.PositionHeight;
                        Settings.Default.DateWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DateTemplateAllocation.ToString()))
                    {
                        Settings.Default.DatesAllocationTop = windowSetting.PositionTop;
                        Settings.Default.DatesAllocationLeft = windowSetting.PositionLeft;
                        Settings.Default.DatesAllocationHeight = windowSetting.PositionHeight;
                        Settings.Default.DatesAllocationWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DateTemplateName.ToString()))
                    {
                        Settings.Default.DateNameTop = windowSetting.PositionTop;
                        Settings.Default.DateNameLeft = windowSetting.PositionLeft;
                        Settings.Default.DateNameHeight = windowSetting.PositionHeight;
                        Settings.Default.DateNameWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InventoryGroupSetUp.ToString()))
                    {
                        Settings.Default.INGroupSetUpTop = windowSetting.PositionTop;
                        Settings.Default.INGroupSetUpLeft = windowSetting.PositionLeft;
                        Settings.Default.INGroupSetUpHeight = windowSetting.PositionHeight;
                        Settings.Default.INGroupSetUpWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InventoryMaster.ToString()))
                    {
                        WindowSettings.INMasterTop = windowSetting.PositionTop;
                        WindowSettings.INMasterLeft = windowSetting.PositionLeft;
                        WindowSettings.INMasterHeight = windowSetting.PositionHeight;
                        WindowSettings.INMasterWidth = windowSetting.PositionWidth;
                        WindowSettings.INMasterMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InventoryGroup.ToString()))
                    {
                        WindowSettings.INGroupTop = windowSetting.PositionTop;
                        WindowSettings.INGroupLeft = windowSetting.PositionLeft;
                        WindowSettings.INGroupHeight = windowSetting.PositionHeight;
                        WindowSettings.INGroupWidth = windowSetting.PositionWidth;
                        WindowSettings.INGroupMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InventoryGroupSetUp.ToString()))
                    {
                        WindowSettings.INGroupSetUpTop = windowSetting.PositionTop;
                        WindowSettings.INGroupSetUpLeft = windowSetting.PositionLeft;
                        WindowSettings.INGroupSetUpHeight = windowSetting.PositionHeight;
                        WindowSettings.INGroupSetUpWidth = windowSetting.PositionWidth;
                        WindowSettings.INGroupSetUpMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InventoryGroup.ToString()))
                    {
                        Settings.Default.INGroupTop = windowSetting.PositionTop;
                        Settings.Default.INGroupLeft = windowSetting.PositionLeft;
                        Settings.Default.INGroupHeight = windowSetting.PositionHeight;
                        Settings.Default.INGroupWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.IMPropertySetUp.ToString()))
                    {
                        Settings.Default.IMPropertySetUpTop = windowSetting.PositionTop;
                        Settings.Default.IMPropertySetUpLeft = windowSetting.PositionLeft;
                        Settings.Default.IMPropertySetUpHeight = windowSetting.PositionHeight;
                        Settings.Default.IMPropertySetUpWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.GS1AI.ToString()))
                    {
                        Settings.Default.GS1AITop = windowSetting.PositionTop;
                        Settings.Default.GS1AILeft = windowSetting.PositionLeft;
                        Settings.Default.GS1AIHeight = windowSetting.PositionHeight;
                        Settings.Default.GS1AIWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DeviceSetUp.ToString()))
                    {
                        Settings.Default.DeviceSetUpTop = windowSetting.PositionTop;
                        Settings.Default.DeviceSetUpLeft = windowSetting.PositionLeft;
                        Settings.Default.DeviceSetUpHeight = windowSetting.PositionHeight;
                        Settings.Default.DeviceSetUpWidth = windowSetting.PositionWidth;
                        continue;
                    }


                    if (windowSetting.Name.Trim().Equals(ViewType.LicenseAdmin.ToString()))
                    {
                        Settings.Default.LicenseAdminTop = windowSetting.PositionTop;
                        Settings.Default.LicenseAdminLeft = windowSetting.PositionLeft;
                        Settings.Default.LicenseAdminHeight = windowSetting.PositionHeight;
                        Settings.Default.LicenseAdminWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.LicenseImport.ToString()))
                    {
                        Settings.Default.LicenseImportTop = windowSetting.PositionTop;
                        Settings.Default.LicenseImportLeft = windowSetting.PositionLeft;
                        Settings.Default.LicenseImportHeight = windowSetting.PositionHeight;
                        Settings.Default.LicenseImportWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.MessageHistory.ToString()))
                    {
                        Settings.Default.MessageHistoryTop = windowSetting.PositionTop;
                        Settings.Default.MessageHistoryLeft = windowSetting.PositionLeft;
                        Settings.Default.MessageHistoryHeight = windowSetting.PositionHeight;
                        Settings.Default.MessageHistoryWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Plant.ToString()))
                    {
                        Settings.Default.PlantTop = windowSetting.PositionTop;
                        Settings.Default.PlantLeft = windowSetting.PositionLeft;
                        Settings.Default.PlantHeight = windowSetting.PositionHeight;
                        Settings.Default.PlantWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Warehouse.ToString()))
                    {
                        Settings.Default.WarehouseTop = windowSetting.PositionTop;
                        Settings.Default.WarehouseLeft = windowSetting.PositionLeft;
                        Settings.Default.WarehouseHeight = windowSetting.PositionHeight;
                        Settings.Default.WarehouseWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PricingMaster.ToString()))
                    {
                        WindowSettings.PriceListMasterTop = windowSetting.PositionTop;
                        WindowSettings.PriceListMasterLeft = windowSetting.PositionLeft;
                        WindowSettings.PriceListMasterHeight = windowSetting.PositionHeight;
                        WindowSettings.PriceListMasterWidth = windowSetting.PositionWidth;
                        WindowSettings.PriceListMasterMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Tabs.ToString()))
                    {
                        WindowSettings.TabsTop = windowSetting.PositionTop;
                        WindowSettings.TabsLeft = windowSetting.PositionLeft;
                        WindowSettings.TabsHeight = windowSetting.PositionHeight;
                        WindowSettings.TabsWidth = windowSetting.PositionWidth;
                        WindowSettings.TabsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Notes.ToString()))
                    {
                        WindowSettings.NotesTop = windowSetting.PositionTop;
                        WindowSettings.NotesLeft = windowSetting.PositionLeft;
                        WindowSettings.NotesHeight = windowSetting.PositionHeight;
                        WindowSettings.NotesWidth = windowSetting.PositionWidth;
                        WindowSettings.NotesMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.AttributeMasterSetUp.ToString()))
                    {
                        WindowSettings.AttributeMasterTop = windowSetting.PositionTop;
                        WindowSettings.AttributeMasterLeft = windowSetting.PositionLeft;
                        WindowSettings.AttributeMasterHeight = windowSetting.PositionHeight;
                        WindowSettings.AttributeMasterWidth = windowSetting.PositionWidth;
                        WindowSettings.AttributeMasterMaximised= windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.AttributeTemplateName.ToString()))
                    {
                        WindowSettings.TemplateNameTop = windowSetting.PositionTop;
                        WindowSettings.TemplateNameLeft = windowSetting.PositionLeft;
                        WindowSettings.TemplateNameHeight = windowSetting.PositionHeight;
                        WindowSettings.TemplateNameWidth = windowSetting.PositionWidth;
                        WindowSettings.TemplateNameMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.WeightBandGroup.ToString()))
                    {
                        WindowSettings.WeightBandGroupTop = windowSetting.PositionTop;
                        WindowSettings.WeightBandGroupLeft = windowSetting.PositionLeft;
                        WindowSettings.WeightBandGroupHeight = windowSetting.PositionHeight;
                        WindowSettings.WeightBandGroupWidth = windowSetting.PositionWidth;
                        WindowSettings.WeightBandGroupMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.WeightBandPricing.ToString()))
                    {
                        WindowSettings.WeightBandPricingTop = windowSetting.PositionTop;
                        WindowSettings.WeightBandPricingLeft = windowSetting.PositionLeft;
                        WindowSettings.WeightBandPricingHeight = windowSetting.PositionHeight;
                        WindowSettings.WeightBandPricingWidth = windowSetting.PositionWidth;
                        WindowSettings.WeightBandPricingMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.WeightBand.ToString()))
                    {
                        WindowSettings.WeightBandTop = windowSetting.PositionTop;
                        WindowSettings.WeightBandLeft = windowSetting.PositionLeft;
                        WindowSettings.WeightBandHeight = windowSetting.PositionHeight;
                        WindowSettings.WeightBandWidth = windowSetting.PositionWidth;
                        WindowSettings.WeightBandMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.AttributeTemplateGroup.ToString()))
                    {
                        WindowSettings.TemplateGroupTop = windowSetting.PositionTop;
                        WindowSettings.TemplateGroupLeft = windowSetting.PositionLeft;
                        WindowSettings.TemplateGroupHeight = windowSetting.PositionHeight;
                        WindowSettings.TemplateGroupWidth = windowSetting.PositionWidth;
                        WindowSettings.TemplateGroupMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TemplateAllocation.ToString()))
                    {
                        WindowSettings.AttributeAllocationTop = windowSetting.PositionTop;
                        WindowSettings.AttributeAllocationLeft = windowSetting.PositionLeft;
                        WindowSettings.AttributeAllocationHeight = windowSetting.PositionHeight;
                        WindowSettings.AttributeAllocationWidth = windowSetting.PositionWidth;
                        WindowSettings.AttributeAllocationMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ReportFolders.ToString()))
                    {
                        WindowSettings.ReportFoldersTop = windowSetting.PositionTop;
                        WindowSettings.ReportFoldersLeft = windowSetting.PositionLeft;
                        WindowSettings.ReportFoldersHeight = windowSetting.PositionHeight;
                        WindowSettings.ReportFoldersWidth = windowSetting.PositionWidth;
                        WindowSettings.ReportFoldersMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ReportProcess.ToString()))
                    {
                        WindowSettings.ReportProcessTop = windowSetting.PositionTop;
                        WindowSettings.ReportProcessLeft = windowSetting.PositionLeft;
                        WindowSettings.ReportProcessHeight = windowSetting.PositionHeight;
                        WindowSettings.ReportProcessWidth = windowSetting.PositionWidth;
                        WindowSettings.ReportProcessMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ProductSelection.ToString()))
                    {
                        Settings.Default.ProductSelectionTop = windowSetting.PositionTop;
                        Settings.Default.ProductSelectionLeft = windowSetting.PositionLeft;
                        Settings.Default.ProductSelectionHeight = windowSetting.PositionHeight;
                        Settings.Default.ProductSelectionWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Traceability.ToString()))
                    {
                        Settings.Default.TraceabilityMasterTop = windowSetting.PositionTop;
                        Settings.Default.TraceabilityMasterLeft = windowSetting.PositionLeft;
                        Settings.Default.TraceabilityMasterHeight = windowSetting.PositionHeight;
                        Settings.Default.TraceabilityMasterWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SqlGenerator.ToString()))
                    {
                        Settings.Default.SqlTop = windowSetting.PositionTop;
                        Settings.Default.SqlLeft = windowSetting.PositionLeft;
                        Settings.Default.SqlHeight = windowSetting.PositionHeight;
                        Settings.Default.SqlWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DataGenerator.ToString()))
                    {
                        Settings.Default.DataTop = windowSetting.PositionTop;
                        Settings.Default.DataLeft = windowSetting.PositionLeft;
                        Settings.Default.DataHeight = windowSetting.PositionHeight;
                        Settings.Default.DataWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TraceabilityTemplateAllocation.ToString()))
                    {
                        Settings.Default.TraceabilityAllocationTop = windowSetting.PositionTop;
                        Settings.Default.TraceabilityAllocationLeft = windowSetting.PositionLeft;
                        Settings.Default.TraceabilityAllocationHeight = windowSetting.PositionHeight;
                        Settings.Default.TraceabilityAllocationWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TraceabilityTemplateName.ToString()))
                    {
                        Settings.Default.TraceabilityNameTop = windowSetting.PositionTop;
                        Settings.Default.TraceabilityNameLeft = windowSetting.PositionLeft;
                        Settings.Default.TraceabilityNameHeight = windowSetting.PositionHeight;
                        Settings.Default.TraceabilityNameWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.UOM.ToString()))
                    {
                        Settings.Default.UOMTop = windowSetting.PositionTop;
                        Settings.Default.UOMLeft = windowSetting.PositionLeft;
                        Settings.Default.UOMHeight = windowSetting.PositionHeight;
                        Settings.Default.UOMWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TelesalesSetUp.ToString()))
                    {
                        WindowSettings.TelesalesSetUpTop = windowSetting.PositionTop;
                        WindowSettings.TelesalesSetUpLeft = windowSetting.PositionLeft;
                        WindowSettings.TelesalesSetUpHeight = windowSetting.PositionHeight;
                        WindowSettings.TelesalesSetUpWidth = windowSetting.PositionWidth;
                        WindowSettings.TelesalesSetUpMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Telesales.ToString()))
                    {
                        WindowSettings.TelesalesTop = windowSetting.PositionTop;
                        WindowSettings.TelesalesLeft = windowSetting.PositionLeft;
                        WindowSettings.TelesalesHeight = windowSetting.PositionHeight;
                        WindowSettings.TelesalesWidth = windowSetting.PositionWidth;
                        WindowSettings.TelesalesMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TelesalesEndCall.ToString()))
                    {
                        WindowSettings.TelesalesEndCallTop = windowSetting.PositionTop;
                        WindowSettings.TelesalesEndCallLeft = windowSetting.PositionLeft;
                        WindowSettings.TelesalesEndCallHeight = windowSetting.PositionHeight;
                        WindowSettings.TelesalesEndCallWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Currency.ToString()))
                    {
                        Settings.Default.CurrencyTop = windowSetting.PositionTop;
                        Settings.Default.CurrencyLeft = windowSetting.PositionLeft;
                        Settings.Default.CurrencyHeight = windowSetting.PositionHeight;
                        Settings.Default.CurrencyWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Authorisations.ToString()))
                    {
                        WindowSettings.AuthorisationsTop = windowSetting.PositionTop;
                        WindowSettings.AuthorisationsLeft = windowSetting.PositionLeft;
                        WindowSettings.AuthorisationsHeight = windowSetting.PositionHeight;
                        WindowSettings.AuthorisationsWidth = windowSetting.PositionWidth;
                        continue;
                    }
                    

                    if (windowSetting.Name.Trim().Equals(ViewType.UserSearch.ToString()))
                    {
                        WindowSettings.UserSearchTop = windowSetting.PositionTop;
                        WindowSettings.UserSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.UserSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.UserSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.UserSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.User.ToString()))
                    {
                        WindowSettings.UserTop = windowSetting.PositionTop;
                        WindowSettings.UserLeft = windowSetting.PositionLeft;
                        WindowSettings.UserHeight = windowSetting.PositionHeight;
                        WindowSettings.UserWidth = windowSetting.PositionWidth;
                        WindowSettings.UserMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.UserSetUp.ToString()))
                    {
                        Settings.Default.UserMasterTop = windowSetting.PositionTop;
                        Settings.Default.UserMasterLeft = windowSetting.PositionLeft;
                        Settings.Default.UserMasterHeight = windowSetting.PositionHeight;
                        Settings.Default.UserMasterWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.UserSearchData.ToString()))
                    {
                        Settings.Default.UserSearchDataTop = windowSetting.PositionTop;
                        Settings.Default.UserSearchDataLeft = windowSetting.PositionLeft;
                        Settings.Default.UserSearchDataHeight = windowSetting.PositionHeight;
                        Settings.Default.UserSearchDataWidth = windowSetting.PositionWidth;

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.UserSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SalesSearchData.ToString()))
                    {
                        WindowSettings.SaleSearchDataTop = windowSetting.PositionTop;
                        WindowSettings.SaleSearchDataLeft = windowSetting.PositionLeft;
                        WindowSettings.SaleSearchDataHeight = windowSetting.PositionHeight;
                        WindowSettings.SaleSearchDataWidth = windowSetting.PositionWidth;
                        WindowSettings.SaleSearchDataMaximised = windowSetting.Maximised.ToBool();

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.SalesSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SpecsSearch.ToString()))
                    {
                        WindowSettings.SpecsSearchTop = windowSetting.PositionTop;
                        WindowSettings.SpecsSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.SpecsSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.SpecsSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.SpecsSearchMaximised = windowSetting.Maximised.ToBool();
                        
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.WorkflowAlerts.ToString()))
                    {
                        WindowSettings.WorkflowAlertsSearchTop = windowSetting.PositionTop;
                        WindowSettings.WorkflowAlertsSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.WorkflowAlertsSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.WorkflowAlertsSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.WorkflowAlertsSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }


                    if (windowSetting.Name.Trim().Equals(ViewType.AlertUsersSetUp.ToString()))
                    {
                        WindowSettings.AlertUsersSetUpTop = windowSetting.PositionTop;
                        WindowSettings.AlertUsersSetUpLeft = windowSetting.PositionLeft;
                        WindowSettings.AlertUsersSetUpHeight = windowSetting.PositionHeight;
                        WindowSettings.AlertUsersSetUpWidth = windowSetting.PositionWidth;
                        WindowSettings.AlertUsersSetUpMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.WorkflowSearch.ToString()))
                    {
                        WindowSettings.WorkflowSearchTop = windowSetting.PositionTop;
                        WindowSettings.WorkflowSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.WorkflowSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.WorkflowSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.WorkflowSearchMaximised = windowSetting.Maximised.ToBool();
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.SalesSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.TelesalesSearch.ToString()))
                    {
                        WindowSettings.TelesalesSearchTop = windowSetting.PositionTop;
                        WindowSettings.TelesalesSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.TelesalesSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.TelesalesSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.TelesalesSearchMaximised = windowSetting.Maximised.ToBool();

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.SalesSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.AttributeSearch.ToString()))
                    {
                        WindowSettings.AttributeSearchTop = windowSetting.PositionTop;
                        WindowSettings.AttributeSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.AttributeSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.AttributeSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.AttributeSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.SalesCustomerSearch.ToString()))
                    {
                        WindowSettings.SearchSaleCustomersTop = windowSetting.PositionTop;
                        WindowSettings.SearchSaleCustomersLeft = windowSetting.PositionLeft;
                        WindowSettings.SearchSaleCustomersHeight = windowSetting.PositionHeight;
                        WindowSettings.SearchSaleCustomersWidth = windowSetting.PositionWidth;
                        WindowSettings.SearchSaleCustomersMaximised = windowSetting.Maximised.ToBool();

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.SalesSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.BPSearchData.ToString()))
                    {
                        WindowSettings.SearchDataTop = windowSetting.PositionTop;
                        WindowSettings.SearchDataLeft = windowSetting.PositionLeft;
                        WindowSettings.SearchDataHeight = windowSetting.PositionHeight;
                        WindowSettings.SearchDataWidth = windowSetting.PositionWidth;
                        WindowSettings.SearchDataMaximised = windowSetting.Maximised.ToBool();
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.BPSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.INSearchData.ToString()))
                    {
                        WindowSettings.INSearchDataTop = windowSetting.PositionTop;
                        WindowSettings.INSearchDataLeft = windowSetting.PositionLeft;
                        WindowSettings.INSearchDataHeight = windowSetting.PositionHeight;
                        WindowSettings.INSearchDataWidth = windowSetting.PositionWidth;
                        WindowSettings.INSearchDataMaximised = windowSetting.Maximised.ToBool();
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.INSearchData);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.APReceiptItems.ToString()))
                    {
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.APReceiptItems);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DeviceSearch.ToString()))
                    {
                        Settings.Default.DeviceSearchTop = windowSetting.PositionTop;
                        Settings.Default.DeviceSearchLeft = windowSetting.PositionLeft;
                        Settings.Default.DeviceSearchHeight = windowSetting.PositionHeight;
                        Settings.Default.DeviceSearchWidth = windowSetting.PositionWidth;

                        if (windowSetting.GridData != null)
                        {
                            this.StoreGridSettings(windowSetting.GridData, ViewType.DeviceSearch);
                        }
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PriceListDetail.ToString()))
                    {
                        WindowSettings.PriceListDetailTop = windowSetting.PositionTop;
                        WindowSettings.PriceListDetailLeft = windowSetting.PositionLeft;
                        WindowSettings.PriceListDetailHeight = windowSetting.PositionHeight;
                        WindowSettings.PriceListDetailWidth = windowSetting.PositionWidth;
                        WindowSettings.PriceListDetailMaximised = windowSetting.Maximised.ToBool();

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.PriceListDetail);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InvoiceCreation.ToString()))
                    {
                        Settings.Default.InvoiceCreationTop = windowSetting.PositionTop;
                        Settings.Default.InvoiceCreationLeft = windowSetting.PositionLeft;
                        Settings.Default.InvoiceCreationHeight = windowSetting.PositionHeight;
                        Settings.Default.InvoiceCreationWidth = windowSetting.PositionWidth;

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.InvoiceCreation);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.INInventory.ToString()))
                    {
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.INInventory);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.StockAdjust.ToString()))
                    {
                        WindowSettings.StockAdjustTop = windowSetting.PositionTop;
                        WindowSettings.StockAdjustLeft = windowSetting.PositionLeft;
                        WindowSettings.StockAdjustHeight = windowSetting.PositionHeight;
                        WindowSettings.StockAdjustWidth = windowSetting.PositionWidth;
                        WindowSettings.StockAdjustMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.UserGroup.ToString()))
                    {
                        WindowSettings.UserGroupTop = windowSetting.PositionTop;
                        WindowSettings.UserGroupLeft = windowSetting.PositionLeft;
                        WindowSettings.UserGroupHeight = windowSetting.PositionHeight;
                        WindowSettings.UserGroupWidth = windowSetting.PositionWidth;
                        WindowSettings.UserGroupMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Authorisations.ToString()))
                    {
                        WindowSettings.AuthorisationsTop = windowSetting.PositionTop;
                        WindowSettings.AuthorisationsLeft = windowSetting.PositionLeft;
                        WindowSettings.AuthorisationsHeight = windowSetting.PositionHeight;
                        WindowSettings.AuthorisationsWidth = windowSetting.PositionWidth;
                        WindowSettings.AuthorisationsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Accounts.ToString()))
                    {
                        WindowSettings.AccountsTop = windowSetting.PositionTop;
                        WindowSettings.AccountsLeft = windowSetting.PositionLeft;
                        WindowSettings.AccountsHeight = windowSetting.PositionHeight;
                        WindowSettings.AccountsWidth = windowSetting.PositionWidth;
                        WindowSettings.AccountsMaximised = windowSetting.Maximised.ToBool();

                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.Accounts);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.DeviceSettings.ToString()))
                    {
                        WindowSettings.DeviceSettingsTop = windowSetting.PositionTop;
                        WindowSettings.DeviceSettingsLeft = windowSetting.PositionLeft;
                        WindowSettings.DeviceSettingsHeight = windowSetting.PositionHeight;
                        WindowSettings.DeviceSettingsWidth = windowSetting.PositionWidth;
                        WindowSettings.DeviceSettingsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ImageView.ToString()))
                    {
                        WindowSettings.ImageViewTop = windowSetting.PositionTop;
                        WindowSettings.ImageViewLeft = windowSetting.PositionLeft;
                        WindowSettings.ImageViewHeight = windowSetting.PositionHeight;
                        WindowSettings.ImageViewWidth = windowSetting.PositionWidth;
                        WindowSettings.ImageViewMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.BatchData.ToString()))
                    {
                        WindowSettings.BatchDataTop = windowSetting.PositionTop;
                        WindowSettings.BatchDataLeft = windowSetting.PositionLeft;
                        WindowSettings.BatchDataHeight = windowSetting.PositionHeight;
                        WindowSettings.BatchDataWidth = windowSetting.PositionWidth;
                        WindowSettings.BatchDataMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ApplyPrice.ToString()))
                    {
                        WindowSettings.ApplyPriceTop = windowSetting.PositionTop;
                        WindowSettings.ApplyPriceLeft = windowSetting.PositionLeft;
                        WindowSettings.ApplyPriceHeight = windowSetting.PositionHeight;
                        WindowSettings.ApplyPriceWidth = windowSetting.PositionWidth;
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.MRP.ToString()))
                    {
                        WindowSettings.MRPTop = windowSetting.PositionTop;
                        WindowSettings.MRPLeft = windowSetting.PositionLeft;
                        WindowSettings.MRPHeight = windowSetting.PositionHeight;
                        WindowSettings.MRPWidth = windowSetting.PositionWidth;
                        WindowSettings.MRPMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Specs.ToString()))
                    {
                        WindowSettings.SpecsTop = windowSetting.PositionTop;
                        WindowSettings.SpecsLeft = windowSetting.PositionLeft;
                        WindowSettings.SpecsHeight = windowSetting.PositionHeight;
                        WindowSettings.SpecsWidth = windowSetting.PositionWidth;
                        WindowSettings.SpecsMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.Department.ToString()))
                    {
                        WindowSettings.DepartmentTop = windowSetting.PositionTop;
                        WindowSettings.DepartmentLeft = windowSetting.PositionLeft;
                        WindowSettings.DepartmentHeight = windowSetting.PositionHeight;
                        WindowSettings.DepartmentWidth = windowSetting.PositionWidth;
                        WindowSettings.DepartmentMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.InvoiceCreation.ToString()))
                    {
                        WindowSettings.InvoiceCreationTop = windowSetting.PositionTop;
                        WindowSettings.InvoiceCreationLeft = windowSetting.PositionLeft;
                        WindowSettings.InvoiceCreationHeight = windowSetting.PositionHeight;
                        WindowSettings.InvoiceCreationWidth = windowSetting.PositionWidth;
                        WindowSettings.InvoiceCreationMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if(windowSetting.Name.Trim().Equals(ViewType.APInvoiceCreation.ToString()))
                    {
                        WindowSettings.APInvoiceCreationTop = windowSetting.PositionTop;
                        WindowSettings.APInvoiceCreationLeft = windowSetting.PositionLeft;
                        WindowSettings.APInvoiceCreationHeight = windowSetting.PositionHeight;
                        WindowSettings.APInvoiceCreationWidth = windowSetting.PositionWidth;
                        WindowSettings.APInvoiceCreationMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.CreditNoteCreation.ToString()))
                    {
                        WindowSettings.CreditNoteCreationTop = windowSetting.PositionTop;
                        WindowSettings.CreditNoteCreationLeft = windowSetting.PositionLeft;
                        WindowSettings.CreditNoteCreationHeight = windowSetting.PositionHeight;
                        WindowSettings.CreditNoteCreationWidth = windowSetting.PositionWidth;
                        WindowSettings.CreditNoteCreationMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ReportSetUp.ToString()))
                    {
                        WindowSettings.ReportSetUpTop = windowSetting.PositionTop;
                        WindowSettings.ReportSetUpLeft = windowSetting.PositionLeft;
                        WindowSettings.ReportSetUpHeight = windowSetting.PositionHeight;
                        WindowSettings.ReportSetUpWidth = windowSetting.PositionWidth;
                        WindowSettings.ReportSetUpMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ReportSearch.ToString()))
                    {
                        WindowSettings.ReportSearchTop = windowSetting.PositionTop;
                        WindowSettings.ReportSearchLeft = windowSetting.PositionLeft;
                        WindowSettings.ReportSearchHeight = windowSetting.PositionHeight;
                        WindowSettings.ReportSearchWidth = windowSetting.PositionWidth;
                        WindowSettings.ReportSearchMaximised = windowSetting.Maximised.ToBool();
                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.ReturnsAccounts.ToString()))
                    {
                        WindowSettings.AccountsReturnsTop = windowSetting.PositionTop;
                        WindowSettings.AccountsReturnsLeft = windowSetting.PositionLeft;
                        WindowSettings.AccountsReturnsHeight = windowSetting.PositionHeight;
                        WindowSettings.AccountsReturnsWidth = windowSetting.PositionWidth;
                        WindowSettings.AccountsReturnsMaximised = windowSetting.Maximised.ToBool();
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.Accounts);
                        }

                        continue;
                    }

                    if (windowSetting.Name.Trim().Equals(ViewType.PurchasesAccounts.ToString()))
                    {
                        WindowSettings.AccountsPurchasesTop = windowSetting.PositionTop;
                        WindowSettings.AccountsPurchasesLeft = windowSetting.PositionLeft;
                        WindowSettings.AccountsPurchasesHeight = windowSetting.PositionHeight;
                        WindowSettings.AccountsPurchasesWidth = windowSetting.PositionWidth;
                        WindowSettings.AccountsPurchasesMaximised = windowSetting.Maximised.ToBool();
                        if (windowSetting.GridData != null)
                        {
                            //this.StoreGridSettings(windowSetting.GridData, ViewType.Accounts);
                        }

                        continue;
                    }

                    Settings.Default.Save();
                }

                #endregion
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Saves the users windows settings (position and grid data, if any)
        /// </summary>
        public void SaveWindowSettings()
        {
            if (ApplicationSettings.TouchScreenModeOnly)
            {
                return;
            }

            var userID = NouvemGlobal.LoggedInUser.UserMaster.UserMasterID;
            var windowsSettings = new List<WindowSetting>();

            try
            {
                #region window settings

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TransactionEditor.ToString(),
                    PositionTop = WindowSettings.TransactionEditorTop,
                    PositionLeft = WindowSettings.TransactionEditorLeft,
                    PositionHeight = WindowSettings.TransactionEditorHeight,
                    PositionWidth = WindowSettings.TransactionEditorWidth,
                    Maximised = WindowSettings.TransactionEditorMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.VatSetUp.ToString(),
                    PositionTop = Settings.Default.VatSetUpTop,
                    PositionLeft = Settings.Default.VatSetUpLeft,
                    PositionHeight = Settings.Default.VatSetUpHeight,
                    PositionWidth = Settings.Default.VatSetUpWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.StockMove.ToString(),
                    PositionTop = WindowSettings.StockMoveTop,
                    PositionLeft = WindowSettings.StockMoveLeft,
                    PositionHeight = WindowSettings.StockMoveHeight,
                    PositionWidth = WindowSettings.StockMoveWidth,
                    Maximised = WindowSettings.StockMoveMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.StockReconciliation.ToString(),
                    PositionTop = WindowSettings.StockReconciliationTop,
                    PositionLeft = WindowSettings.StockReconciliationLeft,
                    PositionHeight = WindowSettings.StockReconciliationHeight,
                    PositionWidth = WindowSettings.StockReconciliationWidth,
                    Maximised = WindowSettings.StockReconciliationMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.StockTake.ToString(),
                    PositionTop = WindowSettings.StockTakeTop,
                    PositionLeft = WindowSettings.StockTakeLeft,
                    PositionHeight = WindowSettings.StockTakeHeight,
                    PositionWidth = WindowSettings.StockTakeWidth,
                    Maximised = WindowSettings.StockTakeMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.AllPrices.ToString(),
                    PositionTop = WindowSettings.AllPricesTop,
                    PositionLeft = WindowSettings.AllPricesLeft,
                    PositionHeight = WindowSettings.AllPricesHeight,
                    PositionWidth = WindowSettings.AllPricesWidth,
                    Maximised = WindowSettings.AllPricesMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BatchEdit.ToString(),
                    PositionTop = WindowSettings.BatchEditTop,
                    PositionLeft = WindowSettings.BatchEditLeft,
                    PositionHeight = WindowSettings.BatchEditHeight,
                    PositionWidth = WindowSettings.BatchEditWidth,
                    Maximised = WindowSettings.BatchEditMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BatchEditor.ToString(),
                    PositionTop = WindowSettings.BatchEditorTop,
                    PositionLeft = WindowSettings.BatchEditorLeft,
                    PositionHeight = WindowSettings.BatchEditorHeight,
                    PositionWidth = WindowSettings.BatchEditorWidth,
                    Maximised = WindowSettings.BatchEditorMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.RecipeSearch.ToString(),
                    PositionTop = WindowSettings.RecipeSearchTop,
                    PositionLeft = WindowSettings.RecipeSearchLeft,
                    PositionHeight = WindowSettings.RecipeSearchHeight,
                    PositionWidth = WindowSettings.RecipeSearchWidth,
                    Maximised = WindowSettings.RecipeSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BatchSetUp.ToString(),
                    PositionTop = WindowSettings.BatchSetUpTop,
                    PositionLeft = WindowSettings.BatchSetUpLeft,
                    PositionHeight = WindowSettings.BatchSetUpHeight,
                    PositionWidth = WindowSettings.BatchSetUpWidth,
                    Maximised = WindowSettings.BatchSetUpMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Specifications.ToString(),
                    PositionTop = WindowSettings.SpecificationsTop,
                    PositionLeft = WindowSettings.SpecificationsLeft,
                    PositionHeight = WindowSettings.SpecificationsHeight,
                    PositionWidth = WindowSettings.SpecificationsWidth,
                    Maximised = WindowSettings.SpecificationsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SpecialPrices.ToString(),
                    PositionTop = WindowSettings.SpecialPricesTop,
                    PositionLeft = WindowSettings.SpecialPricesLeft,
                    PositionHeight = WindowSettings.SpecialPricesHeight,
                    PositionWidth = WindowSettings.SpecialPricesWidth,
                    Maximised = WindowSettings.SpecialPricesMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.StockReconciliation.ToString(),
                    PositionTop = Settings.Default.StockReconciliationTop,
                    PositionLeft = Settings.Default.StockReconciliationLeft,
                    PositionHeight = Settings.Default.StockReconciliationHeight,
                    PositionWidth = Settings.Default.StockReconciliationWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DocumentTrail.ToString(),
                    PositionTop = Settings.Default.DocumentTrailTop,
                    PositionLeft = Settings.Default.DocumentTrailLeft,
                    PositionHeight = Settings.Default.DocumentTrailHeight,
                    PositionWidth = Settings.Default.DocumentTrailWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Invoice.ToString(),
                    PositionTop = WindowSettings.InvoiceTop,
                    PositionLeft = WindowSettings.InvoiceLeft,
                    PositionHeight = WindowSettings.InvoiceHeight,
                    PositionWidth = WindowSettings.InvoiceWidth,
                    Maximised = WindowSettings.InvoiceMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PurchaseInvoice.ToString(),
                    PositionTop = WindowSettings.APInvoiceTop,
                    PositionLeft = WindowSettings.APInvoiceLeft,
                    PositionHeight = WindowSettings.APInvoiceHeight,
                    PositionWidth = WindowSettings.APInvoiceWidth,
                    Maximised = WindowSettings.APInvoiceMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TransactionSearchData.ToString(),
                    PositionTop = WindowSettings.TransactionSearchDataTop,
                    PositionLeft = WindowSettings.TransactionSearchDataLeft,
                    PositionHeight = WindowSettings.TransactionSearchDataHeight,
                    PositionWidth = WindowSettings.TransactionSearchDataWidth,
                    Maximised=  WindowSettings.TransactionSearchDataMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ProductionSearchData.ToString(),
                    PositionTop = WindowSettings.ProductionSearchDataTop,
                    PositionLeft = WindowSettings.ProductionSearchDataLeft,
                    PositionHeight = WindowSettings.ProductionSearchDataHeight,
                    PositionWidth = WindowSettings.ProductionSearchDataWidth,
                    Maximised = WindowSettings.ProductionSearchDataMaximised    
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LabelAssociation.ToString(),
                    PositionTop = WindowSettings.LabelAssociationTop,
                    PositionLeft = WindowSettings.LabelAssociationLeft,
                    PositionHeight = WindowSettings.LabelAssociationHeight,
                    PositionWidth = WindowSettings.LabelAssociationWidth,
                    Maximised = WindowSettings.LabelAssociationMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ReportViewer.ToString(),
                    PositionTop = WindowSettings.ReportViewerTop,
                    PositionLeft = WindowSettings.ReportViewerLeft,
                    PositionHeight = WindowSettings.ReportViewerHeight,
                    PositionWidth = WindowSettings.ReportViewerWidth,
                    Maximised = WindowSettings.ReportViewerMaximised,
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.WarehouseLocation.ToString(),
                    PositionTop = Settings.Default.WarehouseLocationTop,
                    PositionLeft = Settings.Default.WarehouseLocationLeft,
                    PositionHeight = Settings.Default.WarehouseLocationHeight,
                    PositionWidth = Settings.Default.WarehouseLocationWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DispatchContainer.ToString(),
                    PositionTop = WindowSettings.DispatchContainerTop,
                    PositionLeft = WindowSettings.DispatchContainerLeft,
                    PositionHeight = WindowSettings.DispatchContainerHeight,
                    PositionWidth = WindowSettings.DispatchContainerWidth,
                    Maximised = WindowSettings.DispatchContainerMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ARDispatch.ToString(),
                    PositionTop = WindowSettings.DispatchTop,
                    PositionLeft = WindowSettings.DispatchLeft,
                    PositionHeight = WindowSettings.DispatchHeight,
                    PositionWidth = WindowSettings.DispatchWidth,
                    Maximised = WindowSettings.DispatchMaximised,
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SyncPartners.ToString(),
                    PositionTop = WindowSettings.SyncPartnersTop,
                    PositionLeft = WindowSettings.SyncPartnersLeft,
                    PositionHeight = WindowSettings.SyncPartnersHeight,
                    PositionWidth = WindowSettings.SyncPartnersWidth,
                    Maximised = WindowSettings.SyncPartnersMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SyncProducts.ToString(),
                    PositionTop = WindowSettings.SyncProductsTop,
                    PositionLeft = WindowSettings.SyncProductsLeft,
                    PositionHeight = WindowSettings.SyncProductsHeight,
                    PositionWidth = WindowSettings.SyncProductsWidth,
                    Maximised = WindowSettings.SyncProductsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SyncPrices.ToString(),
                    PositionTop = WindowSettings.SyncPricesTop,
                    PositionLeft = WindowSettings.SyncPricesLeft,
                    PositionHeight = WindowSettings.SyncPricesHeight,
                    PositionWidth = WindowSettings.SyncPricesWidth,
                    Maximised = WindowSettings.SyncPricesMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ARDispatch2.ToString(),
                    PositionTop = WindowSettings.Dispatch2Top,
                    PositionLeft = WindowSettings.Dispatch2Left,
                    PositionHeight = WindowSettings.Dispatch2Height,
                    PositionWidth = WindowSettings.Dispatch2Width,
                    Maximised = WindowSettings.Dispatch2Maximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ARDispatchDetails.ToString(),
                    PositionTop = WindowSettings.ARDispatchDetailsTop,
                    PositionLeft = WindowSettings.ARDispatchDetailsLeft,
                    PositionHeight = WindowSettings.ARDispatchDetailsHeight,
                    PositionWidth = WindowSettings.ARDispatchDetailsWidth,
                    Maximised = WindowSettings.ARDispatchDetailsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.IntakeDetails.ToString(),
                    PositionTop = WindowSettings.IntakeDetailsTop,
                    PositionLeft = WindowSettings.IntakeDetailsLeft,
                    PositionHeight = WindowSettings.IntakeDetailsHeight,
                    PositionWidth = WindowSettings.IntakeDetailsWidth,
                    Maximised = WindowSettings.IntakeDetailsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TransactionDetails.ToString(),
                    PositionTop = Settings.Default.TransactionDetailsTop,
                    PositionLeft = Settings.Default.TransactionDetailsLeft,
                    PositionHeight = Settings.Default.TransactionDetailsHeight,
                    PositionWidth = Settings.Default.TransactionDetailsWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TransactionEditor.ToString(),
                    PositionTop = Settings.Default.EditTransactionTop,
                    PositionLeft = Settings.Default.EditTransactionLeft,
                    PositionHeight = Settings.Default.EditTransactionHeight,
                    PositionWidth = Settings.Default.EditTransactionWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BPGroupSetUp.ToString(),
                    PositionTop = Settings.Default.BPGroupSetUpTop,
                    PositionLeft = Settings.Default.BPGroupSetUpLeft,
                    PositionHeight = Settings.Default.BPGroupSetUpHeight,
                    PositionWidth = Settings.Default.BPGroupSetUpWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.APQuote.ToString(),
                    PositionTop = Settings.Default.APQuoteTop,
                    PositionLeft = Settings.Default.APQuoteLeft,
                    PositionHeight = Settings.Default.APQuoteHeight,
                    PositionWidth = Settings.Default.APQuoteWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.APOrder.ToString(),
                    PositionTop = Settings.Default.APOrderTop,
                    PositionLeft = Settings.Default.APOrderLeft,
                    PositionHeight = Settings.Default.APOrderHeight,
                    PositionWidth = Settings.Default.APOrderWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Order2.ToString(),
                    PositionTop = Settings.Default.Order2Top,
                    PositionLeft = Settings.Default.Order2Left,
                    PositionHeight = Settings.Default.Order2Height,
                    PositionWidth = Settings.Default.Order2Width
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.APReceipt.ToString(),
                    PositionTop = Settings.Default.APReceiptTop,
                    PositionLeft = Settings.Default.APReceiptLeft,
                    PositionHeight = Settings.Default.APReceiptHeight,
                    PositionWidth = Settings.Default.APReceiptWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.APReceiptDetails.ToString(),
                    PositionTop = Settings.Default.APReceiptDetailsTop,
                    PositionLeft = Settings.Default.APReceiptDetailsLeft,
                    PositionHeight = Settings.Default.APReceiptDetailsHeight,
                    PositionWidth = Settings.Default.APReceiptDetailsWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.QualityTemplateName.ToString(),
                    PositionTop = Settings.Default.QualityTemplateNameTop,
                    PositionLeft = Settings.Default.QualityTemplateNameLeft,
                    PositionHeight = Settings.Default.QualityTemplateNameHeight,
                    PositionWidth = Settings.Default.QualityTemplateNameWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DataGenerator.ToString(),
                    PositionTop = Settings.Default.DataTop,
                    PositionLeft = Settings.Default.DataLeft,
                    PositionHeight = Settings.Default.DataHeight,
                    PositionWidth = Settings.Default.DataWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.EposSettings.ToString(),
                    PositionTop = Settings.Default.EposSettingsTop,
                    PositionLeft = Settings.Default.EposSettingsLeft,
                    PositionHeight = Settings.Default.EposSettingsHeight,
                    PositionWidth = Settings.Default.EposSettingsWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Quote.ToString(),
                    PositionTop = WindowSettings.QuoteTop,
                    PositionLeft = WindowSettings.QuoteLeft,
                    PositionHeight = WindowSettings.QuoteHeight,
                    PositionWidth = WindowSettings.QuoteWidth,
                    Maximised = WindowSettings.QuoteMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Order.ToString(),
                    PositionTop = WindowSettings.OrderTop,
                    PositionLeft = WindowSettings.OrderLeft,
                    PositionHeight = WindowSettings.OrderHeight,
                    PositionWidth = WindowSettings.OrderWidth,
                    Maximised = WindowSettings.OrderMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ARReturns.ToString(),
                    PositionTop = WindowSettings.ReturnsTop,
                    PositionLeft = WindowSettings.ReturnsLeft,
                    PositionHeight = WindowSettings.ReturnsHeight,
                    PositionWidth = WindowSettings.ReturnsWidth,
                    Maximised = WindowSettings.ReturnsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.QuickOrder.ToString(),
                    PositionTop = WindowSettings.QuickOrderTop,
                    PositionLeft = WindowSettings.QuickOrderLeft,
                    PositionHeight = WindowSettings.QuickOrderHeight,
                    PositionWidth = WindowSettings.QuickOrderWidth,
                    Maximised = WindowSettings.QuickOrderMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Order2.ToString(),
                    PositionTop = WindowSettings.Order2Top,
                    PositionLeft = WindowSettings.Order2Left,
                    PositionHeight = WindowSettings.Order2Height,
                    PositionWidth = WindowSettings.Order2Width,
                    Maximised = WindowSettings.Order2Maximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.IntakeOrder.ToString(),
                    PositionTop = WindowSettings.IntakeOrderTop,
                    PositionLeft = WindowSettings.IntakeOrderLeft,
                    PositionHeight = WindowSettings.IntakeOrderHeight,
                    PositionWidth = WindowSettings.IntakeOrderWidth,
                    Maximised = WindowSettings.IntakeOrderMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Recipe.ToString(),
                    PositionTop = WindowSettings.RecipeTop,
                    PositionLeft = WindowSettings.RecipeLeft,
                    PositionHeight = WindowSettings.RecipeHeight,
                    PositionWidth = WindowSettings.RecipeWidth,
                    Maximised = WindowSettings.RecipeMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LairageOrder.ToString(),
                    PositionTop = WindowSettings.LairageOrderTop,
                    PositionLeft = WindowSettings.LairageOrderLeft,
                    PositionHeight = WindowSettings.LairageOrderHeight,
                    PositionWidth = WindowSettings.LairageOrderWidth,
                    Maximised = WindowSettings.LairageOrderMaximised,
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LairageIntake.ToString(),
                    PositionTop = WindowSettings.LairageIntakeTop,
                    PositionLeft = WindowSettings.LairageIntakeLeft,
                    PositionHeight = WindowSettings.LairageIntakeHeight,
                    PositionWidth = WindowSettings.LairageIntakeWidth,
                    Maximised = WindowSettings.LairageIntakeMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LairageAnimalsSearch.ToString(),
                    PositionTop = WindowSettings.LairageAnimalsSearchTop,
                    PositionLeft = WindowSettings.LairageAnimalsSearchLeft,
                    PositionHeight = WindowSettings.LairageAnimalsSearchHeight,
                    PositionWidth = WindowSettings.LairageAnimalsSearchWidth,
                    Maximised = WindowSettings.LairageAnimalsSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.CarcassDispatch.ToString(),
                    PositionTop = WindowSettings.CarcassDispatchTop,
                    PositionLeft = WindowSettings.CarcassDispatchLeft,
                    PositionHeight = WindowSettings.CarcassDispatchHeight,
                    PositionWidth = WindowSettings.CarcassDispatchWidth,
                    Maximised = WindowSettings.CarcassDispatchMaximised
                });


                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LairageSearch.ToString(),
                    PositionTop = WindowSettings.LairageSearchTop,
                    PositionLeft = WindowSettings.LairageSearchLeft,
                    PositionHeight = WindowSettings.LairageSearchHeight,
                    PositionWidth = WindowSettings.LairageSearchWidth,
                    Maximised = WindowSettings.LairageSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PaymentDeductions.ToString(),
                    PositionTop = WindowSettings.PaymentDeductionsTop,
                    PositionLeft = WindowSettings.PaymentDeductionsLeft,
                    PositionHeight = WindowSettings.PaymentDeductionsHeight,
                    PositionWidth = WindowSettings.PaymentDeductionsWidth,
                    Maximised = WindowSettings.PaymentDeductionsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PricingMatrix.ToString(),
                    PositionTop = WindowSettings.PricingMatrixTop,
                    PositionLeft = WindowSettings.PricingMatrixLeft,
                    PositionHeight = WindowSettings.PricingMatrixHeight,
                    PositionWidth = WindowSettings.PricingMatrixWidth,
                    Maximised = WindowSettings.PricingMatrixMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PaymentProposalCreation.ToString(),
                    PositionTop = WindowSettings.PaymentProposalTop,
                    PositionLeft = WindowSettings.PaymentProposalLeft,
                    PositionHeight = WindowSettings.PaymentProposalHeight,
                    PositionWidth = WindowSettings.PaymentProposalWidth,
                    Maximised = WindowSettings.PaymentProposalMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Payment.ToString(),
                    PositionTop = WindowSettings.PaymentTop,
                    PositionLeft = WindowSettings.PaymentLeft,
                    PositionHeight = WindowSettings.PaymentHeight,
                    PositionWidth = WindowSettings.PaymentWidth,
                    Maximised = WindowSettings.PaymentMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PaymentProposalOpen.ToString(),
                    PositionTop = WindowSettings.PaymentProposalSearchTop,
                    PositionLeft = WindowSettings.PaymentProposalSearchLeft,
                    PositionHeight = WindowSettings.PaymentProposalSearchHeight,
                    PositionWidth = WindowSettings.PaymentProposalSearchWidth,
                    Maximised = WindowSettings.PaymentProposalSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Map.ToString(),
                    PositionTop = Settings.Default.MapTop,
                    PositionLeft = Settings.Default.MapLeft,
                    PositionHeight = Settings.Default.MapHeight,
                    PositionWidth = Settings.Default.MapWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Region.ToString(),
                    PositionTop = Settings.Default.RegionTop,
                    PositionLeft = Settings.Default.RegionLeft,
                    PositionHeight = Settings.Default.RegionHeight,
                    PositionWidth = Settings.Default.RegionWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Route.ToString(),
                    PositionTop = Settings.Default.RouteTop,
                    PositionLeft = Settings.Default.RouteLeft,
                    PositionHeight = Settings.Default.RouteHeight,
                    PositionWidth = Settings.Default.RouteWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DocumentType.ToString(),
                    PositionTop = Settings.Default.DocumentTypeTop,
                    PositionLeft = Settings.Default.DocumentTypeLeft,
                    PositionHeight = Settings.Default.DocumentTypeHeight,
                    PositionWidth = Settings.Default.DocumentTypeWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DocumentNumber.ToString(),
                    PositionTop = Settings.Default.DocumentNumberTop,
                    PositionLeft = Settings.Default.DocumentNumberLeft,
                    PositionHeight = Settings.Default.DocumentNumberHeight,
                    PositionWidth = Settings.Default.DocumentNumberWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TelesalesSetUp.ToString(),
                    PositionTop = WindowSettings.TelesalesSetUpTop,
                    PositionLeft = WindowSettings.TelesalesSetUpLeft,
                    PositionHeight = WindowSettings.TelesalesSetUpHeight,
                    PositionWidth = WindowSettings.TelesalesSetUpWidth,
                    Maximised = WindowSettings.TelesalesSetUpMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Telesales.ToString(),
                    PositionTop = WindowSettings.TelesalesTop,
                    PositionLeft = WindowSettings.TelesalesLeft,
                    PositionHeight = WindowSettings.TelesalesHeight,
                    PositionWidth = WindowSettings.TelesalesWidth,
                    Maximised = WindowSettings.TelesalesMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TelesalesEndCall.ToString(),
                    PositionTop = WindowSettings.TelesalesEndCallTop,
                    PositionLeft = WindowSettings.TelesalesEndCallLeft,
                    PositionHeight = WindowSettings.TelesalesEndCallHeight,
                    PositionWidth = WindowSettings.TelesalesEndCallWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Audit.ToString(),
                    PositionTop = Settings.Default.AuditTop,
                    PositionLeft = Settings.Default.AuditLeft,
                    PositionHeight = Settings.Default.AuditHeight,
                    PositionWidth = Settings.Default.AuditWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BPMaster.ToString(),
                    PositionTop = WindowSettings.BPMasterTop,
                    PositionLeft = WindowSettings.BPMasterLeft,
                    PositionHeight = WindowSettings.BPMasterHeight,
                    PositionWidth = WindowSettings.BPMasterWidth,
                    Maximised = WindowSettings.BPMasterMaximised.ToBool()
                });

                string xmlFileDoc = null;
                if (File.Exists(Settings.Default.SearchGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.SearchGridPath);
                    xmlFileDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BPSearchData.ToString(),
                    PositionTop = WindowSettings.SearchDataTop,
                    PositionLeft = WindowSettings.SearchDataLeft,
                    PositionHeight = WindowSettings.SearchDataHeight,
                    PositionWidth = WindowSettings.SearchDataWidth,
                    Maximised = WindowSettings.SearchDataMaximised,
                    GridData = xmlFileDoc
                });

                string xmlSearchSalesFileDoc = null;
                if (File.Exists(Settings.Default.SalesCustomerSearchGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.SalesCustomerSearchGridPath);
                    xmlSearchSalesFileDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SalesCustomerSearch.ToString(),
                    PositionTop = WindowSettings.SearchSaleCustomersTop,
                    PositionLeft = WindowSettings.SearchSaleCustomersLeft,
                    PositionHeight = WindowSettings.SearchSaleCustomersHeight,
                    PositionWidth = WindowSettings.SearchSaleCustomersWidth,
                    Maximised = WindowSettings.SearchSaleCustomersMaximised,
                    GridData = xmlSearchSalesFileDoc
                });

                string xmlSalesSearchFileDoc = null;
                if (File.Exists(Settings.Default.SalesSearchGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.SalesSearchGridPath);
                    xmlSalesSearchFileDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SalesSearchData.ToString(),
                    PositionTop = WindowSettings.SaleSearchDataTop,
                    PositionLeft = WindowSettings.SaleSearchDataLeft,
                    PositionHeight = WindowSettings.SaleSearchDataHeight,
                    PositionWidth = WindowSettings.SaleSearchDataWidth,
                    Maximised = WindowSettings.SaleSearchDataMaximised,
                    GridData = xmlSalesSearchFileDoc
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SpecsSearch.ToString(),
                    PositionTop = WindowSettings.SpecsSearchTop,
                    PositionLeft = WindowSettings.SpecsSearchLeft,
                    PositionHeight = WindowSettings.SpecsSearchHeight,
                    PositionWidth = WindowSettings.SpecsSearchWidth,
                    Maximised = WindowSettings.SpecsSearchMaximised,
                    GridData = xmlSalesSearchFileDoc
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.WorkflowAlerts.ToString(),
                    PositionTop = WindowSettings.WorkflowAlertsSearchTop,
                    PositionLeft = WindowSettings.WorkflowAlertsSearchLeft,
                    PositionHeight = WindowSettings.WorkflowAlertsSearchHeight,
                    PositionWidth = WindowSettings.WorkflowAlertsSearchWidth,
                    Maximised = WindowSettings.WorkflowAlertsSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.AlertUsersSetUp.ToString(),
                    PositionTop = WindowSettings.AlertUsersSetUpTop,
                    PositionLeft = WindowSettings.AlertUsersSetUpLeft,
                    PositionHeight = WindowSettings.AlertUsersSetUpHeight,
                    PositionWidth = WindowSettings.AlertUsersSetUpWidth,
                    Maximised = WindowSettings.AlertUsersSetUpMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.WorkflowSearch.ToString(),
                    PositionTop = WindowSettings.WorkflowSearchTop,
                    PositionLeft = WindowSettings.WorkflowSearchLeft,
                    PositionHeight = WindowSettings.WorkflowSearchHeight,
                    PositionWidth = WindowSettings.WorkflowSearchWidth,
                    Maximised = WindowSettings.WorkflowSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TelesalesSearch.ToString(),
                    PositionTop = WindowSettings.TelesalesSearchTop,
                    PositionLeft = WindowSettings.TelesalesSearchLeft,
                    PositionHeight = WindowSettings.TelesalesSearchHeight,
                    PositionWidth = WindowSettings.TelesalesSearchWidth,
                    Maximised = WindowSettings.TelesalesSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.AttributeSearch.ToString(),
                    PositionTop = WindowSettings.AttributeSearchTop,
                    PositionLeft = WindowSettings.AttributeSearchLeft,
                    PositionHeight = WindowSettings.AttributeSearchHeight,
                    PositionWidth = WindowSettings.AttributeSearchWidth,
                    Maximised = WindowSettings.AttributeSearchMaximised
                });

                string xmlProductyStockDoc = null;
                if (File.Exists(Settings.Default.ProductStockGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.ProductStockGridPath);
                    xmlProductyStockDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.INInventory.ToString(),
                    GridData = xmlProductyStockDoc
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ContainerSetUp.ToString(),
                    PositionTop = Settings.Default.ContainerTop,
                    PositionLeft = Settings.Default.ContainerLeft,
                    PositionHeight = Settings.Default.ContainerHeight,
                    PositionWidth = Settings.Default.ContainerWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.CountrySetUp.ToString(),
                    PositionTop = Settings.Default.CountryMasterTop,
                    PositionLeft = Settings.Default.CountryMasterLeft,
                    PositionHeight = Settings.Default.CountryMasterHeight,
                    PositionWidth = Settings.Default.CountryMasterWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Date.ToString(),
                    PositionTop = Settings.Default.DateTop,
                    PositionLeft = Settings.Default.DateLeft,
                    PositionHeight = Settings.Default.DateHeight,
                    PositionWidth = Settings.Default.DateWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DateTemplateAllocation.ToString(),
                    PositionTop = Settings.Default.DatesAllocationTop,
                    PositionLeft = Settings.Default.DatesAllocationLeft,
                    PositionHeight = Settings.Default.DatesAllocationHeight,
                    PositionWidth = Settings.Default.DatesAllocationWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DateTemplateName.ToString(),
                    PositionTop = Settings.Default.DateNameTop,
                    PositionLeft = Settings.Default.DateNameLeft,
                    PositionHeight = Settings.Default.DateNameHeight,
                    PositionWidth = Settings.Default.DateNameWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.InventoryGroupSetUp.ToString(),
                    PositionTop = Settings.Default.INGroupSetUpTop,
                    PositionLeft = Settings.Default.INGroupSetUpLeft,
                    PositionHeight = Settings.Default.INGroupSetUpHeight,
                    PositionWidth = Settings.Default.INGroupSetUpWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.InventoryMaster.ToString(),
                    PositionTop = WindowSettings.INMasterTop,
                    PositionLeft = WindowSettings.INMasterLeft,
                    PositionHeight = WindowSettings.INMasterHeight,
                    PositionWidth = WindowSettings.INMasterWidth,
                    Maximised = WindowSettings.INMasterMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.InventoryGroup.ToString(),
                    PositionTop = WindowSettings.INGroupTop,
                    PositionLeft = WindowSettings.INGroupLeft,
                    PositionHeight = WindowSettings.INGroupHeight,
                    PositionWidth = WindowSettings.INGroupWidth,
                    Maximised = WindowSettings.INGroupMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.InventoryGroupSetUp.ToString(),
                    PositionTop = Settings.Default.INGroupSetUpTop,
                    PositionLeft = Settings.Default.INGroupSetUpLeft,
                    PositionHeight = Settings.Default.INGroupSetUpHeight,
                    PositionWidth = Settings.Default.INGroupSetUpWidth
                });

                string xmlInFileDoc = null;
                if (File.Exists(Settings.Default.INSearchGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.INSearchGridPath);
                    xmlInFileDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.INSearchData.ToString(),
                    PositionTop = WindowSettings.INSearchDataTop,
                    PositionLeft = WindowSettings.INSearchDataLeft,
                    PositionHeight = WindowSettings.INSearchDataHeight,
                    PositionWidth = WindowSettings.INSearchDataWidth,
                    Maximised = WindowSettings.INSearchDataMaximised,
                    GridData = xmlInFileDoc
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.IMPropertySetUp.ToString(),
                    PositionTop = Settings.Default.IMPropertySetUpTop,
                    PositionLeft = Settings.Default.IMPropertySetUpLeft,
                    PositionHeight = Settings.Default.IMPropertySetUpHeight,
                    PositionWidth = Settings.Default.IMPropertySetUpWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.GS1AI.ToString(),
                    PositionTop = Settings.Default.GS1AITop,
                    PositionLeft = Settings.Default.GS1AILeft,
                    PositionHeight = Settings.Default.GS1AIHeight,
                    PositionWidth = Settings.Default.GS1AIWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DeviceSetUp.ToString(),
                    PositionTop = Settings.Default.DeviceSetUpTop,
                    PositionLeft = Settings.Default.DeviceSetUpLeft,
                    PositionHeight = Settings.Default.DeviceSetUpHeight,
                    PositionWidth = Settings.Default.DeviceSetUpWidth
                });

                string xmlDeviceFileDoc = null;
                if (File.Exists(Settings.Default.DeviceSearchGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.DeviceSearchGridPath);
                    xmlDeviceFileDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DeviceSearch.ToString(),
                    PositionTop = Settings.Default.DeviceSearchTop,
                    PositionLeft = Settings.Default.DeviceSearchLeft,
                    PositionHeight = Settings.Default.DeviceSearchHeight,
                    PositionWidth = Settings.Default.DeviceSearchWidth,
                    GridData = xmlDeviceFileDoc
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LicenseAdmin.ToString(),
                    PositionTop = Settings.Default.LicenseAdminTop,
                    PositionLeft = Settings.Default.LicenseAdminLeft,
                    PositionHeight = Settings.Default.LicenseAdminHeight,
                    PositionWidth = Settings.Default.LicenseAdminWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.LicenseImport.ToString(),
                    PositionTop = Settings.Default.LicenseImportTop,
                    PositionLeft = Settings.Default.LicenseImportLeft,
                    PositionHeight = Settings.Default.LicenseImportHeight,
                    PositionWidth = Settings.Default.LicenseImportWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Currency.ToString(),
                    PositionTop = Settings.Default.CurrencyTop,
                    PositionLeft = Settings.Default.CurrencyLeft,
                    PositionHeight = Settings.Default.CurrencyHeight,
                    PositionWidth = Settings.Default.CurrencyWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Plant.ToString(),
                    PositionTop = Settings.Default.PlantTop,
                    PositionLeft = Settings.Default.PlantLeft,
                    PositionHeight = Settings.Default.PlantHeight,
                    PositionWidth = Settings.Default.PlantWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Warehouse.ToString(),
                    PositionTop = Settings.Default.WarehouseTop,
                    PositionLeft = Settings.Default.WarehouseLeft,
                    PositionHeight = Settings.Default.WarehouseHeight,
                    PositionWidth = Settings.Default.WarehouseWidth
                });

                string xmlPriceListDetailDoc = null;
                if (File.Exists(Settings.Default.PriceListDetailPath))
                {
                    var file = File.ReadAllText(Settings.Default.PriceListDetailPath);
                    xmlPriceListDetailDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PriceListDetail.ToString(),
                    PositionTop = WindowSettings.PriceListDetailTop,
                    PositionLeft = WindowSettings.PriceListDetailLeft,
                    PositionHeight = WindowSettings.PriceListDetailHeight,
                    PositionWidth = WindowSettings.PriceListDetailWidth,
                    Maximised = WindowSettings.PriceListDetailMaximised,
                    GridData = xmlPriceListDetailDoc
                });

                string xmlReceiptItems = null;
                if (File.Exists(Settings.Default.ReceiptItemsPath))
                {
                    var file = File.ReadAllText(Settings.Default.ReceiptItemsPath);
                    xmlReceiptItems = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.APReceiptItems.ToString(),
                    GridData = xmlReceiptItems
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PricingMaster.ToString(),
                    PositionTop = WindowSettings.PriceListMasterTop,
                    PositionLeft = WindowSettings.PriceListMasterLeft,
                    PositionHeight = WindowSettings.PriceListMasterHeight,
                    PositionWidth = WindowSettings.PriceListMasterWidth,
                    Maximised = WindowSettings.PriceListMasterMaximised,
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Tabs.ToString(),
                    PositionTop = WindowSettings.TabsTop,
                    PositionLeft = WindowSettings.TabsLeft,
                    PositionHeight = WindowSettings.TabsHeight,
                    PositionWidth = WindowSettings.TabsWidth,
                    Maximised = WindowSettings.TabsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Notes.ToString(),
                    PositionTop = WindowSettings.NotesTop,
                    PositionLeft = WindowSettings.NotesLeft,
                    PositionHeight = WindowSettings.NotesHeight,
                    PositionWidth = WindowSettings.NotesWidth,
                    Maximised = WindowSettings.NotesMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.AttributeMasterSetUp.ToString(),
                    PositionTop = WindowSettings.AttributeMasterTop,
                    PositionLeft = WindowSettings.AttributeMasterLeft,
                    PositionHeight = WindowSettings.AttributeMasterHeight,
                    PositionWidth = WindowSettings.AttributeMasterWidth,
                    Maximised = WindowSettings.AttributeMasterMaximised,
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.AttributeTemplateGroup.ToString(),
                    PositionTop = WindowSettings.TemplateGroupTop,
                    PositionLeft = WindowSettings.TemplateGroupLeft,
                    PositionHeight = WindowSettings.TemplateGroupHeight,
                    PositionWidth = WindowSettings.TemplateGroupWidth,
                    Maximised = WindowSettings.TemplateGroupMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.WeightBandGroup.ToString(),
                    PositionTop = WindowSettings.WeightBandGroupTop,
                    PositionLeft = WindowSettings.WeightBandGroupLeft,
                    PositionHeight = WindowSettings.WeightBandGroupHeight,
                    PositionWidth = WindowSettings.WeightBandGroupWidth,
                    Maximised = WindowSettings.WeightBandGroupMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.WeightBandPricing.ToString(),
                    PositionTop = WindowSettings.WeightBandPricingTop,
                    PositionLeft = WindowSettings.WeightBandPricingLeft,
                    PositionHeight = WindowSettings.WeightBandPricingHeight,
                    PositionWidth = WindowSettings.WeightBandPricingWidth,
                    Maximised = WindowSettings.WeightBandPricingMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.WeightBand.ToString(),
                    PositionTop = WindowSettings.WeightBandTop,
                    PositionLeft = WindowSettings.WeightBandLeft,
                    PositionHeight = WindowSettings.WeightBandHeight,
                    PositionWidth = WindowSettings.WeightBandWidth,
                    Maximised = WindowSettings.WeightBandMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TemplateAllocation.ToString(),
                    PositionTop = WindowSettings.AttributeAllocationTop,
                    PositionLeft = WindowSettings.AttributeAllocationLeft,
                    PositionHeight = WindowSettings.AttributeAllocationHeight,
                    PositionWidth = WindowSettings.AttributeAllocationWidth,
                    Maximised = WindowSettings.AttributeAllocationMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ReportFolders.ToString(),
                    PositionTop = WindowSettings.ReportFoldersTop,
                    PositionLeft = WindowSettings.ReportFoldersLeft,
                    PositionHeight = WindowSettings.ReportFoldersHeight,
                    PositionWidth = WindowSettings.ReportFoldersWidth,
                    Maximised = WindowSettings.ReportFoldersMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ReportProcess.ToString(),
                    PositionTop = WindowSettings.ReportProcessTop,
                    PositionLeft = WindowSettings.ReportProcessLeft,
                    PositionHeight = WindowSettings.ReportProcessHeight,
                    PositionWidth = WindowSettings.ReportProcessWidth,
                    Maximised = WindowSettings.ReportProcessMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ProductSelection.ToString(),
                    PositionTop = Settings.Default.ProductSelectionTop,
                    PositionLeft = Settings.Default.ProductSelectionLeft,
                    PositionHeight = Settings.Default.ProductSelectionHeight,
                    PositionWidth = Settings.Default.ProductSelectionWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Traceability.ToString(),
                    PositionTop = Settings.Default.TraceabilityMasterTop,
                    PositionLeft = Settings.Default.TraceabilityMasterLeft,
                    PositionHeight = Settings.Default.TraceabilityMasterHeight,
                    PositionWidth = Settings.Default.TraceabilityMasterWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TraceabilityTemplateAllocation.ToString(),
                    PositionTop = Settings.Default.TraceabilityAllocationTop,
                    PositionLeft = Settings.Default.TraceabilityAllocationLeft,
                    PositionHeight = Settings.Default.TraceabilityAllocationHeight,
                    PositionWidth = Settings.Default.TraceabilityAllocationWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.TraceabilityTemplateName.ToString(),
                    PositionTop = Settings.Default.TraceabilityNameTop,
                    PositionLeft = Settings.Default.TraceabilityNameLeft,
                    PositionHeight = Settings.Default.TraceabilityNameHeight,
                    PositionWidth = Settings.Default.TraceabilityNameWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.UOM.ToString(),
                    PositionTop = Settings.Default.UOMTop,
                    PositionLeft = Settings.Default.UOMLeft,
                    PositionHeight = Settings.Default.UOMHeight,
                    PositionWidth = Settings.Default.UOMWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.SqlGenerator.ToString(),
                    PositionTop = Settings.Default.SqlTop,
                    PositionLeft = Settings.Default.SqlLeft,
                    PositionHeight = Settings.Default.SqlHeight,
                    PositionWidth = Settings.Default.SqlWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Authorisations.ToString(),
                    PositionTop = Settings.Default.AuthorisationsTop,
                    PositionLeft = Settings.Default.AuthorisationsLeft,
                    PositionHeight = Settings.Default.AuthorisationsHeight,
                    PositionWidth = Settings.Default.AuthorisationsWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.UserGroup.ToString(),
                    PositionTop = Settings.Default.UserGroupTop,
                    PositionLeft = Settings.Default.UserGroupLeft,
                    PositionHeight = Settings.Default.UserGroupHeight,
                    PositionWidth = Settings.Default.UserGroupWidth
                });

                string xmlUserFileDoc = null;
                if (File.Exists(Settings.Default.UserSearchGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.UserSearchGridPath);
                    xmlUserFileDoc = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.UserSearchData.ToString(),
                    PositionTop = Settings.Default.UserSearchDataTop,
                    PositionLeft = Settings.Default.UserSearchDataLeft,
                    PositionHeight = Settings.Default.UserSearchDataHeight,
                    PositionWidth = Settings.Default.UserSearchDataWidth,
                    GridData = xmlUserFileDoc
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.UserSetUp.ToString(),
                    PositionTop = Settings.Default.UserMasterTop,
                    PositionLeft = Settings.Default.UserMasterLeft,
                    PositionHeight = Settings.Default.UserMasterHeight,
                    PositionWidth = Settings.Default.UserMasterWidth
                });

                string xmlInvoiceCreation = null;
                if (File.Exists(Settings.Default.InvoiceCreationGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.InvoiceCreationGridPath);
                    xmlInvoiceCreation = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.InvoiceCreation.ToString(),
                    PositionTop = Settings.Default.InvoiceCreationTop,
                    PositionLeft = Settings.Default.InvoiceCreationLeft,
                    PositionHeight = Settings.Default.InvoiceCreationHeight,
                    PositionWidth = Settings.Default.InvoiceCreationWidth,
                    GridData = xmlInvoiceCreation
                });

                string xmlAccounts = null;
                if (File.Exists(Settings.Default.AccountsGridPath))
                {
                    var file = File.ReadAllText(Settings.Default.AccountsGridPath);
                    xmlAccounts = XDocument.Parse(file).ToString();
                }

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Accounts.ToString(),
                    PositionTop = WindowSettings.AccountsTop,
                    PositionLeft = WindowSettings.AccountsLeft,
                    PositionHeight = WindowSettings.AccountsHeight,
                    PositionWidth = WindowSettings.AccountsWidth,
                    Maximised = WindowSettings.AccountsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.DeviceSettings.ToString(),
                    PositionTop = WindowSettings.DeviceSettingsTop,
                    PositionLeft = WindowSettings.DeviceSettingsLeft,
                    PositionHeight = WindowSettings.DeviceSettingsHeight,
                    PositionWidth = WindowSettings.DeviceSettingsWidth,
                    Maximised = WindowSettings.DeviceSettingsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ImageView.ToString(),
                    PositionTop = WindowSettings.ImageViewTop,
                    PositionLeft = WindowSettings.ImageViewLeft,
                    PositionHeight = WindowSettings.ImageViewHeight,
                    PositionWidth = WindowSettings.ImageViewWidth,
                    Maximised = WindowSettings.ImageViewMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.BatchData.ToString(),
                    PositionTop = WindowSettings.BatchDataTop,
                    PositionLeft = WindowSettings.BatchDataLeft,
                    PositionHeight = WindowSettings.BatchDataHeight,
                    PositionWidth = WindowSettings.BatchDataWidth,
                    Maximised = WindowSettings.BatchDataMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ApplyPrice.ToString(),
                    PositionTop = WindowSettings.ApplyPriceTop,
                    PositionLeft = WindowSettings.ApplyPriceLeft,
                    PositionHeight = WindowSettings.ApplyPriceHeight,
                    PositionWidth = WindowSettings.ApplyPriceWidth
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.MRP.ToString(),
                    PositionTop = WindowSettings.MRPTop,
                    PositionLeft = WindowSettings.MRPLeft,
                    PositionHeight = WindowSettings.MRPHeight,
                    PositionWidth = WindowSettings.MRPWidth,
                    Maximised = WindowSettings.MRPMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Specs.ToString(),
                    PositionTop = WindowSettings.SpecsTop,
                    PositionLeft = WindowSettings.SpecsLeft,
                    PositionHeight = WindowSettings.SpecsHeight,
                    PositionWidth = WindowSettings.SpecsWidth,
                    Maximised = WindowSettings.SpecsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Department.ToString(),
                    PositionTop = WindowSettings.DepartmentTop,
                    PositionLeft = WindowSettings.DepartmentLeft,
                    PositionHeight = WindowSettings.DepartmentHeight,
                    PositionWidth = WindowSettings.DepartmentWidth,
                    Maximised = WindowSettings.DepartmentMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.InvoiceCreation.ToString(),
                    PositionTop = WindowSettings.InvoiceCreationTop,
                    PositionLeft = WindowSettings.InvoiceCreationLeft,
                    PositionHeight = WindowSettings.InvoiceCreationHeight,
                    PositionWidth = WindowSettings.InvoiceCreationWidth,
                    Maximised = WindowSettings.InvoiceCreationMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.APInvoiceCreation.ToString(),
                    PositionTop = WindowSettings.APInvoiceCreationTop,
                    PositionLeft = WindowSettings.APInvoiceCreationLeft,
                    PositionHeight = WindowSettings.APInvoiceCreationHeight,
                    PositionWidth = WindowSettings.APInvoiceCreationWidth,
                    Maximised = WindowSettings.APInvoiceCreationMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.CreditNoteCreation.ToString(),
                    PositionTop = WindowSettings.CreditNoteCreationTop,
                    PositionLeft = WindowSettings.CreditNoteCreationLeft,
                    PositionHeight = WindowSettings.CreditNoteCreationHeight,
                    PositionWidth = WindowSettings.CreditNoteCreationWidth,
                    Maximised = WindowSettings.CreditNoteCreationMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ReportSetUp.ToString(),
                    PositionTop = WindowSettings.ReportSetUpTop,
                    PositionLeft = WindowSettings.ReportSetUpLeft,
                    PositionHeight = WindowSettings.ReportSetUpHeight,
                    PositionWidth = WindowSettings.ReportSetUpWidth,
                    Maximised = WindowSettings.ReportSetUpMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ReportSearch.ToString(),
                    PositionTop = WindowSettings.ReportSearchTop,
                    PositionLeft = WindowSettings.ReportSearchLeft,
                    PositionHeight = WindowSettings.ReportSearchHeight,
                    PositionWidth = WindowSettings.ReportSearchWidth,
                    Maximised = WindowSettings.ReportSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.UserGroup.ToString(),
                    PositionTop = WindowSettings.UserGroupTop,
                    PositionLeft = WindowSettings.UserGroupLeft,
                    PositionHeight = WindowSettings.UserGroupHeight,
                    PositionWidth = WindowSettings.UserGroupWidth,
                    Maximised = WindowSettings.UserGroupMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.UserSearch.ToString(),
                    PositionTop = WindowSettings.UserSearchTop,
                    PositionLeft = WindowSettings.UserSearchLeft,
                    PositionHeight = WindowSettings.UserSearchHeight,
                    PositionWidth = WindowSettings.UserSearchWidth,
                    Maximised = WindowSettings.UserSearchMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.User.ToString(),
                    PositionTop = WindowSettings.UserTop,
                    PositionLeft = WindowSettings.UserLeft,
                    PositionHeight = WindowSettings.UserHeight,
                    PositionWidth = WindowSettings.UserWidth,
                    Maximised = WindowSettings.UserMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.Authorisations.ToString(),
                    PositionTop = WindowSettings.AuthorisationsTop,
                    PositionLeft = WindowSettings.AuthorisationsLeft,
                    PositionHeight = WindowSettings.AuthorisationsHeight,
                    PositionWidth = WindowSettings.AuthorisationsWidth,
                    Maximised = WindowSettings.AuthorisationsMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.StockAdjust.ToString(),
                    PositionTop = WindowSettings.StockAdjustTop,
                    PositionLeft = WindowSettings.StockAdjustLeft,
                    PositionHeight = WindowSettings.StockAdjustHeight,
                    PositionWidth = WindowSettings.StockAdjustWidth,
                    Maximised = WindowSettings.StockAdjustMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.PurchasesAccounts.ToString(),
                    PositionTop = WindowSettings.AccountsPurchasesTop,
                    PositionLeft = WindowSettings.AccountsPurchasesLeft,
                    PositionHeight = WindowSettings.AccountsPurchasesHeight,
                    PositionWidth = WindowSettings.AccountsPurchasesWidth,
                    Maximised = WindowSettings.AccountsPurchasesMaximised
                });

                windowsSettings.Add(new WindowSetting
                {
                    UserMasterID = userID,
                    Name = ViewType.ReturnsAccounts.ToString(),
                    PositionTop = WindowSettings.AccountsReturnsTop,
                    PositionLeft = WindowSettings.AccountsReturnsLeft,
                    PositionHeight = WindowSettings.AccountsReturnsHeight,
                    PositionWidth = WindowSettings.AccountsReturnsWidth,
                    Maximised = WindowSettings.AccountsReturnsMaximised
                });

                #endregion

                this.userRepository.SaveWindowSettings(windowsSettings);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Makes a call to update the rule's authorisation value.
        /// </summary>
        /// <param name="rules">The rules to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool AddOrUpdateAuthorisationRules(IList<ViewGroupRule> rules)
        {
            // new rules
            this.userRepository.AddAuthorisationRules(rules.Where(x => x.UserGroupRuleID == 0).ToList());

            // update rules
            this.userRepository.UpdateAuthorisationRules(rules.Where(x => x.UserGroupRuleID > 0).ToList());
            return true;
        }

        /// <summary>
        /// Gets all the user group authorisation values.
        /// </summary>
        /// <returns>A collection of user group authorisation values.</returns>
        public IList<NouAuthorisationValue> GetAuthorisationValues()
        {
            return this.userRepository.GetAuthorisationValues();
        }

        /// <summary>
        /// Gets all the user group authorisation list.
        /// </summary>
        /// <returns>A collection of user group authorisation list.</returns>
        public IList<SystemAuthorisation> GetAuthorisationList()
        {
            return this.userRepository.GetAuthorisations();
        }

        /// <summary>
        /// Gets all the user group rules.
        /// </summary>
        /// <returns>A collection of user group rules.</returns>
        public IList<ViewGroupRule> GetGroupRules()
        {
            return this.userRepository.GetUserGroupRules();
        }

        /// <summary>
        /// Gets the collection of user groups, and their associated rules.
        /// </summary>
        /// <returns>A collection of user group associations.</returns>
        public IList<UserGroupAuthorisations> GetUserGroupAuthorisations()
        {
            var userGroupAuthorisations = new List<UserGroupAuthorisations>();
            var rules = this.GetGroupRules();
            var authorisationsList = this.GetAuthorisationList().OrderBy(x => x.Description);

            this.GetUserGroups().ToList().ForEach(userGroup =>
            {
                var allRules = new List<ViewGroupRule>();

                foreach (var authorisation in authorisationsList)
                {
                    var localRule = rules.FirstOrDefault(x => x.UserGroupID == userGroup.UserGroupID && x.AuthorisationListDescription == authorisation.Description);

                    if (localRule != null)
                    {
                        allRules.Add(localRule);
                    }
                    else
                    {
                        // create blank rule
                        allRules.Add(new ViewGroupRule
                        {
                            UserGroupID = userGroup.UserGroupID,
                            UserGroupRuleID = 0,
                            NouAuthorisationValueID = 3,
                            AuthorisationListDescription = authorisation.Description,
                            NouAuthorisationListtID = authorisation.NouAuthorisationID,
                            GroupName = authorisation.GroupName,
                            AuthorisationValueDescription = Strings.NoAccess
                        });
                    }
                }

                //var userRules = rules.Where(x => x.UserGroupID == userGroup.UserGroupID).ToList();
                userGroupAuthorisations.Add(new UserGroupAuthorisations { UserGroup = userGroup, Rules = allRules.OrderBy(x => x.GroupName).ToList() });
            });

            return userGroupAuthorisations;
        }

        /// <summary>
        /// Method that returns the database names from the server.
        /// </summary>
        /// <returns>A collection of database names.</returns>
        public IList<string> GetDatabaseNames()
        {
            var nouvemDatabases = new List<string>();
            var tryCount = 0;
            try
            {
                nouvemDatabases = this.userRepository.GetDatabaseNames().ToList();
            }
            catch (Exception)
            {
                tryCount++;
                if (tryCount == 1)
                {
                    // retry, using the server default database.
                    var server =
                        NouvemGlobal.Servers.FirstOrDefault(
                            x => x.ServerName.Trim().CompareIgnoringCase(ApplicationSettings.ServerName));

                    if (server != null)
                    {
                        ApplicationSettings.DatabaseName = server.DefaultDatabaseName;
                        EntityConnection.CreateConnectionString();
                        nouvemDatabases = this.userRepository.GetDatabaseNames().ToList();
                    }
                }
            }

            return nouvemDatabases;
        }

        /// <summary>
        /// Method that returns the servers.
        /// </summary>
        /// <returns>A collection of server names.</returns>
        public IList<Server> GetServers()
        {
            return this.userRepository.GetServers();
        }

        /// <summary>
        /// Method that returns the user groups.
        /// </summary>
        /// <returns>A collection of user groups.</returns>
        public IList<UserGroup_> GetUserGroups()
        {
            return this.userRepository.GetUserGroups();
        }

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        public IList<User> GetUsers()
        {
            return this.userRepository.GetUsers();
        }

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        public IList<UserMaster> GetDbUsers()
        {
            return this.userRepository.GetDbUsers();
        }

        /// <summary>
        /// Method that makes a repository call to add or update a new user.
        /// </summary>
        /// <param name="user">The user to add or update.</param>
        /// <param name="snapshot">The snapshot data to compare.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateUser(User user, User snapshot)
        {
            // Create the audit data
            //var audit = new Audit
            //{
            //    TableName = Constant.UserMaster,
            //    TableID = user.UserMaster.UserMasterID,
            //    TransactionType = Constant.Add,
            //    UserMasterID = NouvemGlobal.LoggedInUser.UserMaster.UserMasterID,
            //    UserName = NouvemGlobal.LoggedInUser.UserMaster.UserName,
            //    UserFullName = NouvemGlobal.LoggedInUser.UserMaster.FullName,
            //    TransactionDate = DateTime.Now,
            //    Deleted = false
            //};

            //user.Audit = audit;

            if (user.UserMaster.UserMasterID == 0)
            {
                // New user
                user.UserMaster.UserMasterID = this.userRepository.AddUser(user);
                return user.UserMaster.UserMasterID > 0;
            }

            //user.Audit.TransactionType = Constant.Update;

            var localUser = user.Copy;
            var changes = new Dictionary<object, object> { { localUser.UserMaster, snapshot.UserMaster } };

            //var changedProperties = this.GetChangedProperties(changes);
            //user.Audit.DataChanges = this.CreateAuditXml(changedProperties);

            return this.userRepository.UpdateUser(user);
        }

        /// <summary>
        /// Copys the user group settings.
        /// </summary>
        /// <param name="from">Group to copy from.</param>
        /// <param name="to">Group to Copy to.</param>
        /// <returns>Flag, as to successful copy of not.</returns>
        public bool CopyUserGroupSettings(int from, int to)
        {
            return this.userRepository.CopyUserGroupSettings(from, to);
        }

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        public User GetUser(string name)
        {
            return this.userRepository.GetUser(name);
        }

        /// <summary>
        /// Gets the db pasword.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public string GetDBPassword(int deviceId)
        {
            return this.userRepository.GetDBPassword(deviceId);
        }

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        public IList<UserGroup> GetUserGroupsAndModules(IList<ReportData> reports)
        {
            var groupData = this.userRepository.GetUserGroupsAndModules(reports);
            if (NouvemGlobal.IsNouvemUser)
            {
                return groupData;
            }

            var nouvemGroup = groupData.FirstOrDefault(x => x.Description.CompareIgnoringCase("Nouvem"));
            if (nouvemGroup == null)
            {
                return groupData;
            }

            var nouvemGroupModules = groupData.FirstOrDefault(x => x.UserGroupID == nouvemGroup.UserGroupID);
            if (nouvemGroupModules == null)
            {
                return groupData;
            }

            foreach (var nouvemGroupModule in nouvemGroupModules.Modules)
            {
                UserGroup match = null;
                if (!nouvemGroupModule.IsSelected)
                {
                    foreach (var userGroup in groupData.Where(x => x.UserGroupID != nouvemGroup.UserGroupID))
                    {
                        if (userGroup.Modules == null)
                        {
                            continue;
                        }

                        match = userGroup.Modules.FirstOrDefault(
                            x => x.UserGroupID == nouvemGroupModule.UserGroupID);
                        if (match != null)
                        {
                            userGroup.Modules.Remove(match);
                        }
                    }

                    continue;
                }

                foreach (var nouvemChildGroupModule in nouvemGroupModule.Modules)
                {
                    if (!nouvemChildGroupModule.IsSelected)
                    {
                        foreach (var userGroup in groupData.Where(x => x.UserGroupID != nouvemGroup.UserGroupID))
                        {
                            foreach (var userGroupModule in userGroup.Modules)
                            {
                                if (userGroupModule.Modules == null)
                                {
                                    continue;
                                }

                                if (nouvemGroupModule.Description == "Reports" && userGroupModule.Description == "Reports")
                                {
                                    match = userGroupModule.Modules.FirstOrDefault(
                                        x => x.Description == nouvemChildGroupModule.Description);
                                    if (match != null)
                                    {
                                        userGroupModule.Modules.Remove(match);
                                        break;
                                    }
                                }
                                else
                                {
                                    match = userGroupModule.Modules.FirstOrDefault(
                                        x => x.UserGroupID == nouvemChildGroupModule.UserGroupID);
                                    if (match != null)
                                    {
                                        userGroupModule.Modules.Remove(match);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    foreach (var nouvemChildOfChildGroupModule in nouvemChildGroupModule.Modules)
                    {
                        if (!nouvemChildOfChildGroupModule.IsSelected)
                        {
                            foreach (var userGroup in groupData.Where(x => x.UserGroupID != nouvemGroup.UserGroupID))
                            {
                                foreach (var userGroupModule in userGroup.Modules)
                                {
                                    if (userGroupModule.Modules == null)
                                    {
                                        continue;
                                    }

                                    foreach (var childModule in userGroupModule.Modules)
                                    {
                                        if (childModule.Modules == null)
                                        {
                                            continue;
                                        }

                                        match = childModule.Modules.FirstOrDefault(
                                            x => x.UserGroupID == nouvemChildOfChildGroupModule.UserGroupID);
                                        if (match != null)
                                        {
                                            childModule.Modules.Remove(match);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return groupData;
        }

        /// <summary>
        /// Method that makes a repository call to add or update a new user.
        /// </summary>
        /// <param name="userGroups">The user groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateUserGroups(IList<UserGroup> userGroups)
        {
            return this.userRepository.UpdateUserGroups(userGroups);
        }

        /// <summary>
        /// Stores the remember user password flag.
        /// </summary>
        /// <param name="user">The user in question.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool StoreRememberPassword(User user)
        {
            return this.userRepository.StoreRememberPassword(user);
        }

        /// <summary>
        /// Retrieves the user password.
        /// </summary>
        /// <param name="user">The user in question.</param>
        /// <returns>Flag, as to successful retrieval or not.</returns>
        public string GetUserPassword(string user)
        {
            var encryptedPassword = this.userRepository.GetUserPassword(user);
            if (encryptedPassword != null)
            {
                return Security.DecryptString(encryptedPassword);
            }

            return null;
        }

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        public IList<UserAlertsLookUp> GetUserLookUps(int userMasterID)
        {
            return this.userRepository.GetUserLookUps(userMasterID);
        }

        /// <summary>
        /// Updates the users alerts look ups.
        /// </summary>
        /// <param name="users">The selected users.</param>
        /// <param name="userMasterId">The user id.</param>
        /// <returns>Flag, as to successfl update or not.</returns>
        public bool UpdateUserAlertsSetUp(IList<UserAlertsLookUp> users, int userMasterId)
        {
            return this.userRepository.UpdateUserAlertsSetUp(users, userMasterId);
        }

        /// <summary>
        /// Saves the logged in user shortcuts.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Flag, as to successful save or not.</returns>
        public bool SaveUserShortcuts(User user)
        {
            if (user == null || user.UserShortcuts == null || !user.UserShortcuts.Any())
            {
                return false;
            }

            return this.userRepository.SaveUserShortcuts(user);
        }

        /// <summary>
        /// Method that verifies a users login credentials.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The user password.</param>
        /// <returns>The verified user first name, or empty string if not verified, 
        /// the user full name,
        /// a flag indicating if the user is currently logged in somewhere else,
        /// a flag indicating if the user is currently suspended,
        /// a flag indicating if the users password is to be changed.</returns>
        public Tuple<string, string, string, bool, bool, bool> VerifyLoginUser(string userId, string password, IList<User> localUsers = null, bool passwordNotRequired = false)
        {
            // If a user is logged in, save their settings
            if (NouvemGlobal.LoggedInUser != null)
            {
                this.SaveWindowSettings();
            }

            var loginDetails = Tuple.Create(string.Empty, string.Empty, string.Empty, false, false, false);
            var name = string.Empty;

            var users = localUsers;
            if (localUsers == null)
            {
                users = NouvemGlobal.GetUsers();
            }

            if (users == null)
            {
                // only contains nouvem user. Db issue
                throw new Exception(Message.DBConnectionFailure);
            }

            if (passwordNotRequired)
            {
                NouvemGlobal.LoggedInUser = users.FirstOrDefault(x => x.UserMaster.UserName.CompareIgnoringCase(userId.Trim()));
            }
            else
            {
                if (userId.CompareIgnoringCaseAndWhitespace("Nouvem"))
                {
                    NouvemGlobal.LoggedInUser = users.FirstOrDefault(x => x.UserMaster.UserName.CompareIgnoringCase(userId.Trim()) && ApplicationSettings.NouvemPassword.Equals(password.Trim()));
                }
                else
                {
                    NouvemGlobal.LoggedInUser = users.FirstOrDefault(x => x.UserMaster.UserName.CompareIgnoringCase(userId.Trim()) && Security.DecryptString(x.UserMaster.Password).Trim().Equals(password.Trim()));
                }
                
            }
           
            var fullName = string.Empty;
            var identifier = string.Empty;
            bool changePassword = false;

            if (NouvemGlobal.LoggedInUser != null)
            {
                fullName = NouvemGlobal.LoggedInUser.UserMaster.FullName;
                var index = fullName.IndexOf(" ");
                if (index > 0)
                {
                    name = fullName.Substring(0, index);
                }
                else
                {
                    name = fullName;
                }

                identifier = NouvemGlobal.LoggedInUser.UserMaster.UserName;

                if (NouvemGlobal.LoggedInUser.UserMaster.SuspendUser == true)
                {
                    // user suspended
                    return Tuple.Create(name, fullName, identifier, false, true, false);
                }

                //if (this.CheckLoggedInStatus(NouvemGlobal.LoggedInUser))
                //{
                //    // user currently logged in somewhere else
                //    return Tuple.Create(name, fullName, identifier, true, false, false);
                //}

                changePassword = NouvemGlobal.LoggedInUser.UserMaster.ChangeAtNextLogin.ToBool();

                this.RegisterLogon(userId, NouvemGlobal.LoggedInUser.UserMaster.UserMasterID);

                // Apply the settings associated with the newly logged in user.
                this.CreateWindowSettings(NouvemGlobal.LoggedInUser.UserMaster.UserMasterID);
            }

            return Tuple.Create(name, fullName, identifier, false, false, changePassword);
        }

        /// <summary>
        /// Method that verifies a users login authorisation credentials.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The user password.</param>
        /// <returns>The user identifier.</returns>
        public string VerifyLoginUserForAuthorisationCheck(string userId, string password)
        {
            var users = NouvemGlobal.Users;

            NouvemGlobal.LoggedInUser = users.FirstOrDefault(x => x.UserMaster.UserName.Trim().Equals(userId.Trim()) && Security.DecryptString(x.UserMaster.Password).Trim().Equals(password.Trim()));

            if (NouvemGlobal.LoggedInUser != null)
            {
                var identifier = NouvemGlobal.LoggedInUser.UserMaster.UserName;
                return identifier;
            }

            return string.Empty;
        }

        /// <summary>
        /// Method that verifies a users login credentials for the epos system.
        /// </summary>
        /// <param name="password">The user password.</param>
        /// <returns>The verified user first name, or empty string if not verified, 
        /// the user full name,
        /// a flag indicating if the user is currently logged in somewhere else,
        /// a flag indicating if the user is currently suspended,
        /// a flag indicating if the users password is to be changed.</returns>
        public Tuple<string, string, string, bool, bool, bool> VerifyLoginUser(string password)
        {
            var loginDetails = Tuple.Create(string.Empty, string.Empty, string.Empty, false, false, false);
            var name = string.Empty;
            var users = NouvemGlobal.GetUsers();

            if (users == null)
            {
                // no users sets up
                return loginDetails;
            }

            NouvemGlobal.LoggedInUser = users.FirstOrDefault(x => Security.DecryptString(x.UserMaster.Password).Trim().Equals(password.Trim()));
            var fullName = string.Empty;
            var identifier = string.Empty;
            bool changePassword = false;

            if (NouvemGlobal.LoggedInUser != null)
            {
                identifier = NouvemGlobal.LoggedInUser.UserMaster.UserName;
                fullName = NouvemGlobal.LoggedInUser.UserMaster.FullName;
                var index = fullName.IndexOf(" ");
                if (index > 0)
                {
                    name = fullName.Substring(0, index);
                }
                else
                {
                    name = fullName;
                }

                if (NouvemGlobal.LoggedInUser.UserMaster.SuspendUser == true)
                {
                    // user suspended
                    return Tuple.Create(name, fullName, identifier, false, true, false);
                }

                if (this.CheckLoggedInStatus(NouvemGlobal.LoggedInUser))
                {
                    // user currently logged in somewhere else
                    return Tuple.Create(name, fullName, identifier, true, false, false);
                }

                changePassword = NouvemGlobal.LoggedInUser.UserMaster.ChangeAtNextLogin.ToBool();
                var userId = NouvemGlobal.LoggedInUser.UserMaster.UserName;
                Settings.Default.LoggedInUserID = NouvemGlobal.LoggedInUser.UserMaster.UserMasterID;
                Settings.Default.Save();

                this.RegisterLogon(userId, NouvemGlobal.LoggedInUser.UserMaster.UserMasterID);
                this.CreateWindowSettings(NouvemGlobal.LoggedInUser.UserMaster.UserMasterID);
            }

            return Tuple.Create(name, fullName, identifier, false, false, changePassword);
        }

        /// <summary>
        /// Forces the logging out of another user logged in with the current licensees credentials.
        /// </summary>
        public void ForceLogOff()
        {
            this.userRepository.ForceLogOff();
        }

        /// <summary>
        /// Checks whether the current user has been marked to be forced logged off.
        /// </summary>
        public bool CheckForForcedLogOff()
        {
            return this.userRepository.CheckForForcedLogOff();
        }

        /// <summary>
        /// Register a user logon.
        /// </summary>
        /// <param name="userName">The logon user name.</param>
        /// <param name="userMasterId">The user master id.</param>
        public void RegisterLogon(string userName, int userMasterId)
        {
            var ipAddress = this.GetIPAddress();
            var logon = new UserLoggedOn
            {
                DeviceIP = ipAddress,
                PCName = Environment.MachineName,
                SessionStart = DateTime.Now,
                UserMasterID = userMasterId,
                Deleted = false
            };

            this.userRepository.RegisterLogon(logon, userName);
        }

        /// <summary>
        /// Register a user log out for the current user, and save the settings.
        /// </summary>
        public void ApplicationClosingHandler()
        {
            try
            {
                this.SaveDeviceSettings();
                this.SaveWindowSettings();
                this.userRepository.RegisterLogout();
                Application.Current.Shutdown();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("ApplicationClosingHandler():{0}", ex.Message));
            }
        }

        /// <summary>
        /// Logs out the currenyt user.
        /// </summary>
        public void LogOut()
        {
            this.userRepository.RegisterLogout();
        }

        /// <summary>
        /// Check the logged in status of a user.
        /// </summary>
        /// <param name="user">The user logging in to check.</param>
        /// <returns>A flag, indicating whether the user is already logged in.</returns>
        public bool CheckLoggedInStatus(User user)
        {
            return this.userRepository.CheckLoggedInStatus(user);
        }

        /// <summary>
        /// Method that updates a user pasword.
        /// </summary>
        /// <param name="userId">The user name.</param>
        /// <param name="password">The updated password.</param>
        /// <returns>A flag, as to wherther a password was updated or not.</returns>
        public bool UpdatePassword(string userId, string password)
        {
            var encryptedPassword = Security.EncryptString(password);
            return this.userRepository.UpdatePassword(userId, encryptedPassword);
        }

        /// <summary>
        /// Removes any user log in records over a week old.
        /// </summary>
        public void RemoveOldLogins()
        {
            this.userRepository.RemoveOldLogins();
        }

        /// <summary>
        /// Get the local ip address.
        /// </summary>
        /// <returns>The local machine ip address.</returns>
        public string GetIPAddress()
        {
            try
            {
                var hostName = Dns.GetHostName();
                var address = Dns.GetHostEntry(hostName).AddressList[0].ToString();
                return address;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        #region workflow

        /// <summary>
        /// Adds a new workflow.
        /// </summary>
        /// <param name="workflow">The workflow to add.</param>
        /// <returns>The id of the newly created workflow.</returns>
        public int AddWorkflow(Sale workflow)
        {
            return this.workflowRepository.AddWorkflow(workflow);
        }

        /// <summary>
        /// Marks an alert as opened.
        /// </summary>
        /// <param name="alertId">The alert id.</param>
        /// <returns>Flag indicating whether the alert was marked as opened.</returns>
        public bool MarkAlertAsViewed(int alertId)
        {
            return this.workflowRepository.MarkAlertAsViewed(alertId);
        }

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        /// <returns>The current workflows.</returns>
        public IList<Sale> GetWorkflows(IList<int?> statuses)
        {
            return this.workflowRepository.GetWorkflows(statuses);
        }

        /// <summary>
        /// Marks all user alerts as opened.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>Flag indicating whether all the user alerts ware marked as opened.</returns>
        public bool MarkAllUserAlertAsViewed(int userId)
        {
            return this.workflowRepository.MarkAllUserAlertAsViewed(userId);
        }

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="workflows">The workflow id.</param>
        /// <param name="status">The status to update to.</param>
        /// <returns>Fla, as to successful update or not.</returns>
        public bool UpdateWorkflowStatus(HashSet<Sale> workflows, int status)
        {
            return this.workflowRepository.UpdateWorkflowStatus(workflows, status);
        }

        /// <summary>
        /// Updates alert status.
        /// </summary>
        /// <param name="alertIds">The alert id.</param>
        /// <param name="status">The status to update to.</param>
        /// <returns>Fla, as to successful update or not.</returns>
        public bool UpdateAlertsStatus(HashSet<int> alertIds, int status)
        {
            return this.workflowRepository.UpdateAlertsStatus(alertIds, status);
        }

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        /// <returns>The current workflows.</returns>
        public IList<Sale> GetWorkflowsByDate(IList<int?> statuses, DateTime start, DateTime end)
        {
            return this.workflowRepository.GetWorkflowsByDate(statuses, start, end);
        }

        /// <summary>
        /// Determines if there are any unread alerts.
        /// </summary>
        /// <param name="userIds">The users to check.</param>
        /// <returns>Flag indicating whether there are any unread alerts.</returns>
        public bool AreThereNewAlerts(HashSet<int?> userIds, bool useAlertsOpen)
        {
            return this.workflowRepository.AreThereNewAlerts(userIds, useAlertsOpen);
        }

        /// <summary>
        /// Determines if there are any unread alerts.
        /// </summary>
        /// <param name="userIds">The users to check.</param>
        /// <returns>Flag indicating whether there are any unread alerts.</returns>
        public bool MarkAlertsAsRead(HashSet<int?> userIds)
        {
            return this.workflowRepository.MarkAlertsAsRead(userIds);
        }

        /// <summary>
        /// Marks all user alerts as opened.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>Flag indicating whether all the user alerts ware marked as opened.</returns>
        public bool MarkAllUserAlertAsComplete(int userId)
        {
            return this.workflowRepository.MarkAllUserAlertAsComplete(userId);
        }

        /// <summary>
        /// Gets the workflow.
        /// </summary>
        /// <returns>The current workflow by id.</returns>
        public Sale GetWorkflowById(int id)
        {
            return this.workflowRepository.GetWorkflowById(id);
        }

        /// <summary>
        /// Gets the users to alert.
        /// </summary>
        /// <returns>The user/user groups to alert.</returns>
        public AlertUsers GetAlertUsers()
        {
            return this.workflowRepository.GetAlertUsers();
        }

        /// <summary>
        /// Gets the user alerts.
        /// </summary>
        /// <returns>The user alerts.</returns>
        public IList<Sale> GetAlerts(IList<int?> docStatusIds, DateTime start, DateTime end)
        {
            return this.workflowRepository.GetAlerts(docStatusIds, start, end);
        }

        /// <summary>
        /// Updates a workflow workflow.
        /// </summary>
        /// <param name="workflow">The workflow to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateWorkflow(Model.DataLayer.AttributeWorkflow attribute)
        {
            return this.workflowRepository.UpdateWorkflow(attribute);
        }

        /// <summary>
        /// Gets the date of the last workflow created matching the input template.
        /// </summary>
        /// <param name="templateId">The input template.</param>
        /// <returns>The last workflow created matching the input template.</returns>
        public DateTime? GetLastWorkflowDate(int templateId)
        {
            if (ApplicationSettings.PreviousWorkflowTimeCheck == 0)
            {
                return null;
            }

            var date = this.workflowRepository.GetLastWorkflowDate(templateId);
            if (date != null)
            {
                var timeToCheck = DateTime.Now.AddHours(-ApplicationSettings.PreviousWorkflowTimeCheck);

                if (date >= timeToCheck)
                {
                    return date;
                }
            }

            return null;
        }

        /// <summary>
        /// Updates a workflow workflow.
        /// </summary>
        /// <param name="workflow">The workflow to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool CompleteWorkflow(Sale workflow)
        {
            return this.workflowRepository.CompleteWorkflow(workflow);
        }

        /// <summary>
        /// Adds a non standard response reason.
        /// </summary>
        /// <param name="reason">The reason to add.</param>
        /// <param name="attributeId">The associated attribute.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool AddNonStandardResponseReason(string reason, int attributeId)
        {
            return this.workflowRepository.AddNonStandardResponseReason(reason, attributeId);
        }

        /// <summary>
        /// Adds the alerts to the db.
        /// </summary>
        /// <param name="users">The users to alert.</param>
        /// <param name="message">The alert message.</param>
        /// <returns>Flag, indicating a successful add.</returns>
        public bool AddUserAlerts(HashSet<int> users,
            string message,
            int attributeMasterId,
            int? workflowId = null,
            int? goodsReceiptId = null,
            int? prOrderID = null,
            int? processID = null,
            int? ardispatchId = null)
        {
            var alerts = new List<Model.DataLayer.Alert>();

            foreach (var userId in users)
            {
                alerts.Add(new Model.DataLayer.Alert
                {
                    AlertMessage = message,
                    CreationDate = DateTime.Now,
                    DeviceMasterD = NouvemGlobal.DeviceId,
                    UserMasterID = NouvemGlobal.UserId,
                    UserMasterID_UserToAlert = userId,
                    NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                    AttributeTemplateRecordsID = workflowId,
                    APGoodsReceiptID = goodsReceiptId,
                    PROrderID = prOrderID,
                    ProcessID = processID,
                    ARDispatchID = ardispatchId,
                    AttributeMasterID = attributeMasterId
                });
            }

            return this.workflowRepository.AddUserAlerts(alerts);
        }

        /// <summary>
        /// Removes the alerts matching the input parameters.
        /// </summary>
        /// <param name="workflowiId">The workflow id.</param>
        /// <param name="attributeMasterId">The attribute id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool RemoveUserAlerts(int workflowiId, int attributeMasterId)
        {
            return this.workflowRepository.RemoveUserAlerts(workflowiId, attributeMasterId);
        }

        #endregion

        #endregion

        #endregion

        #region private

        #region helper

        /// <summary>
        /// Filters out orders by partner group.
        /// </summary>
        /// <param name="orders">Thge orders to filter.</param>
        /// <returns>The filtered orders.</returns>
        private IList<Sale> GetFilteredOrders(IList<Sale> orders)
        {
            if (ApplicationSettings.ShowOrHideGroupPartners == 1)
            {
                var localOrders = orders.Where(x => x.BPCustomer != null && x.BPCustomer.BPGroupID == ApplicationSettings.ShowOrHidePartnerGroupID).ToList();
                foreach (var localOrder in localOrders)
                {
                    orders.Remove(localOrder);
                }
            }
            else
            {
                var localOrders = orders.Where(x => x.BPCustomer != null && x.BPCustomer.BPGroupID != ApplicationSettings.ShowOrHidePartnerGroupID).ToList();
                foreach (var localOrder in localOrders)
                {
                    orders.Remove(localOrder);
                }
            }

            return orders;
        }

        /// <summary>
        /// Gets the associated category product.
        /// </summary>
        /// <param name="category">The category to check.</param>
        /// <returns>The associated category product</returns>
        public int GetKillProductByCategory(NouCategory category)
        {
            if (category.CAT.CompareIgnoringCase("A"))
            {
                return ApplicationSettings.ProductIdYoungBull;
            }

            if (category.CAT.CompareIgnoringCase("B"))
            {
                return ApplicationSettings.ProductIdBull;
            }

            if (category.CAT.CompareIgnoringCase("C"))
            {
                return ApplicationSettings.ProductIdSteer;
            }

            if (category.CAT.CompareIgnoringCase("D"))
            {
                return ApplicationSettings.ProductIdCow;
            }

            if (category.Name.CompareIgnoringCase("Ewe over 12 Months"))
            {
                return ApplicationSettings.ProductIdEwe;
            }

            if (category.Name.CompareIgnoringCase("Ewe"))
            {
                return ApplicationSettings.ProductIdEwe;
            }

            if (category.Name.CompareIgnoringCase("Lamb"))
            {
                return ApplicationSettings.ProductIdLamb;
            }

            if (category.Name.CompareIgnoringCase("Spring Lamb"))
            {
                return ApplicationSettings.ProductIdYSpringLamb;
            }

            if (category.Name.CompareIgnoringCase("Hogget"))
            {
                return ApplicationSettings.ProductIdHogget;
            }

            if (category.Name.CompareIgnoringCase("Ram"))
            {
                return ApplicationSettings.ProductIdRam;
            }

            if (category.Name.CompareIgnoringCase("Goat"))
            {
                return ApplicationSettings.ProductIdGoat;
            }

            if (category.CAT.CompareIgnoringCase("V"))
            {
                return ApplicationSettings.ProductIdVealYoung;
            }

            if (category.CAT.CompareIgnoringCase("Z"))
            {
                return ApplicationSettings.ProductIdVealOld;
            }

            if (category.Name.CompareIgnoringCase("Pig"))
            {
                return ApplicationSettings.ProductIdPig;
            }

            if (category.Name.CompareIgnoringCase("Piglet"))
            {
                return ApplicationSettings.ProductIdPiglet;
            }

            if (category.Name.CompareIgnoringCase("Sheep Full"))
            {
                return ApplicationSettings.ProductIdSheepFull;
            }

            return ApplicationSettings.ProductIdHeifer;
        }

        /// <summary>
        /// Stores the user search file to it's associated folder.
        /// </summary>
        /// <param name="xml">The xml string.</param>
        /// <param name="window">The window holding the data grid.</param>
        private void StoreGridSettings(string xml, ViewType window)
        {
            var xmlFile = XDocument.Parse(xml);

            switch (window)
            {
                case ViewType.BPSearchData:
                    xmlFile.Save(Settings.Default.SearchGridPath);
                    break;

                case ViewType.UserSearchData:
                    xmlFile.Save(Settings.Default.UserSearchGridPath);
                    break;

                case ViewType.INSearchData:
                    xmlFile.Save(Settings.Default.INSearchGridPath);
                    break;

                case ViewType.DeviceSearch:
                    xmlFile.Save(Settings.Default.DeviceSearchGridPath);
                    break;

                case ViewType.SalesSearchData:
                    xmlFile.Save(Settings.Default.SalesSearchGridPath);
                    break;

                case ViewType.SalesCustomerSearch:
                    xmlFile.Save(Settings.Default.SalesCustomerSearchGridPath);
                    break;

                case ViewType.PriceListDetail:
                    xmlFile.Save(Settings.Default.PriceListDetailPath);
                    break;

                case ViewType.INInventory:
                    xmlFile.Save(Settings.Default.ProductStockGridPath);
                    break;
            }
        }

        /// <summary>
        /// Creates the audit xml data changes string.
        /// </summary>
        /// <param name="data">The data to use.</param>
        /// <returns>A audit xml data string.</returns>
        private string CreateAuditXml(IList<Tuple<string, string, string>> data)
        {
            var dataXml = XmlHelper.CreateXml(data);
            return dataXml != null ? dataXml.ToString() : string.Empty;
        }

        /// <summary>
        /// Gets the changed property runtime values.
        /// </summary>
        /// <param name="data">The data to compare.</param>
        /// <returns>A collection of changed data.</returns>
        private IList<Tuple<string, string, string>> GetChangedProperties(IDictionary<object, object> data)
        {
            var propertyChanges = new List<Tuple<string, string, string>>();

            try
            {
                foreach (var o in data)
                {
                    var newObject = o.Key;
                    var snapshot = o.Value;

                    // get the properties of the new instance
                    foreach (var newObjectProperty in newObject.GetType().GetProperties())
                    {
                        // loop through the previous instance properties
                        foreach (var snapshotProperty in snapshot.GetType().GetProperties())
                        {
                            if (newObjectProperty.Name.Equals(snapshotProperty.Name))
                            {
                                // compare the old/new property values
                                var propertyValue = newObjectProperty.GetValue(newObject, null) ?? string.Empty;
                                var snapshotValue = snapshotProperty.GetValue(snapshot, null) ?? string.Empty;

                                if (!propertyValue.ToString().Equals(snapshotValue.ToString()))
                                {
                                    // value has changed, so store this data
                                    propertyChanges.Add(Tuple.Create(newObjectProperty.Name, propertyValue.ToString(), snapshotValue.ToString()));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }

            return propertyChanges;
        }

        #endregion

        #endregion
    }
}


﻿// -----------------------------------------------------------------------
// <copyright file="ReportManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;
using Nouvem.ViewModel;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Printing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.ReportService;

    /// <summary>
    /// Manager class for the application reporting.
    /// </summary>
    public class ReportManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Counter for exporting multiple reports.
        /// </summary>
        private int reportCount = 1;

        /// <summary>
        /// The direct print reference.
        /// </summary>
        private ReportPrintDocument directPrint;

        /// <summary>
        /// Is processing flag.
        /// </summary>
        private bool processing;

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly ReportManager Manager = new ReportManager();

        /// <summary>
        /// The report service web service helper object reference.
        /// </summary>
        private ReportingService2010 reportingService = new ReportingService2010();

        /// <summary>
        /// The reporting server reports.
        /// </summary>
        private IList<ReportData> reports = new List<ReportData>();

        /// <summary>
        /// The user report data.
        /// </summary>
        private IList<UserReport> userReports = new List<UserReport>();

        #endregion

        #region constructor

        private ReportManager()
        {
            this.userReports = this.DataManager.GetUserReports();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the report folders.
        /// </summary>
        public IList<ReportFolder> ReportFolders { get; set; }

        /// <summary>
        /// Gets the report manager singleton.
        /// </summary>
        public static ReportManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets the reports.
        /// </summary>
        public IList<ReportData> Reports
        {
            get
            {
                return this.reports;
            }
        }

        #endregion

        #region method 

        /// <summary>
        /// Gets the reporting server reports.
        /// </summary>
        public void GetReports()
        {
            this.Log.LogInfo(this.GetType(), string.Format("GetReports(): Connecting to the reporting service. URL:{0}", this.reportingService.Url));

            try
            {
                this.ReportFolders = this.DataManager.GetReportFolders();
                this.reports.Clear();
                var dbReports = this.DataManager.GetReports();
                var items = this.reportingService.ListChildren("/", true);
                foreach (var item in items.Where(x => x.TypeName.Equals("Report")))
                {
                    var dbReport = dbReports.FirstOrDefault(x => x.Name.CompareIgnoringCase(item.Name));
                    if (dbReport != null)
                    {
                        dbReport.Path = item.Path;
                        this.reports.Add(dbReport);
                    }
                    else
                    {
                        this.reports.Add(new ReportData { Name = item.Name, Path = item.Path });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Ex:{0}, Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Displays the search screen corresponding to the selected report.
        /// </summary>
        /// <param name="path">The selected report path.</param>
        /// <param name="report">The selected report name.</param>
        public void ProcessReport(string path, string report, bool isTouchscreen = false)
        {
            var localReport = this.reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(report));
            if (localReport != null && !string.IsNullOrEmpty(localReport.SearchGridMacro))
            {
                var token = isTouchscreen ? ViewType.ReportTouchscreenSearch : ViewType.ReportSearch;
                Messenger.Default.Send(localReport, token);
                return;
            }

            // no search grid attached to report, so display directly
            this.PreviewReport(new ReportData { Path = path, Name = report }, isTouchscreen);
        }

        /// <summary>
        /// Processes the module reports.
        /// </summary>
        /// <param name="module"></param>
        /// <param name="Id"></param>
        /// <param name="parameter"></param>
        /// <param name="print"></param>
        public async void ProcessModuleReports(int moduleId, string parameter, bool print)
        {
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    var localReports = this.DataManager.GetReportEmailLookups().Where(x =>
                            x.NouModuleID == moduleId && x.UseAtModule.IsTrue())
                        .GroupBy(x => x.ReportID)
                        .ToList();

                    foreach (var groupedReports in localReports)
                    {
                        var report = groupedReports.First();
                        var localReport = this.Reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(report.ReportName));
                        if (localReport == null)
                        {
                            continue;
                        }

                        var reportParams = new List<ReportParameter>();
                        ReportParameter reportParam;
                        if (localReport.IsHistoric.IsTrue())
                        {
                            reportParam = new ReportParameter { Name = localReport.HistoricParameter, Values = { parameter } };
                            reportParams.Add(reportParam);
                        }
                        else
                        {
                            if (localReport.ReportParams != null)
                            {
                                var param = localReport.ReportParams.FirstOrDefault();
                                if (param != null)
                                {
                                    reportParam = new ReportParameter { Name = param.Name, Values = { parameter } };
                                    reportParams.Add(reportParam);
                                }
                            }
                        }

                        var reportData = new ReportData { Name = report.ReportName, ReportParameters = reportParams };
                        if (!print)
                        {
                            this.PreviewReport(reportData);
                        }
                        else
                        {
                            this.PrintReport(reportData);
                        }
                    }
                });
            });
        }

        /// <summary>
        /// Emails the designated module report out.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="partnerId">The partner to email.</param>
        /// <param name="parameter">The parameter value.</param>
        public async void EmailModuleReports(string module, int partnerId, string parameter)
        {
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var localReports = this.DataManager.GetReportEmailLookups().Where(x =>
                        x.ModuleName.CompareIgnoringCaseAndWhitespace(module) && x.BPContact != null && x.BPContact.BPMasterID == partnerId)
                    .GroupBy(x => x.BPContact.BPMasterID)
                    .ToList();

                foreach (var groupedReportLookup in localReports)
                {
                    var reportLookUp = groupedReportLookup.First();
                    var showEmail = reportLookUp.ShowEmail.ToBool();
               
                    var localReport = this.Reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(reportLookUp.ReportName));
                    if (localReport == null)
                    {
                        continue;
                    }

                    var reportParams = new List<ReportParameter>();
                    ReportParameter reportParam;
                    if (localReport.IsHistoric.IsTrue())
                    {
                        reportParam = new ReportParameter { Name = localReport.HistoricParameter, Values = { parameter } };
                        reportParams.Add(reportParam);
                    }
                    else
                    {
                        if (localReport.ReportParams != null)
                        {
                            var param = localReport.ReportParams.FirstOrDefault();
                            if (param != null)
                            {
                                reportParam = new ReportParameter { Name = param.Name, Values = { parameter } };
                                reportParams.Add(reportParam);
                            }
                        }
                    }

                    var emailAddressList = new List<string>();
                    foreach (var reportEmail in groupedReportLookup.Where(x => x.ReportID == reportLookUp.ReportID))
                    {
                        if (reportEmail.BPContact != null && !string.IsNullOrWhiteSpace(reportEmail.BPContact.Email) && reportEmail.BPContact.Email.IsValidEmail())
                        {
                            emailAddressList.Add(reportEmail.BPContact.Email);
                        }
                    }

                    if (!emailAddressList.Any())
                    {
                        continue;
                    }

                    var reportData = new ReportData { Name = reportLookUp.ReportName, ReportParameters = reportParams, EmailAddressList = emailAddressList };

                    this.CreateReportForAttachment(reportData, ApplicationSettings.ExportReportPath);
                    this.AttachReportsToEmail(ApplicationSettings.ExportReportPath, emailAddressList, showEmail);
                }
            });
        }

        /// <summary>
        /// Displays the incoming reports.
        /// </summary>
        /// <param name="rows">The selected rows data.</param>
        /// <param name="report">The report data.</param>
        /// <param name="print">Print\Preview flag.</param>
        public void ShowReports(IList<CollectionData> rows, ReportData report, bool print = false, bool email = false)
        {
            this.reportCount = 1;
            ProgressBar.SetUp(0, rows.Count + 2);
            ProgressBar.Run();
            try
            {
                foreach (var row in rows)
                {
                    ProgressBar.Run();
                    this.ShowReport(row, report, print, email);
                }

                if (email)
                {
                    ProgressBar.Run();
                    this.AttachReportsToEmail(ApplicationSettings.ExportReportPath);
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Displays the incoming report.
        /// </summary>
        /// <param name="row">The selected row data.</param>
        /// <param name="report">The report data.</param>
        /// <param name="print">Print\Preview flag.</param>
        public void ShowReport(CollectionData row, ReportData report, bool print = false, bool email = false, bool isTouchscreen = false)
        {
            var reportParams = new List<ReportParameter>();
            foreach (var param in report.ReportParams)
            {
                var reportParam = new ReportParameter { Name = param.Name, Values = { this.GetParameterValue(param.Property, row.RowData) } };
                reportParams.Add(reportParam);
            }
         
            var reportData = new ReportData { Name = report.Name, ReportParameters = reportParams };

            if (email)
            {
                this.CreateReportForAttachment(reportData, ApplicationSettings.ExportReportPath, reportData.Name);
            }
            else if (!print)
            {
                this.PreviewReport(reportData, isTouchscreen);
            }
            else
            {
                this.PrintReport(reportData, isTouchscreen);
            }
        }

        /// <summary>
        /// Displays the incoming report.
        /// </summary>
        /// <param name="rows">The incoming multi row data.</param>
        /// <param name="report">The report data.</param>
        /// <param name="print">Print\Preview flag.</param>
        public void ShowReport(IList<CollectionData> rows, ReportData report, bool print = false, bool email = false)
        {
            var reportParams = new List<ReportParameter>();
            var paramCount = 1;
            foreach (var param in report.ReportParams)
            {
                if (paramCount == 1)
                {
                    var reportParam = new ReportParameter { Name = param.Name, Values = { this.GetParameterValue(param.Property, rows) } };
                    reportParams.Add(reportParam);
                }
                else
                {
                    var reportParam = new ReportParameter { Name = param.Name, Values = { this.GetParameterValue(param.Property, rows.First().RowData) } };
                    reportParams.Add(reportParam);
                }
                
                paramCount++;
            }

            var reportData = new ReportData { Name = report.Name, ReportParameters = reportParams };

            if (email)
            {
                this.CreateReportForAttachment(reportData, ApplicationSettings.ExportReportPath, reportData.Name);
                this.AttachReportsToEmail(ApplicationSettings.ExportReportPath);
            }
            else if (!print)
            {
                this.PreviewReport(reportData);
            }
            else
            {
                this.PrintReport(reportData);
            }
        }

        /// <summary>
        /// Sets the report settings.
        /// </summary>
        public void SetSettings()
        {
            if (this.Reports == null || !this.reports.Any())
            {
                try
                {
                    this.reportingService.Credentials = CredentialCache.DefaultCredentials;
                    this.reportingService.Url = ApplicationSettings.SSRSWebServiceURL;
                    this.reportingService.Credentials = new NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);

                    this.Log.LogInfo(this.GetType(), string.Format("Reportmanager - Credentials:{0},{1},{2}, URL:{3}", ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain, ApplicationSettings.SSRSWebServiceURL));

                    if (ApplicationSettings.ConnectToSSRS)
                    {
                        this.GetReports();
                    }
                }
                catch (Exception ex)
                {
                    this.Log.LogInfo(this.GetType(), ex.Message);
                }
            }
        }

        /// <summary>
        /// Prints a report directly without calling the report viewer.
        /// </summary>
        /// <param name="report">The server report to print.</param>
        /// <param name="isServerReport">Is the report a server report flag.</param>
        public void PrintReport(ReportData report, bool isServerReport = true)
        {
            try
            {
                if (!this.reports.Any())
                {
                    this.GetReports();
                }

                this.Log.LogInfo(this.GetType(), string.Format("PrintReport(): Attempting to locate report:{0}", report.Name));
                var localReport = this.reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(report.Name));

                if (localReport != null)
                {
                    var userReportData =
                    this.userReports.FirstOrDefault(
                        x => x.UserMasterID == NouvemGlobal.UserId && x.ReportName.CompareIgnoringCase(report.Name));
                    if (userReportData != null)
                    {
                        localReport.PrinterName = userReportData.PrinterName;
                    }

                    this.Log.LogInfo(this.GetType(), string.Format("Report found. Path:{0}, Printer:{1}", localReport.Path, localReport.PrinterName));
                    localReport.ReportParameters = report.ReportParameters;
                    var reportPath = localReport.Path;
                    var reportParameters = localReport.ReportParameters;
                    var serverReport = new ServerReport { ReportPath = reportPath };
                 
                    serverReport.ReportServerUrl = new Uri(ApplicationSettings.ReportServerPath);
                    serverReport.ReportServerCredentials.NetworkCredentials
                         = new System.Net.NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);

                    this.Log.LogInfo(this.GetType(), string.Format("Report Server URL:{0}, User Name:{1}, Password:{2}, Domain:{3}",
                        ApplicationSettings.ReportServerPath, ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain));

                    if (reportParameters != null)
                    {
                        serverReport.SetParameters(reportParameters);
                    }

                    this.directPrint = new ReportPrintDocument(serverReport);
                    if (this.directPrint != null)
                    {
                        if (!string.IsNullOrWhiteSpace(localReport.PrinterName))
                        {
                            this.directPrint.PrinterSettings.PrinterName = localReport.PrinterName;
                        }
                       
                        this.directPrint.Print();
                    }
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, Message.ReportNotPrinted);
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Send the report to the report viewer for processing.
        /// </summary>
        /// <param name="report">The report to print.</param>
        /// <returns>Flag, as to whether the report has been found.</returns>
        /// <param name="isTouchscreen">Is it a touchscreen report.</param>
        public bool PreviewReport(ReportData report, bool isTouchscreen = false)
        {
            if (!this.reports.Any())
            {
                this.GetReports();
            }

            this.Log.LogInfo(this.GetType(), string.Format("PrintReport(): Attempting to locate report:{0}", report.Name));
            var localReport = this.reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(report.Name));
      
            if (localReport != null)
            {
                var userReportData =
                    this.userReports.FirstOrDefault(
                        x => x.UserMasterID == NouvemGlobal.UserId && x.ReportName.CompareIgnoringCase(report.Name));
                if (userReportData != null)
                {
                    localReport.PrinterName = userReportData.PrinterName;
                }

                this.Log.LogInfo(this.GetType(), string.Format("Report found. Path:{0}, Printer:{1}", localReport.Path, localReport.PrinterName));
                localReport.ReportParameters = report.ReportParameters;

                if (isTouchscreen)
                {
                    Messenger.Default.Send(Token.Message, Token.DisplayTouchscreenReportViewer);
                    Messenger.Default.Send(localReport, Token.SetTouchscreenReportPath);
                }
                else
                {
                    if (!ApplicationSettings.TouchScreenMode)
                    {
                        Messenger.Default.Send(localReport, Token.MultiReport);
                    }
                    else
                    {
                        Messenger.Default.Send(localReport, Token.MultiReportTouchscreen);
                    }
                }
           
                return true;
            }

            return false;
        }

        /// Send the report to the report viewer for processing.
        /// </summary>
        /// <param name="report">The report to print.</param>
        /// <returns>Flag, as to whether the report has been found.</returns>
        public bool PreviewReports(IList<ReportData> reportDatas)
        {
            if (!reportDatas.Any())
            {
                return false;
            }

            if (!this.reports.Any())
            {
                this.GetReports();
            }
            
            var localReport = this.reports.FirstOrDefault(x => x.Name.Equals(reportDatas.First().Name));

            if (localReport != null)
            {
                Messenger.Default.Send(ViewType.ReportViewer);

                foreach (var report in reportDatas)
                {
                    localReport.ReportParameters = report.ReportParameters;
                    Messenger.Default.Send(localReport, Token.SetReportPathAndExport);
                }

                Messenger.Default.Send(Token.Message, Token.AttachReportsToEmail);
               
                return true;
            }

            return false;
        }

        /// <summary>
        /// Attach reports to the selected email account.
        /// </summary>
        public void AttachReportsToEmail(string filePath, IList<string> emailAddressList = null, bool display = true)
        {
            try
            {
                if (ApplicationSettings.SSRSEmailType.Equals("Outlook"))
                {
                    Microsoft.Office.Interop.Outlook.Application app =
                        new Microsoft.Office.Interop.Outlook.Application();
                    var mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
                    mailItem.To = ApplicationSettings.SSRSEmailTo;
                    mailItem.Subject = ApplicationSettings.SSRSEmailSubject;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = ApplicationSettings.SSRSEmailBody;

                    if (emailAddressList != null && emailAddressList.Any())
                    {
                        mailItem.To = emailAddressList.First();
                        emailAddressList.RemoveAt(0);
                        mailItem.CC = string.Join(",", emailAddressList.Select(x => x));
                    }

                    var exportType = ApplicationSettings.SSRSEmailExportFormat;
                    var searchpattern = string.Format("*.{0}", exportType);
                    foreach (var file in Directory.GetFiles(filePath, searchpattern))
                    {
                        mailItem.Attachments.Add(file, OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                    }

                    if (display)
                    {
                        mailItem.Display(true);
                    }
                    else
                    {
                        mailItem.Send();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
            finally
            {
                this.reportCount = 1;
                if (!string.IsNullOrEmpty(filePath))
                {
                    if (Directory.Exists(filePath))
                    {
                        Array.ForEach(Directory.GetFiles(filePath), File.Delete);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="filePath"></param>
        /// <param name="reportName"></param>
        public void CreateReportForAttachment(ReportData report, string filePath, string reportName = "Report")
        {
            try
            {
                if (!this.reports.Any())
                {
                    this.GetReports();
                }

                var localReport = this.reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(report.Name));

                if (localReport != null)
                {
                    localReport.ReportParameters = report.ReportParameters;
                    var reportPath = localReport.Path;
                    var reportParameters = localReport.ReportParameters;
                    var serverReport = new ServerReport { ReportPath = reportPath };

                    serverReport.ReportServerUrl = new Uri(ApplicationSettings.ReportServerPath);
                    serverReport.ReportServerCredentials.NetworkCredentials
                        = new System.Net.NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);

                    if (reportParameters != null)
                    {
                        serverReport.SetParameters(reportParameters);
                    }

                    this.ExportReports(serverReport, filePath, reportName);
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, Message.ReportNotPrinted);
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Creates a pallsation report.
        /// </summary>
        /// <param name="id"></param>
        public void CreatePalletisationReport(int id)
        {
            if (this.processing)
            {
                return;
            }

            this.processing = true;

            try
            {
                var palletStock = this.DataManager.GetPalletisationData(id);

                this.Log.LogInfo(this.GetType(),
                    string.Format("Exporting Labels: {0}", ApplicationSettings.LabelImagePath));
                foreach (var item in palletStock)
                {
                    ProgressBar.Run();
                    var label = LabelManager.Instance.GetGs1PalletLabel("Pallet Card");
                    if (label != null)
                    {
                        var palletId = item.PalletID.ToInt();
                        PrintManager.Instance.ExportLabelImage(label, palletId, ApplicationSettings.LabelImagePath);

                        var reportParam = new List<ReportParameter>
                        {
                            new ReportParameter {Name = "PalletID", Values = {palletId.ToString()}},
                        };

                        var reportData = new ReportData
                        {
                            Name = "Pallet Card",
                            ReportParameters = reportParam
                        };

                        this.PrintReport(reportData);
                    }
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
            finally
            {
                this.processing = false;
                ProgressBar.Reset();
            }
        }

        #endregion

        #region private

        private void ShowReport(bool print)
        {
            //if (this.reportToDisplay.Equals(ReportName.StockTraceReport))
            //{
            //    var reportParam = new List<ReportParameter> { new ReportParameter { Name = "LabelId", Values = { reportId.ToString() } } };
            //    var reportData = new ReportData { Name = ReportName.StockTraceReport, ReportParameters = reportParam };
            //    if (!print)
            //    {
            //        this.ReportManager.PreviewReport(reportData, isTouchscreen);
            //    }
            //    else
            //    {
            //        this.ReportManager.PrintReport(reportData, isTouchscreen);
            //    }

            //    return;
            //}
        }

        /// <summary>
        /// Exports the current report out to a pdf file
        /// </summary>
        /// <returns>Path to the file that was generated</returns>
        private string ExportReports(ServerReport report, string filePath, string reportName = "Report")
        {
            Microsoft.Reporting.WinForms.Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;
            var exportType = ApplicationSettings.SSRSEmailExportFormat;

            //Render the report to a byte array
            var bytes = report.Render(exportType, null, out mimeType,
                out encoding, out filenameExtension, out streamids, out warnings);

            //Write report out to temporary PDF file
            var reportSuffix = string.Format("{0}{1}.{2}", reportName, this.reportCount, exportType);
            var filename = Path.Combine(filePath, reportSuffix);
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            this.reportCount++;

            //return path to saved file
            return filename;
        }

        /// <summary>
        /// Gets the row value to pass to a report parameter.
        /// </summary>
        /// <param name="property">The report parameter name.</param>
        /// <param name="rowData">The row data to iterate.</param>
        /// <returns>A corresponding row value to pass to a report parameter.</returns>
        private string GetParameterValue(string property, IList<Tuple<string, string, string>> rowData)
        {
            var data = rowData.FirstOrDefault(x => x.Item2.CompareIgnoringCase(property));
            if (data != null)
            {
                return data.Item3;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the rows values to pass to a report parameter as a concatenated string.
        /// </summary>
        /// <param name="property">The report parameter name.</param>
        /// <param name="rows">The rows to iterate.</param>
        /// <returns>Rows values to pass to a report parameter as a concatenated string.</returns>
        private string GetParameterValue(string property, IList<CollectionData> rows)
        {
            var dataValues = new List<string>();
            foreach (var row in rows)
            {
                var rowData = row.RowData;
                var data = rowData.FirstOrDefault(x => x.Item2.CompareIgnoringCase(property));
                if (data != null)
                {
                    dataValues.Add(data.Item3);
                }
            }

            return string.Join(",", dataValues.Select(x => x));
        }

        #endregion

        #region nested class

        class ReportPrintDocument : PrintDocument
        {
            #region field

            /// <summary>
            /// The page settings.
            /// </summary>
            private PageSettings pageSettings;

            /// <summary>
            /// The current page we are working on.
            /// </summary>
            private int currentPage;

            /// <summary>
            /// The pages to stream.
            /// </summary>
            private readonly List<Stream> pages = new List<Stream>();

            #endregion

            #region public interface

            /// <summary>
            /// Prints the server report.
            /// </summary>
            /// <param name="serverReport">The report to print.</param>
            public ReportPrintDocument(ServerReport serverReport)
                : this((Report)serverReport)
            {
                this.RenderAllServerReportPages(serverReport);
            }

            /// <summary>
            /// Prints the local report.
            /// </summary>
            /// <param name="localReport">The local report to print.</param>
            public ReportPrintDocument(LocalReport localReport)
                : this((Report)localReport)
            {
                this.RenderAllLocalReportPages(localReport);
            }

            #endregion

            #region protected

            /// <summary>
            /// Dispose, and clean up.
            /// </summary>
            /// <param name="disposing">The disposing flag.</param>
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);

                if (disposing)
                {
                    foreach (Stream s in pages)
                    {
                        s.Dispose();
                    }

                    pages.Clear();
                }
            }

            /// <summary>
            /// Handle the print initialisation.
            /// </summary>
            /// <param name="e">The event args.</param>
            protected override void OnBeginPrint(PrintEventArgs e)
            {
                base.OnBeginPrint(e);
                this.currentPage = 0;
            }

            /// <summary>
            /// Handle the page printing.
            /// </summary>
            /// <param name="e">The event args.</param>
            protected override void OnPrintPage(PrintPageEventArgs e)
            {
                base.OnPrintPage(e);

                var pageToPrint = this.pages[this.currentPage];
                pageToPrint.Position = 0;

                // Load each page into a Metafile to draw it.
                using (Metafile pageMetaFile = new Metafile(pageToPrint))
                {
                    var adjustedRect = new Rectangle(
                            e.PageBounds.Left - (int)e.PageSettings.HardMarginX,
                            e.PageBounds.Top - (int)e.PageSettings.HardMarginY,
                            e.PageBounds.Width,
                            e.PageBounds.Height);

                    // Draw a white background for the report
                    e.Graphics.FillRectangle(Brushes.White, adjustedRect);

                    // Draw the report content
                    e.Graphics.DrawImage(pageMetaFile, adjustedRect);

                    // Prepare for next page.  Make sure we haven't hit the end.
                    this.currentPage++;
                    e.HasMorePages = this.currentPage < this.pages.Count;
                }
            }

            /// <summary>
            /// Clone the page settings.
            /// </summary>
            /// <param name="e">The event args.</param>
            protected override void OnQueryPageSettings(QueryPageSettingsEventArgs e)
            {
                e.PageSettings = (PageSettings)this.pageSettings.Clone();
            }

            #endregion

            #region private

            /// <summary>
            /// Set the dimensions.
            /// </summary>
            /// <param name="report">The report to set dimensions for.</param>
            private ReportPrintDocument(Report report)
            {
                // Set the page settings to the default defined in the report
                var reportPageSettings = report.GetDefaultPageSettings();

                /* The page settings object will use the default printer unless
                 * PageSettings.PrinterSettings is changed.  This assumes there               
                 * is a default printer.*/
                this.pageSettings = new PageSettings();
                this.pageSettings.PaperSize = reportPageSettings.PaperSize;
                this.pageSettings.Margins = reportPageSettings.Margins;
            }

            /// <summary>
            /// Render the report pages.
            /// </summary>
            /// <param name="serverReport">The server report to render.</param>
            private void RenderAllServerReportPages(ServerReport serverReport)
            {
                string deviceInfo = CreateEMFDeviceInfo();

                /* Generating Image renderer pages one at a time can be expensive.  In order
                 * to generate page 2, the server would need to recalculate page 1 and throw it
                 * away.  Using PersistStreams causes the server to generate all the pages in
                 * the background but return as soon as page 1 is complete. */
                var firstPageParameters = new NameValueCollection();
                firstPageParameters.Add("rs:PersistStreams", "True");

                // GetNextStream returns the next page in the sequence from the background process
                // started by PersistStreams.
                var nonFirstPageParameters = new NameValueCollection();
                nonFirstPageParameters.Add("rs:GetNextStream", "True");

                string mimeType;
                string fileExtension;
                var pageStream = serverReport.Render("IMAGE", deviceInfo, firstPageParameters, out mimeType, out fileExtension);

                // The server returns an empty stream when moving beyond the last page.
                while (pageStream.Length > 0)
                {
                    this.pages.Add(pageStream);

                    pageStream = serverReport.Render("IMAGE", deviceInfo, nonFirstPageParameters, out mimeType, out fileExtension);
                }
            }

            /// <summary>
            /// Render the local report pages.
            /// </summary>
            /// <param name="localReport">The local report to render.</param>
            private void RenderAllLocalReportPages(LocalReport localReport)
            {
                var deviceInfo = this.CreateEMFDeviceInfo();

                Microsoft.Reporting.WinForms.Warning[] warnings;
                localReport.Render("IMAGE", deviceInfo, this.LocalReportCreateStreamCallback, out warnings);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="extension"></param>
            /// <param name="encoding"></param>
            /// <param name="mimeType"></param>
            /// <param name="willSeek"></param>
            /// <returns></returns>
            private Stream LocalReportCreateStreamCallback(
                string name,
                string extension,
                Encoding encoding,
                string mimeType,
                bool willSeek)
            {
                var stream = new MemoryStream();
                this.pages.Add(stream);

                return stream;
            }

            private string CreateEMFDeviceInfo()
            {
                PaperSize paperSize = pageSettings.PaperSize;
                Margins margins = pageSettings.Margins;

                // The device info string defines the page range to print as well as the size of the page.
                // A start and end page of 0 means generate all pages.
                return string.Format(
                    CultureInfo.InvariantCulture,
                    "<DeviceInfo><OutputFormat>emf</OutputFormat><StartPage>0</StartPage><EndPage>0</EndPage><MarginTop>{0}</MarginTop><MarginLeft>{1}</MarginLeft><MarginRight>{2}</MarginRight><MarginBottom>{3}</MarginBottom><PageHeight>{4}</PageHeight><PageWidth>{5}</PageWidth></DeviceInfo>",
                    ToInches(margins.Top),
                    ToInches(margins.Left),
                    ToInches(margins.Right),
                    ToInches(margins.Bottom),
                    ToInches(paperSize.Height),
                    ToInches(paperSize.Width));
            }

            /// <summary>
            /// Calculate amount in 1/100 inchs.
            /// </summary>
            /// <param name="hundrethsOfInch">The value to calculate.</param>
            /// <returns></returns>
            private static string ToInches(int hundrethsOfInch)
            {
                double inches = hundrethsOfInch / 100.0;
                return inches.ToString(CultureInfo.InvariantCulture) + "in";
            }

            #endregion
        }

        #endregion
    }
}

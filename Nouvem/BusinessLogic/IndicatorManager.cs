﻿// -----------------------------------------------------------------------
// <copyright file="IndicatorManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Model.Enum;
using Nouvem.ViewModel;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Indicator;
    using Nouvem.Shared;

    /// <summary>
    /// Manager class for the application hardware.
    /// </summary>
    public class IndicatorManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly IndicatorManager Manager = new IndicatorManager();

        /// <summary>
        /// The current indicator.
        /// </summary>
        private int currentIndicator;

        /// <summary>
        /// The polling scales timer.
        /// </summary>
        private DispatcherTimer pollForStringTimer = new DispatcherTimer();

        #endregion

        #region constructor

        private IndicatorManager()
        {
            this.Indicator = new Indicator();
            this.Indicator2 = new Indicator();
            this.Indicator3 = new Indicator();

            if (ApplicationSettings.CurrentIndicator == 0)
            {
                this.CurrentIndicator = 1;
            }
            else
            {
                this.CurrentIndicator = ApplicationSettings.CurrentIndicator;
            }
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the current indicator.
        /// </summary>
        public int CurrentIndicator
        {
            get
            {
                return this.currentIndicator;
            }

            set
            {
                this.currentIndicator = value;
                ApplicationSettings.CurrentIndicator = value;
            }
        }

        /// <summary>
        /// Gets the indicator manager singleton.
        /// </summary>
        public static IndicatorManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets the weight mode.
        /// </summary>
        public string WeightMode
        {
            get
            {
                if (this.CurrentIndicator == 1)
                {
                    return this.Indicator.WeightMode;
                }

                if (this.CurrentIndicator == 2)
                {
                    return this.Indicator2.WeightMode;
                }

                return this.Indicator3.WeightMode;
            }
        }

        /// <summary>
        /// The indicator reference.
        /// </summary>
        public Indicator Indicator { get; set; }

        /// <summary>
        /// The indicator reference.
        /// </summary>
        public Indicator Indicator2 { get; set; }

        /// <summary>
        /// The indicator reference.
        /// </summary>
        public Indicator Indicator3 { get; set; }

        /// <summary>
        /// Gets the current captured weight.
        /// </summary>
        public string RecordedIndicatorWeight
        {
            get
            {
                if (this.CurrentIndicator == 1)
                {
                    return this.Indicator != null ? this.Indicator.RecordedWeight : string.Empty;
                }

                if (this.CurrentIndicator == 2)
                {
                    return this.Indicator2 != null ? this.Indicator2.RecordedWeight : string.Empty;
                }

                return this.Indicator3 != null ? this.Indicator3.RecordedWeight : string.Empty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current weight is stable, or not.
        /// </summary>
        public bool IsWeightStable
        {
            get
            {
                if (this.CurrentIndicator == 1)
                {
                    return this.Indicator.IsWeightStable;
                }

                if (this.CurrentIndicator == 2)
                {
                    return this.Indicator2.IsWeightStable;
                }

                return this.Indicator3.IsWeightStable;
            }
        }

        #endregion

        #region method

        #region Set up

        /// <summary>
        /// Connects to the indicator.
        /// </summary>
        public void ConnectToIndicator()
        {
            if (ApplicationSettings.ConnectToIndicator)
            {
                this.SetUpIndicator();
                this.OpenIndicatorPort();
                this.pollForStringTimer.Tick += (sender, args) =>
                {
                    this.SendScalesPollingString();
                };
            }
        }

        /// <summary>
        /// Opens a connection to the indicator.
        /// </summary>
        public void SetUpIndicator()
        {
            this.Log.LogInfo(this.GetType(), "SetUpIndicator. Setting up..");
            this.SetUpIndicator1();
            this.SetUpIndicator2();
            this.SetUpIndicator3();
            this.HandleScalesPolling();
        }

        /// <summary>
        /// Opens a connection to the indicator.
        /// </summary>
        public void SetUpIndicator1()
        {
            this.Log.LogInfo(this.GetType(), "SetUpIndicator1. Setting up..");

            try
            {
                #region indicator 1

                if (ApplicationSettings.IndicatorSettings == null)
                {
                    this.Log.LogError(this.GetType(), "Indicator1 null");
                    return;
                }

                var settings = ApplicationSettings.IndicatorSettings.Split(',');

                if (settings.Count() < 5)
                {
                    this.Log.LogError(this.GetType(), "No indicator data in the db");
                    return;
                }

                var com = settings.ElementAt(0);
                var baud = settings.ElementAt(1);
                var parity = settings.ElementAt(2);
                var stopBits = settings.ElementAt(3);
                var dataBits = settings.ElementAt(4);
                var indicatorType = (Indicator.IndicatorModel)Enum.Parse(typeof(Indicator.IndicatorModel), ApplicationSettings.IndicatorModel);
                var filePath = ApplicationSettings.IndicatorAlibiPath;

                this.Indicator = new Indicator(
                    com,
                    baud,
                    parity,
                    stopBits,
                    dataBits,
                    indicatorType,
                    filePath,
                    ApplicationSettings.ScalesDivision,
                    minWeightDivisions: ApplicationSettings.MinWeightInDivisions,
                    stableReads: ApplicationSettings.IndicatorStableReads,
                    waitTime: ApplicationSettings.IndicatorWaitTime,
                    tolerance: ApplicationSettings.ToleranceWeight,
                    usingStableReadsForMotion: ApplicationSettings.UseStableReadsForMotion,
                    ignoreNoMotionOnScalesWarning: ApplicationSettings.IgnoreNoMovementmentOnScalesWarning,
                    disturbBy: ApplicationSettings.DisturbBy,
                    dropBelow: ApplicationSettings.DropBelow
                    );

                this.Log.LogInfo(this.GetType(), string.Format("Indicator 1: Com:{0} Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}, Indicator Type:{5}, Alibi FilePath:{6}, " +
                                                               "Scales Division:{7}, Min Wgt Divisions:{8}, Stable Reads:{9}, Tolerance:{10}, Using Stable Reads For Motion:{11}," +
                                                               "Ignore No Motion Warning:{12}, Disturb By:{13}, Drop Below:{14}",
                   com, baud, parity, stopBits, dataBits, indicatorType, filePath, ApplicationSettings.ScalesDivision, ApplicationSettings.MinWeightInDivisions, ApplicationSettings.IndicatorStableReads,
                    ApplicationSettings.ToleranceWeight, ApplicationSettings.UseStableReadsForMotion.BoolToYesNo(), ApplicationSettings.IgnoreNoMovementmentOnScalesWarning.BoolToYesNo(), ApplicationSettings.DisturbBy, ApplicationSettings.DropBelow));

                var bufferString = ApplicationSettings.IndicatorStringVariables ?? string.Empty;
                var stringSettings = bufferString.Split(',');

                if (stringSettings.Length == 3)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var motionString = stringSettings[1];
                    var terminatingChar = stringSettings[2].ToInt().ToChar();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}, motionStr:{1}, term:{2}",
                        stringToReadLength, motionString, terminatingChar));

                    this.Indicator.SetPortDataVariables(stringToReadLength, 0, 0, 0, 0, motionString, terminatingChar);
                }
                else if (stringSettings.Length == 4)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var motionString = stringSettings[1];
                    var terminatingChar = stringSettings[2].ToInt().ToChar();
                    var weightStartPos = stringSettings[3].ToInt();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}, motionStr:{1}, term:{2},wgtStart:{3}",
                        stringToReadLength, motionString, terminatingChar,weightStartPos));

                    this.Indicator.SetPortDataVariables(stringToReadLength, weightStartPos, 0, 0, 0, motionString, terminatingChar);
                }
                else if (stringSettings.Length == 7)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var weightStartPos = stringSettings[1].ToInt();
                    var weightLength = stringSettings[2].ToInt();
                    var motionStartPos = stringSettings[3].ToInt();
                    var motionLength = stringSettings[4].ToInt();
                    var motionString = stringSettings[5];
                    var terminatingChar = stringSettings[6].ToInt().ToChar();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}" +
                                                                   "wgtStart:{1}, wgtLen:{2}, motionStart:{3}, motionLen:{4}, motionStr:{5}, term:{6}",
                                                                   stringToReadLength, weightStartPos, weightLength, motionStartPos, motionLength, motionString, terminatingChar));

                    this.Indicator.SetPortDataVariables(stringToReadLength, weightStartPos, weightLength, motionStartPos, motionLength, motionString, terminatingChar);
                }

                #endregion
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"Indicator 1:{e.Message}");
            }
        }

        /// <summary>
        /// Opens a connection to the indicator.
        /// </summary>
        public void SetUpIndicator2()
        {
            this.Log.LogInfo(this.GetType(), "SetUpIndicator2. Setting up..");

            try
            {
                #region indicator 2

                if (ApplicationSettings.Indicator2Settings == null)
                {
                    this.Log.LogError(this.GetType(), "Indicator2 null");
                    return;
                }

                var settings2 = ApplicationSettings.Indicator2Settings.Split(',');

                if (settings2.Count() < 5)
                {
                    this.Log.LogError(this.GetType(), "No indicator2 data in the db");
                    return;
                }

                var com = settings2.ElementAt(0);
                var baud = settings2.ElementAt(1);
                var parity = settings2.ElementAt(2);
                var stopBits = settings2.ElementAt(3);
                var dataBits = settings2.ElementAt(4);
                var indicatorType = (Indicator.IndicatorModel)Enum.Parse(typeof(Indicator.IndicatorModel), ApplicationSettings.Indicator2Model);
                var filePath = ApplicationSettings.IndicatorAlibiPath2;

                this.Indicator2 = new Indicator(
                    com,
                    baud,
                    parity,
                    stopBits,
                    dataBits,
                    indicatorType,
                    filePath,
                    ApplicationSettings.ScalesDivision2,
                    minWeightDivisions: ApplicationSettings.MinWeightInDivisions2,
                    stableReads: ApplicationSettings.IndicatorStableReads2,
                    waitTime: ApplicationSettings.IndicatorWaitTime2,
                    tolerance: ApplicationSettings.ToleranceWeight2,
                    usingStableReadsForMotion: ApplicationSettings.UseStableReadsForMotion2,
                    ignoreNoMotionOnScalesWarning: ApplicationSettings.IgnoreNoMovementmentOnScalesWarning2,
                    disturbBy: ApplicationSettings.DisturbBy2,
                    dropBelow: ApplicationSettings.DropBelow2
                    );

                this.Log.LogInfo(this.GetType(), string.Format("Indicator 1: Com:{0} Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}, Indicator Type:{5}, Alibi FilePath:{6}, " +
                                                               "Scales Division:{7}, Min Wgt Divisions:{8}, Stable Reads:{9}, Tolerance:{10}, Using Stable Reads For Motion:{11}," +
                                                               "Ignore No Motion Warning:{12}, Disturb By:{13}, Drop Below:{14}",
                    com, baud, parity, stopBits, dataBits, indicatorType, filePath, ApplicationSettings.ScalesDivision2, ApplicationSettings.MinWeightInDivisions2, ApplicationSettings.IndicatorStableReads2,
                    ApplicationSettings.ToleranceWeight2, ApplicationSettings.UseStableReadsForMotion2.BoolToYesNo(), ApplicationSettings.IgnoreNoMovementmentOnScalesWarning2.BoolToYesNo(), ApplicationSettings.DisturbBy2, ApplicationSettings.DropBelow2));

                var bufferString = ApplicationSettings.IndicatorStringVariables2 ?? string.Empty;
                var stringSettings = bufferString.Split(',');
                if (stringSettings.Length == 3)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var motionString = stringSettings[1];
                    var terminatingChar = stringSettings[2].ToInt().ToChar();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}, motionStr:{1}, term:{2}",
                        stringToReadLength, motionString, terminatingChar));

                    this.Indicator2.SetPortDataVariables(stringToReadLength, 0, 0, 0, 0, motionString, terminatingChar);
                }
                else if (stringSettings.Length == 4)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var motionString = stringSettings[1];
                    var terminatingChar = stringSettings[2].ToInt().ToChar();
                    var weightStartPos = stringSettings[3].ToInt();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}, motionStr:{1}, term:{2},wgtStart:{3}",
                        stringToReadLength, motionString, terminatingChar, weightStartPos));

                    this.Indicator.SetPortDataVariables(stringToReadLength, weightStartPos, 0, 0, 0, motionString, terminatingChar);
                }
                else if (stringSettings.Length == 7)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var weightStartPos = stringSettings[1].ToInt();
                    var weightLength = stringSettings[2].ToInt();
                    var motionStartPos = stringSettings[3].ToInt();
                    var motionLength = stringSettings[4].ToInt();
                    var motionString = stringSettings[5];
                    var terminatingChar = stringSettings[6].ToInt().ToChar();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings (2). stringToRead:{0}" +
                                                                   "wgtStart:{1}, wgtLen:{2}, motionStart:{3}, motionLen:{4}, motionStr:{5}, term:{6}",
                                                                   stringToReadLength, weightStartPos, weightLength, motionStartPos, motionLength, motionString, terminatingChar));

                    this.Indicator2.SetPortDataVariables(stringToReadLength, weightStartPos, weightLength, motionStartPos, motionLength, motionString, terminatingChar);
                }

                #endregion
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"Indicator 2:{e.Message}");
            }
        }

        /// <summary>
        /// Opens a connection to the indicator.
        /// </summary>
        public void SetUpIndicator3()
        {
            this.Log.LogInfo(this.GetType(), "SetUpIndicator3. Setting up..");
            try
            {
                #region indicator 3

                if (ApplicationSettings.Indicator3Settings == null)
                {
                    this.Log.LogError(this.GetType(), "Indicator3 null");
                    return;
                }

                var settings3 = ApplicationSettings.Indicator3Settings.Split(',');

                if (settings3.Count() < 5)
                {
                    this.Log.LogError(this.GetType(), "No indicator3 data in the db");
                    return;
                }

                var com = settings3.ElementAt(0);
                var baud = settings3.ElementAt(1);
                var parity = settings3.ElementAt(2);
                var stopBits = settings3.ElementAt(3);
                var dataBits = settings3.ElementAt(4);
                var indicatorType = (Indicator.IndicatorModel)Enum.Parse(typeof(Indicator.IndicatorModel), ApplicationSettings.Indicator3Model);
                var filePath = ApplicationSettings.IndicatorAlibiPath3;

                this.Indicator3 = new Indicator(
                    com,
                    baud,
                    parity,
                    stopBits,
                    dataBits,
                    indicatorType,
                    filePath,
                    ApplicationSettings.ScalesDivision3,
                    minWeightDivisions: ApplicationSettings.MinWeightInDivisions3,
                    stableReads: ApplicationSettings.IndicatorStableReads3,
                    waitTime: ApplicationSettings.IndicatorWaitTime3,
                    tolerance: ApplicationSettings.ToleranceWeight3,
                    usingStableReadsForMotion: ApplicationSettings.UseStableReadsForMotion3,
                    ignoreNoMotionOnScalesWarning: ApplicationSettings.IgnoreNoMovementmentOnScalesWarning3,
                    disturbBy: ApplicationSettings.DisturbBy3,
                    dropBelow: ApplicationSettings.DropBelow3
                    );

                this.Log.LogInfo(this.GetType(), string.Format("Indicator 1: Com:{0} Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}, Indicator Type:{5}, Alibi FilePath:{6}, " +
                                                               "Scales Division:{7}, Min Wgt Divisions:{8}, Stable Reads:{9}, Tolerance:{10}, Using Stable Reads For Motion:{11}," +
                                                               "Ignore No Motion Warning:{12}, Disturb By:{13}, Drop Below:{14}",
                    com, baud, parity, stopBits, dataBits, indicatorType, filePath, ApplicationSettings.ScalesDivision3, ApplicationSettings.MinWeightInDivisions3, ApplicationSettings.IndicatorStableReads3,
                    ApplicationSettings.ToleranceWeight3, ApplicationSettings.UseStableReadsForMotion3.BoolToYesNo(), ApplicationSettings.IgnoreNoMovementmentOnScalesWarning3.BoolToYesNo(), ApplicationSettings.DisturbBy3, ApplicationSettings.DropBelow3));

                var bufferString = ApplicationSettings.IndicatorStringVariables3 ?? string.Empty;
                var stringSettings = bufferString.Split(',');

                if (stringSettings.Length == 3)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var motionString = stringSettings[1];
                    var terminatingChar = stringSettings[2].ToInt().ToChar();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}, motionStr:{1}, term:{2}",
                        stringToReadLength, motionString, terminatingChar));

                    this.Indicator3.SetPortDataVariables(stringToReadLength, 0, 0, 0, 0, motionString, terminatingChar);
                }
                else if (stringSettings.Length == 4)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var motionString = stringSettings[1];
                    var terminatingChar = stringSettings[2].ToInt().ToChar();
                    var weightStartPos = stringSettings[3].ToInt();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings. stringToRead:{0}, motionStr:{1}, term:{2},wgtStart:{3}",
                        stringToReadLength, motionString, terminatingChar, weightStartPos));

                    this.Indicator.SetPortDataVariables(stringToReadLength, weightStartPos, 0, 0, 0, motionString, terminatingChar);
                }
                else if (stringSettings.Length == 7)
                {
                    var stringToReadLength = stringSettings[0].ToInt();
                    var weightStartPos = stringSettings[1].ToInt();
                    var weightLength = stringSettings[2].ToInt();
                    var motionStartPos = stringSettings[3].ToInt();
                    var motionLength = stringSettings[4].ToInt();
                    var motionString = stringSettings[5];
                    var terminatingChar = stringSettings[6].ToInt().ToChar();

                    this.Log.LogInfo(this.GetType(), string.Format("Setting string settings (3). stringToRead:{0}" +
                                                                   "wgtStart:{1}, wgtLen:{2}, motionStart:{3}, motionLen:{4}, motionStr:{5}, term:{6}",
                                                                   stringToReadLength, weightStartPos, weightLength, motionStartPos, motionLength, motionString, terminatingChar));

                    this.Indicator3.SetPortDataVariables(stringToReadLength, weightStartPos, weightLength, motionStartPos, motionLength, motionString, terminatingChar);
                }

                #endregion
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"Indicator 3:{e.Message}");
            }
        }

        /// <summary>
        /// Sets the scles to use.
        /// </summary>
        /// <param name="scales">The scales no to use.</param>
        public void SetScales(int? scales)
        {
            if (scales.IsNullOrZero())
            {
                scales = 1;
            }

            if (scales == this.CurrentIndicator)
            {
                return;
            }

            ViewModelLocator.IndicatorStatic.SetScales(scales.ToInt());
        }

        /// <summary>
        /// Opens the port.
        /// </summary>
        /// <returns>A flag, indicating a successful port opening.</returns>
        public bool OpenIndicatorPort()
        {
            Messenger.Default.Send(false, Token.SetIndicatorManualMode);
            if (this.CurrentIndicator == 0)
            {
                this.CurrentIndicator = 1;
            }

            if (this.CurrentIndicator == 1)
            {
                return this.OpenIndicator1Port();
            }

            if (this.CurrentIndicator == 2)
            {
                return this.OpenIndicator2Port();
            }

            return this.OpenIndicator3Port();
        }

        /// <summary>
        /// Sets up the scales polling.
        /// </summary>
        public void HandleScalesPolling()
        {
            #region validation

            if ((ApplicationSettings.PollScalesForStringInterval.IsNullOrZero() || !ApplicationSettings.ConnectToIndicator)
                && (ApplicationSettings.PollScalesForStringInterval2.IsNullOrZero() || !ApplicationSettings.ConnectToIndicator2)
                && (ApplicationSettings.PollScalesForStringInterval3.IsNullOrZero() || !ApplicationSettings.ConnectToIndicator3))
            {
                return;
            }

            #endregion

                if (this.CurrentIndicator == 0)
            {
                this.CurrentIndicator = 1;
            }

            this.pollForStringTimer.Stop();

            if (this.CurrentIndicator == 1)
            {
                if (!ApplicationSettings.PollScalesForStringInterval.IsNullOrZero())
                {
                    this.pollForStringTimer.Interval = TimeSpan.FromMilliseconds(ApplicationSettings.PollScalesForStringInterval.ToInt());
                    this.pollForStringTimer.Start();
                }

                return;
            }

            if (this.CurrentIndicator == 2)
            {
                if (!ApplicationSettings.PollScalesForStringInterval2.IsNullOrZero())
                {
                    this.pollForStringTimer.Interval = TimeSpan.FromMilliseconds(ApplicationSettings.PollScalesForStringInterval2.ToInt());
                    this.pollForStringTimer.Start();
                }

                return;
            }

            if (!ApplicationSettings.PollScalesForStringInterval3.IsNullOrZero())
            {
                this.pollForStringTimer.Interval = TimeSpan.FromMilliseconds(ApplicationSettings.PollScalesForStringInterval3.ToInt());
                this.pollForStringTimer.Start();
            }
        }

        /// <summary>
        /// Polls the scales for strings, when scales not in continuous mode.
        /// </summary>
        public void SendScalesPollingString()
        {
            if (this.CurrentIndicator == 0)
            {
                this.CurrentIndicator = 1;
            }
           
            if (this.CurrentIndicator == 1)
            {
                //this.Log.LogInfo(this.GetType(), $"Sending {ApplicationSettings.PollScalesString} to indicator 1");
                this.Indicator.WriteToPort(ApplicationSettings.PollScalesString + Environment.NewLine);
           
                return;
            }

            if (this.CurrentIndicator == 2)
            {
                //this.Log.LogInfo(this.GetType(), $"Sending {ApplicationSettings.PollScalesString} to indicator 2");
                this.Indicator2.WriteToPort(ApplicationSettings.PollScalesString2 + Environment.NewLine);

                return;
            }

            //this.Log.LogInfo(this.GetType(), $"Sending {ApplicationSettings.PollScalesString} to indicator 3");
            this.Indicator3.WriteToPort(ApplicationSettings.PollScalesString3 + Environment.NewLine);
        }

        /// <summary>
        /// Opens the port.
        /// </summary>
        /// <returns>A flag, indicating a successful port opening.</returns>
        public bool OpenIndicator1Port()
        {
            if (!ApplicationSettings.ConnectToIndicator)
            {
                this.Log.LogDebug(this.GetType(), "OpenIndicator1Port(): Connect to Indicator = false");
                return false;
            }

            try
            {
                if (this.IsIndicatorPortOpen())
                {
                    return true;
                }

                this.Log.LogInfo(this.GetType(), "OpenIndicatorPort(): Opening the indicator connection");
                this.Indicator.OpenPort();

                this.Log.LogInfo(this.GetType(), string.Format("Port open:{0}", this.Indicator.IsPortOpen));
                return this.Indicator.IsPortOpen;
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.Log.LogError(this.GetType(), ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Opens the port.
        /// </summary>
        /// <returns>A flag, indicating a successful port opening.</returns>
        public bool OpenIndicator2Port()
        {
            if (!ApplicationSettings.ConnectToIndicator2)
            {
                this.Log.LogDebug(this.GetType(), "OpenIndicator2Port(): Connect to Indicator = false");
                return false;
            }

            try
            {
                if (this.IsIndicator2PortOpen())
                {
                    return true;
                }

                this.Log.LogInfo(this.GetType(), "OpenIndicator2Port(): Opening the indicator connection");
                this.Indicator2.OpenPort();

                this.Log.LogInfo(this.GetType(), string.Format("Port open:{0}", this.Indicator2.IsPortOpen));
                return this.Indicator2.IsPortOpen;
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.Log.LogError(this.GetType(), ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Opens the port.
        /// </summary>
        /// <returns>A flag, indicating a successful port opening.</returns>
        public bool OpenIndicator3Port()
        {
            if (!ApplicationSettings.ConnectToIndicator3)
            {
                this.Log.LogDebug(this.GetType(), "OpenIndicator3Port(): Connect to Indicator = false");
                return false;
            }

            try
            {
                if (this.IsIndicator3PortOpen())
                {
                    return true;
                }

                this.Log.LogDebug(this.GetType(), "OpenIndicator3Port(): Opening the indicator connection");
                this.Indicator3.OpenPort();

                this.Log.LogDebug(this.GetType(), string.Format("Port open:{0}", this.Indicator3.IsPortOpen));
                return this.Indicator3.IsPortOpen;
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.Log.LogError(this.GetType(), ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Closes the port.
        /// </summary>
        public void CloseIndicatorPort()
        {
            try
            {
                Messenger.Default.Send(true, Token.SetIndicatorManualMode);
                this.Log.LogInfo(this.GetType(), "CloseIndicatorPort(): Closing the indicator connection");
                if (this.IsIndicatorPortOpen())
                {
                    this.Indicator.ClosePort();
                    this.Log.LogInfo(this.GetType(), "Port closed");
                }

                if (this.IsIndicator2PortOpen())
                {
                    this.Indicator2.ClosePort();
                    this.Log.LogInfo(this.GetType(), "Port closed");
                }

                if (this.IsIndicator3PortOpen())
                {
                    this.Indicator3.ClosePort();
                    this.Log.LogInfo(this.GetType(), "Port closed");
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), string.Format("Close Indicator Port:{0}", e.Message));
            }
        }

        /// <summary>
        /// Determines if the port is open.
        /// </summary>
        /// <returns>Flag, as to whether the port is open.</returns>
        public bool IsIndicatorPortOpen()
        {
            this.Log.LogDebug(this.GetType(), "IsIndicatorPortOpen(): Detecting..");
            bool result = false;
            if (this.CurrentIndicator == 1 && this.Indicator != null)
            {
                result = this.Indicator.IsPortOpen;
            }
            else if (this.CurrentIndicator == 2 && this.Indicator2 != null)
            {
                result = this.Indicator2.IsPortOpen;
            }
            else if (this.Indicator3 != null)
            {
                result = this.Indicator3.IsPortOpen;
            }

            this.Log.LogDebug(this.GetType(), string.Format("Port Open:{0}", result.BoolToYesNo()));
            return result;
        }

        /// <summary>
        /// Determines if the port is open.
        /// </summary>
        /// <returns>Flag, as to whether the port is open.</returns>
        public bool IsIndicator2PortOpen()
        {
            this.Log.LogDebug(this.GetType(), "IsIndicator2PortOpen(): Detecting..");
            var result = this.Indicator2 != null && this.Indicator2.IsPortOpen;
            this.Log.LogDebug(this.GetType(), string.Format("Port Open:{0}", result.BoolToYesNo()));
            return result;
        }

        /// <summary>
        /// Determines if the port is open.
        /// </summary>
        /// <returns>Flag, as to whether the port is open.</returns>
        public bool IsIndicator3PortOpen()
        {
            this.Log.LogDebug(this.GetType(), "IsIndicator3PortOpen(): Detecting..");
            var result = this.Indicator3 != null && this.Indicator3.IsPortOpen;
            this.Log.LogDebug(this.GetType(), string.Format("Port Open:{0}", result.BoolToYesNo()));
            return result;
        }

        /// <summary>
        /// Save the weight data.
        /// </summary>
        /// <returns>An empty string if weight validated and asved, otherwise the error message.</returns>
        public string SaveWeight(bool autoWeigh = false)
        {
            if (this.CurrentIndicator == 1)
            {
                return this.Indicator.SaveWeightData(autoWeigh);
            }

            if (this.CurrentIndicator == 2)
            {
                return this.Indicator2.SaveWeightData(autoWeigh);
            }

            return this.Indicator3.SaveWeightData(autoWeigh);
        }

        /// <summary>
        /// Sets the attributes to ignore if dealing with qty based products.
        /// </summary>
        /// <param name="ignore">The ignore or not flag.</param>
        public void IgnoreValidation(bool ignore)
        {
            if (this.Indicator != null)
            {
                this.Indicator.IgnoreValidation(ignore);
            }

            if (this.Indicator2 != null)
            {
                this.Indicator2.IgnoreValidation(ignore);
            }

            if (this.Indicator3 != null)
            {
                this.Indicator3.IgnoreValidation(ignore);
            }
        }

        /// <summary>
        /// Sets the attributes to ignore if dealing with qty based products.
        /// </summary>
        /// <param name="ignore">The ignore or not flag.</param>
        public void SetQtyBasedProductValues(bool ignore)
        {
            if (this.Indicator != null)
            {
                this.Indicator.SetQtyBasedProductValues(ignore);
            }

            if (this.Indicator2 != null)
            {
                this.Indicator2.SetQtyBasedProductValues(ignore);
            }

            if (this.Indicator3 != null)
            {
                this.Indicator3.SetQtyBasedProductValues(ignore);
            }
        }

        /// <summary>
        /// Sets the min wgt allowed.
        /// </summary>
        /// <param name="weight">The min wgt.</param>
        public void SetMinimumWeight(decimal weight)
        {
            if (this.Indicator != null)
            {
                this.Indicator.MinimumWeight = weight;
            }

            if (this.Indicator2 != null)
            {
                this.Indicator2.MinimumWeight = weight;
            }

            if (this.Indicator3 != null)
            {
                this.Indicator3.MinimumWeight = weight;
            }
        }

        /// <summary>
        /// Retrieves the data corresponding to the input alibi number.
        /// </summary>
        /// <param name="alibiNo">The input alibi number.</param>
        /// <returns>Data related to the input alibi data.</returns>
        public AlibiWeightInfo GetAlibiData(int alibiNo)
        {
            if (this.CurrentIndicator == 1)
            {
                return this.Indicator.GetAlibiData(alibiNo);
            }

            if (this.CurrentIndicator == 2)
            {
                return this.Indicator2.GetAlibiData(alibiNo);
            }

            return this.Indicator3.GetAlibiData(alibiNo);
        }

        /// <summary>
        /// Sets the indicator tare.
        /// </summary>
        public void SetTare(decimal tare)
        {
            if (this.CurrentIndicator == 1)
            {
                this.Indicator.Tare = tare;
            }

            if (this.CurrentIndicator == 2)
            {
                this.Indicator2.Tare = tare;
            }

            this.Indicator3.Tare = tare;
        }

        /// <summary>
        /// Retrieve the nmo cert checksum.
        /// </summary>
        /// <returns>The nmo cert checksum.</returns>
        public string GetCertifiedCheckSum()
        {
            if (this.CurrentIndicator == 1)
            {
                return this.Indicator.CertifiedChecksum;
            }

            if (this.CurrentIndicator == 2)
            {
                return this.Indicator2.CertifiedChecksum;
            }

            return this.Indicator3.CertifiedChecksum;
        }

        /// <summary>
        /// Retrieve the start up generated checksum.
        /// </summary>
        /// <returns>The nmo cert checksum.</returns>
        public string GetCurrentCheckSum()
        {
            if (this.CurrentIndicator == 1)
            {
                return this.Indicator.CurrentChecksum;
            }

            if (this.CurrentIndicator == 2)
            {
                return this.Indicator2.CurrentChecksum;
            }

            return this.Indicator3.CurrentChecksum;
        }

        #endregion

        /// <summary>
        /// Zeros the scales.
        /// </summary>
        public void ZeroScales()
        {
            if (this.CurrentIndicator == 1)
            {
                this.Indicator.WriteToPort(ApplicationSettings.ZeroScalesCommand);
            }

            if (this.CurrentIndicator == 2)
            {
                this.Indicator2.WriteToPort(ApplicationSettings.ZeroScalesCommand);
            }

            this.Indicator3.WriteToPort(ApplicationSettings.ZeroScalesCommand);
        }

        #endregion

        #region private

        #endregion
    }
}


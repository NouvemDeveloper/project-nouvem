﻿// -----------------------------------------------------------------------
// <copyright file="TerminalServicesManager.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Windows;
using Cassia;

namespace Nouvem.BusinessLogic
{
    using log4net.Config;
    using Nouvem.Global;

    public class TerminalServicesManager : BusinessLogicBase
    {
        public void Start()
        {
            var cassiaService = new Cassia.TerminalServicesManager();
            var name = cassiaService.CurrentSession.ClientName;
            var ip = cassiaService.CurrentSession.ClientIPAddress;
            var connectionState = cassiaService.CurrentSession.ConnectionState;
            var sessionId = cassiaService.CurrentSession.SessionId;
            NouvemGlobal.RDPIdentifier = name;
            NouvemGlobal.IsRDPConnection = !string.IsNullOrWhiteSpace(name);
            this.GetProcessOwner("Nouvem.exe");

            var logFileName = !string.IsNullOrWhiteSpace(name) ? name : Environment.MachineName;
            var path = string.Format(@"C:\Nouvem\Log\{0}\{0}", logFileName);
            log4net.GlobalContext.Properties["LogFileName"] = path;
            XmlConfigurator.Configure();

            var message =
                string.Format("***************  TERMINAL SERVICES: Name:{0}, IP:{1}, State:{2}, Session Id:{3} *******************",
                    name, ip, connectionState, sessionId);
            this.Log.LogInfo(this.GetType(), message);

            if (ip == null)
            {
                var nouvemProcessName = Process.GetCurrentProcess().ProcessName;
                this.Log.LogInfo(this.GetType(), string.Format("Application running locally. Checking if nouvem is already running: Process name:{0}", nouvemProcessName));

                //if (Process.GetProcesses().Count(p => p.ProcessName.Equals(nouvemProcessName)) > 1)
                //{
                //    this.Log.LogInfo(this.GetType(), "Process already running. Shutting down.");
                //    Application.Current.Shutdown();
                //    Process.GetCurrentProcess().Kill();
                //}

                return;
            }

            var sessions = cassiaService.GetSessions().Where(x => x.ConnectionState != ConnectionState.Disconnected).ToList();
            this.Log.LogInfo(this.GetType(), string.Format("Logged in sessions:{0}", sessions.Count));
            foreach (var terminalServicesSession in sessions)
            {
                var sessionName = terminalServicesSession.ClientName;
                var sessionip = terminalServicesSession.ClientIPAddress;
                var sessionConnectionState = terminalServicesSession.ConnectionState;
                var sessionSessionId = terminalServicesSession.SessionId;

                var localmessage =
                string.Format("Name:{0}, IP:{1}, State:{2}, Session Id:{3} *******************",
                    sessionName, sessionip, sessionConnectionState, sessionSessionId);
                this.Log.LogInfo(this.GetType(), localmessage);
            }

            var alreadyLoggedIn = sessions.Count(x => x.ClientName.Equals(name)) > 1;

            if (alreadyLoggedIn)
            {
                //this.Log.LogInfo(this.GetType(), "Already logged in over rdp: Shuttin down");
                //Application.Current.Shutdown();
                //Process.GetCurrentProcess().Kill();
            }
        }

        public string GetProcessOwner(string processName)
        {
            this.Log.LogInfo(this.GetType(), "Getting processes");
            try
            {
                string query = "Select * from Win32_Process Where Name = \"" + processName + "\"";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
                ManagementObjectCollection processList = searcher.Get();

                foreach (ManagementObject obj in processList)
                {
                    string[] argList = new string[] { string.Empty, string.Empty };
                    int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                    this.Log.LogInfo(this.GetType(), argList[1] + "\\" + argList[0]);
                    //if (returnVal == 0)
                    //{
                    //    // return DOMAIN\user
                    //    string owner = argList[1] + "\\" + argList[0];
                    //    return owner;
                    //}
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            return "NO OWNER";
        }
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="LicenceManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer;
    using Nouvem.Global;
    using Nouvem.Licencing;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Manager class for the licencing logic.
    /// </summary>
    public sealed class LicenceManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly LicenceManager Manager = new LicenceManager();

        /// <summary>
        /// The licence validator reference.
        /// </summary>
        private LicenseValidator licenceValidator = LicenseValidator.CreateNew();

        #endregion

        #region constructor

        private LicenceManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the licence manager singleton.
        /// </summary>
        public static LicenceManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Imports a selected licence.
        /// </summary>
        /// <param name="licenceFile">The licence to import.</param>
        /// <returns>The licence validation errors, if any.</returns>
        public string ImportLicence(string licenceFile)
        {
            var validationError = string.Empty;
            var licenceDetails = this.licenceValidator.ImportLicence(licenceFile);
            if (licenceDetails.ValidationErrors.Any())
            {
                validationError = string.Format("{0}; {1}", licenceDetails.ValidationErrors.First().Key,
                    licenceDetails.ValidationErrors.First().Value);
            }
            else
            {
                // licence validated, so import details.
                var dblicenceDetails = this.DataManager.StoreLicenceDetails(licenceDetails);
                if (!dblicenceDetails.Any())
                {
                    validationError = Message.DatabaseError;
                }

                if (validationError == string.Empty && licenceDetails.UpgradedFromLicenceKey != string.Empty)
                {
                    // this licence is an upgrade
                    this.UpgradeLicence(licenceDetails.UpgradedFromLicenceKey, dblicenceDetails);
                }
            }

            return validationError;
        }
       
        /// <summary>
        /// Gets and sets globally, the host licesee licence.
        /// </summary>
        public void CheckForLicence()
        {
            // check for a device licence first.
            NouvemGlobal.Licensees = this.DataManager.GetLicensees();

            // check for nouvem user
            if (ApplicationSettings.LoggedInUserName.Equals(Constant.NouvemLogin))
            {
                // nouvem engineer/installer..no licence.
                NouvemGlobal.Licensee = new Licensee {Identifier = Constant.NouvemLogin, LicenceeGroup = new UserGroup_{Description = Constant.Administrator}};
                return;
            }

            // check for device
            // var macAddress = NouvemGlobal.GetMacAddress();
            NouvemGlobal.Licensee =
                NouvemGlobal.Licensees.FirstOrDefault(x => x.IsDevice && x.Id == NouvemGlobal.DeviceId && x.LicenceDetailsId != null);

            if (NouvemGlobal.Licensee != null)
            {
                // device licencee match
                return;
            }

            // check for user
            NouvemGlobal.Licensee =
                NouvemGlobal.Licensees.FirstOrDefault(x => !x.IsDevice && x.Identifier.Equals(ApplicationSettings.LoggedInUserIdentifier) && x.LicenceDetailsId != null);

            if (NouvemGlobal.Licensee != null)
            {
                // user licencee match
                return;
            }

            // no licencee match. Create default 'no licence'.
            NouvemGlobal.Licensee = new Licensee();
        }

        /// <summary>
        /// Gets and sets globally, the host licesee licence.
        /// </summary>
        public void CheckForUserLicence()
        {
            // check for nouvem user
            if (ApplicationSettings.LoggedInUserName.Equals(Constant.NouvemLogin))
            {
                // nouvem engineer/installer..no licence.
                NouvemGlobal.Licensee = new Licensee { Identifier = Constant.NouvemLogin, LicenceeGroup = new UserGroup_ { Description = Constant.Administrator } };
                return;
            }

            // check for user
            NouvemGlobal.Licensee =
                NouvemGlobal.Licensees.FirstOrDefault(x => !x.IsDevice && x.Identifier.Equals(ApplicationSettings.LoggedInUserIdentifier) && x.LicenceDetailsId != null);

            if (NouvemGlobal.Licensee != null)
            {
                // user licencee match
                return;
            }

            // no licencee match. Create default 'no licence'.
            NouvemGlobal.Licensee = new Licensee();
        }

        /// <summary>
        /// Returns the days remaining on the licence.
        /// </summary>
        /// <returns>The days remaining on the licence</returns>
        public int GetLicenceDaysRemaining()
        {
            return NouvemGlobal.Licensee.ValidTo.ToDate().Subtract(DateTime.Today).Days;
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Handles the upgrading of a licence.
        /// </summary>
        /// <param name="oldlicenceKey">The old licence key.</param>
        /// <param name="details">The new licence details.</param>
        /// <returns>A flag, indicating a successful upgrade or not.</returns>
        private bool UpgradeLicence(string oldlicenceKey, IEnumerable<LicenceDetail> details)
        {
            var oldLicences =
                NouvemGlobal.Licensees.Where(x => x.LicenceKey != null && x.LicenceKey.Equals(oldlicenceKey)).ToList();

            if (oldLicences.Any())
            {
                var licenceData = new Dictionary<Licensee, LicenceDetail>();
                oldLicences.ForEach(x =>
                {
                    var data =
                        details.FirstOrDefault(detail => detail.NouLicenceTypeID == x.LicenseType.NouLicenceTypeID);

                    if (data != null)
                    {
                        licenceData.Add(x, data);
                    }
                });

                // allocate the new licences (this will also deallocate the old licences from their licensees)
                return this.DataManager.AssignLicenses(licenceData);
            }

            return false;
        }

        #endregion
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ProductionManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;

    public class ProductionManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly ProductionManager Manager = new ProductionManager();

        #endregion

        #region constructor

        private ProductionManager()
        {
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the production manager singleton.
        /// </summary>
        public static ProductionManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public Tuple<int, List<int?>> AddBoxTransaction(StockTransaction transaction, decimal boxTare = 0, int qtyPerBox = 1, bool enforceBoxQtyMatch = false)
        {
            return this.DataManager.AddBoxTransaction(transaction, boxTare, qtyPerBox, enforceBoxQtyMatch);
        }

        /// <summary>
        /// Retrieves the products alloed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>A collection of allowable products.</returns>
        public IList<SaleDetail> GetProductsAllowedOutOfProduction(int prSpecId)
        {
            return this.DataManager.GetProductsAllowedOutOfProduction(prSpecId);
        }

        /// <summary>
        /// Retrieves the products alloed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>A collection of allowable products.</returns>
        public IList<InventoryItem> GetAllProductsAllowedOutOfProduction(int prSpecId)
        {
            return this.DataManager.GetAllProductsAllowedOutOfProduction(prSpecId);
        }

        #endregion

        #endregion

        #region private

        #endregion
    }
}

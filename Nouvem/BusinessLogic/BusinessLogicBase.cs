﻿// -----------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.BusinessLogic
{
    using Nouvem.Logging;

    /// <summary>
    /// Base business object class.
    /// </summary>
    public abstract  class BusinessLogicBase
    {
        /// <summary>
        /// The data manager reference.
        /// </summary>
        protected DataManager DataManager = DataManager.Instance;

        /// <summary>
        /// The logging reference.
        /// </summary>
        protected ILogger Log = new Logger();
    }
}

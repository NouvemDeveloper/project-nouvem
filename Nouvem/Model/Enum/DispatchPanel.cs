﻿// -----------------------------------------------------------------------
// <copyright file="DispatchPanel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum DispatchPanel
    {
        /// <summary>
        /// Standard intake
        /// </summary>
        Standard,

        /// <summary>
        /// Production order created as part of intake.
        /// </summary>
        RecordWeight,

        /// <summary>
        /// Production order created as part of intake.
        /// </summary>
        RecordWeightWithNotes,

        /// <summary>
        /// Production order created as part of intake.
        /// </summary>
        RecordWeightWithShipping,
    }
}


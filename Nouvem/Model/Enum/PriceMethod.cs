﻿// -----------------------------------------------------------------------
// <copyright file="PriceMethod.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum PriceMethod
    {
        /// <summary>
        /// Price x qty
        /// </summary>
        Quantity,

        /// <summary>
        /// Price x wgt
        /// </summary>
        Weight,

        /// <summary>
        /// Price x wgt
        /// </summary>
        TypicalQtyDividedByPrice
    }
}

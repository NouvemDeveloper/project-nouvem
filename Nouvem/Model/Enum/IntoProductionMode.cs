﻿// -----------------------------------------------------------------------
// <copyright file="IntoProductionMode.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum IntoProductionMode
    {
        /// <summary>
        /// Standard, production order creation mode.
        /// </summary>
        IntoProductionStandard,

        /// <summary>
        /// Production order recalled from intake.
        /// </summary>
        RecallOrder,

        /// <summary>
        /// Product selected.
        /// </summary>
        IntoProductionSelectProduct,

        /// <summary>
        /// Product selected.
        /// </summary>
        SelectAllProduct,

        /// <summary>
        /// Scan and split a carcass.
        /// </summary>
        IntoProductionCarcassSplit,

        /// <summary>
        /// Scan and put in whole carcass.
        /// </summary>
        Carcass,

        Recipe,
        IntoProductionConformance,
        IntoProductionBatch,

        /// <summary>
        /// Scan and put in whole carcass, using the scanned weight.
        /// </summary>
        CarcassNoReweigh,

        /// <summary>
        /// The pork into slicing mode.
        /// </summary>
        Butchery,

        /// <summary>
        /// The pork into injection mode.
        /// </summary>
        Injection,

        /// <summary>
        /// Slicing mode.
        /// </summary>
        IntoSlicing,

        /// <summary>
        /// Jointing mode.
        /// </summary>
        IntoJointing,
    }
}

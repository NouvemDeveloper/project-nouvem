﻿// -----------------------------------------------------------------------
// <copyright file="StockMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum StockMode
    {
        /// <summary>
        /// The serial based mode.
        /// </summary>
        Serial,

        /// <summary>
        /// The batch qty mode.
        /// </summary>
        BatchQty,

        /// <summary>
        /// The batch weight mode.
        /// </summary>
        BatchWeight,

        /// <summary>
        /// The batch container qty mode.
        /// </summary>
        BatchQtyAndWeight,

        /// <summary>
        /// The batch qty mode.
        /// </summary>
        ProductQty,

        /// <summary>
        /// The batch weight mode.
        /// </summary>
        ProductWeight,

        /// <summary>
        /// The batch container qty mode.
        /// </summary>
        ProductQtyAndWeight,

        /// <summary>
        /// The edit transaction mode.
        /// </summary>
        EditTransaction
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IntoProductionMode.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum OutOfProductionMode
    {
        /// <summary>
        /// Standard, production order creation mode.
        /// </summary>
        OutOfProductionStandard,

        /// <summary>
        /// Production order recalled from intake.
        /// </summary>
        OutOfProductionWithPieceLabels,

        /// <summary>
        /// Butchery mode.
        /// </summary>
        OutOfButchery,

        /// <summary>
        /// Injection mode.
        /// </summary>
        OutOfInjection
    }
}

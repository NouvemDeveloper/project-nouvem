﻿// -----------------------------------------------------------------------
// <copyright file="ScannerMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// The dispatch stock modes.
    /// </summary>
    public enum ScannerMode
    {
        /// <summary>
        /// Scanning out stock.
        /// </summary>
        Scan,

        /// <summary>
        /// Minus scanning stock.
        /// </summary>
        MinusScan,

        /// <summary>
        /// Reprint label mode.
        /// </summary>
        Reprint,
        
        /// <summary>
        /// Reweigh mode.
        /// </summary>
        ReWeigh,

        /// <summary>
        /// Reweigh mode.
        /// </summary>
        Split,

        /// <summary>
        /// Scan and take a manual weight.
        /// </summary>
        ScanPlusManualWeigh
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="View.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum ViewType
    {
        /// <summary>
        /// The contact view
        /// </summary>
        Contact,

        /// <summary>
        /// The contact update view
        /// </summary>
        ContactUpdate,

        /// <summary>
        /// The report transaction data view
        /// </summary>
        ReportTransactionData,

        /// <summary>
        /// The stock take view
        /// </summary>
        StockMove,

        Recipe,
        PricingMatrix,

        QuickOrder,

        Palletisation,
        PalletsCard,
        PalletsCardMultiBatch,
        CarcassDispatch,

        /// <summary>
        /// The stock take view
        /// </summary>
        StockTake,

        /// <summary>
        /// The stock take view
        /// </summary>
        StockMovement,

        /// <summary>
        /// The stock take view
        /// </summary>
        SystemInformation,

        /// <summary>
        /// The stock take view
        /// </summary>
        BatchEdit,

        /// <summary>
        /// The stock take view
        /// </summary>
        BatchEditor,

        /// <summary>
        /// The stock take view
        /// </summary>
        RecipeSearch,

        /// <summary>
        /// The stock take view
        /// </summary>
        BatchEditOrdered,

        /// <summary>
        /// The workflow view
        /// </summary>
        Workflow,

        /// <summary>
        /// The workflow view
        /// </summary>
        WorkflowSearch,

        /// <summary>
        /// The workflow view
        /// </summary>
        WorkflowAlerts,

        /// <summary>
        /// The group view
        /// </summary>
        Group,

        /// <summary>
        /// The menu view
        /// </summary>
        Menu,

        /// <summary>
        /// The scanner into production view.
        /// </summary>
        ScannerIntoProduction,

        /// <summary>
        /// The INInventory view
        /// </summary>
        INInventory,

        /// <summary>
        /// The accounts view
        /// </summary>
        Accounts,

        StockAdjust,
        ReportSetUp,
        ReportSearch,
        ReportTouchscreenSearch,
        ReportFolders,
        ReportProcess,
        Department,
        BatchData,

        /// <summary>
        /// The purchases accounts view
        /// </summary>
        PurchasesAccounts,
        ReturnsAccounts,
        ImageView,

        /// <summary>
        /// The purchases accounts view
        /// </summary>
        SyncPartners,

        SyncProducts,
        SyncPrices,

        /// <summary>
        /// The document trail view
        /// </summary>
        DocumentTrail,

        /// <summary>
        /// The dwait indicator view
        /// </summary>
        WaitIndicator,

        /// <summary>
        /// The invoice creation view
        /// </summary>
        InvoiceCreation,

        /// <summary>
        /// The invoice creation view
        /// </summary>
        APInvoiceCreation,

        CreditNoteCreation,

        /// <summary>
        /// The module selectionview
        /// </summary>
        TouchscreenModuleSelection,

        /// <summary>
        /// The order view
        /// </summary>
        Order,

        /// <summary>
        /// The order view
        /// </summary>
        IntakeOrder,

        /// <summary>
        /// The order view
        /// </summary>
        Order2,

        /// <summary>
        /// The quality view
        /// </summary>
        Quality,

        /// <summary>
        /// The sql generator view
        /// </summary>
        SqlGenerator,

        /// <summary>
        /// The data generator view
        /// </summary>
        DataGenerator,

        /// <summary>
        /// The transaction search view
        /// </summary>
        TransactionSearchData,

        /// <summary>
        /// The quality template name view
        /// </summary>
        QualityTemplateName,

        /// <summary>
        /// The quality template allocation view
        /// </summary>
        QualityTemplateAllocation,

        /// <summary>
        /// The group update view
        /// </summary>
        GroupUpdate,

        /// <summary>
        /// The report viewer view
        /// </summary>
        ReportViewer,

        /// <summary>
        /// The label association view
        /// </summary>
        LabelAssociation,

        /// <summary>
        /// The invoice view
        /// </summary>
        Invoice,

        /// <summary>
        /// The batch invoice view
        /// </summary>
        BatchInvoice,

        /// <summary>
        /// The batch invoice view
        /// </summary>
        CreditNote,

        CreateCreditNote,

        /// <summary>
        /// The invoice view
        /// </summary>
        PurchaseInvoice,

        /// <summary>
        /// The batch invoice view
        /// </summary>
        PurchaseBatchInvoice,

        /// <summary>
        /// The report product data view
        /// </summary>
        ReportProductData,

        /// <summary>
        /// The partner master view.
        /// </summary>
        Master,

        /// <summary>
        /// The partner master view.
        /// </summary>
        BPMaster,

        /// <summary>
        /// The partner master search data view.
        /// </summary>
        BPSearchData,

        /// <summary>
        /// The production view.
        /// </summary>
        Production,

        /// <summary>
        /// The production batch set up view.
        /// </summary>
        ProductionBatchSetUp,

        /// <summary>
        /// The ap receipt details view.
        /// </summary>
        APReceiptDetails,

        /// <summary>
        /// The user set up view.
        /// </summary>
        UserSetUp,

        User,

        /// <summary>
        /// The user search data view.
        /// </summary>
        UserSearchData,

        /// <summary>
        /// The sale contents view.
        /// </summary>
        SaleContents,

        /// <summary>
        /// The user group view.
        /// </summary>
        UserGroup,

        UserSearch,

        /// <summary>
        /// The user change password view.
        /// </summary>
        UserChangePassword,

        /// <summary>
        /// The plant view.
        /// </summary>
        Plant,

        /// <summary>
        /// The warehouse view.
        /// </summary>
        Warehouse,

        /// <summary>
        /// The warehouse view.
        /// </summary>
        AttributeMasterSetUp,

        /// <summary>
        /// The template allocation view.
        /// </summary>
        TemplateAllocation,

        /// <summary>
        /// The warehouse location view.
        /// </summary>
        WarehouseLocation,

        /// <summary>
        /// The map view.
        /// </summary>
        Map,

        /// <summary>
        /// The currency view.
        /// </summary>
        Currency,

        /// <summary>
        /// The partner group set up view.
        /// </summary>
        BPGroupSetUp,

        /// <summary>
        /// The message history view.
        /// </summary>
        MessageHistory,

        /// <summary>
        /// The server set up selection view.
        /// </summary>
        ServerSetUp,

        /// <summary>
        /// The pricing master set up view.
        /// </summary>
        PricingMaster,

        Tabs,

        Notes,

        /// <summary>
        /// The all prices view.
        /// </summary>
        AllPrices,

        /// <summary>
        /// The all prices view.
        /// </summary>
        TelesalesSearch,

        /// <summary>
        /// The special prices set up view.
        /// </summary>
        SpecialPrices,

        /// <summary>
        /// The special prices set up view.
        /// </summary>
        Specifications,

        /// <summary>
        /// The special prices set up view.
        /// </summary>
        BatchSetUp,

        /// <summary>
        /// The price list detail set up view.
        /// </summary>
        PriceListDetail,

        /// <summary>
        /// The production selection view.
        /// </summary>
        ProductSelection,

        /// <summary>
        /// The inventory search data view.
        /// </summary>
        INSearchData,

        /// <summary>
        /// The audit view.
        /// </summary>
        Audit,

        /// <summary>
        /// The ap quote view.
        /// </summary>
        APQuote,

        /// <summary>
        /// The ap order view.
        /// </summary>
        APOrder,

        /// <summary>
        /// The ap order view.
        /// </summary>
        LairageKillInformation,

        /// <summary>
        /// The ar dispatch view.
        /// </summary>
        ARDispatch,

        /// <summary>
        /// The ar dispatch view.
        /// </summary>
        Pallets,

        /// <summary>
        /// The ar dispatch view.
        /// </summary>
        ARDispatch2,

        /// <summary>
        /// The ar dispatch view.
        /// </summary>
        DispatchContainer,

        FTrace,

        /// <summary>
        /// The ar dispatch view.
        /// </summary>
        ARDispatchAuthorisation,

        /// <summary>
        /// The ar dispatch details view.
        /// </summary>
        ARDispatchDetails,

        /// <summary>
        /// The ar dispatch details view.
        /// </summary>
        IntakeDetails,

        /// <summary>
        /// The bp property set up selection view.
        /// </summary>
        BPPropertySetUp,

        /// <summary>
        /// The bp property set up selection view.
        /// </summary>
        ContainerAccountsView,

        /// <summary>
        /// The item master property set up selection view.
        /// </summary>
        IMPropertySetUp,

        /// <summary>
        /// The login selection view.
        /// </summary>
        LoginSelection,

        /// <summary>
        /// The login view.
        /// </summary>
        Login,

        /// <summary>
        /// The UOM view.
        /// </summary>
        UOM,

        /// <summary>
        /// The sales customer search view.
        /// </summary>
        SalesCustomerSearch,

        /// <summary>
        /// The group authorisations view.
        /// </summary>
        Authorisations,

        /// <summary>
        /// The device set up view.
        /// </summary>
        DeviceSetUp,

        /// <summary>
        /// The device set up view.
        /// </summary>
        DeviceSettings,

        /// <summary>
        /// The device set up view.
        /// </summary>
        QualityAssuranceView,

        /// <summary>
        /// The keyboard view.
        /// </summary>
        Keyboard,

        /// <summary>
        /// The keyboard view.
        /// </summary>
        KeyboardPassword,

        /// <summary>
        /// The small keyboard view.
        /// </summary>
        KeyboardSmall,

        /// <summary>
        /// The into production view.
        /// </summary>
        IntoProduction,

        /// <summary>
        /// The into production view.
        /// </summary>
        Specs,

        /// <summary>
        /// The out of production view.
        /// </summary>
        OutOfProduction,

        /// <summary>
        /// The country set up view.
        /// </summary>
        CountrySetUp,

        /// <summary>
        /// The collection display view.
        /// </summary>
        CollectionDisplay,

        RoutesDisplay,

        /// <summary>
        /// The collection display view.
        /// </summary>
        CollectionDisplayDesktop,

        /// <summary>
        /// The collection display view.
        /// </summary>
        ReportDisplay,

        /// <summary>
        /// The collection display view.
        /// </summary>
        MultiSelectReportDisplay,

        /// <summary>
        /// The container set up view.
        /// </summary>
        ContainerSetUp,

        /// <summary>
        /// The container set up view.
        /// </summary>
        Touchscreen,

        /// <summary>
        /// The device search view.
        /// </summary>
        DeviceSearch,

        /// <summary>
        /// The epos settings view.
        /// </summary>
        EposSettings,

        /// <summary>
        /// The license admin view.
        /// </summary>
        LicenseAdmin,

        /// <summary>
        /// The LicenseImport view.
        /// </summary>
        LicenseImport,

        /// <summary>
        /// The Label design view.
        /// </summary>
        LabelDesign,

        /// <summary>
        /// The report design view.
        /// </summary>
        ReportDesign,

        /// <summary>
        /// The GS1AI view.
        /// </summary>
        GS1AI,

        /// <summary>
        /// The Date view.
        /// </summary>
        Date,

        /// <summary>
        /// The APReceipt view.
        /// </summary>
        APReceipt,
        ARReturns,
        ApplyPrice,

        /// <summary>
        /// The Sales search data view.
        /// </summary>
        SalesSearchData,

        SpecsSearch,

        /// <summary>
        /// The Sales search data view.
        /// </summary>
        TelesalesSetUp,

        /// <summary>
        /// The Sales search data view.
        /// </summary>
        TelesalesEndCall,

        /// <summary>
        /// The Sales search data view.
        /// </summary>
        Telesales,

        /// <summary>
        /// The Sales search data view.
        /// </summary>
        AttributeSearch,

        /// <summary>
        /// The edit transaction data view.
        /// </summary>
        TransactionEditor,

        /// <summary>
        /// The traceability view.
        /// </summary>
        Traceability,

        /// <summary>
        /// The traceability view.
        /// </summary>
        AlertUsersSetUp,

        /// <summary>
        /// The traceability template name view.
        /// </summary>
        TraceabilityTemplateName,

        /// <summary>
        /// The traceability template name view.
        /// </summary>
        AttributeTemplateName,

        /// <summary>
        /// The traceability template group view.
        /// </summary>
        AttributeTemplateGroup,

        /// <summary>
        /// The traceability template group view.
        /// </summary>
        WeightBandGroup,

        /// <summary>
        /// The traceability template group view.
        /// </summary>
        WeightBand,

        /// <summary>
        /// The traceability template group view.
        /// </summary>
        WeightBandPricing,

        /// <summary>
        /// The date template name view.
        /// </summary>
        DateTemplateName,

        /// <summary>
        /// The date template allocation view.
        /// </summary>
        DateTemplateAllocation,

        /// <summary>
        /// The inventory group view.
        /// </summary>
        InventoryGroup,

        /// <summary>
        /// The inventory group set up view.
        /// </summary>
        InventoryGroupSetUp,

        /// <summary>
        /// The inventory master view.
        /// </summary>
        InventoryMaster,

        /// <summary>
        /// The inventory search data view.
        /// </summary>
        InventorySearchData,

        /// <summary>
        /// The traceability template allocation view.
        /// </summary>
        TraceabilityTemplateAllocation,

        /// <summary>
        /// The EPOS view.
        /// </summary>
        Epos,
        MRP,

        /// <summary>
        /// The vat set up view.
        /// </summary>
        VatSetUp,

        /// <summary>
        /// The production search data view.
        /// </summary>
        ProductionSearchData,

        /// <summary>
        /// The document type set up view.
        /// </summary>
        DocumentType,

        /// <summary>
        /// The document number set up view.
        /// </summary>
        DocumentNumber,

        /// <summary>
        /// The region view.
        /// </summary>
        Region,

        /// <summary>
        /// The route view.
        ///  </summary>
        Route,

        /// <summary>
        /// The quote view.
        /// </summary>
        Quote,

        /// <summary>
        /// The factory touch screen view.
        /// </summary>
        FactoryTouchScreen,

        /// <summary>
        /// The scanner dispatch screen view.
        /// </summary>
        ScannerDispatch,

        /// <summary>
        /// The lairage order view.
        /// </summary>
        LairageOrder,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        LairageIntake,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        LairageIntakeTouchscreen,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        Identigen,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        BeefReport,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        ChequePaymentReport,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        RPAReport,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        KillDetails,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        AnimalMovements,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        AnimalPricing,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        LairageSearch,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        LairageAnimalsSearch,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        PaymentDeductions,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        PaymentProposalCreation,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        PaymentProposalOpen,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        Payment,


        /// <summary>
        /// The lairage intake view.
        /// </summary>
        Sequencer,

        /// <summary>
        /// The lairage intake view.
        /// </summary>
        Grader,

        /// <summary>
        /// The scanner factory screen view.
        /// </summary>
        ScannerFactory,

        /// <summary>
        /// The stock reconciliation view
        /// </summary>
        StockReconciliation,

        /// <summary>
        /// The key board search view
        /// </summary>
        KeyboardSearch,

        /// <summary>
        /// The suppliers touch screen view.
        /// </summary>
        TouchscreenSuppliers,

        /// <summary>
        /// The orders touch screen view.
        /// </summary>
        TouchscreenOrders,

        /// <summary>
        /// The production orders touch screen view.
        /// </summary>
        TouchscreenProductionOrders,

        /// <summary>
        /// The touchscreen traceabilityview.
        /// </summary>
        TouchscreenTraceability,

        /// <summary>
        /// The transaction details screen view.
        /// </summary>
        TransactionDetails,

        /// <summary>
        /// The goods in touch screen view.
        /// </summary>
        APReceiptTouchscreen,

        /// <summary>
        /// The receipt items view.
        /// </summary>
        APReceiptItems,

        /// <summary>
        /// The application message box.
        /// </summary>
        NouvemMessageBox
    }
}

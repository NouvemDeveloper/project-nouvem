﻿// -----------------------------------------------------------------------
// <copyright file="IntakeMode.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum SequencerMode
    {
        /// <summary>
        /// Sequencing beef
        /// </summary>
        Beef,

        /// <summary>
        /// Sequencing sheep.
        /// </summary>
        Sheep
    }
}

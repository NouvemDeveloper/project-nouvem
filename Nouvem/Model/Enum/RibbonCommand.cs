﻿// -----------------------------------------------------------------------
// <copyright file="RibbonCommand.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration that models the ribbon bar commands.
    /// </summary>
    public enum RibbonCommand
    {
        /// <summary>
        /// Navigation first
        /// </summary>
        First,

        /// <summary>
        /// Navigation last
        /// </summary>
        Last,

        /// <summary>
        /// Navigation back
        /// </summary>
        Back,

        /// <summary>
        /// Navigation next
        /// </summary>
        Next,

        /// <summary>
        /// Navigation last edit
        /// </summary>
        LastEdit
    }
}

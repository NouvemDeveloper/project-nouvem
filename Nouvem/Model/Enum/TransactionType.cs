﻿// -----------------------------------------------------------------------
// <copyright file="TransactionType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration that models the application transaction types.
    /// </summary>
    public enum TransactionType
    {
        /// <summary>
        /// Epos transaction type.
        /// </summary>
        SaleEpos,

        /// <summary>
        /// lairage intake transaction type.
        /// </summary>
        Lairage,

        /// <summary>
        /// The goods receipt transaction type.
        /// </summary>
        GoodsReceipt,

        /// <summary>
        /// The goods delivery transaction type.
        /// </summary>
        GoodsDelivery,

        /// <summary>
        /// The goods return transaction type.
        /// </summary>
        GoodsReturn,

        /// <summary>
        /// The stock adjust transaction type.
        /// </summary>
        StockAdjust,

         /// <summary>
        /// The production issue type.
        /// </summary>
        ProductionIssue,

        /// <summary>
        /// The production receipt type.
        /// </summary>
        ProductionReceipt,

        /// <summary>
        /// The transaction edit transaction type.
        /// </summary>
        Edit,

        /// <summary>
        /// The goods return to head office type.
        /// </summary>
        GoodsReturnToHeadOffice
    }
}

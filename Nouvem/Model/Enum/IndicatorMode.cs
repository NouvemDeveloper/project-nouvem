﻿// -----------------------------------------------------------------------
// <copyright file="IndicatorMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum IndicatorMode
    {
        /// <summary>
        /// The serial communication mode.
        /// </summary>
        Serial
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="AccountsPackage.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum AccountsPackage
    {
        /// <summary>
        /// Tas books.
        /// </summary>
        TASBooks,

        /// <summary>
        /// Sage 50.
        /// </summary>
        Sage50,

        /// <summary>
        /// Sage 200.
        /// </summary>
        Sage200,


        /// <summary>
        /// Exchequer.
        /// </summary>
        Exchequer
    }
}

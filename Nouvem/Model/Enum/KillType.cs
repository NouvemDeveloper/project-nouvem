﻿// -----------------------------------------------------------------------
// <copyright file="KillType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum KillType
    {
        /// <summary>
        /// The box label type.
        /// </summary>
        Beef,

        /// <summary>
        /// The item label type.
        /// </summary>
        Sheep,

        /// <summary>
        /// The piece label type.
        /// </summary>
        Pig
    }
}


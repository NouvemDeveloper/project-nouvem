﻿// -----------------------------------------------------------------------
// <copyright file="PasswordCheckType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration that holds the password check types..
    /// </summary>
    public enum PasswordCheckType
    {
        /// <summary>
        /// The engineer/reseller database/server set up password.
        /// </summary>
        ServerSetUp
    }
}

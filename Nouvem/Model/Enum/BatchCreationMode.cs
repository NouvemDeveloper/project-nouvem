﻿// -----------------------------------------------------------------------
// <copyright file="BatchCreationMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum BatchCreationMode
    {
        /// <summary>
        /// Creates a new batch only when user decides.
        /// </summary>
        Manual,

        /// <summary>
        /// Creates a new batch for every product.
        /// </summary>
        OnEveryProduct
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IntakeMode.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum IntakeMode
    {
        /// <summary>
        /// Standard intake
        /// </summary>
        Intake,

        /// <summary>
        /// Standard with import intake
        /// </summary>
        IntakeImport,

        /// <summary>
        /// Standard with import intake
        /// </summary>
        IntakeMultiBatch,

        /// <summary>
        /// Standard with import intake
        /// </summary>
        IntakeWithPallet,

        /// <summary>
        /// Production order created as part of intake.
        /// </summary>
        WithProductionOrder,

        /// <summary>
        /// The returns module.
        /// </summary>
        Returns
    }
}

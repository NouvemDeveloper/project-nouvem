﻿// -----------------------------------------------------------------------
// <copyright file="PriceMethod.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum WeighMode
    {
         /// <summary>
        /// Manual mode.
        /// </summary>
        Manual,

        /// <summary>
        /// Auto mode.
        /// </summary>
        Auto
    }
}


//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;

    public partial class INGroup : INotifyPropertyChanged
    {
        public INGroup()
        {
            this.INGroup1 = new HashSet<INGroup>();
            this.INMasters = new HashSet<INMaster>();
            this.LabelAssociations = new HashSet<LabelAssociation>();
        }

        public int INGroupID { get; set; }
        public string Name { get; set; }
        public Nullable<int> ParentInGroupID { get; set; }
        public Nullable<int> TraceabilityTemplateNameID { get; set; }
        public Nullable<int> DateTemplateNameID { get; set; }
        public Nullable<int> QualityTemplateNameID { get; set; }
        public bool Deleted { get; set; }
        public Nullable<int> AttributeTemplateID { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }

        public virtual DateTemplateName DateTemplateName { get; set; }
        public virtual ICollection<INGroup> INGroup1 { get; set; }
        public virtual INGroup INGroupParent { get; set; }
        public virtual TraceabilityTemplateName TraceabilityTemplateName { get; set; }
        public virtual ICollection<INMaster> INMasters { get; set; }
        public virtual AttributeTemplate AttributeTemplate { get; set; }
        public virtual ICollection<LabelAssociation> LabelAssociations { get; set; }

        private bool swapProduct;

        public bool SwapProduct
        {
            get { return this.swapProduct; }
            set
            {
                this.swapProduct = value;
                this.OnPropertyChanged("SwapProduct");
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

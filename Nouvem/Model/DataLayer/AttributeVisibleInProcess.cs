//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttributeVisibleInProcess
    {
        public int AttributeVisibleInProcessID { get; set; }
        public Nullable<int> AttributeAllocationID { get; set; }
        public Nullable<int> ProcessID { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
    
        public virtual Process Process { get; set; }
    }
}

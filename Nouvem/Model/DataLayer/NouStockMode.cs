//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class NouStockMode
    {
        public NouStockMode()
        {
            this.INMasters = new HashSet<INMaster>();
        }
    
        public int NouStockModeID { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
    
        public virtual ICollection<INMaster> INMasters { get; set; }
    }
}

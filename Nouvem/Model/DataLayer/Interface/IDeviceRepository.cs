﻿// -----------------------------------------------------------------------
// <copyright file="IDeviceRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface IDeviceRepository
    {
        /// <summary>
        /// Add a new device to the database.
        /// </summary>
        /// <param name="device">The device to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        bool AddDevice(DeviceMaster device, int? copyId);

        /// <summary>
        /// Method that updates an existing device.
        /// </summary>
        /// <param name="device">The device to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateDevice(DeviceMaster device, int? copyId);

        /// <summary>
        /// Retrieve all the database devices.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        IList<App_GetDeviceSettings_Result> GetDeviceSettings();

        /// <summary>
        /// Method that updates an existing device.
        /// </summary>
        /// <param name="device">The device to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
         bool UpdateDeviceWithLookups(DeviceMaster device, IList<int> departments, int? copyId);

        /// <summary>
        /// Add a new device to the database.
        /// </summary>
        /// <param name="device">The device to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        bool AddDeviceWithLookUp(DeviceMaster device, IList<int> departments, int? copyId);

        /// <summary>
        /// Sets the default module process.
        /// </summary>
        /// <param name="module">The module to set.</param>
        /// <param name="process">The default process.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool SetDefaultModuleProcess(string module, string process, int deviceId);

        /// <summary>
        /// Retrieves any new entities that need to be added locally.
        /// </summary>
        /// <param name="deviceId">The local device.</param>
        /// <returns>A list of new entities that are to be added locally.</returns>
        IList<EntityUpdate> CheckForNewEntities(int deviceId, string entityType = "");

        /// <summary>
        /// Determines if a device name already exists in the db.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns>Flag, as to whether the device already exists.</returns>
        bool IsDeviceNameInSystem(string name);

        /// <summary>
        /// Sets the aplication updates..
        /// </summary>
        /// <returns>Flag, as to whether the update have been updated..</returns>
        bool SetApplicationUpdates();

        /// <summary>
        /// Determines if there are new price edits to apply.
        /// </summary>
        /// <returns>Flag, as to whether there are new edits to apply.</returns>
        bool AreTherePriceEdits();

        /// <summary>
        /// Retrieve all the database devices.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        IList<DeviceMaster> GetDevices();

        /// <summary>
        /// Updates the current device settings.
        /// </summary>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateDeviceSettings(IList<DeviceSetting> settings, bool updateSettings);

        /// <summary>
        /// Gets the current device settings.
        /// </summary>
        /// <returns>The current device settings.</returns>
       IList<DeviceSetting> GetDeviceSettings(int deviceId);

        /// <summary>
        /// Logs data to the db.
        /// </summary>
        /// <param name="log">The data to log.</param>
        /// <returns>Flag, as to successful log or not.</returns>
        bool LogToDatabase(Log log);
    }
}

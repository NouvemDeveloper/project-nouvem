﻿// -----------------------------------------------------------------------
// <copyright file="IBatchRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Nouvem.Model.DataLayer.Interface
{
    public interface IBatchRepository
    {
        /// <summary>
        /// Generates a sequential batch number.
        /// </summary>
        /// <returns>A sequential batch number.</returns>
        BatchNumber GenerateSequentialNumber(string reference, bool referenceOnly = false);

        /// <summary>
        /// Gets a batch number.
        /// </summary>
        /// <returns>A batch number.</returns>
        BatchNumber GetBatchNumber(string batchNumber);

        /// <summary>
        /// Gets the batchs.
        /// </summary>
        /// <returns>The collection of batch options.</returns>
        IList<App_GetEditBatches_Result> GetBatches(int inmasterid);

        /// <summary>
        /// Gets the used batch weight for a recipe batch line.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <param name="inMasterId">The line product id.</param>
        /// <returns>Used batch data for a recipe batch.</returns>
        App_GetRecipeUsedBatchWeight_Result GetLastRecipeUsedBatchWeight(int batchId, int inMasterId);

        /// <summary>
        /// Gets the last recipe batch data for a recipe ingredient line.
        /// </summary>
        /// <param name="prOrderID">The recipe batch.</param>
        /// <param name="inMasterId">The ingredient id.</param>
        /// <returns>The last recipe batch data for a recipe ingredient line.</returns>
        App_GetLastRecipeProductBatch_Result GetLastRecipeProductBatch(int prOrderID, int inMasterId);

        /// <summary>
        /// Generates a manually entered batch number.
        /// </summary>
        /// <param name="batchNo">The manually generated batch number.</param>
        /// <returns>The newly generated batch number.</returns>
        BatchNumber GenerateManualEntryNumber(BatchNumber batchNo);

        /// <summary>
        /// Returns the last batch number associated with a batch product.
        /// </summary>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>The last associated product batch.</returns>
        BatchNumber GetLastProductBatch(int inMasterId);

        /// <summary>
        /// Gets the batch options.
        /// </summary>
        /// <returns>The collection of batch options.</returns>
        IList<BatchOption> GetBatchOptions();

        /// <summary>
        /// Gets a batch number.
        /// </summary>
        /// <returns>A batch number.</returns>
        BatchNumber GetBatchNumber(int batchNumberId);
    }
}

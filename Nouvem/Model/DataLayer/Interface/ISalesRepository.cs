﻿// -----------------------------------------------------------------------
// <copyright file="ISalesRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Data;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;

    public interface ISalesRepository
    {
        /// <summary>
        /// Returns the application document status items.
        /// </summary>
        /// <returns>The application document status items.</returns>
        IList<NouDocStatu> GetNouDocStatusItems();

        /// <summary>
        /// Gets the dispatch data for the input dispatches.
        /// </summary>
        /// <param name="dispatches"></param>
        /// <returns></returns>
        IList<App_CarcassDispatch_Result> GetCarcassDispatchData(string dispatches);

        /// <summary>
        /// Gets the FTrace slaughter data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        bool MarkFTraceAsExported(int dispatchId);

        /// <summary>
        /// Gets the quick order view data.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        IList<App_GetQuickOrderViewData_Result> GetQuickOrderViewData(string mode);

        /// <summary>
        /// Gets the quick order stock view data.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        IList<App_GetQuickOrderStockViewData_Result> GetQuickOrderStockViewData(string type, string stockType, int id);

        /// <summary>
        /// Adds or updates a quick order.
        /// </summary>
        /// <param name="dispatchId">The dispatch id to update.</param>
        /// <param name="bpMasterId">The partner id.</param>
        /// <param name="stocktransactionId">The stock item to add.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>The dispatch id if newly created, and error message if one.</returns>
        Tuple<string, int> AddOrUpdateQuickOrder(int dispatchId, int bpMasterId, int stocktransactionId,
            int userId, int deviceId);

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        DataTable GetFTraceSlaughterOnly(int dispatchId);

        /// <summary>
        /// Gets the batch order data for a product line.
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        IList<App_GetOrderBatchData_Result> GetOrderBatchData(int orderLineId);

        /// <summary>
        /// Gets a sale order line product data.
        /// </summary>
        /// <param name="productId">The product to retrieve data for.</param>
        /// <returns>A summary of the products stock and order amount.</returns>
        Tuple<string, decimal, decimal, decimal, decimal> GetProductStockAndOrderData(int productId);

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        DataTable GetFTraceDispatch(int dispatchId);

        /// <summary>
        /// Gets the FTrace slaughter data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        DataTable GetFTraceSlaughter(int dispatchId);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetRecentDispatches(int customerId);

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        DataTable GetFTraceProcessing(int dispatchId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllARDispatchesForReportByDates(IList<int?> orderStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        DataTable GetFTraceDispatches(bool showAll, DateTime start, DateTime end);

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>The newly created quote id, indicating a successful quote add, or not.</returns>
        int AddNewQuote(Sale sale);

        /// <summary>
        /// Gets pallet dispatch data.
        /// </summary>
        /// <param name="id">The dispatch id.</param>
        /// <returns>Pallet data.</returns>
        IList<App_GetDispatchPalletStock_Result> GetDispatchPalletData(int id);

        /// <summary>
        /// Gets the telesale call instances.
        /// </summary>
        /// <returns>The acall instances.</returns>
        IList<TelesaleCall> GetTelesaleCalls();

        /// <summary>
        /// Updates an existing quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateQuote(Sale sale);

        /// <summary>
        /// Adds a telesale call.
        /// </summary>
        /// <param name="telesale">The telesale to add or update.</param>
        /// <returns>Flag, as to a successful add/update.</returns>
        bool AddTelesaleCall(TelesaleCall telesale);

        /// <summary>
        /// Retrieves all the quotes (and associated quote details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all quotes (and associated quote details).</returns>
        IList<Sale> GetAllQuotes(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the quote (and associated quote details) for the input id.
        /// </summary>
        /// <param name="quoteId">The customer id.</param>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        Sale GetQuoteByID(int quoteId);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        IList<Sale> GetRecentQuotes(int customerId);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        IList<Sale> GetQuotes(int customerId);

        /// <summary>
        /// Retrieves the quote (and associated quote details) for the input id.
        /// </summary>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        Sale GetQuoteByLastEdit();

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        IList<Sale> GetQuotesByName(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        IList<Sale> GetQuotesByCode(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        int AddNewOrder(Sale sale);

        /// <summary>
        /// Updates an existing order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateOrder(Sale sale);

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllOrdersByDate(IList<int?> orderStatuses, DateTime start, DateTime end, string dateType);

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        Sale GetOrderByFirstLast(bool first);

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllOrders(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetOrders(int customerId);

        /// <summary>
        /// Determines if a sale orders master dispatch docket has any transactions recorded against it.
        /// </summary>
        /// <param name="orderId">The order to check.</param>
        /// <returns>A flag, indicating whether the sale orders master dispatch docket has any transactions recorded against it. </returns>
        bool DoesMasterDocumentHaveTransactions(int orderId);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetRecentOrders(int customerId);

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        Sale GetOrderByID(int orderId);

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        Sale GetOrderByLastEdit();

        /// <summary>
        /// Retrieves the quotes (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetOrdersByName(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the qorders (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetOrdersByCode(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves all the orders.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllOrdersForReport(IList<int?> orderStatuses);

        /// <summary>
        /// Removes a quote detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveArQuoteDetail(int itemId);

        /// <summary>
        /// Removes an order detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveArOrderDetail(int itemId);

        /// <summary> Adds a new purchase receipt (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        int AddAPDispatch(Sale sale);

        /// <summary>
        /// Updates an existing dispatch docket (docket and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateARDispatch(Sale sale);

        /// <summary>
        /// Removes an ARDispatchDetail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveArDispatchtDetail(int itemId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllTouchscreenARDispatches(IList<int?> orderStatuses, int? routeId = null);

        /// <summary>
        /// Completes the input dispatch order.
        /// </summary>
        /// <param name="saleOrderId">The sale order to canel the associated dispatch for.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        bool CancelARDispatchFromSaleOrder(int saleOrderId);

        /// <summary>
        /// Completes the input dispatch order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="orderStatusId">The order status id.</param>
        /// <returns>A flag, as to whether the order status was changed or not.</returns>
        bool ChangeARDispatchStatus(Sale sale, int orderStatusId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllARDispatches(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the dispatches (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetARDispatches(int customerId, int ordersToTake);

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of dispatches (and associated dispatch details) for the input search term.</returns>
        IList<Sale> GetARDispatchesByName(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves all the consolidated invoice dispatches.
        /// </summary>
        /// <returns>A collection of consolidated invoice dispatches (and associated order details).</returns>
        IList<Sale> GetARDispatchesByInvoice(int invoiceId);

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        Sale GetARDispatchById(int arDispatchId);

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetARDispatchesByCode(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllARDispatchesByDate(bool showAll, DateTime start, DateTime end, string dateType);

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        Sale GetARDispatchesById(int arDispatchId);

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        bool PriceDispatchDocket(int docketId);

        /// <summary>
        /// Updates an existing dispatch docket (docket and details) when a sale order has beem ammended.
        /// </summary>
        /// <param name="sale">The sale order data.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateARDispatchFromSaleOrder(Sale sale);

        /// <summary>
        /// Gets the dispatch total.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id.</param>
        /// <returns>A flag, indicating a successful retrieval or not.</returns>
        decimal GetDispatchTotal(int arDispatchId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="id">The order id.</param>
        /// <returns>The orders (and associated order details).</returns>
        Sale GetTouchscreenARDispatch(int id);

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        Sale GetARDispatchesByIdWithBoxCount(int arDispatchId);

        /// <summary>
        /// Updates dispatch containers.
        /// </summary>
        /// <param name="containerId">The container to search the dispatches for.</param>
        /// <returns>A date the container is due to go out on.</returns>
        DateTime? GetDispatchContainerDeliveryDate(int containerId);

        /// <summary>
        /// Adds or removes pallet stock from the order.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        /// <param name="arDispatchId">The order id.</param>
        /// <param name="addToOrder">Add or remove flag.</param>
        /// <returns>Flag, as to success or not.</returns>
        bool AddOrRemovePalletStock(int? serial, int? arDispatchId, bool addToOrder);

        /// <summary>
        /// Gets the ardispatch docket id relating to the input sale order id.
        /// </summary>
        /// <param name="arOrderId">The input sale order id.</param>
        /// <returns>The ardispatch docket id relating to the input sale order id.</returns>
        int GetARDispatchIDForSaleOrder(int arOrderId);

        /// <summary>
        /// Retrieves all the non invoiced dispatch data.
        /// </summary>
        /// <returns>A collection of non invoiced dispatch data (and associated order details).</returns>
        IList<Sale> GetNonInvoicedARDispatches(int customerId);

        /// <summary>
        /// Adds a dispatch container.
        /// </summary>
        /// <param name="container">The container to add.</param>
        /// <returns>A flag, indicating a successful addition or not.</returns>
        bool AddDispatchContainer(Model.DataLayer.DispatchContainer container);

        /// <summary>
        /// Removes stock from item.
        /// </summary>
        /// <param name="sale">The sale/detail/transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        bool RemoveStockFromOrder(Sale sale);

        /// <summary>
        /// Updates an order detail line.
        /// </summary>
        /// <param name="detail">The order detail line.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateDetailTotal(CompleteOrderCheck detail);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        IList<Sale> GetQuotesByPartner(int partnerId, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the quotes (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetOrdersByPartner(int partnerId, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        IList<App_GetDispatchDetails_Result> GetDispatchStatusDetails(int arDispatchId);

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of dispatches (and associated dispatch details) for the input search term.</returns>
        IList<Sale> GetARDispatchesByPartner(int partnerId, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the dispatch carcass data.
        /// </summary>
        /// <returns>The dispatch carcass data.</returns>
        IList<Sale> GetDispatchStock(IList<Sale> dispatches);

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        Sale GetARDispatchByFirstLast(bool first);

        /// <summary>
        /// Checks the completed dispatch docket.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        bool CheckDispatchDocket(int docketId);

        /// <summary>
        /// Retrieves the dispatches (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetOpenARDispatches(int customerId);

        /// <summary>
        /// Updates an order delivery date.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="delDate">The new delivery date.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        bool UpdateDeliveryDate(int orderId, DateTime delDate);

        /// <summary>
        /// Gets the transactions and attribute data associated with a dispatch line.
        /// </summary>
        /// <param name="id">The dispatch detail id.</param>
        /// <returns>The transactions and attribute data associated with a dispatch line.</returns>
        IList<App_GetOrderLineTransactionData_Result> GetOrderLineTransactionData(int id, int transactionTypeId, int docketid);

        /// <summary>
        /// Gets the dockets the input product is on.
        /// </summary>
        /// <param name="productId">The input product.</param>
        /// <returns>A list of dockets the product is on.</returns>
        IList<int> GetAllProductDockets(int productId);

        /// <summary>
        /// Updates dispatch containers.
        /// </summary>
        /// <param name="containers">The containers to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateDispatchContainer(IList<Model.BusinessObject.DispatchContainer> containers);

        /// <summary>
        /// Gets the dispatch total.
        /// </summary>
        /// <param name="sale">The dispatch data.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateNotes(Sale sale);

        /// <summary>
        /// Gets the dispatch docket details for a final check before completion.
        /// </summary>
        /// <param name="ardispatchId">The docket id.</param>
        IList<CompleteOrderCheck> CompleteOrderCheck(int ardispatchId);

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        Sale GetARDispatchByLastEdit();

        /// <summary>
        /// Gets pallet dispatch data.
        /// </summary>
        /// <param name="id">The dispatch id.</param>
        /// <returns>Pallet data.</returns>
        DataTable CheckForExtraDispatchLines(int id);

        /// <summary>
        /// Retrieves all the non invoiced dispatch data.
        /// </summary>
        /// <returns>A collection of non invoiced dispatch data (and associated order details).</returns>
        IList<Sale> GetNonInvoicedARDispatches(bool pricingByInvoice = false);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        Tuple<int,DocNumber> AddNewInvoice(Sale sale);

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateInvoice(Sale sale);

        /// <summary>
        /// Removes an invoice detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveArInvoiceDetail(int itemId);

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        Sale GetInvoiceByLastEdit();

        /// <summary>
        /// Copies a dispatch docket to an invoice.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>A newly created invoice number.</returns>
        int CopyDispatchToInvoice(int dispatchId, int userId, int deviceId);

        /// <summary>
        /// Prices an invoice docket, as opposed to creating it from a priced dispatch docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id used to price it.</param>
        /// <param name="invoiceId">The invoice id if updating.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Invoice number if successful, otherwise 0.</returns>
        int PriceInvoice(int dispatchId, int invoiceId, int userId, int deviceId);

        /// <summary>
        /// Gets pallet data.
        /// </summary>
        /// <param name="id">The pallet id.</param>
        /// <returns>Pallet data.</returns>
        IList<App_GetPalletisationStock_Result> GetPalletisationData(int id);

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        IList<Sale> GetAllInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        IList<Sale> GetAllInvoices(IList<int?> invoiceStatuses);

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input customer.</returns>
        IList<Sale> GetInvoices(int customerId);

        /// <summary>
        /// Retrieves the non exported invoices (and associated invoice details).
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        IList<Sale> GetNonExportedInvoices();

        /// <summary>
        /// Removes an invoice detail.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        bool ExportInvoices(IList<int> invoiceIds);

        /// <summary>
        /// Retrieves the non exported invoices (and associated invoice details).
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        void SetInvoiceBaseDocuments(IList<Sale> invoices);
      
        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input id.</returns>
        IList<Sale> GetInvoicesByPartner(int partnerId, IList<int?> invoiceStatuses);

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input search term.</returns>
        IList<Sale> GetInvoicesByName(string searchTerm, IList<int?> invoiceStatuses);

        /// <summary>
        /// Creates a back over from outstanding dispatch amounts.
        /// </summary>
        /// <param name="ardispatchId">The docket to create the back order for.</param>
        /// <returns></returns>
        int CreateBackOrder(int ardispatchId);

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="invoiceStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetInvoicesByCode(string searchTerm, IList<int?> invoiceStatuses);

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        IList<Sale> GetAllInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end,
            string dateType);

        /// <summary>
        /// Retrieves the invoice (and associated invoice details) for the input invoice id.
        /// </summary>
        /// <param name="invoiceId">The invoice id.</param>
        /// <returns>An invoice (and associated invoice details) for the input id.</returns>
        Sale GetInvoicesById(int invoiceId);

        /// <summary>
        /// Marks invoices as printed.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to mark.</param>
        /// <returns>Flag, as to whether the invoices were marked as printed, or not.</returns>
        bool MarkInvoicesPrinted(IList<int> invoiceIds);

        /// <summary>
        /// Marks invoices as printed.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to mark.</param>
        /// <returns>Flag, as to whether the invoices were marked as printed, or not.</returns>
        bool CompleteDispatchContainer(IList<int> invoiceIds, int containerId, bool complete);

        /// <summary>
        /// Gets the sale order document trail.
        /// </summary>
        /// <param name="documentNo">The sale order document id.</param>
        /// <param name="mode">The mode.</param>
        /// <returns>A collection of master-detail sale orders.</returns>
        IList<Sale> GetDocumentTrail(int documentNo, ViewType mode);

        /// <summary>
        /// Adds any delivery charges to the order.
        /// </summary>
        /// <param name="dispatchId">The order id.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool AddDeliveryCharges(int dispatchId);

        /// <summary>
        /// Gets the docket status.
        /// </summary>
        /// <param name="arDispatchId">The docket id.</param>
        /// <returns>The input docket status.</returns>
        int? GetARDispatchOrderStatus(int arDispatchId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllARDispatchesForReport(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves all the pallets between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        /// <returns>A collection of pallets.</returns>
        IList<Sale> GetAllPallets(DateTime start, DateTime end);

        /// <summary>
        /// Retrieves the dispatch containers.
        /// </summary>
        /// <param name="docStatusId">The doc status ofr the containers to retrieve.</param>
        /// <returns>A collection of dispatch containers.</returns>
        IList<DispatchContainer> GetDispatchContainers(int docStatusId);

        /// <summary>
        /// Gets an order transactions.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>The order transactions.</returns>
        IList<StockDetail> GetTransactions(int orderId);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="qtyPerBox">The qty per box.</param>
        /// <returns>The box transaction id.</returns>
        int AddBoxTransaction(StockTransaction transaction, int qtyPerBox);

        /// <summary>
        /// Gets the application call frequencies.
        /// </summary>
        /// <returns>The application call frequencies.</returns>
        IList<NouCallFrequency> GetCallFrequencies();

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        Telesale GetTelesale(int partnerId, int statusId);

        /// <summary>
        /// Gets the telesales relating to the input agents.
        /// </summary>
        /// <returns>A telesales collection.</returns>
        IList<Telesale> GetTelesales(IList<int?> agents, DateTime date);

        /// <summary>
        /// Adds or updates a telesale.
        /// </summary>
        /// <param name="telesale">The telesale to add or update.</param>
        /// <returns>Flag, as to a successful add/update.</returns>
        bool AddOrUpdateTelesale(Telesale telesale);

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        Telesale GetTelesaleByFirstLast(bool first);

        /// <summary>
        /// Gets the telesales for searching.
        /// </summary>
        /// <returns>A telesales collection.</returns>
        IList<Sale> GetSearchTelesales();

        /// <summary>
        /// Adds scanned stock to a dispatch.
        /// </summary>
        /// <param name="serial">The barcode.</param>
        /// <param name="ardispatchId">The docket Id.</param>
        /// <param name="allowDispatchProductNotOnOrder"><Can the product be added if not on order./param>
        /// <param name="allowDispatchOverAmount">Can over the line amount be dispatched.</param>
        /// <param name="allowStockAlreadyOnAnOrder">Can the stock be added to the order if on another order.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error\Message (empty if ok).</returns>
        Tuple<int?, string, DataTable, int?, int?> AddStockToDispatch(
            int serial,
            int ardispatchId,
            bool allowDispatchProductNotOnOrder,
            bool allowDispatchOverAmount,
            bool allowStockAlreadyOnAnOrder,
            int userId,
            int deviceId,
            int warehouseId,
            int processId,
            int customerId);

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        Telesale GetTelesaleByLastEdit();

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        Telesale GetTelesaleById(int id);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="qtyPerBox">The qty per box.</param>
        /// <returns>The box transaction id.</returns>
        IList<int> AddBoxTransactionOnCompleteOrder(StockTransaction transaction, int qtyPerBox);

        /// <summary>
        /// Checks the product lines for unfilled box transactions.
        /// </summary>
        /// <param name="details">The product lines to check.</param>
        /// <returns>A collection of unfilled product line boxes.</returns>
        IList<SaleDetail> CheckBoxTransactions(IList<SaleDetail> details);

        /// <summary>
        /// Gets the dispatch box piece labels.
        /// </summary>
        /// <param name="serial">The serial number to search for.</param>
        /// <returns>The dispatch box piece labels correesponding to the dispatched box.</returns>
        IList<GoodsIn> GetPieceLabels(int serial);

        /// <summary>
        /// Gets the kill carcasses by kill date.
        /// </summary>
        /// <param name="start">The start kill date to serach from.</param>
        /// <param name="end">The end kill date to search to.</param>
        /// <returns>A collection of carcass records.</returns>
        IList<Sale> GetCarcasses(DateTime start, DateTime end);

        /// <summary> Adds a new stock return (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        Tuple<int, int> AddARReturn(Sale sale);

        /// <summary>
        /// Updates an existing return (receipt and details).
        /// </summary>
        /// <param name="sale">The return receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateARReturn(Sale sale);

        /// <summary>
        /// Retrieves the returns (and associated details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        IList<App_GetReturnDetails_Result> GetReturnsStatusDetails(int id);

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetARReturns(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetARReturnsNonInvoiced();

        /// <summary>
        /// Gets the return details.
        /// </summary>
        /// <param name="id">The returns id to search for.</param>
        /// <returns>A return, and it's details.</returns>
        Sale GetARReturnById(int id);

        /// <summary>
        /// Gets the return details by last edit date.
        /// </summary>
        /// <returns>A return, and it's details.</returns>
        Sale GetARReturnByLastEdit();

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        bool UpdateARReturnStatus(Sale sale);

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        bool PriceReturnsDocket(int docketId);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        Tuple<int, DocNumber> AddARReturnInvoice(Sale sale);

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateARReturnInvoice(Sale sale);

        /// <summary>
        /// Retrieves the ap return invoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        IList<Sale> GetARReturnInvoices();

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        Sale GetARReturnInvoiceByID(int id);

        /// <summary>
        /// Exports credit note details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        bool ExportARReturnInvoice(IList<int> invoiceIds);
    }
}

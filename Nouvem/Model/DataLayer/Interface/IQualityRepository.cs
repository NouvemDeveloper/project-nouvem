﻿// -----------------------------------------------------------------------
// <copyright file="IQualityRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface IQualityRepository
    {
        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        IList<ViewQualityMaster> GetQualityMasters();

        /// <summary>
        /// Retrieve all the system date types.
        /// </summary>
        /// <returns>A collection of date types.</returns>
        IList<NouQualityType> GetQualityTypes();

        /// <summary>
        /// Adds or updates date masters.
        /// </summary>
        /// <param name="dateMasters">The date masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateQualityMasters(IList<ViewQualityMaster> dateMasters);

        /// <summary>
        /// Retrieve all the system date template names.
        /// </summary>
        /// <returns>A collection of date template names.</returns>
        IList<QualityTemplateName> GetQualityTemplateNames();

        /// <summary>
        /// Adds or updates date names.
        /// </summary>
        /// <param name="dateNames">The date names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateQualityTemplateNames(IList<QualityTemplateName> dateNames);

        /// <summary>
        /// Retrieve all the system date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        IList<QualityTemplateAllocation> GetQualityTemplateAllocations();

        /// <summary>
        /// Adds a date master to a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddQualityToTemplate(int dateMasterId, int templateId);

        /// <summary>
        /// Removes a date master from a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        bool RemoveQualityFromTemplate(int dateMasterId, int templateId);

        /// <summary>
        /// Retrieve all the date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        IList<ViewQualityTemplateAllocation> GetViewQualityTemplateAllocations();

        /// <summary>
        /// Adds a quality master to a template.
        /// </summary>
        /// <param name="qualityMasters">The collection of the quality masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddQualitysToTemplate(IList<ViewQualityMaster> qualityMasters, int templateId);
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IContainerRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IContainerRepository
    {
        /// <summary>
        /// Retrieve all the containers.
        /// </summary>
        /// <returns>A collection of containers.</returns>
        IList<Container> GetContainers();

        /// <summary>
        /// Retrieve all the containers types.
        /// </summary>
        /// <returns>A collection of container types.</returns>
        IList<ContainerType> GetContainerTypes();

        /// <summary>
        /// Add or updates the containers list.
        /// </summary>
        /// <param name="containers">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateContainers(IList<Container> containers);

        /// <summary>
        /// Retrieve all the product containers.
        /// </summary>
        /// <returns>A collection of containers.</returns>
        IList<TareCalculator> GetProductContainers();
    }
}

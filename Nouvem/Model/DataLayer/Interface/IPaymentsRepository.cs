﻿// -----------------------------------------------------------------------
// <copyright file="IPaymentsRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.BusinessObject;

    public interface IPaymentsRepository
    {
        /// <summary>
        /// Gets the application payment apply methods.
        /// </summary>
        /// <returns>The application payment apply methods.</returns>
        IList<NouApplyMethod> GetApplyMethods();

        /// <summary>
        /// Update the payements pricing matrix.
        /// </summary>
        /// <param name="pricing">The matrix prices.</param>
        bool UpdatePricingMatrix(StockDetail pricing);

        /// <summary>
        /// Returns the pricing.
        /// </summary>
        /// <returns></returns>
        IList<PricingMatrix> GetPricingMatrix();

        /// <summary>
        /// Marks a payment as printed..
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        bool MarkPaymentAsPrinted(int paymentId);

        /// <summary>
        /// Checks if a payment as printed..
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        bool IsPaymentPrinted(int paymentId);

        /// <summary>
        /// Gets a payments base intake ids.
        /// </summary>
        /// <param name="paymentid">The payment ids.</param>
        /// <returns>The base intake ids.</returns>
        IList<int> GetPaymentConsolidatedIntakes(int paymentid);

        /// <summary>
        /// Gets the payment deductions.
        /// </summary>
        /// <returns>The application payment deductions.</returns>
        IList<PaymentDeduction> GetPaymentDeductions();

        /// <summary>
        /// Updates the input payments.
        /// </summary>
        /// <param name="payments">The payments to update.</param>
        /// <param name="status">The status to update the payments to.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdatePaymentsStatus(IList<Sale> payments, int status);

        /// <summary>
        /// Updates the input payments.
        /// </summary>
        /// <param name="payments">The payments to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdatePaymentsOnly(IList<Sale> payments);

        /// <summary>
        /// Updates the grades and categories of the input carcasses.
        /// </summary>
        /// <param name="details">The carcasses to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateCarcasses(IList<StockDetail> details);

        /// <summary>
        /// Gets the application weight bands.
        /// </summary>
        /// <returns>The application bands.</returns>
        IList<WeightBandData> GetWeightBands();

        /// <summary>
        /// Adds or updates weight band groups.
        /// </summary>
        /// <param name="bands">The bands to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateWeightBandGroups(IList<WeightBandGroup> bands);

        /// <summary>
        /// Gets the application weight band groups.
        /// </summary>
        /// <returns>The application band groups.</returns>
        IList<WeightBandGroup> GetWeightBandGroups();

        /// <summary>
        /// Adds or updates weight bands.
        /// </summary>
        /// <param name="bands">The bands to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateWeightBands(IList<WeightBandData> bands);

        /// <summary>
        /// Marks the input payments as to be excluded from the pruchase accounts.
        /// </summary>
        /// <param name="payments">The input payments.</param>
        /// <returns>Flag, as to successful marking or not.</returns>
        bool ExcludePaymentsFromAccounts(IList<Sale> payments);

        /// <summary>
        /// Removes a carcass from a payment.
        /// </summary>
        /// <param name="stocktransactionId">The carcass transaction id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        bool RemoveCarcassFromPayment(int stocktransactionId);

        /// <summary>
        /// Removes a carcass from a payment.
        /// </summary>
        /// <param name="itemId">The carcass transaction id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        bool RemovePaymentDeductionItemFromPayment(int itemId);

        /// <summary>
        /// Updates a payment.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>Flag, as to successfull update or not.</returns>
        bool UpdatePayment(Sale payment);

        /// <summary>
        /// Adds or updates payment deductions.
        /// </summary>
        /// <returns>Flag, indicating successfull add or update.</returns>
        bool AddOrUpdatePaymentDeductions(IList<PaymentDeduction> deductions);

        /// <summary>
        /// Adds a new payment proposal (order and details).
        /// </summary>
        /// <param name="sale">The proposal details.</param>
        /// <returns>The newly created quote id, indicating a successful payment proposal add, or not.</returns>
        Tuple<int, DocNumber> AddNewPaymentProposal(Sale sale);

        /// <summary>
        /// Retrieves all the patment proposals (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetPaymentProposals(bool showAll, DateTime start, DateTime end);

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the input id search term.
        /// </summary>
        /// <param name="paymentId">The order search id.</param>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        Sale GetPaymentById(int paymentId);

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the last edit.
        /// </summary>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        Sale GetPaymentByFirstLast(bool first);

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the last edit.
        /// </summary>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        Sale GetPaymentByLastEdit();

        /// <summary>
        /// Updates a payment, adding a cheque number.
        /// </summary>
        /// <param name="paymentId">The cheque number.</param>
        /// <returns>A payment cheque number.</returns>
        int UpdatePaymentChequeNumber(int paymentId);

        /// <summary>
        /// Exports purchase invoice details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        bool ExportPurchaseInvoices(IList<int> invoiceIds, bool useLairages);

        /// <summary>
        /// Cancels a payment.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>Flag, as to successfull cancellation or not.</returns>
        bool CancelPayment(Sale payment);
    }
}


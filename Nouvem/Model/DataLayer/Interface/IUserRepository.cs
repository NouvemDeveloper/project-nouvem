﻿// -----------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IUserRepository
    {
        /// <summary>
        /// Method that adds a new user.
        /// </summary>
        /// <param name="user">The user to add.</param>
        /// <returns>The bpmaster id, indicating a successful addition or not.</returns>
        int AddUser(User user);

        /// <summary>
        /// Method that updates an existing user.
        /// </summary>
        /// <param name="user">The user to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateUser(User user);

        /// <summary>
        /// Copys the user group settings.
        /// </summary>
        /// <param name="from">Group to copy from.</param>
        /// <param name="to">Group to Copy to.</param>
        /// <returns>Flag, as to successful copy of not.</returns>
        bool CopyUserGroupSettings(int from, int to);

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        User GetUser(string name);

        /// <summary>
        /// Gets the db pasword.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        string GetDBPassword(int deviceId);

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        IList<UserGroup> GetUserGroupsAndModules(IList<ReportData> reports);

        /// <summary>
        /// Stores the remember user password flag.
        /// </summary>
        /// <param name="user">The user in question.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool StoreRememberPassword(User user);

        /// <summary>
        /// Retrieves the user password.
        /// </summary>
        /// <param name="user">The user in question.</param>
        /// <returns>Flag, as to successful retrieval or not.</returns>
        byte[] GetUserPassword(string user);

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        IList<UserGroup_> GetUserGroups();

        /// <summary>
        /// Saves the logged in user shortcuts.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Flag, as to successful save or not.</returns>
        bool SaveUserShortcuts(User user);

        /// <summary>
        /// Updates the users alerts look ups.
        /// </summary>
        /// <param name="users">The selected users.</param>
        /// <param name="userMasterId">The user id.</param>
        /// <returns>Flag, as to successfl update or not.</returns>
        bool UpdateUserAlertsSetUp(IList<UserAlertsLookUp> users, int userMasterId);

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        IList<UserAlertsLookUp> GetUserLookUps(int userMasterID);

        /// <summary>
        /// Method that tests the db cnnection.
        /// </summary>
        /// <returns>A flag, indicating whether a connection can be made to the database.</returns>
        bool TestDatabase();

        /// <summary>
        /// Method that verifies a users login credentials.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The user password.</param>
        /// <returns>The verified user master data object, or null if not verified.</returns>
        UserMaster VerifyLoginUser(string userId, string password);

        /// <summary>
        /// Register a user logon.
        /// </summary>
        /// <param name="logon">The logon data.</param>
        /// <param name="userName">The logon user name.</param>
        void RegisterLogon(UserLoggedOn logon, string userName);

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        IList<User> GetUsers();

        /// <summary>
        /// Retrieve all the database names from the server.
        /// </summary>
        /// <returns>A collection of database names.</returns>
        IList<string> GetDatabaseNames();

        /// <summary>
        /// Retrieve all the servers.
        /// </summary>
        /// <returns>A collection of server details.</returns>
        IList<Server> GetServers();

        /// <summary>
        /// Method that adds a new user group.
        /// </summary>
        /// <param name="userGroup">The user group to add.</param>
        /// <returns>A flag, indicating a successful addition or not.</returns>
        int AddUserGroup(UserGroup_ userGroup);

        /// <summary>
        /// Method that updates an existing user group.
        /// </summary>
        /// <param name="userGroups">The user group to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateUserGroups(IList<UserGroup> userGroups);

        /// <summary>
        /// Retrieve all the user group rules.
        /// </summary>
        /// <returns>A collection of user group rules.</returns>
        IList<ViewGroupRule> GetUserGroupRules();

        /// <summary>
        /// Retrieve all the user group values.
        /// </summary>
        /// <returns>A collection of user group values.</returns>
        IList<NouAuthorisationValue> GetAuthorisationValues();

        /// <summary>
        /// Retrieve all the user group lists.
        /// </summary>
        /// <returns>A collection of user group lists.</returns>
        IList<SystemAuthorisation> GetAuthorisations();

        /// <summary>
        /// Updates a user group rule.
        /// </summary>
        /// <param name="rules">The user group to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateAuthorisationRules(IList<ViewGroupRule> rules);

        /// <summary>
        /// Adds new user group rules.
        /// </summary>
        /// <param name="rules">The user group to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        bool AddAuthorisationRules(IList<ViewGroupRule> rules);

        /// <summary>
        /// Register a user log out for the current user.
        /// </summary>
        void RegisterLogout();

        /// <summary>
        /// Check the logged in status of a user.
        /// </summary>
        /// <param name="user">The user logging in to check.</param>
        /// <returns>A flag, indicating whether the user is already logged in.</returns>
        bool CheckLoggedInStatus(User user);

        /// <summary>
        /// Checks whether the current user has been marked to be forced logged off.
        /// </summary>
        bool CheckForForcedLogOff();

        /// <summary>
        /// Forces the logging out of another user logged in with the current licensees credentials.
        /// </summary>
        void ForceLogOff();

        /// <summary>
        /// Removes any user log in records over a week old.
        /// </summary>
        void RemoveOldLogins();

        /// <summary>
        /// Method that updates a user pasword.
        /// </summary>
        /// <param name="userId">The user name.</param>
        /// <param name="password">The updated password.</param>
        /// <returns>A flag, as to wherther a password was updated or not.</returns>
        bool UpdatePassword(string userId, byte[] password);

        /// <summary>
        /// Stores a users windows settings (positioning and grid data, if any)
        /// </summary>
        /// <param name="windows">The application windows.</param>
        void SaveWindowSettings(IList<WindowSetting> windows);

        /// <summary>
        /// Gets the logged in users window and search grid settings.
        /// </summary>
        /// <param name="userId">The logged in user id.</param>
        /// <returns>A collection of user associated window and data grid settings.</returns>
        IList<WindowSetting> GetWindowSettings(int userId);

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        IList<UserMaster> GetDbUsers();
    }
}

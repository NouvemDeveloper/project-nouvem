﻿// -----------------------------------------------------------------------
// <copyright file="ItemMasterRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IItemMasterRepository
    {
        /// <summary>
        /// Method that adds a new inventory item.
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <returns>The newly added item id.</returns>
        int AddInventoryItem(InventoryItem item);

        /// <summary>
        /// Retrieves the input product stock data.
        /// </summary>
        /// <param name="productId">The product to retrieve the stock data for.</param>
        /// <returns>A collection of stock data.</returns>
        IList<App_GetMRPData_Result> GetMRPData(int productId);

        /// <summary>
        /// Method that updates an existing inventory item.
        /// </summary>
        /// <param name="item">The item to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateInventoryItem(InventoryItem item);

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        IList<INAttachment> GetInventoryAttachments(int inmasterId);

        /// <summary>
        /// Gets the last edited spc.
        /// </summary>
        /// <returns>The last edited spec.</returns>
        ProductionSpecification GetsProductSpecificationById(int id);

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        byte[] GetImage(int id);

        /// <summary>
        /// Gets the last edited spc.
        /// </summary>
        /// <returns>The last edited spec.</returns>
        ProductionSpecification GetsProductSpecificationLastEdit();

        /// <summary>
        /// Gets a collection of specs linked to a product.
        /// </summary>
        /// <returns>A collection of specs linked to a product.</returns>
        IList<Sale> GetsSearchProductSpecifications();

        /// <summary>
        /// Gets the product/partner spec.
        /// </summary>
        /// <returns>Gets the product/partner spec.</returns>
        ProductionSpecification GetsProductSpecificationByPartner(int inmasterid, int bpmasterid);

        /// <summary>
        /// Gets the last edited group.
        /// </summary>
        /// <returns>The last edited group.</returns>
        INGroup GetInventoryGroupLastEdit();

        /// <summary>
        /// Gets a collection of specs linked to a product.
        /// </summary>
        /// <param name="inmasterid">The product id.</param>
        /// <returns>A collection of specs linked to a product.</returns>
        IList<ProductionSpecification> GetsProductSpecifications(int inmasterid);

        /// <summary>
        /// Adds a new specification.
        /// </summary>
        /// <param name="spec">The spec to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool AddOrUpdateProductSpecification(ProductionSpecification spec);

        /// <summary>
        /// Adds or updates departments..
        /// </summary>
        /// <returns>Flag, as to successful update or not.</returns>
        bool AddOrUpdateDepartments(IList<DepartmentLookup> departments);

        /// <summary>
        /// Method which returns the non deleted departmentlook ups.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        IList<DepartmentLookUp> GetDepartmentLookUps(int deviceID);

        /// <summary>
        /// Swaps 2 products groups sort index.
        /// </summary>
        /// <param name="groupA">Group A.</param>
        /// <param name="groupB">Group B.</param>
        /// <returns>Flag, as to successful swap or not.</returns>
        bool SwapGroupSortIndexes(INGroup groupA, INGroup groupB);

        /// <summary>
        /// Swaps 2 products sort index.
        /// </summary>
        /// <param name="productA">Product A.</param>
        /// <param name="productB">Product B.</param>
        /// <returns>Flag, as to successful swap or not.</returns>
        bool SwapSortIndexes(InventoryItem productA, InventoryItem productB);

        /// <summary>
        /// Gets the next product code in the db.
        /// </summary>
        /// <returns>The next code.</returns>
        int? GetNextProductCode();

        /// <summary>
        /// Method which updates the price m,ethods.
        /// </summary>
        /// <returns>Flag, as to successful update or not..</returns>
        bool UpdateSearchItems(IList<InventoryItem> products);

        /// <summary>
        /// Method which returns the inventory item associated with the id.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        InventoryItem GetInventoryItemById(int id);

        /// <summary>
        /// Method which returns the inventory items.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        IList<InventoryItem> GetInventoryItems();

        /// <summary>
        /// Method which returns the product linked to the input linked tare..
        /// </summary>
        /// <returns>A linked product id.</returns>
        INMaster GetLinkedProductTareProduct(int tareId);

        /// <summary>
        /// Method that retrieves the last 100 inventorys.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        IList<InventoryItem> GetRecentInventoryItems();

        /// <summary>
        /// Method which returns the non deleted inventory groups.
        /// </summary>
        /// <returns>A collection of non deleted inventory groups.</returns>
        IList<INGroup> GetInventoryGroups();

        /// <summary>
        /// Method which returns the non deleted item master properties.
        /// </summary>
        /// <returns>A collection of non deleted item master properties.</returns>
        IList<INProperty> GetItemMasterProperties();

        /// <summary>
        /// Add or updates the properties list.
        /// </summary>
        /// <param name="properties">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateProperties(IList<INProperty> properties);

        /// <summary>
        /// Add or updates the groups list.
        /// </summary>
        /// <param name="groups">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateInventoryGroups(IList<INGroup> groups);

        /// <summary>
        /// Add or updates an inventory group.
        /// </summary>
        /// <param name="group">The group to add or update.</param>
        /// <returns>An update inventory group, or null if an issue.</returns>
        INGroup AddOrUpdateInventoryGroup(INGroup group);

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="attachment">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        bool DeleteAttachment(INAttachment attachment);

        /// <summary>
        /// Method which returns the non deleted vat codes.
        /// </summary>
        /// <returns>A collection of non deleted vat codes.</returns>
        IList<VATCode> GetVATCodes();

        /// <summary>
        /// Add or updates the vat codes list.
        /// </summary>
        /// <param name="vatCodes">The vat codes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateVatCodes(IList<VATCode> vatCodes);

        /// <summary>
        /// Method which returns the non deleted departments.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        IList<Department> GetDepartments();

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        IList<INAttachment> GetInventoryAttachments();

        /// <summary>
        /// Method which returns the inventory property selections.
        /// </summary>
        /// <returns>A collection of inventory properties.</returns>
        IList<INPropertySelection> GetINPropertySelections();

        /// <summary>
        /// Method which returns the non deleted inventory properties.
        /// </summary>
        /// <returns>A collection of non deleted inventory properties.</returns>
        IList<INProperty> GetINProperties();

        /// <summary>
        /// Method which returns the non deleted carcass split products.
        /// </summary>
        /// <returns>A collection of non deleted carcass split products.</returns>
        IList<CarcassSplitOption> GetCarcassSplitProducts();

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        GetLinkedProduct_Result2 GetLinkedProduct(string serial);

        /// <summary>
        /// Method which returns the inventory items.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        void GetItemsStock(IList<InventoryItem> inIitems);

        /// <summary>
        /// Method which returns the inventory item nproduct data.
        /// </summary>
        /// <returns>A collection of inventory epos items data.</returns>
        IList<ProductData> GetEposItems();

        /// <summary>
        /// Method which returns the transaction types.
        /// </summary>
        /// <returns>A collection of transaction types.</returns>
        IList<NouTransactionType> GetTransactionTypes();

        /// <summary>
        /// Method which returns the non deleted vat codes.
        /// </summary>
        /// <returns>A collection of non deleted vat codes.</returns>
        IList<Vat> GetVat();

        /// <summary>
        /// Add or updates the vat codes list.
        /// </summary>
        /// <param name="vatCodes">The vat codes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateVat(IList<Vat> vatCodes);

        /// <summary>
        /// Method which returns the stock modes.
        /// </summary>
        /// <returns>A collection of stock modes.</returns>
        IList<NouStockMode> GetStockModes();

        /// <summary>
        /// Determines if there are any transactions recorded against the input product.
        /// </summary>
        /// <returns>Flag, as to whether there are transactions recorded against the input product.</returns>
        bool AreTransactionsRecordedAgainstProduct(int productId);

        /// <summary>
        /// Retrieves the input product stock data.
        /// </summary>
        /// <param name="productId">The product to retrieve the stock data for.</param>
        /// <returns>A collection of stock data.</returns>
        IList<ProductStockData> GetProductStockData(int productId);

        /// <summary>
        /// Gets the product data between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        IList<App_ReportProductData_Result> GetProductData(int customerId, DateTime start, DateTime end);
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ILairageRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;

namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface ILairageRepository
    {
        /// <summary> Adds a new lairage order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag indicating a successful order add, or not.</returns>
        bool AddLairageOrder(Sale sale);

        /// <summary>
        /// Updates a lairage lot queue no.
        /// </summary>
        /// <param name="lots">The lots to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateLotQueue(IList<Sale> lots);

        /// <summary>
        /// Removes a lairage intake item.
        /// </summary>
        /// <param name="attributeId">The attribute id</param>
        /// <returns>Flag, indicating a successful removal or not.</returns>
        DateTime? GetAnimalSequenceDate(int attributeId);

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        IList<SupplierHerd> GetSupplierPartnerHerds(int partnerId);

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetNextCarcassWithEartagsByIntakeId(int id);

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="transId">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetCarcassByStockId(int transId);

        /// <summary>
        /// Retrieves the stock detail corresponsing to the entered eartag.
        /// </summary>
        /// <param name="eartag">The eartag to search for.</param>
        /// <returns>A stock detail.</returns>
        StockDetail VerifyEartag(string eartag);

        /// <summary>
        /// Edits a carcss.
        /// </summary>
        /// <param name="stocktransactionId">The stock id.</param>
        /// <param name="carcassNo">The animal carcass no.</param>
        /// <param name="side1Wgt">It's side1 wgt.</param>
        /// <param name="side2Wgt">It's side2 wgt.</param>
        /// <param name="grade">The animal grade.</param>
        /// <param name="gradingDate">The kill date.</param>
        /// <param name="categoryId">The category.</param>
        /// <param name="farmAssured">The qa status.</param>
        /// <param name="rpa">The rpa status.</param>
        /// <param name="generic2">Generic 2 value.</param>
        /// <param name="killType">The kill type (beef,sheep,pig).</param>
        /// <param name="deviceId">The device id.</param>
        /// <param name="userId">The user editing.</param>
        /// <returns>Flag, as to successful edit or not.</returns>
        bool EditCarcass(int deviceId, int userId, int? stocktransactionId = null, int? carcassNo = null,
            decimal? side1Wgt = null, decimal? side2Wgt = null, string grade = "", DateTime? gradingDate = null,
            int? categoryId = null, bool? farmAssured = null, bool? rpa = null, string generic2 = "",
            string killType = "");

        /// <summary> Adds a new lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        int MoveLairageIntake(Sale sale);

        /// <summary>
        /// Updaes the whole carcass weights.
        /// </summary>
        /// <param name="carcassNo">The carcass number of the animal to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateCarcassWeight(int carcassNo, string killType);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllKilledAnimalDetailsByPrice(DateTime start, DateTime end);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllKilledAnimalDetailsByPriceUnsent(DateTime start, DateTime end);

        /// <summary>
        /// Adds aims data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Flag, as to successfull add or not.</returns>
        bool AddAIMsData(AimsData data);

        /// <summary> Updates a lairage order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag indicating a successful order update, or not.</returns>
        bool UpdateLairageOrder(Sale sale);

        /// <summary>
        /// Gets the lot kill data.
        /// </summary>
        /// <param name="lotNo">The header record id.</param>
        /// <returns>Kill lot data (killdate, or null if not yet killed).</returns>
        IList<DateTime?> GetLotCount(int lotNo);

        /// <summary>
        /// Gets the lairage associated payment status.
        /// </summary>
        /// <param name="intakeId">The intake id.</param>
        /// <returns>The lairage associated payment status.</returns>
        int? GetLairagePaymentStatus(int intakeId);

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        int GetNextKillNumberSheep(bool isSequencer = true);

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        int GetNextKillNumberPig(bool isSequencer = true);

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        int UpdateKillNumberSheep();

        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        int UpdateKillNumberPig();

        /// <summary>
        /// Adds a new grader pig carcass.
        /// </summary>
        /// <param name="transId">The previous pig carcass to copy.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool AddNewPigCarcass(int transId);

        /// <summary>
        /// Adds a new grader sheep carcass.
        /// </summary>
        /// <param name="transId">The previous pig carcass to copy.</param>
        /// <returns>Flag, as to successful add or not.</returns>
         bool AddNewSheepCarcass(int masterTableId);

        /// <summary>
        /// Determines if an eartag is in the system.
        /// </summary>
        /// <param name="eartag">The eartag to check.</param>
        /// <returns>Flag, as to whether an eartag is in the system.</returns>
        bool IsSheepEartagInSystem(string eartag, int lotNo);

        /// <summary>
        /// Set the intake processed falg.
        /// </summary>
        /// <param name="intakeId">The intake id.</param>
        /// <param name="isProcessed">The processed flag.</param>
        /// <returns>Flag, st to successful update or not.</returns>
        bool SetIntakeProcessing(int intakeId, bool isProcessed);

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetCarcassForEdit(int carcassNo);

        /// <summary> Adds a new lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        Tuple<int, int> AddLairageIntake(Sale sale);

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        bool UpdateRPAAttribute(IList<StockDetail> details);

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        bool UpdateLairageIntakeSupplier(Sale sale, int id);

        /// <summary>
        /// Updates the qa status of the input batch.
        /// </summary>
        /// <param name="details">The input batch of animals.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateFarmAssured(IList<StockDetail> details);

        /// <summary>
        /// Updates the lairage animal pricing/rpa.
        /// </summary>
        /// <param name="details">The details to update.</param>
        /// <returns>A flag, as to whether the updates were succcessful.</returns>
        bool UpdateKillAttributes(IList<StockDetail> details);

        /// <summary>
        /// Determines if an eartag is in the system.
        /// </summary>
        /// <param name="eartag">The eartag to check.</param>
        /// <returns>Flag, as to whether an eartag is in the system.</returns>
        int IsEartagInSystem(string eartag);

        /// <summary>
        /// Logs the aims data.
        /// </summary>
        /// <param name="data">The data to log.</param>
        /// <returns>Flag, as to successful logging or not.</returns>
        bool LogAimsData(AimsData data);

        /// <summary>
        /// Updates the lairage animal pricing/rpa.
        /// </summary>
        /// <param name="details">The details to update.</param>
        /// <returns>A flag, as to whether the updates were succcessful.</returns>
        bool UpdateLairagePrices(IList<StockDetail> details);

        /// <summary>
        /// Marks the exported identigen records as being exported.
        /// </summary>
        /// <param name="details">The records to export.</param>
        /// <returns>Flag, as to success export or not.</returns>
        bool ExportCarcasses(IList<StockDetail> details, bool useExport1);

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        Tuple<bool, int> UpdateLairageIntake(Sale sale);

        /// <summary>
        /// Gets all the killed carcass side details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllCarcassSides(DateTime start, DateTime end, string searchType);

        /// <summary>
        /// Cghanges the record status.
        /// </summary>
        /// <param name="details">The records to export.</param>
        /// <returns>Flag, as to success export or not.</returns>
        bool ChangeCarcassStatus(IList<StockDetail> details, int status);

        /// <summary>
        /// Gets all the RPA carcasses to be sent.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllRPAAnimalDetails(DateTime start, DateTime end);

        /// <summary>
        /// Gets all the RPA carcasses to be sent.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetRPAAnimalDetails(DateTime start, DateTime end);
      
        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllKilledLairageAnimalDetails(DateTime start, DateTime end, string searchType);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllBeefKilledAnimalDetailsByStatus(DateTime start, DateTime end);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetIdentigenKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetBeefKilledAnimalDetailsByStatus(DateTime start, DateTime end);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllIdentigenKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status);

        /// <summary>
        /// Gets all the killed lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllKilledAnimalDetailsByStatus(DateTime start, DateTime end, int status);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        Sale GetLairageOrderByLastEdit();

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="transId">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetCarcassById(int transId);

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        IList<SupplierHerd> GetSupplierHerds(int supplierId);

        /// <summary>
        /// Gets the lairage animal details.
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetLairageAnimalDetails(DateTime start, DateTime end);

        /// <summary>
        /// Retrieves the supplier herds types.
        /// </summary>
        /// <returns>The supplier herds.</returns>
        IList<SupplierHerd> GetSupplierHerds();

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="partnerId">The partner id to search orders for.</param>
        /// <returns>A collection of all orders for the input partner.</returns>
        IList<Sale> GetLairageOrdersBySupplier(int partnerId);

        /// <summary>
        /// Gets all the lairage animal details (paid and unpaid).
        /// </summary>
        /// <param name="start">Search from date.</param>
        /// <param name="end">Search to date.</param>
        /// <returns></returns>
        IList<StockDetail> GetAllLairageAnimalDetails(DateTime start, DateTime end);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        Sale GetLairageOrderByID(int id);

        /// <summary>
        /// Removes a lairage intake item.
        /// </summary>
        /// <param name="attributeId">The attribute id</param>
        /// <returns>Flag, indicating a successful removal or not.</returns>
        string RemoveLairageIntakeItem(int attributeId);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        Sale GetLairageOrderByFirstLast(bool first);

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllLairageOrdersByDate(IList<int?> orderStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Retrieves the kill line kill types.
        /// </summary>
        /// <returns>The kill line kill types.</returns>
        IList<NouKillType> GetKillTypes();

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllLairageIntakes(IList<int?> orderStatuses, DateTime start, DateTime end, string searchMode);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input id search term.
        /// </summary>
        /// <param name="apReceiptId">The order serch id.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        Sale GetLairageIntakeById(int apReceiptId, bool includeMovements = true);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the device last edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        Sale GetLairageIntakeByLastEdit();

        /// <summary> Updates lairage intake (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag,indicating a successful order add, or not.</returns>
        bool UpdateLairageIntakeStatus(Sale sale);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the device last edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        Sale GetLairageIntakeByFirstLast(bool first);

        /// <summary>
        /// Retrieves the stock detail corresponsing to the entered eartag.
        /// </summary>
        /// <param name="eartag">The eartag to search for.</param>
        /// <returns>A stock detail.</returns>
        StockDetail GetLairageIntakeStockByEartag(string eartag, string killNo = "");

        /// <summary>
        /// Gets the next the kill number.
        /// </summary>
        /// <returns>The `next kill number.</returns>
        int GetNextKillNumber(bool isSequencer = true);
        
        /// <summary>
        /// Updates the kill number.
        /// </summary>
        /// <returns>The updated number.</returns>
        int UpdateKillNumber();

        /// <summary>
        /// Gets the age bands.
        /// </summary>
        /// <returns>The age bands.</returns>
        IList<AgeBand> GetAgeBands();

        /// <summary>
        /// Updates a stock attribute.
        /// </summary>
        /// <returns>A flag,indicating a successful update or not.</returns>
        bool UpdateStockAttribute(StockDetail detail);

        /// <summary>
        /// Adds a stock attribute.
        /// </summary>
        /// <param name="stock">The attrribute to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool AddStockAttribute(StocktransactionDetail stock);

        /// <summary>
        /// Retrieves the daily sequenced, ungraded carcasses.
        /// </summary>
        /// <returns>A collection of daily sequenced, ungraded carcasses..</returns>
        IList<StockDetail> GetCurrentSequencedStock();

        /// <summary>
        /// Re-order the carcasses at the sequencer.
        /// </summary>
        /// <param name="carcassToMoveUp">The carcass to move up.</param>
        /// <param name="carcassToMoveDown">The carcass to move down.</param>
        /// <param name="docResetNo">The carcass document reset number.</param>
        /// <returns>A flag to indicate that the re-ordering was successful.</returns>
        bool ReOrderCarcasses(StockDetail carcassToMoveUp, StockDetail carcassToMoveDown, int docResetNo);

        /// <summary>
        /// Gets the next carcass number at the grader.
        /// </summary>
        /// <param name="carcassResetNo">The reset value.</param>
        /// <returns>The next carcass number at the grader.</returns>
        int GetLastcarcassNumber(int carcassResetNo, string killType);

        /// <summary>
        /// Adds a grader carcass side weighing.
        /// </summary>
        /// <param name="detail">The carcass to add.</param>
        /// <returns>Flag, as to a successful add or not.</returns>
        bool EditGraderTransaction(StockDetail detail);

        /// <summary>
        /// Updates a pig record with the grading data.
        /// </summary>
        /// <param name="detail">The carcass to update.</param>
        /// <returns>Flag, as to a successful update or not.</returns>
        bool UpdateGraderPigTransaction(StockDetail detail);

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetCarcass(int carcassNo, string mode);

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetNextCarcassByIntakeId(int id);

        /// <summary>
        /// Adds a grader carcass side weighing.
        /// </summary>
        /// <param name="detail">The carcass to add.</param>
        /// <returns>Flag, as to a successful add or not.</returns>
        bool AddGraderTransaction(StockDetail detail);

        /// <summary>
        /// Gets an intake supplier id.
        /// </summary>
        /// <param name="id">The transaction id.</param>
        /// <returns>An intake supplier id.</returns>
        int? GetSupplierId(int id);

        /// <summary>
        /// Gets the carcass record corresponding to the input carcass number.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        IList<StockDetail> GetTodaysCarcasses(string killType);

        /// <summary>
        /// Undos a carcass sequenced, reverting the numbers back 1.
        /// </summary>
        /// <returns>A string, indicating an error or not.</returns>
        string UndoSequencedCarcass(StockDetail detail, int docNumberId);

        /// <summary>
        /// Changes the transaction weight.
        /// </summary>
        /// <param name="trans">The transaction to change.</param>
        /// <returns>Flag, as to successfull change or not.</returns>
        Tuple<bool,KillType,string,string> ChangeTransactionWeight(StockTransaction trans);

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetNextPigCarcassByIntakeId(int id);

        /// <summary>
        /// Gets the carcass record corresponding to the intake number.
        /// </summary>
        /// <param name="id">The carcass number to search for.</param>
        /// <returns>A carcass stock record.</returns>
        StockDetail GetNextSheepCarcassByIntakeId(int id);

        /// <summary>
        /// Gets the stock serial id associated with the input barcode and side.
        /// </summary>
        /// <param name="carcassNo">The carcass number to search for.</param>
        /// <param name="carcassSide">The carcass side to search for.</param>
        /// <returns>A stock serial id associated with the input barcode and side</returns>
        int GetCarcassTransaction(int carcassNo, int carcassSide);

        /// <summary>
        /// Updates a sheep record with the grading data.
        /// </summary>
        /// <param name="detail">The carcass to update.</param>
        /// <returns>Flag, as to a successful updateor not.</returns>
        bool UpdateGraderSheepTransaction(StockDetail detail);

        /// <summary>
        /// Removes the identigen bottle number.
        /// </summary>
        /// <param name="identigen">The number to serach for.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        bool MinusScanIdentigen(string identigen);

        /// <summary>
        /// Determines if the identigen number has already been assigned..
        /// </summary>
        /// <param name="identigen">The number to serach for.</param>
        /// <returns>Carcass number if already assigned.</returns>
        int HasIdentigenBeenAssigned(string identigen);
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IInvoiceRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IInvoiceRepository
    {
        /// <summary>
        /// Adds a new invoice header object to the db.
        /// </summary>
        /// <param name="header">The invoice header to add.</param>
        /// <param name="partner">The business partner (if any).</param>
        /// <returns>The newly created invoice header id.</returns>
        Tuple<int, int?> AddInvoiceHeader(InvoiceHeader header, BusinessPartner partner);

        /// <summary>
        /// Adds a collection of invoice details objects to the db.
        /// </summary>
        /// <param name="details">The invoice header to add.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful add or not.</returns>
        string AddInvoiceItems(IDictionary<InvoiceDetail, ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse);

        /// <summary>
        /// Adds a collection of goods received objects to the db.
        /// </summary>
        /// <param name="details">The intake data.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful add or not.</returns>
        bool ProcessGoodsReceived(IList<ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse);

        /// <summary>
        /// Gets the epos sales for the selected date.
        /// </summary>
        /// <param name="saleDate">The date to retrieve sales for.</param>
        /// <param name="invoiceNo">The invoice number to search for.</param>
        /// <returns>The sales corresponding to the input date.</returns>
        IList<EposSale> GetEposSales(DateTime? saleDate, int? invoiceNo);

        /// <summary>
        /// Cancels the input epos sale, putting the acorresponding stock back into the system.
        /// </summary>
        /// <param name="sale">The sale to cancel.</param>
        /// <param name="transactionType">The transaction type.</param>
        /// <returns>A flag, indicating a successful sale cancellation or not..</returns>
        bool CancelEposSale(EposSale sale, NouTransactionType transactionType);

        /// <summary>
        /// Gets the current stock items.
        /// </summary>
        /// <returns>A collection of stock items.</returns>
        IList<ProductData> GetEposStock();

        /// <summary>
        /// Gets the system delivery methods.
        /// </summary>
        /// <returns>A collection of delivery methods.</returns>
        IList<NouDeliveryMethod> GetDeliveryMethods();

        /// <summary>
        /// Gets the system payment types.
        /// </summary>
        /// <returns>A collection of payment types.</returns>
        IList<NouPaymentType> GetPaymentTypes();

        /// <summary>
        /// Gets the unpaid account epos sales for the selected date.
        /// </summary>
        /// <returns>The sales corresponding to the upaid accounts.</returns>
        IList<EposSale> GetUnpaidEposSales();

        /// <summary>
        /// Marks the unpaid account sales as paid.
        /// </summary>
        /// <param name="sales">The sales collection.</param>
        /// <param name="paymentTypeId">The account payment type id.</param>
        /// <returns>A flag, indicating a successful processing of account sales to me marked paid.</returns>
        bool HandleAccountSalesPaid(IList<EposSale> sales, int paymentTypeId);

        /// <summary>
        /// Removes stock from the db.
        /// </summary>
        /// <param name="details">The remove stock data.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful return or not.</returns>
        bool ProcessGoodsReturned(IList<ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse);
    }
}

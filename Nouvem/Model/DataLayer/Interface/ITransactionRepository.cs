﻿// -----------------------------------------------------------------------
// <copyright file="ITransactionRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;

    public interface ITransactionRepository
    {
        /// <summary>
        /// Adds dispatch transactions to a pallet.
        /// </summary>
        /// <param name="ids">The dispatch transactions.</param>
        /// <param name="palletId">The pallet id.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool UpdateDispatchPalletTransactions(HashSet<int> ids, int palletId);

        /// <summary>
        /// Checks if the dispatch line has transactions recorded against it.
        /// </summary>
        /// <param name="lineId">The product line.</param>
        /// <returns>The line id.</returns>
        bool DoesProductionProductLineContainTransactions(int orderId, int inmasterid);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockDetail GetStockTransactionBySerial(int serial, int nouTransactionTypeId);

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        StockTransaction RecreateTransactionWithWgt(StockTransaction transaction);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetTransactionByComments(string serialNo, int inmasterid);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransactionUncomsumed(int serial);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransactionFromAttribute(int attributeId);

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <param name="deviceId">The device id to search transactions.</param>
        /// <param name="addToStock">The add to stock flag.</param>
        /// <returns>A collection of recent transactions.</returns>
        IList<StockTransactionData> GetTransactions(int transactionsToTake, int deviceId, bool addToStock = true);

        /// <summary>
        /// Checks if the dispatch line has transactions recorded against it.
        /// </summary>
        /// <param name="lineId">The product line.</param>
        /// <returns>The line id.</returns>
        bool DoesProductLineContainTransactions(int lineId);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetTransactionDataForIntoProductionByComments(string serialNo, int inmasterid);

        /// <summary>
        /// Updates edited transactions.
        /// </summary>
        /// <param name="transactions">The transactions to edit.</param>
        /// <returns>Flag, as to successful update or not.</returns>
         bool UpdateTransactions(IList<StockDetail> transactions);

        /// <summary>
        /// Gets a set of transactions.
        /// </summary>
        /// <param name="intakeId">An associated intake/lairage id.</param>
        /// <param name="prOrderId">An associated production id.</param>
        /// <param name="dispatchId">An associated dispatch id.</param>
        /// <param name="serial">A individual transaction to search for.</param>
        /// <param name="start">The start of a range of dates to search from.</param>
        /// <param name="end">The end of a range od dates to search to.</param>
        /// <returns>A set of transactions.</returns>
        IList<StockDetail> GetTransactions(
            int? intakeId = null,
            int? prOrderId = null,
            int? dispatchId = null,
            int? serial = null,
            DateTime? start = null,
            DateTime? end = null);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        bool IsCarcassSplitAnimal(string serialNo);

        /// <summary>
        /// Retrieves the transactions for the goods in line.
        /// </summary>
        /// <param name="apGoodsReceiptId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        IList<StockTransactionData> GetTransactionsForIntakeLine(int apGoodsReceiptId);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockDetail GetStockTransactionByComments(string comments, int warehouseId);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        App_GetProductionStockData_Result GetProductionOrderStockData(string transId);


        /// <summary>
        /// Gets the last attribute for the stock item.
        /// </summary>
        /// <param name="serial">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        StockDetail GetSerialAttributeData(int serial);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetTransactionDataForIntoProduction(int serialNo);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransactionIncDeleted(int serial);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransactionIncProduct(int serial);

        /// <summary>
        /// Gets the last attribute for the input module and batch.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        StockDetail GetLastBatchAttributeData(int batchNumberId, int deviceId, int nouTransactionTypeId);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransactionIncIntakeId(int serial);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransactionById(int transId);

        /// <summary>
        /// Gets an out of butchery transaction data.
        /// </summary>
        /// <param name="serial">The serial number.</param>
        /// <returns>A stoc detail object containing out of butchery data.</returns>
        StockDetail GetButcheryTransactionData(int serial);

        /// <summary>
        /// Unconsumes a stock transaction.
        /// </summary>
        /// <param name="transactionId">A flag, indicating a successful unconsumeor not.</param>
        bool UnConsumePalletTransaction(int transactionId);

        /// <summary>
        /// Adds a pallet transaction.
        /// </summary>
        /// <param name="pallet">The pallet to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
       bool AddPallet(PalletStock pallet);

        /// <summary>
        /// Adds a pallet transaction to goods in.
        /// </summary>
        /// <param name="goodsInId">The receipt detail id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        int CreatePalletTransaction(int goodsInId);

        /// <summary>
        /// Gets the last transaction for the input id.
        /// </summary>
        /// <param name="id">The transaction id.</param>
        /// <returns>The last transaction for the input id</returns>
        StockTransactionData GetTransactionDataById(int id);

        /// <summary>
        /// Gets the stock on a pallet.
        /// </summary>
        /// <param name="id">The pallet id.</param>
        /// <returns>All the stock on a pallet.</returns>
        IList<StockDetail> GetPalletDetails(int id, bool scanOn);

        /// <summary>
        /// Adds/remove stock from a pallet.
        /// </summary>
        /// <param name="id">The stock id.</param>
        /// <param name="palletId">The pallet id.</param>
        /// <param name="scanOn">The scan on/off flag.</param>
        /// <returns>Flag, as to successful scan on/off.</returns>
        StockDetail UpdatePalletTransaction(int id, StockDetail pallet, bool scanOn, bool ignoreConsume);

        /// <summary>
        /// Unconsumes a stock transaction.
        /// </summary>
        /// <param name="transactionId">A flag, indicating a successful unconsumeor not.</param>
        bool UnConsumeTransaction(int transactionId);

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        int AddTransaction(StockTransaction transaction);

        /// <summary>
        /// Retrieves the last transaction in a production batch.
        /// </summary>
        /// <param name="prOrderId">The batch id.</param>
        /// <returns>The last transaction id in a production batch.</returns>
        int GetLastTransactionInBatch(int prOrderId);

        /// <summary>
        /// Gets a carcass number from the input attribute id.
        /// </summary>
        /// <param name="attributeId">The label attribute id.</param>
        /// <returns>A corresponding carcass no.</returns>
        int GetCarcassNo(int attributeId);

        /// <summary>
        /// Retrieves the transactions for the into production line.
        /// </summary>
        /// <param name="prOrderId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        IList<StockTransactionData> GetTransactionsForIntoProductionLine(int prOrderId);

        /// <summary>
        /// Retrieves the transactions for the ispatch line.
        /// </summary>
        /// <param name="dispatchLineId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        IList<StockTransactionData> GetTransactionsForDispatchLine(int dispatchLineId);
       

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetTransactionDataForLinkedCarcassSerialSplit(int serialNo);

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        bool DeleteTransactionAndItems(StockTransactionData transaction);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockAttribute GetTransactionAttributesCarcassSerial(int serialNo);

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="serial">The label serial.</param>
        /// <returns>A collection of recent transactions.</returns>
        IList<StockTransactionData> GetTransaction(int serial, int transId);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetTransactionDataForLinkedCarcassSerial(int serialNo, bool uniqueBarcode = true);

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        bool AddStockTransaction(StockTransaction transaction);

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        StoredLabel GetLabelImages(int transId);

        /// <summary>
        /// Deletes a transaction only. No other processing.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        bool DeleteTransactionOnly(StockTransactionData transaction);

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        Tuple<int, string, StockTransaction> GetTransactionAndItems(StockTransactionData transaction);

        /// <summary>
        /// Deletes a transaction and it's associated traceability data.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        string DeleteTransaction(StockTransactionData transaction);

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        StockTransaction GetStockTransaction(int serial);

        /// <summary>
        /// Gets and sets the transaction data for the order details.
        /// </summary>
        /// <param name="details">The order details.</param>
        /// <returns>The transaction data for the input order.</returns>
        void GetTransactionData(IList<SaleDetail> details);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="data">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        GoodsReceiptData GetTransactionData(GoodsReceiptData data);

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <returns>A collection of recent transactions.</returns>
        IList<StockTransactionData> GetTransactions(int transactionsToTake);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="detail">The goods receipt data containing the stock transaction.</param>
        /// <param name="useId">flag, as to whether the stocktransaction search is by id or serial..</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        bool UpdateTransactionData(SaleDetail detail, bool useId = true);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <param name="module">The relevant module.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetTransactionDataForSerial(int serialNo, ViewType module = ViewType.ARDispatch, bool useAttribute = false, int? splitStockId = null);

        /// <summary>
        /// Gets and sets the transaction data for the dispatch order details.
        /// </summary>
        /// <param name="details">The order details.</param>
        /// <returns>The transaction data for the input order.</returns>
        void GetDispatchTransactionData(IList<SaleDetail> details);

        /// <summary>
        /// Gets and sets the disptached transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <param name="arDispatchId">The ar dispatch id.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetDispatchedTransactionDataForSerial(int serialNo, int arDispatchId);

        /// <summary>
        /// Gets and sets the into production transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The serial number to search for.</param>
        /// <param name="prOrderId">The ar dispatch id.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        StockDetail GetIntoProductionTransactionDataForSerial(int serialNo, int prOrderId);

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <returns>A collection of recent transactions.</returns>
        IList<Sale> GetTransactionsForReports(int from, int to);

        /// <summary>
        /// Retrieves the into production transaction.
        /// </summary>
        /// <param name="serial">The label serial.</param>
        /// <returns>A stocktransaction.</returns>
        StockTransaction GetIntoProductionTransaction(int serial);

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <param name="labelIds">The label ids.</param>
        /// <param name="storedLabel">The label images.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        bool AddLabelToTransaction(int transId, string labelIds, StoredLabel storedLabel);

        /// <summary>
        /// Retrieves the batch traceability data.
        /// </summary>
        /// <param name="batchNo">The batch no to get the for.</param>
        /// <returns>Batch trace data.</returns>
        GoodsReceiptData GetProductionOrderBatchData(ProductionData order);

        /// <summary>
        /// Adds weight from a deleted transaction back to the carcass it came from.
        /// </summary>
        /// <param name="transaction">The transaction deleted.</param>
        /// <returns>A flag, indicating a successful weight addition or not.</returns>
        bool AddWeightBackToCarcass(StockTransaction transaction);

        /// <summary>
        /// Gets the product data between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        IList<ReportStockTransactionData_Result> GetStockTransactionData(int customerId, DateTime start, DateTime end);

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="detail">The goods receipt data containing the stock transaction.</param>
        /// <param name="viewType">The module type.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        bool UpdateTransactionWeight(IList<GoodsIn> transactions, ViewType viewType);

        /// <summary>
        /// Gets the last transaction for the input module.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        StockTransactionData GetLastTransactionData(int batchNumberId);

        /// <summary>
        /// Gets the last transaction for the input module.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        StockTransactionData GetLastTransactionDataByModule(int batchNumberId, int deviceId, int nouTransactionTypeId);

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transactionId">The transaction to find its docket for.</param>
        /// <returns>A flag, indicating a successful find or not.</returns>
        Tuple<int, string, StockTransaction> GetTransactionDocket(int transactionId);

        /// <summary>
        /// Gets the stored label printer number.
        /// </summary>
        /// <param name="labelId">The label id.</param>
        /// <returns>The stored label printer number.</returns>
        int? GetTransactionPrinter(int labelId);
    }
}

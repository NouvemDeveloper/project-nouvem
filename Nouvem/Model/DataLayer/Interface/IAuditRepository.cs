﻿// -----------------------------------------------------------------------
// <copyright file="IAuditrRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface IAuditRepository
    {
        /// <summary>
        /// Gets the audit details.
        /// </summary>
        /// <returns>A collection of audit details.</returns>
        IList<Audit> GetAuditDetails();

        /// <summary>
        /// Gets the audit details for the current day.
        /// </summary>
        /// <returns>A collection of todays audit details.</returns>
        IList<Audit> GetTodaysAuditDetails();
    }
}

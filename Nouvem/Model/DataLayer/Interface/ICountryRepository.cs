﻿// -----------------------------------------------------------------------
// <copyright file="ICountryRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface ICountryRepository
    {
        /// <summary>
        /// Retrieve all the countries.
        /// </summary>
        /// <returns>A collection of countries.</returns>
        IList<Country> GetCountryMaster();

        /// <summary>
        /// Find the country code for a particular country code from an ear tag.
        /// </summary>
        /// <param name="code">The code of the country as determine by the ear tag.</param>
        /// <returns>The country relating to the code.</returns>
        string FindCountryByCode(string code);

        /// <summary>
        /// Add or updates the countries list.
        /// </summary>
        /// <param name="countries">The countries to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateCountryMasters(IList<Country> countries);
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="ILicenseRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface ILicenseRepository
    {
        /// <summary>
        /// Retrieve all the database licensees (devices and users), and their associated license type names.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        IList<Licensee> GetLicensees();
        /// <summary>
        /// Retrieve all the database license totals by license type.
        /// </summary>
        /// <returns>A collection of license totals by type.</returns>
        IList<ViewGroupedLicenseDetail> GetLicenseTotals();

        /// <summary>
        /// Method that assigns a license type to a user/device.
        /// </summary>
        /// <param name="selectedLicensee">The selected user/device.</param>
        /// <param name="selectedLicenseDetails">The licence type to allocate</param>
        /// <returns>A flag, indicating a successful allocation or not.</returns>
        bool AssignLicense(Licensee selectedLicensee, LicenseDetails selectedLicenseDetails);

        /// <summary>
        /// Makes a call to deallocate the collection of licences.
        /// </summary>
        /// <param name="deallocations">The licencess to deallocate licences from.</param>
        bool DeallocateLicences(IList<Licensee> deallocations);

        /// <summary>
        /// Retrieve all the database license details.
        /// </summary>
        /// <returns>A collection of license details.</returns>
        IList<ViewLicenseDetail> GetLicenseDetails();

        /// <summary>
        /// Stores an imported licence details.
        /// </summary>
        /// <param name="details">The licence details to store.</param>
        /// <returns>The newly stored licence details.</returns>
        IList<LicenceDetail> StoreLicenceDetails(Licencing.LicenseDetail details);

        /// <summary>
        /// Assigns a licences to a group of licensee.
        /// </summary>
        /// <param name="selectedLicenses">The licensees, and thier associated licence detail to allocate.</param>
        /// <returns>A flag, indication a successful transaction or not.</returns>
        bool UpgradeLicenses(IDictionary<Licensee, LicenceDetail> selectedLicenses);
    }
}

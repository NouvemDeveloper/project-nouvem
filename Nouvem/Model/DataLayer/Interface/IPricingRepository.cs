﻿// -----------------------------------------------------------------------
// <copyright file="IPricingRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.AccountsIntegration.BusinessObject;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IPricingRepository
    {
        /// <summary>
        /// Retrieve all the rounding options.
        /// </summary>
        /// <returns>A collection of rounding options.</returns>
        IList<NouRoundingOption> GetRoundingOptions();

        /// <summary>
        /// Updates a price.
        /// </summary>
        /// <param name="price">The price to update to.</param>
        /// <param name="priceListId">The price book id.</param>
        /// <param name="inmasterId">The product id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdatePrice(decimal price, int priceListId, int inmasterId);

        /// <summary>
        /// Determines if a product is on a price book.
        /// </summary>
        /// <param name="priceListId">The price book to check.</param>
        /// <param name="inmasterId">The product to check.</param>
        /// <returns>Flag, as to whether a product is on a price book.</returns>
        bool IsProductOnPriceBook(int priceListId, int inmasterId);

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        PriceDetail GetPriceListDetail(int priceListDetailId);

        /// <summary>
        /// Copies a price lists details to another price list.
        /// </summary>
        /// <param name="copyFrom">The price list id we are copying from.</param>
        /// <param name="copyTo">The price list id we are copying to.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        bool CopyPriceListDetails(int copyFrom, int copyTo, int deviceId, int userId);

        /// <summary>
        /// Retrieves the price detail for the input data.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price detail.</returns>
        PriceDetail GetPriceListDetailByPartner(int partnerId, int inMasterId, int basePriceListId);

        /// <summary>
        /// Adds a new collection of partners to the db.
        /// </summary>
        /// <param name="partners">The partners to add.</param>
        void AddOrUpdatePrices(IList<Prices> prices);

        /// <summary>
        /// Gets the price from a price list detail record.
        /// </summary>
        /// <param name="priceListId">The price list detail.</param>
        /// <returns>A corresponding price.</returns>
        decimal? GetPartnerPrice(int priceListId, int inmasterId);

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        IList<PriceMaster> GetPriceListsAndDetails();

        /// <summary>
        /// Retrieve all the price list customer groups.
        /// </summary>
        /// <returns>A collection of price list customer groups.</returns>
        IList<App_GetPriceListCustomerGroups_Result> GetPriceListCustomerGroups();

        /// <summary>
        /// Retrieves the price detail for the input data.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price detail.</returns>
        PriceDetail GetPriceListDetail(int priceListId, int inMasterId);

        /// <summary>
        /// Retrieve the first price detail for the input product.
        /// </summary>
        /// <returns>A corresponding price detail.</returns>
        PriceDetail GetFirstProductPriceListDetail(int inMasterId);

        /// <summary>
        /// Retrieve all the rounding rules.
        /// </summary>
        /// <returns>A collection of rounding rules.</returns>
        IList<NouRoundingRule> GetRoundingRules();

        /// <summary>
        /// Have the prices been edited flag.
        /// </summary>
        /// <returns>A flag as to whether a price has been edited.</returns>
        bool AreTherePriceEdits();

        /// <summary>
        /// Retrieves the price method for the input data.
        /// </summary>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>An associated price method.</returns>
        NouPriceMethod GetProductPriceMethod(int inMasterId);

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        IList<PriceMaster> GetNewPriceLists();

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        PriceMaster GetPriceList(int priceListId);

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        IList<PriceDetail> GetUpdatedPriceListDetails();

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        IList<PriceDetail> GetUpdatedPriceListDetails(int priceListId);

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        IList<PriceMaster> GetPriceLists();

        /// <summary>
        /// Method that makes a repository add or update the input special prices.
        /// </summary>
        /// <param name="specialPrices">The label associations to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateSpecialPrices(IList<SpecialPrice> specialPrices);

        /// <summary>
        /// Retrieve all the special prices.
        /// </summary>
        /// <returns>A collection of special prices.</returns>
        IList<SpecialPrice> GetSpecialPrices();

        /// <summary>
        /// Retrieve all the updated price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        IList<PriceMaster> GetUpdatedPriceLists();

        /// <summary>
        /// Adds or updates the price lists.
        /// </summary>
        /// <param name="priceList">The priceList to add.</param>
        /// <returns>A flag, indicating a successful add.</returns>
        PriceList AddPriceList(PriceList priceList);

        /// <summary>
        /// Retrieve all the price lists.
        /// </summary>
        /// <returns>A collection of price lists.</returns>
        IList<PriceList> GetAllPriceLists();

        /// <summary>
        /// Adds or updates the price lists.
        /// </summary>
        /// <param name="priceLists">The priceLists to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdatePriceMasters(IList<PriceMaster> priceLists);

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        IList<PriceDetail> GetPriceListDetails();

        /// <summary>
        /// Retrieve all the order methods.
        /// </summary>
        /// <returns>A collection of order methods.</returns>
        IList<NouOrderMethod> GetOrderMethods();

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        IList<NouPriceMethod> GetPriceMethods();

        /// <summary>
        /// Retrieve all the inventory master products.
        /// </summary>
        /// <returns>A collection of inventory master products.</returns>
        IList<PriceDetail> GetProducts();

        /// <summary>
        /// Adds or updates the price list details.
        /// </summary>
        /// <param name="priceListDetails">The priceList details to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdatePriceListDetails(IList<PriceDetail> priceListDetails, int progressAmt);

        /// <summary>
        /// Gets the grouped products.
        /// </summary>
        /// <returns>A collection of grouped products.</returns>
        IList<ProductSelection> GetGroupedProducts();

        /// <summary>
        /// Updates a price book with the input price..
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="partnerPriceListId">The partner price list id.</param>
        /// <param name="price">The new price.</param>
        /// <param name="productId">The product id.</param>
        /// <returns>A flag, indicating a successful change or not.</returns>
        Tuple<bool, PriceDetail> UpdatePrice(int priceListId, int partnerPriceListId, decimal price, int productId);

        /// <summary>
        /// Retrieve all the price list details.
        /// </summary>
        /// <returns>A collection of price list details.</returns>
        IList<PriceDetail> GetPriceListDetails(int priceListDetailId);

        /// <summary>
        /// Gets the price from a price list detail record.
        /// </summary>
        /// <param name="priceListDetailId">The price list detail.</param>
        /// <returns>A corresponding price.</returns>
        decimal? GetPrice(int priceListDetailId);
    }
}

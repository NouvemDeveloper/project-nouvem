﻿// -----------------------------------------------------------------------
// <copyright file="IDynamicRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface IDynamicRepository
    {
        /// <summary>
        /// Retrieves the label data result row.
        /// </summary>
        DataTable GetSPData(string spName);

        /// <summary>
        /// Deletes a grader stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        string DeleteGraderStock(int serial, int userId, int deviceId);

        /// <summary>
        /// Gets the table data.
        /// </summary>
        /// <param name="command">The sql query.</param>
        /// <returns>A collection of table data.</returns>
        IList<object> GetData(string command);

        /// <summary>
        /// Deletes a batch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        string DeleteProductionStock(string serial, int id, int userId, int deviceId);

        /// <summary>
        /// Undeletes a stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The transactiontype id.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        string UndeleteStock(int serial, int id, int userId, int deviceId, int orderId = 0);

        /// <summary>
        /// Deletes a batch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        string DeleteOutOfProductionStock(int serial, int userId, int deviceId);

        /// <summary>
        /// Deletes a dispatch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        string DeleteDispatchStock(int serial, int id, int userId, int deviceId, int warehouseId,int inmasterId = 0);

        /// <summary>
        /// Deletes an intake stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        string DeleteIntakeStock(int serial, int id, int userId, int deviceId);

        /// <summary>
        /// Retrieves the search screens macro data.
        /// </summary>
        /// <param name="name">The sp name.</param>
        /// <param name="start">The search start vaue.</param>
        /// <param name="end">The seach end value.</param>
        /// <param name="showAll">The show all in search flag.</param>
        /// <returns>Search scrren macro data.</returns>
        Tuple<DataTable, string> GetSearchScreenMacroData(string name, DateTime start, DateTime end, bool showAll, string searchText, string searchDateType);

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        Tuple<bool, string> GetSPResult(string spName, string value, int id);
       
        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        Tuple<string, string, string> GetSPResultReturn(string spName, string value, int id, int? referenceId = null, int? processId = null, int? productID = 0);
       
        /// <summary>
        /// Validates a transaction.
        /// </summary>
        /// <param name="spName">The sp macro.</param>
        /// <param name="id">The document id.</param>
        /// <param name="param1">param 1</param>
        /// <param name="param2">param 2</param>
        /// <param name="param3">param 3</param>
        /// <param name="param4">param 4</param>
        /// <returns>A validation issue message, or empty if validated.</returns>
        string GetSPDataResultReturn(string spName, int id, decimal wgt, decimal qty, int inmasterid,
            int warehouseId,
            int processId, int userId, int deviceid, string reference = "", string reference2 = "", string reference3 = "", string reference4 = "");
    }
}


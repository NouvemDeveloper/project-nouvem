﻿// -----------------------------------------------------------------------
// <copyright file="IAttributeRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Interface
{
    public interface IAttributeRepository
    {
        /// <summary>
        /// Adds a new attribute.
        /// </summary>
        /// <param name="attributeData">The attribute and associated data to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool AddAttribute(AttributeSetUp attributeData);

        /// <summary>
        /// Sets a modules haccp complete flag.
        /// </summary>
        /// <param name="moduleid">The module id.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        bool SetWorkflowModule(int moduleid, int workflowid);

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        IList<DressSpec> GetDressSpecs();

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        IList<KillingType> GetKillingType();

        /// <summary>
        /// Gets the start up checks for a particular process/module.
        /// </summary>
        /// <returns>The relevant start up checks.</returns>
        App_GetHACCPForModule_Result1 GetProcessModuleStartUpChecks(int processId, int moduleid, int inmasterid);

        /// <summary>
        /// Retrieve all the system attribute allocations permissions.
        /// </summary>
        /// <returns>A collection of attribute allocation permissions.</returns>
        void GetAttributeAllocationPermissions(IList<AttributeAllocationData> allocations, int userId,
            int userGroupId);

        /// <summary>
        /// Adds a range of attributes.
        /// </summary>
        /// <param name="attributes">The attributes to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        bool AddAttributes(IList<Model.DataLayer.Attribute> attributes);

        /// <summary>
        /// Adds/Updates tabs.
        /// </summary>
        /// <param name="tabs">The tabs to update.</param>
        /// <returns>Flag, as to successful add/update.</returns>
        bool UpdateTabs(IList<AttributeTabName> tabs);

        /// <summary>
        /// Copies a template.
        /// </summary>
        /// <param name="templateToCopyId">The template id to copy from.</param>
        /// <param name="name">The new template name.</param>
        /// <returns>Newly created template id.</returns>
        int CopyTemplate(int templateToCopyId, string name);

        /// <summary>
        /// Gets the start up checks, and completed daily workflows, checking for any unstarted or uncompleted checks.
        /// </summary>
        /// <returns>A list of process required workflows not started.or completed.</returns>
        IList<string> GetProcessStartUpChecks(int processId);

        /// <summary>
        /// Retrieve all the system attribute allocations.
        /// </summary>
        /// <returns>A collection of attribute allocations.</returns>
        void GetAttributeAllocations(IList<AttributeAllocationData> allocations);

        /// <summary>
        /// Updates an attribute.
        /// </summary>
        /// <param name="attributeData">The attribute and associated data to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateAttribute(AttributeSetUp attributeData);

        /// <summary>
        /// Retrieve the allocation data for the input template id.
        /// </summary>
        /// <returns>An ttribute allocation.</returns>
        IList<AttributeAllocationData> GetAttributeAllocationForTemplate(int templateId);

        /// <summary>
        /// Gets all the batches in a location.
        /// </summary>
        /// <param name="location">The location to check.</param>
        /// <returns>All the batches in a location.</returns>
        IList<int?> GetBatchesInLocation(Location location);

        /// <summary>
        /// Adds or updates a batch attribute.
        /// </summary>
        /// <returns>Flag, as to successful update.</returns>
        bool UpdateAllBatchAttributes(StockDetail detail, IList<int?> batchIds);

        /// <summary>
        /// Adds or updates a batch attribute.
        /// </summary>
        /// <returns>Flag, as to successful add or update..</returns>
        int AddOrUpdateBatchAttributes(StockDetail details);

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        DataLayer.Attribute GetAttributeByBatchId(int id);

        /// <summary>
        /// Gets the attribute by base id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        DataLayer.Attribute GetAttributeByBaseBatchId(int id);

        /// <summary>
        /// Retrieve all the system attribute allocations.
        /// </summary>
        /// <returns>A collection of attribute allocations.</returns>
        IList<AttributeAllocationData> GetAttributeAllocations();

        /// <summary>
        /// Gets the  processes.
        /// </summary>
        /// <returns>The application processes.</returns>
        IList<Process> GetProcesses();

        /// <summary>
        /// Gets the internal grades.
        /// </summary>
        /// <returns>The internal grades.</returns>
       IList<InternalGrade> GetInternalGrades();

        /// <summary>
        /// Adds an attribute to a template.
        /// </summary>
        /// <param name="traceabilityMasters">The collection of the attributes to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddAttributesToTemplate(IList<AttributeMasterData> traceabilityMasters, int templateId,
            bool isWorkflow, string notes, bool inactive, string attributeType);

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        /// <returns>A collection of tabs.</returns>
        IList<AttributeTabName> GetAttributeTabNames();

        /// <summary>
        /// Gets the non standard response authorised groups.
        /// </summary>
        /// <returns>A collection of non standard response authorised groups.</returns>
        IList<UserGroupNonStandard> GetUserGroupsNonStandard();

        /// <summary>
        /// Retrieve all the system traceability template names.
        /// </summary>
        /// <returns>A collection of traceability template names.</returns>
        IList<AttributeTemplate> GetAttributeTemplateNames();

        /// <summary>
        /// Retrieve all the system traceability template groups.
        /// </summary>
        /// <returns>A collection of traceability template groups.</returns>
        IList<AttributeTemplateGroup> GetAttributeTemplateGroups();

        /// <summary>
        /// Adds or updates traceability groups.
        /// </summary>
        /// <param name="traceabilityNames">The traceability groups to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateAttributeTemplateGroups(IList<AttributeTemplateGroup> traceabilityNames);

        /// <summary>
        /// Adds or updates traceability names.
        /// </summary>
        /// <param name="traceabilityNames">The traceability names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateAttributeTemplateNames(IList<AttributeTemplate> traceabilityNames);

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        /// <returns>A collection of tabs.</returns>
        IList<NouAttributeReset> GetAttributeResets();

        /// <summary>
        /// Gets the attribute processes.
        /// </summary>
        /// <returns>The application attribute processes.</returns>
        IList<AttributeProcess> GetAttributeProcesses();

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>The application attributes.</returns>
        IList<Sale> GetAttributesForSearch();

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>The application attributes.</returns>
        IList<AttributeMasterData> GetAttributes();

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        /// <returns>The attribute look ups.</returns>
        IList<AttributeLookUpData> GetAttributeLookUps();

        /// <summary>
        /// Gets the last attribute.
        /// </summary>
        /// <returns>The last edited attribute.</returns>
        AttributeSetUp GetAttributeByLastEdit();

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        AttributeSetUp GetAttributeById(int id);

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        AttributeSetUp GetAttributeByFirstLast(bool first);

        /// <summary>
        /// Gets the sp names.
        /// </summary>
        /// <returns>The application stored procedure names.</returns>
        IList<string> GetSPNames();

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        IList<NouBreed> GetBreeds();

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>The application categories.</returns>
        IList<NouCategory> GetCategories();

        /// <summary>
        /// Gets the cleanliness values.
        /// </summary>
        /// <returns>The application cleanliness values.</returns>
        IList<NouCleanliness> GetCleanlinesses();

        /// <summary>
        /// Adds a breed.
        /// </summary>
        /// <returns>Flag, indicating successful breed addition.</returns>
        bool AddBreed(NouBreed breed);

        /// <summary>
        /// Gets the fat colours.
        /// </summary>
        /// <returns>The application fat colours.</returns>
        IList<NouFatColour> GetFatColours();

        /// <summary>
        /// Gets the destinations.
        /// </summary>
        /// <returns>The application destinations.</returns>
        IList<Destination> GetDestinations();
    }
}


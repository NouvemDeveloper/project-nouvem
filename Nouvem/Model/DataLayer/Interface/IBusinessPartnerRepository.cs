﻿// -----------------------------------------------------------------------
// <copyright file="IBusinessPartnerRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    /// <summary>
    /// Interface implemented by the BusinessPartnerRepository class.
    /// </summary>
    interface IBusinessPartnerRepository
    {
        /// <summary>
        /// Method that retrieves the business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        IList<ViewBusinessPartner> GetBusinessPartners();

        /// <summary>
        /// Method that retrieves the business partners addresses.
        /// </summary>
        /// <returns>A collection of business partner addresses.</returns>
        IList<BusinessPartnerAddress> GetBusinessPartnerAddresses();

        /// <summary>
        /// Gets the inactive partners.
        /// </summary>
        /// <returns>The inactive partners.</returns>
        IList<App_GetInactivePartners_Result> GetInactivePartners();

        /// <summary>
        /// Updates a partner agent.
        /// </summary>
        /// <param name="partnerId">The partner to update.</param>
        /// <param name="agentId">The agent id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdatePartnerAgent(int partnerId, int? agentId);

        /// <summary>
        /// Method which returns the non deleted business partners contacts.
        /// </summary>
        /// <returns>A collection of non deleted business partner contacts.</returns>
        IList<BusinessPartnerContact> GetBusinessPartnerContactsById(int partnerId);

        /// <summary>
        /// Gets the partner matching in input id..
        /// </summary>
        /// <returns>A matching partner.</returns>
        BusinessPartner GetBusinessPartner(int partnerId);

        /// <summary>
        /// Gets the partner matching in input id..
        /// </summary>
        /// <returns>A matching partner.</returns>
        ViewBusinessPartner GetBusinessPartnerShort(int partnerId);

        /// <summary>
        /// Returns a partners on hold status.
        /// </summary>
        /// <param name="partnerID">The partner id.</param>
        /// <returns>The partners on hold status.</returns>
        bool GetPartnerOnHoldStatus(int partnerID);

        /// <summary>
        /// Gets a bp partner.
        /// </summary>
        /// <param name="partnerId">The partner id to search for.</param>
        /// <returns>A bp partner.</returns>
        BPMaster GetPartner(int partnerId);

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnersToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateBusinessPartnerPricing(IList<BusinessPartner> partnersToUpdate);

        /// <summary>
        /// Method which returns the non deleted business partners contacts.
        /// </summary>
        /// <returns>A collection of non deleted business partner contacts.</returns>
        IList<BusinessPartnerContact> GetBusinessPartnerContacts();

        /// <summary>
        /// Method which returns the non deleted business partner label fields.
        /// </summary>
        /// <returns>A collection of non deleted business partner label fields.</returns>
        IList<BPLabelField> GetLabelFields();

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="partnerId">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        int GetPartnerPriceListId(int partnerId);

        /// <summary>
        /// Method that retrieves the business partners attachments.
        /// </summary>
        /// <returns>A collection of business partner attachments.</returns>
        IList<BPAttachment> GetBusinessPartnerAttachments();

        /// <summary>
        /// Method which returns the non deleted business partner groups.
        /// </summary>
        /// <returns>A collection of non deleted business partner groups.</returns>
        IList<BPGroup> GetGroups();

        /// <summary>
        /// Method that retrieves the business partners currencies.
        /// </summary>
        /// <returns>A collection of business partner currencies.</returns>
        IList<BPCurrency> GetBusinessPartnerCurrencies();

        /// <summary>
        /// Method that adds a new business partner.
        /// </summary>
        /// <param name="partner">The partner to add.</param>
        /// <returns>The newly added partner id.</returns>
        int AddBusinessPartner(BusinessPartner partner);

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnerToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool UpdateBusinessPartner(BusinessPartner partnerToUpdate);

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="attachment">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        bool DeleteAttachment(BPAttachment attachment);

        /// <summary>
        /// Method that retrieves the last 100 business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        IList<ViewBusinessPartner> GetRecentBusinessPartners();

        /// <summary>
        /// Add or updates the properties list.
        /// </summary>
        /// <param name="properties">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateProperties(IList<BPProperty> properties);

        /// <summary>
        /// Method that retrieves the partner groups.
        /// </summary>
        /// <returns>A collection of partner groups.</returns>
        IList<PartnerGroup> GetPartnerGroups();

        /// <summary>
        /// Add or updates the partner groups list.
        /// </summary>
        /// <param name="groups">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdatePartnerGroups(ref List<PartnerGroup> groups);

        /// <summary>
        /// Method that retrieves the partner types.
        /// </summary>
        /// <returns>A collection of partner types.</returns>
        IList<PartnerType> GetPartnerTypes();

        /// <summary>
        /// Add or updates the partner types list.
        /// </summary>
        /// <param name="types">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdatePartnerTypes(ref List<PartnerType> types);

        /// <summary>
        /// Add or updates the currencies list.
        /// </summary>
        /// <param name="currencies">The currencies to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateCurrencies(IList<BPCurrency> currencies);

        /// <summary>
        /// Determines is a partner code already exists in the db.
        /// </summary>
        /// <param name="code">The code to check.</param>
        /// <returns>Flag, as to whether the code exists.</returns>
        bool DoesPartnerCodeExist(string code);

        /// <summary>
        /// Returns a partners on hold status.
        /// </summary>
        /// <param name="partnerID">The partner id.</param>
        /// <returns>The partners on hold status.</returns>
        int IsPartnerLiveOrderOnSystem(int partnerID);
    }
}

﻿// -----------------------------------------------------------------------
// <copyright file="IUOMRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface IUOMRepository
    {
        /// <summary>
        /// Retrieve all the cuoms.
        /// </summary>
        /// <returns>A collection of uoms.</returns>
        IList<UOMMaster> GetUOMMaster();

        /// <summary>
        /// Add or updates the uoms list.
        /// </summary>
        /// <param name="uoms">The uoms to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateUOMMasters(IList<UOMMaster> uoms);
    }
}

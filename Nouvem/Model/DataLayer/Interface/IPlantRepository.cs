﻿// -----------------------------------------------------------------------
// <copyright file="IPlantRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;

    public interface IPlantRepository
    {
        /// <summary>
        /// Retrieve all the cplants.
        /// </summary>
        /// <returns>A collection of plants.</returns>
        IList<Plant> GetAllPlants();

        /// <summary>
        /// Method which returns the warehouses and sub warehouses.
        /// </summary>
        /// <returns>A collection of non deleted warehouses.</returns>
        IList<Location> GetStockWarehouses();

        /// <summary>
        /// Retrieve all the cplants.
        /// </summary>
        /// <returns>A collection of plants.</returns>
        IList<ViewPlant> GetPlants();

        /// <summary>
        /// Method which returns the warehouse location matching the input id.
        /// </summary>
        /// <returns>A warehouse sub location.</returns>
        WarehouseLocation GetSubLocation(int id);

        /// <summary>
        /// Method which returns a warehouse and it's sub warehouses.
        /// </summary>
        /// <returns>A warehouse.</returns>
        Location GetStockWarehouse(int warehouseId);

        /// <summary>
        /// Add or updates the plants.
        /// </summary>
        /// <param name="plants">The plants to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdatePlants(IList<ViewPlant> plants);

        /// <summary>
        /// Method which returns the warehouse locations.
        /// </summary>
        /// <returns>A collection of non deleted warehouse locations.</returns>
        IList<Warehouse> GetWarehouses();

        /// <summary>
        /// Add or updates the warehouses list.
        /// </summary>
        /// <param name="warehouses">The warehouses to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateWarehouses(IList<Warehouse> warehouses);

        /// <summary>
        /// Deleted the input warehouse.
        /// </summary>
        /// <param name="warehouse">The warehouse to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        bool RemoveWarehouse(Warehouse warehouse);

        /// <summary>
        /// Method which returns the warehouse locations.
        /// </summary>
        /// <returns>A collection of non deleted warehouse locations.</returns>
        IList<WarehouseLocation> GetWarehouseLocations();

        /// <summary>
        /// Add or updates the warehouses list.
        /// </summary>
        /// <param name="locations">The warehouses to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateWarehouseLocations(IList<WarehouseLocation> locations);

        /// <summary>
        /// Deleted the input warehouse location.
        /// </summary>
        /// <param name="location">The warehouse location to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        bool RemoveWarehouseLocation(WarehouseLocation location);
    }
}

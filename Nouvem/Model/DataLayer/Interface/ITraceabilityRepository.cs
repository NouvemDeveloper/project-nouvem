﻿// -----------------------------------------------------------------------
// <copyright file="ITraceabilityRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface ITraceabilityRepository
    {
        /// <summary>
        /// Gets the allocation data for the input template.
        /// </summary>
        /// <returns>The associated template data.</returns>
        IList<AttributeAllocationData> GetAttributeAllocationData(int templateId);

        /// <summary>
        /// Gets the template groups.
        /// </summary>
        /// <returns>The template groups.</returns>
        IList<AttributeTemplateGroup> GetTemplateGroups();

        /// <summary>
        /// Gets the templates.
        /// </summary>
        /// <returns>The template names.</returns>
        IList<AttributeTemplateData> GetTemplateNames();

        /// <summary>
        /// Retrieve all the traceability masters.
        /// </summary>
        /// <returns>A collection of traceability masters.</returns>
        IList<ViewTraceabilityMaster> GetTraceabilityMasters();

        /// <summary>
        /// Adds or updates traceability masters.
        /// </summary>
        /// <param name="traceabilityMasters">The raceability masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateTraceabilityMasters(IList<ViewTraceabilityMaster> traceabilityMasters);

        /// <summary>
        /// Retrieve all the system traceability types.
        /// </summary>
        /// <returns>A collection of traceability types.</returns>
        IList<NouTraceabilityType> GetTraceabilityTypes();

        /// <summary>
        /// Retrieve all the system traceability template names.
        /// </summary>
        /// <returns>A collection of traceability template names.</returns>
        IList<TraceabilityTemplateName> GetTraceabilityTemplateNames();

        /// <summary>
        /// Adds or updates traceability names.
        /// </summary>
        /// <param name="traceabilityNames">The traceability names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateTraceabilityTemplateNames(IList<TraceabilityTemplateName> traceabilityNames);

        /// <summary>
        /// Retrieve all the system traceability template allocations.
        /// </summary>
        /// <returns>A collection of traceability template allocations.</returns>
        IList<TraceabilityTemplateAllocation> GetTraceabilityTemplateAllocations();

        /// <summary>
        /// Adds a traceability master to a template.
        /// </summary>
        /// <param name="traceabilityMasterId">The id of the traceability master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddTraceabilityToTemplate(int traceabilityMasterId, int templateId);

        /// <summary>
        /// Removes a traceability master from a template.
        /// </summary>
        /// <param name="traceabilityMasterId">The id of the traceability master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        bool RemoveTraceabilityFromTemplate(int traceabilityMasterId, int templateId);

        /// <summary>
        /// Adds a date masters to a template.
        /// </summary>
        /// <param name="traceabilityMasters">The collection of the date masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddTraceabilitysToTemplate(IList<ViewTraceabilityMaster> traceabilityMasters, int templateId);

        /// <summary>
        /// Retrieve all the traceability template allocations.
        /// </summary>
        /// <returns>A collection of traceability template allocations.</returns>
        IList<ViewTraceabilityTemplateAllocation> GetViewTraceabilityTemplateAllocations();

        /// <summary>
        /// Retrieve all the system traceability data.
        /// </summary>
        /// <returns>A collection of traceability data.</returns>
        IList<TraceabilityData> GetTraceabilityData();

        /// <summary>
        /// Gets the traceability methods.
        /// </summary>
        /// <returns>A collection of traceability methods.</returns>
        IList<NouTraceabilityMethod> GetTraceabilityMethods();
    }
}

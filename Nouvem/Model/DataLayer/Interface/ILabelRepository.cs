﻿// -----------------------------------------------------------------------
// <copyright file="ILicenseRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using System.Data;

    public interface ILabelRepository
    {
        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        IList<Label> GetLabels();

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetPalletCardData(int stockTransactionId);

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetPalletCardDataMultiBatch(int stockTransactionId, decimal wgt, decimal qty,
            int batchNumberId);

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetPalletLabelData(int stockTransactionId);

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetIntoProductionLabelData(int stockTransactionId);

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetStockMovementData(int stockTransactionId);
        
        /// <summary>
        /// Retrieve all the label look ups.
        /// </summary>
        /// <returns>A collection of label look ups.</returns>
        IList<ExternalLabelDataLookUp> GetExternalLabelDataLookUps();

        /// <summary>
        /// Retrieves the sequencer data schema.
        /// </summary>
        /// <param name="atributeId">The stock transaction id to pass in.</param>
        DataTable GetSequencerLabelData(int attributeId);

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        IList<Gs1AIMaster> GetGs1Master();

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        IList<BarcodeParse> GetBarcodeParses();

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        IList<ViewGs1Master> GetGs1MasterView();

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetGraderLabelData(int stockTransactionId);

        /// <summary>
        /// Method that makes a repository call to add or update a gs1 master.
        /// </summary>
        /// <param name="gs1Masters">The gs1 masters to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateGs1Masters(IList<Gs1AIMaster> gs1Masters);

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetIntakeLabelData(int stockTransactionId);

        /// <summary>
        /// Retrieves the out of production data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetOutOfProductionLabelData(int stockTransactionId);

        /// <summary>
        /// Retrieves the dispatch data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        DataTable GetDispatchLabelData(int stockTransactionId);

        /// <summary>
        /// Retrieves the label associations.
        /// </summary>
        /// <returns>A collection of label associations.</returns>
        IList<LabelsAssociation> GetLabelAssociations();

        /// <summary>
        /// Method that makes a repository call to add or update the label associations.
        /// </summary>
        /// <param name="labelAssociations">The label associations to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateLabelAssociations(IList<LabelsAssociation> labelAssociations);

        /// <summary>
        /// Gets the application printers.
        /// </summary>
        /// <returns>The application printers.</returns>
        IList<Printer> GetPrinters();
    }
}

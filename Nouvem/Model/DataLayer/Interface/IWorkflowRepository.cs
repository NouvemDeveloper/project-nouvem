﻿// -----------------------------------------------------------------------
// <copyright file="IWorkflowRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Interface
{
    public interface IWorkflowRepository
    {
        /// <summary>
        /// Adds a new workflow.
        /// </summary>
        /// <param name="workflow">The workflow to add.</param>
        /// <returns>The id of the newly created workflow.</returns>
        int AddWorkflow(Sale workflow);

        /// <summary>
        /// Marks all user alerts as opened.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>Flag indicating whether all the user alerts ware marked as opened.</returns>
        bool MarkAllUserAlertAsViewed(int userId);

        /// <summary>
        /// Marks all user alerts as opened.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>Flag indicating whether all the user alerts ware marked as opened.</returns>
        bool MarkAllUserAlertAsComplete(int userId);

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        /// <returns>The current workflows.</returns>
        IList<Sale> GetWorkflows(IList<int?> statuses);

        /// <summary>
        /// Removes the alerts matching the input parameters.
        /// </summary>
        /// <param name="workflowiId">The workflow id.</param>
        /// <param name="attributeMasterId">The attribute id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        bool RemoveUserAlerts(int workflowiId, int attributeMasterId);

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="workflowId">The workflow id.</param>
        /// <param name="status">The status to update to.</param>
        /// <returns>Fla, as to successful update or not.</returns>
        bool UpdateWorkflowStatus(HashSet<Sale> workflows, int status);


        /// <summary>
        /// Updates alert status.
        /// </summary>
        /// <param name="alertIds">The alert id.</param>
        /// <param name="status">The status to update to.</param>
        /// <returns>Fla, as to successful update or not.</returns>
        bool UpdateAlertsStatus(HashSet<int> alertIds, int status);

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        /// <returns>The current workflows.</returns>
        IList<Sale> GetWorkflowsByDate(IList<int?> statuses, DateTime start, DateTime end);

        /// <summary>
        /// Gets the workflow.
        /// </summary>
        /// <returns>The current workflow by id.</returns>
        Sale GetWorkflowById(int id);

        /// <summary>
        /// Marks an alert as opened.
        /// </summary>
        /// <param name="alertId">The alert id.</param>
        /// <returns>Flag indicating whether the alert was marked as opened.</returns>
        bool MarkAlertAsViewed(int alertId);

        /// <summary>
        /// Updates a workflow workflow.
        /// </summary>
        /// <param name="workflow">The workflow to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateWorkflow(Model.DataLayer.AttributeWorkflow attribute);

        /// <summary>
        /// Updates a workflow workflow.
        /// </summary>
        /// <param name="workflow">The workflow to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool CompleteWorkflow(Sale workflow);

        /// <summary>
        /// Adds a non standard response reason.
        /// </summary>
        /// <param name="reason">The reason to add.</param>
        /// <param name="attributeId">The associated attribute.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool AddNonStandardResponseReason(string reason, int attributeId);

        /// <summary>
        /// Gets the users to alert.
        /// </summary>
        /// <returns>The user/user groups to alert.</returns>
        AlertUsers GetAlertUsers();

        /// <summary>
        /// Adds the alerts to the db.
        /// </summary>
        /// <param name="alerts">The alerts to add.</param>
        /// <returns>Flag, indicating a successful add.</returns>
        bool AddUserAlerts(IList<Alert> alerts);

        /// <summary>
        /// Determines if there are any unread alerts.
        /// </summary>
        /// <param name="userIds">The users to check.</param>
        /// <returns>Flag indicating whether there are any unread alerts.</returns>
        bool AreThereNewAlerts(HashSet<int?> userIds, bool useAlertsOpen);

        /// <summary>
        /// Determines if there are any unread alerts.
        /// </summary>
        /// <param name="userIds">The users to check.</param>
        /// <returns>Flag indicating whether there are any unread alerts.</returns>
        bool MarkAlertsAsRead(HashSet<int?> userIds);

        /// <summary>
        /// Gets the user alerts.
        /// </summary>
        /// <returns>The user alerts.</returns>
        IList<Sale> GetAlerts(IList<int?> docStatusIds, DateTime start, DateTime end);

        /// <summary>
        /// Gets the date of the last workflow created matching the input template.
        /// </summary>
        /// <param name="templateId">The input template.</param>
        /// <returns>The last workflow created matching the input template.</returns>
        DateTime? GetLastWorkflowDate(int templateId);
    }
}


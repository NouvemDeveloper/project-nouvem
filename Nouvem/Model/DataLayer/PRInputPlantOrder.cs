//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRInputPlantOrder
    {
        public int PRInputPlantOrderID { get; set; }
        public int PROrderID { get; set; }
        public int PlantID { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
    
        public virtual PROrder PROrder { get; set; }
    }
}

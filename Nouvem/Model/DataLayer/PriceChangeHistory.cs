//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class PriceChangeHistory
    {
        public int PriceChangeHistoryID { get; set; }
        public int INMasterID { get; set; }
        public Nullable<int> BPMasterID { get; set; }
        public Nullable<decimal> OldPrice { get; set; }
        public Nullable<decimal> NewPrice { get; set; }
        public Nullable<int> PriceListID_Old { get; set; }
        public Nullable<int> PriceListID_New { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    
    public partial class App_GetPalletisationStock_Result
    {
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public Nullable<int> PalletID { get; set; }
        public int StockTransactionID { get; set; }
        public string PLU { get; set; }
        public string AnaCode { get; set; }
        public Nullable<int> Pieces { get; set; }
        public Nullable<int> BatchNumberID { get; set; }
        public string CountryOfSlaughter { get; set; }
        public string CountryOfDeboning { get; set; }
        public string FactoryOfSlaughter { get; set; }
        public string FactoryOfDeboning { get; set; }
        public string UOM { get; set; }
        public string FixedWeight { get; set; }
        public Nullable<decimal> TransactionWeight { get; set; }
        public Nullable<decimal> Tare { get; set; }
        public Nullable<decimal> GrossWeight { get; set; }
        public string KillDate { get; set; }
        public string CaseUID { get; set; }
        public string PalletSerial { get; set; }
        public string UseByDate { get; set; }
        public string PackDate { get; set; }
        public string ProcessBy { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
    }
}

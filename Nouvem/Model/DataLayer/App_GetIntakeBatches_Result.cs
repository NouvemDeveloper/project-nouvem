//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    
    public partial class App_GetIntakeBatches_Result
    {
        public int APGoodsReceiptID { get; set; }
        public int Number { get; set; }
        public int APGoodsReceiptDetailID { get; set; }
        public int BatchNumberID { get; set; }
        public int INMasterID { get; set; }
        public string Supplier { get; set; }
        public string Product { get; set; }
        public string Reference { get; set; }
        public Nullable<decimal> WeightReceived { get; set; }
        public Nullable<decimal> QuantityReceived { get; set; }
        public Nullable<decimal> ProductionWeight { get; set; }
        public Nullable<decimal> ProductionQty { get; set; }
        public string Generic1 { get; set; }
        public string Generic2 { get; set; }
        public string Generic3 { get; set; }
        public string Generic4 { get; set; }
        public string Generic5 { get; set; }
        public string Generic6 { get; set; }
        public string Generic7 { get; set; }
        public string Generic8 { get; set; }
        public int RowNo { get; set; }
        public Nullable<decimal> RemainingWgt { get; set; }
        public Nullable<decimal> RemainingQty { get; set; }
    }
}

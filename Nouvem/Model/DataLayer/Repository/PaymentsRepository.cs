﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer.Interface;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    public class PaymentsRepository : NouvemRepositoryBase, IPaymentsRepository
    {
        /// <summary>
        /// Gets the application payment apply methods.
        /// </summary>
        /// <returns>The application payment apply methods.</returns>
        public IList<NouApplyMethod> GetApplyMethods()
        {
            var methods = new List<NouApplyMethod>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    methods = entities.NouApplyMethods.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return methods;
        }

        /// <summary>
        /// Update the payements pricing matrix.
        /// </summary>
        /// <param name="pricing">The matrix prices.</param>
        public bool UpdatePricingMatrix(StockDetail pricing)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbPricing = entities.PricingMatrices.Where(x => x.Deleted == null);
                    foreach (var pricingMatrix in dbPricing)
                    {
                        pricingMatrix.Deleted = DateTime.Now;
                    }

                    entities.SaveChanges();

                    #region update

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-1-", PriceAdjust = pricing.Attribute1.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-1=", PriceAdjust = pricing.Attribute2.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-1+", PriceAdjust = pricing.Attribute3.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-2-", PriceAdjust = pricing.Attribute4.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-2=", PriceAdjust = pricing.Attribute5.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-2+", PriceAdjust = pricing.Attribute6.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-3-", PriceAdjust = pricing.Attribute7.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-3=", PriceAdjust = pricing.Attribute8.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-3+", PriceAdjust = pricing.Attribute9.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-4-", PriceAdjust = pricing.Attribute10.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-4=", PriceAdjust = pricing.Attribute11.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-4+", PriceAdjust = pricing.Attribute12.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-5-", PriceAdjust = pricing.Attribute13.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-5=", PriceAdjust = pricing.Attribute14.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E-5+", PriceAdjust = pricing.Attribute15.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=1-", PriceAdjust = pricing.Attribute16.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=1=", PriceAdjust = pricing.Attribute17.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=1+", PriceAdjust = pricing.Attribute18.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=2-", PriceAdjust = pricing.Attribute19.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=2=", PriceAdjust = pricing.Attribute20.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=2+", PriceAdjust = pricing.Attribute21.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=3-", PriceAdjust = pricing.Attribute22.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=3=", PriceAdjust = pricing.Attribute23.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=3+", PriceAdjust = pricing.Attribute24.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=4-", PriceAdjust = pricing.Attribute25.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=4=", PriceAdjust = pricing.Attribute26.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=4+", PriceAdjust = pricing.Attribute27.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=5-", PriceAdjust = pricing.Attribute28.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=5=", PriceAdjust = pricing.Attribute29.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E=5+", PriceAdjust = pricing.Attribute30.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+1-", PriceAdjust = pricing.Attribute31.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+1=", PriceAdjust = pricing.Attribute32.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+1+", PriceAdjust = pricing.Attribute33.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+2-", PriceAdjust = pricing.Attribute34.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+2=", PriceAdjust = pricing.Attribute35.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+2+", PriceAdjust = pricing.Attribute36.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+3-", PriceAdjust = pricing.Attribute37.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+3=", PriceAdjust = pricing.Attribute38.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+3+", PriceAdjust = pricing.Attribute39.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+4-", PriceAdjust = pricing.Attribute40.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+4=", PriceAdjust = pricing.Attribute41.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+4+", PriceAdjust = pricing.Attribute42.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+5-", PriceAdjust = pricing.Attribute43.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+5=", PriceAdjust = pricing.Attribute44.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E+5+", PriceAdjust = pricing.Attribute45.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-1-", PriceAdjust = pricing.Attribute46.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-1=", PriceAdjust = pricing.Attribute47.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-1+", PriceAdjust = pricing.Attribute48.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-2-", PriceAdjust = pricing.Attribute49.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-2=", PriceAdjust = pricing.Attribute50.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-2+", PriceAdjust = pricing.Attribute51.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-3-", PriceAdjust = pricing.Attribute52.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-3=", PriceAdjust = pricing.Attribute53.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-3+", PriceAdjust = pricing.Attribute54.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-4-", PriceAdjust = pricing.Attribute55.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-4=", PriceAdjust = pricing.Attribute56.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-4+", PriceAdjust = pricing.Attribute57.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-5-", PriceAdjust = pricing.Attribute58.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-5=", PriceAdjust = pricing.Attribute59.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U-5+", PriceAdjust = pricing.Attribute60.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=1-", PriceAdjust = pricing.Attribute61.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=1=", PriceAdjust = pricing.Attribute62.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=1+", PriceAdjust = pricing.Attribute63.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=2-", PriceAdjust = pricing.Attribute64.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=2=", PriceAdjust = pricing.Attribute65.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=2+", PriceAdjust = pricing.Attribute66.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=3-", PriceAdjust = pricing.Attribute67.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=3=", PriceAdjust = pricing.Attribute68.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=3+", PriceAdjust = pricing.Attribute69.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=4-", PriceAdjust = pricing.Attribute70.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=4=", PriceAdjust = pricing.Attribute71.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=4+", PriceAdjust = pricing.Attribute72.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=5-", PriceAdjust = pricing.Attribute73.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=5=", PriceAdjust = pricing.Attribute74.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U=5+", PriceAdjust = pricing.Attribute75.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+1-", PriceAdjust = pricing.Attribute76.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+1=", PriceAdjust = pricing.Attribute77.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+1+", PriceAdjust = pricing.Attribute78.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+2-", PriceAdjust = pricing.Attribute79.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+2=", PriceAdjust = pricing.Attribute80.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+2+", PriceAdjust = pricing.Attribute81.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+3-", PriceAdjust = pricing.Attribute82.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+3=", PriceAdjust = pricing.Attribute83.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+3+", PriceAdjust = pricing.Attribute84.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+4-", PriceAdjust = pricing.Attribute85.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+4=", PriceAdjust = pricing.Attribute86.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+4+", PriceAdjust = pricing.Attribute87.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+5-", PriceAdjust = pricing.Attribute88.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+5=", PriceAdjust = pricing.Attribute89.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U+5+", PriceAdjust = pricing.Attribute90.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-1-", PriceAdjust = pricing.Attribute91.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-1=", PriceAdjust = pricing.Attribute92.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-1+", PriceAdjust = pricing.Attribute93.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-2-", PriceAdjust = pricing.Attribute94.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-2=", PriceAdjust = pricing.Attribute95.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-2+", PriceAdjust = pricing.Attribute96.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-3-", PriceAdjust = pricing.Attribute97.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-3=", PriceAdjust = pricing.Attribute98.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-3+", PriceAdjust = pricing.Attribute99.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-4-", PriceAdjust = pricing.Attribute100.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-4=", PriceAdjust = pricing.Attribute101.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-4+", PriceAdjust = pricing.Attribute102.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-5-", PriceAdjust = pricing.Attribute103.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-5=", PriceAdjust = pricing.Attribute104.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R-5+", PriceAdjust = pricing.Attribute105.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=1-", PriceAdjust = pricing.Attribute106.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=1=", PriceAdjust = pricing.Attribute107.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=1+", PriceAdjust = pricing.Attribute108.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=2-", PriceAdjust = pricing.Attribute109.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=2=", PriceAdjust = pricing.Attribute110.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=2+", PriceAdjust = pricing.Attribute111.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=3-", PriceAdjust = pricing.Attribute112.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=3=", PriceAdjust = pricing.Attribute113.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=3+", PriceAdjust = pricing.Attribute114.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=4-", PriceAdjust = pricing.Attribute115.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=4=", PriceAdjust = pricing.Attribute116.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=4+", PriceAdjust = pricing.Attribute117.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=5-", PriceAdjust = pricing.Attribute118.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=5=", PriceAdjust = pricing.Attribute119.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R=5+", PriceAdjust = pricing.Attribute120.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+1-", PriceAdjust = pricing.Attribute121.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+1=", PriceAdjust = pricing.Attribute122.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+1+", PriceAdjust = pricing.Attribute123.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+2-", PriceAdjust = pricing.Attribute124.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+2=", PriceAdjust = pricing.Attribute125.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+2+", PriceAdjust = pricing.Attribute126.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+3-", PriceAdjust = pricing.Attribute127.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+3=", PriceAdjust = pricing.Attribute128.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+3+", PriceAdjust = pricing.Attribute129.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+4-", PriceAdjust = pricing.Attribute130.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+4=", PriceAdjust = pricing.Attribute131.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+4+", PriceAdjust = pricing.Attribute132.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+5-", PriceAdjust = pricing.Attribute133.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+5=", PriceAdjust = pricing.Attribute134.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R+5+", PriceAdjust = pricing.Attribute135.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-1-", PriceAdjust = pricing.Attribute136.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-1=", PriceAdjust = pricing.Attribute137.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-1+", PriceAdjust = pricing.Attribute138.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-2-", PriceAdjust = pricing.Attribute139.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-2=", PriceAdjust = pricing.Attribute140.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-2+", PriceAdjust = pricing.Attribute141.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-3-", PriceAdjust = pricing.Attribute142.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-3=", PriceAdjust = pricing.Attribute143.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-3+", PriceAdjust = pricing.Attribute144.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-4-", PriceAdjust = pricing.Attribute145.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-4=", PriceAdjust = pricing.Attribute146.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-4+", PriceAdjust = pricing.Attribute147.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-5-", PriceAdjust = pricing.Attribute148.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-5=", PriceAdjust = pricing.Attribute149.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-5+", PriceAdjust = pricing.Attribute150.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=1-", PriceAdjust = pricing.Attribute151.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=1=", PriceAdjust = pricing.Attribute152.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=1+", PriceAdjust = pricing.Attribute153.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=2-", PriceAdjust = pricing.Attribute154.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=2=", PriceAdjust = pricing.Attribute155.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=2+", PriceAdjust = pricing.Attribute156.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=3-", PriceAdjust = pricing.Attribute157.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=3=", PriceAdjust = pricing.Attribute158.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=3+", PriceAdjust = pricing.Attribute159.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=4-", PriceAdjust = pricing.Attribute160.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=4=", PriceAdjust = pricing.Attribute161.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=4+", PriceAdjust = pricing.Attribute162.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=5-", PriceAdjust = pricing.Attribute163.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=5=", PriceAdjust = pricing.Attribute164.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O=5+", PriceAdjust = pricing.Attribute165.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+1-", PriceAdjust = pricing.Attribute166.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+1=", PriceAdjust = pricing.Attribute167.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+1+", PriceAdjust = pricing.Attribute168.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+2-", PriceAdjust = pricing.Attribute169.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+2=", PriceAdjust = pricing.Attribute170.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+2+", PriceAdjust = pricing.Attribute171.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+3-", PriceAdjust = pricing.Attribute172.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+3=", PriceAdjust = pricing.Attribute173.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+3+", PriceAdjust = pricing.Attribute174.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+4-", PriceAdjust = pricing.Attribute175.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+4=", PriceAdjust = pricing.Attribute176.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+4+", PriceAdjust = pricing.Attribute177.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+5-", PriceAdjust = pricing.Attribute178.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+5=", PriceAdjust = pricing.Attribute179.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+5+", PriceAdjust = pricing.Attribute180.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-1-", PriceAdjust = pricing.Attribute181.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-1=", PriceAdjust = pricing.Attribute182.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-1+", PriceAdjust = pricing.Attribute183.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-2-", PriceAdjust = pricing.Attribute184.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-2=", PriceAdjust = pricing.Attribute185.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-2+", PriceAdjust = pricing.Attribute186.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-3-", PriceAdjust = pricing.Attribute187.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-3=", PriceAdjust = pricing.Attribute188.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-3+", PriceAdjust = pricing.Attribute189.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-4-", PriceAdjust = pricing.Attribute190.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-4=", PriceAdjust = pricing.Attribute191.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-4+", PriceAdjust = pricing.Attribute192.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-5-", PriceAdjust = pricing.Attribute193.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-5=", PriceAdjust = pricing.Attribute194.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-5+", PriceAdjust = pricing.Attribute195.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=1-", PriceAdjust = pricing.Attribute196.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=1=", PriceAdjust = pricing.Attribute197.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=1+", PriceAdjust = pricing.Attribute198.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=2-", PriceAdjust = pricing.Attribute199.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=2=", PriceAdjust = pricing.Attribute200.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=2+", PriceAdjust = pricing.Attribute201.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=3-", PriceAdjust = pricing.Attribute202.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=3=", PriceAdjust = pricing.Attribute203.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=3+", PriceAdjust = pricing.Attribute204.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=4-", PriceAdjust = pricing.Attribute205.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=4=", PriceAdjust = pricing.Attribute206.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=4+", PriceAdjust = pricing.Attribute207.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=5-", PriceAdjust = pricing.Attribute208.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=5=", PriceAdjust = pricing.Attribute209.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P=5+", PriceAdjust = pricing.Attribute210.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+1-", PriceAdjust = pricing.Attribute211.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+1=", PriceAdjust = pricing.Attribute212.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+1+", PriceAdjust = pricing.Attribute213.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+2-", PriceAdjust = pricing.Attribute214.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+2=", PriceAdjust = pricing.Attribute215.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+2+", PriceAdjust = pricing.Attribute216.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+3-", PriceAdjust = pricing.Attribute217.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+3=", PriceAdjust = pricing.Attribute218.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+3+", PriceAdjust = pricing.Attribute219.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+4-", PriceAdjust = pricing.Attribute220.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+4=", PriceAdjust = pricing.Attribute221.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+4+", PriceAdjust = pricing.Attribute222.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+5-", PriceAdjust = pricing.Attribute223.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+5=", PriceAdjust = pricing.Attribute224.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+5+", PriceAdjust = pricing.Attribute225.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E1", PriceAdjust = pricing.Attribute226.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E2", PriceAdjust = pricing.Attribute227.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E3", PriceAdjust = pricing.Attribute228.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E4H", PriceAdjust = pricing.Attribute229.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E4L", PriceAdjust = pricing.Attribute230.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "E5", PriceAdjust = pricing.Attribute231.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U1", PriceAdjust = pricing.Attribute232.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U2", PriceAdjust = pricing.Attribute233.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U3", PriceAdjust = pricing.Attribute234.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U4H", PriceAdjust = pricing.Attribute235.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U4L", PriceAdjust = pricing.Attribute236.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "U5", PriceAdjust = pricing.Attribute237.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R1", PriceAdjust = pricing.Attribute238.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R2", PriceAdjust = pricing.Attribute239.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R3", PriceAdjust = pricing.Attribute240.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R4H", PriceAdjust = pricing.Attribute241.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R4L", PriceAdjust = pricing.Attribute242.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "R5", PriceAdjust = pricing.Attribute243.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O1", PriceAdjust = pricing.Attribute244.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O2", PriceAdjust = pricing.Attribute245.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O3", PriceAdjust = pricing.Attribute246.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O4H", PriceAdjust = pricing.Attribute247.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O4L", PriceAdjust = pricing.Attribute248.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O5", PriceAdjust = pricing.Attribute249.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P1", PriceAdjust = pricing.Attribute250.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P2", PriceAdjust = pricing.Attribute251.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P3", PriceAdjust = pricing.Attribute252.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P4H", PriceAdjust = pricing.Attribute253.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P4L", PriceAdjust = pricing.Attribute254.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P5", PriceAdjust = pricing.Attribute255.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-1", PriceAdjust = pricing.Attribute256.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-2", PriceAdjust = pricing.Attribute257.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-3", PriceAdjust = pricing.Attribute258.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-4H", PriceAdjust = pricing.Attribute259.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-4L", PriceAdjust = pricing.Attribute260.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O-5", PriceAdjust = pricing.Attribute261.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+1", PriceAdjust = pricing.Attribute262.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+2", PriceAdjust = pricing.Attribute263.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+3", PriceAdjust = pricing.Attribute264.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+4H", PriceAdjust = pricing.Attribute265.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+4L", PriceAdjust = pricing.Attribute266.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "O+5", PriceAdjust = pricing.Attribute267.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-1", PriceAdjust = pricing.Attribute268.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-2", PriceAdjust = pricing.Attribute269.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-3", PriceAdjust = pricing.Attribute270.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-4H", PriceAdjust = pricing.Attribute271.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-4L", PriceAdjust = pricing.Attribute272.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P-5", PriceAdjust = pricing.Attribute273.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+1", PriceAdjust = pricing.Attribute274.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+2", PriceAdjust = pricing.Attribute275.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+3", PriceAdjust = pricing.Attribute276.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+4H", PriceAdjust = pricing.Attribute277.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+4L", PriceAdjust = pricing.Attribute278.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });
                    entities.PricingMatrices.Add(new PricingMatrix { Grade = "P+5", PriceAdjust = pricing.Attribute279.ToNullableDecimal(), CreationDate = DateTime.Now, UserMasterID = NouvemGlobal.UserId, DeviceMasterID = NouvemGlobal.DeviceId });

                    #endregion

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Returns the pricing.
        /// </summary>
        /// <returns></returns>
        public IList<PricingMatrix> GetPricingMatrix()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.PricingMatrices.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the application weight bands.
        /// </summary>
        /// <returns>The application bands.</returns>
        public IList<WeightBandData> GetWeightBands()
        {
            var bands = new List<WeightBandData>();
            IEnumerable<WeightBand> dbBands;

            try
            {
                using (var entities = new NouvemEntities())
                {
                     dbBands = entities.WeightBands.Where(x => x.Deleted == null).ToList();
                }

                bands = (from band in dbBands
                    select new WeightBandData
                    {
                        WeightBandId = band.WeightBandID,
                        WeightBandGroupId = band.WeightBandGroupID,
                        Name = band.Name,
                        MinWeight = band.MinWeight,
                        MaxWeight = band.MaxWeight,
                        ApplyCeilingWeight = band.ApplyCeilingWeight
                    }).ToList();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return bands;
        }

        /// <summary>
        /// Gets the application weight band groups.
        /// </summary>
        /// <returns>The application band groups.</returns>
        public IList<WeightBandGroup> GetWeightBandGroups()
        {
            var bands = new List<WeightBandGroup>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    bands = entities.WeightBandGroups.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return bands;
        }

        /// <summary>
        /// Adds or updates weight bands.
        /// </summary>
        /// <param name="bands">The bands to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateWeightBands(IList<WeightBandData> bands)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    bands.ToList().ForEach(x =>
                    {
                        if (x.WeightBandId == 0)
                        {
                            var band = new WeightBand
                            {
                                Name = x.Name,
                                WeightBandGroupID = x.WeightBandGroupId,
                                MinWeight = x.MinWeight,
                                MaxWeight = x.MaxWeight,
                                ApplyCeilingWeight = x.ApplyCeilingWeight,
                                CreationDate = DateTime.Now,
                                DeviceID = NouvemGlobal.DeviceId.ToInt(),
                                UserID = NouvemGlobal.UserId.ToInt()
                            };
                           
                            entities.WeightBands.Add(band);
                        }
                        else
                        {
                            var dbBand =
                                entities.WeightBands.FirstOrDefault(
                                    localBand =>
                                        localBand.WeightBandID == x.WeightBandId);

                            if (dbBand != null)
                            {
                                dbBand.Name = x.Name;
                                dbBand.ApplyCeilingWeight = x.ApplyCeilingWeight;
                                dbBand.MaxWeight = x.MaxWeight;
                                dbBand.MinWeight = x.MinWeight;
                                dbBand.DeviceID = NouvemGlobal.DeviceId.ToInt();
                                dbBand.UserID = NouvemGlobal.UserId.ToInt();
                                dbBand.WeightBandGroupID = x.WeightBandGroupId;
                                dbBand.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Adds or updates weight band groups.
        /// </summary>
        /// <param name="bands">The bands to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateWeightBandGroups(IList<WeightBandGroup> bands)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    bands.ToList().ForEach(x =>
                    {
                        if (x.WeightBandGroupID == 0)
                        {
                            x.CreationDate = DateTime.Now;
                            x.DeviceID = NouvemGlobal.DeviceId.ToInt();
                            x.UserID = NouvemGlobal.UserId.ToInt();
                            entities.WeightBandGroups.Add(x);
                        }
                        else
                        {
                            var dbBand =
                                entities.WeightBandGroups.FirstOrDefault(
                                    localBand =>
                                        localBand.WeightBandGroupID == x.WeightBandGroupID);

                            if (dbBand != null)
                            {
                                dbBand.Name = x.Name;
                                dbBand.DeviceID = NouvemGlobal.DeviceId.ToInt();
                                dbBand.UserID = NouvemGlobal.UserId.ToInt();
                                dbBand.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Gets the payment deductions.
        /// </summary>
        /// <returns>The application payment deductions.</returns>
        public IList<PaymentDeduction> GetPaymentDeductions()
        {
            var deductions = new List<PaymentDeduction>();
            var categories = new List<NouCategory>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    categories = entities.NouCategories.Where(x => x.Deleted == null).ToList();
                    deductions = (from killDud in entities.KillPaymentDeductions
                                  where killDud.Deleted == null
                                  select new PaymentDeduction
                                  {
                                      KillPaymentDeductionID = killDud.KillPaymentDeductionsID,
                                      Name = killDud.Name,
                                      Price = killDud.Price,
                                      Vat = killDud.VATCode,
                                      NouApplyMethod = killDud.NouApplyMethod,
                                      AllowPriceEdit = killDud.AllowPriceEdit == true ? Strings.Yes : Strings.No,
                                      AllowZeroPrice = killDud.AllowZeroPrice == true ? Strings.Yes : Strings.No,
                                      NouApplyMethodID = killDud.NouApplyMethodID,
                                      VatCodeID = killDud.VATCodeID,
                                      CategoryID = killDud.NouCategoryID,
                                      NouKillTypeID = killDud.NouKillTypeID
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var paymentDeduction in deductions)
            {
                paymentDeduction.CategoryName = string.Empty;
                var localCat =
                    categories.FirstOrDefault(x => x.CategoryID == paymentDeduction.CategoryID);
                if (localCat != null)
                {
                    paymentDeduction.CategoryName = localCat.Name;
                }
            }

            return deductions;
        }

        /// <summary>
        /// Adds or updates payment deductions.
        /// </summary>
        /// <returns>Flag, indicating successfull add or update.</returns>
        public bool AddOrUpdatePaymentDeductions(IList<PaymentDeduction> deductions)
        {
           try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var paymentDeduction in deductions)
                    {
                        var dbDeduction =
                            entities.KillPaymentDeductions.FirstOrDefault(
                                x => x.KillPaymentDeductionsID == paymentDeduction.KillPaymentDeductionID);

                        if (dbDeduction == null)
                        {
                            // add
                            var deduction = new KillPaymentDeduction
                            {
                                Name = paymentDeduction.Name,
                                AllowPriceEdit = paymentDeduction.AllowPriceEdit == Strings.Yes,
                                AllowZeroPrice = paymentDeduction.AllowZeroPrice == Strings.Yes,
                                NouApplyMethodID = paymentDeduction.NouApplyMethodID,
                                VATCodeID = paymentDeduction.VatCodeID,
                                Price = paymentDeduction.Price,
                                NouCategoryID = paymentDeduction.CategoryID,
                                NouKillTypeID = paymentDeduction.NouKillTypeID,
                                EditDate = DateTime.Now,
                                DeviceID = NouvemGlobal.DeviceId,
                                UserMasterID = NouvemGlobal.UserId,
                                Deleted = paymentDeduction.Deleted
                            };

                            entities.KillPaymentDeductions.Add(deduction);
                        }
                        else
                        {
                            // update
                            dbDeduction.Name = paymentDeduction.Name;
                            dbDeduction.AllowZeroPrice = paymentDeduction.AllowZeroPrice == Strings.Yes;
                            dbDeduction.AllowPriceEdit = paymentDeduction.AllowPriceEdit == Strings.Yes;
                            dbDeduction.NouApplyMethodID = paymentDeduction.NouApplyMethodID;
                            dbDeduction.VATCodeID = paymentDeduction.VatCodeID;
                            dbDeduction.Price = paymentDeduction.Price;
                            dbDeduction.NouCategoryID = paymentDeduction.CategoryID;
                            dbDeduction.NouKillTypeID = paymentDeduction.NouKillTypeID;
                            dbDeduction.Deleted = paymentDeduction.Deleted;
                            dbDeduction.EditDate = DateTime.Now;
                            dbDeduction.DeviceID = NouvemGlobal.DeviceId;
                            dbDeduction.UserMasterID = NouvemGlobal.UserId;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets a payments base intake ids.
        /// </summary>
        /// <param name="paymentid">The payment ids.</param>
        /// <returns>The base intake ids.</returns>
        public IList<int> GetPaymentConsolidatedIntakes(int paymentid)
        {
            var ids = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    ids = entities.APGoodsReceipts.Where(x => x.Deleted == null && x.ReferenceID == paymentid)
                        .Select(x => x.APGoodsReceiptID).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return ids;
        }

        /// <summary>
        /// Adds a new payment proposal (order and details).
        /// </summary>
        /// <param name="sale">The proposal details.</param>
        /// <returns>The newly created quote id, indicating a successful payment proposal add, or not.</returns>
        public Tuple<int, DocNumber> AddNewPaymentProposal(Sale payment)
        {
            var deductions = payment.PaymentDeductionItems;

            try
            {
                using (var entities = new NouvemEntities())
                {

                    var dbNumber =
                        entities.DocumentNumberings.First(
                            x =>
                                x.NouDocumentName.DocumentName.Equals(Constant.PaymentProposal) &&
                                x.DocumentNumberingType.Name.Equals(Constant.Standard));

                    payment.Number = dbNumber.NextNumber;
                    payment.DocumentNumberingID = dbNumber.DocumentNumberingID;

                    var order = new KillPayment
                    {
                        DocumentNumberingID = payment.DocumentNumberingID,
                        Number = payment.Number,
                        BPContactID = payment.MainContact != null && payment.MainContact.Details != null ? (int?)payment.MainContact.Details.BPContactID : null,
                        BPMasterID_Supplier = payment.BPCustomer != null ? payment.BPCustomer.BPMasterID : payment.Customer != null ? payment.Customer.BPMasterID : (int?)null,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        KillDate = payment.ProposedKillDate,
                        DocumentDate = payment.DocumentDate,
                        CreationDate = payment.CreationDate,
                        SubTotalExVAT = payment.SubTotalExVAT,
                        GrandTotalIncVAT = payment.GrandTotalIncVAT.ToDecimal(),
                        DeductionsIncVAT = payment.DeductionsIncVAT,
                        DeductionsVAT = payment.DeductionsVAT,
                        HaulageCharge = payment.HaulageCharge,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = payment.Remarks,
                        UserMasterID = NouvemGlobal.UserId,
                        BaseDocumentReferenceID = payment.BaseDocumentReferenceID
                    };

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }

                    entities.KillPayments.Add(order);
                    entities.SaveChanges();

                    if (payment.IdsToMarkAsInvoiced != null && payment.IdsToMarkAsInvoiced.Any())
                    {
                        foreach (var i in payment.IdsToMarkAsInvoiced)
                        {
                            var baseDoc = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == i);
                            if (baseDoc != null)
                            {
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                                baseDoc.ReferenceID = order.KillPaymentID;
                                this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", order.BaseDocumentReferenceID));
                            }
                        }
                    }

                    if (deductions != null)
                    {
                        foreach (var paymentDeductionItem in deductions)
                        {
                            var deduction = new KillPaymentDeductionItem
                            {
                                KillPaymentDeductionID = paymentDeductionItem.KillPaymentDeductionID,
                                KillPaymentID = order.KillPaymentID,
                                Price = paymentDeductionItem.Price,
                                SubTotalExclVAT = paymentDeductionItem.SubTotalExclVAT,
                                VAT = paymentDeductionItem.Vat,
                                TotalIncVAT = paymentDeductionItem.TotalIncVAT
                            };

                            entities.KillPaymentDeductionItems.Add(deduction);
                        }
                    }

                    var currentNo = dbNumber.NextNumber;
                    var nextNo = dbNumber.NextNumber + 1;
                    if (nextNo > dbNumber.LastNumber)
                    {
                        // Last number reached, so reset back to first number.
                        nextNo = dbNumber.FirstNumber;
                    }

                    dbNumber.NextNumber = nextNo;
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document number updated");

                    var docNumber =
                    new DocNumber
                    {
                        DocumentNumberingID = dbNumber.DocumentNumberingID,
                        NouDocumentNameID = dbNumber.NouDocumentNameID,
                        DocumentNumberingTypeID = dbNumber.DocumentNumberingTypeID,
                        FirstNumber = dbNumber.FirstNumber,
                        LastNumber = dbNumber.LastNumber,
                        CurrentNumber = currentNo,
                        NextNumber = dbNumber.NextNumber,
                        DocName = dbNumber.NouDocumentName,
                        DocType = dbNumber.DocumentNumberingType
                    };
                   
                    // save the details
                    var saleID = order.KillPaymentID;
                    var intake = payment.SaleDetails.FirstOrDefault();
                    if (intake != null && intake.StockDetails != null && intake.StockDetails.Any())
                    {
                        var intakeItems = intake.StockDetails.Where(x => x.GradingDate != null);
                        foreach (var detail in intakeItems)
                        {
                            var onPayment =
                                entities.KillPaymentItems.Any(
                                    x => x.StockTransactionID == detail.StockTransactionID && x.Deleted == null);

                            if (!onPayment)
                            {
                                var payItem = new KillPaymentItem
                                {
                                    KillPaymentID = saleID,
                                    StockTransactionID = detail.StockTransactionID,
                                    DeviceID = NouvemGlobal.DeviceId,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.KillPaymentItems.Add(payItem);
                            }
                        }
               
                        entities.SaveChanges();
                    }

                    this.Log.LogDebug(this.GetType(), "New payment oproposal successfully created");
                    return Tuple.Create(saleID, docNumber);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, new DocNumber());
        }

        /// <summary>
        /// Removes a carcass from a payment.
        /// </summary>
        /// <param name="stocktransactionId">The carcass transaction id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool RemoveCarcassFromPayment(int stocktransactionId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Removing carcass:{0} from payment", stocktransactionId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var item =
                        entities.KillPaymentItems.FirstOrDefault(
                            x => x.StockTransactionID == stocktransactionId && x.Deleted == null);
                    if (item != null)
                    {
                        item.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("Payment item:{0} marked as deleted", item.KillPaymentItemsID));
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes a carcass from a payment.
        /// </summary>
        /// <param name="itemId">The carcass transaction id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool RemovePaymentDeductionItemFromPayment(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Removing payment deduction item:{0} from payment", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var item =
                        entities.KillPaymentDeductionItems.FirstOrDefault(
                            x => x.KillPaymentDeductionItemID == itemId && x.Deleted == null);
                    if (item != null)
                    {
                        item.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("Payment deduction item:{0} marked as deleted", item.KillPaymentDeductionItemID));
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

       /// <summary>
       /// Updates a payment, adding a cheque number.
       /// </summary>
       /// <param name="paymentId">The cheque number.</param>
       /// <returns>A payment cheque number.</returns>
        public int UpdatePaymentChequeNumber(int paymentId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var payment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == paymentId);
                    if (payment != null)
                    {
                        var lastCheque = entities.KillPayments.Max(x => x.ChequeNumber);
                        payment.ChequeNumber = lastCheque + 1;
                        entities.SaveChanges();
                        return payment.ChequeNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Marks a payment as printed..
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        public bool MarkPaymentAsPrinted(int paymentId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var payment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == paymentId);
                    if (payment != null)
                    {
                        payment.Printed = true;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Checks if a payment as printed..
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        public bool IsPaymentPrinted(int paymentId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var payment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == paymentId);
                    if (payment != null)
                    {
                        return payment.Printed.ToBool();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the grades and categories of the input carcasses.
        /// </summary>
        /// <param name="details">The carcasses to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateCarcasses(IList<StockDetail> details)
        {
            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();

                    foreach (var detail in details)
                    {
                        var command = new SqlCommand("App_EditCarcassCategory", conn);
                        command.CommandType = CommandType.StoredProcedure;
                        var param = new SqlParameter("@AttributeID", SqlDbType.Int);
                        param.Value = detail.AttributeID;

                        var idParam = new SqlParameter("@CategoryID", SqlDbType.Int);
                        idParam.Value = detail.NouCategoryID;

                        var paramGrade = new SqlParameter("@Grade", SqlDbType.VarChar, 10);
                        paramGrade.Value = detail.Grade;
                        command.Parameters.Add(param);
                        command.Parameters.Add(idParam);
                        command.Parameters.Add(paramGrade);
                        command.ExecuteNonQuery();
                    }

                    conn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a payment.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>Flag, as to successfull update or not.</returns>
        public bool UpdatePayment(Sale payment)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdatePayment(): Updating payment No:{0}", payment.SaleID));
            var deductions = payment.PaymentDeductionItems;
            var paymentItems = payment.StockDetails;

            ProgressBar.SetUp(0, paymentItems.Count + deductions.Count);
            ProgressBar.Run();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbDeductions = entities.KillPaymentDeductionItems.Where(x => x.KillPaymentID == payment.SaleID && x.Deleted == null);
                    foreach (var killPaymentDeductionItem in dbDeductions)
                    {
                        killPaymentDeductionItem.Deleted = DateTime.Now;
                    }

                    foreach (var paymentDeductionItem in deductions)
                    {
                        ProgressBar.Run();
                        if (paymentDeductionItem.KillPaymentDeductionItemID == 0)
                        {
                            var deduction = new KillPaymentDeductionItem
                            {
                                KillPaymentDeductionID = paymentDeductionItem.KillPaymentDeductionID,
                                KillPaymentID = payment.SaleID,
                                Price = paymentDeductionItem.Price,
                                SubTotalExclVAT = paymentDeductionItem.SubTotalExclVAT,
                                VAT = paymentDeductionItem.Vat,
                                TotalIncVAT = paymentDeductionItem.TotalIncVAT
                            };

                            entities.KillPaymentDeductionItems.Add(deduction);
                        }
                        else
                        {
                            var dbDeduction =
                                entities.KillPaymentDeductionItems.FirstOrDefault(
                                    x => x.KillPaymentDeductionItemID == paymentDeductionItem.KillPaymentDeductionItemID);
                            if (dbDeduction != null)
                            {
                                dbDeduction.Price = paymentDeductionItem.Price;
                                dbDeduction.SubTotalExclVAT = paymentDeductionItem.SubTotalExclVAT;
                                dbDeduction.VAT = paymentDeductionItem.Vat;
                                dbDeduction.TotalIncVAT = paymentDeductionItem.TotalIncVAT;
                                dbDeduction.Deleted = paymentDeductionItem.Deleted;
                            }
                        }
                    }

                    foreach (var paymentItem in paymentItems)
                    {
                        ProgressBar.Run();
                        if (paymentItem.StockDetailID == 0)
                        {
                            var onPayment =
                               entities.KillPaymentItems.Any(
                                   x => x.StockTransactionID == paymentItem.StockTransactionID && x.Deleted == null);
                            if (!onPayment)
                            {
                                var payItem = new KillPaymentItem
                                {
                                    KillPaymentID = payment.SaleID,
                                    StockTransactionID = paymentItem.StockTransactionID,
                                    OutsideWeightBand = paymentItem.OutsideWeightBand,
                                    WeightBandID = paymentItem.WeightBandId,
                                    PerKiloAdjustment = paymentItem.PriceAdjust,
                                    DeviceID = NouvemGlobal.DeviceId,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                var trans =
                                    entities.StockTransactions.FirstOrDefault(
                                        x => x.StockTransactionID == paymentItem.StockTransactionID);
                                if (trans != null)
                                {
                                    var dbAttribute =
                                            entities.Attributes.FirstOrDefault(
                                                x => x.AttributeID == trans.AttributeID);
                                    if (dbAttribute != null)
                                    {
                                        dbAttribute.RPA = paymentItem.RPA;
                                    }
                                }

                                entities.KillPaymentItems.Add(payItem);
                            }
                        }
                        else
                        {
                            var item =
                            entities.KillPaymentItems.FirstOrDefault(
                                x => x.KillPaymentItemsID == paymentItem.StockDetailID);
                            if (item != null)
                            {
                                item.KillPaymentID = payment.SaleID;
                                item.PaidWeight = paymentItem.PaidWeight;
                                item.UnitPrice = paymentItem.UnitPrice;
                                item.TotalExclVAT = paymentItem.TotalExclVat;
                                item.TotalIncVAT = paymentItem.TotalIncVat;
                                item.VAT = paymentItem.VAT;
                                item.OutsideWeightBand = paymentItem.OutsideWeightBand;
                                item.WeightBandID = paymentItem.WeightBandId;
                                item.Notes = paymentItem.Notes;
                                item.PerKiloAdjustment = paymentItem.PriceAdjust;
                                item.Deleted = paymentItem.Deleted;

                                if (item.StockTransaction != null)
                                {
                                    var dbAttribute =
                                        entities.Attributes.FirstOrDefault(
                                            x => x.AttributeID == item.StockTransaction.AttributeID);
                                    if (dbAttribute != null)
                                    {
                                        dbAttribute.RPA = paymentItem.RPA;
                                    }
                                }
                            }
                        }
                    }

                    var order = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == payment.SaleID);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data corresponding to order id not found");
                        return false;
                    }

                    order.SubTotalExVAT = payment.SubTotalExVAT;
                    order.GrandTotalIncVAT = payment.GrandTotalIncVAT.ToDecimal();
                    order.DeductionsIncVAT = payment.DeductionsIncVAT;
                    order.DeductionsVAT = payment.DeductionsVAT;
                    order.EditDate = DateTime.Now;
                    order.NouDocStatusID = payment.NouDocStatusID;
                    order.Remarks = payment.Remarks;
                    order.Printed = payment.Printed;
                    order.ChequeNumber = payment.ChequeNumber;
                    order.DeviceID = NouvemGlobal.DeviceId;
                    order.UserMasterID = NouvemGlobal.UserId;
                    order.ChequeNumber = payment.ChequeNumber;
                    order.BPMasterID_Supplier = payment.Customer.BPMasterID;

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Cancels a payment.
        /// </summary>
        /// <param name="payment">The payment data.</param>
        /// <returns>Flag, as to successfull cancellation or not.</returns>
        public bool CancelPayment(Sale payment)
        {
            this.Log.LogDebug(this.GetType(), string.Format("CancelPayment(): Cancelling payment No:{0}", payment.SaleID));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    //entities.App_CancelPayment(payment.SaleID, NouvemGlobal.DeviceId, NouvemGlobal.UserId);
                    var dbPayment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == payment.SaleID);
                    if (dbPayment != null)
                    {
                        dbPayment.NouDocStatusID = NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                        dbPayment.DeviceID = NouvemGlobal.DeviceId;
                        dbPayment.UserMasterID = NouvemGlobal.UserId;

                        var items = entities.KillPaymentItems.Where(x => x.KillPaymentID == payment.SaleID);
                        foreach (var killPaymentItem in items)
                        {
                            killPaymentItem.Deleted = DateTime.Now;
                        }

                        var dbDeductions = entities.KillPaymentDeductionItems.Where(x => x.KillPaymentID == payment.SaleID && x.Deleted == null);
                        foreach (var killPaymentDeductionItem in dbDeductions)
                        {
                            killPaymentDeductionItem.Deleted = DateTime.Now;
                        }

                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the input payments.
        /// </summary>
        /// <param name="payments">The payments to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePaymentsOnly(IList<Sale> payments)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var payment in payments)
                    {
                        var dbPayment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == payment.SaleID);
                        if (dbPayment != null)
                        {
                            dbPayment.Reference = payment.Reference;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the input payments.
        /// </summary>
        /// <param name="payments">The payments to update.</param>
        /// <param name="status">The status to update the payments to.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdatePaymentsStatus(IList<Sale> payments, int status)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var payment in payments)
                    {
                        var dbPayment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == payment.SaleID);
                        if (dbPayment != null)
                        {
                            dbPayment.NouDocStatusID = status;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the patment proposals (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetPaymentProposals(bool showAll, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "GetAllLairageIntakes(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.App_GetPayments(start,end,showAll).ToList()
                              select new Sale
                              {
                                  SaleID = order.KillPaymentID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentNumber = order.BaseNumber.ToInt(),
                                  Number = order.Number,
                                  Reference = order.Reference,
                                  PaymentDate = order.PaymentDate,
                                  ProposedKillDate = order.KillDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  ExcludeFromAccounts = order.Exclude ?? false,
                                  DeviceId = order.DeviceID,
                                  Printed = order.Printed,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DeductionsIncVAT = order.DeductionsIncVAT,
                                  DeductionsVAT = order.DeductionsVAT,
                                  EditDate = order.EditDate,
                                  NouDocStatusID = order.NouDocStatusID,
                                  Remarks = order.Remarks,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SalesEmployeeID = order.UserMasterID,
                                  BPCustomer = new BPMaster{BPMasterID = order.BPMasterID, Name = order.Supplier, Code = order.SupplierCode},
                                  Deleted = order.Deleted,
                                  ChequeNumber = order.ChequeNumber,
                                  CarcassesKilled = order.CarcassesKilled,   // order.KillPaymentItems.Count(x => x.Deleted == null),
                                  CarcassesNotKilled = order.CarcassesNotKilled
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the last edit.
        /// </summary>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        public Sale GetPaymentByFirstLast(bool first)
        {
            var payment = new Sale();
          
            try
            {
                using (var entities = new NouvemEntities())
                {
                    KillPayment order = null;
                    if (first)
                    {
                        order = entities.KillPayments.FirstOrDefault();
                    }
                    else
                    {
                        order = entities.KillPayments.OrderByDescending(x => x.KillPaymentID).FirstOrDefault();
                    }

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "payment Data corresponding to order id not found");
                        return null;
                    }

                    int? agentId = null;
                    var intake =
                        entities.APGoodsReceipts.FirstOrDefault(
                            x => x.APGoodsReceiptID == order.BaseDocumentReferenceID);
                    if (intake != null)
                    {
                        agentId = intake.BPMasterID_Agent;
                    }

                    payment = new Sale
                    {
                        SaleID = order.KillPaymentID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        HaulageCharge = order.HaulageCharge,
                        IFALevyApplied = order.BPMaster.IFALevy,
                        Scrapie = order.BPMaster.Scrapie,
                        Insured = order.BPMaster.Insurance,
                        AgentID = agentId,
                        HerdQAS = order.APGoodsReceipt != null ? order.APGoodsReceipt.HerdQAS : false,
                        DeliveryDate = order.APGoodsReceipt != null ? order.APGoodsReceipt.DeliveryDate : null,
                        KillType = order.APGoodsReceipt != null ? order.APGoodsReceipt.KillType : string.Empty,
                        BPSupplier = order.APGoodsReceipt != null ? order.BPMaster : null,
                        PaymentDate = order.PaymentDate,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeviceId = order.DeviceID,
                        Printed = order.Printed,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DeductionsIncVAT = order.DeductionsIncVAT,
                        DeductionsVAT = order.DeductionsVAT,
                        EditDate = order.EditDate,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SalesEmployeeID = order.UserMasterID,
                        BPCustomer = order.BPMaster,
                        Deleted = order.Deleted,
                        ChequeNumber = order.ChequeNumber,
                        CarcassesKilled = order.KillPaymentItems.Count(x => x.Deleted == null),
                        CarcassesNotKilled = 0,
                        PaymentDeductionItems = (from deduction in order.KillPaymentDeductionItems
                                                 where deduction.Deleted == null
                                                 select new PaymentDeductionItem
                                                 {
                                                     LoadingDeductionItem = true,
                                                     KillPaymentDeductionItemID = deduction.KillPaymentDeductionItemID,
                                                     KillPaymentDeductionID = deduction.KillPaymentDeductionID,
                                                     KillPaymentID = deduction.KillPaymentID,
                                                     Price = deduction.Price,
                                                     SubTotalExclVAT = deduction.SubTotalExclVAT,
                                                     TotalIncVAT = deduction.TotalIncVAT,
                                                     Vat = deduction.VAT,
                                                     Deleted = deduction.Deleted
                                                 }).ToList(),
                        StockDetails = (from detail in entities.KillPaymentItems
                                        join stock in entities.StockTransactions on detail.StockTransactionID equals stock.StockTransactionID
                                        join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                        where detail.KillPaymentID == order.KillPaymentID && detail.Deleted == null
                                        select new StockDetail
                                        {
                                            LoadingStockDetail = true,
                                            StockDetailID = detail.KillPaymentItemsID,
                                            StockTransactionID = detail.StockTransactionID,
                                            AttributeID = attribute.AttributeID,
                                            Notes = detail.Notes,
                                            ExtraNotes = detail.ExtraNotes,
                                            Posted = detail.Posted,
                                            RPA = attribute.RPA == true,
                                            Exported = detail.Export,
                                            OutsideWeightBand = detail.OutsideWeightBand,
                                            WeightBandId = detail.WeightBandID,
                                            PriceAdjust = detail.PerKiloAdjustment,
                                            UnitPrice = detail.UnitPrice,
                                            KillType = attribute.KillType,
                                            CarcassNumber = attribute.CarcassNumber,
                                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                            DiscountPercentage = detail.DiscountPercentage,
                                            DiscountAmount = detail.DiscountAmount,
                                            Attribute120 = attribute.Attribute120,
                                            DiscountPrice = detail.DiscountPrice,
                                            LineDiscountAmount = detail.LineDiscountAmount,
                                            LineDiscountPercentage = detail.LineDiscountPercentage,
                                            TotalExclVat = detail.TotalExclVAT ?? attribute.Total,
                                            TotalIncVat = detail.TotalIncVAT,
                                            Detained = attribute.Detained,
                                            Condemned = attribute.Condemned,
                                            ThirdParty = attribute.Attribute199,
                                            VAT = detail.VAT,
                                            VatCodeId = detail.VATCodeID,
                                            Eartag = attribute.Eartag,
                                            Breed = attribute.NouBreed,
                                            HerdNo = attribute.Herd,
                                            WeightSide1 = attribute.Side1Weight ?? 0,
                                            WeightSide2 = attribute.Side2Weight ?? 0,
                                            DOB = attribute.DOB,
                                            AgeInMonths = attribute.AgeInMonths,
                                            AgeInDays = attribute.AgeInDays,
                                            Cleanliness = attribute.NouCleanliness,
                                            NouCategoryID = attribute.NouCategory != null ? attribute.NouCategory.CategoryID : (int?)null,
                                            Category = attribute.NouCategory,
                                            Sex = attribute.Sex,
                                            CustomerID = attribute.Customer,
                                            FarmAssured = attribute.FarmAssured,
                                            FreezerType = attribute.FreezerType,
                                            Clipped = attribute.Clipped,
                                            Casualty = attribute.Casualty,
                                            Lame = attribute.Lame,
                                            NumberOfMoves = attribute.NoOfMoves,
                                            PreviousResidency = attribute.PreviousResidency,
                                            TotalResidency = attribute.TotalResidency,
                                            CurrentResidency = attribute.CurrentResidency,
                                            DateOfLastMove = attribute.DateOfLastMove,
                                            Imported = attribute.Imported,
                                            TransactionWeight = stock.TransactionWeight,
                                            Grade = attribute.Grade,
                                            PaidWeight = detail.PaidWeight,
                                            Deleted = detail.Deleted,
                                            UsingStockPrice = detail.TotalExclVAT == null && attribute.Total != null,
                                        }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return payment;
        }

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the last edit.
        /// </summary>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        public Sale GetPaymentByLastEdit()
        {
            var payment = new Sale();
          
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.KillPayments.Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                                                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "payment Data corresponding to order id not found");
                        return null;
                    }

                    int? agentId = null;
                    var intake =
                        entities.APGoodsReceipts.FirstOrDefault(
                            x => x.APGoodsReceiptID == order.BaseDocumentReferenceID);
                    if (intake != null)
                    {
                        agentId = intake.BPMasterID_Agent;
                    }

                    payment = new Sale
                    {
                        SaleID = order.KillPaymentID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        PaymentDate = order.PaymentDate,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        IFALevyApplied = order.BPMaster.IFALevy,
                        Insured = order.BPMaster.Insurance,
                        Scrapie = order.BPMaster.Scrapie,
                        DeviceId = order.DeviceID,
                        HaulageCharge = order.HaulageCharge,
                        Printed = order.Printed,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        HerdQAS = order.APGoodsReceipt != null ? order.APGoodsReceipt.HerdQAS : false,
                        DeliveryDate = order.APGoodsReceipt != null ? order.APGoodsReceipt.DeliveryDate : null,
                        KillType = order.APGoodsReceipt != null ? order.APGoodsReceipt.KillType : string.Empty,
                        BPSupplier = order.APGoodsReceipt != null ? order.BPMaster : null,
                        SubTotalExVAT = order.SubTotalExVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DeductionsIncVAT = order.DeductionsIncVAT,
                        DeductionsVAT = order.DeductionsVAT,
                        EditDate = order.EditDate,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        ChequeNumber = order.ChequeNumber,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SalesEmployeeID = order.UserMasterID,
                        BPCustomer = order.BPMaster,
                        Deleted = order.Deleted,
                        AgentID = agentId,
                        CarcassesKilled = order.KillPaymentItems.Count(x => x.Deleted == null),
                        CarcassesNotKilled = 0,
                        PaymentDeductionItems = (from deduction in order.KillPaymentDeductionItems
                                                 where deduction.Deleted == null
                                                 select new PaymentDeductionItem
                                                 {
                                                     LoadingDeductionItem = true,
                                                     KillPaymentDeductionItemID = deduction.KillPaymentDeductionItemID,
                                                     KillPaymentDeductionID = deduction.KillPaymentDeductionID,
                                                     KillPaymentID = deduction.KillPaymentID,
                                                     Price = deduction.Price,
                                                     SubTotalExclVAT = deduction.SubTotalExclVAT,
                                                     TotalIncVAT = deduction.TotalIncVAT,
                                                     Vat = deduction.VAT,
                                                     Deleted = deduction.Deleted
                                                 }).ToList(),
                        StockDetails = (from detail in entities.KillPaymentItems
                                        join stock in entities.StockTransactions on detail.StockTransactionID equals stock.StockTransactionID
                                        join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                        where detail.KillPaymentID == order.KillPaymentID && detail.Deleted == null
                                        select new StockDetail
                                        {
                                            LoadingStockDetail = true,
                                            StockDetailID = detail.KillPaymentItemsID,
                                            StockTransactionID = detail.StockTransactionID,
                                            AttributeID = attribute.AttributeID,
                                            Notes = detail.Notes,
                                            RPA = attribute.RPA == true,
                                            ExtraNotes = detail.ExtraNotes,
                                            Posted = detail.Posted,
                                            CarcassNumber = attribute.CarcassNumber,
                                            Exported = detail.Export,
                                            KillType = attribute.KillType,
                                            PriceAdjust = detail.PerKiloAdjustment,
                                            UnitPrice = detail.UnitPrice,
                                            OutsideWeightBand = detail.OutsideWeightBand,
                                            WeightBandId = detail.WeightBandID,
                                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                            DiscountPercentage = detail.DiscountPercentage,
                                            DiscountAmount = detail.DiscountAmount,
                                            DiscountPrice = detail.DiscountPrice,
                                            Attribute120 = attribute.Attribute120,
                                            LineDiscountAmount = detail.LineDiscountAmount,
                                            LineDiscountPercentage = detail.LineDiscountPercentage,
                                            TotalExclVat = detail.TotalExclVAT ?? attribute.Total,
                                            TotalIncVat = detail.TotalIncVAT,
                                            VAT = detail.VAT,
                                            VatCodeId = detail.VATCodeID,
                                            Eartag = attribute.Eartag,
                                            Breed = attribute.NouBreed,
                                            HerdNo = attribute.Herd,
                                            DOB = attribute.DOB,
                                            Detained = attribute.Detained,
                                            Condemned = attribute.Condemned,
                                            ThirdParty = attribute.Attribute199,
                                            AgeInMonths = attribute.AgeInMonths,
                                            AgeInDays = attribute.AgeInDays,
                                            Cleanliness = attribute.NouCleanliness,
                                            Category = attribute.NouCategory,
                                            NouCategoryID = attribute.NouCategory != null ? attribute.NouCategory.CategoryID : (int?)null,
                                            Sex = attribute.Sex,
                                            CustomerID = attribute.Customer,
                                            FarmAssured = attribute.FarmAssured,
                                            FreezerType = attribute.FreezerType,
                                            Clipped = attribute.Clipped,
                                            Casualty = attribute.Casualty,
                                            Lame = attribute.Lame,
                                            WeightSide1 = attribute.Side1Weight ?? 0,
                                            WeightSide2 = attribute.Side2Weight ?? 0,
                                            NumberOfMoves = attribute.NoOfMoves,
                                            PreviousResidency = attribute.PreviousResidency,
                                            TotalResidency = attribute.TotalResidency,
                                            CurrentResidency = attribute.CurrentResidency,
                                            DateOfLastMove = attribute.DateOfLastMove,
                                            Imported = attribute.Imported,
                                            TransactionWeight = stock.TransactionWeight,
                                            Grade = attribute.Grade,
                                            PaidWeight = detail.PaidWeight,
                                            Deleted = detail.Deleted,
                                            UsingStockPrice = detail.TotalExclVAT == null && attribute.Total != null,
                                        }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return payment;
        }

        /// <summary>
        /// Retrieves the kill payment (and associated items) for the input id search term.
        /// </summary>
        /// <param name="paymentId">The order search id.</param>
        /// <returns>A collection of payments (and associated payment details) for the input search term.</returns>
        public Sale GetPaymentById(int paymentId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetPaymentById(): Attempting to get the order receipts for search term:{0}", paymentId));
            var payment = new Sale {SaleDetails = new List<SaleDetail>()};
           
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == paymentId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data corresponding to order id not found");
                        return null;
                    }

                    int? agentId = null;
                    var intake =
                        entities.APGoodsReceipts.FirstOrDefault(
                            x => x.APGoodsReceiptID == order.BaseDocumentReferenceID);
                    if (intake != null)
                    {
                        agentId = intake.BPMasterID_Agent;
                    }

                    payment = new Sale
                    {
                        SaleID = order.KillPaymentID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        PaymentDate = order.PaymentDate,
                        ProposedKillDate = order.KillDate,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        HaulageCharge = order.HaulageCharge,
                        IFALevyApplied = order.BPMaster.IFALevy,
                        Insured = order.BPMaster.Insurance,
                        Scrapie = order.BPMaster.Scrapie,
                        DeviceId = order.DeviceID,
                        Printed = order.Printed,
                        TotalExVAT = order.TotalExVAT,
                        AgentID = agentId,
                        HerdQAS = order.APGoodsReceipt != null ? order.APGoodsReceipt.HerdQAS : false,
                        DeliveryDate = order.APGoodsReceipt != null ? order.APGoodsReceipt.DeliveryDate : null,
                        KillType = order.APGoodsReceipt != null ? order.APGoodsReceipt.KillType : string.Empty,
                        BPSupplier = order.APGoodsReceipt != null ? order.BPMaster : null,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DeductionsIncVAT = order.DeductionsIncVAT,
                        DeductionsVAT = order.DeductionsVAT,
                        EditDate = order.EditDate,
                        NouDocStatusID = order.NouDocStatusID,
                        Remarks = order.Remarks,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SalesEmployeeID = order.UserMasterID,
                        BPCustomer = order.BPMaster,
                        PartnerCurrency = order.BPMaster?.BPCurrency,
                        Deleted = order.Deleted,
                        ChequeNumber = order.ChequeNumber,
                        CarcassesKilled = order.KillPaymentItems.Count(x => x.Deleted == null),
                        CarcassesNotKilled = 0,
                        PaymentDeductionItems = (from deduction in order.KillPaymentDeductionItems
                                                 where deduction.Deleted == null
                                                     select new PaymentDeductionItem
                                                     {
                                                         LoadingDeductionItem = true,
                                                         KillPaymentDeductionItemID = deduction.KillPaymentDeductionItemID,
                                                         KillPaymentDeductionID = deduction.KillPaymentDeductionID,
                                                         KillPaymentID = deduction.KillPaymentID,
                                                         Price = deduction.Price,
                                                         SubTotalExclVAT = deduction.SubTotalExclVAT,
                                                         TotalIncVAT = deduction.TotalIncVAT,
                                                         Vat = deduction.VAT,
                                                         Deleted = deduction.Deleted
                                                     }).ToList(),
                        StockDetails = (from detail in entities.KillPaymentItems
                                        join stock in entities.StockTransactions on detail.StockTransactionID equals stock.StockTransactionID
                                        join attribute in entities.Attributes on stock.AttributeID equals attribute.AttributeID
                                        where detail.KillPaymentID == order.KillPaymentID && detail.Deleted == null
                                       select new StockDetail
                                       {
                                           LoadingStockDetail = true,
                                           StockDetailID = detail.KillPaymentItemsID,
                                           StockTransactionID = detail.StockTransactionID,
                                           Notes = detail.Notes,
                                           AttributeID = attribute.AttributeID,
                                           ExtraNotes = detail.ExtraNotes,
                                           Posted = detail.Posted,
                                           Exported = detail.Export,
                                           PriceAdjust = detail.PerKiloAdjustment,
                                           UnitPrice = detail.UnitPrice ?? attribute.Price,
                                           RPA = attribute.RPA == true,
                                           OutsideWeightBand = detail.OutsideWeightBand,
                                           WeightBandId = detail.WeightBandID,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountAmount = detail.DiscountAmount,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           TotalExclVat = detail.TotalExclVAT ?? attribute.Total,
                                           TotalIncVat = detail.TotalIncVAT,
                                           VAT = detail.VAT,
                                           Detained = attribute.Detained,
                                           Condemned = attribute.Condemned,
                                           ThirdParty = attribute.Attribute199,
                                           VatCodeId = detail.VATCodeID,
                                           Eartag = attribute.Eartag,
                                           Breed = attribute.NouBreed,
                                           CarcassNumber = attribute.CarcassNumber,
                                           HerdNo = attribute.Herd,
                                           DOB = attribute.DOB,
                                           AgeInMonths = attribute.AgeInMonths,
                                           AgeInDays = attribute.AgeInDays,
                                           Cleanliness = attribute.NouCleanliness,
                                           Category = attribute.NouCategory,
                                           WeightSide1 = attribute.Side1Weight ?? 0,
                                           WeightSide2 = attribute.Side2Weight ?? 0,
                                           NouCategoryID = attribute.NouCategory != null ? attribute.NouCategory.CategoryID : (int?)null,
                                           Sex = attribute.Sex,
                                           KillType = attribute.KillType,
                                           CustomerID = attribute.Customer,
                                           FarmAssured = attribute.FarmAssured,
                                           FreezerType = attribute.FreezerType,
                                           Clipped = attribute.Clipped,
                                           Attribute120 = attribute.Attribute120,
                                           Casualty = attribute.Casualty,
                                           Lame = attribute.Lame,
                                           NumberOfMoves = attribute.NoOfMoves,
                                           PreviousResidency = attribute.PreviousResidency,
                                           TotalResidency = attribute.TotalResidency,
                                           CurrentResidency = attribute.CurrentResidency,
                                           DateOfLastMove = attribute.DateOfLastMove,
                                           Imported = attribute.Imported,
                                           TransactionWeight = stock.TransactionWeight,
                                           Grade = attribute.Grade,
                                           PaidWeight = detail.PaidWeight,
                                           UsingStockPrice = detail.TotalExclVAT == null && attribute.Total != null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            payment.SaleDetails = new List<SaleDetail>();
            return payment;
        }

        /// <summary>
        /// Marks the input payments as to be excluded from the pruchase accounts.
        /// </summary>
        /// <param name="payments">The input payments.</param>
        /// <returns>Flag, as to successful marking or not.</returns>
        public bool ExcludePaymentsFromAccounts(IList<Sale> payments)
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var payment in payments)
                    {
                        var dbPayment = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == payment.SaleID);
                        if (dbPayment != null)
                        {
                            dbPayment.Exclude = true;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Exports purchase invoice details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportPurchaseInvoices(IList<int> invoiceIds, bool useLairages)
        {
            this.Log.LogDebug(this.GetType(), "ExportInvoices(): Exporting invoices..");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoiceId in invoiceIds)
                    {
                        if (useLairages)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Attempting to export invoice id:{0}", invoiceId));
                            var invoice = entities.KillPayments.FirstOrDefault(x => x.KillPaymentID == invoiceId);
                            if (invoice != null)
                            {
                                invoice.NouDocStatusID = NouvemGlobal.NouDocStatusExported.NouDocStatusID;
                                invoice.EditDate = DateTime.Now;
                                this.Log.LogDebug(this.GetType(), "Invoice found and marked for export");
                            }
                            else
                            {
                                this.Log.LogError(this.GetType(), "Invoice not found");
                            }
                        }
                        else
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Attempting to export invoice id:{0}", invoiceId));
                            var invoice = entities.APInvoices.FirstOrDefault(x => x.APInvoiceID == invoiceId);
                            if (invoice != null)
                            {
                                invoice.NouDocStatusID = NouvemGlobal.NouDocStatusExported.NouDocStatusID;
                                invoice.EditDate = DateTime.Now;
                                this.Log.LogDebug(this.GetType(), "Invoice found and marked for export");
                            }
                            else
                            {
                                this.Log.LogError(this.GetType(), "Invoice not found");
                            }
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Invoices exported");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}

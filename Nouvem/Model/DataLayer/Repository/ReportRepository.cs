﻿// -----------------------------------------------------------------------
// <copyright file="ReportRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;

    public class ReportRepository : NouvemRepositoryBase, IReportRepository
    {
        /// <summary>
        /// Retrieve the epos sales data falling between the input dates.
        /// </summary>
        /// <returns>A collection of sales data.</returns>
        public IList<ProductData> GetEposSalesData(DateTime startDate, DateTime endDate)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetEposSalesData(): Attempting to retrieve all the epos sales data between {0} and {1}", startDate, endDate));
            var sales = new List<ProductData>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                        sales = (from data in entities.GetEposSalesData(startDate, endDate)
                        select new ProductData
                        {
                            Name = data.Name,
                            Id = data.INMasterID,
                            OpeningStock = data.OpenQty == null ? 0 : (int)data.OpenQty,
                            NewStock = data.NewStock == null ? 0 : (int)data.NewStock,
                            ReturnedStock = data.Returns == null ? 0 : (int)data.Returns,
                            AccountSales = data.AccSales ?? 0,
                            NonAccountSales = data.ActualSales ?? 0,
                            ActualVatAmount = data.CashSalesVat,
                            ActualCCVatAmount = data.CreditCardSalesVat,
                            CashSales = data.CashSales ?? 0,
                            CreditCardSales = data.CCSales ?? 0,
                            AccSalesUnpaid = data.AccSalesUnpaid ?? 0,
                            AccSalesPaid = data.AccSalesPaid ?? 0,
                            AccSalesPaidRevenue = data.AccSalesPaidRevenue,
                            AccSalesUnpaidRevenue = data.AccSalesUnpaidRevenue,
                            Delivery = data.Delivery,
                            Collection = data.Collection,
                            SalesRevenue = data.SalesRevenue,
                            Price = data.BasePrice ?? 0
                        }).ToList();
                  
                    this.Log.LogDebug(this.GetType(), string.Format("{0} sales data items retrieved", sales.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return sales;
        }

        /// <summary>
        /// Gets the modules.  
        /// </summary>
        /// <returns>the user reports.</returns>
        public IList<NouModule> GetModules()
        {
            var reports = new List<NouModule>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    reports = entities.NouModules.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return reports;
        }

       /// <summary>
       /// Updates the report email lookups.
       /// </summary>
       /// <param name="modules">The look ups to add.</param>
       /// <returns>Flag, as to successful operation or not.</returns>
        public bool UpdateReportProcesses(IList<Module> modules)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var lookUps = entities.ReportLookUps.Where(x => x.Deleted == null);
                    foreach (var reportEmailLookUp in lookUps)
                    {
                        reportEmailLookUp.Deleted = DateTime.Now;
                    }

                    entities.SaveChanges();

                    foreach (var module in modules.Where(x => x.ReportID > 0))
                    {
                        var lookUp = new ReportLookUp
                        {
                            BPContactID = module.ContactID,
                            ReportID = module.ReportID,
                            NouModuleID = module.NouModuleID,
                            ShowEmail = module.ShowEmail,
                            UseAtModule = module.UseAtModule
                        };

                        entities.ReportLookUps.Add(lookUp);
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the report emaiul lookups.
        /// </summary>
        /// <returns>The application report email lookups.</returns>
        public IList<ReportEmail> GetReportEmailLookups()
        {
            var lookUps = new List<ReportEmail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    lookUps = (from lookup in entities.ReportLookUps.Where(x => x.Deleted == null)
                        select new ReportEmail
                        {
                            BPContactID = lookup.BPContactID,
                            BPContact = lookup.BPContact,
                            NouModuleID = lookup.NouModuleID,
                            ReportID = lookup.ReportID,
                            ModuleName = lookup.NouModule != null ? lookup.NouModule.Name : string.Empty,
                            ReportName = lookup.Report != null ? lookup.Report.Name : string.Empty,
                            ShowEmail = lookup.ShowEmail,
                            UseAtModule = lookup.UseAtModule
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lookUps;
        }

        /// <summary>
        /// Gets the user reports.  
        /// </summary>
        /// <returns>the user reports.</returns>
        public IList<UserReport> GetUserReports()
        {
            var reports = new List<UserReport>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    reports = entities.UserReports.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return reports;
        }

        /// <summary>
        /// Updates the db reports.
        /// </summary>
        /// <param name="reports">The reports to update.</param>
        /// <returns></returns>
        public bool UpdateNouReports(IList<ReportData> reports)
        {
            try
            {
                var nouReports = new DataTable();
                nouReports.Columns.Add("GenericValue", typeof(String));
             
                foreach (var name in reports.Select(x => x.Name))
                {
                    var workRow = nouReports.NewRow();
                    workRow["GenericValue"] = name.Trim();
                    nouReports.Rows.Add(workRow);
                }

                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var cmd = new SqlCommand("App_UpdateNouReports", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var tvparam = cmd.Parameters.AddWithValue("@dt", nouReports);
                    tvparam.SqlDbType = SqlDbType.Structured;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the intake kill ids between the input dates.
        /// </summary>
        /// <param name="start">Start date.</param>
        /// <param name="end">End date.</param>
        /// <returns>The intake kill ids between the input dates.</returns>
        public IList<int?> GetKillReportIds(DateTime start, DateTime end)
        {
            var ids = new List<int?>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    ids = entities.App_GetBeefKillIdsBetweenDates(start, end).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return ids;
        }

        /// <summary>
        /// Adds or updates report data.
        /// </summary>
        /// <param name="report">The report to add/update.</param>
        /// <returns>Flag, as to successfull add or update.</returns>
        public bool AddOrUpdateReport(ReportData report)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (report.ReportID == 0)
                    {
                        var localReport = new Report
                        {
                            Name = report.Name,
                            Alias = report.Alias,
                            MultiSelect = report.MultiSelect,
                            ViewMultiple = report.ViewMultiple,
                            ShowAllOrders = report.ShowAll,
                            KeepVisible = report.KeepVisible,
                            Refresh = report.Refresh,
                            UseDates = report.ShowDates,
                            UseDateTypes = report.ShowDateTypes,
                            UseSearchID = report.ShowSearchID,
                            CreateFile = report.ShowCreateFile,
                            StoredProcedure = report.ReportMacro,
                            SearchGridMacro = report.SearchGridMacro,
                            EditDate = DateTime.Now
                        };

                        entities.Reports.Add(localReport);
                        entities.SaveChanges();
                        report.ReportID = localReport.ReportID;

                        foreach (var reportReportParam in report.ReportParams)
                        {
                            reportReportParam.ReportID = localReport.ReportID;
                            entities.ReportParams.Add(reportReportParam);
                            entities.SaveChanges();
                        }

                        foreach (var user in report.UserReports)
                        {
                            if (user.UserReportsID > 0)
                            {
                                var dbUserSetting =
                                    entities.UserReports.FirstOrDefault(x => x.UserReportsID == user.UserReportsID);
                                if (dbUserSetting != null)
                                {
                                    dbUserSetting.PrinterName = user.PrinterName;
                                }
                            }
                            else
                            {
                                entities.UserReports.Add(user);
                            }

                            entities.SaveChanges();
                        }

                        foreach (var reportReportDatesTypeId in report.ReportDatesTypeIds)
                        {
                            entities.ReportDateTypeLookUps.Add(new ReportDateTypeLookUp {ReportID = localReport.ReportID, NouSearchDateTypeID = reportReportDatesTypeId});
                        }

                        entities.SaveChanges();
                    }
                    else
                    {
                        var dbReport = entities.Reports.FirstOrDefault(x => x.ReportID == report.ReportID);
                        if (dbReport == null)
                        {
                            return false;
                        }

                        dbReport.Alias = report.Alias;
                        dbReport.MultiSelect = report.MultiSelect;
                        dbReport.ViewMultiple = report.ViewMultiple;
                        dbReport.ShowAllOrders = report.ShowAll;
                        dbReport.KeepVisible = report.KeepVisible;
                        dbReport.Refresh = report.Refresh;
                        dbReport.UseDates = report.ShowDates;
                        dbReport.UseDateTypes = report.ShowDateTypes;
                        dbReport.UseSearchID = report.ShowSearchID;
                        dbReport.StoredProcedure = report.ReportMacro;
                        dbReport.SearchGridMacro = report.SearchGridMacro;
                        dbReport.ReportFolderID = report.ReportFolderID;
                        dbReport.CreateFile = report.ShowCreateFile;
                        dbReport.EditDate = DateTime.Now;

                        var dbParams = entities.ReportParams.Where(
                            x => x.Deleted == null && x.ReportID == report.ReportID);
                        foreach (var reportParam in dbParams)
                        {
                            reportParam.Deleted = DateTime.Now;
                        }

                        foreach (var reportReportParam in report.ReportParams)
                        {
                            reportReportParam.ReportID = report.ReportID;
                            entities.ReportParams.Add(reportReportParam);
                            entities.SaveChanges();
                        }

                        var lookups = entities.ReportDateTypeLookUps.Where(x =>
                            x.Deleted == null && x.ReportID == dbReport.ReportID);
                        foreach (var reportDateTypeLookUp in lookups)
                        {
                            reportDateTypeLookUp.Deleted = DateTime.Now;
                        }

                        entities.SaveChanges();
                        foreach (var reportReportDatesTypeId in report.ReportDatesTypeIds)
                        {
                            entities.ReportDateTypeLookUps.Add(new ReportDateTypeLookUp { ReportID = dbReport.ReportID, NouSearchDateTypeID = reportReportDatesTypeId });
                        }

                        entities.SaveChanges();

                        foreach (var user in report.UserReports)
                        {
                            if (user.UserReportsID > 0)
                            {
                                var dbUserSetting =
                                    entities.UserReports.FirstOrDefault(x => x.UserReportsID == user.UserReportsID);
                                if (dbUserSetting != null)
                                {
                                    dbUserSetting.PrinterName = user.PrinterName;
                                }
                            }
                            else
                            {
                                user.UserMaster = null;
                                entities.UserReports.Add(user);
                            }

                            entities.SaveChanges();
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Adds or updates report data.
        /// </summary>
        /// <param name="reports">The report to add/update.</param>
        /// <returns>Flag, as to successfull add or update.</returns>
        public bool AddOrUpdateReports(IList<ReportData> reports)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var report in reports)
                    {
                        if (report.ReportID == 0)
                        {
                            var localReport = new Report
                            {
                                Name = report.Name,
                                Alias = report.Alias,
                                MultiSelect = report.MultiSelect,
                                ViewMultiple = report.ViewMultiple,
                                ShowAllOrders = report.ShowAll,
                                CreateFile = report.ShowCreateFile,
                                KeepVisible = report.KeepVisible,
                                Refresh = report.Refresh,
                                UseDates = report.ShowDates,
                                UseSearchID = report.ShowSearchID,
                                StoredProcedure = report.ReportMacro,
                                SearchGridMacro = report.SearchGridMacro,
                                ReportFolderID = report.ReportFolderID,
                                EditDate = DateTime.Now
                            };

                            entities.Reports.Add(localReport);
                            entities.SaveChanges();
                        }
                        else
                        {
                            var dbReport = entities.Reports.FirstOrDefault(x => x.ReportID == report.ReportID);
                            if (dbReport == null)
                            {
                                return false;
                            }

                            dbReport.Alias = report.Alias;
                            dbReport.MultiSelect = report.MultiSelect;
                            dbReport.ViewMultiple = report.ViewMultiple;
                            dbReport.ShowAllOrders = report.ShowAll;
                            dbReport.KeepVisible = report.KeepVisible;
                            dbReport.Refresh = report.Refresh;
                            dbReport.UseDates = report.ShowDates;
                            dbReport.UseSearchID = report.ShowSearchID;
                            dbReport.StoredProcedure = report.ReportMacro;
                            dbReport.SearchGridMacro = report.SearchGridMacro;
                            dbReport.ReportFolderID = report.ReportFolderID;
                            dbReport.CreateFile = report.ShowCreateFile;
                            dbReport.EditDate = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the system report data.
        /// </summary>
        /// <returns>The system reports data.</returns>
        public IList< ReportData> GetReports()
        {
            var reports = new List<ReportData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbReports = entities.Reports.Where(x => x.Deleted == null);
                    foreach (var dbReport in dbReports)
                    {
                        var report = new ReportData();
                        report.ReportID = dbReport.ReportID;
                        report.Alias = dbReport.Alias;
                        report.ShowDates = dbReport.UseDates;
                        report.ShowDateTypes = dbReport.UseDateTypes;
                        report.ShowSearchID = dbReport.UseSearchID.ToBool();
                        report.MultiSelect = dbReport.MultiSelect;
                        report.ViewMultiple = dbReport.ViewMultiple.ToBool();
                        report.KeepVisible = dbReport.KeepVisible;
                        report.ShowAll = dbReport.ShowAllOrders;
                        report.Name = dbReport.Name;
                        report.ShowCreateFile = dbReport.CreateFile.ToBool();
                        report.Refresh = dbReport.Refresh;
                        report.CreateFileProcess = dbReport.CreateFileProcess;
                        report.ReportMacro = dbReport.StoredProcedure;
                        report.SearchGridMacro = dbReport.SearchGridMacro;
                        report.ReportFolderID = dbReport.ReportFolderID;
                        report.IsHistoric = dbReport.IsHistoric;
                        report.HistoricParameter = dbReport.HistoricParameter;
                        report.ReportParams = dbReport.ReportParams.Where(x => x.Deleted == null).ToList();
                        report.ReportDateLookUps = entities.ReportDateTypeLookUps
                            .Where(x => x.ReportID == dbReport.ReportID && x.Deleted == null).ToList();
                        report.ReportUserDateType = entities.ReportUserDateTypes.FirstOrDefault(x => x.ReportID == dbReport.ReportID && x.UserID == NouvemGlobal.UserId
                                                                                                                                     && x.Deleted == null);
                        reports.Add(report);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return reports;
        }

        /// <summary>
        /// Saves a report date type agianst a report and user.
        /// </summary>
        /// <param name="reportId">The report to save against.</param>
        /// <param name="dateTypeId">The selected date type.</param>
        /// <param name="userId">The user to save against.</param>
        /// <returns>Flag, as to successful save or not.</returns>
        public bool SaveReportDateType(int reportId,int dateTypeId, int userId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbReport = entities.ReportUserDateTypes.FirstOrDefault(x => x.ReportID == reportId && x.UserID == userId && x.Deleted == null);
                    if (dbReport != null)
                    {
                        dbReport.ReportDateTypeID = dateTypeId;
                    }
                    else
                    {
                        entities.ReportUserDateTypes.Add(new ReportUserDateType {ReportID = reportId, ReportDateTypeID = dateTypeId, UserID = userId});
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the system report data setting for the input report.
        /// </summary>
        /// <returns>The system reports data setting for the input report.</returns>
        public IList<UserReport> GetReportSettings(int reportId)
        {
            var userReports = new List<UserReport>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbReport = entities.Reports.FirstOrDefault(x => x.ReportID == reportId);
                    if (dbReport != null)
                    {
                        userReports = dbReport.UserReports.Where(x => x.Deleted == null && x.UserGroupID == null).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return userReports;
        }

        /// <summary>
        /// Gets the system report data setting for the input report and user.
        /// </summary>
        /// <returns>The system reports data setting for the input report and user.</returns>
        public UserReport GetUserReportSettings(ReportData report, int userId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var userSetting = entities.UserReports.FirstOrDefault(x => x.Deleted == null && x.UserGroupID == null && x.UserMasterID == userId && x.ReportID == report.ReportID);
                    if (userSetting == null)
                    {
                        var newSettings = new UserReport
                        {
                            ReportID = report.ReportID,
                            ReportName = report.Name,
                            UserMasterID = userId
                        };

                        entities.UserReports.Add(newSettings);
                        entities.SaveChanges();
                        return newSettings;
                    }

                    return userSetting;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Save a user report default values.
        /// </summary>
        /// <param name="setting">The default data.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool SaveUserReportSettings(UserReport setting)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbSetting = entities.UserReports.FirstOrDefault(x => x.UserReportsID == setting.UserReportsID);
                    if (dbSetting != null)
                    {
                        dbSetting.ShowAll = setting.ShowAll;
                        dbSetting.KeepVisible = setting.KeepVisible;
                        dbSetting.StartDate = setting.StartDate;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets a report by it's id.
        /// </summary>
        /// <param name="id">The id to search with.</param>
        /// <returns>A system report data.</returns>
        public ReportData GetReportById(int id)
        {
            var report = new ReportData();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbReport = entities.Reports.FirstOrDefault(x => x.ReportID == id);
                    if (dbReport == null)
                    {
                        return report;
                    }

                    report.ReportID = dbReport.ReportID;
                    report.Alias = dbReport.Alias;
                    report.ShowDates = dbReport.UseDates;
                    report.ShowDateTypes = dbReport.UseDateTypes;
                    report.ShowSearchID = dbReport.UseSearchID.ToBool();
                    report.MultiSelect = dbReport.MultiSelect;
                    report.ViewMultiple = dbReport.ViewMultiple.ToBool();
                    report.KeepVisible = dbReport.KeepVisible;
                    report.ShowAll = dbReport.ShowAllOrders;
                    report.ShowCreateFile = dbReport.CreateFile.ToBool();
                    report.CreateFileProcess = dbReport.CreateFileProcess;
                    report.Name = dbReport.Name;
                    report.Refresh = dbReport.Refresh;
                    report.ReportMacro = dbReport.StoredProcedure;
                    report.SearchGridMacro = dbReport.SearchGridMacro;
                    report.ReportParams = dbReport.ReportParams.Where(x => x.Deleted == null).ToList();
                    report.ReportDateLookUps = entities.ReportDateTypeLookUps
                        .Where(x => x.ReportID == dbReport.ReportID && x.Deleted == null).ToList();
                    report.ReportUserDateType = entities.ReportUserDateTypes.FirstOrDefault(x => x.ReportID == dbReport.ReportID && x.UserID == NouvemGlobal.UserId
                                                                                                                                 && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return report;
        }

        /// <summary>
        /// Gets the report folders.
        /// </summary>
        /// <returns></returns>
        public IList<ReportFolder> GetReportFolders()
        {
            var reports = new List<ReportFolder>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    reports = entities.ReportFolders.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return reports;
        }

        /// <summary>
        /// Adds or updates the report folders.
        /// </summary>
        /// <returns>Flag, as to successfull add or update.</returns>
        public bool AddOrUpdateReportFolders(IList<ReportFolder> folders)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var reportFolder in folders)
                    {
                        if (reportFolder.ReportFolderID == 0)
                        {
                            entities.ReportFolders.Add(reportFolder);
                        }
                        else
                        {
                            var folder = entities.ReportFolders.FirstOrDefault(x =>
                                x.ReportFolderID == reportFolder.ReportFolderID);
                            if (folder != null)
                            {
                                folder.Name = reportFolder.Name;
                                folder.ParentID = reportFolder.ParentID;
                                folder.Deleted = reportFolder.Deleted;
                            }
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets a report by first or last entered.
        /// </summary>
        /// <param name="first">First\last flag.</param>
        /// <returns>A system report data.</returns>
        public ReportData GetReportByFirstLast(bool first)
        {
            var report = new ReportData();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    Report dbReport;
                    if (first)
                    {
                        dbReport = entities.Reports.FirstOrDefault();
                    }
                    else
                    {
                        dbReport = entities.Reports.OrderByDescending(x => x.ReportID).FirstOrDefault();
                    }
       
                    if (dbReport == null)
                    {
                        return report;
                    }

                    report.ReportID = dbReport.ReportID;
                    report.Alias = dbReport.Alias;
                    report.ShowDates = dbReport.UseDates;
                    report.ShowDateTypes = dbReport.UseDateTypes;
                    report.ShowSearchID = dbReport.UseSearchID.ToBool();
                    report.MultiSelect = dbReport.MultiSelect;
                    report.ViewMultiple = dbReport.ViewMultiple.ToBool();
                    report.KeepVisible = dbReport.KeepVisible;
                    report.ShowAll = dbReport.ShowAllOrders;
                    report.ShowCreateFile = dbReport.CreateFile.ToBool();
                    report.Name = dbReport.Name;
                    report.CreateFileProcess = dbReport.CreateFileProcess;
                    report.Refresh = dbReport.Refresh;
                    report.ReportMacro = dbReport.StoredProcedure;
                    report.SearchGridMacro = dbReport.SearchGridMacro;
                    report.ReportParams = dbReport.ReportParams.Where(x => x.Deleted == null).ToList();
                    report.ReportDateLookUps = entities.ReportDateTypeLookUps
                        .Where(x => x.ReportID == dbReport.ReportID && x.Deleted == null).ToList();
                    report.ReportUserDateType = entities.ReportUserDateTypes.FirstOrDefault(x => x.ReportID == dbReport.ReportID && x.UserID == NouvemGlobal.UserId
                                                                                                                                 && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return report;
        }

        /// <summary>
        /// Gets a report by last edit.
        /// </summary>
        /// <returns>A system report data.</returns>
        public ReportData GetReportByLastEdit()
        {
            var report = new ReportData();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbReport = entities.Reports.OrderByDescending(x => x.EditDate).FirstOrDefault(); 

                    if (dbReport == null)
                    {
                        return report;
                    }

                    report.ReportID = dbReport.ReportID;
                    report.Alias = dbReport.Alias;
                    report.ShowDates = dbReport.UseDates;
                    report.ShowDateTypes = dbReport.UseDateTypes;
                    report.ShowSearchID = dbReport.UseSearchID.ToBool();
                    report.MultiSelect = dbReport.MultiSelect;
                    report.ViewMultiple = dbReport.ViewMultiple.ToBool();
                    report.KeepVisible = dbReport.KeepVisible;
                    report.ShowCreateFile = dbReport.CreateFile.ToBool();
                    report.ShowAll = dbReport.ShowAllOrders;
                    report.Name = dbReport.Name;
                    report.CreateFileProcess = dbReport.CreateFileProcess;
                    report.Refresh = dbReport.Refresh;
                    report.ReportMacro = dbReport.StoredProcedure;
                    report.SearchGridMacro = dbReport.SearchGridMacro;
                    report.ReportParams = dbReport.ReportParams.Where(x => x.Deleted == null).ToList();
                    report.ReportDateLookUps = entities.ReportDateTypeLookUps
                        .Where(x => x.ReportID == dbReport.ReportID && x.Deleted == null).ToList();
                    report.ReportUserDateType = entities.ReportUserDateTypes.FirstOrDefault(x => x.ReportID == dbReport.ReportID && x.UserID == NouvemGlobal.UserId 
                                                                                                                                 && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return report;
        }

        /// <summary>
        /// Updates the multi kill values for the reports.
        /// </summary>
        /// <param name="values">The values to update.</param>
        public void UpdateMultiKillValueReports(IList<MultiKillReportValue> values)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var valuesToUpdate = entities.MultiKillReportValues.Where(x => x.Deleted == null);
                    foreach (var multiReportValue in valuesToUpdate)
                    {
                        multiReportValue.Deleted = DateTime.Now;
                    }

                    entities.MultiKillReportValues.AddRange(values);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("UpdateMultiKillValueReports(): Beef Kill Ids added:{0}", values.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Updates the multi values for the reports.
        /// </summary>
        /// <param name="values">The values to update.</param>
        public void UpdateMultiValueReports(IList<MultiReportValue> values)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var valuesToUpdate = entities.MultiReportValues.Where(x => x.Deleted == null);
                    foreach (var multiReportValue in valuesToUpdate)
                    {
                        multiReportValue.Deleted = DateTime.Now;
                    }

                    entities.MultiReportValues.AddRange(values);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

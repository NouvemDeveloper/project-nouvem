﻿// -----------------------------------------------------------------------
// <copyright file="RepositoryBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Text;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;

    public class LabelRepository : NouvemRepositoryBase, ILabelRepository
    {
        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<Label> GetLabels()
        {
            this.Log.LogDebug(this.GetType(), "GetLabels(): Attempting to retrieve all the labels");
            var labels = new List<Label>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    labels = entities.Labels.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} labels retrieved", labels.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labels;
        }

        /// <summary>
        /// Retrieve all the label look ups.
        /// </summary>
        /// <returns>A collection of label look ups.</returns>
        public IList<ExternalLabelDataLookUp> GetExternalLabelDataLookUps()
        {
            this.Log.LogDebug(this.GetType(), "GetLabels(): Attempting to retrieve all the external label look ups");
            var labels = new List<ExternalLabelDataLookUp>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    labels = entities.ExternalLabelDataLookUps.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} label look ups retrieved", labels.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labels;
        }

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        public IList<Gs1AIMaster> GetGs1Master()
        {
            this.Log.LogDebug(this.GetType(), "GetGs1Master: Attempting to retrieve all the gs1 masters");
            var lists = new List<Gs1AIMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbLists = entities.Gs1AIMaster.Where(x => !x.Deleted);
                    this.Log.LogDebug(this.GetType(), string.Format("{0} lists retrieved", dbLists.Count()));

                    dbLists.ToList().ForEach(x => lists.Add(x));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lists;
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletCardData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelPalletCard", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletCardDataMultiBatch(int stockTransactionId, decimal wgt, decimal qty, int batchNumberId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelPalletCardMultiBatch", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    command.Parameters.Add(new SqlParameter("@Wgt", wgt));
                    command.Parameters.Add(new SqlParameter("@Qty", qty));
                    command.Parameters.Add(new SqlParameter("@BatchNumberID", batchNumberId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        public IList<ViewGs1Master> GetGs1MasterView()
        {
            this.Log.LogDebug(this.GetType(), "GetGs1MasterView: Attempting to retrieve all the gs1 masters");
            var lists = new List<ViewGs1Master>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    lists = entities.ViewGs1Master.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} gs1 masters retrieved", lists.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lists;
        }

        /// <summary>
        /// Method that makes a repository call to add or update a gs1 master.
        /// </summary>
        /// <param name="gs1Masters">The gs1 masters to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateGs1Masters(IList<Gs1AIMaster> gs1Masters)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateGs1Masters(): Attempting to update the gsi masters");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    gs1Masters.ToList().ForEach(x =>
                    {
                        if (x.GS1AIMasterID == 0)
                        {
                            // new
                            var newGs1 = new Gs1AIMaster
                            {
                                AI = x.AI,
                                OfficialDescription = x.OfficialDescription,
                                DataLength = x.DataLength,
                                RelatedDescription = x.RelatedDescription,
                                IsFixed = x.IsFixed,
                                Deleted = false
                            };

                            entities.Gs1AIMaster.Add(newGs1);
                        }
                        else
                        {
                            // update
                            var dbGs1 =
                                entities.Gs1AIMaster.FirstOrDefault(
                                    gs1 => gs1.GS1AIMasterID == x.GS1AIMasterID && !gs1.Deleted);

                            if (dbGs1 != null)
                            {
                                dbGs1.AI = x.AI;
                                dbGs1.OfficialDescription = x.OfficialDescription;
                                dbGs1.RelatedDescription = x.RelatedDescription;
                                dbGs1.DataLength = x.DataLength;
                                dbGs1.IsFixed = x.IsFixed;
                                dbGs1.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Gs1 masters successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Gets the test data source data corresponding to the input id.
        /// </summary>
        /// <param name="dataId">The test data record to retrieve.</param>
        /// <returns>A test data source record.</returns>
        /// <remarks>TODO Remove</remarks>
        public IEnumerable<TestDataSourceInventory_Result> GetLabelData(int dataId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.TestDataSourceInventory(dataId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetPalletLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelPallet", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetStockMovementData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelStockMovement", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetIntoProductionLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelIntoProd", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the intake label data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetIntakeLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();
           
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelIntake", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the out of production data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetOutOfProductionLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelOutOfProd", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the grader data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetGraderLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelGrader", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the sequencer data schema.
        /// </summary>
        /// <param name="atributeId">The stock transaction id to pass in.</param>
        public DataTable GetSequencerLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelSequencer", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the dispatch data schema.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        public DataTable GetDispatchLabelData(int stockTransactionId)
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelDispatch", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StockTransactionID", stockTransactionId));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the label associations.
        /// </summary>
        /// <returns>A collection of label associations.</returns>
        public IList<LabelsAssociation> GetLabelAssociations()
        {
            this.Log.LogDebug(this.GetType(), "GetLabelAssociations(): Attempting to retrieve the label associations");
            var labAssoc = new List<LabelsAssociation>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var labelAssociations = entities.LabelAssociations.Where(x => x.Deleted == null);

                    foreach (var assoc in labelAssociations)
                    {
                        var localAssoc = new LabelsAssociation
                        {
                            LabelAssociationID = assoc.LabelAssociationID,
                            UsingCustomerGroup = assoc.UsingCustomerGroup,
                            UsingProductGroup = assoc.UsingProductGroup,
                            PieceLabelQty = assoc.PieceLabelQty ?? 1,
                            ItemLabelQty = assoc.ItemLabelQty ?? 1,
                            BoxLabelQty = assoc.BoxLabelQty ?? 1,
                            WarehouseID = assoc.WarehouseID,
                            PalletLabelQty = assoc.PalletLabelQty ?? 1,
                            ShippingLabelQty = assoc.ShippingLabelQty ?? 1,
                            PrinterIDItem = assoc.PrinterID_Item,
                            PrinterIDBox = assoc.PrinterID_Box,
                            PrinterIDPallet = assoc.PrinterID_Pallet,
                            PrinterIDShipping = assoc.PrinterID_Shipping,
                            PrinterIDPiece = assoc.PrinterID_Piece,
                            PrinterItem = assoc.PrinterItem,
                            PrinterBox = assoc.PrinterBox,
                            PrinterPallet = assoc.PrinterPallet,
                            PrinterShipping = assoc.PrinterShipping,
                            PrinterPiece = assoc.PrinterPiece,
                            BPMaster = assoc.BPMaster,
                            INMaster = assoc.INMaster,
                            BPMasterID = assoc.BPMasterID != null ? assoc.BPMasterID : assoc.UsingCustomerGroup != true ? -1 : -2,
                            IsCustomerEnabled = assoc.BPMasterID != null || assoc.UsingCustomerGroup != true,
                            BPGroupID = assoc.BPGroupID != null ? assoc.BPGroupID : assoc.UsingCustomerGroup == true ? -1 : -2,
                            IsCustomerGroupEnabled = assoc.BPGroupID != null || assoc.UsingCustomerGroup == true,
                            INMasterID = assoc.INMasterID != null ? assoc.INMasterID : assoc.UsingProductGroup != true ? -1 : -2,
                            IsProductEnabled = assoc.INMasterID != null || assoc.UsingProductGroup != true,
                            INGroupID = assoc.INGroupID != null ? assoc.INGroupID : assoc.UsingProductGroup == true ? -1 : -2,
                            IsProductGroupEnabled = assoc.INGroupID != null || assoc.UsingProductGroup == true,
                            LabelProcesses = entities.LabelProcesses.Where(x => x.Deleted == null && x.LabelAssociationID == assoc.LabelAssociationID).ToList()
                        };

                        foreach (var process in localAssoc.LabelProcesses)
                        {
                            localAssoc.LabelProcessesForCombo.Add(process.ProcessID.ToInt());
                        }

                        if (assoc.LabelAssociationLabels != null)
                        {
                            var boxLabels = assoc.LabelAssociationLabels.Where(x => x.BoxLabel == true && x.Deleted == null);
                            foreach (var localLabel in boxLabels)
                            {
                                localAssoc.BoxLabels.Add(localLabel.Label);
                                localAssoc.BoxLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var pieceLabels = assoc.LabelAssociationLabels.Where(x => x.PieceLabel == true && x.Deleted == null);
                            foreach (var localLabel in pieceLabels)
                            {
                                localAssoc.PieceLabels.Add(localLabel.Label);
                                localAssoc.PieceLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var itemLabels = assoc.LabelAssociationLabels.Where(x => x.ItemLabel == true && x.Deleted == null);
                            foreach (var localLabel in itemLabels)
                            {
                                localAssoc.ItemLabels.Add(localLabel.Label);
                                localAssoc.ItemLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var palletLabels = assoc.LabelAssociationLabels.Where(x => x.PalletLabel == true && x.Deleted == null);
                            foreach (var localLabel in palletLabels)
                            {
                                localAssoc.PalletLabels.Add(localLabel.Label);
                                localAssoc.PalletLabelsForCombo.Add(localLabel.Label.LabelID);
                            }

                            var shippingLabels = assoc.LabelAssociationLabels.Where(x => x.ShippingLabel == true && x.Deleted == null);
                            foreach (var localLabel in shippingLabels)
                            {
                                localAssoc.ShippingLabels.Add(localLabel.Label);
                                localAssoc.ShippingLabelsForCombo.Add(localLabel.Label.LabelID);
                            }
                        }

                        labAssoc.Add(localAssoc);
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} label associations retrieved", labAssoc.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labAssoc;
        }

        /// <summary>
        /// Method that makes a repository call to add or update the label associations.
        /// </summary>
        /// <param name="labelAssociations">The label associations to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateLabelAssociations(IList<LabelsAssociation> labelAssociations)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateLabelAssociations(): Attempting to update the label associations");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    labelAssociations.ToList().ForEach(x =>
                    {
                        if (x.LabelAssociationID == 0)
                        {
                            // new
                            var labAsssoc = new LabelAssociation
                            {
                                BPGroupID = x.BPGroupID,
                                BPMasterID = x.BPMasterID,
                                INGroupID = x.INGroupID,
                                INMasterID = x.INMasterID,
                                PieceLabelQty = x.PieceLabelQty,
                                ItemLabelQty = x.ItemLabelQty,
                                BoxLabelQty = x.BoxLabelQty,
                                WarehouseID = x.WarehouseID,
                                PalletLabelQty = x.PalletLabelQty,
                                ShippingLabelQty = x.ShippingLabelQty,
                                UsingCustomerGroup = x.IsCustomerGroupEnabled,
                                UsingProductGroup = x.IsProductGroupEnabled,
                                PrinterID_Item = x.PrinterIDItem,
                                PrinterID_Box = x.PrinterIDBox,
                                PrinterID_Pallet = x.PrinterIDPallet,
                                PrinterID_Shipping = x.PrinterIDShipping,
                                PrinterID_Piece = x.PrinterIDPiece
                            };

                            entities.LabelAssociations.Add(labAsssoc);
                            entities.SaveChanges();

                            foreach (var label in x.LabelProcesses)
                            {
                                var labelAssoclabel = new LabelProcess
                                {
                                    LabelAssociationID = labAsssoc.LabelAssociationID,
                                    ProcessID = label.ProcessID
                                };

                                entities.LabelProcesses.Add(labelAssoclabel);
                            }

                            foreach (var label in x.PieceLabels)
                            {
                                var labelAssoclabel = new LabelAssociationLabel
                                {
                                    LabelID = label.LabelID,
                                    LabelAssociationID = labAsssoc.LabelAssociationID,
                                    PieceLabel = true,
                                    CreationDate = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.LabelAssociationLabels.Add(labelAssoclabel);
                            }

                            foreach (var label in x.ItemLabels)
                            {
                                var labelAssoclabel = new LabelAssociationLabel
                                {
                                    LabelID = label.LabelID,
                                    LabelAssociationID = labAsssoc.LabelAssociationID,
                                    ItemLabel = true,
                                    CreationDate = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.LabelAssociationLabels.Add(labelAssoclabel);
                            }

                            foreach (var label in x.BoxLabels)
                            {
                                var labelAssoclabel = new LabelAssociationLabel
                                {
                                    LabelID = label.LabelID,
                                    LabelAssociationID = labAsssoc.LabelAssociationID,
                                    BoxLabel = true,
                                    CreationDate = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.LabelAssociationLabels.Add(labelAssoclabel);
                            }

                            foreach (var label in x.PalletLabels)
                            {
                                var labelAssoclabel = new LabelAssociationLabel
                                {
                                    LabelID = label.LabelID,
                                    LabelAssociationID = labAsssoc.LabelAssociationID,
                                    PalletLabel = true,
                                    CreationDate = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.LabelAssociationLabels.Add(labelAssoclabel);
                            }

                            foreach (var label in x.ShippingLabels)
                            {
                                var labelAssoclabel = new LabelAssociationLabel
                                {
                                    LabelID = label.LabelID,
                                    LabelAssociationID = labAsssoc.LabelAssociationID,
                                    ShippingLabel = true,
                                    CreationDate = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.LabelAssociationLabels.Add(labelAssoclabel);
                            }
                        }
                        else
                        {
                            // update
                            var assoc =
                                entities.LabelAssociations.FirstOrDefault(
                                    la => la.LabelAssociationID == x.LabelAssociationID && la.Deleted == null);

                            if (assoc != null)
                            {
                                assoc.BPGroupID = x.BPGroupID;
                                assoc.INGroupID = x.INGroupID;
                                assoc.BPMasterID = x.BPMasterID;
                                assoc.INMasterID = x.INMasterID;
                                assoc.ItemLabelQty = x.ItemLabelQty;
                                assoc.BoxLabelQty = x.BoxLabelQty;
                                assoc.PieceLabelQty = x.PieceLabelQty;
                                assoc.PalletLabelQty = x.PalletLabelQty;
                                assoc.ShippingLabelQty = x.ShippingLabelQty;
                                assoc.UsingProductGroup = x.IsProductGroupEnabled;
                                assoc.UsingCustomerGroup = x.IsCustomerGroupEnabled;
                                assoc.PrinterID_Item = x.PrinterIDItem;
                                assoc.WarehouseID = x.WarehouseID;
                                assoc.PrinterID_Box = x.PrinterIDBox;
                                assoc.PrinterID_Pallet = x.PrinterIDPallet;
                                assoc.PrinterID_Shipping = x.PrinterIDShipping;
                                assoc.PrinterID_Piece = x.PrinterIDPiece;
                                assoc.Deleted = x.Deleted;

                                var labelsProcessesToDelete =
                                    entities.LabelProcesses.Where(
                                        lab => lab.LabelAssociationID == assoc.LabelAssociationID).ToList();

                                labelsProcessesToDelete.ForEach(labToDelete => labToDelete.Deleted = DateTime.Now);

                                foreach (var label in x.LabelProcesses)
                                {
                                    var labelAssoclabel = new LabelProcess
                                    {
                                        LabelAssociationID = assoc.LabelAssociationID,
                                        ProcessID = label.ProcessID
                                    };

                                    entities.LabelProcesses.Add(labelAssoclabel);
                                }

                                var labelsAssocLabelsToDelete =
                                    entities.LabelAssociationLabels.Where(
                                        lab => lab.LabelAssociationID == assoc.LabelAssociationID).ToList();

                                labelsAssocLabelsToDelete.ForEach(labToDelete => labToDelete.Deleted = DateTime.Now);

                                foreach (var label in x.PieceLabels)
                                {
                                    var labelAssoclabel = new LabelAssociationLabel
                                    {
                                        LabelID = label.LabelID,
                                        LabelAssociationID = assoc.LabelAssociationID,
                                        PieceLabel = true,
                                        CreationDate = DateTime.Now,
                                        UserMasterID = NouvemGlobal.UserId
                                    };

                                    entities.LabelAssociationLabels.Add(labelAssoclabel);
                                }

                                foreach (var label in x.ItemLabels)
                                {
                                    var labelAssoclabel = new LabelAssociationLabel
                                    {
                                        LabelID = label.LabelID,
                                        LabelAssociationID = assoc.LabelAssociationID,
                                        ItemLabel = true,
                                        CreationDate = DateTime.Now,
                                        UserMasterID = NouvemGlobal.UserId
                                    };

                                    entities.LabelAssociationLabels.Add(labelAssoclabel);
                                }

                                foreach (var label in x.BoxLabels)
                                {
                                    var labelAssoclabel = new LabelAssociationLabel
                                    {
                                        LabelID = label.LabelID,
                                        LabelAssociationID = assoc.LabelAssociationID,
                                        BoxLabel = true,
                                        CreationDate = DateTime.Now,
                                        UserMasterID = NouvemGlobal.UserId
                                    };

                                    entities.LabelAssociationLabels.Add(labelAssoclabel);
                                }

                                foreach (var label in x.PalletLabels)
                                {
                                    var labelAssoclabel = new LabelAssociationLabel
                                    {
                                        LabelID = label.LabelID,
                                        LabelAssociationID = assoc.LabelAssociationID,
                                        PalletLabel = true,
                                        CreationDate = DateTime.Now,
                                        UserMasterID = NouvemGlobal.UserId
                                    };

                                    entities.LabelAssociationLabels.Add(labelAssoclabel);
                                }

                                foreach (var label in x.ShippingLabels)
                                {
                                    var labelAssoclabel = new LabelAssociationLabel
                                    {
                                        LabelID = label.LabelID,
                                        LabelAssociationID = assoc.LabelAssociationID,
                                        ShippingLabel = true,
                                        CreationDate = DateTime.Now,
                                        UserMasterID = NouvemGlobal.UserId
                                    };

                                    entities.LabelAssociationLabels.Add(labelAssoclabel);
                                }
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Label associations successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<BarcodeParse> GetBarcodeParses()
        {
            this.Log.LogDebug(this.GetType(), "Getparses(): Attempting to retrieve all the labels");
            var labels = new List<BarcodeParse>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    labels = entities.BarcodeParses.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} labels retrieved", labels.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labels;
        }

        /// <summary>
        /// Gets the application printers.
        /// </summary>
        /// <returns>The application printers.</returns>
        public IList<Printer> GetPrinters()
        {
            this.Log.LogDebug(this.GetType(), "Retrieving printers");
            var printers = new List<Printer>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    printers = entities.Printers.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} printers retrieved", printers.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return printers;
        }
    }
}

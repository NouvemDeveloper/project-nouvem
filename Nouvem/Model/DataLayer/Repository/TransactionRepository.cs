﻿// -----------------------------------------------------------------------
// <copyright file="TransactionRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
using Nouvem.BusinessLogic;
using Nouvem.ViewModel;
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class TransactionRepository : NouvemRepositoryBase, ITransactionRepository
    {

        /// <summary>
        /// Adds dispatch transactions to a pallet.
        /// </summary>
        /// <param name="ids">The dispatch transactions.</param>
        /// <param name="palletId">The pallet id.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool UpdateDispatchPalletTransactions(HashSet<int> ids, int palletId)
        {
            var detail = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var id in ids)
                    {
                        var trans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == id);
                        if (trans != null)
                        {
                            trans.StockTransactionID_Pallet = palletId;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <param name="deviceId">The device id to search transactions.</param>
        /// <param name="addToStock">The add to stock flag.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<StockTransactionData> GetTransactions(int transactionsToTake, int deviceId, bool addToStock = true)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the last {0} transactions", transactionsToTake));
            var transactions = new List<StockTransactionData>();
            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                var localTrans = entities.StockTransactions.Where(transaction => transaction.Deleted == null
                                                                                 &&
                                                                                 transaction.DeviceMasterID == deviceId
                                                                                 &&
                                                                                 transaction.NouTransactionType
                                                                                     .AddToStock == addToStock)
                    .OrderByDescending(x => x.StockTransactionID).Take(transactionsToTake).ToList();

                transactions = (from transaction in localTrans
                                    //where transaction.Deleted == null && transaction.DeviceMasterID == deviceId && transaction.NouTransactionType.AddToStock == addToStock
                                    //orderby transaction.StockTransactionID descending 
                                select new StockTransactionData
                                {
                                    Transaction = transaction,
                                    //TransactionTraceabilities = transaction.TransactionTraceabilities.ToList(),
                                    INMasterId = transaction.INMasterID,
                                    Product = transaction.INMaster != null ? transaction.INMaster.Name : string.Empty,
                                    INGroupId = transaction.INMaster != null ? transaction.INMaster.INGroupID : (int?)null
                                })
                                //.Take(transactionsToTake)
                                .ToList();

                if (ApplicationSettings.Customer == Customer.TSBloor)
                {
                    foreach (var stockTransactionData in transactions)
                    {
                        var batchid = stockTransactionData.Transaction.BatchNumberID;
                        if (batchid != null)
                        {
                            var prOrder =
                                entities.PROrders.FirstOrDefault(x => x.Deleted == null && x.BatchNumberID == batchid);
                            if (prOrder != null)
                            {
                                stockTransactionData.Reference = prOrder.Reference;
                            }
                        }
                    }
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} labels retrieved", transactions.Count));
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactions;
        }

        /// <summary>
        /// Adds a pallet transaction to goods in.
        /// </summary>
        /// <param name="goodsInId">The receipt detail id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public int CreatePalletTransaction(int goodsInId)
        {
            var palletTransId = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var intakeTrans =
                        entities.StockTransactions.Where(x => x.MasterTableID == goodsInId && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && x.Deleted == null);

                    var transactionData = intakeTrans.FirstOrDefault();

                    if (transactionData != null)
                    {
                        var trans = new StockTransaction
                        {
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            UserMasterID = NouvemGlobal.UserId,
                            TransactionDate = DateTime.Now,
                            Comments = Constant.Pallet,
                            INMasterID = transactionData.INMasterID,
                            WarehouseID = transactionData.WarehouseID
                        };

                        entities.StockTransactions.Add(trans);
                        entities.SaveChanges();
                        palletTransId = trans.StockTransactionID;

                        foreach (var localTrans in transactionData.TransactionTraceabilities)
                        {
                            var boxTrans = this.CreateDeepCopyTransactionTraceability(localTrans);
                            boxTrans.StockTransactionID = trans.StockTransactionID;
                            entities.TransactionTraceabilities.Add(boxTrans);
                        }

                        trans.BatchNumberID = transactionData.BatchNumberID;
                        foreach (var stockTransaction in intakeTrans)
                        {
                            stockTransaction.StockTransactionID_Pallet = palletTransId;
                        }

                        entities.SaveChanges();
                        return palletTransId;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Gets an out of butchery transaction data.
        /// </summary>
        /// <param name="serial">The serial number.</param>
        /// <returns>A stoc detail object containing out of butchery data.</returns>
        public StockDetail GetButcheryTransactionData(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == serial);
                    if (trans == null)
                    {
                        return null;
                    }

                    var butcheryTrans = entities.StockTransactions.FirstOrDefault(x => x.MasterTableID == trans.MasterTableID
                                                                                       && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId && x.Deleted == null && x.Consumed == null);

                    if (butcheryTrans != null)
                    {
                        butcheryTrans.Consumed = DateTime.Now;
                        entities.SaveChanges();

                        return new StockDetail
                        {
                            TransactionWeight = butcheryTrans.TransactionWeight,
                            TransactionQty = butcheryTrans.TransactionQTY,
                            INMasterID = butcheryTrans.INMasterID
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionIncProduct(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transaction =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.Serial == serial);

                    if (transaction != null)
                    {
                        transaction.INMasterID = 0;
                        var product =
                            entities.StockTransactions.FirstOrDefault(
                                x => x.MasterTableID == transaction.MasterTableID && x.Deleted == null
                                                                                  && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId);
                        if (product != null)
                        {
                            // get the product id and product group id (use the StoredLabelID column to hold it)
                            transaction.INMasterID = product.INMasterID;
                            transaction.StoredLabelID = product.INMaster.INGroupID;
                        }

                        return transaction;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the transactions for the goods in line.
        /// </summary>
        /// <param name="apGoodsReceiptId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        public IList<StockTransactionData> GetTransactionsForIntakeLine(int apGoodsReceiptId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the transactions for line id:{0}", apGoodsReceiptId));
            var transactions = new List<StockTransactionData>();
            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                transactions = (from transaction in entities.StockTransactions
                                where transaction.Deleted == null
                                && transaction.MasterTableID == apGoodsReceiptId
                                && transaction.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                orderby transaction.StockTransactionID descending
                                select new StockTransactionData
                                {
                                    Transaction = transaction,
                                    TransactionTraceabilities = transaction.TransactionTraceabilities.ToList(),
                                    INMasterId = transaction.INMasterID,
                                    Product = transaction.INMaster != null ? transaction.INMaster.Name : string.Empty,
                                    INGroupId = transaction.INMaster != null ? transaction.INMaster.INGroupID : (int?)null
                                })
                                .ToList();

                if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                {
                    foreach (var stockTransactionData in transactions)
                    {
                        var batchid = stockTransactionData.Transaction.BatchNumberID;
                        if (batchid != null)
                        {
                            var prOrder =
                                entities.PROrders.FirstOrDefault(x => x.Deleted == null && x.BatchNumberID == batchid);
                            if (prOrder != null)
                            {
                                stockTransactionData.Reference = prOrder.Reference;
                            }
                        }
                    }
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} labels retrieved", transactions.Count));
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactions;
        }

        /// <summary>
        /// Retrieves the transactions for the into production line.
        /// </summary>
        /// <param name="prOrderId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        public IList<StockTransactionData> GetTransactionsForIntoProductionLine(int prOrderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the transactions for line id:{0}", prOrderId));
            var transactions = new List<StockTransactionData>();
            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                transactions = (from transaction in entities.StockTransactions
                                where transaction.Deleted == null
                                && transaction.MasterTableID == prOrderId
                                && transaction.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId
                                orderby transaction.StockTransactionID descending
                                select new StockTransactionData
                                {
                                    Transaction = transaction,
                                    TransactionTraceabilities = transaction.TransactionTraceabilities.ToList(),
                                    INMasterId = transaction.INMasterID,
                                    Product = transaction.INMaster != null ? transaction.INMaster.Name : string.Empty,
                                    INGroupId = transaction.INMaster != null ? transaction.INMaster.INGroupID : (int?)null
                                })
                                .ToList();

                this.Log.LogDebug(this.GetType(), string.Format("{0} labels retrieved", transactions.Count));
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactions;
        }

        /// <summary>
        /// Retrieves the transactions for the dispatch line.
        /// </summary>
        /// <param name="dispatchLineId">The line to retrieve transactions for.</param>
        /// <returns>A collection of transactions.</returns>
        public IList<StockTransactionData> GetTransactionsForDispatchLine(int dispatchLineId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the transactions for line id:{0}", dispatchLineId));
            var transactions = new List<StockTransactionData>();
            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                transactions = (from transaction in entities.StockTransactions
                                where transaction.Deleted == null
                                && transaction.MasterTableID == dispatchLineId
                                && transaction.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId
                                orderby transaction.StockTransactionID descending
                                select new StockTransactionData
                                {
                                    Transaction = transaction,
                                    TransactionTraceabilities = transaction.TransactionTraceabilities.ToList(),
                                    INMasterId = transaction.INMasterID,
                                    Product = transaction.INMaster != null ? transaction.INMaster.Name : string.Empty,
                                    INGroupId = transaction.INMaster != null ? transaction.INMaster.INGroupID : (int?)null
                                })
                                .ToList();

                if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                {
                    foreach (var stockTransactionData in transactions)
                    {
                        var batchid = stockTransactionData.Transaction.BatchNumberID;
                        if (batchid != null)
                        {
                            var prOrder =
                                entities.PROrders.FirstOrDefault(x => x.Deleted == null && x.BatchNumberID == batchid);
                            if (prOrder != null)
                            {
                                stockTransactionData.Reference = prOrder.Reference;
                            }
                        }
                    }
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} labels retrieved", transactions.Count));
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactions;
        }

        ///// <summary>
        ///// Retrieves the last x transactions.
        ///// </summary>
        ///// <param name="serial">The label serial.</param>
        ///// <returns>A collection of recent transactions.</returns>
        //public StockTransactionData GetTransaction(int serial)
        //{
        //    var stockTransaction = new StockTransactionData();
        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            var dbTrans = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(x => x.Serial == serial);
        //            if (dbTrans != null)
        //            {
        //                stockTransaction.Transaction = dbTrans;
        //                //stockTransaction.TransactionTraceabilities = dbTrans.TransactionTraceabilities.ToList();
        //                stockTransaction.INMasterId = dbTrans.INMasterID;
        //                stockTransaction.Product = dbTrans.INMaster != null ? dbTrans.INMaster.Name : string.Empty;
        //                stockTransaction.INGroupId = dbTrans.INMaster != null ? dbTrans.INMaster.INGroupID : (int?)null;

        //                if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
        //                {
        //                    var batchid = dbTrans.BatchNumberID;
        //                    if (batchid != null)
        //                    {
        //                        var prOrder =
        //                            entities.PROrders.FirstOrDefault(x => x.Deleted == null && x.BatchNumberID == batchid);
        //                        if (prOrder != null)
        //                        {
        //                            stockTransaction.Reference = prOrder.Reference;
        //                        }
        //                    }
        //                }

        //                return stockTransaction;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return null;
        //}

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="serial">The label serial.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<StockTransactionData> GetTransaction(int serial, int transId)
        {
            var trans = new List<StockTransactionData>();
            
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTrans = entities.App_FindLabel(serial,transId).ToList();
                    foreach (var label in dbTrans)
                    {
                        var stockTransaction = new StockTransactionData();
                        stockTransaction.Transaction = new StockTransaction
                        {
                            StockTransactionID = label.StockTransactionID,
                            MasterTableID = label.MasterTableID,
                            Serial = label.Serial,
                            BatchNumberID = label.BatchNumberID,
                            AttributeID = label.AttributeID,
                            TransactionDate = label.TransactionDate,
                            TransactionQTY = label.TransactionQTY,
                            TransactionWeight = label.TransactionWeight,
                            Reference = label.Reference,
                            Deleted = label.Deleted,
                            Consumed = label.Consumed,
                            DeviceMasterID = label.DeviceMasterID,
                            UserMasterID = label.UserMasterID,
                            StockTransactionID_Container = label.StockTransactionID_Container,
                            StockTransactionID_Pallet = label.StockTransactionID_Pallet,
                            IsBox = label.IsBox,
                            StoredLabelID = label.StoredLabelID
                        };

                        stockTransaction.Product = label.Product;
                        stockTransaction.INGroupId = label.INGroupID;
                        stockTransaction.Reference = label.Reference1;
                        stockTransaction.Reference2 = label.Reference2;
                        
                        trans.Add(stockTransaction);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return trans;
        }

        /// <summary>
        /// Gets a carcass number from the input attribute id.
        /// </summary>
        /// <param name="attributeId">The label attribute id.</param>
        /// <returns>A corresponding carcass no.</returns>
        public int GetCarcassNo(int attributeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == attributeId);
                    if (dbAttribute != null)
                    {
                        return dbAttribute.CarcassNumber.ToInt();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Retrieves the last transaction in a production batch.
        /// </summary>
        /// <param name="prOrderId">The batch id.</param>
        /// <returns>The last transaction id in a production batch.</returns>
        public int GetLastTransactionInBatch(int prOrderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.Where(x => x.MasterTableID == prOrderId
                                                                      &&
                                                                      x.NouTransactionTypeID ==
                                                                      NouvemGlobal.TransactionTypeProductionReceiptId
                                                                      && x.Deleted == null)
                                                                      .OrderByDescending(x => x.StockTransactionID)
                                                                      .FirstOrDefault();

                    if (trans != null)
                    {
                        return trans.StockTransactionID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Retrieves the into production transaction.
        /// </summary>
        /// <param name="serial">The label serial.</param>
        /// <returns>A stocktransaction.</returns>
        public StockTransaction GetIntoProductionTransaction(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTrans = entities.StockTransactions.FirstOrDefault(x => x.Serial == serial
                        && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsReceiptId
                        && x.Deleted == null
                        && x.Consumed == null);
                    return dbTrans;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<StockTransactionData> GetTransactions(int transactionsToTake)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the last {0} transactions", transactionsToTake));
            var transactions = new List<StockTransactionData>();
            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                transactions = (from transaction in entities.StockTransactions
                                where transaction.Deleted == null && transaction.Consumed == null
                                orderby transaction.StockTransactionID descending
                                select new StockTransactionData { Transaction = transaction })
                                .Take(transactionsToTake)
                                .ToList();
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactions;
        }

        /// <summary>
        /// Retrieves the last x transactions.
        /// </summary>
        /// <param name="transactionsToTake">The last x transactions to retrieve.</param>
        /// <returns>A collection of recent transactions.</returns>
        public IList<Sale> GetTransactionsForReports(int fromSerial, int toSerial)
        {
            var transactions = new List<Sale>();

            try
            {
                // Note: This is just for display purposes on the transaction search grid display, so we use existing Sale types to store our transaction data.
                var entities = new NouvemEntities();
                transactions = (from transaction in entities.StockTransactions
                                where transaction.Deleted == null && transaction.Serial >= fromSerial && transaction.Serial <= toSerial
                                orderby transaction.StockTransactionID descending
                                select new Sale
                                {
                                    SaleID = transaction.StockTransactionID,
                                    Serial = transaction.Serial,
                                    OtherReference = transaction.INMaster != null ? transaction.INMaster.Name : string.Empty,
                                    BPContactID = transaction.Serial,
                                    CreationDate = transaction.TransactionDate,
                                    TotalExVAT = transaction.TransactionWeight,
                                    GrandTotalIncVAT = transaction.TransactionQTY,
                                    CustomerPOReference = transaction.NouTransactionType != null ? transaction.NouTransactionType.Description : string.Empty,
                                    Remarks = transaction.BatchNumber != null ? transaction.BatchNumber.Number : string.Empty,
                                    DocketNote = transaction.Warehouse != null ? transaction.Warehouse.Name : string.Empty
                                })
                                .ToList();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactions;
        }

        /// <summary>
        /// Deletes a transaction and it's associated traceability data.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public string DeleteTransaction(StockTransactionData transaction)
        {
            
            return "";
        }

        /// <summary>
        /// Deletes a transaction only. No other processing.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public bool DeleteTransactionOnly(StockTransactionData transaction)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to delete transaction id:{0}", transaction.Transaction.StockTransactionID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTransaction =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.StockTransactionID == transaction.Transaction.StockTransactionID && x.Deleted == null);

                    if (dbTransaction != null)
                    {
                        dbTransaction.Deleted = DateTime.Now;

                        var labelDeletion = new LabelDeletion
                        {
                            StockTransactionID = dbTransaction.StockTransactionID,
                            DeletionTime = DateTime.Now,
                            UserMasterID = NouvemGlobal.UserId,
                            DeviceMasterID = NouvemGlobal.DeviceId
                        };

                        entities.LabelDeletions.Add(labelDeletion);
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds weight from a deleted transaction back to the carcass it came from.
        /// </summary>
        /// <param name="transaction">The transaction deleted.</param>
        /// <returns>A flag, indicating a successful weight addition or not.</returns>
        public bool AddWeightBackToCarcass(StockTransaction transaction)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddWeightBackToCarcass(): " +
                                                           "Attempting to add weight from deleted transaction id:{0} to carcass transaction {1}", transaction.StockTransactionID, transaction.StockTransactionID_Container));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var carcassTrans =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.StockTransactionID == transaction.StockTransactionID_Container);

                    if (carcassTrans != null)
                    {
                        var newCarcassTrans = this.CreateDeepCopyTransaction(carcassTrans);
                        carcassTrans.Consumed = DateTime.Now;
                        carcassTrans.Deleted = DateTime.Now;
                        newCarcassTrans.TransactionWeight = carcassTrans.TransactionWeight.ToDecimal() +
                                                            transaction.TransactionWeight.ToDecimal();

                        entities.StockTransactions.Add(newCarcassTrans);
                        entities.SaveChanges();

                        foreach (var localTrans in entities.StockTransactions.Where(x => x.StockTransactionID_Container == carcassTrans.StockTransactionID))
                        {
                            localTrans.StockTransactionID_Container =
                                newCarcassTrans.StockTransactionID;
                            entities.SaveChanges();
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public Tuple<int, string, StockTransaction> GetTransactionAndItems(StockTransactionData transaction)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get transaction id:{0} and it's items.", transaction.Transaction.StockTransactionID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTransaction =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.StockTransactionID == transaction.Transaction.StockTransactionID && x.Deleted == null);

                    if (dbTransaction != null)
                    {
                        var localTrans = dbTransaction;
                        if (dbTransaction.StockTransactionID_Container != null)
                        {
                            var box =
                                entities.StockTransactions.FirstOrDefault(
                                    x => x.StockTransactionID == dbTransaction.StockTransactionID_Container);

                            if (box != null)
                            {
                                localTrans = box;
                            }
                        }

                        this.Log.LogDebug(this.GetType(), "Transaction found");
                        var dispatchLine =
                                entities.ARDispatchDetails.FirstOrDefault(
                                    x => x.ARDispatchDetailID == localTrans.MasterTableID);

                        if (dispatchLine != null)
                        {
                            var dispatch = dispatchLine.ARDispatch;
                            this.Log.LogDebug(this.GetType(), string.Format("Dispatch docket found:{0}", dispatch.ARDispatchID));
                            if (dispatch.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                            {
                                return Tuple.Create(0, string.Format(Message.StockOnCompletedDispatch, dispatch.Number), localTrans);
                            }

                            return Tuple.Create(dispatch.ARDispatchID, string.Empty, localTrans);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return Tuple.Create(0, ex.Message, new StockTransaction());
            }

            return Tuple.Create(0, Constant.Error, new StockTransaction());
        }

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transactionId">The transaction to find its docket for.</param>
        /// <returns>A flag, indicating a successful find or not.</returns>
        public Tuple<int, string, StockTransaction> GetTransactionDocket(int transactionId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get transaction id:{0} and it's items.", transactionId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTransaction =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.StockTransactionID == transactionId && x.Deleted == null);

                    if (dbTransaction != null)
                    {
                        var localTrans = dbTransaction;
                        this.Log.LogDebug(this.GetType(), "Transaction found");
                        var intakeLine =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == localTrans.MasterTableID);

                        if (intakeLine != null)
                        {
                            var intake = intakeLine.APGoodsReceipt;
                            this.Log.LogDebug(this.GetType(), string.Format("Intake docket found:{0}", intakeLine.APGoodsReceipt));
                            if (intake.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                            {
                                return Tuple.Create(0, string.Format(Message.StockOncompletedIntakeDocket, intake.Number), localTrans);
                            }

                            return Tuple.Create(intake.APGoodsReceiptID, string.Empty, localTrans);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return Tuple.Create(0, ex.Message, new StockTransaction());
            }

            return Tuple.Create(0, Constant.Error, new StockTransaction());
        }

        /// <summary>
        /// Deletes a transaction and any associated box and box items.
        /// </summary>
        /// <param name="transaction">The transaction to delete.</param>
        /// <returns>A flag, indicating a successful deletion or not.</returns>
        public bool DeleteTransactionAndItems(StockTransactionData transaction)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to delete transaction id:{0} details", transaction.Transaction.StockTransactionID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTransaction =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.StockTransactionID == transaction.Transaction.StockTransactionID && x.Deleted == null);

                    if (dbTransaction != null)
                    {
                        dbTransaction.Deleted = DateTime.Now;
                        var localLabelDeletion = new LabelDeletion
                        {
                            StockTransactionID = dbTransaction.StockTransactionID,
                            DeletionTime = DateTime.Now,
                            UserMasterID = NouvemGlobal.UserId,
                            DeviceMasterID = NouvemGlobal.DeviceId
                        };

                        entities.LabelDeletions.Add(localLabelDeletion);

                        if (dbTransaction.IsBox == true)
                        {
                            // box, so delete it's contents too.
                            var containerItems =
                                entities.StockTransactions.Where(
                                    x => x.StockTransactionID_Container == dbTransaction.StockTransactionID);

                            foreach (var item in containerItems)
                            {
                                item.Deleted = DateTime.Now;

                                var labelDeletion = new LabelDeletion
                                {
                                    StockTransactionID = item.StockTransactionID,
                                    DeletionTime = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId,
                                    DeviceMasterID = NouvemGlobal.DeviceId
                                };

                                entities.LabelDeletions.Add(labelDeletion);
                            }
                        }
                        else if (dbTransaction.StockTransactionID_Container != null)
                        {
                            // container item, so delete it's box and the box contents.
                            var items =
                                entities.StockTransactions.Where(
                                    x => x.StockTransactionID_Container == dbTransaction.StockTransactionID_Container
                                        || x.StockTransactionID == dbTransaction.StockTransactionID_Container);

                            foreach (var item in items)
                            {
                                item.Deleted = DateTime.Now;
                                var labelDeletion = new LabelDeletion
                                {
                                    StockTransactionID = item.StockTransactionID,
                                    DeletionTime = DateTime.Now,
                                    UserMasterID = NouvemGlobal.UserId,
                                    DeviceMasterID = NouvemGlobal.DeviceId
                                };

                                entities.LabelDeletions.Add(labelDeletion);
                            }
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        ///// <summary>
        ///// Deletes a transaction and it's associated traceability data.
        ///// </summary>
        ///// <param name="transaction">The transaction to delete.</param>
        ///// <returns>A flag, indicating a successful deletion or not.</returns>
        //public bool DeleteTransaction(StockTransactionData transaction)
        //{
        //    this.Log.LogDebug(this.GetType(), string.Format("Attempting to delete transaction id:{0}", transaction.Transaction.StockTransactionID));

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            var dbTransaction =
        //                entities.StockTransactions.FirstOrDefault(
        //                    x => x.StockTransactionID == transaction.Transaction.StockTransactionID);

        //            if (dbTransaction != null)
        //            {
        //                dbTransaction.Consumed = DateTime.Now;
        //                dbTransaction.Deleted = DateTime.Now;

        //                //if (dbTransaction.IsBox == true)
        //                //{
        //                //    var boxedTransactions =
        //                //        entities.StockTransactions.Where(
        //                //            x => x.StockTransactionID_Container == dbTransaction.StockTransactionID);

        //                //    foreach (var item in boxedTransactions)
        //                //    {
        //                //        item.Deleted = DateTime.Now;
        //                //    }
        //                //}

        //                // add it marked deleted
        //                var transToDelete = new StockTransaction
        //                {
        //                    MasterTableID = dbTransaction.MasterTableID,
        //                    BatchNumberID = dbTransaction.BatchNumberID,
        //                    TransactionDate = dbTransaction.TransactionDate,
        //                    NouTransactionTypeID = dbTransaction.NouTransactionTypeID,
        //                    TransactionQTY = dbTransaction.TransactionQTY,
        //                    TransactionWeight = dbTransaction.TransactionWeight,
        //                    GrossWeight = dbTransaction.GrossWeight,
        //                    Tare = dbTransaction.Tare,
        //                    WarehouseID = dbTransaction.WarehouseID,
        //                    INMasterID = dbTransaction.INMasterID,
        //                    Serial = dbTransaction.Serial,
        //                    LocationID = dbTransaction.LocationID,
        //                    ManualWeight = dbTransaction.ManualWeight,
        //                    DeviceMasterID = NouvemGlobal.DeviceId,
        //                    UserMasterID = NouvemGlobal.UserId,
        //                    IsBox = dbTransaction.IsBox,
        //                    Deleted = DateTime.Now
        //                };

        //                entities.StockTransactions.Add(transToDelete);
        //                entities.SaveChanges();
        //                this.Log.LogDebug(this.GetType(), string.Format("Transaction Id:{0} deleted", dbTransaction.StockTransactionID));

        //                // Master stock transaction deleted so these will never be recalled, but mark as deleted for consistency.
        //                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
        //                {
        //                    // No urgency on these deletions, so run on it's own thread.
        //                    using (var traceEntities = new NouvemEntities())
        //                    {
        //                        foreach (var trace in transaction.TransactionTraceabilities)
        //                        {
        //                            this.Log.LogDebug(this.GetType(), string.Format("Deleting transaction traceability id:{0}", trace.TransactionTraceabilityID));
        //                            var dbTrace =
        //                                traceEntities.TransactionTraceabilities.FirstOrDefault(
        //                                    x => x.TransactionTraceabilityID == trace.TransactionTraceabilityID);

        //                            if (dbTrace != null)
        //                            {
        //                                dbTrace.Deleted = DateTime.Now;
        //                            }
        //                        }

        //                        traceEntities.SaveChanges();
        //                    }
        //                }), DispatcherPriority.Background);
        //            }

        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return false;
        //}

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <param name="labelIds">The label ids.</param>
        /// <param name="storedLabel">The label images.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public bool AddLabelToTransaction(int transId, string labelIds, StoredLabel storedLabel)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to add the printed label ids:{0} to transaction:{1}", labelIds, transId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == transId);
                    if (trans != null)
                    {
                        entities.StoredLabels.Add(storedLabel);
                        trans.LabelID = labelIds;
                        trans.StoredLabelID = storedLabel.StoredLabelID;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Label labelIds saved");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds the printed labelids to the transaction.
        /// </summary>
        /// <param name="transId">The transaction id.</param>
        /// <returns>A flag, indicating a successful label addition.</returns>
        public StoredLabel GetLabelImages(int transId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the stored label images for trans id:{0}", transId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == transId);
                    if (trans != null)
                    {
                        return trans.StoredLabel;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets and sets the transaction data for the order details.
        /// </summary>
        /// <param name="details">The order details.</param>
        /// <returns>The transaction data for the input order.</returns>
        public void GetTransactionData(IList<SaleDetail> details)
        {
            this.Log.LogDebug(this.GetType(), "GetTransactionData(): Attempting to retrieve the transaction data");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var detail in details)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the transaction data for order:{0}", detail.SaleDetailID));
                        var orderId = detail.SaleDetailID;
                        var apGoodsReceipt = entities.APGoodsReceiptDetails.FirstOrDefault(x => x.APGoodsReceiptDetailID == orderId);
                        if (apGoodsReceipt != null)
                        {
                            var batchNumber = apGoodsReceipt.BatchNumber;
                            if (batchNumber == null)
                            {
                                continue;
                            }

                            var goodsReceiptData = new GoodsReceiptData
                            {
                                BatchNumber = batchNumber,
                                BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                TransactionData = (from transaction in entities.StockTransactions.Where(x => x.MasterTableID == apGoodsReceipt.APGoodsReceiptDetailID
                                    && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                    && x.Deleted == null)
                                                   select new StockTransactionData
                                                   {
                                                       Transaction = transaction,
                                                       TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                   }).ToList()
                            };

                            detail.GoodsReceiptDatas = new List<GoodsReceiptData> { goodsReceiptData };
                            this.Log.LogDebug(this.GetType(), string.Format("{0} transaction data retrieved for apGoodsReceipt:{0} retrieved", apGoodsReceipt.APGoodsReceiptDetailID));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Gets the last transaction for the input module.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockTransactionData GetLastTransactionData(int batchNumberId)
        {
            StockTransactionData transData = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transactionData =
                       entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                           .FirstOrDefault(x => x.BatchNumberID == batchNumberId);

                    if (transactionData != null)
                    {
                        transData = new StockTransactionData
                        {
                            Transaction = transactionData,
                            TransactionTraceabilities = transactionData.TransactionTraceabilities.ToList()
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transData;
        }

        /// <summary>
        /// Gets the last transaction for the input id.
        /// </summary>
        /// <param name="id">The transaction id.</param>
        /// <returns>The last transaction for the input id</returns>
        public StockTransactionData GetTransactionDataById(int id)
        {
            StockTransactionData transData = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transaction =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == id);

                    if (transaction != null)
                    {
                        transData = new StockTransactionData
                        {
                            Transaction = transaction,
                            INMasterId = transaction.INMasterID,
                            Product = transaction.INMaster != null ? transaction.INMaster.Name : string.Empty,
                            INGroupId = transaction.INMaster != null ? transaction.INMaster.INGroupID : (int?)null
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transData;
        }

        /// <summary>
        /// Gets the last transaction for the input module.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockTransactionData GetLastTransactionDataByModule(int batchNumberId, int deviceId, int nouTransactionTypeId)
        {
            StockTransactionData transData = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transactionData =
                       entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                           .FirstOrDefault(x => x.BatchNumberID == batchNumberId
                               && x.DeviceMasterID == deviceId && x.NouTransactionTypeID == nouTransactionTypeId && x.Deleted == null);

                    if (transactionData != null)
                    {
                        transData = new StockTransactionData
                        {
                            Transaction = transactionData,
                            TransactionTraceabilities = transactionData.TransactionTraceabilities.ToList()
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transData;
        }

        /// <summary>
        /// Gets and sets the transaction data for the dispatch order details.
        /// </summary>
        /// <param name="details">The order details.</param>
        /// <returns>The transaction data for the input order.</returns>
        public void GetDispatchTransactionData(IList<SaleDetail> details)
        {
            //this.Log.LogDebug(this.GetType(), "GetDispatchTransactionData(): Attempting to retrieve the transaction data");
            //IList<GoodsReceiptData> data = null;
            //try
            //{
            //    //using (var entities = new NouvemEntities())
            //    //{
            //    var entities = new NouvemEntities();
            //    foreach (var detail in details)
            //    {
            //        this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the dispatch transaction data for order:{0}", detail.SaleDetailID));
            //        var orderId = detail.SaleDetailID;
            //        var arDispatchDetail = entities.ARDispatchDetails.FirstOrDefault(x => x.ARDispatchDetailID == orderId);
            //        if (arDispatchDetail != null)
            //        {
            //            var batchNumber = arDispatchDetail.BatchNumber;

            //            if (batchNumber == null)
            //            {
            //                var stock =
            //                    entities.StockTransactions.FirstOrDefault(
            //                        x => x.MasterTableID == orderId && x.Deleted == null);
            //                if (stock != null)
            //                {
            //                    batchNumber = stock.BatchNumber;
            //                }

            //                if (batchNumber == null)
            //                {
            //                    continue;
            //                }
            //            }

            //            var goodsReceiptData = new GoodsReceiptData
            //            {
            //                BatchNumber = batchNumber,
            //                BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
            //                TransactionData = (from transaction in entities.StockTransactions.Where(x => x.MasterTableID == arDispatchDetail.ARDispatchDetailID
            //                    && x.NouTransactionType.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId
            //                    && x.Deleted == null && x.Consumed == null)
            //                                   select new StockTransactionData
            //                                   {
            //                                       Transaction = transaction,
            //                                       IsStockLocation = transaction.Warehouse.StockLocation,
            //                                       TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
            //                                   }).ToList()
            //                //TransactionData = (from transaction in batchNumber.StockTransactions.Where(x => x.Deleted == null && x.Consumed == null)
            //                //                   select new StockTransactionData
            //                //                   {
            //                //                       Transaction = transaction,
            //                //                       IsStockLocation = transaction.Warehouse.StockLocation,
            //                //                       TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
            //                //                   }).ToList()
            //            };

            //            detail.GoodsReceiptDatas = new List<GoodsReceiptData> { goodsReceiptData };
            //            this.Log.LogDebug(this.GetType(), string.Format("{0} transaction data retrieved for ARDispatchDetail:{0} retrieved", arDispatchDetail.ARDispatchDetailID));
            //        }
            //    }
            //    // }
            //}
            //catch (Exception ex)
            //{
            //    this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            //}
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="detail">The goods receipt data containing the stock transaction.</param>
        /// <param name="useId">flag, as to whether the stocktransaction search is by id or serial.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        public bool UpdateTransactionData(SaleDetail detail, bool useId = true)
        {
            var data = detail.GoodsReceiptDatas.First();
            var stockTransaction = data.TransactionData.First().Transaction;
            this.Log.LogDebug(this.GetType(), string.Format("UpdateTransactionData(): Attempting to update the transaction data for stock transaction Id:{0}", stockTransaction.StockTransactionID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    StockTransaction dbTransaction;
                    if (useId)
                    {
                        dbTransaction = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == stockTransaction.StockTransactionID);
                    }
                    else
                    {
                        dbTransaction = entities.StockTransactions.FirstOrDefault(x => x.Serial == stockTransaction.Serial && x.Deleted == null && x.Consumed == null);
                    }

                    if (dbTransaction != null)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Consuming stock transaction id:{0}", dbTransaction.StockTransactionID));
                        dbTransaction.Consumed = DateTime.Now;

                        var newTrans = new StockTransaction
                        {
                            MasterTableID = dbTransaction.MasterTableID,
                            BatchNumberID = dbTransaction.BatchNumberID,
                            TransactionDate = dbTransaction.TransactionDate,
                            NouTransactionTypeID = NouvemGlobal.TransactionTypeEditId,
                            TransactionQTY = stockTransaction.TransactionQTY,
                            TransactionWeight = stockTransaction.TransactionWeight,
                            GrossWeight = stockTransaction.GrossWeight,
                            Tare = stockTransaction.Tare,
                            WarehouseID = dbTransaction.WarehouseID,
                            INMasterID = dbTransaction.INMasterID,
                            Serial = dbTransaction.Serial,
                            LocationID = dbTransaction.LocationID,
                            ManualWeight = stockTransaction.ManualWeight,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            UserMasterID = NouvemGlobal.UserId,
                            IsBox = dbTransaction.IsBox
                        };

                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();

                        this.Log.LogDebug(this.GetType(), string.Format("New stock transaction Id:{0} created", newTrans.StockTransactionID));

                        var weightdifference = newTrans.TransactionWeight.ToDecimal() - dbTransaction.TransactionWeight.ToDecimal();

                        if (weightdifference > 0 || weightdifference < 0)
                        {
                            detail.WeightReceived += weightdifference;
                            var saleDetail = entities.APGoodsReceiptDetails
                                .FirstOrDefault(x => x.APGoodsReceiptDetailID == detail.SaleDetailID);

                            if (saleDetail != null)
                            {
                                saleDetail.WeightReceived = detail.WeightReceived;
                                saleDetail.TotalExcVAT = detail.TotalExVAT;
                                saleDetail.TotalIncVAT = detail.TotalIncVAT;
                                entities.SaveChanges();
                            }
                        }

                        var batchTraceabilities = data.BatchTraceabilities;
                        if (batchTraceabilities != null && batchTraceabilities.Any())
                        {
                            this.Log.LogDebug(this.GetType(), "Updating batch traceabilities");
                            foreach (var trace in batchTraceabilities)
                            {
                                var dbTrace =
                                    entities.BatchTraceabilities.OrderByDescending(x => x.BatchTraceabilityID)
                                    .FirstOrDefault(x => x.BatchTraceabilityID == trace.BatchTraceabilityID);
                                if (dbTrace != null)
                                {
                                    dbTrace.Value = trace.Value;
                                }
                            }
                        }

                        var transData = data.TransactionData;
                        if (transData != null && transData.Any())
                        {
                            var serialTraceabilities = transData.First().TransactionTraceabilities;
                            if (serialTraceabilities != null && serialTraceabilities.Any())
                            {
                                this.Log.LogDebug(this.GetType(), "Updating serial traceabilities");
                                foreach (var trace in serialTraceabilities)
                                {
                                    var dbTrace =
                                        entities.TransactionTraceabilities.OrderByDescending(x => x.TransactionTraceabilityID)
                                        .FirstOrDefault(x => x.TransactionTraceabilityID == trace.TransactionTraceabilityID);

                                    if (dbTrace != null)
                                    {
                                        dbTrace.StockTransactionID = newTrans.StockTransactionID;
                                        dbTrace.Value = trace.Value;
                                    }
                                }
                            }
                        }

                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "transaction data updated");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="detail">The goods receipt data containing the stock transaction.</param>
        /// <param name="viewType">The module type.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        public bool UpdateTransactionWeight(IList<GoodsIn> transactions, ViewType viewType)
        {
            this.Log.LogInfo(this.GetType(), "UpdateTransactionData(): Updating transaction data");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var trans in transactions)
                    {
                        entities.App_UpdateTransaction(trans.StockTransactionID, trans.INMasterID,
                            trans.NettWeight.ToDecimal(), trans.Qty, trans.Price,trans.Delete, NouvemGlobal.UserId, NouvemGlobal.DeviceId);
                    }

                    this.Log.LogDebug(this.GetType(), "transaction data updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction.
        /// </summary>
        /// <param name="data">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction.</returns>
        public GoodsReceiptData GetTransactionData(GoodsReceiptData data)
        {
            var stockTransactionId = data.TransactionData.First().Transaction.StockTransactionID;
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionData(): Attempting to retrieve the transaction data for stock transaction Id:{0}", stockTransactionId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stockTransaction = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == stockTransactionId);
                    if (stockTransaction != null)
                    {
                        data.TransactionData.First().TransactionTraceabilities = stockTransaction.TransactionTraceabilities.ToList();

                        if (stockTransaction.BatchNumber != null)
                        {
                            data.BatchNumber = stockTransaction.BatchNumber;
                            data.BatchTraceabilities = stockTransaction.BatchNumber.BatchTraceabilities.ToList();
                        }

                        this.Log.LogDebug(this.GetType(), "transaction data retrieved");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <param name="module">The relevant module.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForSerial(int serialNo, ViewType module = ViewType.ARDispatch, bool useAttribute = false, int? splitStockId = null)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionDataForSerialNo(): Attempting to retrieve the transaction data for stock transaction serial no:{0}", serialNo));
            var data = new StockDetail { ScanningProductionStock = true };
            var isBoxStock = false;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stockTransaction = entities.StockTransactions
                        .FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.Warehouse.StockLocation);
                    if (stockTransaction != null && stockTransaction.Consumed != null)
                    {
                        StockTransaction localTransaction;
                        if (splitStockId.HasValue)
                        {
                            localTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                                .FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.Consumed == null && x.Warehouse.StockLocation && x.INMasterID == splitStockId);
                        }
                        else
                        {
                            localTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                                .FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.Consumed == null && x.Warehouse.StockLocation);
                        }

                        if (localTransaction != null)
                        {
                            stockTransaction = localTransaction;
                        }
                    }

                    if (stockTransaction != null)
                    {
                        if (stockTransaction.Comments != null && stockTransaction.Comments.Equals(Constant.Pallet))
                        {
                            data.PalletId = stockTransaction.StockTransactionID;
                            return data;
                        }

                        data.BaseWarehouseID = stockTransaction.WarehouseID;
                        data.BaseMasterTableID = stockTransaction.MasterTableID;
                        data.SplitID = stockTransaction.SplitID;
                        if (stockTransaction.Consumed != null && stockTransaction.StockTransactionID_Container != null &&
                            module == ViewType.ARDispatch)
                        {
                            // dispatch box stock
                            isBoxStock = true;
                        }
                        else if (stockTransaction.Consumed != null)
                        {
                            data.Error = Constant.Error;
                            var consumed = entities.StockTransactions
                                .OrderByDescending(x => x.StockTransactionID)
                                .FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.NouTransactionTypeID == 7);

                            if (consumed != null)
                            {
                                // Consumed onto an order, so return the order number it's on.
                                int orderNo = 0;
                                if (module == ViewType.ARDispatch)
                                {
                                    var dispatch = entities.ARDispatchDetails.FirstOrDefault(x => x.ARDispatchDetailID == consumed.MasterTableID);
                                    if (dispatch != null)
                                    {
                                        orderNo = dispatch.ARDispatch.Number;
                                    }
                                }
                                else
                                {
                                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == consumed.MasterTableID);
                                    if (prOrder != null)
                                    {
                                        orderNo = prOrder.Number;
                                    }
                                }

                                data.StockTransactionIdToUnconsume = stockTransaction.StockTransactionID;
                                data.Error = orderNo.ToString();
                                this.Log.LogDebug(this.GetType(), string.Format("Item already consumed and on order no:{0}", orderNo));
                            }

                            return data;
                        }

                        var localProduct =
                            NouvemGlobal.InventoryItems.FirstOrDefault(x =>
                                x.INMasterID == stockTransaction.INMasterID);
                        if (localProduct != null && localProduct.Master.ReprintLabelAtDispatch == true && stockTransaction.Attribute != null)
                        {
                            data.Attribute1 = stockTransaction.Attribute.Attribute1;
                            data.Attribute2 = stockTransaction.Attribute.Attribute2;
                            data.Attribute3 = stockTransaction.Attribute.Attribute3;
                            data.Attribute4 = stockTransaction.Attribute.Attribute4;
                            data.Attribute5 = stockTransaction.Attribute.Attribute5;
                            data.Attribute6 = stockTransaction.Attribute.Attribute6;
                            data.Attribute7 = stockTransaction.Attribute.Attribute7;
                            data.Attribute8 = stockTransaction.Attribute.Attribute8;
                            data.Attribute9 = stockTransaction.Attribute.Attribute9;
                            data.Attribute10 = stockTransaction.Attribute.Attribute10;
                            data.Attribute11 = stockTransaction.Attribute.Attribute11;
                            data.Attribute12 = stockTransaction.Attribute.Attribute12;
                            data.Attribute13 = stockTransaction.Attribute.Attribute13;
                            data.Attribute14 = stockTransaction.Attribute.Attribute14;
                            data.Attribute15 = stockTransaction.Attribute.Attribute15;
                            data.Attribute16 = stockTransaction.Attribute.Attribute16;
                            data.Attribute17 = stockTransaction.Attribute.Attribute17;
                            data.Attribute18 = stockTransaction.Attribute.Attribute18;
                            data.Attribute19 = stockTransaction.Attribute.Attribute19;
                            data.Attribute20 = stockTransaction.Attribute.Attribute20;
                            data.Attribute21 = stockTransaction.Attribute.Attribute21;
                            data.Attribute22 = stockTransaction.Attribute.Attribute22;
                            data.Attribute23 = stockTransaction.Attribute.Attribute23;
                            data.Attribute24 = stockTransaction.Attribute.Attribute24;
                            data.Attribute25 = stockTransaction.Attribute.Attribute25;
                            data.Attribute26 = stockTransaction.Attribute.Attribute26;
                            data.Attribute27 = stockTransaction.Attribute.Attribute27;
                            data.Attribute28 = stockTransaction.Attribute.Attribute28;
                            data.Attribute29 = stockTransaction.Attribute.Attribute29;
                            data.Attribute30 = stockTransaction.Attribute.Attribute30;
                            data.ReprintLabelAtDispatch = true;
                        }

                        data.Transaction = stockTransaction;
                        if (useAttribute)
                        {
                            if (stockTransaction.Attribute != null &&
                                !stockTransaction.Attribute.CarcassNumber.IsNullOrZero())
                            {
                                var coldWeight = stockTransaction.TransactionWeight.ToDecimal() * 0.98M;
                                data.Transaction.TransactionWeight = coldWeight;
                            }
                        }

                        if (stockTransaction.Attribute != null)
                        {
                            data.UseBy = ApplicationSettings.UseLegacyUseByAttribute
                                ? stockTransaction.Attribute.Attribute6
                                : stockTransaction.Attribute.Attribute1;
                        }

                        if (isBoxStock)
                        {
                            var boxTransaction = entities.StockTransactions
                                        .FirstOrDefault(x => x.StockTransactionID == stockTransaction.StockTransactionID_Container);

                            if (boxTransaction != null)
                            {
                                boxTransaction.TransactionWeight -= stockTransaction.TransactionWeight.ToDecimal();
                                boxTransaction.TransactionQTY -= stockTransaction.TransactionQTY.ToDecimal();

                                if (stockTransaction.Pieces != null && boxTransaction.Pieces != null)
                                {
                                    boxTransaction.Pieces -= stockTransaction.Pieces;
                                }

                                stockTransaction.StockTransactionID_Container = null;
                                entities.SaveChanges();
                            }
                        }

                        if (stockTransaction.IsBox == true && module == ViewType.ARDispatch)
                        {
                            data.BoxItemsCount =
                                stockTransaction.StockTransaction1.Count(x => x.Deleted == null);

                            // mark the box items as consumed
                            var boxStockItems = stockTransaction.StockTransaction1;
                            if (boxStockItems.Any())
                            {
                                foreach (var item in boxStockItems)
                                {
                                    item.WarehouseID = NouvemGlobal.DispatchId;
                                }
                            }

                            entities.SaveChanges();
                        }

                        this.Log.LogDebug(this.GetType(), "transaction data retrieved");
                    }
                    else
                    {
                        data.Error = Constant.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForLinkedCarcassSerial(int serialNo, bool uniqueBarcode = true)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionDataForLinkedCarcassSerial(): 1. Serial no:{0}", serialNo));
            var data = new StockDetail();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (uniqueBarcode)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Checking if serial:{0} is already in the db", serialNo));
                        var dbTransaction = entities.StockTransactions
                            .FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.NouTransactionTypeID == 7);
                        if (dbTransaction != null)
                        {
                            this.Log.LogDebug(this.GetType(), "Exists: YES");
                            var orderNo = 0;
                            var dispatch = entities.ARDispatchDetails.FirstOrDefault(x => x.ARDispatchDetailID == dbTransaction.MasterTableID && x.Deleted == null);
                            if (dispatch != null)
                            {
                                orderNo = dispatch.ARDispatch.Number;
                                data.Error = string.Format(Message.AlreadyOnDocket, orderNo);
                                return data;
                            }
                        }
                    }

                    this.Log.LogDebug(this.GetType(), "Exists: NO");
                    var stockTransaction = entities.GetLinkedProduct(serialNo.ToString()).FirstOrDefault();

                    // Test Data *******************************************
                    //var stockTransaction = new GetLinkedProduct_Result2();
                    //stockTransaction.ProductCode = "SS01";
                    //stockTransaction.ScaleNettWeight = 126.89m;
                    //stockTransaction.KillDate = DateTime.Now;
                    //stockTransaction.KillNumber = 101;
                    //stockTransaction.BatchCode = "ytrsf";
                    //stockTransaction.MonthsOld = 15;
                    // *****************************************************

                    if (stockTransaction != null)
                    {
                        var productCode = stockTransaction.ProductCode;
                        var wgt = stockTransaction.ScaleNettWeight;
                        var killdate = stockTransaction.KillDate;
                        var batchCode = stockTransaction.BatchCode;
                        var killNo = stockTransaction.KillNumber;
                        var ageInMonths = stockTransaction.MonthsOld;
                        var attributeId = stockTransaction.AttributeID;
                        var localProduct =
                        NouvemGlobal.InventoryItems.FirstOrDefault(
                           x => x.Master.Code.CompareIgnoringCase(productCode));

                        if (localProduct == null)
                        {
                            this.Log.LogDebug(this.GetType(), "Product code not found in Nouvem db");
                            data.Error = Constant.NotFound;
                            return data;
                        }

                        data.ProductTare = localProduct.PiecesTareWeight;
                        data.BatchNumber = BatchManager.Instance.GenerateBatchNumber(false,"");

                        data.Transaction = new StockTransaction
                        {
                            TransactionWeight = wgt,
                            GrossWeight = wgt,
                            Tare = 0,
                            Serial = serialNo,
                            INMasterID = localProduct.Master.INMasterID,
                            AttributeID = attributeId,
                            StockTransactionID = serialNo,
                            BatchNumberID = data.BatchNumber.BatchNumberID
                        };

                        var attributes = new StockAttribute
                        {
                            BatchNumber = batchCode,
                            KillDate = killdate,
                            KillNumber = killNo.ToInt(),
                            AgeInMonths = ageInMonths.ToInt()
                        };

                        data.StockAttribute = attributes;
                        this.Log.LogDebug(this.GetType(), "transaction data retrieved");
                    }
                    else
                    {
                        data.Error = Constant.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForLinkedCarcassSerialSplit(int serialNo)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionDataForLinkedCarcassSerialSplit(): 1. Serial no:{0}", serialNo));
            var data = new StockDetail();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stockTransaction = entities.GetLinkedProduct(serialNo.ToString()).FirstOrDefault();

                    // Test Data *******************************************
                    //var stockTransaction = new GetLinkedProduct_Result2();
                    //stockTransaction.ProductCode = "COW1";
                    //stockTransaction.ScaleNettWeight = 126.89m;
                    //stockTransaction.KillDate = DateTime.Now;
                    //stockTransaction.KillNumber = 101;
                    //stockTransaction.BatchCode = "ytrsf";
                    //stockTransaction.MonthsOld = 15;
                    // *****************************************************

                    if (stockTransaction != null)
                    {
                        var productCode = stockTransaction.ProductCode;
                        var wgt = stockTransaction.ScaleNettWeight;
                        var killdate = stockTransaction.KillDate;
                        var batchCode = stockTransaction.BatchCode;
                        var killNo = stockTransaction.KillNumber;
                        var ageInMonths = stockTransaction.MonthsOld;
                        var attributeId = stockTransaction.AttributeID;

                        var localProduct =
                        NouvemGlobal.InventoryItems.FirstOrDefault(
                           x => x.Master.Code.CompareIgnoringCase(productCode));

                        this.Log.LogDebug(this.GetType(), string.Format("2. Product found. Wgt:{0}, Code:{1}, Killdate:{2}, BatchCode:{3}, Kill No:{4}, Age:{5}", wgt, productCode, killdate, batchCode, killNo, ageInMonths));

                        if (localProduct == null)
                        {
                            this.Log.LogDebug(this.GetType(), "Product code not found in Nouvem db");
                            data.Error = Constant.NotFound;
                            return data;
                        }

                        data.ProductTare = localProduct.PiecesTareWeight;
                        data.BatchNumber = BatchManager.Instance.GenerateBatchNumber(false,"");

                        data.Transaction = new StockTransaction
                        {
                            TransactionWeight = wgt,
                            GrossWeight = wgt,
                            Tare = 0,
                            Serial = serialNo,
                            INMasterID = localProduct.Master.INMasterID,
                            AttributeID = attributeId,
                            StockTransactionID = serialNo
                        };

                        var attributes = new StockAttribute
                        {
                            BatchNumber = batchCode,
                            KillDate = killdate,
                            KillNumber = killNo.ToInt(),
                            AgeInMonths = ageInMonths.ToInt()
                        };

                        data.StockAttribute = attributes;
                        this.Log.LogDebug(this.GetType(), "transaction data retrieved");
                    }
                    else
                    {
                        data.Error = Constant.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockAttribute GetTransactionAttributesCarcassSerial(int serialNo)
        {
            this.Log.LogDebug(this.GetType(), string.Format("1.1 Serial no:{0}", serialNo));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stockTransaction = entities.GetLinkedProduct(serialNo.ToString()).FirstOrDefault();

                    // Test Data *******************************************
                    //var stockTransaction = new GetLinkedProduct_Result2();
                    //stockTransaction.ProductCode = "1/05";
                    //stockTransaction.ScaleNettWeight = 126.89m;
                    //stockTransaction.KillDate = DateTime.Now;
                    //stockTransaction.KillNumber = 101;
                    //stockTransaction.BatchCode = "ytrsf";
                    //stockTransaction.MonthsOld = 15;
                    // *****************************************************

                    if (stockTransaction != null)
                    {
                        var productCode = stockTransaction.ProductCode;
                        var wgt = stockTransaction.ScaleNettWeight;
                        var killdate = stockTransaction.KillDate;
                        var batchCode = stockTransaction.BatchCode;
                        var killNo = stockTransaction.KillNumber;
                        var ageInMonths = stockTransaction.MonthsOld;

                        this.Log.LogDebug(this.GetType(), string.Format("2. Product found. Wgt:{0}, Code:{1}, Killdate:{2}, BatchCode:{3}, Kill No:{4}, Age:{5}", wgt, productCode, killdate, batchCode, killNo, ageInMonths));
                        var attributes = new StockAttribute
                        {
                            BatchNumber = batchCode,
                            KillDate = killdate,
                            KillNumber = killNo.ToInt(),
                            AgeInMonths = ageInMonths.ToInt()
                        };

                        return attributes;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForIntoProduction(int serialNo)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionDataForSerialIntoProduction(): Attempting to retrieve the transaction data for stock transaction serial no:{0}", serialNo));
            var isBoxStock = false;
            var data = this.GetSerialAttributeData(serialNo);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stockTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                        .FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.Warehouse.StockLocation);
                    if (stockTransaction != null)
                    {
                        if (stockTransaction.Consumed != null && stockTransaction.StockTransactionID_Container != null)
                        {
                            // dispatch box stock
                            isBoxStock = true;
                        }
                        else if (stockTransaction.Consumed != null)
                        {
                            data.Error = Constant.Error;
                            var consumed = entities.StockTransactions.FirstOrDefault(x => x.Serial == serialNo && x.Consumed == null && !x.Warehouse.StockLocation);

                            if (consumed != null)
                            {
                                // Consumed onto an order, so return the order number it's on.
                                var reference = string.Empty;
                                var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == consumed.MasterTableID);
                                if (prOrder != null)
                                {
                                    reference = prOrder.Reference;
                                }
                                else
                                {
                                    reference = Strings.Unknown;
                                }

                                data.INMasterID = stockTransaction.INMasterID;
                                data.Error = reference;
                                this.Log.LogDebug(this.GetType(), string.Format("Item already consumed and on order no:{0}", reference));
                            }

                            return data;
                        }

                        data.TransactionWeight = stockTransaction.TransactionWeight;
                        data.TransactionQty = stockTransaction.TransactionQTY;
                        data.AttributeID = stockTransaction.AttributeID;
                        data.INMasterID = stockTransaction.INMasterID;
                        data.ProductID = stockTransaction.INMasterID;
                        data.BatchNumberID = stockTransaction.BatchNumberID;
                        data.Serial = stockTransaction.StockTransactionID;
                        data.WarehouseID = stockTransaction.WarehouseID;

                        if (ApplicationSettings.RetrieveAttributeOnDispatchScan)
                        {
                            if (!data.CarcassNumber.IsNullOrZero())
                            {
                                data.TransactionWeight = Math.Round(data.TransactionWeight.ToDecimal() * 0.98M,2);
                            }
                        }

                        if (isBoxStock)
                        {
                            var boxTransaction = entities.StockTransactions
                                        .FirstOrDefault(x => x.StockTransactionID == stockTransaction.StockTransactionID_Container && x.Consumed == null && x.Deleted == null);

                            if (boxTransaction != null)
                            {
                                var newBoxTransaction = this.CreateDeepCopyTransaction(boxTransaction);
                                newBoxTransaction.TransactionWeight -= stockTransaction.TransactionWeight.ToDecimal();
                                newBoxTransaction.TransactionQTY -= stockTransaction.TransactionQTY.ToDecimal();
                                boxTransaction.Deleted = DateTime.Now;

                                if (stockTransaction.Pieces != null && newBoxTransaction.Pieces != null)
                                {
                                    newBoxTransaction.Pieces -= stockTransaction.Pieces;
                                }

                                entities.StockTransactions.Add(newBoxTransaction);
                                stockTransaction.StockTransactionID_Container = null;
                                entities.SaveChanges();

                                foreach (var trans in entities.StockTransactions.Where(x => x.StockTransactionID_Container == boxTransaction.StockTransactionID))
                                {
                                    trans.StockTransactionID_Container = newBoxTransaction.StockTransactionID;
                                }

                                foreach (var trace in entities.TransactionTraceabilities.Where(x => x.StockTransactionID == boxTransaction.StockTransactionID))
                                {
                                    trace.StockTransactionID = newBoxTransaction.StockTransactionID;
                                }

                                entities.SaveChanges();
                            }
                            else
                            {
                                data.Error = string.Format("{0}{1}", Message.StockConsumedOnBox,
                                    stockTransaction.StockTransactionID_Container);
                            }
                        }
                    }
                    else
                    {
                        data.Error = Constant.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public bool IsCarcassSplitAnimal(string serialNo)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var result = new System.Data.Entity.Core.Objects.ObjectParameter("Result", typeof(bool));
                    entities.App_IsCarcassSplitStock(serialNo, result);
                    return result.Value.ToBool();

                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionDataForIntoProductionByComments(string serialNo, int inmasterid)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionDataForIntoProductionByComments(): Attempting to retrieve the transaction data for stock transaction serial no:{0}", serialNo));

            var data = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    StockTransaction stockTransaction;
                    if (inmasterid == 0)
                    {
                        stockTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                            .FirstOrDefault(x => x.Comments == serialNo && x.Deleted == null && x.Consumed == null
                                                 && x.Warehouse.StockLocation);
                    }
                    else
                    {
                        stockTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                            .FirstOrDefault(x => x.Comments == serialNo && x.Deleted == null && x.Consumed == null
                                                 && x.Warehouse.StockLocation && x.INMasterID == inmasterid);
                    }
                   
                    if (stockTransaction != null)
                    {
                        data = this.GetSerialAttributeData(stockTransaction.StockTransactionID);
                        data.TransactionWeight = stockTransaction.TransactionWeight;
                        data.TransactionQty = stockTransaction.TransactionQTY;
                        data.AttributeID = stockTransaction.AttributeID;
                        data.INMasterID = stockTransaction.INMasterID;
                        data.BatchNumberID = stockTransaction.BatchNumberID;
                        data.Serial = stockTransaction.StockTransactionID;
                        data.WarehouseID = stockTransaction.WarehouseID;
                    }
                    else
                    {
                        data.Error = Constant.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetTransactionByComments(string serialNo, int inmasterid)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetTransactionDataForIntoProductionByComments(): Attempting to retrieve the transaction data for stock transaction serial no:{0}", serialNo));

            var data = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    StockTransaction stockTransaction;
                    if (inmasterid == 0)
                    {
                        stockTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                            .FirstOrDefault(x => x.Comments == serialNo && x.Deleted == null && x.MasterTableID != null);
                    }
                    else
                    {
                        stockTransaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                            .FirstOrDefault(x => x.Comments == serialNo && x.Deleted == null && x.MasterTableID != null
                                                 && x.INMasterID == inmasterid);
                    }

                    if (stockTransaction != null)
                    {
                        data.Serial = stockTransaction.Serial;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the disptached transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The goods receipt data containing the stock transaction.</param>
        /// <param name="arDispatchId">The ar dispatch id.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetDispatchedTransactionDataForSerial(int serialNo, int arDispatchId)
        {
            //this.Log.LogWarning(this.GetType(), string.Format("2. GetDispatchedTransactionDataForSerialNo(): Attempting to retrieve the transaction data for stock transaction serial no:{0}, ardispatch:{1}", serialNo, arDispatchId));
            var data = new StockDetail();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dispatch = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == arDispatchId);
                    if (dispatch == null)
                    {
                        this.Log.LogError(this.GetType(), "arDispatch not found");
                        data.Error = Constant.StockNotOnOrder;
                        return data;
                    }

                    var palletTrans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == serialNo);

                    if (palletTrans != null && palletTrans.Comments != null &&
                        palletTrans.Comments.Equals(Constant.Pallet))
                    {
                        data.PalletId = serialNo;
                        return data;
                    }

                    var param = new System.Data.Entity.Core.Objects.ObjectParameter("Result", typeof(bool));
                    entities.App_IsStockOnOrder(arDispatchId, serialNo, param);
                    var onOrder = param.Value.ToBool();

                    if (onOrder)
                    {
                        var trans = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(x => x.Serial == serialNo && x.Deleted == null && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId);
                        data.MasterTableID = trans.MasterTableID.ToInt();
                        data.TransactionWeight = trans.TransactionWeight;
                        data.TransactionQty = trans.TransactionQTY;
                    }

                    if (!onOrder)
                    {
                        this.Log.LogDebug(this.GetType(), "Not on order");
                        data.Error = Message.StockNotFoundOnOrder;
                        return data;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Gets and sets the into production transaction data for the input stock transaction serial no.
        /// </summary>
        /// <param name="serialNo">The serial number to search for.</param>
        /// <param name="prOrderId">The ar dispatch id.</param>
        /// <returns>The transaction data for the input stock transaction serial no.</returns>
        public StockDetail GetIntoProductionTransactionDataForSerial(int serialNo, int prOrderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetIntoProductionTransactionDataForSerial(): Attempting to retrieve the transaction data for stock transaction serial no:{0}, prOrderId:{1}", serialNo, prOrderId));
            var data = new StockDetail();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stockTransaction =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.MasterTableID == prOrderId && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId && x.Serial == serialNo && x.Deleted == null);

                    if (stockTransaction == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Not on order");
                        data.Error = Constant.StockNotOnProduction;
                        return data;
                    }

                   
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = Constant.Error;
            }

            return data;
        }

        /// <summary>
        /// Retrieves the batch traceability data.
        /// </summary>
        /// <param name="batchNo">The batch no to get the for.</param>
        /// <returns>Batch trace data.</returns>
        public GoodsReceiptData GetProductionOrderBatchData(ProductionData order)
        {
            var goodsData = new GoodsReceiptData();
            goodsData.BatchNumber = order.BatchNumber;
            goodsData.TransactionData = new List<StockTransactionData>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbBatch = entities.BatchNumbers.FirstOrDefault(x => x.BatchNumberID == order.Order.BatchNumberID);
                    if (dbBatch != null)
                    {
                        this.Log.LogWarning(this.GetType(), "2. Batch no retrieved");
                        goodsData.BatchNumber = dbBatch;
                        //goodsData.BatchTraceabilities = dbBatch.BatchTraceabilities.ToList();

                        // get the intake product group
                        var associatedProductLine =
                            entities.APGoodsReceiptDetails.OrderByDescending(x => x.APGoodsReceiptDetailID).FirstOrDefault(x => x.BatchNumberID == dbBatch.BatchNumberID && x.Deleted == null);

                        if (associatedProductLine != null)
                        {
                            if (associatedProductLine.INMaster != null && associatedProductLine.INMaster.INGroup != null)
                            {
                                goodsData.ProductGroup = associatedProductLine.INMaster.INGroup;
                                goodsData.Product = associatedProductLine.INMaster;

                                var prOrder =
                                    entities.PROrders.FirstOrDefault(
                                        x => x.Deleted == null && x.BatchNumberID == dbBatch.BatchNumberID);
                                if (prOrder != null)
                                {
                                    goodsData.Reference = prOrder.Reference;
                                    goodsData.PROrderId = prOrder.PROrderID;
                                }

                                this.Log.LogWarning(this.GetType(), string.Format("3. Batch product:{0}, Group:{1}, Reference:{2}", goodsData.Product.INMasterID, goodsData.ProductGroup.INGroupID, goodsData.Reference));
                            }
                        }
                        else
                        {
                            var localStock =
                                 entities.StockTransactions
                                .FirstOrDefault(x =>
                                    x.MasterTableID == order.Order.PROrderID
                                    && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsDeliveryId
                                    && x.Consumed == null && x.Deleted == null);

                            if (localStock != null)
                            {
                                var product = entities.INMasters.First(x => x.INMasterID == localStock.INMasterID);
                                goodsData.ProductGroup = product.INGroup;
                                goodsData.Product = product;
                                goodsData.Reference = order.Order.Reference;
                            }
                        }
                    }
                    else
                    {
                        this.Log.LogError(this.GetType(), "Batch no not found");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return goodsData;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockDetail GetStockTransactionByComments(string comments, int warehouseId)
        {
            var detail = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    StockTransaction trans;
                    if (comments.IsNumeric() && comments.Length < 18)
                    {
                        var serial = comments.ToInt();
                        trans = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(
                            x => x.Serial == serial && x.WarehouseID == warehouseId);
                    }
                    else
                    {
                        trans = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(
                            x => x.Comments == comments && x.WarehouseID == warehouseId);
                        if (trans == null)
                        {
                            var localTrans = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(
                                x => x.Comments == comments);
                            if (localTrans != null)
                            {
                                detail.Error = localTrans.TransactionDate.ToString("dd/MM/yyyy HH:mm");
                            }
                        }
                    }

                    if (trans != null && trans.Attribute != null)
                    {
                        detail = this.CreateStockAttribute(trans.Attribute);
                        detail.StockTransactionID = trans.StockTransactionID;
                        detail.INMasterID = trans.INMasterID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return detail;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockDetail GetStockTransactionBySerial(int serial, int nouTransactionTypeId)
        {
            var detail = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans =
                        entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(
                            x => x.Serial == serial && x.NouTransactionTypeID == nouTransactionTypeId && x.Deleted == null);

                    if (trans != null && trans.Attribute != null)
                    {
                        detail = this.CreateStockAttribute(trans.Attribute);
                        detail.Transaction = this.CreateTransaction(trans);
                        detail.StockTransactionID = trans.StockTransactionID;
                        detail.INMasterID = trans.INMasterID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return detail;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionById(int transId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.FirstOrDefault(
                            x => x.StockTransactionID == transId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public App_GetProductionStockData_Result GetProductionOrderStockData(string transId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.App_GetProductionStockData(transId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionIncDeleted(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.FirstOrDefault(
                            x => x.Serial == serial);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the last attribute for the input module and batch.
        /// </summary>
        /// <param name="batchNumberId">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockDetail GetLastBatchAttributeData(int batchNumberId, int deviceId, int nouTransactionTypeId)
        {
            StockDetail transData = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transactionData =
                       entities.StockTransactions
                           .Where(x => x.BatchNumberID == batchNumberId &&
                               x.DeviceMasterID == deviceId && x.NouTransactionTypeID == nouTransactionTypeId && x.Deleted == null)
                               .OrderByDescending(x => x.StockTransactionID).FirstOrDefault();

                    if (transactionData != null && transactionData.Attribute != null)
                    {
                        var data = transactionData.Attribute;

                        #region attributes

                        transData = new StockDetail
                        {
                            Attribute1 = data.Attribute1,
                            Attribute2 = data.Attribute2,
                            Attribute3 = data.Attribute3,
                            Attribute4 = data.Attribute4,
                            Attribute5 = data.Attribute5,
                            Attribute6 = data.Attribute6,
                            Attribute7 = data.Attribute7,
                            Attribute8 = data.Attribute8,
                            Attribute9 = data.Attribute9,
                            Attribute10 = data.Attribute10,
                            Attribute11 = data.Attribute11,
                            Attribute12 = data.Attribute12,
                            Attribute13 = data.Attribute13,
                            Attribute14 = data.Attribute14,
                            Attribute15 = data.Attribute15,
                            Attribute16 = data.Attribute16,
                            Attribute17 = data.Attribute17,
                            Attribute18 = data.Attribute18,
                            Attribute19 = data.Attribute19,
                            Attribute20 = data.Attribute20,
                            Attribute21 = data.Attribute21,
                            Attribute22 = data.Attribute22,
                            Attribute23 = data.Attribute23,
                            Attribute24 = data.Attribute24,
                            Attribute25 = data.Attribute25,
                            Attribute26 = data.Attribute26,
                            Attribute27 = data.Attribute27,
                            Attribute28 = data.Attribute28,
                            Attribute29 = data.Attribute29,
                            Attribute30 = data.Attribute30,
                            Attribute31 = data.Attribute31,
                            Attribute32 = data.Attribute32,
                            Attribute33 = data.Attribute33,
                            Attribute34 = data.Attribute34,
                            Attribute35 = data.Attribute35,
                            Attribute36 = data.Attribute36,
                            Attribute37 = data.Attribute37,
                            Attribute38 = data.Attribute38,
                            Attribute39 = data.Attribute39,
                            Attribute40 = data.Attribute40,
                            Attribute41 = data.Attribute41,
                            Attribute42 = data.Attribute42,
                            Attribute43 = data.Attribute43,
                            Attribute44 = data.Attribute44,
                            Attribute45 = data.Attribute45,
                            Attribute46 = data.Attribute46,
                            Attribute47 = data.Attribute47,
                            Attribute48 = data.Attribute48,
                            Attribute49 = data.Attribute49,
                            Attribute50 = data.Attribute50,
                            Attribute51 = data.Attribute51,
                            Attribute52 = data.Attribute52,
                            Attribute53 = data.Attribute53,
                            Attribute54 = data.Attribute54,
                            Attribute55 = data.Attribute55,
                            Attribute56 = data.Attribute56,
                            Attribute57 = data.Attribute57,
                            Attribute58 = data.Attribute58,
                            Attribute59 = data.Attribute59,
                            Attribute60 = data.Attribute60,
                            Attribute61 = data.Attribute61,
                            Attribute62 = data.Attribute62,
                            Attribute63 = data.Attribute63,
                            Attribute64 = data.Attribute64,
                            Attribute65 = data.Attribute65,
                            Attribute66 = data.Attribute66,
                            Attribute67 = data.Attribute67,
                            Attribute68 = data.Attribute68,
                            Attribute69 = data.Attribute69,
                            Attribute70 = data.Attribute70,
                            Attribute71 = data.Attribute71,
                            Attribute72 = data.Attribute72,
                            Attribute73 = data.Attribute73,
                            Attribute74 = data.Attribute74,
                            Attribute75 = data.Attribute75,
                            Attribute76 = data.Attribute76,
                            Attribute77 = data.Attribute77,
                            Attribute78 = data.Attribute78,
                            Attribute79 = data.Attribute79,
                            Attribute80 = data.Attribute80,
                            Attribute81 = data.Attribute81,
                            Attribute82 = data.Attribute82,
                            Attribute83 = data.Attribute83,
                            Attribute84 = data.Attribute84,
                            Attribute85 = data.Attribute85,
                            Attribute86 = data.Attribute86,
                            Attribute87 = data.Attribute87,
                            Attribute88 = data.Attribute88,
                            Attribute89 = data.Attribute89,
                            Attribute90 = data.Attribute90,
                            Attribute91 = data.Attribute91,
                            Attribute92 = data.Attribute92,
                            Attribute93 = data.Attribute93,
                            Attribute94 = data.Attribute94,
                            Attribute95 = data.Attribute95,
                            Attribute96 = data.Attribute96,
                            Attribute97 = data.Attribute97,
                            Attribute98 = data.Attribute98,
                            Attribute99 = data.Attribute99,
                            Attribute100 = data.Attribute100,
                            Attribute101 = data.Attribute101,
                            Attribute102 = data.Attribute102,
                            Attribute103 = data.Attribute103,
                            Attribute104 = data.Attribute104,
                            Attribute105 = data.Attribute105,
                            Attribute106 = data.Attribute106,
                            Attribute107 = data.Attribute107,
                            Attribute108 = data.Attribute108,
                            Attribute109 = data.Attribute109,
                            Attribute110 = data.Attribute110,
                            Attribute111 = data.Attribute111,
                            Attribute112 = data.Attribute112,
                            Attribute113 = data.Attribute113,
                            Attribute114 = data.Attribute114,
                            Attribute115 = data.Attribute115,
                            Attribute116 = data.Attribute116,
                            Attribute117 = data.Attribute117,
                            Attribute118 = data.Attribute118,
                            Attribute119 = data.Attribute119,
                            Attribute120 = data.Attribute120,
                            Attribute121 = data.Attribute121,
                            Attribute122 = data.Attribute122,
                            Attribute123 = data.Attribute123,
                            Attribute124 = data.Attribute124,
                            Attribute125 = data.Attribute125,
                            Attribute126 = data.Attribute126,
                            Attribute127 = data.Attribute127,
                            Attribute128 = data.Attribute128,
                            Attribute129 = data.Attribute129,
                            Attribute130 = data.Attribute130,
                            Attribute131 = data.Attribute131,
                            Attribute132 = data.Attribute132,
                            Attribute133 = data.Attribute133,
                            Attribute134 = data.Attribute134,
                            Attribute135 = data.Attribute135,
                            Attribute136 = data.Attribute136,
                            Attribute137 = data.Attribute137,
                            Attribute138 = data.Attribute138,
                            Attribute139 = data.Attribute139,
                            Attribute140 = data.Attribute140,
                            Attribute141 = data.Attribute141,
                            Attribute142 = data.Attribute142,
                            Attribute143 = data.Attribute143,
                            Attribute144 = data.Attribute144,
                            Attribute145 = data.Attribute145,
                            Attribute146 = data.Attribute146,
                            Attribute147 = data.Attribute147,
                            Attribute148 = data.Attribute148,
                            Attribute149 = data.Attribute149,
                            Attribute150 = data.Attribute150,

                            Attribute151 = data.Attribute151,
                            Attribute152 = data.Attribute152,
                            Attribute153 = data.Attribute153,
                            Attribute154 = data.Attribute154,
                            Attribute155 = data.Attribute155,
                            Attribute156 = data.Attribute156,
                            Attribute157 = data.Attribute157,
                            Attribute158 = data.Attribute158,
                            Attribute159 = data.Attribute159,
                            Attribute160 = data.Attribute160,
                            Attribute161 = data.Attribute161,
                            Attribute162 = data.Attribute162,
                            Attribute163 = data.Attribute163,
                            Attribute164 = data.Attribute164,
                            Attribute165 = data.Attribute165,
                            Attribute166 = data.Attribute166,
                            Attribute167 = data.Attribute167,
                            Attribute168 = data.Attribute168,
                            Attribute169 = data.Attribute169,
                            Attribute170 = data.Attribute170,
                            Attribute171 = data.Attribute171,
                            Attribute172 = data.Attribute172,
                            Attribute173 = data.Attribute173,
                            Attribute174 = data.Attribute174,
                            Attribute175 = data.Attribute175,
                            Attribute176 = data.Attribute176,
                            Attribute177 = data.Attribute177,
                            Attribute178 = data.Attribute178,
                            Attribute179 = data.Attribute179,
                            Attribute180 = data.Attribute180,
                            Attribute181 = data.Attribute181,
                            Attribute182 = data.Attribute182,
                            Attribute183 = data.Attribute183,
                            Attribute184 = data.Attribute184,
                            Attribute185 = data.Attribute185,
                            Attribute186 = data.Attribute186,
                            Attribute187 = data.Attribute187,
                            Attribute188 = data.Attribute188,
                            Attribute189 = data.Attribute189,
                            Attribute190 = data.Attribute190,
                            Attribute191 = data.Attribute191,
                            Attribute192 = data.Attribute192,
                            Attribute193 = data.Attribute193,
                            Attribute194 = data.Attribute194,
                            Attribute195 = data.Attribute195,
                            Attribute196 = data.Attribute196,
                            Attribute197 = data.Attribute197,
                            Attribute198 = data.Attribute198,
                            Attribute199 = data.Attribute199,
                            Attribute200 = data.Attribute200
                        };

                        #endregion
                    }
                    else
                    {
                        var batch = entities.BatchNumbers.FirstOrDefault(x => x.BatchNumberID == batchNumberId);
                        if (batch != null && batch.AttributeID != null)
                        {
                            var batchAttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == batch.AttributeID);
                            if (batchAttribute != null)
                            {
                                transData = new StockDetail();

                                // override with any batch attributes.
                                #region attributes

                                if (!string.IsNullOrEmpty(batchAttribute.Attribute1)) { transData.Attribute1 = batchAttribute.Attribute1; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute2)) { transData.Attribute2 = batchAttribute.Attribute2; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute3)) { transData.Attribute3 = batchAttribute.Attribute3; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute4)) { transData.Attribute4 = batchAttribute.Attribute4; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute5)) { transData.Attribute5 = batchAttribute.Attribute5; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute6)) { transData.Attribute6 = batchAttribute.Attribute6; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute7)) { transData.Attribute7 = batchAttribute.Attribute7; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute8)) { transData.Attribute8 = batchAttribute.Attribute8; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute9)) { transData.Attribute9 = batchAttribute.Attribute9; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute10)) { transData.Attribute10 = batchAttribute.Attribute10; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute11)) { transData.Attribute11 = batchAttribute.Attribute11; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute12)) { transData.Attribute12 = batchAttribute.Attribute12; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute13)) { transData.Attribute13 = batchAttribute.Attribute13; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute14)) { transData.Attribute14 = batchAttribute.Attribute14; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute15)) { transData.Attribute15 = batchAttribute.Attribute15; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute16)) { transData.Attribute16 = batchAttribute.Attribute16; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute17)) { transData.Attribute17 = batchAttribute.Attribute17; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute18)) { transData.Attribute18 = batchAttribute.Attribute18; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute19)) { transData.Attribute19 = batchAttribute.Attribute19; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute20)) { transData.Attribute20 = batchAttribute.Attribute20; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute21)) { transData.Attribute21 = batchAttribute.Attribute21; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute22)) { transData.Attribute22 = batchAttribute.Attribute22; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute23)) { transData.Attribute23 = batchAttribute.Attribute23; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute24)) { transData.Attribute24 = batchAttribute.Attribute24; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute25)) { transData.Attribute25 = batchAttribute.Attribute25; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute26)) { transData.Attribute26 = batchAttribute.Attribute26; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute27)) { transData.Attribute27 = batchAttribute.Attribute27; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute28)) { transData.Attribute28 = batchAttribute.Attribute28; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute29)) { transData.Attribute29 = batchAttribute.Attribute29; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute30)) { transData.Attribute30 = batchAttribute.Attribute30; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute31)) { transData.Attribute31 = batchAttribute.Attribute31; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute32)) { transData.Attribute32 = batchAttribute.Attribute32; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute33)) { transData.Attribute33 = batchAttribute.Attribute33; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute34)) { transData.Attribute34 = batchAttribute.Attribute34; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute35)) { transData.Attribute35 = batchAttribute.Attribute35; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute36)) { transData.Attribute36 = batchAttribute.Attribute36; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute37)) { transData.Attribute37 = batchAttribute.Attribute37; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute38)) { transData.Attribute38 = batchAttribute.Attribute38; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute39)) { transData.Attribute39 = batchAttribute.Attribute39; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute40)) { transData.Attribute40 = batchAttribute.Attribute40; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute41)) { transData.Attribute41 = batchAttribute.Attribute41; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute42)) { transData.Attribute42 = batchAttribute.Attribute42; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute43)) { transData.Attribute43 = batchAttribute.Attribute43; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute44)) { transData.Attribute44 = batchAttribute.Attribute44; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute45)) { transData.Attribute45 = batchAttribute.Attribute45; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute46)) { transData.Attribute46 = batchAttribute.Attribute46; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute47)) { transData.Attribute47 = batchAttribute.Attribute47; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute48)) { transData.Attribute48 = batchAttribute.Attribute48; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute49)) { transData.Attribute49 = batchAttribute.Attribute49; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute50)) { transData.Attribute50 = batchAttribute.Attribute50; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute51)) { transData.Attribute51 = batchAttribute.Attribute51; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute52)) { transData.Attribute52 = batchAttribute.Attribute52; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute53)) { transData.Attribute53 = batchAttribute.Attribute53; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute54)) { transData.Attribute54 = batchAttribute.Attribute54; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute55)) { transData.Attribute55 = batchAttribute.Attribute55; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute56)) { transData.Attribute56 = batchAttribute.Attribute56; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute57)) { transData.Attribute57 = batchAttribute.Attribute57; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute58)) { transData.Attribute58 = batchAttribute.Attribute58; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute59)) { transData.Attribute59 = batchAttribute.Attribute59; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute60)) { transData.Attribute60 = batchAttribute.Attribute60; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute61)) { transData.Attribute61 = batchAttribute.Attribute61; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute62)) { transData.Attribute62 = batchAttribute.Attribute62; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute63)) { transData.Attribute63 = batchAttribute.Attribute63; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute64)) { transData.Attribute64 = batchAttribute.Attribute64; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute65)) { transData.Attribute65 = batchAttribute.Attribute65; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute66)) { transData.Attribute66 = batchAttribute.Attribute66; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute67)) { transData.Attribute67 = batchAttribute.Attribute67; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute68)) { transData.Attribute68 = batchAttribute.Attribute68; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute69)) { transData.Attribute69 = batchAttribute.Attribute69; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute70)) { transData.Attribute70 = batchAttribute.Attribute70; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute71)) { transData.Attribute71 = batchAttribute.Attribute71; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute72)) { transData.Attribute72 = batchAttribute.Attribute72; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute73)) { transData.Attribute73 = batchAttribute.Attribute73; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute74)) { transData.Attribute74 = batchAttribute.Attribute74; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute75)) { transData.Attribute75 = batchAttribute.Attribute75; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute76)) { transData.Attribute76 = batchAttribute.Attribute76; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute77)) { transData.Attribute77 = batchAttribute.Attribute77; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute78)) { transData.Attribute78 = batchAttribute.Attribute78; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute79)) { transData.Attribute79 = batchAttribute.Attribute79; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute80)) { transData.Attribute80 = batchAttribute.Attribute80; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute81)) { transData.Attribute81 = batchAttribute.Attribute81; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute82)) { transData.Attribute82 = batchAttribute.Attribute82; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute83)) { transData.Attribute83 = batchAttribute.Attribute83; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute84)) { transData.Attribute84 = batchAttribute.Attribute84; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute85)) { transData.Attribute85 = batchAttribute.Attribute85; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute86)) { transData.Attribute86 = batchAttribute.Attribute86; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute87)) { transData.Attribute87 = batchAttribute.Attribute87; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute88)) { transData.Attribute88 = batchAttribute.Attribute88; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute89)) { transData.Attribute89 = batchAttribute.Attribute89; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute90)) { transData.Attribute90 = batchAttribute.Attribute90; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute91)) { transData.Attribute91 = batchAttribute.Attribute91; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute92)) { transData.Attribute92 = batchAttribute.Attribute92; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute93)) { transData.Attribute93 = batchAttribute.Attribute93; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute94)) { transData.Attribute94 = batchAttribute.Attribute94; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute95)) { transData.Attribute95 = batchAttribute.Attribute95; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute96)) { transData.Attribute96 = batchAttribute.Attribute96; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute97)) { transData.Attribute97 = batchAttribute.Attribute97; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute98)) { transData.Attribute98 = batchAttribute.Attribute98; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute99)) { transData.Attribute99 = batchAttribute.Attribute99; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute100)) { transData.Attribute100 = batchAttribute.Attribute100; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute101)) { transData.Attribute101 = batchAttribute.Attribute101; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute102)) { transData.Attribute102 = batchAttribute.Attribute102; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute103)) { transData.Attribute103 = batchAttribute.Attribute103; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute104)) { transData.Attribute104 = batchAttribute.Attribute104; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute105)) { transData.Attribute105 = batchAttribute.Attribute105; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute106)) { transData.Attribute106 = batchAttribute.Attribute106; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute107)) { transData.Attribute107 = batchAttribute.Attribute107; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute108)) { transData.Attribute108 = batchAttribute.Attribute108; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute109)) { transData.Attribute109 = batchAttribute.Attribute109; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute110)) { transData.Attribute110 = batchAttribute.Attribute110; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute111)) { transData.Attribute111 = batchAttribute.Attribute111; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute112)) { transData.Attribute112 = batchAttribute.Attribute112; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute113)) { transData.Attribute113 = batchAttribute.Attribute113; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute114)) { transData.Attribute114 = batchAttribute.Attribute114; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute115)) { transData.Attribute115 = batchAttribute.Attribute115; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute116)) { transData.Attribute116 = batchAttribute.Attribute116; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute117)) { transData.Attribute117 = batchAttribute.Attribute117; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute118)) { transData.Attribute118 = batchAttribute.Attribute118; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute119)) { transData.Attribute119 = batchAttribute.Attribute119; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute120)) { transData.Attribute120 = batchAttribute.Attribute120; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute121)) { transData.Attribute121 = batchAttribute.Attribute121; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute122)) { transData.Attribute122 = batchAttribute.Attribute122; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute123)) { transData.Attribute123 = batchAttribute.Attribute123; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute124)) { transData.Attribute124 = batchAttribute.Attribute124; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute125)) { transData.Attribute125 = batchAttribute.Attribute125; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute126)) { transData.Attribute126 = batchAttribute.Attribute126; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute127)) { transData.Attribute127 = batchAttribute.Attribute127; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute128)) { transData.Attribute128 = batchAttribute.Attribute128; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute129)) { transData.Attribute129 = batchAttribute.Attribute129; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute130)) { transData.Attribute130 = batchAttribute.Attribute130; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute131)) { transData.Attribute131 = batchAttribute.Attribute131; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute132)) { transData.Attribute132 = batchAttribute.Attribute132; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute133)) { transData.Attribute133 = batchAttribute.Attribute133; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute134)) { transData.Attribute134 = batchAttribute.Attribute134; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute135)) { transData.Attribute135 = batchAttribute.Attribute135; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute136)) { transData.Attribute136 = batchAttribute.Attribute136; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute137)) { transData.Attribute137 = batchAttribute.Attribute137; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute138)) { transData.Attribute138 = batchAttribute.Attribute138; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute139)) { transData.Attribute139 = batchAttribute.Attribute139; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute140)) { transData.Attribute140 = batchAttribute.Attribute140; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute141)) { transData.Attribute141 = batchAttribute.Attribute141; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute142)) { transData.Attribute142 = batchAttribute.Attribute142; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute143)) { transData.Attribute143 = batchAttribute.Attribute143; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute144)) { transData.Attribute144 = batchAttribute.Attribute144; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute145)) { transData.Attribute145 = batchAttribute.Attribute145; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute146)) { transData.Attribute146 = batchAttribute.Attribute146; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute147)) { transData.Attribute147 = batchAttribute.Attribute147; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute148)) { transData.Attribute148 = batchAttribute.Attribute148; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute149)) { transData.Attribute149 = batchAttribute.Attribute149; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute150)) { transData.Attribute150 = batchAttribute.Attribute150; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute151)) { transData.Attribute151 = batchAttribute.Attribute151; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute152)) { transData.Attribute152 = batchAttribute.Attribute152; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute153)) { transData.Attribute153 = batchAttribute.Attribute153; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute154)) { transData.Attribute154 = batchAttribute.Attribute154; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute155)) { transData.Attribute155 = batchAttribute.Attribute155; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute156)) { transData.Attribute156 = batchAttribute.Attribute156; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute157)) { transData.Attribute157 = batchAttribute.Attribute157; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute158)) { transData.Attribute158 = batchAttribute.Attribute158; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute159)) { transData.Attribute159 = batchAttribute.Attribute159; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute160)) { transData.Attribute160 = batchAttribute.Attribute160; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute161)) { transData.Attribute161 = batchAttribute.Attribute161; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute162)) { transData.Attribute162 = batchAttribute.Attribute162; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute163)) { transData.Attribute163 = batchAttribute.Attribute163; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute164)) { transData.Attribute164 = batchAttribute.Attribute164; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute165)) { transData.Attribute165 = batchAttribute.Attribute165; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute166)) { transData.Attribute166 = batchAttribute.Attribute166; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute167)) { transData.Attribute167 = batchAttribute.Attribute167; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute168)) { transData.Attribute168 = batchAttribute.Attribute168; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute169)) { transData.Attribute169 = batchAttribute.Attribute169; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute170)) { transData.Attribute170 = batchAttribute.Attribute170; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute171)) { transData.Attribute171 = batchAttribute.Attribute171; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute172)) { transData.Attribute172 = batchAttribute.Attribute172; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute173)) { transData.Attribute173 = batchAttribute.Attribute173; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute174)) { transData.Attribute174 = batchAttribute.Attribute174; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute175)) { transData.Attribute175 = batchAttribute.Attribute175; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute176)) { transData.Attribute176 = batchAttribute.Attribute176; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute177)) { transData.Attribute177 = batchAttribute.Attribute177; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute178)) { transData.Attribute178 = batchAttribute.Attribute178; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute179)) { transData.Attribute179 = batchAttribute.Attribute179; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute180)) { transData.Attribute180 = batchAttribute.Attribute180; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute181)) { transData.Attribute181 = batchAttribute.Attribute181; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute182)) { transData.Attribute182 = batchAttribute.Attribute182; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute183)) { transData.Attribute183 = batchAttribute.Attribute183; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute184)) { transData.Attribute184 = batchAttribute.Attribute184; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute185)) { transData.Attribute185 = batchAttribute.Attribute185; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute186)) { transData.Attribute186 = batchAttribute.Attribute186; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute187)) { transData.Attribute187 = batchAttribute.Attribute187; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute188)) { transData.Attribute188 = batchAttribute.Attribute188; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute189)) { transData.Attribute189 = batchAttribute.Attribute189; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute190)) { transData.Attribute190 = batchAttribute.Attribute190; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute191)) { transData.Attribute191 = batchAttribute.Attribute191; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute192)) { transData.Attribute192 = batchAttribute.Attribute192; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute193)) { transData.Attribute193 = batchAttribute.Attribute193; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute194)) { transData.Attribute194 = batchAttribute.Attribute194; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute195)) { transData.Attribute195 = batchAttribute.Attribute195; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute196)) { transData.Attribute196 = batchAttribute.Attribute196; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute197)) { transData.Attribute197 = batchAttribute.Attribute197; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute198)) { transData.Attribute198 = batchAttribute.Attribute198; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute199)) { transData.Attribute199 = batchAttribute.Attribute199; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute200)) { transData.Attribute200 = batchAttribute.Attribute200; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute201)) { transData.Attribute201 = batchAttribute.Attribute201; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute202)) { transData.Attribute202 = batchAttribute.Attribute202; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute203)) { transData.Attribute203 = batchAttribute.Attribute203; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute204)) { transData.Attribute204 = batchAttribute.Attribute204; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute205)) { transData.Attribute205 = batchAttribute.Attribute205; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute206)) { transData.Attribute206 = batchAttribute.Attribute206; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute207)) { transData.Attribute207 = batchAttribute.Attribute207; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute208)) { transData.Attribute208 = batchAttribute.Attribute208; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute209)) { transData.Attribute209 = batchAttribute.Attribute209; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute210)) { transData.Attribute210 = batchAttribute.Attribute210; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute211)) { transData.Attribute211 = batchAttribute.Attribute211; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute212)) { transData.Attribute212 = batchAttribute.Attribute212; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute213)) { transData.Attribute213 = batchAttribute.Attribute213; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute214)) { transData.Attribute214 = batchAttribute.Attribute214; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute215)) { transData.Attribute215 = batchAttribute.Attribute215; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute216)) { transData.Attribute216 = batchAttribute.Attribute216; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute217)) { transData.Attribute217 = batchAttribute.Attribute217; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute218)) { transData.Attribute218 = batchAttribute.Attribute218; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute219)) { transData.Attribute219 = batchAttribute.Attribute219; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute220)) { transData.Attribute220 = batchAttribute.Attribute220; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute221)) { transData.Attribute221 = batchAttribute.Attribute221; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute222)) { transData.Attribute222 = batchAttribute.Attribute222; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute223)) { transData.Attribute223 = batchAttribute.Attribute223; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute224)) { transData.Attribute224 = batchAttribute.Attribute224; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute225)) { transData.Attribute225 = batchAttribute.Attribute225; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute226)) { transData.Attribute226 = batchAttribute.Attribute226; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute227)) { transData.Attribute227 = batchAttribute.Attribute227; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute228)) { transData.Attribute228 = batchAttribute.Attribute228; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute229)) { transData.Attribute229 = batchAttribute.Attribute229; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute230)) { transData.Attribute230 = batchAttribute.Attribute230; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute231)) { transData.Attribute231 = batchAttribute.Attribute231; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute232)) { transData.Attribute232 = batchAttribute.Attribute232; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute233)) { transData.Attribute233 = batchAttribute.Attribute233; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute234)) { transData.Attribute234 = batchAttribute.Attribute234; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute235)) { transData.Attribute235 = batchAttribute.Attribute235; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute236)) { transData.Attribute236 = batchAttribute.Attribute236; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute237)) { transData.Attribute237 = batchAttribute.Attribute237; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute238)) { transData.Attribute238 = batchAttribute.Attribute238; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute239)) { transData.Attribute239 = batchAttribute.Attribute239; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute240)) { transData.Attribute240 = batchAttribute.Attribute240; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute241)) { transData.Attribute241 = batchAttribute.Attribute241; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute242)) { transData.Attribute242 = batchAttribute.Attribute242; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute243)) { transData.Attribute243 = batchAttribute.Attribute243; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute244)) { transData.Attribute244 = batchAttribute.Attribute244; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute245)) { transData.Attribute245 = batchAttribute.Attribute245; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute246)) { transData.Attribute246 = batchAttribute.Attribute246; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute247)) { transData.Attribute247 = batchAttribute.Attribute247; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute248)) { transData.Attribute248 = batchAttribute.Attribute248; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute249)) { transData.Attribute249 = batchAttribute.Attribute249; }
                                if (!string.IsNullOrEmpty(batchAttribute.Attribute250)) { transData.Attribute250 = batchAttribute.Attribute250; }

                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transData;
        }

        /// <summary>
        /// Gets the last attribute for the stock item.
        /// </summary>
        /// <param name="serial">The transaction id.</param>
        /// <returns>The last transaction for the input module</returns>
        public StockDetail GetSerialAttributeData(int serial)
        {
            StockDetail transData = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transactionData =
                       entities.StockTransactions
                           .Where(x => x.StockTransactionID == serial
                               && x.Deleted == null)
                               .OrderByDescending(x => x.StockTransactionID).FirstOrDefault();

                    if (transactionData != null && transactionData.Attribute != null)
                    {
                        var data = transactionData.Attribute;

                        #region attributes

                        transData = new StockDetail
                        {
                            Attribute1 = data.Attribute1,
                            Attribute2 = data.Attribute2,
                            Attribute3 = data.Attribute3,
                            Attribute4 = data.Attribute4,
                            Attribute5 = data.Attribute5,
                            Attribute6 = data.Attribute6,
                            Attribute7 = data.Attribute7,
                            Attribute8 = data.Attribute8,
                            Attribute9 = data.Attribute9,
                            Attribute10 = data.Attribute10,
                            Attribute11 = data.Attribute11,
                            Attribute12 = data.Attribute12,
                            Attribute13 = data.Attribute13,
                            Attribute14 = data.Attribute14,
                            Attribute15 = data.Attribute15,
                            Attribute16 = data.Attribute16,
                            Attribute17 = data.Attribute17,
                            Attribute18 = data.Attribute18,
                            Attribute19 = data.Attribute19,
                            Attribute20 = data.Attribute20,
                            Attribute21 = data.Attribute21,
                            Attribute22 = data.Attribute22,
                            Attribute23 = data.Attribute23,
                            Attribute24 = data.Attribute24,
                            Attribute25 = data.Attribute25,
                            Attribute26 = data.Attribute26,
                            Attribute27 = data.Attribute27,
                            Attribute28 = data.Attribute28,
                            Attribute29 = data.Attribute29,
                            Attribute30 = data.Attribute30,
                            Attribute31 = data.Attribute31,
                            Attribute32 = data.Attribute32,
                            Attribute33 = data.Attribute33,
                            Attribute34 = data.Attribute34,
                            Attribute35 = data.Attribute35,
                            Attribute36 = data.Attribute36,
                            Attribute37 = data.Attribute37,
                            Attribute38 = data.Attribute38,
                            Attribute39 = data.Attribute39,
                            Attribute40 = data.Attribute40,
                            Attribute41 = data.Attribute41,
                            Attribute42 = data.Attribute42,
                            Attribute43 = data.Attribute43,
                            Attribute44 = data.Attribute44,
                            Attribute45 = data.Attribute45,
                            Attribute46 = data.Attribute46,
                            Attribute47 = data.Attribute47,
                            Attribute48 = data.Attribute48,
                            Attribute49 = data.Attribute49,
                            Attribute50 = data.Attribute50,
                            Attribute51 = data.Attribute51,
                            Attribute52 = data.Attribute52,
                            Attribute53 = data.Attribute53,
                            Attribute54 = data.Attribute54,
                            Attribute55 = data.Attribute55,
                            Attribute56 = data.Attribute56,
                            Attribute57 = data.Attribute57,
                            Attribute58 = data.Attribute58,
                            Attribute59 = data.Attribute59,
                            Attribute60 = data.Attribute60,
                            Attribute61 = data.Attribute61,
                            Attribute62 = data.Attribute62,
                            Attribute63 = data.Attribute63,
                            Attribute64 = data.Attribute64,
                            Attribute65 = data.Attribute65,
                            Attribute66 = data.Attribute66,
                            Attribute67 = data.Attribute67,
                            Attribute68 = data.Attribute68,
                            Attribute69 = data.Attribute69,
                            Attribute70 = data.Attribute70,
                            Attribute71 = data.Attribute71,
                            Attribute72 = data.Attribute72,
                            Attribute73 = data.Attribute73,
                            Attribute74 = data.Attribute74,
                            Attribute75 = data.Attribute75,
                            Attribute76 = data.Attribute76,
                            Attribute77 = data.Attribute77,
                            Attribute78 = data.Attribute78,
                            Attribute79 = data.Attribute79,
                            Attribute80 = data.Attribute80,
                            Attribute81 = data.Attribute81,
                            Attribute82 = data.Attribute82,
                            Attribute83 = data.Attribute83,
                            Attribute84 = data.Attribute84,
                            Attribute85 = data.Attribute85,
                            Attribute86 = data.Attribute86,
                            Attribute87 = data.Attribute87,
                            Attribute88 = data.Attribute88,
                            Attribute89 = data.Attribute89,
                            Attribute90 = data.Attribute90,
                            Attribute91 = data.Attribute91,
                            Attribute92 = data.Attribute92,
                            Attribute93 = data.Attribute93,
                            Attribute94 = data.Attribute94,
                            Attribute95 = data.Attribute95,
                            Attribute96 = data.Attribute96,
                            Attribute97 = data.Attribute97,
                            Attribute98 = data.Attribute98,
                            Attribute99 = data.Attribute99,
                            Attribute100 = data.Attribute100,
                            Attribute101 = data.Attribute101,
                            Attribute102 = data.Attribute102,
                            Attribute103 = data.Attribute103,
                            Attribute104 = data.Attribute104,
                            Attribute105 = data.Attribute105,
                            Attribute106 = data.Attribute106,
                            Attribute107 = data.Attribute107,
                            Attribute108 = data.Attribute108,
                            Attribute109 = data.Attribute109,
                            Attribute110 = data.Attribute110,
                            Attribute111 = data.Attribute111,
                            Attribute112 = data.Attribute112,
                            Attribute113 = data.Attribute113,
                            Attribute114 = data.Attribute114,
                            Attribute115 = data.Attribute115,
                            Attribute116 = data.Attribute116,
                            Attribute117 = data.Attribute117,
                            Attribute118 = data.Attribute118,
                            Attribute119 = data.Attribute119,
                            Attribute120 = data.Attribute120,
                            Attribute121 = data.Attribute121,
                            Attribute122 = data.Attribute122,
                            Attribute123 = data.Attribute123,
                            Attribute124 = data.Attribute124,
                            Attribute125 = data.Attribute125,
                            Attribute126 = data.Attribute126,
                            Attribute127 = data.Attribute127,
                            Attribute128 = data.Attribute128,
                            Attribute129 = data.Attribute129,
                            Attribute130 = data.Attribute130,
                            Attribute131 = data.Attribute131,
                            Attribute132 = data.Attribute132,
                            Attribute133 = data.Attribute133,
                            Attribute134 = data.Attribute134,
                            Attribute135 = data.Attribute135,
                            Attribute136 = data.Attribute136,
                            Attribute137 = data.Attribute137,
                            Attribute138 = data.Attribute138,
                            Attribute139 = data.Attribute139,
                            Attribute140 = data.Attribute140,
                            Attribute141 = data.Attribute141,
                            Attribute142 = data.Attribute142,
                            Attribute143 = data.Attribute143,
                            Attribute144 = data.Attribute144,
                            Attribute145 = data.Attribute145,
                            Attribute146 = data.Attribute146,
                            Attribute147 = data.Attribute147,
                            Attribute148 = data.Attribute148,
                            Attribute149 = data.Attribute149,
                            Attribute150 = data.Attribute150,

                            Attribute151 = data.Attribute151,
                            Attribute152 = data.Attribute152,
                            Attribute153 = data.Attribute153,
                            Attribute154 = data.Attribute154,
                            Attribute155 = data.Attribute155,
                            Attribute156 = data.Attribute156,
                            Attribute157 = data.Attribute157,
                            Attribute158 = data.Attribute158,
                            Attribute159 = data.Attribute159,
                            Attribute160 = data.Attribute160,
                            Attribute161 = data.Attribute161,
                            Attribute162 = data.Attribute162,
                            Attribute163 = data.Attribute163,
                            Attribute164 = data.Attribute164,
                            Attribute165 = data.Attribute165,
                            Attribute166 = data.Attribute166,
                            Attribute167 = data.Attribute167,
                            Attribute168 = data.Attribute168,
                            Attribute169 = data.Attribute169,
                            Attribute170 = data.Attribute170,
                            Attribute171 = data.Attribute171,
                            Attribute172 = data.Attribute172,
                            Attribute173 = data.Attribute173,
                            Attribute174 = data.Attribute174,
                            Attribute175 = data.Attribute175,
                            Attribute176 = data.Attribute176,
                            Attribute177 = data.Attribute177,
                            Attribute178 = data.Attribute178,
                            Attribute179 = data.Attribute179,
                            Attribute180 = data.Attribute180,
                            Attribute181 = data.Attribute181,
                            Attribute182 = data.Attribute182,
                            Attribute183 = data.Attribute183,
                            Attribute184 = data.Attribute184,
                            Attribute185 = data.Attribute185,
                            Attribute186 = data.Attribute186,
                            Attribute187 = data.Attribute187,
                            Attribute188 = data.Attribute188,
                            Attribute189 = data.Attribute189,
                            Attribute190 = data.Attribute190,
                            Attribute191 = data.Attribute191,
                            Attribute192 = data.Attribute192,
                            Attribute193 = data.Attribute193,
                            Attribute194 = data.Attribute194,
                            Attribute195 = data.Attribute195,
                            Attribute196 = data.Attribute196,
                            Attribute197 = data.Attribute197,
                            Attribute198 = data.Attribute198,
                            Attribute199 = data.Attribute199,
                            Attribute200 = data.Attribute200,

                            Attribute201 = data.Attribute201,
                            Attribute202 = data.Attribute202,
                            Attribute203 = data.Attribute203,
                            Attribute204 = data.Attribute204,
                            Attribute205 = data.Attribute205,
                            Attribute206 = data.Attribute206,
                            Attribute207 = data.Attribute207,
                            Attribute208 = data.Attribute208,
                            Attribute209 = data.Attribute209,
                            Attribute210 = data.Attribute210,
                            Attribute211 = data.Attribute211,
                            Attribute212 = data.Attribute212,
                            Attribute213 = data.Attribute213,
                            Attribute214 = data.Attribute214,
                            Attribute215 = data.Attribute215,
                            Attribute216 = data.Attribute216,
                            Attribute217 = data.Attribute217,
                            Attribute218 = data.Attribute218,
                            Attribute219 = data.Attribute219,
                            Attribute220 = data.Attribute220,
                            Attribute221 = data.Attribute221,
                            Attribute222 = data.Attribute222,
                            Attribute223 = data.Attribute223,
                            Attribute224 = data.Attribute224,
                            Attribute225 = data.Attribute225,
                            Attribute226 = data.Attribute226,
                            Attribute227 = data.Attribute227,
                            Attribute228 = data.Attribute228,
                            Attribute229 = data.Attribute229,
                            Attribute230 = data.Attribute230,
                            Attribute231 = data.Attribute231,
                            Attribute232 = data.Attribute232,
                            Attribute233 = data.Attribute233,
                            Attribute234 = data.Attribute234,
                            Attribute235 = data.Attribute235,
                            Attribute236 = data.Attribute236,
                            Attribute237 = data.Attribute237,
                            Attribute238 = data.Attribute238,
                            Attribute239 = data.Attribute239,
                            Attribute240 = data.Attribute240,
                            Attribute241 = data.Attribute241,
                            Attribute242 = data.Attribute242,
                            Attribute243 = data.Attribute243,
                            Attribute244 = data.Attribute244,
                            Attribute245 = data.Attribute245,
                            Attribute246 = data.Attribute246,
                            Attribute247 = data.Attribute247,
                            Attribute248 = data.Attribute248,
                            Attribute249 = data.Attribute249,
                            Attribute250 = data.Attribute250,
                            CarcassNumber = data.CarcassNumber,
                            Eartag = data.Eartag,
                            HerdNo = data.Herd,
                            NouCategoryID = data.Category,
                            GradingDate = data.GradingDate,
                            SequencedDate = data.SequencedDate,
                            SupplierID = data.SupplierID
                        };
                        
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transData;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionIncIntakeId(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTrans = (from intakeDetail in entities.APGoodsReceiptDetails
                                   join stock in entities.StockTransactions
                                       on intakeDetail.APGoodsReceiptDetailID equals stock.MasterTableID
                                   where stock.Serial == serial
                                   select new { Stock = stock, Detail = intakeDetail }).FirstOrDefault();

                    if (dbTrans != null && dbTrans.Stock != null && dbTrans.Detail != null)
                    {
                        dbTrans.Stock.LabelID = dbTrans.Detail.APGoodsReceiptID.ToString();
                        return dbTrans.Stock;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }


        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransaction(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.FirstOrDefault(
                            x => x.Serial == serial && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionFromAttribute(int attributeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.FirstOrDefault(
                            x => x.AttributeID == attributeId && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }


        /// <summary>
        /// Gets the stock label.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        public StockTransaction GetStockTransactionUncomsumed(int serial)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.FirstOrDefault(
                            x => x.Serial == serial && x.Deleted == null && x.Consumed == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the product data between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        public IList<ReportStockTransactionData_Result> GetStockTransactionData(int customerId, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetStockTransactionData(): Retrieving the transaction data between:{0}-{1}", start, end));
            var data = new List<ReportStockTransactionData_Result>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    data = entities.ReportStockTransactionData(customerId, start, end).ToList();
                    this.Log.LogDebug(this.GetType(), "Data retrieved");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Gets a set of transactions.
        /// </summary>
        /// <param name="intakeId">An associated intake/lairage id.</param>
        /// <param name="prOrderId">An associated production id.</param>
        /// <param name="dispatchId">An associated dispatch id.</param>
        /// <param name="serial">A individual transaction to search for.</param>
        /// <param name="start">The start of a range of dates to search from.</param>
        /// <param name="end">The end of a range od dates to search to.</param>
        /// <returns>A set of transactions.</returns>
        public IList<StockDetail> GetTransactions(
            int? intakeId = null,
            int? prOrderId = null,
            int? dispatchId = null,
            int? serial = null,
            DateTime? start = null,
            DateTime? end = null)
        {
            var transToReturn = new List<StockDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.App_GetTransactions(intakeId, prOrderId, dispatchId, serial, start, end)
                         .ToList();

                    foreach (var transaction in trans)
                    {
                        transToReturn.Add(new StockDetail
                        {
                            StockTransactionID = transaction.StockTransactionID,
                            BatchNumberID = transaction.BatchNumberID,
                            Serial = transaction.Serial,
                            TransactionDate = transaction.TransactionDate,
                            TransactionQty = Math.Round(transaction.TransactionQTY.ToDecimal(), 0),
                            TransactionWeight = Math.Round(transaction.TransactionWeight.ToDecimal(), 2),
                            Tare = transaction.Tare.ToDecimal(),
                            ManualWeight = transaction.ManualWeight,
                            Pieces = transaction.Pieces,
                            WarehouseID = transaction.WarehouseID,
                            INMasterID = transaction.INMasterID,
                            Consumed = transaction.Consumed,
                            IsStockLocation = transaction.IsStockLocation,
                            Deleted = transaction.Deleted,
                            DeviceMasterID = transaction.DeviceMasterID,
                            UserMasterID = transaction.UserMasterID,
                            AttributeID = transaction.AttributeID,
                            BPMasterID = transaction.BPMasterID,
                            ProcessID = transaction.ProcessID,
                            MasterTableID = transaction.DocumentID,
                            DocumentReference = transaction.Document_Reference,
                            User = transaction.User,
                            Device = transaction.Device,
                            Warehouse = transaction.Warehouse,
                            Partner = transaction.Partner,
                            Process = transaction.Process,
                            Product = transaction.Product,
                            ProductCode = transaction.Product_Code,
                            Manual = transaction.ManualWeight,
                            NotesCollection = entities.Notes.Where(x => x.MasterTableID == transaction.StockTransactionID
                                                              && x.NoteType == Constant.TransactionEdit && x.Deleted == null).ToList(),
                            StockDetails = (from detail in entities.TransactionEdits.Where(x => x.StockTransactionID == transaction.StockTransactionID)
                                            select new StockDetail
                                            {
                                                Serial = detail.Serial,
                                                INMasterID = (int)detail.INMasterID,
                                                TransactionWeight = detail.TransactionWeight,
                                                TransactionQty = detail.TransactionQTY,
                                                EditDate = detail.EditDate,
                                                UserMasterID = detail.UserMasterID_Edit,
                                                EditStatusID = detail.PostEdit,
                                                DeviceMasterID = detail.DeviceID_Edit
                                            }).ToList()
                        });
                    }
                }

                foreach (var trans in transToReturn)
                {
                    trans.SetNotes();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transToReturn;
        }

        /// <summary>
        /// Updates edited transactions.
        /// </summary>
        /// <param name="transactions">The transactions to edit.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateTransactions(IList<StockDetail> transactions)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var transaction in transactions)
                    {
                        entities.App_UpdateTransaction(
                            transaction.StockTransactionID,
                            transaction.INMasterID,
                            transaction.TransactionWeight,
                            transaction.TransactionQty,
                            transaction.UnitPrice,
                            false,
                            NouvemGlobal.UserId,
                            NouvemGlobal.DeviceId);

                        if (transaction.Notes != null)
                        {
                            entities.Notes.Add(new Note
                            {
                                Notes = transaction.Notes,
                                MasterTableID = transaction.StockTransactionID.ToInt(),
                                CreationDate = DateTime.Now,
                                DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                UserMasterID = NouvemGlobal.UserId.ToInt(),
                                NoteType = Constant.TransactionEdit
                            });
                        }

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public StockTransaction RecreateTransactionWithWgt(StockTransaction transaction)
        {
            var newTransaction = this.CreateTransaction(transaction);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localTrans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == transaction.StockTransactionID);
                    if (localTrans != null)
                    {
                        localTrans.Consumed = DateTime.Now;
                    }

                    entities.StockTransactions.Add(newTransaction);
                    entities.SaveChanges();
                    return newTransaction;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public bool AddStockTransaction(StockTransaction transaction)
        {
            this.Log.LogDebug(this.GetType(), "Adding stock transaction");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.StockTransactions.Add(transaction);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a stock transaction.
        /// </summary>
        /// <param name="transaction">A flag, indicating a successful addition or not.</param>
        public int AddTransaction(StockTransaction transaction)
        {
            this.Log.LogDebug(this.GetType(), "Adding stock transaction");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.StockTransactions.Add(transaction);
                    entities.SaveChanges();
                    transaction.Serial = transaction.StockTransactionID;
                    entities.SaveChanges();
                    return transaction.StockTransactionID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Adds a pallet transaction.
        /// </summary>
        /// <param name="pallet">The pallet to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddPallet(PalletStock pallet)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.PalletStocks.Add(pallet);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the stock on a pallet.
        /// </summary>
        /// <param name="id">The pallet id.</param>
        /// <returns>All the stock on a pallet.</returns>
        public IList<StockDetail> GetPalletDetails(int id, bool scanOn)
        {
            var details = new List<StockDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (scanOn)
                    {
                        details = (from stock in entities.StockTransactions.Where(x => x.StockTransactionID_Pallet == id
                                                                                       && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId
                                                                                       && x.Consumed == null)
                                   select new StockDetail
                                   {
                                       StockTransactionID = stock.StockTransactionID,
                                       Serial = stock.Serial,
                                       TransactionWeight = stock.TransactionWeight,
                                       TransactionQty = stock.TransactionQTY,
                                       ProductID = stock.INMasterID
                                   }).ToList();
                    }
                    else
                    {
                        details = (from stock in entities.StockTransactions.Where(x => x.StockTransactionID_Pallet == id && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId)
                                   select new StockDetail
                                   {
                                       StockTransactionID = stock.StockTransactionID,
                                       Serial = stock.Serial,
                                       TransactionWeight = stock.TransactionWeight,
                                       TransactionQty = stock.TransactionQTY,
                                       ProductID = stock.INMasterID
                                   }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Adds/remove stock from a pallet.
        /// </summary>
        /// <param name="id">The stock id.</param>
        /// <param name="palletId">The pallet id.</param>
        /// <param name="scanOn">The scan on/off flag.</param>
        /// <returns>Flag, as to successful scan on/off.</returns>
        public StockDetail UpdatePalletTransaction(int id, StockDetail pallet, bool scanOn, bool ignoreConsume)
        {
            var detail = new StockDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == id);
                    if (trans != null)
                    {
                        if (trans.Comments != null && trans.Comments.Equals(Constant.Pallet))
                        {
                            detail.WarehouseId = trans.WarehouseID;
                            detail.LogInfo = Constant.Pallet;
                            return detail;
                        }

                        trans =
                            entities.StockTransactions.FirstOrDefault(x => x.Serial == id
                                                                           && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId
                                                                           && x.Deleted == null);
                        var creatingFromWeighedDispatchTransaction = false;
                        detail.Generic1 = string.Empty;
                        if (trans == null)
                        {
                            trans =
                                entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == id
                                                                               && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId
                                                                               && x.Deleted == null);
                            if (trans != null)
                            {
                                var packingTrans = this.CreateTransaction(trans);
                                packingTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionReceiptId;
                                packingTrans.Deleted = DateTime.Now;
                                trans = packingTrans;
                                entities.StockTransactions.Add(packingTrans);
                                creatingFromWeighedDispatchTransaction = true;
                                var prOrderBatch = entities.PROrders.FirstOrDefault(x => x.BatchNumberID == trans.BatchNumberID);
                                if (prOrderBatch != null)
                                {
                                    detail.Generic1 = prOrderBatch.Reference;
                                }
                            }
                        }

                        if (trans == null)
                        {
                            detail.Error = string.Format("Stock {0} cannot be moved onto pallet {1} as it cannot be found", id, pallet.StockTransactionID);
                            return detail;
                        }

                        if (trans.Consumed != null && !ignoreConsume)
                        {
                            detail.Error = string.Format("Stock {0} cannot be moved onto pallet {1} as it was consumed on {2}", id, pallet.StockTransactionID, trans.Consumed);
                            return detail;
                        }

                        if (scanOn)
                        {
                            if (trans.StockTransactionID_Pallet != null)
                            {
                                detail.LogInfo = string.Format("Moving stock {0} off pallet {1} onto pallet {2}", id, trans.StockTransactionID_Pallet, pallet.StockTransactionID);
                            }

                            if (!creatingFromWeighedDispatchTransaction)
                            {
                                if (trans.Deleted != null)
                                {
                                    detail.LogInfo = string.Format("Undeleting stock {0}. Putting on to pallet {1}", id, pallet.StockTransactionID);
                                }

                                trans.Deleted = null;
                            }

                            trans.StockTransactionID_Pallet = pallet.StockTransactionID.ToInt();
                            trans.WarehouseID = pallet.WarehouseId;
                        }
                        else
                        {
                            trans.StockTransactionID_Pallet = null;
                        }

                        entities.SaveChanges();
                        detail.Serial = id;
                        detail.ProductID = trans.INMasterID;
                        detail.TransactionWeight = trans.TransactionWeight;
                        detail.TransactionQty = trans.TransactionQTY;
                        if (!creatingFromWeighedDispatchTransaction)
                        {
                            var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == trans.MasterTableID);
                            if (prOrder != null)
                            {
                                detail.Generic1 = prOrder.Reference;
                            }
                        }
                    }
                    else
                    {
                        detail.Error = string.Format("Stock {0} cannot be moved onto pallet {1} as it cannot be found", id, pallet.StockTransactionID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                detail.Error = ex.Message;
            }

            return detail;
        }

        /// <summary>
        /// Unconsumes a stock transaction.
        /// </summary>
        /// <param name="transactionId">A flag, indicating a successful unconsumeor not.</param>
        public bool UnConsumePalletTransaction(int transactionId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transaction =
                        entities.PalletStocks.FirstOrDefault(x => x.StockTransactionID == transactionId);
                    if (transaction != null)
                    {
                        transaction.Consumed = null;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Unconsumes a stock transaction.
        /// </summary>
        /// <param name="transactionId">A flag, indicating a successful unconsumeor not.</param>
        public bool UnConsumeTransaction(int transactionId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var transaction =
                        entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == transactionId);
                    if (transaction != null)
                    {
                        var localTransaction = this.CreateDeepCopyTransaction(transaction);
                        localTransaction.Consumed = null;
                        entities.StockTransactions.Add(localTransaction);
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the stored label printer number.
        /// </summary>
        /// <param name="labelId">The label id.</param>
        /// <returns>The stored label printer number.</returns>
        public int? GetTransactionPrinter(int labelId)
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var storedlabel = entities.StoredLabels.FirstOrDefault(x => x.StoredLabelID == labelId);
                    if (storedlabel != null)
                    {
                        return storedlabel.PrinterNo;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        #region private

        /// <summary>
        /// Creates a deep copy transaction.
        /// </summary>
        /// <param name="transaction">The transaction to deep copy</param>
        /// <returns>A deep copy transaction.</returns>
        private StockTransaction CreateDeepCopyTransaction(StockTransaction transaction)
        {
            return new StockTransaction
            {
                MasterTableID = transaction.MasterTableID,
                BatchNumberID = transaction.BatchNumberID,
                Serial = transaction.Serial,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = transaction.NouTransactionTypeID,
                TransactionQTY = transaction.TransactionQTY,
                TransactionWeight = transaction.TransactionWeight,
                GrossWeight = transaction.GrossWeight,
                Tare = transaction.Tare,
                Pieces = transaction.Pieces,
                Alibi = transaction.Alibi,
                WarehouseID = transaction.WarehouseID,
                INMasterID = transaction.INMasterID,
                ContainerID = transaction.ContainerID,
                Consumed = transaction.Consumed,
                ManualWeight = transaction.ManualWeight,
                DeviceMasterID = transaction.DeviceMasterID,
                UserMasterID = transaction.UserMasterID,
                IsBox = transaction.IsBox,
                InLocation = transaction.InLocation,
                StockTransactionID_Container = transaction.StockTransactionID_Container,
                LabelID = transaction.LabelID,
                Reference = transaction.Reference,
                Deleted = transaction.Deleted
            };
        }

        /// <summary>
        /// Checks if the dispatch line has transactions recorded against it.
        /// </summary>
        /// <param name="lineId">The product line.</param>
        /// <returns>The line id.</returns>
        public bool DoesProductLineContainTransactions(int lineId)
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.StockTransactions.Any(x => x.MasterTableID == lineId && x.Deleted == null && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Checks if the dispatch line has transactions recorded against it.
        /// </summary>
        /// <param name="lineId">The product line.</param>
        /// <returns>The line id.</returns>
        public bool DoesProductionProductLineContainTransactions(int orderId, int inmasterid)
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.StockTransactions.Any(x => x.MasterTableID == orderId && x.Deleted == null && x.INMasterID == inmasterid && (x.NouTransactionTypeID == 3 || x.NouTransactionTypeID == 4));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Creates a deep copy transaction traceability.
        /// </summary>
        /// <param name="transactionTrace">The transaction trace to deep copy</param>
        /// <returns>A deep copy transaction traceability.</returns>
        private TransactionTraceability CreateDeepCopyTransactionTraceability(TransactionTraceability transactionTrace)
        {
            return new TransactionTraceability
            {
                StockTransactionID = transactionTrace.StockTransactionID,
                NouTraceabilityMethodID = transactionTrace.NouTraceabilityMethodID,
                Value = transactionTrace.Value,
                Deleted = transactionTrace.Deleted,
                TraceabilityTemplateMasterID = transactionTrace.TraceabilityTemplateMasterID,
                DateMasterID = transactionTrace.DateMasterID,
                QualityMasterID = transactionTrace.QualityMasterID,
                DynamicName = transactionTrace.DynamicName
            };
        }

        #endregion
    }
}





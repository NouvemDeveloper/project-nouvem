﻿// -----------------------------------------------------------------------
// <copyright file="AuditrRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer.Interface;

    public class DepartmentBodiesRepository : NouvemRepositoryBase, IDepartmentBodiesRepository
    {
        /// <summary>
        /// The request id.
        /// </summary>
        private int requestId;

        /// <summary>
        /// Log the Bord Bia request in the database.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="requestDate">The date of the request.</param>
        /// <param name="userName">The username for which the request is sent.</param>
        /// <param name="fileName">The file name associated with the request.</param>
        public bool LogBordBiaRequest(string herdNo, string requestDate, string userName, string fileName)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var request = new BordBiaRequest();
                    request.HerdNumber = herdNo;
                    request.DateStamp = requestDate.ToDate();
                    request.RequestSent = true;
                    request.Exported = false;

                    entities.BordBiaRequests.Add(request);
                    entities.SaveChanges();

                    // Save the request id locally.
                    this.requestId = request.BordBiaRequestID;
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Log the Bord Bia response in the database.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="responseDate">The date on which the response was received.</param>
        /// <param name="fileName">The file name associated with the response.</param>
        /// <param name="herdOwner">The owner of the herd.</param>
        /// <param name="certDate">The date of expiry of the validation cert.</param>
        public bool LogBordBiaResponse(string herdNo, string responseDate, string fileName, string herdOwner, string certDate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var response = new BordBiaResponse();
                    response.DateStamp = responseDate.ToDate();
                    response.FileName = fileName;
                    response.BordBiaRequestID = this.requestId;
                    response.HerdNumber = herdNo;
                    response.HerdOwner = herdOwner;
                    response.CertValidDate = certDate.ToDate();
                    response.Exported = false;

                    entities.BordBiaResponses.Add(response);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Find the historical quality assurance records for a herd number.
        /// </summary>
        /// <param name="herdNumber">The herd number.</param>
        /// <returns>A collection of quality assurance expiration records for the herd number.</returns>
        public IList<QualityAssuranceHistory> FindQualityAssuranceEntries(string herdNumber)
        {
            var qa = new List<QualityAssuranceHistory>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    qa = (from entry in entities.QualityAssuranceHistories
                                           where entry.HerdNumber == herdNumber && entry.Deleted == null
                                           select entry).ToList();
                    
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return qa;
        }

        /// <summary>
        /// Add a new quality assurance history entry for a herd number.
        /// </summary>
        /// <param name="herdNumber">The herd number.</param>
        /// <param name="herdOwner">The herd owner.</param>
        /// <param name="certExpiryDate">The expiry date for the certificate.</param>
        /// <returns>The id of the new entry.</returns>
        public int AddQualityAssurance(string herdNumber, string herdOwner, DateTime certExpiryDate)
        {
            var historyId = 0;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var qualityAssuranceHistory = new QualityAssuranceHistory();

                    qualityAssuranceHistory.HerdNumber = herdNumber;
                    qualityAssuranceHistory.HerdOwner = herdOwner;
                    qualityAssuranceHistory.CertExpiryDate = certExpiryDate;
                    qualityAssuranceHistory.Timestamp = DateTime.Now;
                    entities.QualityAssuranceHistories.Add(qualityAssuranceHistory);
                    entities.SaveChanges();

                    historyId = qualityAssuranceHistory.QualityAssuranceHistoryID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return historyId;
        }
    }
}

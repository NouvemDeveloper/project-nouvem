﻿// -----------------------------------------------------------------------
// <copyright file="RegionRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;

    public class RegionRepository : NouvemRepositoryBase, IRegionRepository
    {
        /// <summary>
        /// Returns the application regions.
        /// </summary>
        /// <returns>The application regions.</returns>
        public IList<Region> GetRegions()
        {
            this.Log.LogDebug(this.GetType(), "GetRegions(): Attempting to retrieve all the regions");
            var regions = new List<Region>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    regions = (from region in entities.RegionMasters
                            where region.Deleted == null
                            select new Region
                            {
                                RegionID = region.RegionID,
                                Name = region.Name,
                                RegionNumber = region.RegionNumber,
                                Remark = region.Remark,
                                Deleted = region.Deleted
                            }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} regions successfully retrieved", regions.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return regions;
        }

        /// <summary>
        /// Add or updates the regions.
        /// </summary>
        /// <param name="regions">The regions to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateRegions(IList<Region> regions)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateRegions(): Attempting to update the regions");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    regions.ToList().ForEach(x =>
                    {
                        if (x.RegionID == 0)
                        {
                            // new
                            var newRegion = new RegionMaster
                            {
                                Name = x.Name,
                                RegionNumber = x.RegionNumber,
                                Remark = x.Remark
                            };

                            entities.RegionMasters.Add(newRegion);
                        }
                        else
                        {
                            // update
                            var dbRegion =
                                entities.RegionMasters.FirstOrDefault(
                                    region => region.RegionID == x.RegionID && region.Deleted == null);

                            if (dbRegion != null)
                            {
                                dbRegion.Name = x.Name;
                                dbRegion.RegionNumber = x.RegionNumber;
                                dbRegion.Remark = x.Remark;
                                dbRegion.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Regions successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }
    }
}

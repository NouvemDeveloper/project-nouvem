﻿// -----------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Shared;

    public class LicenseRepository : NouvemRepositoryBase, ILicenseRepository
    {
        /// <summary>
        /// Retrieve all the database licensees (devices and users), and their associated license type names.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        public IList<Licensee> GetLicensees()
        {
            this.Log.LogDebug(this.GetType(), "GetLicensees(): Attempting to retrieve all the licensees");
            var licensees = new List<Licensee>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // get the devices, and their licencing details
                    var devices = (from device in entities.DeviceMasters
                        join detail in entities.LicenceDetails
                            on device.LicenceDetailID equals detail.LicenceDetailID into defaultLicenceDetails 
                            from defaultLicenceDetail in defaultLicenceDetails.DefaultIfEmpty()
                        join type in entities.NouLicenceTypes
                            on defaultLicenceDetail.NouLicenceTypeID equals type.NouLicenceTypeID into defaultTypes
                                  from defaultType in defaultTypes.DefaultIfEmpty()
                        join licence in entities.Licences
                            on defaultLicenceDetail.LicenceID equals licence.LicenceID into defaultLicences
                            from defaultLicence in defaultLicences.DefaultIfEmpty()
                        select new Licensee
                        {
                            Id = device.DeviceID,
                            LicenceDetailsId = defaultLicenceDetail.LicenceDetailID,
                            LicenceKey = defaultLicence.LicenceNumber,
                            Identifier = device.DeviceMAC,
                            IP = device.DeviceIP,
                            Name = device.DeviceName,
                            LicenseType = defaultType,
                            IsDevice = true,
                            ValidFrom = defaultLicence.ValidFrom,
                            ValidTo = defaultLicence.ValidTo,
                            ImportDate = defaultLicence.ImportedOn,
                            IssueDate = defaultLicence.IssuedOn,
                            NoOfDays = defaultLicence.ValidForDays
                        }).ToList();

                    // get the users, and their licencing details
                    var users = (from user in entities.UserMasters
                                  join detail in entities.LicenceDetails
                                      on user.LicenceDetailID equals detail.LicenceDetailID into defaultLicenceDetails
                                  from defaultLicenceDetail in defaultLicenceDetails.DefaultIfEmpty()
                                  join type in entities.NouLicenceTypes
                                      on defaultLicenceDetail.NouLicenceTypeID equals type.NouLicenceTypeID into defaultTypes
                                  from defaultType in defaultTypes.DefaultIfEmpty()
                                  join licence in entities.Licences
                                      on defaultLicenceDetail.LicenceID equals licence.LicenceID into defaultLicences
                                  from defaultLicence in defaultLicences.DefaultIfEmpty()
                                  select new Licensee
                                  {
                                      Id = user.UserMasterID,
                                      LicenceDetailsId = defaultLicenceDetail.LicenceDetailID,
                                      LicenceKey = defaultLicence.LicenceNumber,
                                      Identifier = user.UserName,
                                      IP = Strings.NotApplicable,
                                      Name = user.UserName,
                                      LicenseType = defaultType,
                                      IsDevice = false,
                                      ValidFrom = defaultLicence.ValidFrom,
                                      ValidTo = defaultLicence.ValidTo,
                                      ImportDate = defaultLicence.ImportedOn,
                                      IssueDate = defaultLicence.IssuedOn,
                                      NoOfDays = defaultLicence.ValidForDays,
                                      LicenceeGroup = user.UserGroup_
                                  }).ToList();

                    // join them together
                    licensees = devices.Concat(users).ToList();

                    // now get their authorisations
                    //licensees.ForEach(x =>
                    //{
                    //    if (x.LicenceStatus == LicenceStatus.Valid)
                    //    {
                    //        x.Authorisations = entities.NouLicenceTypeRules
                    //            .Where(rule => rule.LicenceDetailID == x.LicenceDetailsId && !rule.Deleted)
                    //            .ToDictionary(key => key.NouAuthorisation.Description, value => value.NouAuthorisationValue.Description);
                    //    }
                    //});

                    this.Log.LogDebug(this.GetType(), string.Format("{0} licensees successfully retrieved", licensees.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return licensees;
        }

        /// <summary>
        /// Retrieve all the database license details.
        /// </summary>
        /// <returns>A collection of license details.</returns>
        public IList<ViewLicenseDetail> GetLicenseDetails()
        {
            this.Log.LogDebug(this.GetType(), "GetLicenseDetails(): Attempting to retrieve all the license details");
            var localDetails = new List<ViewLicenseDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var details = entities.ViewLicenseDetails;

                    foreach (var detail in details)
                    {
                        localDetails.Add(detail);
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} License details successfully retrieved", localDetails.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localDetails;
        }

        /// <summary>
        /// Retrieve all the database license totals by license type.
        /// </summary>
        /// <returns>A collection of license totals by type.</returns>
        public IList<ViewGroupedLicenseDetail> GetLicenseTotals()
        {
            this.Log.LogDebug(this.GetType(), "GetLicenseTotals(): Attempting to retrieve all the license totals by license type");
            var localDetails = new List<ViewGroupedLicenseDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var details = entities.ViewGroupedLicenseDetails;

                    foreach (var detail in details)
                    {
                        localDetails.Add(detail);
                    }

                    this.Log.LogDebug(this.GetType(), "License totals successfully retrieved");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localDetails;
        }

        /// <summary>
        /// Stores an imported licence details.
        /// </summary>
        /// <param name="details">The licence details to store.</param>
        /// <returns>The newly stored licence details.</returns>
        public IList<LicenceDetail> StoreLicenceDetails(Licencing.LicenseDetail details)
        {
            this.Log.LogDebug(this.GetType(), string.Format("StoreLicenceDetails(): Attempting to store license details for key:{0}", details.LicenseKey));
            var licenceDetails = new List<LicenceDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    // add the licence
                    var licence = new Licence
                    {
                        LicenceNumber = details.LicenseKey,
                        MessageContent = details.MessageContent ?? string.Empty,
                        ValidFrom = details.ValidFrom,
                        ValidTo = details.ValidTo,
                        IssuedOn = details.IssueDate,
                        ValidForDays = details.NoOfDays,
                        ImportedOn = DateTime.Today,
                        Deleted = false
                    };

                    entities.Licences.Add(licence);
                    entities.SaveChanges();
                    
                    // add the licence details
                    details.Types.ToList().ForEach(type =>
                    {
                        var licenceDetail = new LicenceDetail
                        {
                            LicenceID = licence.LicenceID,
                            LicenceQTY = type.Value,
                            Deleted = false
                        };

                        var dbNouLicenceType =
                            entities.NouLicenceTypes.FirstOrDefault(x => x.LicenceName.Equals(type.Key));

                        if (dbNouLicenceType != null)
                        {
                            licenceDetail.NouLicenceTypeID = dbNouLicenceType.NouLicenceTypeID;
                        }

                        entities.LicenceDetails.Add(licenceDetail);
                        entities.SaveChanges();

                        licenceDetails.Add(licenceDetail);

                        // add the authorisations
                        details.Authorisations.Where(x => x.Item1.Equals(licenceDetail.NouLicenceType.LicenceName)).ToList().ForEach(authorisation =>
                        {
                            var nouLicenceTypeRule = new NouLicenceTypeRule();
                            nouLicenceTypeRule.LicenceDetailID = licenceDetail.LicenceDetailID;
                            
                            var dbAuthorisation =
                                entities.NouAuthorisations.FirstOrDefault(x => x.Description.Equals(authorisation.Item2));

                            if (dbAuthorisation != null)
                            {
                                nouLicenceTypeRule.NouAuthorisationID = dbAuthorisation.NouAuthorisationID;
                            }

                            var dbAuthorisationValue =
                                entities.NouAuthorisationValues.FirstOrDefault(
                                    x => x.Description.Equals(authorisation.Item3));

                            if (dbAuthorisationValue != null)
                            {
                                nouLicenceTypeRule.NouAuthorisationValueID = dbAuthorisationValue.NouAuthorisationValueID;
                            }

                            entities.NouLicenceTypeRules.Add(nouLicenceTypeRule);
                        });

                        // save the authorisations.
                        entities.SaveChanges();
                    });

                    this.Log.LogDebug(this.GetType(), "License totals successfully retrieved");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return licenceDetails;
        }

        /// <summary>
        /// Method that assigns a license type to a user/device.
        /// </summary>
        /// <param name="selectedLicensee">The selected user/device.</param>
        /// <param name="selectedLicenseDetails">The licence type to allocate</param>
        /// <returns>A flag, indicating a successful allocation or not.</returns>
        public bool AssignLicense(Licensee selectedLicensee, LicenseDetails selectedLicenseDetails)
        {
            this.Log.LogDebug(this.GetType(), 
                string.Format("AssignLicense(): Attempting to assign a license - Licensee name:{0}, identifier:{1}, license type:{2}, current quantity:{3}", 
                selectedLicensee.Name, selectedLicensee.Identifier, selectedLicenseDetails.Details.LicenceName, selectedLicenseDetails.Details.Quantity));
         
            try
            {
                using (var entities = new NouvemEntities())
                {
                    // Find the first license type matching the selected license type, where qty > 0. (there must be at least one if we've got this far)
                    var licenseDetail =
                        entities.LicenceDetails.FirstOrDefault(x => x.NouLicenceType.LicenceName.Equals(selectedLicenseDetails.Details.LicenceName) && x.LicenceQTY > 0 && !x.Deleted);

                    if (licenseDetail != null)
                    {
                        // Decrement the license quantity
                        licenseDetail.LicenceQTY--;

                        var allocated = false;

                        // allocate it to the licensee
                        if (selectedLicensee.IsDevice)
                        {
                            // device
                            var dbDevice = entities.DeviceMasters.FirstOrDefault(device => device.DeviceID == selectedLicensee.Id);
                            if (dbDevice != null)
                            {
                                // if the device already has a license, we need to put that back into the license pool
                                if (dbDevice.LicenceDetail != null)
                                {
                                    // Find the first license type matching the selected license type.
                                    var detail =
                                        entities.LicenceDetails.FirstOrDefault(x => x.LicenceDetailID == dbDevice.LicenceDetailID);

                                    if (detail != null)
                                    {
                                        detail.LicenceQTY++;
                                    }
                                }

                                dbDevice.LicenceDetailID = licenseDetail.LicenceDetailID;
                                allocated = true;
                            }
                        }
                        else
                        {
                            // user
                            var dbUser= entities.UserMasters.FirstOrDefault(user => user.UserMasterID == selectedLicensee.Id);
                            if (dbUser!= null)
                            {
                                // if the user already has a license, we need to put that back into the license pool
                                if (dbUser.LicenceDetail != null)
                                {
                                    // Find the first license type matching the selected license type.
                                    var detail =
                                        entities.LicenceDetails.FirstOrDefault(x => x.LicenceDetailID == dbUser.LicenceDetailID);

                                    if (detail != null)
                                    {
                                        detail.LicenceQTY++;
                                    }
                                }

                                dbUser.LicenceDetailID = licenseDetail.LicenceDetailID;
                                allocated = true;
                            }
                        }

                        if (allocated)
                        {
                            // if the licence allocated was a trial licence, we need to update the licence end date.
                            if (licenseDetail.Licence.ValidForDays > 0)
                            {
                                // trial licence
                                licenseDetail.Licence.ValidTo =
                                    DateTime.Today.AddDays(licenseDetail.Licence.ValidForDays.ToDouble());
                            }

                            entities.SaveChanges();
                            this.Log.LogDebug(this.GetType(), string.Format("A new license type {0} has been successfully allocated to licensee {1}.",
                                             selectedLicenseDetails.Details.LicenceName, selectedLicensee.Name));
                            return true;
                        }
                    }

                    this.Log.LogError(this.GetType(), "License detail could not be found");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Assigns a licences to a group of licensee.
        /// </summary>
        /// <param name="selectedLicenses">The licensees, and thier associated licence detail to allocate.</param>
        /// <returns>A flag, indication a successful transaction or not.</returns>
        public bool UpgradeLicenses(IDictionary<Licensee, LicenceDetail> selectedLicenses)
        {
            this.Log.LogDebug(this.GetType(), "UpgradeLicense(): Attempting to assign licences");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    selectedLicenses.ToList().ForEach(data =>
                    {
                        var selectedLicensee = data.Key;
                        var licenceDetail = data.Value;
                      
                        // Find the first license type matching the selected license type, where qty > 0. 
                        var licenseDetail =
                            entities.LicenceDetails.FirstOrDefault(detail => detail.LicenceDetailID == licenceDetail.LicenceDetailID && detail.LicenceQTY > 0 && !detail.Deleted);

                        if (licenseDetail != null)
                        {
                            // Decrement the license quantity
                            licenseDetail.LicenceQTY--;

                            var allocated = false;

                            // allocate it to the licensee
                            if (selectedLicensee.IsDevice)
                            {
                                // device
                                var dbDevice = entities.DeviceMasters.FirstOrDefault(device => device.DeviceID == selectedLicensee.Id);
                                if (dbDevice != null)
                                {
                                    // if the device already has a license, we need to deallocate, and remove.
                                    if (dbDevice.LicenceDetail != null)
                                    {
                                        // Find the first license type matching the selected license type.
                                        var detail =
                                            entities.LicenceDetails.FirstOrDefault(x => x.LicenceDetailID == dbDevice.LicenceDetailID);

                                        if (detail != null)
                                        {
                                            detail.LicenceQTY++;
                                            detail.Deleted = true;
                                        }
                                    }

                                    dbDevice.LicenceDetailID = licenseDetail.LicenceDetailID;
                                    allocated = true;
                                }
                            }
                            else
                            {
                                // user
                                var dbUser = entities.UserMasters.FirstOrDefault(user => user.UserMasterID == selectedLicensee.Id);
                                if (dbUser != null)
                                {
                                    // if the user already has a license, we need to put that back into the license pool
                                    if (dbUser.LicenceDetail != null)
                                    {
                                        // Find the first license type matching the selected license type.
                                        var detail =
                                            entities.LicenceDetails.FirstOrDefault(x => x.LicenceDetailID == dbUser.LicenceDetailID);

                                        if (detail != null)
                                        {
                                            detail.LicenceQTY++;
                                            detail.Deleted = true;
                                        }
                                    }

                                    dbUser.LicenceDetailID = licenseDetail.LicenceDetailID;
                                    allocated = true;
                                }
                            }

                            if (allocated)
                            {
                                // we need to save on each iteration, to keep the licence totals updated for the next iteration.
                                entities.SaveChanges();
                                this.Log.LogDebug(this.GetType(), string.Format("A new license type {0} has been successfully allocated to licensee {1}.",
                                                 licenceDetail.NouLicenceType.LicenceName, selectedLicensee.Name));
                            }
                        }

                        this.Log.LogError(this.GetType(), "License detail could not be found");
                    });
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Makes a call to deallocate the collection of licences.
        /// </summary>
        /// <param name="deallocations">The licencess to deallocate licences from.</param>
        public bool DeallocateLicences(IList<Licensee> deallocations)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to deaalocate {0} licence(s)", deallocations.Count));
            var deallocated = false;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    deallocations.ToList().ForEach(licensee =>
                    {
                        if (licensee.IsDevice)
                        {
                            var dbDevice = entities.DeviceMasters.FirstOrDefault(x => x.DeviceID == licensee.Id);
                            if (dbDevice != null)
                            {
                                // deallocate, and release back.
                                dbDevice.LicenceDetail.LicenceQTY++;
                                dbDevice.LicenceDetailID = null;
                                deallocated = true;
                            }
                        }
                        else
                        {
                            var dbUser = entities.UserMasters.FirstOrDefault(x => x.UserMasterID == licensee.Id);
                            if (dbUser != null)
                            {
                                // deallocate, and release back.
                                dbUser.LicenceDetail.LicenceQTY++;
                                dbUser.LicenceDetailID = null;
                                deallocated = true;
                            }
                        }

                        if (deallocated)
                        {
                            var rules = entities.NouLicenceTypeRules.Where(rule => rule.NouAuthorisationID == licensee.LicenceDetailsId);

                            entities.NouLicenceTypeRules.RemoveRange(rules);
                            entities.SaveChanges();
                            this.Log.LogDebug(this.GetType(), string.Format("{0} licence(s) successfully deallocated.", deallocations.Count));
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }
    }
}

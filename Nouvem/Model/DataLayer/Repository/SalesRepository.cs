﻿// -----------------------------------------------------------------------
// <copyright file="SalesRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Mvvm.Native;
using DevExpress.XtraPrinting.Native;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;

    public class SalesRepository : NouvemRepositoryBase, ISalesRepository
    {
        #region quote

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>The newly created quote id, indicating a successful quote add, or not.</returns>
        public int AddNewQuote(Sale sale)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddNewQuote(): Attempting to add a new quote. " +
                                                           "Contact:{0} DiscountIncVat:{1} Doc Numbering Type:{2} Grand Total Inc Vat:{3}" +
                                                           " NouDocStatus:{4} Quote Valid Date:{5} Route:{6} Sales Emp:{7} Sub Total Inc Vat:{8}" +
                                                           " Total Ex Vat:{9} VAT:{10} Sale Details Count:{11}",
                                                           sale.BPContactID, sale.DiscountIncVAT, sale.DocumentNumberingID,
                                                           sale.GrandTotalIncVAT, sale.NouDocStatusID, sale.QuoteValidDate, sale.RouteID,
                                                           sale.SalesEmployeeID, sale.SubTotalExVAT, sale.TotalExVAT, sale.VAT, sale.SaleDetails.Count));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var quote = new ARQuote
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Today,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null,
                        RouteID = sale.RouteID,
                        QuoteValidDate = sale.QuoteValidDate,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        NouDocStatusID = sale.NouDocStatusID,
                        Remarks = sale.Remarks,
                        DispatchNote1 = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        PopUpNote = sale.PopUpNote,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID_SalesEmployee = sale.SalesEmployeeID,
                        PORequired = sale.PORequired
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        quote.BPMasterSnapshotID_Customer = snapshot.BPMasterSnapshotID;
                        quote.BPMasterID_Customer = snapshot.BPMasterID;
                    }

                    if (sale.Haulier != null)
                    {
                        // add the haulier snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Haulier.BPMasterID,
                            BPMaster_Name = sale.Haulier.Name,
                            BPMaster_Code = sale.Haulier.Code,
                            BPMaster_UpLift = sale.Haulier.Uplift,
                            BPMaster_CreditLimit = sale.Haulier.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Haulier.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        quote.BPMasterSnapshotID_Haulier = snapshot.BPMasterSnapshotID;
                        quote.BPMasterID_Haulier = snapshot.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress?.Details?.AddressLine1 ?? string.Empty,
                            AddressLine2 = sale.DeliveryAddress?.Details?.AddressLine2 ?? string.Empty,
                            AddressLine3 = sale.DeliveryAddress?.Details?.AddressLine3 ?? string.Empty,
                            AddressLine4 = sale.DeliveryAddress?.Details?.AddressLine4 ?? string.Empty,
                            PostCode = sale.DeliveryAddress?.Details?.PostCode ?? string.Empty,
                            Shipping = sale.DeliveryAddress?.Details?.Shipping,
                            Billing = sale.DeliveryAddress?.Details?.Billing,
                            PlantID = sale.DeliveryAddress?.Details?.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();

                        quote.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        quote.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        // add the address snapshot
                        var invoiceSnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress?.Details?.AddressLine1 ?? string.Empty,
                            AddressLine2 = sale.DeliveryAddress?.Details?.AddressLine2 ?? string.Empty,
                            AddressLine3 = sale.DeliveryAddress?.Details?.AddressLine3 ?? string.Empty,
                            AddressLine4 = sale.DeliveryAddress?.Details?.AddressLine4 ?? string.Empty,
                            PostCode = sale.DeliveryAddress?.Details?.PostCode ?? string.Empty,
                            Shipping = sale.DeliveryAddress?.Details?.Shipping,
                            Billing = sale.DeliveryAddress?.Details?.Billing,
                            PlantID = sale.DeliveryAddress?.Details?.PlantID
                        };

                        entities.BPAddressSnapshots.Add(invoiceSnapShot);
                        entities.SaveChanges();

                        quote.BPAddressSnapshotID_Invoice = invoiceSnapShot.BPAddressSnapshotID;
                        quote.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.ARQuotes.Add(quote);
                    entities.SaveChanges();

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var quoteAttachment = new QuoteAttachment
                        {
                            ARQuoteID = quote.ARQuoteID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.QuoteAttachments.Add(quoteAttachment);
                    }

                    // save the details
                    var saleID = quote.ARQuoteID;
                    var localQouteDetails = new List<ARQuoteDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        //this.Log.LogDebug(this.GetType(), string.Format(
                        //    "Discount Amount:{0} Discount Percentage:{1} Discount Price:{2} INMaster:{3} Line Dis Amount:{4} Line Dis Percentage:{5}" +
                        //    "Quantity Del:{6} Quantity ordered:{7} Sale:{8} TotalExVat:{9} TotalIncVat:{10} Unit Price:{11} Unit Price/Discount:{12}" +
                        //    "Vat:{13} Wgt Del:{14} Wgt Ordered:{15}", 
                        //    detail.DiscountAmount, detail.DiscountPercentage, detail.DiscountPrice, detail.INMasterID, detail.LineDiscountAmount,
                        //    detail.LineDiscountPercentage, detail.QuantityDelivered, detail.QuantityOrdered, saleID, detail.TotalExVAT, detail.TotalIncVAT,
                        //    detail.UnitPrice, detail.UnitPriceAfterDiscount, detail.VatCodeID, detail.WeightDelivered, detail.WeightOrdered));

                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = detail.InventoryItem.Master.INMasterID,
                            Code = detail.InventoryItem.Master.Code,
                            Name = detail.InventoryItem.Master.Name,
                            MaxWeight = detail.InventoryItem.Master.MaxWeight,
                            MinWeight = detail.InventoryItem.Master.MinWeight,
                            NominalWeight = detail.InventoryItem.Master.NominalWeight,
                            TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var quoteDetail = new ARQuoteDetail
                        {
                            ARQuoteID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            //  QuantityDelivered = detail.QuantityDelivered,
                            // WeightDelivered = detail.WeightDelivered,
                            Notes = detail.Notes,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterSnapshotID = snapshot.INMasterSnapshotID
                        };

                        localQouteDetails.Add(quoteDetail);
                    }

                    entities.ARQuoteDetails.AddRange(localQouteDetails);
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "New quote successfully created");
                    return quote.ARQuoteID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewQuote(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates an existing quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateQuote(Sale sale)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdateQuote(): Attempting to update quote. " +
                                                           "Contact:{0} DiscountIncVat:{1} Doc Numbering Type:{2} Grand Total Inc Vat:{3}" +
                                                           " NouDocStatus:{4} Quote Valid Date:{5} Route:{6} Sales Emp:{7} Sub Total Inc Vat:{8}" +
                                                           " Total Ex Vat:{9} VAT:{10} Sale Details Count:{11}",
                                                           sale.BPContactID, sale.DiscountIncVAT, sale.DocumentNumberingID,
                                                           sale.GrandTotalIncVAT, sale.NouDocStatusID, sale.QuoteValidDate, sale.RouteID,
                                                           sale.SalesEmployeeID, sale.SubTotalExVAT, sale.TotalExVAT, sale.VAT, sale.SaleDetails.Count));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbQuote = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == sale.SaleID && x.Deleted == null);

                    if (dbQuote != null)
                    {
                        dbQuote.BPMasterID_Customer = sale.Customer.BPMasterID;
                        dbQuote.BPContactID_Delivery = sale.DeliveryContact != null? (int?) sale.DeliveryContact.Details.BPContactID : null;
                        dbQuote.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbQuote.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbQuote.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbQuote.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbQuote.RouteID = sale.RouteID;
                        dbQuote.DeviceID = NouvemGlobal.DeviceId;
                        dbQuote.EditDate = DateTime.Now;
                        dbQuote.QuoteValidDate = sale.QuoteValidDate;
                        dbQuote.DocumentDate = sale.DocumentDate;
                        dbQuote.CreationDate = sale.CreationDate;
                        dbQuote.NouDocStatusID = sale.NouDocStatusID;
                        dbQuote.Remarks = sale.Remarks;
                        dbQuote.DispatchNote1 = sale.DocketNote;
                        dbQuote.InvoiceNote = sale.InvoiceNote;
                        dbQuote.PopUpNote = sale.PopUpNote;
                        dbQuote.TotalExVAT = sale.TotalExVAT;
                        dbQuote.DiscountPercentage = sale.DiscountPercentage;
                        dbQuote.VAT = sale.VAT;
                        dbQuote.DiscountIncVAT = sale.DiscountIncVAT;
                        dbQuote.SubTotalExVAT = sale.SubTotalExVAT;
                        dbQuote.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbQuote.UserMasterID_SalesEmployee = sale.SalesEmployeeID;
                        dbQuote.PORequired = sale.PORequired;
                        dbQuote.Deleted = sale.Deleted;

                        foreach (var attachment in sale.Attachments)
                        {
                            var dbAttachment =
                                    entities.QuoteAttachments.FirstOrDefault(
                                        x => x.QuoteAttachmentID == attachment.SaleAttachmentID);

                                if (dbAttachment != null)
                                {
                                    // updating an existing attachment
                                    if (attachment.Deleted != null)
                                    {
                                        entities.QuoteAttachments.Remove(dbAttachment);
                                    }
                                    else
                                    {
                                        dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                        dbAttachment.FileName = attachment.FileName;
                                        dbAttachment.File = attachment.File;
                                    }
                                }
                                else
                                {
                                    // adding a new attachment
                                    var localAttachment = new QuoteAttachment
                                                              {
                                                                  AttachmentDate = attachment.AttachmentDate,
                                                                  ARQuoteID = sale.SaleID,
                                                                  FileName = attachment.FileName,
                                                                  File = attachment.File
                                                              };

                                    entities.QuoteAttachments.Add(localAttachment);
                                }
                        }

                        var localQouteDetails = new List<ARQuoteDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format(
                                "Discount Amount:{0} Discount Percentage:{1} Discount Price:{2} INMaster:{3} Line Dis Amount:{4} Line Dis Percentage:{5}" +
                                "Quantity Del:{6} Quantity ordered:{7} Sale:{8} TotalExVat:{9} TotalIncVat:{10} Unit Price:{11} Unit Price/Discount:{12}" +
                                "Vat:{13} Wgt Del:{14} Wgt Ordered:{15}",
                                detail.DiscountAmount, detail.DiscountPercentage, detail.DiscountPrice, detail.INMasterID, detail.LineDiscountAmount,
                                detail.LineDiscountPercentage, detail.QuantityDelivered, detail.QuantityOrdered, "n/a", detail.TotalExVAT, detail.TotalIncVAT,
                                detail.UnitPrice, detail.UnitPriceAfterDiscount, detail.VatCodeID, detail.WeightDelivered, detail.WeightOrdered));

                            var dbQuoteDetail =
                                entities.ARQuoteDetails.FirstOrDefault(
                                    x => x.ARQuoteDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbQuoteDetail != null)
                            {
                                // existing sale item
                                dbQuoteDetail.QuantityOrdered = detail.QuantityOrdered;
                                //dbQuoteDetail.QuantityDelivered = detail.QuantityDelivered;
                                //dbQuoteDetail.WeightDelivered = detail.WeightDelivered;
                                dbQuoteDetail.WeightOrdered = detail.WeightOrdered;
                                dbQuoteDetail.UnitPrice = detail.UnitPrice;
                                dbQuoteDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbQuoteDetail.DiscountAmount = detail.DiscountAmount;
                                dbQuoteDetail.DiscountPrice = detail.DiscountPrice;
                                dbQuoteDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbQuoteDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbQuoteDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbQuoteDetail.VAT = detail.VAT;
                                dbQuoteDetail.VATCodeID = detail.VatCodeID;
                                dbQuoteDetail.Notes = detail.Notes;
                                dbQuoteDetail.TotalExclVAT = detail.TotalExVAT;
                                dbQuoteDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbQuoteDetail.INMasterID = detail.INMasterID;
                                dbQuoteDetail.PriceListID = detail.PriceListID == 0 ? (int?) null : detail.PriceListID;
                                dbQuoteDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order

                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = detail.InventoryItem.Master.INMasterID,
                                    Code = detail.InventoryItem.Master.Code,
                                    Name = detail.InventoryItem.Master.Name,
                                    MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                    MinWeight = detail.InventoryItem.Master.MinWeight,
                                    NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                    TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);
                                entities.SaveChanges();

                                var quoteDetail = new ARQuoteDetail
                                {
                                    ARQuoteID = sale.SaleID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    //QuantityDelivered = detail.QuantityDelivered,
                                    //WeightDelivered = detail.WeightDelivered,
                                    Notes = detail.Notes,
                                    WeightOrdered = detail.WeightOrdered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID == 0 ? (int?) null : detail.PriceListID,
                                    INMasterSnapshotID = snapshot.INMasterSnapshotID
                                };

                                localQouteDetails.Add(quoteDetail);
                            }
                        }

                        if (localQouteDetails.Any())
                        {
                            entities.ARQuoteDetails.AddRange(localQouteDetails);
                        }
                    }
                    
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Quote order successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("UpdateQuote(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes a quote detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveArQuoteDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveArQuoteDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.ARQuoteDetails.FirstOrDefault(x => x.ARQuoteDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Retrieves all the quotes (and associated quote details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all quotes (and associated quote details).</returns>
        public IList<Sale> GetAllQuotes(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the quotes");
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.ARQuotes
                              where orderStatuses.Contains(quote.NouDocStatusID)
                              && quote.Deleted == null
                              orderby quote.CreationDate
                              select new Sale
                              {
                                  SaleID = quote.ARQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  RouteID = quote.RouteID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  DocketNote = quote.DispatchNote1,
                                  DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                  PopUpNote = quote.PopUpNote,
                                  InvoiceNote = quote.InvoiceNote,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  OtherReference = quote.OtherReference,
                                  SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  PORequired = quote.PORequired,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.QuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.QuoteAttachmentID,
                                                     SaleAttachmentID = attachment.ARQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                              //    SaleDetails = (from detail in quote.ARQuoteDetails
                              //                   where detail.Deleted == null
                              //                   select new SaleDetail
                              //                   {
                              //                       SaleDetailID = detail.ARQuoteDetailID,
                              //                       SaleID = detail.ARQuoteID,
                              //                       QuantityOrdered = detail.QuantityOrdered,
                              //                       WeightOrdered = detail.WeightOrdered,
                              //                       UnitPrice = detail.UnitPrice,
                              //                       DiscountPercentage = detail.DiscountPercentage,
                              //                       DiscountPrice = detail.DiscountPrice,
                              //                       LineDiscountPercentage = detail.LineDiscountPercentage,
                              //                       LineDiscountAmount = detail.LineDiscountAmount,
                              //                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                              //                       VAT = detail.VAT,
                              //                       TotalExVAT = detail.TotalExclVAT,
                              //                       TotalIncVAT = detail.TotalIncVAT,
                              //                       VatCodeID = detail.VATCodeID,
                              //                       LoadingSale = true,
                              //                       INMasterID = detail.INMasterID,
                              //                       PriceListID = detail.PriceListID ?? 0,
                              //                       Deleted = detail.Deleted
                              //                   }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetQuotes(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the quotes for customer id:{0}", customerId));
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                     quotes = (from quote in entities.ARQuotes
                        where quote.BPMasterID_Customer == customerId
                        && quote.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                        && quote.Deleted == null
                        select new Sale
                        {
                            SaleID = quote.ARQuoteID,
                            DocumentNumberingID = quote.DocumentNumberingID,
                            Number = quote.Number,
                            DeviceId = quote.DeviceID,
                            EditDate = quote.EditDate,
                            BPContactID = quote.BPContactID,
                            RouteID = quote.RouteID,
                            QuoteValidDate = quote.QuoteValidDate,
                            DocumentDate = quote.DocumentDate,
                            CreationDate = quote.CreationDate,
                            Printed = quote.Printed,
                            NouDocStatusID = quote.NouDocStatusID,
                            NouDocStatus = quote.NouDocStatu.Value,
                            Remarks = quote.Remarks,
                            PopUpNote = quote.PopUpNote,
                            DocketNote = quote.DispatchNote1,
                            DispatchNote1PopUp = quote.DispatchNote1PopUp,
                            InvoiceNote = quote.InvoiceNote,
                            TotalExVAT = quote.TotalExVAT,
                            VAT = quote.VAT,
                            SubTotalExVAT = quote.SubTotalExVAT,
                            DiscountIncVAT = quote.DiscountIncVAT,
                            GrandTotalIncVAT = quote.GrandTotalIncVAT,
                            DiscountPercentage = quote.DiscountPercentage,
                            OtherReference = quote.OtherReference,
                            SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                            DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                            InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                            DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery},
                            MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                            BPHaulier = quote.BPMasterHaulier,
                            BPCustomer = quote.BPMasterCustomer,
                            PORequired = quote.PORequired,
                            Deleted = quote.Deleted,
                            Attachments = (from attachment in quote.QuoteAttachments
                                           where attachment.Deleted == null
                                           select new Attachment
                                           {
                                               AttachmentID = attachment.QuoteAttachmentID,
                                               SaleAttachmentID = attachment.ARQuoteID,
                                               AttachmentDate = attachment.AttachmentDate,
                                               FilePath = attachment.FilePath,
                                               FileName = attachment.FileName,
                                               File = attachment.File,
                                               Deleted = attachment.Deleted
                                           }).ToList(),
                            SaleDetails = (from detail in quote.ARQuoteDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    LoadingSale = true,
                                    SaleDetailID = detail.ARQuoteDetailID,
                                    SaleID = detail.ARQuoteID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    //QuantityDelivered = detail.QuantityDelivered,
                                    //WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    Deleted = detail.Deleted
                                }).ToList(),
                        })
                        .ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetRecentQuotes(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the quotes for customer id:{0}", customerId));
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.ARQuotes
                              where quote.BPMasterID_Customer == customerId
                              && quote.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID
                              && quote.Deleted == null
                              orderby quote.ARQuoteID descending 
                              select new Sale
                              {
                                  SaleID = quote.ARQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  RouteID = quote.RouteID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  DocketNote = quote.DispatchNote1,
                                  DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                  InvoiceNote = quote.InvoiceNote,
                                  TotalExVAT = quote.TotalExVAT,
                                  PopUpNote = quote.PopUpNote,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  OtherReference = quote.OtherReference,
                                  SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  PORequired = quote.PORequired,
                                  Deleted = quote.Deleted,
                                  SaleDetails = (from detail in quote.ARQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARQuoteDetailID,
                                                     SaleID = detail.ARQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).Take(10).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var receipt in quotes)
            {
                receipt.Attachments = new List<Attachment>();
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quote (and associated quote details) for the input id.
        /// </summary>
        /// <param name="quoteId">The customer id.</param>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        public Sale GetQuoteByID(int quoteId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetQuotesByID(): Attempting to get the quote for id:{0}", quoteId));
            Sale arQuote = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var quote = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == quoteId);

                    if (quote == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Quote not found");
                        return null;
                    }

                    arQuote = new Sale
                    {
                        SaleID = quote.ARQuoteID,
                        DocumentNumberingID = quote.DocumentNumberingID,
                        Number = quote.Number,
                        BPContactID = quote.BPContactID,
                        RouteID = quote.RouteID,
                        QuoteValidDate = quote.QuoteValidDate,
                        DocumentDate = quote.DocumentDate,
                        CreationDate = quote.CreationDate,
                        DeviceId = quote.DeviceID,
                        EditDate = quote.EditDate,
                        Printed = quote.Printed,
                        NouDocStatusID = quote.NouDocStatusID,
                        NouDocStatus = quote.NouDocStatu.Value,
                        Remarks = quote.Remarks,
                        DocketNote = quote.DispatchNote1,
                        PopUpNote = quote.PopUpNote,
                        DispatchNote1PopUp = quote.DispatchNote1PopUp,
                        InvoiceNote = quote.InvoiceNote,
                        TotalExVAT = quote.TotalExVAT,
                        VAT = quote.VAT,
                        SubTotalExVAT = quote.SubTotalExVAT,
                        DiscountIncVAT = quote.DiscountIncVAT,
                        GrandTotalIncVAT = quote.GrandTotalIncVAT,
                        DiscountPercentage = quote.DiscountPercentage,
                        OtherReference = quote.OtherReference,
                        SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                        DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                        InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                        DeliveryContact = new BusinessPartnerContact {Details = quote.BPContactDelivery},
                        MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                        BPHaulier = quote.BPMasterHaulier,
                        BPCustomer = quote.BPMasterCustomer,
                        PORequired = quote.PORequired,
                        Deleted = quote.Deleted,
                        Attachments = (from attachment in quote.QuoteAttachments
                                       where attachment.Deleted == null
                            select new Attachment
                            {
                                AttachmentID = attachment.QuoteAttachmentID,
                                SaleAttachmentID = attachment.ARQuoteID,
                                AttachmentDate = attachment.AttachmentDate,
                                FilePath = attachment.FilePath,
                                FileName = attachment.FileName,
                                File = attachment.File,
                                Deleted = attachment.Deleted
                            }).ToList(),
                        SaleDetails = (from detail in quote.ARQuoteDetails
                                       where detail.Deleted == null
                            select new SaleDetail
                            {
                                LoadingSale = true,
                                SaleDetailID = detail.ARQuoteDetailID,
                                SaleID = detail.ARQuoteID,
                                QuantityOrdered = detail.QuantityOrdered,
                                WeightOrdered = detail.WeightOrdered,
                                //QuantityDelivered = detail.QuantityDelivered,
                                //WeightDelivered = detail.WeightDelivered,
                                UnitPrice = detail.UnitPrice,
                                Notes = detail.Notes,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountPercentage = detail.LineDiscountPercentage,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                TotalExVAT = detail.TotalExclVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                VatCodeID = detail.VATCodeID,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID ?? 0,
                                Deleted = detail.Deleted
                            }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Quote found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return arQuote;
        }

        /// <summary>
        /// Retrieves the quote (and associated quote details) for the input id.
        /// </summary>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        public Sale GetQuoteByLastEdit()
        {
            Sale arQuote = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var quote = entities.ARQuotes
                       .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                       .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (quote == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Quote not found");
                        return null;
                    }

                    arQuote = new Sale
                    {
                        SaleID = quote.ARQuoteID,
                        DocumentNumberingID = quote.DocumentNumberingID,
                        Number = quote.Number,
                        BPContactID = quote.BPContactID,
                        RouteID = quote.RouteID,
                        QuoteValidDate = quote.QuoteValidDate,
                        DocumentDate = quote.DocumentDate,
                        CreationDate = quote.CreationDate,
                        DeviceId = quote.DeviceID,
                        EditDate = quote.EditDate,
                        Printed = quote.Printed,
                        NouDocStatusID = quote.NouDocStatusID,
                        NouDocStatus = quote.NouDocStatu.Value,
                        Remarks = quote.Remarks,
                        DocketNote = quote.DispatchNote1,
                        DispatchNote1PopUp = quote.DispatchNote1PopUp,
                        InvoiceNote = quote.InvoiceNote,
                        TotalExVAT = quote.TotalExVAT,
                        PopUpNote = quote.PopUpNote,
                        VAT = quote.VAT,
                        SubTotalExVAT = quote.SubTotalExVAT,
                        DiscountIncVAT = quote.DiscountIncVAT,
                        GrandTotalIncVAT = quote.GrandTotalIncVAT,
                        DiscountPercentage = quote.DiscountPercentage,
                        OtherReference = quote.OtherReference,
                        SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                        DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                        DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery },
                        MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                        BPHaulier = quote.BPMasterHaulier,
                        BPCustomer = quote.BPMasterCustomer,
                        PORequired = quote.PORequired,
                        Deleted = quote.Deleted,
                        Attachments = (from attachment in quote.QuoteAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.QuoteAttachmentID,
                                           SaleAttachmentID = attachment.ARQuoteID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in quote.ARQuoteDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARQuoteDetailID,
                                           SaleID = detail.ARQuoteID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           Notes = detail.Notes,
                                           //QuantityDelivered = detail.QuantityDelivered,
                                           //WeightDelivered = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Quote found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return arQuote;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetQuotesByPartner(int partnerId, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the quotes for partner id:{0}", partnerId));
            var quotes = new List<Sale>();
        
            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.ARQuotes
                              where quote.BPMasterID_Customer == partnerId
                              && orderStatuses.Contains(quote.NouDocStatusID)
                              && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.ARQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  RouteID = quote.RouteID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  DocketNote = quote.DispatchNote1,
                                  DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                  InvoiceNote = quote.InvoiceNote,
                                  TotalExVAT = quote.TotalExVAT,
                                  PopUpNote = quote.PopUpNote,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  OtherReference = quote.OtherReference,
                                  SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  PORequired = quote.PORequired,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.QuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.QuoteAttachmentID,
                                                     SaleAttachmentID = attachment.ARQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.ARQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARQuoteDetailID,
                                                     SaleID = detail.ARQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                 
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetQuotesByName(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the quotes for search term:{0}", searchTerm));
            var quotes = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.ARQuotes
                              where quote.BPMasterCustomer.Name.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(quote.NouDocStatusID)
                              && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.ARQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  RouteID = quote.RouteID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  DocketNote = quote.DispatchNote1,
                                  DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                  InvoiceNote = quote.InvoiceNote,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  PopUpNote = quote.PopUpNote,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  OtherReference = quote.OtherReference,
                                  SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  PORequired = quote.PORequired,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.QuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.QuoteAttachmentID,
                                                     SaleAttachmentID = attachment.ARQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.ARQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARQuoteDetailID,
                                                     SaleID = detail.ARQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetQuotesByCode(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the quotes for search term:{0}", searchTerm));
            var quotes = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.ARQuotes
                              where quote.BPMasterCustomer.Code.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(quote.NouDocStatusID)
                              && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.ARQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  RouteID = quote.RouteID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  DocketNote = quote.DispatchNote1,
                                  DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                  InvoiceNote = quote.InvoiceNote,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  PopUpNote = quote.PopUpNote,
                                  OtherReference = quote.OtherReference,
                                  SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = quote.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  PORequired = quote.PORequired,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.QuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.QuoteAttachmentID,
                                                     SaleAttachmentID = attachment.ARQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.ARQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARQuoteDetailID,
                                                     SaleID = detail.ARQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        #endregion

        #region order

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public int AddNewOrder(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    //if (sale.BaseDocumentReferenceID.HasValue)
                    //{
                    //    var quote = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == sale.BaseDocumentReferenceID);
                    //    if (quote == null)
                    //    {
                    //        this.Log.LogError(this.GetType(), "No quote match batch doc reference id. Resetting base doc to null");
                    //        sale.BaseDocumentReferenceID = null;
                    //    }
                    //}

                    var order = new AROrder
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null,
                        RouteID = sale.RouteID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        DeliveryDate = sale.DeliveryDate,
                        ShippingDate = sale.ShippingDate,
                        NouDocStatusID = sale.NouDocStatusID,
                        Remarks = sale.Remarks,
                        DispatchNote1 = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        TotalExVAT = sale.TotalExVAT,
                        PopUpNote = sale.PopUpNote,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID_OrderCreation = sale.SalesEmployeeID,
                        UserMasterID_SalesEmployee = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        PORequired = sale.PORequired,
                        CustomerPOReference = sale.CustomerPOReference,
                        BPMasterID_Agent = sale.AgentID
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        order.BPMasterSnapshotID_Customer = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Customer = snapshot.BPMasterID;
                    }

                    if (sale.Haulier != null)
                    {
                        // add the haulier snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Haulier.BPMasterID,
                            BPMaster_Name = sale.Haulier.Name,
                            BPMaster_Code = sale.Haulier.Code,
                            BPMaster_UpLift = sale.Haulier.Uplift,
                            BPMaster_CreditLimit = sale.Haulier.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Haulier.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        order.BPMasterSnapshotID_Haulier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Haulier = snapshot.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress?.Details?.AddressLine1 ?? string.Empty,
                            AddressLine2 = sale.DeliveryAddress?.Details?.AddressLine2 ?? string.Empty,
                            AddressLine3 = sale.DeliveryAddress?.Details?.AddressLine3 ?? string.Empty,
                            AddressLine4 = sale.DeliveryAddress?.Details?.AddressLine4 ?? string.Empty,
                            PostCode = sale.DeliveryAddress?.Details?.PostCode ?? string.Empty,
                            Shipping = sale.DeliveryAddress?.Details?.Shipping,
                            Billing = sale.DeliveryAddress?.Details?.Billing,
                            PlantID = sale.DeliveryAddress?.Details?.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();

                        order.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Delivery = sale.DeliveryAddress?.Details?.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        // add the address snapshot
                        var invoiceSnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress?.Details?.AddressLine1 ?? string.Empty,
                            AddressLine2 = sale.DeliveryAddress?.Details?.AddressLine2 ?? string.Empty,
                            AddressLine3 = sale.DeliveryAddress?.Details?.AddressLine3 ?? string.Empty,
                            AddressLine4 = sale.DeliveryAddress?.Details?.AddressLine4 ?? string.Empty,
                            PostCode = sale.DeliveryAddress?.Details?.PostCode ?? string.Empty,
                            Shipping = sale.DeliveryAddress?.Details?.Shipping,
                            Billing = sale.DeliveryAddress?.Details?.Billing,
                            PlantID = sale.DeliveryAddress?.Details?.PlantID
                        };

                        entities.BPAddressSnapshots.Add(invoiceSnapShot);
                        entities.SaveChanges();

                        order.BPAddressSnapshotID_Invoice = invoiceSnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.AROrders.Add(order);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("Order Created: Id:{0}", order.AROrderID));

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            this.Log.LogDebug(this.GetType(), string.Format("Base quote id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var orderAttachment = new OrderAttachment
                        {
                            AROrderID = order.AROrderID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.OrderAttachments.Add(orderAttachment);
                    }

                    // save the details
                    var saleID = order.AROrderID;
                    var localOrderDetails = new List<AROrderDetail>();

                    this.Log.LogDebug(this.GetType(), "Adding sale order items");
                    foreach (var detail in sale.SaleDetails)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("INMaster:{0}", detail.INMasterID));

                        INMasterSnapshot snapshot;
                        try
                        {
                            // Add to the master snapshot table first
                            snapshot = new INMasterSnapshot
                            {
                                INMasterID = detail.InventoryItem.Master.INMasterID,
                                Code = detail.InventoryItem.Master.Code,
                                Name = detail.InventoryItem.Master.Name,
                                MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                MinWeight = detail.InventoryItem.Master.MinWeight,
                                NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                            };
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), string.Format("Rectifying Snapshot error:{0}", ex.Message));
                            snapshot = new INMasterSnapshot
                            {
                                INMasterID = 0,
                                Code = string.Empty,
                                Name = string.Empty,
                                MaxWeight = 0,
                                MinWeight = 0,
                                NominalWeight = 0,
                                TypicalPieces = 0
                            };
                        }

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var orderDetail = new AROrderDetail
                        {
                            AROrderID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = null,
                            DiscountAmount = null,
                            DiscountPrice = null,
                            LineDiscountAmount = null,
                            LineDiscountPercentage = null,
                            UnitPriceAfterDiscount = detail.UnitPrice,
                            PriceBookUnitPrice = detail.UnitPriceOnPriceBook,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            Notes = detail.Notes,
                            CostTotal = detail.CostTotal,
                            Margin = detail.Margin,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterSnapshotID = snapshot.INMasterSnapshotID
                        };

                        localOrderDetails.Add(orderDetail);
                    }

                    entities.AROrderDetails.AddRange(localOrderDetails);
                    // mark the corresponding quote(if any) as completed.
                    //var dbQuote = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == sale.SaleID);
                    //if (dbQuote != null)
                    //{
                    //    dbQuote.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                    //}

                    entities.SaveChanges();

                    var orderBatchData = sale.SaleDetails.Where(x => x.OrderBatchData != null && x.OrderBatchData.Any())
                        .SelectMany(x => x.OrderBatchData).ToList();
                    if (orderBatchData != null && orderBatchData.Any())
                    {
                        foreach (var orderBatch in orderBatchData)
                        {
                            try
                            {
                                entities.App_AddOrderBatchData(saleID, orderBatch.INMasterID, orderBatch.BatchID,
                                    orderBatch.QuantityOrdered, orderBatch.WeightOrdered, orderBatch.Enforce);
                            }
                            catch (Exception e)
                            {
                                this.Log.LogError(this.GetType(), string.Format("AddNewOrder Batch Data(): Exception:{0} Inner:{1}", e.Message, e.InnerException));
                            }
                        }
                    }

                    if (sale.Copy)
                    {
                        var dispatchId = new System.Data.Entity.Core.Objects.ObjectParameter("DispatchID", typeof(int));
                        entities.App_CopySaleOrderToDispatch(order.AROrderID, dispatchId);
                        if (dispatchId.Value != null)
                        {
                            sale.TopDocumentID = dispatchId.Value.ToInt();
                        }
                    }

                    this.Log.LogDebug(this.GetType(), "New order successfully created");
                    return order.AROrderID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates an existing order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateOrder(Sale sale)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdateOrder(): Attempting to update order. " +
                                                           "Contact:{0} DiscountIncVat:{1} Doc Numbering Type:{2} Grand Total Inc Vat:{3}" +
                                                           " NouDocStatus:{4} Quote Valid Date:{5} Route:{6} Sales Emp:{7} Sub Total Inc Vat:{8}" +
                                                           " Total Ex Vat:{9} VAT:{10} Sale Details Count:{11}, Base Doc Ref Id:{12}, Number:{13}",
                                                           sale.BPContactID, sale.DiscountIncVAT, sale.DocumentNumberingID,
                                                           sale.GrandTotalIncVAT, sale.NouDocStatusID, sale.QuoteValidDate, sale.RouteID,
                                                           sale.SalesEmployeeID, sale.SubTotalExVAT, sale.TotalExVAT, sale.VAT, sale.SaleDetails.Count, sale.BaseDocumentReferenceID, sale.Number));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.AROrders.FirstOrDefault(x => x.AROrderID == sale.SaleID && x.Deleted == null);

                    if (sale.BaseDocumentReferenceID.HasValue)
                    {
                        var quote = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == sale.BaseDocumentReferenceID);
                        if (quote == null)
                        {
                            this.Log.LogError(this.GetType(), "No quote match batch doc reference id. Resetting base doc to null");
                            sale.BaseDocumentReferenceID = null;
                        }
                    }

                    if (dbOrder != null)
                    {
                        dbOrder.BPMasterID_Customer = sale.Customer.BPMasterID;
                        dbOrder.BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null;
                        dbOrder.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbOrder.RouteID = sale.RouteID;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.BaseDocumentReferenceID = sale.BaseDocumentReferenceID;
                        dbOrder.DocumentDate = sale.DocumentDate;
                        dbOrder.CreationDate = sale.CreationDate;
                        dbOrder.DeliveryDate = sale.DeliveryDate;
                        dbOrder.ShippingDate = sale.ShippingDate;
                        dbOrder.NouDocStatusID = sale.NouDocStatusID;
                        dbOrder.Remarks = sale.Remarks;
                        dbOrder.DispatchNote1 = sale.DocketNote;
                        dbOrder.InvoiceNote = sale.InvoiceNote;
                        dbOrder.TotalExVAT = sale.TotalExVAT;
                        dbOrder.DiscountPercentage = sale.DiscountPercentage;
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = sale.SubTotalExVAT;
                        dbOrder.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbOrder.UserMasterID_SalesEmployee = sale.SalesEmployeeID;
                        dbOrder.PORequired = sale.PORequired;
                        dbOrder.Deleted = sale.Deleted;
                        dbOrder.CustomerPOReference = sale.CustomerPOReference;
                        dbOrder.BPMasterID_Agent = sale.AgentID;

                        foreach (var attachment in sale.Attachments)
                        {
                            var dbAttachment =
                                    entities.OrderAttachments.FirstOrDefault(
                                        x => x.OrderAttachmentID == attachment.SaleAttachmentID);

                            if (dbAttachment != null)
                            {
                                // updating an existing attachment
                                if (attachment.Deleted != null)
                                {
                                    entities.OrderAttachments.Remove(dbAttachment);
                                }
                                else
                                {
                                    dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                    dbAttachment.FileName = attachment.FileName;
                                    dbAttachment.File = attachment.File;
                                }
                            }
                            else
                            {
                                // adding a new attachment
                                var localAttachment = new OrderAttachment
                                {
                                    AttachmentDate = attachment.AttachmentDate,
                                    AROrderID = sale.SaleID,
                                    FileName = attachment.FileName,
                                    File = attachment.File
                                };

                                entities.OrderAttachments.Add(localAttachment);
                            }
                        }

                        var localOrderDetails = new List<AROrderDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            //this.Log.LogDebug(this.GetType(), string.Format(
                            //    "Discount Amount:{0} Discount Percentage:{1} Discount Price:{2} INMaster:{3} Line Dis Amount:{4} Line Dis Percentage:{5}" +
                            //    "Quantity Del:{6} Quantity ordered:{7} Sale:{8} TotalExVat:{9} TotalIncVat:{10} Unit Price:{11} Unit Price/Discount:{12}" +
                            //    "Vat:{13} Wgt Del:{14} Wgt Ordered:{15}",
                            //    detail.DiscountAmount, detail.DiscountPercentage, detail.DiscountPrice, detail.INMasterID, detail.LineDiscountAmount,
                            //    detail.LineDiscountPercentage, detail.QuantityDelivered, detail.QuantityOrdered, "n/a", detail.TotalExVAT, detail.TotalIncVAT,
                            //    detail.UnitPrice, detail.UnitPriceAfterDiscount, detail.VatCodeID, detail.WeightDelivered, detail.WeightOrdered));

                            var dbOrderDetail =
                                entities.AROrderDetails.FirstOrDefault(
                                    x => x.AROrderDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbOrderDetail != null)
                            {
                                // existing sale item
                                dbOrderDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbOrderDetail.WeightOrdered = detail.WeightOrdered;
                                dbOrderDetail.UnitPrice = detail.UnitPrice;
                                dbOrderDetail.DiscountPercentage = null;
                                dbOrderDetail.DiscountAmount = null;
                                dbOrderDetail.DiscountPrice = null;
                                dbOrderDetail.LineDiscountAmount = null;
                                dbOrderDetail.LineDiscountPercentage = null;
                                dbOrderDetail.UnitPriceAfterDiscount = detail.UnitPrice;
                                dbOrderDetail.VAT = detail.VAT;
                                dbOrderDetail.VATCodeID = detail.VatCodeID;
                                dbOrderDetail.CostTotal = detail.CostTotal;
                                dbOrderDetail.Margin = detail.Margin;
                                dbOrderDetail.TotalExclVAT = detail.TotalExVAT;
                                dbOrderDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbOrderDetail.INMasterID = detail.INMasterID;
                                dbOrderDetail.PriceBookUnitPrice = detail.UnitPriceOnPriceBook;
                                dbOrderDetail.Notes = detail.Notes;
                                dbOrderDetail.PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID;
                                dbOrderDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order

                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = detail.InventoryItem.Master.INMasterID,
                                    Code = detail.InventoryItem.Master.Code,
                                    Name = detail.InventoryItem.Master.Name,
                                    MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                    MinWeight = detail.InventoryItem.Master.MinWeight,
                                    NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                    TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);
                                entities.SaveChanges();

                                var orderDetail = new AROrderDetail
                                {
                                    AROrderID = sale.SaleID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = null,
                                    DiscountAmount = null,
                                    DiscountPrice = null,
                                    LineDiscountAmount = null,
                                    LineDiscountPercentage = null,
                                    UnitPriceAfterDiscount = detail.UnitPrice,
                                    PriceBookUnitPrice = detail.UnitPriceOnPriceBook,
                                    VAT = detail.VAT,
                                    CostTotal = detail.CostTotal,
                                    Margin = detail.Margin,
                                    Notes = detail.Notes,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterSnapshotID = snapshot.INMasterSnapshotID
                                };

                                localOrderDetails.Add(orderDetail);
                            }
                        }

                        if (localOrderDetails.Any())
                        {
                            entities.AROrderDetails.AddRange(localOrderDetails);
                        }

                        sale.SaleID = dbOrder.AROrderID;
                    }

                    entities.SaveChanges();

                    var orderBatchData = sale.SaleDetails.Where(x => x.OrderBatchData != null && x.OrderBatchData.Any())
                        .SelectMany(x => x.OrderBatchData).ToList();
                    if (orderBatchData != null && orderBatchData.Any())
                    {
                        foreach (var orderBatch in orderBatchData)
                        {
                            try
                            {
                                entities.App_AddOrderBatchData(sale.SaleID, orderBatch.INMasterID, orderBatch.BatchID,
                                    orderBatch.QuantityOrdered, orderBatch.WeightOrdered, orderBatch.Enforce);
                            }
                            catch (Exception e)
                            {
                                this.Log.LogError(this.GetType(), string.Format("AddNewOrder Batch Data(): Exception:{0} Inner:{1}", e.Message, e.InnerException));
                            }
                        }
                    }

                    if (sale.Copy)
                    {
                        var dispatchId = new System.Data.Entity.Core.Objects.ObjectParameter("DispatchID", typeof(int));
                        entities.App_CopySaleOrderToDispatch(sale.SaleID, dispatchId);
                        if (dispatchId.Value != null)
                        {
                            sale.TopDocumentID = dispatchId.Value.ToInt();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("UpdateOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes an order detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveArOrderDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveArOrderDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.AROrderDetails.FirstOrDefault(x => x.AROrderDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Gets the batch order data for a product line.
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public IList<App_GetOrderBatchData_Result> GetOrderBatchData(int orderLineId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetOrderBatchData(orderLineId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
      
            return null;
        }

        /// <summary>
        /// Copies a dispatch docket to an invoice.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>A newly created invoice number.</returns>
        public int CopyDispatchToInvoice(int dispatchId, int userId, int deviceId)
        {
            this.Log.LogInfo(this.GetType(), $"Copying dispatch {dispatchId} to an invoice");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoiceId = new System.Data.Entity.Core.Objects.ObjectParameter("InvoiceID", typeof(int));
                    entities.App_CopyDispatchToInvoice(dispatchId, userId, deviceId,invoiceId);
                    if (invoiceId.Value != null)
                    {
                        var invoiceNo = invoiceId.Value.ToInt();
                        this.Log.LogInfo(this.GetType(), $"Invoice no {invoiceNo} created");
                        return invoiceNo;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
          
            return 0;
        }

        /// <summary>
        /// Prices an invoice docket, as opposed to creating it from a priced dispatch docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id used to price it.</param>
        /// <param name="invoiceId">The invoice id if updating.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Invoice number if successful, otherwise 0.</returns>
        public int PriceInvoice(int dispatchId, int invoiceId, int userId, int deviceId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoiceNo = new System.Data.Entity.Core.Objects.ObjectParameter("InvoiceNo", typeof(int));
                    entities.App_PriceInvoice(dispatchId, invoiceId, userId, deviceId, invoiceNo);
                    if (invoiceNo.Value != null)
                    {
                        var invoiceNumber = invoiceNo.Value.ToInt();
                        this.Log.LogInfo(this.GetType(), $"Invoice no {invoiceNumber} created");
                        return invoiceNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Determines if a sale orders master dispatch docket has any transactions recorded against it.
        /// </summary>
        /// <param name="orderId">The order to check.</param>
        /// <returns>A flag, indicating whether the sale orders master dispatch docket has any transactions recorded against it. </returns>
        public bool DoesMasterDocumentHaveTransactions(int orderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dispatch = entities.ARDispatches.FirstOrDefault(x => 
                        x.BaseDocumentReferenceID == orderId && x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID 
                        && x.Deleted == null);

                    if (dispatch != null)
                    {
                        var ids = new List<int?>();
                        var details = dispatch.ARDispatchDetails;
                        foreach (var detail in details)
                        {
                            ids.Add(detail.ARDispatchDetailID.ToNullableInt());
                        }

                        return
                            entities.StockTransactions.Any(
                                x =>
                                    x.Deleted == null &&
                                    x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId &&
                                    ids.Contains(x.MasterTableID));
                        //return dispatch.ARDispatchDetails.Any(x => (x.QuantityDelivered > 0 || x.WeightDelivered > 0) && x.Deleted == null);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllOrders(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the orders");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.AROrders
                              where orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.AROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  RouteID = order.RouteID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  PopUpNote = order.PopUpNote,
                                  DocketNote = order.DispatchNote1,
                                  DispatchNote1PopUp = order.DispatchNote1PopUp,
                                  InvoiceNote = order.InvoiceNote,
                                  TotalExVAT = order.TotalExVAT,
                                  CustomerPOReference = order.CustomerPOReference,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  OtherReference = order.OtherReference,
                                  SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  PORequired = order.PORequired,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.OrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.OrderAttachmentID,
                                                     SaleAttachmentID = attachment.AROrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                              //    SaleDetails = (from detail in order.AROrderDetails
                              //                   where detail.Deleted == null
                              //                   select new SaleDetail
                              //                   {
                              //                       SaleDetailID = detail.AROrderDetailID,
                              //                       SaleID = detail.AROrderID,
                              //                       QuantityOrdered = detail.QuantityOrdered,
                              //                       WeightOrdered = detail.WeightOrdered,
                              //                       //QuantityDelivered = detail.QuantityDelivered,
                              //                       //WeightDelivered = detail.WeightDelivered,
                              //                       UnitPrice = detail.UnitPrice,
                              //                       DiscountPercentage = detail.DiscountPercentage,
                              //                       DiscountPrice = detail.DiscountPrice,
                              //                       LineDiscountPercentage = detail.LineDiscountPercentage,
                              //                       LineDiscountAmount = detail.LineDiscountAmount,
                              //                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                              //                       VAT = detail.VAT,
                              //                       TotalExVAT = detail.TotalExclVAT,
                              //                       TotalIncVAT = detail.TotalIncVAT,
                              //                       VatCodeID = detail.VATCodeID,
                              //                       LoadingSale = true,
                              //                       INMasterID = detail.INMasterID,
                              //                       PriceListID = detail.PriceListID ?? 0,
                              //                       NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                              //                       Deleted = detail.Deleted
                              //                   }).ToList(),
                              }).ToList();

                    //foreach (var order in orders)
                    //{
                    //    var dispatch =
                    //        entities.ARDispatches.FirstOrDefault(
                    //            x => x.Deleted == null && x.BaseDocumentReferenceID == order.SaleID);
                    //    if (dispatch != null)
                    //    {
                    //        order.DispatchNumber = dispatch.Number;

                    //        var invoice =
                    //            entities.ARInvoices.FirstOrDefault(
                    //                x => x.Deleted == null && x.BaseDocumentReferenceID == dispatch.ARDispatchID);
                    //        if (invoice != null)
                    //        {
                    //            order.InvoiceNumber = invoice.Number;
                    //        }
                    //    }
                    //}
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllOrdersByDate(IList<int?> orderStatuses, DateTime start, DateTime end, string dateType)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the orders");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (dateType.Equals(Strings.ShippingDate))
                    {
                        orders = (from order in entities.AROrders
                                  where orderStatuses.Contains(order.NouDocStatusID)
                                  && order.Deleted == null
                                  && (order.ShippingDate >= start && order.ShippingDate <= end)
                                  orderby order.CreationDate
                                  select new Sale
                                  {
                                      SaleID = order.AROrderID,
                                      DocumentNumberingID = order.DocumentNumberingID,
                                      Number = order.Number,
                                      BPContactID = order.BPContactID,
                                      RouteID = order.RouteID,
                                      BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                      DocumentDate = order.DocumentDate,
                                      CreationDate = order.CreationDate,
                                      DeliveryDate = order.DeliveryDate,
                                      ShippingDate = order.ShippingDate,
                                      DeviceId = order.DeviceID,
                                      EditDate = order.EditDate,
                                      Printed = order.Printed,
                                      NouDocStatusID = order.NouDocStatusID,
                                      NouDocStatus = order.NouDocStatu.Value,
                                      Remarks = order.Remarks,
                                      PopUpNote = order.PopUpNote,
                                      DocketNote = order.DispatchNote1,
                                      DispatchNote1PopUp = order.DispatchNote1PopUp,
                                      InvoiceNote = order.InvoiceNote,
                                      TotalExVAT = order.TotalExVAT,
                                      CustomerPOReference = order.CustomerPOReference,
                                      VAT = order.VAT,
                                      UserIDCreation = order.UserMasterID_OrderCreation,
                                      SubTotalExVAT = order.SubTotalExVAT,
                                      DiscountIncVAT = order.DiscountIncVAT,
                                      GrandTotalIncVAT = order.GrandTotalIncVAT,
                                      DiscountPercentage = order.DiscountPercentage,
                                      OtherReference = order.OtherReference,
                                      SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                      DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                      InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                      DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                      MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                      BPHaulier = order.BPMasterHaulier,
                                      BPCustomer = order.BPMasterCustomer,
                                      PORequired = order.PORequired,
                                      Deleted = order.Deleted,
                                      Attachments = (from attachment in order.OrderAttachments
                                                     where attachment.Deleted == null
                                                     select new Attachment
                                                     {
                                                         AttachmentID = attachment.OrderAttachmentID,
                                                         SaleAttachmentID = attachment.AROrderID,
                                                         AttachmentDate = attachment.AttachmentDate,
                                                         FilePath = attachment.FilePath,
                                                         FileName = attachment.FileName,
                                                         File = attachment.File,
                                                         Deleted = attachment.Deleted
                                                     }).ToList(),

                                  }).ToList();

                    }
                    else if (dateType.Equals(Strings.DeliveryDate))
                    {
                        orders = (from order in entities.AROrders
                                  where orderStatuses.Contains(order.NouDocStatusID)
                                  && order.Deleted == null
                                  && (order.DeliveryDate >= start && order.DeliveryDate <= end)
                                  orderby order.CreationDate
                                  select new Sale
                                  {
                                      SaleID = order.AROrderID,
                                      DocumentNumberingID = order.DocumentNumberingID,
                                      Number = order.Number,
                                      BPContactID = order.BPContactID,
                                      RouteID = order.RouteID,
                                      BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                      DocumentDate = order.DocumentDate,
                                      CreationDate = order.CreationDate,
                                      DeliveryDate = order.DeliveryDate,
                                      ShippingDate = order.ShippingDate,
                                      DeviceId = order.DeviceID,
                                      EditDate = order.EditDate,
                                      Printed = order.Printed,
                                      NouDocStatusID = order.NouDocStatusID,
                                      NouDocStatus = order.NouDocStatu.Value,
                                      Remarks = order.Remarks,
                                      PopUpNote = order.PopUpNote,
                                      DocketNote = order.DispatchNote1,
                                      DispatchNote1PopUp = order.DispatchNote1PopUp,
                                      InvoiceNote = order.InvoiceNote,
                                      TotalExVAT = order.TotalExVAT,
                                      CustomerPOReference = order.CustomerPOReference,
                                      VAT = order.VAT,
                                      UserIDCreation = order.UserMasterID_OrderCreation,
                                      SubTotalExVAT = order.SubTotalExVAT,
                                      DiscountIncVAT = order.DiscountIncVAT,
                                      GrandTotalIncVAT = order.GrandTotalIncVAT,
                                      DiscountPercentage = order.DiscountPercentage,
                                      OtherReference = order.OtherReference,
                                      SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                      DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                      InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                      DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                      MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                      BPHaulier = order.BPMasterHaulier,
                                      BPCustomer = order.BPMasterCustomer,
                                      PORequired = order.PORequired,
                                      Deleted = order.Deleted,
                                      Attachments = (from attachment in order.OrderAttachments
                                                     where attachment.Deleted == null
                                                     select new Attachment
                                                     {
                                                         AttachmentID = attachment.OrderAttachmentID,
                                                         SaleAttachmentID = attachment.AROrderID,
                                                         AttachmentDate = attachment.AttachmentDate,
                                                         FilePath = attachment.FilePath,
                                                         FileName = attachment.FileName,
                                                         File = attachment.File,
                                                         Deleted = attachment.Deleted
                                                     }).ToList(),

                                  }).ToList();

                    }
                    else
                    {
                        orders = (from order in entities.AROrders
                                  where orderStatuses.Contains(order.NouDocStatusID)
                                  && order.Deleted == null
                                  && (order.DocumentDate >= start && order.DocumentDate <= end)
                                  orderby order.CreationDate
                                  select new Sale
                                  {
                                      SaleID = order.AROrderID,
                                      DocumentNumberingID = order.DocumentNumberingID,
                                      Number = order.Number,
                                      BPContactID = order.BPContactID,
                                      RouteID = order.RouteID,
                                      BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                      DocumentDate = order.DocumentDate,
                                      CreationDate = order.CreationDate,
                                      DeliveryDate = order.DeliveryDate,
                                      ShippingDate = order.ShippingDate,
                                      DeviceId = order.DeviceID,
                                      EditDate = order.EditDate,
                                      Printed = order.Printed,
                                      NouDocStatusID = order.NouDocStatusID,
                                      NouDocStatus = order.NouDocStatu.Value,
                                      Remarks = order.Remarks,
                                      PopUpNote = order.PopUpNote,
                                      DocketNote = order.DispatchNote1,
                                      DispatchNote1PopUp = order.DispatchNote1PopUp,
                                      InvoiceNote = order.InvoiceNote,
                                      TotalExVAT = order.TotalExVAT,
                                      CustomerPOReference = order.CustomerPOReference,
                                      VAT = order.VAT,
                                      UserIDCreation = order.UserMasterID_OrderCreation,
                                      SubTotalExVAT = order.SubTotalExVAT,
                                      DiscountIncVAT = order.DiscountIncVAT,
                                      GrandTotalIncVAT = order.GrandTotalIncVAT,
                                      DiscountPercentage = order.DiscountPercentage,
                                      OtherReference = order.OtherReference,
                                      SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                      DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                      InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                      DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                      MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                      BPHaulier = order.BPMasterHaulier,
                                      BPCustomer = order.BPMasterCustomer,
                                      PORequired = order.PORequired,
                                      Deleted = order.Deleted,
                                      Attachments = (from attachment in order.OrderAttachments
                                                     where attachment.Deleted == null
                                                     select new Attachment
                                                     {
                                                         AttachmentID = attachment.OrderAttachmentID,
                                                         SaleAttachmentID = attachment.AROrderID,
                                                         AttachmentDate = attachment.AttachmentDate,
                                                         FilePath = attachment.FilePath,
                                                         FileName = attachment.FileName,
                                                         File = attachment.File,
                                                         Deleted = attachment.Deleted
                                                     }).ToList(),

                                  }).ToList();

                    }
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the orders.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllOrdersForReport(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the orders");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.AROrders
                              where orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.AROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  RouteID = order.RouteID,
                                  RouteMaster = entities.RouteMasters.FirstOrDefault(x => x.RouteID == order.RouteID),
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = entities.ARDispatches.FirstOrDefault(x => x.BaseDocumentReferenceID == order.AROrderID).NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  PopUpNote = order.PopUpNote,
                                  DocketNote = order.DispatchNote1,
                                  DispatchNote1PopUp = order.DispatchNote1PopUp,
                                  InvoiceNote = order.InvoiceNote,
                                  TotalExVAT = order.TotalExVAT,
                                  CustomerPOReference = order.CustomerPOReference,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  OtherReference = order.OtherReference,
                                  SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  PORequired = order.PORequired,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.OrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.OrderAttachmentID,
                                                     SaleAttachmentID = attachment.AROrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList()}).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetOrders(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the orders for customer id:{0}", customerId));
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.AROrders
                              where order.BPMasterID_Customer == customerId
                              && order.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID 
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.AROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  RouteID = order.RouteID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  DocketNote = order.DispatchNote1,
                                  DispatchNote1PopUp = order.DispatchNote1PopUp,
                                  CustomerPOReference = order.CustomerPOReference,
                                  InvoiceNote = order.InvoiceNote,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  OtherReference = order.OtherReference,
                                  SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  PORequired = order.PORequired,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.OrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.OrderAttachmentID,
                                                     SaleAttachmentID = attachment.AROrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.AROrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.AROrderDetailID,
                                                     SaleID = detail.AROrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     Notes = detail.Notes,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentOrders(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the recent orders for customer id:{0}", customerId));
            var orders = new List<Sale>();

            try
            {
                IList<App_GetRecentSaleOrders_Result> dbOrders;
                using (var entities = new NouvemEntities())
                {
                    dbOrders = entities.App_GetRecentSaleOrders(customerId).ToList();
                }

                var groupedOrders = dbOrders.OrderByDescending(x => x.AROrderID).GroupBy(x => x.AROrderID).ToList();
                foreach (var localOrders in groupedOrders)
                {
                    var arOrder = new Sale
                    {
                        SaleID = localOrders.First().AROrderID,
                        Number = localOrders.First().Number,
                        CreationDate = localOrders.First().CreationDate,
                        ShippingDate = localOrders.First().ShippingDate,
                        DeliveryDate = localOrders.First().DeliveryDate,
                        SaleDetails = new List<SaleDetail>()
                    };

                    foreach (var detail in localOrders)
                    {
                        arOrder.SaleDetails.Add(new SaleDetail
                        {
                            IgnoreStockRetrieval = true,
                            LoadingSale = true,
                            SaleID = detail.AROrderID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID ?? 0
                        });
                    }

                    orders.Add(arOrder);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var receipt in orders)
            {
                receipt.Attachments = new List<Attachment>();
            }

            return orders;
        }

        ///// <summary>
        ///// Retrieves the orders (and associated order details) for the input customer.
        ///// </summary>
        ///// <param name="customerId">The customer id.</param>
        ///// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        //public IList<Sale> GetRecentOrders(int customerId)
        //{
        //    this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the recent orders for customer id:{0}", customerId));
        //    var orders = new List<Sale>();

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            orders = (from order in entities.AROrders
        //                      where order.BPMasterID_Customer == customerId
        //                      && order.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID
        //                      && order.Deleted == null
        //                      orderby order.AROrderID descending 
        //                      select new Sale
        //                      {
        //                          SaleID = order.AROrderID,
        //                          DocumentNumberingID = order.DocumentNumberingID,
        //                          Number = order.Number,
        //                          BPContactID = order.BPContactID,
        //                          RouteID = order.RouteID,
        //                          DeviceId = order.DeviceID,
        //                          EditDate = order.EditDate,
        //                          BaseDocumentReferenceID = order.BaseDocumentReferenceID,
        //                          DocumentDate = order.DocumentDate,
        //                          CreationDate = order.CreationDate,
        //                          DeliveryDate = order.DeliveryDate,
        //                          ShippingDate = order.ShippingDate,
        //                          Printed = order.Printed,
        //                          NouDocStatusID = order.NouDocStatusID,
        //                          NouDocStatus = order.NouDocStatu.Value,
        //                          PopUpNote = order.PopUpNote,
        //                          Remarks = order.Remarks,
        //                          DocketNote = order.DispatchNote1,
        //                          DispatchNote1PopUp = order.DispatchNote1PopUp,
        //                          CustomerPOReference = order.CustomerPOReference,
        //                          InvoiceNote = order.InvoiceNote,
        //                          TotalExVAT = order.TotalExVAT,
        //                          VAT = order.VAT,
        //                          SubTotalExVAT = order.SubTotalExVAT,
        //                          DiscountIncVAT = order.DiscountIncVAT,
        //                          GrandTotalIncVAT = order.GrandTotalIncVAT,
        //                          DiscountPercentage = order.DiscountPercentage,
        //                          OtherReference = order.OtherReference,
        //                          SalesEmployeeID = order.UserMasterID_SalesEmployee,
        //                          DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
        //                          InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
        //                          DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
        //                          MainContact = new BusinessPartnerContact { Details = order.BPContact },
        //                          BPHaulier = order.BPMasterHaulier,
        //                          BPCustomer = order.BPMasterCustomer,
        //                          PORequired = order.PORequired,
        //                          Deleted = order.Deleted,
        //                          //Attachments = (from attachment in order.OrderAttachments
        //                          //               where attachment.Deleted == null
        //                          //               select new Attachment
        //                          //               {
        //                          //                   AttachmentID = attachment.OrderAttachmentID,
        //                          //                   SaleAttachmentID = attachment.AROrderID,
        //                          //                   AttachmentDate = attachment.AttachmentDate,
        //                          //                   FilePath = attachment.FilePath,
        //                          //                   FileName = attachment.FileName,
        //                          //                   File = attachment.File,
        //                          //                   Deleted = attachment.Deleted
        //                          //               }).ToList(),
        //                          SaleDetails = (from detail in order.AROrderDetails
        //                                         where detail.Deleted == null
        //                                         select new SaleDetail
        //                                         {
        //                                             LoadingSale = true,
        //                                             SaleDetailID = detail.AROrderDetailID,
        //                                             SaleID = detail.AROrderID,
        //                                             QuantityOrdered = detail.QuantityOrdered,
        //                                             WeightOrdered = detail.WeightOrdered,
        //                                             //QuantityDelivered = detail.QuantityDelivered,
        //                                             //WeightDelivered = detail.WeightDelivered,
        //                                             UnitPrice = detail.UnitPrice,
        //                                             DiscountPercentage = detail.DiscountPercentage,
        //                                             DiscountPrice = detail.DiscountPrice,
        //                                             LineDiscountPercentage = detail.LineDiscountPercentage,
        //                                             LineDiscountAmount = detail.LineDiscountAmount,
        //                                             UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
        //                                             CostTotal = detail.CostTotal,
        //                                             Margin = detail.Margin,
        //                                             VAT = detail.VAT,
        //                                             TotalExVAT = detail.TotalExclVAT,
        //                                             TotalIncVAT = detail.TotalIncVAT,
        //                                             VatCodeID = detail.VATCodeID,
        //                                             INMasterID = detail.INMasterID,
        //                                             PriceListID = detail.PriceListID ?? 0,
        //                                             NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
        //                                             Deleted = detail.Deleted
        //                                         }).ToList(),
        //                      }).Take(ApplicationSettings.RecentOrdersAmtToTake).ToList();
        //        }

        //        this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    foreach (var receipt in orders)
        //    {
        //        receipt.Attachments = new List<Attachment>();
        //    }

        //    return orders;
        //}

        /// <summary>
        /// Retrieves the quotes (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetOrdersByPartner(int partnerId, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the orders for partner id:{0}", partnerId));
            var orders = new List<Sale>();
     
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.AROrders
                              where order.BPMasterID_Customer == partnerId
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.AROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  RouteID = order.RouteID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  DocketNote = order.DispatchNote1,
                                  CustomerPOReference = order.CustomerPOReference,
                                  DispatchNote1PopUp = order.DispatchNote1PopUp,
                                  InvoiceNote = order.InvoiceNote,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  OtherReference = order.OtherReference,
                                  SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  PORequired = order.PORequired,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.OrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.OrderAttachmentID,
                                                     SaleAttachmentID = attachment.AROrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.AROrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.AROrderDetailID,
                                                     SaleID = detail.AROrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     CostTotal = detail.CostTotal,
                                                     Margin = detail.Margin,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        public Sale GetOrderByID(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetOrderByID(): Attempting to get the order for id:{0}", orderId));
            Sale arOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.AROrders.FirstOrDefault(x => x.AROrderID == orderId);
                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    arOrder = new Sale
                    {
                        SaleID = order.AROrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        RouteID = order.RouteID,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        PopUpNote = order.PopUpNote,
                        DocketNote = order.DispatchNote1,
                        DispatchNote1PopUp = order.DispatchNote1PopUp,
                        CustomerPOReference = order.CustomerPOReference,
                        InvoiceNote = order.InvoiceNote,
                        TotalExVAT = order.TotalExVAT,
                        AgentID = order.BPMasterID_Agent,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        OtherReference = order.OtherReference,
                        SalesEmployeeID = order.UserMasterID_SalesEmployee,
                        DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                        InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                        DeliveryContact = new BusinessPartnerContact {Details = order.BPContactDelivery},
                        MainContact = new BusinessPartnerContact {Details = order.BPContact},
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        PORequired = order.PORequired,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.OrderAttachments
                                       where attachment.Deleted == null
                            select new Attachment
                            {
                                AttachmentID = attachment.OrderAttachmentID,
                                SaleAttachmentID = attachment.AROrderID,
                                AttachmentDate = attachment.AttachmentDate,
                                FilePath = attachment.FilePath,
                                FileName = attachment.FileName,
                                File = attachment.File,
                                Deleted = attachment.Deleted
                            }).ToList(),
                        SaleDetails = (from detail in order.AROrderDetails
                                       where detail.Deleted == null
                            select new SaleDetail
                            {
                                LoadingSale = true,
                                SaleDetailID = detail.AROrderDetailID,
                                SaleID = detail.AROrderID,
                                QuantityOrdered = detail.QuantityOrdered,
                                WeightOrdered = detail.WeightOrdered,
                                //QuantityDelivered = detail.QuantityDelivered,
                                //WeightDelivered = detail.WeightDelivered,
                                UnitPrice = detail.UnitPrice,
                                Notes = detail.Notes,
                                DiscountPercentage = detail.DiscountPercentage,
                                CostTotal = detail.CostTotal,
                                Margin = detail.Margin,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountPercentage = detail.LineDiscountPercentage,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                UnitPriceOnPriceBook = detail.PriceBookUnitPrice,
                                VAT = detail.VAT,
                                TotalExVAT = detail.TotalExclVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                VatCodeID = detail.VATCodeID,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID ?? 0,
                                NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                Deleted = detail.Deleted
                            }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return arOrder;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        public Sale GetOrderByFirstLast(bool first)
        {
            Sale arOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    AROrder order = null;
                    if (first)
                    {
                        order = entities.AROrders.FirstOrDefault();
                    }
                    else
                    {
                        order = entities.AROrders.OrderByDescending(x => x.AROrderID).FirstOrDefault();
                    }

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    arOrder = new Sale
                    {
                        SaleID = order.AROrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        RouteID = order.RouteID,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        PopUpNote = order.PopUpNote,
                        DocketNote = order.DispatchNote1,
                        DispatchNote1PopUp = order.DispatchNote1PopUp,
                        CustomerPOReference = order.CustomerPOReference,
                        InvoiceNote = order.InvoiceNote,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        OtherReference = order.OtherReference,
                        AgentID = order.BPMasterID_Agent,
                        SalesEmployeeID = order.UserMasterID_SalesEmployee,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        PORequired = order.PORequired,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.OrderAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.OrderAttachmentID,
                                           SaleAttachmentID = attachment.AROrderID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.AROrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.AROrderDetailID,
                                           SaleID = detail.AROrderID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           //QuantityDelivered = detail.QuantityDelivered,
                                           //WeightDelivered = detail.WeightDelivered,
                                           Notes = detail.Notes,
                                           UnitPrice = detail.UnitPrice,
                                           CostTotal = detail.CostTotal,
                                           Margin = detail.Margin,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           UnitPriceOnPriceBook = detail.PriceBookUnitPrice,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return arOrder;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the inputid.
        /// </summary>
        /// <returns>An orders (and associated order details) for the input id.</returns>
        public Sale GetOrderByLastEdit()
        {
            Sale arOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.AROrders
                        .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    arOrder = new Sale
                    {
                        SaleID = order.AROrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        RouteID = order.RouteID,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        DocketNote = order.DispatchNote1,
                        DispatchNote1PopUp = order.DispatchNote1PopUp,
                        CustomerPOReference = order.CustomerPOReference,
                        InvoiceNote = order.InvoiceNote,
                        TotalExVAT = order.TotalExVAT,
                        PopUpNote = order.PopUpNote,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        OtherReference = order.OtherReference,
                        SalesEmployeeID = order.UserMasterID_SalesEmployee,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        PORequired = order.PORequired,
                        AgentID = order.BPMasterID_Agent,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.OrderAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.OrderAttachmentID,
                                           SaleAttachmentID = attachment.AROrderID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.AROrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.AROrderDetailID,
                                           SaleID = detail.AROrderID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           //QuantityDelivered = detail.QuantityDelivered,
                                           //WeightDelivered = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           UnitPriceOnPriceBook = detail.PriceBookUnitPrice,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           CostTotal = detail.CostTotal,
                                           Margin = detail.Margin,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return arOrder;
        }

        /// <summary>
        /// Retrieves the quotes (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetOrdersByName(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the orders for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.AROrders
                              where order.BPMasterCustomer.Name.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.AROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  RouteID = order.RouteID,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  PopUpNote = order.PopUpNote,
                                  Remarks = order.Remarks,
                                  DocketNote = order.DispatchNote1,
                                  CustomerPOReference = order.CustomerPOReference,
                                  DispatchNote1PopUp = order.DispatchNote1PopUp,
                                  InvoiceNote = order.InvoiceNote,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  OtherReference = order.OtherReference,
                                  SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  PORequired = order.PORequired,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.OrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.OrderAttachmentID,
                                                     SaleAttachmentID = attachment.AROrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.AROrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.AROrderDetailID,
                                                     SaleID = detail.AROrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     CostTotal = detail.CostTotal,
                                                     Margin = detail.Margin,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the qorders (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetOrdersByCode(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the orders for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.AROrders
                              where order.BPMasterCustomer.Code.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID) 
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.AROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  RouteID = order.RouteID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  PopUpNote = order.PopUpNote,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  CustomerPOReference = order.CustomerPOReference,
                                  DocketNote = order.DispatchNote1,
                                  DispatchNote1PopUp = order.DispatchNote1PopUp,
                                  InvoiceNote = order.InvoiceNote,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  OtherReference = order.OtherReference,
                                  SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  PORequired = order.PORequired,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.OrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.OrderAttachmentID,
                                                     SaleAttachmentID = attachment.AROrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.AROrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.AROrderDetailID,
                                                     SaleID = detail.AROrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     //QuantityDelivered = detail.QuantityDelivered,
                                                     //WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     CostTotal = detail.CostTotal,
                                                     Margin = detail.Margin,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        #endregion

        #region dispatch

        /// <summary> Adds a new purchase receipt (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        public int AddAPDispatch(Sale sale)
        {
            this.Log.LogWarning(this.GetType(), string.Format("7. AddAPDispatch():Adding new dispatch. Base Doc:{0}, Sale Items Count:{1}", sale.BaseDocumentReferenceID, sale.SaleDetails.Count));
            var saleID = 0;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new ARDispatch
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        DispatchContainerID = sale.DispatchContainerID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = sale.Remarks,
                        TotalExVAT = sale.TotalExVAT.ToDecimal(),
                        DeliveryDate = sale.DeliveryDate,
                        ShippingDate = sale.ShippingDate,
                        VAT = sale.VAT,
                        DeviceID = NouvemGlobal.DeviceId,
                        OtherReference = sale.OtherReference,
                        EditDate = DateTime.Now,
                        PopUpNote = sale.PopUpNote,
                        DispatchNote1 = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        UserMasterID_OrderCreation = sale.SalesEmployeeID,
                        CustomerPOReference = sale.CustomerPOReference,
                        RouteID = sale.RouteID,
                        BPMasterID_Agent = sale.AgentID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Customer = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Customer = sale.Customer.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress?.Details?.AddressLine1 ?? string.Empty,
                            AddressLine2 = sale.DeliveryAddress?.Details?.AddressLine2 ?? string.Empty,
                            AddressLine3 = sale.DeliveryAddress?.Details?.AddressLine3 ?? string.Empty,
                            AddressLine4 = sale.DeliveryAddress?.Details?.AddressLine4 ?? string.Empty,
                            PostCode = sale.DeliveryAddress?.Details?.PostCode ?? string.Empty,
                            Shipping = sale.DeliveryAddress?.Details?.Shipping,
                            Billing = sale.DeliveryAddress?.Details?.Billing,
                            PlantID = sale.DeliveryAddress?.Details?.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    //entities.SaveChanges();

                    if (sale.Haulier != null)
                    {
                        // add the haulier snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Haulier.BPMasterID,
                            BPMaster_Name = sale.Haulier.Name,
                            BPMaster_Code = sale.Haulier.Code,
                            BPMaster_UpLift = sale.Haulier.Uplift,
                            BPMaster_CreditLimit = sale.Haulier.CreditLimit,
                            BPGroup_BPGroupName = sale.Haulier.BPGroupName ?? string.Empty,
                            BPType_Type = Constant.Customer,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Haulier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Haulier = sale.Haulier.BPMasterID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        // add the address snapshot
                        var invoiceSnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress?.Details?.AddressLine1 ?? string.Empty,
                            AddressLine2 = sale.DeliveryAddress?.Details?.AddressLine2 ?? string.Empty,
                            AddressLine3 = sale.DeliveryAddress?.Details?.AddressLine3 ?? string.Empty,
                            AddressLine4 = sale.DeliveryAddress?.Details?.AddressLine4 ?? string.Empty,
                            PostCode = sale.DeliveryAddress?.Details?.PostCode ?? string.Empty,
                            Shipping = sale.DeliveryAddress?.Details?.Shipping,
                            Billing = sale.DeliveryAddress?.Details?.Billing,
                            PlantID = sale.DeliveryAddress?.Details?.PlantID
                        };

                        entities.BPAddressSnapshots.Add(invoiceSnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Invoice = invoiceSnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.ARDispatches.Add(order);

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var goodsReceiptAttachment = new ARDispatchAttachment
                        {
                            ARDispatchID = order.ARDispatchID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.ARDispatchAttachments.Add(goodsReceiptAttachment);
                    }

                    entities.SaveChanges();

                    // pass the newly created apgoodsreceipt id back out.
                    sale.SaleID = order.ARDispatchID;

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        var baseDoc = entities.AROrders.FirstOrDefault(x => x.AROrderID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            this.Log.LogWarning(this.GetType(), string.Format("8. Updating base order. Setting status to complete:{0}", baseDoc.AROrderID));
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                        }
                    }

                    // save the details
                    saleID = order.ARDispatchID;

                    var saleDetailsToProcess = sale.CopyingFromOrder
                        ? sale.SaleDetails
                        : sale.SaleDetails.Where(x => x.Process).ToList();

                    if (saleDetailsToProcess.Any(x => x.Process))
                    {
                        order.NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                    }

                    foreach (var detail in saleDetailsToProcess)
                    {
                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = detail.InventoryItem.Master.INMasterID,
                            Code = detail.InventoryItem.Master.Code,
                            Name = detail.InventoryItem.Master.Name,
                            MaxWeight = detail.InventoryItem.Master.MaxWeight,
                            MinWeight = detail.InventoryItem.Master.MinWeight,
                            NominalWeight = detail.InventoryItem.Master.NominalWeight,
                            TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);

                        var orderDetail = new ARDispatchDetail
                        {
                            ARDispatchID = saleID,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            QuantityDelivered = detail.QuantityDelivered,
                            WeightDelivered = detail.WeightDelivered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount ?? detail.UnitPrice,
                            Notes = detail.Notes,
                            VAT = detail.VAT,
                            CostTotal = detail.CostTotal,
                            Margin = detail.Margin,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterSnapshotID = snapshot.INMasterSnapshotID,
                            Comments = detail.Comments
                        };

                        entities.ARDispatchDetails.Add(orderDetail);
                        entities.SaveChanges();

                        if (detail.Concessions != null && detail.Concessions.Any())
                        {
                            foreach (var concession in detail.Concessions)
                            {
                                concession.ARDispatchDetailID = orderDetail.ARDispatchDetailID;
                                concession.ConcessionTime = DateTime.Now;
                                entities.Concessions.Add(concession);
                            }
                        }

                        #region stock transaction/traceability

                        var stockDetail = detail.StockDetailToProcess;
                        if (stockDetail != null)
                        {
                            var localAttributeId = stockDetail.AttributeID;
                            if (localAttributeId.IsNullOrZero())
                            {
                                var localAttribute = this.CreateAttribute(stockDetail);
                                entities.Attributes.Add(localAttribute);
                                entities.SaveChanges();
                                localAttributeId = localAttribute.AttributeID;
                            }

                            if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                            {
                                foreach (var nonStandardResponse in sale.NonStandardResponses)
                                {
                                    nonStandardResponse.AttributeID_Parent = localAttributeId;
                                    entities.Attributes.Add(nonStandardResponse);
                                    entities.SaveChanges();
                                }
                            }

                            var localTransaction = this.CreateTransaction(stockDetail);
                            localTransaction.MasterTableID = orderDetail.ARDispatchDetailID;
                            localTransaction.IsBox = stockDetail.IsBox;
                            localTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId;
                            localTransaction.AttributeID = localAttributeId;
                            if (localTransaction.TransactionQTY.IsNullOrZero())
                            {
                                localTransaction.TransactionQTY = 1;
                            }

                            if (localTransaction.Pieces.IsNullOrZero())
                            {
                                localTransaction.Pieces = 1;
                            }

                            if (stockDetail.BatchAttribute != null)
                            {
                                var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                batchAttribute.IsBatch = true;
                                var localBatch =
                                    entities.BatchNumbers.FirstOrDefault(
                                        x => x.BatchNumberID == localTransaction.BatchNumberID);

                                if (localBatch != null)
                                {
                                    if (localBatch.AttributeID == null)
                                    {
                                        entities.Attributes.Add(batchAttribute);
                                        entities.SaveChanges();
                                        localBatch.AttributeID = batchAttribute.AttributeID;
                                    }
                                    else
                                    {
                                        #region batch attribute update

                                        var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                        if (attribute != null)
                                        {
                                            this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                        }

                                        #endregion
                                    }
                                }
                            }

                            if (localTransaction.Serial.HasValue)
                            {
                                var transToConsume = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == stockDetail.StockTransactionIdToConsume);
                                if (transToConsume != null)
                                {
                                    transToConsume.Consumed = DateTime.Now;
                                    transToConsume.InLocation = false;
                                }
                            }

                            if (stockDetail.AutoSplittingStock)
                            {
                                var splitTransaction = this.CreateTransaction(stockDetail);
                                splitTransaction.MasterTableID = null;
                                splitTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                splitTransaction.INMasterID = stockDetail.AutoSplittingStockId;
                                splitTransaction.WarehouseID = stockDetail.BaseWarehouseID;
                                splitTransaction.MasterTableID = stockDetail.BaseMasterTableID;
                                splitTransaction.AttributeID = localAttributeId;
                                splitTransaction.SplitID = 1;
                                if (splitTransaction.TransactionQTY.IsNullOrZero())
                                {
                                    splitTransaction.TransactionQTY = 1;
                                }

                                if (splitTransaction.Pieces.IsNullOrZero())
                                {
                                    splitTransaction.Pieces = 1;
                                }

                                entities.StockTransactions.Add(splitTransaction);
                                entities.SaveChanges();

                                splitTransaction = this.CreateTransaction(stockDetail);
                                splitTransaction.MasterTableID = null;
                                splitTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                splitTransaction.Consumed = DateTime.Now;
                                splitTransaction.WarehouseID = stockDetail.BaseWarehouseID;
                                splitTransaction.MasterTableID = stockDetail.BaseMasterTableID;
                                splitTransaction.AttributeID = localAttributeId;
                                splitTransaction.SplitID = 1;
                                if (splitTransaction.TransactionQTY.IsNullOrZero())
                                {
                                    splitTransaction.TransactionQTY = 1;
                                }

                                if (splitTransaction.Pieces.IsNullOrZero())
                                {
                                    splitTransaction.Pieces = 1;
                                }

                                entities.StockTransactions.Add(splitTransaction);
                                entities.SaveChanges();
                            }

                            entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);

                            entities.StockTransactions.Add(localTransaction);
                            entities.SaveChanges();
                            sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;

                            if (!localTransaction.Serial.HasValue)
                            {
                                localTransaction.Serial = localTransaction.StockTransactionID;
                                entities.SaveChanges();
                            }

                            if (stockDetail.StockDetails != null && stockDetail.StockDetails.Any())
                            {
                                foreach (var localDetail in stockDetail.StockDetails)
                                {
                                    var trans = this.CreateTransaction(localTransaction);
                                    
                                    trans.INMasterID = localDetail.INMasterID;
                                    trans.NouTransactionTypeID = localDetail.NouTransactionTypeID;
                                    trans.TransactionWeight = localDetail.TransactionWeight;
                                    trans.TransactionQTY = localDetail.TransactionQty;
                                    trans.Consumed = DateTime.Now;
                                    trans.MasterTableID = localDetail.MasterTableID;
                                    entities.StockTransactions.Add(trans);
                                    entities.SaveChanges();
                                    trans.Serial = localDetail.DispatchProduct ? localTransaction.Serial : trans.StockTransactionID;
                                    entities.SaveChanges();

                                    if (trans.NouTransactionTypeID == 3)
                                    {
                                        entities.App_UpdateProductStockData(trans.INMasterID,
                                            trans.BatchNumberID, trans.TransactionQTY,
                                            trans.TransactionWeight, trans.WarehouseID, trans.NouTransactionTypeID);
                                    }
                                }
                            }
                        }

                        #endregion

                        // pass the id back out
                        detail.SaleDetailID = orderDetail.ARDispatchDetailID;
                    }

                    this.Log.LogWarning(this.GetType(), string.Format("9. Dispatch created. Id:{0}", order.ARDispatchID));
                    return order.ARDispatchID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("AddARDispatch():Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates an existing dispatch docket (docket and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARDispatch(Sale sale)
        {
            var saleID = 0;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == sale.SaleID);

                    if (dbOrder != null)
                    {
                        saleID = dbOrder.ARDispatchID;
                        dbOrder.NouDocStatusID = sale.IsOrderFilled()
                            ? NouvemGlobal.NouDocStatusFilled.NouDocStatusID
                            : NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                        dbOrder.TotalExVAT = sale.TotalExVAT.ToDecimal();
                        dbOrder.DiscountPercentage = sale.DiscountPercentage;
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = sale.SubTotalExVAT;
                        dbOrder.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.DispatchNote1 = sale.DocketNote;
                        dbOrder.OtherReference = sale.OtherReference;
                        dbOrder.InvoiceNote = sale.InvoiceNote;
                        dbOrder.CustomerPOReference = sale.CustomerPOReference;
                        dbOrder.DispatchContainerID = sale.DispatchContainerID;
                        dbOrder.BPMasterID_Agent = sale.AgentID;

                        if (!ApplicationSettings.TouchScreenMode)
                        {
                            if (sale.Customer != null && sale.Customer.BPMasterID > 0)
                            {
                                dbOrder.BPMasterID_Customer = sale.Customer.BPMasterID;
                            }
                            else if (sale.BPCustomer != null && sale.BPCustomer.BPMasterID > 0)
                            {
                                dbOrder.BPMasterID_Customer = sale.BPCustomer.BPMasterID;
                            }
                          
                            dbOrder.BPContactID = sale.MainContact != null
                                ? (int?)sale.MainContact.Details.BPContactID
                                : null;
                            dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                            dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null
                                ? (int?)sale.DeliveryAddress.Details.BPAddressID
                                : null;
                            dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null
                                ? (int?)sale.InvoiceAddress.Details.BPAddressID
                                : null;
                            dbOrder.DocumentDate = sale.DocumentDate;
                            dbOrder.CreationDate = sale.CreationDate;
                            dbOrder.Remarks = sale.Remarks;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.ShippingDate = sale.ShippingDate;
                            dbOrder.UserMasterID = sale.SalesEmployeeID;
                            dbOrder.PopUpNote = sale.PopUpNote;
                            dbOrder.Deleted = sale.Deleted;
                            dbOrder.RouteID = sale.RouteID;

                            foreach (var attachment in sale.Attachments)
                            {
                                var dbAttachment =
                                    entities.ARDispatchAttachments.FirstOrDefault(
                                        x => x.ARDispatchAttachmentID == attachment.SaleAttachmentID);

                                if (dbAttachment != null)
                                {
                                    // updating an existing attachment
                                    if (attachment.Deleted != null)
                                    {
                                        entities.ARDispatchAttachments.Remove(dbAttachment);
                                    }
                                    else
                                    {
                                        dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                        dbAttachment.FileName = attachment.FileName;
                                        dbAttachment.File = attachment.File;
                                    }
                                }
                                else
                                {
                                    // adding a new attachment
                                    var localAttachment = new ARDispatchAttachment
                                    {
                                        AttachmentDate = attachment.AttachmentDate,
                                        ARDispatchID = sale.SaleID,
                                        FileName = attachment.FileName,
                                        File = attachment.File
                                    };

                                    entities.ARDispatchAttachments.Add(localAttachment);
                                }
                            }
                        }

                        // update the details
                        var detail = sale.SaleDetails.FirstOrDefault(x => x.Process);
                        if (detail != null)
                        {
                            if (detail.SaleDetailID != 0)
                            {
                                // existing sale item
                                var dbOrderDetail =
                                    entities.ARDispatchDetails.FirstOrDefault(
                                        x => x.ARDispatchDetailID == detail.SaleDetailID);

                                if (dbOrderDetail == null)
                                {
                                    // serious error - should never happen
                                    this.Log.LogError(this.GetType(),
                                        string.Format("ARDispatchDetail:{0} not found in the database",
                                            detail.SaleDetailID));
                                    return false;
                                }

                                //this.Log.LogDebug(this.GetType(), string.Format("Updating ARDispatchDetail:{0}", dbOrderDetail.ARDispatchDetailID));
                                dbOrderDetail.QuantityDelivered = detail.QuantityDelivered.ToDecimal();
                                dbOrderDetail.WeightDelivered = detail.WeightDelivered;
                                dbOrderDetail.TotalExclVAT = detail.TotalExVAT;
                                dbOrderDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbOrderDetail.UnitPrice = detail.UnitPrice;
                                dbOrderDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbOrderDetail.CostTotal = detail.CostTotal;
                                dbOrderDetail.Margin = detail.Margin;
                                dbOrderDetail.Notes = detail.Notes;

                                if (!string.IsNullOrEmpty(detail.Comments))
                                {
                                    dbOrderDetail.Comments = detail.Comments;
                                }

                                if (detail.Concessions != null && detail.Concessions.Any())
                                {
                                    foreach (var concession in detail.Concessions.Where(x => x.ConcessionID == 0))
                                    {
                                        concession.ARDispatchDetailID = dbOrderDetail.ARDispatchDetailID;
                                        concession.ConcessionTime = DateTime.Now;
                                        entities.Concessions.Add(concession);
                                    }
                                }

                                #region stock transaction/traceability

                                var stockDetail = detail.StockDetailToProcess;
                                if (stockDetail != null)
                                {
                                    var localAttributeId = stockDetail.AttributeID;
                                    if (localAttributeId.IsNullOrZero())
                                    {
                                        var localAttribute = this.CreateAttribute(stockDetail);
                                        entities.Attributes.Add(localAttribute);
                                        entities.SaveChanges();
                                        localAttributeId = localAttribute.AttributeID;
                                    }
                                   
                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttributeId;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    localTransaction.MasterTableID = dbOrderDetail.ARDispatchDetailID;
                                    localTransaction.IsBox = stockDetail.IsBox;
                                    localTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId;
                                    localTransaction.AttributeID = localAttributeId;
                                    if (localTransaction.TransactionQTY.IsNullOrZero())
                                    {
                                        localTransaction.TransactionQTY = 1;
                                    }

                                    if (localTransaction.Pieces.IsNullOrZero())
                                    {
                                        localTransaction.Pieces = 1;
                                    }

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    if (localTransaction.Serial.HasValue)
                                    {
                                        var transToConsume = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == stockDetail.StockTransactionIdToConsume);
                                        if (transToConsume != null)
                                        {
                                            transToConsume.Consumed = DateTime.Now;
                                            transToConsume.InLocation = false;
                                        }
                                    }

                                    if (stockDetail.AutoSplittingStock)
                                    {
                                        var splitTransaction = this.CreateTransaction(stockDetail);
                                        splitTransaction.MasterTableID = null;
                                        splitTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                        splitTransaction.INMasterID = stockDetail.AutoSplittingStockId;
                                        splitTransaction.WarehouseID = stockDetail.BaseWarehouseID;
                                        splitTransaction.MasterTableID = stockDetail.BaseMasterTableID;
                                        splitTransaction.AttributeID = localAttributeId;
                                        splitTransaction.SplitID = 1;
                                        if (splitTransaction.TransactionQTY.IsNullOrZero())
                                        {
                                            splitTransaction.TransactionQTY = 1;
                                        }

                                        if (splitTransaction.Pieces.IsNullOrZero())
                                        {
                                            splitTransaction.Pieces = 1;
                                        }

                                        entities.StockTransactions.Add(splitTransaction);
                                        entities.SaveChanges();

                                        splitTransaction = this.CreateTransaction(stockDetail);
                                        splitTransaction.MasterTableID = null;
                                        splitTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                        splitTransaction.Consumed = DateTime.Now;
                                        splitTransaction.WarehouseID = stockDetail.BaseWarehouseID;
                                        splitTransaction.MasterTableID = stockDetail.BaseMasterTableID;
                                        splitTransaction.AttributeID = localAttributeId;
                                        splitTransaction.SplitID = 1;
                                        if (splitTransaction.TransactionQTY.IsNullOrZero())
                                        {
                                            splitTransaction.TransactionQTY = 1;
                                        }

                                        if (splitTransaction.Pieces.IsNullOrZero())
                                        {
                                            splitTransaction.Pieces = 1;
                                        }

                                        entities.StockTransactions.Add(splitTransaction);
                                        entities.SaveChanges();
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);

                                    if (detail.AutoBoxing && !detail.CompleteAutoBoxing)
                                    {
                                        localTransaction.TransactionWeight = 0;
                                        localTransaction.TransactionQTY = 0;
                                    }

                                    IList<StockTransaction> products = null;
                                    if (detail.CompleteAutoBoxing)
                                    {
                                        products = entities.StockTransactions.Where(x =>
                                            x.MasterTableID == localTransaction.MasterTableID
                                            && x.NouTransactionTypeID == 7
                                            && x.INMasterID == localTransaction.INMasterID
                                            && x.TransactionQTY == 0
                                            && x.StockTransactionID_Container == null
                                            && x.IsBox == null
                                            && x.Deleted == null).ToList();

                                        localTransaction.IsBox = true;
                                        localTransaction.TransactionQTY = products.Count;
                                        localTransaction.Serial = null;
                                    }

                                    entities.StockTransactions.Add(localTransaction);

                                    entities.SaveChanges();
                                    sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                    if (!localTransaction.Serial.HasValue)
                                    {
                                        localTransaction.Serial = localTransaction.StockTransactionID;
                                        entities.SaveChanges();
                                    }

                                    if (stockDetail.StockDetails != null && stockDetail.StockDetails.Any())
                                    {
                                        foreach (var localDetail in stockDetail.StockDetails)
                                        {
                                            var trans = this.CreateTransaction(localTransaction);
                                            trans.INMasterID = localDetail.INMasterID;
                                            trans.NouTransactionTypeID = localDetail.NouTransactionTypeID;
                                            trans.TransactionWeight = localDetail.TransactionWeight;
                                            trans.TransactionQTY = localDetail.TransactionQty;
                                            trans.Consumed = DateTime.Now;
                                            trans.MasterTableID = localDetail.MasterTableID;
                                            entities.StockTransactions.Add(trans);
                                            entities.SaveChanges();
                                            trans.Serial = localDetail.DispatchProduct ? localTransaction.Serial : trans.StockTransactionID;
                                            entities.SaveChanges();

                                            if (trans.NouTransactionTypeID == 3)
                                            {
                                                entities.App_UpdateProductStockData(trans.INMasterID,
                                                    trans.BatchNumberID, trans.TransactionQTY,
                                                    trans.TransactionWeight, trans.WarehouseID, trans.NouTransactionTypeID);
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = detail.InventoryItem.Master.INMasterID,
                                    Code = detail.InventoryItem.Master.Code,
                                    Name = detail.InventoryItem.Master.Name,
                                    MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                    MinWeight = detail.InventoryItem.Master.MinWeight,
                                    NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                    TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);

                                var orderDetail = new ARDispatchDetail
                                {
                                    ARDispatchID = dbOrder.ARDispatchID,
                                    QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                                    QuantityDelivered = detail.QuantityDelivered.ToDecimal(),
                                    WeightOrdered = detail.WeightOrdered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    CostTotal = detail.CostTotal,
                                    Margin = detail.Margin,
                                    LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount ?? detail.UnitPrice,
                                    VAT = detail.VAT,
                                    Notes = detail.Notes,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterSnapshotID = snapshot.INMasterSnapshotID,
                                    Comments = detail.Comments
                                };

                                entities.ARDispatchDetails.Add(orderDetail);
                                entities.SaveChanges();

                                if (detail.Concessions != null && detail.Concessions.Any())
                                {
                                    foreach (var concession in detail.Concessions.Where(x => x.ConcessionID == 0))
                                    {
                                        concession.ARDispatchDetailID = orderDetail.ARDispatchDetailID;
                                        concession.ConcessionTime = DateTime.Today;
                                        entities.Concessions.Add(concession);
                                    }
                                }

                                // pass the id back out
                                //this.Log.LogDebug(this.GetType(), string.Format("ARDispatchDetail created. Id:{0}", orderDetail.ARDispatchDetailID));
                                detail.SaleDetailID = orderDetail.ARDispatchDetailID;

                                #region stock transaction/traceability

                                var stockDetail = detail.StockDetailToProcess;
                                if (stockDetail != null)
                                {
                                    var localAttributeId = stockDetail.AttributeID;
                                    if (localAttributeId.IsNullOrZero())
                                    {
                                        var localAttribute = this.CreateAttribute(stockDetail);
                                        entities.Attributes.Add(localAttribute);
                                        entities.SaveChanges();
                                        localAttributeId = localAttribute.AttributeID;
                                    }

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttributeId;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    localTransaction.MasterTableID = orderDetail.ARDispatchDetailID;
                                    localTransaction.IsBox = stockDetail.IsBox;
                                    localTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId;
                                    localTransaction.AttributeID = localAttributeId;
                                    if (localTransaction.TransactionQTY.IsNullOrZero())
                                    {
                                        localTransaction.TransactionQTY = 1;
                                    }

                                    if (localTransaction.Pieces.IsNullOrZero())
                                    {
                                        localTransaction.Pieces = 1;
                                    }

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    if (localTransaction.Serial.HasValue)
                                    {
                                        var transToConsume = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == stockDetail.StockTransactionIdToConsume);
                                        if (transToConsume != null)
                                        {
                                            transToConsume.Consumed = DateTime.Now;
                                            transToConsume.InLocation = false;
                                        }
                                    }

                                    if (stockDetail.AutoSplittingStock)
                                    {
                                        var splitTransaction = this.CreateTransaction(stockDetail);
                                        splitTransaction.MasterTableID = null;
                                        splitTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                        splitTransaction.INMasterID = stockDetail.AutoSplittingStockId;
                                        splitTransaction.WarehouseID = stockDetail.BaseWarehouseID;
                                        splitTransaction.MasterTableID = stockDetail.BaseMasterTableID;
                                        splitTransaction.AttributeID = localAttributeId;
                                        splitTransaction.SplitID = 1;
                                        if (splitTransaction.TransactionQTY.IsNullOrZero())
                                        {
                                            splitTransaction.TransactionQTY = 1;
                                        }

                                        if (splitTransaction.Pieces.IsNullOrZero())
                                        {
                                            splitTransaction.Pieces = 1;
                                        }

                                        entities.StockTransactions.Add(splitTransaction);
                                        entities.SaveChanges();

                                        splitTransaction = this.CreateTransaction(stockDetail);
                                        splitTransaction.MasterTableID = null;
                                        splitTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                        splitTransaction.Consumed = DateTime.Now;
                                        splitTransaction.WarehouseID = stockDetail.BaseWarehouseID;
                                        splitTransaction.MasterTableID = stockDetail.BaseMasterTableID;
                                        splitTransaction.AttributeID = localAttributeId;
                                        splitTransaction.SplitID = 1;
                                        if (splitTransaction.TransactionQTY.IsNullOrZero())
                                        {
                                            splitTransaction.TransactionQTY = 1;
                                        }

                                        if (splitTransaction.Pieces.IsNullOrZero())
                                        {
                                            splitTransaction.Pieces = 1;
                                        }

                                        entities.StockTransactions.Add(splitTransaction);
                                        entities.SaveChanges();
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);

                                    entities.StockTransactions.Add(localTransaction);

                                    entities.SaveChanges();
                                    sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                    if (!localTransaction.Serial.HasValue)
                                    {
                                        localTransaction.Serial = localTransaction.StockTransactionID;
                                        entities.SaveChanges();
                                    }

                                    if (stockDetail.StockDetails != null && stockDetail.StockDetails.Any())
                                    {
                                        foreach (var localDetail in stockDetail.StockDetails)
                                        {
                                            var trans = this.CreateTransaction(localTransaction);
                                            trans.INMasterID = localDetail.INMasterID;
                                            trans.NouTransactionTypeID = localDetail.NouTransactionTypeID;
                                            trans.Consumed = DateTime.Now;
                                            trans.TransactionWeight = localDetail.TransactionWeight;
                                            trans.TransactionQTY = localDetail.TransactionQty;
                                            trans.MasterTableID = localDetail.MasterTableID;
                                            entities.StockTransactions.Add(trans);
                                            entities.SaveChanges();
                                            trans.Serial = localDetail.DispatchProduct ? localTransaction.Serial : trans.StockTransactionID;
                                            entities.SaveChanges();

                                            if (trans.NouTransactionTypeID == 3)
                                            {
                                                entities.App_UpdateProductStockData(trans.INMasterID,
                                                    trans.BatchNumberID, trans.TransactionQTY,
                                                    trans.TransactionWeight, trans.WarehouseID, trans.NouTransactionTypeID);
                                            }
                                        }
                                    }
                                }

                                #endregion
                               
                            }
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("UpdateARdispatch(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            //finally
            //{
            //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //    {
            //        if (saleID > 0)
            //        {
            //            using (var dispatchEntities = new NouvemEntities())
            //            {
            //                dispatchEntities.UpdateDispatchDocket(saleID);
            //            }
            //        }
            //    }));
            //}

            return false;
        }

        /// <summary>
        /// Updates an order delivery date.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="delDate">The new delivery date.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool UpdateDeliveryDate(int orderId, DateTime delDate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == orderId);
                    if (order != null)
                    {
                        order.DeliveryDate = delDate;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets pallet dispatch data.
        /// </summary>
        /// <param name="id">The dispatch id.</param>
        /// <returns>Pallet data.</returns>
        public DataTable CheckForExtraDispatchLines(int id)
        {
            var dataResult = new DataTable();

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_GetExtraDispatchLineProducts", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@ARDispatchID", SqlDbType.Int);
                    param.Value = id;
                    command.Parameters.Add(param);
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Adds scanned stock to a dispatch.
        /// </summary>
        /// <param name="serial">The barcode.</param>
        /// <param name="ardispatchId">The docket Id.</param>
        /// <param name="allowDispatchProductNotOnOrder"><Can the product be added if not on order./param>
        /// <param name="allowDispatchOverAmount">Can over the line amount be dispatched.</param>
        /// <param name="allowStockAlreadyOnAnOrder">Can the stock be added to the order if on another order.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error\Message (empty if ok).</returns>
        public Tuple<int?, string, DataTable, int?,int?> AddStockToDispatch(
            int serial,
            int ardispatchId,
            bool allowDispatchProductNotOnOrder,
            bool allowDispatchOverAmount,
            bool allowStockAlreadyOnAnOrder,
            int userId,
            int deviceId,
            int warehouseId,
            int processId,
            int customerId)
        {
            var dataResult = string.Empty;
            int? newStockId = null;
            int? newDispatchId = null;
            int? newDocketNo = null;
            var dataReturned = new DataTable();

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_AddStockToDispatch", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@ARDispatchID", SqlDbType.Int);
                    param.Value = ardispatchId;

                    var paramDispatchProduct = new SqlParameter("@AllowDispatchProductNotOnOrder", SqlDbType.Bit);
                    paramDispatchProduct.Value = allowDispatchProductNotOnOrder;

                    var paramDispatchOverAmount = new SqlParameter("@AllowDispatchOverAmount", SqlDbType.Bit);
                    paramDispatchOverAmount.Value = allowDispatchOverAmount;

                    var paramStockAlreadyOnAnOrder = new SqlParameter("@AllowStockAlreadyOnAnOrder", SqlDbType.Bit);
                    paramStockAlreadyOnAnOrder.Value = allowStockAlreadyOnAnOrder;

                    var customerParam = new SqlParameter("@CustomerID", SqlDbType.Int);
                    customerParam.Value = customerId;

                    var serialParam = new SqlParameter("@Serial", SqlDbType.Int);
                    serialParam.Value = serial;
                    var userParam = new SqlParameter("@UserID", SqlDbType.Int);
                    userParam.Value = userId;
                    var deviceParam = new SqlParameter("@DeviceID", SqlDbType.Int);
                    deviceParam.Value = deviceId;
                    var warehouseParam = new SqlParameter("@WarehouseID", SqlDbType.Int);
                    warehouseParam.Value = warehouseId;
                    var processParam = new SqlParameter("@ProcessID", SqlDbType.Int);
                    processParam.Value = processId;
                    var outputParam = new SqlParameter("@Message", SqlDbType.NVarChar, 1000);
                    outputParam.Direction = ParameterDirection.Output;
                    var outputParamStockId = new SqlParameter("@NewStockID", SqlDbType.Int);
                    outputParamStockId.Direction = ParameterDirection.Output;
                    var outputParamDispatchId = new SqlParameter("@NewARDispatchID", SqlDbType.Int);
                    outputParamDispatchId.Direction = ParameterDirection.Output;

                    var outputParamDispatchNo = new SqlParameter("@DocketNumber", SqlDbType.Int);
                    outputParamDispatchNo.Direction = ParameterDirection.Output;

                    command.Parameters.Add(param);
                    command.Parameters.Add(paramDispatchProduct);
                    command.Parameters.Add(paramDispatchOverAmount);
                    command.Parameters.Add(paramStockAlreadyOnAnOrder);
                    command.Parameters.Add(serialParam);
                    command.Parameters.Add(customerParam);
                    command.Parameters.Add(userParam);
                    command.Parameters.Add(deviceParam);
                    command.Parameters.Add(warehouseParam);
                    command.Parameters.Add(processParam);
                    command.Parameters.Add(outputParam);
                    command.Parameters.Add(outputParamStockId);
                    command.Parameters.Add(outputParamDispatchId);
                    command.Parameters.Add(outputParamDispatchNo);

                    var dataReader = command.ExecuteReader();
                    dataReturned.Load(dataReader);

                    var localDataResult = command.Parameters["@Message"].Value;
                    if (localDataResult != DBNull.Value)
                    {
                        dataResult = (string)localDataResult;
                    }

                    var localDataStockResult = command.Parameters["@NewStockID"].Value;
                    if (localDataStockResult != DBNull.Value)
                    {
                        newStockId = (int?)localDataStockResult;
                    }

                    var localDataDispatchResult = command.Parameters["@NewARDispatchID"].Value;
                    if (localDataDispatchResult != DBNull.Value)
                    {
                        newDispatchId = (int?)localDataDispatchResult;
                    }

                    var localDataDispatchNoResult = command.Parameters["@DocketNumber"].Value;
                    if (localDataDispatchNoResult != DBNull.Value)
                    {
                        newDocketNo = (int?)localDataDispatchNoResult;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                dataResult = string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException);
            }

            return Tuple.Create(newStockId, dataResult, dataReturned, newDispatchId,newDocketNo);
        }

        /// <summary>
        /// Creates a back over from outstanding dispatch amounts.
        /// </summary>
        /// <param name="ardispatchId">The docket to create the back order for.</param>
        /// <returns></returns>
        public int CreateBackOrder(int ardispatchId)
        {
            var dataResult = 0;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_CreateBackOrder", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@DispatchID", SqlDbType.Int);
                    param.Value = ardispatchId;
                    
                    var outputParam = new SqlParameter("@NewDispatchID", SqlDbType.Int);
                    outputParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);
                    command.Parameters.Add(outputParam);
                    command.ExecuteNonQuery();

                    var localDataResult = command.Parameters["@NewDispatchID"].Value;
                    if (localDataResult != DBNull.Value)
                    {
                        dataResult = (int)localDataResult;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Gets pallet dispatch data.
        /// </summary>
        /// <param name="id">The dispatch id.</param>
        /// <returns>Pallet data.</returns>
        public IList<App_GetDispatchPalletStock_Result> GetDispatchPalletData(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetDispatchPalletStock(id).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets pallet data.
        /// </summary>
        /// <param name="id">The pallet id.</param>
        /// <returns>Pallet data.</returns>
        public IList<App_GetPalletisationStock_Result> GetPalletisationData(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetPalletisationStock(id).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the transactions and attribute data associated with a dispatch line.
        /// </summary>
        /// <param name="id">The dispatch detail id.</param>
        /// <returns>The transactions and attribute data associated with a dispatch line.</returns>
        public IList<App_GetOrderLineTransactionData_Result> GetOrderLineTransactionData(int id, int transactionTypeId, int docketid)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetOrderLineTransactionData(id, transactionTypeId, docketid).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Updates an existing dispatch docket (docket and details) when a sale order has beem ammended.
        /// </summary>
        /// <param name="sale">The sale order data.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARDispatchFromSaleOrder(Sale sale)
        {
            var dispatchID = sale.ParentID.ToInt();

            this.Log.LogDebug(this.GetType(), string.Format("UpdateARDispatchFromSaleOrder(): Updating dispatch docket:{0}", dispatchID));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == dispatchID);

                    if (dbOrder != null)
                    {
                        dbOrder.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
                        dbOrder.TotalExVAT = 0;
                        dbOrder.DiscountPercentage = sale.DiscountPercentage;
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = 0;
                        dbOrder.GrandTotalIncVAT = 0;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.DispatchNote1 = sale.DocketNote;
                        dbOrder.InvoiceNote = sale.InvoiceNote;
                        dbOrder.CustomerPOReference = sale.CustomerPOReference;
                        dbOrder.DispatchContainerID = sale.DispatchContainerID;
                        if (sale.Customer != null && sale.Customer.BPMasterID > 0)
                        {
                            dbOrder.BPMasterID_Customer = sale.Customer.BPMasterID;
                        }
                        else if (sale.BPCustomer != null && sale.BPCustomer.BPMasterID > 0)
                        {
                            dbOrder.BPMasterID_Customer = sale.BPCustomer.BPMasterID;
                        }

                        dbOrder.BPContactID = sale.MainContact != null
                            ? (int?)sale.MainContact.Details.BPContactID
                            : null;
                        dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null
                            ? (int?)sale.DeliveryAddress.Details.BPAddressID
                            : null;
                        dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null
                            ? (int?)sale.InvoiceAddress.Details.BPAddressID
                            : null;
                        dbOrder.DocumentDate = sale.DocumentDate;
                        dbOrder.Remarks = sale.Remarks;
                        dbOrder.DeliveryDate = sale.DeliveryDate;
                        dbOrder.ShippingDate = sale.ShippingDate;
                        dbOrder.UserMasterID = sale.SalesEmployeeID;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.Deleted = sale.Deleted;
                        dbOrder.RouteID = sale.RouteID;

                        var dbDetails = dbOrder.ARDispatchDetails.Where(x => x.Deleted == null);
                        foreach (var arDispatchDetail in dbDetails)
                        {
                            var dbDispatchDetail =
                                entities.ARDispatchDetails.FirstOrDefault(
                                    x => x.ARDispatchDetailID == arDispatchDetail.ARDispatchDetailID);
                            if (dbDispatchDetail != null)
                            {
                                dbDispatchDetail.Deleted = DateTime.Now;
                            }
                        }

                        var details = sale.SaleDetails.Where(x => x.Deleted == null);
                        foreach (var detail in details)
                        {
                            // Add to the master snapshot table first
                            var snapshot = new INMasterSnapshot
                            {
                                INMasterID = detail.InventoryItem.Master.INMasterID,
                                Code = detail.InventoryItem.Master.Code,
                                Name = detail.InventoryItem.Master.Name,
                                MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                MinWeight = detail.InventoryItem.Master.MinWeight,
                                NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                            };

                            entities.INMasterSnapshots.Add(snapshot);

                            var orderDetail = new ARDispatchDetail
                            {
                                ARDispatchID = dbOrder.ARDispatchID,
                                QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                                QuantityDelivered = detail.QuantityDelivered.ToDecimal(),
                                WeightOrdered = detail.WeightOrdered,
                                TotalExclVAT = 0,
                                TotalIncVAT = 0,
                                WeightDelivered = detail.WeightDelivered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = null,
                                DiscountAmount = null,
                                DiscountPrice = null,
                                LineDiscountAmount = null,
                                LineDiscountPercentage = null,
                                UnitPriceAfterDiscount = detail.UnitPrice,
                                VAT = detail.VAT,
                                Notes = detail.Notes,
                                VATCodeID = detail.VatCodeID,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterSnapshotID = snapshot.INMasterSnapshotID,
                                CostTotal = detail.CostTotal,
                                Margin = detail.Margin,
                                Comments = detail.Comments
                            };

                            entities.ARDispatchDetails.Add(orderDetail);
                            entities.SaveChanges();
                        }

                        // if we used a base document to create this, then we mark the base document as closed.
                        if (sale.SaleID != 0)
                        {
                            var baseDoc = entities.AROrders.FirstOrDefault(x => x.AROrderID == sale.SaleID);
                            if (baseDoc != null)
                            {
                                this.Log.LogWarning(this.GetType(), string.Format("8. Updating base order. Setting status to complete:{0}", sale.SaleID));
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            }
                        }

                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("UpdateARDispatchFromSaleOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
           
            return false;
        }

        /// <summary>
        /// Adds or removes pallet stock from the order.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        /// <param name="arDispatchId">The order id.</param>
        /// <param name="addToOrder">Add or remove flag.</param>
        /// <returns>Flag, as to success or not.</returns>
        public bool AddOrRemovePalletStock(int? serial, int? arDispatchId, bool addToOrder)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_AddPalletStockToOrder(serial, arDispatchId, NouvemGlobal.DeviceId, NouvemGlobal.UserId,
                        addToOrder);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates an order detail line.
        /// </summary>
        /// <param name="detail">The order detail line.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDetailTotal(CompleteOrderCheck detail)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdateDetailTotal(): Id:{0}, Total:{1}", detail.ARDispatchDetailId, detail.TotalExVat));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrderDetail =
                        entities.ARDispatchDetails.FirstOrDefault(x => x.ARDispatchDetailID == detail.ARDispatchDetailId);
                    if (dbOrderDetail != null)
                    {
                        dbOrderDetail.TotalExclVAT = detail.TotalExVat;
                        dbOrderDetail.TotalIncVAT = detail.TotalIncVat;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Order detail updated");
                        return true;
                    }

                    this.Log.LogError(this.GetType(), "Order detail not found");
                }
            } 
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the dispatch total.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id.</param>
        /// <returns>A flag, indicating a successful retrieval or not.</returns>
        public decimal GetDispatchTotal(int arDispatchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetDispatchTotal(): Id:{0}", arDispatchId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder =
                        entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == arDispatchId);
                    if (dbOrder != null)
                    {
                        return dbOrder.SubTotalExVAT.ToDecimal();
                    }

                    this.Log.LogError(this.GetType(), "Dispatch not found");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Gets the dispatch total.
        /// </summary>
        /// <param name="sale">The dispatch data.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateNotes(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder =
                        entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == sale.SaleID);
                    if (dbOrder != null)
                    {
                        dbOrder.DispatchNote1 = sale.DocketNote;
                        dbOrder.CustomerPOReference = sale.CustomerPOReference;
                        dbOrder.DispatchContainerID = sale.DispatchContainerID;
                        entities.SaveChanges();
                        return true;
                    }

                    this.Log.LogError(this.GetType(), "Dispatch not found");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the dispatch carcass data.
        /// </summary>
        /// <returns>The dispatch carcass data.</returns>
        public IList<Sale> GetDispatchStock(IList<Sale> dispatches)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var dispatch in dispatches)
                    {
                        dispatch.StockDetails = (from stock in entities.App_DispatchExportData(dispatch.SaleID)
                            select new StockDetail
                            {
                                Number = stock.Number,
                                Serial = stock.Serial,
                                TransactionWeight = stock.ColdWeight,
                                Eartag = stock.Eartag,
                                TotalResidency = stock.TotalResidency,
                                CurrentResidency = stock.CurrentResidency,
                                Grade = stock.Grade,
                                GradingDate = stock.GradingDate,
                                CarcassNumber = stock.CarcassNumber,
                                FarmAssured = stock.FarmAssured,
                                DOB = stock.DOB,
                                DateOfLastMove = stock.DateOfLastMove,
                                HerdNo = stock.Herd,
                                CatName = stock.CAT,
                                BreedName = stock.Breed,
                                LotNo = stock.PlantNo,
                                Attribute100 = stock.Reference1,
                                Attribute101 = stock.Reference2,
                                Attribute102 = stock.Reference3,
                                Attribute103 = stock.Reference4,
                                Attribute104 = stock.Reference5,
                                Attribute105 = stock.Reference6,
                                Attribute106 = stock.Reference7,
                                Attribute107 = stock.Reference8,
                                Attribute108 = stock.Reference9,
                                Attribute109 = stock.Reference10
                            }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dispatches;
        }


        /// <summary>
        /// Gets the dispatch docket details for a final check before completion.
        /// </summary>
        /// <param name="ardispatchId">The docket id.</param>
        public IList<CompleteOrderCheck> CompleteOrderCheck(int ardispatchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("CompleteOrderCheck(): dispatchId:{0}", ardispatchId));
            var details = new List<CompleteOrderCheck>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dispatchDetails =
                        entities.ARDispatchDetails.Where(x => x.ARDispatchID == ardispatchId && x.Deleted == null);

                    foreach (var arDispatchDetail in dispatchDetails)
                    {
                        var detail = new CompleteOrderCheck();
                        detail.ARDispatchDetailId = arDispatchDetail.ARDispatchDetailID;
                        detail.QuantityDelivered = arDispatchDetail.QuantityDelivered;
                        detail.WeightDelivered = arDispatchDetail.WeightDelivered;
                        detail.PriceListId = arDispatchDetail.PriceListID;
                        detail.TotalExVat = arDispatchDetail.TotalExclVAT;
                        detail.TotalIncVat = arDispatchDetail.TotalIncVAT;
                        detail.INMasterId = arDispatchDetail.INMasterID;
                        detail.UnitPrice = arDispatchDetail.UnitPrice;
                        detail.UnitPriceAfterDiscount = arDispatchDetail.UnitPriceAfterDiscount;
                        detail.NominalWeight = entities.INMasters.First(x => x.INMasterID == arDispatchDetail.INMasterID).NominalWeight;
                        var localPriceListId = arDispatchDetail.PriceListID.ToInt();
                        var priceListDetail = entities.PriceListDetails.FirstOrDefault(x => !x.Deleted
                            && x.PriceListID == localPriceListId && x.INMasterID == arDispatchDetail.INMasterID);
                        if (priceListDetail != null)
                        {
                            detail.NouPriceMethodId = priceListDetail.NouPriceMethodID;
                        }

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Removes an ARDispatchDetail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveArDispatchtDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveArDispatchtDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.ARDispatchDetails.FirstOrDefault(x => x.ARDispatchDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");

                        var deletion = new LabelDeletion
                        {
                            StockTransactionID = itemId,
                            UserMasterID = NouvemGlobal.UserId,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            DeletionTime = DateTime.Now,
                            Reason = "Dispatch Line"
                        };

                        entities.LabelDeletions.Add(deletion);
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Removes stock from item.
        /// </summary>
        /// <param name="sale">The sale/detail/transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveStockFromOrder(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (sale.Serial.HasValue)
                    {
                        var serial = sale.Serial;
                        //entities.App_DeleteDispatchStock(serial, NouvemGlobal.OutOfProductionId);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Completes the input dispatch order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="orderStatusId">The order status id.</param>
        /// <returns>A flag, as to whether the order status was changed or not.</returns>
        public bool ChangeARDispatchStatus(Sale sale, int orderStatusId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("ChangeARDispatchStatus(): Attempting to change order status of ar dispatch:{0}. Order status id:{1}", sale.SaleID, orderStatusId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == sale.SaleID);
                    if (order != null)
                    {
                        sale.OrderAlreadyCompleted = false;
                        if (orderStatusId == NouvemGlobal.NouDocStatusComplete.NouDocStatusID &&
                            order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                        {
                            // already completed
                            this.Log.LogDebug(this.GetType(), string.Format("Order:{0} has already been completed", sale.SaleID));
                            sale.OrderAlreadyCompleted = true;
                            return true;
                        }

                        //order.BPMasterID_Customer = sale.BPMasterID;
                        order.NouDocStatusID = orderStatusId;
                        order.TotalExVAT = sale.TotalExVAT.ToDecimal();
                        order.DiscountPercentage = sale.DiscountPercentage;
                        order.VAT = sale.VAT;
                        order.DeviceID = NouvemGlobal.DeviceId;
                        order.UserMasterID_OrderCompletion = orderStatusId ==
                                                             NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                                                             ? NouvemGlobal.UserId: null;
                        order.EditDate = DateTime.Now;
                        order.DiscountIncVAT = sale.DiscountIncVAT;
                        order.SubTotalExVAT = sale.SubTotalExVAT;
                        order.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        order.CustomerPOReference = sale.CustomerPOReference;
                        order.DispatchNote1 = sale.DocketNote;
                        order.InvoiceNote = sale.InvoiceNote;
                        order.Remarks = sale.Remarks;
                        //order.RouteID = sale.RouteID;
                        order.BPMasterID_Agent = sale.AgentID;
                        order.DeliveryDate = sale.DeliveryDate;
                        order.ShippingDate = sale.ShippingDate;
                        order.DocumentDate = sale.DocumentDate;
                        order.PopUpNote = sale.PopUpNote;
                        order.BPContactID = sale.MainContact != null && sale.MainContact.Details != null
                            ? sale.MainContact.Details.BPContactID
                            : (int?) null;
                        order.BPContactID_Delivery = sale.DeliveryContact != null && sale.DeliveryContact.Details != null
                            ? sale.DeliveryContact.Details.BPContactID
                            : (int?)null;
                        order.BPAddressID_Delivery = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.BPAddressID : (int?)null;
                        order.BPAddressID_Invoice = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.BPAddressID : (int?)null;

                        if (sale.Haulier != null)
                        {
                            order.BPMasterID_Haulier = sale.Haulier.BPMasterID;
                        }

                        foreach (var detail in sale.SaleDetails.Where(x => x.SaleDetailID > 0))
                        {
                            var dbOrderDetail =
                                     entities.ARDispatchDetails.FirstOrDefault(
                                       x => x.ARDispatchDetailID == detail.SaleDetailID);

                            if (dbOrderDetail == null)
                            {
                                // serious error - should never happen
                                this.Log.LogError(this.GetType(), string.Format("ARDispatchDetail:{0} not found in the database", detail.SaleDetailID));
                                continue;
                            }

                            this.Log.LogDebug(this.GetType(), string.Format("Updating ARDispatchDetail:{0}", dbOrderDetail.ARDispatchDetailID));
                            dbOrderDetail.QuantityDelivered = detail.QuantityDelivered.ToDecimal();
                            dbOrderDetail.WeightDelivered = detail.WeightDelivered.ToDecimal();
                            dbOrderDetail.QuantityOrdered = detail.QuantityOrdered.ToDecimal();
                            dbOrderDetail.WeightOrdered = detail.WeightOrdered.ToDecimal();
                            dbOrderDetail.TotalExclVAT = detail.TotalExVAT;
                            dbOrderDetail.TotalIncVAT = detail.TotalIncVAT;
                            dbOrderDetail.UnitPrice = detail.UnitPrice;
                            dbOrderDetail.UnitPriceAfterDiscount = detail.UnitPrice;
                            if (detail.PriceListID > 0)
                            {
                                dbOrderDetail.PriceListID = detail.PriceListID;
                            }

                            // only allow product change if nothing has been booked out.
                            if (detail.QuantityDelivered <= 0 && detail.WeightDelivered <= 0)
                            {
                                dbOrderDetail.INMasterID = detail.INMasterID;
                            }
                        }

                        entities.SaveChanges();
                        foreach (var detail in sale.SaleDetails.Where(x => x.SaleDetailID == 0))
                        {
                            // Add to the master snapshot table first
                            var snapshot = new INMasterSnapshot
                            {
                                INMasterID = detail.InventoryItem.Master.INMasterID,
                                Code = detail.InventoryItem.Master.Code,
                                Name = detail.InventoryItem.Master.Name,
                                MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                MinWeight = detail.InventoryItem.Master.MinWeight,
                                NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                            };

                            entities.INMasterSnapshots.Add(snapshot);
                            entities.SaveChanges();

                            var orderDetail = new ARDispatchDetail
                            {
                                ARDispatchID = sale.SaleID,
                                QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                                QuantityDelivered = detail.QuantityDelivered,
                                WeightDelivered = detail.WeightDelivered,
                                WeightOrdered = detail.WeightOrdered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = null,
                                DiscountAmount = null,
                                DiscountPrice = null,
                                LineDiscountAmount = null,
                                LineDiscountPercentage = null,
                                CostTotal = detail.CostTotal,
                                Margin = detail.Margin,
                                UnitPriceAfterDiscount = detail.UnitPrice,
                                VAT = detail.VAT,
                                VATCodeID = detail.VatCodeID,
                                TotalExclVAT = detail.TotalExVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterSnapshotID = snapshot.INMasterSnapshotID,
                                Comments = detail.Comments
                            };

                            entities.ARDispatchDetails.Add(orderDetail);
                            entities.SaveChanges();

                            if (detail.WeightDelivered.ToDouble() > 0 || detail.QuantityDelivered.ToDouble() > 0)
                            {
                                var attribute = new Model.DataLayer.Attribute { IsBatch = false};
                                entities.Attributes.Add(attribute);
                                entities.SaveChanges();

                                var transaction = new StockTransaction
                                {
                                    MasterTableID = orderDetail.ARDispatchDetailID,
                                    TransactionQTY = orderDetail.QuantityDelivered.ToDecimal(),
                                    Pieces = orderDetail.QuantityDelivered.ToInt(),
                                    TransactionWeight = orderDetail.WeightDelivered.ToDecimal(),
                                    DeviceMasterID = NouvemGlobal.DeviceId,
                                    UserMasterID = NouvemGlobal.UserId,
                                    TransactionDate = DateTime.Now,
                                    ManualWeight = true,
                                    INMasterID = orderDetail.INMasterID,
                                    NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId,
                                    WarehouseID = NouvemGlobal.DispatchId,
                                    AttributeID = attribute.AttributeID
                                };

                                entities.App_UpdateProductStockData(transaction.INMasterID,
                                    transaction.BatchNumberID, transaction.TransactionQTY,
                                    transaction.TransactionWeight, transaction.WarehouseID, transaction.NouTransactionTypeID);
                                entities.StockTransactions.Add(transaction);
                                entities.SaveChanges();
                                transaction.Serial = transaction.StockTransactionID;
                                entities.SaveChanges();
                            }
                        }

                        this.Log.LogDebug(this.GetType(), string.Format("Order:{0} status changed", sale.SaleID));
                        return true;
                    }
                    else
                    {
                        // No corresponding aporder, so nothing to do.
                        this.Log.LogDebug(this.GetType(), string.Format("APDispatch:{0} has no corresponding dispatch order. Completing", sale.SaleID));
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Completes the input dispatch order.
        /// </summary>
        /// <param name="saleOrderId">The sale order to canel the associated dispatch for.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        public bool CancelARDispatchFromSaleOrder(int saleOrderId)
        {
            this.Log.LogWarning(this.GetType(), string.Format("CancelARDispatchFromSaleOrder(): Attempting to cancel dispatch docket(s) associated with sale order:{0}.", saleOrderId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var orders = entities.ARDispatches.Where(x => x.BaseDocumentReferenceID == saleOrderId);
                    foreach (var order in orders)
                    {
                        this.Log.LogWarning(this.GetType(), string.Format("Dispatch Order:{0} cancelled.", order.ARDispatchID));
                        order.NouDocStatusID = NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Checks the completed dispatch docket.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        public bool CheckDispatchDocket(int docketId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("CheckDispatchDocket(): Checking dispatch docket:{0}.", docketId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.CheckDispatch(docketId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the docket status.
        /// </summary>
        /// <param name="arDispatchId">The docket id.</param>
        /// <returns>The input docket status.</returns>
        public int? GetARDispatchOrderStatus(int arDispatchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchOrderStatus(): Attempting to get the status for dispatch id:{0}", arDispatchId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == arDispatchId);
                    if (order != null)
                    {
                        return order.NouDocStatusID;
                    }
                }

                this.Log.LogDebug(this.GetType(), "Dispatch successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return null;
        }

        /// <summary>
        /// Adds any delivery charges to the order.
        /// </summary>
        /// <param name="dispatchId">The order id.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddDeliveryCharges(int dispatchId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_AddDeliveryCharges(dispatchId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceDispatchDocket(int docketId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_PriceDispatchDocket(docketId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        ///// <summary>
        ///// Checks that the docket lines are corect.
        ///// </summary>
        ///// <param name="docketId">The docket to check.</param>
        ///// <returns>Flag, to to whether there was an issue checking or not.</returns>
        //public bool CheckDispatchDocketDetails(int docketId)
        //{
        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == docketId);
        //            if (order == null)
        //            {
        //                return false;
        //            }

        //            if (order.Invoiced == true)
        //            {
        //                return false;
        //            }

        //            var updating = false;
        //            var details = order.ARDispatchDetails.Where(x => x.Deleted == null).ToList();
        //            foreach (var orderDetail in details)
        //            {
        //                var stockdata = entities.StockTransactions.Where(x =>
        //                    x.MasterTableID == orderDetail.ARDispatchDetailID && x.Deleted == null
        //                    && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId &&
        //                    x.StockTransactionID_Container == null);

        //                var totalQty = stockdata.Sum(x => x.TransactionQTY).ToDouble();
        //                var totalWgt = stockdata.Sum(x => x.TransactionWeight).ToDouble();

        //                if (orderDetail.QuantityDelivered.ToDouble().GetDifference(totalQty) > 0.1)
        //                {
        //                    orderDetail.QuantityDelivered = totalQty;
        //                    if (orderDetail.WeightDelivered.ToDouble().GetDifference(totalWgt) > 0.1)
        //                    {
        //                        orderDetail.WeightDelivered = Math.Round(totalWgt, 3);
        //                    }

        //                    entities.SaveChanges();
        //                }
        //                else if (orderDetail.WeightDelivered.ToDouble().GetDifference(totalWgt) > 0.1)
        //                {
        //                    var localWgt = Math.Round(totalWgt, 3);
        //                    if (localWgt < 0)
        //                    {
        //                        localWgt = 0;
        //                    }

        //                    orderDetail.WeightDelivered = localWgt;
        //                    entities.SaveChanges();
        //                }

        //                if (orderDetail.PriceListID.IsNullOrZero())
        //                {
        //                    var customer =
        //                        entities.BPMasters.FirstOrDefault(x => x.BPMasterID == order.BPMasterID_Customer);
        //                    if (customer != null)
        //                    {
        //                        var customerPriceBook = customer.PriceListID;
        //                        if (customerPriceBook != null)
        //                        {
        //                            var localPriceDetail = entities.PriceListDetails.FirstOrDefault(x =>
        //                                x.PriceListID == customerPriceBook && x.INMasterID == orderDetail.INMasterID && !x.Deleted);

        //                            if (localPriceDetail != null)
        //                            {
        //                                orderDetail.PriceListID = customerPriceBook;
        //                                entities.SaveChanges();
        //                            }
        //                            else
        //                            {
        //                                var localBasePriceDetail = entities.PriceListDetails.FirstOrDefault(x =>
        //                                    x.PriceListID == NouvemGlobal.BasePriceList.PriceListID && x.INMasterID == orderDetail.INMasterID && !x.Deleted);
        //                                if (localBasePriceDetail != null)
        //                                {
        //                                    orderDetail.PriceListID = NouvemGlobal.BasePriceList.PriceListID;
        //                                    entities.SaveChanges();
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            var localBasePriceDetail = entities.PriceListDetails.FirstOrDefault(x =>
        //                                x.PriceListID == NouvemGlobal.BasePriceList.PriceListID && x.INMasterID == orderDetail.INMasterID && !x.Deleted);
        //                            if (localBasePriceDetail != null)
        //                            {
        //                                orderDetail.PriceListID = NouvemGlobal.BasePriceList.PriceListID;
        //                                entities.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                }

        //                // product price method (if exists), overrides price book price method.
        //                NouPriceMethod productPriceMethod = null;
        //                var localProduct =
        //                    entities.INMasters.FirstOrDefault(x => x.INMasterID == orderDetail.INMasterID);
        //                if (localProduct != null && localProduct.NouPriceMethodID != null)
        //                {
        //                    productPriceMethod = entities.NouPriceMethods.FirstOrDefault(x => x.NouPriceMethodID == localProduct.NouPriceMethodID);
        //                }

        //                if (productPriceMethod == null)
        //                {
        //                    var priceMethod =
        //                        entities.PriceListDetails.FirstOrDefault(
        //                            x =>
        //                                x.PriceListID == orderDetail.PriceListID &&
        //                                x.INMasterID == orderDetail.INMasterID && !x.Deleted);
        //                    if (priceMethod != null)
        //                    {
        //                        productPriceMethod = priceMethod.NouPriceMethod;
        //                    }
        //                }

        //                if (productPriceMethod != null)
        //                {
        //                    if (productPriceMethod.Name.Equals(Constant.PriceByWgt))
        //                    {
        //                        var correctTotal = decimal.Round(orderDetail.WeightDelivered.ToDecimal() * orderDetail.UnitPrice.ToDecimal(), 2);
        //                        if (correctTotal.GetDifference(orderDetail.TotalExclVAT.ToDecimal()) > 0.1m)
        //                        {
        //                            orderDetail.TotalIncVAT = correctTotal;
        //                            orderDetail.TotalExclVAT = correctTotal;
        //                            entities.SaveChanges();
        //                            updating = true;
        //                        }
        //                    }
        //                    else if (productPriceMethod.Name.Equals(Constant.PriceByQty))
        //                    {
        //                        var correctTotal = decimal.Round(orderDetail.QuantityDelivered.ToDecimal() * orderDetail.UnitPrice.ToDecimal(), 2);
        //                        if (correctTotal.GetDifference(orderDetail.TotalExclVAT.ToDecimal()) > 0.1m)
        //                        {
        //                            orderDetail.TotalIncVAT = correctTotal;
        //                            orderDetail.TotalExclVAT = correctTotal;
        //                            entities.SaveChanges();
        //                            updating = true;
        //                        }
        //                    }
        //                    else if (orderDetail.INMaster != null)
        //                    {
        //                        // typical weight.
        //                        var correctTotal =
        //                            decimal.Round((orderDetail.QuantityDelivered.ToDecimal() * orderDetail.INMaster.NominalWeight.ToDecimal())
        //                                * orderDetail.UnitPrice.ToDecimal(), 2);
        //                        if (correctTotal.GetDifference(orderDetail.TotalExclVAT.ToDecimal()) > 0.1m)
        //                        {
        //                            orderDetail.TotalIncVAT = correctTotal;
        //                            orderDetail.TotalExclVAT = correctTotal;
        //                            entities.SaveChanges();
        //                            updating = true;
        //                        }
        //                    }
        //                }
        //            }

        //            var orderTotal = order.SubTotalExVAT.ToDecimal();
        //            var orderDetailsTotal = details.Sum(x => x.TotalExclVAT).ToDecimal();

        //            if (orderTotal.GetDifference(orderDetailsTotal) > 0.1m)
        //            {
        //                order.SubTotalExVAT = orderDetailsTotal;
        //                order.GrandTotalIncVAT = orderDetailsTotal;
        //                entities.SaveChanges();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatchesForReport(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatchesForReport(): Attempting to get all the dispatches");
            var orders = new List<Sale>();
            System.Threading.Thread.CurrentThread.Join(10);
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainer = order.DispatchContainer,
                                  Number = order.Number,
                                  SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  EditDate = order.EditDate,
                                  SaleType = ViewType.ARDispatch,
                                  RouteID = order.RouteID,
                                  PopUpNote = order.PopUpNote,
                                  DocketNote = order.DispatchNote1,
                                  InvoiceNote = order.InvoiceNote,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  IsBaseDocument = order.Invoiced,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList()
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatchesForReportByDates(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatchesForReport(): Attempting to get all the dispatches");
            var orders = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.DocumentDate >= start && order.DocumentDate <= end
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainer = order.DispatchContainer,
                                  Number = order.Number,
                                  SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  EditDate = order.EditDate,
                                  SaleType = ViewType.ARDispatch,
                                  RouteID = order.RouteID,
                                  PopUpNote = order.PopUpNote,
                                  DocketNote = order.DispatchNote1,
                                  InvoiceNote = order.InvoiceNote,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  IsBaseDocument = order.Invoiced,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList()
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }


        /// <summary>
        /// Retrieves all the pallets between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        /// <returns>A collection of pallets.</returns>
        public IList<Sale> GetAllPallets(DateTime start, DateTime end)
        {
            var orders = new List<Sale>();
            var pallets = new List<App_GetPallets_Result2>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    pallets = entities.App_GetPallets(start, end).ToList();
                }

                foreach (var pallet in pallets)
                {
                    orders.Add(new Sale
                    {
                        SaleID = pallet.StockTransactionID,
                        WarehouseID = pallet.WarehouseID,
                        Location = pallet.Location,
                        ProductName = pallet.Product,
                        CreationDate = pallet.TransactionDate,
                        Weight = Math.Round(pallet.Wgt.ToDecimal(),2),
                        Quantity = pallet.Qty.ToInt()
                    });
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatches(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatches(): Attempting to get all the dispatches");
            var orders = new List<Sale>();
        
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.Deleted == null 
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  InvoiceNote = order.InvoiceNote,
                                  DispatchContainer = order.DispatchContainer,
                                  SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DispatchContainerID = order.DispatchContainerID,
                                  SaleType = ViewType.ARDispatch,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  RouteID = order.RouteID,
                                  PopUpNote = order.PopUpNote,
                                  DocketNote = order.DispatchNote1,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  IsBaseDocument = order.Invoiced,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  //SaleDetails = (from detail in order.ARDispatchDetails
                                  //               where detail.Deleted == null
                                  //               select new SaleDetail
                                  //               {
                                  //                   SaleDetailID = detail.ARDispatchDetailID,
                                  //                   SaleID = detail.ARDispatchID,
                                  //                   QuantityOrdered = detail.QuantityOrdered,
                                  //                   WeightOrdered = detail.WeightOrdered,
                                  //                   QuantityDelivered = detail.QuantityDelivered,
                                  //                   WeightDelivered = detail.WeightDelivered,
                                  //                   UnitPrice = detail.UnitPrice,
                                  //                   DiscountPercentage = detail.DiscountPercentage,
                                  //                   DiscountPrice = detail.DiscountPrice,
                                  //                   LineDiscountPercentage = detail.LineDiscountPercentage,
                                  //                   LineDiscountAmount = detail.LineDiscountAmount,
                                  //                   UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                  //                   SaleDetailType = ViewType.APReceipt,
                                  //                   VAT = detail.VAT,
                                  //                   TotalExVAT = detail.TotalExclVAT,
                                  //                   TotalIncVAT = detail.TotalIncVAT,
                                  //                   VatCodeID = detail.VATCodeID,
                                  //                   LoadingSale = true,
                                  //                   INMasterID = detail.INMasterID,
                                  //                   PriceListID = detail.PriceListID ?? 0,
                                  //                   Concessions = detail.Concessions.ToList(),
                                  //                   NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                  //                   Deleted = detail.Deleted,
                                  //                   //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                  //                   //                     select new GoodsReceiptData
                                  //                   //                     {
                                  //                   //                         BatchNumber = batchNumber,
                                  //                   //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                  //                   //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                  //                   //                                           select new StockTransactionData
                                  //                   //                                           {
                                  //                   //                                               Transaction = transaction,
                                  //                   //                                               TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                  //                   //                                           }).ToList()
                                  //                   //                     }).ToList(),
                                  //               }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public DataTable GetFTraceDispatches(bool showAll, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatches(): Attempting to get all the dispatches");
            var dataResult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_GetFTraceDispatches", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Start", SqlDbType.Date)).Value = start;
                    command.Parameters.Add(new SqlParameter("@End", SqlDbType.Date)).Value = end;
                    command.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Bit)).Value = showAll;
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceDispatch(int dispatchId)
        {
            var dataResult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_FTrace_Dispatch", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@DispatchID", SqlDbType.Int)).Value = dispatchId;
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceSlaughterOnly(int dispatchId)
        {
            var dataResult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_FTrace_SlaughterOnly", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@DispatchID", SqlDbType.Int)).Value = dispatchId;
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Gets the FTrace delivery data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceProcessing(int dispatchId)
        {
            var dataResult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_FTrace_Processing", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@DispatchID", SqlDbType.Int)).Value = dispatchId;
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Gets the FTrace slaughter data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public bool MarkFTraceAsExported(int dispatchId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_FTrace_MarkAsExported", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@DispatchID", SqlDbType.Int)).Value = dispatchId;
                    command.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the FTrace slaughter data for a docket.
        /// </summary>
        /// <param name="dispatchId">The dispatch id.</param>
        /// <returns>The FTrace delivery data for a docket.</returns>
        public DataTable GetFTraceSlaughter(int dispatchId)
        {
            var dataResult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_FTrace_Slaughter", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@DispatchID", SqlDbType.Int)).Value = dispatchId;
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllARDispatchesByDate(bool showAll, DateTime start, DateTime end, string dateType)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatches(): Attempting to get all the dispatches");
            var orders = new List<Sale>();

            try
            {
                IList<App_GetDispatches_Result> dbOrders;
                using (var entities = new NouvemEntities())
                {
                     dbOrders = entities.App_GetDispatches(start, end, showAll, dateType).ToList();
                }

                orders = (from order in dbOrders
                          select new Sale
                          {
                              SaleID = order.ARDispatchID,
                              DocumentNumberingID = order.DocumentNumberingID,
                              Number = order.Number,
                              DispatchNumber = order.Number,
                              InvoiceNumber = order.Generic2.ToNullableInt(),
                              DispatchContainer = order.DispatchContainerID.HasValue ? new Model.DataLayer.DispatchContainer { Reference = order.DispatchContainer, DispatchContainerID = order.DispatchContainerID.ToInt()} : null,
                              SaleOrderNumber = order.SaleOrderNo,
                              BPContactID = order.BPContactID,
                              DocumentDate = order.DocumentDate,
                              CreationDate = order.CreationDate,
                              DispatchContainerID = order.DispatchContainerID,
                              SaleType = ViewType.ARDispatch,
                              DeviceId = order.DeviceID,
                              EditDate = order.EditDate,
                              RouteID = order.RouteID,
                              PopUpNote = order.PopUpNote,
                              DocketNote = order.DispatchNote1,
                              CustomerPOReference = order.CustomerPOReference,
                              BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                              Printed = order.Printed,
                              IsBaseDocument = order.Invoiced,
                              NouDocStatusID = order.NouDocStatusID,
                              NouDocStatus = order.DocStatus,
                              Remarks = order.Remarks,
                              TotalExVAT = order.TotalExVAT,
                              DeliveryDate = order.DeliveryDate,
                              ShippingDate = order.ShippingDate,
                              VAT = order.VAT,
                              SubTotalExVAT = order.SubTotalExVAT,
                              GrandTotalIncVAT = order.GrandTotalIncVAT,
                              DiscountPercentage = order.DiscountPercentage,
                              SalesEmployeeID = order.UserMasterID,
                              UserIDCreation = order.UserMasterID_OrderCreation,
                              UserIDCompletion = order.UserMasterID_OrderCompletion,
                              BPHaulier = new BPMaster{BPMasterID = order.HaulierID.ToInt(), Name = order.HaulierName},
                              BPCustomer = new BPMaster { BPMasterID = order.CustomerID.ToInt(), Name = order.CustomerName, Code = order.Generic1, BPGroupID = order.Generic2.ToInt()},
                              BoxCount = order.BoxCount.ToInt(),
                              Weight = Math.Round(order.DocketWeight.ToDecimal(),2),
                              ShippingAddress = order.Generic2,
                              DeliveryAddress = new BusinessPartnerAddress
                              {
                                  Details = new BPAddress
                                  {
                                      AddressLine1 = order.ShippingAddress1 ?? string.Empty,
                                      AddressLine2 = order.ShippingAddress2 ?? string.Empty,
                                      AddressLine3 = order.ShippingAddress3 ?? string.Empty,
                                      AddressLine4 = order.ShippingAddress4 ?? string.Empty,
                                      AddressLine5 = order.ShippingAddress5 ?? string.Empty,
                                      PostCode = order.ShippingPostCode ?? string.Empty
                                  }
                              },

                              InvoiceAddress = new BusinessPartnerAddress
                              {
                                  Details = new BPAddress
                                  {
                                      AddressLine1 = order.AddressLine1 ?? string.Empty,
                                      AddressLine2 = order.AddressLine2 ?? string.Empty,
                                      AddressLine3 = order.AddressLine3 ?? string.Empty,
                                      AddressLine4 = order.AddressLine4 ?? string.Empty,
                                      AddressLine5 = order.AddressLine5 ?? string.Empty,
                                      PostCode = order.PostCode ?? string.Empty
                                  }
                              },
                          }).ToList();

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Gets the dockets the input product is on.
        /// </summary>
        /// <param name="productId">The input product.</param>
        /// <returns>A list of dockets the product is on.</returns>
        public IList<int> GetAllProductDockets(int productId)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatches(): Attempting to get all the dispatches");
            var orders = new List<int>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatchDetails
                        where order.Deleted == null && order.INMasterID == productId
                        select order.ARDispatchID).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="id">The order id.</param>
        /// <returns>The orders (and associated order details).</returns>
        public Sale GetTouchscreenARDispatch(int id)
        {
            var dbOrder = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == id);
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    dbOrder = new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainer = order.DispatchContainer,
                                  DispatchContainerID = order.DispatchContainerID,
                                  InvoiceNote = order.InvoiceNote,
                                  Number = order.Number,
                                  SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.ARDispatch,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  RouteID = order.RouteID,
                                  DocketNote = order.DispatchNote1,
                                  RouteMaster = order.RouteMaster,
                                  PopUpNote = order.PopUpNote,
                                  AgentID = order.BPMasterID_Agent,
                                  OtherReference = order.OtherReference,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  IsBaseDocument = order.Invoiced,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPMasterID = order.BPMasterID_Customer,
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     VAT = detail.VAT,
                                                     Notes = detail.Notes,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Concessions = detail.Concessions.ToList(),
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     OrderBatchID = detail.OrderBatchID,
                                                     AROrderDetailID =  detail.AROrderDetailID,
                                                     BPMasterID_Intake = detail.BPMasterID_Intake,
                                                     OrderBatchNumber = detail.OrderBatchID == null ? "" : detail.OrderBatchID == 0 ?  "--View Batches--" : entities.PROrders.First(x => x.BatchNumberID == detail.OrderBatchID).Reference,
                                                     Enforce = detail.EnforceOrderBatch,
                                                     BoxCount = 0 // entities.StockTransactions.Count(x =>
                                                    // x.Deleted == null &&
                                                    // x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId &&
                                                     //x.MasterTableID == detail.ARDispatchDetailID && x.IsBox == true)
                                                 }).ToList(),
                              };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dbOrder;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllTouchscreenARDispatches(IList<int?> orderStatuses, int? routeId = null)
        {
            this.Log.LogDebug(this.GetType(), "GetAllARDispatches(): Attempting to get all the dispatches");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (routeId.IsNullOrZero())
                    {
                        orders = (from order in entities.ARDispatches
                                  where order.Deleted == null && order.BPMasterID_Customer != null
                                  && orderStatuses.Contains(order.NouDocStatusID)
                                  select new Sale
                                  {
                                      SaleID = order.ARDispatchID,
                                      DocumentNumberingID = order.DocumentNumberingID,
                                      DispatchContainer = order.DispatchContainer,
                                      DispatchContainerID = order.DispatchContainerID,
                                      InvoiceNote = order.InvoiceNote,
                                      Number = order.Number,
                                      SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                      BPContactID = order.BPContactID,
                                      DocumentDate = order.DocumentDate,
                                      CreationDate = order.CreationDate,
                                      SaleType = ViewType.ARDispatch,
                                      DeviceId = order.DeviceID,
                                      EditDate = order.EditDate,
                                      RouteID = order.RouteID,
                                      DocketNote = order.DispatchNote1,
                                      RouteMaster = order.RouteMaster,
                                      PopUpNote = order.PopUpNote,
                                      CustomerPOReference = order.CustomerPOReference,
                                      BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                      Printed = order.Printed,
                                      IsBaseDocument = order.Invoiced,
                                      NouDocStatusID = order.NouDocStatusID,
                                      NouDocStatus = order.NouDocStatu.Value,
                                      Remarks = order.Remarks,
                                      TotalExVAT = order.TotalExVAT,
                                      DeliveryDate = order.DeliveryDate,
                                      ShippingDate = order.ShippingDate,
                                      VAT = order.VAT,
                                      SubTotalExVAT = order.SubTotalExVAT,
                                      DiscountIncVAT = order.DiscountIncVAT,
                                      GrandTotalIncVAT = order.GrandTotalIncVAT,
                                      DiscountPercentage = order.DiscountPercentage,
                                      SalesEmployeeID = order.UserMasterID,
                                      DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                      InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                      MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                      BPHaulier = order.BPMasterHaulier,
                                      BPCustomer = order.BPMasterCustomer,
                                      Deleted = order.Deleted
                                  }).ToList();
                    }
                    else
                    {
                        orders = (from order in entities.ARDispatches
                                  where order.Deleted == null
                                  && orderStatuses.Contains(order.NouDocStatusID) && order.RouteID == routeId && order.BPMasterID_Customer != null
                                  select new Sale
                                  {
                                      SaleID = order.ARDispatchID,
                                      DocumentNumberingID = order.DocumentNumberingID,
                                      DispatchContainer = order.DispatchContainer,
                                      DispatchContainerID = order.DispatchContainerID,
                                      InvoiceNote = order.InvoiceNote,
                                      Number = order.Number,
                                      SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                      BPContactID = order.BPContactID,
                                      DocumentDate = order.DocumentDate,
                                      CreationDate = order.CreationDate,
                                      SaleType = ViewType.ARDispatch,
                                      DeviceId = order.DeviceID,
                                      EditDate = order.EditDate,
                                      RouteID = order.RouteID,
                                      DocketNote = order.DispatchNote1,
                                      RouteMaster = order.RouteMaster,
                                      PopUpNote = order.PopUpNote,
                                      CustomerPOReference = order.CustomerPOReference,
                                      BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                      Printed = order.Printed,
                                      IsBaseDocument = order.Invoiced,
                                      NouDocStatusID = order.NouDocStatusID,
                                      NouDocStatus = order.NouDocStatu.Value,
                                      Remarks = order.Remarks,
                                      TotalExVAT = order.TotalExVAT,
                                      DeliveryDate = order.DeliveryDate,
                                      ShippingDate = order.ShippingDate,
                                      VAT = order.VAT,
                                      SubTotalExVAT = order.SubTotalExVAT,
                                      DiscountIncVAT = order.DiscountIncVAT,
                                      GrandTotalIncVAT = order.GrandTotalIncVAT,
                                      DiscountPercentage = order.DiscountPercentage,
                                      SalesEmployeeID = order.UserMasterID,
                                      DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                      InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                      MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                      BPHaulier = order.BPMasterHaulier,
                                      BPCustomer = order.BPMasterCustomer,
                                      Deleted = order.Deleted
                                  }).ToList();
                    }
                    
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the non invoiced dispatch data.
        /// </summary>
        /// <returns>A collection of non invoiced dispatch data (and associated order details).</returns>
        public IList<Sale> GetNonInvoicedARDispatches(bool pricingByInvoice = false)
        {
            this.Log.LogDebug(this.GetType(), "GetNonInvoicedARDispatches(): Attempting to get all the non invoiced dispatches");
            var orders = new List<Sale>();

            var docStatus = new List<int?>();
            docStatus.Add(NouvemGlobal.NouDocStatusComplete.NouDocStatusID);
            if (pricingByInvoice)
            {
                docStatus.Add(NouvemGlobal.NouDocStatusInProgress.NouDocStatusID);
            }

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.Deleted == null
                              && docStatus.Contains(order.NouDocStatusID)
                              && order.Invoiced != true
                              orderby order.Number descending 
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  InvoiceNote = order.InvoiceNote,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.ARDispatch,
                                  RouteID = order.RouteID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  IsBaseDocument = order.Invoiced,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  PopUpNote = order.PopUpNote,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} non invoiced dispatches successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the non invoiced dispatch data.
        /// </summary>
        /// <returns>A collection of non invoiced dispatch data (and associated order details).</returns>
        public IList<Sale> GetNonInvoicedARDispatches(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetNonInvoicedARDispatches(): Attempting to get all the non invoiced dispatches for customer id:{0}", customerId));
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.Deleted == null
                              && order.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID
                              && order.Invoiced != true
                              && order.BPMasterID_Customer == customerId
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  InvoiceNote = order.InvoiceNote,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  SaleType = ViewType.ARDispatch,
                                  RouteID = order.RouteID,
                                  DocketNote = order.DispatchNote1,
                                  DispatchContainerID = order.DispatchContainerID,
                                  PopUpNote = order.PopUpNote,
                                  IsBaseDocument = order.Invoiced,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} non invoiced dispatches successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        ///// <summary>
        ///// Retrieves the dispatches (and associated order details) for the input customer.
        ///// </summary>
        ///// <param name="customerId">The customer id.</param>
        ///// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        //public IList<Sale> GetARDispatches(int customerId)
        //{
        //    this.Log.LogDebug(this.GetType(), string.Format("GetARDispatches(): Attempting to get the dispatches for customer id:{0}", customerId));
        //    var receipts = new List<Sale>();

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            var localReceipts = entities.GetRecentDispatches(customerId, 10).ToList();
        //            var receipts1 = localReceipts.GroupBy(x => x.ARDispatchID).ToDictionary(key => key.Key, value => value.ToList());
        //            //var receipts1 = entities.ARDispatches.Where(x => x.BPMasterID_Customer == customerId)
        //            //    .OrderByDescending(x => x.ARDispatchID).Take(10);

        //            foreach (var localOrder in receipts1)
        //            {
        //                var order = localOrder.Value;
        //                var dispatch = new Sale
        //                {
        //                    SaleID = order.First().ARDispatchID,
        //                    //DocumentNumberingID = order.DocumentNumberingID,
        //                    //DispatchContainerID = order.DispatchContainerID,
        //                    //Number = order.Number,
        //                    //BPContactID = order.BPContactID,
        //                    //DocumentDate = order.DocumentDate,
        //                    CreationDate = order.First().creationdate,
        //                    //SaleType = ViewType.ARDispatch,
        //                    //DeviceId = order.DeviceID,
        //                    //EditDate = order.EditDate,
        //                    //Printed = order.Printed,
        //                    //IsBaseDocument = order.Invoiced,
        //                    //RouteID = order.RouteID,
        //                    //PopUpNote = order.PopUpNote,
        //                    //BaseDocumentReferenceID = order.BaseDocumentReferenceID,
        //                    //CustomerPOReference = order.CustomerPOReference,
        //                    //NouDocStatusID = order.NouDocStatusID,
        //                    //NouDocStatus = order.NouDocStatu.Value,
        //                    //Remarks = order.Remarks,
        //                    //TotalExVAT = order.TotalExVAT,
        //                    //DeliveryDate = order.DeliveryDate,
        //                    //ShippingDate = order.ShippingDate,
        //                    //VAT = order.VAT,
        //                    //SubTotalExVAT = order.SubTotalExVAT,
        //                    //DiscountIncVAT = order.DiscountIncVAT,
        //                    //GrandTotalIncVAT = order.GrandTotalIncVAT,
        //                    //DiscountPercentage = order.DiscountPercentage,
        //                    //SalesEmployeeID = order.UserMasterID,
        //                    //DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
        //                    //InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
        //                    //MainContact = new BusinessPartnerContact { Details = order.BPContact },
        //                    //BPHaulier = order.BPMasterHaulier,
        //                    //BPCustomer = order.BPMasterCustomer,
        //                    //Deleted = order.Deleted,
        //                    SaleDetails = new List<SaleDetail>()
        //                };

        //                foreach (var detail in order)
        //                {
        //                    //var id = detail.PriceListID;
        //                    //var c = id;
        //                    //var sd = new SaleDetail();
        //                    dispatch.SaleDetails.Add(new SaleDetail
        //                    {
        //                        LoadingSale = true,
        //                        SaleDetailID = detail.ARDispatchDetailID,
        //                        SaleID = detail.ARDispatchID,
        //                        QuantityOrdered = detail.QuantityOrdered,
        //                        WeightOrdered = detail.WeightOrdered,
        //                        QuantityDelivered = detail.QuantityDelivered,
        //                        WeightDelivered = detail.WeightDelivered,
        //                        UnitPrice = detail.UnitPrice,
        //                        ////SaleDetailType = ViewType.ARDispatch,
        //                        ////DiscountPercentage = detail.DiscountPercentage,
        //                        ////DiscountPrice = detail.DiscountPrice,
        //                        ////LineDiscountPercentage = detail.LineDiscountPercentage,
        //                        ////LineDiscountAmount = detail.LineDiscountAmount,
        //                        UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
        //                        ////VAT = detail.VAT,
        //                        ////TotalExVAT = detail.TotalExclVAT,
        //                        ////TotalIncVAT = detail.TotalIncVAT,
        //                        ////VatCodeID = detail.VATCodeID,
        //                        //LoadingSale = true,
        //                        INMasterID = detail.INMasterID,
        //                        ////INMaster = detail.INMaster,
        //                        PriceListID = detail.PriceListID ?? 0,
        //                        //OrderDate = order.DocumentDate,
        //                        //NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
        //                        //Deleted = detail.Deleted,
        //                    });
        //                }

        //                receipts.Add(dispatch);
        //            }


        //            //receipts = (from order in receipts1
        //            //            //where order.BPMasterID_Customer == customerId //&& order.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID && order.Deleted == null
        //            //            //orderby order.ARDispatchID descending 
        //            //            select new Sale
        //            //            {
        //            //                SaleID = order.ARDispatchID,
        //            //                DocumentNumberingID = order.DocumentNumberingID,
        //            //                DispatchContainerID = order.DispatchContainerID,
        //            //                Number = order.Number,
        //            //                BPContactID = order.BPContactID,
        //            //                DocumentDate = order.DocumentDate,
        //            //                CreationDate = order.CreationDate,
        //            //                SaleType = ViewType.ARDispatch,
        //            //                DeviceId = order.DeviceID,
        //            //                EditDate = order.EditDate,
        //            //                Printed = order.Printed,
        //            //                IsBaseDocument = order.Invoiced,
        //            //                RouteID = order.RouteID,
        //            //                PopUpNote = order.PopUpNote,
        //            //                BaseDocumentReferenceID = order.BaseDocumentReferenceID,
        //            //                CustomerPOReference = order.CustomerPOReference,
        //            //                NouDocStatusID = order.NouDocStatusID,
        //            //                NouDocStatus = order.NouDocStatu.Value,
        //            //                Remarks = order.Remarks,
        //            //                TotalExVAT = order.TotalExVAT,
        //            //                DeliveryDate = order.DeliveryDate,
        //            //                ShippingDate = order.ShippingDate,
        //            //                VAT = order.VAT,
        //            //                SubTotalExVAT = order.SubTotalExVAT,
        //            //                DiscountIncVAT = order.DiscountIncVAT,
        //            //                GrandTotalIncVAT = order.GrandTotalIncVAT,
        //            //                DiscountPercentage = order.DiscountPercentage,
        //            //                SalesEmployeeID = order.UserMasterID,
        //            //                DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
        //            //                InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
        //            //                MainContact = new BusinessPartnerContact { Details = order.BPContact },
        //            //                BPHaulier = order.BPMasterHaulier,
        //            //                BPCustomer = order.BPMasterCustomer,
        //            //                Deleted = order.Deleted,
        //            //                //Attachments = (from attachment in order.ARDispatchAttachments
        //            //                //               where attachment.Deleted == null
        //            //                //               select new Attachment
        //            //                //               {
        //            //                //                   AttachmentID = attachment.ARDispatchAttachmentID,
        //            //                //                   SaleAttachmentID = attachment.ARDispatchID,
        //            //                //                   AttachmentDate = attachment.AttachmentDate,
        //            //                //                   FilePath = attachment.FilePath,
        //            //                //                   FileName = attachment.FileName,
        //            //                //                   File = attachment.File,
        //            //                //                   Deleted = attachment.Deleted
        //            //                //               }).ToList(),
        //            //                SaleDetails = (from detail in order.ARDispatchDetails
        //            //                               //where detail.Deleted == null
        //            //                               select new SaleDetail
        //            //                               {
        //            //                                   SaleDetailID = detail.ARDispatchDetailID,
        //            //                                   //SaleID = detail.ARDispatchID,
        //            //                                   //QuantityOrdered = detail.QuantityOrdered,
        //            //                                   //WeightOrdered = detail.WeightOrdered,
        //            //                                   //QuantityDelivered = detail.QuantityDelivered,
        //            //                                   //WeightDelivered = detail.WeightDelivered,
        //            //                                   //UnitPrice = detail.UnitPrice,
        //            //                                   //SaleDetailType = ViewType.ARDispatch,
        //            //                                   //DiscountPercentage = detail.DiscountPercentage,
        //            //                                   //DiscountPrice = detail.DiscountPrice,
        //            //                                   //LineDiscountPercentage = detail.LineDiscountPercentage,
        //            //                                   //LineDiscountAmount = detail.LineDiscountAmount,
        //            //                                   //UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
        //            //                                   //VAT = detail.VAT,
        //            //                                   //TotalExVAT = detail.TotalExclVAT,
        //            //                                   //TotalIncVAT = detail.TotalIncVAT,
        //            //                                   //VatCodeID = detail.VATCodeID,
        //            //                                   //LoadingSale = true,
        //            //                                   //INMasterID = detail.INMasterID,
        //            //                                   //INMaster = detail.INMaster,
        //            //                                   //  PriceListID = detail.PriceListID ?? 0,
        //            //                                   //OrderDate = order.DocumentDate,
        //            //                                   //NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
        //            //                                   //Deleted = detail.Deleted,
        //            //                                   //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
        //            //                                   //                     select new GoodsReceiptData
        //            //                                   //                     {
        //            //                                   //                         BatchNumber = batchNumber,
        //            //                                   //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
        //            //                                   //                         TransactionData = (from transaction in batchNumber.StockTransactions
        //            //                                   //                                            select new StockTransactionData
        //            //                                   //                                            {
        //            //                                   //                                                Transaction = transaction,
        //            //                                   //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
        //            //                                   //                                            }).ToList()
        //            //                                   //                     }).ToList(),
        //            //                               }).ToList(),
        //            //            })

        //            //            .ToList();
        //        }

        //        this.Log.LogDebug(this.GetType(), string.Format("{0} orders receipts successfully retrieved", receipts.Count));
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return receipts;
        //}

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentDispatches(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the recent orders for customer id:{0}", customerId));
            var orders = new List<Sale>();

            try
            {
                IList<App_GetRecentDispatchOrders_Result> dbOrders;
                using (var entities = new NouvemEntities())
                {
                    dbOrders = entities.App_GetRecentDispatchOrders(customerId).ToList();
                }

                var groupedOrders = dbOrders.OrderByDescending(x => x.ARDispatchID).GroupBy(x => x.ARDispatchID).ToList();
                foreach (var localOrders in groupedOrders)
                {
                    var arOrder = new Sale
                    {
                        SaleID = localOrders.First().ARDispatchID,
                        Number = localOrders.First().Number,
                        CreationDate = localOrders.First().CreationDate,
                        ShippingDate = localOrders.First().ShippingDate,
                        DeliveryDate = localOrders.First().DeliveryDate,
                        SaleDetails = new List<SaleDetail>()
                    };

                    foreach (var detail in localOrders)
                    {
                        arOrder.SaleDetails.Add(new SaleDetail
                        {
                            LoadingSale = true,
                            SaleID = detail.ARDispatchID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID ?? 0
                        });
                    }

                    orders.Add(arOrder);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var receipt in orders)
            {
                receipt.Attachments = new List<Attachment>();
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the dispatches (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetARDispatches(int customerId, int ordersToTake)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatches(): Attempting to get the dispatches for customer id:{0}", customerId));
            var receipts = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    receipts = (from order in entities.ARDispatches
                                where order.BPMasterID_Customer == customerId && order.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID && order.Deleted == null
                                orderby order.DocumentDate descending
                                select new Sale
                                {
                                    SaleID = order.ARDispatchID,
                                    InvoiceNote = order.InvoiceNote,
                                    DocumentNumberingID = order.DocumentNumberingID,
                                    Number = order.Number,
                                    BPContactID = order.BPContactID,
                                    DocumentDate = order.DocumentDate,
                                    CreationDate = order.CreationDate,
                                    SaleType = ViewType.ARDispatch,
                                    DeviceId = order.DeviceID,
                                    EditDate = order.EditDate,
                                    Printed = order.Printed,
                                    IsBaseDocument = order.Invoiced,
                                    RouteID = order.RouteID,
                                    PopUpNote = order.PopUpNote,
                                    BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                    CustomerPOReference = order.CustomerPOReference,
                                    NouDocStatusID = order.NouDocStatusID,
                                    NouDocStatus = order.NouDocStatu.Value,
                                    Remarks = order.Remarks,
                                    TotalExVAT = order.TotalExVAT,
                                    DeliveryDate = order.DeliveryDate,
                                    VAT = order.VAT,
                                    SubTotalExVAT = order.SubTotalExVAT,
                                    DiscountIncVAT = order.DiscountIncVAT,
                                    GrandTotalIncVAT = order.GrandTotalIncVAT,
                                    DiscountPercentage = order.DiscountPercentage,
                                    SalesEmployeeID = order.UserMasterID,
                                    BPHaulier = order.BPMasterHaulier,
                                    BPCustomer = order.BPMasterCustomer,
                                    Deleted = order.Deleted,
                                    SaleDetails = (from detail in order.ARDispatchDetails
                                                   where detail.Deleted == null
                                                   select new SaleDetail
                                                   {
                                                       LoadingSale = true,
                                                       SaleDetailID = detail.ARDispatchDetailID,
                                                       SaleID = detail.ARDispatchID,
                                                       QuantityOrdered = detail.QuantityOrdered,
                                                       WeightOrdered = detail.WeightOrdered,
                                                       QuantityDelivered = detail.QuantityDelivered,
                                                       WeightDelivered = detail.WeightDelivered,
                                                       UnitPrice = detail.UnitPrice,
                                                       SaleDetailType = ViewType.ARDispatch,
                                                       DiscountPercentage = detail.DiscountPercentage,
                                                       DiscountPrice = detail.DiscountPrice,
                                                       LineDiscountPercentage = detail.LineDiscountPercentage,
                                                       LineDiscountAmount = detail.LineDiscountAmount,
                                                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                       VAT = detail.VAT,
                                                       TotalExVAT = detail.TotalExclVAT,
                                                       TotalIncVAT = detail.TotalIncVAT,
                                                       VatCodeID = detail.VATCodeID,
                                                       INMasterID = detail.INMasterID,
                                                       PriceListID = detail.PriceListID ?? 0,
                                                       OrderDate = order.DocumentDate
                                                   }).ToList(),
                                })
                                .Take(ordersToTake) // ApplicationSettings.RecentOrdersAmtToTake
                                .ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders receipts successfully retrieved", receipts.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return receipts;
        }

        /// <summary>
        /// Retrieves the dispatches (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetOpenARDispatches(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatches(): Attempting to get the dispatches for customer id:{0}", customerId));
            var receipts = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    receipts = (from order in entities.ARDispatches
                                where order.BPMasterID_Customer == customerId
                                && (order.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID || order.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID)
                                && order.Deleted == null
                                orderby order.DocumentDate descending
                                select new Sale
                                {
                                    SaleID = order.ARDispatchID,
                                    DocumentNumberingID = order.DocumentNumberingID,
                                    DispatchContainerID = order.DispatchContainerID,
                                    Number = order.Number,
                                    BPContactID = order.BPContactID,
                                    DocumentDate = order.DocumentDate,
                                    CreationDate = order.CreationDate,
                                    SaleType = ViewType.ARDispatch,
                                    InvoiceNote = order.InvoiceNote,
                                    DeviceId = order.DeviceID,
                                    EditDate = order.EditDate,
                                    Printed = order.Printed,
                                    IsBaseDocument = order.Invoiced,
                                    RouteID = order.RouteID,
                                    PopUpNote = order.PopUpNote,
                                    BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                    CustomerPOReference = order.CustomerPOReference,
                                    NouDocStatusID = order.NouDocStatusID,
                                    NouDocStatus = order.NouDocStatu.Value,
                                    Remarks = order.Remarks,
                                    TotalExVAT = order.TotalExVAT,
                                    DeliveryDate = order.DeliveryDate,
                                    ShippingDate = order.ShippingDate,
                                    VAT = order.VAT,
                                    SubTotalExVAT = order.SubTotalExVAT,
                                    DiscountIncVAT = order.DiscountIncVAT,
                                    GrandTotalIncVAT = order.GrandTotalIncVAT,
                                    DiscountPercentage = order.DiscountPercentage,
                                    SalesEmployeeID = order.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                    MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                    BPHaulier = order.BPMasterHaulier,
                                    BPCustomer = order.BPMasterCustomer,
                                    Deleted = order.Deleted,
                                    SaleDetails = (from detail in order.ARDispatchDetails
                                                   where detail.Deleted == null
                                                   select new SaleDetail
                                                   {
                                                       LoadingSale = true,
                                                       SaleDetailID = detail.ARDispatchDetailID,
                                                       SaleID = detail.ARDispatchID,
                                                       QuantityOrdered = detail.QuantityOrdered,
                                                       WeightOrdered = detail.WeightOrdered,
                                                       QuantityDelivered = detail.QuantityDelivered,
                                                       WeightDelivered = detail.WeightDelivered,
                                                       UnitPrice = detail.UnitPrice,
                                                       SaleDetailType = ViewType.ARDispatch,
                                                       DiscountPercentage = detail.DiscountPercentage,
                                                       DiscountPrice = detail.DiscountPrice,
                                                       LineDiscountPercentage = detail.LineDiscountPercentage,
                                                       LineDiscountAmount = detail.LineDiscountAmount,
                                                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                       VAT = detail.VAT,
                                                       TotalExVAT = detail.TotalExclVAT,
                                                       TotalIncVAT = detail.TotalIncVAT,
                                                       VatCodeID = detail.VATCodeID,
                                                       INMasterID = detail.INMasterID,
                                                       INMaster = detail.INMaster,
                                                       PriceListID = detail.PriceListID ?? 0,
                                                       OrderDate = order.DocumentDate,
                                                       NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                       Deleted = detail.Deleted,
                                                   }).ToList(),
                                })
                                .Take(20)
                                .ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders receipts successfully retrieved", receipts.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return receipts;
        }

        /// <summary>
        /// Retrieves all the consolidated invoice dispatches.
        /// </summary>
        /// <returns>A collection of consolidated invoice dispatches (and associated order details).</returns>
        public IList<Sale> GetARDispatchesByInvoice(int invoiceId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesByInvoice(): Attempting to get all the invoice dispatches for invoice id:{0}", invoiceId));
            var orders = new List<Sale>();
     
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.ARInvoiceID == invoiceId
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  Number = order.Number,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.ARDispatch,
                                  RouteID = order.RouteID,
                                  DocketNote = order.DispatchNote1,
                                  PopUpNote = order.PopUpNote,
                                  IsBaseDocument = order.Invoiced,
                                  CustomerPOReference = order.CustomerPOReference,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                           
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoice dispatches successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of dispatches (and associated dispatch details) for the input search term.</returns>
        public IList<Sale> GetARDispatchesByPartner(int partnerId, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesByName(): Attempting to get the dispatches for id:{0}", partnerId));
            var orders = new List<Sale>();
           
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.BPMasterID_Customer == partnerId
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  SaleType = ViewType.ARDispatch,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  Number = order.Number,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  DocketNote = order.DispatchNote1,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  RouteID = order.RouteID,
                                  IsBaseDocument = order.Invoiced,
                                  PopUpNote = order.PopUpNote,
                                  CustomerPOReference = order.CustomerPOReference,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of dispatches (and associated dispatch details) for the input search term.</returns>
        public IList<Sale> GetARDispatchesByName(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesByName(): Attempting to get the dispatches for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.BPMasterCustomer.Name.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  SaleType = ViewType.ARDispatch,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  Number = order.Number,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  IsBaseDocument = order.Invoiced,
                                  DocketNote = order.DispatchNote1,
                                  PopUpNote = order.PopUpNote,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  RouteID = order.RouteID,
                                  CustomerPOReference = order.CustomerPOReference,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order dispatches (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetARDispatchesByCode(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesByCode(): Attempting to get the order dispatches for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARDispatches
                              where order.BPMasterCustomer.Code.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.ARDispatchID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  DispatchContainerID = order.DispatchContainerID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  DocketNote = order.DispatchNote1,
                                  SaleType = ViewType.ARDispatch,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  CreationDate = order.CreationDate,
                                  RouteID = order.RouteID,
                                  PopUpNote = order.PopUpNote,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  CustomerPOReference = order.CustomerPOReference,
                                  Printed = order.Printed,
                                  IsBaseDocument = order.Invoiced,
                                  DeliveryDate = order.DeliveryDate,
                                  ShippingDate = order.ShippingDate,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterCustomer,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.ARDispatchAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARDispatchAttachmentID,
                                                     SaleAttachmentID = attachment.ARDispatchID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.ARDispatchDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARDispatchDetailID,
                                                     SaleID = detail.ARDispatchID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchById(int arDispatchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchById(): Attempting to get the receipt for dispatch id:{0}", arDispatchId));
            Sale apReceipt;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == arDispatchId);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Dispatch Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.ARDispatchID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        DispatchContainerID = order.DispatchContainerID,
                        InvoiceNote = order.InvoiceNote,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        SaleType = ViewType.APReceipt,
                        CreationDate = order.CreationDate,
                        RouteID = order.RouteID,
                        IsBaseDocument = order.Invoiced,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        PopUpNote = order.PopUpNote,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        CustomerPOReference = order.CustomerPOReference,
                        Printed = order.Printed,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        AgentID = order.BPMasterID_Agent,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.ARDispatchAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.ARDispatchAttachmentID,
                                           SaleAttachmentID = attachment.ARDispatchID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.ARDispatchDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARDispatchDetailID,
                                           SaleID = detail.ARDispatchID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityDelivered,
                                           WeightDelivered = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           SaleDetailType = ViewType.APReceipt,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           Notes = detail.Notes,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           CostTotal = detail.CostTotal,
                                           Margin = detail.Margin,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           BPMasterID_Intake = detail.BPMasterID_Intake,
                                           Deleted = detail.Deleted,
                                           Enforce = detail.EnforceOrderBatch,
                                           OrderBatchID = detail.OrderBatchID
                                       }).ToList(),
                    };
                }

                this.Log.LogDebug(this.GetType(), "Dispatch successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchByFirstLast(bool first)
        {
            Sale apReceipt;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    ARDispatch order = null;
                    if (first)
                    {
                        var id = entities.ARDispatches.FirstOrDefault();
                        if (id != null)
                        {
                            this.PriceDispatchDocket(id.ARDispatchID);
                            order = entities.ARDispatches.FirstOrDefault();
                        }
                    }
                    else
                    {
                        var id = entities.ARDispatches.OrderByDescending(x => x.ARDispatchID).FirstOrDefault();
                        if (id != null)
                        {
                            this.PriceDispatchDocket(id.ARDispatchID);
                            order = entities.ARDispatches.First(x => x.ARDispatchID == id.ARDispatchID);
                        }
                    }

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Dispatch Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.ARDispatchID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        DispatchContainerID = order.DispatchContainerID,
                        InvoiceNote = order.InvoiceNote,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        SaleType = ViewType.APReceipt,
                        CreationDate = order.CreationDate,
                        RouteID = order.RouteID,
                        IsBaseDocument = order.Invoiced,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        PopUpNote = order.PopUpNote,
                        AgentID = order.BPMasterID_Agent,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        CustomerPOReference = order.CustomerPOReference,
                        Printed = order.Printed,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.ARDispatchAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.ARDispatchAttachmentID,
                                           SaleAttachmentID = attachment.ARDispatchID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.ARDispatchDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARDispatchDetailID,
                                           SaleID = detail.ARDispatchID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityDelivered,
                                           WeightDelivered = detail.WeightDelivered,

                                           UnitPrice = detail.UnitPrice,
                                           SaleDetailType = ViewType.APReceipt,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           Notes = detail.Notes,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           CostTotal = detail.CostTotal,
                                           Margin = detail.Margin,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           BPMasterID_Intake = detail.BPMasterID_Intake,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }

                this.Log.LogDebug(this.GetType(), "Dispatch successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the dispatch (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchByLastEdit()
        {
            Sale apReceipt;

            try
            {
                int dispatchId;
                using (var entities = new NouvemEntities())
                {
                    var orderId = entities.ARDispatches
                        .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (orderId == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Dispatch Data corresponding to order id not found");
                        return null;
                    }

                    dispatchId = orderId.ARDispatchID;
                    this.PriceDispatchDocket(dispatchId);
                }

                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.First(x => x.ARDispatchID == dispatchId);
                    apReceipt = new Sale
                    {
                        SaleID = order.ARDispatchID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        DispatchContainerID = order.DispatchContainerID,
                        InvoiceNote = order.InvoiceNote,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        SaleType = ViewType.ARDispatch,
                        CreationDate = order.CreationDate,
                        RouteID = order.RouteID,
                        IsBaseDocument = order.Invoiced,
                        DeviceId = order.DeviceID,
                        DocketNote = order.DispatchNote1,
                        EditDate = order.EditDate,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        CustomerPOReference = order.CustomerPOReference,
                        Printed = order.Printed,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        AgentID = order.BPMasterID_Agent,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        PopUpNote = order.PopUpNote,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.ARDispatchAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.ARDispatchAttachmentID,
                                           SaleAttachmentID = attachment.ARDispatchID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.ARDispatchDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARDispatchDetailID,
                                           SaleID = detail.ARDispatchID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityDelivered,
                                           WeightDelivered = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           SaleDetailType = ViewType.APReceipt,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           Notes = detail.Notes,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           CostTotal = detail.CostTotal,
                                           Margin = detail.Margin,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           BPMasterID_Intake = detail.BPMasterID_Intake,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }

                this.Log.LogDebug(this.GetType(), "Dispatch successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetDispatchDetails_Result> GetDispatchStatusDetails(int arDispatchId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetDispatchDetails(arDispatchId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchesById(int arDispatchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesById(): Attempting to get the receipt for dispatch id:{0}", arDispatchId));
            Sale apReceipt;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == arDispatchId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Dispatch Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.ARDispatchID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        DispatchContainerID = order.DispatchContainerID,
                        DispatchContainer = order.DispatchContainer,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        BPMasterID = order.BPMasterID_Customer,
                        SaleType = ViewType.ARDispatch,
                        CreationDate = order.CreationDate,
                        RouteID = order.RouteID,
                        IsBaseDocument = order.Invoiced,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        PopUpNote = order.PopUpNote,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        CustomerPOReference = order.CustomerPOReference,
                        Printed = order.Printed,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        InvoiceNote = order.InvoiceNote,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.ARDispatchAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.ARDispatchAttachmentID,
                                           SaleAttachmentID = attachment.ARDispatchID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList()
                    };

                    var localDetails = new List<SaleDetail>();
                    foreach (var detail in order.ARDispatchDetails.Where(x => x.Deleted == null))
                    {
                        var localSaleDetail = new SaleDetail();

                        var trans = entities.StockTransactions
                            .Where(x => x.Deleted == null &&
                                        x.MasterTableID == detail.ARDispatchDetailID
                                        && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId
                                        && x.IsBox != true);

                        var localQty = trans.Sum(x => x.TransactionQTY);
                        var localWeight = trans.Sum(x => x.TransactionWeight);

                        if (localQty.ToDecimal() != detail.QuantityDelivered.ToDecimal())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesById(): Updating qty from {0} to {1}", detail.QuantityDelivered, localQty.ToDecimal()));
                            detail.QuantityDelivered = localQty.ToDecimal();
                            entities.SaveChanges();
                        }

                        if (localWeight.ToDecimal() != detail.WeightDelivered.ToDecimal())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesById(): Updating wgt from {0} to {1}", detail.WeightDelivered, localWeight.ToDecimal()));
                            detail.WeightDelivered = localWeight.ToDecimal();
                            entities.SaveChanges();
                        }

                        localSaleDetail.LoadingSale = true;
                        localSaleDetail.INMasterID = detail.INMasterID;
                        localSaleDetail.SaleDetailID = detail.ARDispatchDetailID;
                        localSaleDetail.QuantityOrdered = detail.QuantityOrdered;
                        localSaleDetail.WeightOrdered = detail.WeightOrdered;
                        localSaleDetail.QuantityDelivered = detail.QuantityDelivered;
                        localSaleDetail.WeightDelivered = detail.WeightDelivered;
                        localSaleDetail.SaleID = detail.ARDispatchID;
                        localSaleDetail.UnitPrice = detail.UnitPrice;
                        localSaleDetail.DiscountPercentage = detail.DiscountPercentage;
                        localSaleDetail.DiscountPrice = detail.DiscountPrice;
                        localSaleDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                        localSaleDetail.LineDiscountAmount = detail.LineDiscountAmount;
                        localSaleDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                        localSaleDetail.VAT = detail.VAT;
                        localSaleDetail.TotalExVAT = detail.TotalExclVAT;
                        localSaleDetail.TotalIncVAT = detail.TotalIncVAT;
                        localSaleDetail.VatCodeID = detail.VATCodeID;
                        localSaleDetail.PriceListID = detail.PriceListID ?? 0;
                        localSaleDetail.NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null;
                        localSaleDetail.CostTotal = detail.CostTotal;
                        localSaleDetail.Margin = detail.Margin;
                        localSaleDetail.Deleted = detail.Deleted;
                        localDetails.Add(localSaleDetail);
                        if (detail.Concessions != null)
                        {
                            localSaleDetail.Concessions = detail.Concessions.ToList();
                        }
                    }

                    apReceipt.SaleDetails = localDetails;
                }

                this.Log.LogDebug(this.GetType(), "Dispatch successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the dispatches (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="arDispatchId">The dispatch id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetARDispatchesByIdWithBoxCount(int arDispatchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesById(): Attempting to get the receipt for dispatch id:{0}", arDispatchId));
            Sale apReceipt;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == arDispatchId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Dispatch Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.ARDispatchID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        DispatchContainerID = order.DispatchContainerID,
                        DispatchContainer = order.DispatchContainer,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        SaleType = ViewType.ARDispatch,
                        CreationDate = order.CreationDate,
                        RouteID = order.RouteID,
                        IsBaseDocument = order.Invoiced,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        PopUpNote = order.PopUpNote,
                        AgentID = order.BPMasterID_Agent,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        CustomerPOReference = order.CustomerPOReference,
                        Printed = order.Printed,
                        DeliveryDate = order.DeliveryDate,
                        ShippingDate = order.ShippingDate,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        InvoiceNote = order.InvoiceNote,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterCustomer,
                        Deleted = order.Deleted,
                    };

                    var localDetails = new List<SaleDetail>();
                    foreach (var detail in order.ARDispatchDetails.Where(x => x.Deleted == null))
                    {
                        var localSaleDetail = new SaleDetail();

                        var trans = entities.StockTransactions
                            .Where(x => x.Deleted == null &&
                                        x.MasterTableID == detail.ARDispatchDetailID
                                        && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId
                                        && x.IsBox != true);

                        var localQty = trans.Sum(x => x.TransactionQTY);
                        var localWeight = trans.Sum(x => x.TransactionWeight);

                        if (localQty.ToDecimal() != detail.QuantityDelivered.ToDecimal())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesById(): Updating qty from {0} to {1}", detail.QuantityDelivered, localQty.ToDecimal()));
                            detail.QuantityDelivered = localQty.ToDecimal();
                            entities.SaveChanges();
                        }

                        if (localWeight.ToDecimal() != detail.WeightDelivered.ToDecimal())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("GetARDispatchesById(): Updating wgt from {0} to {1}", detail.WeightDelivered, localWeight.ToDecimal()));
                            detail.WeightDelivered = localWeight.ToDecimal();
                            entities.SaveChanges();
                        }

                        localSaleDetail.LoadingSale = true;
                        localSaleDetail.INMasterID = detail.INMasterID;
                        localSaleDetail.SaleDetailID = detail.ARDispatchDetailID;
                        localSaleDetail.QuantityOrdered = detail.QuantityOrdered;
                        localSaleDetail.WeightOrdered = detail.WeightOrdered;
                        localSaleDetail.QuantityDelivered = detail.QuantityDelivered;
                        localSaleDetail.WeightDelivered = detail.WeightDelivered;
                        localSaleDetail.SaleID = detail.ARDispatchID;
                        localSaleDetail.UnitPrice = detail.UnitPrice;
                        localSaleDetail.DiscountPercentage = detail.DiscountPercentage;
                        localSaleDetail.DiscountPrice = detail.DiscountPrice;
                        localSaleDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                        localSaleDetail.LineDiscountAmount = detail.LineDiscountAmount;
                        localSaleDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                        localSaleDetail.VAT = detail.VAT;
                        localSaleDetail.TotalExVAT = detail.TotalExclVAT;
                        localSaleDetail.TotalIncVAT = detail.TotalIncVAT;
                        localSaleDetail.VatCodeID = detail.VATCodeID;
                        localSaleDetail.PriceListID = detail.PriceListID ?? 0;
                        localSaleDetail.NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null;
                        localSaleDetail.CostTotal = detail.CostTotal;
                        localSaleDetail.Margin = detail.Margin;
                        localSaleDetail.Deleted = detail.Deleted;
                        localSaleDetail.Notes = detail.Notes;
                        localSaleDetail.BoxCount = entities.StockTransactions.Count(x =>
                            x.Deleted == null &&
                            x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId &&
                            x.MasterTableID == detail.ARDispatchDetailID && x.IsBox == true);
                        localDetails.Add(localSaleDetail);
                        if (detail.Concessions != null)
                        {
                            localSaleDetail.Concessions = detail.Concessions.ToList();
                        }
                    }

                    apReceipt.SaleDetails = localDetails;
                }

                this.Log.LogDebug(this.GetType(), "Dispatch successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="qtyPerBox">The qty per box.</param>
        /// <returns>The box transaction id.</returns>
        public int AddBoxTransaction(StockTransaction transaction, int qtyPerBox)
        {
            var orderId = transaction.MasterTableID;
            var productId = transaction.INMasterID;

            this.Log.LogDebug(this.GetType(), string.Format("GetBoxTransactions(): Retrieving box transactions for order id:{0}, productId:{1}", orderId, productId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var products =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == orderId
                                && x.INMasterID == productId
                                && x.StockTransactionID_Container == null
                                && x.Consumed == null
                                && x.Deleted == null
                                && x.IsBox != true
                                && x.WarehouseID == NouvemGlobal.DispatchId
                                && x.DeviceMasterID == NouvemGlobal.DeviceId
                                && x.MasterTableID == orderId
                                && x.CanAddToBox != false).ToList();

                    if (!products.Any() || products.Count < qtyPerBox)
                    {
                        return -1;
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} transactions retrieved", products.Count));

                    var weight = products.Sum(x => x.TransactionWeight);
                    var qty = products.Sum(x => x.TransactionQTY);
                    var pieces = products.Sum(x => x.Pieces);
                    var tare = products.Sum(x => x.Tare);
                    var batchId = products.Last().BatchNumberID;
                    var attributeId = products.Last().AttributeID;

                    transaction.TransactionWeight = weight;
                    transaction.TransactionQTY = qty;
                    transaction.Pieces = pieces;
                    transaction.Tare = tare;
                    transaction.BatchNumberID = batchId;
                    transaction.AttributeID = attributeId;

                    entities.StockTransactions.Add(transaction);

                    //var transactionData = products.Last();
                       //entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                           //.FirstOrDefault(x => x.BatchNumberID == transaction.BatchNumberID);

                    entities.SaveChanges();

                    //if (transactionData != null)
                    //{
                    //    foreach (var trans in transactionData.TransactionTraceabilities)
                    //    {
                    //        var boxTrans = this.CreateDeepCopyTransactionTraceability(trans);
                    //        boxTrans.StockTransactionID = transaction.StockTransactionID;
                    //        entities.TransactionTraceabilities.Add(boxTrans);
                    //    }
                    //}

                    products.ForEach(x =>
                    {
                        x.StockTransactionID_Container = transaction.StockTransactionID;
                        x.Consumed = DateTime.Now;
                    });

                    transaction.Serial = transaction.StockTransactionID;
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), string.Format("Box transaction created id:{0}", transaction.StockTransactionID));
                    return transaction.StockTransactionID;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("No Products"))
                {
                    throw;
                }

                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="qtyPerBox">The qty per box.</param>
        /// <returns>The box transaction id.</returns>
        public IList<int> AddBoxTransactionOnCompleteOrder(StockTransaction transaction, int qtyPerBox)
        {
            var transIds = new List<int>();
            var orderId = transaction.MasterTableID;
            var productId = transaction.INMasterID;

            this.Log.LogDebug(this.GetType(), string.Format("GetBoxTransactionOnCompleteOrder(): Retrieving box transactions for order id:{0}, productId:{1}", orderId, productId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var products =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == orderId
                                && x.INMasterID == productId
                                && x.StockTransactionID_Container == null
                                && x.Consumed == null
                                && x.Deleted == null
                                && x.IsBox != true
                                && x.WarehouseID == NouvemGlobal.DispatchId
                                && x.MasterTableID == orderId
                                && x.CanAddToBox != false).ToList();

                    if (!products.Any() || products.Count < qtyPerBox)
                    {
                        return transIds;
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} transactions retrieved", products.Count));

                    var groupedProducts = products.GroupBy(x => x.DeviceMasterID);

                    foreach (var tranactions in groupedProducts)
                    {
                        var localTransaction = new StockTransaction
                        {
                            INMasterID = transaction.INMasterID,
                            MasterTableID = transaction.MasterTableID,
                            WarehouseID = NouvemGlobal.DispatchId,
                            UserMasterID = NouvemGlobal.UserId,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            TransactionDate = DateTime.Now,
                            IsBox = true,
                            StoredLabelID = transaction.StoredLabelID,
                            NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId
                        };

                        var weight = tranactions.Sum(x => x.TransactionWeight);
                        var qty = tranactions.Sum(x => x.TransactionQTY);
                        var pieces = tranactions.Sum(x => x.Pieces);
                        var tare = tranactions.Sum(x => x.Tare);
                        int? attributeId = null;
                        int? batchId = null;
                        int? processId = null;
                        if (tranactions.Any())
                        {
                            batchId = tranactions.Last().BatchNumberID;
                            attributeId = tranactions.Last().AttributeID;
                            processId = tranactions.Last().ProcessID;
                        }

                        localTransaction.TransactionWeight = weight;
                        localTransaction.TransactionQTY = qty;
                        localTransaction.Pieces = pieces;
                        localTransaction.Tare = tare;
                        localTransaction.BatchNumberID = batchId;
                        localTransaction.AttributeID = attributeId;
                        localTransaction.ProcessID = processId;

                        entities.StockTransactions.Add(localTransaction);
                        entities.SaveChanges();

                        foreach (var stockTransaction in products.Where(x => x.DeviceMasterID == tranactions.Key))
                        {
                            stockTransaction.StockTransactionID_Container = localTransaction.StockTransactionID;
                            stockTransaction.Consumed = DateTime.Now;
                        }

                        localTransaction.Serial = localTransaction.StockTransactionID;
                        entities.SaveChanges();

                        transIds.Add(localTransaction.StockTransactionID);
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("Box transaction created id:{0}", transaction.StockTransactionID));
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("No Products"))
                {
                    throw;
                }

                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transIds;
        }

        /// <summary>
        /// Checks the product lines for unfilled box transactions.
        /// </summary>
        /// <param name="details">The product lines to check.</param>
        /// <returns>A collection of unfilled product line boxes.</returns>
        public IList<SaleDetail> CheckBoxTransactions(IList<SaleDetail> details)
        {
            var halfBoxLines = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var detail in details)
                    {
                        var orderId = detail.SaleDetailID;
                        var productId = detail.INMasterID;

                        if (entities.StockTransactions.Any(
                            x => x.MasterTableID == orderId
                                 && x.INMasterID == productId
                                 && x.StockTransactionID_Container == null
                                 && x.Consumed == null
                                 && x.Deleted == null
                                 && x.IsBox != true
                                 && x.WarehouseID == NouvemGlobal.DispatchId
                                 && x.CanAddToBox != false))
                        {
                            halfBoxLines.Add(detail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return halfBoxLines;
        }

        /// <summary>
        /// Gets the dispatch box piece labels.
        /// </summary>
        /// <param name="serial">The serial number to search for.</param>
        /// <returns>The dispatch box piece labels correesponding to the dispatched box.</returns>
        public IList<GoodsIn> GetPieceLabels(int serial)
        {
            var pieceLabels = new List<GoodsIn>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var pieces =
                        entities.StockTransactions.Where(
                            x => x.Deleted == null && x.StockTransactionID_Container == serial);

                    foreach (var piece in pieces)
                    {
                        pieceLabels.Add(new GoodsIn
                        {
                            StockTransactionID = piece.StockTransactionID,
                            SerialNumber = piece.Serial,
                            NettWeight = piece.TransactionWeight.ToDouble(),
                            GrossWeight = piece.GrossWeight.ToDouble(),
                            TareWeight = piece.Tare.ToDouble(),
                            BatchNumber = piece.BatchNumber,
                            Price = piece.Price
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return pieceLabels;
        }

        /// <summary>
        /// Gets the ardispatch docket id relating to the input sale order id.
        /// </summary>
        /// <param name="arOrderId">The input sale order id.</param>
        /// <returns>The ardispatch docket id relating to the input sale order id.</returns>
        public int GetARDispatchIDForSaleOrder(int arOrderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dis = entities.ARDispatches.FirstOrDefault(x => x.BaseDocumentReferenceID == arOrderId && x.Deleted == null);
                    if (dis != null)
                    {
                        return dis.ARDispatchID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }


        /// <summary>
        /// Retrieves the dispatch containers.
        /// </summary>
        /// <param name="docStatusId">The doc status ofr the containers to retrieve.</param>
        /// <returns>A collection of dispatch containers.</returns>
        public IList<Model.BusinessObject.DispatchContainer> GetDispatchContainers(int docStatusId)
        {
            this.Log.LogDebug(this.GetType(), "GetDispatchContainers(): Retrieving..");
            var containers = new List<DispatchContainer>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (docStatusId == 0)
                    {
                        containers =
                            (from con in
                                 entities.DispatchContainers.Where(
                                     x => x.Deleted == null)
                             select new Model.BusinessObject.DispatchContainer
                             {
                                 DispatchContainerID = con.DispatchContainerID,
                                 CreationDate = con.CreationDate,
                                 Deleted = con.Deleted,
                                 DeviceMasterID = con.DeviceMasterID,
                                 UserMasterID = con.UserMasterID,
                                 Reference = con.Reference,
                                 NouDocStatusID = con.NouDocStatusID,
                                 Sales = (from order in entities.ARDispatches
                                          where
                                              order.Deleted == null &&
                                              order.DispatchContainerID == con.DispatchContainerID
                                          select new Sale
                                          {
                                              SaleID = order.ARDispatchID,
                                              DocumentNumberingID = order.DocumentNumberingID,
                                              Number = order.Number,
                                              Invoice =
                                                  entities.ARInvoices.FirstOrDefault(
                                                      x => x.BaseDocumentReferenceID == order.ARDispatchID),
                                              DispatchContainer = order.DispatchContainer,
                                              SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                              BPContactID = order.BPContactID,
                                              DocumentDate = order.DocumentDate,
                                              CreationDate = order.CreationDate,
                                              DispatchContainerID = order.DispatchContainerID,
                                              SaleType = ViewType.ARDispatch,
                                              DeviceId = order.DeviceID,
                                              EditDate = order.EditDate,
                                              RouteID = order.RouteID,
                                              PopUpNote = order.PopUpNote,
                                              DocketNote = order.DispatchNote1,
                                              CustomerPOReference = order.CustomerPOReference,
                                              BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                              Printed = order.Printed,
                                              IsBaseDocument = order.Invoiced,
                                              NouDocStatusID = order.NouDocStatusID,
                                              NouDocStatus = order.NouDocStatu.Value,
                                              Remarks = order.Remarks,
                                              TotalExVAT = order.TotalExVAT,
                                              DeliveryDate = order.DeliveryDate,
                                              ShippingDate = order.ShippingDate,
                                              VAT = order.VAT,
                                              SubTotalExVAT = order.SubTotalExVAT,
                                              DiscountIncVAT = order.DiscountIncVAT,
                                              GrandTotalIncVAT = order.GrandTotalIncVAT,
                                              DiscountPercentage = order.DiscountPercentage,
                                              SalesEmployeeID = order.UserMasterID,
                                              DeliveryAddress =
                                                  new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                              InvoiceAddress =
                                                  new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                              MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                              BPHaulier = order.BPMasterHaulier,
                                              BPCustomer = order.BPMasterCustomer,
                                              Deleted = order.Deleted
                                          }).OrderByDescending(x => x.NouDocStatusID).ThenByDescending(x => x.EditDate).ToList()
                             }).OrderByDescending(x => x.DispatchContainerID).ToList();
                    }
                    else
                    {
                        containers =
                       (from con in
                            entities.DispatchContainers.Where(x => x.NouDocStatusID == docStatusId && x.Deleted == null)
                        select new Model.BusinessObject.DispatchContainer
                        {
                            DispatchContainerID = con.DispatchContainerID,
                            CreationDate = con.CreationDate,
                            Deleted = con.Deleted,
                            DeviceMasterID = con.DeviceMasterID,
                            UserMasterID = con.UserMasterID,
                            Reference = con.Reference,
                            NouDocStatusID = con.NouDocStatusID,
                            Sales = (from order in entities.ARDispatches
                                     where order.Deleted == null && order.DispatchContainerID == con.DispatchContainerID
                                     select new Sale
                                     {
                                         SaleID = order.ARDispatchID,
                                         DocumentNumberingID = order.DocumentNumberingID,
                                         Number = order.Number,
                                         Invoice = entities.ARInvoices.FirstOrDefault(x => x.BaseDocumentReferenceID == order.ARDispatchID),
                                         DispatchContainer = order.DispatchContainer,
                                         SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                         BPContactID = order.BPContactID,
                                         DocumentDate = order.DocumentDate,
                                         CreationDate = order.CreationDate,
                                         DispatchContainerID = order.DispatchContainerID,
                                         SaleType = ViewType.ARDispatch,
                                         DeviceId = order.DeviceID,
                                         EditDate = order.EditDate,
                                         RouteID = order.RouteID,
                                         PopUpNote = order.PopUpNote,
                                         DocketNote = order.DispatchNote1,
                                         CustomerPOReference = order.CustomerPOReference,
                                         BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                         Printed = order.Printed,
                                         IsBaseDocument = order.Invoiced,
                                         NouDocStatusID = order.NouDocStatusID,
                                         NouDocStatus = order.NouDocStatu.Value,
                                         Remarks = order.Remarks,
                                         TotalExVAT = order.TotalExVAT,
                                         DeliveryDate = order.DeliveryDate,
                                         ShippingDate = order.ShippingDate,
                                         VAT = order.VAT,
                                         SubTotalExVAT = order.SubTotalExVAT,
                                         DiscountIncVAT = order.DiscountIncVAT,
                                         GrandTotalIncVAT = order.GrandTotalIncVAT,
                                         DiscountPercentage = order.DiscountPercentage,
                                         SalesEmployeeID = order.UserMasterID,
                                         DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                         InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                         MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                         BPHaulier = order.BPMasterHaulier,
                                         BPCustomer = order.BPMasterCustomer,
                                         Deleted = order.Deleted
                                     }).OrderByDescending(x => x.NouDocStatusID).ThenByDescending(x => x.EditDate).ToList()
                        }).OrderByDescending(x => x.DispatchContainerID).ToList();
                    }

                    var container = new DispatchContainer { Reference = Strings.NoContainer };
                    container.Sales = (from order in entities.ARDispatches
                                       where order.Deleted == null && order.DispatchContainerID == null
                                       select new Sale
                                       {
                                           SaleID = order.ARDispatchID,
                                           DocumentNumberingID = order.DocumentNumberingID,
                                           Number = order.Number,
                                           DispatchContainer = order.DispatchContainer,
                                           SaleOrderNumber = order.AROrder != null ? order.AROrder.Number : 0,
                                           BPContactID = order.BPContactID,
                                           DocumentDate = order.DocumentDate,
                                           CreationDate = order.CreationDate,
                                           DispatchContainerID = order.DispatchContainerID,
                                           SaleType = ViewType.ARDispatch,
                                           DeviceId = order.DeviceID,
                                           EditDate = order.EditDate,
                                           RouteID = order.RouteID,
                                           PopUpNote = order.PopUpNote,
                                           DocketNote = order.DispatchNote1,
                                           CustomerPOReference = order.CustomerPOReference,
                                           BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                           Printed = order.Printed,
                                           IsBaseDocument = order.Invoiced,
                                           NouDocStatusID = order.NouDocStatusID,
                                           NouDocStatus = order.NouDocStatu.Value,
                                           Remarks = order.Remarks,
                                           TotalExVAT = order.TotalExVAT,
                                           DeliveryDate = order.DeliveryDate,
                                           ShippingDate = order.ShippingDate,
                                           VAT = order.VAT,
                                           SubTotalExVAT = order.SubTotalExVAT,
                                           DiscountIncVAT = order.DiscountIncVAT,
                                           GrandTotalIncVAT = order.GrandTotalIncVAT,
                                           DiscountPercentage = order.DiscountPercentage,
                                           SalesEmployeeID = order.UserMasterID,
                                           DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                           InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                           MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                           BPHaulier = order.BPMasterHaulier,
                                           BPCustomer = order.BPMasterCustomer,
                                           Deleted = order.Deleted
                                       }).OrderByDescending(x => x.DispatchContainerID).ToList();

                    containers.Add(container);
                    this.Log.LogDebug(this.GetType(), string.Format("{0} dispatch containers retrieved.", containers.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return containers;
        }

        /// <summary>
        /// Gets the dispatch data for the input dispatches.
        /// </summary>
        /// <param name="dispatches"></param>
        /// <returns></returns>
        public IList<App_CarcassDispatch_Result> GetCarcassDispatchData(string dispatches)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_CarcassDispatch(dispatches).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds a dispatch container.
        /// </summary>
        /// <param name="container">The container to add.</param>
        /// <returns>A flag, indicating a successful addition or not.</returns>
        public bool AddDispatchContainer(Model.DataLayer.DispatchContainer container)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.DispatchContainers.Add(container);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates dispatch containers.
        /// </summary>
        /// <param name="containers">The containers to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDispatchContainer(IList<Model.BusinessObject.DispatchContainer> containers)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var dispatchContainer in containers)
                    {
                        var dbContainer =
                            entities.DispatchContainers.FirstOrDefault(
                                x => x.DispatchContainerID == dispatchContainer.DispatchContainerID);
                        if (dbContainer != null)
                        {
                            dbContainer.NouDocStatusID = dispatchContainer.NouDocStatusID;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates dispatch containers.
        /// </summary>
        /// <param name="containerId">The container to search the dispatches for.</param>
        /// <returns>A date the container is due to go out on.</returns>
        public DateTime? GetDispatchContainerDeliveryDate(int containerId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dispatch =
                        entities.ARDispatches.FirstOrDefault(
                            x => x.DispatchContainerID == containerId && x.Deleted == null);

                    if (dispatch != null)
                    {
                        return dispatch.ShippingDate;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets a sale order line product data.
        /// </summary>
        /// <param name="productId">The product to retrieve data for.</param>
        /// <returns>A summary of the products stock and order amount.</returns>
        public Tuple<string,decimal,decimal,decimal,decimal> GetProductStockAndOrderData(int productId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var onOrderWgt = new System.Data.Entity.Core.Objects.ObjectParameter("WgtOnOrder", typeof(decimal));
                    var onOrderQty = new System.Data.Entity.Core.Objects.ObjectParameter("QtyOnOrder", typeof(decimal));
                    var stockWgt = new System.Data.Entity.Core.Objects.ObjectParameter("WgtInStock", typeof(decimal));
                    var stockQty = new System.Data.Entity.Core.Objects.ObjectParameter("QtyInStock", typeof(decimal));
                    var summary = new System.Data.Entity.Core.Objects.ObjectParameter("Summary", typeof(string));
                    entities.App_ProductStockData(productId, onOrderWgt, onOrderQty, stockWgt, stockQty, summary);

                    return Tuple.Create(summary.Value.ToString(), onOrderWgt.Value.ToDecimal(),onOrderQty.Value.ToDecimal(),stockWgt.Value.ToDecimal(),stockQty.Value.ToDecimal());
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }


        /// <summary>
        /// Gets an order transactions.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>The order transactions.</returns>
        public IList<StockDetail> GetTransactions(int orderId)
        {
            var data = new List<StockDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    data = (from dispatch in entities.ARDispatchDetails
                        join stock in entities.StockTransactions on dispatch.ARDispatchDetailID equals stock.MasterTableID
                        join order in entities.PROrders
                            on stock.BatchNumberID equals order.BatchNumberID into orders
                        from prOrder in orders.DefaultIfEmpty()
                        where dispatch.ARDispatchID == orderId && stock.Deleted == null && stock.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsDeliveryId
                        select new StockDetail
                        {
                            StockTransactionID = stock.StockTransactionID,
                            Serial = stock.Serial,
                            TransactionWeight = stock.TransactionWeight,
                            TransactionQty = stock.TransactionQTY,
                            TransactionDate = stock.TransactionDate,
                            ProductID = stock.INMasterID,
                            Notes = prOrder.Reference ?? string.Empty
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Gets the quick order view data.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IList<App_GetQuickOrderViewData_Result> GetQuickOrderViewData(string mode)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetQuickOrderViewData(mode).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the quick order stock view data.
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IList<App_GetQuickOrderStockViewData_Result> GetQuickOrderStockViewData(string type,string stockType,int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetQuickOrderStockViewData(type,stockType,id).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds or updates a quick order.
        /// </summary>
        /// <param name="dispatchId">The dispatch id to update.</param>
        /// <param name="bpMasterId">The partner id.</param>
        /// <param name="stocktransactionId">The stock item to add.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>The dispatch id if newly created, and error message if one.</returns>
        public Tuple<string,int> AddOrUpdateQuickOrder(int dispatchId, int bpMasterId, int stocktransactionId, int userId, int deviceId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var newDispatchId = new System.Data.Entity.Core.Objects.ObjectParameter("NewDispatchID", typeof(int));
                    entities.App_AddOrUpdateQuickOrder(dispatchId, bpMasterId,stocktransactionId,userId,deviceId,newDispatchId);
                    return Tuple.Create(string.Empty, newDispatchId.Value.ToInt());
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return Tuple.Create(ex.Message,0);
            }
        }

        #endregion

        #region invoice

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddNewInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (!sale.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        var alreadyInvoiced = entities.ARInvoices.Any(x =>
                            x.BaseDocumentReferenceID == sale.BaseDocumentReferenceID && x.Deleted == null &&
                            x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID);

                        if (alreadyInvoiced)
                        {
                            return Tuple.Create(0, new DocNumber { DocumentNumberingTypeID = 1000 });
                        }
                    }

                    var dbNumber =
                        entities.DocumentNumberings.First(
                            x =>
                                x.NouDocumentName.DocumentName.Equals(Constant.ARInvoice) &&
                                x.DocumentNumberingType.Name.Equals(Constant.Standard));

                    sale.Number = dbNumber.NextNumber;
                    sale.DocumentNumberingID = dbNumber.DocumentNumberingID;

                    var currentNo = dbNumber.NextNumber;
                    var nextNo = dbNumber.NextNumber + 1;
                    if (nextNo > dbNumber.LastNumber)
                    {
                        // Last number reached, so reset back to first number.
                        nextNo = dbNumber.FirstNumber;
                    }

                    dbNumber.NextNumber = nextNo;
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document number updated");

                    var order = new ARInvoice
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null && sale.MainContact.Details != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        BPContactID_Delivery = sale.DeliveryContact != null && sale.DeliveryContact.Details != null ? (int?)sale.DeliveryContact.Details.BPContactID : null,
                        BPMasterID_Customer = sale.BPCustomer != null ? sale.BPCustomer.BPMasterID : sale.Customer != null ? sale.Customer.BPMasterID : (int?)null,
                        BPMasterID_Haulier = sale.BPHaulier != null ? sale.BPHaulier.BPMasterID : (int?)null,
                        BPAddressID_Delivery = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.BPAddressID : (int?)null,
                        BPAddressID_Invoice = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.BPAddressID : (int?)null,
                        RouteID = sale.RouteID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        DeliveryDate = sale.DeliveryDate,
                        ShippingDate = sale.ShippingDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                        CustomerPOReference = sale.CustomerPOReference,
                        DispatchContainerID = sale.DispatchContainerID,
                        Remarks = sale.Remarks,
                        InvoiceNote = sale.InvoiceNote,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        PORequired = sale.PORequired
                    };

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            if (baseDoc.Invoiced == true)
                            {
                                // already invoiced, return;
                                return Tuple.Create(0, new DocNumber());
                            }

                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            baseDoc.Invoiced = true;
                            this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }
                    else if (sale.IdsToMarkAsInvoiced != null && sale.IdsToMarkAsInvoiced.Any())
                    {
                        // Consolidated invoice created, so we mark all the associated dispatch dockets as invoiced.
                        //Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                        //{
                            //using (var invoiceEntities = new NouvemEntities())
                            //{
                                var dispatchesToInvoice =
                                    entities.ARDispatches.Where(
                                        x => sale.IdsToMarkAsInvoiced.Contains(x.ARDispatchID) && x.Invoiced != true);

                                foreach (var docket in dispatchesToInvoice)
                                {
                                    docket.Invoiced = true;
                                    docket.ARInvoiceID = order.ARInvoiceID;
                                }

                                //entities.SaveChanges();
                            //}
                       // }), DispatcherPriority.Background);
                    }

                    entities.ARInvoices.Add(order);
                    entities.SaveChanges();
                   
                    var docNumber =
                    new DocNumber
                    {
                        DocumentNumberingID = dbNumber.DocumentNumberingID,
                        NouDocumentNameID = dbNumber.NouDocumentNameID,
                        DocumentNumberingTypeID = dbNumber.DocumentNumberingTypeID,
                        FirstNumber = dbNumber.FirstNumber,
                        LastNumber = dbNumber.LastNumber,
                        CurrentNumber = currentNo,
                        NextNumber = dbNumber.NextNumber,
                        DocName = dbNumber.NouDocumentName,
                        DocType = dbNumber.DocumentNumberingType
                    };

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var orderAttachment = new ARInvoiceAttachment
                        {
                            ARInvoiceID = order.ARInvoiceID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.ARInvoiceAttachments.Add(orderAttachment);
                    }

                    // save the details
                    var saleID = order.ARInvoiceID;
                    var localInvoiceDetails = new List<ARInvoiceDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        var invoiceDetail = new ARInvoiceDetail
                        {
                            ARInvoiceID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            QuantityDelivered = detail.QuantityDelivered,
                            WeightDelivered = detail.WeightDelivered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            DeductionRate = detail.DeductionRate,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterID = detail.INMasterID
                        };

                        entities.ARInvoiceDetails.Add(invoiceDetail);
                        entities.SaveChanges();
                    }

                    this.Log.LogDebug(this.GetType(), "New invoice successfully created");
                    return Tuple.Create(order.ARInvoiceID, docNumber);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, new DocNumber());
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbInvoice = entities.ARInvoices.FirstOrDefault(x => x.ARInvoiceID == sale.SaleID && x.Deleted == null);

                    if (dbInvoice != null)
                    {
                        dbInvoice.DeviceID = NouvemGlobal.DeviceId;
                        dbInvoice.EditDate = DateTime.Now;
                        
                        if (sale.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Cancelling invoice...Base Doc Id:{0}", dbInvoice.BaseDocumentReferenceID.ToInt()));

                            // cancelling invoice
                            dbInvoice.NouDocStatusID = sale.NouDocStatusID;

                            if (dbInvoice.BaseDocumentReferenceID == null || dbInvoice.BaseDocumentReferenceID == 0)
                            {
                                this.Log.LogDebug(this.GetType(), "Cancelling consolidated invoice..."); 

                                // invoice was created using multiple consolidated dispatch dockets.
                                var baseDispatches = dbInvoice.ARDispatches;

                                // roll back the dispatch dockets
                                foreach (var baseDispatch in baseDispatches)
                                {
                                    baseDispatch.Invoiced = null;
                                    baseDispatch.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
                                }
                            }
                            else
                            {
                                this.Log.LogDebug(this.GetType(), "Cancelling single invoice..."); 

                                // single dispath docket used for invoice.
                                var dispatch =
                                    entities.ARDispatches.FirstOrDefault(
                                        x => x.ARDispatchID == dbInvoice.BaseDocumentReferenceID);

                                if (dispatch != null)
                                {
                                    dispatch.Invoiced = null;
                                    //dispatch.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
                                }
                            }

                            entities.SaveChanges();
                            return true;
                        }

                        dbInvoice.BPMasterID_Customer = sale.Customer.BPMasterID;
                        dbInvoice.BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null;
                        dbInvoice.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbInvoice.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbInvoice.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbInvoice.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbInvoice.RouteID = sale.RouteID;
                        dbInvoice.BaseDocumentReferenceID = sale.BaseDocumentReferenceID;
                        dbInvoice.DocumentDate = sale.DocumentDate;
                        dbInvoice.CreationDate = sale.CreationDate;
                        dbInvoice.DeliveryDate = sale.DeliveryDate;
                        dbInvoice.ShippingDate = sale.ShippingDate;
                        dbInvoice.NouDocStatusID = sale.NouDocStatusID;
                        dbInvoice.Remarks = sale.Remarks;
                        dbInvoice.InvoiceNote = sale.InvoiceNote;
                        dbInvoice.TotalExVAT = sale.TotalExVAT;
                        dbInvoice.DispatchContainerID = sale.DispatchContainerID;
                        dbInvoice.DiscountPercentage = sale.DiscountPercentage;
                        dbInvoice.VAT = sale.VAT;
                        dbInvoice.DiscountIncVAT = sale.DiscountIncVAT;
                        dbInvoice.SubTotalExVAT = sale.SubTotalExVAT;
                        dbInvoice.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbInvoice.UserMasterID = sale.SalesEmployeeID;
                        dbInvoice.PORequired = sale.PORequired;
                        dbInvoice.Deleted = sale.Deleted;

                        // if we used a base document to create this, then we mark the base document as closed.
                        if (!dbInvoice.BaseDocumentReferenceID.IsNullOrZero())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", dbInvoice.BaseDocumentReferenceID));
                            var baseDoc = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == dbInvoice.BaseDocumentReferenceID);
                            if (baseDoc != null)
                            {
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                                baseDoc.Invoiced = true;
                                this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", dbInvoice.BaseDocumentReferenceID));
                            }
                        }
                        else if (dbInvoice.ARDispatches != null && dbInvoice.ARDispatches.Any())
                        {
                            // Consolidated invoice created, so we mark all the associated dispatch dockets as invoiced.
                            foreach (var docket in dbInvoice.ARDispatches)
                            {
                                docket.Invoiced = true;
                            }
                        }

                        foreach (var attachment in sale.Attachments)
                        {
                            var dbAttachment =
                                    entities.ARInvoiceAttachments.FirstOrDefault(
                                        x => x.ARInvoiceAttachmentID == attachment.SaleAttachmentID);

                            if (dbAttachment != null)
                            {
                                // updating an existing attachment
                                if (attachment.Deleted != null)
                                {
                                    entities.ARInvoiceAttachments.Remove(dbAttachment);
                                }
                                else
                                {
                                    dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                    dbAttachment.FileName = attachment.FileName;
                                    dbAttachment.File = attachment.File;
                                }
                            }
                            else
                            {
                                // adding a new attachment
                                var localAttachment = new ARInvoiceAttachment
                                {
                                    AttachmentDate = attachment.AttachmentDate,
                                    ARInvoiceID = sale.SaleID,
                                    FileName = attachment.FileName,
                                    File = attachment.File
                                };

                                entities.ARInvoiceAttachments.Add(localAttachment);
                            }
                        }

                        var localInvoiceDetails = new List<ARInvoiceDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            //this.Log.LogDebug(this.GetType(), string.Format(
                            //    "Discount Amount:{0} Discount Percentage:{1} Discount Price:{2} INMaster:{3} Line Dis Amount:{4} Line Dis Percentage:{5}" +
                            //    "Quantity Del:{6} Quantity ordered:{7} Sale:{8} TotalExVat:{9} TotalIncVat:{10} Unit Price:{11} Unit Price/Discount:{12}" +
                            //    "Vat:{13} Wgt Del:{14} Wgt Ordered:{15}",
                            //    detail.DiscountAmount, detail.DiscountPercentage, detail.DiscountPrice, detail.INMasterID, detail.LineDiscountAmount,
                            //    detail.LineDiscountPercentage, detail.QuantityDelivered, detail.QuantityOrdered, "n/a", detail.TotalExVAT, detail.TotalIncVAT,
                            //    detail.UnitPrice, detail.UnitPriceAfterDiscount, detail.VatCodeID, detail.WeightDelivered, detail.WeightOrdered));

                            var dbInvoiceDetail =
                                entities.ARInvoiceDetails.FirstOrDefault(
                                    x => x.ARInvoiceDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbInvoiceDetail != null)
                            {
                                // existing sale item
                                dbInvoiceDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbInvoiceDetail.WeightOrdered = detail.WeightOrdered;
                                dbInvoiceDetail.QuantityDelivered = detail.QuantityDelivered;
                                dbInvoiceDetail.WeightDelivered = detail.WeightDelivered;
                                dbInvoiceDetail.UnitPrice = detail.UnitPrice;
                                dbInvoiceDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbInvoiceDetail.DiscountAmount = detail.DiscountAmount;
                                dbInvoiceDetail.DiscountPrice = detail.DiscountPrice;
                                dbInvoiceDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbInvoiceDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbInvoiceDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbInvoiceDetail.VAT = detail.VAT;
                                dbInvoiceDetail.VATCodeID = detail.VatCodeID;
                                dbInvoiceDetail.TotalExclVAT = detail.TotalExVAT;
                                dbInvoiceDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbInvoiceDetail.INMasterID = detail.INMasterID;
                                dbInvoiceDetail.DeductionRate = detail.DeductionRate;
                                dbInvoiceDetail.PriceListID = detail.PriceListID == 0 ? (int?) null : detail.PriceListID;
                                dbInvoiceDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order
                                var invoiceDetail = new ARInvoiceDetail
                                {
                                    ARInvoiceID = sale.SaleID,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    DeductionRate = detail.DeductionRate,
                                    VAT = detail.VAT,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?) null : detail.PriceListID,
                                    INMasterID = detail.INMasterID
                                };

                                localInvoiceDetails.Add(invoiceDetail);
                            }
                        }

                        if (localInvoiceDetails.Any())
                        {
                            entities.ARInvoiceDetails.AddRange(localInvoiceDetails);
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Invoice successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes an invoice detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveArInvoiceDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveArInvoiceDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.ARInvoiceDetails.FirstOrDefault(x => x.ARInvoiceDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Removes an invoice detail.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportInvoices(IList<int> invoiceIds)
        {
            this.Log.LogDebug(this.GetType(), "ExportInvoices(): Exporting invoices..");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoiceId in invoiceIds)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to export invoice id:{0}", invoiceId));
                        var invoice = entities.ARInvoices.FirstOrDefault(x => x.ARInvoiceID == invoiceId);
                        if (invoice != null)
                        {
                            invoice.NouDocStatusID = NouvemGlobal.NouDocStatusExported.NouDocStatusID;
                            invoice.EditDate = DateTime.Now;
                            this.Log.LogDebug(this.GetType(), "Invoice found and marked for export");
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), "Invoice not found");
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Invoices exported");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
      
            return false;
        }


        /// <summary>
        /// Marks invoices as printed.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to mark.</param>
        /// <returns>Flag, as to whether the invoices were marked as printed, or not.</returns>
        public bool CompleteDispatchContainer(IList<int> invoiceIds, int containerId, bool complete)
        {
            this.Log.LogDebug(this.GetType(), "Marking Invoices printed():..");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoiceId in invoiceIds)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark as printed invoice id:{0}", invoiceId));
                        var invoice = entities.ARInvoices.FirstOrDefault(x => x.ARInvoiceID == invoiceId);
                        if (invoice != null)
                        {
                            invoice.Printed = complete;
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), "Invoice not found");
                        }
                    }


                    var container = entities.DispatchContainers.FirstOrDefault(x => x.DispatchContainerID == containerId);
                    if (container != null)
                    {
                        if (complete)
                        {
                            container.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                        }
                        else
                        {
                            container.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Invoices exported");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Marks invoices as printed.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to mark.</param>
        /// <returns>Flag, as to whether the invoices were marked as printed, or not.</returns>
        public bool MarkInvoicesPrinted(IList<int> invoiceIds)
        {
            this.Log.LogDebug(this.GetType(), "Marking Invoices printed():..");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoiceId in invoiceIds)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark as printed invoice id:{0}", invoiceId));
                        var invoice = entities.ARInvoices.FirstOrDefault(x => x.ARInvoiceID == invoiceId);
                        if (invoice != null)
                        {
                            invoice.Printed = true;
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), "Invoice not found");
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Invoices exported");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non exported invoices (and associated invoice details).
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetNonExportedInvoices()
        {
            this.Log.LogDebug(this.GetType(), "GetNonExportedInvoices(): Attempting to get the non exported invoices");
            var invoices = new List<Sale>();
            //invoice.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID
            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                                where 
                                invoice.Deleted == null
                                orderby invoice.ARInvoiceID descending 
                                select new Sale
                                {
                                    SaleID = invoice.ARInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    DispatchContainerID = invoice.DispatchContainerID,
                                    DispatchContainer = invoice.DispatchContainer,
                                    Number = invoice.Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    ShippingDate = invoice.ShippingDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPHaulier = invoice.BPMasterHaulier,
                                    BPCustomer = invoice.BPMasterCustomer,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted,
                                    Attachments = (from attachment in invoice.ARInvoiceAttachments
                                                   where attachment.Deleted == null
                                                   select new Attachment
                                                   {
                                                       AttachmentID = attachment.ARInvoiceAttachmentID,
                                                       SaleAttachmentID = attachment.ARInvoiceID,
                                                       AttachmentDate = attachment.AttachmentDate,
                                                       FilePath = attachment.FilePath,
                                                       FileName = attachment.FileName,
                                                       File = attachment.File,
                                                       Deleted = attachment.Deleted
                                                   }).ToList(),
                                    //SaleDetails = (from detail in invoice.ARInvoiceDetails
                                    //               where detail.Deleted == null
                                    //               select new SaleDetail
                                    //               {
                                    //                   SaleDetailID = detail.ARInvoiceDetailID,
                                    //                   SaleID = detail.ARInvoiceID,
                                    //                   QuantityDelivered = detail.QuantityDelivered,
                                    //                   WeightDelivered = detail.WeightDelivered,
                                    //                   UnitPrice = detail.UnitPrice,
                                    //                   DiscountPercentage = detail.DiscountPercentage,
                                    //                   DiscountPrice = detail.DiscountPrice,
                                    //                   LineDiscountPercentage = detail.LineDiscountPercentage,
                                    //                   LineDiscountAmount = detail.LineDiscountAmount,
                                    //                   UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    //                   VAT = detail.VAT,
                                    //                   TotalExVAT = detail.TotalExclVAT,
                                    //                   TotalIncVAT = detail.TotalIncVAT,
                                    //                   VatCodeID = detail.VATCodeID,
                                    //                   LoadingSale = true,
                                    //                   INMasterID = detail.INMasterID,
                                    //                   PriceListID = detail.PriceListID ?? 0,
                                    //                   NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    //                   Deleted = detail.Deleted
                                    //               }).ToList(),
                                }).ToList();

                    //foreach (var invoice in invoices)
                    //{
                    //    if (invoice.BaseDocumentReferenceID != null)
                    //    {
                    //        var baseDocNo =
                    //            entities.ARDispatches.FirstOrDefault(
                    //                x => x.ARDispatchID == invoice.BaseDocumentReferenceID);
                    //        if (baseDocNo != null)
                    //        {
                    //            invoice.BaseDocumentNumber = baseDocNo.Number;
                    //            if (baseDocNo.BaseDocumentReferenceID != null)
                    //            {
                    //                var saleOrder =
                    //                entities.AROrders.FirstOrDefault(
                    //                  x => x.AROrderID == baseDocNo.BaseDocumentReferenceID);
                    //                if (saleOrder != null)
                    //                {
                    //                    invoice.SaleOrderNumber = saleOrder.Number;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            this.Log.LogError(this.GetType(), string.Format("Base doc not found:{0}", invoice.BaseDocumentReferenceID));
                    //        }
                    //    }
                    //}
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves the non exported invoices (and associated invoice details).
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public void SetInvoiceBaseDocuments(IList<Sale> invoices)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoice in invoices)
                    {
                        if (invoice.BaseDocumentReferenceID != null)
                        {
                            var baseDocNo =
                                entities.ARDispatches.FirstOrDefault(
                                    x => x.ARDispatchID == invoice.BaseDocumentReferenceID);
                            if (baseDocNo != null)
                            {
                                invoice.BaseDocumentNumber = baseDocNo.Number;
                                if (baseDocNo.BaseDocumentReferenceID != null)
                                {
                                    var saleOrder =
                                    entities.AROrders.FirstOrDefault(
                                      x => x.AROrderID == baseDocNo.BaseDocumentReferenceID);
                                    if (saleOrder != null)
                                    {
                                        invoice.SaleOrderNumber = saleOrder.Number;
                                    }
                                }
                            }
                            else
                            {
                                this.Log.LogError(this.GetType(), string.Format("Base doc not found:{0}", invoice.BaseDocumentReferenceID));
                            }
                        }
                    }
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllInvoices(IList<int?> invoiceStatuses)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the invoices");
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                              where invoiceStatuses.Contains(invoice.NouDocStatusID)
                              && invoice.Deleted == null
                              orderby invoice.CreationDate
                              select new Sale
                              {
                                  SaleID = invoice.ARInvoiceID,
                                  DocumentNumberingID = invoice.DocumentNumberingID,
                                  DispatchContainerID = invoice.DispatchContainerID,
                                  DispatchContainer = invoice.DispatchContainer,
                                  CustomerPOReference = invoice.CustomerPOReference,
                                  Number = invoice.Number,
                                  BPContactID = invoice.BPContactID,
                                  RouteID = invoice.RouteID,
                                  DeviceId = invoice.DeviceID,
                                  EditDate = invoice.EditDate,
                                  BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                  DocumentDate = invoice.DocumentDate,
                                  CreationDate = invoice.CreationDate,
                                  DeliveryDate = invoice.DeliveryDate,
                                  ShippingDate = invoice.ShippingDate,
                                  Printed = invoice.Printed,
                                  NouDocStatusID = invoice.NouDocStatusID,
                                  NouDocStatus = invoice.NouDocStatu.Value,
                                  Remarks = invoice.Remarks,
                                  DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                  InvoiceNote = invoice.InvoiceNote,
                                  TotalExVAT = invoice.TotalExVAT,
                                  VAT = invoice.VAT,
                                  SubTotalExVAT = invoice.SubTotalExVAT,
                                  DiscountIncVAT = invoice.DiscountIncVAT,
                                  GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                  DiscountPercentage = invoice.DiscountPercentage,
                                  OtherReference = invoice.OtherReference,
                                  SalesEmployeeID = invoice.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                  BPHaulier = invoice.BPMasterHaulier,
                                  BPCustomer = invoice.BPMasterCustomer,
                                  PORequired = invoice.PORequired,
                                  Deleted = invoice.Deleted,
                                  //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                  //               where attachment.Deleted == null
                                  //               select new Attachment
                                  //               {
                                  //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                  //                   SaleAttachmentID = attachment.ARInvoiceID,
                                  //                   AttachmentDate = attachment.AttachmentDate,
                                  //                   FilePath = attachment.FilePath,
                                  //                   FileName = attachment.FileName,
                                  //                   File = attachment.File,
                                  //                   Deleted = attachment.Deleted
                                  //               }).ToList(),
                                  //SaleDetails = (from detail in invoice.ARInvoiceDetails
                                  //               where detail.Deleted == null
                                  //               select new SaleDetail
                                  //               {
                                  //                   SaleDetailID = detail.ARInvoiceDetailID,
                                  //                   SaleID = detail.ARInvoiceID,
                                  //                   QuantityDelivered = detail.QuantityDelivered,
                                  //                   WeightDelivered = detail.WeightDelivered,
                                  //                   UnitPrice = detail.UnitPrice,
                                  //                   DiscountPercentage = detail.DiscountPercentage,
                                  //                   DiscountPrice = detail.DiscountPrice,
                                  //                   LineDiscountPercentage = detail.LineDiscountPercentage,
                                  //                   LineDiscountAmount = detail.LineDiscountAmount,
                                  //                   UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                  //                   VAT = detail.VAT,
                                  //                   TotalExVAT = detail.TotalExclVAT,
                                  //                   TotalIncVAT = detail.TotalIncVAT,
                                  //                   VatCodeID = detail.VATCodeID,
                                  //                   LoadingSale = true,
                                  //                   INMasterID = detail.INMasterID,
                                  //                   PriceListID = detail.PriceListID ?? 0,
                                  //                   NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                  //                   Deleted = detail.Deleted
                                  //               }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end, string dateType)
        { 
            this.Log.LogDebug(this.GetType(), "Attempting to get all the invoices");
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (dateType.Equals(Strings.ShippingDate))
                    {
                        invoices = (from invoice in entities.ARInvoices
                                    where invoiceStatuses.Contains(invoice.NouDocStatusID)
                                    && invoice.Deleted == null
                                    && (invoice.ShippingDate >= start && invoice.ShippingDate <= end)
                                    orderby invoice.CreationDate
                                    select new Sale
                                    {
                                        SaleID = invoice.ARInvoiceID,
                                        DocumentNumberingID = invoice.DocumentNumberingID,
                                        DispatchContainerID = invoice.DispatchContainerID,
                                        DispatchContainer = invoice.DispatchContainer,
                                        CustomerPOReference = invoice.CustomerPOReference,
                                        Number = invoice.Number,
                                        InvoiceNumber = invoice.Number,
                                        DispatchNumber = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == invoice.BaseDocumentReferenceID && x.Deleted == null).Number,
                                        BPContactID = invoice.BPContactID,
                                        RouteID = invoice.RouteID,
                                        DeviceId = invoice.DeviceID,
                                        EditDate = invoice.EditDate,
                                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                        DocumentDate = invoice.DocumentDate,
                                        CreationDate = invoice.CreationDate,
                                        DeliveryDate = invoice.DeliveryDate,
                                        ShippingDate = invoice.ShippingDate,
                                        Printed = invoice.Printed,
                                        NouDocStatusID = invoice.NouDocStatusID,
                                        NouDocStatus = invoice.NouDocStatu.Value,
                                        Remarks = invoice.Remarks,
                                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                        InvoiceNote = invoice.InvoiceNote,
                                        TotalExVAT = invoice.TotalExVAT,
                                        VAT = invoice.VAT,
                                        SubTotalExVAT = invoice.SubTotalExVAT,
                                        DiscountIncVAT = invoice.DiscountIncVAT,
                                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                        DiscountPercentage = invoice.DiscountPercentage,
                                        OtherReference = invoice.OtherReference,
                                        SalesEmployeeID = invoice.UserMasterID,
                                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                        BPHaulier = invoice.BPMasterHaulier,
                                        BPCustomer = invoice.BPMasterCustomer,
                                        PORequired = invoice.PORequired,
                                        Deleted = invoice.Deleted,
                                        //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                        //               where attachment.Deleted == null
                                        //               select new Attachment
                                        //               {
                                        //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                        //                   SaleAttachmentID = attachment.ARInvoiceID,
                                        //                   AttachmentDate = attachment.AttachmentDate,
                                        //                   FilePath = attachment.FilePath,
                                        //                   FileName = attachment.FileName,
                                        //                   File = attachment.File,
                                        //                   Deleted = attachment.Deleted
                                        //               }).ToList(),
                                    }).ToList();
                    }
                    else if (dateType.Equals(Strings.DeliveryDate))
                    {
                        invoices = (from invoice in entities.ARInvoices
                                    where invoiceStatuses.Contains(invoice.NouDocStatusID)
                                    && invoice.Deleted == null
                                    && (invoice.DeliveryDate >= start && invoice.DeliveryDate <= end)
                                    orderby invoice.CreationDate
                                    select new Sale
                                    {
                                        SaleID = invoice.ARInvoiceID,
                                        DocumentNumberingID = invoice.DocumentNumberingID,
                                        DispatchContainerID = invoice.DispatchContainerID,
                                        DispatchContainer = invoice.DispatchContainer,
                                        CustomerPOReference = invoice.CustomerPOReference,
                                        Number = invoice.Number,
                                        InvoiceNumber = invoice.Number,
                                        DispatchNumber = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == invoice.BaseDocumentReferenceID && x.Deleted == null).Number,
                                        BPContactID = invoice.BPContactID,
                                        RouteID = invoice.RouteID,
                                        DeviceId = invoice.DeviceID,
                                        EditDate = invoice.EditDate,
                                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                        DocumentDate = invoice.DocumentDate,
                                        CreationDate = invoice.CreationDate,
                                        DeliveryDate = invoice.DeliveryDate,
                                        ShippingDate = invoice.ShippingDate,
                                        Printed = invoice.Printed,
                                        NouDocStatusID = invoice.NouDocStatusID,
                                        NouDocStatus = invoice.NouDocStatu.Value,
                                        Remarks = invoice.Remarks,
                                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                        InvoiceNote = invoice.InvoiceNote,
                                        TotalExVAT = invoice.TotalExVAT,
                                        VAT = invoice.VAT,
                                        SubTotalExVAT = invoice.SubTotalExVAT,
                                        DiscountIncVAT = invoice.DiscountIncVAT,
                                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                        DiscountPercentage = invoice.DiscountPercentage,
                                        OtherReference = invoice.OtherReference,
                                        SalesEmployeeID = invoice.UserMasterID,
                                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                        BPHaulier = invoice.BPMasterHaulier,
                                        BPCustomer = invoice.BPMasterCustomer,
                                        PORequired = invoice.PORequired,
                                        Deleted = invoice.Deleted,
                                        //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                        //               where attachment.Deleted == null
                                        //               select new Attachment
                                        //               {
                                        //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                        //                   SaleAttachmentID = attachment.ARInvoiceID,
                                        //                   AttachmentDate = attachment.AttachmentDate,
                                        //                   FilePath = attachment.FilePath,
                                        //                   FileName = attachment.FileName,
                                        //                   File = attachment.File,
                                        //                   Deleted = attachment.Deleted
                                        //               }).ToList(),
                                    }).ToList();
                    }
                    else
                    {
                        end = end.AddDays(1);
                        invoices = (from invoice in entities.ARInvoices
                                    where invoiceStatuses.Contains(invoice.NouDocStatusID)
                                    && invoice.Deleted == null
                                    && (invoice.DocumentDate>= start && invoice.DocumentDate <= end)
                                    orderby invoice.CreationDate
                                    select new Sale
                                    {
                                        SaleID = invoice.ARInvoiceID,
                                        DocumentNumberingID = invoice.DocumentNumberingID,
                                        DispatchContainerID = invoice.DispatchContainerID,
                                        DispatchContainer = invoice.DispatchContainer,
                                        CustomerPOReference = invoice.CustomerPOReference,
                                        Number = invoice.Number,
                                        InvoiceNumber = invoice.Number,
                                        DispatchNumber = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == invoice.BaseDocumentReferenceID && x.Deleted == null).Number,
                                        BPContactID = invoice.BPContactID,
                                        RouteID = invoice.RouteID,
                                        DeviceId = invoice.DeviceID,
                                        EditDate = invoice.EditDate,
                                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                        DocumentDate = invoice.DocumentDate,
                                        CreationDate = invoice.CreationDate,
                                        DeliveryDate = invoice.DeliveryDate,
                                        ShippingDate = invoice.ShippingDate,
                                        Printed = invoice.Printed,
                                        NouDocStatusID = invoice.NouDocStatusID,
                                        NouDocStatus = invoice.NouDocStatu.Value,
                                        Remarks = invoice.Remarks,
                                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                        InvoiceNote = invoice.InvoiceNote,
                                        TotalExVAT = invoice.TotalExVAT,
                                        VAT = invoice.VAT,
                                        SubTotalExVAT = invoice.SubTotalExVAT,
                                        DiscountIncVAT = invoice.DiscountIncVAT,
                                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                        DiscountPercentage = invoice.DiscountPercentage,
                                        OtherReference = invoice.OtherReference,
                                        SalesEmployeeID = invoice.UserMasterID,
                                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                        BPHaulier = invoice.BPMasterHaulier,
                                        BPCustomer = invoice.BPMasterCustomer,
                                        PORequired = invoice.PORequired,
                                        Deleted = invoice.Deleted,
                                        Attachments = (from attachment in invoice.ARInvoiceAttachments
                                                       where attachment.Deleted == null
                                                       select new Attachment
                                                       {
                                                           AttachmentID = attachment.ARInvoiceAttachmentID,
                                                           SaleAttachmentID = attachment.ARInvoiceID,
                                                           AttachmentDate = attachment.AttachmentDate,
                                                           FilePath = attachment.FilePath,
                                                           FileName = attachment.FileName,
                                                           File = attachment.File,
                                                           Deleted = attachment.Deleted
                                                       }).ToList(),
                                    }).ToList();
                    }
                   
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }


        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input customer.</returns>
        public IList<Sale> GetInvoices(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the invoices for customer id:{0}", customerId));
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                              where invoice.BPMasterID_Customer == customerId
                              && invoice.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                              && invoice.Deleted == null
                              select new Sale
                              {
                                  SaleID = invoice.ARInvoiceID,
                                  DocumentNumberingID = invoice.DocumentNumberingID,
                                  DispatchContainerID = invoice.DispatchContainerID,
                                  Number = invoice.Number,
                                  BPContactID = invoice.BPContactID,
                                  RouteID = invoice.RouteID,
                                  DeviceId = invoice.DeviceID,
                                  EditDate = invoice.EditDate,
                                  BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                  DocumentDate = invoice.DocumentDate,
                                  CreationDate = invoice.CreationDate,
                                  DeliveryDate = invoice.DeliveryDate,
                                  ShippingDate = invoice.ShippingDate,
                                  Printed = invoice.Printed,
                                  NouDocStatusID = invoice.NouDocStatusID,
                                  NouDocStatus = invoice.NouDocStatu.Value,
                                  Remarks = invoice.Remarks,
                                  DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                  InvoiceNote = invoice.InvoiceNote,
                                  TotalExVAT = invoice.TotalExVAT,
                                  VAT = invoice.VAT,
                                  SubTotalExVAT = invoice.SubTotalExVAT,
                                  DiscountIncVAT = invoice.DiscountIncVAT,
                                  GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                  DiscountPercentage = invoice.DiscountPercentage,
                                  OtherReference = invoice.OtherReference,
                                  SalesEmployeeID = invoice.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                  BPHaulier = invoice.BPMasterHaulier,
                                  BPCustomer = invoice.BPMasterCustomer,
                                  PORequired = invoice.PORequired,
                                  Deleted = invoice.Deleted,
                                  SaleDetails = (from detail in invoice.ARInvoiceDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARInvoiceDetailID,
                                                     SaleID = detail.ARInvoiceID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                    
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var receipt in invoices)
            {
                receipt.Attachments = new List<Attachment>();
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input id.</returns>
        public IList<Sale> GetInvoicesByPartner(int partnerId, IList<int?> invoiceStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the invoices for partner id:{0}", partnerId));
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                              where invoice.BPMasterID_Customer == partnerId
                              && invoiceStatuses.Contains(invoice.NouDocStatusID)
                              && invoice.Deleted == null
                              select new Sale
                              {
                                  SaleID = invoice.ARInvoiceID,
                                  DocumentNumberingID = invoice.DocumentNumberingID,
                                  DispatchContainerID = invoice.DispatchContainerID,
                                  Number = invoice.Number,
                                  BPContactID = invoice.BPContactID,
                                  RouteID = invoice.RouteID,
                                  DeviceId = invoice.DeviceID,
                                  EditDate = invoice.EditDate,
                                  BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                  DocumentDate = invoice.DocumentDate,
                                  CreationDate = invoice.CreationDate,
                                  DeliveryDate = invoice.DeliveryDate,
                                  ShippingDate = invoice.ShippingDate,
                                  Printed = invoice.Printed,
                                  NouDocStatusID = invoice.NouDocStatusID,
                                  NouDocStatus = invoice.NouDocStatu.Value,
                                  Remarks = invoice.Remarks,
                                  DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                  InvoiceNote = invoice.InvoiceNote,
                                  TotalExVAT = invoice.TotalExVAT,
                                  VAT = invoice.VAT,
                                  SubTotalExVAT = invoice.SubTotalExVAT,
                                  DiscountIncVAT = invoice.DiscountIncVAT,
                                  GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                  DiscountPercentage = invoice.DiscountPercentage,
                                  OtherReference = invoice.OtherReference,
                                  SalesEmployeeID = invoice.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                  BPHaulier = invoice.BPMasterHaulier,
                                  BPCustomer = invoice.BPMasterCustomer,
                                  PORequired = invoice.PORequired,
                                  Deleted = invoice.Deleted,
                                  //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                  //               where attachment.Deleted == null
                                  //               select new Attachment
                                  //               {
                                  //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                  //                   SaleAttachmentID = attachment.ARInvoiceID,
                                  //                   AttachmentDate = attachment.AttachmentDate,
                                  //                   FilePath = attachment.FilePath,
                                  //                   FileName = attachment.FileName,
                                  //                   File = attachment.File,
                                  //                   Deleted = attachment.Deleted
                                  //               }).ToList(),
                                  SaleDetails = (from detail in invoice.ARInvoiceDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARInvoiceDetailID,
                                                     SaleID = detail.ARInvoiceID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                 
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves the invoice (and associated invoice details) for the input invoice id.
        /// </summary>
        /// <param name="invoiceId">The invoice id.</param>
        /// <returns>An invoice (and associated invoice details) for the input id.</returns>
        public Sale GetInvoicesById(int invoiceId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the invoices for partner id:{0}", invoiceId));
            Sale dbInvoice = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoice = entities.ARInvoices.FirstOrDefault(x => x.ARInvoiceID == invoiceId);

                    if (invoice == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Invoice not found");
                        return null;
                    }
                                dbInvoice = new Sale
                                {
                                    SaleID = invoice.ARInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    DispatchContainerID = invoice.DispatchContainerID,
                                    Number = invoice.Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    ShippingDate = invoice.ShippingDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPHaulier = invoice.BPMasterHaulier,
                                    BPCustomer = invoice.BPMasterCustomer,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted,
                                    //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                    //               where attachment.Deleted == null
                                    //               select new Attachment
                                    //               {
                                    //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                    //                   SaleAttachmentID = attachment.ARInvoiceID,
                                    //                   AttachmentDate = attachment.AttachmentDate,
                                    //                   FilePath = attachment.FilePath,
                                    //                   FileName = attachment.FileName,
                                    //                   File = attachment.File,
                                    //                   Deleted = attachment.Deleted
                                    //               }).ToList(),
                                    SaleDetails = (from detail in invoice.ARInvoiceDetails
                                                   where detail.Deleted == null
                                                   select new SaleDetail
                                                   {
                                                       LoadingSale = true,
                                                       SaleDetailID = detail.ARInvoiceDetailID,
                                                       SaleID = detail.ARInvoiceID,
                                                       QuantityOrdered = detail.QuantityOrdered,
                                                       WeightOrdered = detail.WeightOrdered,
                                                       QuantityDelivered = detail.QuantityDelivered,
                                                       WeightDelivered = detail.WeightDelivered,
                                                       UnitPrice = detail.UnitPrice,
                                                       DiscountPercentage = detail.DiscountPercentage,
                                                       DiscountPrice = detail.DiscountPrice,
                                                       LineDiscountPercentage = detail.LineDiscountPercentage,
                                                       LineDiscountAmount = detail.LineDiscountAmount,
                                                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                       VAT = detail.VAT,
                                                       TotalExVAT = detail.TotalExclVAT,
                                                       TotalIncVAT = detail.TotalIncVAT,
                                                       VatCodeID = detail.VATCodeID,
                                                 
                                                       INMasterID = detail.INMasterID,
                                                       PriceListID = detail.PriceListID ?? 0,
                                                       NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                       Deleted = detail.Deleted,
                                                       BPMasterID_Intake = detail.BPMasterID_Intake,
                                                       DeductionRate = detail.DeductionRate
                                                   }).ToList(),
                                };

                    if (dbInvoice.BaseDocumentReferenceID.HasValue)
                    {
                        var baseDoc =
                            entities.ARDispatches.FirstOrDefault(
                                x => x.ARDispatchID == dbInvoice.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            dbInvoice.BaseDocumentCreationDate = baseDoc.CreationDate;
                            dbInvoice.BaseDocumentNumber = baseDoc.Number;
                            dbInvoice.DocketNote = baseDoc.DispatchNote1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dbInvoice;
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer search term.</param>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of invoices (and associated invoice details) for the input search term.</returns>
        public IList<Sale> GetInvoicesByName(string searchTerm, IList<int?> invoiceStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the invoices for search term:{0}", searchTerm));
            var invoices = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                              where invoice.BPMasterCustomer.Name.ToLower().StartsWith(localSearchTerm)
                              && invoiceStatuses.Contains(invoice.NouDocStatusID)
                              && invoice.Deleted == null
                              select new Sale
                              {
                                  SaleID = invoice.ARInvoiceID,
                                  DocumentNumberingID = invoice.DocumentNumberingID,
                                  DispatchContainerID = invoice.DispatchContainerID,
                                  Number = invoice.Number,
                                  DeviceId = invoice.DeviceID,
                                  EditDate = invoice.EditDate,
                                  BPContactID = invoice.BPContactID,
                                  RouteID = invoice.RouteID,
                                  BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                  DocumentDate = invoice.DocumentDate,
                                  CreationDate = invoice.CreationDate,
                                  DeliveryDate = invoice.DeliveryDate,
                                  ShippingDate = invoice.ShippingDate,
                                  Printed = invoice.Printed,
                                  NouDocStatusID = invoice.NouDocStatusID,
                                  NouDocStatus = invoice.NouDocStatu.Value,
                                  Remarks = invoice.Remarks,
                                  DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                  InvoiceNote = invoice.InvoiceNote,
                                  TotalExVAT = invoice.TotalExVAT,
                                  VAT = invoice.VAT,
                                  SubTotalExVAT = invoice.SubTotalExVAT,
                                  DiscountIncVAT = invoice.DiscountIncVAT,
                                  GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                  DiscountPercentage = invoice.DiscountPercentage,
                                  OtherReference = invoice.OtherReference,
                                  SalesEmployeeID = invoice.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                  BPHaulier = invoice.BPMasterHaulier,
                                  BPCustomer = invoice.BPMasterCustomer,
                                  PORequired = invoice.PORequired,
                                  Deleted = invoice.Deleted,
                                  //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                  //               where attachment.Deleted == null
                                  //               select new Attachment
                                  //               {
                                  //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                  //                   SaleAttachmentID = attachment.ARInvoiceID,
                                  //                   AttachmentDate = attachment.AttachmentDate,
                                  //                   FilePath = attachment.FilePath,
                                  //                   FileName = attachment.FileName,
                                  //                   File = attachment.File,
                                  //                   Deleted = attachment.Deleted
                                  //               }).ToList(),
                                  SaleDetails = (from detail in invoice.ARInvoiceDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARInvoiceDetailID,
                                                     SaleID = detail.ARInvoiceID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the invoices");
            var invoices = new List<Sale>();
            end = end.AddDays(1);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                                where invoiceStatuses.Contains(invoice.NouDocStatusID)
                                && invoice.Deleted == null && invoice.CreationDate > start && invoice.CreationDate <= end
                                orderby invoice.CreationDate
                                select new Sale
                                {
                                    SaleID = invoice.ARInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    DispatchContainerID = invoice.DispatchContainerID,
                                    DispatchContainer = invoice.DispatchContainer,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    Number = invoice.Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    ShippingDate = invoice.ShippingDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPHaulier = invoice.BPMasterHaulier,
                                    BPCustomer = invoice.BPMasterCustomer,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted
                                }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetInvoiceByLastEdit()
        {
            Sale localInvoice = null;
 
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoice = entities.ARInvoices
                      .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (invoice == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Invoice not found");
                        return null;
                    }

                    localInvoice = new Sale
                                {
                                    SaleID = invoice.ARInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    DispatchContainerID = invoice.DispatchContainerID,
                                    Number = invoice.Number,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    ShippingDate = invoice.ShippingDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPHaulier = invoice.BPMasterHaulier,
                                    BPCustomer = invoice.BPMasterCustomer,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted,
                                    //Attachments = (from attachment in invoice.ARInvoiceAttachments
                                    //               where attachment.Deleted == null
                                    //               select new Attachment
                                    //               {
                                    //                   AttachmentID = attachment.ARInvoiceAttachmentID,
                                    //                   SaleAttachmentID = attachment.ARInvoiceID,
                                    //                   AttachmentDate = attachment.AttachmentDate,
                                    //                   FilePath = attachment.FilePath,
                                    //                   FileName = attachment.FileName,
                                    //                   File = attachment.File,
                                    //                   Deleted = attachment.Deleted
                                    //               }).ToList(),
                                    SaleDetails = (from detail in entities.ARInvoiceDetails
                                                   where detail.ARInvoiceID == invoice.ARInvoiceID && detail.Deleted == null
                                                   select new SaleDetail
                                                   {
                                                       LoadingSale = true,
                                                       SaleDetailID = detail.ARInvoiceDetailID,
                                                       SaleID = detail.ARInvoiceID,
                                                       QuantityOrdered = detail.QuantityOrdered,
                                                       WeightOrdered = detail.WeightOrdered,
                                                       QuantityDelivered = detail.QuantityDelivered,
                                                       WeightDelivered = detail.WeightDelivered,
                                                       UnitPrice = detail.UnitPrice,
                                                       DiscountPercentage = detail.DiscountPercentage,
                                                       DiscountPrice = detail.DiscountPrice,
                                                       LineDiscountPercentage = detail.LineDiscountPercentage,
                                                       LineDiscountAmount = detail.LineDiscountAmount,
                                                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                       VAT = detail.VAT,
                                                       TotalExVAT = detail.TotalExclVAT,
                                                       TotalIncVAT = detail.TotalIncVAT,
                                                       VatCodeID = detail.VATCodeID,
                                                       INMasterID = detail.INMasterID,
                                                       PriceListID = detail.PriceListID ?? 0,
                                                       NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                       Deleted = detail.Deleted,
                                                       BPMasterID_Intake = detail.BPMasterID_Intake,
                                                       DeductionRate = detail.DeductionRate
                                                      
                                                   }).ToList(),
                                };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localInvoice;
        }

        /// <summary>
        /// Retrieves the invoices (and associated invoice details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="invoiceStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetInvoicesByCode(string searchTerm, IList<int?> invoiceStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the invoices for search term:{0}", searchTerm));
            var invoices = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARInvoices
                              where invoice.BPMasterCustomer.Code.ToLower().StartsWith(localSearchTerm)
                              && invoiceStatuses.Contains(invoice.NouDocStatusID)
                              && invoice.Deleted == null
                              select new Sale
                              {
                                  SaleID = invoice.ARInvoiceID,
                                  DocumentNumberingID = invoice.DocumentNumberingID,
                                  DispatchContainerID = invoice.DispatchContainerID,
                                  Number = invoice.Number,
                                  BPContactID = invoice.BPContactID,
                                  DeviceId = invoice.DeviceID,
                                  EditDate = invoice.EditDate,
                                  RouteID = invoice.RouteID,
                                  BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                  DocumentDate = invoice.DocumentDate,
                                  CreationDate = invoice.CreationDate,
                                  DeliveryDate = invoice.DeliveryDate,
                                  ShippingDate = invoice.ShippingDate,
                                  Printed = invoice.Printed,
                                  NouDocStatusID = invoice.NouDocStatusID,
                                  NouDocStatus = invoice.NouDocStatu.Value,
                                  Remarks = invoice.Remarks,
                                  DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                  InvoiceNote = invoice.InvoiceNote,
                                  TotalExVAT = invoice.TotalExVAT,
                                  VAT = invoice.VAT,
                                  SubTotalExVAT = invoice.SubTotalExVAT,
                                  DiscountIncVAT = invoice.DiscountIncVAT,
                                  GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                  DiscountPercentage = invoice.DiscountPercentage,
                                  OtherReference = invoice.OtherReference,
                                  SalesEmployeeID = invoice.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContactDelivery },
                                  MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                  BPHaulier = invoice.BPMasterHaulier,
                                  BPCustomer = invoice.BPMasterCustomer,
                                  PORequired = invoice.PORequired,
                                  Deleted = invoice.Deleted,
                                  Attachments = (from attachment in invoice.ARInvoiceAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.ARInvoiceAttachmentID,
                                                     SaleAttachmentID = attachment.ARInvoiceID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in invoice.ARInvoiceDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.ARInvoiceDetailID,
                                                     SaleID = detail.ARInvoiceID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     DeductionRate = detail.DeductionRate,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        #endregion

        #region Telesales

        /// <summary>
        /// Gets the application call frequencies.
        /// </summary>
        /// <returns>The application call frequencies.</returns>
        public IList<NouCallFrequency> GetCallFrequencies()
        {
            var freq = new List<NouCallFrequency>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    freq = entities.NouCallFrequencies.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return freq;
        }

        /// <summary>
        /// Gets the telesale call instances.
        /// </summary>
        /// <returns>The acall instances.</returns>
        public IList<TelesaleCall> GetTelesaleCalls()
        {
            var calls = new List<TelesaleCall>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    calls = entities.TelesaleCalls.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return calls;
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesaleByFirstLast(bool first)
        {
            var sale = new Telesale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (first)
                    {
                        sale = entities.Telesales.FirstOrDefault();
                    }
                    else
                    {
                        sale = entities.Telesales.OrderByDescending(x => x.TelesalesID).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return sale;
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesaleByLastEdit()
        {
            var sale = new Telesale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    sale = entities.Telesales.OrderByDescending(x => x.EditDate).FirstOrDefault(x => x.DeviceMasterID == NouvemGlobal.DeviceId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return sale;
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesaleById(int id)
        {
            var sale = new Telesale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    sale = entities.Telesales.FirstOrDefault(x => x.TelesalesID == id);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return sale;
        }

        /// <summary>
        /// Gets the open telesale relating to the input partner.
        /// </summary>
        /// <returns>A telesale.</returns>
        public Telesale GetTelesale(int partnerId, int statusId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.Telesales.FirstOrDefault(x => x.BPMasterID == partnerId && x.Deleted == null && x.NouDocStatusID == statusId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

       /// <summary>
       /// Adds or updates a telesale.
       /// </summary>
       /// <param name="telesale">The telesale to add or update.</param>
       /// <returns>Flag, as to a successful add/update.</returns>
        public bool AddOrUpdateTelesale(Telesale telesale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (telesale.TelesalesID == 0)
                    {
                        entities.Telesales.Add(telesale);
                    }
                    else
                    {
                        var dbTelesale = entities.Telesales.FirstOrDefault(x => x.TelesalesID == telesale.TelesalesID);
                        if (dbTelesale == null)
                        {
                            return false;
                        }

                        dbTelesale.BPMasterID = telesale.BPMasterID;
                        dbTelesale.UserMasterID = telesale.UserMasterID;
                        dbTelesale.NouDocStatusID = telesale.NouDocStatusID;
                        dbTelesale.NouCallFrequencyID = telesale.NouCallFrequencyID;
                        dbTelesale.NextCallDate = telesale.NextCallDate;
                        dbTelesale.NextCallTime = telesale.NextCallTime;
                        dbTelesale.CallTimeMonday = telesale.CallTimeMonday;
                        dbTelesale.CallTimeTuesday = telesale.CallTimeTuesday;
                        dbTelesale.CallTimeWednesday = telesale.CallTimeWednesday;
                        dbTelesale.CallTimeThursday = telesale.CallTimeThursday;
                        dbTelesale.CallTimeFriday = telesale.CallTimeFriday;
                        dbTelesale.CallTimeSaturday = telesale.CallTimeSaturday;
                        dbTelesale.CallTimeSunday = telesale.CallTimeSunday;
                        dbTelesale.UserMasterID_CreatedBy = telesale.UserMasterID_CreatedBy;
                        dbTelesale.EditDate = telesale.EditDate;
                        dbTelesale.DeviceMasterID = telesale.DeviceMasterID;
                        dbTelesale.NouDocStatusID = telesale.NouDocStatusID;
                        dbTelesale.Remarks = telesale.Remarks;
                        dbTelesale.UserMasterID_Agent = telesale.UserMasterID_Agent;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a telesale call.
        /// </summary>
        /// <param name="telesale">The telesale to add or update.</param>
        /// <returns>Flag, as to a successful add/update.</returns>
        public bool AddTelesaleCall(TelesaleCall telesale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.TelesaleCalls.Add(telesale);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the telesales relating to the input agents.
        /// </summary>
        /// <returns>A telesales collection.</returns>
        public IList<Telesale> GetTelesales(IList<int?> agents, DateTime date)
        {
            var sales = new List<Telesale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    sales = entities.Telesales.Where(x => agents.Contains(x.UserMasterID)).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return sales;
        }

        /// <summary>
        /// Gets the telesales for searching.
        /// </summary>
        /// <returns>A telesales collection.</returns>
        public IList<Sale> GetSearchTelesales()
        {
            var sales = new List<Sale>();
            try
            {
                IList<Telesale> localSales;
                using (var entities = new NouvemEntities())
                {
                    localSales = entities.Telesales.Where(x => x.Deleted == null).ToList();
                }

                sales = (from sale in localSales
                         select new Sale
                         {
                             SaleID = sale.TelesalesID,
                             DeliveryDate = sale.NextCallDate,
                             DeliveryTime = sale.NextCallTime,
                             Customer = NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == sale.BPMasterID).Details,
                             UserIDCreation = sale.UserMasterID_CreatedBy,
                             UserIDCompletion = sale.UserMasterID,
                             EditDate = sale.EditDate,
                             NouDocStatusID = sale.NouDocStatusID
                         }).ToList();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return sales;
        }

        #endregion

        #region shared

        /// <summary>
        /// Gets the kill carcasses by kill date.
        /// </summary>
        /// <param name="start">The start kill date to serach from.</param>
        /// <param name="end">The end kill date to search to.</param>
        /// <returns>A collection of carcass records.</returns>
        public IList<Sale> GetCarcasses(DateTime start, DateTime end)
        {
            var carcasses = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    carcasses = (from carcass in entities.GetCarcasses(start, end)
                        select new Sale
                        {
                            Eartag = carcass.Eartag,
                            KillNumber = carcass.KillNumber,
                            Breed = carcass.Breed,
                            Herd = carcass.Herd,
                            Grade = carcass.Grade,
                            ColdWeight = carcass.EstimatedColdWeight,
                            DeliveryDate = carcass.KillDate,
                            CreationDate = carcass.DateOfBirth,
                            GroupDescription = carcass.GroupDescription,
                            GroupId = carcass.id
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return carcasses;
        }

        public void GetData()
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var data = entities.OrderAttachments.First(x => x.OrderAttachmentID == 3);
                    var localFile = data.File;
                    string xx = Encoding.ASCII.GetString(localFile);

                    // if the original encoding was UTF-8
                    string y = Encoding.UTF8.GetString(localFile);

                    var a = xx;
                    var aa = y;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Gets the sale order document trail.
        /// </summary>
        /// <param name="documentNo">The sale order document id.</param>
        /// <param name="mode">The mode.</param>
        /// <returns>A collection of master-detail sale orders.</returns>
        public IList<Sale> GetDocumentTrail(int documentNo, ViewType mode)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Getting the document trail for doc no:{0}, mode:{1}", documentNo, mode));
            IList<Sale> documents = new List<Sale>();
           
            try
            {
                using (var entities = new NouvemEntities())
                {
                    #region quote

                    if (mode == ViewType.Quote)
                    {
                        ProgressBar.SetUp(0, 4);
                        ProgressBar.Run(1);
                        var quote = entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == documentNo);
                        if (quote == null)
                        {
                            return documents;
                        }

                        var localQuote = new Sale
                        {
                            SaleID = quote.ARQuoteID,
                            DocumentNumberingID = quote.DocumentNumberingID,
                            Number = quote.Number,
                            BPContactID = quote.BPContactID,
                            RouteID = quote.RouteID,
                            QuoteValidDate = quote.QuoteValidDate,
                            DocumentDate = quote.DocumentDate,
                            CreationDate = quote.CreationDate,
                            Printed = quote.Printed,
                            NouDocStatusID = quote.NouDocStatusID,
                            NouDocStatus = quote.NouDocStatu.Value,
                            Remarks = quote.Remarks,
                            DocketNote = quote.DispatchNote1,
                            DispatchNote1PopUp = quote.DispatchNote1PopUp,
                            InvoiceNote = quote.InvoiceNote,
                            TotalExVAT = quote.TotalExVAT,
                            VAT = quote.VAT,
                            SubTotalExVAT = quote.SubTotalExVAT,
                            DiscountIncVAT = quote.DiscountIncVAT,
                            GrandTotalIncVAT = quote.GrandTotalIncVAT,
                            DiscountPercentage = quote.DiscountPercentage,
                            OtherReference = quote.OtherReference,
                            SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                            DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = quote.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                            BPHaulier = quote.BPMasterHaulier,
                            BPCustomer = quote.BPMasterCustomer,
                            PORequired = quote.PORequired,
                            Deleted = quote.Deleted,
                            NodeID = 1,
                            ParentID = 0,
                            DocumentTrailType = Constant.Quote,
                            Attachments = (from attachment in quote.QuoteAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.QuoteAttachmentID,
                                    SaleAttachmentID = attachment.ARQuoteID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in quote.ARQuoteDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARQuoteDetailID,
                                    SaleID = detail.ARQuoteID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    //QuantityDelivered = detail.QuantityDelivered,
                                    //WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        documents.Add(localQuote);

                        ProgressBar.Run();
                        var order = entities.AROrders.FirstOrDefault(x => x.BaseDocumentReferenceID == localQuote.SaleID);

                        if (order == null)
                        {
                            return documents;
                        }

                        var localOrder = new Sale
                        {
                            SaleID = order.AROrderID,
                            DocumentNumberingID = order.DocumentNumberingID,
                            Number = order.Number,
                            BPContactID = order.BPContactID,
                            RouteID = order.RouteID,
                            BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                            DocumentDate = order.DocumentDate,
                            CreationDate = order.CreationDate,
                            DeliveryDate = order.DeliveryDate,
                            ShippingDate = order.ShippingDate,
                            Printed = order.Printed,
                            NouDocStatusID = order.NouDocStatusID,
                            NouDocStatus = order.NouDocStatu.Value,
                            Remarks = order.Remarks,
                            DocketNote = order.DispatchNote1,
                            DispatchNote1PopUp = order.DispatchNote1PopUp,
                            InvoiceNote = order.InvoiceNote,
                            TotalExVAT = order.TotalExVAT,
                            VAT = order.VAT,
                            SubTotalExVAT = order.SubTotalExVAT,
                            DiscountIncVAT = order.DiscountIncVAT,
                            GrandTotalIncVAT = order.GrandTotalIncVAT,
                            DiscountPercentage = order.DiscountPercentage,
                            OtherReference = order.OtherReference,
                            SalesEmployeeID = order.UserMasterID_SalesEmployee,
                            DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = order.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = order.BPContact},
                            BPHaulier = order.BPMasterHaulier,
                            BPCustomer = order.BPMasterCustomer,
                            PORequired = order.PORequired,
                            Deleted = order.Deleted,
                            NodeID = 2,
                            ParentID = 0,
                            DocumentTrailType = Constant.Order,
                            Attachments = (from attachment in order.OrderAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.OrderAttachmentID,
                                    SaleAttachmentID = attachment.AROrderID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in order.AROrderDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.AROrderDetailID,
                                    SaleID = detail.AROrderID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    //QuantityDelivered = detail.QuantityDelivered,
                                    //WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        localQuote.ParentID = 2;
                        documents.Add(localOrder);

                        ProgressBar.Run();
                        var dispatch =
                            entities.ARDispatches.FirstOrDefault(x => x.BaseDocumentReferenceID == localOrder.SaleID);

                        if (dispatch == null)
                        {
                            return documents;
                        }

                        var localDispatch = new Sale
                        {
                            SaleID = dispatch.ARDispatchID,
                            DocumentNumberingID = dispatch.DocumentNumberingID,
                            Number = dispatch.Number,
                            BPContactID = dispatch.BPContactID,
                            DocumentDate = dispatch.DocumentDate,
                            CreationDate = dispatch.CreationDate,
                            SaleType = ViewType.ARDispatch,
                            RouteID = dispatch.RouteID,
                            CustomerPOReference = dispatch.CustomerPOReference,
                            BaseDocumentReferenceID = dispatch.BaseDocumentReferenceID,
                            Printed = dispatch.Printed,
                            NouDocStatusID = dispatch.NouDocStatusID,
                            NouDocStatus = dispatch.NouDocStatu.Value,
                            Remarks = dispatch.Remarks,
                            TotalExVAT = dispatch.TotalExVAT,
                            DeliveryDate = dispatch.DeliveryDate,
                            ShippingDate = dispatch.ShippingDate,
                            VAT = dispatch.VAT,
                            SubTotalExVAT = dispatch.SubTotalExVAT,
                            DiscountIncVAT = dispatch.DiscountIncVAT,
                            GrandTotalIncVAT = dispatch.GrandTotalIncVAT,
                            DiscountPercentage = dispatch.DiscountPercentage,
                            SalesEmployeeID = dispatch.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = dispatch.BPContact},
                            BPHaulier = dispatch.BPMasterHaulier,
                            BPCustomer = dispatch.BPMasterCustomer,
                            Deleted = dispatch.Deleted,
                            NodeID = 3,
                            ParentID = 0,
                            DocumentTrailType = Constant.Dispatch,
                            Attachments = (from attachment in dispatch.ARDispatchAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARDispatchAttachmentID,
                                    SaleAttachmentID = attachment.ARDispatchID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in dispatch.ARDispatchDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARDispatchDetailID,
                                    SaleID = detail.ARDispatchID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    SaleDetailType = ViewType.APReceipt,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        localOrder.ParentID = 3;
                        documents.Add(localDispatch);

                        ProgressBar.Run(1);
                        var invoice =
                            entities.ARInvoices.FirstOrDefault(x => x.BaseDocumentReferenceID == localDispatch.SaleID);

                        if (invoice == null)
                        {
                            return documents;
                        }

                        var localInvoice = new Sale
                        {
                            SaleID = invoice.ARInvoiceID,
                            DocumentNumberingID = invoice.DocumentNumberingID,
                            Number = invoice.Number,
                            BPContactID = invoice.BPContactID,
                            RouteID = invoice.RouteID,
                            BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                            DocumentDate = invoice.DocumentDate,
                            CreationDate = invoice.CreationDate,
                            DeliveryDate = invoice.DeliveryDate,
                            ShippingDate = invoice.ShippingDate,
                            Printed = invoice.Printed,
                            NouDocStatusID = invoice.NouDocStatusID,
                            NouDocStatus = invoice.NouDocStatu.Value,
                            Remarks = invoice.Remarks,
                            DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                            InvoiceNote = invoice.InvoiceNote,
                            TotalExVAT = invoice.TotalExVAT,
                            VAT = invoice.VAT,
                            SubTotalExVAT = invoice.SubTotalExVAT,
                            DiscountIncVAT = invoice.DiscountIncVAT,
                            GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                            DiscountPercentage = invoice.DiscountPercentage,
                            OtherReference = invoice.OtherReference,
                            SalesEmployeeID = invoice.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = invoice.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = invoice.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = invoice.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = invoice.BPContact},
                            BPHaulier = invoice.BPMasterHaulier,
                            BPCustomer = invoice.BPMasterCustomer,
                            PORequired = invoice.PORequired,
                            Deleted = invoice.Deleted,
                            NodeID = 4,
                            ParentID = 0,
                            DocumentTrailType = Constant.Invoice,
                            Attachments = (from attachment in invoice.ARInvoiceAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARInvoiceAttachmentID,
                                    SaleAttachmentID = attachment.ARInvoiceID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in invoice.ARInvoiceDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARInvoiceDetailID,
                                    SaleID = detail.ARInvoiceID,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList(),
                        };

                        localDispatch.ParentID = 4;
                        documents.Add(localInvoice);
                    }

                    #endregion

                    #region order

                    if (mode == ViewType.Order)
                    {
                        ProgressBar.SetUp(0,4);
                        ProgressBar.Run(1);
                        var order = entities.AROrders.FirstOrDefault(x => x.AROrderID == documentNo);

                        if (order == null)
                        {
                            return documents;
                        }

                        var localOrder = new Sale
                        {
                            SaleID = order.AROrderID,
                            DocumentNumberingID = order.DocumentNumberingID,
                            Number = order.Number,
                            BPContactID = order.BPContactID,
                            RouteID = order.RouteID,
                            BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                            DocumentDate = order.DocumentDate,
                            CreationDate = order.CreationDate,
                            DeliveryDate = order.DeliveryDate,
                            ShippingDate = order.ShippingDate,
                            Printed = order.Printed,
                            NouDocStatusID = order.NouDocStatusID,
                            NouDocStatus = order.NouDocStatu.Value,
                            Remarks = order.Remarks,
                            DocketNote = order.DispatchNote1,
                            DispatchNote1PopUp = order.DispatchNote1PopUp,
                            InvoiceNote = order.InvoiceNote,
                            TotalExVAT = order.TotalExVAT,
                            VAT = order.VAT,
                            SubTotalExVAT = order.SubTotalExVAT,
                            DiscountIncVAT = order.DiscountIncVAT,
                            GrandTotalIncVAT = order.GrandTotalIncVAT,
                            DiscountPercentage = order.DiscountPercentage,
                            OtherReference = order.OtherReference,
                            SalesEmployeeID = order.UserMasterID_SalesEmployee,
                            DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = order.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = order.BPContact},
                            BPHaulier = order.BPMasterHaulier,
                            BPCustomer = order.BPMasterCustomer,
                            PORequired = order.PORequired,
                            Deleted = order.Deleted,
                            NodeID = 2,
                            ParentID = 0,
                            DocumentTrailType = Constant.Order,
                            Attachments = (from attachment in order.OrderAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.OrderAttachmentID,
                                    SaleAttachmentID = attachment.AROrderID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in order.AROrderDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.AROrderDetailID,
                                    SaleID = detail.AROrderID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    //QuantityDelivered = detail.QuantityDelivered,
                                    //WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        documents.Add(localOrder);

                        ProgressBar.Run(1);
                        var quote =
                            entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == localOrder.BaseDocumentReferenceID);
                        Sale localQuote;
                        if (quote != null)
                        {
                            localQuote = new Sale
                            {
                                SaleID = quote.ARQuoteID,
                                DocumentNumberingID = quote.DocumentNumberingID,
                                Number = quote.Number,
                                BPContactID = quote.BPContactID,
                                RouteID = quote.RouteID,
                                QuoteValidDate = quote.QuoteValidDate,
                                DocumentDate = quote.DocumentDate,
                                CreationDate = quote.CreationDate,
                                Printed = quote.Printed,
                                NouDocStatusID = quote.NouDocStatusID,
                                NouDocStatus = quote.NouDocStatu.Value,
                                Remarks = quote.Remarks,
                                DocketNote = quote.DispatchNote1,
                                DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                InvoiceNote = quote.InvoiceNote,
                                TotalExVAT = quote.TotalExVAT,
                                VAT = quote.VAT,
                                SubTotalExVAT = quote.SubTotalExVAT,
                                DiscountIncVAT = quote.DiscountIncVAT,
                                GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                DiscountPercentage = quote.DiscountPercentage,
                                OtherReference = quote.OtherReference,
                                SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                                InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                                DeliveryContact = new BusinessPartnerContact {Details = quote.BPContactDelivery},
                                MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                                BPHaulier = quote.BPMasterHaulier,
                                BPCustomer = quote.BPMasterCustomer,
                                PORequired = quote.PORequired,
                                Deleted = quote.Deleted,
                                NodeID = 1,
                                ParentID = 2,
                                DocumentTrailType = Constant.Quote,
                                Attachments = (from attachment in quote.QuoteAttachments
                                               where attachment.Deleted == null
                                    select new Attachment
                                    {
                                        AttachmentID = attachment.QuoteAttachmentID,
                                        SaleAttachmentID = attachment.ARQuoteID,
                                        AttachmentDate = attachment.AttachmentDate,
                                        FilePath = attachment.FilePath,
                                        FileName = attachment.FileName,
                                        File = attachment.File,
                                        Deleted = attachment.Deleted
                                    }).ToList(),
                                SaleDetails = (from detail in quote.ARQuoteDetails
                                               where detail.Deleted == null
                                    select new SaleDetail
                                    {
                                        SaleDetailID = detail.ARQuoteDetailID,
                                        SaleID = detail.ARQuoteID,
                                        QuantityOrdered = detail.QuantityOrdered,
                                        WeightOrdered = detail.WeightOrdered,
                                        //QuantityDelivered = detail.QuantityDelivered,
                                        //WeightDelivered = detail.WeightDelivered,
                                        UnitPrice = detail.UnitPrice,
                                        DiscountPercentage = detail.DiscountPercentage,
                                        DiscountPrice = detail.DiscountPrice,
                                        LineDiscountPercentage = detail.LineDiscountPercentage,
                                        LineDiscountAmount = detail.LineDiscountAmount,
                                        UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                        VAT = detail.VAT,
                                        TotalExVAT = detail.TotalExclVAT,
                                        TotalIncVAT = detail.TotalIncVAT,
                                        VatCodeID = detail.VATCodeID,
                                        LoadingSale = true,
                                        INMasterID = detail.INMasterID,
                                        PriceListID = detail.PriceListID ?? 0,
                                        Deleted = detail.Deleted
                                    }).ToList()
                            };

                            documents.Add(localQuote);
                        }

                        ProgressBar.Run(1);
                        var dispatch =
                            entities.ARDispatches.FirstOrDefault(x => x.BaseDocumentReferenceID == localOrder.SaleID);

                        if (dispatch == null)
                        {
                            return documents;
                        }

                        var localDispatch = new Sale
                        {
                            SaleID = dispatch.ARDispatchID,
                            DocumentNumberingID = dispatch.DocumentNumberingID,
                            Number = dispatch.Number,
                            BPContactID = dispatch.BPContactID,
                            DocumentDate = dispatch.DocumentDate,
                            CreationDate = dispatch.CreationDate,
                            SaleType = ViewType.ARDispatch,
                            RouteID = dispatch.RouteID,
                            CustomerPOReference = dispatch.CustomerPOReference,
                            BaseDocumentReferenceID = dispatch.BaseDocumentReferenceID,
                            Printed = dispatch.Printed,
                            NouDocStatusID = dispatch.NouDocStatusID,
                            NouDocStatus = dispatch.NouDocStatu.Value,
                            Remarks = dispatch.Remarks,
                            TotalExVAT = dispatch.TotalExVAT,
                            DeliveryDate = dispatch.DeliveryDate,
                            ShippingDate = dispatch.ShippingDate,
                            VAT = dispatch.VAT,
                            SubTotalExVAT = dispatch.SubTotalExVAT,
                            DiscountIncVAT = dispatch.DiscountIncVAT,
                            GrandTotalIncVAT = dispatch.GrandTotalIncVAT,
                            DiscountPercentage = dispatch.DiscountPercentage,
                            SalesEmployeeID = dispatch.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = dispatch.BPContact},
                            BPHaulier = dispatch.BPMasterHaulier,
                            BPCustomer = dispatch.BPMasterCustomer,
                            Deleted = dispatch.Deleted,
                            NodeID = 3,
                            ParentID = 0,
                            DocumentTrailType = Constant.Dispatch,
                            Attachments = (from attachment in dispatch.ARDispatchAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARDispatchAttachmentID,
                                    SaleAttachmentID = attachment.ARDispatchID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in dispatch.ARDispatchDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARDispatchDetailID,
                                    SaleID = detail.ARDispatchID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    SaleDetailType = ViewType.APReceipt,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    BPMasterID_Intake = detail.BPMasterID_Intake,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        localOrder.ParentID = 3;
                        documents.Add(localDispatch);

                        ProgressBar.Run(1);
                        var invoice =
                            entities.ARInvoices.FirstOrDefault(x => x.BaseDocumentReferenceID == localDispatch.SaleID);

                        if (invoice == null)
                        {
                            return documents;
                        }

                        var localInvoice = new Sale
                        {
                            SaleID = invoice.ARInvoiceID,
                            DocumentNumberingID = invoice.DocumentNumberingID,
                            Number = invoice.Number,
                            BPContactID = invoice.BPContactID,
                            RouteID = invoice.RouteID,
                            BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                            DocumentDate = invoice.DocumentDate,
                            CreationDate = invoice.CreationDate,
                            DeliveryDate = invoice.DeliveryDate,
                            ShippingDate = invoice.ShippingDate,
                            Printed = invoice.Printed,
                            NouDocStatusID = invoice.NouDocStatusID,
                            NouDocStatus = invoice.NouDocStatu.Value,
                            Remarks = invoice.Remarks,
                            DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                            InvoiceNote = invoice.InvoiceNote,
                            TotalExVAT = invoice.TotalExVAT,
                            VAT = invoice.VAT,
                            SubTotalExVAT = invoice.SubTotalExVAT,
                            DiscountIncVAT = invoice.DiscountIncVAT,
                            GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                            DiscountPercentage = invoice.DiscountPercentage,
                            OtherReference = invoice.OtherReference,
                            SalesEmployeeID = invoice.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = invoice.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = invoice.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = invoice.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = invoice.BPContact},
                            BPHaulier = invoice.BPMasterHaulier,
                            BPCustomer = invoice.BPMasterCustomer,
                            PORequired = invoice.PORequired,
                            Deleted = invoice.Deleted,
                            NodeID = 4,
                            ParentID = 0,
                            DocumentTrailType = Constant.Invoice,
                            Attachments = (from attachment in invoice.ARInvoiceAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARInvoiceAttachmentID,
                                    SaleAttachmentID = attachment.ARInvoiceID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in invoice.ARInvoiceDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARInvoiceDetailID,
                                    SaleID = detail.ARInvoiceID,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList(),
                        };

                        localDispatch.ParentID = 4;
                        documents.Add(localInvoice);
                    }

                    #endregion

                    #region dispatch

                    if (mode == ViewType.ARDispatch)
                    {
                        ProgressBar.SetUp(0,4);
                        ProgressBar.Run(1);
                        var dispatch = entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == documentNo);

                        if (dispatch == null)
                        {
                            return documents;
                        }

                        var localDispatch = new Sale
                        {
                            SaleID = dispatch.ARDispatchID,
                            DocumentNumberingID = dispatch.DocumentNumberingID,
                            Number = dispatch.Number,
                            BPContactID = dispatch.BPContactID,
                            DocumentDate = dispatch.DocumentDate,
                            CreationDate = dispatch.CreationDate,
                            SaleType = ViewType.ARDispatch,
                            RouteID = dispatch.RouteID,
                            CustomerPOReference = dispatch.CustomerPOReference,
                            BaseDocumentReferenceID = dispatch.BaseDocumentReferenceID,
                            Printed = dispatch.Printed,
                            NouDocStatusID = dispatch.NouDocStatusID,
                            NouDocStatus = dispatch.NouDocStatu.Value,
                            Remarks = dispatch.Remarks,
                            TotalExVAT = dispatch.TotalExVAT,
                            DeliveryDate = dispatch.DeliveryDate,
                            ShippingDate = dispatch.ShippingDate,
                            VAT = dispatch.VAT,
                            SubTotalExVAT = dispatch.SubTotalExVAT,
                            DiscountIncVAT = dispatch.DiscountIncVAT,
                            GrandTotalIncVAT = dispatch.GrandTotalIncVAT,
                            DiscountPercentage = dispatch.DiscountPercentage,
                            SalesEmployeeID = dispatch.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = dispatch.BPContact},
                            BPHaulier = dispatch.BPMasterHaulier,
                            BPCustomer = dispatch.BPMasterCustomer,
                            Deleted = dispatch.Deleted,
                            NodeID = 3,
                            ParentID = 0,
                            DocumentTrailType = Constant.Dispatch,
                            Attachments = (from attachment in dispatch.ARDispatchAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARDispatchAttachmentID,
                                    SaleAttachmentID = attachment.ARDispatchID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in dispatch.ARDispatchDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARDispatchDetailID,
                                    SaleID = detail.ARDispatchID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    SaleDetailType = ViewType.APReceipt,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    BPMasterID_Intake = detail.BPMasterID_Intake,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        documents.Add(localDispatch);

                        ProgressBar.Run(1);
                        var order =
                            entities.AROrders.FirstOrDefault(x => x.AROrderID == localDispatch.BaseDocumentReferenceID);

                        Sale localOrder;
                        if (order != null)
                        {
                            localOrder = new Sale
                            {
                                SaleID = order.AROrderID,
                                DocumentNumberingID = order.DocumentNumberingID,
                                Number = order.Number,
                                BPContactID = order.BPContactID,
                                RouteID = order.RouteID,
                                BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                DocumentDate = order.DocumentDate,
                                CreationDate = order.CreationDate,
                                DeliveryDate = order.DeliveryDate,
                                ShippingDate = order.ShippingDate,
                                Printed = order.Printed,
                                NouDocStatusID = order.NouDocStatusID,
                                NouDocStatus = order.NouDocStatu.Value,
                                Remarks = order.Remarks,
                                DocketNote = order.DispatchNote1,
                                DispatchNote1PopUp = order.DispatchNote1PopUp,
                                InvoiceNote = order.InvoiceNote,
                                TotalExVAT = order.TotalExVAT,
                                VAT = order.VAT,
                                SubTotalExVAT = order.SubTotalExVAT,
                                DiscountIncVAT = order.DiscountIncVAT,
                                GrandTotalIncVAT = order.GrandTotalIncVAT,
                                DiscountPercentage = order.DiscountPercentage,
                                OtherReference = order.OtherReference,
                                SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                                InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                                DeliveryContact = new BusinessPartnerContact {Details = order.BPContactDelivery},
                                MainContact = new BusinessPartnerContact {Details = order.BPContact},
                                BPHaulier = order.BPMasterHaulier,
                                BPCustomer = order.BPMasterCustomer,
                                PORequired = order.PORequired,
                                Deleted = order.Deleted,
                                NodeID = 2,
                                ParentID = 3,
                                DocumentTrailType = Constant.Order,
                                Attachments = (from attachment in order.OrderAttachments
                                               where attachment.Deleted == null
                                    select new Attachment
                                    {
                                        AttachmentID = attachment.OrderAttachmentID,
                                        SaleAttachmentID = attachment.AROrderID,
                                        AttachmentDate = attachment.AttachmentDate,
                                        FilePath = attachment.FilePath,
                                        FileName = attachment.FileName,
                                        File = attachment.File,
                                        Deleted = attachment.Deleted
                                    }).ToList(),
                                SaleDetails = (from detail in order.AROrderDetails
                                               where detail.Deleted == null
                                    select new SaleDetail
                                    {
                                        SaleDetailID = detail.AROrderDetailID,
                                        SaleID = detail.AROrderID,
                                        QuantityOrdered = detail.QuantityOrdered,
                                        WeightOrdered = detail.WeightOrdered,
                                        //QuantityDelivered = detail.QuantityDelivered,
                                        //WeightDelivered = detail.WeightDelivered,
                                        UnitPrice = detail.UnitPrice,
                                        DiscountPercentage = detail.DiscountPercentage,
                                        DiscountPrice = detail.DiscountPrice,
                                        LineDiscountPercentage = detail.LineDiscountPercentage,
                                        LineDiscountAmount = detail.LineDiscountAmount,
                                        UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                        VAT = detail.VAT,
                                        TotalIncVAT = detail.TotalIncVAT,
                                        TotalExVAT = detail.TotalExclVAT,
                                        VatCodeID = detail.VATCodeID,
                                        LoadingSale = true,
                                        INMasterID = detail.INMasterID,
                                        PriceListID = detail.PriceListID ?? 0,
                                        NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                        Deleted = detail.Deleted
                                    }).ToList()
                            };

                            documents.Add(localOrder);

                            ProgressBar.Run(1);
                            var quote =
                                entities.ARQuotes.FirstOrDefault(x => x.ARQuoteID == localOrder.BaseDocumentReferenceID);
                            Sale localQuote;
                            if (quote != null)
                            {
                                localQuote = new Sale
                                {
                                    SaleID = quote.ARQuoteID,
                                    DocumentNumberingID = quote.DocumentNumberingID,
                                    Number = quote.Number,
                                    BPContactID = quote.BPContactID,
                                    RouteID = quote.RouteID,
                                    QuoteValidDate = quote.QuoteValidDate,
                                    DocumentDate = quote.DocumentDate,
                                    CreationDate = quote.CreationDate,
                                    Printed = quote.Printed,
                                    NouDocStatusID = quote.NouDocStatusID,
                                    NouDocStatus = quote.NouDocStatu.Value,
                                    Remarks = quote.Remarks,
                                    DocketNote = quote.DispatchNote1,
                                    DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                    InvoiceNote = quote.InvoiceNote,
                                    TotalExVAT = quote.TotalExVAT,
                                    VAT = quote.VAT,
                                    SubTotalExVAT = quote.SubTotalExVAT,
                                    DiscountIncVAT = quote.DiscountIncVAT,
                                    GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                    DiscountPercentage = quote.DiscountPercentage,
                                    OtherReference = quote.OtherReference,
                                    SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                    DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                                    InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                                    DeliveryContact = new BusinessPartnerContact {Details = quote.BPContactDelivery},
                                    MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                                    BPHaulier = quote.BPMasterHaulier,
                                    BPCustomer = quote.BPMasterCustomer,
                                    PORequired = quote.PORequired,
                                    Deleted = quote.Deleted,
                                    NodeID = 1,
                                    ParentID = 2,
                                    DocumentTrailType = Constant.Quote,
                                    Attachments = (from attachment in quote.QuoteAttachments
                                                   where attachment.Deleted == null
                                        select new Attachment
                                        {
                                            AttachmentID = attachment.QuoteAttachmentID,
                                            SaleAttachmentID = attachment.ARQuoteID,
                                            AttachmentDate = attachment.AttachmentDate,
                                            FilePath = attachment.FilePath,
                                            FileName = attachment.FileName,
                                            File = attachment.File,
                                            Deleted = attachment.Deleted
                                        }).ToList(),
                                    SaleDetails = (from detail in quote.ARQuoteDetails
                                                   where detail.Deleted == null
                                        select new SaleDetail
                                        {
                                            SaleDetailID = detail.ARQuoteDetailID,
                                            SaleID = detail.ARQuoteID,
                                            QuantityOrdered = detail.QuantityOrdered,
                                            WeightOrdered = detail.WeightOrdered,
                                            //QuantityDelivered = detail.QuantityDelivered,
                                            //WeightDelivered = detail.WeightDelivered,
                                            UnitPrice = detail.UnitPrice,
                                            DiscountPercentage = detail.DiscountPercentage,
                                            DiscountPrice = detail.DiscountPrice,
                                            LineDiscountPercentage = detail.LineDiscountPercentage,
                                            LineDiscountAmount = detail.LineDiscountAmount,
                                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                            VAT = detail.VAT,
                                            TotalExVAT = detail.TotalExclVAT,
                                            TotalIncVAT = detail.TotalIncVAT,
                                            VatCodeID = detail.VATCodeID,
                                            LoadingSale = true,
                                            INMasterID = detail.INMasterID,
                                            PriceListID = detail.PriceListID ?? 0,
                                            Deleted = detail.Deleted
                                        }).ToList()
                                };

                                documents.Add(localQuote);
                            }
                        }

                        ProgressBar.Run(1);
                        var invoice =
                            entities.ARInvoices.FirstOrDefault(x => x.BaseDocumentReferenceID == localDispatch.SaleID && x.NouDocStatusID != 2 && x.Deleted == null);

                        if (invoice == null)
                        {
                            return documents;
                        }

                        var localInvoice = new Sale
                        {
                            SaleID = invoice.ARInvoiceID,
                            DocumentNumberingID = invoice.DocumentNumberingID,
                            Number = invoice.Number,
                            BPContactID = invoice.BPContactID,
                            RouteID = invoice.RouteID,
                            BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                            DocumentDate = invoice.DocumentDate,
                            CreationDate = invoice.CreationDate,
                            DeliveryDate = invoice.DeliveryDate,
                            ShippingDate = invoice.ShippingDate,
                            Printed = invoice.Printed,
                            NouDocStatusID = invoice.NouDocStatusID,
                            NouDocStatus = invoice.NouDocStatu.Value,
                            Remarks = invoice.Remarks,
                            DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                            InvoiceNote = invoice.InvoiceNote,
                            TotalExVAT = invoice.TotalExVAT,
                            VAT = invoice.VAT,
                            SubTotalExVAT = invoice.SubTotalExVAT,
                            DiscountIncVAT = invoice.DiscountIncVAT,
                            GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                            DiscountPercentage = invoice.DiscountPercentage,
                            OtherReference = invoice.OtherReference,
                            SalesEmployeeID = invoice.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = invoice.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = invoice.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = invoice.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = invoice.BPContact},
                            BPHaulier = invoice.BPMasterHaulier,
                            BPCustomer = invoice.BPMasterCustomer,
                            PORequired = invoice.PORequired,
                            Deleted = invoice.Deleted,
                            NodeID = 4,
                            ParentID = 0,
                            DocumentTrailType = Constant.Invoice,
                            Attachments = (from attachment in invoice.ARInvoiceAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARInvoiceAttachmentID,
                                    SaleAttachmentID = attachment.ARInvoiceID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in invoice.ARInvoiceDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARInvoiceDetailID,
                                    SaleID = detail.ARInvoiceID,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    BPMasterID_Intake = detail.BPMasterID_Intake,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList(),
                        };

                        localDispatch.ParentID = 4;
                        documents.Add(localInvoice);
                    }

                    #endregion

                    #region invoice

                    if (mode == ViewType.Invoice)
                    {
                        var invoice = entities.ARInvoices.FirstOrDefault(x => x.ARInvoiceID == documentNo);

                        if (invoice == null)
                        {
                            return documents;
                        }

                        var localInvoice = new Sale
                        {
                            SaleID = invoice.ARInvoiceID,
                            DocumentNumberingID = invoice.DocumentNumberingID,
                            Number = invoice.Number,
                            BPContactID = invoice.BPContactID,
                            RouteID = invoice.RouteID,
                            BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                            DocumentDate = invoice.DocumentDate,
                            CreationDate = invoice.CreationDate,
                            DeliveryDate = invoice.DeliveryDate,
                            ShippingDate = invoice.ShippingDate,
                            Printed = invoice.Printed,
                            NouDocStatusID = invoice.NouDocStatusID,
                            NouDocStatus = invoice.NouDocStatu.Value,
                            Remarks = invoice.Remarks,
                            DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                            InvoiceNote = invoice.InvoiceNote,
                            TotalExVAT = invoice.TotalExVAT,
                            VAT = invoice.VAT,
                            SubTotalExVAT = invoice.SubTotalExVAT,
                            DiscountIncVAT = invoice.DiscountIncVAT,
                            GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                            DiscountPercentage = invoice.DiscountPercentage,
                            OtherReference = invoice.OtherReference,
                            SalesEmployeeID = invoice.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = invoice.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = invoice.BPAddressInvoice},
                            DeliveryContact = new BusinessPartnerContact {Details = invoice.BPContactDelivery},
                            MainContact = new BusinessPartnerContact {Details = invoice.BPContact},
                            BPHaulier = invoice.BPMasterHaulier,
                            BPCustomer = invoice.BPMasterCustomer,
                            PORequired = invoice.PORequired,
                            Deleted = invoice.Deleted,
                            NodeID = 4,
                            ParentID = 0,
                            DocumentTrailType = Constant.Invoice,
                            Attachments = (from attachment in invoice.ARInvoiceAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.ARInvoiceAttachmentID,
                                    SaleAttachmentID = attachment.ARInvoiceID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in invoice.ARInvoiceDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.ARInvoiceDetailID,
                                    SaleID = detail.ARInvoiceID,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    BPMasterID_Intake = detail.BPMasterID_Intake,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList(),
                        };

                        documents.Add(localInvoice);

                        ProgressBar.SetUp(0,4);
                        ProgressBar.Run(1);
                        var dispatch =
                            entities.ARDispatches.FirstOrDefault(
                                x => x.ARDispatchID == localInvoice.BaseDocumentReferenceID);

                        if (dispatch != null)
                        {
                            var localDispatch = new Sale
                            {
                                SaleID = dispatch.ARDispatchID,
                                DocumentNumberingID = dispatch.DocumentNumberingID,
                                Number = dispatch.Number,
                                BPContactID = dispatch.BPContactID,
                                DocumentDate = dispatch.DocumentDate,
                                CreationDate = dispatch.CreationDate,
                                SaleType = ViewType.ARDispatch,
                                RouteID = dispatch.RouteID,
                                CustomerPOReference = dispatch.CustomerPOReference,
                                BaseDocumentReferenceID = dispatch.BaseDocumentReferenceID,
                                Printed = dispatch.Printed,
                                NouDocStatusID = dispatch.NouDocStatusID,
                                NouDocStatus = dispatch.NouDocStatu.Value,
                                Remarks = dispatch.Remarks,
                                TotalExVAT = dispatch.TotalExVAT,
                                DeliveryDate = dispatch.DeliveryDate,
                                ShippingDate = dispatch.ShippingDate,
                                VAT = dispatch.VAT,
                                SubTotalExVAT = dispatch.SubTotalExVAT,
                                DiscountIncVAT = dispatch.DiscountIncVAT,
                                GrandTotalIncVAT = dispatch.GrandTotalIncVAT,
                                DiscountPercentage = dispatch.DiscountPercentage,
                                SalesEmployeeID = dispatch.UserMasterID,
                                DeliveryAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressDelivery},
                                InvoiceAddress = new BusinessPartnerAddress {Details = dispatch.BPAddressInvoice},
                                MainContact = new BusinessPartnerContact {Details = dispatch.BPContact},
                                BPHaulier = dispatch.BPMasterHaulier,
                                BPCustomer = dispatch.BPMasterCustomer,
                                Deleted = dispatch.Deleted,
                                NodeID = 3,
                                ParentID = 4,
                                DocumentTrailType = Constant.Dispatch,
                                Attachments = (from attachment in dispatch.ARDispatchAttachments
                                               where attachment.Deleted == null
                                    select new Attachment
                                    {
                                        AttachmentID = attachment.ARDispatchAttachmentID,
                                        SaleAttachmentID = attachment.ARDispatchID,
                                        AttachmentDate = attachment.AttachmentDate,
                                        FilePath = attachment.FilePath,
                                        FileName = attachment.FileName,
                                        File = attachment.File,
                                        Deleted = attachment.Deleted
                                    }).ToList(),
                                SaleDetails = (from detail in dispatch.ARDispatchDetails
                                               where detail.Deleted == null
                                    select new SaleDetail
                                    {
                                        SaleDetailID = detail.ARDispatchDetailID,
                                        SaleID = detail.ARDispatchID,
                                        QuantityOrdered = detail.QuantityOrdered,
                                        WeightOrdered = detail.WeightOrdered,
                                        QuantityDelivered = detail.QuantityDelivered,
                                        WeightDelivered = detail.WeightDelivered,
                                        UnitPrice = detail.UnitPrice,
                                        DiscountPercentage = detail.DiscountPercentage,
                                        DiscountPrice = detail.DiscountPrice,
                                        LineDiscountPercentage = detail.LineDiscountPercentage,
                                        LineDiscountAmount = detail.LineDiscountAmount,
                                        UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                        SaleDetailType = ViewType.APReceipt,
                                        VAT = detail.VAT,
                                        TotalExVAT = detail.TotalExclVAT,
                                        TotalIncVAT = detail.TotalIncVAT,
                                        VatCodeID = detail.VATCodeID,
                                        LoadingSale = true,
                                        INMasterID = detail.INMasterID,
                                        PriceListID = detail.PriceListID ?? 0,
                                        BPMasterID_Intake = detail.BPMasterID_Intake,
                                        NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                        Deleted = detail.Deleted
                                    }).ToList()
                            };

                            documents.Add(localDispatch);

                            ProgressBar.Run(1);
                            var order =
                                entities.AROrders.FirstOrDefault(
                                    x => x.AROrderID == localDispatch.BaseDocumentReferenceID);

                            Sale localOrder;
                            if (order != null)
                            {
                                localOrder = new Sale
                                {
                                    SaleID = order.AROrderID,
                                    DocumentNumberingID = order.DocumentNumberingID,
                                    Number = order.Number,
                                    BPContactID = order.BPContactID,
                                    RouteID = order.RouteID,
                                    BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                    DocumentDate = order.DocumentDate,
                                    CreationDate = order.CreationDate,
                                    DeliveryDate = order.DeliveryDate,
                                    ShippingDate = order.ShippingDate,
                                    Printed = order.Printed,
                                    NouDocStatusID = order.NouDocStatusID,
                                    NouDocStatus = order.NouDocStatu.Value,
                                    Remarks = order.Remarks,
                                    DocketNote = order.DispatchNote1,
                                    DispatchNote1PopUp = order.DispatchNote1PopUp,
                                    InvoiceNote = order.InvoiceNote,
                                    TotalExVAT = order.TotalExVAT,
                                    VAT = order.VAT,
                                    SubTotalExVAT = order.SubTotalExVAT,
                                    DiscountIncVAT = order.DiscountIncVAT,
                                    GrandTotalIncVAT = order.GrandTotalIncVAT,
                                    DiscountPercentage = order.DiscountPercentage,
                                    OtherReference = order.OtherReference,
                                    SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                    DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                                    InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                                    DeliveryContact = new BusinessPartnerContact {Details = order.BPContactDelivery},
                                    MainContact = new BusinessPartnerContact {Details = order.BPContact},
                                    BPHaulier = order.BPMasterHaulier,
                                    BPCustomer = order.BPMasterCustomer,
                                    PORequired = order.PORequired,
                                    Deleted = order.Deleted,
                                    NodeID = 2,
                                    ParentID = 3,
                                    DocumentTrailType = Constant.Order,
                                    Attachments = (from attachment in order.OrderAttachments
                                                   where attachment.Deleted == null
                                        select new Attachment
                                        {
                                            AttachmentID = attachment.OrderAttachmentID,
                                            SaleAttachmentID = attachment.AROrderID,
                                            AttachmentDate = attachment.AttachmentDate,
                                            FilePath = attachment.FilePath,
                                            FileName = attachment.FileName,
                                            File = attachment.File,
                                            Deleted = attachment.Deleted
                                        }).ToList(),
                                    SaleDetails = (from detail in order.AROrderDetails
                                                   where detail.Deleted == null
                                        select new SaleDetail
                                        {
                                            SaleDetailID = detail.AROrderDetailID,
                                            SaleID = detail.AROrderID,
                                            QuantityOrdered = detail.QuantityOrdered,
                                            WeightOrdered = detail.WeightOrdered,
                                            //QuantityDelivered = detail.QuantityDelivered,
                                            //WeightDelivered = detail.WeightDelivered,
                                            UnitPrice = detail.UnitPrice,
                                            DiscountPercentage = detail.DiscountPercentage,
                                            DiscountPrice = detail.DiscountPrice,
                                            LineDiscountPercentage = detail.LineDiscountPercentage,
                                            LineDiscountAmount = detail.LineDiscountAmount,
                                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                            VAT = detail.VAT,
                                            TotalExVAT = detail.TotalExclVAT,
                                            TotalIncVAT = detail.TotalIncVAT,
                                            VatCodeID = detail.VATCodeID,
                                            LoadingSale = true,
                                        
                                            INMasterID = detail.INMasterID,
                                            PriceListID = detail.PriceListID ?? 0,
                                            NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                            Deleted = detail.Deleted
                                        }).ToList()
                                };

                                documents.Add(localOrder);

                                ProgressBar.Run(1);
                                var quote =
                                    entities.ARQuotes.FirstOrDefault(
                                        x => x.ARQuoteID == localOrder.BaseDocumentReferenceID);
                                Sale localQuote;
                                if (quote != null)
                                {
                                    localQuote = new Sale
                                    {
                                        SaleID = quote.ARQuoteID,
                                        DocumentNumberingID = quote.DocumentNumberingID,
                                        Number = quote.Number,
                                        BPContactID = quote.BPContactID,
                                        RouteID = quote.RouteID,
                                        QuoteValidDate = quote.QuoteValidDate,
                                        DocumentDate = quote.DocumentDate,
                                        CreationDate = quote.CreationDate,
                                        Printed = quote.Printed,
                                        NouDocStatusID = quote.NouDocStatusID,
                                        NouDocStatus = quote.NouDocStatu.Value,
                                        Remarks = quote.Remarks,
                                        DocketNote = quote.DispatchNote1,
                                        DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                        InvoiceNote = quote.InvoiceNote,
                                        TotalExVAT = quote.TotalExVAT,
                                        VAT = quote.VAT,
                                        SubTotalExVAT = quote.SubTotalExVAT,
                                        DiscountIncVAT = quote.DiscountIncVAT,
                                        GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                        DiscountPercentage = quote.DiscountPercentage,
                                        OtherReference = quote.OtherReference,
                                        SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                        DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                                        InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                                        DeliveryContact = new BusinessPartnerContact {Details = quote.BPContactDelivery},
                                        MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                                        BPHaulier = quote.BPMasterHaulier,
                                        BPCustomer = quote.BPMasterCustomer,
                                        PORequired = quote.PORequired,
                                        Deleted = quote.Deleted,
                                        NodeID = 1,
                                        ParentID = 2,
                                        DocumentTrailType = Constant.Quote,
                                        Attachments = (from attachment in quote.QuoteAttachments
                                                       where attachment.Deleted == null
                                            select new Attachment
                                            {
                                                AttachmentID = attachment.QuoteAttachmentID,
                                                SaleAttachmentID = attachment.ARQuoteID,
                                                AttachmentDate = attachment.AttachmentDate,
                                                FilePath = attachment.FilePath,
                                                FileName = attachment.FileName,
                                                File = attachment.File,
                                                Deleted = attachment.Deleted
                                            }).ToList(),
                                        SaleDetails = (from detail in quote.ARQuoteDetails
                                                       where detail.Deleted == null
                                            select new SaleDetail
                                            {
                                                SaleDetailID = detail.ARQuoteDetailID,
                                                SaleID = detail.ARQuoteID,
                                                QuantityOrdered = detail.QuantityOrdered,
                                                WeightOrdered = detail.WeightOrdered,
                                                //QuantityDelivered = detail.QuantityDelivered,
                                                //WeightDelivered = detail.WeightDelivered,
                                                UnitPrice = detail.UnitPrice,
                                                DiscountPercentage = detail.DiscountPercentage,
                                                DiscountPrice = detail.DiscountPrice,
                                                LineDiscountPercentage = detail.LineDiscountPercentage,
                                                LineDiscountAmount = detail.LineDiscountAmount,
                                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                VAT = detail.VAT,
                                                TotalExVAT = detail.TotalExclVAT,
                                                TotalIncVAT = detail.TotalIncVAT,
                                                VatCodeID = detail.VATCodeID,
                                                LoadingSale = true,
                                                INMasterID = detail.INMasterID,
                                                PriceListID = detail.PriceListID ?? 0,
                                                Deleted = detail.Deleted
                                            }).ToList()
                                    };

                                    documents.Add(localQuote);
                                }
                            }
                        }
                        else
                        {
                            // check for multiple associated invoice dispatch dockets.
                            var dispatches = entities.ARDispatches.Where(x => x.ARInvoiceID == localInvoice.SaleID);
                            var localNodeID = 5;
                            foreach (var localDispatchDocket in dispatches)
                            {
                                ProgressBar.SetUp(0, dispatches.Count());
                                ProgressBar.Run(1);
                                localNodeID++;
                                var dispatchDoc = new Sale
                                {
                                    SaleID = localDispatchDocket.ARDispatchID,
                                    DocumentNumberingID = localDispatchDocket.DocumentNumberingID,
                                    Number = localDispatchDocket.Number,
                                    BPContactID = localDispatchDocket.BPContactID,
                                    DocumentDate = localDispatchDocket.DocumentDate,
                                    CreationDate = localDispatchDocket.CreationDate,
                                    SaleType = ViewType.ARDispatch,
                                    RouteID = localDispatchDocket.RouteID,
                                    CustomerPOReference = localDispatchDocket.CustomerPOReference,
                                    BaseDocumentReferenceID = localDispatchDocket.BaseDocumentReferenceID,
                                    Printed = localDispatchDocket.Printed,
                                    NouDocStatusID = localDispatchDocket.NouDocStatusID,
                                    NouDocStatus = localDispatchDocket.NouDocStatu.Value,
                                    Remarks = localDispatchDocket.Remarks,
                                    TotalExVAT = localDispatchDocket.TotalExVAT,
                                    DeliveryDate = localDispatchDocket.DeliveryDate,
                                    ShippingDate = localDispatchDocket.ShippingDate,
                                    VAT = localDispatchDocket.VAT,
                                    SubTotalExVAT = localDispatchDocket.SubTotalExVAT,
                                    DiscountIncVAT = localDispatchDocket.DiscountIncVAT,
                                    GrandTotalIncVAT = localDispatchDocket.GrandTotalIncVAT,
                                    DiscountPercentage = localDispatchDocket.DiscountPercentage,
                                    SalesEmployeeID = localDispatchDocket.UserMasterID,
                                    DeliveryAddress =
                                        new BusinessPartnerAddress {Details = localDispatchDocket.BPAddressDelivery},
                                    InvoiceAddress =
                                        new BusinessPartnerAddress {Details = localDispatchDocket.BPAddressInvoice},
                                    MainContact = new BusinessPartnerContact {Details = localDispatchDocket.BPContact},
                                    BPHaulier = localDispatchDocket.BPMasterHaulier,
                                    BPCustomer = localDispatchDocket.BPMasterCustomer,
                                    Deleted = localDispatchDocket.Deleted,
                                    NodeID = localNodeID,
                                    ParentID = 4,
                                    DocumentTrailType = Constant.Dispatch,
                                    Attachments = (from attachment in localDispatchDocket.ARDispatchAttachments
                                                   where attachment.Deleted == null
                                        select new Attachment
                                        {
                                            AttachmentID = attachment.ARDispatchAttachmentID,
                                            SaleAttachmentID = attachment.ARDispatchID,
                                            AttachmentDate = attachment.AttachmentDate,
                                            FilePath = attachment.FilePath,
                                            FileName = attachment.FileName,
                                            File = attachment.File,
                                            Deleted = attachment.Deleted
                                        }).ToList(),
                                    SaleDetails = (from detail in localDispatchDocket.ARDispatchDetails
                                                   where detail.Deleted == null
                                        select new SaleDetail
                                        {
                                            SaleDetailID = detail.ARDispatchDetailID,
                                            SaleID = detail.ARDispatchID,
                                            QuantityOrdered = detail.QuantityOrdered,
                                            WeightOrdered = detail.WeightOrdered,
                                            QuantityDelivered = detail.QuantityDelivered,
                                            WeightDelivered = detail.WeightDelivered,
                                            UnitPrice = detail.UnitPrice,
                                            DiscountPercentage = detail.DiscountPercentage,
                                            DiscountPrice = detail.DiscountPrice,
                                            LineDiscountPercentage = detail.LineDiscountPercentage,
                                            LineDiscountAmount = detail.LineDiscountAmount,
                                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                            SaleDetailType = ViewType.APReceipt,
                                            VAT = detail.VAT,
                                            TotalExVAT = detail.TotalExclVAT,
                                            TotalIncVAT = detail.TotalIncVAT,
                                            VatCodeID = detail.VATCodeID,
                                            LoadingSale = true,
                                            INMasterID = detail.INMasterID,
                                            PriceListID = detail.PriceListID ?? 0,
                                            BPMasterID_Intake = detail.BPMasterID_Intake,
                                            NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                            Deleted = detail.Deleted
                                        }).ToList()
                                };

                                documents.Add(dispatchDoc);

                                localNodeID++;
                                var order =
                                    entities.AROrders.FirstOrDefault(
                                        x => x.AROrderID == dispatchDoc.BaseDocumentReferenceID);

                                Sale localOrder;
                                if (order != null)
                                {
                                    localOrder = new Sale
                                    {
                                        SaleID = order.AROrderID,
                                        DocumentNumberingID = order.DocumentNumberingID,
                                        Number = order.Number,
                                        BPContactID = order.BPContactID,
                                        RouteID = order.RouteID,
                                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                        DocumentDate = order.DocumentDate,
                                        CreationDate = order.CreationDate,
                                        DeliveryDate = order.DeliveryDate,
                                        ShippingDate = order.ShippingDate,
                                        Printed = order.Printed,
                                        NouDocStatusID = order.NouDocStatusID,
                                        NouDocStatus = order.NouDocStatu.Value,
                                        Remarks = order.Remarks,
                                        DocketNote = order.DispatchNote1,
                                        DispatchNote1PopUp = order.DispatchNote1PopUp,
                                        InvoiceNote = order.InvoiceNote,
                                        TotalExVAT = order.TotalExVAT,
                                        VAT = order.VAT,
                                        SubTotalExVAT = order.SubTotalExVAT,
                                        DiscountIncVAT = order.DiscountIncVAT,
                                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                                        DiscountPercentage = order.DiscountPercentage,
                                        OtherReference = order.OtherReference,
                                        SalesEmployeeID = order.UserMasterID_SalesEmployee,
                                        DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                                        InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                                        DeliveryContact = new BusinessPartnerContact {Details = order.BPContactDelivery},
                                        MainContact = new BusinessPartnerContact {Details = order.BPContact},
                                        BPHaulier = order.BPMasterHaulier,
                                        BPCustomer = order.BPMasterCustomer,
                                        PORequired = order.PORequired,
                                        Deleted = order.Deleted,
                                        NodeID = localNodeID,
                                        ParentID = dispatchDoc.NodeID,
                                        DocumentTrailType = Constant.Order,
                                        Attachments = (from attachment in order.OrderAttachments
                                                       where attachment.Deleted == null
                                            select new Attachment
                                            {
                                                AttachmentID = attachment.OrderAttachmentID,
                                                SaleAttachmentID = attachment.AROrderID,
                                                AttachmentDate = attachment.AttachmentDate,
                                                FilePath = attachment.FilePath,
                                                FileName = attachment.FileName,
                                                File = attachment.File,
                                                Deleted = attachment.Deleted
                                            }).ToList(),
                                        SaleDetails = (from detail in order.AROrderDetails
                                                       where detail.Deleted == null
                                            select new SaleDetail
                                            {
                                                SaleDetailID = detail.AROrderDetailID,
                                                SaleID = detail.AROrderID,
                                                QuantityOrdered = detail.QuantityOrdered,
                                                WeightOrdered = detail.WeightOrdered,
                                                //QuantityDelivered = detail.QuantityDelivered,
                                                //WeightDelivered = detail.WeightDelivered,
                                                UnitPrice = detail.UnitPrice,
                                                DiscountPercentage = detail.DiscountPercentage,
                                                DiscountPrice = detail.DiscountPrice,
                                                LineDiscountPercentage = detail.LineDiscountPercentage,
                                                LineDiscountAmount = detail.LineDiscountAmount,
                                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                VAT = detail.VAT,
                                                TotalExVAT = detail.TotalExclVAT,
                                                TotalIncVAT = detail.TotalIncVAT,
                                                VatCodeID = detail.VATCodeID,
                                                LoadingSale = true,
                                                INMasterID = detail.INMasterID,
                                                PriceListID = detail.PriceListID ?? 0,
                                             
                                                NouStockMode =
                                                    detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                Deleted = detail.Deleted
                                            }).ToList()
                                    };

                                    documents.Add(localOrder);

                                    localNodeID++;
                                    var quote =
                                        entities.ARQuotes.FirstOrDefault(
                                            x => x.ARQuoteID == localOrder.BaseDocumentReferenceID);
                                    Sale localQuote;
                                    if (quote != null)
                                    {
                                        localQuote = new Sale
                                        {
                                            SaleID = quote.ARQuoteID,
                                            DocumentNumberingID = quote.DocumentNumberingID,
                                            Number = quote.Number,
                                            BPContactID = quote.BPContactID,
                                            RouteID = quote.RouteID,
                                            QuoteValidDate = quote.QuoteValidDate,
                                            DocumentDate = quote.DocumentDate,
                                            CreationDate = quote.CreationDate,
                                            Printed = quote.Printed,
                                            NouDocStatusID = quote.NouDocStatusID,
                                            NouDocStatus = quote.NouDocStatu.Value,
                                            Remarks = quote.Remarks,
                                            DocketNote = quote.DispatchNote1,
                                            DispatchNote1PopUp = quote.DispatchNote1PopUp,
                                            InvoiceNote = quote.InvoiceNote,
                                            TotalExVAT = quote.TotalExVAT,
                                            VAT = quote.VAT,
                                            SubTotalExVAT = quote.SubTotalExVAT,
                                            DiscountIncVAT = quote.DiscountIncVAT,
                                            GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                            DiscountPercentage = quote.DiscountPercentage,
                                            OtherReference = quote.OtherReference,
                                            SalesEmployeeID = quote.UserMasterID_SalesEmployee,
                                            DeliveryAddress =
                                                new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                                            InvoiceAddress =
                                                new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                                            DeliveryContact =
                                                new BusinessPartnerContact {Details = quote.BPContactDelivery},
                                            MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                                            BPHaulier = quote.BPMasterHaulier,
                                            BPCustomer = quote.BPMasterCustomer,
                                            PORequired = quote.PORequired,
                                            Deleted = quote.Deleted,
                                            NodeID = localNodeID,
                                            ParentID = localOrder.NodeID,
                                            DocumentTrailType = Constant.Quote,
                                            Attachments = (from attachment in quote.QuoteAttachments
                                                           where attachment.Deleted == null
                                                select new Attachment
                                                {
                                                    AttachmentID = attachment.QuoteAttachmentID,
                                                    SaleAttachmentID = attachment.ARQuoteID,
                                                    AttachmentDate = attachment.AttachmentDate,
                                                    FilePath = attachment.FilePath,
                                                    FileName = attachment.FileName,
                                                    File = attachment.File,
                                                    Deleted = attachment.Deleted
                                                }).ToList(),
                                            SaleDetails = (from detail in quote.ARQuoteDetails
                                                           where detail.Deleted == null
                                                select new SaleDetail
                                                {
                                                    SaleDetailID = detail.ARQuoteDetailID,
                                                    SaleID = detail.ARQuoteID,
                                                    QuantityOrdered = detail.QuantityOrdered,
                                                    WeightOrdered = detail.WeightOrdered,
                                                    //QuantityDelivered = detail.QuantityDelivered,
                                                    //WeightDelivered = detail.WeightDelivered,
                                                    UnitPrice = detail.UnitPrice,
                                                    DiscountPercentage = detail.DiscountPercentage,
                                                    DiscountPrice = detail.DiscountPrice,
                                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                                    LineDiscountAmount = detail.LineDiscountAmount,
                                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                    VAT = detail.VAT,
                                                    TotalExVAT = detail.TotalExclVAT,
                                                    TotalIncVAT = detail.TotalIncVAT,
                                                    VatCodeID = detail.VATCodeID,
                                                    LoadingSale = true,
                                                    INMasterID = detail.INMasterID,
                                                    PriceListID = detail.PriceListID ?? 0,
                                                    Deleted = detail.Deleted
                                                }).ToList()
                                        };

                                        documents.Add(localQuote);
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region ap quote

                    if (mode == ViewType.APQuote)
                    {
                        ProgressBar.Run(1);
                        var quote = entities.APQuotes.FirstOrDefault(x => x.APQuoteID == documentNo);
                        if (quote == null)
                        {
                            return documents;
                        }

                        var localQuote = new Sale
                        {
                            SaleID = quote.APQuoteID,
                            DocumentNumberingID = quote.DocumentNumberingID,
                            Number = quote.Number,
                            BPContactID = quote.BPContactID,
                            QuoteValidDate = quote.QuoteValidDate,
                            DocumentDate = quote.DocumentDate,
                            CreationDate = quote.CreationDate,
                            Printed = quote.Printed,
                            NouDocStatusID = quote.NouDocStatusID,
                            NouDocStatus = quote.NouDocStatu.Value,
                            Remarks = quote.Remarks,
                            TotalExVAT = quote.TotalExVAT,
                            VAT = quote.VAT,
                            SubTotalExVAT = quote.SubTotalExVAT,
                            DiscountIncVAT = quote.DiscountIncVAT,
                            GrandTotalIncVAT = quote.GrandTotalIncVAT,
                            DiscountPercentage = quote.DiscountPercentage,
                            SalesEmployeeID = quote.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                            BPHaulier = quote.BPMasterHaulier,
                            BPCustomer = quote.BPMasterCustomer,
                            Deleted = quote.Deleted,
                            DocumentTrailType = Constant.APQuote,
                            NodeID = 1,
                            ParentID = 0,
                            Attachments = (from attachment in quote.APQuoteAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.APQuoteAttachmentID,
                                    SaleAttachmentID = attachment.APQuoteID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in quote.APQuoteDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.APQuoteDetailID,
                                    SaleID = detail.APQuoteID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        documents.Add(localQuote);

                        ProgressBar.Run(1);
                        var locOrder =
                            entities.APOrders.FirstOrDefault(x => x.BaseDocumentReferenceID == localQuote.SaleID);

                        if (locOrder == null)
                        {
                            return documents;
                        }

                        var localOrder = new Sale
                        {
                            SaleID = locOrder.APOrderID,
                            DocumentNumberingID = locOrder.DocumentNumberingID,
                            Number = locOrder.Number,
                            BPContactID = locOrder.BPContactID,
                            QuoteValidDate = locOrder.QuoteValidDate,
                            DocumentDate = locOrder.DocumentDate,
                            CreationDate = locOrder.CreationDate,
                            SaleType = ViewType.APOrder,
                            DeliveryDate = locOrder.DeliveryDate,
                            Printed = locOrder.Printed,
                            NouDocStatusID = locOrder.NouDocStatusID,
                            NouDocStatus = locOrder.NouDocStatu.Value,
                            Remarks = locOrder.Remarks,
                            TotalExVAT = locOrder.TotalExVAT,
                            VAT = locOrder.VAT,
                            SubTotalExVAT = locOrder.SubTotalExVAT,
                            DiscountIncVAT = locOrder.DiscountIncVAT,
                            GrandTotalIncVAT = locOrder.GrandTotalIncVAT,
                            DiscountPercentage = locOrder.DiscountPercentage,
                            SalesEmployeeID = locOrder.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = locOrder.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = locOrder.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = locOrder.BPContact},
                            BPHaulier = locOrder.BPMasterHaulier,
                            BPCustomer = locOrder.BPMasterSupplier,
                            Deleted = locOrder.Deleted,
                            DocumentTrailType = Constant.APOrder,
                            NodeID = 2,
                            ParentID = 0,
                            Attachments = (from attachment in locOrder.APOrderAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.APOrderAttachmentID,
                                    SaleAttachmentID = attachment.APOrderID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in locOrder.APOrderDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.APOrderDetailID,
                                    SaleID = detail.APOrderID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityReceived = detail.QuantityDelivered,
                                    WeightReceived = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        localQuote.ParentID = 2;
                        documents.Add(localOrder);

                        ProgressBar.Run(1);
                        var order =
                            entities.APGoodsReceipts.FirstOrDefault(x => x.BaseDocumentReferenceID == localOrder.SaleID);

                        if (order == null)
                        {
                            return documents;
                        }

                        var localGoodsIn = new Sale
                        {
                            SaleID = order.APGoodsReceiptID,
                            APOrderID = order.APOrderID,
                            DocumentNumberingID = order.DocumentNumberingID,
                            Number = order.Number,
                            SaleType = ViewType.APReceipt,
                            BPContactID = order.BPContactID,
                            DocumentDate = order.DocumentDate,
                            CreationDate = order.CreationDate,
                            DeliveryDate = order.DeliveryDate,
                            Printed = order.Printed,
                            NouDocStatusID = order.NouDocStatusID,
                            NouDocStatus = order.NouDocStatu.Value,
                            Remarks = order.Remarks,
                            BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                            TotalExVAT = order.TotalExVAT,
                            VAT = order.VAT,
                            SubTotalExVAT = order.SubTotalExVAT,
                            DiscountIncVAT = order.DiscountIncVAT,
                            GrandTotalIncVAT = order.GrandTotalIncVAT,
                            DiscountPercentage = order.DiscountPercentage,
                            SalesEmployeeID = order.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = order.BPContact},
                            BPHaulier = order.BPMasterHaulier,
                            BPCustomer = order.BPMasterSupplier,
                            Deleted = order.Deleted,
                            DocumentTrailType = Constant.GoodsIn,
                            NodeID = 3,
                            ParentID = 0,
                            Attachments = (from attachment in order.APGoodsReceiptAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                    SaleAttachmentID = attachment.APGoodsReceiptID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in order.APGoodsReceiptDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.APGoodsReceiptID,
                                    SaleID = detail.APGoodsReceiptID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityReceived = detail.QuantityReceived,
                                    WeightReceived = detail.WeightReceived,
                                    UnitPrice = detail.UnitPrice,
                                    SaleDetailType = ViewType.APReceipt,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    TotalExVAT = detail.TotalExcVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    INMaster = detail.INMaster,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    OrderDate = order.DocumentDate,
                                    Deleted = detail.Deleted
                                    //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                    //                     select new GoodsReceiptData
                                    //                     {
                                    //                         BatchNumber = batchNumber,
                                    //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                    //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                    //                                            select new StockTransactionData
                                    //                                            {
                                    //                                                Transaction = transaction,
                                    //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                    //                                            }).ToList()
                                    //                     }).ToList(),
                                }).ToList()
                        };

                        localOrder.ParentID = 3;
                        documents.Add(localGoodsIn);
                    }

                    #endregion

                    #region ap order

                    if (mode == ViewType.APOrder)
                    {
                        ProgressBar.SetUp(0,3);
                        ProgressBar.Run(1);
                        var order = entities.APOrders.FirstOrDefault(x => x.APOrderID == documentNo);

                        if (order == null)
                        {
                            return documents;
                        }

                        var localOrder = new Sale
                        {
                            SaleID = order.APOrderID,
                            DocumentNumberingID = order.DocumentNumberingID,
                            BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                            Number = order.Number,
                            BPContactID = order.BPContactID,
                            QuoteValidDate = order.QuoteValidDate,
                            DocumentDate = order.DocumentDate,
                            CreationDate = order.CreationDate,
                            SaleType = ViewType.APOrder,
                            DeliveryDate = order.DeliveryDate,
                            Printed = order.Printed,
                            NouDocStatusID = order.NouDocStatusID,
                            NouDocStatus = order.NouDocStatu.Value,
                            Remarks = order.Remarks,
                            TotalExVAT = order.TotalExVAT,
                            VAT = order.VAT,
                            SubTotalExVAT = order.SubTotalExVAT,
                            DiscountIncVAT = order.DiscountIncVAT,
                            GrandTotalIncVAT = order.GrandTotalIncVAT,
                            DiscountPercentage = order.DiscountPercentage,
                            SalesEmployeeID = order.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = order.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = order.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = order.BPContact},
                            BPHaulier = order.BPMasterHaulier,
                            BPCustomer = order.BPMasterSupplier,
                            Deleted = order.Deleted,
                            DocumentTrailType = Constant.APOrder,
                            NodeID = 2,
                            ParentID = 0,
                            Attachments = (from attachment in order.APOrderAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.APOrderAttachmentID,
                                    SaleAttachmentID = attachment.APOrderID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in order.APOrderDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.APOrderDetailID,
                                    SaleID = detail.APOrderID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityReceived = detail.QuantityDelivered,
                                    WeightReceived = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    TotalExVAT = detail.TotalExclVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    Deleted = detail.Deleted
                                }).ToList()
                        };

                        documents.Add(localOrder);

                        ProgressBar.Run(1);
                        var quote =
                            entities.APQuotes.FirstOrDefault(x => x.APQuoteID == localOrder.BaseDocumentReferenceID);
                        Sale localQuote;
                        if (quote != null)
                        {
                            localQuote = new Sale
                            {
                                SaleID = quote.APQuoteID,
                                DocumentNumberingID = quote.DocumentNumberingID,
                                Number = quote.Number,
                                BPContactID = quote.BPContactID,
                                QuoteValidDate = quote.QuoteValidDate,
                                DocumentDate = quote.DocumentDate,
                                CreationDate = quote.CreationDate,
                                Printed = quote.Printed,
                                NouDocStatusID = quote.NouDocStatusID,
                                NouDocStatus = quote.NouDocStatu.Value,
                                Remarks = quote.Remarks,
                                TotalExVAT = quote.TotalExVAT,
                                VAT = quote.VAT,
                                SubTotalExVAT = quote.SubTotalExVAT,
                                DiscountIncVAT = quote.DiscountIncVAT,
                                GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                DiscountPercentage = quote.DiscountPercentage,
                                SalesEmployeeID = quote.UserMasterID,
                                DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                                InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                                MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                                BPHaulier = quote.BPMasterHaulier,
                                BPCustomer = quote.BPMasterCustomer,
                                Deleted = quote.Deleted,
                                DocumentTrailType = Constant.APQuote,
                                NodeID = 1,
                                ParentID = 2,
                                Attachments = (from attachment in quote.APQuoteAttachments
                                               where attachment.Deleted == null
                                    select new Attachment
                                    {
                                        AttachmentID = attachment.APQuoteAttachmentID,
                                        SaleAttachmentID = attachment.APQuoteID,
                                        AttachmentDate = attachment.AttachmentDate,
                                        FilePath = attachment.FilePath,
                                        FileName = attachment.FileName,
                                        File = attachment.File,
                                        Deleted = attachment.Deleted
                                    }).ToList(),
                                SaleDetails = (from detail in quote.APQuoteDetails
                                               where detail.Deleted == null
                                    select new SaleDetail
                                    {
                                        SaleDetailID = detail.APQuoteDetailID,
                                        SaleID = detail.APQuoteID,
                                        QuantityOrdered = detail.QuantityOrdered,
                                        WeightOrdered = detail.WeightOrdered,
                                        QuantityDelivered = detail.QuantityDelivered,
                                        WeightDelivered = detail.WeightDelivered,
                                        UnitPrice = detail.UnitPrice,
                                        DiscountPercentage = detail.DiscountPercentage,
                                        DiscountPrice = detail.DiscountPrice,
                                        LineDiscountPercentage = detail.LineDiscountPercentage,
                                        LineDiscountAmount = detail.LineDiscountAmount,
                                        UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                        VAT = detail.VAT,
                                        TotalExVAT = detail.TotalExclVAT,
                                        TotalIncVAT = detail.TotalIncVAT,
                                        VatCodeID = detail.VATCodeID,
                                        LoadingSale = true,
                                        INMasterID = detail.INMasterID,
                                        PriceListID = detail.PriceListID ?? 0,
                                        Deleted = detail.Deleted
                                    }).ToList()
                            };

                            documents.Add(localQuote);
                        }

                        ProgressBar.Run(1);
                        var goodsIn =
                            entities.APGoodsReceipts.FirstOrDefault(x => x.BaseDocumentReferenceID == localOrder.SaleID);

                        if (goodsIn == null)
                        {
                            return documents;
                        }

                        var localGoodsIn = new Sale
                        {
                            SaleID = goodsIn.APGoodsReceiptID,
                            DocumentNumberingID = goodsIn.DocumentNumberingID,
                            Number = goodsIn.Number,
                            SaleType = ViewType.APReceipt,
                            BPContactID = goodsIn.BPContactID,
                            DocumentDate = goodsIn.DocumentDate,
                            CreationDate = goodsIn.CreationDate,
                            DeliveryDate = goodsIn.DeliveryDate,
                            Printed = goodsIn.Printed,
                            NouDocStatusID = goodsIn.NouDocStatusID,
                            NouDocStatus = goodsIn.NouDocStatu.Value,
                            Remarks = goodsIn.Remarks,
                            BaseDocumentReferenceID = goodsIn.BaseDocumentReferenceID,
                            TotalExVAT = goodsIn.TotalExVAT,
                            VAT = goodsIn.VAT,
                            SubTotalExVAT = goodsIn.SubTotalExVAT,
                            DiscountIncVAT = goodsIn.DiscountIncVAT,
                            GrandTotalIncVAT = goodsIn.GrandTotalIncVAT,
                            DiscountPercentage = goodsIn.DiscountPercentage,
                            SalesEmployeeID = goodsIn.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = goodsIn.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = goodsIn.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = goodsIn.BPContact},
                            BPHaulier = goodsIn.BPMasterHaulier,
                            BPCustomer = goodsIn.BPMasterSupplier,
                            Deleted = goodsIn.Deleted,
                            DocumentTrailType = Constant.GoodsIn,
                            NodeID = 3,
                            ParentID = 0,
                            Attachments = (from attachment in goodsIn.APGoodsReceiptAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                    SaleAttachmentID = attachment.APGoodsReceiptID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in goodsIn.APGoodsReceiptDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.APGoodsReceiptID,
                                    SaleID = detail.APGoodsReceiptID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityReceived = detail.QuantityReceived,
                                    WeightReceived = detail.WeightReceived,
                                    UnitPrice = detail.UnitPrice,
                                    SaleDetailType = ViewType.APReceipt,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExcVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    INMaster = detail.INMaster,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    OrderDate = order.DocumentDate,
                                    Deleted = detail.Deleted
                                    //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                    //                     select new GoodsReceiptData
                                    //                     {
                                    //                         BatchNumber = batchNumber,
                                    //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                    //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                    //                                            select new StockTransactionData
                                    //                                            {
                                    //                                                Transaction = transaction,
                                    //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                    //                                            }).ToList()
                                    //                     }).ToList(),
                                }).ToList()
                        };

                        localOrder.ParentID = 3;
                        documents.Add(localGoodsIn);
                    }

                    #endregion

                    #region goods in

                    if (mode == ViewType.APReceipt)
                    {
                        ProgressBar.SetUp(0, 3);
                        ProgressBar.Run(1);
                        var goodsIn = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == documentNo);

                        if (goodsIn == null)
                        {
                            return documents;
                        }

                        var localGoodsIn = new Sale
                        {
                            SaleID = goodsIn.APGoodsReceiptID,
                            APOrderID = goodsIn.APOrderID,
                            DocumentNumberingID = goodsIn.DocumentNumberingID,
                            Number = goodsIn.Number,
                            SaleType = ViewType.APReceipt,
                            BPContactID = goodsIn.BPContactID,
                            DocumentDate = goodsIn.DocumentDate,
                            CreationDate = goodsIn.CreationDate,
                            DeliveryDate = goodsIn.DeliveryDate,
                            Printed = goodsIn.Printed,
                            NouDocStatusID = goodsIn.NouDocStatusID,
                            NouDocStatus = goodsIn.NouDocStatu.Value,
                            Remarks = goodsIn.Remarks,
                            BaseDocumentReferenceID = goodsIn.BaseDocumentReferenceID,
                            TotalExVAT = goodsIn.TotalExVAT,
                            VAT = goodsIn.VAT,
                            SubTotalExVAT = goodsIn.SubTotalExVAT,
                            DiscountIncVAT = goodsIn.DiscountIncVAT,
                            GrandTotalIncVAT = goodsIn.GrandTotalIncVAT,
                            DiscountPercentage = goodsIn.DiscountPercentage,
                            SalesEmployeeID = goodsIn.UserMasterID,
                            DeliveryAddress = new BusinessPartnerAddress {Details = goodsIn.BPAddressDelivery},
                            InvoiceAddress = new BusinessPartnerAddress {Details = goodsIn.BPAddressInvoice},
                            MainContact = new BusinessPartnerContact {Details = goodsIn.BPContact},
                            BPHaulier = goodsIn.BPMasterHaulier,
                            BPCustomer = goodsIn.BPMasterSupplier,
                            Deleted = goodsIn.Deleted,
                            DocumentTrailType = Constant.GoodsIn,
                            NodeID = 3,
                            ParentID = 0,
                            Attachments = (from attachment in goodsIn.APGoodsReceiptAttachments
                                           where attachment.Deleted == null
                                select new Attachment
                                {
                                    AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                    SaleAttachmentID = attachment.APGoodsReceiptID,
                                    AttachmentDate = attachment.AttachmentDate,
                                    FilePath = attachment.FilePath,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = attachment.Deleted
                                }).ToList(),
                            SaleDetails = (from detail in goodsIn.APGoodsReceiptDetails
                                           where detail.Deleted == null
                                select new SaleDetail
                                {
                                    SaleDetailID = detail.APGoodsReceiptID,
                                    SaleID = detail.APGoodsReceiptID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    QuantityReceived = detail.QuantityReceived,
                                    WeightReceived = detail.WeightReceived,
                                    UnitPrice = detail.UnitPrice,
                                    SaleDetailType = ViewType.APReceipt,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    TotalExVAT = detail.TotalExcVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    VatCodeID = detail.VATCodeID,
                                    LoadingSale = true,
                                    INMasterID = detail.INMasterID,
                                    PriceListID = detail.PriceListID ?? 0,
                                    INMaster = detail.INMaster,
                                    NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                    OrderDate = goodsIn.DocumentDate,
                                    Deleted = detail.Deleted
                                    //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                    //                     select new GoodsReceiptData
                                    //                     {
                                    //                         BatchNumber = batchNumber,
                                    //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                    //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                    //                                            select new StockTransactionData
                                    //                                            {
                                    //                                                Transaction = transaction,
                                    //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                    //                                            }).ToList()
                                    //                     }).ToList(),
                                }).ToList()
                        };

                        documents.Add(localGoodsIn);

                        ProgressBar.Run(1);
                        var aporder =
                            entities.APOrders.FirstOrDefault(x => x.APOrderID == localGoodsIn.BaseDocumentReferenceID);

                        Sale localOrder;
                        if (aporder != null)
                        {
                            localOrder = new Sale
                            {
                                SaleID = aporder.APOrderID,
                                DocumentNumberingID = aporder.DocumentNumberingID,
                                BaseDocumentReferenceID = aporder.BaseDocumentReferenceID,
                                Number = aporder.Number,
                                BPContactID = aporder.BPContactID,
                                QuoteValidDate = aporder.QuoteValidDate,
                                DocumentDate = aporder.DocumentDate,
                                CreationDate = aporder.CreationDate,
                                SaleType = ViewType.APOrder,
                                DeliveryDate = aporder.DeliveryDate,
                                Printed = aporder.Printed,
                                NouDocStatusID = aporder.NouDocStatusID,
                                NouDocStatus = aporder.NouDocStatu.Value,
                                Remarks = aporder.Remarks,
                                TotalExVAT = aporder.TotalExVAT,
                                VAT = aporder.VAT,
                                SubTotalExVAT = aporder.SubTotalExVAT,
                                DiscountIncVAT = aporder.DiscountIncVAT,
                                GrandTotalIncVAT = aporder.GrandTotalIncVAT,
                                DiscountPercentage = aporder.DiscountPercentage,
                                SalesEmployeeID = aporder.UserMasterID,
                                DeliveryAddress = new BusinessPartnerAddress {Details = aporder.BPAddressDelivery},
                                InvoiceAddress = new BusinessPartnerAddress {Details = aporder.BPAddressInvoice},
                                MainContact = new BusinessPartnerContact {Details = aporder.BPContact},
                                BPHaulier = aporder.BPMasterHaulier,
                                BPCustomer = aporder.BPMasterSupplier,
                                Deleted = aporder.Deleted,
                                DocumentTrailType = Constant.APOrder,
                                NodeID = 2,
                                ParentID = 3,
                                Attachments = (from attachment in aporder.APOrderAttachments
                                               where attachment.Deleted == null
                                    select new Attachment
                                    {
                                        AttachmentID = attachment.APOrderAttachmentID,
                                        SaleAttachmentID = attachment.APOrderID,
                                        AttachmentDate = attachment.AttachmentDate,
                                        FilePath = attachment.FilePath,
                                        FileName = attachment.FileName,
                                        File = attachment.File,
                                        Deleted = attachment.Deleted
                                    }).ToList(),
                                SaleDetails = (from detail in aporder.APOrderDetails
                                               where detail.Deleted == null
                                    select new SaleDetail
                                    {
                                        SaleDetailID = detail.APOrderDetailID,
                                        SaleID = detail.APOrderID,
                                        QuantityOrdered = detail.QuantityOrdered,
                                        WeightOrdered = detail.WeightOrdered,
                                        QuantityReceived = detail.QuantityDelivered,
                                        WeightReceived = detail.WeightDelivered,
                                        UnitPrice = detail.UnitPrice,
                                        DiscountPercentage = detail.DiscountPercentage,
                                        DiscountPrice = detail.DiscountPrice,
                                        LineDiscountPercentage = detail.LineDiscountPercentage,
                                        LineDiscountAmount = detail.LineDiscountAmount,
                                        UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                        VAT = detail.VAT,
                                        TotalExVAT = detail.TotalExclVAT,
                                        TotalIncVAT = detail.TotalIncVAT,
                                        VatCodeID = detail.VATCodeID,
                                        LoadingSale = true,
                                        INMasterID = detail.INMasterID,
                                        PriceListID = detail.PriceListID ?? 0,
                                        NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                        Deleted = detail.Deleted
                                    }).ToList()
                            };

                            documents.Add(localOrder);

                            ProgressBar.Run(1);
                            var quote =
                                entities.APQuotes.FirstOrDefault(x => x.APQuoteID == localOrder.BaseDocumentReferenceID);
                            Sale localQuote;
                            if (quote != null)
                            {
                                localQuote = new Sale
                                {
                                    SaleID = quote.APQuoteID,
                                    DocumentNumberingID = quote.DocumentNumberingID,
                                    Number = quote.Number,
                                    BPContactID = quote.BPContactID,
                                    QuoteValidDate = quote.QuoteValidDate,
                                    DocumentDate = quote.DocumentDate,
                                    CreationDate = quote.CreationDate,
                                    Printed = quote.Printed,
                                    NouDocStatusID = quote.NouDocStatusID,
                                    NouDocStatus = quote.NouDocStatu.Value,
                                    Remarks = quote.Remarks,
                                    TotalExVAT = quote.TotalExVAT,
                                    VAT = quote.VAT,
                                    SubTotalExVAT = quote.SubTotalExVAT,
                                    DiscountIncVAT = quote.DiscountIncVAT,
                                    GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                    DiscountPercentage = quote.DiscountPercentage,
                                    SalesEmployeeID = quote.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                                    InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                                    MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                                    BPHaulier = quote.BPMasterHaulier,
                                    BPCustomer = quote.BPMasterCustomer,
                                    Deleted = quote.Deleted,
                                    DocumentTrailType = Constant.APQuote,
                                    NodeID = 1,
                                    ParentID = 2,
                                    Attachments = (from attachment in quote.APQuoteAttachments
                                                   where attachment.Deleted == null
                                        select new Attachment
                                        {
                                            AttachmentID = attachment.APQuoteAttachmentID,
                                            SaleAttachmentID = attachment.APQuoteID,
                                            AttachmentDate = attachment.AttachmentDate,
                                            FilePath = attachment.FilePath,
                                            FileName = attachment.FileName,
                                            File = attachment.File,
                                            Deleted = attachment.Deleted
                                        }).ToList(),
                                    SaleDetails = (from detail in quote.APQuoteDetails
                                                   where detail.Deleted == null
                                        select new SaleDetail
                                        {
                                            SaleDetailID = detail.APQuoteDetailID,
                                            SaleID = detail.APQuoteID,
                                            QuantityOrdered = detail.QuantityOrdered,
                                            WeightOrdered = detail.WeightOrdered,
                                            QuantityDelivered = detail.QuantityDelivered,
                                            WeightDelivered = detail.WeightDelivered,
                                            UnitPrice = detail.UnitPrice,
                                            DiscountPercentage = detail.DiscountPercentage,
                                            DiscountPrice = detail.DiscountPrice,
                                            LineDiscountPercentage = detail.LineDiscountPercentage,
                                            LineDiscountAmount = detail.LineDiscountAmount,
                                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                            VAT = detail.VAT,
                                            TotalExVAT = detail.TotalExclVAT,
                                            TotalIncVAT = detail.TotalIncVAT,
                                            VatCodeID = detail.VATCodeID,
                                            LoadingSale = true,
                                            INMasterID = detail.INMasterID,
                                            PriceListID = detail.PriceListID ?? 0,
                                            Deleted = detail.Deleted
                                        }).ToList()
                                };

                                documents.Add(localQuote);
                            }
                        }
                    }

                    #endregion

                    #region lairage

                    if (mode == ViewType.LairageIntake)
                    {
                        // check for multiple associated invoice dispatch dockets.
                        var orders = entities.KillPayments.Where(x => x.BaseDocumentReferenceID == documentNo).ToList();
                        var localPayNodeID = 5;
                        foreach (var order in orders)
                        {
                            ProgressBar.SetUp(0, orders.Count());
                            ProgressBar.Run(1);
                            localPayNodeID++;
                            var payDoc = new Sale
                            {
                                SaleID = order.KillPaymentID,
                                DocumentNumberingID = order.DocumentNumberingID,
                                BaseDocumentNumber = order.APGoodsReceipt != null ? order.APGoodsReceipt.Number : 0,
                                NouKillTypeID = order.APGoodsReceipt != null ? order.APGoodsReceipt.NouKillTypeID : (int?)null,
                                Number = order.Number,
                                PaymentDate = order.PaymentDate,
                                DocumentDate = order.DocumentDate,
                                CreationDate = order.CreationDate,
                                ExcludeFromAccounts = order.Exclude ?? false,
                                DeviceId = order.DeviceID,
                                Printed = order.Printed,
                                TotalExVAT = order.TotalExVAT,
                                VAT = order.VAT,
                                SubTotalExVAT = order.SubTotalExVAT,
                                GrandTotalIncVAT = order.GrandTotalIncVAT,
                                DeductionsIncVAT = order.DeductionsIncVAT,
                                DeductionsVAT = order.DeductionsVAT,
                                EditDate = order.EditDate,
                                NouDocStatusID = order.NouDocStatusID,
                                Remarks = order.Remarks,
                                BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                SalesEmployeeID = order.UserMasterID,
                                BPCustomer = order.BPMaster,
                                Deleted = order.Deleted,
                                ChequeNumber = order.ChequeNumber,
                                CarcassesKilled = order.KillPaymentItems.Count(x => x.Deleted == null),
                                CarcassesNotKilled = 0,
                                NodeID = localPayNodeID,
                                ParentID = 4,
                                DocumentTrailType = Strings.Payment
                            };

                            documents.Add(payDoc);
                            localPayNodeID++;
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            finally
            {
                ProgressBar.Reset();
            }

            return documents;
        }

        /// <summary>
        /// Returns the application document status items.
        /// </summary>
        /// <returns>The application document status items.</returns>
        public IList<NouDocStatu> GetNouDocStatusItems()
        {
            this.Log.LogDebug(this.GetType(), "GetNouDocStatusItems(): Attempting to retrieve all the nou doc status items");
            var items = new List<NouDocStatu>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    items = entities.NouDocStatus.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} Nou doc status itemssuccessfully retrieved", items.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return items;
        }

        /// <summary>
        /// Creates a deep copy transaction traceability object.
        /// </summary>
        /// <param name="newTransactionId">The stocktransaction id.</param>
        /// <param name="transactionTraceability">The object to deep copy.</param>
        /// <returns>A deep copied transaction traceability object.</returns>
        private TransactionTraceability CreateDeepCopyTransactionTraceability(int newTransactionId, TransactionTraceability transactionTraceability)
        {
            return new TransactionTraceability
            {
                StockTransactionID = newTransactionId,
                NouTraceabilityMethodID = transactionTraceability.NouTraceabilityMethodID,
                Value = transactionTraceability.Value,
                TraceabilityTemplateMasterID = transactionTraceability.TraceabilityTemplateMasterID,
                QualityMasterID = transactionTraceability.QualityMasterID,
                DateMasterID = transactionTraceability.DateMasterID,
                DynamicName = transactionTraceability.DynamicName
            };
        }

        /// <summary>
        /// Creates a deep copy transaction traceability.
        /// </summary>
        /// <param name="transactionTrace">The transaction trace to deep copy</param>
        /// <returns>A deep copy transaction traceability.</returns>
        private TransactionTraceability CreateDeepCopyTransactionTraceability(TransactionTraceability transactionTrace)
        {
            return new TransactionTraceability
            {
                StockTransactionID = transactionTrace.StockTransactionID,
                NouTraceabilityMethodID = transactionTrace.NouTraceabilityMethodID,
                Value = transactionTrace.Value,
                Deleted = transactionTrace.Deleted,
                TraceabilityTemplateMasterID = transactionTrace.TraceabilityTemplateMasterID,
                DateMasterID = transactionTrace.DateMasterID,
                QualityMasterID = transactionTrace.QualityMasterID,
                DynamicName = transactionTrace.DynamicName
            };
        }

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        private IList<NouPriceMethod> GetPriceMethods()
        {
            this.Log.LogDebug(this.GetType(), "GetPriceMethods(): Attempting to retrieve all the Price methods");
            var priceMethods = new List<NouPriceMethod>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceMethods = entities.NouPriceMethods.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} price methods retrieved", priceMethods.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return priceMethods;
        }

        #endregion

        #region returns

        /// <summary> Adds a new stock return (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddARReturn(Sale sale)
        {
            var localStockTransactionId = 0;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new ARReturn
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = sale.Remarks,
                        TotalExVAT = sale.TotalExVAT.ToDecimal(),
                        DeliveryDate = sale.DeliveryDate,
                        GoodsReceiptDate = DateTime.Now,
                        VAT = sale.VAT,
                        PopUpNote = sale.PopUpNote,
                        IntakeNote = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BPMasterID_Supplier = sale.Customer.BPMasterID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        UserID_CreatedBy = NouvemGlobal.UserId,
                        Reference = sale.Reference
                    };

                    if (sale.DeliveryAddress != null)
                    {
                        order.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        order.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.ARReturns.Add(order);

                    // pass the newly created apgoodsreceipt id back out.
                    sale.SaleID = order.ARReturnID;
                    entities.SaveChanges();

                    // save the details
                    var saleID = order.ARReturnID;
                    foreach (var detail in sale.SaleDetails)
                    {
                        if (detail != null)
                        {
                            var orderDetail = new ARReturnDetail
                            {
                                ARReturnID = saleID,
                                QuantityOrdered = detail.QuantityOrdered,
                                QuantityReceived = detail.QuantityReceived,
                                WeightReceived = detail.WeightReceived,
                                WeightOrdered = detail.WeightOrdered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountAmount = detail.DiscountAmount,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                Notes = detail.Notes,
                                BatchNumberID = detail.BatchNumberId,
                                VATCodeID = detail.VatCodeID,
                                TotalExcVAT = detail.TotalExVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterID = detail.INMasterID
                            };

                            entities.ARReturnDetails.Add(orderDetail);
                            entities.SaveChanges();

                            // pass the apgoodsreceiptdetail id back out
                            detail.SaleDetailID = orderDetail.ARReturnDetailID;

                            #region stock transaction/traceability

                            var stockDetail = detail.StockDetailToProcess;
                            if (stockDetail != null)
                            {
                                if (stockDetail.Transaction != null)
                                {
                                    // scanned stock
                                    stockDetail.Transaction.MasterTableID = detail.SaleDetailID;
                                    entities.StockTransactions.Add(stockDetail.Transaction);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var localAttribute = this.CreateAttribute(stockDetail);
                                    entities.Attributes.Add(localAttribute);
                                    entities.SaveChanges();

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    if (localTransaction.TransactionQTY.IsNullOrZero())
                                    {
                                        localTransaction.TransactionQTY = 1;
                                    }

                                    localTransaction.MasterTableID = orderDetail.ARReturnDetailID;
                                    localTransaction.Comments = stockDetail.Comments;
                                    localTransaction.NouTransactionTypeID = 9;
                                    localTransaction.AttributeID = localAttribute.AttributeID;

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);
                                    entities.StockTransactions.Add(localTransaction);
                                    entities.SaveChanges();
                                    localStockTransactionId = localTransaction.StockTransactionID;
                                    localTransaction.Serial = localTransaction.StockTransactionID;
                                    entities.SaveChanges();
                                }
                            }

                            #endregion
                        }
                    }

                    return Tuple.Create(order.ARReturnID, localStockTransactionId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, 0);
        }

        /// <summary>
        /// Updates an existing return (receipt and details).
        /// </summary>
        /// <param name="sale">The return receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARReturn(Sale sale)
        {
            var batchNoId = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder =
                        entities.ARReturns.FirstOrDefault(x => x.ARReturnID == sale.SaleID && x.Deleted == null);

                    if (dbOrder != null)
                    {
                        dbOrder.TotalExVAT = sale.TotalExVAT.ToDecimal();
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = sale.SubTotalExVAT;
                        dbOrder.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Today;
                        dbOrder.Reference = sale.Reference;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.NouDocStatusID = sale.IsOrderFilled()
                            ? NouvemGlobal.NouDocStatusFilled.NouDocStatusID
                            : NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;

                        if (!ApplicationSettings.TouchScreenMode)
                        {
                            //dbOrder.BPMasterID_Supplier = sale.Customer.BPMasterID;
                            dbOrder.BPContactID = sale.MainContact != null
                                ? (int?)sale.MainContact.Details.BPContactID
                                : null;
                            dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                            dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null
                                ? (int?)sale.DeliveryAddress.Details.BPAddressID
                                : null;
                            dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null
                                ? (int?)sale.InvoiceAddress.Details.BPAddressID
                                : null;
                            dbOrder.DocumentDate = sale.DocumentDate;
                            dbOrder.CreationDate = sale.CreationDate;
                            dbOrder.DiscountPercentage = sale.DiscountPercentage;
                            dbOrder.Remarks = sale.Remarks;
                            dbOrder.InvoiceNote = sale.InvoiceNote;
                            dbOrder.IntakeNote = sale.DocketNote;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.UserMasterID = sale.SalesEmployeeID;
                            dbOrder.Deleted = sale.Deleted;
                        }

                        var detail = sale.SaleDetails.FirstOrDefault(x => x.Process);
                        if (detail.SaleDetailID != 0)
                        {
                            // existing sale item
                            var dbOrderDetail =
                                entities.ARReturnDetails.FirstOrDefault(
                                    x => x.ARReturnDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbOrderDetail == null)
                            {
                                // serious error - should never happen
                                this.Log.LogError(this.GetType(),
                                    string.Format("APReceiptDetail:{0} not found in the database",
                                        detail.SaleDetailID));
                                return false;
                            }

                            dbOrderDetail.QuantityReceived = detail.QuantityReceived;
                            dbOrderDetail.WeightReceived = detail.WeightReceived;
                            dbOrderDetail.BatchNumberID = detail.BatchNumberId;

                            #region stock transaction/traceability

                            var stockDetail = detail.StockDetailToProcess;
                            if (stockDetail != null)
                            {
                                if (stockDetail.Transaction != null)
                                {
                                    // scanned stock
                                    stockDetail.Transaction.MasterTableID = detail.SaleDetailID;
                                    entities.StockTransactions.Add(stockDetail.Transaction);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var localAttribute = this.CreateAttribute(stockDetail);
                                    entities.Attributes.Add(localAttribute);
                                    entities.SaveChanges();

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    localTransaction.MasterTableID = dbOrderDetail.ARReturnDetailID;
                                    if (localTransaction.TransactionQTY.IsNullOrZero())
                                    {
                                        localTransaction.TransactionQTY = 1;
                                    }

                                    localTransaction.Comments = stockDetail.Comments;
                                    localTransaction.NouTransactionTypeID = 9;
                                    localTransaction.AttributeID = localAttribute.AttributeID;

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x =>
                                                    x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID,
                                        localTransaction.NouTransactionTypeID);

                                    entities.StockTransactions.Add(localTransaction);
                                    entities.SaveChanges();
                                    sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                    localTransaction.Serial = localTransaction.StockTransactionID;
                                    entities.SaveChanges();
                                }
                            }

                            #endregion

                            entities.SaveChanges();
                        }
                        else
                        {
                            var orderDetail = new ARReturnDetail
                            {
                                ARReturnID = dbOrder.ARReturnID,
                                QuantityOrdered = detail.QuantityOrdered,
                                QuantityReceived = detail.QuantityReceived,
                                WeightOrdered = detail.WeightOrdered,
                                WeightReceived = detail.WeightReceived,
                                UnitPrice = detail.UnitPrice,
                                BatchNumberID = detail.BatchNumberId,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountAmount = detail.DiscountAmount,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                Notes = detail.Notes,
                                VATCodeID = detail.VatCodeID,
                                TotalExcVAT = detail.TotalExVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterID = detail.INMasterID
                            };

                            entities.ARReturnDetails.Add(orderDetail);
                            entities.SaveChanges();

                            // pass the apgoodsreceiptdetail id back out
                            detail.SaleDetailID = orderDetail.ARReturnDetailID;

                            #region stock transaction/traceability

                            var stockDetail = detail.StockDetailToProcess;
                            if (stockDetail != null)
                            {
                                if (stockDetail.Transaction != null)
                                {
                                    // scanned stock
                                    stockDetail.Transaction.MasterTableID = detail.SaleDetailID;
                                    entities.StockTransactions.Add(stockDetail.Transaction);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var localAttribute = this.CreateAttribute(stockDetail);
                                    entities.Attributes.Add(localAttribute);
                                    entities.SaveChanges();

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    localTransaction.MasterTableID = orderDetail.ARReturnDetailID;
                                    localTransaction.NouTransactionTypeID = 9;
                                    localTransaction.AttributeID = localAttribute.AttributeID;
                                    localTransaction.Comments = stockDetail.Comments;

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x =>
                                                    x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID,
                                        localTransaction.NouTransactionTypeID);

                                    entities.StockTransactions.Add(localTransaction);
                                    entities.SaveChanges();
                                    sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                    localTransaction.Serial = localTransaction.StockTransactionID;
                                    entities.SaveChanges();
                                }
                            }

                            #endregion
                        }

                    }

                    return true;
                }

            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the returns (and associated details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetReturnDetails_Result> GetReturnsStatusDetails(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetReturnDetails(id).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetARReturns(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARReturns
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.ARReturnID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPMasterID = order.BPMasterID_Supplier,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  TechnicalNotes = order.Notes,
                                  UserIDModified = order.UserMasterID,
                                  Reference = order.Reference,
                                  Address = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == order.BPAddressID_Delivery),
                                  BPCustomer = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == order.BPMasterID_Supplier),
                                  UserIDCreation = order.UserID_CreatedBy,
                                  UserIDCompletion = order.UserID_CompletedBy,
                                  Deleted = order.Deleted,
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetARReturnsNonInvoiced()
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.ARReturns
                              where order.Deleted == null
                              && order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                              && order.Invoiced != true
                              orderby order.Number descending
                              select new Sale
                              {
                                  SaleID = order.ARReturnID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPMasterID = order.BPMasterID_Supplier,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  TechnicalNotes = order.Notes,
                                  UserIDModified = order.UserMasterID,
                                  Reference = order.Reference,
                                  Address = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == order.BPAddressID_Delivery),
                                  BPCustomer = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == order.BPMasterID_Supplier),
                                  UserIDCreation = order.UserID_CreatedBy,
                                  UserIDCompletion = order.UserID_CompletedBy,
                                  Deleted = order.Deleted,
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Gets the return details.
        /// </summary>
        /// <param name="id">The returns id to search for.</param>
        /// <returns>A return, and it's details.</returns>
        public Sale GetARReturnById(int id)
        {
            var dbOrder = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARReturns.FirstOrDefault(x => x.ARReturnID == id);
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    dbOrder = new Sale
                    {
                        SaleID = order.ARReturnID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        BPMasterID = order.BPMasterID_Supplier,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        SaleType = ViewType.APReceipt,
                        DeviceId = order.DeviceID,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        //NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        DeliveryDate = order.DeliveryDate,
                        VAT = order.VAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        //DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        //InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        //MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        //BPHaulier = order.BPMasterHaulier,

                        Reference = order.Reference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.ARReturnDetails.Where(x => x.Deleted == null)
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARReturnDetailID,
                                           SaleID = detail.ARReturnID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           //Batch = detail.BatchNumber,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           SaleDetailType = ViewType.APReceipt,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            dbOrder.BPCustomer = new BPMaster { BPMasterID = dbOrder.BPMasterID.ToInt() };
            return dbOrder;
        }

        /// <summary>
        /// Gets the return details by last edit date.
        /// </summary>
        /// <returns>A return, and it's details.</returns>
        public Sale GetARReturnByLastEdit()
        {
            var dbOrder = new Sale();
            try
            {
                int returnsid;
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARReturns.OrderByDescending(x => x.EditDate).FirstOrDefault();
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    returnsid = order.ARReturnID;
                    this.PriceReturnsDocket(returnsid);
                }

                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARReturns.FirstOrDefault(x => x.ARReturnID == returnsid);
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    dbOrder = new Sale
                    {
                        SaleID = order.ARReturnID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        BPMasterID = order.BPMasterID_Supplier,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        SaleType = ViewType.APReceipt,
                        DeviceId = order.DeviceID,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        //NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        DeliveryDate = order.DeliveryDate,
                        VAT = order.VAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        //DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        //InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        //MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        //BPHaulier = order.BPMasterHaulier,

                        Reference = order.Reference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.ARReturnDetails.Where(x => x.Deleted == null)
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARReturnDetailID,
                                           SaleID = detail.ARReturnID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           //Batch = detail.BatchNumber,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           SaleDetailType = ViewType.APReceipt,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            dbOrder.BPCustomer = new BPMaster { BPMasterID = dbOrder.BPMasterID.ToInt() };
            return dbOrder;
        }

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        public bool UpdateARReturnStatus(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.ARReturns.FirstOrDefault(x => x.ARReturnID == sale.SaleID);
                    if (order != null)
                    {
                        order.NouDocStatusID = sale.NouDocStatusID;
                        order.DeviceID = NouvemGlobal.DeviceId;
                        order.EditDate = DateTime.Now;

                        if (order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                        {
                            order.UserID_CompletedBy = NouvemGlobal.UserId;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceReturnsDocket(int docketId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_PriceReturnsDocket(docketId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddARReturnInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (!sale.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        var alreadyInvoiced = entities.ARReturnInvoices.Any(x =>
                            x.BaseDocumentReferenceID == sale.BaseDocumentReferenceID && x.Deleted == null &&
                            x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID);

                        if (alreadyInvoiced)
                        {
                            return Tuple.Create(0, new DocNumber { DocumentNumberingTypeID = 1000 });
                        }
                    }

                    var dbNumber =
                        entities.DocumentNumberings.First(
                            x =>
                                x.NouDocumentName.DocumentName.Equals(Constant.ARReturnInvoice) &&
                                x.DocumentNumberingType.Name.Equals(Constant.Standard));

                    sale.Number = dbNumber.NextNumber;
                    sale.DocumentNumberingID = dbNumber.DocumentNumberingID;

                    var currentNo = dbNumber.NextNumber;
                    var nextNo = dbNumber.NextNumber + 1;
                    if (nextNo > dbNumber.LastNumber)
                    {
                        // Last number reached, so reset back to first number.
                        nextNo = dbNumber.FirstNumber;
                    }

                    dbNumber.NextNumber = nextNo;
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document number updated");

                    var order = new ARReturnInvoice
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null && sale.MainContact.Details != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        BPContactID_Delivery = sale.DeliveryContact != null && sale.DeliveryContact.Details != null ? (int?)sale.DeliveryContact.Details.BPContactID : null,
                        BPMasterID_Supplier = sale.BPCustomer != null ? sale.BPCustomer.BPMasterID : sale.Customer != null ? sale.Customer.BPMasterID : (int?)null,
                        BPMasterID_Haulier = sale.BPHaulier != null ? sale.BPHaulier.BPMasterID : (int?)null,
                        BPAddressID_Delivery = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.BPAddressID : (int?)null,
                        BPAddressID_Invoice = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.BPAddressID : (int?)null,
                        RouteID = sale.RouteID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        DeliveryDate = sale.DeliveryDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                        CustomerPOReference = sale.CustomerPOReference,
                        Remarks = sale.Remarks,
                        InvoiceNote = sale.InvoiceNote,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        PORequired = sale.PORequired
                    };

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.ARReturns.FirstOrDefault(x => x.ARReturnID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            if (baseDoc.Invoiced == true)
                            {
                                // already invoiced, return;
                                return Tuple.Create(0, new DocNumber());
                            }

                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            baseDoc.Invoiced = true;
                            this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }
                    //else if (sale.IdsToMarkAsInvoiced != null && sale.IdsToMarkAsInvoiced.Any())
                    //{
                    //    // Consolidated invoice created, so we mark all the associated intake dockets as invoiced.
                    //    var returnsToInvoice =
                    //        entities.ARReturns.Where(
                    //            x => sale.IdsToMarkAsInvoiced.Contains(x.ARReturnID) && x.Invoiced != true);

                    //    foreach (var docket in returnsToInvoice)
                    //    {
                    //        docket.Invoiced = true;
                    //        docket.APInvoiceID = order.APInvoiceID;
                    //    }
                    //}

                    entities.ARReturnInvoices.Add(order);
                    entities.SaveChanges();

                    var docNumber =
                    new DocNumber
                    {
                        DocumentNumberingID = dbNumber.DocumentNumberingID,
                        NouDocumentNameID = dbNumber.NouDocumentNameID,
                        DocumentNumberingTypeID = dbNumber.DocumentNumberingTypeID,
                        FirstNumber = dbNumber.FirstNumber,
                        LastNumber = dbNumber.LastNumber,
                        CurrentNumber = currentNo,
                        NextNumber = dbNumber.NextNumber,
                        DocName = dbNumber.NouDocumentName,
                        DocType = dbNumber.DocumentNumberingType
                    };

                    // save the details
                    var saleID = order.ARReturnInvoiceID;
                    var localInvoiceDetails = new List<ARReturnInvoiceDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        var invoiceDetail = new ARReturnInvoiceDetail
                        {
                            ARReturnInvoiceID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            QuantityReceived = detail.QuantityReceived,
                            WeightReceived = detail.WeightReceived,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterID = detail.INMasterID
                        };

                        entities.ARReturnInvoiceDetails.Add(invoiceDetail);
                        entities.SaveChanges();
                    }

                    this.Log.LogDebug(this.GetType(), "New invoice successfully created");
                    return Tuple.Create(order.ARReturnInvoiceID, docNumber);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, new DocNumber());
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateARReturnInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbInvoice = entities.ARReturnInvoices.FirstOrDefault(x => x.ARReturnInvoiceID == sale.SaleID && x.Deleted == null);

                    if (dbInvoice != null)
                    {
                        dbInvoice.DeviceID = NouvemGlobal.DeviceId;
                        dbInvoice.EditDate = DateTime.Now;

                        if (sale.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Cancelling invoice...Base Doc Id:{0}", dbInvoice.BaseDocumentReferenceID.ToInt()));

                            // cancelling invoice
                            dbInvoice.NouDocStatusID = sale.NouDocStatusID;

                            if (dbInvoice.BaseDocumentReferenceID != null)
                            {
                                this.Log.LogDebug(this.GetType(), "Cancelling single invoice...");

                                // single dispath docket used for invoice.
                                var intake =
                                    entities.APGoodsReceipts.FirstOrDefault(
                                        x => x.APGoodsReceiptID == dbInvoice.BaseDocumentReferenceID);

                                if (intake != null)
                                {
                                    intake.Invoiced = null;
                                }
                            }

                            entities.SaveChanges();
                            return true;
                        }

                        dbInvoice.BPMasterID_Supplier = sale.Customer.BPMasterID;
                        dbInvoice.BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null;
                        dbInvoice.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbInvoice.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbInvoice.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbInvoice.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbInvoice.RouteID = sale.RouteID;
                        dbInvoice.BaseDocumentReferenceID = sale.BaseDocumentReferenceID;
                        dbInvoice.DocumentDate = sale.DocumentDate;
                        dbInvoice.CreationDate = sale.CreationDate;
                        dbInvoice.DeliveryDate = sale.DeliveryDate;
                        dbInvoice.NouDocStatusID = sale.NouDocStatusID;
                        dbInvoice.Remarks = sale.Remarks;
                        dbInvoice.InvoiceNote = sale.InvoiceNote;
                        dbInvoice.TotalExVAT = sale.TotalExVAT;
                        dbInvoice.DiscountPercentage = sale.DiscountPercentage;
                        dbInvoice.VAT = sale.VAT;
                        dbInvoice.DiscountIncVAT = sale.DiscountIncVAT;
                        dbInvoice.SubTotalExVAT = sale.SubTotalExVAT;
                        dbInvoice.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbInvoice.UserMasterID = sale.SalesEmployeeID;
                        dbInvoice.PORequired = sale.PORequired;
                        dbInvoice.Deleted = sale.Deleted;

                        // if we used a base document to create this, then we mark the base document as closed.
                        if (!dbInvoice.BaseDocumentReferenceID.IsNullOrZero())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", dbInvoice.BaseDocumentReferenceID));
                            var baseDoc = entities.ARReturns.FirstOrDefault(x => x.ARReturnID == dbInvoice.BaseDocumentReferenceID);
                            if (baseDoc != null)
                            {
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                                baseDoc.Invoiced = true;
                                this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", dbInvoice.BaseDocumentReferenceID));
                            }
                        }

                        var localInvoiceDetails = new List<ARReturnInvoiceDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            var dbInvoiceDetail =
                                entities.ARReturnInvoiceDetails.FirstOrDefault(
                                    x => x.ARReturnInvoiceDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbInvoiceDetail != null)
                            {
                                // existing sale item
                                dbInvoiceDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbInvoiceDetail.WeightOrdered = detail.WeightOrdered;
                                dbInvoiceDetail.QuantityReceived = detail.QuantityDelivered;
                                dbInvoiceDetail.WeightReceived = detail.WeightDelivered;
                                dbInvoiceDetail.UnitPrice = detail.UnitPrice;
                                dbInvoiceDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbInvoiceDetail.DiscountAmount = detail.DiscountAmount;
                                dbInvoiceDetail.DiscountPrice = detail.DiscountPrice;
                                dbInvoiceDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbInvoiceDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbInvoiceDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbInvoiceDetail.VAT = detail.VAT;
                                dbInvoiceDetail.VATCodeID = detail.VatCodeID;
                                dbInvoiceDetail.TotalExclVAT = detail.TotalExVAT;
                                dbInvoiceDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbInvoiceDetail.INMasterID = detail.INMasterID;
                                dbInvoiceDetail.PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID;
                                dbInvoiceDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order
                                var invoiceDetail = new ARReturnInvoiceDetail
                                {
                                    ARReturnInvoiceID = sale.SaleID,
                                    QuantityReceived = detail.QuantityReceived,
                                    WeightReceived = detail.WeightReceived,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterID = detail.INMasterID
                                };

                                localInvoiceDetails.Add(invoiceDetail);
                            }
                        }

                        if (localInvoiceDetails.Any())
                        {
                            entities.ARReturnInvoiceDetails.AddRange(localInvoiceDetails);
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Invoice successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the ap return invoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetARReturnInvoices()
        {
            this.Log.LogDebug(this.GetType(), "GetNonExportedInvoices(): Attempting to get the non exported invoices");
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.ARReturnInvoices
                                where
                                invoice.Deleted == null
                                orderby invoice.ARReturnInvoiceID descending
                                select new Sale
                                {
                                    SaleID = invoice.ARReturnInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    Number = invoice.Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    ExcludeFromPurchaseAccounts = invoice.Exclude,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPCustomer = invoice.BPMaster1,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted
                                }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetARReturnInvoiceByID(int id)
        {
            Sale localInvoice = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoice = entities.ARReturnInvoices.FirstOrDefault(x => x.ARReturnInvoiceID == id);
                    if (invoice == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Invoice not found");
                        return null;
                    }

                    localInvoice = new Sale
                    {
                        SaleID = invoice.ARReturnInvoiceID,
                        DocumentNumberingID = invoice.DocumentNumberingID,
                        Number = invoice.Number,
                        DeviceId = invoice.DeviceID,
                        EditDate = invoice.EditDate,
                        BPContactID = invoice.BPContactID,
                        RouteID = invoice.RouteID,
                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                        DocumentDate = invoice.DocumentDate,
                        CreationDate = invoice.CreationDate,
                        DeliveryDate = invoice.DeliveryDate,
                        Printed = invoice.Printed,
                        NouDocStatusID = invoice.NouDocStatusID,
                        NouDocStatus = invoice.NouDocStatu.Value,
                        Remarks = invoice.Remarks,
                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                        InvoiceNote = invoice.InvoiceNote,
                        TotalExVAT = invoice.TotalExVAT,
                        VAT = invoice.VAT,
                        SubTotalExVAT = invoice.SubTotalExVAT,
                        DiscountIncVAT = invoice.DiscountIncVAT,
                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                        DiscountPercentage = invoice.DiscountPercentage,
                        OtherReference = invoice.OtherReference,
                        SalesEmployeeID = invoice.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                        BPCustomer = invoice.BPMaster1,
                        PORequired = invoice.PORequired,
                        Deleted = invoice.Deleted,
                        SaleDetails = (from detail in invoice.ARReturnInvoiceDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.ARReturnInvoiceDetailID,
                                           SaleID = detail.ARReturnInvoiceID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityReceived,
                                           WeightDelivered = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localInvoice;
        }

        /// <summary>
        /// Exports credit note details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportARReturnInvoice(IList<int> invoiceIds)
        {
            this.Log.LogDebug(this.GetType(), "ExportInvoices(): Exporting invoices..");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoiceId in invoiceIds)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to export invoice id:{0}", invoiceId));
                        var invoice = entities.ARReturnInvoices.FirstOrDefault(x => x.ARReturnInvoiceID == invoiceId);
                        if (invoice != null)
                        {
                            invoice.NouDocStatusID = NouvemGlobal.NouDocStatusExported.NouDocStatusID;
                            invoice.EditDate = DateTime.Now;
                            this.Log.LogDebug(this.GetType(), "Invoice found and marked for export");
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), "Invoice not found");
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Invoices exported");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion
    }
}

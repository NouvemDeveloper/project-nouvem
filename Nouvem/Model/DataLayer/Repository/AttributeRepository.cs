﻿// -----------------------------------------------------------------------
// <copyright file="AttributeRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using DevExpress.Xpf.Editors.Helpers;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer.Interface;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AttributeRepository : NouvemRepositoryBase, IAttributeRepository
    {
        /// <summary>
        /// Adds a new attribute.
        /// </summary>
        /// <param name="attributeData">The attribute and associated data to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddAttribute(AttributeSetUp attributeData)
        {
            var attributeMaster = attributeData.AttributeMaster;
            var userAlerts = attributeData.UserAlerts;
            var userGroupAlerts = attributeData.UserGroupAlerts;
            var userGroupsNonStandard = attributeData.UserGroupsNonStandard;
            var attributeName = attributeData.AttributeLookUpName;
            var attachments = attributeData.AttributeAttachments;
            var userPermissions = attributeData.UserPermissions;
            var userGroupPermissions = attributeData.UserGroupPermissions;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.AttributeMasters.Add(attributeMaster);
                    entities.SaveChanges();

                    // add the alerts
                    if (userAlerts != null)
                    {
                        foreach (var userAlert in userAlerts)
                        {
                            userAlert.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserAlerts.Add(userAlert);
                        }
                    }

                    if (userGroupAlerts != null)
                    {
                        foreach (var userGroupAlert in userGroupAlerts)
                        {
                            userGroupAlert.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserGroupAlerts.Add(userGroupAlert);
                        }
                    }

                    if (userPermissions != null)
                    {
                        foreach (var userPermission in userPermissions)
                        {
                            userPermission.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserAttributePermissions.Add(userPermission);
                        }
                    }

                    if (userGroupPermissions != null)
                    {
                        foreach (var userGroupPermission in userGroupPermissions)
                        {
                            userGroupPermission.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserGroupAttributePermissions.Add(userGroupPermission);
                        }
                    }

                    if (userGroupsNonStandard != null)
                    {
                        // add the non standard respose authorised groups
                        foreach (var userGroupNonStandard in userGroupsNonStandard)
                        {
                            userGroupNonStandard.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserGroupNonStandards.Add(userGroupNonStandard);
                        }
                    }

                    if (attachments != null)
                    {
                        // add the attachments
                        foreach (var attachment in attachments)
                        {
                            attachment.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.AttributeAttachments.Add(attachment);
                        }
                    }

                    if (!attributeMaster.IsWorkflow.ToBool() && !string.IsNullOrWhiteSpace(attributeName))
                    {
                        // add the attribute look up
                        var attributeLookUp = new AttributeLookup
                        {
                            Attribute_Name = attributeName,
                            AttributeMasterID = attributeMaster.AttributeMasterID
                        };

                        entities.AttributeLookups.Add(attributeLookUp);
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a range of attributes.
        /// </summary>
        /// <param name="attributes">The attributes to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddAttributes(IList<Model.DataLayer.Attribute> attributes)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Attributes.AddRange(attributes);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the reset values.
        /// </summary>
        /// <returns>A collection of reset values.</returns>
        public IList<NouAttributeReset> GetAttributeResets()
        {
            var tabs = new List<NouAttributeReset>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    tabs = entities.NouAttributeResets.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return tabs;
        }

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        /// <returns>A collection of tabs.</returns>
        public IList<AttributeTabName> GetAttributeTabNames()
        {
            var tabs = new List<AttributeTabName>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    tabs = entities.AttributeTabNames.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return tabs;
        }

        /// <summary>
        /// Adds/Updates tabs.
        /// </summary>
        /// <param name="tabs">The tabs to update.</param>
        /// <returns>Flag, as to successful add/update.</returns>
        public bool UpdateTabs(IList<AttributeTabName> tabs)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var attributeTabName in tabs)
                    {
                        if (attributeTabName.AttributeTabNameID == 0)
                        {
                            entities.AttributeTabNames.Add(attributeTabName);
                        }
                        else
                        {
                            var tab = entities.AttributeTabNames.FirstOrDefault(x =>
                                x.AttributeTabNameID == attributeTabName.AttributeTabNameID);
                            if (tab != null)
                            {
                                tab.Name = attributeTabName.Name;
                                tab.OrderIndex = attributeTabName.OrderIndex;
                                tab.Deleted = attributeTabName.Deleted;
                            }
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates an attribute.
        /// </summary>
        /// <param name="attributeData">The attribute and associated data to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateAttribute(AttributeSetUp attributeData)
        {
            var attributeMaster = attributeData.AttributeMaster;
            var userAlerts = attributeData.UserAlerts;
            var userGroupAlerts = attributeData.UserGroupAlerts;
            var userGroupsNonStandard = attributeData.UserGroupsNonStandard;
            var attributeName = attributeData.AttributeLookUpName;
            var userPermissions = attributeData.UserPermissions;
            var userGroupPermissions = attributeData.UserGroupPermissions;
            var attachments = attributeData.AttributeAttachments;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute =
                        entities.AttributeMasters.FirstOrDefault(
                            x => x.AttributeMasterID == attributeMaster.AttributeMasterID);
                    if (dbAttribute == null)
                    {
                        return false;
                    }

                    dbAttribute.Code = attributeMaster.Code;
                    dbAttribute.IsWorkflow = attributeMaster.IsWorkflow;
                    dbAttribute.Description = attributeMaster.Description;
                    dbAttribute.NouTraceabilityTypeID = attributeMaster.NouTraceabilityTypeID;
                    dbAttribute.NouTraceabilityTypeID_NonStd = attributeMaster.NouTraceabilityTypeID_NonStd;
                    dbAttribute.SQL = attributeMaster.SQL;
                    dbAttribute.SQL_NonStd = attributeMaster.SQL_NonStd;
                    dbAttribute.Collection = attributeMaster.Collection;
                    dbAttribute.Collection_NonStd = attributeMaster.Collection_NonStd;
                    dbAttribute.MessageText = attributeMaster.MessageText;
                    dbAttribute.MessageText_NonStd = attributeMaster.MessageText_NonStd;
                    dbAttribute.StandardResponseChecker = attributeMaster.StandardResponseChecker;
                    dbAttribute.LogAlertNonStd = attributeMaster.LogAlertNonStd;
                    dbAttribute.LoadMACRO = attributeMaster.LoadMACRO;
                    dbAttribute.PostSelectionMACRO = attributeMaster.PostSelectionMACRO;
                    dbAttribute.AlertMessage = attributeMaster.AlertMessage;
                    dbAttribute.UserMasterID = attributeMaster.UserMasterID;
                    dbAttribute.DeviceMasterID = attributeMaster.DeviceMasterID;
                    dbAttribute.EditDate = attributeMaster.EditDate;
                    dbAttribute.NouDateTypeID = attributeMaster.NouDateTypeID;
                    dbAttribute.AttributeMasterID_BasedOn = attributeMaster.AttributeMasterID_BasedOn;
                    dbAttribute.Notes = attributeMaster.Notes;

                    var dbUserPermissions =
                        entities.UserAttributePermissions.Where(
                            x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                    foreach (var dbUserPermission in dbUserPermissions)
                    {
                        dbUserPermission.Deleted = DateTime.Now;
                    }

                    if (userPermissions != null)
                    {
                        foreach (var userPermission in userPermissions)
                        {
                            userPermission.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserAttributePermissions.Add(userPermission);
                        }
                    }

                    var dbUserGroupPermissions =
                        entities.UserGroupAttributePermissions.Where(
                            x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                    foreach (var dbUserGroupPermission in dbUserGroupPermissions)
                    {
                        dbUserGroupPermission.Deleted = DateTime.Now;
                    }

                    if (userGroupPermissions != null)
                    {
                        foreach (var userGroupPermission in userGroupPermissions)
                        {
                            userGroupPermission.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserGroupAttributePermissions.Add(userGroupPermission);
                        }
                    }

                    var dbUserAlerts =
                        entities.UserAlerts.Where(
                            x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                    foreach (var dbUserAlert in dbUserAlerts)
                    {
                        dbUserAlert.Deleted = DateTime.Now;
                    }

                    // add the alerts
                    if (userAlerts != null)
                    {
                        foreach (var userAlert in userAlerts)
                        {
                            userAlert.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserAlerts.Add(userAlert);
                        }
                    }

                    var dbUserGroupAlerts =
                        entities.UserGroupAlerts.Where(
                            x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                    foreach (var dbUserGroupAlert in dbUserGroupAlerts)
                    {
                        dbUserGroupAlert.Deleted = DateTime.Now;
                    }

                    if (userGroupAlerts != null)
                    {
                        foreach (var userGroupAlert in userGroupAlerts)
                        {
                            userGroupAlert.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserGroupAlerts.Add(userGroupAlert);
                        }
                    }

                    var dbGroupsNonStandard =
                        entities.UserGroupNonStandards.Where(
                            x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                    foreach (var dbGroup in dbGroupsNonStandard)
                    {
                        dbGroup.Deleted = DateTime.Now;
                    }

                    if (userGroupsNonStandard != null)
                    {
                        // add the non standard respose authorised groups
                        foreach (var userGroupNonStandard in userGroupsNonStandard)
                        {
                            userGroupNonStandard.AttributeMasterID = attributeMaster.AttributeMasterID;
                            entities.UserGroupNonStandards.Add(userGroupNonStandard);
                        }
                    }

                    if (!attributeMaster.IsWorkflow.ToBool() && !string.IsNullOrWhiteSpace(attributeName))
                    {
                        var alreadyInLookUp =
                            entities.AttributeLookups.Any(
                                x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                        if (!alreadyInLookUp)
                        {
                            // add the attribute look up
                            var attributeLookUp = new AttributeLookup
                            {
                                Attribute_Name = attributeName,
                                AttributeMasterID = attributeMaster.AttributeMasterID
                            };

                            entities.AttributeLookups.Add(attributeLookUp);
                        }
                    }
                    else if (attributeMaster.IsWorkflow.ToBool())
                    {
                        var dbLookUp =
                            entities.AttributeLookups.FirstOrDefault(
                                x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                        if (dbLookUp != null)
                        {
                            dbLookUp.Deleted = DateTime.Now;
                        }
                    }

                    //var dbAttachments =
                    //   entities.AttributeAttachments.Where(
                    //       x => x.AttributeMasterID == attributeMaster.AttributeMasterID && x.Deleted == null);

                    //foreach (var dbAttachment in dbAttachments)
                    //{
                    //    dbAttachment.Deleted = DateTime.Now;
                    //}

                    //if (attachments != null)
                    //{
                    //    // add the attachments
                    //    foreach (var attachment in attachments)
                    //    {
                    //        attachment.AttributeMasterID = attributeMaster.AttributeMasterID;
                    //        entities.AttributeAttachments.Add(attachment);
                    //    }
                    //}

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds an attribute to a template.
        /// </summary>
        /// <param name="traceabilityMasters">The collection of the attributes to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddAttributesToTemplate(IList<AttributeMasterData> traceabilityMasters, int templateId, bool isWorkflow, string notes, bool inactive, string attributeType)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddTraceabilitysToTemplate(): Attempting to add {0} traceability masters to template id:{1}", traceabilityMasters.Count, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localTemplate =
                            entities.AttributeTemplates.FirstOrDefault(x => x.AttributeTemplateID == templateId);
                    if (localTemplate == null)
                    {
                        this.Log.LogError(this.GetType(), string.Format("Template id:{0} does not exist", templateId));
                        return false;
                    }

                    localTemplate.Notes = notes;
                    localTemplate.Inactive = inactive;
                    localTemplate.IsWorkflow = isWorkflow;
                    localTemplate.AttributeType = attributeType;

                    if (!traceabilityMasters.Any())
                    {
                        // we're not updating the attributes.
                        entities.SaveChanges();
                        return true;
                    }

                    ProgressBar.Run();

                    // Remove the current template quality fields.
                    var currentTMs =
                        entities.AttributeAllocations.Where(x => x.AttributeTemplateID == templateId);
                    //entities.AttributeAllocations.RemoveRange(currentTMs);
                    //entities.SaveChanges();

                    var attributeIds = currentTMs.Select(x => (int?)x.AttributeAllocationID);

                    var currentEdits =
                        entities.AttributeEditInProcesses.Where(x => attributeIds.Contains(x.AttributeAlocationID));
                    //entities.AttributeEditInProcesses.RemoveRange(currentEdits);

                    var currentVis =
                        entities.AttributeVisibleInProcesses.Where(x => attributeIds.Contains(x.AttributeAllocationID));
                    //entities.AttributeVisibleInProcesses.RemoveRange(currentVis);

                    //entities.SaveChanges();
                    entities.AttributeAllocations.RemoveRange(currentTMs);
                    entities.AttributeEditInProcesses.RemoveRange(currentEdits);
                    entities.AttributeVisibleInProcesses.RemoveRange(currentVis);
                    entities.SaveChanges();

                    foreach (var traceabilityMaster in traceabilityMasters)
                    {
                        ProgressBar.Run();

                        var allocation = new AttributeAllocation
                        {
                            AttributeMasterID = traceabilityMaster.AttributeMasterId,
                            AttributeTemplateID = templateId,
                            Batch = traceabilityMaster.Batch,
                            Transaction = traceabilityMaster.Transaction,
                            RequiredBeforeContinue = traceabilityMaster.RequiredBeforeContinue,
                            RequiredBeforeCompletion = traceabilityMaster.RequiredBeforeCompletion,
                            NouAttributeResetID = traceabilityMaster.NouAttributeResetId,
                            Sequence = traceabilityMaster.Sequence,
                            AttributeTabNameID = traceabilityMaster.AttributeTabNameId,
                            DefaultValueSQL = traceabilityMaster.DefaultSQL,
                            UseAsWorkflow = traceabilityMaster.UseAsWorkflow,
                            LoadMacro = traceabilityMaster.LoadMacro,
                            PostSelectionMACRO = traceabilityMaster.PostSelectionMacro
                        };

                        entities.AttributeAllocations.Add(allocation);
                        entities.SaveChanges();

                        foreach (var o in traceabilityMaster.VisibleProcessesForCombo)
                        {
                            var edit = new AttributeVisibleInProcess
                            {
                                AttributeAllocationID = allocation.AttributeAllocationID,
                                ProcessID = (int)o,
                                Visible = true
                            };

                            entities.AttributeVisibleInProcesses.Add(edit);
                        }

                        foreach (var o in traceabilityMaster.EditProcessesForCombo)
                        {
                            var edit = new AttributeEditInProcess
                            {
                                AttributeAlocationID = allocation.AttributeAllocationID,
                                ProcessID = (int)o,
                                Edit = true
                            };

                            entities.AttributeEditInProcesses.Add(edit);
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Allocations successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            finally
            {
                this.RemoveTemplateDuplication(templateId);
            }

            return false;
        }

        ///// <summary>
        ///// Adds an attribute to a template.
        ///// </summary>
        ///// <param name="traceabilityMasters">The collection of the attributes to add.</param>
        ///// <param name="templateId">The id of the target template.</param>
        ///// <returns>A flag, indicating a successful allocation, or not.</returns>
        //public bool AddAttributesToTemplate(IList<AttributeMasterData> traceabilityMasters, int templateId, bool isWorkflow, string notes, bool inactive)
        //{
        //    this.Log.LogDebug(this.GetType(), string.Format("AddTraceabilitysToTemplate(): Attempting to add {0} traceability masters to template id:{1}", traceabilityMasters.Count, templateId));

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            ProgressBar.Run();

        //            // Remove the current template quality fields.
        //            var currentTMs =
        //                entities.AttributeAllocations.Where(x => x.AttributeTemplateID == templateId).ToList();
        //            entities.AttributeAllocations.RemoveRange(currentTMs);
        //            entities.SaveChanges();

        //            var attributeIds = currentTMs.Select(x => (int?)x.AttributeAllocationID).ToList();

        //            var currentEdits =
        //                entities.AttributeEditInProcesses.Where(x => attributeIds.Contains(x.AttributeAlocationID)).ToList();
        //            entities.AttributeEditInProcesses.RemoveRange(currentEdits);

        //            var currentVis =
        //                entities.AttributeVisibleInProcesses.Where(x => attributeIds.Contains(x.AttributeAllocationID)).ToList();
        //            entities.AttributeVisibleInProcesses.RemoveRange(currentVis);

        //            entities.SaveChanges();

        //            var localTemplate =
        //                    entities.AttributeTemplates.FirstOrDefault(x => x.AttributeTemplateID == templateId);
        //            if (localTemplate != null)
        //            {
        //                localTemplate.Notes = notes;
        //                localTemplate.Inactive = inactive;
        //            }

        //            foreach (var traceabilityMaster in traceabilityMasters)
        //            {
        //                ProgressBar.Run();

        //                var allocation = new AttributeAllocation
        //                {
        //                    AttributeMasterID = traceabilityMaster.AttributeMasterId,
        //                    AttributeTemplateID = templateId,
        //                    Batch = traceabilityMaster.Batch,
        //                    Transaction = traceabilityMaster.Transaction,
        //                    RequiredBeforeContinue = traceabilityMaster.RequiredBeforeContinue,
        //                    RequiredBeforeCompletion = traceabilityMaster.RequiredBeforeCompletion,
        //                    NouAttributeResetID = traceabilityMaster.NouAttributeResetId,
        //                    Sequence = traceabilityMaster.Sequence,
        //                    AttributeTabNameID = traceabilityMaster.AttributeTabNameId,
        //                    DefaultValueSQL = traceabilityMaster.DefaultSQL,
        //                    UseAsWorkflow = traceabilityMaster.UseAsWorkflow,
        //                    LoadMacro = traceabilityMaster.LoadMacro,
        //                    PostSelectionMACRO = traceabilityMaster.PostSelectionMacro
        //                };

        //                entities.AttributeAllocations.Add(allocation);
        //                entities.SaveChanges();

        //                foreach (var o in traceabilityMaster.VisibleProcessesForCombo)
        //                {
        //                    var edit = new AttributeVisibleInProcess
        //                    {
        //                        AttributeAllocationID = allocation.AttributeAllocationID,
        //                        ProcessID = (int)o,
        //                        Visible = true
        //                    };

        //                    entities.AttributeVisibleInProcesses.Add(edit);
        //                }

        //                foreach (var o in traceabilityMaster.EditProcessesForCombo)
        //                {
        //                    var edit = new AttributeEditInProcess
        //                    {
        //                        AttributeAlocationID = allocation.AttributeAllocationID,
        //                        ProcessID = (int)o,
        //                        Edit = true
        //                    };

        //                    entities.AttributeEditInProcesses.Add(edit);
        //                }
        //            }

        //            var template = entities.AttributeTemplates.FirstOrDefault(x => x.AttributeTemplateID == templateId);
        //            if (template != null)
        //            {
        //                template.IsWorkflow = isWorkflow;
        //            }

        //            entities.SaveChanges();

        //            this.Log.LogDebug(this.GetType(), "Allocations successful");
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return false;
        //}

        /// <summary>
        /// Copies a template.
        /// </summary>
        /// <param name="templateToCopyId">The template id to copy from.</param>
        /// <param name="name">The new template name.</param>
        /// <returns>Newly created template id.</returns>
        public int CopyTemplate(int templateToCopyId, string name)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var id = entities.App_CopyTemplate(templateToCopyId, name);
                    if (id == -1)
                    {
                        return entities.AttributeTemplates.Max(x => x.AttributeTemplateID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Retrieve all the system attribute allocations.
        /// </summary>
        /// <returns>A collection of attribute allocations.</returns>
        public IList<AttributeAllocationData> GetAttributeAllocations()
        {
            var allocationData = new List<AttributeAllocationData>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocationData =
                        (from attributeAllocation in entities.AttributeAllocations.Where(x => x.Deleted == null)
                         select new AttributeAllocationData
                         {
                             AttributionAllocation = attributeAllocation,
                             AttributeMaster = attributeAllocation.AttributeMaster,
                             AttributeTemplate = attributeAllocation.AttributeTemplate,
                             AttributeReset = attributeAllocation.NouAttributeReset,
                             NouDateType = attributeAllocation.AttributeMaster != null ? attributeAllocation.AttributeMaster.NouDateType : null,
                             AttributeTabName = attributeAllocation.AttributeTabName,
                             NouTraceabilityType = attributeAllocation.AttributeMaster != null ? attributeAllocation.AttributeMaster.NouTraceabilityType : null,
                             NouTraceabilityTypeNonStandard = attributeAllocation.AttributeMaster != null ? attributeAllocation.AttributeMaster.NouTraceabilityType1 : null
                             //AttributeEditProcesses = (from x in entities.AttributeEditInProcesses
                             //                          where x.AttributeAlocationID == attributeAllocation.AttributeAllocationID && x.Deleted == null
                             //                          select new AttributeProcess {Process = x.Process}).ToList(),
                             //AttributeVisibleProcesses = (from x in entities.AttributeVisibleInProcesses
                             //                             where x.AttributeAllocationID == attributeAllocation.AttributeAllocationID && x.Deleted == null
                             //                             select new AttributeProcess { Process = x.Process }).ToList()
                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocationData;
        }

        /// <summary>
        /// Retrieve all the system attribute allocations permissions.
        /// </summary>
        /// <returns>A collection of attribute allocation permissions.</returns>
        public void GetAttributeAllocationPermissions(IList<AttributeAllocationData> allocations, int userId, int userGroupId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var attributeAllocation in allocations)
                    {
                        var outputAddParam = new System.Data.Entity.Core.Objects.ObjectParameter("Result", typeof(bool));
                        entities.App_CanAddAttribute(attributeAllocation.AttributeMaster.AttributeMasterID, userId, userGroupId,
                            outputAddParam).FirstOrDefault();
                        attributeAllocation.UserCanAdd = outputAddParam.Value.ToBool();

                        var outputEditParam = new System.Data.Entity.Core.Objects.ObjectParameter("Result", typeof(bool));
                        entities.App_CanEditAttribute(attributeAllocation.AttributeMaster.AttributeMasterID, userId, userGroupId,
                            outputEditParam).FirstOrDefault();
                        attributeAllocation.UserCanEdit = outputEditParam.Value.ToBool();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieve all the system attribute allocations.
        /// </summary>
        /// <returns>A collection of attribute allocations.</returns>
        public void GetAttributeAllocations(IList<AttributeAllocationData> allocations)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var attributeAllocation in allocations)
                    {
                        attributeAllocation.AttributeEditProcesses = (from x in entities.AttributeEditInProcesses
                                                                      where
                                                                          x.AttributeAlocationID ==
                                                                          attributeAllocation.AttributionAllocation.AttributeAllocationID && x.Deleted == null
                                                                      select new AttributeProcess { Process = x.Process }).ToList();

                        attributeAllocation.AttributeVisibleProcesses =
                            (from x in entities.AttributeVisibleInProcesses
                             where
                                 x.AttributeAllocationID ==
                                 attributeAllocation.AttributionAllocation.AttributeAllocationID && x.Deleted == null
                             select new AttributeProcess { Process = x.Process }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieve the allocation data for the input template id.
        /// </summary>
        /// <returns>An ttribute allocation.</returns>
        public IList<AttributeAllocationData> GetAttributeAllocationForTemplate(int templateId)
        {
            var allocationData = new List<AttributeAllocationData>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocationData =
                        (from attributeAllocation in entities.AttributeAllocations.Where(x => x.Deleted == null && x.AttributeTemplateID == templateId)
                         select new AttributeAllocationData
                         {
                             AttributionAllocation = attributeAllocation,
                             AttributeMaster = attributeAllocation.AttributeMaster,
                             AttributeReset = attributeAllocation.NouAttributeReset,
                             NouDateType = attributeAllocation.AttributeMaster != null ? attributeAllocation.AttributeMaster.NouDateType : null,
                             AttributeTabName = attributeAllocation.AttributeTabName,
                             NouTraceabilityType = attributeAllocation.AttributeMaster != null ? attributeAllocation.AttributeMaster.NouTraceabilityType : null,
                             NouTraceabilityTypeNonStandard = attributeAllocation.AttributeMaster != null ? attributeAllocation.AttributeMaster.NouTraceabilityType1 : null,
                             AttributeEditProcesses = (from x in entities.AttributeEditInProcesses
                                                       where x.AttributeAlocationID == attributeAllocation.AttributeAllocationID && x.Deleted == null
                                                       select new AttributeProcess { Process = x.Process }).ToList(),
                             AttributeVisibleProcesses = (from x in entities.AttributeVisibleInProcesses
                                                          where x.AttributeAllocationID == attributeAllocation.AttributeAllocationID && x.Deleted == null
                                                          select new AttributeProcess { Process = x.Process }).ToList()
                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocationData;
        }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>The application attributes.</returns>
        public IList<AttributeMasterData> GetAttributes()
        {
            var attributes = new List<AttributeMasterData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttributes = entities.AttributeMasters.Where(x => x.Deleted == null).ToList();
                    foreach (var attributeMaster in dbAttributes)
                    {
                        attributes.Add(new AttributeMasterData
                        {
                            AttributeMasterId = attributeMaster.AttributeMasterID,
                            Code = attributeMaster.Code,
                            Description = attributeMaster.Description,
                            UseAsWorkflow = attributeMaster.IsWorkflow.ToBool(),
                            TraceabilityType = attributeMaster.NouTraceabilityType != null ? attributeMaster.NouTraceabilityType.TraceabilityType : string.Empty
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return attributes;
        }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>The application attributes.</returns>
        public IList<Sale> GetAttributesForSearch()
        {
            var attributes = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttributes = entities.AttributeMasters.Where(x => x.Deleted == null).ToList();
                    foreach (var attributeMaster in dbAttributes)
                    {
                        attributes.Add(new Sale
                        {
                            SaleID = attributeMaster.AttributeMasterID,
                            Name = attributeMaster.Code,
                            Description = attributeMaster.Description,
                            IsWorkflow = attributeMaster.IsWorkflow,
                            EditDate = attributeMaster.EditDate,
                            UserIDModified = attributeMaster.UserMasterID,
                            PopUpNote = attributeMaster.Notes
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return attributes;
        }

        /// <summary>
        /// Gets the last attribute.
        /// </summary>
        /// <returns>The last edited attribute.</returns>
        public AttributeSetUp GetAttributeByLastEdit()
        {
            var attribute = new AttributeSetUp();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.AttributeMasters.Where(x => x.DeviceMasterID == NouvemGlobal.DeviceId)
                        .OrderByDescending(x => x.EditDate).FirstOrDefault();
                    if (dbAttribute == null)
                    {
                        return attribute;
                    }

                    attribute.AttributeMaster = dbAttribute;
                    attribute.UserAlerts = dbAttribute.UserAlerts.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupAlerts = dbAttribute.UserGroupAlerts.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupsNonStandard = dbAttribute.UserGroupNonStandards.Where(x => x.Deleted == null).ToList();
                    attribute.UserPermissions = dbAttribute.UserAttributePermissions.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupPermissions = dbAttribute.UserGroupAttributePermissions.Where(x => x.Deleted == null).ToList();
                    attribute.AttributeAttachments = dbAttribute.AttributeAttachments.Where(x => x.Deleted == null).ToList();
                    attribute.AttributeTemplates = new List<string>();
                    foreach (var attributeAllocation in dbAttribute.AttributeAllocations.Where(x => x.Deleted == null && x.AttributeTemplate.Inactive != true))
                    {
                        attribute.AttributeTemplates.Add(attributeAllocation.AttributeTemplate.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return attribute;
        }

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public AttributeSetUp GetAttributeById(int id)
        {
            var attribute = new AttributeSetUp();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttribute = entities.AttributeMasters.FirstOrDefault(x => x.AttributeMasterID == id);
                    if (dbAttribute == null)
                    {
                        return attribute;
                    }

                    attribute.AttributeMaster = dbAttribute;
                    attribute.UserAlerts = dbAttribute.UserAlerts.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupAlerts = dbAttribute.UserGroupAlerts.Where(x => x.Deleted == null).ToList();
                    attribute.UserPermissions = dbAttribute.UserAttributePermissions.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupPermissions = dbAttribute.UserGroupAttributePermissions.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupsNonStandard = dbAttribute.UserGroupNonStandards.Where(x => x.Deleted == null).ToList();
                    attribute.AttributeAttachments = dbAttribute.AttributeAttachments.Where(x => x.Deleted == null).ToList();
                    attribute.AttributeTemplates = new List<string>();
                    foreach (var attributeAllocation in dbAttribute.AttributeAllocations.Where(x => x.Deleted == null && x.AttributeTemplate.Inactive != true))
                    {
                        attribute.AttributeTemplates.Add(attributeAllocation.AttributeTemplate.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return attribute;
        }

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public DataLayer.Attribute GetAttributeByBatchId(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batchAttribute =
                        (from stock in
                             entities.StockTransactions
                         join batch in entities.BatchNumbers
                           on stock.BatchNumberID equals batch.BatchNumberID
                         join attribute in entities.Attributes
                             on batch.AttributeID equals attribute.AttributeID
                         where stock.StockTransactionID == id
                         select new { attribute, batch.BatchNumberID }).FirstOrDefault();

                    if (batchAttribute != null && batchAttribute.attribute != null)
                    {
                        batchAttribute.attribute.Generic1 = batchAttribute.BatchNumberID.ToString();
                        return batchAttribute.attribute;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the attribute by base id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public DataLayer.Attribute GetAttributeByBaseBatchId(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batchAttribute =
                        (from stock in
                             entities.StockTransactions
                         join batch in entities.BatchNumbers
                           on stock.BatchID_Base equals batch.BatchNumberID
                         join attribute in entities.Attributes
                             on batch.AttributeID equals attribute.AttributeID
                         where stock.StockTransactionID == id
                         select new { attribute, BatchId = batch.BatchNumberID, StockbatchId = stock.BatchNumberID, BaseBatchId = stock.BatchID_Base, MasterTableId = stock.MasterTableID }).FirstOrDefault();

                    if (batchAttribute == null)
                    {
                        batchAttribute =
                        (from stock in
                             entities.StockTransactions
                         join batch in entities.BatchNumbers
                           on stock.BatchNumberID equals batch.BatchNumberID
                         join attribute in entities.Attributes
                             on batch.AttributeID equals attribute.AttributeID
                         where stock.StockTransactionID == id
                         select new { attribute, BatchId = batch.BatchNumberID, StockbatchId = stock.BatchNumberID, BaseBatchId = stock.BatchID_Base, MasterTableId = stock.MasterTableID }).FirstOrDefault();
                    }

                    if (batchAttribute != null && batchAttribute.attribute != null)
                    {
                        batchAttribute.attribute.Generic1 = batchAttribute.BatchId.ToString();
                        batchAttribute.attribute.Generic2 = batchAttribute.StockbatchId.ToString();
                        batchAttribute.attribute.HoldingNumber = batchAttribute.BaseBatchId.ToString();

                        var intakeDetail = entities.APGoodsReceiptDetails.FirstOrDefault(x =>
                            x.APGoodsReceiptDetailID == batchAttribute.MasterTableId);
                        if (intakeDetail != null)
                        {
                            var intake = intakeDetail.APGoodsReceipt;
                            if (intake != null)
                            {
                                batchAttribute.attribute.SupplierID = intake.BPMasterID_Supplier;
                                batchAttribute.attribute.AgeInMonths = intake.APGoodsReceiptID;
                            }
                        }

                        return batchAttribute.attribute;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the attribute by id.
        /// </summary>
        /// <returns>The attribute by id.</returns>
        public AttributeSetUp GetAttributeByFirstLast(bool first)
        {
            var attribute = new AttributeSetUp();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    AttributeMaster dbAttribute = null;
                    if (first)
                    {
                        dbAttribute = entities.AttributeMasters.FirstOrDefault();
                    }
                    else
                    {
                        dbAttribute = entities.AttributeMasters.OrderByDescending(x => x.AttributeMasterID).FirstOrDefault();
                    }

                    if (dbAttribute == null)
                    {
                        return attribute;
                    }

                    attribute.AttributeMaster = dbAttribute;
                    attribute.UserAlerts = dbAttribute.UserAlerts.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupAlerts = dbAttribute.UserGroupAlerts.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupsNonStandard = dbAttribute.UserGroupNonStandards.Where(x => x.Deleted == null).ToList();
                    attribute.AttributeAttachments = dbAttribute.AttributeAttachments.Where(x => x.Deleted == null).ToList();
                    attribute.UserPermissions = dbAttribute.UserAttributePermissions.Where(x => x.Deleted == null).ToList();
                    attribute.UserGroupPermissions = dbAttribute.UserGroupAttributePermissions.Where(x => x.Deleted == null).ToList();
                    attribute.AttributeTemplates = new List<string>();
                    foreach (var attributeAllocation in dbAttribute.AttributeAllocations.Where(x => x.Deleted == null && x.AttributeTemplate.Inactive != true))
                    {
                        attribute.AttributeTemplates.Add(attributeAllocation.AttributeTemplate.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return attribute;
        }

        /// <summary>
        /// Gets the sp names.
        /// </summary>
        /// <returns>The application stored procedure names.</returns>
        public IList<string> GetSPNames()
        {
            var lookUps = new List<string>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    lookUps = entities.GetSPNames().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lookUps;
        }

        /// <summary>
        /// Retrieve all the system traceability template names.
        /// </summary>
        /// <returns>A collection of traceability template names.</returns>
        public IList<AttributeTemplate> GetAttributeTemplateNames()
        {
            this.Log.LogDebug(this.GetType(),
                "GetTraceabilityTemplateNames(): Attempting to retrieve all the traceability template names");
            var names = new List<AttributeTemplate>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                names = entities.AttributeTemplates.Where(x => x.Deleted == null).ToList();
                this.Log.LogDebug(this.GetType(),
                    string.Format("{0} traceability template names retrieved", names.Count()));
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return names;
        }

        /// <summary>
        /// Retrieve all the system traceability template groups.
        /// </summary>
        /// <returns>A collection of traceability template groups.</returns>
        public IList<AttributeTemplateGroup> GetAttributeTemplateGroups()
        {
            var groups = new List<AttributeTemplateGroup>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                groups = entities.AttributeTemplateGroups.Where(x => x.Deleted == null).ToList();
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return groups;
        }

        /// <summary>
        /// Adds or updates traceability names.
        /// </summary>
        /// <param name="traceabilityNames">The traceability names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateAttributeTemplateNames(IList<AttributeTemplate> traceabilityNames)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    traceabilityNames.ToList().ForEach(x =>
                    {
                        if (x.AttributeTemplateID == 0)
                        {
                            var templateName = new AttributeTemplate
                            {
                                Name = x.Name,
                                AttributeTemplateGroupID = x.AttributeTemplateGroupID,
                                UserMasterID = NouvemGlobal.UserId.ToInt(),
                                DeviceID = NouvemGlobal.DeviceId.ToInt(),
                                CreationDate = DateTime.Now
                            };

                            entities.AttributeTemplates.Add(templateName);
                        }
                        else
                        {
                            var traceabilityName =
                                entities.AttributeTemplates.FirstOrDefault(
                                    template =>
                                        template.AttributeTemplateID == x.AttributeTemplateID);

                            if (traceabilityName != null)
                            {
                                traceabilityName.Name = x.Name;
                                traceabilityName.AttributeTemplateGroupID = x.AttributeTemplateGroupID;
                                traceabilityName.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Traceability tempate names successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Adds or updates traceability groups.
        /// </summary>
        /// <param name="traceabilityNames">The traceability groups to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateAttributeTemplateGroups(IList<AttributeTemplateGroup> traceabilityNames)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    traceabilityNames.ToList().ForEach(x =>
                    {
                        if (x.AttributeTemplateGroupID == 0)
                        {
                            var templateName = new AttributeTemplateGroup
                            {
                                Name = x.Name,
                                UserMasterID = NouvemGlobal.UserId.ToInt(),
                                DeviceID = NouvemGlobal.DeviceId.ToInt(),
                                CreationDate = DateTime.Now
                            };

                            entities.AttributeTemplateGroups.Add(templateName);
                        }
                        else
                        {
                            var traceabilityName =
                                entities.AttributeTemplateGroups.FirstOrDefault(
                                    template =>
                                        template.AttributeTemplateGroupID == x.AttributeTemplateGroupID);

                            if (traceabilityName != null)
                            {
                                traceabilityName.Name = x.Name;
                                traceabilityName.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Traceability tempate names successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Gets the non standard response authorised groups.
        /// </summary>
        /// <returns>A collection of non standard response authorised groups.</returns>
        public IList<UserGroupNonStandard> GetUserGroupsNonStandard()
        {
            var lookUps = new List<UserGroupNonStandard>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    lookUps = entities.UserGroupNonStandards.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lookUps;
        }

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        /// <returns>The attribute look ups.</returns>
        public IList<AttributeLookUpData> GetAttributeLookUps()
        {
            var lookUps = new List<AttributeLookUpData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    lookUps = (from lookUp in entities.AttributeLookups
                               join master in entities.AttributeMasters on lookUp.AttributeMasterID equals master.AttributeMasterID
                               where lookUp.Deleted == null
                               select new AttributeLookUpData
                               {
                                   AttributeLookupID = lookUp.AttributeLookupID,
                                   Attribute_Name = lookUp.Attribute_Name,
                                   AttributeMasterID = lookUp.AttributeMasterID,
                                   AttributeCode = master.Code,
                                   AttributeDescription = master.Description
                               }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lookUps;
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>The application categories.</returns>
        public IList<NouCategory> GetCategories()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouCategories.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        public IList<DressSpec> GetDressSpecs()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.DressSpecs.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        public IList<KillingType> GetKillingType()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.KillingTypes.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the breeds.
        /// </summary>
        /// <returns>The application breeds.</returns>
        public IList<NouBreed> GetBreeds()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouBreeds.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the internal grades.
        /// </summary>
        /// <returns>The internal grades.</returns>
        public IList<InternalGrade> GetInternalGrades()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.InternalGrades.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds a breed.
        /// </summary>
        /// <returns>Flag, indicating successful breed addition.</returns>
        public bool AddBreed(NouBreed breed)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.NouBreeds.Add(breed);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the cleanliness values.
        /// </summary>
        /// <returns>The application cleanliness values.</returns>
        public IList<NouCleanliness> GetCleanlinesses()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouCleanlinesses.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the destinations.
        /// </summary>
        /// <returns>The application destinations.</returns>
        public IList<Destination> GetDestinations()
        {
            var destinations = new List<Destination>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.Destinations.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return destinations;
        }

        /// <summary>
        /// Gets the fat colours.
        /// </summary>
        /// <returns>The application fat colours.</returns>
        public IList<NouFatColour> GetFatColours()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.NouFatColours.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the attribute processes.
        /// </summary>
        /// <returns>The application attribute processes.</returns>
        public IList<AttributeProcess> GetAttributeProcesses()
        {
            var processes = new List<AttributeProcess>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var visibleProcesses = entities.AttributeVisibleInProcesses.Where(x => x.Deleted == null).ToList();
                    var editableProcesses = entities.AttributeEditInProcesses.Where(x => x.Deleted == null).ToList();

                    foreach (var attributeVisibleInProcess in visibleProcesses)
                    {
                        processes.Add(new AttributeProcess
                        {
                            AttributeProcessId = attributeVisibleInProcess.AttributeVisibleInProcessID,
                            AttributeAllocationId = attributeVisibleInProcess.AttributeAllocationID.ToInt(),
                            Visible = attributeVisibleInProcess.Visible.ToBool(),
                            Type = AttributeProcess.ProcessType.Visible,
                            Process = attributeVisibleInProcess.Process
                        });
                    }

                    foreach (var attributeEditInProcess in editableProcesses)
                    {
                        processes.Add(new AttributeProcess
                        {
                            AttributeProcessId = attributeEditInProcess.AttributeEditInProcessID,
                            AttributeAllocationId = attributeEditInProcess.AttributeAlocationID.ToInt(),
                            Editable = attributeEditInProcess.Edit.ToBool(),
                            Type = AttributeProcess.ProcessType.Editable,
                            Process = attributeEditInProcess.Process
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return processes;
        }

        /// <summary>
        /// Gets the  processes.
        /// </summary>
        /// <returns>The application processes.</returns>
        public IList<Process> GetProcesses()
        {
            var processes = new List<Process>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    processes = entities.Processes.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return processes;
        }

        /// <summary>
        /// Adds or updates a batch attribute.
        /// </summary>
        /// <returns>Flag, as to successful add or update..</returns>
        public int AddOrUpdateBatchAttributes(StockDetail detail)
        {
            var details = detail.BatchAttribute;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var attribute = new DataLayer.Attribute();
                    var batch = entities.BatchNumbers.FirstOrDefault(x => x.BatchNumberID == detail.BatchNumberID);
                    if (batch == null)
                    {
                        return 0;
                    }

                    if (batch.AttributeID != null)
                    {
                        // update
                        attribute = entities.Attributes.First(x => x.AttributeID == batch.AttributeID);
                    }

                    #region attributes

                    attribute.IsBatch = true;
                    attribute.Attribute1 = details.Attribute1;
                    attribute.Attribute2 = details.Attribute2;
                    attribute.Attribute3 = details.Attribute3;
                    attribute.Attribute4 = details.Attribute4;
                    attribute.Attribute5 = details.Attribute5;
                    attribute.Attribute6 = details.Attribute6;
                    attribute.Attribute7 = details.Attribute7;
                    attribute.Attribute8 = details.Attribute8;
                    attribute.Attribute9 = details.Attribute9;
                    attribute.Attribute10 = details.Attribute10;
                    attribute.Attribute11 = details.Attribute11;
                    attribute.Attribute12 = details.Attribute12;
                    attribute.Attribute13 = details.Attribute13;
                    attribute.Attribute14 = details.Attribute14;
                    attribute.Attribute15 = details.Attribute15;
                    attribute.Attribute16 = details.Attribute16;
                    attribute.Attribute17 = details.Attribute17;
                    attribute.Attribute18 = details.Attribute18;
                    attribute.Attribute19 = details.Attribute19;
                    attribute.Attribute20 = details.Attribute20;
                    attribute.Attribute21 = details.Attribute21;
                    attribute.Attribute22 = details.Attribute22;
                    attribute.Attribute23 = details.Attribute23;
                    attribute.Attribute24 = details.Attribute24;
                    attribute.Attribute25 = details.Attribute25;
                    attribute.Attribute26 = details.Attribute26;
                    attribute.Attribute27 = details.Attribute27;
                    attribute.Attribute28 = details.Attribute28;
                    attribute.Attribute29 = details.Attribute29;
                    attribute.Attribute30 = details.Attribute30;
                    attribute.Attribute31 = details.Attribute31;
                    attribute.Attribute32 = details.Attribute32;
                    attribute.Attribute33 = details.Attribute33;
                    attribute.Attribute34 = details.Attribute34;
                    attribute.Attribute35 = details.Attribute35;
                    attribute.Attribute36 = details.Attribute36;
                    attribute.Attribute37 = details.Attribute37;
                    attribute.Attribute38 = details.Attribute38;
                    attribute.Attribute39 = details.Attribute39;
                    attribute.Attribute40 = details.Attribute40;
                    attribute.Attribute41 = details.Attribute41;
                    attribute.Attribute42 = details.Attribute42;
                    attribute.Attribute43 = details.Attribute43;
                    attribute.Attribute44 = details.Attribute44;
                    attribute.Attribute45 = details.Attribute45;
                    attribute.Attribute46 = details.Attribute46;
                    attribute.Attribute47 = details.Attribute47;
                    attribute.Attribute48 = details.Attribute48;
                    attribute.Attribute49 = details.Attribute49;
                    attribute.Attribute50 = details.Attribute50;
                    attribute.Attribute51 = details.Attribute51;
                    attribute.Attribute52 = details.Attribute52;
                    attribute.Attribute53 = details.Attribute53;
                    attribute.Attribute54 = details.Attribute54;
                    attribute.Attribute55 = details.Attribute55;
                    attribute.Attribute56 = details.Attribute56;
                    attribute.Attribute57 = details.Attribute57;
                    attribute.Attribute58 = details.Attribute58;
                    attribute.Attribute59 = details.Attribute59;
                    attribute.Attribute60 = details.Attribute60;
                    attribute.Attribute61 = details.Attribute61;
                    attribute.Attribute62 = details.Attribute62;
                    attribute.Attribute63 = details.Attribute63;
                    attribute.Attribute64 = details.Attribute64;
                    attribute.Attribute65 = details.Attribute65;
                    attribute.Attribute66 = details.Attribute66;
                    attribute.Attribute67 = details.Attribute67;
                    attribute.Attribute68 = details.Attribute68;
                    attribute.Attribute69 = details.Attribute69;
                    attribute.Attribute70 = details.Attribute70;
                    attribute.Attribute71 = details.Attribute71;
                    attribute.Attribute72 = details.Attribute72;
                    attribute.Attribute73 = details.Attribute73;
                    attribute.Attribute74 = details.Attribute74;
                    attribute.Attribute75 = details.Attribute75;
                    attribute.Attribute76 = details.Attribute76;
                    attribute.Attribute77 = details.Attribute77;
                    attribute.Attribute78 = details.Attribute78;
                    attribute.Attribute79 = details.Attribute79;
                    attribute.Attribute80 = details.Attribute80;
                    attribute.Attribute81 = details.Attribute81;
                    attribute.Attribute82 = details.Attribute82;
                    attribute.Attribute83 = details.Attribute83;
                    attribute.Attribute84 = details.Attribute84;
                    attribute.Attribute85 = details.Attribute85;
                    attribute.Attribute86 = details.Attribute86;
                    attribute.Attribute87 = details.Attribute87;
                    attribute.Attribute88 = details.Attribute88;
                    attribute.Attribute89 = details.Attribute89;
                    attribute.Attribute90 = details.Attribute90;
                    attribute.Attribute91 = details.Attribute91;
                    attribute.Attribute92 = details.Attribute92;
                    attribute.Attribute93 = details.Attribute93;
                    attribute.Attribute94 = details.Attribute94;
                    attribute.Attribute95 = details.Attribute95;
                    attribute.Attribute96 = details.Attribute96;
                    attribute.Attribute97 = details.Attribute97;
                    attribute.Attribute98 = details.Attribute98;
                    attribute.Attribute99 = details.Attribute99;
                    attribute.Attribute100 = details.Attribute100;
                    attribute.Attribute101 = details.Attribute101;
                    attribute.Attribute102 = details.Attribute102;
                    attribute.Attribute103 = details.Attribute103;
                    attribute.Attribute104 = details.Attribute104;
                    attribute.Attribute105 = details.Attribute105;
                    attribute.Attribute106 = details.Attribute106;
                    attribute.Attribute107 = details.Attribute107;
                    attribute.Attribute108 = details.Attribute108;
                    attribute.Attribute109 = details.Attribute109;
                    attribute.Attribute110 = details.Attribute110;
                    attribute.Attribute111 = details.Attribute111;
                    attribute.Attribute112 = details.Attribute112;
                    attribute.Attribute113 = details.Attribute113;
                    attribute.Attribute114 = details.Attribute114;
                    attribute.Attribute115 = details.Attribute115;
                    attribute.Attribute116 = details.Attribute116;
                    attribute.Attribute117 = details.Attribute117;
                    attribute.Attribute118 = details.Attribute118;
                    attribute.Attribute119 = details.Attribute119;
                    attribute.Attribute120 = details.Attribute120;
                    attribute.Attribute121 = details.Attribute121;
                    attribute.Attribute122 = details.Attribute122;
                    attribute.Attribute123 = details.Attribute123;
                    attribute.Attribute124 = details.Attribute124;
                    attribute.Attribute125 = details.Attribute125;
                    attribute.Attribute126 = details.Attribute126;
                    attribute.Attribute127 = details.Attribute127;
                    attribute.Attribute128 = details.Attribute128;
                    attribute.Attribute129 = details.Attribute129;
                    attribute.Attribute130 = details.Attribute130;
                    attribute.Attribute131 = details.Attribute131;
                    attribute.Attribute132 = details.Attribute132;
                    attribute.Attribute133 = details.Attribute133;
                    attribute.Attribute134 = details.Attribute134;
                    attribute.Attribute135 = details.Attribute135;
                    attribute.Attribute136 = details.Attribute136;
                    attribute.Attribute137 = details.Attribute137;
                    attribute.Attribute138 = details.Attribute138;
                    attribute.Attribute139 = details.Attribute139;
                    attribute.Attribute140 = details.Attribute140;
                    attribute.Attribute141 = details.Attribute141;
                    attribute.Attribute142 = details.Attribute142;
                    attribute.Attribute143 = details.Attribute143;
                    attribute.Attribute144 = details.Attribute144;
                    attribute.Attribute145 = details.Attribute145;
                    attribute.Attribute146 = details.Attribute146;
                    attribute.Attribute147 = details.Attribute147;
                    attribute.Attribute148 = details.Attribute148;
                    attribute.Attribute149 = details.Attribute149;
                    attribute.Attribute150 = details.Attribute150;
                    attribute.Attribute151 = details.Attribute151;
                    attribute.Attribute152 = details.Attribute152;
                    attribute.Attribute153 = details.Attribute153;
                    attribute.Attribute154 = details.Attribute154;
                    attribute.Attribute155 = details.Attribute155;
                    attribute.Attribute156 = details.Attribute156;
                    attribute.Attribute157 = details.Attribute157;
                    attribute.Attribute158 = details.Attribute158;
                    attribute.Attribute159 = details.Attribute159;
                    attribute.Attribute160 = details.Attribute160;
                    attribute.Attribute161 = details.Attribute161;
                    attribute.Attribute162 = details.Attribute162;
                    attribute.Attribute163 = details.Attribute163;
                    attribute.Attribute164 = details.Attribute164;
                    attribute.Attribute165 = details.Attribute165;
                    attribute.Attribute166 = details.Attribute166;
                    attribute.Attribute167 = details.Attribute167;
                    attribute.Attribute168 = details.Attribute168;
                    attribute.Attribute169 = details.Attribute169;
                    attribute.Attribute170 = details.Attribute170;
                    attribute.Attribute171 = details.Attribute171;
                    attribute.Attribute172 = details.Attribute172;
                    attribute.Attribute173 = details.Attribute173;
                    attribute.Attribute174 = details.Attribute174;
                    attribute.Attribute175 = details.Attribute175;
                    attribute.Attribute176 = details.Attribute176;
                    attribute.Attribute177 = details.Attribute177;
                    attribute.Attribute178 = details.Attribute178;
                    attribute.Attribute179 = details.Attribute179;
                    attribute.Attribute180 = details.Attribute180;
                    attribute.Attribute181 = details.Attribute181;
                    attribute.Attribute182 = details.Attribute182;
                    attribute.Attribute183 = details.Attribute183;
                    attribute.Attribute184 = details.Attribute184;
                    attribute.Attribute185 = details.Attribute185;
                    attribute.Attribute186 = details.Attribute186;
                    attribute.Attribute187 = details.Attribute187;
                    attribute.Attribute188 = details.Attribute188;
                    attribute.Attribute189 = details.Attribute189;
                    attribute.Attribute190 = details.Attribute190;
                    attribute.Attribute191 = details.Attribute191;
                    attribute.Attribute192 = details.Attribute192;
                    attribute.Attribute193 = details.Attribute193;
                    attribute.Attribute194 = details.Attribute194;
                    attribute.Attribute195 = details.Attribute195;
                    attribute.Attribute196 = details.Attribute196;
                    attribute.Attribute197 = details.Attribute197;
                    attribute.Attribute198 = details.Attribute198;
                    attribute.Attribute199 = details.Attribute199;
                    attribute.Attribute200 = details.Attribute200;

                    attribute.Attribute201 = details.Attribute201;
                    attribute.Attribute202 = details.Attribute202;
                    attribute.Attribute203 = details.Attribute203;
                    attribute.Attribute204 = details.Attribute204;
                    attribute.Attribute205 = details.Attribute205;
                    attribute.Attribute206 = details.Attribute206;
                    attribute.Attribute207 = details.Attribute207;
                    attribute.Attribute208 = details.Attribute208;
                    attribute.Attribute209 = details.Attribute209;
                    attribute.Attribute210 = details.Attribute210;
                    attribute.Attribute211 = details.Attribute211;
                    attribute.Attribute212 = details.Attribute212;
                    attribute.Attribute213 = details.Attribute213;
                    attribute.Attribute214 = details.Attribute214;
                    attribute.Attribute215 = details.Attribute215;
                    attribute.Attribute216 = details.Attribute216;
                    attribute.Attribute217 = details.Attribute217;
                    attribute.Attribute218 = details.Attribute218;
                    attribute.Attribute219 = details.Attribute219;
                    attribute.Attribute220 = details.Attribute220;
                    attribute.Attribute221 = details.Attribute221;
                    attribute.Attribute222 = details.Attribute222;
                    attribute.Attribute223 = details.Attribute223;
                    attribute.Attribute224 = details.Attribute224;
                    attribute.Attribute225 = details.Attribute225;
                    attribute.Attribute226 = details.Attribute226;
                    attribute.Attribute227 = details.Attribute227;
                    attribute.Attribute228 = details.Attribute228;
                    attribute.Attribute229 = details.Attribute229;
                    attribute.Attribute230 = details.Attribute230;
                    attribute.Attribute231 = details.Attribute231;
                    attribute.Attribute232 = details.Attribute232;
                    attribute.Attribute233 = details.Attribute233;
                    attribute.Attribute234 = details.Attribute234;
                    attribute.Attribute235 = details.Attribute235;
                    attribute.Attribute236 = details.Attribute236;
                    attribute.Attribute237 = details.Attribute237;
                    attribute.Attribute238 = details.Attribute238;
                    attribute.Attribute239 = details.Attribute239;
                    attribute.Attribute240 = details.Attribute240;
                    attribute.Attribute241 = details.Attribute241;
                    attribute.Attribute242 = details.Attribute242;
                    attribute.Attribute243 = details.Attribute243;
                    attribute.Attribute244 = details.Attribute244;
                    attribute.Attribute245 = details.Attribute245;
                    attribute.Attribute246 = details.Attribute246;
                    attribute.Attribute247 = details.Attribute247;
                    attribute.Attribute248 = details.Attribute248;
                    attribute.Attribute249 = details.Attribute249;
                    attribute.Attribute250 = details.Attribute250;


                    #endregion

                    if (batch.AttributeID == null)
                    {
                        entities.Attributes.Add(attribute);
                        entities.SaveChanges();
                        batch.AttributeID = attribute.AttributeID;
                    }

                    entities.SaveChanges();
                    return attribute.AttributeID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Adds or updates a batch attribute.
        /// </summary>
        /// <returns>Flag, as to successful update.</returns>
        public bool UpdateAllBatchAttributes(StockDetail detail, IList<int?> batchIds)
        {
            var details = detail.BatchAttribute;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var batchId in batchIds)
                    {
                        var dbBatch = entities.BatchNumbers.FirstOrDefault(x => x.BatchNumberID == batchId);
                        if (dbBatch == null)
                        {
                            continue;
                        }

                        var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == dbBatch.AttributeID);
                        if (attribute == null)
                        {
                            continue;
                        }

                        #region attributes

                        if (!string.IsNullOrWhiteSpace(details.Attribute1) && string.IsNullOrEmpty(attribute.Attribute1)) { attribute.Attribute1 = details.Attribute1; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute2) && string.IsNullOrEmpty(attribute.Attribute2)) { attribute.Attribute2 = details.Attribute2; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute3) && string.IsNullOrEmpty(attribute.Attribute3)) { attribute.Attribute3 = details.Attribute3; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute4) && string.IsNullOrEmpty(attribute.Attribute4)) { attribute.Attribute4 = details.Attribute4; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute5) && string.IsNullOrEmpty(attribute.Attribute5)) { attribute.Attribute5 = details.Attribute5; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute6) && string.IsNullOrEmpty(attribute.Attribute6)) { attribute.Attribute6 = details.Attribute6; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute7) && string.IsNullOrEmpty(attribute.Attribute7)) { attribute.Attribute7 = details.Attribute7; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute8) && string.IsNullOrEmpty(attribute.Attribute8)) { attribute.Attribute8 = details.Attribute8; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute9) && string.IsNullOrEmpty(attribute.Attribute9)) { attribute.Attribute9 = details.Attribute9; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute10) && string.IsNullOrEmpty(attribute.Attribute10)) { attribute.Attribute10 = details.Attribute10; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute11) && string.IsNullOrEmpty(attribute.Attribute11)) { attribute.Attribute11 = details.Attribute11; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute12) && string.IsNullOrEmpty(attribute.Attribute12)) { attribute.Attribute12 = details.Attribute12; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute13) && string.IsNullOrEmpty(attribute.Attribute13)) { attribute.Attribute13 = details.Attribute13; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute14) && string.IsNullOrEmpty(attribute.Attribute14)) { attribute.Attribute14 = details.Attribute14; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute15) && string.IsNullOrEmpty(attribute.Attribute15)) { attribute.Attribute15 = details.Attribute15; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute16) && string.IsNullOrEmpty(attribute.Attribute16)) { attribute.Attribute16 = details.Attribute16; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute17) && string.IsNullOrEmpty(attribute.Attribute17)) { attribute.Attribute17 = details.Attribute17; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute18) && string.IsNullOrEmpty(attribute.Attribute18)) { attribute.Attribute18 = details.Attribute18; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute19) && string.IsNullOrEmpty(attribute.Attribute19)) { attribute.Attribute19 = details.Attribute19; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute20) && string.IsNullOrEmpty(attribute.Attribute20)) { attribute.Attribute20 = details.Attribute20; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute21) && string.IsNullOrEmpty(attribute.Attribute21)) { attribute.Attribute21 = details.Attribute21; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute22) && string.IsNullOrEmpty(attribute.Attribute22)) { attribute.Attribute22 = details.Attribute22; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute23) && string.IsNullOrEmpty(attribute.Attribute23)) { attribute.Attribute23 = details.Attribute23; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute24) && string.IsNullOrEmpty(attribute.Attribute24)) { attribute.Attribute24 = details.Attribute24; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute25) && string.IsNullOrEmpty(attribute.Attribute25)) { attribute.Attribute25 = details.Attribute25; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute26) && string.IsNullOrEmpty(attribute.Attribute26)) { attribute.Attribute26 = details.Attribute26; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute27) && string.IsNullOrEmpty(attribute.Attribute27)) { attribute.Attribute27 = details.Attribute27; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute28) && string.IsNullOrEmpty(attribute.Attribute28)) { attribute.Attribute28 = details.Attribute28; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute29) && string.IsNullOrEmpty(attribute.Attribute29)) { attribute.Attribute29 = details.Attribute29; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute30) && string.IsNullOrEmpty(attribute.Attribute30)) { attribute.Attribute30 = details.Attribute30; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute31) && string.IsNullOrEmpty(attribute.Attribute31)) { attribute.Attribute31 = details.Attribute31; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute32) && string.IsNullOrEmpty(attribute.Attribute32)) { attribute.Attribute32 = details.Attribute32; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute33) && string.IsNullOrEmpty(attribute.Attribute33)) { attribute.Attribute33 = details.Attribute33; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute34) && string.IsNullOrEmpty(attribute.Attribute34)) { attribute.Attribute34 = details.Attribute34; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute35) && string.IsNullOrEmpty(attribute.Attribute35)) { attribute.Attribute35 = details.Attribute35; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute36) && string.IsNullOrEmpty(attribute.Attribute36)) { attribute.Attribute36 = details.Attribute36; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute37) && string.IsNullOrEmpty(attribute.Attribute37)) { attribute.Attribute37 = details.Attribute37; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute38) && string.IsNullOrEmpty(attribute.Attribute38)) { attribute.Attribute38 = details.Attribute38; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute39) && string.IsNullOrEmpty(attribute.Attribute39)) { attribute.Attribute39 = details.Attribute39; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute40) && string.IsNullOrEmpty(attribute.Attribute40)) { attribute.Attribute40 = details.Attribute40; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute41) && string.IsNullOrEmpty(attribute.Attribute41)) { attribute.Attribute41 = details.Attribute41; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute42) && string.IsNullOrEmpty(attribute.Attribute42)) { attribute.Attribute42 = details.Attribute42; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute43) && string.IsNullOrEmpty(attribute.Attribute43)) { attribute.Attribute43 = details.Attribute43; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute44) && string.IsNullOrEmpty(attribute.Attribute44)) { attribute.Attribute44 = details.Attribute44; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute45) && string.IsNullOrEmpty(attribute.Attribute45)) { attribute.Attribute45 = details.Attribute45; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute46) && string.IsNullOrEmpty(attribute.Attribute46)) { attribute.Attribute46 = details.Attribute46; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute47) && string.IsNullOrEmpty(attribute.Attribute47)) { attribute.Attribute47 = details.Attribute47; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute48) && string.IsNullOrEmpty(attribute.Attribute48)) { attribute.Attribute48 = details.Attribute48; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute49) && string.IsNullOrEmpty(attribute.Attribute49)) { attribute.Attribute49 = details.Attribute49; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute50) && string.IsNullOrEmpty(attribute.Attribute50)) { attribute.Attribute50 = details.Attribute50; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute51) && string.IsNullOrEmpty(attribute.Attribute51)) { attribute.Attribute51 = details.Attribute51; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute52) && string.IsNullOrEmpty(attribute.Attribute52)) { attribute.Attribute52 = details.Attribute52; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute53) && string.IsNullOrEmpty(attribute.Attribute53)) { attribute.Attribute53 = details.Attribute53; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute54) && string.IsNullOrEmpty(attribute.Attribute54)) { attribute.Attribute54 = details.Attribute54; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute55) && string.IsNullOrEmpty(attribute.Attribute55)) { attribute.Attribute55 = details.Attribute55; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute56) && string.IsNullOrEmpty(attribute.Attribute56)) { attribute.Attribute56 = details.Attribute56; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute57) && string.IsNullOrEmpty(attribute.Attribute57)) { attribute.Attribute57 = details.Attribute57; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute58) && string.IsNullOrEmpty(attribute.Attribute58)) { attribute.Attribute58 = details.Attribute58; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute59) && string.IsNullOrEmpty(attribute.Attribute59)) { attribute.Attribute59 = details.Attribute59; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute60) && string.IsNullOrEmpty(attribute.Attribute60)) { attribute.Attribute60 = details.Attribute60; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute61) && string.IsNullOrEmpty(attribute.Attribute61)) { attribute.Attribute61 = details.Attribute61; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute62) && string.IsNullOrEmpty(attribute.Attribute62)) { attribute.Attribute62 = details.Attribute62; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute63) && string.IsNullOrEmpty(attribute.Attribute63)) { attribute.Attribute63 = details.Attribute63; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute64) && string.IsNullOrEmpty(attribute.Attribute64)) { attribute.Attribute64 = details.Attribute64; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute65) && string.IsNullOrEmpty(attribute.Attribute65)) { attribute.Attribute65 = details.Attribute65; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute66) && string.IsNullOrEmpty(attribute.Attribute66)) { attribute.Attribute66 = details.Attribute66; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute67) && string.IsNullOrEmpty(attribute.Attribute67)) { attribute.Attribute67 = details.Attribute67; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute68) && string.IsNullOrEmpty(attribute.Attribute68)) { attribute.Attribute68 = details.Attribute68; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute69) && string.IsNullOrEmpty(attribute.Attribute69)) { attribute.Attribute69 = details.Attribute69; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute70) && string.IsNullOrEmpty(attribute.Attribute70)) { attribute.Attribute70 = details.Attribute70; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute71) && string.IsNullOrEmpty(attribute.Attribute71)) { attribute.Attribute71 = details.Attribute71; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute72) && string.IsNullOrEmpty(attribute.Attribute72)) { attribute.Attribute72 = details.Attribute72; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute73) && string.IsNullOrEmpty(attribute.Attribute73)) { attribute.Attribute73 = details.Attribute73; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute74) && string.IsNullOrEmpty(attribute.Attribute74)) { attribute.Attribute74 = details.Attribute74; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute75) && string.IsNullOrEmpty(attribute.Attribute75)) { attribute.Attribute75 = details.Attribute75; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute76) && string.IsNullOrEmpty(attribute.Attribute76)) { attribute.Attribute76 = details.Attribute76; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute77) && string.IsNullOrEmpty(attribute.Attribute77)) { attribute.Attribute77 = details.Attribute77; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute78) && string.IsNullOrEmpty(attribute.Attribute78)) { attribute.Attribute78 = details.Attribute78; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute79) && string.IsNullOrEmpty(attribute.Attribute79)) { attribute.Attribute79 = details.Attribute79; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute80) && string.IsNullOrEmpty(attribute.Attribute80)) { attribute.Attribute80 = details.Attribute80; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute81) && string.IsNullOrEmpty(attribute.Attribute81)) { attribute.Attribute81 = details.Attribute81; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute82) && string.IsNullOrEmpty(attribute.Attribute82)) { attribute.Attribute82 = details.Attribute82; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute83) && string.IsNullOrEmpty(attribute.Attribute83)) { attribute.Attribute83 = details.Attribute83; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute84) && string.IsNullOrEmpty(attribute.Attribute84)) { attribute.Attribute84 = details.Attribute84; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute85) && string.IsNullOrEmpty(attribute.Attribute85)) { attribute.Attribute85 = details.Attribute85; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute86) && string.IsNullOrEmpty(attribute.Attribute86)) { attribute.Attribute86 = details.Attribute86; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute87) && string.IsNullOrEmpty(attribute.Attribute87)) { attribute.Attribute87 = details.Attribute87; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute88) && string.IsNullOrEmpty(attribute.Attribute88)) { attribute.Attribute88 = details.Attribute88; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute89) && string.IsNullOrEmpty(attribute.Attribute89)) { attribute.Attribute89 = details.Attribute89; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute90) && string.IsNullOrEmpty(attribute.Attribute90)) { attribute.Attribute90 = details.Attribute90; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute91) && string.IsNullOrEmpty(attribute.Attribute91)) { attribute.Attribute91 = details.Attribute91; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute92) && string.IsNullOrEmpty(attribute.Attribute92)) { attribute.Attribute92 = details.Attribute92; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute93) && string.IsNullOrEmpty(attribute.Attribute93)) { attribute.Attribute93 = details.Attribute93; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute94) && string.IsNullOrEmpty(attribute.Attribute94)) { attribute.Attribute94 = details.Attribute94; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute95) && string.IsNullOrEmpty(attribute.Attribute95)) { attribute.Attribute95 = details.Attribute95; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute96) && string.IsNullOrEmpty(attribute.Attribute96)) { attribute.Attribute96 = details.Attribute96; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute97) && string.IsNullOrEmpty(attribute.Attribute97)) { attribute.Attribute97 = details.Attribute97; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute98) && string.IsNullOrEmpty(attribute.Attribute98)) { attribute.Attribute98 = details.Attribute98; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute99) && string.IsNullOrEmpty(attribute.Attribute99)) { attribute.Attribute99 = details.Attribute99; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute100) && string.IsNullOrEmpty(attribute.Attribute100)) { attribute.Attribute100 = details.Attribute100; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute101) && string.IsNullOrEmpty(attribute.Attribute101)) { attribute.Attribute101 = details.Attribute101; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute102) && string.IsNullOrEmpty(attribute.Attribute102)) { attribute.Attribute102 = details.Attribute102; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute103) && string.IsNullOrEmpty(attribute.Attribute103)) { attribute.Attribute103 = details.Attribute103; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute104) && string.IsNullOrEmpty(attribute.Attribute104)) { attribute.Attribute104 = details.Attribute104; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute105) && string.IsNullOrEmpty(attribute.Attribute105)) { attribute.Attribute105 = details.Attribute105; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute106) && string.IsNullOrEmpty(attribute.Attribute106)) { attribute.Attribute106 = details.Attribute106; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute107) && string.IsNullOrEmpty(attribute.Attribute107)) { attribute.Attribute107 = details.Attribute107; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute108) && string.IsNullOrEmpty(attribute.Attribute108)) { attribute.Attribute108 = details.Attribute108; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute109) && string.IsNullOrEmpty(attribute.Attribute109)) { attribute.Attribute109 = details.Attribute109; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute110) && string.IsNullOrEmpty(attribute.Attribute110)) { attribute.Attribute110 = details.Attribute110; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute111) && string.IsNullOrEmpty(attribute.Attribute111)) { attribute.Attribute111 = details.Attribute111; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute112) && string.IsNullOrEmpty(attribute.Attribute112)) { attribute.Attribute112 = details.Attribute112; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute113) && string.IsNullOrEmpty(attribute.Attribute113)) { attribute.Attribute113 = details.Attribute113; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute114) && string.IsNullOrEmpty(attribute.Attribute114)) { attribute.Attribute114 = details.Attribute114; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute115) && string.IsNullOrEmpty(attribute.Attribute115)) { attribute.Attribute115 = details.Attribute115; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute116) && string.IsNullOrEmpty(attribute.Attribute116)) { attribute.Attribute116 = details.Attribute116; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute117) && string.IsNullOrEmpty(attribute.Attribute117)) { attribute.Attribute117 = details.Attribute117; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute118) && string.IsNullOrEmpty(attribute.Attribute118)) { attribute.Attribute118 = details.Attribute118; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute119) && string.IsNullOrEmpty(attribute.Attribute119)) { attribute.Attribute119 = details.Attribute119; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute120) && string.IsNullOrEmpty(attribute.Attribute120)) { attribute.Attribute120 = details.Attribute120; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute121) && string.IsNullOrEmpty(attribute.Attribute121)) { attribute.Attribute121 = details.Attribute121; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute122) && string.IsNullOrEmpty(attribute.Attribute122)) { attribute.Attribute122 = details.Attribute122; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute123) && string.IsNullOrEmpty(attribute.Attribute123)) { attribute.Attribute123 = details.Attribute123; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute124) && string.IsNullOrEmpty(attribute.Attribute124)) { attribute.Attribute124 = details.Attribute124; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute125) && string.IsNullOrEmpty(attribute.Attribute125)) { attribute.Attribute125 = details.Attribute125; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute126) && string.IsNullOrEmpty(attribute.Attribute126)) { attribute.Attribute126 = details.Attribute126; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute127) && string.IsNullOrEmpty(attribute.Attribute127)) { attribute.Attribute127 = details.Attribute127; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute128) && string.IsNullOrEmpty(attribute.Attribute128)) { attribute.Attribute128 = details.Attribute128; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute129) && string.IsNullOrEmpty(attribute.Attribute129)) { attribute.Attribute129 = details.Attribute129; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute130) && string.IsNullOrEmpty(attribute.Attribute130)) { attribute.Attribute130 = details.Attribute130; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute131) && string.IsNullOrEmpty(attribute.Attribute131)) { attribute.Attribute131 = details.Attribute131; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute132) && string.IsNullOrEmpty(attribute.Attribute132)) { attribute.Attribute132 = details.Attribute132; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute133) && string.IsNullOrEmpty(attribute.Attribute133)) { attribute.Attribute133 = details.Attribute133; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute134) && string.IsNullOrEmpty(attribute.Attribute134)) { attribute.Attribute134 = details.Attribute134; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute135) && string.IsNullOrEmpty(attribute.Attribute135)) { attribute.Attribute135 = details.Attribute135; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute136) && string.IsNullOrEmpty(attribute.Attribute136)) { attribute.Attribute136 = details.Attribute136; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute137) && string.IsNullOrEmpty(attribute.Attribute137)) { attribute.Attribute137 = details.Attribute137; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute138) && string.IsNullOrEmpty(attribute.Attribute138)) { attribute.Attribute138 = details.Attribute138; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute139) && string.IsNullOrEmpty(attribute.Attribute139)) { attribute.Attribute139 = details.Attribute139; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute140) && string.IsNullOrEmpty(attribute.Attribute140)) { attribute.Attribute140 = details.Attribute140; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute141) && string.IsNullOrEmpty(attribute.Attribute141)) { attribute.Attribute141 = details.Attribute141; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute142) && string.IsNullOrEmpty(attribute.Attribute142)) { attribute.Attribute142 = details.Attribute142; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute143) && string.IsNullOrEmpty(attribute.Attribute143)) { attribute.Attribute143 = details.Attribute143; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute144) && string.IsNullOrEmpty(attribute.Attribute144)) { attribute.Attribute144 = details.Attribute144; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute145) && string.IsNullOrEmpty(attribute.Attribute145)) { attribute.Attribute145 = details.Attribute145; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute146) && string.IsNullOrEmpty(attribute.Attribute146)) { attribute.Attribute146 = details.Attribute146; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute147) && string.IsNullOrEmpty(attribute.Attribute147)) { attribute.Attribute147 = details.Attribute147; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute148) && string.IsNullOrEmpty(attribute.Attribute148)) { attribute.Attribute148 = details.Attribute148; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute149) && string.IsNullOrEmpty(attribute.Attribute149)) { attribute.Attribute149 = details.Attribute149; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute150) && string.IsNullOrEmpty(attribute.Attribute150)) { attribute.Attribute150 = details.Attribute150; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute151) && string.IsNullOrEmpty(attribute.Attribute151)) { attribute.Attribute151 = details.Attribute151; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute152) && string.IsNullOrEmpty(attribute.Attribute152)) { attribute.Attribute152 = details.Attribute152; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute153) && string.IsNullOrEmpty(attribute.Attribute153)) { attribute.Attribute153 = details.Attribute153; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute154) && string.IsNullOrEmpty(attribute.Attribute154)) { attribute.Attribute154 = details.Attribute154; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute155) && string.IsNullOrEmpty(attribute.Attribute155)) { attribute.Attribute155 = details.Attribute155; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute156) && string.IsNullOrEmpty(attribute.Attribute156)) { attribute.Attribute156 = details.Attribute156; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute157) && string.IsNullOrEmpty(attribute.Attribute157)) { attribute.Attribute157 = details.Attribute157; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute158) && string.IsNullOrEmpty(attribute.Attribute158)) { attribute.Attribute158 = details.Attribute158; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute159) && string.IsNullOrEmpty(attribute.Attribute159)) { attribute.Attribute159 = details.Attribute159; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute160) && string.IsNullOrEmpty(attribute.Attribute160)) { attribute.Attribute160 = details.Attribute160; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute161) && string.IsNullOrEmpty(attribute.Attribute161)) { attribute.Attribute161 = details.Attribute161; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute162) && string.IsNullOrEmpty(attribute.Attribute162)) { attribute.Attribute162 = details.Attribute162; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute163) && string.IsNullOrEmpty(attribute.Attribute163)) { attribute.Attribute163 = details.Attribute163; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute164) && string.IsNullOrEmpty(attribute.Attribute164)) { attribute.Attribute164 = details.Attribute164; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute165) && string.IsNullOrEmpty(attribute.Attribute165)) { attribute.Attribute165 = details.Attribute165; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute166) && string.IsNullOrEmpty(attribute.Attribute166)) { attribute.Attribute166 = details.Attribute166; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute167) && string.IsNullOrEmpty(attribute.Attribute167)) { attribute.Attribute167 = details.Attribute167; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute168) && string.IsNullOrEmpty(attribute.Attribute168)) { attribute.Attribute168 = details.Attribute168; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute169) && string.IsNullOrEmpty(attribute.Attribute169)) { attribute.Attribute169 = details.Attribute169; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute170) && string.IsNullOrEmpty(attribute.Attribute170)) { attribute.Attribute170 = details.Attribute170; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute171) && string.IsNullOrEmpty(attribute.Attribute171)) { attribute.Attribute171 = details.Attribute171; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute172) && string.IsNullOrEmpty(attribute.Attribute172)) { attribute.Attribute172 = details.Attribute172; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute173) && string.IsNullOrEmpty(attribute.Attribute173)) { attribute.Attribute173 = details.Attribute173; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute174) && string.IsNullOrEmpty(attribute.Attribute174)) { attribute.Attribute174 = details.Attribute174; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute175) && string.IsNullOrEmpty(attribute.Attribute175)) { attribute.Attribute175 = details.Attribute175; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute176) && string.IsNullOrEmpty(attribute.Attribute176)) { attribute.Attribute176 = details.Attribute176; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute177) && string.IsNullOrEmpty(attribute.Attribute177)) { attribute.Attribute177 = details.Attribute177; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute178) && string.IsNullOrEmpty(attribute.Attribute178)) { attribute.Attribute178 = details.Attribute178; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute179) && string.IsNullOrEmpty(attribute.Attribute179)) { attribute.Attribute179 = details.Attribute179; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute180) && string.IsNullOrEmpty(attribute.Attribute180)) { attribute.Attribute180 = details.Attribute180; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute181) && string.IsNullOrEmpty(attribute.Attribute181)) { attribute.Attribute181 = details.Attribute181; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute182) && string.IsNullOrEmpty(attribute.Attribute182)) { attribute.Attribute182 = details.Attribute182; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute183) && string.IsNullOrEmpty(attribute.Attribute183)) { attribute.Attribute183 = details.Attribute183; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute184) && string.IsNullOrEmpty(attribute.Attribute184)) { attribute.Attribute184 = details.Attribute184; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute185) && string.IsNullOrEmpty(attribute.Attribute185)) { attribute.Attribute185 = details.Attribute185; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute186) && string.IsNullOrEmpty(attribute.Attribute186)) { attribute.Attribute186 = details.Attribute186; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute187) && string.IsNullOrEmpty(attribute.Attribute187)) { attribute.Attribute187 = details.Attribute187; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute188) && string.IsNullOrEmpty(attribute.Attribute188)) { attribute.Attribute188 = details.Attribute188; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute189) && string.IsNullOrEmpty(attribute.Attribute189)) { attribute.Attribute189 = details.Attribute189; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute190) && string.IsNullOrEmpty(attribute.Attribute190)) { attribute.Attribute190 = details.Attribute190; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute191) && string.IsNullOrEmpty(attribute.Attribute191)) { attribute.Attribute191 = details.Attribute191; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute192) && string.IsNullOrEmpty(attribute.Attribute192)) { attribute.Attribute192 = details.Attribute192; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute193) && string.IsNullOrEmpty(attribute.Attribute193)) { attribute.Attribute193 = details.Attribute193; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute194) && string.IsNullOrEmpty(attribute.Attribute194)) { attribute.Attribute194 = details.Attribute194; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute195) && string.IsNullOrEmpty(attribute.Attribute195)) { attribute.Attribute195 = details.Attribute195; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute196) && string.IsNullOrEmpty(attribute.Attribute196)) { attribute.Attribute196 = details.Attribute196; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute197) && string.IsNullOrEmpty(attribute.Attribute197)) { attribute.Attribute197 = details.Attribute197; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute198) && string.IsNullOrEmpty(attribute.Attribute198)) { attribute.Attribute198 = details.Attribute198; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute199) && string.IsNullOrEmpty(attribute.Attribute199)) { attribute.Attribute199 = details.Attribute199; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute200) && string.IsNullOrEmpty(attribute.Attribute200)) { attribute.Attribute200 = details.Attribute200; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute201) && string.IsNullOrEmpty(attribute.Attribute201)) { attribute.Attribute201 = details.Attribute201; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute202) && string.IsNullOrEmpty(attribute.Attribute202)) { attribute.Attribute202 = details.Attribute202; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute203) && string.IsNullOrEmpty(attribute.Attribute203)) { attribute.Attribute203 = details.Attribute203; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute204) && string.IsNullOrEmpty(attribute.Attribute204)) { attribute.Attribute204 = details.Attribute204; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute205) && string.IsNullOrEmpty(attribute.Attribute205)) { attribute.Attribute205 = details.Attribute205; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute206) && string.IsNullOrEmpty(attribute.Attribute206)) { attribute.Attribute206 = details.Attribute206; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute207) && string.IsNullOrEmpty(attribute.Attribute207)) { attribute.Attribute207 = details.Attribute207; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute208) && string.IsNullOrEmpty(attribute.Attribute208)) { attribute.Attribute208 = details.Attribute208; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute209) && string.IsNullOrEmpty(attribute.Attribute209)) { attribute.Attribute209 = details.Attribute209; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute210) && string.IsNullOrEmpty(attribute.Attribute210)) { attribute.Attribute210 = details.Attribute210; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute211) && string.IsNullOrEmpty(attribute.Attribute211)) { attribute.Attribute211 = details.Attribute211; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute212) && string.IsNullOrEmpty(attribute.Attribute212)) { attribute.Attribute212 = details.Attribute212; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute213) && string.IsNullOrEmpty(attribute.Attribute213)) { attribute.Attribute213 = details.Attribute213; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute214) && string.IsNullOrEmpty(attribute.Attribute214)) { attribute.Attribute214 = details.Attribute214; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute215) && string.IsNullOrEmpty(attribute.Attribute215)) { attribute.Attribute215 = details.Attribute215; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute216) && string.IsNullOrEmpty(attribute.Attribute216)) { attribute.Attribute216 = details.Attribute216; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute217) && string.IsNullOrEmpty(attribute.Attribute217)) { attribute.Attribute217 = details.Attribute217; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute218) && string.IsNullOrEmpty(attribute.Attribute218)) { attribute.Attribute218 = details.Attribute218; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute219) && string.IsNullOrEmpty(attribute.Attribute219)) { attribute.Attribute219 = details.Attribute219; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute220) && string.IsNullOrEmpty(attribute.Attribute220)) { attribute.Attribute220 = details.Attribute220; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute221) && string.IsNullOrEmpty(attribute.Attribute221)) { attribute.Attribute221 = details.Attribute221; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute222) && string.IsNullOrEmpty(attribute.Attribute222)) { attribute.Attribute222 = details.Attribute222; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute223) && string.IsNullOrEmpty(attribute.Attribute223)) { attribute.Attribute223 = details.Attribute223; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute224) && string.IsNullOrEmpty(attribute.Attribute224)) { attribute.Attribute224 = details.Attribute224; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute225) && string.IsNullOrEmpty(attribute.Attribute225)) { attribute.Attribute225 = details.Attribute225; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute226) && string.IsNullOrEmpty(attribute.Attribute226)) { attribute.Attribute226 = details.Attribute226; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute227) && string.IsNullOrEmpty(attribute.Attribute227)) { attribute.Attribute227 = details.Attribute227; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute228) && string.IsNullOrEmpty(attribute.Attribute228)) { attribute.Attribute228 = details.Attribute228; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute229) && string.IsNullOrEmpty(attribute.Attribute229)) { attribute.Attribute229 = details.Attribute229; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute230) && string.IsNullOrEmpty(attribute.Attribute230)) { attribute.Attribute230 = details.Attribute230; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute231) && string.IsNullOrEmpty(attribute.Attribute231)) { attribute.Attribute231 = details.Attribute231; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute232) && string.IsNullOrEmpty(attribute.Attribute232)) { attribute.Attribute232 = details.Attribute232; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute233) && string.IsNullOrEmpty(attribute.Attribute233)) { attribute.Attribute233 = details.Attribute233; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute234) && string.IsNullOrEmpty(attribute.Attribute234)) { attribute.Attribute234 = details.Attribute234; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute235) && string.IsNullOrEmpty(attribute.Attribute235)) { attribute.Attribute235 = details.Attribute235; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute236) && string.IsNullOrEmpty(attribute.Attribute236)) { attribute.Attribute236 = details.Attribute236; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute237) && string.IsNullOrEmpty(attribute.Attribute237)) { attribute.Attribute237 = details.Attribute237; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute238) && string.IsNullOrEmpty(attribute.Attribute238)) { attribute.Attribute238 = details.Attribute238; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute239) && string.IsNullOrEmpty(attribute.Attribute239)) { attribute.Attribute239 = details.Attribute239; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute240) && string.IsNullOrEmpty(attribute.Attribute240)) { attribute.Attribute240 = details.Attribute240; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute241) && string.IsNullOrEmpty(attribute.Attribute241)) { attribute.Attribute241 = details.Attribute241; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute242) && string.IsNullOrEmpty(attribute.Attribute242)) { attribute.Attribute242 = details.Attribute242; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute243) && string.IsNullOrEmpty(attribute.Attribute243)) { attribute.Attribute243 = details.Attribute243; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute244) && string.IsNullOrEmpty(attribute.Attribute244)) { attribute.Attribute244 = details.Attribute244; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute245) && string.IsNullOrEmpty(attribute.Attribute245)) { attribute.Attribute245 = details.Attribute245; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute246) && string.IsNullOrEmpty(attribute.Attribute246)) { attribute.Attribute246 = details.Attribute246; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute247) && string.IsNullOrEmpty(attribute.Attribute247)) { attribute.Attribute247 = details.Attribute247; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute248) && string.IsNullOrEmpty(attribute.Attribute248)) { attribute.Attribute248 = details.Attribute248; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute249) && string.IsNullOrEmpty(attribute.Attribute249)) { attribute.Attribute249 = details.Attribute249; }
                        if (!string.IsNullOrWhiteSpace(details.Attribute250) && string.IsNullOrEmpty(attribute.Attribute250)) { attribute.Attribute250 = details.Attribute250; }


                        #endregion
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets all the batches in a location.
        /// </summary>
        /// <param name="location">The location to check.</param>
        /// <returns>All the batches in a location.</returns>
        public IList<int?> GetBatchesInLocation(Location location)
        {
            var batchIds = new List<int?>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.Where(
                            x =>
                                x.Deleted == null && x.Consumed == null && x.WarehouseID == location.WarehouseId)
                                .Select(x => x.BatchNumberID).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return batchIds;
        }

        /// <summary>
        /// Gets the start up checks, and completed daily workflows, checking for any unstarted or uncompleted checks.
        /// </summary>
        /// <returns>A list of process required workflows not started.or completed.</returns>
        public IList<string> GetProcessStartUpChecks(int processId)
        {
            var dailyChecksNotComplete = new List<string>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var checksToMake =
                        entities.ProcessStartUpChecks.Where(x => x.Deleted == null && x.ProcessID == processId && x.NouModuleID == null).ToList();

                    if (!checksToMake.Any())
                    {
                        return dailyChecksNotComplete;
                    }

                    var checks = checksToMake.Select(x => (int?)x.AttributeTemplateID).ToList();

                    // gets all the relevant process completed start up checks
                    var startUps =
                         entities.AttributeTemplateRecords.Where(
                             x => x.Deleted == null && x.CreationDate >= DateTime.Today && checks.Contains(x.AttributeTemplateID)
                                 && x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID).Select(x => x.AttributeTemplateID).ToList();

                    foreach (var check in checksToMake)
                    {
                        if (!startUps.Contains(check.AttributeTemplateID))
                        {
                            dailyChecksNotComplete.Add(check.AttributeTemplate.Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dailyChecksNotComplete;
        }

        /// <summary>
        /// Gets the start up checks for a particular process/module.
        /// </summary>
        /// <returns>The relevant start up checks.</returns>
        public App_GetHACCPForModule_Result1 GetProcessModuleStartUpChecks(int processId, int moduleid, int inmasterid)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetHACCPForModule(processId, moduleid, NouvemGlobal.DeviceId, inmasterid).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

       /// <summary>
       /// Sets a modules haccp complete flag.
       /// </summary>
       /// <param name="moduleid">The module id.</param>
       /// <returns>Flag, as to successful change or not.</returns>
        public bool SetWorkflowModule(int moduleid, int workflowid)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == moduleid);
                    if (prOrder != null)
                    {
                        prOrder.AttributeTemplateRecordID = workflowid;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes any duplicate templates.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool RemoveTemplateDuplication(int templateId)
        {
            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_RemoveTemplateDuplication", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var idParam = new SqlParameter("@Id", SqlDbType.Int);
                    idParam.Value = templateId;
                    command.Parameters.Add(idParam);
                    command.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}


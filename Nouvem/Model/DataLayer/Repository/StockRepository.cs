﻿// -----------------------------------------------------------------------
// <copyright file="StockRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using DevExpress.Xpf.Editors.Helpers;
using Nouvem.BusinessLogic;
using Nouvem.Model.Enum;
using Nouvem.Shared;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;
    using Nouvem.Global;

    public class StockRepository : NouvemRepositoryBase, IStockRepository
    {
        /// <summary>
        /// Checks the dispatched stock against the production stock.
        /// </summary>
        /// <returns>Flag, as to successful run or not.</returns>
        public bool CheckStock()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.CheckStock();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Prices a group of stocktransactions.
        /// </summary>
        /// <param name="ids">The comman delimited stock ids.</param>
        /// <param name="price">The price to apply.</param>
        /// <returns>Flag, as to successful pricing or not.</returns>
        public bool PriceStocktransactions(string ids, decimal price)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_PriceStocktransactions(ids,price);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets transactions related to the current device and module.
        /// </summary>
        /// <param name="deviceId">The current device.</param>
        /// <param name="nouTransactionTypeId">The module type.</param>
        /// <returns>A collection a related transactions.</returns>
        public IList<App_GetDeviceTransactions_Result> GetTransactions(int deviceId, int nouTransactionTypeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetDeviceTransactions(deviceId, nouTransactionTypeId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the batch/product stock levels.
        /// </summary>
        /// <returns>Batch/Product stock levels.</returns>
        public int? GetSerialByCarcassNo(string carcassNo)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetSerialFromCarcassNo(carcassNo).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Gets the batch/product stock levels.
        /// </summary>
        /// <returns>Batch/Product stock levels.</returns>
        public IList<StockDetail> GetStock()
        {
            var stock = new List<StockDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    stock = (from s in entities.App_GetBatchStock()
                        select new StockDetail
                        {
                            StockDetailID = s.INMasterStockDataID,
                            INMasterID = s.INMasterID,
                            TransactionWeight = s.Wgt,
                            TransactionQty = s.Qty,
                            BatchNumberID = s.BatchNumberID,
                            Generic1 = s.Number,
                            ProductID = s.INMasterID
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return stock;
        }

        /// <summary>
        /// Gets all the edits.
        /// </summary>
        /// <returns>The transaction edits.</returns>
        public IList<StockDetail> GetTransactionEdits()
        {
            var edits = new List<StockDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    edits = (from detail in entities.TransactionEdits
                        where detail.Deleted == null
                        select new StockDetail
                        {
                            StockDetailID = (int) detail.StockTransactionID,
                            INMasterID = (int) detail.INMasterID,
                            TransactionWeight = detail.TransactionWeight,
                            TransactionQty = detail.TransactionQTY,
                            BatchNumberID = detail.BatchNumberID,
                            ProductID = detail.INMasterID,
                            EditDate = detail.EditDate,
                            UserMasterID = detail.UserMasterID_Edit,
                            EditStatusID = detail.PostEdit,
                            DeviceMasterID = detail.DeviceID_Edit
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return edits;
        }

        /// <summary>
        /// Updates batch/product stock levels.
        /// </summary>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateStock(IList<StockDetail> stocks)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockDetail in stocks)
                    {
                        entities.App_UpdateProductStock(stockDetail.StockDetailID, stockDetail.ProductID,
                            stockDetail.TransactionWeight, stockDetail.TransactionQty, stockDetail.BatchNumberID,
                            NouvemGlobal.UserId, NouvemGlobal.DeviceId);
                    }
             
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the adjusted stock items.
        /// </summary>
        /// <param name="stock">All the stock items.</param>
        /// <param name="transactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool AdjustStock(IList<ProductData> stock, NouTransactionType transactionType, Warehouse warehouse)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AdjustStock(): Attempting to adjust {0} stock items.", stock.Count));
            var stockTakeDetails = new List<StockTakeDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    stock.Where(x => x.StockAdjusted).ToList().ForEach(x =>
                    {
                        //this.Log.LogDebug(this.GetType(), string.Format("Stock with id:{0} adjusting quantity from {1} to {2}.", x.StockID, x.Quantity, x.StockQuantity));
                 
                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = x.Id,
                            Code = x.Code,
                            Name = x.Name ?? string.Empty,
                            MaxWeight = x.MaxWeight,
                            MinWeight = x.MinWeight,
                            NominalWeight = x.NominalWeight,
                            TypicalPieces = x.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var localQty = 0;
                        //if (x.Quantity > x.StockQuantity)
                        //{
                            localQty = x.StockQuantity - x.Quantity;
                       // }
                       // else
                      //  {
                           // localQty = x.Quantity - x.StockQuantity;
                      //  }

                        var stockTransaction = new StockTransaction
                        {
                            INMasterID = x.Id,
                            TransactionDate = DateTime.Now,
                            NouTransactionTypeID = transactionType.NouTransactionTypeID,
                            WarehouseID = warehouse.WarehouseID,
                            TransactionQTY = localQty,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            UserMasterID = NouvemGlobal.UserId
                        };

                        entities.StockTransactions.Add(stockTransaction);

                        // create a stock take detail record
                        var detail = new StockTakeDetail
                        {
                            OpeningQty = x.Quantity,
                            ClosingQty = x.Quantity + Math.Abs(x.Quantity - x.StockQuantity),
                            INMasterID = stockTransaction.INMasterID
                        };

                        stockTakeDetails.Add(detail);
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} stock items successfully updated", stock.Count));

                    // add to the stock take
                    var stockTake = new Nouvem.Model.DataLayer.StockTake
                    {
                        CreationDate = DateTime.Now,
                        Description = Constant.EposStockTake,WarehouseID = warehouse.WarehouseID
                    };

                    entities.StockTakes.Add(stockTake);

                    // we need to add the non adjusted stock to our stock take as well.
                    foreach (var item in stock.Where(x => !x.StockAdjusted))
                    {
                        stockTakeDetails.Add(new StockTakeDetail
                        {
                            OpeningQty = item.Quantity,
                            ClosingQty = item.Quantity,
                            INMasterID = item.Id
                        });
                    }

                    // add the details
                    stockTakeDetails.ForEach(x => x.StockTakeID = stockTake.StockTakeID);
                    entities.StockTakeDetails.AddRange(stockTakeDetails);

                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a new stock take.
        /// </summary>
        /// <param name="stockTake">The stock take to add.</param>
        /// <returns>A Flag, as to whether the stock take has been added.</returns>
        public bool AddStockTake(Model.DataLayer.StockTake stockTake)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to add a new stock take");
      
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.StockTakes.Add(stockTake);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "stock takes successfully added");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a new stock take.
        /// </summary>
        /// <param name="stockTake">The stock take to add.</param>
        /// <returns>A Flag, as to whether the stock take has been added.</returns>
        public bool UpdateStockTake(DataLayer.StockTake stockTake)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to update stock take");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbStock = entities.StockTakes.FirstOrDefault(x => x.StockTakeID == stockTake.StockTakeID);
                    if (dbStock != null)
                    {
                        dbStock.Description = stockTake.Description;
                        dbStock.NouDocStatusID = stockTake.NouDocStatusID;
                        entities.SaveChanges();

                        this.Log.LogDebug(this.GetType(), "stock take successfully updated");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the application stock takes.
        /// </summary>
        /// <returns>A collection of all the application stock takes.</returns>
        public IList<StockTake> GetStockTakes()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the stock takes");
            var stockTakes = new List<StockTake>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    stockTakes = (from stockTake in entities.App_GetStocktakes()
                        select new StockTake
                        {
                            StockTakeID = stockTake.StockTakeID,
                            CreationDate = stockTake.CreationDate,
                            Description = stockTake.Description,
                            WarehouseID = stockTake.WarehouseID,
                            Locations = stockTake.Locations,
                            NouDocStatusID = stockTake.NouDocStatusID
                        }).ToList();

                    //var changeStatus = false;
                    //foreach (var stock in stockTakes)
                    //{
                    //    if (stock.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID && stock.StockTakeDetails.Any())
                    //    {
                    //        stock.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                    //        changeStatus = true;
                    //    }
                    //}

                    //if (changeStatus)
                    //{
                    //    entities.SaveChanges();
                    //}

                    this.Log.LogDebug(this.GetType(), string.Format("{0} stock takes successfully retrieved", stockTakes.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return stockTakes;
        }

        public void StockTakeDetailCheck(IList<StockTakeData> details)
        {
            var missing = new List<StockTakeData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var detail in details)
                    {
                        var serial = detail.Barcode.Trim().ToInt();
                        if (!entities.StockTransactions.Any(x => x.Serial == serial && x.WarehouseID == 2004))
                        {
                            missing.Add(detail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        public IList<App_GetStockTakeData_Result> GetStockTakeData(StockTake stockTake)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities
                        .App_GetStockTakeData(stockTake.StockTakeID, NouvemGlobal.UserId, NouvemGlobal.DeviceId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        public void GetStockTakeDetails(StockTake stockTake)
        {
            //this.ConsumeDispatchedStock();

            try
            {
                IEnumerable<App_GetStockTakeDetails_Result> stockDetails;
                using (var entities = new NouvemEntities())
                {
                    stockDetails = entities.App_GetStockTakeDetails(stockTake.StockTakeID);
                }

                stockTake.StockTakeDetails = (from detail in stockDetails
                                              select new StockTakeData
                                              {
                                                  StockTakeDetailID = detail.StockTakeDetailID.ToInt(),
                                                  StockTakeID = detail.StockTakeID,
                                                  StockTransactionId = detail.StockTransactionID,
                                                  UserId = detail.UserID,
                                                  INMasterID = detail.INMasterID,
                                                  Barcode = detail.Barcode,
                                                  WarehouseID = detail.WarehouseID,
                                                  WarehouseName = detail.WarehouseName,
                                                  CurrentWarehouseName = detail.WarehouseNameCurrent,
                                                  OpeningQty = detail.OpeningQty,
                                                  ClosingQty = detail.ClosingQty,
                                                  OpeningWgt = detail.OpeningWgt,
                                                  ClosingWgt = detail.ClosingWgt,
                                                  Qty = detail.Qty,
                                                  Wgt = detail.Wgt,
                                                  StockDeletionDate = detail.StockDeletedDate,
                                                  CreationDate = detail.CreationDate
                                              }).ToList();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        //public void GetStockTakeDetails(StockTake stockTake)
        //{
        //    this.Log.LogDebug(this.GetType(), "GetStockTakeDetails(): Attempting to retrieve all the stock takes");
        //    //this.ConsumeDispatchedStock();

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            var dbStockTake = entities.StockTakes.FirstOrDefault(x => x.StockTakeID == stockTake.StockTakeID);
        //            if (dbStockTake != null)
        //            {
        //                stockTake.StockTakeDetails = (from detail in dbStockTake.StockTakeDetails.Where(x => x.Deleted == null)
        //                    select new StockTakeData
        //                    {
        //                        StockTakeDetailID = detail.StockTakeDetailID,
        //                        StockTakeID = detail.StockTakeID,
        //                        StockTransactionId = detail.StockTransactionID,
        //                        UserId = detail.UserID,
        //                        INMasterID = detail.INMasterID,
        //                        Barcode = detail.Barcode,
        //                        WarehouseID = detail.WarehouseID,
        //                        WarehouseName = detail.Warehouse.Name,
        //                        CurrentWarehouseName = detail.WarehouseID_Current != null ? detail.Warehouse1.Name : string.Empty,
        //                        OpeningQty = detail.OpeningQty,
        //                        ClosingQty = detail.ClosingQty,
        //                        OpeningWgt = detail.OpeningWgt,
        //                        ClosingWgt = detail.ClosingWgt,
        //                        Qty = detail.Qty,
        //                        Wgt = detail.Wgt,
        //                        StockDeletionDate = detail.StockDeletedDate,
        //                        CreationDate = detail.CreationDate
        //                    }).ToList();

        //                if (dbStockTake.StockTakeDetails.Any())
        //                {
        //                    dbStockTake.NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
        //                    entities.SaveChanges();
        //                }
        //            }

        //            this.Log.LogDebug(this.GetType(), string.Format("{0} stock takes successfully retrieved", stockTake.StockTakeDetails.Count));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }
        //}

        /// <summary>
        /// Reconciles the stock.
        /// </summary>
        /// <returns>A flag, indicating a successful reconciliation or not.</returns>
        public bool ReconcileStock(StockTake stockData)
        {
            this.Log.LogDebug(this.GetType(), "ReconcileStock(): Attempting to rconcile stock");
            //var data = stockData.StockTakeDetails;
            //ProgressBar.SetUp(1, data.Count);

            //var missingStock = data.Where(x => x.StockDetailType == StockTakeData.DetailType.Missing).ToList();
            //var recoveredStock = data.Where(x => x.StockDetailType == StockTakeData.DetailType.Recovered).ToList();
            //var accountedForStock = data.Where(x => x.StockDetailType == StockTakeData.DetailType.AccountedFor).ToList();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Database.CommandTimeout = 1800;
                    entities.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                        "exec App_ReconcileStockTake @StocktakeId = {0},@UserID = {1},@DeviceID = {2},@PreReconcilliation = {3}", stockData.StockTakeID, NouvemGlobal.UserId, NouvemGlobal.DeviceId, 0);
                    entities.Database.CommandTimeout = 30;

                    // entities.App_ReconcileStockTake(stockData.StockTakeID, NouvemGlobal.UserId, NouvemGlobal.DeviceId);
                    //foreach (var detail in missingStock)
                    //{
                    //    ProgressBar.Run();
                    //    var dbStock =
                    //        entities.StockTransactions.FirstOrDefault(
                    //            x => x.StockTransactionID == detail.StockTransactionId);

                    //    if (dbStock != null)
                    //    {
                    //        if (dbStock.WarehouseID != detail.WarehouseID)
                    //        {
                    //            // location has changed since stocktake i.e. recovered/used, so ignore. It's gone from here.
                    //            continue;
                    //        }

                    //        dbStock.InLocation = false;
                    //        if (dbStock.IsBox == true)
                    //        {
                    //            var boxStock = dbStock.StockTransaction1;
                    //            foreach (var boxStockItem in boxStock)
                    //            {
                    //                boxStockItem.InLocation = false;
                    //            }
                    //        }
                    //    }
                    //}

                    //foreach (var detail in recoveredStock)
                    //{
                    //    ProgressBar.Run();
                    //    var dbStock =
                    //        entities.StockTransactions.FirstOrDefault(
                    //            x => x.StockTransactionID == detail.StockTransactionId);

                    //    if (dbStock != null)
                    //    {
                    //        dbStock.Deleted = null;
                    //        dbStock.InLocation = true;
                    //        dbStock.WarehouseID = detail.WarehouseID.ToInt();
                    //        dbStock.Consumed = null;

                    //        //if (dbStock.StockTransactionID_Container == null)
                    //        //{
                                
                    //        //}

                    //        if (dbStock.IsBox == true)
                    //        {
                    //            var boxStock = dbStock.StockTransaction1;
                    //            foreach (var boxStockItem in boxStock)
                    //            {
                    //                boxStockItem.Deleted = null;
                    //                dbStock.InLocation = true;
                    //                boxStockItem.WarehouseID = detail.WarehouseID.ToInt();
                    //                if (boxStockItem.Consumed == null)
                    //                {
                    //                    boxStockItem.Consumed = DateTime.Now;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    //foreach (var detail in accountedForStock)
                    //{
                    //    ProgressBar.Run();
                    //    var dbStock =
                    //        entities.StockTransactions.FirstOrDefault(
                    //            x => x.StockTransactionID == detail.StockTransactionId);

                    //    if (dbStock != null)
                    //    {
                    //        dbStock.Deleted = null;
                    //        dbStock.InLocation = true;
                    //        dbStock.Consumed = null;

                    //        if (dbStock.IsBox == true)
                    //        {
                    //            var boxStock = dbStock.StockTransaction1;
                    //            foreach (var boxStockItem in boxStock)
                    //            {
                    //                boxStockItem.Deleted = null;
                    //                dbStock.InLocation = true;
                    //                boxStockItem.WarehouseID = detail.WarehouseID.ToInt();
                    //                if (boxStockItem.Consumed == null)
                    //                {
                    //                    boxStockItem.Consumed = DateTime.Now;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    //var stockTake = entities.StockTakes.FirstOrDefault(x => x.StockTakeID == stockData.StockTakeID);
                    //if (stockTake != null)
                    //{
                    //    stockTake.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                    //}

                    //entities.SaveChanges();
                    //this.Log.LogDebug(this.GetType(), "Stock take successfully reconciled");
             
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            //finally
            //{
            //    ProgressBar.Reset();
            //}
          
            return false;
        }

        /// <summary>
        /// Retrieves the stock for the stock reconciliation.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        public IList<StockTakeData> GetStock(StockTake stockTake)
        {
            this.Log.LogDebug(this.GetType(), "GetStock(): Attempting to retrieve all the stock for the input locations");
            var dbStock = new List<StockTakeData>();

            var locations = stockTake.Locations.Split(',').Select(x => x.ToInt()).ToList();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    dbStock =
                        (from stock in
                            entities.StockTransactions.Where(
                                x => locations.Contains(x.WarehouseID) 
                                    && x.Deleted == null 
                                    && x.Consumed == null 
                                    && x.MissingDate == null
                                    && x.Warehouse.StockLocation
                                    && x.TransactionDate <= stockTake.CutOffDate)
                            select new StockTakeData
                            {
                                Barcode = SqlFunctions.StringConvert((double)stock.Serial).Trim(),
                                INMasterID= stock.INMasterID,
                                StockTransactionId = stock.StockTransactionID,
                                Qty = stock.TransactionQTY,
                                WarehouseID = stock.WarehouseID,
                                Wgt = stock.TransactionWeight
                            }).ToList();
                   
                    this.Log.LogDebug(this.GetType(), string.Format("{0} stock successfully retrieved", dbStock.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dbStock;
        }

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        /// <param name="stockTakeId">The stock take the detail belongs to.</param>
        /// <param name="warehouseId">The current location.</param>
        /// <returns>A stock take detail result.</returns>
        public StockTakeData AddToStockTake(string barcode, int stockTakeId, int warehouseId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Adding barcode:{0} to stock take:{1}", barcode, stockTakeId));
            var stockData = new StockTakeData { StockTakeID = stockTakeId };
            var dbStockTake = new StockTakeDetail {Barcode = barcode, StockTakeID = stockTakeId, UserID = NouvemGlobal.UserId, WarehouseID = warehouseId};
            stockData.Barcode = barcode;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localBarcode = barcode.ToInt();
                    var transaction = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID).FirstOrDefault(x => x.Serial == localBarcode);

                    if (transaction != null)
                    {
                        if (transaction.Comments == "Pallet")
                        {
                            var stock = entities
                                .App_AddToStockTake(stockTakeId, barcode, warehouseId, NouvemGlobal.UserId, true)
                                .ToList();
                            stockData.PalletStock = new List<StockTakeData>();
                            foreach (var appAddToStockTakeResult in stock)
                            {
                                stockData.PalletStock.Add(new StockTakeData
                                {
                                    StockTakeID = stockTakeId,
                                    Barcode = appAddToStockTakeResult.Serial,
                                    INMasterID = appAddToStockTakeResult.INMasterID,
                                    Qty = Math.Round(appAddToStockTakeResult.TransactionQTY.ToDecimal(), 0),
                                    Wgt = Math.Round(appAddToStockTakeResult.TransactionWeight.ToDecimal(), 0)
                                });
                            }

                            return stockData;
                        }

                        dbStockTake.WarehouseID_Current = transaction.WarehouseID;
                        dbStockTake.INMasterID = transaction.INMasterID;
                        dbStockTake.StockTransactionID = transaction.StockTransactionID;
                        dbStockTake.Qty = transaction.TransactionQTY;
                        dbStockTake.Wgt = transaction.TransactionWeight;
                        dbStockTake.StockDeletedDate = transaction.Deleted;
                        dbStockTake.CreationDate = DateTime.Now;
                        
                        this.Log.LogDebug(this.GetType(), "Stock take detail saved");

                        stockData.INMasterID = transaction.INMasterID;
                        stockData.Qty = Math.Round(transaction.TransactionQTY.ToDecimal(), 0);
                        stockData.Wgt = Math.Round(transaction.TransactionWeight.ToDecimal(),2);
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), "Stock transaction not found");
                    }

                    entities.StockTakeDetails.Add(dbStockTake);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return stockData;
        }

        /// <summary>
        /// Removes a stock take detail.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        /// <returns>A flag, as to a successful removal or not.</returns>
        public bool RemoveFromStockTake(string barcode)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Removing barcode:{0} from stock take", barcode));
            
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var detail = entities.StockTakeDetails.FirstOrDefault(x => x.Barcode == barcode);

                    if (detail != null)
                    {
                        detail.Deleted = DateTime.Now;
                        this.Log.LogDebug(this.GetType(), "Stock take detail removed");
                        entities.SaveChanges();
                        return true;
                    }

                    this.Log.LogDebug(this.GetType(), "Stock detail not found");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a stock move order.
        /// </summary>
        /// <param name="order">The order to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateStockMoveOrder(Sale order)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.StockMoveOrders.FirstOrDefault(x => x.StockMoveOrderID == order.SaleID);
                    if (dbOrder == null)
                    {
                        return false;
                    }

                    dbOrder.MoveFromLocation = order.WarehouseIDFrom;
                    dbOrder.MoveToLocation = order.WarehouseID;
                    dbOrder.MoveDate = order.DeliveryDate;
                    dbOrder.UserMasterID = NouvemGlobal.UserId;
                    dbOrder.DeviceMasterID = NouvemGlobal.DeviceId;
                    dbOrder.UserMasterID_Operator = order.UserIDCreation;
                    dbOrder.Remarks = order.Remarks;
                    dbOrder.Reference = order.Reference;
                    dbOrder.NouDocStatusID = order.NouDocStatusID;
                    dbOrder.DocumentDate = order.DocumentDate;

                    foreach (var detail in order.StockMoveDetails)
                    {
                        var localDetail = entities.StockMoveOrderDetails.FirstOrDefault(x =>
                            x.StockMoveOrderDetailID == detail.StockMoveDetailID);
                        if (localDetail == null)
                        {
                            entities.StockMoveOrderDetails.Add(new StockMoveOrderDetail
                            {
                                StockMoveOrderID = dbOrder.StockMoveOrderID,
                                QuantityToMove = detail.Qty,
                                WeightToMove = detail.Wgt,
                                INMasterID = detail.INMasterID,
                                BatchID = detail.BatchID
                            });
                        }
                        else
                        {
                            localDetail.BatchID = detail.BatchID;
                            localDetail.INMasterID = detail.INMasterID;
                            localDetail.QuantityToMove = detail.Qty;
                            localDetail.WeightToMove = detail.Wgt;
                        }

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a stock move order.
        /// </summary>
        /// <param name="order">The order to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool ChangeStockMoveOrderStatus(Sale order)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.StockMoveOrders.FirstOrDefault(x => x.StockMoveOrderID == order.SaleID);
                    if (dbOrder == null)
                    {
                        return false;
                    }

                    dbOrder.NouDocStatusID = order.NouDocStatusID;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a stock move order.
        /// </summary>
        /// <param name="order">The order to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public int AddStockMoveOrder(Sale order)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbNumber =
                        entities.DocumentNumberings.First(
                            x =>
                                x.NouDocumentName.DocumentName.Equals(Constant.StockMove) &&
                                x.DocumentNumberingType.Name.Equals(Constant.Standard));

                    order.Number = dbNumber.NextNumber;
                    order.DocumentNumberingID = dbNumber.DocumentNumberingID;

                    var dbOrder = new StockMoveOrder
                    {
                        MoveFromLocation = order.WarehouseIDFrom,
                        MoveToLocation = order.WarehouseID,
                        MoveDate = order.DeliveryDate,
                        UserMasterID = NouvemGlobal.UserId,
                        DeviceMasterID = NouvemGlobal.DeviceId,
                        UserMasterID_Operator = order.UserIDCreation,
                        Remarks = order.Remarks,
                        Reference = order.Reference,
                        Number = order.Number,
                        DocumentNumberingID = order.DocumentNumberingID,
                        NouDocStatusID = order.NouDocStatusID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate
                    };

                    entities.StockMoveOrders.Add(dbOrder);
                    entities.SaveChanges();

                    var nextNo = dbNumber.NextNumber + 1;
                    if (nextNo > dbNumber.LastNumber)
                    {
                        // Last number reached, so reset back to first number.
                        nextNo = dbNumber.FirstNumber;
                    }

                    dbNumber.NextNumber = nextNo;
                    entities.SaveChanges();

                    foreach (var detail in order.StockMoveDetails)
                    {
                        entities.StockMoveOrderDetails.Add(new StockMoveOrderDetail
                        {
                            StockMoveOrderID = dbOrder.StockMoveOrderID,
                            QuantityToMove = detail.Qty,
                            WeightToMove = detail.Wgt,
                            INMasterID = detail.INMasterID,
                            BatchID = detail.BatchID
                        });

                        entities.SaveChanges();
                    }

                    return dbOrder.StockMoveOrderID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="location">The warehouse to move to.</param>
        /// <param name="subLocation">The sub warehouse to move to.</param>
        /// <param name="barcode">The scanned barcode.</param>
        /// <returns>A stock movement result.</returns>
        public IList<StockTakeData> MoveStock(int location, int? subLocation, string barcode, int orderId, bool logForReprint)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Adding barcode:{0} to location:{1}", barcode, location));
            var stockDatas = new List<StockTakeData>();
            //var stockData = new StockTakeData { Barcode = barcode, WarehouseID = location, WarehouseLocationID = subLocation };

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localBarcode = barcode.ToInt();
                    var transaction = entities.StockTransactions.FirstOrDefault(x => x.Serial == localBarcode && x.Deleted == null);

                    if (transaction != null)
                    {
                        StockTransaction localTrans;
                        if (transaction.Comments == Constant.Pallet)
                        {
                            var boxItems = entities.StockTransactions
                                .Where(x => x.StockTransactionID_Pallet == transaction.StockTransactionID
                                            && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId
                                            && x.Deleted == null);
                            foreach (var item in boxItems)
                            {
                                var palletData = new StockTakeData { Barcode = item.Serial.ToString(), WarehouseID = location, WarehouseLocationID = subLocation };
                                palletData.WarehouseIDPrevious = item.WarehouseID;
                                palletData.WarehouseLocationIDPrevious = item.LocationID;
                                palletData.INMasterID = item.INMasterID;
                                palletData.ClosingQty = Math.Round(item.TransactionQTY.ToDecimal(), 0);
                                palletData.ClosingWgt = Math.Round(item.TransactionWeight.ToDecimal(), 2);
                                stockDatas.Add(palletData);
                                localTrans = this.CreateTransaction(item);
                                localTrans.WarehouseID = location;
                                localTrans.LocationID = subLocation;
                                localTrans.InLocation = true;
                                localTrans.Consumed = null;
                                localTrans.MissingDate = null;
                                localTrans.StockTransactionID_StockMove = orderId;
                                entities.StockTransactions.Add(localTrans);
                                entities.SaveChanges();
                                item.Consumed = DateTime.Now;
                                item.NouTransactionTypeID = NouvemGlobal.TransactionTypeEditId;
                            }

                            transaction.WarehouseID = location;
                            transaction.LocationID = subLocation;
                            transaction.StockTransactionID_StockMove = orderId;
                            transaction.EditDate = DateTime.Now;
                            entities.SaveChanges();
                            return stockDatas;
                        }

                        var stockData = new StockTakeData { Barcode = barcode, WarehouseID = location, WarehouseLocationID = subLocation };
                        stockData.WarehouseIDPrevious = transaction.WarehouseID;
                        stockData.WarehouseLocationIDPrevious = transaction.LocationID;

                        var transToConsume = entities.StockTransactions.Where(x => x.Serial == localBarcode && x.Deleted == null && x.Consumed == null && x.NouTransactionTypeID != 7);
                        foreach (var stockTransaction in transToConsume)
                        {
                            stockTransaction.Consumed = DateTime.Now;
                            entities.SaveChanges();
                        }

                        localTrans = this.CreateTransaction(transaction);
                        localTrans.WarehouseID = location;
                        localTrans.LocationID = subLocation;
                        localTrans.Consumed = null;
                        localTrans.MissingDate = null;
                        localTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        localTrans.UserMasterID = NouvemGlobal.UserId;
                        localTrans.StockTransactionID_StockMove = orderId;
                        entities.StockTransactions.Add(localTrans);
                        entities.SaveChanges();

                        if (logForReprint)
                        {
                            entities.PalletStocks.Add(new PalletStock
                            {
                                StockTransactionID = localTrans.StockTransactionID,
                                IsBox = localTrans.IsBox,
                                INMasterID = localTrans.INMasterID,
                                BPMasterID = localTrans.BPMasterID,
                                WarehouseID = localTrans.WarehouseID,
                                NouTransactionTypeID = localTrans.NouTransactionTypeID
                            });
                        }

                        if (transaction.Consumed == null)
                        {
                            transaction.Consumed = DateTime.Now;
                        }

                        if (transaction.IsBox == true)
                        {
                            var boxItems = transaction.StockTransaction1;
                            foreach (var item in boxItems)
                            {
                                var boxItem = this.CreateTransaction(item);
                                boxItem.WarehouseID = location;
                                boxItem.LocationID = subLocation;
                                localTrans.StockTransactionID_StockMove = orderId;
                                boxItem.StockTransactionID_Container = localTrans.StockTransactionID;
                                entities.StockTransactions.Add(boxItem);
                                entities.SaveChanges();
                                item.Consumed = DateTime.Now;

                                if (logForReprint && ApplicationSettings.ReprintStockMoveBoxItem)
                                {
                                    entities.PalletStocks.Add(new PalletStock
                                    {
                                        StockTransactionID = boxItem.StockTransactionID,
                                        INMasterID = boxItem.INMasterID,
                                        BPMasterID = boxItem.BPMasterID,
                                        WarehouseID = boxItem.WarehouseID,
                                        NouTransactionTypeID = localTrans.NouTransactionTypeID
                                    });
                                }
                            }
                        }

                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Stock move complete");

                        stockData.INMasterID = transaction.INMasterID;
                        stockData.ClosingQty = Math.Round(transaction.TransactionQTY.ToDecimal(), 0);
                        stockData.ClosingWgt = Math.Round(transaction.TransactionWeight.ToDecimal(), 2);
                        stockDatas.Add(stockData);
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), "Stock transaction not found");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return stockDatas;
        }

        /// <summary>
        /// Gets a stock move order by first/last.
        /// </summary>
        /// <param name="id">The id to retrieve the data for.</param>
        /// <returns>The retrieved data.</returns>
        public Sale GetStockMoveOrderByFirstLast(bool first)
        {
            var order = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    StockMoveOrder dbOrder = null;
                    if (first)
                    {
                        dbOrder = entities.StockMoveOrders.FirstOrDefault();
                    }
                    else
                    {
                        dbOrder = entities.StockMoveOrders.OrderByDescending(x => x.StockMoveOrderID).FirstOrDefault();
                    }

                    if (dbOrder == null)
                    {
                        return order;
                    }

                    return new Sale
                    {
                        SaleID = dbOrder.StockMoveOrderID,
                        WarehouseID = dbOrder.MoveToLocation,
                        WarehouseIDFrom = dbOrder.MoveFromLocation,
                        DocumentNumberingID = dbOrder.DocumentNumberingID,
                        Number = dbOrder.Number,
                        DocumentDate = dbOrder.DocumentDate,
                        CreationDate = dbOrder.CreationDate,
                        UserIDCreation = dbOrder.UserMasterID_Operator,
                        NouDocStatusID = dbOrder.NouDocStatusID,
                        Remarks = dbOrder.Remarks,
                        Reference = dbOrder.Reference,
                        StockMoveDetails = (from detail in entities.App_GetStockMoveDetails(dbOrder.StockMoveOrderID)
                                            select new StockTakeData
                                            {
                                                StockMoveDetailID = dbOrder.StockMoveOrderID,
                                                Qty = detail.QuantityToMove,
                                                Wgt = detail.WeightToMove,
                                                ClosingQty = detail.QtyMoved,
                                                ClosingWgt = detail.WeightMoved,
                                                INMasterID = detail.INMasterID,
                                                BatchID = detail.BatchID
                                            }).ToList()
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return order;
        }

        /// <summary>
        /// Moves/Splits batch stock.
        /// </summary>
        /// <param name="stockdata">The batch stock data.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveBatchStock(IList<StockTakeData> stockdata)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var stockTakeData in stockdata)
                    {
                        entities.App_MoveProductStockData(stockTakeData.INMasterID, stockTakeData.BatchID,
                            stockTakeData.Qty, stockTakeData.Wgt, stockTakeData.WarehouseID);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetStockMoveOrders(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            var orders = new List<Sale>();
            end = end.AddDays(1);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.StockMoveOrders
                              where orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              && (order.DocumentDate >= start && order.DocumentDate <= end)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.StockMoveOrderID,
                                  WarehouseID = order.MoveToLocation,
                                  WarehouseIDFrom = order.MoveFromLocation,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  UserIDCreation = order.UserMasterID_Operator,
                                  NouDocStatusID = order.NouDocStatusID,
                                  Remarks = order.Remarks,
                                  Reference = order.Reference
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Gets a stock move order by id.
        /// </summary>
        /// <param name="id">The id to retrieve the data for.</param>
        /// <returns>The retrieved data.</returns>
        public Sale GetStockMoveOrderById(int id)
        {
            var order = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.StockMoveOrders.FirstOrDefault(x => x.StockMoveOrderID == id);
                    if (dbOrder == null)
                    {
                        return order;
                    }

                    order = new Sale
                    {
                        SaleID = dbOrder.StockMoveOrderID,
                        WarehouseID = dbOrder.MoveToLocation,
                        WarehouseIDFrom = dbOrder.MoveFromLocation,
                        DocumentNumberingID = dbOrder.DocumentNumberingID,
                        Number = dbOrder.Number,
                        UserIDCreation = dbOrder.UserMasterID_Operator,
                        DocumentDate = dbOrder.DocumentDate,
                        CreationDate = dbOrder.CreationDate,
                        NouDocStatusID = dbOrder.NouDocStatusID,
                        Remarks = dbOrder.Remarks,
                        Reference = dbOrder.Reference,
                        StockMoveDetails = (from detail in entities.App_GetStockMoveDetails(dbOrder.StockMoveOrderID)
                                            select new StockTakeData
                                            {
                                                StockMoveDetailID = detail.StockMoveOrderDetailID,
                                                Qty = detail.QuantityToMove,
                                                Wgt = detail.WeightToMove,
                                                ClosingQty = detail.QtyMoved,
                                                ClosingWgt = detail.WeightMoved,
                                                INMasterID = detail.INMasterID,
                                                BatchID = detail.BatchID
                                            }).ToList(),
                        StockMoveOrderDetails = (from detail in entities.StockTransactions.Where(x => x.StockTransactionID_StockMove == dbOrder.StockMoveOrderID && x.Deleted == null)
                                                 select new StockTakeData
                                                 {
                                                     Barcode = SqlFunctions.StringConvert((double)detail.Serial).Trim(),
                                                     WarehouseID = detail.WarehouseID,
                                                     WarehouseName = detail.Warehouse != null ? detail.Warehouse.Name : string.Empty,
                                                     SubWarehouseName = detail.WarehouseLocation != null ? detail.WarehouseLocation.Name : string.Empty,
                                                     ClosingQty = detail.TransactionQTY,
                                                     ClosingWgt = detail.TransactionWeight,
                                                     INMasterID = detail.INMasterID,
                                                     BatchID = detail.BatchNumberID
                                                 }).ToList()
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var detail in order.StockMoveDetails)
            {
                detail.Qty = detail.Qty.ToDecimal();
                detail.Wgt = detail.Wgt.ToDecimal();
                detail.ClosingQty = detail.ClosingQty.ToDecimal();
                detail.ClosingWgt = detail.ClosingWgt.ToDecimal();
            }

            return order;
        }

        /// <summary>
        /// Changes a batch data (product).
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="currentINMasterID">The id of the product to change.</param>
        /// <param name="inMasterId">The new product.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool ChangeBatchProduct(int prOrderid, int currentINMasterID, int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stock =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == prOrderid && x.Consumed == null
                                 && x.Deleted == null && x.INMasterID == currentINMasterID
                                 &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                 x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));

                    if (stock.Any())
                    {
                        var order = entities.PROrders.FirstOrDefault(x => x.PROrderID == prOrderid);
                        if (order != null)
                        {
                            order.INMasterID = inMasterId;
                            entities.SaveChanges();
                        }

                        foreach (var stockTransaction in stock)
                        {
                            entities.App_UpdateTransaction(
                                stockTransaction.StockTransactionID,
                                inMasterId,
                                stockTransaction.TransactionWeight,
                                stockTransaction.TransactionQTY,
                                stockTransaction.Price,
                                false,
                                NouvemGlobal.UserId,
                                NouvemGlobal.DeviceId);
                            //var newTrans = this.CreateTransaction(stockTransaction);
                            //newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                            //newTrans.UserMasterID = NouvemGlobal.UserId;
                            //newTrans.TransactionDate = DateTime.Now;
                            //newTrans.INMasterID = inMasterId;
                            //stockTransaction.Consumed = DateTime.Now;
                            //stockTransaction.Deleted = DateTime.Now;
                            //entities.StockTransactions.Add(newTrans);
                            //entities.SaveChanges();
                            //newTrans.Serial = newTrans.StockTransactionID;
                            //entities.SaveChanges();
                        }

                        return true;
                    }

                    //entities.App_MoveBatchStock(prOrderid, location.WarehouseId, location.WarehouseLocationId);
                    //return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Splits batch stock.
        /// </summary>
        /// <param name="prOrderid">The productionn order.</param>
        /// <param name="stockToSplit">The stock to split.</param>
        /// <param name="splitStock">The split stock.</param>
        /// <returns>Flag, as to successful split or not.</returns>
        public bool SplitBatchStock(int prOrderid, SaleDetail stockToSplit, SaleDetail splitStock)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stock =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.MasterTableID == prOrderid && x.INMasterID == stockToSplit.INMasterID
                                 && x.Deleted == null && x.Consumed == null
                                 &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                 x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));

                    if (stock != null)
                    {
                        var newTrans = this.CreateTransaction(stock);
                        newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        newTrans.UserMasterID = NouvemGlobal.UserId;
                        newTrans.TransactionDate = DateTime.Now;
                        newTrans.TransactionWeight = splitStock.TransactionWeight.ToDecimal();
                        newTrans.TransactionQTY = splitStock.TransactionQty.ToDecimal();
                        newTrans.INMasterID = splitStock.INMasterID;
                        newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeStockMoveId.ToInt();

                        if (stockToSplit.INMasterID == splitStock.INMasterID)
                        {
                            var newSplitId = 1;
                            var splitId = stock.SplitID;
                            if (splitId != null)
                            {
                                newSplitId = splitId.ToInt() + 1;
                            }

                            newTrans.SplitID = newSplitId;
                        }

                        var splitTrans = this.CreateTransaction(stock);
                        splitTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        splitTrans.UserMasterID = NouvemGlobal.UserId;
                        splitTrans.TransactionDate = DateTime.Now;
                        splitTrans.TransactionWeight = stock.TransactionWeight - splitStock.TransactionWeight.ToDecimal();
                        splitTrans.TransactionQTY = stock.TransactionQTY - splitStock.TransactionQty.ToDecimal();

                        stock.Consumed = DateTime.Now;
                        stock.Reference = null;
                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();
                        newTrans.Serial = newTrans.StockTransactionID;

                        entities.StockTransactions.Add(splitTrans);
                        entities.SaveChanges();
                        splitTrans.Serial = splitTrans.StockTransactionID;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Changes a batch data (wgt,qty and product).
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="wgt">The new wgt.</param>
        /// <param name="qty">The new qty.</param>
        /// <param name="inMasterId">The new product.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        public bool ChangeBatchData(int prOrderid, decimal wgt, decimal qty, int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stocks =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == prOrderid && x.Consumed == null
                                 && x.Deleted == null && x.INMasterID == inMasterId
                                 && x.Warehouse.StockLocation &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId)).ToList();

                    if (stocks.Any())
                    {
                        foreach (var stockTransaction in stocks)
                        {
                            stockTransaction.Consumed = DateTime.Now;
                        }

                        var newTrans = this.CreateTransaction(stocks.First());
                        newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        newTrans.UserMasterID = NouvemGlobal.UserId;
                        newTrans.TransactionDate = DateTime.Now;
                        newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeStockMoveId.ToInt();
                        newTrans.Consumed = null;
                        newTrans.TransactionWeight = wgt;
                        newTrans.TransactionQTY = qty;

                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();
                        newTrans.Serial = newTrans.StockTransactionID;
                        entities.SaveChanges();
                        entities.App_UpdateBatchWeight(prOrderid);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Moves a batch to the input location.
        /// </summary>
        /// <param name="fromLocation">The move from location.</param>
        /// <param name="toLocation">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveAllBatchStock(Location fromLocation, Location toLocation)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stocks =
                        entities.StockTransactions.Where(
                            x => x.WarehouseID == fromLocation.WarehouseId
                                 && x.Consumed == null
                                 && x.Warehouse.StockLocation
                                 && x.Deleted == null
                                 &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                 x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));

                    foreach (var stock in stocks)
                    {
                        var newTrans = this.CreateTransaction(stock);
                        newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        newTrans.UserMasterID = NouvemGlobal.UserId;
                        newTrans.WarehouseID = toLocation.WarehouseId;
                        newTrans.LocationID = toLocation.WarehouseLocationId;
                        newTrans.TransactionDate = DateTime.Now;
                        newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeStockMoveId.ToInt();
                        newTrans.ProcessID = toLocation.ProcessId;
                        stock.Consumed = DateTime.Now;
                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();
                        newTrans.Serial = newTrans.StockTransactionID;
                        entities.SaveChanges();

                        entities.App_UpdateBatchWeight(newTrans.MasterTableID);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Moves a batch to the input location.
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="location">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveBatchStock(int prOrderid, Location location)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stocks =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == prOrderid
                                 && x.Deleted == null && x.Consumed == null
                                 && x.Warehouse.StockLocation
                                 &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                 x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId)).ToList();

                    if (stocks.Any())
                    {
                        var newStock = stocks.First();
                        var newTrans = this.CreateTransaction(newStock);
                        foreach (var stock in stocks)
                        {
                            stock.Consumed = DateTime.Now;
                        }

                        newTrans.TransactionWeight = stocks.Sum(x => x.TransactionWeight);
                        newTrans.TransactionQTY = stocks.Sum(x => x.TransactionQTY);
                        newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        newTrans.UserMasterID = NouvemGlobal.UserId;
                        newTrans.WarehouseID = location.WarehouseId;
                        newTrans.LocationID = location.WarehouseLocationId;
                        newTrans.TransactionDate = DateTime.Now;
                        newTrans.Reference = null;
                        newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeStockMoveId.ToInt();
                        newTrans.ProcessID = location.ProcessId;
                        newTrans.Consumed = null;
                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();
                        newTrans.Serial = newTrans.StockTransactionID;
                        entities.SaveChanges();

                        entities.App_UpdateBatchWeight(prOrderid);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Moves a split batch to the input location, creating a replica batch.
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="location">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveSplitBatchStock(PROrder prOrder, SaleDetail product, Location location)
        {
            var prOrderid = prOrder.PROrderID;
            var inMasterId = product.INMasterID;
            var splitStockId = product.SplitStockID;
            var creatingNewBatch = location.StockMoveMode == StockMovementMode.SplitBatch;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stocks =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == prOrderid && x.INMasterID == inMasterId && x.Consumed == null
                                 && x.Warehouse.StockLocation
                                 && x.Deleted == null && x.SplitID == splitStockId
                                 &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                 x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));

                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == prOrderid);
                    if (dbOrder == null)
                    {
                        return false;
                    }

                    var newOrder = this.CreatePROrder(dbOrder);
                    newOrder.CreationDate = DateTime.Now;
                    newOrder.UserID = NouvemGlobal.UserId.ToInt();
                    newOrder.DeviceID = NouvemGlobal.DeviceId.ToInt();

                    var lastSplitOrder =
                        entities.PROrders.Where(x => x.BatchNumberID == dbOrder.BatchNumberID).OrderByDescending(x => x.PROrderID).FirstOrDefault();

                    var splitId = 1;
                    var localRefrence = string.Format("{0}-{1}", dbOrder.Reference, splitId);
                    if (lastSplitOrder != null)
                    {
                        if (lastSplitOrder.Reference.Length > 2)
                        {
                            var reference = lastSplitOrder.Reference.Substring(lastSplitOrder.Reference.Length - 3, 3);
                            if (reference.Contains("-"))
                            {
                                if (reference.ElementAt(0) == '-')
                                {
                                    splitId = reference.RemoveNonDecimals().ToInt() + 1;
                                    localRefrence = string.Format("{0}{1}", lastSplitOrder.Reference.Substring(0, lastSplitOrder.Reference.Length - 2), splitId);
                                }
                                else
                                {
                                    splitId = reference.ElementAt(2).ToString().ToInt() + 1;
                                    localRefrence = string.Format("{0}{1}", lastSplitOrder.Reference.Substring(0, lastSplitOrder.Reference.Length - 1), splitId);
                                }
                            }
                        }
                    }

                    newOrder.Reference = localRefrence;

                    BatchNumber newBatch = null;
                    if (creatingNewBatch)
                    {
                        newBatch = BatchManager.Instance.GenerateBatchNumber(false,"");
                        newOrder.BatchNumberID = newBatch.BatchNumberID;
                    }

                    entities.PROrders.Add(newOrder);
                    entities.SaveChanges();

                    // pass the new batch data out
                    prOrder.PROrderID = newOrder.PROrderID;
                    prOrder.Reference = newOrder.Reference;
                    prOrder.BatchNumberID = newOrder.BatchNumberID;

                    foreach (var stock in stocks)
                    {
                        var newTrans = this.CreateTransaction(stock);
                        newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                        newTrans.UserMasterID = NouvemGlobal.UserId;
                        newTrans.WarehouseID = location.WarehouseId;
                        newTrans.LocationID = location.WarehouseLocationId;
                        newTrans.TransactionDate = DateTime.Now;
                        newTrans.Reference = 2;
                        newTrans.MasterTableID = newOrder.PROrderID;
                        newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeStockMoveId.ToInt();

                        newTrans.ProcessID = location.ProcessId;
                        if (newBatch != null)
                        {
                            newTrans.BatchID_Base = newTrans.BatchNumberID;
                            newTrans.BatchNumberID = newBatch.BatchNumberID;
                        }

                        stock.Consumed = DateTime.Now;
                        if (creatingNewBatch)
                        {
                            stock.Deleted = DateTime.Now;
                        }

                        stock.Reference = null;
                        entities.StockTransactions.Add(newTrans);
                        entities.SaveChanges();
                        newTrans.Serial = newTrans.StockTransactionID;
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Determines if there's a lighter batch in a lower cooker rack.
        /// </summary>
        /// <param name="batchWgt">The move batch wgt.</param>
        /// <param name="location">The move location.</param>
        /// <returns>Data, as to whether there's a lighter batch wgt.</returns>
        public Tuple<string, decimal, int> IsLighterBatchInCooker(decimal batchWgt, Location location)
        {
            var moveToRack = location.Name.RemoveNonIntegers().ToInt();
            var warehouseIds = new List<int?>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var warehouseRacks =
                        entities.WarehouseLocations.Where(
                            x => x.WarehouseID == location.WarehouseId && x.Deleted == null);

                    foreach (var warehouseLocation in warehouseRacks)
                    {
                        var rack = warehouseLocation.Name.RemoveNonIntegers().ToInt();
                        if (rack > moveToRack)
                        {
                            warehouseIds.Add(warehouseLocation.WarehouseLocationID);
                        }
                    }

                    var stocks =
                       entities.StockTransactions.Where(
                           x => x.Consumed == null
                                && x.Deleted == null
                                && warehouseIds.Contains(x.LocationID)).GroupBy(x => x.LocationID);

                    foreach (var stock in stocks)
                    {
                        var orderId = stock.First().MasterTableID;
                        var localLocation = stock.First().LocationID;
                        var wgt = stock.Sum(x => x.TransactionWeight);
                        if (wgt < batchWgt)
                        {
                            var rackNo = warehouseRacks.First(x => x.WarehouseLocationID == localLocation).Name.RemoveNonIntegers().ToInt();
                            var batch =
                                entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                            if (batch != null)
                            {
                                return Tuple.Create(batch.Reference, Math.Round(wgt.ToDecimal(), 2), rackNo);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Determines if another batch is in the location.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <param name="location">The location to check.</param>
        /// <returns>Flag, as to whether another batch is in the location.</returns>
        public bool IsAnotherBatchInLocation(int batchId, Location location)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return
                        entities.StockTransactions.Any(
                            x => x.BatchNumberID != batchId && x.Consumed == null
                                                            && x.Deleted == null
                                                            && x.WarehouseID == location.WarehouseId &&
                                                            x.LocationID == location.WarehouseLocationId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the location of the batch.
        /// </summary>
        /// <returns>The batch location id.</returns>
        public Tuple<int, int?> GetBatchStockLocation(int prOrderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    //var transaction =
                    //    entities.StockTransactions.FirstOrDefault(
                    //        x => x.Deleted == null && x.Consumed == null && x.MasterTableID == prOrderId && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsDeliveryId);
                    var transaction =
                        entities.PROrders.FirstOrDefault(
                            x => x.Deleted == null && x.PROrderID == prOrderId);
                    if (transaction != null)
                    {
                        return Tuple.Create(transaction.WarehouseID.ToInt(), transaction.WarehouseLocationID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Moves stock down the slicing/joints production line.
        /// </summary>
        /// <param name="prOrder">The batch id.</param>
        /// <param name="inMasterId">The product to split off.</param>
        /// <param name="locationId">The new location.</param>
        /// <param name="wgt">The weight to split off.</param>
        /// <param name="qty">The qty to split off.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        public bool MoveBatchStockIntoProduction(PROrder prOrder, int inMasterId, int locationId, decimal wgt, int qty, int? processId)
        {
            var prOrderid = prOrder.PROrderID;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stock =
                        entities.StockTransactions.FirstOrDefault(
                            x => x.MasterTableID == prOrderid && x.INMasterID == inMasterId && x.Consumed == null
                                 && x.Deleted == null
                                 &&
                                 (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                 x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                  x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));

                    if (stock == null)
                    {
                        return false;
                    }

                    // consume original stock, and recreate minus what's been sent down the line
                    //var newTrans = this.CreateTransaction(stock);
                    //newTrans.Reference = null;
                    //newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                    //newTrans.UserMasterID = NouvemGlobal.UserId;
                    //newTrans.TransactionDate = DateTime.Now;
                    //newTrans.TransactionQTY = stock.TransactionQTY - qty;
                    //newTrans.TransactionWeight = stock.TransactionWeight - wgt;
                    //stock.Consumed = DateTime.Now;

                    // create and consume what's been sent down the line.
                    var lineTrans = this.CreateTransaction(stock);
                    lineTrans.Reference = null;
                    lineTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                    lineTrans.UserMasterID = NouvemGlobal.UserId;
                    lineTrans.TransactionDate = DateTime.Now;
                    lineTrans.TransactionWeight = wgt;
                    lineTrans.TransactionQTY = qty;
                    lineTrans.WarehouseID = locationId;
                    lineTrans.ProcessID = processId;
                    lineTrans.Consumed = DateTime.Now;

                    entities.StockTransactions.Add(lineTrans);
                    entities.SaveChanges();
                    lineTrans.Serial = lineTrans.StockTransactionID;

                    //entities.StockTransactions.Add(newTrans);
                    //entities.SaveChanges();
                    //newTrans.Serial = newTrans.StockTransactionID;

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="data">The stock move data.</param>
        /// <returns>A stock movement result.</returns>
        public bool UndoMoveStock(StockTakeData data)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localBarcode = data.Barcode.Trim().ToInt();
                    var transaction = entities.StockTransactions.FirstOrDefault(x => x.Serial == localBarcode);

                    if (transaction != null)
                    {
                        transaction.WarehouseID = data.WarehouseIDPrevious.ToInt();
                        transaction.LocationID = data.WarehouseLocationIDPrevious;

                        if (transaction.IsBox == true)
                        {
                            var boxItems = transaction.StockTransaction1;
                            foreach (var item in boxItems)
                            {
                                item.WarehouseID = data.WarehouseIDPrevious.ToInt(); ;
                                item.LocationID = data.WarehouseLocationIDPrevious;
                            }
                        }

                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Stock undo move complete");
                        return true;
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), "Stock transaction not found");
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Start of day to ensure leftover stock cannot be boxed.
        /// </summary>
        /// <returns>A flag, as to a successful procedure.</returns>
        public bool DisableAddingStockToBox()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.DisableAddingStockToBox(NouvemGlobal.DeviceId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Consume any dispatched production stock.
        /// </summary>
        private void ConsumeDispatchedStock()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_ConsumeIssuedStock();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}

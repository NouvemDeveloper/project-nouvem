﻿// -----------------------------------------------------------------------
// <copyright file="CountryRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer.Interface;

    public class CountryRepository : NouvemRepositoryBase, ICountryRepository
    {
        /// <summary>
        /// Retrieve all the countries.
        /// </summary>
        /// <returns>A collection of countries.</returns>
        public IList<Country> GetCountryMaster()
        {
            this.Log.LogDebug(this.GetType(), "GetCountryMaster(): Attempting to retrieve all the countries");
            var countries = new List<Country>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    countries = entities.Countries.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} countries retrieved", countries.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return countries;
        }

        /// <summary>
        /// Find the country code for a particular country code from an ear tag.
        /// </summary>
        /// <param name="code">The code of the country as determine by the ear tag.</param>
        /// <returns>The country relating to the code.</returns>
        public string FindCountryByCode(string code)
        {
            var country = string.Empty;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbCountryCode = entities.Countries.FirstOrDefault(x => x.PassportCode == code && !x.Deleted);
                    if (dbCountryCode != null)
                    {
                        return dbCountryCode.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return country;
        }

        /// <summary>
        /// Add or updates the countries list.
        /// </summary>
        /// <param name="countries">The countries to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateCountryMasters(IList<Country> countries)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateCountryMasters(): Attempting to update the country masters");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    countries.ToList().ForEach(x =>
                    {
                        if (x.CountryID == 0)
                        {
                            // new
                            var newCountry = new Country
                            {
                                Code = x.Code,
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.Countries.Add(newCountry);
                        }
                        else
                        {
                            // update
                            var dbCountry =
                                entities.Countries.FirstOrDefault(
                                    country => country.CountryID == x.CountryID && !country.Deleted);

                            if (dbCountry != null)
                            {
                                dbCountry.Code = x.Code;
                                dbCountry.Name = x.Name;
                                dbCountry.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Country masters successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }
    }
}

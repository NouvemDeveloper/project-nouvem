﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class InvoiceRepository : NouvemRepositoryBase, IInvoiceRepository
    {
        /// <summary>
        /// Adds a new invoice header object to the db.
        /// </summary>
        /// <param name="header">The invoice header to add.</param>
        /// <param name="partner">The business partner (if any).</param>
        /// <returns>The newly created invoice header id and number.</returns>
        public Tuple<int, int?> AddInvoiceHeader(InvoiceHeader header, BusinessPartner partner)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to add a new invoice header - " +
                                                           "Invoice Number:{0}, PO Number:{1}, CustomerId:{2}," +
                                                           "RouteID:{3}, HaulierID:{4}, Delivery Date:{5}," +
                                                           "NouStatusID:{6}, Creation:{7}, Creation UserID:{8}," +
                                                           "Cancellation:{9}, Cancellation UserID:{10}," +
                                                           "Vat Code id:{11}, Vat:{12}, SubincVat:{13}," +
                                                           "DiscountIncVat:{14}, GrandIncVat:{15}, Tendered:{16}, Change:{17}",
                header.InvoiceNumber, header.PONumber, header.BPMasterSnapshotID_Customer, header.RouteID,
                header.BPMasterSnapshotID_Haulier, header.DeliveryDate, header.NouDocStatusID, header.CreationDate,
                header.CreationUserID, header.CancelledDate, header.CancellationUserID, header.VATCodeID,
                header.VAT, header.SubTotalIncVAT, header.DiscountIncVAT, header.GrandTotalIncVAT,
                header.Tendered, header.Change));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (partner != null)
                    {
                        // it's on account, so we record the partners details.
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = partner.Details.BPMasterID,
                            BPMaster_Name = partner.Details.Name,
                            BPMaster_Code = partner.Details.Code,
                            BPMaster_UpLift = partner.Details.Uplift,
                            BPMaster_CreditLimit = partner.Details.CreditLimit,
                            BPType_Type = partner.PartnerType.Type,
                            BPGroup_BPGroupName = partner.PartnerGroup.BPGroupName,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        // update the invoice header with the customer snapshot id.
                        header.BPMasterSnapshotID_Customer = snapshot.BPMasterSnapshotID;
                    }

                    // get the last invoice number
                    var invoiceNo = entities.InvoiceHeaders.Max(x => x.InvoiceNumber);

                    // update by 1, and assign.
                    invoiceNo = invoiceNo ?? 0;
                    invoiceNo++;
                    header.InvoiceNumber = invoiceNo;
                   
                    entities.InvoiceHeaders.Add(header);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("New invoice header successfully created - id:{0}", header.InvoiceHeaderID));

                    return Tuple.Create(header.InvoiceHeaderID, invoiceNo);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Adds a collection of invoice details objects to the db.
        /// </summary>
        /// <param name="details">The invoice header to add.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful add or not.</returns>
        public string AddInvoiceItems(IDictionary<InvoiceDetail, ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse)
        {
            if (!details.Any())
            {
                return Constant.Error;
            }

            this.Log.LogDebug(this.GetType(),
                string.Format("Attempting to add {0} items to invoice header id {1}", details.Count,
                    details.First().Key.InvoiceDetailID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details.ToList().ForEach(x =>
                    {
                        this.Log.LogDebug(this.GetType(),
                            string.Format("Header id:{0}, Item master id:{1}, Qty:{2}, Price:{3}, Total:{4}",
                                x.Key.InvoiceHeaderID, x.Key.INMasterSnapshotID, x.Key.QuantityDelivered,
                                x.Key.UnitPrice, x.Key.TotalIncVAT));

                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = x.Value.Id,
                            Code = x.Value.Code,
                            Name = x.Value.Name,
                            MaxWeight = x.Value.MaxWeight,
                            MinWeight = x.Value.MinWeight,
                            NominalWeight = x.Value.NominalWeight,
                            TypicalPieces = x.Value.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var stockTransaction = new StockTransaction
                        {
                            INMasterID = x.Value.Id,
                            TransactionDate = DateTime.Now,
                            NouTransactionTypeID = eposTransactionType.NouTransactionTypeID,
                            WarehouseID = warehouse.WarehouseID,
                            TransactionQTY = x.Key.QuantityDelivered,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            UserMasterID = NouvemGlobal.UserId
                        };

                        entities.StockTransactions.Add(stockTransaction);

                        // update the invoice with the snapshot id.
                        x.Key.INMasterSnapshotID = snapshot.INMasterSnapshotID;
                    });

                    entities.InvoiceDetails.AddRange(details.Keys);

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} new invoice details successfully created - id:{0}", details.Count));

                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));

                // invalidate the invoice header
                this.DeleteInvoiceHeader(details.First().Key.InvoiceHeaderID);

                if (ex.Message.StartsWith(Constant.NoStockRecorded))
                {
                    return ex.Message;
                }
                
                return Message.DatabaseError;
            }
        }

        /// <summary>
        /// Deletes an invoice header.
        /// </summary>
        /// <param name="headerID">The header id.</param>
        public void DeleteInvoiceHeader(int headerID)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var header = entities.InvoiceHeaders.FirstOrDefault(x => x.InvoiceHeaderID == headerID);
                    if (header != null)
                    {
                        header.Deleted = DateTime.Now;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Adds a collection of goods received objects to the db.
        /// </summary>
        /// <param name="details">The intake data.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful add or not.</returns>
        public bool ProcessGoodsReceived(IList<ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse)
        {
            if (!details.Any())
            {
                return false;
            }

            this.Log.LogDebug(this.GetType(),
                string.Format("ProcessGoodsReceived(): Attempting to add {0} goods received items", details.Count));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details.ToList().ForEach(x =>
                    {
                        //this.Log.LogDebug(this.GetType(), string.Format("Header id:{0}, Qty Received:{1}, Opening Qty:{2}", x.Id, x.Quantity, openQty));

                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = x.Id,
                            Code = x.Code,
                            Name = x.Name,
                            MaxWeight = x.MaxWeight,
                            MinWeight = x.MinWeight,
                            NominalWeight = x.NominalWeight,
                            TypicalPieces = x.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var stockTransaction = new StockTransaction
                        {
                            INMasterID = x.Id,
                            TransactionDate = DateTime.Now,
                            NouTransactionTypeID = eposTransactionType.NouTransactionTypeID,
                            WarehouseID = warehouse.WarehouseID,
                            TransactionQTY = x.Quantity,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            UserMasterID = NouvemGlobal.UserId
                        };

                        entities.StockTransactions.Add(stockTransaction);
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} new goods receipt stock transactions successfully created - id:{0}",
                            details.Count));
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes stock from the db.
        /// </summary>
        /// <param name="details">The remove stock data.</param>
        /// <param name="eposTransactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag indicating a successful return or not.</returns>
        public bool ProcessGoodsReturned(IList<ProductData> details, NouTransactionType eposTransactionType, Warehouse warehouse)
        {
            if (!details.Any())
            {
                return false;
            }

            this.Log.LogDebug(this.GetType(),
                string.Format("ProcessGoodsReturned(): Attempting to return {0} items", details.Count));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var x in details)
                    {
                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = x.Id,
                            Code = x.Code,
                            Name = x.Name,
                            MaxWeight = x.MaxWeight,
                            MinWeight = x.MinWeight,
                            NominalWeight = x.NominalWeight,
                            TypicalPieces = x.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var stockTransaction = new StockTransaction
                        {
                            INMasterID = x.Id,
                            TransactionDate = DateTime.Now,
                            NouTransactionTypeID = eposTransactionType.NouTransactionTypeID,
                            WarehouseID = warehouse.WarehouseID,
                            TransactionQTY = x.Quantity,
                            DeviceMasterID = NouvemGlobal.DeviceId,
                            UserMasterID = NouvemGlobal.UserId
                        };

                        entities.StockTransactions.Add(stockTransaction);
                    }
                  
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} new stock return transactions successfully created - id:{0}",
                            details.Count));
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the epos sales for the selected date.
        /// </summary>
        /// <param name="saleDate">The date to retrieve sales for.</param>
        /// <param name="invoiceNo">The invoice number to search for.</param>
        /// <returns>The sales corresponding to the input date.</returns>
        public IList<EposSale> GetEposSales(DateTime? saleDate, int? invoiceNo)
        {
            this.Log.LogDebug(this.GetType(),
                string.Format("GetEposSales(): Attempting to retrieve the epos sales for {0}", saleDate));
            var eposSales = new List<EposSale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    
                    if (invoiceNo.IsNullOrZero())
                    {
                        // get the invoice header/details for the input date.
                        var saleData = (from invoiceDetail in entities.InvoiceDetails
                                        join invoiceHeader in entities.InvoiceHeaders
                                            on invoiceDetail.InvoiceHeaderID equals invoiceHeader.InvoiceHeaderID
                                        where
                                            invoiceHeader.Deleted == null &&
                                            DbFunctions.TruncateTime(invoiceHeader.CreationDate) == DbFunctions.TruncateTime(saleDate)
                                        select new { Header = invoiceHeader, Detail = invoiceDetail })
                                        .ToList();

                        foreach (var sale in saleData.GroupBy(x => x.Header))
                        {
                            var productDetails = new List<ProductData>();
                            foreach (var saleItem in sale)
                            {
                                productDetails.Add(new ProductData
                                {
                                    Id = saleItem.Detail.InvoiceDetailID,
                                    Quantity = saleItem.Detail.QuantityDelivered.ToInt(),
                                    Price = saleItem.Detail.UnitPrice.ToDecimal(),
                                    Description = saleItem.Detail.INMaster.Name,
                                    CreationDate = saleItem.Detail.CreationDate
                                });
                            }

                            eposSales.Add(new EposSale { InvoiceHeader = sale.Key, InvoiceDetails = productDetails });
                        }
                    }
                    else
                    {
                        // get the invoice header/details for the invoice no.
                        var saleData = (from invoiceDetail in entities.InvoiceDetails
                                        join invoiceHeader in entities.InvoiceHeaders
                                        on invoiceDetail.InvoiceHeaderID equals invoiceHeader.InvoiceHeaderID
                                        where
                                            invoiceHeader.Deleted == null &&
                                           invoiceHeader.InvoiceNumber == invoiceNo
                                        select new { Header = invoiceHeader, Detail = invoiceDetail })
                                        .ToList();

                        foreach (var sale in saleData.GroupBy(x => x.Header))
                        {
                            var productDetails = new List<ProductData>();
                            foreach (var saleItem in sale)
                            {
                                productDetails.Add(new ProductData
                                {
                                    Id = saleItem.Detail.InvoiceDetailID,
                                    Quantity = saleItem.Detail.QuantityDelivered.ToInt(),
                                    Price = saleItem.Detail.UnitPrice.ToDecimal(),
                                    Description = saleItem.Detail.INMaster.Name
                                });
                            }

                            eposSales.Add(new EposSale { InvoiceHeader = sale.Key, InvoiceDetails = productDetails });
                        }
                    }
                }
            }
            catch (Exception ex){
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return eposSales;
        }

        /// <summary>
        /// Gets the unpaid account epos sales for the selected date.
        /// </summary>
        /// <returns>The sales corresponding to the upaid accounts.</returns>
        public IList<EposSale> GetUnpaidEposSales()
        {
            this.Log.LogDebug(this.GetType(), "GetUnpaidEposSales(): Attempting to retrieve the all the unpaid epos sales for {0}");
            var eposSales = new List<EposSale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // get the invoice header/details of unpaid sales only.
                    var saleData = (from invoiceDetail in entities.InvoiceDetails
                                    join invoiceHeader in entities.InvoiceHeaders
                                    on invoiceDetail.InvoiceHeaderID equals invoiceHeader.InvoiceHeaderID
                                    where
                                        invoiceHeader.Deleted == null && invoiceHeader.NouPaymentType == null
                                    select new { Header = invoiceHeader, Detail = invoiceDetail })
                                    .ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} unpaid account sales retrieved", saleData.Count));

                    foreach (var sale in saleData.GroupBy(x => x.Header))
                    {
                        //var customerName = sale.Key.BPMasterSnapshotCustomer.BPMaster_Name;

                        var productDetails = new List<ProductData>();
                        foreach (var saleItem in sale)
                        {
                            productDetails.Add(new ProductData
                            {
                                Id = saleItem.Detail.InvoiceDetailID,
                                Quantity = saleItem.Detail.QuantityDelivered.ToInt(),
                                Price = saleItem.Detail.UnitPrice.ToDecimal(),
                                Description = saleItem.Detail.INMaster.Name
                            });
                        }

                        //eposSales.Add(new EposSale { InvoiceHeader = sale.Key, InvoiceDetails = productDetails, CustomerName = customerName });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return eposSales;
        }

        /// <summary>
        /// Marks the unpaid account sales as paid.
        /// </summary>
        /// <param name="sales">The sales collection.</param>
        /// <param name="paymentTypeId">The account payment type id.</param>
        /// <returns>A flag, indicating a successful processing of account sales to me marked paid.</returns>
        public bool HandleAccountSalesPaid(IList<EposSale> sales, int paymentTypeId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark {0} account sales as paid", sales.Count));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    sales.ToList().ForEach(sale =>
                    {
                        var invoice =
                            entities.InvoiceHeaders.FirstOrDefault(
                                x => x.InvoiceHeaderID == sale.InvoiceHeader.InvoiceHeaderID);

                        if (invoice != null)
                        {
                            invoice.NouPaymentTypeID = paymentTypeId;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Cancels the input epos sale, putting the acorresponding stock back into the system.
        /// </summary>
        /// <param name="sale">The sale to cancel.</param>
        /// <param name="transactionType">The transaction type.</param>
        /// <returns>A flag, indicating a successful sale cancellation or not..</returns>
        public bool CancelEposSale(EposSale sale, NouTransactionType transactionType)
        {
            this.Log.LogDebug(this.GetType(), string.Format("CancelEposSale(): Attempting to cancel the epos sale for invoice header with id:{0}", sale.InvoiceHeader.InvoiceHeaderID));
            var cancelled = false;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var saleToCancel =
                        entities.InvoiceHeaders.FirstOrDefault(
                            x => x.InvoiceHeaderID == sale.InvoiceHeader.InvoiceHeaderID && x.Deleted == null);

                    if (saleToCancel == null)
                    {
                        this.Log.LogDebug(this.GetType(),
                            string.Format("Invoiceheader with id:{0} cannot be found",
                                sale.InvoiceHeader.InvoiceHeaderID));
                        return false;
                    }

                    // mark the invoice header as void
                    saleToCancel.Deleted = DateTime.Now;

                    // mark the invoice items as void
                    var invoiceItems =
                        entities.InvoiceDetails.Where(x => x.InvoiceHeaderID == saleToCancel.InvoiceHeaderID).ToList();

                    invoiceItems.ForEach(x =>
                    {
                        x.Deleted = true;

                        // put the stock quantity back into the system
                        var localProduct = sale.InvoiceDetails.FirstOrDefault(item => item.Id == x.InvoiceDetailID);
                        if (localProduct == null)
                        {
                            // really shouldn't happen, but log it if it does
                            this.Log.LogError(this.GetType(),
                                string.Format("invoice detail with id:{0} cannot be found in EposSale.InvoiceDetails",
                                    x.InvoiceDetailID));
                            cancelled = false;
                        }
                        else
                        {
                            var lastRecord =
                                entities.StockTransactions.Where(
                                    transaction => transaction.INMasterID == x.INMasterID)
                                    .OrderByDescending(transaction => transaction.StockTransactionID)
                                    .FirstOrDefault();

                            if (lastRecord == null)
                            {
                                // again, really shouldn't happen, but log it if it does
                                this.Log.LogError(this.GetType(), string.Format(
                                        "no stock transaction records have been recorded for inmaster with id:{0}",
                                        x.INMasterID));
                                cancelled = false;
                            }
                            else
                            {
                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = x.INMasterID,
                                    Code = x.INMaster.Code,
                                    Name = x.INMaster.Name,
                                    MaxWeight = x.INMaster.MaxWeight,
                                    MinWeight = x.INMaster.MinWeight,
                                    NominalWeight = x.INMaster.NominalWeight,
                                    TypicalPieces = x.INMaster.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);
                                entities.SaveChanges();

                                var stockTransaction = new StockTransaction
                                {
                                    INMasterID = x.INMasterID,TransactionDate = DateTime.Now,
                                    NouTransactionTypeID = transactionType.NouTransactionTypeID,
                                    WarehouseID = NouvemGlobal.WarehouseLocations.First().WarehouseID,
                                    TransactionQTY = x.QuantityDelivered,
                                    DeviceMasterID = NouvemGlobal.DeviceId,
                                    UserMasterID = NouvemGlobal.UserId
                                };

                                entities.StockTransactions.Add(stockTransaction);
                                entities.SaveChanges();
                                cancelled = true;
                            }}
                    });
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                cancelled = false;
            }

            return cancelled;
        }

        /// <summary>
        /// Gets the current stock items.
        /// </summary>
        /// <returns>A collection of stock items.</returns>
        public IList<ProductData> GetEposStock()
        {
            this.Log.LogDebug(this.GetType(), "GetEposStock(): Attempting to retrieve the current epos stock data.");
            var productData = new List<ProductData>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var products = entities.INMasters.Where(x => x.Deleted == null);

                    foreach (var product in products)
                    {
                        decimal? qtyIn =
                            entities.StockTransactions.Where(
                                x =>
                                    x.INMasterID == product.INMasterID && x.Deleted == null &&
                                    (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId
                                    || x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockAdjustIdId)).Sum(prod => prod.TransactionQTY);

                        decimal? qtyOut =
                           entities.StockTransactions.Where(
                               x =>
                                   x.Deleted == null &&
                                   x.INMasterID == product.INMasterID &&
                                   x.NouTransactionTypeID == NouvemGlobal.TransactionTypeSaleEposId
                                   ).Sum(prod => prod.TransactionQTY);

                        var qty = (qtyIn.ToDecimal() - qtyOut.ToDecimal()).ToInt();

                        productData.Add(new ProductData
                        {
                            Id = product.INMasterID,
                            Code = product.Code,
                            Description = product.Name,
                            Location = NouvemGlobal.WarehouseLocations.FirstOrDefault(x => x.WarehouseID == NouvemGlobal.WarehouseIntakeId),
                            Quantity = qty,
                            StockQuantity = qty
                        });
                    }
                    
                    this.Log.LogDebug(this.GetType(), string.Format("{0} epos stock data retrieved", productData.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return productData;
        }

        /// <summary>
        /// Gets the system delivery methods.
        /// </summary>
        /// <returns>A collection of delivery methods.</returns>
        public IList<NouDeliveryMethod> GetDeliveryMethods()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve the delivery methods");
            var methods = new List<NouDeliveryMethod>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    methods = entities.NouDeliveryMethods.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} delivery methods retrieved", methods.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return methods;
        }

        /// <summary>
        /// Gets the system payment types.
        /// </summary>
        /// <returns>A collection of payment types.</returns>
        public IList<NouPaymentType> GetPaymentTypes()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve the payment types");
            var types = new List<NouPaymentType>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    types = entities.NouPaymentTypes.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} payment types retrieved", types.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return types;
        }
    }}




﻿// -----------------------------------------------------------------------
// <copyright file="BatchRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Linq;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Shared;

    public class BatchRepository : NouvemRepositoryBase, IBatchRepository
    {
        /// <summary>
        /// Generates a sequential batch number.
        /// </summary>
        /// <returns>A sequential batch number.</returns>
        public BatchNumber GenerateSequentialNumber(string reference = "", bool referenceOnly = false)
        {
            this.Log.LogDebug(this.GetType(), "GenerateSequentialNumber(): Attempting to generate a sequential batch number.");
            BatchNumber batchNo = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var lastBatch = entities.BatchNumbers.OrderByDescending(x => x.BatchNumberID).FirstOrDefault();
                    if (lastBatch == null)
                    {
                        batchNo = new BatchNumber {Number = "1", CreatedDate = DateTime.Now};
                    }
                    else
                    {
                        var number = reference != string.Empty
                            ? $"{reference} {lastBatch.BatchNumberID + 1}"
                            : $"{lastBatch.Number.ToInt() + 1}";
                        if (referenceOnly)
                        {
                            number = reference;
                        }

                        batchNo = new BatchNumber {Number = number, CreatedDate = DateTime.Now};
                    }

                    entities.BatchNumbers.Add(batchNo);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("Batch number:{0} generated", batchNo.Number));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return batchNo;
        }

        /// <summary>
        /// Generates a manually entered batch number.
        /// </summary>
        /// <param name="batchNo">The manually generated batch number.</param>
        /// <returns>The newly generated batch number.</returns>
        public BatchNumber GenerateManualEntryNumber(BatchNumber batchNo)
        {
            this.Log.LogDebug(this.GetType(), "GenerateManualEntryNumber(): Attempting to generate a manual entry batch number.");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.BatchNumbers.Add(batchNo);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("Batch number:{0} generated", batchNo.Number));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return batchNo;
        }

        /// <summary>
        /// Returns the last batch number associated with a batch product.
        /// </summary>
        /// <param name="inMasterId">The product id.</param>
        /// <returns>The last associated product batch.</returns>
        public BatchNumber GetLastProductBatch(int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var lastBatchTrans = entities.StockTransactions.OrderByDescending(x => x.StockTransactionID)
                        .FirstOrDefault(x => x.INMasterID == inMasterId && x.Deleted == null);
                    if (lastBatchTrans != null)
                    {
                        return lastBatchTrans.BatchNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the last recipe batch data for a recipe ingredient line.
        /// </summary>
        /// <param name="prOrderID">The recipe batch.</param>
        /// <param name="inMasterId">The ingredient id.</param>
        /// <returns>The last recipe batch data for a recipe ingredient line.</returns>
        public App_GetLastRecipeProductBatch_Result GetLastRecipeProductBatch(int prOrderID, int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetLastRecipeProductBatch(prOrderID, inMasterId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the used batch weight for a recipe batch line.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <param name="inMasterId">The line product id.</param>
        /// <returns>Used batch data for a recipe batch.</returns>
        public App_GetRecipeUsedBatchWeight_Result GetLastRecipeUsedBatchWeight(int batchId, int inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetRecipeUsedBatchWeight(inMasterId, batchId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets a batch number.
        /// </summary>
        /// <returns>A batch number.</returns>
        public BatchNumber GetBatchNumber(string batchNumber)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BatchNumbers.FirstOrDefault(x => x.Number == batchNumber);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets a batch number.
        /// </summary>
        /// <returns>A batch number.</returns>
        public BatchNumber GetBatchNumber(int batchNumberId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BatchNumbers.FirstOrDefault(x => x.BatchNumberID == batchNumberId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the batch options.
        /// </summary>
        /// <returns>The collection of batch options.</returns>
        public IList<BatchOption> GetBatchOptions()
        {
            var options = new List<BatchOption>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    options = entities.BatchOptions.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return options;
        }

        /// <summary>
        /// Gets the batchs.
        /// </summary>
        /// <returns>The collection of batch options.</returns>
        public IList<App_GetEditBatches_Result> GetBatches(int inmasterid)
        {
            var options = new List<BatchOption>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetEditBatches(inmasterid).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }
    }
}

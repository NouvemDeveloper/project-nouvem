﻿// -----------------------------------------------------------------------
// <copyright file="DocumentRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;

    public class DocumentRepository : NouvemRepositoryBase, IDocumentRepository
    {
        /// <summary>
        /// Returns the application document types.
        /// </summary>
        /// <returns>The application document types.</returns>
        public IList<DocumentNumberingType> GetDocumentNumberingTypes()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the document types");
            var types = new List<DocumentNumberingType>();

            try
            {
                //using (var entities = new NouvemEntities())
               // {
                    var entities = new NouvemEntities();
                    types = entities.DocumentNumberingTypes.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} document types successfully retrieved", types.Count));
               // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return types;
        }

        /// <summary>
        /// Returns the application document names.
        /// </summary>
        /// <returns>The application document names.</returns>
        public IList<NouDocumentName> GetNouDocumentNames()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the document names");
            var names = new List<NouDocumentName>();

            try
            {
                //using (var entities = new NouvemEntities())// {
                var entities = new NouvemEntities();
                    names = entities.NouDocumentNames.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} document names successfully retrieved", names.Count));
               // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return names;
        }

        /// <summary>
        /// Returns the application document numbers.
        /// </summary>
        /// <returns>The application document names.</returns>
        public IList<DocNumber> GetDocumentNumbers()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the document numbers");
            var docs = new List<DocNumber>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    docs = (from number in entities.DocumentNumberings
                        where number.Deleted == null
                        select new DocNumber
                        {
                            DocumentNumberingID = number.DocumentNumberingID,
                            NouDocumentNameID = number.NouDocumentNameID,
                            DocumentNumberingTypeID = number.DocumentNumberingTypeID,
                            FirstNumber = number.FirstNumber,
                            LastNumber = number.LastNumber,
                            NextNumber = number.NextNumber,
                            DocName = number.NouDocumentName,
                            DocType = number.DocumentNumberingType
                        }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} document numbers successfully retrieved", docs.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return docs;
        }

        /// <summary>
        /// Returns the application document numbers.
        /// </summary>
        /// <returns>The application document names.</returns>
        public IList<DocNumber> GetDocumentNumbers(string docType)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the document numbers");
            var docs = new List<DocNumber>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    docs = (from number in entities.DocumentNumberings
                            where number.Deleted == null
                            select new DocNumber
                            {
                                DocumentNumberingID = number.DocumentNumberingID,
                                NouDocumentNameID = number.NouDocumentNameID,
                                DocumentNumberingTypeID = number.DocumentNumberingTypeID,
                                FirstNumber = number.FirstNumber,
                                LastNumber = number.LastNumber,
                                NextNumber = number.NextNumber,
                                DocName = number.NouDocumentName,
                                DocType = number.DocumentNumberingType
                            }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} document numbers successfully retrieved", docs.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return docs;
        }

        /// <summary>
        /// Add or updates the document types list.
        /// </summary>
        /// <param name="types">The document types to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateDocumentTypes(IList<DocumentNumberingType> types)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateDocumentTypes(): Attempting to update the document types");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    types.ToList().ForEach(x =>
                    {
                        if (x.DocumentNumberingTypeID == 0)
                        {
                            // new
                            var newType = new DocumentNumberingType { Name = x.Name };
                            entities.DocumentNumberingTypes.Add(newType);
                        }
                        else
                        {
                            // update
                            var dbType =
                                entities.DocumentNumberingTypes.FirstOrDefault(
                                    type => type.DocumentNumberingTypeID == x.DocumentNumberingTypeID && type.Deleted == null);

                            if (dbType != null)
                            {
                                dbType.Name = x.Name;
                                dbType.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document types successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="numbers">The document numbers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateDocumentNumbers(IList<DocNumber> numbers)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateDocumentNumbers(): Attempting to update the document numbers");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    numbers.ToList().ForEach(x =>
                    {
                        if (x.DocumentNumberingID == 0)
                        {
                            // new
                            var newDoc = new DocumentNumbering
                            {
                                NouDocumentNameID = x.NouDocumentNameID,
                                DocumentNumberingTypeID = x.DocumentNumberingTypeID,
                                FirstNumber = x.FirstNumber,
                                LastNumber = x.LastNumber,
                                NextNumber = x.FirstNumber
                            };

                            entities.DocumentNumberings.Add(newDoc);
                        }
                        else
                        {
                            // update
                            var dbNumber =
                                entities.DocumentNumberings.FirstOrDefault(
                                    number => number.DocumentNumberingID == x.DocumentNumberingID && number.Deleted == null);

                            if (dbNumber != null)
                            {
                                if (x.NextNumber > dbNumber.LastNumber)
                                {
                                    // Last number reached, so reset back to first number.
                                    x.NextNumber = dbNumber.FirstNumber;
                                }

                                dbNumber.NouDocumentNameID = x.NouDocumentNameID;
                                dbNumber.DocumentNumberingTypeID = x.DocumentNumberingTypeID;
                                dbNumber.FirstNumber = x.FirstNumber;
                                dbNumber.LastNumber = x.LastNumber;
                                dbNumber.NextNumber = x.NextNumber;
                                //dbNumber.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document numbers successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        public DocNumber SetDocumentNumber(int docNumberId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("SetDocumentNumber(): Updating doc number ID:{0}", docNumberId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // update
                    var dbNumber =
                        entities.DocumentNumberings.FirstOrDefault(
                            number => number.DocumentNumberingID == docNumberId);

                    if (dbNumber != null)
                    {
                        var currentNo = dbNumber.NextNumber;
                        var nextNo = dbNumber.NextNumber + 1;
                        if (nextNo > dbNumber.LastNumber)
                        {
                            // Last number reached, so reset back to first number.
                            nextNo = dbNumber.FirstNumber;
                        }
                
                        dbNumber.NextNumber = nextNo;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Document number updated");

                        return new DocNumber
                        {
                            DocumentNumberingID = dbNumber.DocumentNumberingID,
                            NouDocumentNameID = dbNumber.NouDocumentNameID,
                            DocumentNumberingTypeID = dbNumber.DocumentNumberingTypeID,
                            FirstNumber = dbNumber.FirstNumber,
                            LastNumber = dbNumber.LastNumber,
                            CurrentNumber = currentNo,
                            NextNumber = dbNumber.NextNumber,
                            DocName = dbNumber.NouDocumentName,
                            DocType = dbNumber.DocumentNumberingType
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return null;
        }
    }
}

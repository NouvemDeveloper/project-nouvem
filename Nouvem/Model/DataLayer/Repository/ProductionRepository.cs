﻿// -----------------------------------------------------------------------
// <copyright file="ProductionRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Nouvem.BusinessLogic;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Global;
    using Nouvem.Shared;

    public class ProductionRepository : NouvemRepositoryBase, IProductionRepository
    {
        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionOrders(IList<ProductionData> currentData = null)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve the live production orders");
            var orders = new List<ProductionData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (currentData == null || !currentData.Any())
                    {
                        orders = (from order in entities.PROrders.OrderByDescending(x => x.PROrderID)
                                  where order.Deleted == null
                                  && order.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                  select new ProductionData
                                  {
                                      Order = order,
                                      BPCustomer = order.BPMaster,
                                      BPContact = order.BPContact,
                                      Specification = order.PRSpec,
                                      BatchNumber = order.BatchNumber,
                                      DocStatus = order.NouDocStatu,
                                      DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null
                                  }).
                        ToList();
                    }
                    else
                    {
                        List<int> orderIds = currentData.Select(x => x.Order.PROrderID).ToList();
                       var localOrders = (from order in entities.PROrders.OrderByDescending(x => x.PROrderID)
                                  where order.Deleted == null
                                  select new ProductionData
                                  {
                                      Order = order,
                                      BPCustomer = order.BPMaster,
                                      BPContact = order.BPContact,
                                      Specification = order.PRSpec,
                                      BatchNumber = order.BatchNumber,
                                      DocStatus = order.NouDocStatu,
                                      DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null
                                  }).Take(50).
                        ToList();

                        foreach (var localOrder in localOrders)
                        {
                            if (ApplicationSettings.ResetRecipeQtyOnOrdersScreen)
                            {
                                var order = currentData.FirstOrDefault(x =>
                                    x.Order.PROrderID == localOrder.Order.PROrderID);
                                if (order != null)
                                {
                                    order.Order.BatchWeight = localOrder.Order.BatchWeight;
                                }
                            }

                            if (localOrder.Order.Deleted != null
                                || localOrder.Order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                                || localOrder.Order.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                            {
                                var appOrder =
                                    currentData.FirstOrDefault(x => x.Order.PROrderID == localOrder.Order.PROrderID);
                                if (appOrder != null)
                                {
                                    currentData.Remove(appOrder);
                                }

                                continue;
                            }

                            if (orderIds.Contains(localOrder.Order.PROrderID))
                            {
                                continue;
                            }

                            orders.Add(localOrder);
                        }
                    }
                    
                    this.Log.LogDebug(this.GetType(), string.Format("{0} production orders returned", orders.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            if (currentData == null || !currentData.Any())
            {
                return orders;
            }

            var appendedOrders = currentData.Union(orders).ToList();
            return appendedOrders;
        }

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public ProductionBatchData GetProductionBatchData(int? inMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batchId = new System.Data.Entity.Core.Objects.ObjectParameter("BatchID", typeof(int));
                    var reference = new System.Data.Entity.Core.Objects.ObjectParameter("Reference", typeof(string));
                    var remaining = new System.Data.Entity.Core.Objects.ObjectParameter("Remaining", typeof(decimal));
                    var currentBatchId = new System.Data.Entity.Core.Objects.ObjectParameter("CurrentBatchNumberID", typeof(int));
                    var currentBatchNumber = new System.Data.Entity.Core.Objects.ObjectParameter("CurrentNumber", typeof(string));
                    var currentRemaining = new System.Data.Entity.Core.Objects.ObjectParameter("CurrentRemaining", typeof(decimal));
      
                    entities.App_GetBatchProductData(inMasterId, batchId,reference,remaining,currentBatchId,currentBatchNumber,currentRemaining);
                    return new ProductionBatchData
                    {
                        BatchId = batchId.Value != null ? batchId.Value.ToInt() : (int?)null,
                        CurrentBatchId = currentBatchId.Value != null ? currentBatchId.Value.ToInt() : (int?)null,
                        BatchNo = reference.Value != null ? reference.Value.ToString(): string.Empty,
                        CurrentBatchNo = currentBatchNumber.Value != null ? currentBatchNumber.Value.ToString() : string.Empty,
                        Remaining = remaining.Value != null ? remaining.Value.ToDecimal() : 0,
                        CurrentRemaining = currentRemaining.Value != null ? currentRemaining.Value.ToDecimal() : 0,
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the batch stock data for a product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public IList<App_ProductStockDataByBatch_Result> GetProductStockDataByBatch(int productId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_ProductStockDataByBatch(productId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionMultiBatchOrders(IList<ProductionData> currentData = null, int? inMasterId = null, bool productionLocationOnly = true)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve the live production orders");
            var orders = new List<ProductionData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localOrders = (from order in entities.App_GetBatches(inMasterId, productionLocationOnly)
                            select new
                            {
                                IntakeID = order.APGoodsReceiptID,
                                ID = order.APGoodsReceiptDetailID,
                                BatchID = order.BatchNumberID,
                                BPCustomer = order.Supplier,
                                ProductID = order.INMasterID,
                                INMaster = order.Product,
                                order.Reference,
                                order.Generic1,
                                order.Generic2,
                                order.Generic3,
                                order.Generic4,
                                order.Generic5,
                                order.Generic6,
                                order.Generic7,
                                order.Generic8,
                                order.WeightReceived,
                                order.QuantityReceived,
                                order.ProductionWeight,
                                order.ProductionQty,
                                order.RowNo,
                                order.RemainingQty,
                                order.RemainingWgt
                            }).
                        ToList();

                    foreach (var localOrder in localOrders.OrderByDescending(x => x.IntakeID))
                    {
                        orders.Add(new ProductionData
                        {
                            Order = new PROrder { PROrderID = localOrder.ID, Reference = localOrder.Reference, BatchNumberID = localOrder.BatchID, PROrderID_Base = localOrder.IntakeID},
                            PartnerName = localOrder.BPCustomer,
                            BatchNumber = new BatchNumber { BatchNumberID = localOrder.BatchID, Number = localOrder.Generic2 },
                            BatchProduct = localOrder.INMaster,
                            INMasterID = localOrder.ProductID,
                            Generic1 = localOrder.Generic1,
                            Generic2 = localOrder.Generic2,
                            Generic3 = localOrder.Generic3,
                            Generic4 = localOrder.Generic4,
                            Generic5 = localOrder.Generic5,
                            Generic6 = localOrder.Generic6,
                            Generic7 = localOrder.Generic7,
                            Generic8 = localOrder.Generic8,
                            BatchWeight = localOrder.ProductionWeight,
                            BatchQty = localOrder.ProductionQty,
                            IntakeWeight = localOrder.WeightReceived,
                            IntakeQty = localOrder.QuantityReceived,
                            RemainingQty = localOrder.RemainingQty.ToDecimal(),
                            RemainingWgt = localOrder.RemainingWgt.ToDecimal(),
                            IsIntakeBatch = true,
                            RowNo = localOrder.RowNo
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionBatchOrders(IList<int> orderStatuses, DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            var orders = new List<ProductionData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.PROrders.OrderByDescending(x => x.PROrderID)
                            where order.Deleted == null
                                  && order.CreationDate >= start && order.CreationDate <= end
                                  && orderStatuses.Contains(order.NouDocStatusID)
                            select new ProductionData
                            {
                                Order = order,
                                BPCustomer = order.BPMaster,
                                BPContact = order.BPContact,
                                Specification = order.PRSpec,
                                BatchNumber = order.BatchNumber,
                                DocStatus = order.NouDocStatu,
                                DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                                FatScores = order.PRInputFatScoreOrders.ToList(),
                                Confirmations = order.PRInputConfirmationOrders.ToList(),
                                CarcassTypes = order.PRInputCarcassTypeOrders.Where(x => x.Deleted == null).ToList(),
                                Outputs = order.PROrderOutputs.Where(x => x.Deleted == null).ToList(),
                                Plants = order.PRInputPlantOrders.Where(x => x.Deleted == null).ToList(),
                                Countries = order.PRInputCountryOrders.Where(x => x.Deleted == null).ToList()
                            }).
                        ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
           
            return orders;
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionBatchOrdersForDesktop(IList<int> orderStatuses, DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            var orders = new List<ProductionData>();
            var localOrders = new List<CollectionData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    localOrders = (from order in entities.PROrders.OrderByDescending(x => x.PROrderID)
                        where order.Deleted == null
                              && order.CreationDate >= start && order.CreationDate <= end
                              && orderStatuses.Contains(order.NouDocStatusID)
                        select new CollectionData
                        {
                            ID = order.PROrderID,
                            Data = order.Reference
                        }).ToList();
                }

                foreach (var collectionData in localOrders)
                {
                    orders.Add(new ProductionData {Order = new PROrder { PROrderID = collectionData.ID, Reference = collectionData.Data}});
                }

                localOrders = null;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Returns the production order data.
        /// </summary>
        /// <returns>The production order data.</returns>
        public IList<App_GetBatchData_Result> GetBatchData(int prOrderId, int transId, bool linesOnly)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetBatchData(prOrderId, transId, linesOnly).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Edits the batch row data.
        /// </summary>
        /// <returns></returns>
        public bool EditBatchData(BatchData batchData)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (batchData.AttributesOnly)
                    {
                        entities.App_EditBatchAttributes(
                            batchData.PROrderID, NouvemGlobal.UserId, NouvemGlobal.DeviceId,
                            batchData.Attribute1,
                            batchData.Attribute2,
                            batchData.Attribute3,
                            batchData.Attribute4,
                            batchData.Attribute5,
                            batchData.Attribute6,
                            batchData.Attribute7,
                            batchData.Attribute8,
                            batchData.Attribute9,
                            batchData.Attribute10,
                            batchData.Attribute11,
                            batchData.Attribute12,
                            batchData.Attribute13,
                            batchData.Attribute14,
                            batchData.Attribute15,
                            batchData.Attribute16,
                            batchData.Attribute17,
                            batchData.Attribute18,
                            batchData.Attribute19,
                            batchData.Attribute20,
                            batchData.Attribute21,
                            batchData.Attribute22,
                            batchData.Attribute23,
                            batchData.Attribute24,
                            batchData.Attribute25,
                            batchData.Attribute26,
                            batchData.Attribute27,
                            batchData.Attribute28,
                            batchData.Attribute29,
                            batchData.Attribute30);
                        return true;
                    }

                    entities.App_EditBatchData(batchData.StockTransactionID, batchData.Qty, batchData.Wgt, batchData.Price,
                        batchData.BatchNumberID, batchData.Delete,NouvemGlobal.UserId,NouvemGlobal.DeviceId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrderByNumber(int orderId)
        {
            ProductionData prOrder;
            var details = new List<PROrderDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.FirstOrDefault(x => x.Number == orderId);
                    if (order == null)
                    {
                        return null;
                    }

                    prOrder = new ProductionData
                    {
                        Order = order,
                        BPCustomer = order.BPMaster,
                        BPContact = order.BPContact,
                        Specification = order.PRSpec,
                        BatchNumber = order.BatchNumber,
                        DocStatus = order.NouDocStatu,
                        DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                        FatScores = order.PRInputFatScoreOrders.ToList(),
                        Confirmations = order.PRInputConfirmationOrders.ToList(),
                        CarcassTypes = order.PRInputCarcassTypeOrders.Where(x => x.Deleted == null).ToList(),
                        Outputs = order.PROrderOutputs.Where(x => x.Deleted == null).ToList(),
                        Plants = order.PRInputPlantOrders.Where(x => x.Deleted == null).ToList(),
                        Countries = order.PRInputCountryOrders.Where(x => x.Deleted == null).ToList()
                    };

                    prOrder.ProductionType = Constant.Standard;
                    if (order.NouPROrderTypeID.HasValue)
                    {
                        var localType =
                            entities.NouPROrderTypes.FirstOrDefault(x => x.NouPROrderTypeID == order.NouPROrderTypeID);
                        if (localType != null)
                        {
                            prOrder.ProductionType = localType.Description;
                        }
                    }

                    details = order.PROrderDetails.Where(x => x.Deleted == null).ToList();
                    prOrder.ProductionDetails = new List<SaleDetail>();
                    foreach (var prOrderDetail in details)
                    {
                        prOrder.ProductionDetails.Add(new SaleDetail
                        {
                            QuantityOrdered = prOrderDetail.QuantityOrdered.ToDecimal(),
                            WeightOrdered = prOrderDetail.WeightOrdered.ToDecimal(),
                            PriceListID = prOrderDetail.PriceListID.ToInt(),
                            UnitPrice = prOrderDetail.UnitPrice,
                            INMasterID = prOrderDetail.INMasterID,
                            IsHeaderProduct = prOrderDetail.IsHeaderProduct,
                            TransactionWgt = entities.StockTransactions.Where(x => x.MasterTableID == orderId
                                                                                    && x.INMasterID == prOrderDetail.INMasterID && x.Deleted == null).Sum(x => x.TransactionWeight),
                            TransactionQuantity = entities.StockTransactions.Where(x => x.MasterTableID == prOrder.Order.PROrderID
                                                                                    && x.INMasterID == prOrderDetail.INMasterID && x.Deleted == null).Sum(x => x.TransactionQTY),
                            WeightReceived = prOrderDetail.WeightReceived.ToDecimal(),
                            QuantityReceived = prOrderDetail.QuantityReceived.ToDecimal(),
                            BaseQuantity = prOrderDetail.TypicalBatchSize,
                            Mixes = prOrderDetail.Mixes.ToInt(),
                            MixCount = prOrderDetail.MixCount.ToInt(),
                            NouIssueMethodID = prOrderDetail.NouIssueMethodID,
                            NouIssueMethod = prOrderDetail.NouIssueMethod,
                            NouUOMID = prOrderDetail.NouUOMID,
                            NouStockMode = prOrderDetail.INMaster.NouStockMode,
                            PlusTolerance = prOrderDetail.PlusTolerance,
                            MinusTolerance = prOrderDetail.MinusTolerance,
                            PlusToleranceUOM = prOrderDetail.PlusToleranceUOM,
                            MinusToleranceUOM = prOrderDetail.MinusToleranceUOM,
                            TypicalBatchSize = prOrderDetail.TypicalBatchSize,
                            UseAllBatch = prOrderDetail.UseFullBatch,
                            IgnoreTolerances = prOrderDetail.IgnoreTolerances,
                            RecordLostWeight = prOrderDetail.RecordLostWeight,
                            AllowManualWeightEntry = prOrderDetail.AllowManualWeightEntry,
                            Notes = prOrderDetail.Notes,
                            OrderIndex = prOrderDetail.OrderIndex
                        });
                    }
                }

                if (prOrder.ProductionDetails.Any())
                {
                    prOrder.MixCount = prOrder.ProductionDetails.Max(x => x.MixCount);
                }

                if (prOrder.MixCount == 0)
                {
                    prOrder.MixCount = 1;
                }

                return prOrder;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrder(int orderId)
        {
            ProductionData prOrder;
            var details = new List<PROrderDetail>();
             try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (order == null)
                    {
                        return null;
                    }

                    prOrder = new ProductionData
                    {
                        Order = order,
                        BPCustomer = order.BPMaster,
                        BPContact = order.BPContact,
                        Specification = order.PRSpec,
                        BatchNumber = order.BatchNumber,
                        DocStatus = order.NouDocStatu,
                        DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                        FatScores = order.PRInputFatScoreOrders.ToList(),
                        Confirmations = order.PRInputConfirmationOrders.ToList(),
                        CarcassTypes = order.PRInputCarcassTypeOrders.Where(x => x.Deleted == null).ToList(),
                        Outputs = order.PROrderOutputs.Where(x => x.Deleted == null).ToList(),
                        Plants = order.PRInputPlantOrders.Where(x => x.Deleted == null).ToList(),
                        Countries = order.PRInputCountryOrders.Where(x => x.Deleted == null).ToList()
                    };

                    prOrder.ProductionType = Constant.Standard;
                    if (order.NouPROrderTypeID.HasValue)
                    {
                        var localType =
                            entities.NouPROrderTypes.FirstOrDefault(x => x.NouPROrderTypeID == order.NouPROrderTypeID);
                        if (localType != null)
                        {
                            prOrder.ProductionType = localType.Description;
                        }
                    }

                    details = order.PROrderDetails.Where(x => x.Deleted == null).ToList();
                    prOrder.ProductionDetails = new List<SaleDetail>();
                    foreach (var prOrderDetail in details)
                    {
                        prOrder.ProductionDetails.Add(new SaleDetail
                        {
                            QuantityOrdered = prOrderDetail.QuantityOrdered.ToDecimal(),
                            WeightOrdered = prOrderDetail.WeightOrdered.ToDecimal(),
                            PriceListID = prOrderDetail.PriceListID.ToInt(),
                            UnitPrice = prOrderDetail.UnitPrice,
                            INMasterID = prOrderDetail.INMasterID,
                            IsHeaderProduct = prOrderDetail.IsHeaderProduct,
                            TransactionWgt = entities.StockTransactions.Where(x => x.MasterTableID == orderId
                                                                                    && x.INMasterID == prOrderDetail.INMasterID && x.Deleted == null).Sum(x => x.TransactionWeight),
                            TransactionQuantity = entities.StockTransactions.Where(x => x.MasterTableID == prOrder.Order.PROrderID
                                                                                    && x.INMasterID == prOrderDetail.INMasterID && x.Deleted == null).Sum(x => x.TransactionQTY),
                            WeightReceived = prOrderDetail.WeightReceived.ToDecimal(),
                            QuantityReceived = prOrderDetail.QuantityReceived.ToDecimal(),
                            BaseQuantity = prOrderDetail.TypicalBatchSize,
                            Mixes = prOrderDetail.Mixes.ToInt(),
                            MixCount = prOrderDetail.MixCount.ToInt(),
                            NouIssueMethodID = prOrderDetail.NouIssueMethodID,
                            NouIssueMethod = prOrderDetail.NouIssueMethod,
                            NouUOMID = prOrderDetail.NouUOMID,
                            NouStockMode = prOrderDetail.INMaster.NouStockMode,
                            PlusTolerance = prOrderDetail.PlusTolerance,
                            MinusTolerance = prOrderDetail.MinusTolerance,
                            PlusToleranceUOM = prOrderDetail.PlusToleranceUOM,
                            MinusToleranceUOM = prOrderDetail.MinusToleranceUOM,
                            TypicalBatchSize = prOrderDetail.TypicalBatchSize,
                            UseAllBatch = prOrderDetail.UseFullBatch,
                            IgnoreTolerances = prOrderDetail.IgnoreTolerances,
                            RecordLostWeight = prOrderDetail.RecordLostWeight,
                            AllowManualWeightEntry = prOrderDetail.AllowManualWeightEntry,
                            Notes = prOrderDetail.Notes,
                            OrderIndex = prOrderDetail.OrderIndex
                        });
                    }
                }

                if (prOrder.ProductionDetails.Any())
                {
                    prOrder.MixCount = prOrder.ProductionDetails.Max(x => x.MixCount);
                }
         
                if (prOrder.MixCount == 0)
                {
                    prOrder.MixCount = 1;
                }

                return prOrder;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrderLastEdit()
        {
            ProductionData prOrder;
            var details = new List<PROrderDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.Where(x => x.DeviceID == NouvemGlobal.DeviceId).OrderByDescending(x => x.EditDate).FirstOrDefault();
                    if (order == null)
                    {
                        return null;
                    }

                    prOrder = new ProductionData
                    {
                        Order = order,
                        BPCustomer = order.BPMaster,
                        BPContact = order.BPContact,
                        Specification = order.PRSpec,
                        BatchNumber = order.BatchNumber,
                        DocStatus = order.NouDocStatu,
                        DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                        FatScores = order.PRInputFatScoreOrders.ToList(),
                        Confirmations = order.PRInputConfirmationOrders.ToList(),
                        CarcassTypes = order.PRInputCarcassTypeOrders.Where(x => x.Deleted == null).ToList(),
                        Outputs = order.PROrderOutputs.Where(x => x.Deleted == null).ToList(),
                        Plants = order.PRInputPlantOrders.Where(x => x.Deleted == null).ToList(),
                        Countries = order.PRInputCountryOrders.Where(x => x.Deleted == null).ToList()
                    };

                    prOrder.ProductionType = Constant.Standard;
                    if (order.NouPROrderTypeID.HasValue)
                    {
                        var localType =
                            entities.NouPROrderTypes.FirstOrDefault(x => x.NouPROrderTypeID == order.NouPROrderTypeID);
                        if (localType != null)
                        {
                            prOrder.ProductionType = localType.Description;
                        }
                    }

                    details = order.PROrderDetails.Where(x => x.Deleted == null).ToList();
                }

                prOrder.ProductionDetails = new List<SaleDetail>();
                foreach (var prOrderDetail in details)
                {
                    prOrder.ProductionDetails.Add(new SaleDetail
                    {
                        QuantityOrdered = prOrderDetail.QuantityOrdered.ToDecimal(),
                        WeightOrdered = prOrderDetail.WeightOrdered.ToDecimal(),
                        INMasterID = prOrderDetail.INMasterID,
                        PriceListID = prOrderDetail.PriceListID.ToInt(),
                        UnitPrice = prOrderDetail.UnitPrice,
                        WeightReceived = prOrderDetail.WeightReceived.ToDecimal(),
                        QuantityReceived = prOrderDetail.QuantityReceived.ToDecimal(),
                        IsHeaderProduct = prOrderDetail.IsHeaderProduct,
                        BaseQuantity = prOrderDetail.TypicalBatchSize,
                        Mixes = prOrderDetail.Mixes.ToInt(),
                        MixCount = prOrderDetail.MixCount.ToInt(),
                        NouIssueMethodID = prOrderDetail.NouIssueMethodID,
                        NouUOMID = prOrderDetail.NouUOMID,
                        PlusTolerance = prOrderDetail.PlusTolerance,
                        MinusTolerance = prOrderDetail.MinusTolerance,
                        PlusToleranceUOM = prOrderDetail.PlusToleranceUOM,
                        MinusToleranceUOM = prOrderDetail.MinusToleranceUOM,
                        Notes = prOrderDetail.Notes,
                        OrderIndex = prOrderDetail.OrderIndex
                    });
                }

                return prOrder;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        public ProductionData GetProductionBatchOrderFirstLast(bool first)
        {
            ProductionData prOrder;
            var details = new List<PROrderDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    PROrder order;
                    if (first)
                    {
                         order = entities.PROrders.FirstOrDefault();
                    }
                    else
                    {
                        order = entities.PROrders.OrderByDescending(x => x.PROrderID).FirstOrDefault();
                    }
                    
                    if (order == null)
                    {
                        return null;
                    }

                    prOrder = new ProductionData
                    {
                        Order = order,
                        BPCustomer = order.BPMaster,
                        BPContact = order.BPContact,
                        Specification = order.PRSpec,
                        BatchNumber = order.BatchNumber,
                        DocStatus = order.NouDocStatu,
                        DocumentNumberingType = order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                        FatScores = order.PRInputFatScoreOrders.ToList(),
                        Confirmations = order.PRInputConfirmationOrders.ToList(),
                        CarcassTypes = order.PRInputCarcassTypeOrders.Where(x => x.Deleted == null).ToList(),
                        Outputs = order.PROrderOutputs.Where(x => x.Deleted == null).ToList(),
                        Plants = order.PRInputPlantOrders.Where(x => x.Deleted == null).ToList(),
                        Countries = order.PRInputCountryOrders.Where(x => x.Deleted == null).ToList()
                    };

                    prOrder.ProductionType = Constant.Standard;
                    if (order.NouPROrderTypeID.HasValue)
                    {
                        var localType =
                            entities.NouPROrderTypes.FirstOrDefault(x => x.NouPROrderTypeID == order.NouPROrderTypeID);
                        if (localType != null)
                        {
                            prOrder.ProductionType = localType.Description;
                        }
                    }

                    details = order.PROrderDetails.Where(x => x.Deleted == null).ToList();
                }

                prOrder.ProductionDetails = new List<SaleDetail>();
                foreach (var prOrderDetail in details)
                {
                    prOrder.ProductionDetails.Add(new SaleDetail
                    {
                        QuantityOrdered = prOrderDetail.QuantityOrdered.ToDecimal(),
                        WeightOrdered = prOrderDetail.WeightOrdered.ToDecimal(),
                        PriceListID = prOrderDetail.PriceListID.ToInt(),
                        UnitPrice = prOrderDetail.UnitPrice,
                        INMasterID = prOrderDetail.INMasterID,
                        WeightReceived = prOrderDetail.WeightReceived.ToDecimal(),
                        IsHeaderProduct = prOrderDetail.IsHeaderProduct,
                        QuantityReceived = prOrderDetail.QuantityReceived.ToDecimal(),
                        BaseQuantity = prOrderDetail.TypicalBatchSize,
                        Mixes = prOrderDetail.Mixes.ToInt(),
                        MixCount = prOrderDetail.MixCount.ToInt(),
                        NouIssueMethodID = prOrderDetail.NouIssueMethodID,
                        NouUOMID = prOrderDetail.NouUOMID,
                        PlusTolerance = prOrderDetail.PlusTolerance,
                        MinusTolerance = prOrderDetail.MinusTolerance,
                        PlusToleranceUOM = prOrderDetail.PlusToleranceUOM,
                        MinusToleranceUOM = prOrderDetail.MinusToleranceUOM,
                        Notes = prOrderDetail.Notes,
                        OrderIndex = prOrderDetail.OrderIndex
                    });
                }

                return prOrder;
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetStockMovementTransactionData(int orderId, int locationId = 0, int? subLocationId = null)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetStockMovementTransactionData(): Attempting to get the stock transaction data for prorder id:{0}", orderId));
            var products = new List<SaleDetail>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batch = new BatchNumber();
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (prOrder != null)
                    {
                        batch = prOrder.BatchNumber;
                    }

                    if (locationId == 0)
                    {
                        var groupedProducts =
                      entities.StockTransactions
                          .Where(x => x.MasterTableID == prOrder.PROrderID && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsDeliveryId
                                                                 && x.Deleted == null && x.Consumed == null);


                        var originalProducts = groupedProducts.Where(x => x.Warehouse.StockLocation).GroupBy(x => new { x.INMasterID, x.SplitID });
                        var lineProducts = groupedProducts.Where(x => !x.Warehouse.StockLocation).GroupBy(x => new { x.INMasterID, x.SplitID });

                        foreach (var groupedProduct in originalProducts)
                        {
                            decimal lineWgt = 0;
                            decimal lineQty = 0;

                            var line = lineProducts.FirstOrDefault(x =>
                                x.Key.INMasterID == groupedProduct.Key.INMasterID &&
                                x.Key.SplitID == groupedProduct.Key.SplitID);
                            if (line != null && line.Any())
                            {
                                lineWgt = line.Sum(x => x.TransactionWeight).ToDecimal();
                                lineQty = line.Sum(x => x.TransactionQTY).ToDecimal();
                            }

                            products.Add(new SaleDetail
                            {
                                INMasterID = groupedProduct.Key.INMasterID,
                                SplitStockID = groupedProduct.Key.SplitID,
                                InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                                WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal() - lineWgt,
                                QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal() - lineQty,
                                Batch = batch,
                                PROrder = prOrder
                            });
                        }
                    }
                    else
                    {
                        var groupedProducts =
                      entities.StockTransactions
                          .Where(x => x.MasterTableID == prOrder.PROrderID && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsDeliveryId
                                                                 && x.Deleted == null && x.Consumed == null && x.WarehouseID == locationId && x.LocationID == subLocationId);


                        var originalProducts = groupedProducts.Where(x => x.Warehouse.StockLocation).GroupBy(x => new { x.INMasterID, x.SplitID });
                        var lineProducts = groupedProducts.Where(x => !x.Warehouse.StockLocation).GroupBy(x => new { x.INMasterID, x.SplitID });

                        foreach (var groupedProduct in originalProducts)
                        {
                            decimal lineWgt = 0;
                            decimal lineQty = 0;

                            var line = lineProducts.FirstOrDefault(x =>
                                x.Key.INMasterID == groupedProduct.Key.INMasterID &&
                                x.Key.SplitID == groupedProduct.Key.SplitID);
                            if (line != null && line.Any())
                            {
                                lineWgt = line.Sum(x => x.TransactionWeight).ToDecimal();
                                lineQty = line.Sum(x => x.TransactionQTY).ToDecimal();
                            }

                            products.Add(new SaleDetail
                            {
                                INMasterID = groupedProduct.Key.INMasterID,
                                SplitStockID = groupedProduct.Key.SplitID,
                                InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                                WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal() - lineWgt,
                                QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal() - lineQty,
                                Batch = batch,
                                PROrder = prOrder
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            return products;
        }

        /// <summary>
        /// Returns the production order for the associated batch.
        /// </summary>
        /// <returns>The associated production order.</returns>
        public ProductionData GetProductionOrderForBatch(int batchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to retrieve the productio order for batch id :{0}", batchId));
            ProductionData order = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.BatchNumberID == batchId && x.Deleted == null);
                    if (dbOrder != null)
                    {
                        order = new ProductionData
                        {
                             Specification = dbOrder.PRSpec,
                             GoodsInDetails = dbOrder.BatchNumber.APGoodsReceiptDetails.ToList()
                        };
                    }

                    this.Log.LogDebug(this.GetType(), "production order returned");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return order;
        }

        /// <summary>
        /// Cancel a batch.
        /// </summary>
        /// <returns>The batch id.</returns>
        public bool CancelBatch(int batchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to cancel batch for id :{0}", batchId));
          
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.BatchNumberID == batchId && x.Deleted == null);
                    if (dbOrder != null)
                    {
                        dbOrder.NouDocStatusID = NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                        entities.SaveChanges();
                        return true;
                    }

                    this.Log.LogError(this.GetType(), "production order not found");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Open a batch.
        /// </summary>
        /// <returns>The batch id.</returns>
        public bool OpenBatch(int batchId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to open batch for id :{0}", batchId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.BatchNumberID == batchId && x.Deleted == null);
                    if (dbOrder != null)
                    {
                        dbOrder.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
                        entities.SaveChanges();
                        return true;
                    }

                    this.Log.LogError(this.GetType(), "production order not found");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the production details.
        /// </summary>
        /// <param name="prorderid">The order id.</param>
        /// <param name="transactionTypeid">The into/out of value.</param>
        /// <returns>A production details.</returns>
        public IList<App_GetProductionDetails_Result> GetProductionDetails(int prorderid, int transactionTypeid)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetProductionDetails(prorderid, transactionTypeid).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public int? GetProductionOrderProduct(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.FirstOrDefault(x => x.PROrderID == id);
                    if (order != null)
                    {
                        return order.INMasterID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetBatchOrderDataPreDispatch(ProductionData order)
        {
            var products = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batch = new BatchNumber();
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == order.Order.PROrderID);
                    if (prOrder != null)
                    {
                        batch = prOrder.BatchNumber;
                        order.Order = prOrder;
                    }

                    var groupedProducts =
                        entities.StockTransactions
                            .Where(x => x.MasterTableID == order.Order.PROrderID && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsDeliveryId
                                                                   && x.Deleted == null && x.Consumed == null);

                    var originalProducts = groupedProducts.Where(x => x.Warehouse.StockLocation).GroupBy(x => new { x.INMasterID, x.SplitID });
                    var lineProducts = groupedProducts.Where(x => !x.Warehouse.StockLocation).GroupBy(x => new { x.INMasterID, x.SplitID });

                    foreach (var groupedProduct in originalProducts)
                    {
                        decimal lineWgt = 0;
                        decimal lineQty = 0;

                        var line = lineProducts.FirstOrDefault(x =>
                            x.Key.INMasterID == groupedProduct.Key.INMasterID &&
                            x.Key.SplitID == groupedProduct.Key.SplitID);
                        if (line != null && line.Any())
                        {
                            lineWgt = line.Sum(x => x.TransactionWeight).ToDecimal();
                            lineQty = line.Sum(x => x.TransactionQTY).ToDecimal();
                        }

                        products.Add(new SaleDetail
                        {
                            INMasterID = groupedProduct.Key.INMasterID,
                            SplitStockID = groupedProduct.Key.SplitID,
                            InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                            WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal() - lineWgt,
                            QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal() - lineQty,
                            Batch = batch
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            order.ProductionDetails = products;
            return products;
        }


        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetBatchOrderData(ProductionData order)
        {
            var products = new List<SaleDetail>();
        
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batch = new BatchNumber();
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == order.Order.PROrderID);
                    if (prOrder != null)
                    {
                        batch = prOrder.BatchNumber;
                        order.Order = prOrder;
                    }

                    var groupedProducts =
                        entities.StockTransactions
                            .Where(x => x.MasterTableID == order.Order.PROrderID && x.NouTransactionTypeID != NouvemGlobal.TransactionTypeGoodsDeliveryId
                                                                   && x.Deleted == null && x.Reference > 0).GroupBy(x => new { x.INMasterID, x.SplitID });

                    if (!groupedProducts.Any())
                    {
                        groupedProducts =
                            entities.StockTransactions
                                .Where(x => x.BatchNumberID == prOrder.BatchNumberID
                                            && (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
                                            && x.Deleted == null).GroupBy(x => new { x.INMasterID, x.SplitID });
                    }

                    foreach (var groupedProduct in groupedProducts)
                    {
                        products.Add(new SaleDetail
                        {
                            INMasterID = groupedProduct.Key.INMasterID,
                            SplitStockID = groupedProduct.Key.SplitID,
                            InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                            WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal(),
                            QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal(),
                            Batch = batch
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            order.ProductionDetails = products;
            return products;
        }
        /// <summary>
        /// Gets the pr order.
        /// </summary>
        /// <param name="id">The order id.</param>
        /// <returns>A pr order.</returns>
        public PROrder GetPROrder(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.PROrders.FirstOrDefault(x => x.PROrderID == id);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Completes a Recipe mix.
        /// </summary>
        /// <param name="order">The batch/Recipe data.</param>
        /// <returns>Flag, as to successful completion or not.</returns>
        public bool CompleteMix(ProductionData order)
        {
            var mix = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var RecipeProducts =
                        entities.PROrderDetails.Where(x => x.PROrderID == order.Order.PROrderID && x.Deleted == null);
                    foreach (var RecipeProduct in RecipeProducts)
                    {
                        if (RecipeProduct.IsHeaderProduct == true)
                        {
                            if (RecipeProduct.MixCount.IsNullOrZero())
                            {
                                RecipeProduct.MixCount = 2;
                                mix = 2;
                            }
                            else
                            {
                                RecipeProduct.MixCount++;
                                mix = RecipeProduct.MixCount.ToInt();
                            }
                        }
                    }

                    this.Log.LogInfo(this.GetType(), $"Complete Mix: PROrder ID:{order.OrderID}, Reference:{order.Order.Reference}, Mix:{mix - 1} ");
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public ProductionData GetPROrderByReference(string reference)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order =
                        entities.PROrders.FirstOrDefault(
                            x => x.Deleted == null && x.Reference.ToLower() == reference.ToLower());
                    if (order != null)
                    {
                        return new ProductionData
                        {
                            Order = order,
                            BPCustomer = order.BPMaster,
                            BPContact = order.BPContact,
                            Specification = order.PRSpec,
                            BatchNumber = order.BatchNumber,
                            DocStatus = order.NouDocStatu,
                            DocumentNumberingType =
                                order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                            FatScores = order.PRInputFatScoreOrders.ToList(),
                            Confirmations = order.PRInputConfirmationOrders.ToList(),
                            CarcassTypes = order.PRInputCarcassTypeOrders.ToList(),
                            Outputs = order.PROrderOutputs.ToList(),
                            INMasterID = order.INMasterID,
                            BatchWeight = order.BatchWeight,
                            BatchQuantity = order.BatchQty
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<ProductionData> GetProductionOrdersByLocation(int locationId)
        {
            var orders = new List<ProductionData>();

            try
            {
                IList<App_GetBatchesByLocation_Result> dbOrders;
                using (var entities = new NouvemEntities())
                {
                    dbOrders = entities.App_GetBatchesByLocation(locationId).ToList();
                }

                foreach (var order in dbOrders)
                {
                    orders.Add(new ProductionData
                    {
                        Order = new PROrder
                        {
                            PROrderID = order.PROrderID,
                            BatchNumberID = order.BatchNumberID,
                            DeviceID = order.DeviceID,
                            Reference = order.Reference,
                            Number = order.Number
                        },
                        INMasterID = order.INMasterID,
                        BatchWeight = Math.Round(order.TransactionWeight.ToDecimal(), 2),
                        BatchQuantity = order.TransactionQTY.ToInt(),
                        Warehouse = order.Warehouse,
                        SubLocation = order.SubLocation,
                        WarehouseId = order.WarehouseID.ToInt(),
                        SubLocationId = order.LocationID,
                        BatchProductId = order.INMasterID,
                        IsStockBatch = true
                    });
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Returns the production orders data for the reports.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<Sale> GetProductionOrdersForReportByOrderId(DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            this.Log.LogInfo(this.GetType(), "Attempting to retrieve the production orders data for the reports");
            var orders = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.PROrders
                            where order.Deleted == null && order.CreationDate >= start && order.CreationDate < end
                            orderby order.PROrderID descending
                            select new Sale
                            {
                                SaleID = order.PROrderID,
                                PROrderID = order.PROrderID,
                                BatchNo = order.BatchNumber.Number,
                                Number = order.Number,
                                OtherReference = order.Reference,
                                DocumentDate = order.ScheduledDate,
                                NouDocStatusID = order.NouDocStatusID
                            })
                        .ToList();

                    this.Log.LogInfo(this.GetType(), string.Format("{0} production orders returned", orders.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Returns the production orders data for the reports.
        /// </summary>
        /// <returns>The application production orders.</returns>
        public IList<Sale> GetProductionOrdersForReport(DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve the production orders data for the reports");
            var orders = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.PROrders
                            where order.Deleted == null && order.CreationDate >= start && order.CreationDate < end
                            orderby order.PROrderID descending
                            select new Sale
                            {
                                SaleID = order.BatchNumberID,
                                PROrderID = order.PROrderID,
                                BatchNo = order.BatchNumber.Number,
                                Number = order.Number,
                                CreationDate = order.CreationDate,
                                OtherReference = order.Reference,
                                DocumentDate = order.ScheduledDate,
                                NouDocStatusID = order.NouDocStatusID,
                                InternalBatchNo = order.BatchNumber.Number
                            })
                        .ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} production orders returned", orders.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Returns the specifications.
        /// </summary>
        /// <returns>The application specifications.</returns>
        public IList<ProductionData> GetSpecifications()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve the specifications");
            var specs = new List<ProductionData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    specs = (from prspec in entities.PRSpecs
                             where prspec.Deleted == null
                        select new ProductionData
                        {
                            Specification = prspec, 
                            FatScoresSpec = prspec.PRInputFatScores.Where(x => x.Deleted == null).ToList(),
                            ConfirmationsSpec = prspec.PRInputConfirmations.Where(x => x.Deleted == null).ToList(),
                            CarcassTypesSpec = prspec.PRInputCarcassTypes.Where(x => x.Deleted == null).ToList(),
                            OutputsSpec = prspec.PRSpecOutputs.Where(x => x.Deleted == null).ToList(),
                            CountriesSpec = prspec.PRInputCountries.Where(x => x.Deleted == null).ToList(),
                            PlantsSpec = prspec.PRInputPlants.Where(x => x.Deleted == null).ToList()
                        })
                        .ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} specifications returned", specs.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return specs;
        }

        /// <summary>
        /// Returns the production batch reference, or empty string if user generated.
        /// </summary>
        /// <returns>The production batch reference.</returns>
        public string GetProductionBatchReference(int deviceId,int userId,int processid,DateTime scheduledDate)
        {
            var specs = new List<ProductionData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var reference = new System.Data.Entity.Core.Objects.ObjectParameter("Reference", typeof(string));
                    entities.App_CreateProductionBatchReference(deviceId, userId, processid, scheduledDate,reference);
                    if (reference.Value != null)
                    {
                        return reference.Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return string.Empty;
        }

        /// <summary>
        /// Adds a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was added.</returns>
        public int AddProductionOrder(ProductionData order)
        {
            this.Log.LogDebug(this.GetType(), "AddProductionOrder(): Attempting to add a new production order");
            var prOrder = order.Order;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (prOrder.Number == 0)
                    {
                        var doc = entities.DocumentNumberings
                            .FirstOrDefault(x => x.NouDocumentName.DocumentName == Constant.PRProductionBatch && x.DocumentNumberingType.Name == Constant.Standard);

                        if (doc != null)
                        {
                            prOrder.Number = doc.NextNumber;
                            doc.NextNumber++;
                        }
                    }

                    entities.PROrders.Add(prOrder);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("PROrder created id:{0}", prOrder.PROrderID));
                    return prOrder.PROrderID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Adds a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was added.</returns>
        public int AddUniqueProductionOrder(ProductionData order)
        {
            this.Log.LogDebug(this.GetType(), "AddProductionOrder(): Attempting to add a new production order");
            var prOrder = order.Order;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alreadyExists =
                        entities.PROrders.Any(x => x.Reference == prOrder.Reference && x.Deleted == null);
                    if (alreadyExists)
                    {
                        return 0;
                    }

                    if (prOrder.Number == 0)
                    {
                        var doc = entities.DocumentNumberings
                            .FirstOrDefault(x => x.NouDocumentName.DocumentName == Constant.PRProductionBatch && x.DocumentNumberingType.Name == Constant.Standard);

                        if (doc != null)
                        {
                            prOrder.Number = doc.NextNumber;
                            doc.NextNumber++;
                        }
                    }

                    entities.PROrders.Add(prOrder);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("PROrder created id:{0}", prOrder.PROrderID));
                    return prOrder.PROrderID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        public bool UpdateProductionOrder(PROrder order)
        {
            this.Log.LogDebug(this.GetType(), "UpdateProductionOrder(): Attempting to update a production order");
            
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == order.PROrderID);
                    if (dbOrder != null)
                    {
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.NouDocStatusID = order.NouDocStatusID;
                        dbOrder.BPMasterID = order.BPMasterID;
                        dbOrder.BPContactID = order.BPContactID;
                        dbOrder.QALabelPrinted = order.QALabelPrinted;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("PROrder updated id:{0}", dbOrder.PROrderID));
                        return true;
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("PROrder id:{0} not found", order.PROrderID));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        public bool ChangeOrderStatus(int orderId, int nouDocStatusId)
        {
            this.Log.LogInfo(this.GetType(), $"Marking production order:{orderId} with noudocstatusid:{nouDocStatusId}");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (dbOrder != null)
                    {
                        dbOrder.EditDate = DateTime.Now;
                        //dbOrder.ReferenceID3 = 1;
                        dbOrder.NouDocStatusID = nouDocStatusId;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId.ToInt();
                        dbOrder.UserID = NouvemGlobal.UserId.ToInt();
                        entities.SaveChanges();
                        this.Log.LogInfo(this.GetType(), "Nou doc status updated");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        public bool ChangeRecipeBatchStatus(int orderId, int? status)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (dbOrder != null)
                    {
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.ReferenceID3 = status;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId.ToInt();
                        dbOrder.UserID = NouvemGlobal.UserId.ToInt();
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="inmasterid">The product to check.</param>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        public bool HasCarcassSplitStockBeenScanned(int inmasterid, string serial)
        {
            this.Log.LogDebug(this.GetType(), string.Format("HasCarcassSplitStockBeenScanned(): Checking is stock scanned:InmasterId:{0}, serial:{1}", inmasterid, serial));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var result =
                        entities.StockTransactions.Any(
                            x => x.LabelID == serial && x.INMasterID == inmasterid && x.MasterTableID > 0 && x.Deleted == null);

                    this.Log.LogDebug(this.GetType(), string.Format("Result:{0}", result));
                    return result;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Determines if a batch reference exists.
        /// </summary>
        /// <param name="reference">The reference to check.</param>
        /// <returns></returns>
        public DateTime? DoesPROrderByReferenceExist(string reference)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order =
                        entities.PROrders.FirstOrDefault(
                            x => x.Deleted == null && x.Reference.ToLower() == reference.ToLower() && x.NouDocStatusID == 1);
                    if (order != null)
                    {
                        return order.CreationDate;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        ///// <summary>
        ///// Determines if a carcass split scan already exists in the db.
        ///// </summary>
        ///// <param name="serial">The serial number to check.</param>
        ///// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        //public IList<int> GetScannedCarcassSplitStock(string serial, bool intake = false)
        //{
        //    this.Log.LogDebug(this.GetType(), string.Format("ScannedCarcassSplitStock(): Checking for scanned stock:Serial:{0}",  serial));
        //    var scannedProducts = new List<int>();
        //    IList<StockTransaction> result;
        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            if (!intake)
        //            {
        //                result =
        //                    entities.StockTransactions.Where(
        //                        x => x.LabelID == serial && x.Deleted == null).ToList();
        //            }
        //            else
        //            {
        //                if (serial.IsNumeric())
        //                {
        //                    var localSerial = serial.ToInt();
        //                    result =
        //                        entities.StockTransactions.Where(
        //                            x => x.Serial == localSerial && x.Deleted == null && x.WarehouseID != ApplicationSettings.ImportWarehouseId).ToList();
        //                }
        //                else
        //                {
        //                    result =
        //                        entities.StockTransactions.Where(
        //                            x => x.Comments == serial && x.Deleted == null && x.WarehouseID != ApplicationSettings.ImportWarehouseId).ToList();
        //                }
                        
        //            }

        //            foreach (var stockTransaction in result)
        //            {
        //                scannedProducts.Add(stockTransaction.INMasterID);
        //            }

        //            this.Log.LogDebug(this.GetType(), string.Format("Result:{0} nouvem records found matching serial {1}", result.Count(), serial));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return scannedProducts;
        //}

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        public IList<int> GetScannedCarcassSplitStock(string serial, bool imported = false, int transactionTypeId = 3)
        {
            this.Log.LogDebug(this.GetType(), string.Format("ScannedCarcassSplitStock(): Checking for scanned stock:Serial:{0}", serial));
            var scannedProducts = new List<int>();
            IList<StockTransaction> result;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (imported)
                    {
                        result =
                            entities.StockTransactions.Where(
                                x => x.Comments == serial && x.Deleted == null && x.NouTransactionTypeID == transactionTypeId).ToList();
                    }
                    else
                    {
                        var localSerial = serial.ToInt();
                        result =
                            entities.StockTransactions.Where(
                                x => x.Serial == localSerial && x.Deleted == null && x.NouTransactionTypeID == transactionTypeId).ToList();
                    }

                    foreach (var stockTransaction in result)
                    {
                        scannedProducts.Add(stockTransaction.INMasterID);
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("Result:{0} nouvem records found matching serial {1}", result.Count(), serial));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return scannedProducts;
        }

        /// <summary>
        /// Adds stock to a production.
        /// </summary>
        /// <returns>Flag, as to whether stock has been added to a production.</returns>
        public bool AddStockToProduction(SaleDetail product)
        {
            this.Log.LogDebug(this.GetType(), "AddStockToProduction()..adding stock");
            var stockTransaction = product.StockDetailToProcess; // transData.TransactionData.First().Transaction;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    StockTransaction transaction = null;

                    try
                    {
                        if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery
                       || ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection)
                        {
                            var attribute = this.CreateAttribute(stockTransaction);
                            entities.Attributes.Add(attribute);
                            entities.SaveChanges();

                            if (stockTransaction.BatchAttribute != null)
                            {
                                var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                batchAttribute.IsBatch = true;
                                var localBatch =
                                    entities.BatchNumbers.FirstOrDefault(
                                        x => x.BatchNumberID == stockTransaction.BatchNumberID);

                                if (localBatch != null)
                                {
                                    if (localBatch.AttributeID == null)
                                    {
                                        entities.Attributes.Add(batchAttribute);
                                        entities.SaveChanges();
                                        localBatch.AttributeID = batchAttribute.AttributeID;
                                    }
                                    else
                                    {
                                        #region batch attribute update

                                        var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                        if (localttribute != null)
                                        {
                                            this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                        }

                                        #endregion
                                    }
                                }
                            }

                            if (stockTransaction.IntakeID.HasValue)
                            {
                                var prOrder =
                                    entities.PROrders.FirstOrDefault(x => x.PROrderID == stockTransaction.MasterTableID);
                                if (prOrder != null)
                                {
                                    if (prOrder.PROrderID_Base == null)
                                    {
                                        prOrder.PROrderID_Base = stockTransaction.IntakeID;
                                    }
                                    else if (prOrder.PROrderID_Base != stockTransaction.IntakeID)
                                    {
                                        if (prOrder.BaseDocumentReferenceID2 == null)
                                        {
                                            prOrder.BaseDocumentReferenceID2 = stockTransaction.IntakeID;
                                        }
                                        else if (prOrder.BaseDocumentReferenceID2 != stockTransaction.IntakeID)
                                        {
                                            prOrder.BaseDocumentReferenceID3 = stockTransaction.IntakeID;
                                        }
                                    }

                                    if (prOrder.BPMasterID == null)
                                    {
                                        prOrder.BPMasterID = stockTransaction.BPMasterID;
                                    }
                                    else if (prOrder.BPMasterID != stockTransaction.BPMasterID)
                                    {
                                        if (prOrder.ReferenceID == null)
                                        {
                                            prOrder.ReferenceID = stockTransaction.BPMasterID;
                                        }
                                        else if (prOrder.ReferenceID != stockTransaction.BPMasterID)
                                        {
                                            prOrder.ReferenceID2 = stockTransaction.BPMasterID;
                                        }
                                    }
                                }
                            }

                            transaction = this.CreateTransaction(stockTransaction);
                            transaction.AttributeID = attribute.AttributeID;
                            transaction.InLocation = false;
                            entities.StockTransactions.Add(transaction);
                            entities.SaveChanges();
                            transaction.Serial = transaction.StockTransactionID;
                            entities.SaveChanges();
                            return true;
                        }

                        if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe && stockTransaction.RecordingWeight)
                        {
                            var line = entities.PROrderDetails.FirstOrDefault(x =>
                                x.INMasterID == stockTransaction.INMasterID &&
                                x.PROrderID == stockTransaction.MasterTableID && x.Deleted == null);

                            if (line != null)
                            {
                                line.WeightReceived =
                                    line.WeightReceived.ToDecimal() + stockTransaction.TransactionWeight;
                                line.QuantityReceived =
                                    line.QuantityReceived.ToDecimal() + stockTransaction.TransactionQty;
                            }

                            var attribute = this.CreateAttribute(stockTransaction);
                            entities.Attributes.Add(attribute);
                            entities.SaveChanges();

                            if (stockTransaction.NonStandardResponses != null && stockTransaction.NonStandardResponses.Any())
                            {
                                foreach (var nonStandardResponse in stockTransaction.NonStandardResponses)
                                {
                                    nonStandardResponse.AttributeID_Parent = attribute.AttributeID;
                                    entities.Attributes.Add(nonStandardResponse);
                                    entities.SaveChanges();
                                }
                            }

                            if (stockTransaction.BatchAttribute != null)
                            {
                                var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                batchAttribute.IsBatch = true;
                                var localBatch =
                                    entities.BatchNumbers.FirstOrDefault(
                                        x => x.BatchNumberID == stockTransaction.BatchNumberID);

                                if (localBatch != null)
                                {
                                    if (localBatch.AttributeID == null)
                                    {
                                        entities.Attributes.Add(batchAttribute);
                                        entities.SaveChanges();
                                        localBatch.AttributeID = batchAttribute.AttributeID;
                                    }
                                    else
                                    {
                                        #region batch attribute update

                                        var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                        if (localttribute != null)
                                        {
                                            this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                        }

                                        #endregion
                                    }
                                }
                            }

                            var details = entities.PROrderDetails.Where(x =>
                                x.PROrderID == stockTransaction.MasterTableID && x.Deleted == null);
                            foreach (var prOrderDetail in details)
                            {
                                prOrderDetail.MixCount = stockTransaction.MixCount;
                            }

                            transaction = this.CreateTransaction(stockTransaction);
                            transaction.AttributeID = attribute.AttributeID;
                            transaction.InLocation = false;

                            if (product.IsHeaderProduct.IsTrue())
                            {
                                transaction.WarehouseID = NouvemGlobal.OutOfProductionId;
                                transaction.InLocation = true;
                                transaction.Consumed = null;
                                transaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionReceiptId;
                            }

                            entities.StockTransactions.Add(transaction);
                            entities.SaveChanges();
                            transaction.Serial = transaction.StockTransactionID;
                            entities.SaveChanges();
                            product.StockDetailToProcess.StockTransactionID = transaction.StockTransactionID;
                            return true;
                        }

                        if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch && stockTransaction.RecordingWeight)
                        {
                            var attribute = this.CreateAttribute(stockTransaction);
                            entities.Attributes.Add(attribute);
                            entities.SaveChanges();

                            if (stockTransaction.NonStandardResponses != null && stockTransaction.NonStandardResponses.Any())
                            {
                                foreach (var nonStandardResponse in stockTransaction.NonStandardResponses)
                                {
                                    nonStandardResponse.AttributeID_Parent = attribute.AttributeID;
                                    entities.Attributes.Add(nonStandardResponse);
                                    entities.SaveChanges();
                                }
                            }

                            if (stockTransaction.BatchAttribute != null)
                            {
                                var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                batchAttribute.IsBatch = true;
                                var localBatch =
                                    entities.BatchNumbers.FirstOrDefault(
                                        x => x.BatchNumberID == stockTransaction.ProductionBatchID);

                                if (localBatch != null)
                                {
                                    if (localBatch.AttributeID == null)
                                    {
                                        entities.Attributes.Add(batchAttribute);
                                        entities.SaveChanges();
                                        localBatch.AttributeID = batchAttribute.AttributeID;
                                    }
                                    else
                                    {
                                        #region batch attribute update

                                        var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                        if (localttribute != null)
                                        {
                                            this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                        }

                                        #endregion
                                    }
                                }
                            }

                            transaction = this.CreateTransaction(stockTransaction);
                            transaction.AttributeID = attribute.AttributeID;
                            transaction.InLocation = false;
                            entities.StockTransactions.Add(transaction);
                            entities.SaveChanges();
                            transaction.Serial = transaction.StockTransactionID;
                            entities.SaveChanges();
                            product.StockDetailToProcess.StockTransactionID = transaction.StockTransactionID;
                            return true;
                        }

                        if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard
                            || ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe
                            || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionBatch
                            || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
                        {
                            var dbTransaction =
                            entities.StockTransactions.FirstOrDefault(
                                x => x.StockTransactionID == stockTransaction.Serial);

                            if (dbTransaction != null)
                            {
                                dbTransaction.InLocation = false;
                                dbTransaction.Consumed = DateTime.Now;
                                var newTransaction = this.CreateTransaction(dbTransaction);
                                newTransaction.MasterTableID = product.SaleID;
                                newTransaction.TransactionWeight = stockTransaction.TransactionWeight;
                                if (ApplicationSettings.IntoProductionMode != IntoProductionMode.IntoProductionBatch)
                                {
                                    newTransaction.BatchNumberID = product.StockDetailToProcess.BatchNumberID;
                                }

                                if (ApplicationSettings.IntoProductionMode ==
                                    IntoProductionMode.IntoProductionCarcassSplit)
                                {
                                    newTransaction.INMasterID = product.StockDetailToProcess.INMasterID;
                                    if (stockTransaction.RecordingWeight)
                                    {
                                        newTransaction.TransactionWeight =
                                            product.StockDetailToProcess.TransactionWeight;
                                    }
                                }
                             
                                newTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
                                newTransaction.TransactionDate = DateTime.Now;
                                newTransaction.WarehouseID = product.StockDetailToProcess.WarehouseID;
                                newTransaction.UserMasterID = NouvemGlobal.UserId;
                                newTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
                                newTransaction.InLocation = false;
                                newTransaction.Consumed = null;
                                newTransaction.SplitID = product.StockDetailToProcess.MixCount;
                                product.StockDetailToProcess.INMasterID = newTransaction.INMasterID;
                                entities.StockTransactions.Add(newTransaction);

                                if(ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                                {
                                    var line = entities.PROrderDetails.FirstOrDefault(x =>
                                        x.INMasterID == newTransaction.INMasterID &&
                                        x.PROrderID == newTransaction.MasterTableID && x.Deleted == null);

                                    if (line != null)
                                    {
                                        line.WeightReceived =
                                            line.WeightReceived.ToDecimal() + newTransaction.TransactionWeight;
                                        line.QuantityReceived =
                                            line.QuantityReceived.ToDecimal() + newTransaction.TransactionQTY;
                                    }
                                }

                                if (stockTransaction.BatchAttribute != null)
                                {
                                    var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                    batchAttribute.IsBatch = true;
                                    var localBatch =
                                        entities.BatchNumbers.FirstOrDefault(
                                            x => x.BatchNumberID == stockTransaction.BatchNumberID);

                                    if (localBatch != null)
                                    {
                                        if (localBatch.AttributeID == null)
                                        {
                                            entities.Attributes.Add(batchAttribute);
                                            entities.SaveChanges();
                                            localBatch.AttributeID = batchAttribute.AttributeID;
                                        }
                                        else
                                        {
                                            #region batch attribute update

                                            var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                            if (localttribute != null)
                                            {
                                                this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                            }

                                            #endregion
                                        }
                                    }
                                }

                                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Recipe)
                                {
                                    var details = entities.PROrderDetails.Where(x =>
                                        x.PROrderID == stockTransaction.MasterTableID && x.Deleted == null);
                                    foreach (var prOrderDetail in details)
                                    {
                                        prOrderDetail.MixCount = stockTransaction.MixCount;
                                    }
                                }

                                transaction = newTransaction;
                                entities.SaveChanges();
                                return true;
                            }
                        }
                        else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionCarcassSplit)
                        {
                            var dbTransaction =
                                entities.StockTransactions.FirstOrDefault(
                                    x => x.StockTransactionID == stockTransaction.Serial);

                            if (dbTransaction != null)
                            {
                                dbTransaction.InLocation = false;
                                dbTransaction.Consumed = DateTime.Now;
                                transaction = this.CreateTransaction(stockTransaction);
                                transaction.InLocation = false;
                                transaction.Consumed = null;
                                entities.StockTransactions.Add(transaction);
                                if (stockTransaction.BatchAttribute != null)
                                {
                                    var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                    batchAttribute.IsBatch = true;
                                    var localBatch =
                                        entities.BatchNumbers.FirstOrDefault(
                                            x => x.BatchNumberID == stockTransaction.BatchNumberID);

                                    if (localBatch != null)
                                    {
                                        if (localBatch.AttributeID == null)
                                        {
                                            entities.Attributes.Add(batchAttribute);
                                            entities.SaveChanges();
                                            localBatch.AttributeID = batchAttribute.AttributeID;
                                        }
                                        else
                                        {
                                            #region batch attribute update

                                            var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                            if (localttribute != null)
                                            {
                                                this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                            }

                                            #endregion
                                        }
                                    }
                                }

                                entities.SaveChanges();
                                return true;
                            }
                        }
                        else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Carcass
                            || ApplicationSettings.IntoProductionMode == IntoProductionMode.CarcassNoReweigh)
                        {
                            //entities.StockTransactions.Add(stockTransaction);
                            var serial = stockTransaction.LabelID.ToInt();
                            var carcassToConsume =
                                    entities.StockTransactions.FirstOrDefault(
                                        x => x.Serial == serial && x.NouTransactionTypeID == 2);
                            if (carcassToConsume != null)
                            {
                                carcassToConsume.Consumed = DateTime.Now;
                                stockTransaction.AttributeID = carcassToConsume.AttributeID;
                            }

                            entities.SaveChanges();
                            return true;
                        }
                        else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionSelectProduct
                                 || ApplicationSettings.IntoProductionMode == IntoProductionMode.SelectAllProduct)
                        {
                            var dbTransaction =
                                entities.StockTransactions.FirstOrDefault(
                                    x => x.StockTransactionID == stockTransaction.Serial);

                            if (dbTransaction != null)
                            {
                                dbTransaction.InLocation = false;
                                dbTransaction.Consumed = DateTime.Now;
                                transaction = this.CreateTransaction(stockTransaction);
                                transaction.InLocation = false;
                                transaction.Consumed = null;
                                transaction.AttributeID = dbTransaction.AttributeID;
                                if (product.AutoBoxing && !product.CompleteAutoBoxing)
                                {
                                    transaction.TransactionWeight = 0;
                                    transaction.TransactionQTY = 0;
                                }

                                IList<StockTransaction> products = null;
                                if (product.CompleteAutoBoxing)
                                {
                                    products = entities.StockTransactions.Where(x =>
                                        x.MasterTableID == transaction.MasterTableID
                                        && x.NouTransactionTypeID == 3
                                        && x.INMasterID == transaction.INMasterID
                                        && x.TransactionWeight == 0
                                        && x.StockTransactionID_Container == null
                                        && x.IsBox == null
                                        && x.Deleted == null).ToList();

                                    transaction.IsBox = true;
                                    transaction.TransactionQTY = products.Count;
                                    transaction.Serial = null;
                                    product.AutoBoxQty = products.Count;
                                }

                                entities.StockTransactions.Add(transaction);
                                if (stockTransaction.BatchAttribute != null)
                                {
                                    var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                    batchAttribute.IsBatch = true;
                                    var localBatch =
                                        entities.BatchNumbers.FirstOrDefault(
                                            x => x.BatchNumberID == stockTransaction.BatchNumberID);

                                    if (localBatch != null)
                                    {
                                        if (localBatch.AttributeID == null)
                                        {
                                            entities.Attributes.Add(batchAttribute);
                                            entities.SaveChanges();
                                            localBatch.AttributeID = batchAttribute.AttributeID;
                                        }
                                        else
                                        {
                                            #region batch attribute update

                                            var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                            if (localttribute != null)
                                            {
                                                this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                            }

                                            #endregion
                                        }
                                    }
                                }

                                entities.SaveChanges();
                                if (products != null && products.Any())
                                {
                                    foreach (var localTransaction in products)
                                    {
                                        localTransaction.StockTransactionID_Container = transaction.StockTransactionID;
                                        localTransaction.Consumed = DateTime.Now;
                                        entities.SaveChanges();
                                    }

                                    transaction.Serial = transaction.StockTransactionID;
                                    entities.SaveChanges();
                                }

                                return true;
                            }
                        }
                        else if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionConformance)
                        {
                            var dbTransaction =
                                entities.StockTransactions.FirstOrDefault(
                                    x => x.StockTransactionID == stockTransaction.Serial);

                            if (dbTransaction != null)
                            {
                                var attribute = this.CreateAttribute(stockTransaction);
                                entities.Attributes.Add(attribute);
                                entities.SaveChanges();

                                if (stockTransaction.NonStandardResponses != null && stockTransaction.NonStandardResponses.Any())
                                {
                                    foreach (var nonStandardResponse in stockTransaction.NonStandardResponses)
                                    {
                                        nonStandardResponse.AttributeID_Parent = attribute.AttributeID;
                                        entities.Attributes.Add(nonStandardResponse);
                                        entities.SaveChanges();
                                    }
                                }

                                dbTransaction.InLocation = false;
                                dbTransaction.Consumed = DateTime.Now;
                                transaction = this.CreateTransaction(stockTransaction);
                                transaction.InLocation = false;
                                transaction.Consumed = null;
                                transaction.AttributeID = attribute.AttributeID;
                                entities.StockTransactions.Add(transaction);
                                if (stockTransaction.BatchAttribute != null)
                                {
                                    var batchAttribute = this.CreateAttribute(stockTransaction.BatchAttribute);
                                    batchAttribute.IsBatch = true;
                                    var localBatch =
                                        entities.BatchNumbers.FirstOrDefault(
                                            x => x.BatchNumberID == stockTransaction.BatchNumberID);

                                    if (localBatch != null)
                                    {
                                        if (localBatch.AttributeID == null)
                                        {
                                            entities.Attributes.Add(batchAttribute);
                                            entities.SaveChanges();
                                            localBatch.AttributeID = batchAttribute.AttributeID;
                                        }
                                        else
                                        {
                                            #region batch attribute update

                                            var localttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                            if (localttribute != null)
                                            {
                                                this.UpdateAttribute(localttribute, stockTransaction.BatchAttribute);
                                            }

                                            #endregion
                                        }
                                    }
                                }

                                entities.SaveChanges();
                                return true;
                            }
                        }
                    }
                    finally
                    {
                        if (transaction != null)
                        {
                            entities.App_UpdateProductStockData(transaction.INMasterID,
                                transaction.BatchNumberID, transaction.TransactionQTY,
                                transaction.TransactionWeight, transaction.WarehouseID, transaction.NouTransactionTypeID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderTransactionData(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetProductionOrderTransactionData(): Attempting to get the stock transaction data for prorder id:{0}", orderId));
            var products = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batch = new BatchNumber();
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (prOrder != null)
                    {
                        batch = prOrder.BatchNumber;
                    }

                    var groupedProducts =
                               entities.StockTransactions
                               .Where(x => x.MasterTableID == orderId
                                   && (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId || x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId)
                                   && x.Deleted == null && x.Consumed == null).GroupBy(x => new { x.INMasterID, x.SplitID });

                    foreach (var groupedProduct in groupedProducts)
                    {
                        products.Add(new SaleDetail
                        {
                            INMasterID = groupedProduct.Key.INMasterID,
                            SplitStockID = groupedProduct.Key.SplitID,
                            InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                            WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal(),
                            QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal(),
                            Batch = batch,
                            NouStockMode = groupedProduct.First().INMaster.NouStockMode,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            return products;
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderTransactionDataNotSplit(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetProductionOrderTransactionData(): Attempting to get the stock transaction data for prorder id:{0}", orderId));
            var products = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batch = new BatchNumber();
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (prOrder != null)
                    {
                        batch = prOrder.BatchNumber;
                    }

                    var groupedProducts =
                               entities.StockTransactions
                               .Where(x => x.MasterTableID == orderId
                                   && (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId || x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId)
                                   && x.Deleted == null && x.Consumed == null).GroupBy(x => new { x.INMasterID });

                    foreach (var groupedProduct in groupedProducts)
                    {
                        products.Add(new SaleDetail
                        {
                            INMasterID = groupedProduct.Key.INMasterID,
                            InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                            WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal(),
                            QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal(),
                            Batch = batch
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            return products;
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public SaleDetail GetProductionOrderTransactionDataForProduct(int orderId, int inMasterId, int productionStage)
        {
            var product = new SaleDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var groupedProducts =
                               entities.StockTransactions
                               .Where(x => x.MasterTableID == orderId
                                   && (x.NouTransactionTypeID == productionStage) && x.INMasterID == inMasterId
                                   && x.Deleted == null && x.Consumed == null);

                    product.WeightIntoProduction = groupedProducts.Sum(x => x.TransactionWeight).ToDecimal();
                    product.QuantityIntoProduction = groupedProducts.Sum(x => x.TransactionQTY).ToDecimal();
                    product.InventoryItem = new InventoryItem {Master = groupedProducts.First().INMaster};
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return product;
        }

        /// <summary>
        /// Issues recipe backflush.
        /// </summary>
        /// <param name="orderId">The pr orderid.</param>
        /// <param name="batchId">the batch number id.</param>
        /// <param name="qty">The issue qty.</param>
        /// <param name="wgt">The issue wgt.</param>
        /// <param name="warehouseId">The warehouse id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <param name="processId">The process id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <param name="splitId">The mix count.</param>
        /// <returns>Flag , as to successful issue or not.</returns>
        public bool IssueBackflush(int orderId, int batchId, decimal qty, decimal wgt, int warehouseId, int inMasterId, int? processId,int userId, int deviceId,int splitId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_IssueBackFlush(orderId, batchId, qty,wgt,warehouseId,inMasterId,processId, userId, deviceId,splitId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the recipe order methods.
        /// </summary>
        /// <returns>The recipe order methods</returns>
        public IList<App_GetRecipeOrderMethods_Result> GetRecipeOrderMethods()
        {
            var product = new SaleDetail();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetRecipeOrderMethods().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderRecallData(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetProductionOrderRecallTransactionData(): Attempting to get the stock transaction data for prorder id:{0}", orderId));
            var products = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var groupedProducts =
                                entities.PROrderInputs
                                .Where(x => x.PROrderID == orderId && x.Deleted == null).GroupBy(x => x.INMasterID);

                    var transactions =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == orderId 
                                && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId && !x.Warehouse.StockLocation && x.Deleted == null);

                    foreach (var groupedProduct in groupedProducts)
                    {
                        products.Add(new SaleDetail
                        {
                            INMasterID = groupedProduct.Key,
                            InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                            WeightIntoProduction = transactions.Sum(x => x.TransactionWeight).ToDecimal(),
                            QuantityIntoProduction = transactions.Sum(x => x.TransactionQTY).ToDecimal(),
                            GoodsReceiptDatas = new List<GoodsReceiptData> 
                            { 
                                new GoodsReceiptData
                                {
                                        BatchNumber = groupedProduct.First().PROrder.BatchNumber ?? new BatchNumber(),
                                        BatchTraceabilities = groupedProduct.First().PROrder.BatchNumber != null
                                                              && groupedProduct.First().PROrder.BatchNumber.BatchTraceabilities != null 
                                                              ? groupedProduct.First().PROrder.BatchNumber.BatchTraceabilities.ToList() : new List<BatchTraceability>(),
                                        TransactionData = new List<StockTransactionData> { new StockTransactionData { Transaction = new StockTransaction(), TransactionTraceabilities = new List<TransactionTraceability>()}}
                                } 
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            return products;
        }

        /// <summary>
        /// Gets the spec and batch only data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetProductionOrderData(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetProductionOrderData(): Attempting to get the stock transaction data for prorder id:{0}", orderId));
            var products = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (order != null)
                    {
                        var groupedProducts = order.BatchNumber
                                .StockTransactions.Where(x => x.NouTransactionType.AddToStock == false && x.Deleted == null).GroupBy(x => x.INMasterID);

                        foreach (var groupedProduct in groupedProducts)
                        {
                            products.Add(new SaleDetail
                            {
                                INMasterID = groupedProduct.Key,
                                InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                                WeightIntoProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal(),
                                QuantityIntoProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal(),
                                GoodsReceiptDatas = new List<GoodsReceiptData> { new GoodsReceiptData
                                    {
                                        BatchNumber = groupedProduct.First().BatchNumber ?? new BatchNumber(),
                                        BatchTraceabilities = groupedProduct.First().BatchNumber != null
                                                              && groupedProduct.First().BatchNumber.BatchTraceabilities != null 
                                                              ? groupedProduct.First().BatchNumber.BatchTraceabilities.ToList() : new List<BatchTraceability>(),
                                        TransactionData = groupedProduct.First().BatchNumber != null 
                                        && groupedProduct.First().BatchNumber.StockTransactions != null ?
                                        new List<StockTransactionData>{new StockTransactionData
                                        {
                                            Transaction = groupedProduct.First(),
                                            TransactionTraceabilities = groupedProduct.First().TransactionTraceabilities.ToList()
                                        }} 
                                        : new List<StockTransactionData>()
                                    } 
                                    }
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            return products;
        }

        /// <summary>
        /// Gets the spec and batch only data for the out of production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        public IList<SaleDetail> GetOutOfProductionOrderData(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetOutOfProductionOrderData(): Attempting to get the stock transaction data for prorder id:{0}", orderId));
            var products = new List<SaleDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.FirstOrDefault(x => x.PROrderID == orderId);
                    if (order != null)
                    {
                        var groupedProducts = order.BatchNumber
                                .StockTransactions
                                .Where(x => x.NouTransactionType.AddToStock == true && x.StockTransactionID_Container == null && x.Deleted == null)
                                .GroupBy(x => new {x.INMasterID, x.BPMasterID});

                        foreach (var groupedProduct in groupedProducts)
                        {
                            var customer = string.Empty;
                            var localCustomer =
                                NouvemGlobal.CustomerPartners.FirstOrDefault(
                                    x => x.Details.BPMasterID == groupedProduct.Key.BPMasterID);
                            if (localCustomer != null)
                            {
                                customer = localCustomer.Details.Name;
                            }

                            products.Add(new SaleDetail
                            {
                                INMasterID = groupedProduct.Key.INMasterID,
                                CustomerName = customer,
                                InventoryItem = new InventoryItem { Master = groupedProduct.First().INMaster },
                                WeightOutOfProduction = groupedProduct.Sum(x => x.TransactionWeight).ToDecimal(),
                                QuantityOutOfProduction = groupedProduct.Sum(x => x.TransactionQTY).ToDecimal(),
                                GoodsReceiptDatas = new List<GoodsReceiptData> { new GoodsReceiptData
                                    {
                                        BatchNumber = groupedProduct.First().BatchNumber ?? new BatchNumber(),
                                        BatchTraceabilities = groupedProduct.First().BatchNumber != null
                                                              && groupedProduct.First().BatchNumber.BatchTraceabilities != null 
                                                              ? groupedProduct.First().BatchNumber.BatchTraceabilities.ToList() : new List<BatchTraceability>(),
                                        TransactionData = groupedProduct.First().BatchNumber != null 
                                        && groupedProduct.First().BatchNumber.StockTransactions != null ?
                                        new List<StockTransactionData>{new StockTransactionData
                                        {
                                            Transaction = groupedProduct.First(),
                                            TransactionTraceabilities = groupedProduct.First().TransactionTraceabilities.ToList()
                                        }} 
                                        : new List<StockTransactionData>()
                                    } 
                                    }
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} products retrieved", products.Count));
            return products.OrderBy(x => x.CustomerName).ToList();
        }

        /// <summary>
        /// Removes stock from a production order.
        /// </summary>
        /// <param name="product">The transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveStockFromOrder(SaleDetail product)
        {
            var trans = product.GoodsReceiptDatas.First().TransactionData.First().Transaction;
            this.Log.LogDebug(this.GetType(), string.Format("RemoveStockFromOrder(): Attempting to mark transaction item:{0} as deleted", trans.StockTransactionID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTransaction =
                        entities.StockTransactions.FirstOrDefault(x => 
                            x.Serial == trans.Serial && x.WarehouseID == NouvemGlobal.ProductionId
                            && x.Deleted == null);
                    if (dbTransaction != null)
                    {
                        dbTransaction.Deleted = DateTime.Now;

                        var transToUncomsume = entities.StockTransactions.FirstOrDefault(x =>
                            x.Serial == dbTransaction.Serial && x.Consumed != null);

                        if (transToUncomsume != null)
                        {
                            transToUncomsume.Consumed = null;
                            transToUncomsume.InLocation = true;

                            entities.SaveChanges();
                            this.Log.LogDebug(this.GetType(), string.Format("Transaction id:{0} deleted, Transaction id:{1} unconsumed", dbTransaction.StockTransactionID, transToUncomsume.StockTransactionID));
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), string.Format("Transaction to unconsume not found. Serial:{0}", dbTransaction.Serial));
                            return false;
                        }
                    }
                    else
                    {
                        this.Log.LogError(this.GetType(), string.Format("Transaction to delete id:{0} not found", trans.StockTransactionID));
                        return false;
                    }
               }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Removes stock from a production order.
        /// </summary>
        /// <param name="product">The transactions to update.</param>
        /// <param name="unconsumeStock">The flag as to whether stock is to be unconsumed.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveStockFromIntoProductionOrder(StockDetail trans)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbTransaction =
                        entities.StockTransactions.FirstOrDefault(x =>
                            x.Serial == trans.Serial && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId
                            && x.Deleted == null);
                    if (dbTransaction != null)
                    {
                        dbTransaction.Deleted = DateTime.Now;
                        var transToUncomsume = entities.StockTransactions.FirstOrDefault(x =>
                            x.Serial == dbTransaction.Serial && x.Consumed != null);

                        if (transToUncomsume != null)
                        {
                            transToUncomsume.Consumed = null;
                            transToUncomsume.InLocation = true;

                            entities.SaveChanges();
                            this.Log.LogDebug(this.GetType(), string.Format("Transaction id:{0} deleted, Transaction id:{1} unconsumed", dbTransaction.StockTransactionID, transToUncomsume.StockTransactionID));
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), string.Format("Transaction to unconsume not found. Serial:{0}", dbTransaction.Serial));
                            return false;
                        }
                    }
                    else
                    {
                        this.Log.LogError(this.GetType(), string.Format("Transaction to delete id:{0} not found", trans.StockTransactionID));
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the oldest kill date from a production batch.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <param name="batchId">The batch id.</param>
        /// <returns>The oldest kill date from a production batch, or null if none exists.</returns>
        public DateTime? GetProductionBatchOldestKillDate(int prOrderId, int batchId)
        {
            DateTime? oldestDate = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    // get the batch kill dates first.
                    var batchKilldates =
                   entities.BatchTraceabilities.Where(
                       x =>
                           x.BatchNumberID == batchId && x.DateMaster != null &&
                           x.DateMaster.DateCode.Equals("KillDate") && x.Deleted == null);

                    IList<string> localDates = new List<string>();
                    if (batchKilldates.Any())
                    {
                        localDates = batchKilldates.Select(x => x.Value).ToList();
                    }

                    // get the transactions that are part of the batch.
                    var batchTransactionIds =
                        entities.StockTransactions.Where(
                            x =>
                                x.MasterTableID == prOrderId && x.Deleted == null &&
                                x.WarehouseID == NouvemGlobal.ProductionId).Select(trans => trans.StockTransactionID);

                    // get the serial kill dates.
                    var killdates =
                        entities.TransactionTraceabilities.Where(
                            x =>
                                batchTransactionIds.Contains(x.StockTransactionID) && x.DateMaster != null &&
                                x.DateMaster.DateCode.Equals("KillDate") && x.Deleted == null);

                    if (killdates.Any())
                    {
                        var dates = killdates.Select(x => x.Value);
                        localDates = localDates.Concat(dates).ToList();
                    }

                    if (localDates.Any())
                    {
                        oldestDate = localDates.Select(x => x.ToDate()).OrderBy(x => x).First();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            if (!oldestDate.HasValue)
            {
                oldestDate = "01/01/01".ToDate();
            }

            return oldestDate;
        }

        /// <summary>
        /// Gets the oldest kill date from an external production batch.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <returns>The oldest kill date from a production batch, or null if none exists.</returns>
        public DateTime? GetProductionBatchOldestKillDateExternal(int prOrderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Getting oldest kill date for PROrderID:{0}", prOrderId));
            DateTime? oldestDate = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localDate = entities.GetOldestKillDateInBatch(prOrderId).FirstOrDefault();
                    if (localDate != null)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Oldest kill date found:{0} for PROrderID:{1}", oldestDate, prOrderId));
                        oldestDate = localDate;
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Oldest kill date NOT FOUND for PROrderID:{0}", prOrderId));
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            //if (!oldestDate.HasValue)
            //{
            //    oldestDate = "01/01/01".ToDate();
            //}

            return oldestDate;
        }

        /// <summary>
        /// Gets the external production batch data.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <returns>The production batch data.</returns>
        public Tuple<string,string> GetBatchTraceData(int prOrderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var stock =
                            entities.StockTransactions.FirstOrDefault(x =>
                                    x.MasterTableID == prOrderId && x.Deleted == null &&
                                    x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId);

                    if (stock != null)
                    {
                        if (stock.Attribute != null)
                        {
                            return Tuple.Create(stock.Attribute.CountryOfOrigin, stock.Attribute.RearedIn);
                        }

                        this.Log.LogDebug(this.GetType(), string.Format("Using stock id:{0}", stock.LabelID));
                        var localData = entities.GetBatchTraceData(stock.LabelID.ToInt()).FirstOrDefault();
                        if (localData != null)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("batch data retrieved. Born in:{0}, Reared In:{1}", localData.BornIn, localData.Reared_In));
                            return Tuple.Create(localData.BornIn, localData.Reared_In);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves the products allowed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>A collection of allowable products.</returns>
        public IList<SaleDetail> GetProductsAllowedOutOfProduction(int prSpecId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Retrieving the products allowed for prspec id:{0}", prSpecId));
            var products = new List<SaleDetail>();
            // TODO need to implement production order set ups. We'll be looking at PROutputOrder then.

            try
            {
                using (var entities = new NouvemEntities())
                {
                    products = (from output in entities.PRSpecOutputs
                        where output.PRSpecID == prSpecId && output.Deleted == null
                        orderby output.INMaster.Name
                        select new SaleDetail
                        {
                            INMasterID = output.INMasterID,
                            InventoryItem = new InventoryItem{ Master = output.INMaster }
                        }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} allowable products retrieved", products.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return products;
        }
    
        /// <summary>
        /// Retrieves the products allowed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>The products alloed out of production.</returns>
        public IList<InventoryItem> GetAllProductsAllowedOutOfProduction(int prOrderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAllProductsAllowedOutOfProduction(): Retrieving the products allowed for prspec id:{0}", prOrderId));
            var products = new List<InventoryItem>();

            // TODO need to implement production order set ups. We'll be looking at PROutputOrder then.
            try
            {
                using (var entities = new NouvemEntities())
                {
                    products = (from output in entities.PROrderOutputs
                                where output.PROrderID == prOrderId && output.Deleted == null
                                select new InventoryItem{  Master = output.INMaster, Group = output.INMaster.INGroup }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} allowable products retrieved", products.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return products;
        }

        /// <summary>
        /// Adds an out of production transaction data.
        /// </summary>
        /// <param name="transData">The transaction data.</param>
        /// <returns>Flag, indicating a successful addition or not.</returns>
        public int AddOutOfProductionTransaction(StockDetail transData)
        {
            var transactionId = 0;
            try
            {
                var transaction = this.CreateTransaction(transData);
                var attribute = this.CreateAttribute(transData);
                var localTrans = transaction;
                if (transaction.StockTransactionID > 0)
                {
                    localTrans = this.CreateDeepCopyTransaction(transaction);
                }

                using (var entities = new NouvemEntities())
                {
                    if (transData.BatchAttribute != null)
                    {
                        var batchAttribute = this.CreateAttribute(transData.BatchAttribute);
                        batchAttribute.IsBatch = true;
                        var localBatch =
                            entities.BatchNumbers.FirstOrDefault(
                                x => x.BatchNumberID == transData.BatchNumberID);

                        if (localBatch != null)
                        {
                            if (localBatch.AttributeID == null)
                            {
                                entities.Attributes.Add(batchAttribute);
                                entities.SaveChanges();
                                localBatch.AttributeID = batchAttribute.AttributeID;
                            }
                            else
                            {
                                #region batch attribute update

                                var localttribute =
                                    entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                if (localttribute != null)
                                {
                                    this.UpdateAttribute(localttribute, transData.BatchAttribute);
                                }

                                #endregion
                            }
                        }
                    }

                    entities.Attributes.Add(attribute);
                    entities.SaveChanges();
                    localTrans.AttributeID = attribute.AttributeID;
                    entities.StockTransactions.Add(localTrans);
                    entities.SaveChanges();
                    localTrans.Serial = localTrans.StockTransactionID;

                    if (ApplicationSettings.RecallCustomerFromProductAtOutOfProduction)
                    {
                        var productId = localTrans.INMasterID;
                        var product = entities.INMasters.FirstOrDefault(x => x.INMasterID == productId);
                        if (product != null)
                        {
                            product.BPMasterID_LabelPartner = localTrans.BPMasterID;
                            var localItem =
                                NouvemGlobal.InventoryItems.FirstOrDefault(
                                    x => x.Master.INMasterID == localTrans.INMasterID);
                            if (localItem != null)
                            {
                                localItem.Master.BPMasterID_LabelPartner = localTrans.BPMasterID;
                            }
                        }
                    }

                    entities.SaveChanges();
                    transactionId = localTrans.StockTransactionID;

                    entities.App_UpdateProductStockData(localTrans.INMasterID,
                        localTrans.BatchNumberID, localTrans.TransactionQTY,
                        localTrans.TransactionWeight, localTrans.WarehouseID, localTrans.NouTransactionTypeID);

                    return transactionId;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return transactionId;
        }

        /// <summary>
        /// Retrieves all the production batches.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllBatches(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            var orders = new List<Sale>();
            var localStatuses = orderStatuses;
            end = end.AddDays(1);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrders = (from order in entities.PROrders
                                    where
                                        order.CreationDate >= start && order.CreationDate <= end &&
                                        order.Deleted == null 
                                        && localStatuses.Contains(order.NouDocStatusID)
                                    select new { ID = order.PROrderID }).ToList();
                    foreach (var dbOrder in dbOrders)
                    {
                        entities.App_UpdateBatchWeight(dbOrder.ID);
                    }

                    orders = (from order in entities.PROrders
                              where
                              order.CreationDate >= start && order.CreationDate <= end &&
                              order.Deleted == null 
                              && localStatuses.Contains(order.NouDocStatusID)
                              select new Sale
                              {
                                  SaleID = order.PROrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.PROrderID_Base,
                                  BaseDocumentReferenceID2 = order.BaseDocumentReferenceID2,
                                  BaseDocumentReferenceID3 = order.BaseDocumentReferenceID3,
                                  BPMasterID = order.BPMasterID,
                                  Number = order.PROrderID,
                                  Reference = order.Reference,
                                  CreationDate = order.CreationDate,
                                  ScheduledDate = order.ScheduledDate,
                                  NouDocStatusID = order.NouDocStatusID,
                                  BatchNumberID = order.BatchNumberID,
                                  PopUpNote = order.Comments,
                                  EditDate = order.EditDate,
                                  UserIDModified = order.UserMasterID_ModifiedBy,
                                  ColdWeight = order.BatchWeight,
                                  Quantity = order.BatchQty,
                                  WarehouseID = order.WarehouseID,
                                  INMasterID = order.INMasterID,
                                  ReferenceID = order.ReferenceID,
                                  ReferenceID2 = order.ReferenceID2,
                                  CanEditBatches = ApplicationSettings.CanEditBatches,
                                  Notes = entities.Notes.Where(x => x.MasterTableID == order.PROrderID
                                                                    && x.NoteType == Constant.BatchEdit && x.Deleted == null).ToList()
                              }).ToList();
                }

                foreach (var order in orders)
                {
                    order.SetPopUpNotes();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} batches successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="batches">The workflow batches.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateBatches(HashSet<Sale> batches)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var i in batches)
                    {
                        if (i.SaleID == 0)
                        {
                            var docs = entities.DocumentNumberings
                                .Where(x => x.NouDocumentName.DocumentName == Constant.PRProductionBatch);
                            var selectedDocNumbering = docs.FirstOrDefault(x => x.DocumentNumberingType.Name == Constant.Standard);
                            if (selectedDocNumbering == null)
                            {
                                continue;
                            }

                            var batch = BatchManager.Instance.GenerateBatchNumber(false,i.Reference, referenceOnly:true);
                            var prOrder = new PROrder
                            {
                                Reference = i.Reference,
                                DocumentNumberingID = selectedDocNumbering.DocumentNumberingID,
                                Number = selectedDocNumbering.NextNumber,
                                NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                                Comments = i.PopUpNote,
                                CreationDate = DateTime.Now,
                                UserID = NouvemGlobal.UserId.ToInt(),
                                DeviceID = NouvemGlobal.DeviceId.ToInt(),
                                BatchNumberID = batch.BatchNumberID,
                                INMasterID = i.INMasterID,
                                PRSpecID = 1,
                                ScheduledDate = DateTime.Today,
                                PROrderID_Base = i.BaseDocumentReferenceID,
                                BPMasterID = i.BPMasterID,
                                ReferenceID = i.ReferenceID
                            };

                            selectedDocNumbering.NextNumber++;
                            entities.PROrders.Add(prOrder);
                            entities.SaveChanges();

                            if (i.PopUpNote != null)
                            {
                                if (ApplicationSettings.AccumulateNotesAtBatchEdit)
                                {
                                    entities.Notes.Add(new Note
                                    {
                                        Notes = i.PopUpNote,
                                        MasterTableID = prOrder.PROrderID,
                                        CreationDate = DateTime.Now,
                                        DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                        UserMasterID = NouvemGlobal.UserId.ToInt(),
                                        NoteType = Constant.BatchEdit
                                    });
                                }
                                else
                                {
                                    var existingNote = entities.Notes.FirstOrDefault(x =>
                                        x.MasterTableID == prOrder.PROrderID && x.NoteType == Constant.BatchEdit);
                                    if (existingNote != null)
                                    {
                                        existingNote.Notes = i.PopUpNote;
                                    }
                                    else
                                    {
                                        entities.Notes.Add(new Note
                                        {
                                            Notes = i.PopUpNote,
                                            MasterTableID = prOrder.PROrderID,
                                            CreationDate = DateTime.Now,
                                            DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                            UserMasterID = NouvemGlobal.UserId.ToInt(),
                                            NoteType = Constant.BatchEdit
                                        });
                                    }
                                }
                            }

                            entities.SaveChanges();

                            if (i.INMasterID.HasValue)
                            {
                                var newTrans = new StockTransaction();
                                newTrans.MasterTableID = prOrder.PROrderID;
                                newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
                                newTrans.TransactionWeight = i.ColdWeight;
                                newTrans.TransactionQTY = i.Quantity;
                                newTrans.INMasterID = i.INMasterID.ToInt();
                                newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                                newTrans.BPMasterID = i.BPMasterID;
                                newTrans.UserMasterID = NouvemGlobal.UserId;
                                newTrans.TransactionDate = DateTime.Now;
                                newTrans.BatchNumberID = prOrder.BatchNumberID;
                                newTrans.Reference = 1;
                                newTrans.ProcessID = i.ProcessID;
                                newTrans.Consumed = null;
                                newTrans.WarehouseID = ApplicationSettings.ManualBatchDefaultLocationID == 0 ? NouvemGlobal.ProductionId : ApplicationSettings.ManualBatchDefaultLocationID;
                                newTrans.Deleted = null;
                                entities.StockTransactions.Add(newTrans);
                                entities.SaveChanges();
                                newTrans.Serial = newTrans.StockTransactionID;
                                entities.SaveChanges();
                                DataManager.Instance.UpdateBatchWeight(prOrder.PROrderID);
                            }

                            if (i.ReferenceID.HasValue)
                            {
                                var newTrans = new StockTransaction();
                                newTrans.MasterTableID = prOrder.PROrderID;
                                newTrans.NouTransactionTypeID = NouvemGlobal.TransactionTypeProductionIssueId;
                                newTrans.TransactionWeight = 0;
                                newTrans.TransactionQTY = 0;
                                newTrans.BPMasterID = i.ReferenceID;
                                newTrans.INMasterID = i.INMasterID.ToInt();
                                newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                                newTrans.UserMasterID = NouvemGlobal.UserId;
                                newTrans.TransactionDate = DateTime.Now;
                                newTrans.BatchNumberID = prOrder.BatchNumberID;
                                newTrans.Reference = 1;
                                newTrans.ProcessID = i.ProcessID;
                                newTrans.Consumed = null;
                                newTrans.WarehouseID = ApplicationSettings.ManualBatchDefaultLocationID == 0 ? NouvemGlobal.ProductionId : ApplicationSettings.ManualBatchDefaultLocationID;
                                newTrans.Deleted = null;
                                entities.StockTransactions.Add(newTrans);
                                entities.SaveChanges();
                                newTrans.Serial = newTrans.StockTransactionID;
                                entities.SaveChanges();
                            }
                        }
                        else
                        {
                            var batch =
                          entities.PROrders.FirstOrDefault(x => x.PROrderID == i.SaleID);
                            if (batch != null)
                            {
                                if (i.INMasterID.HasValue && batch.INMasterID != i.INMasterID)
                                {
                                    var localTrans = entities.StockTransactions.FirstOrDefault(x =>
                                        x.MasterTableID == batch.PROrderID && x.Reference == 1 && x.Deleted == null
                                        &&
                                        (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                         x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                         x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));
                                    if (localTrans != null)
                                    {
                                        entities.App_UpdateTransaction(localTrans.StockTransactionID, i.INMasterID,
                                             localTrans.TransactionWeight, localTrans.TransactionQTY, localTrans.Price,false, NouvemGlobal.UserId, NouvemGlobal.DeviceId);
                                    }
                                }

                                batch.NouDocStatusID = i.NouDocStatusID.ToInt();
                                batch.Comments = i.PopUpNote;
                                batch.Reference = i.Reference;
                                batch.EditDate = DateTime.Now;
                                batch.INMasterID = i.INMasterID;
                                batch.ReferenceID = i.ReferenceID;
                                batch.ReferenceID2 = i.ReferenceID2;
                                batch.PROrderID_Base = i.BaseDocumentReferenceID;
                                batch.BaseDocumentReferenceID2 = i.BaseDocumentReferenceID2;
                                batch.BaseDocumentReferenceID3 = i.BaseDocumentReferenceID3;
                                batch.BPMasterID = i.BPMasterID;
                                batch.UserMasterID_ModifiedBy = NouvemGlobal.UserId;

                                if (i.PopUpNote != null)
                                {
                                    if (ApplicationSettings.AccumulateNotesAtBatchEdit)
                                    {
                                        entities.Notes.Add(new Note
                                        {
                                            Notes = i.PopUpNote,
                                            MasterTableID = i.SaleID,
                                            CreationDate = DateTime.Now,
                                            DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                            UserMasterID = NouvemGlobal.UserId.ToInt(),
                                            NoteType = Constant.BatchEdit
                                        });
                                    }
                                    else
                                    {
                                        var existingNote = entities.Notes.FirstOrDefault(x =>
                                            x.MasterTableID == batch.PROrderID && x.NoteType == Constant.BatchEdit);
                                        if (existingNote != null)
                                        {
                                            existingNote.Notes = i.PopUpNote;
                                        }
                                        else
                                        {
                                            entities.Notes.Add(new Note
                                            {
                                                Notes = i.PopUpNote,
                                                MasterTableID = batch.PROrderID,
                                                CreationDate = DateTime.Now,
                                                DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                                UserMasterID = NouvemGlobal.UserId.ToInt(),
                                                NoteType = Constant.BatchEdit
                                            });
                                        }
                                    }
                                }

                                entities.SaveChanges();

                                if (!i.EditingBatches)
                                {
                                    continue;
                                }

                                var stocks =
                                entities.StockTransactions.Where(
                                x => x.MasterTableID == batch.PROrderID
                                     && x.Deleted == null
                                     &&
                                     (x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionIssueId ||
                                     x.NouTransactionTypeID == NouvemGlobal.TransactionTypeStockMoveId ||
                                      x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId));

                                if (!stocks.Any())
                                {
                                    continue;
                                }

                                //foreach (var stock in stocks)
                                //{
                                //    stock.Consumed = DateTime.Now;
                                //    stock.Reference = null;
                                //}

                                var currentWgt = batch.BatchWeight;
                                var currentQty = batch.BatchQty;
                                var transWgt = i.ColdWeight.ToDecimal().Subtract(currentWgt.ToDecimal());
                                var transQty = i.Quantity.ToDecimal().Subtract(currentQty.ToDecimal());


                                var newTrans = this.CreateTransaction(stocks.First(x => x.Reference == 1));
                                newTrans.TransactionWeight = transWgt;
                                newTrans.TransactionQTY = transQty;
                                newTrans.INMasterID = i.INMasterID.ToInt();
                                newTrans.DeviceMasterID = NouvemGlobal.DeviceId;
                                newTrans.UserMasterID = NouvemGlobal.UserId;
                                newTrans.TransactionDate = DateTime.Now;
                                newTrans.Consumed = null;
                                newTrans.Deleted = null;
                                newTrans.Reference = 1;
                                if (!batch.WarehouseID.IsNullOrZero())
                                {
                                    newTrans.WarehouseID = batch.WarehouseID.ToInt();
                                }

                                entities.StockTransactions.Add(newTrans);
                                entities.SaveChanges();
                                newTrans.Serial = newTrans.StockTransactionID;
                                entities.SaveChanges();
                                DataManager.Instance.UpdateBatchWeight(batch.PROrderID);
                            }
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public ProductionData GetPROrderById(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order =
                        entities.PROrders.FirstOrDefault(
                            x => x.Deleted == null && x.PROrderID == id);
                    if (order != null)
                    {
                        var warehouseId = 0;
                        var warehouse =
                            entities.StockTransactions.FirstOrDefault(
                                x => x.MasterTableID == id && x.Consumed == null && x.Deleted == null);
                        if (warehouse != null)
                        {
                            warehouseId = warehouse.WarehouseID;
                        }

                        return new ProductionData
                        {
                            Order = order,
                            BPCustomer = order.BPMaster,
                            BPContact = order.BPContact,
                            Specification = order.PRSpec,
                            BatchNumber = order.BatchNumber,
                            DocStatus = order.NouDocStatu,
                            DocumentNumberingType =
                                order.DocumentNumbering != null ? order.DocumentNumbering.DocumentNumberingType : null,
                            FatScores = order.PRInputFatScoreOrders.ToList(),
                            Confirmations = order.PRInputConfirmationOrders.ToList(),
                            CarcassTypes = order.PRInputCarcassTypeOrders.ToList(),
                            Outputs = order.PROrderOutputs.ToList(),
                            WarehouseId = warehouseId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Consumes all items in a batch at the input module.
        /// </summary>
        /// <param name="prOrderId">The batch id.</param>
        /// <param name="transTypeId">The module type.</param>
        /// <returns>Flag, as to successfull consumption or not.</returns>
        public bool ConsumeProductionBatch(int prOrderId, int transTypeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var data =
                        entities.StockTransactions.Where(
                            x =>
                                x.MasterTableID == prOrderId && x.NouTransactionTypeID == transTypeId &&
                                x.Consumed == null);

                    foreach (var stockTransaction in data)
                    {
                        stockTransaction.Consumed = DateTime.Now;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets a prorder reference.
        /// </summary>
        /// <param name="prOrderId"></param>
        /// <returns></returns>
        public string GetPROrderReference(int prOrderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.PROrders.FirstOrDefault(x => x.PROrderID == prOrderId);
                    if (order != null)
                    {
                        return order.Reference;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return string.Empty;
        }

        /// <summary>
        /// Updates batch weight/qty.
        /// </summary>
        /// <param name="orderId">The prorder id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateBatchWeight(int orderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_UpdateBatchWeight(orderId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public Tuple<int, List<int?>> AddBoxTransaction(StockTransaction transaction, decimal boxTare = 0, int qtyPerBox = 1, bool enforceBoxQtyMatch = false)
        {
            var prOrderId = transaction.MasterTableID;
            var productId = transaction.INMasterID;
            this.Log.LogDebug(this.GetType(), string.Format("GetBoxTransactions(): Retrieving box transactions for order id:{0}, productId:{1}", prOrderId, productId));
            var errorList = new List<int?>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var products =
                        entities.StockTransactions.Where(
                            x => x.MasterTableID == prOrderId
                                && x.INMasterID == productId
                                && x.StockTransactionID_Container == null
                                && x.Consumed == null
                                && x.Deleted == null
                                && x.IsBox != true
                                && x.DeviceMasterID == NouvemGlobal.DeviceId
                                && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeProductionReceiptId
                                && x.CanAddToBox != false).ToList();

                    if (!products.Any())
                    {
                        throw new Exception("No Products");
                    }

                    if (enforceBoxQtyMatch)
                    {
                        if (products.Count > qtyPerBox)
                        {
                            foreach (var stockTransaction in products)
                            {
                                errorList.Add(stockTransaction.Serial);
                            }

                            return Tuple.Create(0, errorList);
                        }

                        if (products.Count < qtyPerBox)
                        {
                            foreach (var stockTransaction in products)
                            {
                                errorList.Add(stockTransaction.Serial);
                            }

                            return Tuple.Create(-1, errorList);
                        }
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("{0} transactions retrieved", products.Count));

                    var weight = products.Sum(x => x.TransactionWeight);
                    var qty = products.Sum(x => x.TransactionQTY);
                    var pieces = products.Sum(x => x.Pieces);
                    var piecesTare = products.Sum(x => x.Tare);

                    var tare = piecesTare + boxTare;
                    if (transaction.CanAddToBox == true)
                    {
                        tare = transaction.Tare.ToDecimal() + piecesTare;
                    }

                    transaction.TransactionWeight = weight;
                    transaction.TransactionQTY = qty;
                    transaction.Pieces = pieces;
                    transaction.Tare = tare;
                    transaction.InLocation = true;
                    transaction.AttributeID = products.Last().AttributeID;

                    if (ApplicationSettings.BoxQtyAlwaysOne)
                    {
                        transaction.TransactionQTY = 1;
                    }

                    entities.StockTransactions.Add(transaction);
                    entities.SaveChanges();

                    products.ForEach(x =>
                    {
                        x.StockTransactionID_Container = transaction.StockTransactionID;
                        x.Consumed = DateTime.Now;
                    });

                    transaction.Serial = transaction.StockTransactionID;

                    if (ApplicationSettings.RecallCustomerFromProductAtOutOfProduction)
                    {
                        var localProductId = transaction.INMasterID;
                        var product = entities.INMasters.FirstOrDefault(x => x.INMasterID == localProductId);
                        if (product != null)
                        {
                            product.BPMasterID_LabelPartner = transaction.BPMasterID;
                            var localItem =
                                NouvemGlobal.InventoryItems.FirstOrDefault(
                                    x => x.Master.INMasterID == transaction.INMasterID);
                            if (localItem != null)
                            {
                                localItem.Master.BPMasterID_LabelPartner = transaction.BPMasterID;
                            }
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), string.Format("Box transaction created id:{0}", transaction.StockTransactionID));
                    return Tuple.Create(transaction.StockTransactionID, errorList);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("No Products"))
                {
                    throw;
                }

                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, errorList);
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public bool UpdatePieceTransaction(int transactionId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.FirstOrDefault(x => x.StockTransactionID == transactionId);
                    if (trans != null)
                    {
                        trans.Comments = "Batch";
                        trans.Serial = trans.Serial.ToString().PadLeft(9, '5').ToInt();
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        public int? GetPROrderNumber(int transactionId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var trans = entities.StockTransactions.FirstOrDefault(x => x.Serial == transactionId);
                    if (trans != null)
                    {
                        return trans.MasterTableID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Updates an order output.
        /// </summary>
        /// <param name="spec">The outputs to add.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        public bool UpdateOrderSpecification(ProductionData spec)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (spec.Order.NouDocStatusID == 3)
                    {
                        var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == spec.Order.PROrderID);
                        if (prOrder != null)
                        {
                            prOrder.NouDocStatusID = 3;
                            entities.SaveChanges();
                            return true;
                        }

                        return false;
                    }

                    var orderOutputs =
                        entities.PROrderOutputs.Where(x => x.PROrderID == spec.Order.PROrderID && x.Deleted == null);
                    foreach (var prOrderOutput in orderOutputs)
                    {
                        prOrderOutput.Deleted = DateTime.Now;
                    }

                    var categories = entities.PRInputCarcassTypeOrders.Where(x => x.PROrderID == spec.Order.PROrderID && x.Deleted == null);
                    foreach (var category in categories)
                    {
                        category.Deleted = DateTime.Now;
                    }

                    foreach (var category in spec.CarcassTypes)
                    {
                        category.PROrderID = spec.Order.PROrderID;
                    }

                    var countries = entities.PRInputCountryOrders.Where(x => x.PROrderID == spec.Order.PROrderID && x.Deleted == null);
                    foreach (var country in countries)
                    {
                        country.Deleted = DateTime.Now;
                    }

                    foreach (var country in spec.Countries)
                    {
                        country.PROrderID = spec.Order.PROrderID;
                    }

                    foreach (var plant in spec.Plants)
                    {
                        plant.PROrderID = spec.Order.PROrderID;
                    }

                    var plants = entities.PRInputPlantOrders.Where(x => x.PROrderID == spec.Order.PROrderID && x.Deleted == null);
                    foreach (var plant in plants)
                    {
                        plant.Deleted = DateTime.Now;
                    }
               
                    entities.PRInputCarcassTypeOrders.AddRange(spec.CarcassTypes);
                    entities.PRInputCountryOrders.AddRange(spec.Countries);
                    entities.PRInputPlantOrders.AddRange(spec.Plants);               
                    entities.PROrderOutputs.AddRange(spec.Outputs.Where(x => x.INMasterID > 0));
                    entities.SaveChanges();
                }

                using (var entities = new NouvemEntities())
                {
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == spec.Order.PROrderID);
                    if (prOrder == null)
                    {
                        return false;
                    }

                    prOrder.EditDate = DateTime.Now;
                    prOrder.NouPROrderTypeID = spec.Order.NouPROrderTypeID;
                    prOrder.QA = spec.Order.QA;
                    prOrder.OverAge = spec.Order.OverAge;
                    prOrder.INMasterID = spec.Order.INMasterID;
                    prOrder.NouDocStatusID = spec.Order.NouDocStatusID;
                    prOrder.ActualBatchSize = spec.Order.ActualBatchSize;
                    prOrder.ScheduledDate = spec.Order.ScheduledDate;
                    prOrder.BatchWeight = spec.Order.BatchWeight;
                    var specId = spec.Specification?.PRSpecID;
                    if (specId > 0)
                    {
                        prOrder.PRSpecID = spec.Specification.PRSpecID;
                    }

                    var details = entities.PROrderDetails
                        .Where(x => x.PROrderID == prOrder.PROrderID && x.Deleted == null).ToList();
                    foreach (var prOrderDetail in details)
                    {
                        prOrderDetail.Deleted = DateTime.Now;
                    }

                    if (spec.ProductionDetails != null && spec.ProductionDetails.Any())
                    {
                        foreach (var detail in spec.ProductionDetails)
                        {
                            entities.PROrderDetails.Add(new PROrderDetail
                            {
                                PROrderID = prOrder.PROrderID,
                                QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                                WeightOrdered = detail.WeightOrdered.ToDecimal(),
                                PriceListID = detail.PriceListID,
                                UnitPrice = detail.UnitPrice,
                                INMasterID = detail.INMasterID,
                                VATCodeID = detail.VatCodeID,
                                VAT = detail.VAT,
                                TypicalBatchSize = detail.BaseQuantity,
                                IsHeaderProduct = detail.IsHeaderProduct,
                                Mixes = detail.Mixes,
                                NouIssueMethodID = detail.NouIssueMethodID,
                                NouUOMID = detail.NouUOMID,
                                PlusTolerance = detail.PlusTolerance,
                                MinusTolerance = detail.MinusTolerance,
                                PlusToleranceUOM = detail.PlusToleranceUOM,
                                MinusToleranceUOM = detail.MinusToleranceUOM,
                                UseFullBatch = detail.UseAllBatch,
                                IgnoreTolerances = detail.IgnoreTolerances,
                                RecordLostWeight = detail.RecordLostWeight,
                                AllowManualWeightEntry = detail.AllowManualWeightEntry,
                                Notes = detail.Notes,
                                OrderIndex = detail.OrderIndex
                            });

                            entities.SaveChanges();
                        }
                    }

                    entities.SaveChanges();
                    return true;
                } 
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates an order output.
        /// </summary>
        /// <param name="spec">The outputs to add.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        public bool UpdateRecipeOrderAmounts(ProductionData order)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var prOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == order.Order.PROrderID);
                    if (prOrder == null)
                    {
                        return false;
                    }

                    prOrder.EditDate = DateTime.Now;
                    prOrder.BatchWeight = order.BatchWeight;

                    foreach (var detail in order.ProductionDetails)
                    {
                        var localDetail = entities.PROrderDetails
                            .FirstOrDefault(x => x.PROrderID == order.Order.PROrderID && x.INMasterID == detail.INMasterID && x.Deleted == null);
                        if (localDetail != null)
                        {
                            localDetail.WeightOrdered = detail.WeightOrdered;
                            localDetail.QuantityOrdered = detail.QuantityOrdered;
                            localDetail.Mixes = detail.Mixes;
                            entities.SaveChanges();
                        }
                    }
                   
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a new production spec.
        /// </summary>
        /// <param name="spec">The spec to add.</param>
        /// <returns>A flag, as to successful add or not.</returns>
        public bool AddSpecification(ProductionData spec)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    spec.Specification.CreationDate = DateTime.Now;
                    entities.PRSpecs.Add(spec.Specification);
                    entities.SaveChanges();

                    foreach (var prSpecOutput in spec.OutputsSpec)
                    {
                        prSpecOutput.PRSpecID = spec.Specification.PRSpecID;
                    }

                    foreach (var category in spec.CarcassTypesSpec)
                    {
                        category.PRSpecID = spec.Specification.PRSpecID;
                    }

                    foreach (var country in spec.CountriesSpec)
                    {
                        country.PRSpecID = spec.Specification.PRSpecID;
                    }

                    foreach (var plant in spec.PlantsSpec)
                    {
                        plant.PRSpecID = spec.Specification.PRSpecID;
                    }

                    entities.PRSpecOutputs.AddRange(spec.OutputsSpec);
                    entities.PRInputCarcassTypes.AddRange(spec.CarcassTypesSpec);
                    entities.PRInputCountries.AddRange(spec.CountriesSpec);
                    entities.PRInputPlants.AddRange(spec.PlantsSpec);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// AUpdates production spec.
        /// </summary>
        /// <param name="spec">The spec to update.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        public bool UpdateSpecification(ProductionData spec)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbSpec = entities.PRSpecs.FirstOrDefault(x => x.PRSpecID == spec.Specification.PRSpecID);
                    if (dbSpec != null)
                    {
                        dbSpec.QA = spec.Specification.QA;
                    }

                    var outputs = entities.PRSpecOutputs.Where(x => x.PRSpecID == spec.Specification.PRSpecID && x.Deleted == null);
                    foreach (var prSpecOutput in outputs)
                    {
                        prSpecOutput.Deleted = DateTime.Now;
                    }

                    foreach (var prSpecOutput in spec.OutputsSpec)
                    {
                        prSpecOutput.PRSpecID = spec.Specification.PRSpecID;
                    }

                    var categories = entities.PRInputCarcassTypes.Where(x => x.PRSpecID == spec.Specification.PRSpecID && x.Deleted == null);
                    foreach (var category in categories)
                    {
                        category.Deleted = DateTime.Now;
                    }

                    foreach (var category in spec.CarcassTypesSpec)
                    {
                        category.PRSpecID = spec.Specification.PRSpecID;
                    }

                    var countries = entities.PRInputCountries.Where(x => x.PRSpecID == spec.Specification.PRSpecID && x.Deleted == null);
                    foreach (var country in countries)
                    {
                        country.Deleted = DateTime.Now;
                    }
                   
                    foreach (var country in spec.CountriesSpec)
                    {
                        country.PRSpecID = spec.Specification.PRSpecID;
                    }

                    foreach (var plant in spec.PlantsSpec)
                    {
                        plant.PRSpecID = spec.Specification.PRSpecID;
                    }

                    var plants = entities.PRInputPlants.Where(x => x.PRSpecID == spec.Specification.PRSpecID && x.Deleted == null);
                    foreach (var plant in plants)
                    {
                        plant.Deleted = DateTime.Now;
                    }

                    entities.PRSpecOutputs.AddRange(spec.OutputsSpec);
                    entities.PRInputCarcassTypes.AddRange(spec.CarcassTypesSpec);
                    entities.PRInputCountries.AddRange(spec.CountriesSpec);
                    entities.PRInputPlants.AddRange(spec.PlantsSpec);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Copies a production order.
        /// </summary>
        /// <param name="orderNo">The order no to copy from.</param>
        /// <param name="userId">The current user.</param>
        /// <param name="deviceId">The current device.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        public ProductStockData CopyProductionOrder(int orderNo, int userId, int deviceId, DateTime scheduledDate)
        {
            var data = new ProductStockData {OrderType = Strings.Production};
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var id = new System.Data.Entity.Core.Objects.ObjectParameter("ID", typeof(int));
                    var number = new System.Data.Entity.Core.Objects.ObjectParameter("Number", typeof(int));
                    var wgt = new System.Data.Entity.Core.Objects.ObjectParameter("Wgt", typeof(decimal));
                    var qty = new System.Data.Entity.Core.Objects.ObjectParameter("Qty", typeof(decimal));
                    entities.App_CopyProductionOrder(orderNo, userId, deviceId,scheduledDate, id,number,wgt,qty);
                    data.OrderId = id.Value != null ? id.Value.ToInt() : 0;
                    data.OrderNo = number.Value != null ? number.Value.ToInt() : 0;
                    data.AllocatedWgt = wgt.Value != null ? wgt.Value.ToDecimal() : 0;
                    data.AllocatedQty = qty.Value != null ? qty.Value.ToDecimal() : 0;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = ex.Message;
            }

            return data;
        }

        /// <summary>
        /// Checks if a product is a recipe product.
        /// </summary>
        /// <param name="inmasterid"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool IsProductRecipeProduct(int inmasterid, int orderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.PROrders.Any(x =>
                        x.INMasterID == inmasterid && x.PROrderID == orderId && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the application production types.
        /// </summary>
        /// <returns>The application production types.</returns>
        public IList<NouPROrderType> GetPrOrderTypes()
        {
            var types = new List<NouPROrderType>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    types = entities.NouPROrderTypes.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return types;
        }
        
        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public int AddRecipe(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var Recipe = new Recipe
                    {
                        INMasterID = sale.INMasterID.ToInt(),
                        Alias = sale.Alias,
                        PriceLIst_ID_Value = sale.PriceListIDValue,
                        PriceListID_Cost = sale.PriceListID,
                        TypicalBatchSize = sale.TypicalBatchSize,
                        TypicalMixQty = sale.TypicalMixQty,
                        MultipleReceipts = sale.MultipleReceipts,
                        ReworkNonAdjust = sale.ReworkStockNonAdjust,
                        MixNote = sale.TechnicalNotes,
                        AutoCompleteMix = sale.AutoCompleteMix,
                        DeviceID = NouvemGlobal.DeviceId,
                        UserMasterID = NouvemGlobal.UserId,
                        UserMasterID_Creation = NouvemGlobal.UserId,
                        NouOrderMethodID = sale.NouOrderMethodID,
                        EditDate = DateTime.Now,
                        CreationDate = DateTime.Now
                    };

                    entities.Recipes.Add(Recipe);
                    entities.SaveChanges();

                    var localRecipes = new List<RecipeDetail>();
                    foreach (var detail in sale.SaleDetails)
                    {
                        var orderDetail = new RecipeDetail
                        {
                            RecipeID = Recipe.RecipeID,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            UnitPrice = detail.UnitPrice,
                            VatCodeID = detail.VatCodeID,
                            INMasterID = detail.INMasterID,
                            NouIssueMethodID = detail.NouIssueMethodID,
                            PriceListID_Cost = detail.PriceListID,
                            NouUOMID = detail.NouUOMID,
                            PlusTolerance = detail.PlusTolerance,
                            PlusToleranceUOM = detail.PlusToleranceUOM,
                            MinusTolerance = detail.MinusTolerance,
                            MinusToleranceUOM = detail.MinusToleranceUOM,
                            UseFullBatch = detail.UseAllBatch,
                            IgnoreTolerances = detail.IgnoreTolerances,
                            RecordLostWeight = detail.RecordLostWeight,
                            AllowManualWeightEntry = detail.AllowManualWeightEntry,
                            Notes = detail.Notes,
                            OrderIndex = detail.OrderIndex
                        };

                        localRecipes.Add(orderDetail);
                    }

                    entities.RecipeDetails.AddRange(localRecipes);
                    entities.SaveChanges();
                
                    return Recipe.RecipeID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public bool UpdateRecipe(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var Recipe = entities.Recipes.FirstOrDefault(x => x.RecipeID == sale.SaleID);
                    if (Recipe == null)
                    {
                        throw new Exception(string.Format("Recipe id:{0} not found", sale.SaleID));
                    }

                    Recipe.INMasterID = sale.INMasterID.ToInt();
                    Recipe.Alias = sale.Alias;
                    Recipe.PriceLIst_ID_Value = sale.PriceListIDValue;
                    Recipe.PriceListID_Cost = sale.PriceListID;
                    Recipe.TypicalBatchSize = sale.TypicalBatchSize;
                    Recipe.TypicalMixQty = sale.TypicalMixQty;
                    Recipe.MixNote = sale.TechnicalNotes;
                    Recipe.DeviceID = NouvemGlobal.DeviceId;
                    Recipe.UserMasterID = NouvemGlobal.UserId;
                    Recipe.EditDate = DateTime.Now;
                    Recipe.AutoCompleteMix = sale.AutoCompleteMix;
                    Recipe.MultipleReceipts = sale.MultipleReceipts;
                    Recipe.ReworkNonAdjust = sale.ReworkStockNonAdjust;
                    Recipe.NouOrderMethodID = sale.NouOrderMethodID;

                    var dbDetails =
                        entities.RecipeDetails.Where(x => x.RecipeID == Recipe.RecipeID && x.Deleted == null).ToList();
                    foreach (var recipeDetail in dbDetails)
                    {
                        recipeDetail.Deleted = DateTime.Now;
                    }

                    var localRecipes = new List<RecipeDetail>();
                    foreach (var detail in sale.SaleDetails)
                    {
                        var orderDetail = new RecipeDetail
                        {
                            RecipeID = Recipe.RecipeID,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            UnitPrice = detail.UnitPrice,
                            VatCodeID = detail.VatCodeID,
                            INMasterID = detail.INMasterID,
                            NouIssueMethodID = detail.NouIssueMethodID,
                            PriceListID_Cost = detail.PriceListID,
                            NouUOMID = detail.NouUOMID,
                            PlusTolerance = detail.PlusTolerance,
                            PlusToleranceUOM = detail.PlusToleranceUOM,
                            MinusTolerance = detail.MinusTolerance,
                            MinusToleranceUOM = detail.MinusToleranceUOM,
                            UseFullBatch = detail.UseAllBatch,
                            IgnoreTolerances = detail.IgnoreTolerances,
                            RecordLostWeight = detail.RecordLostWeight,
                            AllowManualWeightEntry = detail.AllowManualWeightEntry,
                            Notes = detail.Notes,
                            OrderIndex = detail.OrderIndex
                        };

                        localRecipes.Add(orderDetail);
                    }

                    entities.RecipeDetails.AddRange(localRecipes);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
           
            return false;
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeById(int id)
        {
            var Recipe = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbRecipe = entities.Recipes.FirstOrDefault(x => x.RecipeID == id);
                    if (dbRecipe == null)
                    {
                        throw new Exception(string.Format("Recipe id:{0} not found", id));
                    }

                    Recipe.SaleID = dbRecipe.RecipeID;
                    Recipe.INMasterID = dbRecipe.INMasterID;
                    Recipe.Alias = dbRecipe.Alias;
                    Recipe.TechnicalNotes = dbRecipe.MixNote;
                    Recipe.PriceListIDValue = dbRecipe.PriceLIst_ID_Value;
                    Recipe.PriceListID = dbRecipe.PriceListID_Cost;
                    Recipe.TypicalBatchSize = dbRecipe.TypicalBatchSize;
                    Recipe.TypicalMixQty = dbRecipe.TypicalMixQty;
                    Recipe.MultipleReceipts = dbRecipe.MultipleReceipts;
                    Recipe.ReworkStockNonAdjust = dbRecipe.ReworkNonAdjust;
                    Recipe.NouOrderMethodID = dbRecipe.NouOrderMethodID;
                    Recipe.SaleDetails = new List<SaleDetail>();
                    Recipe.AutoCompleteMix = dbRecipe.AutoCompleteMix;

                    var details =
                        entities.RecipeDetails.Where(x => x.RecipeID == dbRecipe.RecipeID && x.Deleted == null);
                    foreach (var detail in details)
                    {
                        var orderDetail = new SaleDetail
                        {
                            //LoadingSale = true,
                            SaleDetailID = detail.RecipeDetailID,
                            UnitPrice = detail.UnitPrice,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            VatCodeID = detail.VatCodeID,
                            INMasterID = detail.INMasterID,
                            NouIssueMethodID = detail.NouIssueMethodID,
                            NouUOMID = detail.NouUOMID,
                            PriceListID = detail.PriceListID_Cost.ToInt(),
                            PlusTolerance = detail.PlusTolerance,
                            PlusToleranceUOM = detail.PlusToleranceUOM,
                            MinusTolerance = detail.MinusTolerance,
                            MinusToleranceUOM = detail.MinusToleranceUOM,
                            UseAllBatch = detail.UseFullBatch,
                            IgnoreTolerances = detail.IgnoreTolerances,
                            RecordLostWeight = detail.RecordLostWeight,
                            AllowManualWeightEntry = detail.AllowManualWeightEntry,
                            Notes = detail.Notes,
                            OrderIndex = detail.OrderIndex
                        };

                        Recipe.SaleDetails.Add(orderDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Recipe;
        }

        /// <summary>
        /// Gets the recipes.
        /// </summary>
        /// <returns></returns>
        public IList<Sale> GetRecipes()
        {
            var recipes = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    recipes = (from recipe in entities.Recipes
                        join product in entities.INMasters on recipe.INMasterID equals product.INMasterID
                        where recipe.Deleted == null
                        select new Sale
                        {
                            SaleID = recipe.RecipeID,
                            Alias = recipe.Alias,
                            TypicalBatchSize = recipe.TypicalBatchSize,
                            TypicalMixQty = recipe.TypicalMixQty,
                            MultipleReceipts = recipe.MultipleReceipts,
                            ReworkStockNonAdjust = recipe.ReworkNonAdjust,
                            INMaster = product.Name,
                            CreationDate = recipe.CreationDate,
                            INMasterID = recipe.INMasterID
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return recipes;
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeByProductId(int id)
        {
            var Recipe = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbRecipe = entities.Recipes.FirstOrDefault(x => x.INMasterID == id && x.Deleted == null);
                    if (dbRecipe == null)
                    {
                        return null;
                    }

                    Recipe.SaleID = dbRecipe.RecipeID;
                    Recipe.INMasterID = dbRecipe.INMasterID;
                    Recipe.Alias = dbRecipe.Alias;
                    Recipe.TechnicalNotes = dbRecipe.MixNote;
                    Recipe.PriceListIDValue = dbRecipe.PriceLIst_ID_Value;
                    Recipe.PriceListID = dbRecipe.PriceListID_Cost;
                    Recipe.TypicalBatchSize = dbRecipe.TypicalBatchSize;
                    Recipe.TypicalMixQty = dbRecipe.TypicalMixQty;
                    Recipe.MultipleReceipts = dbRecipe.MultipleReceipts;
                    Recipe.ReworkStockNonAdjust = dbRecipe.ReworkNonAdjust;
                    Recipe.NouOrderMethodID = dbRecipe.NouOrderMethodID;
                    Recipe.AutoCompleteMix = dbRecipe.AutoCompleteMix;
                    Recipe.SaleDetails = new List<SaleDetail>();

                    var details =
                        entities.RecipeDetails.Where(x => x.RecipeID == dbRecipe.RecipeID && x.Deleted == null);
                    foreach (var detail in details)
                    {
                        var orderDetail = new SaleDetail
                        {
                            //LoadingSale = true,
                            SaleDetailID = detail.RecipeDetailID,
                            UnitPrice = detail.UnitPrice,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            VatCodeID = detail.VatCodeID,
                            INMasterID = detail.INMasterID,
                            NouIssueMethodID = detail.NouIssueMethodID,
                            NouUOMID = detail.NouUOMID,
                            PriceListID = detail.PriceListID_Cost.ToInt(),
                            PlusTolerance = detail.PlusTolerance,
                            PlusToleranceUOM = detail.PlusToleranceUOM,
                            MinusTolerance = detail.MinusTolerance,
                            MinusToleranceUOM = detail.MinusToleranceUOM,
                            UseAllBatch = detail.UseFullBatch,
                            IgnoreTolerances = detail.IgnoreTolerances,
                            RecordLostWeight = detail.RecordLostWeight,
                            AllowManualWeightEntry = detail.AllowManualWeightEntry,
                            Notes = detail.Notes,
                            OrderIndex = detail.OrderIndex
                        };

                        Recipe.SaleDetails.Add(orderDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Recipe;
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeOrderMethodByProductId(int id)
        {
            var recipe = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbRecipe = (from inMaster in entities.INMasters
                        join localRecipe in entities.Recipes
                            on inMaster.INMasterID equals localRecipe.INMasterID
                        where localRecipe.INMasterID == id && localRecipe.Deleted == null
                        select new {inMaster.Name, localRecipe.NouOrderMethodID}).FirstOrDefault();

                    if (dbRecipe == null)
                    {
                        return null;
                    }

                    recipe.NouOrderMethodID = dbRecipe.NouOrderMethodID;
                    recipe.Name = dbRecipe.Name;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return recipe;
        }

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        public IList<StockDetail> GetRecipeByIngredientProductId(int id)
        {
            var recipe = new List<StockDetail>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbRecipeDetail = entities.RecipeDetails.FirstOrDefault(x => x.INMasterID == id && x.Deleted == null);
                    if (dbRecipeDetail == null)
                    {
                        return null;
                    }

                    var dbRecipe = entities.Recipes.First(x => x.RecipeID == dbRecipeDetail.RecipeID);
                    
                    recipe.Add(new StockDetail
                    {
                        INMasterID = dbRecipe.INMasterID,
                        TransactionQty = dbRecipe.TypicalBatchSize.ToDecimal(),
                        TypicalBatchSize = dbRecipe.TypicalBatchSize.ToDecimal(),
                        NouTransactionTypeID = 3,
                        OrderMethod = OrderMethod.Weight
                    });

                    var details =
                        entities.RecipeDetails.Where(x => x.RecipeID == dbRecipe.RecipeID && x.Deleted == null);
                    foreach (var detail in details)
                    {
                        var dispatchProduct = detail.INMasterID == id;
                        var orderMethod = OrderMethod.Quantity;
                        var method = entities.UOMMasters.FirstOrDefault(x => x.UOMMasterID == detail.NouUOMID);
                        if (method != null)
                        {
                            orderMethod = entities.NouOrderMethods
                                .First(x => x.NouOrderMethodID == method.NouOrderMethodID).Name;
                        }

                        recipe.Add(new StockDetail
                        {
                            INMasterID = detail.INMasterID,
                            TransactionQty = detail.QuantityOrdered.ToDecimal(),
                            TypicalBatchSize = detail.QuantityOrdered.ToDecimal(),
                            NouTransactionTypeID = 4,
                            OrderMethod = orderMethod,
                            DispatchProduct = dispatchProduct
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return recipe;
        }

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeByLastEdit()
        {
            var Recipe = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbRecipe = entities.Recipes.Where(x => x.Deleted == null).OrderByDescending(x => x.EditDate).FirstOrDefault();
                    if (dbRecipe == null)
                    {
                        throw new Exception("Last edit Recipe not found");
                    }

                    Recipe.SaleID = dbRecipe.RecipeID;
                    Recipe.INMasterID = dbRecipe.INMasterID;
                    Recipe.Alias = dbRecipe.Alias;
                    Recipe.TechnicalNotes = dbRecipe.MixNote;
                    Recipe.PriceListIDValue = dbRecipe.PriceLIst_ID_Value;
                    Recipe.PriceListID = dbRecipe.PriceListID_Cost;
                    Recipe.TypicalBatchSize = dbRecipe.TypicalBatchSize;
                    Recipe.TypicalMixQty = dbRecipe.TypicalMixQty;
                    Recipe.MultipleReceipts = dbRecipe.MultipleReceipts;
                    Recipe.ReworkStockNonAdjust = dbRecipe.ReworkNonAdjust;
                    Recipe.NouOrderMethodID = dbRecipe.NouOrderMethodID;
                    Recipe.AutoCompleteMix = dbRecipe.AutoCompleteMix;
                    Recipe.SaleDetails = new List<SaleDetail>();

                    var details =
                        entities.RecipeDetails.Where(x => x.RecipeID == dbRecipe.RecipeID && x.Deleted == null);
                    foreach (var detail in details)
                    {
                        var orderDetail = new SaleDetail
                        {
                            //LoadingSale = true,
                            SaleDetailID = detail.RecipeDetailID,
                            UnitPrice = detail.UnitPrice,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            VatCodeID = detail.VatCodeID,
                            INMasterID = detail.INMasterID,
                            NouIssueMethodID = detail.NouIssueMethodID,
                            NouUOMID = detail.NouUOMID,
                            PriceListID = detail.PriceListID_Cost.ToInt(),
                            PlusTolerance = detail.PlusTolerance,
                            PlusToleranceUOM = detail.PlusToleranceUOM,
                            MinusTolerance = detail.MinusTolerance,
                            MinusToleranceUOM = detail.MinusToleranceUOM,
                            UseAllBatch = detail.UseFullBatch,
                            IgnoreTolerances = detail.IgnoreTolerances,
                            RecordLostWeight = detail.RecordLostWeight,
                            AllowManualWeightEntry = detail.AllowManualWeightEntry,
                            Notes = detail.Notes,
                            OrderIndex = detail.OrderIndex
                        };

                        Recipe.SaleDetails.Add(orderDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Recipe;
        }

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        public PROrder GetPROrderLastEdit()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.PROrders.OrderByDescending(x => x.EditDate).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        public Sale GetRecipeByFirstLast(bool first)
        {
            var Recipe = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    Recipe dbRecipe;
                    if (first)
                    {
                        dbRecipe = entities.Recipes.FirstOrDefault(x => x.Deleted == null);
                    }
                    else
                    {
                        dbRecipe = entities.Recipes.Where(x => x.Deleted == null).OrderByDescending(x => x.RecipeID).FirstOrDefault();
                    }
                    
                    if (dbRecipe == null)
                    {
                        throw new Exception("Last edit Recipe not found");
                    }

                    Recipe.SaleID = dbRecipe.RecipeID;
                    Recipe.INMasterID = dbRecipe.INMasterID;
                    Recipe.Alias = dbRecipe.Alias;
                    Recipe.TechnicalNotes = dbRecipe.MixNote;
                    Recipe.PriceListIDValue = dbRecipe.PriceLIst_ID_Value;
                    Recipe.PriceListID = dbRecipe.PriceListID_Cost;
                    Recipe.TypicalBatchSize = dbRecipe.TypicalBatchSize;
                    Recipe.TypicalMixQty = dbRecipe.TypicalMixQty;
                    Recipe.MultipleReceipts = dbRecipe.MultipleReceipts;
                    Recipe.ReworkStockNonAdjust = dbRecipe.ReworkNonAdjust;
                    Recipe.NouOrderMethodID = dbRecipe.NouOrderMethodID;
                    Recipe.AutoCompleteMix = dbRecipe.AutoCompleteMix;
                    Recipe.SaleDetails = new List<SaleDetail>();

                    var details =
                        entities.RecipeDetails.Where(x => x.RecipeID == dbRecipe.RecipeID && x.Deleted == null);
                    foreach (var detail in details)
                    {
                        var orderDetail = new SaleDetail
                        {
                            //LoadingSale = true,
                            SaleDetailID = detail.RecipeDetailID,
                            UnitPrice = detail.UnitPrice,
                            QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                            VatCodeID = detail.VatCodeID,
                            INMasterID = detail.INMasterID,
                            NouIssueMethodID = detail.NouIssueMethodID,
                            NouUOMID = detail.NouUOMID,
                            PriceListID = detail.PriceListID_Cost.ToInt(),
                            PlusTolerance = detail.PlusTolerance,
                            PlusToleranceUOM = detail.PlusToleranceUOM,
                            MinusTolerance = detail.MinusTolerance,
                            MinusToleranceUOM = detail.MinusToleranceUOM,
                            UseAllBatch = detail.UseFullBatch,
                            IgnoreTolerances = detail.IgnoreTolerances,
                            RecordLostWeight = detail.RecordLostWeight,
                            AllowManualWeightEntry = detail.AllowManualWeightEntry,
                            Notes = detail.Notes,
                            OrderIndex = detail.OrderIndex
                        };

                        Recipe.SaleDetails.Add(orderDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("AddNewOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Recipe;
        }

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        /// <returns>The application issue methods.</returns>
        public IList<NouIssueMethod> GetNouIssueMethods()
        {
            var issues = new List<NouIssueMethod>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    issues = entities.NouIssueMethods.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return issues;
        }

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        /// <returns>The application issue methods.</returns>
        public IList<UOMMaster> GetNouUOMs()
        {
            var issues = new List<UOMMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    issues = entities.UOMMasters.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return issues;
        }

       /// <summary>
       /// Reworks recipe stock into the current batch.
       /// </summary>
       /// <param name="stockTransactionId">The rework stock id.</param>
       /// <param name="prOrderId">The batch id.</param>
       /// <param name="deviceid">The device id.</param>
       /// <param name="userid">The user id.</param>
       /// <returns>Rework message.</returns>
        public string ReworkRecipeStock(int stockTransactionId, int prOrderId, int deviceid, int userid)
        {
            var response = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_ReworkRecipeStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@StockTransactionID", SqlDbType.Int);
                    param.Value = stockTransactionId;
                    var idParam = new SqlParameter("@PROrderID", SqlDbType.Int);
                    idParam.Value = prOrderId;
                    var deviceParam = new SqlParameter("@DeviceID", SqlDbType.Int);
                    deviceParam.Value = deviceid;
                    var userParam = new SqlParameter("@UserID", SqlDbType.Int);
                    userParam.Value = userid;

                    var outputParamResponse = new SqlParameter("@Message", SqlDbType.NVarChar, 1000);
                    outputParamResponse.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);
                    command.Parameters.Add(idParam);
                    command.Parameters.Add(deviceParam);
                    command.Parameters.Add(userParam);
                    command.Parameters.Add(outputParamResponse);

                    command.ExecuteNonQuery();

                    var localResponse = command.Parameters["@Message"].Value;
                    if (localResponse != DBNull.Value)
                    {
                        response = (string)localResponse;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                response = ex.Message;
            }

            return response;
        }

        #region private

        /// <summary>
        /// Creates a deep copy transaction.
        /// </summary>
        /// <param name="transaction">The transaction to deep copy</param>
        /// <returns>A deep copy transaction.</returns>
        private StockTransaction CreateDeepCopyTransaction(StockTransaction transaction)
        {
            return new StockTransaction
            {
                MasterTableID = transaction.MasterTableID,
                BatchNumberID = transaction.BatchNumberID,
                Serial = transaction.Serial,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = transaction.NouTransactionTypeID,
                TransactionQTY = transaction.TransactionQTY,
                TransactionWeight = transaction.TransactionWeight,
                GrossWeight = transaction.GrossWeight,
                Tare = transaction.Tare,
                Pieces = transaction.Pieces,
                Alibi = transaction.Alibi,
                WarehouseID = transaction.WarehouseID,
                INMasterID = transaction.INMasterID,
                ContainerID = transaction.ContainerID,
                Consumed = transaction.Consumed,
                ManualWeight = transaction.ManualWeight,
                DeviceMasterID = transaction.DeviceMasterID,
                UserMasterID = transaction.UserMasterID,
                IsBox = transaction.IsBox,
                InLocation = transaction.InLocation,
                StockTransactionID_Container = transaction.StockTransactionID_Container,
                LabelID = transaction.LabelID,
                Reference = transaction.Reference,
                Deleted = transaction.Deleted,
                BPMasterID = transaction.BPMasterID
            };
        }

        /// <summary>
        /// Creates a deep copy transaction traceability.
        /// </summary>
        /// <param name="transactionTrace">The transaction trace to deep copy</param>
        /// <returns>A deep copy transaction traceability.</returns>
        private TransactionTraceability CreateDeepCopyTransactionTraceability(TransactionTraceability transactionTrace)
        {
            return new TransactionTraceability
            {
               StockTransactionID = transactionTrace.StockTransactionID,
               NouTraceabilityMethodID = transactionTrace.NouTraceabilityMethodID,
               Value = transactionTrace.Value,
               Deleted = transactionTrace.Deleted,
               TraceabilityTemplateMasterID = transactionTrace.TraceabilityTemplateMasterID,
               DateMasterID = transactionTrace.DateMasterID,
               QualityMasterID = transactionTrace.QualityMasterID,
               DynamicName = transactionTrace.DynamicName
            };
        }

        #endregion
    }
}

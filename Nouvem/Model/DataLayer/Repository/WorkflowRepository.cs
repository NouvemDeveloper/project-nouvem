﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer.Interface;

    public class WorkflowRepository : NouvemRepositoryBase, IWorkflowRepository
    {
        /// <summary>
        /// Adds a new workflow.
        /// </summary>
        /// <param name="workflow">The workflow to add.</param>
        /// <returns>The id of the newly created workflow.</returns>
        public int AddWorkflow(Sale workflow)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var newWorkflow = new AttributeTemplateRecord
                    {
                        AttributeTemplateID = workflow.TemplateID,
                        CreationDate = DateTime.Now,
                        DeviceMasterID = NouvemGlobal.DeviceId,
                        NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                        DocumentNumberingID = workflow.DocumentNumberingID,
                        Number = workflow.Number,
                        UserMasterID_Creation = NouvemGlobal.UserId
                    };

                    entities.AttributeTemplateRecords.Add(newWorkflow);
                    entities.SaveChanges();

                    var attribute = workflow.CurrentWorkflowData.Attribute;
                    attribute.AttributeTemplateRecordsID = newWorkflow.AttributeTemplateRecordsID;
                    entities.AttributeWorkflows.Add(attribute);
                    entities.SaveChanges();
                    workflow.SaleID = newWorkflow.AttributeTemplateRecordsID;
                    return newWorkflow.AttributeTemplateRecordsID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="workflows">The workflow id's.</param>
        /// <param name="status">The status to update to.</param>
        /// <returns>Fla, as to successful update or not.</returns>
        public bool UpdateWorkflowStatus(HashSet<Sale> workflows, int status)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var i in workflows)
                    {
                        var workflow =
                          entities.AttributeTemplateRecords.FirstOrDefault(x => x.AttributeTemplateRecordsID == i.SaleID);
                        if (workflow != null)
                        {
                            workflow.NouDocStatusID = i.NouDocStatusID;
                            workflow.Notes = i.PopUpNote;
                            workflow.ModifiedDate = DateTime.Now;
                            workflow.UserMasterID_ModifiedBy = NouvemGlobal.UserId;

                            entities.Notes.Add(new Note
                            {
                                Notes = i.PopUpNote,
                                MasterTableID = i.SaleID,
                                CreationDate = DateTime.Now,
                                DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                UserMasterID = NouvemGlobal.UserId.ToInt(),
                                NoteType = Constant.Workflow
                            });

                            entities.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates alert status.
        /// </summary>
        /// <param name="alertIds">The alert id.</param>
        /// <param name="status">The status to update to.</param>
        /// <returns>Fla, as to successful update or not.</returns>
        public bool UpdateAlertsStatus(HashSet<int> alertIds, int status)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var i in alertIds)
                    {
                        var alert =
                          entities.Alerts.FirstOrDefault(x => x.AlertID == i);
                        if (alert != null)
                        {
                            alert.NouDocStatusID = status;
                            entities.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #region attribute workflow

        /// <summary>
        /// Updates a workflow workflow.
        /// </summary>
        /// <param name="attribute">The workflow to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateWorkflow(Model.DataLayer.AttributeWorkflow attribute)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    ;
                    if (attribute.AttributeWorkflowID == 0)
                    {
                        // new
                        entities.AttributeWorkflows.Add(attribute);
                    }
                    else
                    {
                        // update
                        var dbAttribute = entities.AttributeWorkflows.FirstOrDefault(x => x.AttributeWorkflowID == attribute.AttributeWorkflowID);
                        if (dbAttribute != null)
                        {
                            dbAttribute.Answer = attribute.Answer;
                            dbAttribute.DeviceID = NouvemGlobal.DeviceId;
                            dbAttribute.UserMasterID = NouvemGlobal.UserId;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        /// <summary>
        /// Adds a non standard response reason.
        /// </summary>
        /// <param name="reason">The reason to add.</param>
        /// <param name="attributeId">The associated attribute.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool AddNonStandardResponseReason(string reason, int attributeId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var nonStandard = new NonStandardResponseReason
                    {
                        Reason = reason
                    };

                    entities.NonStandardResponseReasons.Add(nonStandard);
                    entities.SaveChanges();
                    var dbAttribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == attributeId);
                    if (dbAttribute != null)
                    {
                        //dbAttribute.NonStandardResponseReasonID = nonStandard.NonStandardResponseReasonID;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a workflow workflow.
        /// </summary>
        /// <param name="workflow">The workflow to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool CompleteWorkflow(Sale workflow)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbWorkflow =
                        entities.AttributeTemplateRecords.FirstOrDefault(
                            x => x.AttributeTemplateRecordsID == workflow.SaleID);
                    if (dbWorkflow == null)
                    {
                        return false;
                    }

                    dbWorkflow.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                    dbWorkflow.UserMasterID_Completion = NouvemGlobal.UserId;
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        /// <returns>The current workflows.</returns>
        public IList<Sale> GetWorkflows(IList<int?> statuses)
        {
            var workflows = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbWorkflow = entities.AttributeTemplateRecords.Where(x => x.Deleted == null && statuses.Contains(x.NouDocStatusID));
                    foreach (var attributeTemplateRecord in dbWorkflow)
                    {
                        workflows.Add(new Sale
                        {
                            SaleID = attributeTemplateRecord.AttributeTemplateRecordsID,
                            CreationDate = attributeTemplateRecord.CreationDate,
                            TemplateID = attributeTemplateRecord.AttributeTemplateID,
                            AttributeTemplate = attributeTemplateRecord.AttributeTemplate,
                            DeviceId = attributeTemplateRecord.DeviceMasterID,
                            NouDocStatusID = attributeTemplateRecord.NouDocStatusID,
                            Number = attributeTemplateRecord.Number,
                            DocumentNumberingID = attributeTemplateRecord.DocumentNumberingID,
                            UserIDCreation = attributeTemplateRecord.UserMasterID_Creation,
                            UserIDCompletion = attributeTemplateRecord.UserMasterID_Completion,
                            SaleType = ViewType.Workflow
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return workflows;
        }

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        /// <returns>The current workflows.</returns>
        public IList<Sale> GetWorkflowsByDate(IList<int?> statuses, DateTime start, DateTime end)
        {
            end = end.AddDays(1);
            var workflows = new List<Sale>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbWorkflow = entities.AttributeTemplateRecords.Where(x => x.Deleted == null
                        && x.CreationDate >= start && x.CreationDate < end
                        && statuses.Contains(x.NouDocStatusID));
                    foreach (var attributeTemplateRecord in dbWorkflow)
                    {
                        var workflow = new Sale
                        {
                            SaleID = attributeTemplateRecord.AttributeTemplateRecordsID,
                            CreationDate = attributeTemplateRecord.CreationDate,
                            TemplateID = attributeTemplateRecord.AttributeTemplateID,
                            DeviceId = attributeTemplateRecord.DeviceMasterID,
                            NouDocStatusID = attributeTemplateRecord.NouDocStatusID,
                            Number = attributeTemplateRecord.Number,
                            DocumentNumberingID = attributeTemplateRecord.DocumentNumberingID,
                            UserIDCreation = attributeTemplateRecord.UserMasterID_Creation,
                            UserIDCompletion = attributeTemplateRecord.UserMasterID_Completion,
                            //PopUpNote = attributeTemplateRecord.Notes,
                            SaleType = ViewType.Workflow,
                            Name = attributeTemplateRecord.AttributeTemplate.Name,
                            EditDate = attributeTemplateRecord.ModifiedDate,
                            UserIDModified = attributeTemplateRecord.UserMasterID_ModifiedBy,
                            Notes = entities.Notes.Where(x => x.MasterTableID == attributeTemplateRecord.AttributeTemplateRecordsID
                                && x.NoteType == Constant.Workflow && x.Deleted == null).ToList()
                        };

                        workflow.SetPopUpNotes();
                        workflows.Add(workflow);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return workflows;
        }

        /// <summary>
        /// Gets the workflow.
        /// </summary>
        /// <returns>The current workflow by id.</returns>
        public Sale GetWorkflowById(int id)
        {
            var workflow = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var attributeTemplateRecord = entities.AttributeTemplateRecords.FirstOrDefault(x => x.AttributeTemplateRecordsID == id);

                    if (attributeTemplateRecord == null)
                    {
                        return workflow;
                    }

                    workflow = new Sale
                    {
                        SaleID = attributeTemplateRecord.AttributeTemplateRecordsID,
                        CreationDate = attributeTemplateRecord.CreationDate,
                        TemplateID = attributeTemplateRecord.AttributeTemplateID,
                        DeviceId = attributeTemplateRecord.DeviceMasterID,
                        NouDocStatusID = attributeTemplateRecord.NouDocStatusID,
                        Number = attributeTemplateRecord.Number,
                        DocumentNumberingID = attributeTemplateRecord.DocumentNumberingID,
                        UserIDCreation = attributeTemplateRecord.UserMasterID_Creation,
                        UserIDCompletion = attributeTemplateRecord.UserMasterID_Completion,
                        WorkflowData = new List<AttributeAllocationData>()
                    };

                    var attributeTemplate = attributeTemplateRecord.AttributeTemplate;
                    workflow.TemplateData = new AttributeTemplateData
                    {
                        AttributeTemplateID = attributeTemplate.AttributeTemplateID,
                        AttributeTemplateGroupID = attributeTemplate.AttributeTemplateGroupID,
                        DeviceID = attributeTemplate.DeviceID,
                        Name = attributeTemplate.Name,
                        UserMasterID = attributeTemplate.UserMasterID
                    };

                    var dbData =
                        entities.AttributeAllocations.Where(
                            x => x.AttributeTemplateID == workflow.TemplateID && x.Deleted == null).OrderBy(x => x.Sequence).ToList();

                    var templateRecordAttributes = entities.App_GetTemplateRecordAttributes(workflow.SaleID).ToList();

                    foreach (var attributeAllocation in dbData)
                    {
                        Model.DataLayer.AttributeWorkflow localAttribute = null;
                        Model.DataLayer.AttributeWorkflow localNonStandardAttribute = null;

                        var alertsRecorded = false;
                        var localDbAttribute =
                            templateRecordAttributes.FirstOrDefault(
                                x => x.AttributeMasterID == attributeAllocation.AttributeMaster.AttributeMasterID && x.AttributeID_Parent == null);
                        if (localDbAttribute != null)
                        {
                            localAttribute = this.CreateAttribute(localDbAttribute);

                            var localNonStandardDbAttribute =
                           templateRecordAttributes.FirstOrDefault(
                               x => x.AttributeMasterID == attributeAllocation.AttributeMaster.AttributeMasterID && x.AttributeID_Parent == localAttribute.AttributeWorkflowID);
                            if (localNonStandardDbAttribute != null)
                            {
                                localNonStandardAttribute = this.CreateAttribute(localNonStandardDbAttribute);
                                alertsRecorded = true;
                            }
                        }

                        workflow.WorkflowData.Add(new AttributeAllocationData
                        {
                            AttributionAllocation = attributeAllocation,
                            AttributeMaster = attributeAllocation.AttributeMaster,
                            Attribute = localAttribute,
                            AttributeNonStandard = localNonStandardAttribute,
                            NouTraceabilityType = attributeAllocation.AttributeMaster.NouTraceabilityType,
                            NouTraceabilityTypeNonStandard = attributeAllocation.AttributeMaster.NouTraceabilityType1,
                            AlertsRecorded = alertsRecorded
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return workflow;
        }

        /// <summary>
        /// Gets the user alerts.
        /// </summary>
        /// <returns>The user alerts.</returns>
        public IList<Sale> GetAlerts(IList<int?> docStatusIds, DateTime start, DateTime end)
        {
            var alerts = new List<Sale>();
            var localEnd = end.AddDays(1);
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAlerts =
                        entities.Alerts.Where(
                            x =>
                                docStatusIds.Contains(x.NouDocStatusID) && x.CreationDate >= start &&
                                x.CreationDate < localEnd && x.Deleted == null).ToList();

                    foreach (var dbAlert in dbAlerts)
                    {
                        var number = 0;
                        var description = string.Empty;
                        var batch = string.Empty;
                        int? refId = 0;
                        int? num = 0;
                        var status = string.Empty;
                        if (dbAlert.AttributeTemplateRecord != null)
                        {
                            number = dbAlert.AttributeTemplateRecord.Number;
                            description = Strings.HACCP;
                            refId = dbAlert.AttributeTemplateRecordsID;
                            num = dbAlert.AttributeTemplateRecord.NouDocStatusID;
                            status = dbAlert.AttributeTemplateRecord.AttributeTemplate.Name;
                        }
                        else if (dbAlert.APGoodsReceipt != null)
                        {
                            number = dbAlert.APGoodsReceipt.Number;
                            description = Strings.GoodsIn;
                            refId = dbAlert.APGoodsReceiptID;
                            num = dbAlert.APGoodsReceipt.NouDocStatusID;
                            var goods = dbAlert.APGoodsReceipt.APGoodsReceiptDetails.FirstOrDefault();
                            if (goods != null)
                            {
                                status = goods.INMaster.AttributeTemplate.Name;
                            }
                        }
                        else if (dbAlert.PROrderID != null)
                        {
                            var localOrder = entities.PROrders.FirstOrDefault(x => x.PROrderID == dbAlert.PROrderID);

                            description = Strings.Production;
                            refId = dbAlert.PROrderID;

                            if (localOrder != null)
                            {
                                number = localOrder.Number;
                                num = localOrder.NouDocStatusID;
                                batch = localOrder.Reference;
                                var goods = entities.StockTransactions.FirstOrDefault(x => x.MasterTableID == dbAlert.PROrderID && x.NouTransactionTypeID != 7);
                                if (goods != null)
                                {
                                    status = goods.INMaster.AttributeTemplate.Name;
                                }
                            }
                        }
                        else if (dbAlert.ARDispatchID != null)
                        {
                            description = Strings.Dispatch;
                            refId = dbAlert.ARDispatchID;

                            var dispatch =
                                entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == dbAlert.ARDispatchID);

                            if (dispatch != null)
                            {
                                number = dispatch.Number;
                                num = dispatch.NouDocStatusID;
                                var template = entities.AttributeAllocations.FirstOrDefault(x =>
                                    x.AttributeMasterID == dbAlert.AttributeMasterID);
                                if (template != null)
                                {
                                    status = template.AttributeTemplate.Name;
                                }
                            }
                        }

                        alerts.Add(new Sale
                        {
                            SaleID = dbAlert.AlertID,
                            APOrderID = dbAlert.AttributeMasterID,
                            Reference = dbAlert.AlertMessage,
                            UserIDCreation = dbAlert.UserMasterID_UserToAlert,
                            CreationDate = dbAlert.CreationDate,
                            Opened = dbAlert.AlertOpened.BoolToYesNo(),
                            NouDocStatusID = dbAlert.NouDocStatusID,
                            BaseDocumentReferenceID = refId,
                            Number = number,
                            UserIDCompletion = dbAlert.UserMasterID,
                            InvoiceNumber = num,
                            NouDocStatus = status,
                            Name = dbAlert.AttributeMaster != null ? dbAlert.AttributeMaster.Description : string.Empty,
                            Description = description,
                            ProcessID = dbAlert.ProcessID,
                            BatchNo = batch
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return alerts;
        }

        /// <summary>
        /// Gets the users to alert.
        /// </summary>
        /// <returns>The user/user groups to alert.</returns>
        public AlertUsers GetAlertUsers()
        {
            var users = new AlertUsers();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbUsers = entities.UserAlerts.Where(x => x.Deleted == null).ToList();
                    var dbGroupUsers = entities.UserGroupAlerts.Where(x => x.Deleted == null).ToList();
                    users.UserAlerts = dbUsers;
                    users.UserGroupAlerts = dbGroupUsers;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return users;
        }

        /// <summary>
        /// Adds the alerts to the db.
        /// </summary>
        /// <param name="alerts">The alerts to add.</param>
        /// <returns>Flag, indicating a successful add.</returns>
        public bool AddUserAlerts(IList<Alert> alerts)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Alerts.AddRange(alerts);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes the alerts matching the input parameters.
        /// </summary>
        /// <param name="workflowiId">The workflow id.</param>
        /// <param name="attributeMasterId">The attribute id.</param>
        /// <returns>Flag, as to successful removal or not.</returns>
        public bool RemoveUserAlerts(int workflowiId, int attributeMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alertsToRemove =
                        entities.Alerts.Where(
                            x =>
                                x.AttributeTemplateRecordsID == workflowiId && x.AttributeMasterID == attributeMasterId &&
                                x.Deleted == null);

                    var nonStandardToRemove =
                        entities.AttributeWorkflows.FirstOrDefault(
                            x => x.AttributeTemplateRecordsID == workflowiId && x.AttributeMasterID == attributeMasterId && x.AttributeID_Parent != null);
                    if (nonStandardToRemove != null)
                    {
                        nonStandardToRemove.Deleted = DateTime.Now;
                    }

                    foreach (var alert in alertsToRemove)
                    {
                        alert.Deleted = DateTime.Now;
                    }

                    entities.SaveChanges();
                    return true;
                    ;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Determines if there are any unread alerts.
        /// </summary>
        /// <param name="userIds">The users to check.</param>
        /// <returns>Flag indicating whether there are any unread alerts.</returns>
        public bool AreThereNewAlerts(HashSet<int?> userIds, bool useAlertsOpen)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (useAlertsOpen)
                    {
                        return entities.Alerts
                            .Any(x => userIds.Contains(x.UserMasterID_UserToAlert)
                                      && x.AlertOpened != true && x.Deleted == null);
                    }

                    return entities.Alerts
                        .Any(x => userIds.Contains(x.UserMasterID_UserToAlert)
                                  && x.AlertRead != true && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Determines if there are any unread alerts.
        /// </summary>
        /// <param name="userIds">The users to check.</param>
        /// <returns>Flag indicating whether there are any unread alerts.</returns>
        public bool MarkAlertsAsRead(HashSet<int?> userIds)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alerts =
                        entities.Alerts.Where(x => x.AlertRead == null && userIds.Contains(x.UserMasterID_UserToAlert));
                    foreach (var alert in alerts)
                    {
                        alert.AlertRead = true;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Marks all user alerts as opened.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>Flag indicating whether all the user alerts ware marked as opened.</returns>
        public bool MarkAllUserAlertAsComplete(int userId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alerts = entities.Alerts.Where(x => x.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID && x.UserMasterID_UserToAlert == userId);
                    foreach (var alert in alerts)
                    {
                        alert.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Marks all user alerts as opened.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>Flag indicating whether all the user alerts ware marked as opened.</returns>
        public bool MarkAllUserAlertAsViewed(int userId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alerts = entities.Alerts.Where(x => (x.AlertOpened == null || x.AlertOpened == false) && x.UserMasterID_UserToAlert == userId);
                    foreach (var alert in alerts)
                    {
                        alert.AlertOpened = true;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Marks an alert as opened.
        /// </summary>
        /// <param name="alertId">The alert id.</param>
        /// <returns>Flag indicating whether the alert was marked as opened.</returns>
        public bool MarkAlertAsViewed(int alertId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alert = entities.Alerts.FirstOrDefault(x => x.AlertID == alertId);
                    if (alert != null)
                    {
                        alert.AlertOpened = true;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the date of the last workflow created matching the input template.
        /// </summary>
        /// <param name="templateId">The input template.</param>
        /// <returns>The last workflow created matching the input template.</returns>
        public DateTime? GetLastWorkflowDate(int templateId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var workflow = entities.AttributeTemplateRecords
                        .OrderByDescending(x => x.AttributeTemplateRecordsID)
                        .FirstOrDefault(x => x.AttributeTemplateID == templateId);

                    if (workflow != null)
                    {
                        return workflow.CreationDate;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Creates an attribute from the input data.
        /// </summary>
        /// <param name="attribute">The attrubute to copy.</param>
        /// <returns>A copied attribute.</returns>
        private Model.DataLayer.AttributeWorkflow CreateAttribute(App_GetTemplateRecordAttributes_Result attribute)
        {
            var localAttribute = new Model.DataLayer.AttributeWorkflow();

            localAttribute.Answer = attribute.Answer;
            localAttribute.DeviceID = NouvemGlobal.DeviceId;
            localAttribute.UserMasterID = NouvemGlobal.UserId;
            localAttribute.AttributeWorkflowID = attribute.AttributeWorkflowID;
            localAttribute.AttributeTemplateRecordsID = attribute.AttributeTemplateRecordsID;
            localAttribute.AttributeMasterID = attribute.AttributeMasterID;
            localAttribute.AttributeID_Parent = attribute.AttributeID_Parent;
            return localAttribute;
        }
    }
}



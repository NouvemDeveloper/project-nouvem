﻿// -----------------------------------------------------------------------
// <copyright file="UOMRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer.Interface;

    public class UOMRepository : NouvemRepositoryBase, IUOMRepository
    {
        /// <summary>
        /// Retrieve all the cuoms.
        /// </summary>
        /// <returns>A collection of uoms.</returns>
        public IList<UOMMaster> GetUOMMaster()
        {
            this.Log.LogDebug(this.GetType(), "GetUOMMaster(): Attempting to retrieve all the uoms");
            var uoms= new List<UOMMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    uoms = entities.UOMMasters.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} uoms retrieved", uoms.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return uoms;
        }

        /// <summary>
        /// Add or updates the uoms list.
        /// </summary>
        /// <param name="uoms">The uoms to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateUOMMasters(IList<UOMMaster> uoms)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateUOMMasters(): Attempting to update the uoms");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    uoms.ToList().ForEach(x =>
                    {
                        if (x.UOMMasterID == 0)
                        {
                            // new
                            var newUOM = new UOMMaster
                            {
                                Code = x.Code,
                                Name = x.Name,
                                NouOrderMethodID = x.NouOrderMethodID,
                                Deleted = false
                            };

                            entities.UOMMasters.Add(newUOM);
                        }
                        else
                        {
                            // update
                            var dbUOM =
                                entities.UOMMasters.FirstOrDefault(
                                    uom => uom.UOMMasterID == x.UOMMasterID && !uom.Deleted);

                            if (dbUOM != null)
                            {
                                dbUOM.Code = x.Code;
                                dbUOM.Name = x.Name;
                                dbUOM.NouOrderMethodID = x.NouOrderMethodID;
                                dbUOM.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Uoms successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }
    }
}

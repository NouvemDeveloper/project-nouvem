﻿// -----------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Xml.Linq;
using DevExpress.Data;
using DevExpress.Data.Filtering.Helpers;
using Microsoft.Win32;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;
    using Nouvem.Security;

    public class UserRepository : NouvemRepositoryBase, IUserRepository
    {
        /// <summary>
        /// Method that adds a new user.
        /// </summary>
        /// <param name="user">The user to add.</param>
        /// <returns>A flag, indicating a successful addition or not.</returns>
        public int AddUser(User user)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to add a new user master record.");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var newUser = new UserMaster();
                    newUser.UserName = user.UserMaster.UserName;
                    newUser.FullName = user.UserMaster.FullName;
                    newUser.Mobile = user.UserMaster.Mobile;
                    newUser.Email = user.UserMaster.Email;
                    newUser.Password = user.UserMaster.Password;
                    newUser.DeviceID = NouvemGlobal.DeviceId;
                    newUser.EditDate = DateTime.Now;
                    newUser.PasswordNeverExpires = user.UserMaster.PasswordNeverExpires;
                    newUser.ChangeAtNextLogin = user.UserMaster.ChangeAtNextLogin;
                    newUser.SuspendUser = user.UserMaster.SuspendUser;
                    newUser.UserGroupID = user.Group.UserGroupID;
                    newUser.Photo = user.UserMaster.Photo;
                    newUser.Reference = user.UserMaster.Reference;
                    newUser.DisplayPassword = user.UserMaster.DisplayPassword;
                    newUser.InActiveFrom = user.UserMaster.InActiveFrom;
                    newUser.InActiveTo = user.UserMaster.InActiveTo;
                    newUser.ActiveFrom = user.UserMaster.ActiveFrom;
                    newUser.ActiveTo = user.UserMaster.ActiveTo;

                    entities.UserMasters.Add(newUser);
                    entities.SaveChanges();

                    //user.Audit.TableID = newUser.UserMasterID;
                    //entities.Audits.Add(user.Audit);
                    //entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), string.Format("New user with user master id {0} has been successfully created", newUser.UserMasterID));
                    return newUser.UserMasterID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Method that updates an existing user.
        /// </summary>
        /// <param name="user">The user to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateUser(User user)
        {
            this.Log.LogDebug(this.GetType(), "UpdateUser(): Attempting to update a user record.");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbUser = entities.UserMasters.FirstOrDefault(
                        x => x.UserMasterID == user.UserMaster.UserMasterID);

                    if (dbUser != null)
                    {
                        dbUser.UserName = user.UserMaster.UserName;
                        dbUser.FullName = user.UserMaster.FullName;
                        dbUser.Mobile = user.UserMaster.Mobile;
                        dbUser.Reference = user.UserMaster.Reference;
                        dbUser.Email = user.UserMaster.Email;
                        dbUser.DeviceID = NouvemGlobal.DeviceId;
                        dbUser.EditDate = DateTime.Now;
                        dbUser.Password = user.UserMaster.Password;
                        dbUser.PasswordNeverExpires = user.UserMaster.PasswordNeverExpires;
                        dbUser.ChangeAtNextLogin = user.UserMaster.ChangeAtNextLogin;
                        dbUser.SuspendUser = user.UserMaster.SuspendUser;
                        dbUser.UserGroupID = user.Group.UserGroupID;
                        dbUser.Photo = user.UserMaster.Photo;
                        dbUser.DisplayPassword = user.UserMaster.DisplayPassword;
                        dbUser.InActiveFrom = user.UserMaster.InActiveFrom;
                        dbUser.InActiveTo = user.UserMaster.InActiveTo;
                        dbUser.ActiveFrom = user.UserMaster.ActiveFrom;
                        dbUser.ActiveTo = user.UserMaster.ActiveTo;

                        //entities.Audits.Add(user.Audit);

                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("User with user id {0} has been successfully updated", user.UserMaster.UserMasterID));
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("User with user id {0} was not found", user.UserMaster.UserMasterID));
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Method that adds a new user group.
        /// </summary>
        /// <param name="userGroup">The user group to add.</param>
        /// <returns>A flag, indicating a successful addition or not.</returns>
        public int AddUserGroup(UserGroup_ userGroup)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to add a new user group record.");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.UserGroup_.Add(userGroup);
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), string.Format("New user group with user group id {0} has been successfully created", userGroup.UserGroupID));
                    return userGroup.UserGroupID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Method that updates an existing user group.
        /// </summary>
        /// <param name="userGroups">The user group to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateUserGroups(IList<UserGroup> userGroups)
        {
            this.Log.LogDebug(this.GetType(), "UpdateUser(): Attempting to update the user groups");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    userGroups.ToList().ForEach(x =>
                    {
                        ProgressBar.Run();
                        if (x.UserGroupID == 0)
                        {
                            // new
                            var newGroup = new UserGroup_
                            {
                                Description = x.Description,
                                Deleted = false
                            };

                            entities.UserGroup_.Add(newGroup);
                        }
                        else
                        {
                            // update
                            var dbGroup =
                                entities.UserGroup_.FirstOrDefault(
                                    group => group.UserGroupID == x.UserGroupID && !group.Deleted);

                            if (dbGroup != null)
                            {
                                dbGroup.Description = x.Description;
                                dbGroup.Deleted = x.Deleted;
                            }

                            var modules = x.Modules;
                            foreach (var module in modules)
                            {
                                var lookUp = entities.ModuleLookUps.FirstOrDefault(look =>
                                    look.UserGroupID == module.ParentID && look.NouModuleID == module.UserGroupID &&
                                    look.Deleted == null);
                                if (lookUp != null)
                                {
                                    if (module.IsSelected)
                                    {
                                        lookUp.Deleted = null;
                                    }
                                    else
                                    {
                                        lookUp.Deleted = DateTime.Now;
                                    }
                                }
                                else if (module.IsSelected)
                                {
                                    var dbLookUp = new ModuleLookUp
                                    {
                                        UserGroupID = module.ParentID,
                                        NouModuleID = module.UserGroupID
                                    };

                                    entities.ModuleLookUps.Add(dbLookUp);
                                }

                                if (module.Modules != null)
                                {
                                    foreach (var childModule in module.Modules)
                                    {
                                        var childlookUp = entities.ModuleLookUps.FirstOrDefault(look =>
                                            look.UserGroupID == module.ParentID && look.NouModuleID == childModule.UserGroupID &&
                                            look.Deleted == null);
                                        if (childlookUp != null)
                                        {
                                            if (childModule.IsSelected)
                                            {
                                                childlookUp.Deleted = null;
                                            }
                                            else
                                            {
                                                childlookUp.Deleted = DateTime.Now;
                                            }
                                        }
                                        else if (childModule.IsSelected)
                                        {
                                            var dbLookUp = new ModuleLookUp
                                            {
                                                UserGroupID = module.ParentID,
                                                NouModuleID = childModule.UserGroupID
                                            };

                                            entities.ModuleLookUps.Add(dbLookUp);
                                        }

                                        if (childModule.Modules != null)
                                        {
                                            foreach (var childOfChildModule in childModule.Modules)
                                            {
                                                var childOfChildlookUp = entities.ModuleLookUps.FirstOrDefault(look =>
                                                    look.UserGroupID == module.ParentID && look.NouModuleID == childOfChildModule.UserGroupID &&
                                                    look.Deleted == null);
                                                if (childOfChildlookUp != null)
                                                {
                                                    if (childOfChildModule.IsSelected)
                                                    {
                                                        childOfChildlookUp.Deleted = null;
                                                    }
                                                    else
                                                    {
                                                        childOfChildlookUp.Deleted = DateTime.Now;
                                                    }
                                                }
                                                else if (childOfChildModule.IsSelected)
                                                {
                                                    var dbLookUp = new ModuleLookUp
                                                    {
                                                        UserGroupID = module.ParentID,
                                                        NouModuleID = childOfChildModule.UserGroupID
                                                    };

                                                    entities.ModuleLookUps.Add(dbLookUp);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (module.Description.CompareIgnoringCase(Strings.Reports))
                                {
                                    foreach (var report in module.Modules)
                                    {
                                        var reportLookUp = entities.UserReports.FirstOrDefault(look =>
                                            look.UserGroupID == report.UserGroupID && look.ReportID == report.NodeID &&
                                            look.Deleted == null);
                                        if (reportLookUp != null)
                                        {
                                            if (report.IsSelected)
                                            {
                                                reportLookUp.Deleted = null;
                                            }
                                            else
                                            {
                                                reportLookUp.Deleted = DateTime.Now;
                                            }
                                        }
                                        else if (report.IsSelected)
                                        {
                                            var dbReportLookUp = new UserReport
                                            {
                                                UserGroupID = report.UserGroupID,
                                                ReportID = report.NodeID == 0 ? (int?)null : report.NodeID,
                                                ReportName = report.Description
                                            };

                                            entities.UserReports.Add(dbReportLookUp);
                                        }
                                    }
                                }
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "User groups successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Copys the user group settings.
        /// </summary>
        /// <param name="from">Group to copy from.</param>
        /// <param name="to">Group to Copy to.</param>
        /// <returns>Flag, as to successful copy of not.</returns>
        public bool CopyUserGroupSettings(int from, int to)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_CopyUserGroupSettings(from, to);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        public IList<UserGroup_> GetUserGroups()
        {
            this.Log.LogDebug(this.GetType(), "GetUserGroups(): Attempting to retrieve all the user groups");
            List<UserGroup_> groups;

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                    var entities = new NouvemEntities();
                    groups = entities.UserGroup_
                        .Where(userGroup => !userGroup.Deleted)
                        .OrderBy(x => x.Description)
                        .ToList();
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetUserGroups(): {0} business partner groups successfully retrieved.", groups.Count));
            return groups;
        }

        ///// <summary>
        ///// Method which returns the non deleted user groups.
        ///// </summary>
        ///// <returns>A collection of non deleted user groups.</returns>
        //public IList<UserGroup> GetUserGroupsAndModules()
        //{
        //    this.Log.LogDebug(this.GetType(), "GetUserGroups(): Attempting to retrieve all the user groups");
        //    List<UserGroup> userGroups = new List<UserGroup>();

        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //           var groups = entities.UserGroup_
        //                .Where(userGroup => !userGroup.Deleted)
        //                .OrderBy(x => x.Description);

        //            var modules = entities.NouModules.Where(x => x.Deleted == null);

        //            var lookUps = (from lookup in entities.ModuleLookUps
        //                    join module in entities.NouModules on lookup.NouModuleID equals module.NouModuleID
        //                    where lookup.Deleted == null
        //                    select new { lookup.NouModuleID, lookup.ModuleLookUpID, lookup.UserGroupID, module.Name })
        //                .ToList();

        //            var groupNodeId = 0;
        //            foreach (var userGroup in groups)
        //            {
        //                var group = new UserGroup
        //                {
        //                    UserGroupID = userGroup.UserGroupID,
        //                    Description = userGroup.Description,
        //                    NodeID = userGroup.UserGroupID
        //                };

        //                userGroups.Add(group);

        //                foreach (var module in modules)
        //                {
        //                    groupNodeId--;
        //                    userGroups.Add(new UserGroup
        //                    {
        //                        UserGroupID = module.NouModuleID,
        //                        Description = module.Name,
        //                        ParentID = userGroup.UserGroupID,
        //                        NodeID = groupNodeId,
        //                        IsSelected = lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID && x.NouModuleID == module.NouModuleID) || !lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID),
        //                        Modules = new List<UserGroup> { new UserGroup { Description = "Test", ParentID = groupNodeId, NodeID = groupNodeId - 100} }
        //                    });
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //    }

        //    return userGroups;
        //}

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        public IList<UserGroup> GetUserGroupsAndModules(IList<ReportData> reports)
        {
            this.Log.LogDebug(this.GetType(), "GetUserGroups(): Attempting to retrieve all the user groups");
            List<UserGroup> userGroups = new List<UserGroup>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var groups = entities.UserGroup_
                         .Where(userGroup => !userGroup.Deleted)
                         .OrderBy(x => x.Description);

                    var modules = entities.NouModules.Where(x => x.Deleted == null);
                    reports = reports.Where(x => !x.Name.StartsWithIgnoringCase(Constant.SubReport)).ToList();

                    var lookUps = (from lookup in entities.ModuleLookUps
                                   join module in entities.NouModules on lookup.NouModuleID equals module.NouModuleID
                                   where lookup.Deleted == null
                                   select new { lookup.NouModuleID, lookup.ModuleLookUpID, lookup.UserGroupID, module.Name })
                        .ToList();

                    var reportLookUps = entities.UserReports.Where(x =>
                        x.Deleted == null && x.UserGroupID != null && x.ReportID != null);

                    var groupNodeId = 0;
                    foreach (var userGroup in groups)
                    {
                        var group = new UserGroup
                        {
                            UserGroupID = userGroup.UserGroupID,
                            Description = userGroup.Description,
                            NodeID = userGroup.UserGroupID,
                            Modules =  new List<UserGroup>()
                        };

                        foreach (var module in modules.Where(x => !x.ParentID.HasValue))
                        {
                            groupNodeId--;
                            var localGroup = new UserGroup
                            {
                                UserGroupID = module.NouModuleID,
                                Description = module.Name,
                                ParentID = userGroup.UserGroupID,
                                NodeID = groupNodeId,
                                IsSelected = lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID && x.NouModuleID == module.NouModuleID) || !lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID),
                                Modules = new List<UserGroup>()
                            };

                            var childModules = modules.Where(x => x.ParentID == module.NouModuleID);
                            foreach (var childModule in childModules)
                            {
                                var localChildGroup = new UserGroup
                                {
                                    UserGroupID = childModule.NouModuleID,
                                    Description = childModule.Name,
                                    ParentID = module.ParentID.ToInt(),
                                    NodeID = groupNodeId,
                                    IsSelected = lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID && x.NouModuleID == childModule.NouModuleID) || !lookUps.Any(x => x.NouModuleID == childModule.NouModuleID),
                                    Modules = new List<UserGroup>()
                                };

                                var childOfChildModules = modules.Where(x => x.ParentID == childModule.NouModuleID);
                                foreach (var childOfChildModule in childOfChildModules)
                                {
                                    var localChildOfChildGroup = new UserGroup
                                    {
                                        UserGroupID = childOfChildModule.NouModuleID,
                                        Description = childOfChildModule.Name,
                                        ParentID = childModule.ParentID.ToInt(),
                                        NodeID = groupNodeId,
                                        IsSelected = lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID && x.NouModuleID == childOfChildModule.NouModuleID) || !lookUps.Any(x => x.NouModuleID == childOfChildModule.NouModuleID),
                                        Modules = new List<UserGroup>()
                                    };

                                    var childOfChildOfChildModules = modules.Where(x => x.ParentID == childOfChildModule.NouModuleID);
                                    foreach (var childOfChildOfChildModule in childOfChildOfChildModules)
                                    {
                                        var localChildOfChildOfChildGroup = new UserGroup
                                        {
                                            UserGroupID = childOfChildOfChildModule.NouModuleID,
                                            Description = childOfChildOfChildModule.Name,
                                            ParentID = childOfChildModule.ParentID.ToInt(),
                                            NodeID = groupNodeId,
                                            IsSelected = lookUps.Any(x => x.UserGroupID == userGroup.UserGroupID && x.NouModuleID == childOfChildOfChildModule.NouModuleID) || !lookUps.Any(x => x.NouModuleID == childOfChildOfChildModule.NouModuleID),
                                            Modules = new List<UserGroup>()
                                        };

                                        localChildOfChildGroup.Modules.Add(localChildOfChildOfChildGroup);
                                    }

                                    localChildGroup.Modules.Add(localChildOfChildGroup);
                                }
                            
                                localGroup.Modules.Add(localChildGroup);
                            }

                            if (localGroup.Description.CompareIgnoringCase(Strings.Reports))
                            {
                                foreach (var nouReport in reports)
                                {
                                    localGroup.Modules.Add(new UserGroup
                                    {
                                        UserGroupID = userGroup.UserGroupID,
                                        Description = nouReport.Name,
                                        NodeID = nouReport.ReportID,
                                        IsSelected = reportLookUps.Any(x => x.UserGroupID == userGroup.UserGroupID  && x.ReportName == nouReport.Name) || !reportLookUps.Any(x => x.UserGroupID == userGroup.UserGroupID),
                                    });
                                }
                            }

                            group.Modules.Add(localGroup);
                        }

                        userGroups.Add(group);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return userGroups;
        }

        /// <summary>
        /// Method which returns the non deleted user groups.
        /// </summary>
        /// <returns>A collection of non deleted user groups.</returns>
        public IList<UserAlertsLookUp> GetUserLookUps(int userMasterID)
        {
            var users = new List<UserAlertsLookUp>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    users =
                        entities.UserAlertsLookUps.Where(x => x.Deleted == null && x.UserMasterID == userMasterID)
                            .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return users;
        }

        /// <summary>
        /// Method that verifies a users login credentials.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The user password.</param>
        /// <returns>The verified user master data object, or null if not verified.</returns>
        public UserMaster VerifyLoginUser(string userId, string password)
        {
            this.Log.LogDebug(this.GetType(), string.Format("VerifyLoginUser(): Attempting to verify user:{0}", userId));
            UserMaster userMaster = null;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    userMaster =
                        entities.UserMasters.FirstOrDefault(
                            user =>
                            user.UserName.Trim().Equals(userId.Trim()) && Security.DecryptString(user.Password).Trim().Equals(password.Trim()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("User {0} verification result:{1}", userId, userMaster != null));
            return userMaster;
        }

        /// <summary>
        /// Saves the logged in user shortcuts.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Flag, as to successful save or not.</returns>
        public bool SaveUserShortcuts(User user)
        {
            this.Log.LogDebug(this.GetType(), string.Format("SaveUserShortcuts(): Saving shortcuts for user:{0}", user.UserMaster.UserMasterID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var dbShortcut in entities.UserShortcuts.Where(x => x.UserMasterID == user.UserMaster.UserMasterID))
                    {
                        dbShortcut.ShowShortcut = false;
                    }

                    foreach (var userShortcut in user.UserShortcuts)
                    {
                        var dbShortcut =
                            entities.UserShortcuts.FirstOrDefault(x => x.UserMasterID == user.UserMaster.UserMasterID && x.Name == userShortcut.Name);
                      
                        if (dbShortcut != null)
                        {
                            dbShortcut.UserMasterID = user.UserMaster.UserMasterID;
                            dbShortcut.ShowShortcut = userShortcut.ShowShortcut;
                            dbShortcut.Deleted = userShortcut.Deleted;
                        }
                        else
                        {
                            userShortcut.UserMasterID = user.UserMaster.UserMasterID;
                            entities.UserShortcuts.Add(userShortcut);
                        }
                    }

                    entities.SaveChanges();
                    var userShortCuts = entities.UserShortcuts.Where(x => x.UserMasterID == user.UserMaster.UserMasterID && x.ShowShortcut);
                    NouvemGlobal.LoggedInUser.UserShortcuts = userShortCuts.ToList();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
      
            return false;
        }


        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        public IList<User> GetUsers()
        {
            this.Log.LogDebug(this.GetType(), "GetUsers(): Retrieving all the application users.");
            List<User> users;
            try
            {
                using (var entities = new NouvemEntities())
                {
                   users = (from dbUser in entities.UserMasters
                            select new User
                            {
                                UserMaster = dbUser, 
                                Group = dbUser.UserGroup_,
                                UserShortcuts = dbUser.UserShortcuts.Where(x => x.ShowShortcut).ToList()
                            }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} users successfully retrieved", users.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return users;
        }

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        public User GetUser(string name)
        {
            name = name.ToUpper();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var user = entities.UserMasters.FirstOrDefault(x => x.UserName.ToUpper().Equals(name));
                    if (user != null)
                    {
                        return new User {UserMaster = user, Group = user.UserGroup_};
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Method that returns the users.
        /// </summary>
        /// <returns>A collection of users.</returns>
        public IList<UserMaster> GetDbUsers()
        {
            this.Log.LogDebug(this.GetType(), "GetDbUsers(): Retrieving all the application users.");
            var users = new List<UserMaster>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    users = entities.UserMasters.Where(x =>
                            (x.InActiveFrom == null && x.InActiveTo == null)
                            || !(DateTime.Today >= x.InActiveFrom && DateTime.Today < x.InActiveTo))
                        .ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} users successfully retrieved", users.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return users;
        }

        /// <summary>
        /// Stores the remember user password flag.
        /// </summary>
        /// <param name="user">The user in question.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool StoreRememberPassword(User user)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbUser =
                        entities.UserMasters.FirstOrDefault(x => x.UserMasterID == user.UserMaster.UserMasterID);
                    if (dbUser != null)
                    {
                        dbUser.RememberPassword = user.UserMaster.RememberPassword;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the user password.
        /// </summary>
        /// <param name="user">The user in question.</param>
        /// <returns>Flag, as to successful retrieval or not.</returns>
        public byte[] GetUserPassword(string user)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbUser =
                        entities.UserMasters.FirstOrDefault(x => x.UserName == user);
                    if (dbUser != null && dbUser.RememberPassword == true)
                    {
                        return dbUser.Password;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Register a user logon.
        /// </summary>
        /// <param name="logon">The logon data.</param>
        /// <param name="userName">The logon user name.</param>
        public void RegisterLogon(UserLoggedOn logon, string userName)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RegisterLogon(): Attempting to register a logon for user:{0}", userName));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.UserLoggedOns.Add(logon);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("RegisterLogon(): logon registration for user:{0} successful", userName));
        }

        /// <summary>
        /// Register a user log out for the current user.
        /// </summary>
        public void RegisterLogout()
        {
            var userId= NouvemGlobal.UserId;
            this.Log.LogDebug(this.GetType(), string.Format("RegisterLogout(): Attempting to register a log out for user:{0}", NouvemGlobal.UserId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var loggedInUser = entities.UserLoggedOns
                        .Where(x => x.UserMasterID == userId).OrderByDescending(x => x.UserLoggedOnID)
                        .FirstOrDefault();

                    if (loggedInUser != null)
                    {
                        var loggedOutTime = DateTime.Now;
                        loggedInUser.SessionEnd = loggedOutTime;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("RegisterLogout():User {0} logged out at:{1}", userId, loggedOutTime));
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Check the logged in status of a user.
        /// </summary>
        /// <param name="user">The user logging in to check.</param>
        /// <returns>A flag, indicating whether the user is already logged in.</returns>
        public bool CheckLoggedInStatus(User user)
        {
            this.Log.LogDebug(this.GetType(), string.Format("CheckLoggedInStatus(): Checking the logged in status for user:{0}", user.UserMaster.UserName));
            var loggedIn = false;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var loggedInData =
                        entities.UserLoggedOns.Where(x => x.UserMasterID == user.UserMaster.UserMasterID && !x.Deleted)
                            .OrderByDescending(x => x.UserLoggedOnID)
                            .FirstOrDefault();

                    if (loggedInData != null)
                    {
                        var currentTime = DateTime.Now;

                        // if the system crashes, sessionend will be null, so if the local machine name and ip addresses match, ignore. (user won't be logged in twice on the same machine)
                        loggedIn = currentTime >= loggedInData.SessionStart 
                            && loggedInData.SessionEnd == null 
                            && loggedInData.DeviceIP != NouvemGlobal.GetIpAddress() 
                            && loggedInData.PCName != Environment.MachineName;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} User already logged in result:{0}", loggedIn));
            return loggedIn;
        }

        /// <summary>
        /// Forces the logging out of another user logged in with the current licensees credentials.
        /// </summary>
        public void ForceLogOff()
        {
            this.Log.LogDebug(this.GetType(), string.Format("ForceLogOff(): Attempting to force log out another user logged in the current licensees credentials: User Id{0}", NouvemGlobal.Licensee.Identifier));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localMachineName = Environment.MachineName;

                    var loggedInData =
                        entities.UserLoggedOns.Where(x => x.UserMasterID == NouvemGlobal.Licensee.Id && !x.PCName.Equals(localMachineName) && !x.Deleted) 
                            .OrderByDescending(x => x.UserLoggedOnID)
                            .FirstOrDefault();

                    if (loggedInData != null)
                    {
                        var currentTime = DateTime.Now;

                        // if the other user is still logged in, mark that user to be forced logged out..
                        if (currentTime >= loggedInData.SessionStart
                            && loggedInData.SessionEnd == null
                            && loggedInData.PCName != localMachineName)
                        {
                            loggedInData.ForceLogout = true;
                            entities.SaveChanges();

                            this.Log.LogDebug(this.GetType(), string.Format(
                                "UserLoggedOn id {0} marked for force log out. User id: {1}, PC name:{2}", 
                                loggedInData.UserLoggedOnID, 
                                loggedInData.UserMasterID, 
                                loggedInData.PCName));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Checks whether the current user has been marked to be forced logged off.
        /// </summary>
        public bool CheckForForcedLogOff()
        {
            if (NouvemGlobal.Licensee == null)
            {
                return false;
            }

            this.Log.LogDebug(this.GetType(), string.Format("CheckForForcedLogOff(): Checking if the current user has been marked to be forced logged off: User Id{0}", NouvemGlobal.Licensee.Identifier));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var loggedInData =
                        entities.UserLoggedOns.FirstOrDefault(
                            x => x.UserMasterID == NouvemGlobal.Licensee.Id && x.ForceLogout == true && !x.Deleted);

                    if (loggedInData != null)
                    {
                        if (loggedInData.DeviceIP == NouvemGlobal.GetIpAddress())
                        {
                            loggedInData.Deleted = true;
                            entities.SaveChanges();

                            this.Log.LogDebug(this.GetType(), "Current user has been marked to be forced logged off");
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes any user log in records over a week old.
        /// </summary>
        public void RemoveOldLogins()
        {
            this.Log.LogDebug(this.GetType(), "RemoveOldLogins(): Attempting to remove user login records over a week old");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var lastWeek = DateTime.Today.Subtract(TimeSpan.FromDays(7));
                    var recordsToRemove =
                        entities.UserLoggedOns.Where(x => DbFunctions.TruncateTime(x.SessionEnd) < lastWeek);

                    if (recordsToRemove.Any())
                    {
                        entities.UserLoggedOns.RemoveRange(recordsToRemove);
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("{0} userlogon records successfully removed", recordsToRemove.Count()));
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieve all the database names from the server.
        /// </summary>
        /// <returns>A collection of database names.</returns>
        public IList<string> GetDatabaseNames()
        {
            this.Log.LogDebug(this.GetType(), "GetDatabaseNames(): Attempting to retrieve all the database names on the server");
            var dbNames = new List<string>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localDatabases = entities.GetDatabaseNames().ToList();

                    foreach (var database in localDatabases)
                    {
                        ApplicationSettings.DatabaseName = database;
                        EntityConnection.CreateConnectionString();

                        try
                        {
                            this.TestDatabase();
                            dbNames.Add(database);
                            this.Log.LogDebug(this.GetType(), string.Format("Database {0} is nouvem compatible", database));
                        }
                        catch (Exception)
                        {
                            // not compatible
                            this.Log.LogDebug(this.GetType(), string.Format("Database {0} is not nouvem compatible", database));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} database names retrieved", dbNames.Count));
            return dbNames;
        }

        /// <summary>
        /// Retrieve all the servers.
        /// </summary>
        /// <returns>A collection of server details.</returns>
        public IList<Server> GetServers()
        {
            this.Log.LogDebug(this.GetType(), "GetServers(): Attempting to retrieve all the servers");
            var localServers = new List<Server>();

            if (Settings.Default.ApplicationServers == null)
            {
                Settings.Default.ApplicationServers = new StringCollection();
            }

            foreach (var server in Settings.Default.ApplicationServers)
            {
                var servers = server.Split('|');
                if (servers.Count() == 5)
                {
                    // sql server authentication
                    localServers.Add(new Server {CompanyName = servers[0], ServerName = servers[1], DefaultDatabaseName = servers[2], UserId = servers[3], Password = servers[4] });
                }
                else
                {
                    // windows authentication
                    localServers.Add(new Server { CompanyName = servers[0], ServerName = servers[1], DefaultDatabaseName = servers[2] });
                }
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} servers retrieved", localServers.Count));

            return localServers;
        }

        /// <summary>
        /// Retrieve all the user group rules.
        /// </summary>
        /// <returns>A collection of user group rules.</returns>
        public IList<ViewGroupRule> GetUserGroupRules()
        {
            this.Log.LogDebug(this.GetType(), "GetUserGroupRules(): Attempting to retrieve all the user group rules");
            var rules = new List<ViewGroupRule>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localRules = entities.ViewGroupRules;
                    this.Log.LogDebug(this.GetType(), string.Format("{0} group rules retrieved", rules.Count()));

                    foreach (var localRule in localRules)
                    {
                        rules.Add(localRule);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return rules;
        }

        /// <summary>
        /// Retrieve all the user group values.
        /// </summary>
        /// <returns>A collection of user group values.</returns>
        public IList<NouAuthorisationValue> GetAuthorisationValues()
        {
            this.Log.LogDebug(this.GetType(), "GetAuthorisationValues(): Attempting to retrieve all the user authorisation values");
            var values = new List<NouAuthorisationValue>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbValues = entities.NouAuthorisationValues.Where(x => !x.Deleted);
                    this.Log.LogDebug(this.GetType(), string.Format("{0} values retrieved", dbValues.Count()));

                    dbValues.ToList().ForEach(x => values.Add(x));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return values;
        }

        /// <summary>
        /// Retrieve all the user group lists.
        /// </summary>
        /// <returns>A collection of user group lists.</returns>
        public IList<SystemAuthorisation> GetAuthorisations()
        {
            this.Log.LogDebug(this.GetType(), "GetAuthorisationLists(): Attempting to retrieve all the user authorisation lists");
            var lists = new List<SystemAuthorisation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    lists = (from aut in entities.NouAuthorisations.Where(x => !x.Deleted)
                       select new SystemAuthorisation
                       {
                           NouAuthorisationID = aut.NouAuthorisationID,
                           CodeSetting = aut.CodeSetting,
                           Description = aut.Description,
                           NouAuthorisationGroupNameID = aut.NouAuthorisationGroupNameID,
                           GroupName = aut.NouAuthorisationGroupName.GroupName
                       }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return lists;
        }

        /// <summary>
        /// Adds new user group rules.
        /// </summary>
        /// <param name="rules">The user group to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        public bool AddAuthorisationRules(IList<ViewGroupRule> rules)
        {
            if (!rules.Any())
            {
                return true;
            }

            this.Log.LogDebug(this.GetType(),
                string.Format("Attempting to add new user group rules - UserGroupID {0},", rules.First().UserGroupID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    rules.ToList().ForEach(x =>
                    {
                        var newRule = new UserGroupRule
                        {
                            NouAuthorisationListtID = x.NouAuthorisationListtID,
                            NouAuthorisationValueID = x.NouAuthorisationValueID,
                            UserGroupID = x.UserGroupID,
                            Deleted = false
                        };

                        entities.UserGroupRules.Add(newRule);
                    });
                   
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "New rule groups successfully added");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }

        /// <summary>
        /// Updates a user group rule.
        /// </summary>
        /// <param name="rules">The user group to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateAuthorisationRules(IList<ViewGroupRule> rules)
        {
            if (!rules.Any())
            {
                return true;
            }

            this.Log.LogDebug(this.GetType(),
                string.Format("Attempting to update user group rules - UserGroupID {0}", rules.First().UserGroupID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    rules.ToList().ForEach(x =>
                    {
                        var userGroupRule = entities.UserGroupRules.FirstOrDefault(
                           dbRule => dbRule.UserGroupRuleID == x.UserGroupRuleID && dbRule.NouAuthorisationListtID == x.NouAuthorisationListtID && !dbRule.Deleted);

                        if (userGroupRule != null)
                        {
                            userGroupRule.NouAuthorisationValueID = x.NouAuthorisationValueID;
                            this.Log.LogDebug(this.GetType(), "Rule group successfully updated");
                        }
                    });

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }

        /// <summary>
        /// Method that updates a user pasword.
        /// </summary>
        /// <param name="userId">The user name.</param>
        /// <param name="password">The updated password.</param>
        /// <returns>A flag, as to wherther a password was updated or not.</returns>
        public bool UpdatePassword(string userId, byte[] password)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdatePassword(): Attempting to update the password for user: {0}", userId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var user = entities.UserMasters.FirstOrDefault(x => x.UserName.Equals(userId));

                    if (user != null)
                    {
                        user.Password = password;
                        user.ChangeAtNextLogin = false;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "User password changed");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "User password could not be changed");
            return false;
        }
      
        /// <summary>
        /// Gets the logged in users window and search grid settings.
        /// </summary>
        /// <param name="userId">The logged in user id.</param>
        /// <returns>A collection of user associated window and data grid settings.</returns>
        public IList<WindowSetting> GetWindowSettings(int userId)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the window settings");
            var settings = new List<WindowSetting>();
            
            try
            {
                using (var entities = new NouvemEntities())
                {
                    settings =
                        entities.WindowSettings.Where(
                            x => x.UserMasterID == userId && !x.Deleted)
                            .ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} window settings retrieved", settings.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return settings;
        }

        /// <summary>
        /// Stores a users windows settings (positioning and grid data, if any)
        /// </summary>
        /// <param name="windows">The application windows.</param>
        public void SaveWindowSettings(IList<WindowSetting> windows)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to save {0} window settings for user:{1}", NouvemGlobal.LoggedInUser.UserMaster.UserMasterID, windows.Count));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    windows.ToList().ForEach(x =>
                    {
                        var setting =
                            entities.WindowSettings.FirstOrDefault(
                                window => window.Name == x.Name && window.UserMasterID == x.UserMasterID && !window.Deleted);

                        if (setting == null)
                        {
                            // new window setting
                            entities.WindowSettings.Add(x);
                        }
                        else
                        {
                            // existing window setting
                            setting.UserMasterID = x.UserMasterID;
                            setting.Name = x.Name;
                            setting.PositionTop = x.PositionTop;
                            setting.PositionLeft = x.PositionLeft;
                            setting.PositionWidth = x.PositionWidth;
                            setting.PositionHeight = x.PositionHeight;
                            setting.Maximised = x.Maximised;
                            setting.GridData = x.GridData;
                            entities.SaveChanges();
                        }
                    });

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Updates the users alerts look ups.
        /// </summary>
        /// <param name="users">The selected users.</param>
        /// <param name="userMasterId">The user id.</param>
        /// <returns>Flag, as to successfl update or not.</returns>
        public bool UpdateUserAlertsSetUp(IList<UserAlertsLookUp> users, int userMasterId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var alertLookUps =
                        entities.UserAlertsLookUps.Where(x => x.UserMasterID == userMasterId && x.Deleted == null);
                    foreach (var userAlertsLookUp in alertLookUps)
                    {
                        userAlertsLookUp.Deleted = DateTime.Now;
                    }

                    entities.UserAlertsLookUps.AddRange(users);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Method that tests the db cnnection.
        /// </summary>
        /// <returns>A flag, indicating whether a connection can be made to the database.</returns>
        /// <remarks>We could just use the entity framework test connection, but that just tests the connect.
        ///          We need to ensure it's the right database.</remarks>
        public bool TestDatabase()
        {
            this.Log.LogInfo(this.GetType(), string.Format("TestDatabase(): Attempting to connect to the database with the following entity string: {0}", ApplicationSettings.ConnectionString));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.NouBPTypes.FirstOrDefault();
                    this.Log.LogInfo(this.GetType(), "Connection successful");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the db pasword.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public string GetDBPassword(int deviceId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var password =
                        entities.DeviceSettings.FirstOrDefault(x =>
                            x.Name == "LogInData" && x.DeviceMasterID == deviceId);
                    if (password != null)
                    {
                        return password.Value5;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return string.Empty;
        }

        ///// <summary>
        ///// Method that tests the db cnnection.
        ///// </summary>
        ///// <returns>A flag, indicating whether a connection can be made to the database.</returns>
        ///// <remarks>We could just use the entity framework test connection, but that just tests the connect.
        /////          We need to ensure it's the right database.</remarks>
        //public bool TestDatabase()
        //{
        //    this.Log.LogInfo(this.GetType(), string.Format("TestDatabase(): Attempting to connect to the database with the following entity string: {0}", Settings.Default.ConnectionString));
        //    try
        //    {
        //        using (var entities = new NouvemEntities())
        //        {
        //            if (string.IsNullOrEmpty(Settings.Default.BackupDatabaseName))
        //            {
        //                return this.DbConnectionBackUp();
        //            }
        //            else
        //            {
        //                entities.NouBPTypes.FirstOrDefault();
        //                this.Log.LogInfo(this.GetType(), "Connection successful");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //        return this.DbConnectionBackUp();
        //    }

        //    return true;
        //}

        ///// <summary>
        ///// Uses the back up file connection string.
        ///// </summary>
        ///// <returns>A flag, indicating whether a connection can be made to the database.</returns>
        //public bool DbConnectionBackUp()
        //{
        //    this.Log.LogDebug(this.GetType(), "DbConnectionBackUp(): Attempting to use the stored xml back up connection string");
        //    try
        //    {
        //        var filePath = Path.Combine(Settings.Default.ConnectionStringPath, "conn.xml");
        //        var file = new XDocument();
        //        Security.DecryptXmlFile(ref file, filePath);

        //        var conn = file.Root.Element(Constant.ConnectionString).Value;
        //        var provider = file.Root.Element(Constant.Provider).Value;
        //        var server = file.Root.Element(Constant.Server).Value;
        //        var database = file.Root.Element(Constant.Database).Value;
        //        var userId = file.Root.Element(Constant.UserID).Value;
        //        var password = file.Root.Element(Constant.Password).Value;
        //        var company = file.Root.Element(Constant.Company).Value;
        //        var appConnString = file.Root.Element(Constant.ApplicationConnString).Value;
        //        var user = file.Root.Element(Constant.LoggedInUser).Value;

        //        Security.EncryptXmlFile(file, filePath);

        //        Settings.Default.ConnectionString = conn;
        //        Settings.Default.ProviderName = provider;
        //        Settings.Default.ServerName = server;
        //        Settings.Default.DatabaseName = database;
        //        Settings.Default.UserId = userId;
        //        Settings.Default.Password = password;
        //        Settings.Default.CompanyName = company;
        //        ApplicationSettings.ApplicationConnectionString = appConnString;
        //        ApplicationSettings.LoggedInUserIdentifier = user;
        //        Settings.Default.Save();

        //        using (var entities = new NouvemEntities())
        //        {
        //            this.Log.LogDebug(this.GetType(), string.Format("Conn:{0}, provider:{1}, server:{2}, database:{3}, userId:{4}, password:{5}, appConnString:{6}, user:{7}",
        //                                                        conn, provider, server, database, userId, password, appConnString, user));

        //            this.Log.LogDebug(this.GetType(), "Retrying db connection with stored credentials");
        //            var test = entities.NouBPTypes.FirstOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
        //        throw;
        //    }

        //    this.Log.LogDebug(this.GetType(), "Connection successful");
        //    return true;
        //}
    }
}

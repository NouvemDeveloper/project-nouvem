﻿// -----------------------------------------------------------------------
// <copyright file="ContainerRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer.Interface;

    public class ContainerRepository : NouvemRepositoryBase, IContainerRepository
    {
        /// <summary>
        /// Retrieve all the containers.
        /// </summary>
        /// <returns>A collection of containers.</returns>
        public IList<Container> GetContainers()
        {
            this.Log.LogDebug(this.GetType(), "GetContainers(): Attempting to retrieve all the containers");
            var containers = new List<Container>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                containers = entities.Containers.Where(x => !x.Deleted).ToList();
                this.Log.LogDebug(this.GetType(), string.Format("{0} containers retrieved", containers.Count()));
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return containers;
        }

        /// <summary>
        /// Retrieve all the containers types.
        /// </summary>
        /// <returns>A collection of container types.</returns>
        public IList<ContainerType> GetContainerTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetContainerTypes(): Attempting to retrieve all the container types");
            var containerTypes = new List<ContainerType>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                containerTypes = entities.ContainerTypes.Where(x => !x.Deleted).ToList();
                this.Log.LogDebug(this.GetType(), string.Format("{0} container types retrieved", containerTypes.Count()));
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return containerTypes;
        }

        /// <summary>
        /// Add or updates the containers list.
        /// </summary>
        /// <param name="containers">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateContainers(IList<Container> containers)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateContainers(): Attempting to update the containers");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    containers.ToList().ForEach(x =>
                    {
                        if (x.ContainerID == 0)
                        {
                            // new
                            entities.Containers.Add(x);
                        }
                        else
                        {
                            // update
                            var dbContainer =
                                entities.Containers.FirstOrDefault(
                                    container => container.ContainerID == x.ContainerID && !container.Deleted);

                            if (dbContainer != null)
                            {
                                dbContainer.Tare = x.Tare;
                                dbContainer.Photo = x.Photo;
                                dbContainer.Name = x.Name;
                                dbContainer.ContainerTypeID = x.ContainerTypeID;
                                dbContainer.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Containers successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the product containers.
        /// </summary>
        /// <returns>A collection of containers.</returns>
        public IList<TareCalculator> GetProductContainers()
        {
            this.Log.LogDebug(this.GetType(), "GetProductContainers(): Attempting to retrieve all the container product tare");
            var containers = new List<TareCalculator>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    containers = (from tare in entities.Containers.Where(x => !x.Deleted)
                        select new TareCalculator
                        {
                            ContainerID = tare.ContainerID,
                            ContainerName = tare.Name,
                            Tare = tare.Tare
                        }).ToList();
                    
                    this.Log.LogDebug(this.GetType(), string.Format("{0} containers retrieved", containers.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return containers;
        }
    }
}

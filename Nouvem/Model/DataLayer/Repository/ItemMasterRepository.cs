﻿// -----------------------------------------------------------------------
// <copyright file="ItemMasterRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;

namespace Nouvem.Model.DataLayer.Repository
{
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ItemMasterRepository : NouvemRepositoryBase, IItemMasterRepository
    {
        /// <summary>
        /// Method that adds a new inventory item.
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <returns>The newly added item id.</returns>
        public int AddInventoryItem(InventoryItem item)
        {
            this.Log.LogDebug(this.GetType(), "AddInventoryItem(): Attempting to add a new inventory item");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var newItem = new INMaster();
                    newItem.Code = item.Master.Code;
                    newItem.Name = item.Master.Name;
                    newItem.StockItem = item.Master.StockItem;
                    newItem.SalesItem = item.Master.SalesItem;
                    newItem.DeviceID = NouvemGlobal.DeviceId;
                    newItem.WeightRequired = item.Master.WeightRequired;
                    newItem.EditDate = DateTime.Now;
                    newItem.PurchaseItem = item.Master.PurchaseItem;
                    newItem.ReprintLabelAtDispatch = item.Master.ReprintLabelAtDispatch;
                    newItem.FixedAsset = item.Master.FixedAsset;
                    newItem.ProductionProduct = item.Master.ProductionProduct;
                    newItem.ReceipeProduct = item.Master.ReceipeProduct;
                    newItem.SalesNominalCode = item.Master.SalesNominalCode;
                    newItem.SalesNominalDeptCode = item.Master.SalesNominalDeptCode;
                    newItem.PurchaseNominalCode = item.Master.PurchaseNominalCode;
                    newItem.PurchaseNominalDeptCode = item.Master.PurchaseNominalDeptCode;
                    newItem.Remarks = item.Master.Remarks;
                    newItem.UserMasterID = NouvemGlobal.UserId;
                    newItem.CreationDate = DateTime.Now;
                    newItem.TypicalPieces = item.Master.TypicalPieces;
                    newItem.QtyPerBox = item.Master.QtyPerBox;
                    newItem.ActiveFrom = item.Master.ActiveFrom;
                    newItem.ActiveTo = item.Master.ActiveTo;
                    newItem.InActiveFrom = item.Master.InActiveFrom;
                    newItem.InActiveTo = item.Master.InActiveTo;
                    newItem.INGroupID = item.Group.INGroupID;
                    newItem.AccumulateTares = item.Master.AccumulateTares;
                    newItem.NominalWeight = item.Master.NominalWeight;
                    newItem.MinWeight = item.Master.MinWeight;
                    newItem.MaxWeight = item.Master.MaxWeight;
                    //newItem.ItemImage = item.Master.ItemImage;
                    newItem.APOrderID_Copy = item.Master.APOrderID_Copy;
                    newItem.OrderLeadTime = item.Master.OrderLeadTime;
                    newItem.MinimumStockLevel = item.Master.MinimumStockLevel;
                    newItem.PrintPieceLabels = item.Master.PrintPieceLabels;
                    newItem.PROrderID_Copy = item.Master.PROrderID_Copy;
                    newItem.Reference = item.Master.Reference;
                    newItem.WarehouseID = item.Master.WarehouseID;
                    newItem.Scales = item.Master.Scales;
                    newItem.BPMasterID = item.Master.BPMasterID;
                    newItem.NouPriceMethodID = item.Master.NouPriceMethodID;
                    newItem.NouOrderMethodID = item.Master.NouOrderMethodID;
                    newItem.SortIndex = 100000;
                    newItem.IsDeduction = item.Master.IsDeduction;
                    newItem.DeductionRate = item.Master.DeductionRate;
                    newItem.ApplyDeductionToDocket = item.Master.ApplyDeductionToDocket;

                    if (item.TraceabilityTemplate != null && item.TraceabilityTemplate.AttributeTemplateID > 0)
                    {
                        newItem.AttributeTemplateID = item.TraceabilityTemplate.AttributeTemplateID;
                    }
                    //else
                    //{
                    //    var group = entities.INGroups.FirstOrDefault(x => x.INGroupID == newItem.INGroupID);
                    //    if (group != null)
                    //    {
                    //        newItem.AttributeTemplateID = group.AttributeTemplateID;
                    //    }
                    //}

                    if (item.VatCode != null && item.VatCode.VATCodeID > 0)
                    {
                        newItem.VATCodeID = item.VatCode.VATCodeID;
                    }

                    if (item.Department != null && item.Department.DepartmentID > 0)
                    {
                        newItem.DepartmentID = item.Department.DepartmentID;
                    }

                    if (item.BoxTareContainer != null && item.BoxTareContainer.ContainerID > 0)
                    {
                        newItem.BoxTareContainerID = item.BoxTareContainer.ContainerID;
                    }

                    if (item.PiecesTareContainer != null && item.PiecesTareContainer.ContainerID > 0)
                    {
                        newItem.PiecesTareContainerID = item.PiecesTareContainer.ContainerID;
                    }

                    if (item.PalletTareContainer != null && item.PalletTareContainer.ContainerID > 0)
                    {
                        newItem.PalletTareContainerID = item.PalletTareContainer.ContainerID;
                    }

                    if (item.LinkedProductTareContainer != null && item.LinkedProductTareContainer.ContainerID > 0)
                    {
                        newItem.TareProductContainerID = item.LinkedProductTareContainer.ContainerID;
                    }
                    else
                    {
                        newItem.TareProductContainerID = null;
                    }

                    if (item.StockMode != null && item.StockMode.NouStockModeID > 0)
                    {
                        newItem.NouStockModeID = item.StockMode.NouStockModeID;
                    }

                    // save here, so as to retrieve the newly created INMasterID
                    entities.INMasters.Add(newItem);
                    entities.SaveChanges();

                    //item.PriceListDetail.INMasterID = newItem.INMasterID;
                    //entities.PriceListDetails.Add(item.PriceListDetail);

                    if (item.Attachments != null && item.Attachments.Any())
                    {
                        item.Attachments.ToList().ForEach(
                            attachment =>
                            {
                                attachment.INMasterID = newItem.INMasterID;
                                entities.INAttachments.Add(attachment);
                            });
                    }

                    if (item.Properties != null && item.Properties.Any())
                    {
                        item.Properties.ToList().ForEach(x =>
                        {
                            var localPropertySelection = new INPropertySelection
                            {
                                INMasterID = newItem.INMasterID,
                                INPropertyID = x.INPropertyID
                            };

                            entities.INPropertySelections.Add(localPropertySelection);
                        });
                    }

                    if (item.LabelField != null)
                    {
                        entities.ProductLabelFields.Add(item.LabelField);
                        newItem.ProductLabelFieldID = item.LabelField.ProductLabelFieldID;
                    }

                    entities.SaveChanges();
                    entities.App_AddEntityUpdate(newItem.INMasterID, EntityType.INMaster);
                    this.AddImage(newItem.INMasterID, item.Master.ItemImage);

                    //item.Audit.TableID = newItem.INMasterID;
                    //entities.Audits.Add(item.Audit);
                    //entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), string.Format("New inventory successfully created - ID:{0}", newItem.INMasterID));
                    return newItem.INMasterID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }
        
        /// <summary>
        /// Method that updates an existing inventory item.
        /// </summary>
        /// <param name="item">The item to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateInventoryItem(InventoryItem item)
        {
            this.Log.LogDebug(this.GetType(), string.Format("UpdateInventoryItem(): Attempting to update inventory item with id:{0}", item.Master.INMasterID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbItem = entities.INMasters.FirstOrDefault(x => x.INMasterID == item.Master.INMasterID);

                    if (dbItem != null)
                    {
                        dbItem.Code = item.Master.Code;
                        dbItem.Name = item.Master.Name;
                        dbItem.StockItem = item.Master.StockItem;
                        dbItem.SalesItem = item.Master.SalesItem;
                        dbItem.DeviceID = NouvemGlobal.DeviceId;
                        dbItem.EditDate = DateTime.Now;
                        dbItem.PurchaseItem = item.Master.PurchaseItem;
                        dbItem.FixedAsset = item.Master.FixedAsset;
                        dbItem.ProductionProduct = item.Master.ProductionProduct;
                        dbItem.ReceipeProduct = item.Master.ReceipeProduct;
                        dbItem.SalesNominalCode = item.Master.SalesNominalCode;
                        dbItem.SalesNominalDeptCode = item.Master.SalesNominalDeptCode;
                        dbItem.PurchaseNominalCode = item.Master.PurchaseNominalCode;
                        dbItem.PurchaseNominalDeptCode = item.Master.PurchaseNominalDeptCode;
                        dbItem.Remarks = item.Master.Remarks;
                        dbItem.PROrderID_Copy = item.Master.PROrderID_Copy;
                        dbItem.WeightRequired = item.Master.WeightRequired;
                        dbItem.TypicalPieces = item.Master.TypicalPieces;
                        dbItem.ReprintLabelAtDispatch = item.Master.ReprintLabelAtDispatch;
                        dbItem.QtyPerBox = item.Master.QtyPerBox;
                        dbItem.ActiveFrom = item.Master.ActiveFrom;
                        dbItem.ActiveTo = item.Master.ActiveTo;
                        dbItem.InActiveFrom = item.Master.InActiveFrom;
                        dbItem.InActiveTo = item.Master.InActiveTo;
                        dbItem.INGroupID = item.Group.INGroupID;
                        dbItem.NominalWeight = item.Master.NominalWeight;
                        dbItem.MinWeight = item.Master.MinWeight;
                        dbItem.MaxWeight = item.Master.MaxWeight;
                        //dbItem.ItemImage = item.Master.ItemImage;
                        dbItem.AccumulateTares = item.Master.AccumulateTares;
                        dbItem.PrintPieceLabels = item.Master.PrintPieceLabels;
                        dbItem.WarehouseID = item.Master.WarehouseID;
                        dbItem.Scales = item.Master.Scales;
                        dbItem.MinimumStockLevel = item.Master.MinimumStockLevel;
                        dbItem.OrderLeadTime = item.Master.OrderLeadTime;
                        dbItem.APOrderID_Copy = item.Master.APOrderID_Copy;
                        dbItem.Reference = item.Master.Reference;
                        dbItem.BPMasterID = item.Master.BPMasterID;
                        dbItem.NouPriceMethodID = item.Master.NouPriceMethodID;
                        dbItem.NouOrderMethodID = item.Master.NouOrderMethodID;
                        dbItem.IsDeduction = item.Master.IsDeduction;
                        dbItem.DeductionRate = item.Master.DeductionRate;
                        dbItem.ApplyDeductionToDocket = item.Master.ApplyDeductionToDocket;

                        if (item.TraceabilityTemplate != null && item.TraceabilityTemplate.AttributeTemplateID > 0)
                        {
                            dbItem.AttributeTemplateID = item.TraceabilityTemplate.AttributeTemplateID;
                        }
                        else
                        {
                            dbItem.TraceabilityTemplateNameID = null;
                        }

                        if (item.VatCode != null && item.VatCode.VATCodeID > 0)
                        {
                            dbItem.VATCodeID = item.VatCode.VATCodeID;
                        }
                        else
                        {
                            dbItem.VATCodeID = null;
                        }

                        if (item.Department != null && item.Department.DepartmentID > 0)
                        {
                            dbItem.DepartmentID = item.Department.DepartmentID;
                        }
                        else
                        {
                            dbItem.DepartmentID = null;
                        }

                        if (item.BoxTareContainer != null && item.BoxTareContainer.ContainerID > 0)
                        {
                            dbItem.BoxTareContainerID = item.BoxTareContainer.ContainerID;
                        }
                        else
                        {
                            dbItem.BoxTareContainerID = null;
                        }

                        if (item.PiecesTareContainer != null && item.PiecesTareContainer.ContainerID > 0)
                        {
                            dbItem.PiecesTareContainerID = item.PiecesTareContainer.ContainerID;
                        }
                        else
                        {
                            dbItem.PiecesTareContainerID = null;
                        }

                        if (item.PalletTareContainer != null && item.PalletTareContainer.ContainerID > 0)
                        {
                            dbItem.PalletTareContainerID = item.PalletTareContainer.ContainerID;
                        }
                        else
                        {
                            dbItem.PalletTareContainerID = null;
                        }

                        if (item.LinkedProductTareContainer != null && item.LinkedProductTareContainer.ContainerID > 0)
                        {
                            dbItem.TareProductContainerID = item.LinkedProductTareContainer.ContainerID;
                        }
                        else
                        {
                            dbItem.TareProductContainerID = null;
                        }

                        if (item.StockMode != null && item.StockMode.NouStockModeID > 0)
                        {
                            dbItem.NouStockModeID = item.StockMode.NouStockModeID;
                        }
                    }

                    if (item.Attachments != null)
                    {
                        item.Attachments.ToList().ForEach(
                        attachment =>
                        {
                            var dbAttachment =
                                entities.INAttachments.FirstOrDefault(
                                    x => x.INAttachmentID == attachment.INAttachmentID);

                            if (dbAttachment != null)
                            {
                                // updating an existing attachment
                                if (attachment.Deleted)
                                {
                                    entities.INAttachments.Remove(dbAttachment);
                                }
                                else
                                {
                                    dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                    dbAttachment.FileName = attachment.FileName;
                                    dbAttachment.File = attachment.File;
                                }
                            }
                            else
                            {
                                // adding a new attachment
                                var localAttachment = new INAttachment
                                {
                                    AttachmentDate = attachment.AttachmentDate,
                                    INMasterID = item.Master.INMasterID,
                                    FileName = attachment.FileName,
                                    File = attachment.File,
                                    Deleted = false
                                };

                                entities.INAttachments.Add(localAttachment);
                            }
                        });
                    }

                    if (item.Properties != null)
                    {
                        var propertiesToDelete =
                            entities.INPropertySelections.Where(
                                x => x.INMasterID == item.Master.INMasterID);

                        // Remove the current inventory properties.
                        entities.INPropertySelections.RemoveRange(propertiesToDelete);
                        item.Properties.ToList().ForEach(x =>
                        {
                            var localPropertySelection = new INPropertySelection
                            {
                                INMasterID = item.Master.INMasterID,
                                INPropertyID = x.INPropertyID
                            };

                            entities.INPropertySelections.Add(localPropertySelection);
                        });
                    }

                    if (item.SelectedPriceData != null && item.SelectedPriceData.Item1 != null && item.SelectedPriceData.Item2 != null)
                    {
                        // update the price.
                        var priceList =
                            entities.PriceListDetails.FirstOrDefault(
                                x =>
                                    x.PriceListID == item.SelectedPriceData.Item1.PriceListID &&
                                    x.INMasterID == item.Master.INMasterID && !x.Deleted);

                        if (priceList != null)
                        {
                            priceList.Price = item.SelectedPriceData.Item2.ToDecimal();
                        }
                    }

                    if (item.LabelField != null)
                    {
                        var localField =
                            entities.ProductLabelFields.FirstOrDefault(
                                x => x.ProductLabelFieldID == item.LabelField.ProductLabelFieldID);

                        if (localField != null)
                        {
                            #region update label items

                            localField.Field1 = item.LabelField.Field1;
                            localField.Field2 = item.LabelField.Field2;
                            localField.Field3 = item.LabelField.Field3;
                            localField.Field4 = item.LabelField.Field4;
                            localField.Field5 = item.LabelField.Field5;
                            localField.Field6 = item.LabelField.Field6;
                            localField.Field7 = item.LabelField.Field7;
                            localField.Field8 = item.LabelField.Field8;
                            localField.Field9 = item.LabelField.Field9;
                            localField.Field10 = item.LabelField.Field10;
                            localField.Field11 = item.LabelField.Field11;
                            localField.Field12 = item.LabelField.Field12;
                            localField.Field13 = item.LabelField.Field13;
                            localField.Field14 = item.LabelField.Field14;
                            localField.Field15 = item.LabelField.Field15;
                            localField.Field16 = item.LabelField.Field16;
                            localField.Field17 = item.LabelField.Field17;
                            localField.Field18 = item.LabelField.Field18;
                            localField.Field19 = item.LabelField.Field19;
                            localField.Field20 = item.LabelField.Field20;
                            localField.Field21 = item.LabelField.Field21;
                            localField.Field22 = item.LabelField.Field22;
                            localField.Field23 = item.LabelField.Field23;
                            localField.Field24 = item.LabelField.Field24;
                            localField.Field25 = item.LabelField.Field25;
                            localField.Field26 = item.LabelField.Field26;
                            localField.Field27 = item.LabelField.Field27;
                            localField.Field28 = item.LabelField.Field28;
                            localField.Field29 = item.LabelField.Field29;
                            localField.Field30 = item.LabelField.Field30;
                            localField.MultiLineField1 = item.LabelField.MultiLineField1;
                            localField.MultiLineField2 = item.LabelField.MultiLineField2;
                            localField.Barcode1 = item.LabelField.Barcode1;
                            localField.Barcode2 = item.LabelField.Barcode2;
                            localField.Barcode3 = item.LabelField.Barcode3;
                            localField.Barcode4 = item.LabelField.Barcode4;

                            #endregion
                        }
                        else
                        {
                            entities.ProductLabelFields.Add(item.LabelField);
                            dbItem.ProductLabelFieldID = item.LabelField.ProductLabelFieldID;
                        }
                    }

                    //entities.Audits.Add(item.Audit);
                    entities.SaveChanges();
                    this.AddImage(dbItem.INMasterID, item.Master.ItemImage);

                    this.Log.LogDebug(this.GetType(), string.Format("Inventory item ID:{0} successfully updated", item.Master.INMasterID));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public bool AddImage(int id, byte[] image)
        {
            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_AddProductImage", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@INMasterID", SqlDbType.Int);
                    param.Value = id;
                    var idParam = new SqlParameter("@Image", SqlDbType.VarBinary, -1);
                    idParam.Value = image;
                    command.Parameters.Add(param);
                    command.Parameters.Add(idParam);
                    command.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public byte[] GetImage(int id)
        {
            byte[] dataResult = null;
            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand("App_GetProductImage", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@INMasterID", SqlDbType.Int);
                    param.Value = id;
                    var outputParam = new SqlParameter("@Image", SqlDbType.VarBinary, -1);
                    outputParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);
                    command.Parameters.Add(outputParam);
                    command.ExecuteNonQuery();
                    var localDataResult = command.Parameters["@Image"].Value;


                    if (localDataResult != DBNull.Value)
                    {
                        dataResult = (byte[])localDataResult;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));

            }

            return dataResult;
        }

        /// <summary>
        /// Method which returns the inventory items.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        public IList<InventoryItem> GetInventoryItems()
        {
            this.Log.LogDebug(this.GetType(), "GetInventoryItems(): Attempting to retrieve all the inventory items");
            var items = new List<InventoryItem>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    items = (from inMaster in entities.INMasters
                             where inMaster.Deleted == null
                             select new InventoryItem
                             {
                                 Master = inMaster,
                                 Group = inMaster.INGroup,
                                 BoxTareContainer = inMaster.BoxTareContainer,
                                 PiecesTareContainer = inMaster.PiecesTareContainer,
                                 PalletTareContainer = inMaster.PalletTareContainer,
                                 LinkedProductTareContainer = inMaster.Container3,
                                 Department = inMaster.Department,
                                 //DateTemplate = inMaster.DateTemplateName,
                                 TraceabilityTemplate = inMaster.AttributeTemplate,
                                 //QualityTemplate = inMaster.QualityTemplateName,
                                 VatCode = inMaster.VATCode,
                                 StockMode = inMaster.NouStockMode,
                                 LabelField = inMaster.ProductLabelField
                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} inventory items successfully retrieved.", items.Count));
            return items;
        }

        /// <summary>
        /// Method which returns the inventory item associated with the id.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        public InventoryItem GetInventoryItemById(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var inMaster = entities.INMasters.FirstOrDefault(x => x.INMasterID == id);
                    if (inMaster != null)
                    {
                        return new InventoryItem
                        {
                            Master = inMaster,
                            Group = inMaster.INGroup,
                            BoxTareContainer = inMaster.BoxTareContainer,
                            PiecesTareContainer = inMaster.PiecesTareContainer,
                            PalletTareContainer = inMaster.PalletTareContainer,
                            LinkedProductTareContainer = inMaster.Container3,
                            Department = inMaster.Department,
                            DateTemplate = inMaster.DateTemplateName,
                            TraceabilityTemplate = inMaster.AttributeTemplate,
                            QualityTemplate = inMaster.QualityTemplateName,
                            VatCode = inMaster.VATCode,
                            StockMode = inMaster.NouStockMode,
                            LabelField = inMaster.ProductLabelField
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }
        
        /// <summary>
        /// Method which returns the inventory items.
        /// </summary>
        /// <returns>A collection of inventory items.</returns>
        public void GetItemsStock(IList<InventoryItem> inIitems)
        {
            //this.Log.LogDebug(this.GetType(), "GetInventoryItems(): Attempting to retrieve all the inventory items");
         
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var item in inIitems)
                    {
                        var data = entities.GetProductStock(item.Master.INMasterID).FirstOrDefault();
                        if (data != null)
                        {
                            item.StockQty = data.Qty.ToDecimal();
                            item.StockWgt = data.Weight.ToDecimal();
                            item.OutstandingStockQty = data.OutstandingQty.ToDecimal();
                            item.OutstandingStockWgt = data.OutstandingWgt.ToDecimal();

                            if (item.StockQty < 0)
                            {
                                item.StockQty = 0;
                            }

                            if (item.OutstandingStockQty < 0)
                            {
                                item.OutstandingStockQty = 0;
                            }

                            if (item.StockWgt < 0)
                            {
                                item.StockWgt = 0;
                            }

                            if (item.OutstandingStockWgt < 0)
                            {
                                item.OutstandingStockWgt = 0;
                            }

                            item.AvailableStockQty = item.StockQty - item.OutstandingStockQty;
                            item.AvailableStockWgt = item.StockWgt - item.OutstandingStockWgt;

                            if (item.AvailableStockQty < 0)
                            {
                                item.AvailableStockQty = 0;
                            }

                            if (item.AvailableStockWgt < 0)
                            {
                                item.AvailableStockWgt = 0;
                            }
                        }
                        else
                        {
                            item.StockQty = 0;
                            item.StockWgt = 0;
                            item.OutstandingStockQty = 0;
                            item.OutstandingStockWgt = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Method which returns the inventory item product data.
        /// </summary>
        /// <returns>A collection of inventory epos items data.</returns>
        public IList<ProductData> GetEposItems()
        {
            this.Log.LogDebug(this.GetType(), "GetEposItems((): Attempting to retrieve all the inventory epos items");
            var items = new List<ProductData>();
           
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var basePriceList =
                        entities.PriceLists.FirstOrDefault(
                            x => x.PriceListID == Settings.Default.BasePriceListId);

                    if (basePriceList == null)
                    {
                        this.Log.LogError(this.GetType(), string.Format("No price list found matching Settings.Default.BasePricelistId:{0}", Settings.Default.BasePriceListId));
                        return items;
                    }

                    items = (from inMaster in entities.INMasters
                             join priceDetail in entities.PriceListDetails
                             on inMaster.INMasterID equals priceDetail.INMasterID
                             where inMaster.Deleted == null && ((inMaster.ActiveFrom <= DateTime.Today && inMaster.ActiveTo >= DateTime.Today)
                               || !inMaster.ActiveFrom.HasValue || !inMaster.ActiveTo.HasValue)
                              && (!(inMaster.InActiveFrom <= DateTime.Today && inMaster.InActiveTo >= DateTime.Today)
                                  || !inMaster.InActiveFrom.HasValue || !inMaster.InActiveTo.HasValue)
                                  && !priceDetail.Deleted && priceDetail.PriceListID == basePriceList.PriceListID
                            orderby inMaster.Name
                        select new ProductData
                        {
                            Id = inMaster.INMasterID,
                            GroupId = inMaster.INGroupID,
                            Description = inMaster.Name.Trim(),
                            Price = priceDetail.Price,
                            Code = inMaster.Code,
                            Name = inMaster.Name,
                            MaxWeight = inMaster.MaxWeight,
                            MinWeight = inMaster.MinWeight,
                            NominalWeight = inMaster.NominalWeight,
                            TypicalPieces = inMaster.TypicalPieces,
                            ItemImage = inMaster.ItemImage,
                            VatRate = inMaster.VATCode != null ? (decimal)inMaster.VATCode.Percentage : 0
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} inventory items successfully retrieved.", items.Count));
            return items;
        }

        /// <summary>
        /// Method that retrieves the last 100 inventorys.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public IList<InventoryItem> GetRecentInventoryItems()
        {
            this.Log.LogDebug(this.GetType(), "GetRecentInventoryItems(): Attempting to retrieve the last 100 inventory items");

            IList<InventoryItem> items = new List<InventoryItem>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                     items = (from inMaster in entities.INMasters
                             select new InventoryItem
                             {
                                 Master = inMaster,
                                 Group = inMaster.INGroup,
                                 BoxTareContainer = inMaster.BoxTareContainer,
                                 PiecesTareContainer = inMaster.PiecesTareContainer,
                                 PalletTareContainer = inMaster.PalletTareContainer,
                                 Department = inMaster.Department,
                                 DateTemplate = inMaster.DateTemplateName,
                                 TraceabilityTemplate = inMaster.AttributeTemplate,
                                 QualityTemplate = inMaster.QualityTemplateName,
                                 VatCode = inMaster.VATCode,
                                 StockMode = inMaster.NouStockMode
                             })
                             .OrderByDescending(x => x.Master.INMasterID)
                             .Take(100)
                             .ToList();

                    this.Log.LogDebug(this.GetType(), "Last 100 inventory items have been successfully retrieved");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return items;
        }

        /// <summary>
        /// Adds a new specification.
        /// </summary>
        /// <param name="spec">The spec to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        public bool AddOrUpdateProductSpecification(ProductionSpecification spec)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    // add
                    if (spec.INMasterSpecificationID == 0)
                    {
                        var localSpec = new INMasterSpecification
                        {
                            Name = spec.Name,
                            INMasterID = spec.INMasterID,
                            CuttingSpec = spec.CuttingSpec,
                            PackingSpec = spec.PackingSpec,
                            SpecImage = spec.SpecImage,
                            EditDate = DateTime.Now
                        };

              
                        entities.INMasterSpecifications.Add(localSpec);
                        entities.SaveChanges();

                        foreach (var specProduct in spec.Partners)
                        {
                            entities.INMasterSpecificationLookUps.Add(new INMasterSpecificationLookUp
                            {
                                BPMasterID = specProduct,
                                INMasterSpecificationID = localSpec.INMasterSpecificationID,
                                CreationDate = DateTime.Now,
                                UserMasterID = NouvemGlobal.UserId,
                                DeviceMasterID = NouvemGlobal.DeviceId
                            });

                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        // update
                        var dbSpec = entities.INMasterSpecifications.FirstOrDefault(x =>
                            x.INMasterSpecificationID == spec.INMasterSpecificationID);
                        if (dbSpec != null)
                        {
                            dbSpec.Name = spec.Name;
                            dbSpec.CuttingSpec = spec.CuttingSpec;
                            dbSpec.PackingSpec = spec.PackingSpec;
                            dbSpec.SpecImage = spec.SpecImage;
                            dbSpec.EditDate = DateTime.Now;
                        }

                        var lookups = entities.INMasterSpecificationLookUps.Where(x =>
                            x.Deleted == null && x.INMasterSpecificationID == spec.INMasterSpecificationID);
                        foreach (var inMasterSpecificationLookUp in lookups)
                        {
                            inMasterSpecificationLookUp.Deleted = DateTime.Now;
                        }

                        entities.SaveChanges();
                        foreach (var specProduct in spec.Partners)
                        {
                            entities.INMasterSpecificationLookUps.Add(new INMasterSpecificationLookUp
                            {
                                BPMasterID = specProduct,
                                INMasterSpecificationID = spec.INMasterSpecificationID,
                                CreationDate = DateTime.Now,
                                UserMasterID = NouvemGlobal.UserId,
                                DeviceMasterID = NouvemGlobal.DeviceId
                            });

                            entities.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets a collection of specs linked to a product.
        /// </summary>
        /// <param name="inmasterid">The product id.</param>
        /// <returns>A collection of specs linked to a product.</returns>
        public IList<ProductionSpecification> GetsProductSpecifications(int inmasterid)
        {
            var specs = new List<ProductionSpecification>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    specs =
                        (from dbSpec in entities.INMasterSpecifications.Where(x =>
                                x.Deleted == null && x.INMasterID == inmasterid)
                            select new ProductionSpecification
                            {
                                INMasterSpecificationID = dbSpec.INMasterSpecificationID,
                                Name = dbSpec.Name,
                                INMasterID = dbSpec.INMasterID,
                                CuttingSpec = dbSpec.CuttingSpec,
                                PackingSpec = dbSpec.PackingSpec,
                                SpecImage = dbSpec.SpecImage
                            }).ToList();

                    foreach (var inMasterSpecification in specs)
                    {
                        inMasterSpecification.Partners = entities.INMasterSpecificationLookUps.Where(x =>
                            x.Deleted == null && x.INMasterSpecificationID ==
                            inMasterSpecification.INMasterSpecificationID).Select(x => (int)x.BPMasterID).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return specs;
        }

        /// <summary>
        /// Gets a collection of specs linked to a product.
        /// </summary>
        /// <returns>A collection of specs linked to a product.</returns>
        public IList<Sale> GetsSearchProductSpecifications()
        {
            var specs = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    specs =
                        (from dbSpec in entities.INMasterSpecifications.Where(x =>
                                x.Deleted == null)
                            select new Sale
                            {
                                SaleID = dbSpec.INMasterSpecificationID,
                                Name = dbSpec.Name,
                                INMasterID = dbSpec.INMasterID
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return specs;
        }

        /// <summary>
        /// Gets the last edited spc.
        /// </summary>
        /// <returns>The last edited spec.</returns>
        public ProductionSpecification GetsProductSpecificationById(int id)
        {
            var spec = new ProductionSpecification();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbSpec = entities.INMasterSpecifications.FirstOrDefault(x => x.INMasterSpecificationID == id);
                    if (dbSpec != null)
                    {
                        spec = new ProductionSpecification
                        {
                            INMasterSpecificationID = dbSpec.INMasterSpecificationID,
                            Name = dbSpec.Name,
                            INMasterID = dbSpec.INMasterID,
                            CuttingSpec = dbSpec.CuttingSpec,
                            PackingSpec = dbSpec.PackingSpec,
                            SpecImage = dbSpec.SpecImage
                        };
                    }

                    spec.Partners = entities.INMasterSpecificationLookUps.Where(x =>
                        x.Deleted == null && x.INMasterSpecificationID ==
                        spec.INMasterSpecificationID).Select(x => (int)x.BPMasterID).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return spec;
        }

        /// <summary>
        /// Gets the last edited spc.
        /// </summary>
        /// <returns>The last edited spec.</returns>
        public ProductionSpecification GetsProductSpecificationLastEdit()
        {
            var spec = new ProductionSpecification();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbSpec = entities.INMasterSpecifications.OrderByDescending(x => x.EditDate).FirstOrDefault();
                    if (dbSpec != null)
                    {
                        spec = new ProductionSpecification
                        {
                            INMasterSpecificationID = dbSpec.INMasterSpecificationID,
                            Name = dbSpec.Name,
                            INMasterID = dbSpec.INMasterID,
                            CuttingSpec = dbSpec.CuttingSpec,
                            PackingSpec = dbSpec.PackingSpec,
                            SpecImage = dbSpec.SpecImage
                        };
                    }

                    spec.Partners = entities.INMasterSpecificationLookUps.Where(x =>
                        x.Deleted == null && x.INMasterSpecificationID ==
                        spec.INMasterSpecificationID).Select(x => (int)x.BPMasterID).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return spec;
        }

        /// <summary>
        /// Gets the product/partner spec.
        /// </summary>
        /// <returns>Gets the product/partner spec.</returns>
        public ProductionSpecification GetsProductSpecificationByPartner(int inmasterid, int bpmasterid)
        {
            var spec = new ProductionSpecification();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    spec = (from pspec in entities.INMasterSpecifications
                        join speclookup in entities.INMasterSpecificationLookUps
                            on pspec.INMasterSpecificationID equals speclookup.INMasterSpecificationID
                        where pspec.INMasterID == inmasterid && speclookup.BPMasterID == bpmasterid &&
                              speclookup.Deleted == null
                        select new ProductionSpecification
                        {
                            INMasterSpecificationID = pspec.INMasterSpecificationID,
                            Name = pspec.Name,
                            INMasterID = pspec.INMasterID,
                            CuttingSpec = pspec.CuttingSpec,
                            PackingSpec = pspec.PackingSpec,
                            SpecImage = pspec.SpecImage
                        }).FirstOrDefault();

                    if (spec != null)
                    {
                        spec.Partners = entities.INMasterSpecificationLookUps.Where(x =>
                            x.Deleted == null && x.INMasterSpecificationID ==
                            spec.INMasterSpecificationID).Select(x => (int)x.BPMasterID).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return spec;
        }

        /// <summary>
        /// Method that retrieves the last 100 business partners.
        /// </summary>
        /// <returns>A collection of business partners.</returns>
        public IList<ViewBusinessPartner> GetRecentBusinessPartners()
        {
            this.Log.LogDebug(this.GetType(), "GetBusinessPartners(): Attempting to retrieve the last 100 business partners");

            IList<ViewBusinessPartner> partners = new List<ViewBusinessPartner>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localPartners = entities.ViewBusinessPartners.OrderByDescending(x => x.BPMasterID).Take(100);
                    this.Log.LogDebug(this.GetType(), "GetBusinessPartners(): Last 100 business partners have been successfully retrieved");

                    foreach (var partner in localPartners)
                    {
                        partners.Add(partner);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return partners;
        }

        /// <summary>
        /// Gets the last edited group.
        /// </summary>
        /// <returns>The last edited group.</returns>
        public INGroup GetInventoryGroupLastEdit()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.INGroups.OrderByDescending(x => x.EditDate).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
           
            return null;
        }

        /// <summary>
        /// Method which returns the non deleted inventory groups.
        /// </summary>
        /// <returns>A collection of non deleted inventory groups.</returns>
        public IList<INGroup> GetInventoryGroups()
        {
            this.Log.LogDebug(this.GetType(), "GetInventoryGroups(): Attempting to retrieve all the inventory groups");
            var groups = new List<INGroup>();

            try
            {
                //using (var entities = new NouvemEntities())
               // {
                var entities = new NouvemEntities();
                groups = entities.INGroups.Where(x => !x.Deleted).ToList();
               // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetBPProperties(): {0} inventory groups properties successfully retrieved.", groups.Count));
            return groups;
        }

        /// <summary>
        /// Method which returns the non deleted item master properties.
        /// </summary>
        /// <returns>A collection of non deleted item master properties.</returns>
        public IList<INProperty> GetItemMasterProperties()
        {
            this.Log.LogDebug(this.GetType(), "GetItemMasterProperties(): Attempting to retrieve all the item master properties");
            var properties = new List<INProperty>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties = entities.INProperties.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetBPProperties(): {0} item master properties successfully retrieved.", properties.Count));
            return properties;
        }

        /// <summary>
        /// Method which returns the product linked to the input linked tare..
        /// </summary>
        /// <returns>A linked product id.</returns>
        public INMaster GetLinkedProductTareProduct(int tareId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var linkedProduct = entities.INMasters.FirstOrDefault(x => x.TareProductContainerID == tareId && x.Deleted == null);
                    return linkedProduct;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
            
            return null;
        }

        /// <summary>
        /// Gets the next product code in the db.
        /// </summary>
        /// <returns>The next code.</returns>
        public int? GetNextProductCode()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetNextProductCode().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Add or updates the properties list.
        /// </summary>
        /// <param name="properties">The containers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateProperties(IList<INProperty> properties)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateProperties(): Attempting to update the properties");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties.ToList().ForEach(x =>
                    {
                        if (x.INPropertyID == 0)
                        {
                            // new
                            var newProperty = new INProperty
                            {
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.INProperties.Add(newProperty);
                        }
                        else
                        {
                            // update
                            var dbProperty =
                                entities.INProperties.FirstOrDefault(
                                    property => property.INPropertyID == x.INPropertyID && !property.Deleted);

                            if (dbProperty != null)
                            {
                                dbProperty.Name = x.Name;
                                dbProperty.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Properties successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates the groups list.
        /// </summary>
        /// <param name="groups">The groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateInventoryGroups(IList<INGroup> groups)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateGroups(): Attempting to update the groups");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    groups.ToList().ForEach(x =>
                    {
                        if (x.INGroupID == 0)
                        {
                            // new
                            var newGroup = new INGroup
                            {
                                Name = x.Name,
                                ParentInGroupID = x.ParentInGroupID,
                                AttributeTemplateID = x.AttributeTemplateID,
                                SortIndex = 100000,
                                EditDate = DateTime.Now,
                                Deleted = false
                            };

                            entities.INGroups.Add(newGroup);
                        }
                        else
                        {
                            // update
                            var dbGroup =
                                entities.INGroups.FirstOrDefault(
                                    group => group.INGroupID == x.INGroupID && !group.Deleted);

                            if (dbGroup != null)
                            {
                                dbGroup.Name = x.Name;
                                dbGroup.ParentInGroupID = x.ParentInGroupID.IsNullOrZero() ? null : x.ParentInGroupID;
                                dbGroup.AttributeTemplateID = x.AttributeTemplateID;
                                dbGroup.EditDate = DateTime.Now;
                                dbGroup.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Groups successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Add or updates an inventory group.
        /// </summary>
        /// <param name="group">The group to add or update.</param>
        /// <returns>An update inventory group, or null if an issue.</returns>
        public INGroup AddOrUpdateInventoryGroup(INGroup group)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateGroup(): Attempting to update an inventory group");

            try
            {
                using (var entities = new NouvemEntities())
                {
                        if (group.INGroupID == 0)
                        {
                            // new
                            group.EditDate = DateTime.Now;
                            entities.INGroups.Add(group);
                            entities.SaveChanges();
                            this.Log.LogDebug(this.GetType(), "Group successfully added");
                            return group;
                        }

                        // update
                        var dbGroup =
                                entities.INGroups.FirstOrDefault(
                                    x => x.INGroupID == group.INGroupID && !group.Deleted);

                        if (dbGroup != null)
                        {
                            var currentTemplateId = dbGroup.AttributeTemplateID;
                            dbGroup.Name = group.Name;
                            dbGroup.EditDate = DateTime.Now;
                            dbGroup.ParentInGroupID = group.ParentInGroupID != 0 ? group.ParentInGroupID : null;
                            dbGroup.AttributeTemplateID = group.AttributeTemplateID;
                            dbGroup.Deleted = group.Deleted;

                            // Update the directly dependant
                            var inMasters = entities.INMasters.Where(x => x.INGroupID == group.INGroupID && x.AttributeTemplateID == currentTemplateId);

                            if (inMasters.Any())
                            {
                                foreach (var product in inMasters)
                                {
                                    product.AttributeTemplateID = group.AttributeTemplateID;
                                }

                                entities.SaveChanges();
                            }

                            entities.SaveChanges();
                            this.Log.LogDebug(this.GetType(), "Group successfully updated");
                        }

                    return dbGroup;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }
        }

        /// <summary>
        /// Method that removes an attachment from the database.
        /// </summary>
        /// <param name="attachment">The attachment to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool DeleteAttachment(INAttachment attachment)
        {
            this.Log.LogDebug(this.GetType(), string.Format("DeleteAttachment(): attempting to remove attachment with id {0}", attachment.INAttachmentID));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAttachment =
                        entities.INAttachments.FirstOrDefault(x => x.INAttachmentID.Equals(attachment.INAttachmentID));

                    if (dbAttachment != null)
                    {
                        entities.INAttachments.Remove(dbAttachment);
                        entities.SaveChanges();

                        this.Log.LogDebug(this.GetType(), string.Format("Attachment with id {0} successfully removed.", attachment.INAttachmentID));
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return false;
        }

        /// <summary>
        /// Method which returns the non deleted vat codes.
        /// </summary>
        /// <returns>A collection of non deleted vat codes.</returns>
        public IList<VATCode> GetVATCodes()
        {
            this.Log.LogDebug(this.GetType(), "GetVATCodes(): Attempting to retrieve all the vat codes");
            var codes = new List<VATCode>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    codes = entities.VATCodes.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} vat codes successfully retrieved.", codes.Count));
            return codes;
        }

        /// <summary>
        /// Method which returns the non deleted vat codes.
        /// </summary>
        /// <returns>A collection of non deleted vat codes.</returns>
        public IList<Vat> GetVat()
        {
            this.Log.LogDebug(this.GetType(), "GetVat(): Attempting to retrieve all the vat codes");
            var codes = new List<Vat>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    codes = (from code in entities.VATCodes
                        where !code.Deleted
                        select new Vat
                        {
                            VatCodeId = code.VATCodeID,
                            Code = code.Code,
                            Description = code.Description,
                            Percentage = code.Percentage
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} vat codes successfully retrieved.", codes.Count));
            return codes;
        }

        /// <summary>
        /// Add or updates the vat codes list.
        /// </summary>
        /// <param name="vatCodes">The vat codes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateVatCodes(IList<VATCode> vatCodes)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateVatCodes(): Attempting to update the vat codes");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    vatCodes.ToList().ForEach(x =>
                    {
                        if (x.VATCodeID == 0)
                        {
                            // new
                            var newVatCode = new VATCode
                            {
                                Code = x.Code,
                                Description = x.Description,
                                Percentage = x.Percentage,
                                Deleted = false
                            };

                            entities.VATCodes.Add(newVatCode);
                        }
                        else
                        {
                            // update
                            var dbVatCode =
                                entities.VATCodes.FirstOrDefault(
                                    vatCode => vatCode.VATCodeID == x.VATCodeID && !vatCode.Deleted);

                            if (dbVatCode != null)
                            {
                                dbVatCode.Code = x.Code;
                                dbVatCode.Description = x.Description;
                                dbVatCode.Percentage = x.Percentage;
                                dbVatCode.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Vat codes successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Swaps 2 products sort index.
        /// </summary>
        /// <param name="productA">Product A.</param>
        /// <param name="productB">Product B.</param>
        /// <returns>Flag, as to successful swap or not.</returns>
        public bool SwapSortIndexes(InventoryItem productA, InventoryItem productB)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_SwapProductSortIndexes(productA.INMasterID, productB.INMasterID, productA.SortIndex, productB.SortIndex);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }

        /// <summary>
        /// Swaps 2 products groups sort index.
        /// </summary>
        /// <param name="groupA">Group A.</param>
        /// <param name="groupB">Group B.</param>
        /// <returns>Flag, as to successful swap or not.</returns>
        public bool SwapGroupSortIndexes(INGroup groupA, INGroup groupB)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_SwapProductGroupSortIndexes(groupA.INGroupID, groupB.INGroupID, groupA.SortIndex, groupB.SortIndex);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }

        /// <summary>
        /// Add or updates the vat codes list.
        /// </summary>
        /// <param name="vatCodes">The vat codes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateVat(IList<Vat> vatCodes)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateVat(): Attempting to update the vat codes");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    vatCodes.ToList().ForEach(x =>
                    {
                        if (x.VatCodeId == 0)
                        {
                            // new
                            var newVatCode = new VATCode
                            {
                                Code = x.Code,
                                Description = x.Description,
                                Percentage = x.Percentage,
                                Deleted = false
                            };

                            entities.VATCodes.Add(newVatCode);
                        }
                        else
                        {
                            // update
                            var dbVatCode =
                                entities.VATCodes.FirstOrDefault(
                                    vatCode => vatCode.VATCodeID == x.VatCodeId && !vatCode.Deleted);

                            if (dbVatCode != null)
                            {
                                dbVatCode.Code = x.Code;
                                dbVatCode.Description = x.Description;
                                dbVatCode.Percentage = x.Percentage;
                                dbVatCode.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Vat codes successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Adds or updates departments..
        /// </summary>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool AddOrUpdateDepartments(IList<DepartmentLookup> departments)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var departmentLookup in departments)
                    {
                        var dbDepartment =
                            entities.Departments.FirstOrDefault(x => x.DepartmentID == departmentLookup.DepartmentID);

                        if (dbDepartment == null)
                        {
                            entities.Departments.Add(new Department{Code = departmentLookup.Code, Name = departmentLookup.DepartmentName});
                        }
                        else
                        {
                            dbDepartment.Code = departmentLookup.Code;
                            dbDepartment.Name = departmentLookup.DepartmentName;
                            dbDepartment.Deleted = departmentLookup.Deleted;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }

        /// <summary>
        /// Method which returns the non deleted departmentlook ups.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        public IList<DepartmentLookUp> GetDepartmentLookUps(int deviceID)
        {
            var departments = new List<DepartmentLookUp>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    departments = entities.DepartmentLookUps.Where(x => x.DeviceMasterID == deviceID && x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
  
            return departments;
        }

        /// <summary>
        /// Method which returns the non deleted departments.
        /// </summary>
        /// <returns>A collection of non deleted departments.</returns>
        public IList<Department> GetDepartments()
        {
            this.Log.LogDebug(this.GetType(), "GetDepartments(): Attempting to retrieve all the departments");
            var departments = new List<Department>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    departments = entities.Departments.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} departments successfully retrieved.", departments.Count));
            return departments;
        }

        /// <summary>
        /// Method which updates the price m,ethods.
        /// </summary>
        /// <returns>Flag, as to successful update or not..</returns>
        public bool UpdateSearchItems(IList<InventoryItem> products)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var inventoryItem in products)
                    {
                        var dbProduct =
                            entities.INMasters.FirstOrDefault(x => x.INMasterID == inventoryItem.Master.INMasterID);
                        if (dbProduct != null && inventoryItem.Master.NouPriceMethodID.HasValue)
                        {
                            dbProduct.NouPriceMethodID = inventoryItem.Master.NouPriceMethodID;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        public IList<INAttachment> GetInventoryAttachments()
        {
            this.Log.LogDebug(this.GetType(), "GetInventoryAttachments(): Attempting to retrieve all the inventory attachments");
            var attachments = new List<INAttachment>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    attachments = entities.INAttachments.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} inventory attachments successfully retrieved.", attachments.Count));
            return attachments;
        }

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        public IList<INAttachment> GetInventoryAttachments(int inmasterId)
        {
            this.Log.LogDebug(this.GetType(), "GetInventoryAttachments(): Attempting to retrieve all the inventory attachments");
            var attachments = new List<INAttachment>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    attachments = entities.INAttachments.Where(x => !x.Deleted && x.INMasterID == inmasterId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} inventory attachments successfully retrieved.", attachments.Count));
            return attachments;
        }

        /// <summary>
        /// Method which returns the non deleted inventory attachments.
        /// </summary>
        /// <returns>A collection of non deleted inventory attachments.</returns>
        public GetLinkedProduct_Result2 GetLinkedProduct(string serial)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetLinkedProduct(): Attempting to linked stock data for serial:{0}", serial));
        
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var result = entities.GetLinkedProduct(serial).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
           
            return null;
        }

        /// <summary>
        /// Method which returns the inventory property selections.
        /// </summary>
        /// <returns>A collection of inventory properties.</returns>
        public IList<INPropertySelection> GetINPropertySelections()
        {
            this.Log.LogDebug(this.GetType(), "GetINPropertySelections(): Attempting to retrieve all the inventory property selections");
            var properties = new List<INPropertySelection>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties = entities.INPropertySelections.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("GetTypes(): {0} inventory property selections successfully retrieved.", properties.Count));
            return properties;
        }
        
        /// <summary>
        /// Method which returns the non deleted inventory properties.
        /// </summary>
        /// <returns>A collection of non deleted inventory properties.</returns>
        public IList<INProperty> GetINProperties()
        {
            this.Log.LogDebug(this.GetType(), "GetINProperties(): Attempting to retrieve all the inventory properties");
            List<INProperty> properties;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    properties = entities.INProperties.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} inventory properties successfully retrieved.", properties.Count));
            return properties;
        }

        /// <summary>
        /// Method which returns the non deleted carcass split products.
        /// </summary>
        /// <returns>A collection of non deleted carcass split products.</returns>
        public IList<CarcassSplitOption> GetCarcassSplitProducts()
        {
            this.Log.LogDebug(this.GetType(), "GetCarcassSplitProducts(): Attempting to retrieve all the carcass spli products");
            
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.CarcassSplitOptions.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Method which returns the transaction types.
        /// </summary>
        /// <returns>A collection of transaction types.</returns>
        public IList<NouTransactionType> GetTransactionTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetTransactionTypes(): Attempting to retrieve all the transaction types");
            var types = new List<NouTransactionType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    types = entities.NouTransactionTypes.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} transaction types successfully retrieved.", types.Count));
            return types;
        }

        /// <summary>
        /// Method which returns the stock modes.
        /// </summary>
        /// <returns>A collection of stock modes.</returns>
        public IList<NouStockMode> GetStockModes()
        {
            this.Log.LogDebug(this.GetType(), "GetStockModes(): Attempting to retrieve all the stock modes");
            var types = new List<NouStockMode>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    types = entities.NouStockModes.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} stock modes successfully retrieved.", types.Count));
            return types;
        }

        /// <summary>
        /// Determines if there are any transactions recorded against the input product.
        /// </summary>
        /// <returns>Flag, as to whether there are transactions recorded against the input product.</returns>
        public bool AreTransactionsRecordedAgainstProduct(int productId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AreTransactionsRecordedAgainstProduct(): Attempting to determine if there are transactions recorded against product id:{0}", productId));
            var transactionsRecorded = false;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    transactionsRecorded = entities.StockTransactions.Any(x => x.INMasterID == productId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} transactions recorded:{0}", transactionsRecorded));
            return transactionsRecorded;
        }

        /// <summary>
        /// Retrieves the input product stock data.
        /// </summary>
        /// <param name="productId">The product to retrieve the stock data for.</param>
        /// <returns>A collection of stock data.</returns>
        public IList<ProductStockData> GetProductStockData(int productId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the stock data for product Id:{0}", productId));
            var productStockData = new List<ProductStockData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    productStockData = (from stockData in entities.App_GetStockByProduct(productId)
                                        select new ProductStockData { ProductStock = stockData }).ToList();

                    //ProductStockData.CommittedStockQty = entities.AROrderDetails
                    //    .Where(detail => detail.AROrder != null && detail.INMasterID == productId && detail.AROrder.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                    //    .Sum(x => x.QuantityOrdered);

                    //ProductStockData.CommittedStockWgt = entities.AROrderDetails
                    //    .Where(detail => detail.AROrder != null && detail.INMasterID == productId && detail.AROrder.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                    //    .Sum(x => x.WeightOrdered);

                    ProductStockData.StockQty = decimal.Round(productStockData.Sum(x => x.ProductStock.Qty).ToDecimal(), 5);
                    ProductStockData.StockWgt = decimal.Round(productStockData.Sum(x => x.ProductStock.Wgt).ToDecimal(), 5);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return productStockData;
        }

        /// <summary>
        /// Retrieves the input product stock data.
        /// </summary>
        /// <param name="productId">The product to retrieve the stock data for.</param>
        /// <returns>A collection of stock data.</returns>
        public IList<App_GetMRPData_Result> GetMRPData(int productId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetMRPData(productId).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the product data between the input dates.
        /// </summary>
        /// <param name="start">The start date.</param>
        /// <param name="end">The end date.</param>
        public IList<App_ReportProductData_Result> GetProductData(int customerId, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetProductData(): Retrieving the product data between:{0}-{1}", start, end));
            var data = new List<App_ReportProductData_Result>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    data = entities.App_ReportProductData(customerId, start, end).ToList();
                    this.Log.LogDebug(this.GetType(), "Data retrieved");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }
    }
}
